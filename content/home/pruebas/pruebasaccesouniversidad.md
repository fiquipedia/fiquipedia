---
aliases:
  - /home/pruebasaccesouniversidad/
---

# Pruebas Acceso Universidad (PAU)

* [PAU Física](/home/pruebas/pruebasaccesouniversidad/paufisica)  
* [PAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica)  
* [PAU Mecánica (LOGSE)](/home/pruebas/pruebasaccesouniversidad/pau-mecanica) 
* [PAU Electrotecnia (LOE, desparece con LOMCE)](/home/pruebas/pruebasaccesouniversidad/pau-electrotecnia) 
* [PAU Tecnología Industrial (LOE-LOMCE, desaparece con LOMLOE)](/home/pruebas/pruebasaccesouniversidad/pau-tecnologiaindustrial) 
* [PAU Ciencias Generales (surge con LOMLOE)](/home/pruebas/pruebasaccesouniversidad/pau-cienciasgenerales) 
* [PAU Tecnología e Ingeniería II (surge con LOMLOE)](/home/pruebas/pruebasaccesouniversidad/pau-tecnologiaeingenieria) 
* [Recursos PAU](/home/recursos/recursospau)  
* [Recursos PAU mayores 25 años](/home/recursos/recursospau/recursos-pau-mayores-25-anos)  


**Se usa el nombre "LOE" que es PAU, también se siguen conociendo como selectividad**  
*2016: con LOMCE se aprueba Real Decreto 310/2016 según el cual la PAU deja de existir en 2017, por lo que se crea una nueva página aparte, [Evaluaciones Finales Bachillerato](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-bachillerato). Las evaluaciones finales se paralizan en 2016 y se derogan definitivamente en 2020, y la estructura sigue siendo muy similar*  
*2021: LOMLOE se supone que cambiarán de nuevo, de momento información junto a [Evaluación LOMLOE](/home/legislacion-educativa/lomloe/evaluacion)*  
La posible normativa asociada al acceso a la universidad se pone junto a la legislación educativa.  
Las pruebas son distintas según cada comunidad autónoma, inicialmente me centro en la Comunidad de Madrid que es la que conozco, y a las materias más ligadas a la especialidad de Física y Química.  
Las pruebas de acceso originales completas también se incluyen: no tienen derechos de autor, y algunas pueden tener el logotipo de alguna universidad, logotipo que puede no estar o ser de otra universidad distinta en otra prueba.  
Es importante tener claro que las pruebas son comunes para todas las universidades públicas, como se ve en el enunciado "UNIVERSIDADES PÚBLICAS DE LA COMUNIDAD DE MADRID"  
Las pruebas de acceso han cambiado con el tiempo, e implica cambios de estructura.  
Actualmente se llaman "PRUEBA DE ACCESO A LAS ENSEÑANZAS UNIVERSITARIAS OFICIALES DE GRADO" y hay dos opciones completas distintas.  
Antiguamente era la selectividad o "PRUEBAS DE ACCESO A LOS ESTUDIOS UNIVERSITARIOS DE LOS ALUMNOS DE BACHILLERATO LOGSE" y no había dos opciones completas distintas sino opcionalidad parcial entre cuestiones y opciones distintas de preguntas.  
En general me centro en PAU asociada a acceso desde Bachillerato, aunque también hay otros accesos.  
En [comunidad.madrid servicios educacion acceso-universidad](https://www.comunidad.madrid/servicios/educacion/acceso-universidad) se mencionan: 
- [Mayores de 25](https://www.comunidad.madrid/servicios/educacion/acceso-universidad-mayores-25-anos)
- [Acceso desde FP Grado Superior](https://www.comunidad.madrid/servicios/educacion/acceso-universidad-formacion-profesional-grado-superior)
- [Acceso para titulados universitarios](https://www.comunidad.madrid/servicios/educacion/acceso-universidad-titulados-universitarios)
- [Acceso desde estudios extranjeros](https://www.comunidad.madrid/servicios/educacion/acceso-universidad-estudios-extranjeros)
- [Acceso para mayores 40 y 45 años](https://www.comunidad.madrid/servicios/educacion/acceso-universidad-mayores-40-45-anos)
- Discapacidad y NEE, deportistas y univ. para mayores
- Grados con pruebas específicas de acceso


También puede haber información asociada a [Pruebas evaluaciones internacionales](/home/pruebas/pruebas-evaluaciones-internacionales) donde puede haber pruebas de acceso a universidad e otros países.  
[La EvAU de Singapur - masideas-menoscuentas.com](https://masideas-menoscuentas.com/2018/02/17/la-evau-de-singapur/)  

[Reforma de la Selectividad – Comparativa internacional - masideas-menoscuentas.com](https://masideas-menoscuentas.com/2022/08/31/reforma-de-la-selectividad-comparativa-internacional/)  


   * [Provas e Exames. Instituto de Avaliação Educativa, I.P. (IAVE)](https://iave.pt/provas-e-exames/provas-e-exames/)  
      * [Provas e Exames finais nacionais Ensino Secundário](https://iave.pt/provas-e-exames/provas-e-exames/provas-e-exames-finais-nacionais-es/)  
      * [Exame Final Nacional de Física e Química A Prova 715 | 1.ª Fase | Ensino Secundário | 2022 11.º Ano de Escolaridade](https://iave.pt/wp-content/uploads/2022/06/EX-FQA715-F1-2022-V1_net.pdf)  
   * [Esame di Stato, Ministero dell'Istruzione](https://www.istruzione.it/esame_di_stato/index.shtml)  
   * [Weiterentwicklung der Abschlussprüfung im Fach Physik (Realschule) - isb.bayern.de/](https://www.isb.bayern.de/realschule/faecher/mathematik-naturwissenschaften/physik/materialien/weiterentwicklung-abschlusspruefung-physik/)  

El tema de las pruebas para extranjeros vía UNED es para documentar aparte  

[ Preparan una denuncia contra la UNED por anular 81 exámenes de selectividad en EEUU - theobjective](https://theobjective.com/espana/tribunales/2023-06-14/demanda-uned-selectividad-seattle/)  
El centro no explica la razón que ha llevado a no corregir las pruebas en Seattle y defienden que se trata de un «incidencia técnica» que no aclaran  

[Una "incidencia técnica" anula 81 exámenes de acceso universitario de estudiantes españoles en el extranjero - 20minutos](https://www.20minutos.es/tecnologia/actualidad/uned-incidencia-tecnica-anula-81-examenes-selectividad-extranjero-seattle-5138084/)  
La UNED avisó que los alumnos deberían volver a repetir sus pruebas del 15 al 16 de junio, sin embargo, muchos se han negado por los costes del desplazamiento y algunos padres amenazan con denunciar.   

[La odisea de los españoles que hicieron la Selectividad en EE UU: “El examen fue en la cafetería de Microsoft” - elpais](https://elpais.com/educacion/2023-06-22/la-odisea-de-los-espanoles-que-hicieron-la-selectividad-en-ee-uu-el-examen-fue-en-la-cafeteria-de-microsoft.html)  
Una estudiante madrileña asegura haber gastado 3.000 euros en ir a Seattle y repetir las pruebas de la EVAU que la UNED canceló por “problemas técnicos”  

[Lo que pasó realmente en Seattle para que la UNED anulara 81 exámenes de selectividad - elconfidencial](https://www.elconfidencial.com/tecnologia/ciencia/2023-06-23/uned-seattle-examen-anulado-selectividad-ebau_3670571)  
Desde la universidad pública a distancia, que organiza el acceso a la universidad para quienes estudian en el extranjero, aludieron a un "error técnico". Fue bastante peor que eso  

