
# Teluro

[ Z = 52, telurio, Te. Un calcógeno en extinción. An. Quím., 115 (2), 2019, 114](https://analesdequimica.es/index.php/AnalesQuimica/article/view/1430)  

RAE admite [telurio](https://dle.rae.es/telurio) y [teluro](https://dle.rae.es/teluro) 

En [Nomenclatura de Química Inorgánica. Recomendaciones de la IUPAC de 2005. Una adaptación del Libro Rojo a bachillerato (2011-2014) Salvador Olivares, IES Floridablanca](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/formulacion/2011-Nomenclatura%20de%20Qu%C3%ADmica%20Inorg%C3%A1nica.%20Recomendaciones%20de%20la%20IUPAC%20de%202005.%20Una%20adaptaci%C3%B3n%20del%20Libro%20Rojo%20a%20bachillerato.pdf) se indica 

> El Te (grupo 16) puede llamarse teluro o telurio, y el Ta (grupo 5

En [Nomenclatura de Química Inorgánica. Recomendaciones de la IUPAC de 2005](http://old.iupac.org/publications/books/author/RedBook-spanish.html) traducción 2005 libro rojo al castellano aparece una vez como "teluro" en 

> bis(cianato-N)(ciclopentil)(metil)teluro (de adición)

En [Resumen de las normas IUPAC 2005 de nomenclatura de Química Inorgánica para su uso en enseñanza secundaria y recomendaciones didácticas. Errores](https://rseq.org/wp-content/uploads/2018/09/4-Errores.pdf) se cita que 

> ... De teluro (telurio también para la Real Academia Española o RAE) deriva telururo, nunca «teleniuro».
