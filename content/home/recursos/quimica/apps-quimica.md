
# Apps Química

Aparte de recursos de simulaciones, existen apps específicas de química, que son interesantes ya que es fácil para los alumnos acceder a ellas y usarlas.  
(De momento se ponen las gratuitas)  
Algunas inicialmente se han podido colocar dentro de algún recurso (por ejemplo apps sobre tabla periódica o sobre  [química orgánica](/home/recursos/quimica/quimica-organica) , sobre  [formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion) ), pero se intentan centralizar aquí generales y categorías, y enlazar desde otros recursos a esta página  
También puede haber apps asociadas a visualización moléculas en recursos  [geometría molecular](/home/recursos/quimica/geometria-molecular)  o también asociadas a  [realidad virtual](/home/recursos/recursos-realidad-virtual)   

## Apps Química Generales  


* Chemistry Toolbox, Turvy  [https://play.google.com/store/apps/details?id=com.turvy.pocketchemistry](https://play.google.com/store/apps/details?id=com.turvy.pocketchemistry)  (gran cantidad de herramientas: tabla periódica, indicadores, RMN, ...)  
* Chemistry Lab, ProsoLAB [https://play.google.com/store/apps/details?id=com.josemip.chemistry.lab](https://play.google.com/store/apps/details?id=com.josemip.chemistry.lab)  (básicamente ajustar reacciones, castellano)  
* Chemistry 101  [https://play.google.com/store/apps/details?id=air.info.airination.air.mobile.android.chemistry4mobile](https://play.google.com/store/apps/details?id=air.info.airination.air.mobile.android.chemistry4mobile)  
Chemistry 101 is an educational application that allows you to learn more about the Chemical Elements and the Periodic Table of Elements. Here's how it works: The chemical symbol for a random element from the Periodic Table is displayed. You type the name of this element (spelling counts) and click the 'Lookup' button to see if you are correct. The Element's atomic number, group, and period are displayed. An image of that Element is also displayed. Chemistry 101 also plays a dynamic MP3 (teaching the pronunciation of each element). Clicking the 'Element Details' tab allows you to view even more details about an element. Clicking the 'Try Another' button randomly selects another chemical symbol to learn about next.

Una aplicación Tactilium no está operativa en 2021, veo referencias antiguas y en su momento estaba para android
[Tactillium rises to the top](https://waghostwriter.com/28165/features/tactillium-rises-to-the-top/)  
Existía una aplicación "Chemist Free- Virtual Chem Lab, THIX" que tampoco está operativa y hay referencias [Chemist - Virtual Chem Lab by THIX](https://gfx-tools.com/chemist-virtual-chem-lab.html)  
Quizá ambas ahora son Beaker (asociada a THIX); se enlaza Beaker en esta página.  

## Apps Química formulación  
[Quimify - resolver formulación química](https://play.google.com/store/apps/details?id=com.quimify)  
Entrada vía teclado y con imagen  

##  Apps Química Orgánica formulación
* [Organic Chemistry Nomenclature, Next-gen EduChem - apkpure.com](https://apkpure.com/organic-chemistry-nomenclature/o.chemquiz)  
* Hidrocarburos: Estructuras, Andrey Solovyev  [https://play.google.com/store/apps/details?id=com.asmolgam.hydrocarbon](https://play.google.com/store/apps/details?id=com.asmolgam.hydrocarbon)  
* [Apps Android de Química Orgánica - quimicaorganica.org](https://www.quimicaorganica.org/164-indices/determinacion-estructural/835-apps-android-de-quimica-organica.html)  
   * [Química Orgánica: aplicación que contiene gran parte del contenido mostrado en quimicaorganica.org](https://play.google.com/store/apps/details?id=com.net.quimicaorganica&hl=es)  
   * [Nomenclatura Química Orgánica: aplicación con teoría y ejercicios interactivos para nombrar compuestos orgánicos.](https://play.google.com/store/apps/details?id=german.nomenclatura_organica&hl=es)  

##  Apps Química Orgánica mecanismos, reacciones
* [**Chemistry Lab**, Electrolytic Earth](https://play.google.com/store/apps/details?id=com.electrolyticearth.chemistrylab)  (mecanismos con lecciones gráficas y "táctiles") Permite aprender mecanismos de orgánica con interfaz visual e interactiva  
![](https://play-lh.googleusercontent.com/6Mm0IhzcZmRWRQSjkBWz6YHrx646oDg0SmUmxXqnxtU-LdtptLvxAHjXx7Y-QyaeUBA=w2560-h1440)  
* [BEAKER](http://thix.co/beaker)  
**BEAKER** turns your device into, well, a beaker where you can explore 150+ chemicals anywhere, anytime. With the mind-blowing physics simulation, you can shake it up, light up a match, or even tilt your device to dump everything out, without worrying about making a mess.  
Originalmente para android e iOS, ya solo en iOS pero se puede descargar app android de otros repositorios.  
![](https://images.squarespace-cdn.com/content/v1/561e0af6e4b06fe5135b6550/1610149050144-F341741CR50ED4FK79ED/0.png?format=500w)  
* [Organic Chemistry Visualized, Budgietainment](https://apkpure.com/organic-chemistry-3d/com.budgietainment.oc3d/download)  (animaciones para ver geometría moléculas y mecanismos)  
* [Organic Reactions, Turvy (inglés)](https://play.google.com/store/apps/details?id=com.turvy.organicreaction)  (detalle de muchos mecanismos, muchas reacciones orgánicas organizadas por grupos funcionales: síntesis, reacciones)  
![](https://play-lh.googleusercontent.com/vgBZkXKYL1P7ThrtxD9WWajGQbADV2en9ogDyI6yekkv6h05_lL-Ei6Wr-35v_5qEg=w526-h296)  
* [Unreal Chemist - Chemistry Lab](https://play.google.com/store/apps/details?id=com.PixelMiller.UnrealChemist)  
![](https://play-lh.googleusercontent.com/cuo8cBdQo7jqXQFdZhwRAXdEQbqnWAWjdXXXOGqTz8RB8wW6sGVDCwj2k4AUfCCXHaQ=w526-h296)  



