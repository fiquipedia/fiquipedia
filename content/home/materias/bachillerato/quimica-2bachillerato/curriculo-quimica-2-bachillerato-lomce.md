# Currículo Química 2º Bachillerato (LOMCE)

Ver DECRETO 52/2015 en  [legislación](/home/legislacion-educativa)  que indica   
  
*Artículo 9 Materias y currículo*  
El currículo del Bachillerato en los centros docentes de la Comunidad de Madrid se establece del modo siguiente:  
a) Materias del bloque de asignaturas troncales: Los contenidos, criterios de evaluación y estándares de aprendizaje evaluables de las materias del bloque de asignaturas troncales son los del currículo básico fijados para dichas materias en el Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.   
La Administración educativa de la Comunidad de Madrid podrá complementar los contenidos del bloque de materias troncales.  
  
La Comunidad de Madrid no ha complementado contenidos por lo que aplica el Real Decreto 1105/2014  
El pdf del RD 1105/2014 son 378 páginas y no tiene índice: Química 2º de Bachillerato en páginas 279 a 283.  
  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver Contenidos currículo con enlaces a recursos: Química 2º Bachillerato (LOMCE) (pendiente)  

## Comparación para Química de 2º Bachillerato de currículo LOMCE con  [currículo LOE](/home/materias/bachillerato/quimica-2bachillerato/curriculoquimica-2bachillerato) 

La tabla inicialmente la hago para LOMCE vs LOE, pero la muevo a página separada para incluir en comparación LOMLOE

Ver [comparación currículo Quimica 2º Bachillerato](/home/materias/bachillerato/quimica-2bachillerato/comparacion-curriculo-quimica-2-bachillerato)

## Currículo Química 2º Bachillerato condicionado por laboratorio, TIC y simulaciones

El currículo cita a veces laboratorio y simulaciones, por lo que si no hay laboratorio (por ejemplo por falta de desdoble es habitual) o no hay posibilidad de un aula de informática para todos los alumnos, hay ciertas partes del currículo que no se pueden cubrir, y se citan aquí. Puede enlazar con [proyectos de investigación](/home/recursos/proyectos-de-investigacion) a los que se puede asociar TIC para la búsqueda de información y para la defensa.  

Bloque 1. La actividad científica

2.1. **Utiliza el material e instrumentos de laboratorio** empleando las normas de seguridad adecuadas para la **realización de diversas experiencias químicas**.   

4.3. **Localiza y utiliza aplicaciones y programas de simulación de prácticas de laboratorio**.   

4.4. **Realiza y defiende un trabajo de investigación utilizando las TIC**.  

Bloque 2. Origen y evolución de los componentes del Universo

13.2. Conoce y **explica algunas aplicaciones** de los semiconductores y superconductores analizando su repercusión en el avance tecnológico de la sociedad.   

Bloque 3. Reacciones químicas 

4.2. Comprueba e interpreta **experiencias de laboratorio** donde se ponen de manifiesto los factores que influyen en el desplazamiento del equilibrio químico, tanto en equilibrios homogéneos como heterogéneos.   

13.1. **Describe el procedimiento para realizar una volumetría ácido-base** de una disolución de concentración desconocida, realizando los cálculos necesarios.   

15.1. Determina la concentración de un ácido o base **valorándola** con otra de concentración conocida estableciendo el punto de equivalencia de la neutralización mediante el empleo de indicadores ácido-base.   

19.2. **Diseña una pila** conociendo los potenciales estándar de reducción, utilizándolos para calcular el potencial generado formulando las semirreacciones redox correspondientes.   

20.1. **Describe el procedimiento para realizar una volumetría redox** realizando los cálculos estequiométricos correspondientes.   


## Introducción
La Química es una ciencia que profundiza en el conocimiento de los principios fundamentales de la naturaleza, amplía la formación científica de los estudiantes y les proporciona una herramienta para la comprensión del mundo en que se desenvuelven, no solo por sus repercusiones directas en numerosos ámbitos de la sociedad actual sino también por su relación con otros campos del conocimiento como la Biología, la Medicina, la Ingeniería, la Geología, la Astronomía, la Farmacia o la Ciencia de los Materiales, por citar algunos.  
La Química es capaz de utilizar el conocimiento científico para identificar preguntas y obtener conclusiones a partir de pruebas, con la finalidad de comprender y ayudar a tomar decisiones sobre el mundo natural y los cambios que la actividad humana producen en él; ciencia y tecnología están hoy en la base del bienestar de la sociedad.   
Para el desarrollo de esta materia se considera fundamental relacionar los contenidos con otras disciplinas y que el conjunto esté contextualizado, ya que su aprendizaje se facilita mostrando la vinculación con nuestro entorno social y su interés tecnológico o industrial. El acercamiento entre la ciencia en Bachillerato y los conocimientos que se han de tener para poder comprender los avances científicos y tecnológicos actuales contribuye a que los individuos sean capaces de valorar críticamente las implicaciones sociales que comportan dichos avances, con el objetivo último de dirigir la sociedad hacia un futuro sostenible.   
La Química es una ciencia experimental y, como tal, el aprendizaje de la misma conlleva una parte teórico-conceptual y otra de desarrollo práctico que implica la realización de experiencias de laboratorio así como la búsqueda, análisis y elaboración de información.   
El uso de las Tecnologías de la Información y de la Comunicación como herramienta para obtener datos, elaborar la información, analizar resultados y exponer conclusiones se hace casi imprescindible en la actualidad. Como alternativa y complemento a las prácticas de laboratorio, el uso de aplicaciones informáticas de simulación y la búsqueda en internet de información relacionada fomentan la competencia digital del alumnado, y les hace más partícipes de su propio proceso de aprendizaje.   
Los contenidos se estructuran en 4 bloques, de los cuales el primero (La actividad científica) se configura como transversal a los demás. En el segundo de ellos se estudia la estructura atómica de los elementos y su repercusión en las propiedades periódicas de los mismos. La visión actual del concepto del átomo y las subpartículas que lo conforman contrasta con las nociones de la teoría atómico-molecular conocidas previamente por los alumnos. Entre las características propias de cada elemento destaca la reactividad de sus átomos y los distintos tipos de enlaces y fuerzas que aparecen entre ellos y, como consecuencia, las propiedades fisicoquímicas de los compuestos que pueden formar.   
El tercer bloque introduce la reacción química, estudiando tanto su aspecto dinámico (cinética) como el estático (equilibrio químico). En ambos casos se analizarán los factores que modifican tanto la velocidad de reacción como el desplazamiento de su equilibrio. A continuación se estudian las reacciones ácido-base y de oxidación-reducción, de las que se destacan las implicaciones industriales y sociales relacionadas con la salud y el medioambiente. El cuarto bloque aborda la química orgánica y sus aplicaciones actuales relacionadas con la química de polímeros y macromoléculas, la química médica, la química farmacéutica, la química de los alimentos y la química medioambiental.  
  
## Bloque 1. La actividad científica

### Contenidos
Utilización de estrategias básicas de la actividad científica.  
Investigación científica: documentación, elaboración de informes, comunicación y difusión de resultados.  
Importancia de la investigación científica en la industria y en la empresa.  

### Criterios de evaluación
1. Realizar interpretaciones, predicciones y representaciones de fenómenos químicos a partir de los datos de una investigación científica y obtener conclusiones.   
2. Aplicar la prevención de riesgos en el laboratorio de química y conocer la importancia de los fenómenos químicos y sus aplicaciones a los individuos y a la sociedad.   
3. Emplear adecuadamente las TIC para la búsqueda de información, manejo de aplicaciones de simulación de pruebas de laboratorio, obtención de datos y elaboración de informes.   
4. Diseñar, elaborar, comunicar y defender informes de carácter científico realizando una investigación basada en la práctica experimental.  

### Estándares de aprendizaje evaluables
1.1. Aplica habilidades necesarias para la investigación científica: trabajando tanto individualmente como en grupo, planteando preguntas, identificando problemas, recogiendo datos mediante la observación o experimentación, analizando y comunicando los resultados y desarrollando explicaciones mediante la realización de un informe final.   
2.1. Utiliza el material e instrumentos de laboratorio empleando las normas de seguridad adecuadas para la realización de diversas experiencias químicas.   
3.1. Elabora información y relaciona los conocimientos químicos aprendidos con fenómenos de la naturaleza y las posibles aplicaciones y consecuencias en la sociedad actual.   
4.1. Analiza la información obtenida principalmente a través de Internet identificando las principales características ligadas a la fiabilidad y objetividad del flujo de información científica.   
4.2. Selecciona, comprende e interpreta información relevante en una fuente información de divulgación científica y transmite las conclusiones obtenidas utilizando el lenguaje oral y escrito con propiedad.   
4.3. Localiza y utiliza aplicaciones y programas de simulación de prácticas de laboratorio.   
4.4. Realiza y defiende un trabajo de investigación utilizando las TIC.  

## Bloque 2. Origen y evolución de los componentes del Universo

### Contenidos
Estructura de la materia. Hipótesis de Planck. Modelo atómico de Bohr.  
Mecánica cuántica: Hipótesis de De Broglie, Principio de Incertidumbre de Heisenberg.  
Orbitales atómicos. Números cuánticos y su interpretación.  
Partículas subatómicas: origen del Universo.  
Clasificación de los elementos según su estructura electrónica: Sistema Periódico.  
Propiedades de los elementos según su posición en el Sistema Periódico: energía de ionización, afinidad electrónica, electronegatividad, radio atómico.  
Enlace químico.  
Enlace iónico.  
Propiedades de las sustancias con enlace iónico.  
Enlace covalente. Geometría y polaridad de las moléculas.  
Teoría del enlace de valencia (TEV) e hibridación  
Teoría de repulsión de pares electrónicos de la capa de valencia (TRPECV)  
Propiedades de las sustancias con enlace covalente.  
Enlace metálico.  
Modelo del gas electrónico y teoría de bandas.  
Propiedades de los metales. Aplicaciones de superconductores y semiconductores.  
Enlaces presentes en sustancias de interés biológico.  
Naturaleza de las fuerzas intermoleculares.  

### Criterios de evaluación
1. Analizar cronológicamente los modelos atómicos hasta llegar al modelo actual discutiendo sus limitaciones y la necesitad de uno nuevo.   
2. Reconocer la importancia de la teoría mecanocuántica para el conocimiento del átomo.   
3. Explicar los conceptos básicos de la mecánica cuántica: dualidad onda-corpúsculo e incertidumbre.   
4. Describir las características fundamentales de las partículas subatómicas diferenciando los distintos tipos.   
5. Establecer la configuración electrónica de un átomo relacionándola con su posición en la Tabla Periódica.   
6. Identificar los números cuánticos para un electrón según en el orbital en el que se encuentre.  
7. Conocer la estructura básica del Sistema Periódico actual, definir las propiedades periódicas estudiadas y describir su variación a lo largo de un grupo o periodo.   
8. Utilizar el modelo de enlace correspondiente para explicar la formación de moléculas, de cristales y estructuras macroscópicas y deducir sus propiedades.   
9. Construir ciclos energéticos del tipo Born-Haber para calcular la energía de red, analizando de forma cualitativa la variación de energía de red en diferentes compuestos.   
10. Describir las características básicas del enlace covalente empleando diagramas de Lewis y utilizar la TEV para su descripción más compleja.   
11. Emplear la teoría de la hibridación para explicar el enlace covalente y la geometría de distintas moléculas.   
12. Conocer las propiedades de los metales empleando las diferentes teorías estudiadas para la formación del enlace metálico.   
13. Explicar la posible conductividad eléctrica de un metal empleando la teoría de bandas.   
14. Reconocer los diferentes tipos de fuerzas intermoleculares y explicar cómo afectan a las propiedades de determinados compuestos en casos concretos.   
15. Diferenciar las fuerzas intramoleculares de las intermoleculares en compuestos iónicos o covalentes.  

### Estándares de aprendizaje evaluables
1.1. Explica las limitaciones de los distintos modelos atómicos relacionándolo con los distintos hechos experimentales que llevan asociados.   
1.2. Calcula el valor energético correspondiente a una transición electrónica entre dos niveles dados relacionándolo con la interpretación de los espectros atómicos.   
2.1. Diferencia el significado de los números cuánticos según Bohr y la teoría mecanocuántica que define el modelo atómico actual, relacionándolo con el concepto de órbita y orbital.   
3.1. Determina longitudes de onda asociadas a partículas en movimiento para justificar el comportamiento ondulatorio de los electrones.   
3.2. Justifica el carácter probabilístico del estudio de partículas atómicas a partir del principio de incertidumbre de Heisenberg.   
4.1. Conoce las partículas subatómicas y los tipos de quarks presentes en la naturaleza íntima de la materia y en el origen primigenio del Universo, explicando las características y clasificación de los mismos.   
5.1. Determina la configuración electrónica de un átomo, conocida su posición en la Tabla Periódica y los números cuánticos posibles del electrón diferenciador.   
6.1. Justifica la reactividad de un elemento a partir de la estructura electrónica o su posición en la Tabla Periódica.   
7.1. Argumenta la variación del radio atómico, potencial de ionización, afinidad electrónica y electronegatividad en grupos y periodos, comparando dichas propiedades para elementos diferentes.   
8.1. Justifica la estabilidad de las moléculas o cristales formados empleando la regla del octeto o basándose en las interacciones de los electrones de la capa de valencia para la formación de los enlaces.   
9.1. Aplica el ciclo de Born-Haber para el cálculo de la energía reticular de cristales iónicos.   
9.2. Compara la fortaleza del enlace en distintos compuestos iónicos aplicando la fórmula de Born-Landé para considerar los factores de los que depende la energía reticular.   
10.1. Determina la polaridad de una molécula utilizando el modelo o teoría más adecuados para explicar su geometría.   
10.2. Representa la geometría molecular de distintas sustancias covalentes aplicando la TEV y la TRPECV.   
11.1. Da sentido a los parámetros moleculares en compuestos covalentes utilizando la teoría de hibridación para compuestos inorgánicos y orgánicos.   
12.1. Explica la conductividad eléctrica y térmica mediante el modelo del gas electrónico aplicándolo también a sustancias semiconductoras y superconductoras.   
13.1. Describe el comportamiento de un elemento como aislante, conductor o semiconductor eléctrico utilizando la teoría de bandas.   
13.2. Conoce y explica algunas aplicaciones de los semiconductores y superconductores analizando su repercusión en el avance tecnológico de la sociedad.   
14.1. Justifica la influencia de las fuerzas intermoleculares para explicar cómo varían las propiedades específicas de diversas sustancias en función de dichas interacciones.   
15.1. Compara la energía de los enlaces intramoleculares en relación con la energía correspondiente a las fuerzas intermoleculares justificando el comportamiento fisicoquímico de las moléculas.  

## Bloque 3. Reacciones químicas

### Contenidos
Concepto de velocidad de reacción.  
Teoría de colisiones  
Factores que influyen en la velocidad de las reacciones químicas.  
Utilización de catalizadores en procesos industriales.  
Equilibrio químico. Ley de acción de masas. La constante de equilibrio: formas de expresarla.  
Factores que afectan al estado de equilibrio: Principio de Le Chatelier.  
Equilibrios con gases.  
Equilibrios heterogéneos: reacciones de precipitación.  
Aplicaciones e importancia del equilibrio químico en procesos industriales y en situaciones de la vida cotidiana.  
Equilibrio ácido-base.  
Concepto de ácido-base.  
Teoría de Brönsted-Lowry.  
Fuerza relativa de los ácidos y bases, grado de ionización.  
Equilibrio iónico del agua.  
Concepto de pH. Importancia del pH a nivel biológico.  
Volumetrías de neutralización ácido-base.  
Estudio cualitativo de la hidrólisis de sales.  
Estudio cualitativo de las disoluciones reguladoras de pH.  
Ácidos y bases relevantes a nivel industrial y de consumo. Problemas medioambientales.  
Equilibrio redox  
Concepto de oxidación-reducción. Oxidantes y reductores. Número de oxidación.  
Ajuste redox por el método del ion-electrón. Estequiometría de las reacciones redox.  
Potencial de reducción estándar.  
Volumetrías redox.  
Leyes de Faraday de la electrolisis.  
Aplicaciones y repercusiones de las reacciones de oxidación reducción: baterías eléctricas, pilas de combustible, prevención de la corrosión de metales.  

### Criterios de evaluación
1. Definir velocidad de una reacción y aplicar la teoría de las colisiones y del estado de transición utilizando el concepto de energía de activación.   
2. Justificar cómo la naturaleza y concentración de los reactivos, la temperatura y la presencia de catalizadores modifican la velocidad de reacción.   
3. Conocer que la velocidad de una reacción química depende de la etapa limitante según su mecanismo de reacción establecido.   
4. Aplicar el concepto de equilibrio químico para predecir la evolución de un sistema.   
5. Expresar matemáticamente la constante de equilibrio de un proceso, en el que intervienen gases, en función de la concentración y de las presiones parciales.   
6. Relacionar Kc y Kp en equilibrios con gases, interpretando su significado.   
7. Resolver problemas de equilibrios homogéneos, en particular en reacciones gaseosas, y de equilibrios heterogéneos, con especial atención a los de disolución-precipitación.   
8. Aplicar el principio de Le Chatelier a distintos tipos de reacciones teniendo en cuenta el efecto de la temperatura, la presión, el volumen y la concentración de las sustancias presentes prediciendo la evolución del sistema.   
9. Valorar la importancia que tiene el principio Le Chatelier en diversos procesos industriales.   
10. Explicar cómo varía la solubilidad de una sal por el efecto de un ion común.   
11. Aplicar la teoría de Brönsted para reconocer las sustancias que pueden actuar como ácidos o bases.   
12. Determinar el valor del pH de distintos tipos de ácidos y bases.   
13. Explicar las reacciones ácido-base y la importancia de alguna de ellas así como sus aplicaciones prácticas.   
14. Justificar el pH resultante en la hidrólisis de una sal.   
15. Utilizar los cálculos estequiométricos necesarios para llevar a cabo una reacción de neutralización o volumetría ácido-base.   
16. Conocer las distintas aplicaciones de los ácidos y bases en la vida cotidiana tales como productos de limpieza, cosmética, etc.   
17. Determinar el número de oxidación de un elemento químico identificando si se oxida o reduce en una reacción química.   
18. Ajustar reacciones de oxidación-reducción utilizando el método del ion-electrón y hacer los cálculos estequiométricos correspondientes.   
19. Comprender el significado de potencial estándar de reducción de un par redox, utilizándolo para predecir la espontaneidad de un proceso entre dos pares redox.   
20. Realizar cálculos estequiométricos necesarios para aplicar a las volumetrías redox.   
21. Determinar la cantidad de sustancia depositada en los electrodos de una cuba electrolítica empleando las leyes de Faraday.   
22. Conocer algunas de las aplicaciones de la electrolisis como la prevención de la corrosión, la fabricación de pilas de distinto tipos (galvánicas, alcalinas, de combustible) y la obtención de elementos puros.  

### Estándares de aprendizaje evaluables
1.1. Obtiene ecuaciones cinéticas reflejando las unidades de las magnitudes que intervienen.   
2.1. Predice la influencia de los factores que modifican la velocidad de una reacción.   
2.2. Explica el funcionamiento de los catalizadores relacionándolo con procesos industriales y la catálisis enzimática analizando su repercusión en el medio ambiente y en la salud.   
3.1. Deduce el proceso de control de la velocidad de una reacción química identificando la etapa limitante correspondiente a su mecanismo de reacción.   
4.1. Interpreta el valor del cociente de reacción comparándolo con la constante de equilibrio previendo la evolución de una reacción para alcanzar el equilibrio.   
4.2. Comprueba e interpreta experiencias de laboratorio donde se ponen de manifiesto los factores que influyen en el desplazamiento del equilibrio químico, tanto en equilibrios homogéneos como heterogéneos.   
5.1. Halla el valor de las constantes de equilibrio, Kc y Kp, para un equilibrio en diferentes situaciones de presión, volumen o concentración.   
5.2. Calcula las concentraciones o presiones parciales de las sustancias presentes en un equilibrio químico empleando la ley de acción de masas y cómo evoluciona al variar la cantidad de producto o reactivo.   
6.1. Utiliza el grado de disociación aplicándolo al cálculo de concentraciones y constantes de equilibrio Kc y Kp.   
7.1. Relaciona la solubilidad y el producto de solubilidad aplicando la ley de Guldberg y Waage en equilibrios heterogéneos sólido-líquido y lo aplica como método de separación e identificación de mezclas de sales disueltas.   
8.1. Aplica el principio de Le Chatelier para predecir la evolución de un sistema en equilibrio al modificar la temperatura, presión, volumen o concentración que lo definen, utilizando como ejemplo la obtención industrial del amoníaco.   
9.1. Analiza los factores cinéticos y termodinámicos que influyen en las velocidades de reacción y en la evolución de los equilibrios para optimizar la obtención de compuestos de interés industrial, como por ejemplo el amoníaco.   
10.1. Calcula la solubilidad de una sal interpretando cómo se modifica al añadir un ion común.   
11.1. Justifica el comportamiento ácido o básico de un compuesto aplicando la teoría de Brönsted-Lowry de los pares de ácido-base conjugados.   
12.1. Identifica el carácter ácido, básico o neutro y la fortaleza ácido-base de distintas disoluciones según el tipo de compuesto disuelto en ellas determinando el valor de pH de las mismas.   
13.1. Describe el procedimiento para realizar una volumetría ácido-base de una disolución de concentración desconocida, realizando los cálculos necesarios.   
14.1. Predice el comportamiento ácido-base de una sal disuelta en agua aplicando el concepto de hidrólisis, escribiendo los procesos intermedios y equilibrios que tienen lugar.   
15.1. Determina la concentración de un ácido o base valorándola con otra de concentración conocida estableciendo el punto de equivalencia de la neutralización mediante el empleo de indicadores ácido-base.   
16.1. Reconoce la acción de algunos productos de uso cotidiano como consecuencia de su comportamiento químico ácido-base.   
17.1. Define oxidación y reducción relacionándolo con la variación del número de oxidación de un átomo en sustancias oxidantes y reductoras.   
18.1. Identifica reacciones de oxidación-reducción empleando el método del ion-electrón para ajustarlas.   
19.1. Relaciona la espontaneidad de un proceso redox con la variación de energía de Gibbs considerando el valor de la fuerza electromotriz obtenida.  
19.2. Diseña una pila conociendo los potenciales estándar de reducción, utilizándolos para calcular el potencial generado formulando las semirreacciones redox correspondientes.   
19.3. Analiza un proceso de oxidación-reducción con la generación de corriente eléctrica representando una célula galvánica.   
20.1. Describe el procedimiento para realizar una volumetría redox realizando los cálculos estequiométricos correspondientes.   
21.1. Aplica las leyes de Faraday a un proceso electrolítico determinando la cantidad de materia depositada en un electrodo o el tiempo que tarda en hacerlo.   
22.1. Representa los procesos que tienen lugar en una pila de combustible, escribiendo la semirreacciones redox, e indicando las ventajas e inconvenientes del uso de estas pilas frente a las convencionales.   
22.2. Justifica las ventajas de la anodización y la galvanoplastia en la protección de objetos metálicos.  

## Bloque 4. Síntesis orgánica y nuevos materiales

### Contenidos
Estudio de funciones orgánicas.  
Nomenclatura y formulación orgánica según las normas de la IUPAC.  
Funciones orgánicas de interés: oxigenadas y nitrogenadas, derivados halogenados tioles peracidos. Compuestos orgánicos polifuncionales.  
Tipos de isomería.  
Tipos de reacciones orgánicas.  
Principales compuestos orgánicos de interés biológico e industrial: materiales polímeros y medicamentos  
Macromoléculas y materiales polímeros.  
Polímeros de origen natural y sintético: propiedades.  
Reacciones de polimerización.  
Fabricación de materiales plásticos y sus transformados: impacto medioambiental.  
Importancia de la Química del Carbono en el desarrollo de la sociedad del bienestar.  

### Criterios de evaluación
1. Reconocer los compuestos orgánicos, según la función que los caracteriza.   
2. Formular compuestos orgánicos sencillos con varias funciones.   
3. Representar isómeros a partir de una fórmula molecular dada.   
4. Identificar los principales tipos de reacciones orgánicas: sustitución, adición, eliminación, condensación y redox.   
5. Escribir y ajustar reacciones de obtención o transformación de compuestos orgánicos en función del grupo funcional presente.   
6. Valorar la importancia de la química orgánica vinculada a otras áreas de conocimiento e interés social.   
7. Determinar las características más importantes de las macromoléculas.   
8. Representar la fórmula de un polímero a partir de sus monómeros y viceversa.   
9. Describir los mecanismos más sencillos de polimerización y las propiedades de algunos de los principales polímeros de interés industrial.   
10. Conocer las propiedades y obtención de algunos compuestos de interés en biomedicina y en general en las diferentes ramas de la industria.   
11. Distinguir las principales aplicaciones de los materiales polímeros, según su utilización en distintos ámbitos.   
12. Valorar la utilización de las sustancias orgánicas en el desarrollo de la sociedad actual y los problemas medioambientales que se pueden derivar.  

### Estándares de aprendizaje evaluables
1.1. Relaciona la forma de hibridación del átomo de carbono con el tipo de enlace en diferentes compuestos representando gráficamente moléculas orgánicas sencillas.   
2.1. Diferencia distintos hidrocarburos y compuestos orgánicos que poseen varios grupos funcionales, nombrándolos y formulándolos.   
3.1. Distingue los diferentes tipos de isomería representando, formulando y nombrando los posibles isómeros, dada una fórmula molecular.   
4.1. Identifica y explica los principales tipos de reacciones orgánicas: sustitución, adición, eliminación, condensación y redox, prediciendo los productos, si es necesario.   
5.1. Desarrolla la secuencia de reacciones necesarias para obtener un compuesto orgánico determinado a partir de otro con distinto grupo funcional aplicando la regla de Markovnikov o de Saytzeff para la formación de distintos isómeros.   
6.1. Relaciona los principales grupos funcionales y estructuras con compuestos sencillos de interés biológico.   
7.1. Reconoce macromoléculas de origen natural y sintético.   
8.1. A partir de un monómero diseña el polímero correspondiente explicando el proceso que ha tenido lugar.   
9.1. Utiliza las reacciones de polimerización para la obtención de compuestos de interés industrial como polietileno, PVC, poliestireno, caucho, poliamidas y poliésteres, poliuretanos, baquelita.   
10.1. Identifica sustancias y derivados orgánicos que se utilizan como principios activos de medicamentos, cosméticos y biomateriales valorando la repercusión en la calidad de vida.   
11.1. Describe las principales aplicaciones de los materiales polímeros de alto interés tecnológico y biológico (adhesivos y revestimientos, resinas, tejidos, pinturas, prótesis, lentes, etc.) relacionándolas con las ventajas y desventajas de su uso según las propiedades que lo caracterizan.   
12.1. Reconoce las distintas utilidades que los compuestos orgánicos tienen en diferentes sectores como la alimentación, agricultura, biomedicina, ingeniería de materiales, energía frente a las posibles desventajas que conlleva su desarrollo.  
 
