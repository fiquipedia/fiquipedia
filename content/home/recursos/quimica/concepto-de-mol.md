
# Concepto de mol

A nivel personal y en los  [apuntes de de elaboración propia de química de 2º de Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato) , dentro de "bloque 0" de repaso de estequiometría tengo un pequeño apartado sobre mol.  
También hay apuntes breves en  [apuntes de elaboración propia de física y química de 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso/apuntes-elaboracion-propia-3-eso)  
También en  [pizarras física y química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel)  para 3 y 4 ESO  
Un resumen muy rápido es que mi visión, un tanto especial, para reforzar la idea de que mol es unidad de cantidad de sustancia, es contar a los alumnos el mol como "una docena muy grande", y que la palabra que mejor sustituye a mol es "cuatrillón de" (en inglés, 602 hexilions)  

Apuntes de FisQuiWeb sobre mol. Distintos apuntes por nivel. Licenciamiento cc-by-nc-sa  
[Fisquiweb - apuntes](http://fisquiweb.es/Apuntes/apuntes.htm)  
[mol 3º ESO](http://fisquiweb.es/Apuntes/Apuntes3/Mol3.pdf)  
[mol 4º ESO](http://fisquiweb.es/Apuntes/Apuntes4/Mol.pdf)  
[mol 1º Bachillerato ](http://fisquiweb.es/Apuntes/Apuntes4/Mol.pdf)  

[twitter CarlosChNav/status/950062969068621824](https://twitter.com/CarlosChNav/status/950062969068621824)  
Que digo yo que para enseñar el mol lo mejor es enseñar algún mol. De cosas que hay en casa, sin complicacionesPero antes de que lo vean calcularán las masas correspondientes.  
[![](https://pbs.twimg.com/media/DS9NNbXW0AENw0z.jpg "") ](https://pbs.twimg.com/media/DS9NNbXW0AENw0z.jpg)  

*CONCEPTOS GENERALES. Autora: Mercedes de la Fuente Rubio. Curso 0 Química UNED, OCW*  
[http://ocw.innova.uned.es/quimicas/](http://ocw.innova.uned.es/quimicas/)  
*Ficha 4 Mol*  
  
[A vueltas con el mol: estrategias para explicar e introducir el concepto en secundaria. An. Quím. 2013, 109(3), 209–212](http://www.rseq.org/documentos/doc_download/552-pag209-212-vol9-03)  
Luis Ignacio García. © 2012 Real Sociedad Española de Química  
  
Introduction to Moles, Tyler DeWitt (vídeo 10 min)  
[![Introduction to Moles, Tyler DeWitt](http://img.youtube.com/vi/wI56mHUDJgQ/0.jpg)](http://www.youtube.com/watch?v=wI56mHUDJgQ "Introduction to Moles, Tyler DeWitt")  

Counting Atoms: Intro to Moles Part 2 (vídeo 10 min)  
[![Counting Atoms: Intro to Moles Part 2](http://img.youtube.com/vi/hY7lzRBylSk/0.jpg)](http://www.youtube.com/watch?v=hY7lzRBylSk "Counting Atoms: Intro to Moles Part 2")  

[How big is a mole? (Not the animal, the other one.) - Daniel Dulek](https://ed.ted.com/lessons/daniel-dulek-how-big-is-a-mole-not-the-animal-the-other-one) (vídeo 4 min)  

[The Mole and Avogadro's Constant chenwiki](http://chemwiki.ucdavis.edu/Physical_Chemistry/Atomic_Theory/The_Mole_and_Avogadro%27s_Constant)  
cc-by-nc-sa  

[Stoichiometry Tutorials: The Mole - chemcollective](http://chemcollective.org/activities/tutorials/stoich/the_mole)  
[Stoichiometry Tutorials: The Mole - chemcollective.oli.cmu.edu ](https://chemcollective.oli.cmu.edu/activities/tutorials/stoich/the_mole)  **url operativa en 2022 cuando dominio org no funcionaba**  

[twitter FiQuiPedia/status/1064493718672211968](https://twitter.com/FiQuiPedia/status/1064493718672211968)  
El universo tiene una edad de unos 14000 millones de años  
14000e6*365,25*24*3600=4,4e17 s  
Si miramos la proporción con mol / nº Avogadro 6,022e23/4,4e17=1,367e6  
Un mol de segundos es una cantidad de segundos mayor que un millón de veces los segundos de edad del universo  

[Just How Big is Avogadro's Number](http://web.archive.org/web/20200708145509/http://www2.ucdsb.on.ca/tiss/stretton/CHEM1/avogad1.html)  waybackmachine   

This is a good way to find out about the size of Avogadro's Number but also to properly deal with scientific notation on a calculator.  
[![](http://www.compoundchem.com/wp-content/uploads/2014/10/Avogadro-The-Mole-723x1024.png "") ](http://www.compoundchem.com/wp-content/uploads/2014/10/Avogadro-The-Mole-723x1024.png) 

[El mol… ¡la explicación REEEEfinitiva! - lacienciaparatodos](https://lacienciaparatodos.wordpress.com/2024/11/16/el-mol-la-explicacion-reeeefinitiva/)  


##   Definición de mol 
Cambia oficialmente a finales 2018, aunque IUPAC la cambia antes  

*El mol en el Sistema Internacional*  
[Draft 9th Brochure 16 December 2013, 2.4.6 The SI unit of amount of substance, the mole](http://www.bipm.org/utils/common/pdf/si_brochure_draft_ch123.pdf)  
  
[mole - goldbook.iupac.org](https://goldbook.iupac.org/terms/view/M03980)  
Definición antigua  

> SI base unit for the amount of substance (symbol: mol). The mole is the amount of substance of a system which contains as many elementary entities as there are atoms in 0.012 kilogram of carbon-12. When the mole is used, the elementary entities must be specified and may be atoms, molecules, ions, electrons, other particles, or specified groups of such particles.  


[A new definition of the mole has arrived. 8 Jan 2018 - iupac.org](https://iupac.org/new-definition-mole-arrived/) 

[mole - goldbook.iupac.org](https://goldbook.iupac.org/terms/view/M03980)  
Nueva definición  
> The mole, symbol mol, is the SI unit of amount of substance. One mole contains exactly 6.022 140 76×1023 elementary entities. This number is the fixed numerical value of the Avogadro constant, NA, when expressed in the unit mol-1 and is called the Avogadro number.  
The amount of substance, symbol n, of a system is a measure of the number of specified elementary entities. An elementary entity may be an atom, a molecule, an ion, an electron, any other particle or specified group of particles.  
Note: The formulation of this definition was agreed upon by the 26th CGPM in November 2018 with effect from 20 May 2019.  

[Avogadro constant - nist](https://physics.nist.gov/cgi-bin/cuu/Value?na)  



##   Simulaciones 
Ejercicios con moles, corrección online  
[IES Al Ándalus Enlaces a aplicaciones informáticas útiles (flash, scripts, etc). elaboradas por José Antonio Navarro.](http://web.archive.org/web/20180705110131/http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=139&Itemid=199) (waybackmachine) 
* [El mol: Cambio de unidades (g, moléculas, etc) paso a paso.](http://web.archive.org/web/20161012235937/http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq3eso/ejercicios_moles_inicial.swf)  (waybackmachine) Para iniciarse en el cambio de unidades con moles usando factores de conversión.  
* [Ejercicios de cambio de unidades de moles (g, moléculas, L, M). Aplicación Flash.](http://web.archive.org/web/20161012125226/http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq4eso/ejercicios_moles.swf) (waybackmachine). Propone ejercicios sobre moles y los va corrigiendo paso a paso. Cada vez, un ejercicio diferente.Jose Antonio Navarro Dominguez  

