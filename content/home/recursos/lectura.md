
# Lectura

Esta página es distinta a la página asociada a [libros](/home/recursos/libros)  

Está pensada asociada a lo que indica normativa, que algunos centros reflejan en un plan de lectura

[LOE Artículo 2. Fines](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a2)  
> 2. Los poderes públicos prestarán una atención prioritaria al conjunto de factores que favorecen la calidad de la enseñanza y, en especial, la cualificación y formación del profesorado, su trabajo en equipo, la dotación de recursos educativos, humanos y materiales, las condiciones ambientales y de salud del centro escolar y su entorno, la investigación, la experimentación y la renovación educativa, **el fomento de la lectura y el uso de bibliotecas**, la autonomía pedagógica, organizativa y de gestión, la función directiva, la orientación educativa y profesional, la inspección educativa y la evaluación

[LOE Artículo 26. Principios pedagógicos (ESO)](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a26)  
> 2. En esta etapa se prestará una atención especial a la adquisición y el desarrollo de las competencias establecidas y se fomentará la correcta expresión oral y escrita y el uso de las matemáticas. **A fin de promover el hábito de la lectura, se dedicará un tiempo a la misma en la práctica docente de todas las materias.**  

[LOE Artículo 33. Objetivos (Bachillerato)](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a33)  
> El bachillerato contribuirá a desarrollar en los alumnos y las alumnas las capacidades que les permitan  
> ...   
> d) Afianzar los hábitos de lectura, estudio y disciplina, como condiciones necesarias para el eficaz aprovechamiento del aprendizaje, y como medio de desarrollo personal.

Los propios enunciados de los ejercicios pueden suponer un ejercicio de lectura comprensiva.

Aparte de intentar recopilar lecturas de libros completos, y de ideas sobre cómo evaluar esa lectura, también está la idea de recopilar lecturas "cortas" y hacer pequeños resúmenes asociados.  
[x fenris1234/status/1816053398396879192](https://x.com/fenris1234/status/1816053398396879192)  
Hace un año y medio empecé a ayudar a mi sobrina con las clases.  
El mayor problema era que no sabia leer.  "Juntar las letras" si pero  entender lo que leía, un desastre.  
Os lo comento porque 2 años después el cambio ha sido brutal, por si os sirve a alguno lo que hemos hecho  
Consulte a @Otrosvendran (¡Gracias!) y me dijo que leyese y escribiese. Parece obvio pero sobre todo lo de hacerles escribir, no lo es tanto.  
Y a eso yo añadi una cosita de mi cosecha.  
Tengo bastantes libros que son recopilaciones de relatos cortos. Historias de 15 o 20 paginas  
**Así que empezamos por ahí. Cada semana del verano, una historia corta.  15, 20 o 30 paginas.**  
**Son cosas completamente alcanzables en una semana, y desaparece el problema del "buf, es que son 300 paginas".**  
Es algo que ellos ven que pueden lograr.  
Además de la historia, le mande cada semana escribir un folio o dos.  Normalmente relacionado con la historia (un resumen, su opinión, un "que pasaría si").  
La ventaja de las historias cortas es que puedes ir cambiándole de estilo.  
Esta semana, ciencia ficción.  La siguiente, novela negra.  Después, historia de terror de Lovercraft.  Y así, puede ir buscando su estilo.   
Y no tiene que sufrir un tocho de 300 paginas si le aburre  
Entiendo que no todo el mundo tiene en casa tantos libros como yo y los relatos cortos no son algo que mucha gente tenga. (y menos, varios libros diferentes)  
Pero una visita al librero o mas barato aún, a la biblioteca publica, puede ayudar.  
Y después, ha entrado en juego el factor social:  las amigas se habían leído "culpa tuya" la comentaban, y nos la pidió.  
Y con ella han caído después los otros 5 libros de la autora.  

_Se hace referencia a la trilogía Culpa Mía, Culpa Tuya, Culpa nuestra de Mercedes Ron_

Que si, que es una novela romántica adolescente que yo no leería ni bajo amenaza de tortura, pero lo importante es ella, no yo.  
Así que preguntad a las amigas que leen.  O que película les gusta, y comprad el libro de la película.  
Porque lo que cuesta es que empiecen.  
Y una vez que empiezan, y que cogen la carrerilla, os aseguro que la cosa cambia.  
Lo de este año ha sido una pasada.  En clase les han metido el quijote (una versión para estudiantes, la sombra del viento, y el tenorio).  Pues con el tenorio, hasta se divirtió.  
Y hace 2 semanas le metí una comedia de Aristófanes sobre unas mujeres que toman el poder en Atenas, y se rio muchísimo con ella.  
Y sobre todo: VAYA CAMBIO EN LA EXPRESION Y EN LA COMPRENSION AL LEER.  
**Por supuesto, esto no es facil.  Requiere tiempo. Y dinero en libros, que a veces alguno ha sido abandonado a las 20 paginas.**  
Todo eso además, acompañado de estudiar con ella cada semana 1 o 2 días CON LOS LIBROS.  
"Léete el párrafo"  
"¿Entero?"  
"Si, entero"  
"Es que eso es la introducción y  no cae"  
"Vale, pero te lo lees y me lo cuentas con tus palabras"  
Y así casi 2 años. Todas las semanas al menos 1 día muchas 2, y algunas, 3.  
¿Es fácil? No.  
Pero joer, como compensa ver el resultado...  
Os lo comento por que igual estabais como estaba yo hace 2 años. Preguntandos que  puñetas podéis hacer.  
Por supuesto cada persona es un mundo. No tengo ni idea de si esto funcionara en otra gente.  
Pero si no sabéis por donde empezar, lo de los relatos cortos quizás os ayude.  
Si no tenéis ni idea de por que libro de relatos cortos empezar, a mi sobrina (adolescente de 16 años) le ha encantado "cuarentena" de Petros Markaris.  
Creo que es un libro ideal  para todo el mundo.  (Y si no les gusta a ellos, al menos creo que os gustara a vosotros.)  

[CIENCIA, y... La gran colección de textos, más de trescientos, de divulgación científica. Licencia Creative Commons. - cienciayelcosmosdelsigloxxi - archive.org](https://web.archive.org/web/20230401225809/https://cienciayelcosmosdelsigloxxi.blogspot.com/p/ciencia-y.html)  




