# PAU Tecnología e Ingeniería

La materia de Tecnología e Ingeniería surge en 2º Bachillerato LOMLOE, ver [Tecnología e Ingeniería 2º de bachillerato](/home/materias/bachillerato/tecnologia-e-ingeneria-2-bachillerato).  
Hay contenidos asociables a Física y Química: de momento se enlazan enunciados, y ya se agruparán por bloques o se asociarán a recopilaciones de ejercicios de ciertos tipos.  
Por ejemplo el primer modelo de EvAU 2024 de Madrid tiene un ejercicio de termodinámica nivel Bachillerato

Se puede ver como una materia que ocupa su lugar de electrotecnia (materia LOE que en LOMCE seguía existiendo en comunidades como Cataluña) y Tecnología Industrial 
Ver [PAU Electrotecnia](/home/pruebas/pruebasaccesouniversidad/pau-electrotecnia) 


##  [Recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales)](/home/recursos/recursospau/ficheros-enunciados-pau-tecnologiaeingenieria) 
