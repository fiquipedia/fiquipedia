
# Recursos ácidos y bases

Desde un punto de vista simple se introducen en 4º ESO, pero se tratan en química de 2º Bachillerato una vez visto equilibrio.  

Ver apuntes bloque ácido base en  [apuntes química 2º Bachillerato de elaboración propia](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato)  

Ver prácticas de laboratorio, donde es habitual realizar una valoracioń ácido base.  

* [Generador ejercicios disociación. Cálculo del pH](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/disociacion_jquery.htm?t=1466098417872) Vicente Martín Morales  
* [Generador ejercicios hidrólisis](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/hidrolisis_jquery.htm?t=1466103151912) Vicente Martín Morales  

[Apuntes ácido base. IES Clara Campoamor](http://fresno.pntic.mec.es/~fgutie6/quimica2/ArchivosPDF/04AcidoBase.pdf)  

[Ácido base - fisiquimicamente.com](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2bach/quimica/acido-base/)  

[Animación ionización ácidos. Flash - mhhe.com (archive.org)](https://web.archive.org/web/20100701152357/http://www.mhhe.com/physsci/chemistry/animations/chang_2e/acid_ionization.swf)  

[Animación ionización bases. Flash - mhhe.com  (archive.org)](https://web.archive.org/web/20100701152411/http://www.mhhe.com/physsci/chemistry/animations/chang_7e_esp/acm2s2_1.swf)  

[Acids, Alkalis, and the pH Scale - compoundchem.com](http://www.compoundchem.com/2015/07/09/ph-scale/)  
La escala no está limitada al rango 0-14, pero es el rango más habitual ....  
[![Escala pH ( rango 0 a 14)](http://www.compoundchem.com/wp-content/uploads/2015/07/Acids-and-Alkalis-The-pH-Scale-724x1024.png "Escala pH ( rango 0 a 14)") ](http://www.compoundchem.com/wp-content/uploads/2015/07/Acids-and-Alkalis-The-pH-Scale-724x1024.png)  

[Scratch Chemist's pH Scale - reddit.com](https://www.reddit.com/r/Infographics/comments/8h22my/scratch_chemists_ph_scale/)  
![](https://preview.redd.it/r13pguzj8wv01.png?width=960&crop=smart&auto=webp&s=c7cf80df5b910a139eeb6e018eda602295f36aff  

[Pobblebonk frogs have a weird trick for surviving very acidic pools - newscientist.com](https://www.newscientist.com/article/2331382-pobblebonk-frogs-have-a-weird-trick-for-surviving-very-acidic-pools/)  
> Highly acidic water usually breaks down a tadpole’s gill lining, but an Australian frog has evolved to suck in more protective calcium from the extreme ecosystem where it lives  


##  Disoluciones amortiguadoras / tampón
 [EFICACIA AMORTIGUADORA - ehu.eus](http://www.ehu.eus/biomoleculas/buffers/efica.htm)  
 [e-ducativa.catedu.es El pH de las disoluciones reguladoras](https://e-ducativa.catedu.es/44700165/aula/archivos/repositorio/4750/4857/html/31_el_ph_de_las_disoluciones_reguladoras.html)  incluye simulación  
 [Ejercicio resuelto pH de un búfer de amoniaco y amonio - ejercicios-fyq.com](https://ejercicios-fyq.com/pH-de-un-bufer-de-amoniaco-y-amonio-6608)  
 [Cálculo del pH de una disolución amortiguadora - es.khanacademy.org](https://es.khanacademy.org/science/ap-chemistry/buffers-titrations-solubility-equilibria-ap/buffer-solutions-tutorial-ap/v/buffer-solution-calculations)  

##  Referencias pK
 [pKa table - chem.wisc.edu](http://www.chem.wisc.edu/areas/reich/pkatable/)  
 [pH calculator » dissociation constants - chembuddy.com](http://www.chembuddy.com/?left=BATE&right=dissociation_constants)  
 [pKa Table - masterorganicchemistry.com](https://cdn.masterorganicchemistry.com/wp-content/uploads/2022/01/Gavin-edited-Masterorg-chem-Pka-table-GM.pdf)  

