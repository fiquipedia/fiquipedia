
# Fuerzas intermoleculares

Enlaza con  [enlace](/home/recursos/quimica/enlace-quimico)  y  [propiedades sustancias](/home/recursos/quimica/recursos-propiedades-sustancias)  

[Fuerzas intermoleculares -fyq1bach.wordpress.com](http://fyq1bach.wordpress.com/2010/03/27/fuerzas-intermoleculares/)  Aida Ivars  

Recurso asociado a polaridad [Polaridad y apolaridad: una experiencia visual y divertida  - quimitube](http://www.quimitube.com/polaridad-y-apolaridad-una-experiencia-visual-y-divertida/) 


##  Fuerzas de Van der Waals
[Fuerzas de Van der Waals - wikipedia](https://es.wikipedia.org/wiki/Fuerzas_de_Van_der_Waals)  

Se suele hablar de dipolo permanente-dipolo permanente (entre polares), dipolo permanente-dipolo inducido (entre polares y apolares) y dipolo instantáneo (entre apolares), pero también se habla de Keesom, Debye y London  

[Fuerzas de Keesom](https://es.wikipedia.org/wiki/Fuerzas_de_Keesom)  
[Keesom interaction](https://www.sciencedirect.com/topics/engineering/keesom-interaction)  
The Keesom interactions are due to the dipole–dipole interaction between two permanent dipoles on a surface.  

[Difference between Keesom, Debye and London forces](https://www.curlyarrows.com/difference-between-keesom-debye-and-london-forces)0

 [Solo hay un material sobre el que los geckos no pueden trepar, y ha inspirado una nueva generación de adhesivos - gizmodo.com](https://es.gizmodo.com/solo-hay-un-material-sobre-el-que-los-geckos-no-pueden-1796847997)  
 
[![](https://img.youtube.com/vi/Wbtps4325Cg/0.jpg)](https://www.youtube.com/watch?v=Wbtps4325Cg "La sustancia no pegajosa más pegajosa")  
Los adhesivos basados en la piel del gecko pueden soportar grandes pesos sin pegarse a nada.   
Veritasium, incluye referencias  

##  Enlaces de hidrógeno
El enlace de hidrógeno a veces se llama puente de hidrógeno, pero parece ser que lo más correcto es enlace de hidrógeno; comentado aquí  [Comentario en post Temarios oposiciones docentes](http://algoquedaquedecir.blogspot.com.es/2017/11/temarios-oposiciones-docentes.html#comments)  
*Aunque está muy extendido el uso de enlaces por puentes de hidrógeno debería decirse enlaces de hidrógeno, al menos en un documento oficial.*  
*Nuestro armonizador de universidad de PAU, PAEU y ahora EBAU es muy estricto al respecto y tiene escrito en el foro de las pruebas textualmente que esos enlaces son ENLACES de hidrógeno y que los puentes de hidrógeno son otra cosa distinta, son enlaces deficientes en electrones que se dan en complejos de metales de transición, en diboranos, etc.*  
Referencias: [Hidrogen bond en IUPAC](https://goldbook.iupac.org/html/H/H02899.html)  
[https://en.wikipedia.org/wiki/Hydrogen_bond](https://en.wikipedia.org/wiki/Hydrogen_bond)   


