
# Recursos Modelo Estándar

En esta página, asociada a física de partículas, intento poner enlaces asociados al modelo estándar, inicialmente diagramas de partículas de modelo estándar  
  
El Modelo Estándar es más que una tabla de partículas, incluye interacciones  
  
 [The deconstructed Standard Model equation | symmetry magazine](http://www.symmetrymagazine.org/article/the-deconstructed-standard-model-equation)   
  
 [Archivo de la etiqueta: modelo-estándar - cuentos-cuanticos.com (archive.org)](https://web.archive.org/web/20120225004207/https://cuentos-cuanticos.com/tag/modelo-estandar/)   
 [Diseño gráfico y visualización científica en física de partículas](https://francisthemulenews.wordpress.com/2012/05/30/diseno-grafico-y-visualizacion-cientifica-en-fisica-de-particulas/)   
  
 [El Modelo Estándar de partículas - fisquiweb](http://fisquiweb.es/Apuntes/Apuntes2Fis/ResModeloEstandar.pdf)   

## Diagramas partículas Modelo Estándar
  
En 2016 creo mi propia versión, en pdf en este enlace  [ModeloEstandar-propio.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/pdf/ModeloEstandar-propio.pdf)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/svg/ModeloEstandar-propio.svg)  
  
 [File:Standard Model Of Particle Physics--Most Complete Diagram.png - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Standard_Model_Of_Particle_Physics--Most_Complete_Diagram.png) , cc-by sa Latham Boyle  
  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Standard_Model_Of_Particle_Physics--Most_Complete_Diagram.png/800px-Standard_Model_Of_Particle_Physics--Most_Complete_Diagram.png)   
  
 [Standardmodell_Uebersicht-2808-F.jpg](http://www.mesa.uni-mainz.de/Illustrationen/Standardmodell_Uebersicht-2808-F.jpg)   
  
 [![http://www.mesa.uni-mainz.de/Illustrationen/Standardmodell_Uebersicht-2808-F.jpg ](http://www.mesa.uni-mainz.de/Illustrationen/Standardmodell_Uebersicht-2808-F.jpg "http://www.mesa.uni-mainz.de/Illustrationen/Standardmodell_Uebersicht-2808-F.jpg ") ](http://www.mesa.uni-mainz.de/Illustrationen/Standardmodell_Uebersicht-2808-F.jpg)   
  
 [Partículas fundamentales del Modelo estándar &#8211; Mola Saber](https://molasaber.org/2016/06/12/particulas-fundamentales-del-modelo-estandar/)   
 [![Partículas fundamentales, molasaber.org](https://molasaber.files.wordpress.com/2016/06/qlb3.png?w=648 "Partículas fundamentales, molasaber.org") ](https://molasaber.files.wordpress.com/2016/06/qlb3.png?w=648)   
  
 [Go on a particle quest at the first CERN webfest - CERN Bulletin](https://cds.cern.ch/journal/CERNBulletin/2012/35/News%20Articles/1473657)   
Dos versiones interesantes   
 [![https://cds.cern.ch/record/1473657/files/ParticleQuestSprites_image.jpeg?subformat=](https://cds.cern.ch/record/1473657/files/ParticleQuestSprites_image.jpeg?subformat= "https://cds.cern.ch/record/1473657/files/ParticleQuestSprites_image.jpeg?subformat=") ](https://cds.cern.ch/record/1473657/files/ParticleQuestSprites_image.jpeg?subformat=)   
  
 [![https://cds.cern.ch/record/1473657/files/SMinfographic_image.png?subformat=](https://cds.cern.ch/record/1473657/files/SMinfographic_image.png?subformat= "https://cds.cern.ch/record/1473657/files/SMinfographic_image.png?subformat=") ](https://cds.cern.ch/record/1473657/files/SMinfographic_image.png?subformat=)   
  
 [UZH > Physik-Institut > Group Serra > More Info > Standard Model ](http://www.physik.uzh.ch/groups/serra/StandardModel.html)   
 [![http://www.physik.uzh.ch/groups/serra/images/SM1.png](http://www.physik.uzh.ch/groups/serra/images/SM1.png "http://www.physik.uzh.ch/groups/serra/images/SM1.png") ](http://www.physik.uzh.ch/groups/serra/images/SM1.png)   
  
Muy interesante, con posters  
 [Periodic Table of the Elements, in Pictures and Words](http://elements.wlonk.com/#Particles)   
 [Particles SM (1 page)](http://elements.wlonk.com/ParticlesSM.pdf)   
  
 [![https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/The_incomplete_circle_of_everything.svg/651px-The_incomplete_circle_of_everything.svg.png](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/The_incomplete_circle_of_everything.svg/651px-The_incomplete_circle_of_everything.svg.png "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/The_incomplete_circle_of_everything.svg/651px-The_incomplete_circle_of_everything.svg.png") ](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/The_incomplete_circle_of_everything.svg/651px-The_incomplete_circle_of_everything.svg.png)   
 [File:The_incomplete_circle_of_everything.svg](https://commons.wikimedia.org/wiki/File:The_incomplete_circle_of_everything.svg)   

## Diagramas interacciones modelo estándar
 [https://twitter.com/PHDcomics/status/865273785741705218](https://twitter.com/PHDcomics/status/865273785741705218)   
 [![https://pbs.twimg.com/media/DAHr_kKXcAE4inG.jpg](https://pbs.twimg.com/media/DAHr_kKXcAE4inG.jpg "https://pbs.twimg.com/media/DAHr_kKXcAE4inG.jpg") ](https://pbs.twimg.com/media/DAHr_kKXcAE4inG.jpg)   
 
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/svg/ResumenParticulasInteracciones-propio.svg)  
[Elementary-particle-interactions-es propio.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/pdf/Elementary-particle-interactions-es%20propio.pdf)  

## Interacciones modelo estándar

En principio electromagnetismo por separado. Aquí en principio enlaces a interacción fuerte y débil. 

Deep dive into the known forces - Fermilab  
[![](https://img.youtube.com/vi/NwbsffunM10/0.jpg)](https://www.youtube.com/watch?v=NwbsffunM10 "Deep dive into the known forces - Fermilab")  

Is the weak nuclear force really a force? - Fermilab  
[![](https://img.youtube.com/vi/RvH0hLaBOTk/0.jpg)](https://www.youtube.com/watch?v=RvH0hLaBOTk "Is the weak nuclear force really a force? - Fermilab")  


## Modelo Estándar + SUSY
  
 [ParticlesSUSY.pdf](http://elements.wlonk.com/ParticlesSUSY.pdf)   
  
 [Five Independent Signs Of New Physics In The Universe](https://www.forbes.com/sites/startswithabang/2016/11/04/five-independent-signs-of-new-physics-in-the-universe/)   
 [![SUSY](https://blogs-images.forbes.com/startswithabang/files/2016/11/susy_spectrum.jpg?width=960 "SUSY") ](https://blogs-images.forbes.com/startswithabang/files/2016/11/susy_spectrum.jpg?width=960)   
  
