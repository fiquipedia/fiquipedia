
# Diversificación Ámbito Científico Tecnológico

Con el cambio de LOE a LOMCE que según calendario empieza en 3º ESO en curso 2015-2016, Diversificación curricular (que eran dos cursos "paralelos" a 3º y 4º ESO) desaparece y pasa a ser Programa de Mejora del Aprendizaje y del Rendimiento (PMAR, dos cursos paralelos a 2º y 3º ESO)   
Con LOMLOE que se implanta en 3ºESO en 2022-2023 y en 4º en 2023-2024 desaparece PMAR y vuelve a ser Diversificación paralelo a 3º y 4º ESO.  
* [Currículo (LOE)](/home/materias/eso/ambitocientificotecnologico-diversificacion/ambitocientificotecnologico-diversificacion-curriculo) 
* [Currículo (LOMLOE)](/home/materias/eso/ambitocientificotecnologico-diversificacion/ambitocientificotecnologico-diversificacion-curriculo-lomloe) 

Ver [FP Básica](/home/materias/fpbasica/) 

## Recursos
Muchas veces comunes a ambos cursos, primero y segundo, que muchas veces se llaman "3º diver" y "4º diver" en lugar de 1º de Diversificación y 2º de Diversificación.  
Pueden enlazar con recursos de algunas de las materias que forman el ámbito, como  
* [recursos de matemáticas](/home/recursos/matematicas)  
* [Superdiver](http://superdiver3.blogspot.com.es/p/indice-actividades-ambito-cientifico.html)  
* [Web de Ángel Prieto Benito](http://platea.pntic.mec.es/apriet3/)   
[Matemáticas APB Web](http://platea.pntic.mec.es/apriet3/matematicas.html)  
[Matemáticas APB Web, descargas](http://platea.pntic.mec.es/apriet3/DescargasMat/descargasmat.html)  
[* CIENCIAS NATURALES 3º DIVERSIFICACIÓN - matematicasonline.es ](https://www.matematicasonline.es/diver/diver_ciencias_1.html)  
* [Ciencias naturales 3º (Ámbito científico-tecnológico) - eureka *waybackmachine*](http://web.archive.org/web/20190310223908/http://blog.educastur.es/eureka/ambito-ct/ccnn-3%C2%BA-version-recuperada/)  

A nivel de recursos está relacionado con los ámbitos en educación de adultos, ver recursos en [Ámbitos](/home/materias/ambitos) 

