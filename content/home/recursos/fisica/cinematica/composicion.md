# Composición de movimientos

Esta es una subpágina de la página de [cinemática](/home/recursos/fisica/cinematica) para descargarla centralizando aquí recursos solo sobre composición de movimientos, que pueden incluir experiencias o [prácticas experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio) asociadas, por lo que puede haber información también, a veces duplicada, en otras páginas.  

El caso más representativo de composición es MRU-MRUA que lleva al movimiento parabólico y se introduce en 1º de Bachillerato (a veces con subtipos como tiro horizontal, tiro oblicuo ...), pero hay otros como MRU-MRU que lleva a un MRU, MRUA-MRUA que lleva a un MRUA, o MCU-MRU que lleva a movimiento helicoidal  

### Movimiento parabólico

[Movimiento de un proyectil - phet.colorad.edu](https://phet.colorado.edu/sims/html/projectile-motion/latest/projectile-motion_es.html)  
![](https://phet.colorado.edu/sims/html/projectile-motion/latest/projectile-motion-900.png)  

[TIRO PARABÓLICO](http://recursostic.educacion.es/descartes/web/materiales_didacticos/comp_movimientos/parabolico.htm) 

[Aprender física es más fácil y divertido con esta web donde el Coyote por fin acaba con el Correcaminos - genbeta](https://www.genbeta.com/web/aprender-fisica-facil-divertido-esta-web-donde-coyote-fin-atrapa-al-correcaminos)  
![](https://i.blogs.es/c55827/captura-de-pantalla-2023-12-18-a-las-13.33.03/1366_2000.webp)  
[Catch The Roadrunner - afreeparticle.com](https://afreeparticle.com/roadrunner.html)  
El Coyote tiene que capturar al Correcaminos lanzándole una red con un tiro parabólico.  

Movimiento de Proyectiles  
[Movimiento de Proyectiles](http://www.walter-fendt.de/html5/phes/projectile_es.htm)  
[Projectile motion, duffy, HTML5](http://physics.bu.edu/~duffy/HTML5/projectile_motion.html)  
[tiro parabólico (simulador) - geogebra](https://www.geogebra.org/m/MZRKudEF)  Alexis Javier Morillo Galván  

Jorge Lay Gajardo.  
[parabola2profisica.swf](http://www.profisica.cl/images/stories/animaciones/parabola2profisica.swf)  
[lanzhorizontal_profisica.swf](http://www.profisica.cl/images/stories/animaciones/lanzhorizontal_profisica.swf) 

[La ciencia que explica el lanzamiento perfecto de un tiro libre en baloncesto - microsiervos](https://www.microsiervos.com/archivo/ciencia/ciencia-lanzamiento-tiro-libre.html)  
![](https://img.microsiervos.com/images2018/LanzamientoBasket.jpg)

[Tiros frontales a canasta - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica/cinematica/baloncesto/BALONCES.htm)  

[La Física del vuelo de Michael Jordan - fogonazos](http://www.fogonazos.es/2014/04/la-fisica-del-vuelo-de-michael-jordan.html)  

[Galileo Got Game: 5 Things You Didn't Know About the Physics of Basketball - wired](https://www.wired.com/2014/04/basketball-physics/)  

En general se pueden citar aquí ideas asociadas a disparos, que pueden llegar a ser muy frikis 

[twitter goorgelas_/status/1079431755717885953](https://twitter.com/goorgelas_/status/1079431755717885953)  
@FiQuiPedia próximo problema de tiro parabólico  
[![](https://pbs.twimg.com/media/DvrpdQiX0AEcpkK.jpg "") ](https://pbs.twimg.com/media/DvrpdQiX0AEcpkK.jpg)  

[twitter FiQuiPedia/status/1279349080053948416](https://twitter.com/FiQuiPedia/status/1279349080053948416)  
Yo veo un problema de tiro parabólico para 1° Bachillerato  
"We note that the maximal height of rocks is h = 2.0 m in the Katsurahama aquarium. In this case, we obtain dmax =1.34 m at θmax = 16.9° and d(θ = 0) = 1.24 m.  
"Projectile Trajectory of Penguin's Faeces and Rectal Pressure Revisited  
[[2007.00926] Projectile Trajectory of Penguin&#39;s Faeces and Rectal Pressure Revisited](https://arxiv.org/abs/2007.00926)  
how far faeceses reach when penguins expel them from higher places. Such information is useful for keepers to avoid the direct hitting of faeceses.  
[![](https://pbs.twimg.com/media/EcBjq0UX0AAgnGJ?format=jpg "") ](https://pbs.twimg.com/media/EcBjq0UX0AAgnGJ?format=jpg)  

[Tear Gun by Yi-Fei Chen](https://www.ignant.com/2017/10/20/tear-gun-by-yi-fei-chen/)  
> During Dutch Design Week in 2016, Taiwanese designer Yi-Fei Chen presented her project named ‘Tear Gun’ that collects and freezes actual tears to shoot them back at the person who caused the cry.  
![](https://www.ignant.com/wp-content/uploads/2017/10/Art_TearGun_Yi-FeiChen_1-720x870.jpg)  

[Tiro parabólico - geogebra](https://www.geogebra.org/m/ag5bVqNF)  
Introduciendo la velocidad inicial y el ángulo de lanzamiento, junto al punto inicial desde el que se produce, observamos la trayectoria del proyectil , el punto más alto y el alcance.  

[Tiro parabólico, simulador - geogebra](https://www.geogebra.org/m/MZRKudEF)  
podrás simular el tiro parabólico de un objeto cambiando algunos parámetros como: altura del lanzamiento, velocidad de salida del objeto, angulo; también podrás visualizar el vector posición y el vector velocidad del objeto.  
Usa los deslizaderos para cambiar los parámetros, mueve el deslizador del tiempo y el objeto se moverá  

[Experiencia de éxito convertida en REA: Tiro Parabólico en la Vida Real - cedec.intef.es](https://cedec.intef.es/experiencias-de-exito-convertidas-en-rea-tiro-parabolico-en-la-vida-real/)  

[La canasta de baloncesto en la que es (casi) imposible fallar: física, robótica y mucho ingenio - xataka](https://www.xataka.com/makers/canasta-baloncesto-que-casi-imposible-fallar-fisica-robotica-mucho-ingenio)  
![](https://i.blogs.es/32cb0f/b0f6ee91-75d4-4b6b-ac9b-707f9f8b0d49/1366_2000.jpeg)  

[EL TIRO DE MESSI - geogebra](https://www.geogebra.org/m/ryaasb3d)  
La aplicación de GeoGebra contiene una imagen del diario Sport (19/03/2019) que informa de un gol de Messi. Se ha intentado que las distancias en el plano que contiene la escena del gol de Messi se aproximen todo lo posible a las medidas reales. Ese plano es el que  incluye al jugador y a la trayectoria de la pelota.  

[![](https://img.youtube.com/vi/Y33bya9tWe0/0.jpg)](https://www.youtube.com/watch?v=Y33bya9tWe0 "I made a HUMAN SLINGSHOT! (World record) - Torp upgrades")  

[twitter a_freeparticle/status/1732826942271803473](https://twitter.com/a_freeparticle/status/1732826942271803473)  
In the immortal words of @hosey_dan "will never get old" 🤩  
Each group gets a horizontal distance from the launcher and has to set the height of their ring of fire.  
This is what I gave them, but we also huddle and discuss certain things to our for and see what the launcher looks like and how fast it shoots (without letting it go anywhere)  
[Ring of fire lab - docs.google](https://docs.google.com/presentation/d/1kWx2waLuq2P7jjbRt-yc0H8rmn8mjI1epdlek174hhs/edit)  
[Vernier Projectile Launcher](https://www.vernier.com/product/vernier-projectile-launcher/)  


