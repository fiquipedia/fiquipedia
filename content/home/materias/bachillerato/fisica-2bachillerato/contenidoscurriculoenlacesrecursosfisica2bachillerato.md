# Contenidos currículo con enlaces a recursos: Física 2º Bachillerato

Esta página está en construcción, es solamente un primer borrador. Pretende conseguir uno de los objetivos puestos en la página inicial de la web.  
Para ver el currículo completo "estático", mirar  [Currículo Física 2º Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculofisica2bachillerato)   
 
### 1. Contenidos comunes.
— Utilización de estrategias básicas del trabajo científico: Planteamiento de problemas y reflexión sobre el interés de los mismos, formulación de hipótesis, estrategias de resolución, diseños experimentales y análisis de resultados y de su fiabilidad.  
— Búsqueda y selección de información; comunicación de resultados utilizando la terminología adecuada.  

### 2.  [Interacción gravitatoria](/home/recursos/fisica/recursos-gravitacion) .
— De las Leyes de Kepler a la Ley de la gravitación universal.  
Momento de una fuerza respecto de un punto y momento angular. Fuerzas centrales y fuerzas conservativas. Energía potencial gravitatoria.  
— La acción a distancia y el concepto físico de campo: El campo gravitatorio. Magnitudes que lo caracterizan: Intensidad de campo y potencial gravitatorio.  
— Campo gravitatorio terrestre. Determinación experimental de g. Movimiento de satélites y cohetes.  

### 3.  [Vibraciones](/home/recursos/fisica/movimiento-oscilatorio)  y  [ondas](/home/recursos/fisica/movimiento-ondulatorio) .
— Movimiento oscilatorio: Movimiento vibratorio armónico simple. Elongación, velocidad, aceleración. Estudio experimental de las oscilaciones de un muelle. Dinámica del movimiento armónico simple. Energía de un oscilador armónico.  
— Movimiento ondulatorio. Tipos de ondas. Magnitudes características de las ondas. Ecuación de las ondas armónicas planas. Aspectos energéticos.  
— Principio de Huygens: Reflexión y refracción. Estudio cualitativo de difracción e interferencias. Ondas estacionarias. Ondas sonoras. Contaminación acústica: Sus fuentes y efectos.  
— Aplicaciones de las ondas al desarrollo tecnológico y a la mejora de las condiciones de vida. Impacto en el medio ambiente.  

### 4. Interacción electromagnética.
— Campo eléctrico. Magnitudes que lo caracterizan: Intensidad de campo y potencial eléctrico. Teorema de Gauss. Aplicación a campos eléctricos creados por un elemento continuo: Esfera, hilo y placa.  
—  [Magnetismo](/home/recursos/fisica/recursos-campo-magnetico)  natural e imanes. Relación entre fenómenos eléctricos y magnéticos. Campos magnéticos creados por corrientes eléctricas. Fuerzas sobre cargas móviles situadas en campos magnéticos. Ley de Lorentz. Interacciones magnéticas entre corrientes rectilíneas. Experiencias con bobinas, imanes, motores, etcétera. Analogías y diferencias entre campos gravitatorio, eléctrico y magnético.  
— Inducción electromagnética. Leyes de Faraday y de Lenz. Producción de energía eléctrica, impacto y sostenibilidad. Energía eléctrica de fuentes renovables.  
— Aproximación histórica a la síntesis electromagnética de Maxwell.  

### 5.  [Óptica](/home/recursos/fisica/recursos-optica) .
— Controversia histórica sobre la naturaleza de la luz: Los modelos corpuscular y ondulatorio. La naturaleza electromagnética de la luz: Espectro electromagnético y espectro visible. Variación de la velocidad de la luz con el medio. Fenómenos producidos con el cambio de medio: Reflexión, refracción, absorción y dispersión.  
— Óptica geométrica. Comprensión de la visión y formación de imágenes en espejos y lentes delgadas. Pequeñas experiencias con las mismas. Construcción de algún instrumento óptico.  
— Estudio cualitativo de la difracción, el fenómeno de interferencias y la dispersión. Aplicaciones médicas y tecnológicas.  

### 6. Introducción a la Física moderna.
— La crisis de la Física clásica. Principios fundamentales de la relatividad especial. Repercusiones de la teoría de la relatividad. Variación de la masa con la velocidad y equivalencia entre masa y energía.  
— Efecto fotoeléctrico y espectros discontinuos: Insuficiencia de la Física clásica para explicarlos. Hipótesis de Planck. Cuantización de la energía. Hipótesis de De Broglie.  [Dualidad onda corpúsculo](/home/recursos/fisica/recursos-fisica-cuantica/dualidad-onda-corpusculo) . Relaciones de indeterminación. Aportaciones de la Física moderna al desarrollo científico y tecnológico.  
— Física nuclear: Composición y estabilidad de los núcleos. Energía de enlace. Radiactividad. Tipos, repercusiones y aplicaciones. Reacciones nucleares de fisión y fusión, aplicaciones y riesgos.  


