# Comparación currículo Química 2º Bachillerato

La tabla inicialmente la hago para LOMCE vs LOE, pero la muevo a página separada para incluir en comparación LOMLOE

Esta tabla la inicio como un borrador y está en evolución; pongo fecha de última revisión.  

En la columna LOE / LOMLOE se indica el currículo para la comunidad de Madrid, a veces no idéntico al RD estatal. En LOMCE los estándares de aprendizaje y parte del currículo no variaba entre el estatal y autonómico.  

LOMCE:   
Cuando en una de las dos columnas indico "se cita xxx" suele querer decir que se cita en ese caso (LOE ó LOMCE) pero no en el anterior. A veces indico "se citaba", "se añade", "desaparece" ...  
En la columna LOE se indica el currículo para la comunidad de Madrid, a veces no idéntico al RD estatal.  

LOMLOE: comienzo a poner ideas con borrador estatal de octubre 2021  
Considero 4 sesiones semanales como en LOE y LOMCE. "Anexo IV. Horario escolar, expresado en horas, correspondiente a las 
enseñanzas mínimas para el Bachillerato." indica 87,5 horas, y considerando 175 días lectivos según [disposición adicional quinta LOE](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#daquinta) (aunque es para enseñanza obligatoria) lleva a 35 semanas (aparte de que 2º Bachillerato sea un curso más corto), por lo que serían 2,5 sesiones a la semana, pero lo que indica la normativa estatal es mínimo.  
Al publicarse [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) en ANEXO I Organización del primer curso de Bachillerato página 19 del pdf se indican 4 horas semanales.  
En junio 2022 (todavía sin currículos autonómicos) se publica un documento que realiza una comparativa  
[Física y Química en la LOMLOE: Una mirada al nuevo currículo de ESO y Bachillerato (Luis Moreno, Almudena de la Fuente y Alejandro Rodríguez‐Villamil), 4-14, Boletín informativo del grupo especializado en Didáctica e Historia de la Física y la Química, común a las Reales Sociedades Españolas de Física y de Química, Nº 37, Junio 2022](https://gedh.rseq.org/wp-content/uploads/2022/06/Boletin-37-GEDH-red.pdf)  

La cabecera de las tablas lleva a cada uno de los currículos  

**Tabla revisada 2 agosto 2022**  
  
| **[LOE](/home/materias/bachillerato/quimica-2bachillerato/curriculoquimica-2bachillerato)** | **[LOMCE](/home/materias/bachillerato/quimica-2bachillerato/curriculo-quimica-2-bachillerato-lomce)**   | **[LOMLOE](/home/materias/bachillerato/quimica-2bachillerato/curriculo-quimica-2-bachillerato-lomloe)** |
|:-|:-|:-| 
|  4 sesiones semanales   |  4 sesiones semanales | 4 sesiones semanales | 
|  Materia de modalidad en la modalidad de bachillerato de Ciencias y Tecnología |  Materia troncal en la modalidad de bachillerato de Ciencias   | Materia de modalidad dentro de la modalidad de Bachillerato de Ciencias y Tecnología |
|  8 objetivos (se usa número 1 a 8) asociados a la materia   |  [14 objetivos de etapa](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a25)  (se usa letra desde a hasta n)  | [15 objetivos de etapa](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a7) (se usa letra desde a hasta o)  |
|  8 bloques de contenidos fijados en decreto comunidad   |  4 bloques de contenidos complementables por la comunidad (Madrid no los ha modificado)   | 12 bloques de **saberes básicos** (A: 1, 2, 3, 4, B: 1, 2, 3, 4, 5 y C: 1, 2, 3, 4) (C1 añadido por Madrid) |
|  11 criterios de evaluación fijados en decreto comunidad   |  Criterios de evaluación fijados a nivel estatal a nivel de cada uno de los 4 bloques | Criterios de evaluación fijados en 6 **competencias específicas** que tienen 19 apartados en total |
|  No había estándares de aprendizaje |  Estándares de aprendizaje fijados a nivel estatal a nivel de cada uno de los 4 bloques | Desaparecen los estándares de aprendizaje |
|  1. Contenidos comunes. |  Bloque 1. La actividad científica   | **No hay bloque de saberes básicos asociado** |
|  2. Estructura atómica y clasificación periódica de los elementos. |  Bloque 2. Origen y evolución de los componentes del Universo  <br> -Se citan quarks, origen del Universo <br> Se cita explícitamente principio de incertidumbre de Heisenberg   | Saberes básicos **A. Enlace químico y estructura de la materia: 1. Espectros atómicos,  2. Principios cuánticos de la estructura atómica y 3. Tabla periódica y propiedades de los átomos** (el 4 se comenta en bloque enlace) <br> Madrid cita Hund, Aufbau (también como "Building-up o Construcción Progresiva") y Moeller<br> Madrid enumera explícitamente radio atómico, radio iónico, energía de ionización, afinidad electrónica, electronegatividad |
|  3. El enlace químico y propiedades de las sustancias. |  Bloque 2. Origen y evolución de los componentes del Universo  <br> -Se cita explícitamente Born-Landé, superconductores, teoría de bandas <br>  Desaparece mención explícita a _"Propiedades de algunas sustancias de interés industrial o biológico en función de su estructura o enlaces."_ |  Saber básico **A. Enlace químico y estructura de la materia: 4. Enlaces intra e intermoleculares**  |
|  4. Transformaciones energéticas en las reacciones químicas. Espontaneidad de las reacciones químicas. | **Desaparece (pasa a [Física y Química 1º Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomce) )**  | **B. Reacciones químicas: 1. Termodinámica química** <br> Se recupera la situación previa a LOMCE |  
|  5. El equilibrio químico. |  Bloque 3. Reacciones químicas  <br> -Se cita Guldberg y Waage <br> -Se cita "etapa limitante" y "mecanismo de reacción"    |  Saberes básicos **B. Reacciones químicas: 2. Cinética química, 3. Equilibrio químico** <br> Madrid cita explícitamente Ecuación de Arrhenius y catalizadores en procesos industriales <br> Se deja de citar explícitamente ion común  |
|  6. Ácidos y bases. |  Bloque 3. Reacciones químicas  |  Saber básico **2. Reacciones químicas: 4. Reacciones ácido-base** <br> Se cita teoría Arrhenius <br> Se añaden **Disoluciones reguladoras del pH. Concepto y aplicaciones en la vida cotidiana.** <br>  |
|  7. Introducción a la electroquímica.   |  Bloque 3. Reacciones químicas  <br> -Se cita anodización, galvanoplastia  <br> -Se cita corrosión  <br> -Se cita electrolisis (en LOMCE sin tilde, en LOE tenía tilde), la  [RAE admite ambas](http://dle.rae.es/?id=EUlJdt1)  | Saber básico **B. Reacciones químicas: 5. Reacciones redox** <br> Se deja de citar explícitamente anodización y galvanoplastia |
|  8. Química del carbono.   |  Bloque 4. Síntesis orgánica y nuevos materiales  <br> -Se citan tioles y perácidos  <br> -Se citan explícitamente Markovnikov, Saytzeff, polietileno, PVC, poliestireno, caucho, poliamidas, poliésteres.   | Saberes básicos **C. Química orgánica: 1. Nomenclatura de compuestos orgánicos, 2. Isomería, 3. Reactividad orgánica, 4. Polímeros** <br> Madrid añade 1. Nomenclatura de compuestos orgánicos que no está en currículo estatal concretando hidrocarburos alifáticos y aromáticos, derivados halogenados, alcoholes, éteres, aldehídos, cetonas, ácidos, ésteres, amidas y aminas. <br> Se dejan de citar tioles y perácidos <br> Madrid concreta tipos de isomería: de posición, cadena y función. Isomería cis-trans. Representación de moléculas orgánicas <br> Se dejan de citar explícitamente Markovnikov, Saytzeff, polietileno, PVC, poliestireno, caucho, poliamidas, poliésteres.|  


