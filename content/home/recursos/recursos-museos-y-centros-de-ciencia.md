# Recursos museos y centros de ciencia

Se trata de tener direcciones de internet para consultas o visitas virtuales; para visitas física (cerca de Madrid) ver  [actividades fuera del centro](/home/recursos/recursos-de-actividades-fuera-del-centro)  

**España**  
Museu de la Ciencia de la Fundació «La Caixa». Barcelona: [https://obrasociallacaixa.org/es/ciencia/cosmocaixa/el-museo](https://obrasociallacaixa.org/es/ciencia/cosmocaixa/el-museo)  
Museo de la Ciencia y el Cosmos. Tenerife:  [http://www.museosdetenerife.org/mcc-museo-de-la-ciencia-y-el-cosmos](http://www.museosdetenerife.org/mcc-museo-de-la-ciencia-y-el-cosmos)  

**Extranjero**
Para extranjeros en general se intenta poner dirección asociada a lenguaje inglés  
Cité des sciences et de l’industrie. París:  [http://www.cite-sciences.fr/en/home/](http://www.cite-sciences.fr/en/home/)  
Deutsches Museum. Munich:  [http://www.deutsches-museum.de/index.php?id=1&L=1](http://www.deutsches-museum.de/index.php?id=1&L=1)  
Experimentarium. Dinamarca:  [https://en.experimentarium.dk/](https://en.experimentarium.dk/)   
Exploratorium. San Francisco:  [http://www.exploratorium.edu](http://www.exploratorium.edu)  
Franklin Institute Science Museum. Filadelfia:  [http://sln.fi.edu](http://sln.fi.edu)  
Heureka, the Finnish Science Centre. Finlandia:  [http://www.heureka.fi/en](http://www.heureka.fi/en)  
Lawrence Hall of Science. Berkeley:  [http://www.lhs.berkeley.edu](http://www.lhs.berkeley.edu)  
Ontario Science Centre. Canadá:  [https://www.ontariosciencecentre.ca/](https://www.ontariosciencecentre.ca/)  
