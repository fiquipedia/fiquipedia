# Dinámica de fluidos

A nivel de secundaria (ESO y Bachillerato) solo se ve estática de fluidos en 4º ESO.  
La dinámica de fluidos con aspectos como Bernoulli quedan fuera del temario Bachillerato, pero se comentan recursos de momento aquí  
La parte de "estática de fluidos" está en página separada [fuerzas en fluidos](/home/recursos/fisica/fuerzas-en-fluidos/)


[MECANICA DE FLUIDOS PARA BACHILLERATO - udistrital.edu.co](https://comunidad.udistrital.edu.co/geaf/files/2012/09/2006No1-006.pdf)  
 
[Dinámica de fluídos, velas, globos y cucharas - mheducation.es](https://www.mheducation.es/blog/dinamica-de-fluidos-velas-globos-y-cucharas)   
![](https://www.mheducation.es/download-resource/blog/dinamica-de-fluidos-velas-globos-y-cucharas-es22-600-3.jpg)  

[Apuntes de Mecánica de Fluidos. Agustín Martín Domingo. cc-by-sa - upm.es](https://oa.upm.es/6531/1/amd-apuntes-fluidos.pdf)  
[Apuntes de mecánica de fluidos. Arregui de la Cruz, F. J.; Cabrera Rochera, E.; Cobacho Jordán, R.; Gómez Sellés, E.; Soriano Olivares, J. (2017). Valencia: Universitat Politècnica de València](https://riunet.upv.es/bitstream/handle/10251/78258/PDFArregui%3BCabrera%3BCobacho%20%20Apuntes%20de%20mec%C3%A1nica%20de%20fluidos.pdf?sequence=1)  
 
[Aerodinámica en la Fórmula 1. Teoría - aerodinamicaf1.com](https://www.aerodinamicaf1.com/teoria/)  
Se pueden buscar TFGs asociados a diseños aerodinámicos en fórmula 1    
[TRABAJO DE FINAL DE GRADO Grado en Ingeniería Mecánica ESTUDIO Y DISEÑO DEL ALERÓN FRONTAL DE UN VEHÍCULO DE FÓRMULA 1. Javier Ríos Cuadrado](https://upcommons.upc.edu/bitstream/handle/2117/343837/TFG%20Javier%20R%C3%ADos%20Cuadrado%20-%20ESTUDIO%20Y%20DISE%C3%91O%20DEL%20ALER%C3%93N%20FRONTAL%20DE%20UN%20VEH%C3%8DCULO%20DE%20F%C3%93RMULA%201.pdf?sequence=1&isAllowed=y)

[Tema 7 Dinámica de fluidos - ugr.es](https://www.ugr.es/~jtorres/t7.pdf)   

Ecuación de Navier-Stokes, uno de los problemas del milenio   
[Grandes retos en las ecuaciones de los fluidos. Dos preguntas sobre las ecuaciones de Navier-Stokes son consideradas unos de los problemas más importantes de las matemáticas - elpais.com](https://elpais.com/ciencia/2020-09-07/grandes-retos-en-las-ecuaciones-de-los-fluidos.html)  

Enlaza con túneles de viento  

## Aerodinámica

[Descubre cómo funciona la aerodinámica de un monoplaza de Fórmula 1 - teoría](https://www.aerodinamicaf1.com/teoria/)  

## Bernoulli

[¿Qué es la ecuación de Bernoulli? - es.khanacademy.org](https://es.khanacademy.org/science/physics/fluids/fluid-dynamics/a/what-is-bernoullis-equation)  

[Ecuación de Bernoulli - hyperphysics](http://hyperphysics.phy-astr.gsu.edu/hbasees/pber.html)  

[Fluidos. Ecuación de Bernoulli - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica3/fluidos/bernoulli/bernoulli.html)  

[Física universitaria volumen 1 14.6 Ecuación de Bernoulli - openstax.org](https://openstax.org/books/f%C3%ADsica-universitaria-volumen-1/pages/14-6-ecuacion-de-bernoulli)  
![](https://openstax.org/apps/archive/20240226.174525/resources/e2dca04d3ddbc58d68d94d7ac722de8c3f0c1474)  

[Principio de Bernoulli: ¿Por qué vuelan los aviones? - oneair.es](https://www.oneair.es/principio-de-bernoulli/)  

[El principio de Bernoulli, la base de la aerodinámica - aerodinamicaf1.com](http://www.aerodinamicaf1.com/2019/09/03/el-principio-de-bernoulli/)  

[![](https://img.youtube.com/vi/eKEorBipbO8/0.jpg)](https://www.youtube.com/watch?v=eKEorBipbO8 "Bernoulli's principle - GetAClass Physics")  

[![](https://img.youtube.com/vi/DW4rItB20h4/0.jpg)](https://www.youtube.com/watch?v=DW4rItB20h4 "Understanding Bernoulli's Equation - The efficient Engineer")  

[twitter Rainmaker1973/status/1738481878485663887](https://twitter.com/Rainmaker1973/status/1738481878485663887)  
How prairie dogs use the Bernoulli’s principle.   
In order to ensure sufficient oxygen within their burrow, one of the entrances is built with a raised mound of dirt, while the other is not. This determines different wind velocities and different pressures + natural ventilation.  
![](https://pbs.twimg.com/media/GCBUBptWYAAB90C?format=png)  

## Pelota de ping pong y secador

[Pelota de ping-pong levitando - ucm.es](https://www.ucm.es/data/cont/docs/76-2013-07-10-03_Airflow_lift_force.pdf)  
> . Principio físico que ilustra  
Efecto Venturi  
Ecuación de Bernoulli  
Efecto Coanda  

[PELOTA DE PING PONG QUE FLOTA - cienciafacil](https://www.cienciafacil.com/PelotaPingPong.html)   

## Venturi

[![](https://img.youtube.com/vi/fq8zqRRBEEY/0.jpg)](https://www.youtube.com/watch?v=fq8zqRRBEEY "Why Does Fluid Pressure Decrease and Velocity Increase in a Tapering Pipe? Integral Physics")  

[¿Qué es el efecto Venturi? - oneair.es](https://www.oneair.es/que-es-el-efecto-venturi/)  
Aplicaciones del efecto Venturi en aviación  
Aplicaciones del efecto Venturi en la vida cotidiana  
    Con los labios al soplar  
    Efecto lengüeta con un folio  
    Venturi con doble lengüeta  

## Efecto Magnus

Se puede ver en MUNCYT, 2:35

[![](https://img.youtube.com/vi/9Aze73RI_14/0.jpg)](https://www.youtube.com/watch?v=9Aze73RI_14 "LOS 21 MEJORES EXPERIMENTOS DEL MUNCYT ||||||||||||| MUSEO NACIONAL DE CIENCIA Y TECNOLOGÍA")  


[![](https://img.youtube.com/vi/23f1jvGUWJs/0.jpg)](https://www.youtube.com/watch?v=23f1jvGUWJs "What Is The Magnus Force? Veritasium")  

[![](https://img.youtube.com/vi/2OSrvzNW9FE/0.jpg)](https://www.youtube.com/watch?v=2OSrvzNW9FE "Backspin Basketball Flies Off Dam Veritasium")   

[![](https://img.youtube.com/vi/f77HvEbFr0Y/0.jpg)](https://www.youtube.com/watch?v=f77HvEbFr0Y "The Magnus effect - GetAClass Physics")  


[Experimentos Física Efecto Magnus Universidad Pontificia de Chile](https://experimentosfisicauc.wixsite.com/experimentos/magnus-effect)  

[Sí, esto es un velero, y eso que ves girar son sus velas - gizmodo.com](https://es.gizmodo.com/si-esto-es-un-velero-y-eso-que-ves-girar-son-sus-vela-1828756191 )  

[Velas mecánicas de alta tecnología para ahorrar combustible, reducir emisiones y cruzar los océanos - xataka.com](https://www.xataka.com/investigacion/velas-mecanicas-alta-tecnologia-para-ahorrar-combustible-reducir-emisiones-cruzar-oceanos)  

[twitter fermatslibrary/status/1604524053435453441](https://twitter.com/fermatslibrary/status/1604524053435453441)  
Roberto Carlos' free kick against France in 1997 is an extreme example of the Magnus Effect! He had to kick the ball with an angular speed of 14 revolutions per second (faster than some DVD players)  

[ANÁLISIS DEL EFECTO MAGNUS EN CUERPOS CILÍNDRICOS CON ALETAS DE DIFERENTE CUERDA - unlp.edu.ar](http://sedici.unlp.edu.ar/bitstream/handle/10915/127744/Documento.pdf?sequence=1)  

## Flujo laminar y turbulento

[![](https://img.youtube.com/vi/9A-uUG0WR0w/0.jpg)](https://www.youtube.com/watch?v=9A-uUG0WR0w "Understanding Laminar and Turbulent Flow - The efficient Engineer")  

[NÚMERO DE REYNOLDS; FLUJO LAMINAR Y TURBULENTO](http://numerodereynoldsortega.blogspot.com/2016/07/numero-de-reynolds-flujo-laminar-y.html)  

[Flujo laminar y turbulento - aerodinamicaf1.com](http://www.aerodinamicaf1.com/2019/10/07/flujo-laminar-y-turbulento/)  
[¿Qué es el número de Reynolds? - aerodinamicaf1..com](http://www.aerodinamicaf1.com/2020/10/que-es-el-numero-de-reynolds/)  

## Efecto Coanda

Efecto Coanda, fuerza Magnus y velas rotativas Flettner. Física. STEM. Ingeniería para niños  
[![](https://img.youtube.com/vi/HGdiZyo01Xg/0.jpg)](https://www.youtube.com/watch?v=HGdiZyo01Xg "Efecto Coanda, fuerza Magnus y velas rotativas Flettner. Física. STEM. Ingeniería para niños")  
- Chorro de agua cayendo sobre esfera   
- Chorro de aire hacia arriba sobre esfera   
- Barco velas rotatorias  

[Efecto Coanda y drag de forma - aerodinamicaf1.com](http://www.aerodinamicaf1.com/2019/09/10/efecto-coanda-y-drag-de-forma/)  

## Levitación hidrodinámica  

La Levitación Hidrodinámica es Increíble | Curiosidades de la Ciencia - Veritasium  
[![](https://img.youtube.com/vi/67oJ-6ux4LE/0.jpg)](https://www.youtube.com/watch?v=67oJ-6ux4LE "La Levitación Hidrodinámica es Increíble | Curiosidades de la Ciencia - Veritasium")  
Sobre una corriente de agua se pueden hacer levitar bolas ligeras de todos los tamaños e incluso discos y cilindros. El mecanismo no es el efecto Bernoulli.  


