# Recursos cinemática

## Generales 
Puede haber recursos asociados a  [movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio)  en el que se suele tratar conjuntamente cinemática, dinámica y energía.  
En los  [problemas de selectividad de la materia de Mecánica de 2º Bachillerato (LOGSE)](/home/pruebas/pruebasaccesouniversidad/pau-mecanica)  hay algunos problemas asociados a cinemática.   
Dentro de escalas y [recursos notación científica](/home/recursos/recursos-notacion-cientifica) hay relación para comparar movimientos  

Existe una página separada para recursos sobre [MRU](/home/recursos/fisica/cinematica/mru)  

Ver también uso para cinemática de apps en [Prácticas / experimentos / laboratorio con smartphone](/home/recursos/practicas-experimentos-laboratorio/practicas-experimentos-laboratorio-smartphone/)  


[Cinematica 1](http://www.catfisica.com/00cinemat/00cinemat.htm) Página libre de derechos de autor. Se pueden imprimir páginas de problemas de 2º de Bachillerato hechos con flash y páginas de teoría y problemas en pdf.Autor: Josep Maria Domènech i Roca. IES Ramon Muntaner. Figueres (Alt Empordà).  

[Física por temas - Matemáticas Física y Química](http://www.matematicasfisicaquimica.com/fisica-temas/cinematica-fisica.html) Alberto Pérez Montesdeoca. Copyright

## Apuntes
En esta página coloco unos  [pequeños apuntes de teoría sobre cinemática para 4º ESO y 1º de Bachillerato](/home/recursos/fisica/cinematica/Fis-TeoriaCinematica.pdf) . Al igual que hago en los apuntes de 2º de Bachillerato, **en general intento revisar que se cubre el currículo oficial de Madrid, e intento reflejarlo indicando subrayados los epígrafes / contenidos que aparecen en el currículo oficial.** En el bloque de cinemática el currículo menciona "sistemas de referencia inerciales", "influencia de la velocidad en un choque" que considero que deberían estar en el bloque de dinámica.  

En el apartado de  [apuntes](/home/recursos/apuntes) , hay apuntes asociados a distintos niveles: la cinemática se trata en:  
* [Física y Química de 4º ESO](/home/materias/eso/fisicayquimica-4eso/curriculofisicayquimica-4eso)  dentro de bloque 2 "Fuerzas y movimiento", Iniciación al estudio del movimiento.  
* [Física y Química de 1º Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato) , bloque "2. Estudio del movimiento.  
Con LOMCE se introduce en 2º ESO y en 3º ESO

"Acercándonos al LHC, física en el LHC, cinemática. Datos adaptados a secundaria. Relacionado con  [física de partículas](/home/recursos/fisica/fisica-de-particulas).  
[Taking a closer look at LHC - Home](http://www.lhc-closer.es/2/4/1/0)  Movimiento simple  
[Taking a closer look at LHC - Home](http://www.lhc-closer.es/2/4/1/1)  Movimiento complejo  

4ESO Física: resumen Cinemática  
[4ESO_Física_resumen_Cinemática](https://www.academia.edu/10877196/4ESO_F%C3%ADsica_resumen_Cinem%C3%A1tica)  

Cinemática de la partícula [Cinemática_de_la_partícula_(G.I.T.I)](http://laplace.us.es/wiki/index.php/Cinem%C3%A1tica_de_la_part%C3%ADcula_%28G.I.T.I%29)  
Departamento de física aplicada III Universidad de Sevilla. cc-by-nc-sa  

2. Fuerzas y movimientos  
[Tema2.htm](http://darwin-milenium.com/Estudiante/Fisica/Temario/Tema2.htm)  
3. Fuerzas y movimientos circulares  
[Tema3.htm](http://darwin-milenium.com/Estudiante/Fisica/Temario/Tema3.htm)  

Cinemática (educaplus)  
[Cinemática | Educaplus](http://www.educaplus.org/movi/index.html)  
Incluye apartado estudio gráfico con gráficas e-t, v-t y a-t ....  

[Movimiento física - fisicalab](https://www.fisicalab.com/tema/movimiento-fisica)  

## Ejercicios / actividades
Asociados a  [pruebas libres de graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)  y a  [pruebas de acceso a Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio) hay ejercicios de cinemática  

A veces la cinemática se usa dentro de problemas de  [dinámica](/home/recursos/fisica/dinamica)  y de  [energía](/home/recursos/energia).  

Ejercicios COU, José Carlos Vilches Peña, con soluciones (no resolución)  
[José Carlos Vilches Peña - Problemas de Cinemática](https://sites.google.com/site/jvilchesp/fisica/cou/cinematica)  

[ej_cinematica-3eso.pdf](https://matematicasiesoja.files.wordpress.com/2015/03/ej_cinematica-3eso.pdf)  

Curso Interactivo de Física en Internet © Ángel Franco García  
[Movimiento rectil&iacute;neo](http://www.sc.ehu.es/sbweb/fisica3/problemas/cinematica/rectilineo/rectilineo.html)  
[Movimiento circular](http://www.sc.ehu.es/sbweb/fisica3/problemas/cinematica/circular/circular.html)  

Ejercicios cinemática  [juliweb.es](http://www.juliweb.es/fisica/ejercicios/problemascinematica.pdf)  

Ejercicios Cinemática 1º Bachillerato. Departamento física y química Colegio Cervantes. Talavera de la Reina  
[index.php](http://edu.jccm.es/con/cervantes/index.php?option=com_content&view=article&id=166:fisica-y-quimica-1o-bachillerato&catid=64:fisica-y-quimica&Itemid=29)  

[movimiento3.htm](http://genesis.uag.mx/edmedia/material/fisica/movimiento3.htm)  
Ejercicios interpretar gráficas movimiento  

[twitter FlippedBioGeo/status/1257242100841750528](https://twitter.com/FlippedBioGeo/status/1257242100841750528)  
Tímida incursión en el mundo de http://Genial.ly inspirada por el trabajo de @quirogafyq (incluye uno de sus http://Genial.ly y varios recursos de fikipedia)  
Repaso muy sencillo de cinemática para 2º ESO de la mano de Los #Vengadores  
[2º ESO CINEMÁTICA by martamaja21 on Genially](https://view.genial.ly/5eabe6da32376f0d7de390d7/interactive-image-2o-eso-cinematica) 

[Kahoot cinemática FQ 4º ESO - fruzgz](https://create.kahoot.it/share/kahoot-cinematica-fq-4-eso/5b848291-3f42-4818-9970-2b7b4041026a)

## Efecto coriolis 

Se cita a vecese como fuerza y se trata dentro de [dinámica](/home/recursos/fisica/dinamica)  

[Physics » Rotational motion » Coriolis effect - kaiserscience.com](https://kaiserscience.wordpress.com/physics/rotational-motion/coriolis-effect/)  

## Vídeos  
Ejercicio resuelto analizando vídeo  
[twitter ProfaDeQuimica/status/1051182591804620801](https://twitter.com/ProfaDeQuimica/status/1051182591804620801)  
Nuevo #vídeo en mi canal (sección "La ciencia friki"):  
[![La Ciencia Friki - Análisis cinemático y dinámico de &quot;Flash (1x06)&quot; - YouTube](https://img.youtube.com/vi/0CsAJhtK2P8/0.jpg)](https://www.youtube.com/watch?v=0CsAJhtK2P8)  
Analizo desde el punto de vista cinemático/dinámico una escena del episodio 6 de la 1ª temporada de #Flash (nivel 4º ESO o 1º Bachillerato).  
¡Espero que os guste! 😃#LaCienciaFriki  
La Ciencia Friki - Análisis cinemático y dinámico de "Flash (1x06)"  

Flip(e)ando con la Física y Química  
1º BACHILLERATO: Tiro parabólico (ejemplo resuelto)  
Física y Química de 1º de Bachillerato. Tema de Cinemática, composición de movimientos rectilíneos. Tiro horizontal (ejemplo resuelto basado en una escena de película). (ant-man)  
[![1º BACHILLERATO: Tiro parabólico (ejemplo resuelto) - YouTube](https://img.youtube.com/vi/jOs8JowQEzs/0.jpg)](https://www.youtube.com/watch?v=jOs8JowQEzs)  

[twitter bjmahillo/status/1122539664693501953](https://twitter.com/bjmahillo/status/1122539664693501953)  
Rick Charls todavía ostenta el récord de salto de trampolín de altura. Se tiró desde 52 m en 1983 y salió ileso (muchos lo han intentado después, pero han resultado heridos). 🥴 Lo usaré en clase para estudiar la caída libre  
[twitter Havenlust/status/1122172845629628416](https://twitter.com/Havenlust/status/1122172845629628416)  
Rick Charls diving 172 feet at Sea World on the show Wide World of Sports 1983. No one has ever jumped this high without injury ...  


[![Light Speed – fast, but slow](https://img.youtube.com/vi/nQUwHdSAhmw/0.jpg)](https://www.youtube.com/watch?v=nQUwHdSAhmw "Light Speed – fast, but slow")  Dr James O'Donoghue, 1 min 8 s   
[Solar System vs. Lightspeed, lista de vídeos](https://www.youtube.com/playlist?list=PLD9fyFS-QKPkhflcV6iTbYB1HtG_y5L7p)


[![CD Shattering at 170,000FPS! - The Slow Mo Guys - YouTube](https://img.youtube.com/vi/zs7x1Hu29Wc/0.jpg)](https://www.youtube.com/watch?v=zs7x1Hu29Wc "CD Shattering at 170,000FPS! - The Slow Mo Guys")  

[twitter Rainmaker1973/status/1302237673134985217](https://twitter.com/Rainmaker1973/status/1302237673134985217)  
This trampoline moving at constant velocity (and therefore not accelerated) shows you how the horizontal component of velocity in an acrobat motion remains constant  
source of the gif, Dunking Devils Squad: 
[![Dunking Devils Trampoline - YouTube](https://img.youtube.com/vi/HJ79PJq-CWY/0.jpg)](https://www.youtube.com/watch?v=HJ79PJq-CWY "Dunking Devils Trampoline")  

[![Dunking Devils Trampoline - YouTube](https://img.youtube.com/vi/HJ79PJq-CWY/0.jpg)](https://www.youtube.com/watch?v=HJ79PJq-CWY "Dunking Devils Trampoline")  

[![](https://img.youtube.com/vi/j1URC2G2qnc/0.jpg)](https://www.youtube.com/watch?v=j1URC2G2qnc "Newton's Law of Inertia: Projectile from a Moving Truck Demo")  

[![](https://img.youtube.com/vi/BLuI118nhzc/0.jpg)](https://www.youtube.com/watch?v=BLuI118nhzc "Mythbusters - Soccer Ball Shot from Truck")  


## Simulaciones
En el apartado de  [simulaciones](/home/recursos/simulaciones) , puede haber varias. Por ejemplo, dentro de  [Inici | fislab](http://www.fislab.net/)  hay un apartado para cinemática: Caída libre, Movimiento rectilíneo, Dos móviles, Movimiento parabólico, Movimiento circular. En mayo 2018 fislab pasa las simulaciones a ficheros .jar que funcionan sin las limitaciones de applets.  

[Laboratorio virtual de cinemática - educaplus.org](http://www.educaplus.org/game/laboratorio-virtual-de-cinematica)  
Gráficas de los movimientos rectilíneos  
Gráficas simultáneas x-t, v-t, a-t y movimiento  
![](https://pbs.twimg.com/media/FtWbyVTWwAAaaBQ?format=jpg)  

[Cinemática - educaplus](https://www.educaplus.org/games/cinematica)  

[Cinemática - montes.upm.es](https://www2.montes.upm.es/dptos/digfa/cfisica/cinematica/cinematica_portada.htm)  

Varios tipos, incluye movimientos en 1 y 2 dimensiones  
[Simulation list](http://physics.bu.edu/~duffy/HTML5/index.html)  

[moving-man](https://phet.colorado.edu/es/simulation/moving-man)  (Java con CheerpJ)

[Trazador de movimiento - universeandmore.com](https://www.universeandmore.com/motion-mapper-es/)  
![](https://www.universeandmore.com/static/media/MotionMapperPreview.cf7849d5144baa5494ed.jpg)  

### Desplazamiento y distancia recorrida
 [Diferencia entre Desplazamiento y Espacio recorrido | Fisicalab](https://www.fisicalab.com/apartado/desplazamiento-vs-espacio#contenidos) 

### Movimiento relativo
 [twitter WorldAndScience/status/952344579885404160](https://twitter.com/WorldAndScience/status/952344579885404160)  
 Firing a soccer ball, at 50 mph, out of a cannon from a truck going 50 mph. In other words, cancel momentum  
 
[Sistemas de referencia  | Educaplus](http://www.educaplus.org/game/sistemas-de-referencia)  

Top Secret train scene  
[![](https://img.youtube.com/vi/A_peCIVBTAY/0.jpg)](https://www.youtube.com/watch?v=A_peCIVBTAY "Top Secret train scene")  
Escena de la película Top Secret en la que desde dentro del tren se ve la estación y parece que el tren arranca, pero en realidad el tren está quieto y la estación es la que arranca 

[EspacioTiempo.swf](http://www3.gobiernodecanarias.org/medusa/ecoescuela/secundaria/files/2012/01/EspacioTiempo.swf) 

[![](https://img.youtube.com/vi/v0gg1F0sz0E/0.jpg)](https://www.youtube.com/watch?v=v0gg1F0sz0E "Relative Motion Gun")  
A cart moving at constant velocity shoots a ball straight upwards. Since the ball has the same translational velocity as the cart, it is caught when it comes back down.   

[twitter Rainmaker1973/status/1543562713108037632](https://twitter.com/Rainmaker1973/status/1543562713108037632)  
Life's often a matter of refence frames  
[He's so fast - reddit](https://old.reddit.com/r/confusing_perspective/comments/vq24gy/hes_so_fast/)  

### [MRU](/home/recursos/fisica/cinematica/mru)

### [MRUA](/home/recursos/fisica/cinematica/mrua)

### Movimiento general, x, v, a, derivación, componentes intrísecas

[Ecuación movimiento. Posición, velocidad y aceleración. Componentes intrínsecas. - geogebra.org](https://www.geogebra.org/m/umwtjkxw)  

### Movimiento circular 

Sin ser una simulación, es un vídeo hecho con tracker  
[twitter exosferablog/status/1195768674172444672](https://twitter.com/exosferablog/status/1195768674172444672)  
Normalmente se explica la aceleración normal con ejemplos de este tipo pero nunca se ven en vivo. Una pelota atada a una cuerda realiza un movimiento circular, cuando se libera la cuerda se pierde la aceleración normal y la bola prosigue de forma rectilínea con la dirección tan.  
Lo bueno de los ordenadores es que podemos decirle que nos ponga unas flechitas con la aceleración y la velocidad. La velocidad siempre es tangente, sin la aceleración normal hacia el centro saldría el línea recta, la aceleración cambia la dirección de la velocidad.  
Aún más lento. La aceleración normal no para de luchar para que la velocidad vaya siguiendo el movimiento circular. En cuando suelo la cuerda... Simplemente sigue la dirección de la velocidad en ese momento.

#### [MCU](/home/recursos/fisica/cinematica/mcu)

#### MCUA
[Movimiento circular uniformemente acelerado - geogebra](https://www.geogebra.org/m/kRDTwEcv#material/K5Cdezzc) Rafael Cabrera
[M.C.U. y M.C.U.A. - geogebra](https://www.geogebra.org/m/kRDTwEcv#material/cejCNNGv) José Ignacio Pérez Cadenas  

Algunas de  [animaciones profisica.cl (waybackmachine)](http://web.archive.org/web/20191021111844/http://profisica.cl/materialaula/animaciones.html)  
[movcircunferencial.swf (waybackmachine)](http://web.archive.org/web/20190808003311/http://www.profisica.cl/images/stories/animaciones/parabola2profisica.swf)  
[muac_antihor2006.swf (waybackmachine)](http://web.archive.org/web/20190808003317/https://www.profisica.cl/images/stories/animaciones/muac_antihor2006.swf)  

### [Composición de movimientos](/home/recursos/fisica/cinematica/composicion)

## Actividades

16 febrero 2019   
[Motion Graph Matchmakers - passionatelycurioussci.weebly.com](https://passionatelycurioussci.weebly.com/blog/motion-graph-matchmakers)  
This activity is intended to help interpret the overall shapes of displacement vs time and velocity vs time graphs.  
[![](https://passionatelycurioussci.weebly.com/uploads/5/8/6/6/58665899/published/motion-cards.jpg?1550345056 "") ](https://passionatelycurioussci.weebly.com/uploads/5/8/6/6/58665899/published/motion-cards.jpg?1550345056)  

También está implementado de manera virtual con desmos  
[Motion Graph Matchmakers. Virtual](https://passionatelycurioussci.weebly.com/blog/motion-graph-matchmakers#virtual)  
[Motion Graph Matchmakers - Basic Motion - desmos.com](https://teacher.desmos.com/activitybuilder/custom/5f70edf1523f557ab6d65de9?lang=es)  
[Motion Graph Matchmakers - Advanced Motion - desmos.com](https://teacher.desmos.com/activitybuilder/custom/5f737f24a2a1440cb7580e38?lang=es)  

[Motion Graph (Scavenger Hunt)​ Walk Around](https://passionatelycurioussci.weebly.com/google-form-walk-arounds.html#motiongraph)  

[Graphing Motion Walk Around](http://passionatelycurioussci.weebly.com/google-form-walk-arounds.html#graphing)  
[Graphing Motion Walk Around. Displacement (pdf, 21 páginas)](https://passionatelycurioussci.weebly.com/uploads/5/8/6/6/58665899/graphing-motion-walk-around-displacement.pdf)  
[Graphing Motion Walk Around. Position (pdf, 21 páginas)](https://passionatelycurioussci.weebly.com/uploads/5/8/6/6/58665899/graphing-motion-walk-around-position.pdf)  

[twitter quirogafyq/status/1227634530481254401](https://twitter.com/quirogafyq/status/1227634530481254401)  
Buenas tardes. Comparto con todos una aventura en forma de historia de piratas para tratar la cinemática de 2º de ESO.  
La idea es que cada grupo de alumnos reciba el capítulo 1 y que, si responde correctamente, reciba el 2, y así sucesivamente.  
[![](https://pbs.twimg.com/media/EQlvF5CXsAAeNhC?format=jpg&name=small "") ](https://pbs.twimg.com/media/EQlvF5CXsAAeNhC?format=jpg&name=small)  
[![](https://pbs.twimg.com/media/EQlvF5CX0AMXvgg?format=jpg&name=small "") ](https://pbs.twimg.com/media/EQlvF5CX0AMXvgg?format=jpg&name=small)  
Está todo descrito, documentado y resuelto en mi web: [la-burla-de-los-mares-cinemática-2º-eso](https://sites.google.com/view/eldelafisicaylaquimica/bloque-4-el-movimiento-y-las-fuerzas/la-burla-de-los-mares-cinem%C3%A1tica-2%C2%BA-eso)   
Espero que os guste.  

[twitter miquelpadilla/status/1055832178439962624](https://twitter.com/miquelpadilla/status/1055832178439962624)  
Buggy challenge: determining the speed of each buggy. 1/2  #modphys #TramsFis #iteachphysics @stMiq @FTrams  
[![](https://pbs.twimg.com/media/DqcRt1_XQAMJcnM?format=jpg "") ](https://pbs.twimg.com/media/DqcRt1_XQAMJcnM?format=jpg)  
[![](https://pbs.twimg.com/media/DqcRt1cXgAAriFo?format=jpg "") ](https://pbs.twimg.com/media/DqcRt1cXgAAriFo?format=jpg)  


Los ficheros se veían inicialmente anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia recursos/fisica/cinematica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/fisica/cinematica)  


