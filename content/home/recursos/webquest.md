
# Webquest

*"Una webquest es una actividad didáctica de orientación constructivista en la que los estudiantes tienen que realizar una investigación dirigida, manejando y procesando información principalmente de Internet, con la creación de un producto o unos objetivos que tengan que lograr como fin. Básicamente consiste en una búsqueda asistida y orientada por el profesor, pero no consiste simplemente en contestar unas preguntas sobre hechos o conceptos. En una webquest se propone una tarea factible y atractiva para los estudiantes. Las webquest fomentan el aprendizaje colaborativo: se divide a los alumnos en grupos y se le asigna a cada uno un rol diferente. Los alumnos deben conocer de antemano las pautas o rúbrica mediante la cual será evaluado su trabajo. Para su realización los estudiantes tendrán que trabajar con la información realizando las siguientes tareas: búsqueda, selección, análisis, síntesis, comprensión, transformación, creación, valoración y publicación."*  
Definición tomada del curso "La competencia digital en Secundaria" de Ángeles Puertas Serrano, Departamento TIC del CRIF Las Acacias de Madrid, Material con licencia cc-by-nc-sa

   * [Using the Periodic Table](http://www.can-do.com/uci/lessons98/Periodic.html)  (validado en 2021)  
   L. SWAIN. Licenciamiento no detallado. En inglés. Investigar sobre un elemento y presentarlo en clase.
   * [Chemistry WebQuest: Chemicals at Home. A WebQuest for 11th and 12th Grade Chemistry](http://dante.udallas.edu/nare/Interactive_Project/webquest_template/formulas_webquest/index.htm)  (validado 2021)  
   Amy Trauth-Nare 11th-12th grade, equivale a Bachillerato. Trabajo sobre compuestos químicos que podemos encontrar en casa.
   * [Element Project. A WebQuest for High School Chemistry](http://mvhs.shodor.org/activities/chemistry/periodicf/webquest.html)  (validado en 2021)  
   Charlotte M. Trout. Realizar un poster con información sobre un elemento.
   * [Mrs. Shimp's "The Chemistry Behind Cooking" WebQuest](http://web.archive.org/web/20081101175618/http://chsweb.lr.k12.nj.us/kshimp/cooking/chemwebquest.htm)  (waybackmachine, no operativo en 2021)  
   Mrs. K. Shimp. Hacer una exposición sobre la química de la cocina y unos panfletos para la audiencia
   * [A WebQuest Resource Wiki](https://sites.google.com/site/442webquests/home)  (validado en 2021)  
   Licenciamiento no detallado
   * Listado de webquest de física (inglés) [http://step.nn.k12.va.us/science/phy/physics_wq.html](http://web.archive.org/web/20120815181440/http://step.nn.k12.va.us/science/phy/physics_wq.html)  (waybackmachine, no operativo en 2021) 
