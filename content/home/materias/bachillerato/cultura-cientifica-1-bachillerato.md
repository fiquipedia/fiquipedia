# 1º Bachillerato Cultura Científica

Se trata de una nueva materia en LOMCE, que está en 4º ESO y en 1º de Bachillerato, en cierto modo sustituyendo a la materia  [Ciencias para el Mundo Contemporáneo](/home/materias/bachillerato/cienciasparaelmundocontemporaneo-1bachillerato)  que antes de LOMCE era obligatoria en 1º de Bachillerato.  
En LOMLOE desaparece, aunque durante el curso 2022-2023 de implantación LOMLOE se sigue impartiendo en Madrid.  
* [Currículo](/home/materias/bachillerato/cultura-cientifica-1-bachillerato/curriculo-cultura-cientifica-1-bachillerato)  
* [Recursos](/home/recursos/recursos-por-materia-curso/recursos-cultura-cientifica-1-bachillerato)  
* Contenidos del currículo con enlaces a recursos (pendiente)  
 
