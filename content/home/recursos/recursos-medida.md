
# Recursos medida

Se intentan centralizar aquí todas los recursos asociados a la medida. Se hace desde un punto de vista clásico: la medición en mecánica cuántica es otra historia.  
Se separan en una página aparte recursos sobre la notación científica, muy relacionada y en currículo de 4º ESO comentada al tiempo que la medida.  

Ver ejercicios sobre unidades en [Pruebas libres graduado ESO](/home/pruebas/pruebas-libres-graduado-eso/)
  
Esta página es, como tantas, un borrador y batiburrillo de contenidos ( Magnitudes, Medición, Unidades, Notación científica, Alfabeto griego en ciencias...) y enlaces: según se vayan acumulando intentaré crear páginas aparte por cada tema  
  
Algunas categorías de recursos relacionados:  [notación científica](/home/recursos/recursos-notacion-cientifica) ,  [método científico](/home/recursos/recursos-metodo-cientifico) ,  [prácticas - experimentos - laboratorio](/home/recursos/practicas-experimentos-laboratorio) ,  [cifras significativas](/home/recursos/recursos-cifras-significativas) , ...  
Asociados a medida pueden surgir ideas de [proyectos de investigación](/home/recursos/proyectos-de-investigacion) 

[Escena sobre cambio de unidades de la película "Yo hice a Roque III" ](https://mediateca.educa.madrid.org/video/yeeey1nolyd5dljs)  


[![](https://img.youtube.com/vi/JYqfVE-fykk/0.jpg)](https://www.youtube.com/watch?v=JYqfVE-fykk "Washington's Dream - Saturday Night Live")  
George Washington (Nate Bargatze) tells his soldiers (Kenan Thompson, Mikey Day, Bowen Yang, James Austin Johnson) his dream for the country.   

Parodia
[twitter WKCosmo/status/1850640817267851529](https://x.com/WKCosmo/status/1850640817267851529)  
We shall measure stellar brightness logarithmically, and it will be called the "magnitude."  
- Sir, will that logarithm be base 10, or based on Napier's constant?  
Neither. It shall be of the base of the fifth root of one hundred.  
- and brighter stars will have larger magnitude?  
No, it shall be the opposite.  
And we shall classify stellar temperature by letters of the alphabet.   
- In alphabetical order, sir?  
No, it shall be O B A F GK M.  
- But why, sir?  
No one knows.  
And stars will come in two types, depending on their age, and they shall be called Type I and Type II.  
- With Type II being younger stars, descended from Type I, sir?   
No, Type II shall be older stars.  
We shall measure large distances by a new unit, the parsec.   
- Will it be an integer multiple of light years sir?   
No, it shall be in astronomical units the number of arc seconds in a radian.   
- But how much is that in light years, sir?  
It is 3.26.  
- What is an "astronomical unit," sir?  
No one knows.  
And luminosity shall be measured in units of solar luminosity, except when it's not, in which case it will be measured in "absolute magnitude", which shall be the apparent magnitude at a fixed distance in parsecs.   
- Will that be one parsec, sir?  
No, ten.  
In this new land, we shall designate stars in decreasing order of brightness, and increasing magnitude, also by assigning letters.  
- In alphabetical order this time, sir?  
Yes, of course.  
- In the Latin alphabet, sir?  
No, it shall be Greek.  
As free men, we shall measure fluxes in magnitudes, except in radio, where we shall measure fluxes in Janskys.   
- Sir, how do you convert between Janskys and magnitudes?  
Nobody knows.  
In this new land of freedom l, we shall divide the sky into latitudes and longitudes, like the Earth, except they shall be called 'declination" and "right ascension"  
- And will they be measured in degrees, sir like the Earth?  
Only the declination.  
- Sir, what units shall right ascension be?  
Hours.  
Supernovae shall also be of two types, I and II.  
- The same types as stars, Sir?   
No, totally different.  
And they shall have subtypes, denoted by letters.  
-Alphabetical, sir?  
Only Type I. Type II will be subdivided into B and L.  
- This is so confusing, Sir.  
And all shall be core collapse supernovae, except Type Ia, which is something completely different.
 
[intef.es ¡Aprende sobre Metrología y llévala a tu aula](https://intef.es/Noticias/aprende-sobre-metrologia-y-llevala-a-tu-aula/)  
> el Centro Español de Metrología, en colaboración con Instituto de Ingeniería de España (IIE), publicó en diciembre de 2019 un libro/folleto de carácter introductorio sobre la metrología, La Metrología también existe, que puede ser especialmente provechoso para el profesorado de Educación Primaria, Secundaria y Formación Profesional. En él, se pueden encontrar interesantes informaciones sobre qué es la Metrología, algunos conceptos claves del campo, las unidades legales de medida, los principales acuerdos en relación a este tema, etc.
[cem.es La Metrología también existe](https://www.cem.es/sites/default/files/30363_lametrologiatambienexiste_web.pdf)  

## Normativa  
[RECOMENDACIONES DEL CENTRO ESPAÑOL DE METROLOGÍA PARA LA ENSEÑANZA Y UTILIZACIÓN DEL SISTEMA INTERNACIONAL DE UNIDADES DE MEDIDA](http://www.cem.es/sites/default/files/recomendaciones_cem_ensenanza_metrologia_sep_2014_v01.pdf)   
Interesante Reglas de escritura de los símbolos y nombres de las unidades. y ANEXO: COMPENDIO DE REGLAS DE ESCRITURA PARA DOCUMENTOS CIENTÍFICO-TÉCNICOS, CONFORME AL SISTEMA INTERNACIONAL DE UNIDADES (SI) Y LA SERIE DE NORMAS ESPAÑOLAS UNE 82100-0:1996 a UNE 82100-13:1996, Y UNE 82103:1996  

[Real Decreto 2032/2009, de 30 de diciembre, por el que se establecen las unidades legales de medida.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-927)  

> Capítulo III. Reglas de escritura de los símbolos y nombres de las unidades, de expresión de los valores de las magnitudes y para la formación de los múltiplos y submúltiplos decimales de las unidades del SI  
> 2.6. El valor numérico precede siempre a la unidad y siempre se deja un espacio entre el número y la unidad  

## Magnitudes
 -Definición magnitud  
-Tipos de magnitudes:  
 -- escalares, vectoriales, tensoriales  
 -- extensivas / intensivas  
 -- fundamentales derivadas  
  
Proyecto Ulloa (Recursos para Química). Magnitudes y medidas, 3º ESO  
 [Proyecto Ulloa](http://recursostic.educacion.es/ciencias/ulloa/web/ulloa2/3eso/secuencia1/menu.html)   
 cc-by-nc-sa  
  
 [Introducción a la Física: Magnitudes, Unidades y Medidas | Fisicalab](https://www.fisicalab.com/tema/intro-magnitudes#contenidos)   
 [Proyecto Newton. Magnitudes f&iacute;sicas](http://newton.cnice.mec.es/materiales_didacticos/magnitudes/magnitudes.html)   
 [Magnitudes escalares y vectoriales  | Educaplus](http://www.educaplus.org/game/magnitudes-escalares-y-vectoriales)   
 [vector_magnitude_direction](http://mathinsight.org/applet/vector_magnitude_direction)   
  
 Ejercicios interactivos de medidas y magnitudes  
 [Ejercicios interactivos de medidas y magnitudes | Superprof](https://www.vitutor.com/di/m/a_1e.html)   


## Unidades
 Definición unidad  

[RSEQ Un resumen conciso de magnitudes, unidades y símbolos en Química Física. Versión en español de septiembre de 2022, Nomenclatura Química y Normas de la IUPAC en Español](https://dialnet.unirioja.es/descarga/libro/873818.pdf)  

Puede haber temas en alguna página separada, por ejemplo sobre unidades de temperatura en termodinámica  

[Unidades y medidas. Reglas de escritura de los símbolos y nombres de las unidades, de expresión de los valores de las magnitudes y para la formación de los múltiplos y submúltiplos decimales de las unidades del SI - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica_/unidades/unidades/unidades_2.html)

### Estándares

[Carpeta que en 2023 comparte estándares ISO, IEC, DIN](https://cloudflare-ipfs.com/ipfs/bafykbzacedpku3duvucdb3nfbfrlirxqjeas4sld4phi2qti6tfrwxadnr4fa/standards/)  
Por ejemplo [ISO-80000-6-2008 Quantities and units – Part 6: Electromagnetism](https://cloudflare-ipfs.com/ipfs/bafykbzacedpku3duvucdb3nfbfrlirxqjeas4sld4phi2qti6tfrwxadnr4fa/standards/IEC/80xxx/80000/IEC%2080000-6-2008.pdf)  

### Sistema internacional de unidades
 [Welcome - BIPM](http://www.bipm.org/en/si/)   
  
 Borrador de 2010 con nuevas definiciones de unidades  
 [Welcome - BIPM](http://www.bipm.org/utils/common/pdf/si_brochure_draft_ch2.pdf)   

[Guide for the Use of the International System of Units (SI). NIST Special Publication 811 2008 Edition - nist.gov](https://physics.nist.gov/cuu/pdf/sp811.pdf)  
  
 [twitter steckel/status/1136825825070788608](https://twitter.com/steckel/status/1136825825070788608)  
 holy shit the video is 100x better than the still image ever could have led me to believeVídeo en el que americanos critican el Sistema Internacional: parece un chiste si no fuese un vídeo real de FOX  
 
[RSEQ Un resumen conciso del Sistema Internacional de Unidades, SI. Versión en español de septiembre de 2022, Nomenclatura Química y Normas de la IUPAC en Español](https://dialnet.unirioja.es/descarga/libro/873818.pdf)  

[NPL SI units](https://www.npl.co.uk/si-units)  
[NPL SI school posters](https://www.npl.co.uk/school-posters)  
[SI Prefixes 2023.pdf](https://www.npl.co.uk/getmedia/04bd0be7-6d93-4d9f-883a-fe82260d3ab2/SI_PREFIXES_2023.pdf)  
[7 SI bases units.pdf](https://www.npl.co.uk/getmedia/b097a52d-8043-46e0-aa0c-14e64b56d95b/NPL-Schools-poster-_-7-SI-BASE-UNITS-v12-HR-NC.pdf)  
[Full set of SI units posters for secondary schools](https://www.npl.co.uk/getmedia/4239930d-8d2d-4574-8c19-f0a0f4ce4b0e/NPL-Schools-poster-Secondary-2023.zip)  

### Unidad de masa atómica 

[unified atomic mass unit - IUPAC](https://goldbook.iupac.org/terms/view/U06554)  
Símbolo es u, no uma  
[Unified atomic mass unit - mass-spec.lsu.edu](http://mass-spec.lsu.edu/msterms/index.php/Unified_atomic_mass_unit)  
Note: The abbreviation amu for atomic mass unit is deprecated; it has been used to denote atomic masses measured relative to a single atom of 16O, or to the isotope-averaged mass of an oxygen atom, or to a single atom of 12C.

#### Cambios Sistema Internacional en 2018   

Ideas sobre el kilogramo previo  
Artículo 2014  
 [La aventura del kilo español](https://www.vozpopuli.com/altavoz/next/Metrologia-Masa-xx-Fisica-CEM_0_670732944.html)  La aventura del kilo español  
 El kilogramo es la última unidad de medida que se basa en un objeto físico y no en una constante fundamental. España tiene dos patrones históricos (K3 y K24), dos copias del patrón internacional que se guarda en París fabricadas a finales del siglo XIX. Una de ellas, el K24, es el patrón nacional de masa y solo se saca una vez cada cuatro años para calibrar los patrones de referencia. Next estuvo allí....No es tan fácil medir un kilogramo...Porque a la hora de medir la masa hay que tener en cuenta numerosas variables, y una de ellas es el empuje del aire. Para calcularlo, los científicos deben medir tanto la densidad del objeto (muy distinta en el hierro y la paja) como la del aire, para lo que necesitan conocer la temperatura, la humedad relativa y la presión atmosférica, así como la fracción de CO2 en el aire. Teniendo en cuenta todos estos factores, y si no hiciéramos las correspondientes correcciones, nuestro kilo de paja ideal daría una indicación en la báscula distinta al de hierro salvo que lo pesáramos en el vacío.  
  
 El nivel de exactitud de un centro como el CEM es tal, que las precauciones a la hora de medir no se acaban aquí. El laboratorio de masa está asentado sobre una cimentación independiente al resto del edificio, para evitar vibraciones, y los comparadores de masa descansan sobre grandes moles de granito que aumentan la estabilidad. Cuando se va a calibrar un patrón de masa hay que tener en cuenta su temperatura y las corrientes de convección que genera la diferencia con el ambiente, de modo que se espera hasta una semana para que ambas temperaturas se igualen. Y lo mismo si se limpia el patrón: al frotar la superficie se pueden generar cargas electrostáticas y esto puede afectar a la medición. El otro factor que se tiene en cuenta es el gradiente de gravedad: un patrón de masa de acero de 1kg es generalmente más alto que el de platino e iridio, por lo que su centro de gravedad está más alto y eso afecta a la determinación de la masa y se debe corregir.   
  
 [twitter GDHFQ/status/975833207513014274](https://twitter.com/GDHFQ/status/975833207513014274) Profes de #FyQ, ¿estáis preparados para actualizar vuestras clases sobre el Sistema Internacional? El Centro Español de Metrología ha preparado un interesante póster sobre los cambios del nuevo SI. Se puede descargar en:  [el-sistema-internacional-de-unidades-si](http://www.cem.es/content/el-sistema-internacional-de-unidades-si)   
  
![](https://pbs.twimg.com/media/DYraPTKWAAAuBJX.jpg)  
[Poster Nuevo-SI.pdf](http://www.cem.es/sites/default/files/files/Poster%20Nuevo-SI.pdf)   
  


#### Unidades básicas
Las unidades básicas son 7...  
[BIPM, Base units](http://www.bipm.org/en/si/base_units/)  
El mol es una unidad especialmente interesante, y poco entendida.   
Ver [recursos sobre concepto mol](/home/recursos/quimica/concepto-de-mol) 

#### Unidades derivadas
 [Welcome - BIPM](http://www.bipm.org/en/si/derived_units/)   
  
Idea friki (preguntada por un alumno): qué unidad tiene un símbolo con más caracteres? Creo que sería daTorr (Torr se escribe con mayúscula aunque se nombre torr) [Torr - Wikipedia](https://en.wikipedia.org/wiki/Torr#Nomenclature_and_common_errors)   


#### [Cambio de unidades con factores de conversión](/home/recursos/factores-de-conversion)

### Los símbolos para algunas unidades: para litro, ¿l minúscula o L mayúscula?
[Real Decreto 2032/2009, de 30 de diciembre, por el que se establecen las unidades legales de medida.](http://www.boe.es/buscar/act.php?id=BOE-A-2010-927)   
 "Se escriben en minúsculas excepto si derivan de un nombre propio, en cuyo caso la primera letra es mayúscula. **Como excepción se permite el uso de la letra L en mayúscula o l en minúscula como símbolos del litro, a fin de evitar la confusión entre la cifra 1 (uno) y la letra l (ele).**"  

### Símbolos y unidades IUPAC
Se cita tema de unidades de Kc y Kp en  [equilibrio químico](/home/recursos/quimica/equilibrio-quimico) Hay simbología y unidades fijadas por IUPAC  
[Quantities, Units, Symbols and Nomenclature used in NCEA Chemistry Level 3 and Scholarship Examination Papers](https://www.nzqa.govt.nz/assets/qualifications-and-standards/qualifications/ncea/NCEA-subject-resources/Chemistry/chemistry-reference-sheet-L3-Schl.pdf)   

### Análisis dimensional
 Aparece en currículo LOMCE 4ºESO [Análisis Dimensional - Matemath | DE 0 A 100](https://matemathweb.com/fisica/analisis-dimensional/)   
 [Magnitudes.pdf](https://fisquiweb.es/Apuntes/Apuntes4/Magnitudes.pdf)   

## Medición
 -Definición medida / medición  
 -Métodos de medición  
 --Cualidades instrumentos medición  
 [DivulgaMadrid: La diferencia entre sensibilidad y precisión.](http://divulgamadrid.blogspot.com.es/2014/09/diferencia-sensibilidad-precision.html)   
  
 [ Simulador de medidas](http://teleformacion.edu.aytolacoruna.es/FISICA/document/fisicaInteractiva/medidas/ExpMedida.htm)   
  
--Tipos de errores en la medida  
 --Expresión de resultados de medida  
 ---Redondeo  
  
[LABORATORIO DE FÍSICA I Y FÍSICA GENERAL. PRÁCTICA Nº 1: TEORÍA DE ERROR. PRÁCTICA N° 2: MEDICIONES DE LONGITUD](https://labfisicasabino.files.wordpress.com/2008/06/practica-nc2b0-1-nc2b0-2.pdf)   
Ejercicios unidades-medidas (Flash) [GENMAGIC.ORG - UNIDADES-MEDIDAS](https://www.genmagic.net/repositorio/thumbnails.php?album=2)   
 BANCO DE OBJETOS INTERACTIVOS MULTIMEDIA (genmagic.org @ 2009-2013)  
Enlaza con error absoluto / relativo y tipos de errores, como sistemáticos / paralaje  
  
16 diciembre 2020  
Dos españoles encuentran una solución al enigma del metrónomo de Beethoven   
 [Una solución al enigma del metrónomo de Beethoven](https://www.vozpopuli.com/altavoz/next/metronomo-Beethoven-polemica-investigacion-musica_0_1419159281.html)   
Es decir, que Beethoven se estaba fijando en la parte que no era y estaba anotando los valores mal”.

[Medidas y errores - FisQuiWeb](https://fisquiweb.es/Laboratorio/Medidas/index.htm)  


[El Nobel de Rayleigh - lacienciaparatodos.wordpress.com](https://lacienciaparatodos.wordpress.com/2011/03/01/el-nobel-de-rayleigh/)  
> Pero al final, si lo ganaron y si descubrieron la existencia del argón fue simplemente porque Rayleigh tenía auténtica confianza en su trabajo, ¡y un error del 0,1% le parecía inaceptable!  


[twitter ThePhysicsMemes/status/1841136256224211064](https://x.com/ThePhysicsMemes/status/1841136256224211064)  
![](https://pbs.twimg.com/media/GY0HqqnXYAgdn5N?format=jpg)  

### Actividades asociadas a medida
Aparte de ir al laboratorio, se pueden pensar otras, y enlaza con estimación  
[twitter onio72/status/1311170817594740737](https://twitter.com/onio72/status/1311170817594740737) Inspirado en una conversación de hace siglos con @currofisico, he encargado a mis alumnos de 2º de ESO en Física y Química que determinen la masa de un grano de garbanzo, arroz, lenteja, chícharo y un fideo. Empiezo a divertirme. #iteachphysics  

[twitter alfredo24404099/status/1708066765974974606](https://twitter.com/alfredo24404099/status/1708066765974974606)  
Hola a todos... ahora que estamos con el método científico os dejo por aquí una actividad que bien puede usarse tanto como repaso como para un examen, ambientada en el viejo oeste.... ¡¡cuidado con Black Bart ¡¡ 🤠También lo dejo en fiquillados..  
[Wanted: Back Bart. Francisco Collados](https://drive.google.com/file/d/1HAYcv6DUsk8hJ32uWEZjr6pkRlhZTUiC/view)  
 
[twitter memecrashes/status/1669742719084765185](https://twitter.com/memecrashes/status/1669742719084765185) 
HELP  
Mom pick me up I'm scared  
how tall do u guys think I am?  
174 ± 1 %  
![](https://pbs.twimg.com/media/FyweJobWwAEw1xo?format=jpg)  

###  [Cifras significativas](/home/recursos/recursos-cifras-significativas) 

### Precisión
[twitter fqmente/status/1629774285328842753](https://twitter.com/fqmente/status/1629774285328842753)  
Gracias a un comentario de @DaviidMPB , hemos actualizado nuestros apuntes de actividad científica de 4.° ESO para incorporar las nuevas (1994 😬) definiciones de #precisión, #veracidad y #exactitud.  
![](https://pbs.twimg.com/media/Fp4fEtPX0AAC8P0?format=jpg)  
[The difference between ACCURACY, PRECISION & TRUENESS. Several words are internationally agreed to describe measurement results. This poster should help you understand some of these words. - npl.co.uk](https://www.npl.co.uk/getmedia/10e33275-dcb4-4203-943d-6edf2bbf79ac/ACCURACY_PRECISION_TRUENESS_2023.pdf.aspx)  
[Precisión, veracidad y exactitud - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/4eso/actividad-cientifica/#precisi%C3%B3n-veracidad-y-exactitud)  

[Estimación y errores en la medición - goconqr](https://www.goconqr.com/mapamental/21773372/estimacion-y-errores-en-la-medicion)  

### Teoría de errores / ajuste por mínimos cuadrados 

En 2023 asociado a olimpiadas de Física OFISMAD publica [documento adaptado cálculo de incertidumbres](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/Olimpiadas%20F%C3%ADsica/2023-04-15-OFISMAD-c%C3%A1lculo%20de%20incertidumbres.pdf)  basado en  [Apuntes de Laboratorio - fisicas.ucm.es](https://fisicas.ucm.es/data/cont/media/www/pag-39682/incierto.pdf)  
> 1. Medidas
> 2. Unidades
> 3. Error e incertidumbre
> 4. Cálculo de incertidumbres
> 5. Otros tipos de medidas
> 6. Interpolación lineal
> 7. Presentación de resultados
> 8. Recomendaciones prácticas

 [Errores en las medidas - ehu.es](http://www.sc.ehu.es/sbweb/fisica/unidades/medidas/medidas.htm)   
 Incluye propagación de errores, medidas indirectas función de varias variables

 [Propagación de errores. Técnicas experimentales de Física General - uv.es](http://www.uv.es/zuniga/3.2_Propagacion_de_errores.pdf)  
 [Practica 3 Propagación de Incertidumbres. Medición indirecta. Incertidumbres en cantidades calculadas](https://es.scribd.com/doc/243007562/practica-3-2010-1-pdf)   
 [Apuntes Física Experimental I 2do. Cuat. 2018](http://users.exa.unicen.edu.ar/catedras/fisexp1/files/Apuntes.pdf)  

[Teoría de errores. Laboratorio de Bases Físicas del Medio Ambiente - ugr.es](https://www.ugr.es/~esteban/earth/apuntesbasesfisicas/tr_err.pdf)  
En página 12 incluye expresiones de incertidumbre en pendiente a y en la ordenada en el origen b en un ajuste por mínimos cuadrados  
  
 [Teoría de errores - Incertezas de medición. Física re-Creativa – S. Gil y E. Rodríguez](http://www.fisicarecreativa.com/guias/capitulo1.pdf)  
 [Propagación de errores - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Propagaci%C3%B3n_de_errores) Análisis de datosRegresión lineal ... enlaza con  [recursos calculadora](/home/recursos/recursos-uso-calculadora)   
 
[Determinación de errores y tratamiento de datos. Posadas, A.M. Departamento de Física aplicada, Universidad de Almería](https://w3.ual.es/~aposadas/TeoriaErrores.pdf)  

[Evaluación de datos de medición. Guía para la expresión de la incertidumbre en la medida. Centro Español de Metrología. Septiembre 2008.](https://www.cem.es/sites/default/files/gum20digital1202010_0.pdf)  
 
 
 [Desviación típica - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Desviaci%C3%B3n_t%C3%ADpica)   
 [Corrección de Bessel - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Correcci%C3%B3n_de_Bessel) ¿Usar n o n-1?   
 [¿Cuál es la diferencia entre N y N-1 en el cálculo de la varianza de la población? - i-ciencias.com](https://www.i-ciencias.com/pregunta/5279/cual-es-la-diferencia-entre-n-y-n-1-calculo-varianza-de-poblacion) Por estas razones, guiada por consideraciones pedagógicas (es decir, de centrarse en los detalles que la materia y la glosa sobre los detalles que no), algunas de excelente introducción a las estadísticas, textos ni siquiera se preocupan por enseñar la diferencia: simplemente proporcionan una única fórmula de varianza (dividir por N o n según sea el caso).  

No hay que confundir el ajuste por mínimos cuadrados con el Análisis de Componentes Principales 
[twitter ApuntesCiencia/status/1430048873100324888](https://twitter.com/ApuntesCiencia/status/1430048873100324888)  

[Análisis de Componentes Principales - wikipedia](https://es.wikipedia.org/wiki/An%C3%A1lisis_de_componentes_principales)  

Un poco de humor  
[twitter Quasilocal/status/1659085866508140544](https://twitter.com/Quasilocal/status/1659085866508140544)  
Based on the data collected, my tennis ball will reach orbit by tomorrow  
![](https://pbs.twimg.com/media/FwZA652WcAAA2MV?format=png)  

[¿Qué significa 5 Sigma? - noticiasdelcosmos.com](https://www.noticiasdelcosmos.com/2021/05/que-significa-5-sigma.html)  
[5 Sigma What's That? - scientificamerican.com](https://www.scientificamerican.com/blog/observations/five-sigmawhats-that/)  

## Aparatos de medida, tipos  

 Masa: balanza  
 Tiempo: cronómetro  
 Espacio: regla, calibre  
 Voltaje, Corriente eléctrica: polímetro  

##  [Notación científica](/home/recursos/recursos-notacion-cientifica) 
 ( [En página aparte sobre notación en ciencias: notación científica, śeparador decimal...](/home/recursos/recursos-notacion-cientifica) )  

## Alfabeto griego en física y química
 Se incluye un documento de una cara cuyo objetivo es poner las distintas letras con una tipografía y pronunciación oficial en castellano (a veces se duda cómo se escriben y/o pronuncian)  
 
[Alfabeto Griego en Física y Química y Química pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-medida/AlfabetoGriegoFisicaQu%C3%ADmica.pdf)

También alfabeto griego en [RSEQ Nomenclatura Química y Normas de la IUPAC en Español](https://dialnet.unirioja.es/descarga/libro/873818.pdf)  

## Gráficas en Física y Química

Realizar gráficas e interpretarlas es algo esencial y relacionado con unidades.  
Aquí una idea de "lista de cotejo" para evaluar la realización de gráficas  
[Twitter leysbiotutor/status/1791353178472185989](https://x.com/leysbiotutor/status/1791353178472185989)  
Before I reinvent the wheel - has anyone produced a KS3 Science Graph-drawing SLOP booklet that I could look at please? Thanks!  
![](https://pbs.twimg.com/media/GNwqOJPXEAAsDs4?format=jpg)  
Título  
Etiqueta eje x  
Unidades eje x  
Etiqueta eje y  
Unidades eje y  
Escala adecuada  
Puntos  
Línea  
Regla  
Clave (si aplica)  
Total  

  

