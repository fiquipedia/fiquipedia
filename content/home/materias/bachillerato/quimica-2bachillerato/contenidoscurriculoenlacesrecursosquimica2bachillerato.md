# Contenidos currículo con enlaces a recursos: Química 2º Bachillerato

Esta página está en construcción, es solamente un primer borrador. Pretende conseguir uno de los objetivos puestos en la página inicial de la web.  
Para ver el currículo completo "estático", mirar  [Currículo Química 2º Bachillerato](/home/materias/bachillerato/quimica-2bachillerato/curriculoquimica-2bachillerato)   

### 1. Contenidos comunes.
— Utilización de estrategias básicas de la actividad científica tales como el planteamiento de problemas y la toma de decisiones acerca del interés y la conveniencia o no de su estudio; formulación de hipótesis, elaboración de estrategias de resolución y de diseños experimentales y análisis de los resultados y de su fiabilidad.  
— Búsqueda, selección y comunicación de información y de resultados utilizando la terminología adecuada.  

### 2. Estructura atómica y clasificación periódica de los elementos.
— Espectros atómicos. Orígenes de la teoría cuántica. Hipótesis de Planck. Efecto fotoeléctrico. Modelo atómico de Bohr y sus limitaciones. Introducción a la mecánica cuántica moderna. Su importancia. Orbitales atómicos. Números cuánticos. Configuraciones electrónicas: Principio de Pauli y regla de Hund.  
— Evolución histórica de la ordenación periódica de los elementos. Tabla periódica de Mendeleev. Predicciones y defectos.  
— Sistema periódico actual. Estructura electrónica y periodicidad. Tendencias periódicas en las propiedades de los elementos.  

### 3. El enlace químico y propiedades de las sustancias.
— Concepto de enlace en relación con la estabilidad energética de los átomos enlazados.  
— Enlace iónico. Concepto de energía de red. Ciclo de Born-Haber. Propiedades de las sustancias iónicas.  
— Enlace covalente. Estructuras de Lewis. Parámetros moleculares. Polaridad de enlaces y moléculas. Teoría del enlace de valencia. Hibridación de orbitales atómicos (sp, sp2, sp3) y teoría de la repulsión de pares de electrones de la capa de valencia. Sólidos covalentes. Propiedades de las sustancias covalentes.  
— Fuerzas intermoleculares.  
— Estudio cualitativo del enlace metálico. Propiedades de los metales.  
— Propiedades de algunas sustancias de interés industrial o biológico en función de su estructura o enlaces.  

### 4. Transformaciones energéticas en las reacciones químicas.Espontaneidad de las reacciones químicas.  
— Sistemas termodinámicos. Variables termodinámicas. Cambios energéticos en las reacciones químicas. Procesos endo y exotérmicos.  
— Primer principio de la termodinámica. Transferencias de calor a volumen y a presión constante. Concepto de entalpía. Cálculo de entalpías de reacción a partir de las entalpías de formación. Diagramas entálpicos. Ley de Hess. Entalpías de enlace.  
— Segundo principio de la termodinámica. Concepto de entropía. Energía libre. Espontaneidad de las reacciones químicas.  
— Aplicaciones energéticas de las reacciones químicas. Repercusiones sociales y medioambientales.  
— Valor energético de los alimentos. Implicaciones para la salud.  

### 5. El equilibrio químico.
— Introducción a la cinética química: Aspecto dinámico de las reacciones químicas. Conceptos básicos de cinética: Velocidad de reacción y factores de los que depende. Orden de reacción y molecularidad.  
— Concepto de equilibrio químico. Características macroscópicas e interpretación microscópica. Cociente de reacción y constante de equilibrio. Formas de expresar la constante de equilibrio: Kc y Kp; relación entre ambas. Factores que modifican el estado de equilibrio: Principio de Le Chatelier. Equilibrios heterogéneos.  
— Las reacciones de precipitación como equilibrios heterogéneos. Aplicaciones analíticas de las reacciones de precipitación.  
— Aplicaciones del equilibrio químico a la vida cotidiana y a procesos industriales.  

### 6. Ácidos y bases.
— Concepto de ácido y base según las teorías de Arrhenius y Brönsted-Lowry. Concepto de pares ácido-base conjugados. Fuerza relativa de los ácidos. Constante y grado de disociación. Equilibrio iónico del agua.  
— Concepto de pH. Cálculo y medida del pH en disoluciones acuosas de ácidos y bases. Importancia del pH en la vida cotidiana. Reacciones de neutralización. Punto de equivalencia.  
— Volumetrías ácido-base. Aplicaciones y tratamiento experimental.  
— Equilibrios ácido-base de sales en disolución acuosa. Estudio cualitativo de la hidrólisis.  
— Estudio de algunos ácidos y bases de interés industrial y en la vida cotidiana. Amoniaco, ácidos sulfúrico, nítrico y clorhídrico. El problema de la lluvia ácida y sus consecuencias.  

### 7. Introducción a la electroquímica.
— Concepto de oxidación y reducción. Sustancias oxidantes y reductoras.  [Número de oxidación](/home/recursos/quimica/estados-de-oxidacion) . Reacciones de oxidación reducción. Ajuste de reacciones red-ox por el método del ión-electrón. Estequiometría de las reacciones red-ox.  
— Estudio de la pila Daniell. Potencial normal de reducción. Escala de oxidantes y reductores.  
— Potencial de una pila. Potencial de electrodo. Espontaneidad de los procesos red-ox. Pilas, baterías y acumuladores eléctricos.  
— Electrólisis. Importancia industrial y económica de la electrólisis.  
— La corrosión de metales y su prevención. Residuos y reciclaje.  

### 8. Química del carbono.
—  [Nomenclatura y formulación](/home/recursos/quimica/formulacion)  de los principales compuestos orgánicos. Estudio de los principales tipos de reacciones orgánicas: Sustitución, adición, eliminación y oxidación-reducción.  
— Ejemplos característicos de reacciones orgánicas de interés, con especial referencia a la obtención de alcoholes, ácidos y ésteres; propiedades e importancia de los mismos.  
— Polímeros y reacciones de polimerización. Valorar la utilización de sustancias orgánicas en el desarrollo de la sociedad actual. Problemas medioambientales.  
— La síntesis de medicamentos. Importancia y repercusiones de la industria química orgánica.  

