
# Páginas ciencia / física / química / recopilatorios enlaces recursos

También relacionado con  [páginas personales, blogs ...](/home/recursos/paginas-blog-personales)   

* En el [repositorio de aulas virtuales de EducaMadrid](https://aulavirtual35.educa.madrid.org/aulas/) hay aulas con recursos, y se citan más:  
* [Recursos CEDEC - INTEF](https://cedec.intef.es/recursos/)
* [Aula Abierta ARASAAC (Aragón)](http://aulaabierta.arasaac.org/)
* [Repositorio cursos AEDUCAR (Aragón](https://aeducar.es/repositorio-cursos/))
* [CREA (Extremadura)](https://emtic.educarex.es/creasite/recursos-crea)
* [CREA (Andalucía)](http://www.juntadeandalucia.es/educacion/permanente/materiales/)
* [Contenidos educativos (Castilla-La Mancha)](http://www.educa.jccm.es/recursos/es/contenidos-educativos)
* [Recursos educativos (Castilla y León)](http://www.educa.jcyl.es/educacyl/cm/gallery/web_recursos/index.html)
* [Educastur (Asturias)](https://alojaweb.educastur.es/web/abierto24x7/recursos)
* [eDUCERE (La Rioja)](https://educere.larioja.org/recursos-educativos)
* [Recursos digitales (Canarias)](http://www3.gobiernodecanarias.org/medusa/ecoescuela/recursosdigitales/)
* [Contenidos digitales (Cantabria)](https://www.educantabria.es/recursos/contenidos-digitales-cantabros.html)
* [Recursos FP (Cataluña)](https://ioc.xtec.cat/educacio/recursos)
  

   * Noticias y enlaces de física  
 [Главная - buildrussia.ru](http://fisica.35webs.com) /  
Muchos enlaces a eltamiz, xatakaciencia, cienciakanija ...  

   * Química  
 [Webs Ciencias](http://www.aula21.net/primera/paginaspersonales.htm)   
Recopilatorio de enlaces, no indica fecha y en noviembre 2011 hay algunos no operativos** (ToDo revisar esos enlaces y ponerlos por separado los que sean interesantes**  

   * Física  
 [Webs Fsica](http://www.aula21.net/primera/fisica.htm)   
Recopilatorio de enlaces, no indica fecha y en noviembre 2011 hay algunos no operativos
   * Materiales didácticos Ciencias.  
 [index.shtml](http://iescarbula.net/gt/GTCO-0708152/fisica/index.shtml)   
Física, Química, laboratorios virtuales ...Grupo de Trabajo CIENTÍFICO-TECNOLÓGICO - I.E.S. Carbula (Almodóvar del Río, Córdoba)
   * Physics Hotlists and Portals de la web [Fsica re-Creativa](http://www.fisicarecreativa.com/)   
 [mapa_frc.htm#Physics Hotlists and Portals](http://www.fisicarecreativa.com/mapa_frc.htm#Physics%20Hotlists%20and%20Portals) 
   * Materiales Didácticos de Ciencias  
 [index.shtml](http://iescarbula.net/gt/GTCO-0708152/fisica/index.shtml)   
Grupo de Trabajo CIENTÍFICO-TECNOLÓGICO - I.E.S. Carbula (Almodóvar del Río, Córdoba). Física, Química, laboratorios virtuales, agrupados por bloques
   * Recursos Universidad Nacional Autónoma de México  
 [(http://distancia.cuaed.unam.mx/recursos/](http://distancia.cuaed.unam.mx/recursos/) 
   * Páxinas de Física e Química en Internet  
 [enlaces.html](http://perso.wanadoo.es/alfbar/enlaces.html)   
Alfonso J. Barbadillo Marán, Dpto. Física e Química, IES Elviña A Coruña
   * "Enlaces a páginas con recursos educativos"  
 [Oposiciones secundaria: mis consejos. Física y Química](http://opsfisquim.blogspot.com/)   
Página dedicada en general a las oposiciones de física y química, pero en la parte derecha incluye un listado de enlaces a recursos. Agustín Sánchiz, licenciamiento cc-by-nc-nd.
   * PortaESO  
 [Página personal de Antonio Bueno](http://portaleso.com/)   
Muchas materias además de física y química. Fundamentalmente recopila enlaces y recursos. Se puede colaborar. Portaleso, es una marca registrada en el Registro de Patentes y Marcas con el número M 2558399(9), cuyo propietario es: Antonio Bueno Juan.
   * CLUSTER. Divulgación científica  
 [http://cluster-divulgacioncientifica.blogspot.com.es/](http://cluster-divulgacioncientifica.blogspot.com.es/) 
   * Página Web del Departamento de Física y Química del IES Nuestra Señora de la Almudena de Madrid.  
 [Documento sin título](http://almudenafyq.webcindario.com/)   
Enlaces, materiales separados por física / química y por curso. Antonio Jose Vasco Merino. 
   * Recopilatorio de recursos, imágenes, vídeos... (no específicos de física y química)  
 [Recursos](http://quierounblog.wikispaces.com/Recursos) 
   * IES Leonardo da Vinci. Alicante. Selección de enlaces a páginas WEB de interés para alumnos y profesores de física y de química  
 [Enlaces.htm](http://intercentres.edu.gva.es/iesleonardodavinci/Fisica/Enlaces.htm) 
   * INTA "Descrubre y Aprende", no solamente sobre astronomía ...  
 [profesorado2.htm](http://www.inta.es/descubreAprende/htm/profesorado2.htm) 
   *  [Enlaces de interés](http://chopo.pntic.mec.es/jmillan/enlaces.htm)   
Enlaces Química y Física, ES "Rey Fernando VI" @ Jesús Millán Crespo
   * Yo con Ciencia, más  
 [Yo Con Ciencia, Más: FÍSICA Y QUÍMICA](http://yoconcienciamas.blogspot.com.es/p/fisica-y-quimica.html)  (enlace validado en 2015)   
Alfonso León
   * How Stuff Works  
 [How stuff works](http://www.howstuffworks.com/) 
   * Explain that Stuff  
 [Explain that Stuff](http://www.explainthatstuff.com/) 
   * ¿Cómo funciona qué?  
 [Como Funciona Que - NOSOTROS EXPLICAMOS EL MUNDO](http://comofuncionaque.com/) 
   *  [Animagraffs - Animated infographics about everything.](https://animagraffs.com/)   
   * Exploring Nature  
Recursos biología con partes de física  
 [1823](http://exploringnature.org/db/view/1823) 
   * La ciencia para todos  
 [Cubaeduca | Portal educativo](http://www.cubaeduca.cu/medias/cienciatodos/)   
Para acceder a los textos completos de los libros haga clic sobre los enlaces a cada uno de los volúmenes que integran la Colección. El Volumen IV está en edición. También puede consultar el listado de todos los libros por  [MATERIAS](http://www.cubaeduca.cu/medias/cienciatodos/Libros_M.html)  o por  [NÚMERO](http://www.cubaeduca.cu/medias/cienciatodos/Libros_N.html)  consecutivo.   
\Ciencia_Todos\Libros_1\ -->  [Volumen I](http://www.cubaeduca.cu/medias/cienciatodos/Libros_1/ciencia2/menu.htm)   
\Ciencia_Todos\Libros_2\ -->  [Volumen II](http://www.cubaeduca.cu/medias/cienciatodos/Libros_2/ciencia3/menu.htm)   
\Ciencia_Todos\Libros_3\ -->  [Volumen III](http://www.cubaeduca.cu/medias/cienciatodos/Libros_3/ciencia3/menu.html)   
 
