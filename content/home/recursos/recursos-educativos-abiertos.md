
# Recursos Educativos Abiertos

En la página de  [libros](/home/recursos/libros)  se citan algunos libros libres que encajan en esta idea.Hay muchos recursos de muchos tipos y en muchas categorías que encajarían; en toda la página intento enlazar algunos,y compartir los de elaboración propia.  

## Definición REA - OER
Recursos Educativos Abiertos, REA por sus siglas en español (OER, Open Educational Resources en inglés), son *“materiales digitalizados ofrecidos de forma abierta y gratuita a los educadores, estudiantes y auto-didactas, para utilizar y re-utilizar en la enseñanza, aprendizaje e investigación”. *El término “recursos educativos abiertos” fue adoptado por primera vez por la UNESCO en el 2002, en el foro sobre el Impacto del Open CourseWare para la Educación Superior en Países en Desarrollo ( [Open Educational Resources - en.wikipedia.org](http://en.wikipedia.org/wiki/Open_educational_resources) ).  

## Vídeos sobre REA - open education
 [Winners Announced! – Why Open Education Matters Video Competition - creativecommons.org](https://creativecommons.org/2012/07/18/winners-announced-why-open-education-matters-video-competition/) Tres primeros puestos en concurso 2012   

## Información general sobre REA

   *  [Módulo I: Introducción a los recursos educativos abiertos - vidadigital.net](http://www.vidadigital.net/blog/2008/08/30/tutorial-sobre-recursos-educativos-abiertos/)   
Traducción y adaptación del tutorial sobre recursos educativos abiertos desarrollado por Judith Baker. El mismo incluye los siguientes módulos:  
Módulo 1: Visión general  
Módulo 2: Open CourseWare  
Módulo 3: Derechos de autor, Uso Justo, y TEACH Act  
Módulo 4: Herramientas para identificar y seleccionar REA  
Módulo 5: Accesibilidad y diseño universal  
Módulo 6: Fuentes REA de acuerdo a las disciplinas  
Módule 7: Uso de fuentes primarias  
Módul0 8: Fuentes de libros de texto abiertos  
Módule 9: Desarrollo y colaboración  
Módulo 10: Entrega y organización de los REA  
Módulo 11: Apoyo para el uso de los REA  


[REA y competencia digital del alumnado - intef](https://cedec.intef.es/rea-y-competencia-digital-del-alumnado/)  

## Fuentes de REA
   * [Openverse](https://wordpress.org/openverse/)  
   Explore more than 600 million creative works. An extensive library of free stock photos, images, and audio, available for free use  
   * [Internet Archive](https://archive.org/)  
   "archive.org Search the history of over 752 billion web pages on the Internet."  
   Guarda histórico de páginas en Internet (waybackmachine) pero también recursos.   
   * [Wikimedia Commons](https://commons.wikimedia.org)  
   * [5 directorios internacionales de recursos educativos abiertos (OER)  (publicado 2011, versión 2012 en archive.org)](https://web.archive.org/web/20120621160139/http://camarotic.es/?p=1506)   
(los recursos que contiene los cito por separado)  
   * [Open Educational Resources Commons - Browse OER Materials](http://www.oercommons.org/oer)   
Creado en 2007 por ISKME (Institute for the Study of Knowledge Management in Education), licenciamiento cc-by-nc-sa  
Con más de 30.000 recursos que podemos seleccionar desde su  [buscador avanzado](http://www.oercommons.org/advanced-search) .
   * [Open Culture](https://www.openculture.com/)  

>What is Open Culture’s Mission?  
Open Culture brings together high-quality cultural & educational media for the worldwide lifelong learning community. Web 2.0 has given us great amounts of intelligent audio and video. It’s all free. It’s all enriching. But it’s also scattered across the web, and not easy to find. Our whole mission is to centralize this content, curate it, and give you access to this high quality content whenever and wherever you want it. Some of our major resource collections include:  
    1,700 Free Online Courses from Top Universities  
    4,000+ Free Movies Online: Great Classics, Indies, Noir, Westerns, Documentaries & More  
    1,000 Free Audio Books: Download Great Books for Free  
    800 Free eBooks for iPad, Kindle & Other Devices  
    MOOCs from Great Universities (Many With Certificates)  
    Learn 46 Languages Online for Free: Spanish, Chinese, English & More  
    200 Free Kids Educational Resources: Video Lessons, Apps, Books, Websites & More  

   * Connexions  
 [OpenStax CNX](http://cnx.org/)   
A place to view and share educational material made of small knowledge chunks called modules that can be organized as courses, books, reports, etc.  
Comentado en libros  
En 2017 aparece como "OpenStax CNX", ligado a   
OpenStax( a nonprofit based at Rice University)  
 [OpenStax](https://openstax.org/subjects/science)   
[En 2020 anuncian que desaparecen](https://openstax.org/blog/saying-goodbye-cnx)  y quedan en [archive.org]
   * [MERLOT - Multimedia Education Resource for Learning and Online Teaching](http://www.merlot.org/merlot/index.htm)   
Licenciamiento creative commons. Ofrecido por California State University en colaboración con otras instituciones, tiene más de 99.000 usuarios registrados con más de 30.000 recursos localizables mediante su  [buscador avanzado](http://www.merlot.org/merlot/advSearchMaterials.htm) .
   * [Curriki | Shaping the Future of Learning](http://www.curriki.org/)   
 Licenciamiento Creative Commons. Curricular Wiki, organización sin ánimo de lucro con diversos patrocinios que ofrece más de  [40.000 recursos libres](http://www.curriki.org/xwiki/bin/view/Search/#o%3Aa%3Do%253Aresource%253Do%25253Aa%25253Db%2525253A1%5Ef%3Do%253Aresource%253Do%25253Aterms%25253Ds%2525253A)  para el currículo no universitario, con más de 250.000 usuarios registrados.  
 En 2022 el enlace de búsqueda de recursos no funciona ni están enlazados claramente en su web  
   * [LeMill Web community for finding, authoring and sharing open educational resources](http://lemill.net/)   
 Datos noviembre 2011: 23869 teachers from 69 countries. 41022 learning resources in 67 languages.  
 En 2022 enlace no funciona, parece un proyecto abandonado [LeMill, case studies creativecommons](https://wiki.creativecommons.org/wiki/Case_Studies/LeMill)  
   * [OpenDOAR Directory of Open Access Repositories](http://www.opendoar.org/)   
 Listado de más de 2.000 repositorios de materiales de libre disposición, validados, permite búsquedas.  
 En 2022 redirige a [sherpa.ac.uk / opendoar](https://v2.sherpa.ac.uk/opendoar/)  y ofrece búsquedas de repositorios por país  
 [Ejemplo España](https://v2.sherpa.ac.uk/view/repository_by_country/Spain.html) 180 resultados en 2022  
   * [WikiEducator - Free elearning content](http://wikieducator.org/)   
 Incluye tutoriales para búsqueda de recursos educativos abiertos  [Open Educational Content es/olcos/SEARCH es - WikiEducator](http://wikieducator.org/Open_Educational_Content_es/olcos/SEARCH_es) 
   * [Free educational resources for Mathematics, Physics, Chemistry, Biochemistry, Astronomy, Geophysics and Others - VaxaSoftware - Educational documents](http://www.vaxasoftware.com/doc_eduen/index.html)   
Indica "VaxaSoftware makes no representation about the accuracy, correctness, or suitability of this material for any purpose." En la parte de química tiene numerosas tablas de constantes: constantes de acidez y basicidad, constantes de equilibrio, valores termoquímicos (entalpías, energías de Gibbs, entropías...)
   * [Eduteka OER- Recursos Educativos Abiertos (REA)](http://www.eduteka.org/OER.php)   
Artículo con enlaces. Autor: Juan Carlos López García Publicado: 2009-02-08, cc-by-nc-sa  
En 2022 el enlace está operativo pero redirige a [eduteka.icesi.edu.co](https://eduteka.icesi.edu.co/articulos/OER)  
   * [UNCW Randall Library Catalog](http://libcat.uncw.edu/search~S4/X?SEARCH=&searchscope=4&SORT=D&b=il)  Permite buscar recursos por taxonomía: primero disciplina( chemistry, biology, physics, mathematics, y computer science), y dentro de cada uno de ellos por subject y topic.
   *  [Welcome to CK-12 Foundation | CK-12 Foundation](http://www.ck12.org/)   
cc-by-nc  
FlexBook® Textbooks  
 [Welcome to CK-12 Foundation | CK-12 Foundation](http://www.ck12.org/physics/)   
 [Welcome to CK-12 Foundation | CK-12 Foundation](http://www.ck12.org/chemistry/)   
También simulaciones (se ponen en apartado de  [simulaciones](/home/recursos/simulaciones) )  


Españoles
   * [Agrega2](http://agrega.educacion.es/)   
Agrega 2 es un proyecto desarrollado entre el Ministerio de Educación, Cultura y Deporte, Red.es y las Comunidades Autónomas. La federación de repositorios de objetos digitales educativos Agrega es una plataforma que cuenta con nodos en todas las Consejerías de Educación de las Comunidades Autónomas. Los contenidos educativos que se pueden encontrar están organizados de acuerdo al currículo de las enseñanzas de niveles anteriores a la universidad y están preparados para su descarga y uso directo por el profesorado y los alumnos. 
      * [Agrega2 Junta de Andalucía](http://agrega.juntadeandalucia.es/)  
      * [Crea tu REA para el Proyecto REA Andalucía](https://edea.juntadeandalucia.es/bancorecursos/file/6d215d5c-1983-41e3-80d1-50572b852eb9/1/guia_crea_tu_rea_proyecto_rea_andalucia.zip/index.html)  
      * [Averroes. Contenidos Digitales - juntadeandalucia.es](https://www.juntadeandalucia.es/educacion/portalaverroes/contenidosdigitales)  
   * [Proyecto  EDIA (Educativo, Digital, Innovador y Abierto)](https://cedec.intef.es/proyecto-edia/)  
     [EDIA ciencias](https://cedec.intef.es/proyecto-edia-ciencias/)   
     [Recursos cedec.inte.es](http://cedec.intef.es/recursos/)  
   * [Procomún INTEF. Recursos educativos en abierto](https://procomun.intef.es/)
   * [Banco de imágenes y sonidos INTEF](http://recursostic.educacion.es/bancoimagenes/web/)  
   Antiguo. Usa flash player en 2022 !!  
   * [Eduvers](https://www.eduvers.org/inicio)  
   * [Recursos - didactalia.net](https://didactalia.net/comunidad/materialeducativo/recursos) 
   * [Directorio de recursos educativos gratis - educaplay.com](https://es.educaplay.com/recursos-educativos/) 
   * [Proyecto Facilitamos &#124; Facilitamos](http://facilitamos.catedu.es/portal-facilitamos/) 
   * [Recursos Educativos – Material Didáctico - pantallasamigas.net](https://www.pantallasamigas.net/recursos-educativos-material-didactico/) 
   * [Recursos-educativos - educa.jccm.es](http://www.educa.jccm.es/recursos/es/recursos-educativos) 
   * [Recursos | Recursos TIC | EducaMadrid ](https://www.educa2.madrid.org/web/recursostic/recursos/-/visor/contenidos-digitales-organizados-por-nivel-educativo) 
      * [ESPAD Educación Secundaria para Personas Adultas a Distancia](https://www.educa2.madrid.org/web/recursostic/espad)  
      * [CIDEAD  Centro para la Innovación y Desarrollo de la Educación a Distancia](https://www.educa2.madrid.org/web/recursostic/eso-cidead)  
   *  [recursos. XTEC - Xarxa Telemàtica Educativa de Catalunya](http://xtec.gencat.cat/ca/recursos/) 
   *  [Conteni2 educarex](https://conteni2.educarex.es/) 
   *  [Rincones educarex](https://rincones.educarex.es/) 
   *  [Recursos educativos - theflippedclassroom.es](https://www.theflippedclassroom.es/recursos-educativos/) 
   *  [La red educativa escolar - Tiching](http://es.tiching.com/)   
   
   [READ Recursos Educativos Abiertos Digitales - gobiernnodecanarias.org](https://www3.gobiernodecanarias.org/medusa/ecoescuela/recursosdigitales/)  

[Miles de materiales educativos en la red - xarxatic.com](https://xarxatic.com/miles-de-materiales-educativos-en-la-red/)  
  
[Free Educational Resources for Teachers - perimeterinstitute.ca](https://resources.perimeterinstitute.ca/)   
> Perimeter Institute's free digital resources are designed to help teachers explain a range of important physics and science topics. Each compilation includes a set of lesson plans, hands-on activities and demos, modifiable worksheets, background information for teachers, and original PI videos.  
 
Asociado a educación a distancia se suelen generar recursos online que pueden ser REA  
[Contenidos y recursos educativos Andalucía. Materiales educación permanente - juntadeandalucia.es](https://www.juntadeandalucia.es/educacion/permanente/materiales/)  

Asociado a artículos
[Twitter OACerebro/status/1824959280870469862](https://x.com/OACerebro/status/1824959280870469862)  
Guía rápida para descargar artículos científicos.   
No siempre nuestras instituciones cuentan con los recursos suficientes para pagar todos los derechos.   
Recuerde que siempre tiene la opción de solicitar el artículo al autor.  
![](https://pbs.twimg.com/media/GVOOyyiWgAAZqEo?format=jpg)
