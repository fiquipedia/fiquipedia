1. [Licenciamiento](#Licenciamiento)
2. [¿Puedo usar estos materiales en otro sitio?](#TOC-Puedo-usar-estos-materiales-en-otro-sitio-)
3. [¿Es legal citar cosas, por ejemplo imágenes, que tienen derechos de autor?](#TOC-Es-legal-citar-cosas-por-ejemplo-im-genes-que-tienen-derechos-de-autor-)
4. [¡Compartid, Malditos!](#TOC-Compartid-Malditos-)
5. [¿Puedes compartir mis materiales en FiQuiPedia?](#TOC-puedes-compartir-mis-materiales-en-fiquipedia) 

# Licenciamiento <a name="Licenciamiento"></a>

Todas las páginas de este sitio tienen una información general sobre licenciamiento, y enlazan a esta página donde se aportan más detalles

Parece que en general la licencia que tenemos que poner a un material, condicionada por los materiales de partida, es única, pero realmente no es así: distintas partes del material elaborado pueden tener distintas licencias y no hay que pensar en un único tipo de licencia, por lo que creo que cobra mucho sentido lo de "salvo donde se indique lo contrario".

**Todos los contenidos incluidos en esta página de elaboración propia, salvo que se indique lo contrario, tendrán licencia  [Creative Commons Reconocimiento CompartirIgual 4.0 Internacional (CC BY SA 4.0)](http://creativecommons.org/licenses/by-sa/4.0/deed.es_ES) El autor de los materiales de elaboración propia es Enrique García Simón**

Cuando los contenidos no sean de elaboración propia, en la medida de lo posible intentaré incluir recursos con licencia Creative Commons, para que puedan ser reutilizados legalmente, citando en cualquier caso siempre los recursos con su autor, fuente y licenciamiento, siendo responsabilidad del usuario validar el licenciamento de materiales referenciados. Se incluye como pie de todas las páginas del sitio esta información de licenciamiento.  
No existen unos materiales definidos y estáticos, ya que el contenido del sitio puede variar y variará frecuentemente. El autor manifiesta su intención de mantener indefinidamente el licenciamiento cc (aunque puede variar la modalidad) indicado para cualquier material de elaboración propia incluido.  

Un ejemplo que aplica perfectamente a este sitio web fiquipedia, donde hay mezclas de licenciamiento el el mismo sitio web, al recopilarse materiales diversos: [http://www.juntadeandalucia.es/averroes/mochiladigital/legales.html](http://web.archive.org/web/20120827030522/www.juntadeandalucia.es/averroes/mochiladigital/legales.html) (_waybackmachine, enlace no operativo en 2021_)  
_Cada uno de los materiales y documentos tiene su propia licencia, decidida por el/los propietario/s de sus derechos. Se ha puesto especial hincapié en proporcionar recursos que puedan ser reutilizados y modificados libremente. No obstante en ocasiones se han seleccionado materiales bajo la cláusula «*no comercial*»; el usuario deberá tener en cuenta en qué circunstancias los utiliza._  

En general no almaceno recursos, sino que los enlazo: el responsable de los derechos de autor es el que almacena los recursos, como lo hago yo con los propios. Eso genera más problemas de enlaces rotos, pero minimiza los de licenciamiento.   

## ¿Puedo usar estos materiales en otro sitio? <a name="TOC-Puedo-usar-estos-materiales-en-otro-sitio-"></a>

Como ya he recibido varias peticiones sobre utilizar mis materiales, aclaro ideas, y así no lo cuento una y otra vez por correo, y la próxima enlazo aquí :-)
- Al tener licenciamiento cc-by (cc-by-sa) nadie tiene por qué pedirme permiso para usar estos materiales, lo estoy dando automáticamente; mientras mantenga cc-by basta con que se me cite. Si tengo cc-by-sa además de citar hay que compartir con el mismo licenciamiento.Recomiendo echar un vistazo a este buen resumen del licenciamiento Creative Commons [http://www.xarxatic.com/infografia-sobre-las-licencias-creative-commons/](http://web.archive.org/web/20140227200115/http://www.xarxatic.com/infografia-sobre-las-licencias-creative-commons) (_waybackmachine, no operativo en 2021_)  
Enlaza con [Licencias Creative Commons. Fácil infografía. - curioseandito.blogspot.com](https://curioseandito.blogspot.com/2011/03/licencias-creative-commons-facil.html) que a su vez viene de [](https://geeksroom.com/2011/03/todo-lo-que-necesitas-saber-de-creative-commons-infografia/46356/)  
![](https://lh5.googleusercontent.com/-11vsD0tp0Hg/TXbA5YjxIRI/AAAAAAAAFJ8/bzPwoBxUsi4/s1600/creative-commons-comic-ingles.png)  
- Inicialmente puse licenciamento cc-by sin añadir el "sa" ni el "nd", y mi idea sigue siendo mantener el cc, aunque con modificaciones de términos. En 2017 empiezo a usar cc-by-sa en los nuevos materiales y voy cambiando de cc-by a cc-by-sa algunos según los reviso, pero el cambio total llevará tiempo.
- Además de que sean materiales libres, mi idea inicial y que pretendo mantener es no poner nunca publicidad. Con el número de visitas creciente me comentan que supondría ingresos por publicidad, pero mi objetivo no es ingresar dinero, el coste para mi es en tiempo y el poco coste del dominio al año, y creo que una página sin publicidad es más agradable de visitar.
- Sobre que facilite los "ficheros word" o "ficheros google docs con el add-on para latex g-math"; como comento en la página uso fundamentalmente LibreOffice, aunque según para qué también inkscape, latex (con paquete pst-optic), bkchem ... En los enunciados y soluciones PAU sí que puse en su momento que igual subía los documentos .odt, pero mucho de lo que tengo es borrador, algunos tienen páginas enteras de anotaciones (no es broma, por ejemplo en el de óptica tengo el texto original de la norma DIN 1335 en alemán junto con la traducción) y comentarios que no salen en el pdf. Si alguien quiere el documento fuente que contacte y me explique para qué es, pero será para algo colaborativo ... puedo intentar "limpiar" y pasar ese documento en concreto, o modificarlo uno de los dos o conjuntamente, o crear algo nuevo, o enlazarlo...
- Los pdf los comparto con todos, no es la primera vez que me piden los documentos; empecé a detectar que la gente subía a scribd los pdf y había versiones desactualizadas, así que empecé a añadir códigos QR que enlazan a la última versión del documento en FiQuiPedia. 

##  ¿Es legal citar cosas, por ejemplo imágenes, que tienen derechos de autor? <a name="TOC-Es-legal-citar-cosas-por-ejemplo-im-genes-que-tienen-derechos-de-autor-"></a>
Poner imágenes en recursos es útil, pero a veces es complicado encontrar alguna con el licenciamiento adecuado o se necesita una muestra de un "original"  
En la medida de lo posible intento citar la fuente de las imágenes, que se incluyen por ejemplo en actividades o en ejercicios como mera ilustración, en unos materiales cc-by-sa que no tienen ánimo de lucro.  
Yo no tengo puesto explícitamente el "nc" ( [CC's NonCommercial (NC) licenses prohibit uses that are "primarily intended for or directed toward commercial advantage or monetary compensation."](https://creativecommons.org/faq/#does-my-use-violate-the-noncommercial-clause-of-the-licenses) ), pero ni cuando uso imágenes ni en otros materiales tengo ánimo de lucro; si alguien lo usase con ese fin (al publicarlo yo sin "nc" sí lo permito), sería el que lo hiciese quien tiene que revisarlo.  
Los materiales de la página están destinados a educación, y considero que el uso puntual de imágenes está amparado en  [Artículo 32 de Real Decreto Legislativo 1/1996](http://www.boe.es/buscar/act.php?id=BOE-A-1996-8930#a32) , al tiempo que se manifiesto la disposición a retirar una imagen citada en caso de que el propietario de los derechos lo indique.En ese caso intentaría buscar otra imagen o realizarla yo mismo como dibujo si es posible.

## ¡Compartid, Malditos! <a name="TOC-Compartid-Malditos-"></a>
Cito este post que enlaza con la idea de compartir, cuidando el licenciamiento [http://algoquedaquedecir.blogspot.com/2018/04/compartid-malditos.html](http://algoquedaquedecir.blogspot.com/2018/04/compartid-malditos.html) 

## ¿Puedes compartir mis materiales en FiQuiPedia? <a name="TOC-puedes-compartir-mis-materiales-en-fiquipedia"></a>
He recibido la pregunta varias veces, escribo la respuesta para redirigir aquí:  
* Si tienes tu web accesible con enlace y ya compartes, sin problema alguno en enlazar. El objetivo de la web es enlazar a otros recuros que otros comparten y encantado.
Por ejemplo asociado a oposiciones enlazo materiales de [Rodrigo Alcaraz](https://fisiquimicamente.com/recursos-fisica-quimica/oposiciones/fisica/cantabria-2018-o1-e1/cantabria-2018-o1-e1.pdf), y justamente antes de que él tuviese su web, los publicaba yo en FiQuiPedia  
* Si lo que quieres es que yo publique en FiQuiPedia:
     * Publicarlo aquí es una decisión personal mía,lo miraré y valoraré cuando pueda. Supone añadir mantenimiento, ya que si los actualizas y los almaceno no, tengo que actualizarlo yo aquí; es imprescindible que los materiales indiquen fecha de revisión. 
     * Cuido mucho el licenciamiento por lo que los materiales deben ser de elaboración propia y/o respetar licenciamiento: citar autoría y licenciamiento: yo uso y propongo cc-by-sa (cc=creative commons, por defecto das permiso para compartir, by=atribución, al compartir hay que citar la autoría, sa=share alike, al compartir hay que seguir compartiendo de la misma manera). 
Almaceno por ejemplo algunos materiales de oposiciones de [Leticia Cabezas](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/TemarioBOE93/Temas%20propios%20FyQ%20Leticia%20Cabezas.pdf) (cc-by-nc-nd) y [Javier Perán Jódar, Javier Sánchez Pina y José A. Illán Ortuño](/home/recursos/recursos-para-oposiciones/2021-06-19-Murcia-FQ-enunciados-completos-resuelto.pdf) (cc-by-nc-nd)
     * Inicialmente tenía problemas de espacio, pero con la migración a GitLab en 2021 y el uso de [https://gitlab.com/fiquipedia/drive.fiquipedia/](https://gitlab.com/fiquipedia/drive.fiquipedia/) en principio no hay problema.

