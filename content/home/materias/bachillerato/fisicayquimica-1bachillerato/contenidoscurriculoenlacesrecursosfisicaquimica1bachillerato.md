# Contenidos currículo con enlaces a recursos: Física y Química 1º Bachillerato

Esta página está en construcción, es solamente un primer borrador. Pretende conseguir uno de los objetivos puestos en la página inicial de la web.  
Para ver el currículo completo "estático", mirar  [Currículo Física y Química 1º Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculofisicayquimica-1bachillerato)   

### 1. Contenidos comunes.
— Utilización de estrategias básicas de la actividad científica tales como el planteamiento de problemas y la toma de decisiones acerca del interés y la conveniencia o no de su estudio; formulación de hipótesis, elaboración de estrategias de resolución y de diseños experimentales y análisis de los resultados y de su fiabilidad.  
— Búsqueda, selección y comunicación de información y de resultados utilizando la terminología adecuada.  
— Magnitudes: Tipos y su medida. Unidades. Factores de conversión. Representaciones gráficas. Instrumentos de medida: Sensibilidad y precisión. Errores en la medida.  

### 2. Estudio del movimiento.
— Importancia del estudio de la  [cinemática](/home/recursos/fisica/cinematica)  en la vida cotidiana y en el surgimiento de la ciencia moderna.  
— Elementos que integran un movimiento. Sistemas de referencia inerciales. Magnitudes necesarias para la descripción del movimiento. Iniciación al carácter vectorial de las magnitudes que intervienen.  
— Estudio de los movimientos con trayectoria rectilínea y del movimiento circular uniforme.  
— Las aportaciones de Galileo al desarrollo de la cinemática y de la ciencia en general. Superposición de movimientos. Aplicación a casos particulares: Tiro horizontal y tiro oblicuo.  
— Importancia de la educación vial. Estudio de situaciones cinemáticas de interés, como el espacio de frenado, la influencia de la velocidad en un choque, etcétera.  

### 3.  [Dinámica](/home/recursos/fisica/dinamica) .
— De la idea de fuerza de la Física aristotélico-escolástica al concepto de fuerza como interacción.  
— Revisión y profundización de las Leyes de la dinámica de Newton. Momento lineal e impulso mecánico. Variación y conservación del momento lineal.  
— Dinámica del movimiento circular uniforme. Interacción gravitatoria: Ley de gravitación universal. Importancia de esta ley.  
— Estudio de algunas situaciones dinámicas de interés: Peso, fuerzas de fricción en superficies horizontales e inclinadas, fuerzas elásticas y tensiones.  

### 4. La energía y su transferencia. Trabajo y calor.
— Revisión y profundización de los conceptos de energía, trabajo y calor y sus relaciones. Eficacia en la realización de trabajo: Potencia. Formas de energía. Energía debida al movimiento. Teorema de las fuerzas vivas. Energía debida a la posición en el campo gravitatorio. Energía potencial elástica.  
— Principio de conservación y transformación de la energía. Sistemas y variables termodinámicas. Transferencias de energía. Calor y trabajo termodinámico. Principios cero y  
primero de la termodinámica. Degradación de la energía.  

### 5. Electricidad.
— Revisión de la fenomenología de la electrización y la naturaleza eléctrica de la materia ordinaria. Interacción electrostática.  
— Introducción al estudio del campo eléctrico. Concepto de potencial. Diferencia de potencial entre dos puntos de un campo eléctrico.  
— La corriente eléctrica. Ley de Ohm; aparatos de medida y asociación de resistencias. Aplicación al estudio de circuito. Efectos energéticos de la corriente eléctrica. Generadores de corriente.  
— La energía eléctrica en las sociedades actuales: Profundización en el estudio de su generación, consumo y repercusiones de su utilización.  

### 6. Teoría atómico-molecular de la materia.
— Revisión y profundización de la teoría atómica de Dalton. Interpretación de las leyes básicas asociadas a su establecimiento: Leyes ponderales. Ley de los volúmenes de combinación. Ley de Avogadro. Constante de Avogadro. Leyes de los gases.  
— Masas atómicas y moleculares.  [La cantidad de sustancia y su unidad, el mol](/home/recursos/quimica/concepto-de-mol) .  
— Ecuación de estado de los gases ideales.  
— Determinación de fórmulas empíricas y moleculares.  
— Preparación de disoluciones de concentración determinada. Uso de la concentración en cantidad de sustancia.  

### 7. El átomo y sus enlaces.
— Primeros modelos atómicos: Thomson y Rutheford. Interacción de la radiación electromagnética con la materia: Los  [espectros](/home/recursos/recursos-espectro)  atómicos. El modelo atómico de Bohr. Distribución electrónica en niveles energéticos. Introducción cualitativa al modelo cuántico.  
— Abundancia e importancia de los elementos en la naturaleza.  [El sistema periódico](/home/recursos/quimica/recursos-tabla-periodica) . Ordenación periódica de los elementos: Su relación con los electrones externos.  
— Estabilidad energética y  [enlace químico](/home/recursos/quimica/enlace-quimico) . Enlaces covalente, iónico, metálico e intermoleculares.  [Propiedades de las sustancias](/home/recursos/quimica/recursos-propiedades-sustancias)  en relación con el tipo de enlace.  
—  [Formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion)  y nomenclatura de los compuestos inorgánicos, siguiendo las normas de la IUPAC.  

### 8. Estudio de las transformaciones químicas.
— Importancia del estudio de las transformaciones químicas y sus implicaciones.  
— Interpretación microscópica de las reacciones químicas. Velocidad de reacción. Factores de los que depende: Hipótesis y puesta a prueba experimental.  
— Relaciones estequiométricas de masa y/o volumen en las reacciones químicas utilizando factores de conversión. Reactivo limitante y rendimiento de una reacción. Cálculos en sistemas en los que intervienen disoluciones.  
— Tipos de reacciones químicas. Estudio de un caso habitual: Reacciones de combustión.  
— Química e industria: Materias primas y productos de consumo. Implicaciones de la química industrial.  
— Valoración de algunas reacciones químicas que, por su importancia biológica, industrial o repercusión ambiental, tienen mayor interés en nuestra sociedad. El papel de la química en la construcción de un futuro sostenible.  

### 9. Introducción a la química del carbono.
— Orígenes de la química orgánica: Superación de la barrera del vitalismo. Importancia y repercusiones de las síntesis orgánicas.  
— Posibilidades de combinación del átomo de carbono. Grupos funcionales. Introducción a la  [formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion)  de los compuestos de carbono. Isomería.  
— Los hidrocarburos, aplicaciones, propiedades y reacciones químicas. Fuentes naturales de hidrocarburos. El petróleo y sus aplicaciones. Repercusiones socioeconómicas, éticas y medioambientales asociadas al uso de combustibles fósiles.  
— El desarrollo de los compuestos orgánicos de síntesis: De la revolución de los nuevos materiales a los contaminantes orgánicos permanentes. Ventajas e impacto sobre la sostenibilidad.  

