
# Recursos Química y Arte

Página donde ir recopilando ideas  
[http://kimikarte.blogspot.com/](http://kimikarte.blogspot.com/)  
[twitter Oskar_KimikArte/status/1065156107650441216](https://twitter.com/Oskar_KimikArte/status/1065156107650441216)  
Ya que @museodelprado encara su 200 aniversario, hagámosle un pequeño homenaje a su más emblemática obra. Tampoco penséis que Velázquez necesitó muchos pigmentos para crear tan maravillosa obra. #Prado200#KimikArte 325  
[![](https://pbs.twimg.com/media/Dsb1SQUXcAAgdgQ.png "") ](https://pbs.twimg.com/media/Dsb1SQUXcAAgdgQ.png)  
(hilo con más información, por ejemplo cita pdf al final)  
  
[EN TORNO A UNA OBRA MAESTRA: LAS MENINAS, PINCELADAS DE CIENCIA EN LAS MENINAS, Ponencia presentada por Fernando Ignacio de Prada Pérez de Azpeitia y José Antonio Martínez Pons.](https://content.cdnprado.net/imagenes/proyectos/personalizacion/7317a29a-d846-4c54-9034-6a114c3658fe/cms/pdf/Fernando_I._Perez_de_Prada_y_Jose_Antonio_Martinez._PINCELADAS_DE_CIENCIA_EN_LAS_MENINAS.pdf)  

[twitter Oskar_KimikArte/status/1071346900480221189](https://twitter.com/Oskar_KimikArte/status/1071346900480221189)  
Murillo impulsó esta célebre representación de la Inmaculada Concepción. La estatigrafía realizada en @museodelprado muestra que el manto está formado por capas de albayalde, azul esmalte con laca roja y, sólo al final, azul ultramar.  #OrgulloBarroco #Prado200 #KimikArte 342  
[![](https://pbs.twimg.com/media/DtwC3_kW4AAhJtr?format=png "") ](https://pbs.twimg.com/media/DtwC3_kW4AAhJtr?format=png)  
