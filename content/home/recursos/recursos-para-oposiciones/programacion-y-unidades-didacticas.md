
# Programación y unidades didácticas

Es algo que hay que elaborar obligatoriamente en una oposiciones docentes. Quizá para alguien que se presenta por primera vez es lo más novedoso: las oposiciones tienen una parte en la que pregunta "teoría /los temas", una parte en la que se hace el "práctico/problemas" y luego la parte "didáctica/normativa": la programación didáctica y las unidades didácticas. En ellas se debe plasmar la normativa (currículo, evaluación...) y se deben plasmar recursos (metodológicos, actividades...)  

El tema de la programación didáctica no es algo limitado a opociones, es algo que hay que hacer en la docencia normal.  
En Madrid hay documentación publicada por inspección en 
[Subdirección General de Inspección Educativa/Centros/Documentos de centros](https://www.educa2.madrid.org/web/sginspeccioneducativa/documentos-organizativos-de-los-centros)  

En 2022-2023 (año de implantación LOMLOE) soy jefe de departamento y comparto las programaciones en web del centro   
[Departamentos IES Alkala Nahar](https://www.educa2.madrid.org/web/centro.ies.alkalanahar.alcala/departamentos) desde donde se enlaza [la carpeta de cloud EducaMadrid cuyo contenido es gestionado por el departamento de Física y Química](http://cloud.educa.madrid.org/index.php/s/u1Is6lINDkovEtb)  

[EDUCACIÓN SECUNDARIA OBLIGATORIA Y BACHILLERATO DOCUMENTO DE APOYO PARA LA ELABORACIÓN DE LAS PROGRAMACIONES DIDÁCTICAS. Plan General de Actuación de la Inspección Educativa para el curso 2016-2017 - madrid.org](https://www.educa2.madrid.org/web/educamadrid/principal/files/e36ca3ac-fb19-4373-a9f1-1f5ed67aff12/PROGRAMACIONES%20E.SECUNDARIAb.pdf?t=1508922706654)  

[Orientaciones para la elaboración de las programaciones didácticas. Septiembre 2022.](https://www.educa2.madrid.org/web/educamadrid/principal/files/1efa619a-f951-4111-aabc-467fab66acb3/Nueva%20WEB/P%C3%BAblico/Centros/Documentos%20de%20Centros/Orientaciones%20programaciones%20did%C3%A1cticas%20septiembre%202022.pdf?t=1664432347902)  

[LA PROGRAMACIÓN EN  EL CONTEXTO DE LA LOMLOE. Aspectos básicos. INSTITUTO SUPERIOR MADRILEÑO DE INNOVACIÓN EDUCATIVA. MADRID 1 DE MARZO DE 2023](https://www.educa2.madrid.org/web/educamadrid/principal/files/1efa619a-f951-4111-aabc-467fab66acb3/Nueva%20WEB/P%C3%BAblico/Centros/Documentos%20de%20Centros/PROGRAMACIONES%20DID%C3%81CTICAS.%20ISMIE.pdf?t=1677746054087)  

[ORIENTACIONES PARA LA ELABORACIÓN DE LAS PROGRAMACIONES DIDÁCTICAS EN EDUCACIÓN SECUNDARIA OBLIGATORIA Y EN BACHILLERATO. Subdirección General de Inspección Educativa CONSEJERÍA DE EDUCACIÓN, CIENCIA Y UNIVERSIDADES. 18 septiembre 2023](https://www.educa2.madrid.org/web/educamadrid/principal/files/1efa619a-f951-4111-aabc-467fab66acb3/Nueva%20WEB/P%C3%BAblico/Centros/Documentos%20de%20Centros/20230918%20Orientaciones%20programaciones%20did%C3%A1cticas.pdf?t=1695020750501)  
[ORIENTACIONES PARA EL DISEÑO DE PROGRAMACIONES DIDÁCTICAS EN FORMACIÓN PROFESIONAL. Subdirección General de Inspección Educativa CONSEJERÍA DE EDUCACIÓN, CIENCIA Y UNIVERSIDADES. 13 septiembre 2023](https://www.educa2.madrid.org/web/educamadrid/principal/files/1efa619a-f951-4111-aabc-467fab66acb3/Nueva%20WEB/P%C3%BAblico/Centros/Documentos%20de%20Centros/20230915%20ORIENTACIONES%20PARA%20EL%20DISE%C3%91O%20DE%20PROGRAMACIONES%20DID%C3%81CTICAS%20EN%20FP.pdf?t=1694766445881)  

## Progrmación y unidades didácticas en oposiciones  
En Madrid, en la convocatoria de 2014 se indica  
[RESOLUCIÓN de 14 de abril de 2014, de la Dirección General de Recursos Humanos, por la que se convocan procedimientos selectivos para ingreso y accesos al Cuerpo de Profesores de Enseñanza Secundaria y procedimiento para la adquisición de nuevas especialidades por los funcionarios del citado Cuerpo.](http://www.bocm.es/boletin/CM_Orden_BOCM/2014/04/21/BOCM-20140421-2.PDF)  
> Parte A. Presentación de una programación didáctica: La programación didáctica, que será defendida oralmente ante el Tribunal, hará referencia al currículo de un área, materia o módulo relacionados con la especialidad por la que se participa  
...  
En cualquier caso, una programación para un curso escolar deberá contener **un mínimo de quince unidades didácticas**, cada una de las cuales deberá ir debidamente numerada en un índice.  
...   
Parte B. Preparación y exposición oral de una unidad didáctica ante el Tribunal: La parte B de la prueba consistirá en la preparación y exposición oral de una unidad didáctica  

Es similar en convocatoria 2022
[RESOLUCIÓN de 2 de febrero de 2022, la Dirección General de Recursos Humanos, por la que se convocan procedimientos selectivos para ingreso y accesos a los Cuerpos de Profesores de Enseñanza Secundaria, Profesores de Escuelas Oficiales de Idiomas, Profesores de Música y Artes Escénicas, Profesores de Artes Plásticas y Diseño, Maestros de Taller de Artes Plásticas y Diseño y procedimiento para la adquisición de nuevas especialidades por los funcionarios de los citados Cuerpos.](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/02/08/BOCM-20220208-5.PDF)  

Respecto a qué materias se pueden elegir para hacer la programación didáctica, mirar las asociadas a Física y Química en  [Especialidades y materias afines](https://sites.google.com/site/especialidadesymateriasafines/)  

Por ejemplo para ESO comentadas en  [Materias ESO](/home/materias/eso)  

Con los cambios en legislación, es importante tener claro (lo indica cada convocatoria) con qué legislación se hace la programación: LOE / LOMCE / LOMLOE.  
Ver apartado  [legislación educativa](/home/legislacion-educativa)  

Ver publicación de la Comunidad de Madrid ISBN: 978-84-451-3733-8, de 2018:   
[Documentos de apoyo para la elaboración de las programaciones didácticas. Educación infantil. Educación Primaria. Educación Secundaria Obligatoria y Bachillerato](https://www.comunidad.madrid/publicacion/ref/16407)  
[Documentos de apoyo para la elaboración de las programaciones didácticas, Educación Infantil Educación Primaria Educación Secundaria Obligatoria y Bachillerato (PDF)](http://www.madrid.org/bvirtual/BVCM016407.pdf)  


En recursos opos tengo compartido  [documento interno tribunales de 2015 Madrid de cómo valoran PD y UD](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/Madrid/2015-Madrid-GuionValoracionPD-UD.jpg)  

También cito blog de José Sande con cosas relacionadas como   
[Más sobre los defectos de forma en las programaciones didáctica de oposiciones](https://josesande.com/2018/06/20/mas-sobre-los-defectos-de-forma-en-las-programaciones-didactica-de-oposiciones/)  
Un error forma típico son tipo y tamaño letra: en Madrid se indica Arial 12, y lo uso en portada también  
En Madrid suelen citar usar currículo de curso pasado, y resulta raro hacer en julio una defensa de PD y UD de un curso anterior, pero es lo habitual.  
En 2018 la normativa es LOMCE, no LOE, pero recuerdo que tengo compartidas en internet programaciones que hice cuando fui jefe de departamento unipersonal, por si son útiles a alguien  
[Programación Didáctica Materia Física y Química 3º ESO Curso 2014-2015](http://www.educa2.madrid.org/web/educamadrid/principal/files/f55bd711-7e98-4044-aa32-a3a80e84864d/Programación-ESO-FQ3-2014-2015.pdf?t=1418320186545)  
[Programación Didáctica Materia Física y Química 4º ESO Curso 2014-2015](http://www.educa2.madrid.org/web/educamadrid/principal/files/f55bd711-7e98-4044-aa32-a3a80e84864d/Programación-ESO-FQ4-2014-2015.pdf?t=1418320197477)  
[Programación Didáctica. Evaluación y Calificación en ESO Curso 2014-2015](http://www.educa2.madrid.org/web/educamadrid/principal/files/f55bd711-7e98-4044-aa32-a3a80e84864d/Programación-ESO-Eval-Calif-2014-2015.pdf?t=1418320136801)  
[Programación Didáctica Física 2º Bachillerato Curso 2014-2015](http://www.educa2.madrid.org/web/educamadrid/principal/files/f55bd711-7e98-4044-aa32-a3a80e84864d/Programación-BACH-FIS2-2014-2015.pdf?t=1418320173231)  
[Programación Didáctica Química 2º Bachillerato Curso 2014-2015](http://www.educa2.madrid.org/web/educamadrid/principal/files/f55bd711-7e98-4044-aa32-a3a80e84864d/Programación-BACH-QUI2-2014-2015.pdf?t=1418320160843)  
[Programación Didáctica. Evaluación y Calificación en Bachillerato Curso 2014-2015](http://www.educa2.madrid.org/web/educamadrid/principal/files/f55bd711-7e98-4044-aa32-a3a80e84864d/Programación-BACH-Eval-Calif-2014-2015.pdf?t=1418320150613)  
[Programación Didáctica Ciencias Aplicadas I Formación Profesional Básica Curso 2014-2015](http://www.educa2.madrid.org/web/educamadrid/principal/files/f55bd711-7e98-4044-aa32-a3a80e84864d/Programación%20FP%20Basica%20Modulo%20Ciencias%20Aplicadas%20I.pdf?t=1418320240608)  

La programación que utilicé en 2016, año en el que fui seleccionado en las oposiciones, la enlazo aquí (sigue siendo LOE)  
[2016-PortadaProgramación-2Bach.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-para-oposiciones/programacion-y-unidades-didacticas/2016-PortadaProgramación-2Bach.pdf)  
[Programacion Didactica 2016- FQ 2Bach-EnriqueGarciaSimon.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-para-oposiciones/programacion-y-unidades-didacticas/Programacion%20Didactica%202016-%20FQ%202Bach-EnriqueGarciaSimon.pdf)


Ideas sobre programaciones LOMCE en este enlace  
[Programaciones LOMCE: todo lo que usted siempre quiso saber y nunca se atrevió a preguntar](http://docentesconeducacion.es/viewtopic.php?f=134&t=3855&p=26821#p17128)  

En abril 2021 Leticia comparte sus programaciones  
[https://twitter.com/ProfaDeQuimica/status/1383855862851067904](https://twitter.com/ProfaDeQuimica/status/1383855862851067904)  
DIFUSIÓN, POR FAVOR.Comparto mi progr. didáctica de #oposición de Secundaria (Física y Química #FyQ) de #Madrid y #CLM 2015 con licencia CC By-Nc-Nd para ayudar a otros opositores. Obtuve plaza en Madrid y quedé a las puertas en CLM.Descargar aquí:  
[https://drive.google.com/drive/folders/1Zk083BTEAAA6KMh7lfqXaiSlF5005ZCj](https://drive.google.com/drive/folders/1Zk083BTEAAA6KMh7lfqXaiSlF5005ZCj)  

*Pendiente separar ideas generales de programaciones, no solo ligadas a oposiciones. De momento pongo aquí*  
Un ejemplo es cómo contemplar la ortografía en Física y Química  
[twitter currofisico/status/1416339925465714691](https://twitter.com/currofisico/status/1416339925465714691)  
La ortografía es un problema. Así que en los exámenes de ESO les dejo su guía de ortografía que les doy en 2°de ESO. Cuando a los 30min dice ¡ya profe! de que ya ha terminado...pues repasas la ortografía...y sí les resto puntos.  
[Guía- completa de las Reglas Ortográficas. I.E.S. FEDERICO MAYOR ZARAGOZA. SEVILLA. DEPARTAMENTO FÍSICA Y QUÍMICA. Profesor: Curro Martínez (www.currofisico.es) (Corresponden al PLAN DE ORTOGRAFÍA PARA EDUCACIÓN PRIMARIA del CEIP JOSÉ SEBASTIÁN Y BANDARÁN: 1a Edición, Febrero 2018)](https://drive.google.com/file/d/1r3Q_Fj1DcQOPs0dIOJ2ymNUsd6TjqXL0/view)  

Otra idea es cómo valorar el cuaderno, y enlaza con la idea de "examen de cuaderno"  
[EVALUAR EL CUADERNO DEL ALUMNO](http://iesactiva.com/evaluar-el-cuaderno-del-alumno/)  
[¿Qué es esto del examen de cuaderno?](https://www.tierradenumeros.com/post/examen-de-cuaderno/)  

[twitter edugehis/status/1587365334440443904](https://twitter.com/edugehis/status/1587365334440443904)  
Para opositores.  
Esquema de programación con legislación LOMCE.  
Más adelante subo esquema de UD tanto de LOMCE como de LOMLOE.  
![](https://pbs.twimg.com/media/Fgdz5WZXgAEAMXJ?format=jpg)  

[twitter edugehis/status/1585880053963194368](https://twitter.com/edugehis/status/1585880053963194368)  
Para opositores.  
Guión sistematizado que indica las partes que deberían aparecer en una programación LOMLOE.  
Espero que os sirva.  
![](https://pbs.twimg.com/media/FgItaQ8WYAAGS2e?format=jpg)  


