# Rúbricas

Página para intentar centralizar ideas  

[Banco de Rúbricas y otros documentos - intef.es](https://cedec.intef.es/banco-de-rubricas-y-otros-documentos/)  

[Rúbricas de las diferentes etapas educativas por cursos - gobiernodecanarias.org](https://www.gobiernodecanarias.org/educacion/web/ensenanzas/rubricas/)  

[Evaluación LOMLOE en el Entorno de aprendizaje - castillalamancha.es](https://educamosclm.castillalamancha.es/portal/noticias/evaluacion-lomloe-en-el-entorno-de-aprendizaje)  
Cita 5. Tareas con calificación avanzada (rúbricas).  

[Rubrica de valoración Cuaderno de clase- orientacionandujar.es](https://www.orientacionandujar.es/2023/03/14/rubrica-de-valoracion-cuaderno-de-clase/)  

[twitter currofisico/status/1744476571178615050](https://twitter.com/currofisico/status/1744476571178615050)  
Corregir con una rúbrica un examen de física de 2º de bachillerato es la muerte a pellizcos, tanto en el diseño como en las horas de corrección si tienes muchos alumnos, sin embargo elimina todo tipo de subjetividad.   
[INTERACCIÓN MAGNÉTICA EXAMEN RÚBRICA](https://drive.google.com/file/d/1WMNNU8aN7I1sgolvlNkeJ7rgjXSqPXyG/view?usp=sharing)  
A no todo estudiante le gusta, pero el "hormiguita" empieza a comprender qué es lo que se le pide y en qué aspectos puede mejorar. No se si este tipo de rúbrica es la más adecuada, pero si establece las reglas de juego. Ahora parece que reinventamos la pólvora, pero yo personalmente hace muchos años que las uso, aunque es verdad que las plataformas educativas facilitan el ponerlas a disposición  para el alumnado y familia. El resultado es 0 reclamaciones y teléfono móvil autorizado para escanear su prueba y cotejar en casa.  

[twitter MarianaMorale19/status/1629797586671370240](https://twitter.com/MarianaMorale19/status/1629797586671370240)  
Hablemos, una vez más, de #rúbricas. Usaré como ejemplo esta que me ha facilitado una compañera del #claustrovirtual. Ella diseñó esta rúbrica de manera detallada para que le resultara más útil. Va hilo con mis aportaciones para la #evaluacionformativa  
![](https://pbs.twimg.com/media/Fp40RDpWAAEUdqt?format=jpg)  
El hecho de que haya concretado en las descripciones es mucho más claro para ella y para el alumnado (añadiría una línea divisoria entre los descriptores). Este tipo de diseño es muy propicio para hacer una evaluación formativa. A continuación van algunas ideas🔽
\1. Muestra modelos de diferente calidad y valóralos con la rúbrica. Discute los puntos dudosos. Luego dales modelos a ellos para que hagan lo mismo.   
(esto les centra en la tarea e interiorizan los criterios). Los modelos pueden ser redacciones del curso pasado  
\2. En lugar de hacer 3 redacciones, haz 1 con 3 vueltas. En la 1ª elige algunos elementos de la rúbrica (los que sean más estructurales)+ rehacer. En la 2ª usa los más más superficiales+ rehacer. En la 3ª úsalos todos (+ calificar si te lo exigen).  
\2.bis. En alguna de esas vueltas pídeles que contrasten con los modelos o que se den ideas entre ellos. Te ahorrarás feedback y ellos se fijarán más.  
\3. Introduce alguna pregunta de metacognición durante el proceso. Por ej: ¿De qué me he dado cuenta? ¿En qué tengo que fijarme más?  
Y recuerda: Usar la rúbrica solo para poner notas es como comprarse la Thermomix para rallar el pan. Espero que alguna de las ideas del hilo te ayude a sacarle más partido a las rúbricas para el aprendizaje de tu alumnado. ¡Buen domingo!  
Por cierto, para hacer este proceso quizá os resulta más práctico probar la rúbrica de un solo punto (Thanks, @Arbay38, for the tip)  
[Meet the Single Point Rubric](https://www.cultofpedagogy.com/single-point-rubric/)  

[FyQ RÚBRICA TRABAJO/MINIPROYECTO ESCRITO - IES Rafael Frübeck de Burgos](https://irfb.es/download/fisica_y_quimica/rUbrica_trabajo_escrito_de_miniproyectos/FyQ-RUBRICA-TRABAJO-ESCRITO-MDR.pdf)  
