
# Recursos astronomía

Esta página, como tantas, es un borrador pendiente de ordenar ....  
Se pueden ver recursos relacionados en  [recursos sobre notación científica](/home/recursos/recursos-notacion-cientifica)   
  
 [astronomia-con-smartphone.html](http://fisicamartin.blogspot.com.es/2017/10/astronomia-con-smartphone.html)   

A nivel sencillo (hay una materia de [taller de astronomía](/home/materias/eso/taller-de-astronomia)), se puede introducir el "movimiento" solar y asociarlo a distintos relojes de Sol   
[twitter EnsedeCiencia/status/1565662689187225607](https://twitter.com/EnsedeCiencia/status/1565662689187225607)  
Este curioso reloj de sol se encuentra ubicado en el jardín botánico de Río de Janeiro.  
![](https://pbs.twimg.com/media/FbpZ6r3XgAUkUMy?format=jpg)  

[Digital sundial - thingiverse](https://www.thingiverse.com/thing:1068443)  
![](https://cdn.thingiverse.com/renders/e6/59/fe/a2/34/002_preview_featured.jpg)  

También se puede asociar a escalas, y distintos modelos a escala del Sistema Solar  

[![](https://static.wixstatic.com/media/66bfed_4e7e69dc504744d29ef346ab3d35f095~mv2.png/v1/fill/w_980,h_577,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/66bfed_4e7e69dc504744d29ef346ab3d35f095~mv2.png)](https://bit.ly/3Dhusi9)  

 
[My Galaxies. Create your own message in real galaxies!](http://writing.galaxyzoo.org/)   
Permite crear un texto utilizando como caracteres imágenes de galaxias    
  
NASA Astronomy Picture Of the Day  
 [ Astronomy Picture of the Day ](http://apod.nasa.gov/apod/)   
Authors & editors: Robert Nemiroff (MTU) & Jerry Bonnell (UMCP)  
NASA Official: Phillip Newman Specific rights apply.  
 [NASA Web Privacy Policy and Important Notices](http://www.nasa.gov/about/highlights/HP_Privacy.html)   
A service of: ASD at NASA / GSFC & Michigan Tech. U.  
  
 [Evolución estelar para todos los públicos &#8211; Mola Saber](https://molasaber.org/2014/11/18/evolucion-estelar-para-todos-los-publicos/)   
  
 [Solar System Scale Model Calculator](http://thinkzone.wlonk.com/SS/SolarSystemModel.php)   
Una película importante es Contact, basada en la novela de Carl Sagan  
Contact (1997), 2h30min  
 [http://www.imdb.com/title/tt0118884/](http://www.imdb.com/title/tt0118884/)   
  
Voyager I  
Ficheros de audio Creative Commons license: Public Domain Mark 1.0  [Voyager1](http://archive.org/details/Voyager1)   
  
Página para navegar por el contenido disco, sin instrucciones porque un posible lector no las tendría  
 [Voyager Golden Record](http://goldenrecord.org/#discus-aureus)   
  
Terence Tao. Cosmic Ladder  
 [The Cosmic Distance Ladder (version 4.1) | What&#039;s new](http://terrytao.wordpress.com/2010/10/10/the-cosmic-distance-ladder-ver-4-1/)   
 [cosmic-distance-ladder2.pptx](http://terrytao.files.wordpress.com/2010/10/cosmic-distance-ladder2.pptx)   
  
Asociable también a gravitación, a método científico ...  
Vídeo NASA con caída de pluma y martillo  
 [Nueva demostración de que el hombre pisó la Luna - La Ciencia de la Mula Francis](http://francis.naukas.com/2010/12/29/nueva-demostracion-de-que-el-hombre-piso-la-luna/)   
 [Apollo 15 Hammer-Feather Drop](http://nssdc.gsfc.nasa.gov/planetary/lunar/apollo_15_feather_drop.html)   
 [![Feather &amp; Hammer Drop on Moon - YouTube](https://img.youtube.com/vi/5C5_dOEyAfk/0.jpg)](https://www.youtube.com/watch?v=5C5_dOEyAfk)   
  
Una versión reciente: Brian Cox visits the world's biggest vacuum chamber - Human Universe: Episode 4 Preview - BBC Two   
 [![Brian Cox visits the world&#39;s biggest vacuum | Human Universe - BBC - YouTube](https://img.youtube.com/vi/E43-CfukEgs/0.jpg)](https://www.youtube.com/watch?v=E43-CfukEgs)   
  
 [Solar System Scope](http://www.solarsystemscope.com/es)   
Modelo interactivo 3D de los planetas del sistema solar y el cielo nocturno  
  
CASSINI’S GRAND FINALE [CASSINI&#039;S GRAND FINALE on Vimeo](https://vimeo.com/210782375) en general todos los vídeos de  [Erik Wernquist - CGI Artist, Director &amp; VFX Artist](https://vimeo.com/erikwernquist)   
 [Cassini&#x27;s Grand Tour](http://www.nationalgeographic.com/science/2017/09/cassini-saturn-nasa-3d-grand-tour/)   
  
 [https://twitter.com/physicsJ/status/1079061451845890048](https://twitter.com/physicsJ/status/1079061451845890048) *NEW* Final version: FASTER, longer, and corrected thanks to Twitter! 👍Day length (sidereal) & axial tilt for the 8 largest planets in our solar system!  
Which planet best represents you? 🧐   

[What is a day - minutelabs.io](https://labs.minutelabs.io/what-is-a-day/#/welcome)  
An interactive video exploring stellar, solar, and standard days.  
I built an interactive video to explain what a “day” is… it’s not as obvious as you may think.  

