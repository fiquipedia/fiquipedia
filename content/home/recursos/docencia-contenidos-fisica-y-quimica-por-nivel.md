
# Docencia contenidos Física y Química por nivel

* [Docencia contenidos física y química por nivel, según currículo](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/docencia-contenidos-fisica-y-quimica-por-nivel-segun-curriculo)  (estática y muy voluminosa)
* [Docencia contenidos física y química por nivel, visión personal](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/docencia-contenidos-fisica-y-quimica-por-nivel-vision-personal)  (variable, intentando ser más breve que el currículo y si es posible argumentando por qué cada cosa en cada nivel, cuando se profundiza, cuando no se repite ...)  
En 2017 creo una página similar pero enlazando con la idea de "pizarras"
* [Pizarras física y química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel)  
* [Actividades física y química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/actividades-fisica-y-quimica-por-nivel)  

Esta página tiene uno de los objetivos iniciales de la página cuando la creé, aunque no la inicio hasta 2016, y en 2021 no está terminado. Se trata de poner argumentadamente cómo el mismo contenido va variando de profundidad entre cursos. Por ejemplo en LOE estructura atómica, enlaces, formulación y la tabla periódica se veían en 3º ESO, 4º ESO, 1º Bachillerato (parte desaparece en LOMCE) y 2º Bachillerato, y a veces surgen dudas de cómo y qué se va ampliando.  
Esto se puede ver como tener claro cómo cumplir con "el currículo legal", pero realmente es cómo plantear una secuenciación lógica en un currículo ideal.  

Esta página la inicié en 2016 asociada a comparar 2º y 3º ESO por similitudes currículo LOMCE (ver comentarios en [Docencia contenidos física y química por nivel, según currículo](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/docencia-contenidos-fisica-y-quimica-por-nivel-segun-curriculo)) y luego abandoné: en curso 2017-2018 impartí Física y Química en 2º, 3º y 4º ESO e intento aprovechar para revisar esos 3 niveles y hacer materiales de elaboración propia ("pizarras") asociados  
[Pizarras Física y Química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel)  

En principio podría plantearlo con contenidos genéricos para que no esté asociado a una ley educativa concreta, pero mi planteamiento inicial es hacer una referencia a ambos. La idea es tenerlo en una tabla, que puede crecer y quedar algo grande. En los encabezados pongo enlace a currículo vigente de cada uno de los cursos, en principio física y química (puede haber otras materias, puede que separe física de química, puede que ponga temas de cómo van las matemáticas asociadas, puede agruparse por bloques ...   

Los bloques generales pueden tener un nombre variable: comienzo con nombres de bloques aproximados de ESO, en Bachillerato se van ampliando, y algunos no tienen continuidad en física y/o química. A veces intento dividir los bloques generales del currículo en bloques de contenidos más pequeños, pero que se pueden solapar. Bajar a mucho detalle en los bloques lleva a más probabilidad de que haya conceptos relacionados que se solapen a la hora de la docencia, por lo que intento hacer bloques genéricos, no tan genéricos como en el currículo, intentando agrupar lo que sí he puesto en los mismos apuntes/pizarras y pertenencen a la misma unidad didáctica.  
Puede haber bloques/subbloques asociados a conceptos que hay que explicar pero que no estén explícitamente el el currículo, por ejemplo asociados a conceptos matemáticos: vectores, trigonometría, ...   

Una idea asociada a estos bloques y el nivel es que como no uso libro y preparo [materiales propios](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel), veo que es mejor preparlos por bloques y no solo por nivel (que puede parecer algo contradictorio porque aquí hablo de currículo/visión/pizarras por "nivel" donde combino nivel y bloque, pero en actividades suelo preparar materiales por bloque de contenidos y no por un nivel concreto), de modo que según el grupo y las circunstancias se pueden usar los materiales y las actividades del nivel más adecuado al alumnado. A veces en 1º Bachillerato se hacen actividades de "nivel 4ºESO", porque no trabajaron ciertos contenidos, y a veces en 4ºESO se hacen actividades de "nivel 1º Bachillerato" si el grupo lo permite, o a veces ese cruce de nivel se hace solamente por algunos alumnos que lo necesitan como refuerzo/ampliación.  

_LOMCE_  
_Intento citar C (contenidos), CE (criterios de evaluación) y EAE (estándares de aprendizaje evaluables); luego se trata de ver qué se repite y cómo varía. Primera revisión planteamiento: tras un primer intento, veo que poner simplemente todos C, CE y EAE supone tabla inmensa, en la que se va a perder ver claramente cómo secuenciar la docencia de contenidos por nivel. Creo que lo mejor es:_ 
_- una tabla de C+CE+EAE por nivel (laboriosa pero "estática")_  
_- otra tabla separada de docencia por nivel en la que aportar visión personal, variable, pero no mezclar en la misma tabla ambas cosas._  
_Por ello creo inicialmente dos páginas separadas. Si alguna tabla se hace muy grande igual separo más subpáginas_ 

**LOMLOE**  
Mantengo la idea de separar en dos tablas "currículo" y "visión personal", pero ya no cito criterios de evaluación y estándares, me limito a saberes básicos (contenidos en Madrid), y mantengo en negrita lo que indico en las páginas de currículo como contenidos añadidos por Madrid respecto a lo estatal.  

Se puede enlazar con metodologías y otros aspectos  
[ENSEÑANZA DE LA FÍSICA Y LA QUÍMICA. Sección: Enseñanza Programada Personalizada - heurema.com](http://www.heurema.com/EPPG.htm)  
> INTRODUCCIÓN AL EMPLEO DE LA ENSEÑANZA PROGRAMADA PERSONALIZADA  
La enseñanza programada, se puso de moda en España en los años 70, aplicándose a lo que se denominaba  Enseñanza General Básica (EGB), pero sus antecedentes son bastante antiguos, las ideas se encuentran en Sócrates (siglo V a.C. y sus primeras reglas en la “Didáctica Magna” del checo Jan Amos Komensky (siglo XVII), normas que chocaron con los sistemas escolásticos tradicionales que le valdrían persecuciones y destierro. El trabajo de Skinner “Technology of Teaching”, de 1968, abrió el camino para su aplicación posterior.  
...   
La enseñanza programada personalizada se ha programado en 3 cursos: CIENCIAS DE LA NATURALEZA en 2ºESO (13-14 años), FÍSICA Y QUÍMICA en 3º ESO (14-15 años) y FÍSICA Y QUÍMICA en 4ºESO (15-16 años), para acceder a uno de esos cursos se deberá hacer click cada subsección, con enlace, situado en la cabecera de esta página.  


[Algunas imprecisiones que nos encontramos en la Física teórica actual - Latin-American Journal of Physics Education, ISSN-e 1870-9095, Vol. 3, Nº. 2, 2009 - dialnet](https://dialnet.unirioja.es/servlet/articulo?codigo=3693181)  
> El propósito de este artículo es exponer muchas imprecisiones y arreglos que se han dado y se dan tanto en el estudio de la Física clásica como de la Física moderna. Se ve la falta de definición de diferentes magnitudes Físicas, el hecho de que no se conozcan las características básicas de la naturaleza del Universo, e incluso el que algunas leyes resulten incompletas. Finalmente, se indicará la necesidad de una nueva teoría que logre explicar toda la Física de una manera más coherente y sin imprecisiones.  

[Algunas imprecisiones que nos encontramos en la Física teórica actual (PDF)](https://dialnet.unirioja.es/descarga/articulo/3693181.pdf)  
I. LA FUERZA Y LA MASA NO ESTÁN DEFINIDAS  
II. LA ENERGÍA NO TIENE DEFINICIÓN  
IV. LAS LEYES DE NEWTON SON INCOMPLETAS  
V. LA CURVATURA RELATIVISTA DE LA LUZ ES CONTRADICTORIA  

