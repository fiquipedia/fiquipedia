# Currículo Química 2º Bachillerato (LOE)

En la Comunidad de Madrid se desarrolla en DECRETO 67/2008, de 19 de junio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato, B.O.C.M. Núm. 152 de VIERNES 27 DE JUNIO DE 2008, páginas 66 a 62 en versión pdf.  
 [DECRETO 67/2008 en html](http://gestiona.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&idnorma=6166&word=S&wordperfect=N&pdf=S)   
  
A nivel estatal se desarrolla en REAL DECRETO 1467/2007, de 2 de noviembre, por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas, BOE núm. 266, Martes 6 noviembre 2007, páginas 45451 a 45453.   
  
Se intentan indicar las diferencias: **como el RD es de mínimos, en decreto se suele ampliar: se marca en negrita lo adicional**. Por ejemplo, ver bloque de equilibrio químico.  
  
También he marcado en negrita una frase para dejar claro que el propio currículo hace mención al uso del laboratorio.  
  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver  [Contenidos currículo con enlaces a recursos: Química 2º Bachillerato](/home/materias/bachillerato/quimica-2bachillerato/contenidoscurriculoenlacesrecursosquimica2bachillerato)   
  
Este currículo deja de estar en vigor en 2016/2017 según calendario de implantación LOMCE, ver  [currículo Química 2º Bachillerato (LOMCE)](/home/materias/bachillerato/quimica-2bachillerato/curriculo-quimica-2-bachillerato-lomce)   

Ver [comparación currículo Quimica 2º Bachillerato](/home/materias/bachillerato/quimica-2bachillerato/comparacion-curriculo-quimica-2-bachillerato)

## Introducción
Esta materia requiere conocimientos incluidos en Física y Química.  
  
Materia de modalidad del Bachillerato de Ciencias y Tecnología, la Química amplía la formación científica de los estudiantes y sigue proporcionando una herramienta para la comprensión del mundo en que se desenvuelven, no solo por sus repercusiones directas en numerosos ámbitos de la sociedad actual, sino por su relación con otros campos del conocimiento como la medicina, la farmacología, las tecnologías de nuevos materiales y de la alimentación, las ciencias medioambientales, la bioquímica, etcétera. Ya en etapas anteriores los estudiantes han tenido ocasión de empezar a comprender su importancia, junto al resto de las ciencias, en las condiciones de vida y en las concepciones de los seres humanos.  
  
El desarrollo de esta materia debe contribuir a una profundización en la familiarización con la naturaleza de la actividad científica y tecnológica y a la apropiación de las competencias que dicha actividad conlleva, en particular en el campo de la química. En esta familiarización** las prácticas de laboratorio juegan un papel relevante** como parte de la actividad científica, teniendo en cuenta los problemas planteados, su interés, las respuestas tentativas, los diseños experimentales, el cuidado en su puesta a prueba, el análisis crítico de los resultados, etcétera, aspectos fundamentales que dan sentido a la experimentación.  
  
En el desarrollo de esta disciplina se debe seguir prestando atención a las relaciones Ciencia, Tecnología, Sociedad y Ambiente (CTSA), en particular a las aplicaciones de la química, así como a su presencia en la vida cotidiana, de modo que contribuya a una formación crítica del papel que la química desarrolla en la sociedad, tanto como elemento de progreso como por los posibles efectos negativos de algunos de sus desarrollos.  
  
El estudio de la química pretende, pues, una profundización en los aprendizajes realizados en etapas precedentes, poniendo el acento en su carácter orientador y preparatorio de estudios posteriores, así como en el papel de la Química y sus repercusiones en el entorno natural y social y su contribución a la solución de los problemas y grandes retos a los que se enfrenta la humanidad.  
  
La química contemplada en la materia de Física y Química se centra fundamentalmente en el estudio del papel y desarrollo de la teoría de Dalton y, en particular, se hace énfasis en la introducción de la estequiometría química. En este curso se trata de profundizar en estos aspectos e introducir nuevos temas que ayuden a comprender mejor la química y sus aplicaciones.  
  
Los contenidos propuestos se agrupan en bloques. Se parte de un bloque de contenidos comunes destinados a familiarizar a los alumnos con las estrategias básicas de la actividad científica que, por su carácter transversal, deberán ser tenidos en cuenta al desarrollar el resto. Los dos siguientes pretenden ser una profundización de los modelos atómicos tratados en el curso anterior al introducir las soluciones que la mecánica cuántica aporta a la comprensión de la estructura de los átomos y a sus uniones. En el cuarto y quinto se tratan aspectos energéticos y cinéticos de las reacciones químicas y la introducción del equilibrio químico que se aplica a los procesos de precipitación en particular. En el sexto y séptimo se contempla el estudio de dos tipos de reacciones de gran trascendencia en la vida cotidiana; las ácido-base y las de oxidación-reducción, analizando su papel en los procesos vitales y sus implicaciones en la industria y la economía. Finalmente, el último, con contenidos de química orgánica, está destinado al estudio de alguna de las funciones orgánicas oxigenadas y los polímeros, abordando sus características, cómo se producen y la gran importancia que tienen en la actualidad debido a las numerosas aplicaciones que presentan.   

## Objetivos
La enseñanza de la Química en el Bachillerato tendrá como finalidad el desarrollo de las siguientes capacidades:  
  
1. Comprender y aplicar correctamente y con autonomía los principales conceptos de la química, así como sus leyes, teorías y modelos. Conocer las estrategias empleadas en su construcción.  
2. Familiarizarse con el diseño y realización de experimentos químicos, con el uso del material apropiado, y conocer algunas técnicas específicas, de acuerdo con las normas de seguridad de los laboratorios.  
3. Obtener y ampliar información procedente de diferentes fuentes y utilizando tecnologías de la información y comunicación.  
4. Evaluar la información proveniente de otras áreas del saber para formarse una opinión propia, que permita al alumno expresarse con criterio en aquellos aspectos relacionados con la química.  
5. Familiarizarse con la terminología científica y emplearla de manera habitual en expresiones de ámbito científico. Relacionar la experiencia diaria con la científica y explicar expresiones científicas con lenguaje cotidiano.  
6. Comprender y valorar la naturaleza de la química, el carácter tentativo y evolutivo de sus leyes y teorías, evitando posiciones dogmáticas y apreciando sus perspectivas de desarrollo.  
7. Comprender el papel de la química en la vida cotidiana y su contribución a la mejora de la calidad de vida de las personas. Valorar, de forma fundamentada, los problemas que sus aplicaciones puede generar y cómo puede contribuir al logro de la sostenibilidad y de estilos de vida saludables.  
8. Reconocer los principales retos a los que se enfrenta la investigación química en la actualidad.  

## Contenidos

### 1. Contenidos comunes.
— Utilización de estrategias básicas de la actividad científica tales como el planteamiento de problemas y la toma de decisiones acerca del interés y la conveniencia o no de su estudio; formulación de hipótesis, elaboración de estrategias de resolución y de diseños experimentales y análisis de los resultados y de su fiabilidad.  
— Búsqueda, selección y comunicación de información y de resultados utilizando la terminología adecuada.  

### 2. Estructura atómica y clasificación periódica de los elementos.
— Espectros atómicos. Orígenes de la teoría cuántica. Hipótesis de Planck. Efecto fotoeléctrico. Modelo atómico de Bohr y sus limitaciones. Introducción a la mecánica cuántica moderna. Su importancia. Orbitales atómicos. Números cuánticos. Configuraciones electrónicas: Principio de Pauli y regla de Hund.  
— Evolución histórica de la ordenación periódica de los elementos. Tabla periódica de Mendeleev. Predicciones y defectos.  
— Sistema periódico actual. Estructura electrónica y periodicidad. Tendencias periódicas en las propiedades de los elementos.  

### 3. El enlace químico y propiedades de las sustancias.
— Concepto de enlace en relación con la estabilidad energética de los átomos enlazados.  
— Enlace iónico. Concepto de energía de red. Ciclo de Born-Haber. Propiedades de las sustancias iónicas.  
— Enlace covalente. Estructuras de Lewis. Parámetros moleculares. Polaridad de enlaces y moléculas. Teoría del enlace de valencia. Hibridación de orbitales atómicos (sp, sp2, sp3) y teoría de la repulsión de pares de electrones de la capa de valencia. Sólidos covalentes. Propiedades de las sustancias covalentes.  
— Fuerzas intermoleculares.  
— Estudio cualitativo del enlace metálico. Propiedades de los metales.  
— Propiedades de algunas sustancias de interés industrial o biológico en función de su estructura o enlaces.  

### 4. Transformaciones energéticas en las reacciones químicas.Espontaneidad de las reacciones químicas.  
— Sistemas termodinámicos. Variables termodinámicas. Cambios energéticos en las reacciones químicas. Procesos endo y exotérmicos.  
— Primer principio de la termodinámica. Transferencias de calor a volumen y a presión constante. Concepto de entalpía. Cálculo de entalpías de reacción a partir de las entalpías de formación. Diagramas entálpicos. Ley de Hess. Entalpías de enlace.  
— Segundo principio de la termodinámica. Concepto de entropía. Energía libre. Espontaneidad de las reacciones químicas.  
— Aplicaciones energéticas de las reacciones químicas. Repercusiones sociales y medioambientales.  
— Valor energético de los alimentos. Implicaciones para la salud.  

### 5. El equilibrio químico.
**— Introducción a la cinética química: Aspecto dinámico de las reacciones químicas. Conceptos básicos de cinética: Velocidad de reacción y factores de los que depende. Orden de reacción y molecularidad.**  
— **Concepto de equilibrio químico.** Características macroscópicas e interpretación microscópica. **Cociente de reacción y** constante de equilibrio. **Formas de expresar la constante de equilibrio: Kc y Kp; relación entre ambas**. Factores que modifican el estado de equilibrio: **Principio de Le Chatelier. Equilibrios heterogéneos.**  
— Las reacciones de precipitación como equilibrios heterogéneos. Aplicaciones analíticas de las reacciones de precipitación.  
— Aplicaciones del equilibrio químico a la vida cotidiana y a procesos industriales.  

### 6. Ácidos y bases.
— Concepto de ácido y base según las teorías de Arrhenius y Brönsted-Lowry. Concepto de pares ácido-base conjugados. Fuerza relativa de los ácidos. Constante y grado de disociación. Equilibrio iónico del agua.  
— Concepto de pH. Cálculo y medida del pH en disoluciones acuosas de ácidos y bases. Importancia del pH en la vida cotidiana. Reacciones de neutralización. Punto de equivalencia.  
— Volumetrías ácido-base. Aplicaciones y tratamiento experimental.  
— Equilibrios ácido-base de sales en disolución acuosa. Estudio cualitativo de la hidrólisis.  
— Estudio de algunos ácidos y bases de interés industrial y en la vida cotidiana. Amoniaco, ácidos sulfúrico, nítrico y clorhídrico. El problema de la lluvia ácida y sus consecuencias.  

### 7. Introducción a la electroquímica.
— Concepto de oxidación y reducción. Sustancias oxidantes y reductoras. Número de oxidación. Reacciones de oxidación reducción. Ajuste de reacciones red-ox por el método del ión-electrón. Estequiometría de las reacciones red-ox.  
— Estudio de la pila Daniell. Potencial normal de reducción. Escala de oxidantes y reductores.  
— Potencial de una pila. Potencial de electrodo. Espontaneidad de los procesos red-ox. Pilas, baterías y acumuladores eléctricos.  
— Electrólisis. Importancia industrial y económica de la electrólisis.  
— La corrosión de metales y su prevención. Residuos y reciclaje.  

### 8. Química del carbono.
— Nomenclatura y formulación de los principales compuestos orgánicos. Estudio de los principales tipos de reacciones orgánicas: Sustitución, adición, eliminación y oxidación-reducción.  
— Ejemplos característicos de reacciones orgánicas de interés, con especial referencia a la obtención de alcoholes, ácidos y ésteres; propiedades e importancia de los mismos.  
— Polímeros y reacciones de polimerización. Valorar la utilización de sustancias orgánicas en el desarrollo de la sociedad actual. Problemas medioambientales.  
— La síntesis de medicamentos. Importancia y repercusiones de la industria química orgánica.  

## Criterios de evaluación
1. Utilizar estrategias básicas del trabajo científico para analizar situaciones y obtener información sobre fenómenos químicos.  
2. Explicar los conceptos básicos de la mecánica cuántica: Dualidad onda-corpúsculo e incertidumbre. Describir los modelos atómicos discutiendo sus limitaciones y aplicar la teoría mecano-cuántica para el conocimiento del átomo.  
3. Aplicar el modelo mecano-cuántico para explicar variaciones de propiedades periódicas.  
4. Describir las características básicas de los diferentes tipos de enlace. Conocer las fuerzas intermoleculares. Comprender la formación de cristales y moléculas y estructuras macroscópicas. Deducir, en función del enlace, las propiedades de diferentes tipos de sustancias.  
5. Definir el primer principio de la termodinámica y aplicarlo correctamente a un proceso químico. Diferenciar un proceso exotérmico de otro endotérmico utilizando diagramas entálpicos. Explicar el significado de la entalpía de un sistema, determinar la variación de entalpía de una reacción química aplicando el concepto de entalpías de formación mediante la correcta utilización de tablas, valorar las implicaciones de las variaciones energéticas en las reacciones químicas y predecir, de forma cualitativa, la espontaneidad de un proceso en determinadas condiciones.  
6. Comprender el concepto de equilibrio químico y aplicarlo para predecir la evolución de un sistema y resolver problemas de equilibrios homogéneos, en particular en reacciones gaseosas, y de equilibrios heterogéneos, en especial los de disolución-precipitación.  
7. Definir y aplicar correctamente conceptos como: Ácido y base según las teorías estudiadas, fuerza de ácidos, pares conjugados, hidrólisis de una sal, volumetrías de neutralización. Aplicar la teoría de Brönsted para reconocer las sustancias que pueden actuar como ácidos o bases y saber determinar el pH de las disoluciones. Conocer y explicar las reacciones ácido-base, la importancia de algunas de ellas y sus aplicaciones prácticas.  
8. Identificar reacciones de oxidación-reducción que se producen en nuestro entorno. Saber ajustar reacciones de oxidación reducción y aplicarlas a problemas estequiométricos. Conocer el significado de potencial normal de reducción de un par redox y predecir, de forma cualitativa, el posible proceso entre dos pares redox.  
9. Conocer algunas de las aplicaciones de la oxidación-reducción tales como la prevención de la corrosión, la fabricación de pilas y la electrólisis.  
10. Formular y nombrar correctamente los diferentes compuestos orgánicos. Describir las características principales de alcoholes, ácidos y ésteres.  
11. Describir el mecanismo de polimerización y la estructura general de los polímeros. Valorar su interés económico, biológico o industrial. Conocer el papel de la industria química orgánica y sus repercusiones.  
 
