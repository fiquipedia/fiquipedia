
# Recursos flotabilidad

Página en borrador ...  
Relacionado con  [densidad](/home/recursos/fisica/recursos-densidad) , gases (pensar en flotabilidad de globos aerostáticos),  [presión](/home/recursos/presion) ,  [fuerzas en fluidos](/home/recursos/fisica/fuerzas-en-fluidos)   
Por hacer: tomar simulaciones de seed y llevar a simulaciones  
  
Flotabilidad  
 [Global Energy Services &amp; Equipment | Schlumberger](http://www.planetseed.com/es/seedkits/buoyancy)   
Exprimentos, apuntes, vídeos, simulaciones ...© 2012 Schlumberger Excellence in Educational Development, Inc.  

  
[Flotabilidad, universidad de Colorado](https://phet.colorado.edu/es/simulations/buoyancy)   
![](https://phet.colorado.edu/sims/html/buoyancy/latest/buoyancy-900.png)   

[Principio de Arquímedes  | Educaplus](http://www.educaplus.org/play-133-Principio-de-Arqu%C3%ADmedes.html)   

[Flotabilidad - educaplus](https://www.educaplus.org/game/flotabilidad)  

[Hablemos de barcos. Por qué los patrones de los grandes buques veneran a Arquímedes.](https://aberron.substack.com/p/hablemos-de-barcos)  
![](https://cdn.substack.com/image/fetch/w_1456,c_limit,f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fba028b32-2f20-4c27-be50-bb014b33bbfd_1598x897.jpeg)  

[Sea Surface Temperature, Salinity and Density - nasa.gov](https://svs.gsfc.nasa.gov/3652)  
  
 [Arquímedes &#8211; puede un petrolero flotar en un litro de agua? &laquo; Hiperesfera](https://hiperesfera.wordpress.com/2009/01/13/arquimedes-puede-un-petrolero-flotar-en-un-litro-de-agua/)   
  
Si desde un bote tiras una piedra al agua, ¿el nivel del agua baja, sube o se mantiene igual?  
 [Si desde un bote tiras una piedra al agua, ¿el nivel del agua baja, sube o se mantiene igual? | Microsiervos (Ciencia)](http://www.microsiervos.com/archivo/ciencia/problema-bote-piedra-agua.html)   
  
Iceberger  
Draw an iceberg and see how it will float.  
 [Iceberger](https://joshdata.me/iceberger.html)   
  
 [https://twitter.com/UrbanFoxxxx/status/1375762272321208321](https://twitter.com/UrbanFoxxxx/status/1375762272321208321) Heavy-lift vessels are a mood  
 [![](https://pbs.twimg.com/media/ExevuqoWEAAR79d?format=jpg "") ](https://pbs.twimg.com/media/ExevuqoWEAAR79d?format=jpg)  
 [![](https://pbs.twimg.com/media/Exev86wXEAQOiev?format=jpg "") ](https://pbs.twimg.com/media/Exev86wXEAQOiev?format=jpg)   
  
 [BOKA Vanguard loading Carnival Vista on Vimeo](https://vimeo.com/348147095) BOKA Vanguard loading Carnival Vista  
 [https://twitter.com/Rainmaker1973/status/140084479898728038](https://twitter.com/Rainmaker1973/status/140084479898728038) 6  
This 2012 shot by photographer Maurizio Alejo titled "balloons" is a work of art, but also shows a physics balance condition realized with a helium filled balloon and an air filled one [author's site:  [Photo - Mauricio Alejo](http://ow.ly/400h30on69g) ] [![](https://pbs.twimg.com/media/E2-APS2X0AYzhOf?format=jpg "") ](https://pbs.twimg.com/media/E2-APS2X0AYzhOf?format=jpg)   
  
