---
aliases:
  - /home/pruebas-de-acceso-a-grado-medio/
---

# Pruebas de acceso a Grado Medio

Relacionado con el ámbito científico en diversificación, FP Básica, educación de adultos,  [pruebas libres graduado ESO](/home/pruebas/pruebas-libres-graduado-eso) Las pruebas tienen una parte matemática / científico / tecnológica, y también pueden suponer un repertorio de ejercicios útiles, ya que una vez publicadas son de dominio público, sin derechos de autor.  
Información [todofp.es Pruebas de acceso a ciclos formativos de grado medio](https://www.todofp.es/pruebas-convalidaciones/pruebas/ciclos-grado-medio.html)
[todofp.es, Pruebas de acceso a ciclos formativos de grado medio, Modelos de exámenes de años anteriores ](https://www.todofp.es/pruebas-convalidaciones/pruebas/ciclos-grado-medio/modelos-examen-anteriores.html)

[Pruebas de acceso a ciclos formativos de Formación Profesional - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/pruebas-acceso-ciclos-formativos-formacion-profesional)  

[Pruebas de acceso a ciclos formativos de formación profesional, de artes plásticas y diseño y prueba sustitutiva del requisito académico de acceso a enseñanzas deportivas de régimen especial. - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/pruebas-acceso)  

Ver separado [pruebas Acceso FP Grado Superior](/home/pruebas/pruebas-de-acceso-a-grado-superior)

##   [Normativa y currículo Pruebas de Acceso Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio/normativa-y-curriculo-pruebas-acceso-grado-medio) 

##  Enunciados agrupados por bloques, elaboración propia (parte científico - tecnológica)
Se agrupan solamente en dos bloques: Materia y Energía (que incluye electricidad) y Mecánica (cinemática y dinámica). Están en [drive.fiquipedia ficheros-pruebas-acceso-grado-medio](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/pruebas-de-acceso-a-grado-medio/ficheros-pruebas-acceso-grado-medio) , y aquí organizados en tabla.
|  Bloque |  Enunciados |  Soluciones | 
|:-:|:-:|:-:|
| Energía y Materia (incluye electricidad) |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-de-acceso-a-grado-medio/ficheros-pruebas-acceso-grado-medio/GM-PruebaAcceso-MateriaEnergia.pdf)  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-de-acceso-a-grado-medio/ficheros-pruebas-acceso-grado-medio/GM-PruebaAcceso-MateriaEnergia-soluc.pdf)  | 
| Mecánica (cinemática, dinámica) |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-de-acceso-a-grado-medio/ficheros-pruebas-acceso-grado-medio/GM-PruebaAcceso-Mec%C3%A1nica.pdf)  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-de-acceso-a-grado-medio/ficheros-pruebas-acceso-grado-medio/GM-PruebaAcceso-Mec%C3%A1nica-soluc.pdf)  | 

##  Recopilación local de enunciados completos / originales ámbito científico-tecnológico
Los enunciados se almacenan por separado en [drive.fiquipedia GradoMedioxComunidades](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/GradoMedioxComunidades)  
Eso implica que no se puede llegar a ellos encontrándolos a través de un buscador como Google en los casos en los que no están escaneados, aunque en general los enunciados estarán como texto en el apartado de enunciados agrupados por bloques. 

| **Curso** | **Madrid** | **Castilla-La Mancha** | 
|:-:|:-:|:-:|
| 2000 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2000-madrid-GM-CT-exam.pdf)  |  | 
| 2001 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2001-madrid-GM-CT-exam.pdf)  |  | 
| 2002 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2002-madrid-GM-CT-exam.pdf)  |  | 
| 2003 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2003-madrid-GM-CT-exam.pdf)  |  | 
| 2004 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2004-madrid-GM-CT-exam.doc)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2004-madrid-GM-CT-soluc.doc "soluciones")  |  | 
| 2005 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2005-madrid-GM-CT-exam.doc)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2005-madrid-GM-CT-soluc.doc "soluciones")  |  | 
| 2006 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2006-madrid-GM-CT-exam.pdf)  |  | 
| 2007 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2007-madrid-GM-CT-exam.pdf)  |  | 
| 2008 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2008-madrid-GM-CT-exam.pdf)  |  | 
| 2009 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2009-06-madrid-GM-CT-exam.pdf)  |  | 
| 2010 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2010-06-madrid-GM-CT-exam.pdf)  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2010-CastillaLaMancha-modelo-GM-CT.pdf "modelo")  | 
| 2011 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2011-06-madrid-GM-CT-exam.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2011-06-madrid-GM-CT-soluc.pdf "soluciones")  |  | 
| 2012 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2012-05-madrid-GM-CT-exam.pdf)  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2012-CastillaLaMancha-06-GM-CT.pdf "junio")  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2012-CastillaLaMancha-09-GM-CT.pdf "septiembre")  | 
| 2013 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2013-06-madrid-GM-CT-exam.pdf)  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2013-CastillaLaMancha-06-GM-CT.pdf "junio")  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2013-CastillaLaMancha-09-GM-CT.pdf "septiembre")  | 
| 2014 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2014-05-madrid-GM-CT-exam.pdf)  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2014-CastillaLaMancha-ord-GM-CT.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2014-CastillaLaMancha-ext-GM-CT.pdf) | 
| 2015 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2015-05-madrid-GM-CT-exam.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2015-05-madrid-GM-CT-Criterios-Soluciones-HojaErrataSoluciones.pdf "criterios, soluciones, errata soluciones")  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2015-CastillaLaMancha-ord-GM-CT.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2015-CastillaLaMancha-ext-GM-CT.pdf) | 
| 2016 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2016-05-madrid-GM-CT-exam.pdf)  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2016-CastillaLaMancha-ord-GM-CT.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2016-CastillaLaMancha-ext-GM-CT.pdf) | 
| 2017 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2017-05-madrid-GM-CT-exam.pdf)  | [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2017-CastillaLaMancha-ord-GM-CT.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2017-CastillaLaMancha-ext-GM-CT.pdf)   | 
| 2018 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2018-05-madrid-GM-CT-exam.pdf)  | [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2018-CastillaLaMancha-ord-GM-CT.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2018-CastillaLaMancha-ext-GM-CT.pdf)   | 
| 2019 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2019-05-madrid-GM-CT-exam.pdf)  | [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2019-CastillaLaMancha-ord-GM-CT.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2019-CastillaLaMancha-ext-GM-CT.pdf)  | 
| 2020 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2020-madrid-GM-CT-exam.pdf) | [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2020-CastillaLaMancha-ord-GM-CT.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2020-CastillaLaMancha-ext-GM-CT.pdf) | 
| 2021 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2021-05-madrid-GM-CT-exam.pdf)  | [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2021-CastillaLaMancha-ord-GM-CT.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2021-CastillaLaMancha-ext-GM-CT.pdf)  | 
| 2022 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2022-05-madrid-GM-CT-exam.pdf)  | [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2022-CastillaLaMancha-ord-GM-CT.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/CastillaLaMancha/2022-CastillaLaMancha-ext-GM-CT.pdf)  | 
| 2023 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2023-05-madrid-GM-CT-exam.pdf)  |  | 
| 2024 |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/GradoMedioxComunidades/Madrid/2024-05-madrid-GM-CT-exam.pdf)  |  | 


##  Recursos externos con enunciados/soluciones


###  Andalucía
[juntadeandalucia.es Quiero información > Pruebas y procedimientos > Pruebas de acceso a ciclos formativos de Formación Profesional](https://www.juntadeandalucia.es/educacion/portals/web/formacion-profesional-andaluza/quiero-formarme/pruebas-y-procedimientos/pruebas-acceso)  

[juntadeandalucia.es     Temas > Estudiar >  Formación Profesional > Pruebas de acceso](https://www.juntadeandalucia.es/temas/estudiar/fp/pruebas-acceso.html)

[juntadeandalucia.es  Pruebas y procedimientos > Pruebas de acceso a ciclos > Ejercicios de años anteriores ](https://www.juntadeandalucia.es/educacion/portals/web/formacion-profesional-andaluza/quiero-formarme/pruebas-y-procedimientos/pruebas-acceso/ejercicios_anteriores) 

###  Aragón
 [aragon.es Pruebas de acceso a la enseñanzas de formación profesional](https://www.aragon.es/tramitador/-/tramite/participacion-pruebas-acceso-formacion-profesional) 

###  Asturias
 [www.educastur.es Pruebas acceso ciclos formativos. GM-GS](https://www.educastur.es/estudiantes/fp/pruebas-acceso-gm-gs) 

###  Canarias
 [gobiernodecanarias.org Pruebas de acceso > Acceso a los ciclos formativos de grado medio](https://www.gobiernodecanarias.org/educacion/web/formacion_profesional/acceso_ciclos_formativos/grado_medio/) 

###  Castilla La Mancha
[Pruebas de Acceso a Ciclos Formativos de Formación Profesional - Estructura, requisitos, modelos. › Grado Medio. Exámenes de años anteriores](http://www.educa.jccm.es/educa-jccm/cm/educa_jccm/tkContent;jsessionid=0a1201dd30d6e802df5a565f4436a568fb8e5c3ee05f.e34KaxaRc3iTe3yQbxmPbNuNb41DqQLvae?pgseed=1240580121322&idContent=28863&locale=es_ES&textOnly=false) 

###  Madrid  
[sauce.pntic.mec.es Recursos en internet para orientación académica y profesional](http://sauce.pntic.mec.es/mbenit4/) © Manuel Benito Blanco  
[sauce.pntic.mec.es Formación profesional](http://sauce.pntic.mec.es/mbenit4/fprofesional.htm)  

Departamento de orientación Colegio Marqués del Vallejo, Valdemoro, Madrid  
[orientacionjuncarejo.blogspot.com.es Exámenes Prueba Acceso Grado Medio COMUNIDAD de MADRID](http://orientacionjuncarejo.blogspot.com.es/p/examenes-prueba-acceso-grado-medio.html)  

[Ejercicios resueltos de acceso a FP grado medio de Madrid - Javier Fernánedez Panadero](https://www.youtube.com/playlist?list=PLzqyAKVt4MgPR70pIOsuo2wrqpBFE_ejp)  

###  Valencia
 [ceice.gva.es Pruebas de acceso a ciclos y cursos preparatorios Orientaciones Grado Medio](https://ceice.gva.es/es/web/formacion-profesional/orientaciones-grado-medio) 
 
