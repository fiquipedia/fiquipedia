
# Caza del tesoro

*"Una caza de tesoro es una actividad didáctica que consiste en una serie de preguntas y una lista de direcciones de páginas web en las que los alumnos deben buscar las respuestas. Con este método, además de conseguir que los alumnos aprendan unos hechos o conceptos determinados, conseguimos desarrollar su competencia digital. Estas actividades estimulan la adquisición de destrezas sobre navegación y otros conocimientos prácticos sobre Internet. Utilizando cazas de tesoros podemos también mejorar las destrezas de lectura y comprensión de textos en línea.*  
*El profesor debe formular las preguntas adecuadas, elegir los recursos web adaptados al currículo y el nivel de los alumnos, formar los grupos de alumnos y monitorizar el trabajo de los grupos y decidir el tiempo necesario para la resolución de la caza.*  
*Una caza de tesoros es una actividad muy estructurada y más sencilla para los alumnos que una webquest ya que no requiere la transformación final de la información y su valoración."*  

Definición tomada del curso "La competencia digital en Secundaria" de Ángeles Puertas Serrano, Departamento TIC del CRIF Las Acacias de Madrid, Material con licencia cc-by-nc-sa

* Curiosidades de la Tabla Periódica  
[http://www.aula21.net/cazas/cazasaula21/tablaperiodica.html](http://www.aula21.net/cazas/cazasaula21/tablaperiodica.html)  
Javier Ortiz de Urbina, Licenciamiento no detallado. Es una caza del tesoro sobre la tabla periódica, con 10 preguntas sencillas y una gran pregunta que realmente son varias. Sí detalla criterios de evaluación. Los recursos no están validados, fallan varios enlaces pero son fácilmente sustituibles por otros.
 
