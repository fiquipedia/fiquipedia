# Currículo Electrotecnia 2º Bachillerato

En la Comunidad de Madrid se desarrolla en  [DECRETO 67/2008, de 19 de junio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato](http://gestiona.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&idnorma=6166&word=S&wordperfect=N&pdf=S) , B.O.C.M. Núm. 152 de VIERNES 27 DE JUNIO DE 2008, páginas 59 a 60 en formato pdf.  
  
Es una materia que desaparece en 2º de Bachillerato en la LOMCE, no figura en anexo I (donde sí están Física, Química...)  
Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato  
 [BOE.es - BOE-A-2015-37 Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2015-37)   

## Introducción
  
Esta materia requiere conocimientos incluidos en Física y Química.  
  
Los fenómenos electromagnéticos y sus efectos están actualmente entre los campos de conocimiento con mayor capacidad para intervenir en la vida de las personas y de la sociedad. La enorme cantidad de aplicaciones que se han desarrollado desde finales del siglo XIX han modificado sustancialmente las condiciones de vida de las personas, los procesos económicos, la gestión del conocimiento y la investigación científica. El manejo de los fundamentos de los fenómenos electromagnéticos y de las soluciones que se pueden aplicar para utilizarlos se ha convertido en un elemento esencial en cualquier proceso tecnológico.  
  
La Electrotecnia en Bachillerato debe permitir la consolidación de los aprendizajes sobre las leyes que permiten conocer los fenómenos eléctricos, predecir su desarrollo y, sobre todo, utilizarlos con propósitos determinados a través de las aplicaciones de la electricidad con fines industriales, científicos, etcétera. Se trata, con ello, de proporcionar aprendizajes relevantes que ayuden a consolidar una sólida formación de carácter tecnológico abriendo, además, un gran abanico de posibilidades en múltiples opciones de formación electrotécnica más especializada. Esta materia cumple, así, el doble propósito de servir como formación de base para quienes decidan orientar su vida profesional hacia los ciclos formativos y para quienes continúen con vías académicas del campo de los estudios técnicos.  
  
El carácter de ciencia aplicada le confiere un importante valor formativo, al integrar y poner en función conocimientos procedentes de disciplinas científicas de naturaleza más abstracta y especulativa, permitiendo ver desde otro punto de vista y de forma más palpable la necesidad de los conocimientos científicos anteriormente adquiridos. También ejerce un papel de catalizador del tono científico y técnico que le es propio, profundizando y sistematizando aprendizajes afines procedentes de etapas educativas anteriores.  
  
La enseñanza de la Electrotecnia debe conjugar de manera equilibrada los tres ejes transversales que la configuran. Por una parte la fundamentación científica necesaria para comprender suficientemente los fenómenos y las aplicaciones. En segundo lugar el conocimiento de las soluciones técnicas que han permitido la utilización de los fenómenos electromagnéticos en una amplia variedad de aplicaciones y, en tercer lugar, la experimentación que haga posible la medida precisa y el manejo por parte de los alumnos de los dispositivos electrotécnicos con destreza y seguridad suficientes. Para lograr el equilibrio entre estos tres ejes es preciso el trabajo en tres grandes campos del conocimiento y la experiencia: Los conceptos y leyes científicas que explican los fenómenos físicos que tienen lugar en los dispositivos eléctricos; los elementos con los que se componen circuitos y aparatos eléctricos, su principio de funcionamiento y su disposición y conexiones características y, por último, las técnicas de análisis, cálculo y predicción del comportamiento de circuitos y dispositivos eléctricos.  
  
El campo disciplinar abarca, pues, el estudio de los fenómenos eléctricos y electro-magnéticos, desde el punto de vista de su utilidad práctica, las técnicas de diseño y construcción de dispositivos eléctricos característicos, ya sean circuitos, máquinas o sistemas complejos, y las técnicas de cálculo y medida de magnitudes en ellos. Los contenidos de Electrotecnia recorren la revisión teórico-práctica de los fenómenos eléctricos y electromagnéticos, el estudio de los circuitos y las máquinas eléctricas, y los dispositivos básicos que permiten su utilización y aplicación.  
  
El desarrollo de esta materia parte de contenidos que se han desarrollado en la materia de Física y Química, especialmente los asociados a la fundamentación de la electricidad y el estudio de la energía.  

## Objetivos 
La enseñanza de la Electrotecnia en el Bachillerato tendrá como finalidad el desarrollo de las siguientes capacidades:  
1. Comprender y explicar el comportamiento de dispositivos eléctricos sencillos y los principios y leyes físicas que los fundamentan.  
2. Seleccionar y utilizar correctamente los componentes de un circuito eléctrico que responda a una finalidad predeterminada, comprendiendo su funcionamiento.  
3. Calcular y medir las principales magnitudes de un circuito eléctrico, en corriente continua y alterna, compuesto por elementos discretos en régimen permanente.  
4. Analizar e interpretar esquemas y planos de instalaciones y equipos eléctricos característicos, comprendiendo la función de un elemento o grupo funcional de elementos en el conjunto.  
5. Seleccionar e interpretar información adecuada para plantear y valorar soluciones, en el ámbito de la electrotecnia, a problemas técnicos comunes.  
6. Conocer el funcionamiento, elegir y utilizar adecuadamente los aparatos de medida de magnitudes eléctricas, estimando anticipadamente su orden de magnitud y valorando su grado de precisión.  
7. Proponer soluciones a problemas en el campo de la electrotecnia con un nivel de precisión coherente con el de las diversas magnitudes que intervienen en ellos.  
8. Comprender las descripciones y características de los dispositivos eléctricos y transmitir con precisión los conocimientos e ideas sobre ellos, utilizando vocabulario, símbolos y formas de expresión apropiadas.  
9. Actuar con autonomía, confianza y seguridad al inspeccionar, manipular e intervenir en circuitos y máquinas eléctricas para comprender su funcionamiento.  

## Contenidos

### 1. Conceptos y fenómenos eléctricos básicos y medidas electrotécnicas.
- Magnitudes y unidades eléctricas. Diferencia de potencial. Fuerza electromotriz. Intensidad y densidad de corriente. Resistencia eléctrica. Ley de Ohm.  
- Corriente continua (c.c.) y corriente alterna (c.a.).  
- Condensador. Capacidad. Carga y descarga del condensador.  
- Potencia, trabajo y energía. Efecto Joule.  
- Efectos de la corriente eléctrica.  
- Medidas en circuitos. Medida de magnitudes de corriente continua y corriente alterna.  
- Instrumentos. Procedimientos de medida.  


### 2. Circuitos eléctricos de corriente continua.
- Características e identificación de resistencias y condensadores. Pilas y acumuladores.  
- Análisis de circuitos de corriente continua. Leyes y procedimientos: Leyes de Kirchoff, método de las mallas, principio de superposición, teorema de Thevenin. Acoplamientos de receptores: Asociación de resistencias y condensadores. Divisores de tensión e intensidad.  
- Circuito RC. Carga y descarga de un condensador. Constante de tiempo.  


### 3. Conceptos y fenómenos electromagnéticos.
- Imanes. Intensidad del campo magnético. Inducción y flujo magnético.  
- Campos y fuerzas magnéticas creados por corrientes eléctricas. Fuerzas electromagnética y electrodinámica. Fuerza sobre una corriente en un campo magnético. Par de fuerzas sobre una espira plana.  
- Propiedades magnéticas de los materiales. Permeabilidad. Circuito magnético. Fuerza magnetomotriz. Reluctancia. Ley de Ohm de los circuitos magnéticos.  
- Inducción electromagnética. Leyes de Faraday y de Lenz. Inductancia. Autoinducción.  
- Circuito RL. Carga y descarga de una autoinducción.  


### 4. Circuitos eléctricos de corriente alterna.
- Características y magnitudes de la corriente alterna. Magnitudes senoidales. Efectos de la resistencia, autoinducción y capacidad en la corriente alterna. Reactancia. Impedancia. Variación de la impedancia con la frecuencia. Representación gráfica. Impedancia compleja.  
- Análisis de circuitos de corriente alterna monofásicos. Leyes y procedimientos. Circuitos simples. Potencia en corriente alterna monofásica: Instantánea, activa, reactiva y aparente. Factor de potencia y su corrección. Representación gráfica. Resonancia.  
- Sistemas trifásicos: Generación, acoplamientos, tipos, potencias. Mejora del factor de potencia.  
- Semiconductores. Diodos, transistores, tiristores. Valores característicos y su comprobación. Circuitos electrónicos básicos: Rectificadores, amplificadores, multivibradores.  
- Seguridad en instalaciones eléctricas. Introducción a las instalaciones domésticas, interruptores diferenciales.  


### 5. Circuitos prácticos y de aplicación.
- Circuitos de alumbrado. Circuitos de calefacción. Elementos y materiales. Consumo, rendimiento y aplicaciones.  


### 6. Máquinas eléctricas.
- Transformadores. Funcionamiento. Constitución. Pérdidas. Rendimiento. Ensayos básicos: De vacío y de cortocircuito.  
- Máquinas de corriente continua. Dinamos y motores de c.c. Funcionamiento. Conexionados: Tipos de excitación. Conmutación. Reacción del inducido. Sentido de giro. Velocidad. Par electromagnético, resistente y motor. Balance de potencias. Rendimiento. Ensayos básicos.  
- Máquinas rotativas de corriente alterna. Alternadores, motores síncronos y motores asíncronos. Funcionamiento. Conexionados. Sentido de giro. Velocidad. Balance de potencias. Rendimiento.  
- Eficiencia energética de los dispositivos eléctricos y electrónicos.  


## Criterios de evaluación
1. Explicar cualitativamente el funcionamiento de circuitos simples destinados a producir luz, energía motriz o calor y señalar las relaciones e interacciones entre los fenómenos que tienen lugar.  
2. Seleccionar elementos o componentes de valor adecuado y conectarlos correctamente para formar un circuito, característico y sencillo.  
3. Explicar cualitativamente los fenómenos derivados de una alteración en un elemento de un circuito eléctrico sencillo y describir las variaciones que se espera que tomen los valores de tensión y corriente.  
4. Calcular y representar vectorialmente las magnitudes básicas de un circuito mixto simple, compuesto por cargas resistivas y reactivas y alimentado por un generador senoidal monofásico.  
5. Analizar planos de circuitos, instalaciones y equipos eléctricos de uso común e identificar la función de un elemento discreto o de un bloque funcional en el conjunto.  
6. Representar gráficamente en un esquema de conexiones o en un diagrama de bloques funcionales la composición y el funcionamiento de una instalación o equipo eléctrico sencillo y de uso común.  
7. Interpretar las especificaciones técnicas de un elemento o dispositivo eléctrico y determinar las magnitudes principales de su comportamiento en condiciones nominales.  
8. Medir las magnitudes básicas de un circuito eléctrico y seleccionar el aparato de medida adecuado, conectándolo correctamente y eligiendo la escala óptima.  
9. Interpretar las medidas efectuadas sobre circuitos eléctricos o sobre sus componentes para verificar su correcto funcionamiento, localizar averías e identificar sus posibles causas.  
10. Utilizar las magnitudes de referencia de forma coherente y correcta a la hora de expresar la solución de los problemas.  
11. Describir los elementos y materiales fundamentales de un circuito básico de alumbrado y de un circuito básico de calefacción.  
 
