# Currículo LOMLOE: criterios de evaluación

Enlazan con las [competencias específicas](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas)  
También enlazan con [indicadores de logro](/home/legislacion-educativa/indicadores-de-logro)  

Se indentifican con números asociados a las competencias específicas. Por ejemplo el criterio 1.2 será el segundo criterio asociado a la competencia específica 1.  

[Real Decreto 217/2022 ESO Artículo 2. Definiciones.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a2)  
[Real Decreto 243/2022 Bachillerato Artículo 2. Definiciones.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a2)  
> d) Criterios de evaluación: referentes que indican los niveles de desempeño esperados en el alumnado en las situaciones o actividades a las que se refieren las competencias específicas de cada materia o ámbito en un momento determinado de su proceso de aprendizaje.  

En [Ideas clave de la LOMLOE - Raúl Diego](https://www.rauldiego.es/ideas-clave-de-la-lomloe/) se indica en página 42 de 52  

> Se formulan a partir del PARA QUÉ del Criterio Específico.  
(Usa "criterio específico" pero hace referencia a competencia específica)  

En página 43 se indica  
> Estructura sintáctica de la formulación  
> qué (infinitivo referido a un proceso y la capacidad \[intencionalidad última] que se pretende adquirir + contenido \[lo que el alumnado debe saber y el profesorado enseñar]  
> + cómo (contexto, situación o modo de aplicación, en gerundio o adverbios y expresiones modales)   


En [Currículo LOMLOE concepto y estructura](/home/legislacion-educativa/lomloe/curriculo-concepto-estructura) comento relacionado el vídeo "Prepárate para la LOMLOE: cómo evaluar y planificar con Additio"   

Y me comentan que en Andalucía las instrucciones sí indican pesos en criterios de evaluación  
[twitter rcabreratic/status/1572207946909552641](https://twitter.com/rcabreratic/status/1572207946909552641)  
[Te adjunto la instrucción](https://drive.google.com/file/d/1koVOE9cgFmXGblJxBtUib8u848UzIAkY/view?usp=sharing). En punto 9 de las instrucciones, apartado 5 (pag. 7)  

[INSTRUCCIÓN CONJUNTA 1 /2022, DE 23 DE JUNIO, DE LA DIRECCIÓN GENERAL DE ORDENACIÓN Y EVALUACIÓN EDUCATIVA Y DE LA DIRECCIÓN GENERAL DE FORMACIÓN PROFESIONAL, POR LA QUE SE ESTABLECEN ASPECTOS DE ORGANIZACIÓN Y FUNCIONAMIENTO PARA LOS CENTROS QUE IMPARTAN EDUCACIÓN SECUNDARIA OBLIGATORIA PARA EL CURSO 2022/2023. (PDF de 259 páginas)](https://www.juntadeandalucia.es/educacion/portals/delegate/content/2f56ec21-36e2-4651-bd5e-160408e96d17)  

Consultable en [Herramienta Centralizada de Verificación](https://ws050.juntadeandalucia.es/verificarFirma) con tFc2e7N3Z4Z68QL9FJT5PMHC4F7YYM  


> 5. En los cursos primero y tercero, **la totalidad de los criterios de evaluación contribuyen en la misma medida, al grado de desarrollo de la competencia específica, por lo que tendrán el mismo valor a la hora de determinar el grado de desarrollo de la misma**.  

Además se comenta que sí se pueden crear criterios  
[twitter rcabreratic/status/1572209065081593857](https://twitter.com/rcabreratic/status/1572209065081593857)  
Saberes básicos junto a sus criterios ordenados por bloques de conocimiento. Ayuda a saber qué criterios se usan en los bloques. **Hemos visto que hay saberes básicos sin criterios asociados**. Entiendo que los asociaremos en el departamento y **añadiremos más**.  

Añadir más criterios de evaluación respecto a los que fija el currículo no sabía si era posible (en LOMCE había cosas que eran "competencia" estatal [art6.bis](https://boe.es/buscar/act.php?id=BOE-A-2006-7899&tn=1&p=20131210) )

[twitter rcabreratic/status/1572222777964503040](https://twitter.com/rcabreratic/status/1572222777964503040)  
Si se puede viene también fijado en las propias instrucciones. Punto Segundo apartado 3( pag. 3)  
> 3. En los cursos primero y tercero se consideran saberes básicos mínimos, los saberes básicos que como
mínimo están vinculados a cada criterio de evaluación y se han de trabajar de manera conjunta. En los Anexos
III, IV, V y VI se incluyen tablas en cada materia de los cursos primero y tercero, que presentan la vinculación de
competencias específicas, con criterios de evaluación y saberes básicos. Así como la relación de criterios de
evaluación y competencias específicas viene establecida en la normativa básica, la vinculación de saberes se
presenta con la intención de orientar la planificación de los aprendizajes de manera que **pueda ser ampliada**
siempre que se desarrollen todos los saberes mínimos.  


Aparte en ese documento de 2022/2023, en el que se implantan cursos impares, para 3ºESO se indica para CE los saberes básicos asociados (en Andalucía se pone un código a cada saber básico)  
[Página 84](https://www.juntadeandalucia.es/educacion/portals/delegate/content/2f56ec21-36e2-4651-bd5e-160408e96d17#page=84)  
Tabla Física y Química (tercer curso)  
Competencias específicas  vs Criterios de Evaluación vs Saberes básicos mínimos  

La relación de Andalucía entre competencias específicas y saberes se cuestiona según las actividades / situaciones de aprendizaje  
[twitter pbeltranp/status/1583210227016806400](https://twitter.com/pbeltranp/status/1583210227016806400)  
No se pueden relacionar competencias específicas con saberes. Un mismo saber, dependiendo de con qué tareas se trabaje (por no decir situaciones de aprendizaje) movilizará unas CE y otras. O ninguna.  

> Opinión personal: entiendo el cuestionamiento de que un saber puede trabajar competencias específicas distintas, pero no que haya asociación porque la definición de competencia específica oficial se relaciona con saberes [Real Decreto 217/2022 ESO Artículo 2. Definiciones.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a2) ya que para poder desarrollar una competencia hay que hacerla sobre unos saberes básicos, no en vacío.   
c) **Competencias específicas**: desempeños que el alumnado debe poder desplegar en actividades o en situaciones cuyo abordaje **requiere de los saberes básicos** de cada materia o ámbito. 



>**Criterios de evaluación**   
Se incluirán los criterios de evaluación —y, consecuentemente, los saberes básicos/contenidos a ellos 
asociados—  para  cada  área  o  materia  al  finalizar  el  segundo  y  tercer  ciclo  de  primaria,  4º  y  6º 
respectivamente, y para 2º y 4º en ESO.  
Su redacción debe ser inequívocamente competencial, en un modelo similar al de esta definición: 
«Expresan un proceso y la capacidad que el alumnado debe adquirir (redactada en infinitivo) + el 
contenido (que se expresa con sustantivos y es lo que el alumno debe aprender) + el contexto o 
modo de aplicación y uso del contenido (que se expresa en gerundio, con adverbios de modo o con 
expresiones «a través de»).   
La capacidad es el referente, la intencionalidad última. El contenido es lo que tiene el alumno que 
aprender, conocer y el profesorado enseñar, para conseguir la capacidad.   
El contexto es la aplicación, implementación, uso del contenido en situaciones diversas»   
El gran reto está en conseguir que estos criterios de evaluación, aunque se formulen para cada materia y 
ciclo  o  curso,  vayan  indisolublemente  unidos  a  los  descriptores  del  Perfil  de  salida,  a  través  de  las 
competencias  específicas,  de  tal  manera  que  no  se  pueda  producir  una  evaluación  de  la  materia 
independiente de las competencias clave.  


Ver [Septiembre se acerca, y ahora ¿qué? (I) - migranito](http://migranito.blogspot.com/2022/08/septiembre-se-acerca-y-ahora-que-i.html)  
Se comenta para el primer criterio 1.1 de GH de ESO en Baleares  
> En teoría, todos los criterios deberían tener la misma estructura:  
> Criterios de evaluación: estructura  
> (Qué -> infinitivo) + (complemento) + (cómo)   
> En este caso, esta estructura se cumple. Te aviso que no siempre, ¿vale?  

TRADUCTOR LOMLOE V2  
 [![](https://img.youtube.com/vi/Vkkt0NExBqE/0.jpg)](https://www.youtube.com/watch?v=Vkkt0NExBqE "TRADUCTOR LOMLOE V2")   
Tabla de relación criterios de evaluación / instrumentos de evaluación  


**noviembre 2020**
[Una propuesta de evaluación en Matemáticas (ESO, adaptable a primaria)](https://tierradenumeros.com/post/hilo-evaluacion-calificacion-propuesta-completa/)  
Versión en blog de este hilo [Twitter pbeltranp/status/1322861015676182528](https://x.com/pbeltranp/status/1322861015676182528)  
Se cita esto al final que no está en blog
[Twitter pbeltranp/status/1546518814044700672](https://x.com/pbeltranp/status/1546518814044700672)  
Añado referencia de interés que he dejado en alguna interacción. Esta forma de evaluar encaja mucho con lo que propone @pgliljedahl en su libro Building Thinking Classrooms (2021). De hecho, para algunos aspectos nos basta el día a día.  
![](https://pbs.twimg.com/media/FXZWWDCWQAA-VBt?format=png)  

**29 enero 2024**  

[Twitter DaviidMPB/status/1751992250693779487](https://x.com/DaviidMPB/status/1751992250693779487)  
Estoy leyendo todo lo que puedo sobre el tema, pero en Bachillerato me surgen varias dudas con la calificación. Cómo aterrizar y ser coherente con la ley (relación entre compt clave, saberes, crit ev, inst ev), a la vez que explicar de qué manera se va a obtener la nota numérica.  
He leído libros (sobre todo de evaluación formativa), he visto vídeos de charlas que ha dado @pbeltranp sobre el tema, he leído los desarrollos autonómicos y guías que ofrece la propia administración... pero no termino de ver cómo aterrizarlo en Bach.  
Es decir, en la ley se establece que lo que se evalúa y califica, son los crit de ev (CE). La "dificultad" en FyQ es que los CE son tan generales que aplican a varios saberes de la materia. Esto resulta en que cada saber acaba con un número distinto de CE y viceversa.  
Podemos plantear diferentes instrumentos de evaluación a los que asociemos diversos CE. Ahora bien, ¿cómo sacamos una nota de ahí? Si damos un mismo % a todos los CE, todas las compt específicas dejan de valer lo mismo (y admitimos que hay contenidos más importantes que otros).  
Pero la alternativa es dar arbitrariamente un % a cada instrumento de evaluación, y eso entra en contradicción con la ley (no se deben evaluar/calificar los instrumentos, sino los criterios).  
También se podría pensar en dar a cada compt especifica el mismo peso, y aquí hay dos opciones: 1) dividir por igual el % en función del número de CE o 2) poner un % a cada CE. Sin embargo, de nuevo hay contenidos que tendrán +/- CE, y "sumando" % cada contenido pesaría distinto.  
**Todo esto es porque me niego a plantear 90% exámenes - 10 % trabajo en clase, que es lo que veo en la mayoría de PD**. Sería mucho más fácil de esa forma. Pero no me parece honesto, ni acorde a la ley, ni sobre todo bueno para que el alumnado aprenda (que al final es el fin).  
De momento he hecho una **distribución de cada CE en los diferentes saberes de mi materia (al estilo que he visto que hace el decreto autonómico andaluz)**, pero no veo bien cómo sacar la nota de cada UD o de cada CE en las evaluaciones.  
¿Planteo instrumentos de ev generales y les asigno siempre los mismo CE? ¿Especifico en cada UD dependiendo de los contenidos diferentes % a cada instrumento de evaluación? En definitiva, ¿cómo sacar de una forma "sencilla" la nota numérica, sin contravenir el espíritu de la ley?  

[Twitter pbeltranp/status/1752052757274456408](https://x.com/pbeltranp/status/1752052757274456408)  
Qué hilo más honesto e interesante. Con el contexto de acceso a estudios universitarios actual (y siendo consciente además de que no es la única continuación a bachillerato) voy a intentar plasmar mi visión. 1/n  
La evaluación ha de ser continua y diferenciada, además de, obviamente, formativa. Apenas se distingue de la de ESO en lo de diferenciada, porque ya no tenemos ese perfil de salida global. En bachillerato cada asignatura va "a su bola". 2/n  
Esto cada CCAA lo desarrolla con sus cosas, pero está recogido en el RD de enseñanzas mínimas, que además habla de «promover el uso generalizado de instrumentos de evaluación variados, diversos, flexibles y adaptados a las distintas situaciones de aprendizaje... 3/n  
que permitan la valoración objetiva de todo el alumnado, y que garanticen, asimismo, que las condiciones de realización de los procesos asociados a la evaluación se adaptan a las necesidades del alumnado con necesidad específica de apoyo educativo.» 4/n  
¿Qué hacemos entonces? Bueno, pocas cosas se pueden escribir en piedra, pero me atrevería a decir que **lo primero sería asumir que eso de que sea «objetiva» ha de entenderse más desde la idea de transparencia.** 5/n  
Hay una tensión más difícil de relajar que en EP/ESO con la necesidad de asignar una calificación. En bachillerato es numérica (aunque hay alguna CCAA que está con numéricas en ESO) y la sombra de la nota de admisión planea sobre este proceso. 6/n  
No es incompatible con utilizar instrumentos variados, mientras las reglas del juego estén claras. Desde luego, no lo es con ofrecer las oportunidades que haga falta para mejorar (el RD dice que debe ser continua, así que, ¿por qué penalizar por una prueba pasada?) 7/n  
Yo me declaro incapaz de afinar a la centésima. Incluso, a la décima. Sería partidario de fijarme una nota natural (sin decimales) que pueda asociar más fácilmente a los tipos de tareas. Como si fuera suspenso, aprobado, bien, notable bajo, notable alto, sobresaliente. 8/n  
Recoger indicios de aprendizaje que pueda relacionar luego con esa forma de calificar, y **jugar con la mediana. La mediana te soluciona los problemas de ponderación que señalabas**. Si haces media con los criterios, no atiendes a las competencias por igual; 9/n  
si la haces con las competencias, no atiendes a los criterios por igual. Esto podrías hacerlo como quisieras, pero es más transparente y honesto hacerlo en función de las tareas que se han ido haciendo. Por supuesto, abordando todas las competencias, eh. 10/n  
Si una evaluación has trabajado más la competencia N por lo que sea, ¿qué? En mates nos pasa parecido a lo de FyQ, que son competencias transversales, pero no me cuesta imaginarme una evaluación sobre geometría con más énfasis en la CE de razonamiento. 11/n  
No obstante, no tengo por qué decir que esa evaluación los criterios de la CE de razonamiento valdrán el doble o lo que sea. Simplemente, hago la mediana, por un lado. Por otro, me aseguro de trabajar las competencias convenientemente. 12/n  
Para terminar, una prueba escrita puede servir fácilmente como instrumento criterial si pensamos al estilo de lo que hacen en exámenes de idiomas. Para ello, es mejor que sean cortos y que se ciñan especialmente a ciertas competencias (dos o tres, por  ej.). 13/n  
Se califica con la guía que se considere oportuno, incluso con decimales si gustas. Pero a la hora de llevarse eso a los criterios de evaluación, se piensa en que un "nivel avanzado" sacaría entre 7,5 y 10 (me lo invento) y entonces sería un indicio avanzado. 14/n  
Digo «avanzado» y no «notable alto» porque a mí me resultaría más operativo jugar con básico-medio-avanzado y luego al final caracterizar las notas a partir de esos indicios. Y ya vale de rollo, que van 15 tuits 🤭. ¿Qué te parece? 15/15  

[Twitter DaviidMPB/status/1752250738833526830](https://x.com/DaviidMPB/status/1752250738833526830)  
Me los he visto, y me parecen una propuesta interesante, pero sobre todo en Bach sigo viendo el problema de que habrá que asignar los indicadores de logro de los criterios de evaluación a ciertos instrumentos, si no, ¿de dónde saco la nota? ¿Por qué 8.4 y no 7.7?  

pablofcayqca:  
Pero, por qué tienes problemas para asignar indicadores e instrumentos? De hecho, yo diseño el instrumento una vez que he definido el indicador y sus niveles.  

DaviidMPB:  
Creo que me he explicado mal. Lo que estoy haciendo es crear los indicadores de los crit de ev en función de los contenidos de la UD, y asignarlos a instrumentos. Mi problema está a la hora de cómo traducir eso en un número.  
Pongo en ejemplo concreto simple: para una UD establezco 3 inst ev (IE), y a cada uno entre 5-10 CE (algunos se repetirán y otros no). ¿Doy arbitrariamente un % a cada IE y según una rúbrica con diferentes niveles de desempeño, saco una nota? ¿o mido cada CE de forma individual?  

pablofcayqca:  
Creo que el problema puede estar en cómo se interpreta la asignación de instrumentos a criterios, que puede dar lugar a entender que lo que evaluamos es el instrumento y no el criterio. Intento desarrollar con tu ejemplo:  
En una UD propongo 10 aprendizajes, que se traducen en 10 indicadores (concretan los criterios haciendo uso de los contenidos, que son los que les dan sentido). Independientemente de la ponderación que les des a cada uno de ellos, tienes que pensar en cuáles serían los mejores...  
...instrumentos para valorar el nivel de adquisición de esos aprendizajes.  
10 indicadores (aprendizajes) = 10 instrumentos (para valorar cuál ha sido el nivel de aprendizaje alcanzado).  
Obviamente, pueden darse coincidencias, es decir, que utilice el mismo instrumento para valorar distintos aprendizajes.
Pero, aunque coincida el tipo, puede no coincidir el momento. Por ejemplo, yo valoro 2 aprendizajes diferentes haciendo uso de una lista de cotejo y a través...  
...de la observación en clase, pero lo hago en 2 momentos diferentes (serían 2 actividades diferentes). Aquí entiendo que no hay problema, porque utilizo el instrumento una vez, y luego otra vez (el uso es independiente para cada aprendizaje).  
¿Puede ser que la dificultad se dé cuando sí coincidan en el tiempo? Por ejemplo, cuando evaluamos varios criterios (interpretar un fenómeno, resolver problemas, cumplir con las normas básicas de la FyQ, etc.) en una prueba escrita.  
En ese caso, **el error sería asignar una calificación a la prueba escrita y extrapolarla a todos los criterios por igual**.
En resumen, independientemente de que uses el mismo instrumento para varios criterios (sea simultáneamente o no), o uses un solo instrumento para cada...  
...criterio, la clave es que lo que evalúas es el criterio/indicador/aprendizaje: la evaluación de cada aprendizaje es independiente del resto.  

>Intento leer y procesar, aunque me parece de una complejidad excesiva.  
En mis programaciones (soy jefe de departamento desde 2022-2023 que se implantó LOMLOE) intento reflejar ideas con la idea de simplificar. 

