# Recursos simulaciones: aspectos técnicos

1. Aspectos técnicos de simulaciones  
    1.1 Java  
    1.2 Navegadores y applets Java  
    1.2.1 Applets Java no soportados en Chrome desde 2015  
    1.2.2 Applets Java no soportados en Firefox desde marzo 2017  
    1.2.3 Applets Java no soportados en Microsoft Edge  
    1.2.4 Ejemplos de recursos que globalmente usan applets java  
    1.2.5 Solución definitiva: no usar applets   
    1.2.6 Soluciones temporales / parches  
    1.2.7 Configurar plugin java en el navegador  
    1.3 Descartes  
    1.4 Modellus  
2. Visualización Flash  
    2.1 Ruffle  
    2.2 Macromedia flashplayer como ejecutable windows portable  
    2.3 Basilisk portable con Flash  

(antiguo índice antes de la migración, pendiente crear como enlaces)  

##   Aspectos técnicos de simulaciones
Aunque algunas simulaciones nuevas se desarrollan ya por fin con HTML5 y son 100% portables (por ejemplo  [algunas simulaciones de la Universidad de Colorado](https://phet.colorado.edu/es/simulations/category/html) ), lo habitual no es eso, sino que dependen de algún requisito técnico. Es muy habitual por ejemplo que sean simulaciones java (applets), o por ejemplo en España depender de entornos como Descartes.  
Como java ha ido cambiando con el tiempo (aparte del java original de Sun (comprado por Oracle) también está openJDK), o Flash desapareció en 2020, hay veces que no funciona correctamente  
Intento comentar temas según los recuerdo o los voy usando

###   Java

####   Aplicaciones java bloqueadas en las últimas versiones de java
Información en [Why are Java applications blocked by your security settings with the latest Java?](http://www.java.com/en/download/help/java_blocked.xml)  
El tema es que por defecto aplicaciones que antes funcionaban ahora no lo hacen.  
Se ofrece como "workaround" (manera de para evitar el problema) añadir el sitio web a una lista de excepciones desde el panel de control.  
El panel de control es sencillo de iniciar desde windows, pero desde linux no es tan inmediato  
[¿Cómo puedo activar y ver la consola de Java para Linux y Solaris?](https://www.java.com/es/download/help/enable_console_linux.xml)  
En junio 2014 probado ok con `/opt/java/64/jre1.7.0_55/bin$ ./ControlPanel` añadiendo `http://walter-fendt.de` funcionan todos los applets, por ejemplo al tiempo  [http://www.walter-fendt.de/ph14s/springpendulum_s](http://www.walter-fendt.de/ph14s/springpendulum_s)  y  [http://www.walter-fendt.de/ph6es/springpendulum_es.htm](http://www.walter-fendt.de/ph6es/springpendulum_es.htm)  
Cuando hay dependencias de applets de otro servidor (por ejemplo geogebra descarga de jars.geogebra.org y/o www.geogebra.org), hay que añadirlos también  
En diciembre 2014 se comprueba que con java 8 update 20 ya no es posible, porque en "Niveles de seguridad en el Panel de Control de Java" desaparece la opción de seguridad media, por lo que aunque se añada como excepción no funciona.  
[¿Cómo se controla cuándo un applet o aplicación que no son de confianza se ejecutan en mi explorador web?](https://www.java.com/es/download/help/jcp_security.xml)  
Única opción: instalar una versión de java anterior!  
Pero java 7 deja de estar soportado a partir de abril de 2015!  
[Aviso de fin de las actualizaciones públicas de Java SE 7](http://www.java.com/es/download/faq/java_7.xml)  

Parece que hay foros que dicen que la lista sigue siendo válida aunque no se pueda cambiar con interfaz de panel de control?  
[Run unsigned Java applets](http://superuser.com/questions/809027/run-unsigned-java-applets)  
**Por verificar.**  
En [Upcoming Exception Site List in 7u51](https://blogs.oracle.com/java-platform-group/entry/upcoming_exception_site_list_in)  
Se indica  
>The file controlling the Exception Site List is stored in the user’s deployment location as described in the deployment configuration. On my Windows 7 laptop, this location is C:\Users\ecostlow\ AppData\LocalLow\  Sun\Java\Deployment\ security\exception.sites  

Parece que el fichero podría seguirse usando desde java 8 ??  
[Java 7 Deployment Configuration File and Properties](http://docs.oracle.com/javase/7/docs/technotes/guides/jweb/jcp/properties)  
[Java 8 Deployment Configuration File and Properties](http://docs.oracle.com/javase/8/docs/technotes/guides/deploy/properties)  
En linux visto en "~/.java/deployment/security/exception.sites"

####   Problemas al abrir jnlp (Java Network Launching Protocol) en firefox/ubuntu
Se soluciona asociando en firefox abrir esa extensión con el ejecutable javaws (Java Web Start) en /opt/java/...  
[Cannot open .jnlp file when asked to open with firefox web browser in ubuntu 11.10 using firefox 6](https://support.mozilla.org/es/questions/887742) 

####   Resolver problemas habituales en applets java
Orientados a desarrolladores, no a usuarios, pero con información útil.  
[Solving Common Applet Problems](http://docs.oracle.com/javase/tutorial/deployment/applet/problemsindex) 

####   Buscando e instalando versiones de java anteriores
Como java va cambiando, hay desarrollos anteriores que no funcionan. Algunas páginas, que ya no son mantenidas, indican instalar versiones antiguas de java que son con las que funcionan....  
Es una mala solución, pero a veces es la única posible desde el lado del "usuario", si en el servidor no se revisa el código y se adapta a las nuevas versiones de java (y si java cambia cosas sin tener en cuenta que hay cosas antiguas que se usan pero no se pueden revisar)  
 [Oracle Java Archive](http://www.oracle.com/technetwork/java/javase/archive-139210)  
 >The Oracle Java Archive offers self-service download access to some of our historical Java releases.**WARNING: These older versions of the JRE and JDK are provided to help developers debug issues in older systems. They are not updated with the latest security patches and are not recommended for use in production.**

###   Navegadores y applets Java
Oracle anunció en 2016 que dejaba de dar soporte a applets en navegadores  
[Moving to a Plugin-Free Web](https://blogs.oracle.com/java-platform-group/entry/moving_to_a_plugin_free)  

[JEP 289: Deprecate the Applet API, openjdk](http://openjdk.java.net/jeps/289)  
No se permiten applets pero sí se permite Java Web Start (JNLP)  
[¿Qué es Java Web Start y cómo se ejecuta?](https://www.java.com/es/download/faq/java_webstart.xml) 

####   Applets Java no soportados en Chrome desde 2015
[Java Plugins disappear after new Chrome update to Version 42.0.2311.90 m](https://productforums.google.com/forum/#!topic/chrome/m5sRq0_UKek)  

[The Final Countdown for NPAPI](https://blog.chromium.org/2014/11/the-final-countdown-for-npapi)  

####   Applets Java no soportados en Firefox desde marzo 2017
 [¿Por qué los plugins de Java, Silverlight y Adobe Acrobat, entre otros, ya no funcionan?](https://support.mozilla.org/es/kb/por-que-los-plugins-de-java-silverlight-y-adobe-acrobat-no-funcionan)  
 En la web de mozilla se indica cómo usar  
 [Usar el plugin de Java para ver contenido interactivo en Internet](https://support.mozilla.org/es/kb/usar-el-plugin-de-java-para-ver-contenido-interactivo-Internet)  
 aunque se avisaA partir de la [versión 52](https://support.mozilla.org/es/kb/averigua-que-version-de-firefox-estas-usando) de Firefox, se finalizará el soporte para todos los [plugins](https://support.mozilla.org/es/kb/usar-plugins-para-reproducir-audio-video-juegos-y-mas) NPAPI, excepto para Flash.  
 Para más detalles, consulta  [este documento de compatibilidad](https://www.fxsitecompat.com/docs/2016/plug-in-support-has-been-dropped-other-than-flash/)  y  [este artículo](https://support.mozilla.org/es/kb/por-que-los-plugins-de-java-silverlight-y-adobe-acrobat-no-funcionan).  
Se enlaza a información oracle de instalación del plugin en linux  
[Manual Installation and Registration of Java Plugin for Linux](http://www.oracle.com/technetwork/java/javase/manual-plugin-install-linux-136395) 

####   Applets Java no soportados en Microsoft Edge
 [Windows 10 y Java](https://www.java.com/es/download/faq/win10_faq.xml)  
 Windows indica "abrir con Explorer 11"

####   Ejemplos de recursos que globalmente usan applets java
No se trata de poner simulaciones concretas, sino tipos de recursos.  
Un ejemplo es usa el editor de wiris desde moodle. En 2018 su solución es pasar a usar JNLP  

####   Solución definitiva: no usar applets 
La solución definitiva sería migrar a HTML5 ó Java Web Start (JNLP), pero depende del desarrollador del applet, y a veces son antiguos / el desarrollador no está disponible para hacerlo.  
Algunos enlaces  
[Applet Development Guide, 12.5 Migrating Java Applets to the Java Network Launching Protocol](https://docs.oracle.com/javase/8/docs/technotes/guides/deploy/applet_dev_guide.html#CIADJHDC)  

[https://kbdeveloper.qoppa.com/?p=2404](https://kbdeveloper.qoppa.com/?p=2404)  

[Dels Applets a Java Web Start amb JNLP](http://canigo.ctti.gencat.cat/blog/2016/04/applets-to-jnlp/)  
Es increíble que algunas cosas recientes / nuevas usen eso, cuando por neutralidad tecnológica se sabe que Chrome y Firefox, mucho más usados que IE, no lo soportan [http://gs.statcounter.com/](http://gs.statcounter.com/)  
Otro enlace reciente que muestra navegadores más usados  
[web-browser-statistics](https://www.ukwebhostreview.com/blog/web-browser-statistics/)  
En 2018 Chrome es el 60% de cuota, Firefox 5% mientras que IE solamente 3%  
Según normativa en BOE [Artículo 38 Ley 40/2015](https://www.boe.es/buscar/act.php?id=BOE-A-2015-10566#a38).  
>3. Cada Administración Pública determinará las condiciones e instrumentos de creación de las sedes electrónicas, con sujeción a los principios de transparencia, publicidad, responsabilidad, calidad, seguridad, disponibilidad, accesibilidad, neutralidad e interoperabilidad......  
>5. La publicación en las sedes electrónicas de informaciones, servicios y transacciones respetará los principios de accesibilidad y uso de acuerdo con las normas establecidas al respecto, estándares abiertos y, en su caso, aquellos otros que sean de uso generalizado por los ciudadanos.  

En 2017 al ir a usar este applet  [http://www.falstad.com/vector3de/](http://www.falstad.com/vector3de/)  indica  
>Huge thanks to  [Bob Hanson and his team](https://chemapps.stolaf.edu/swingjs/site/swingjs/examples/)  for help converting this applet to javascript.  

y en enlace se indica 

>Why bother converting Java applets to JavaScript?  
Java applets for web applications have become outdated and clumsy, requiring the user-install of Java, updated security certificates, and use of specific browers. Thousands of web sites depending upon these applets are no longer functional. By converting Java applets to JavaScript, we can regain the functionality of these resources, returning these web pages to active service to education and science.  This website was created to demonstrate and archive the applets that we have successfully converted to JavaScript without need for extenstive rewriting of the Java code or starting de novo. They are not "ported" to JavaScript. They are not recreated in JavaScript. The JavaScript "applets" are created in parallel with standard Eclipse-based Java development, along with their Java counterparts. In the end, we have the original functional Java applet or stand-alone application, and we have its virtually identical JavaScript counterpart, with all the layout, events, and functionality of the original. One of the most powerful and important features of Java -- reflection (the loading of classes on the fly, only as needed) -- is fully preserved, providing the modularity necessary for complex web application development.

####   Soluciones temporales / parches 
 [How to Use Java, Silverlight, and Other Plugins in Modern Browsers](https://www.howtogeek.com/221720/how-to-use-java-silverlight-and-other-browser-plug-ins-on-windows-10/)  

En Firefox (no funcionará en futuras versiones, pero se puede descargar 52 ESR (Extended Support Release)  
[Firefox 52.0 ESR Release Notes](https://www.mozilla.org/en-US/firefox/52.0esr/releasenotes/) )  

[How To Re-Enable NPAPI Plugin Support (i.e. Java) in Firefox 52](http://www.omgubuntu.co.uk/2017/03/force-enable-firefox-52-npapi-support)  
En 2017 compruebo que últimas versiones 52 ESR ya no permiten parchear, hay que descargar la anterior ESR que es 45.9  
Usar versiones anteriores de navegadores, con instalaciones paralelas, versiones portables ...  
[Mozilla Firefox Portable en sourceforge.nt](https://sourceforge.net/projects/portableapps/files/Mozilla%20Firefox%2C%20Portable%20Ed./)  

Usar appletviewer  [Java SE Documentation: appletviewer](http://docs.oracle.com/javase/8/docs/technotes/tools/windows/appletviewer)  
Ejemplo desde línea de comandos (la url no funciona en navegador):  
appletviewer  [http://www.brainjar.com/java/games/snakepit/](http://www.brainjar.com/java/games/snakepit/)   

Nuevo problema septiembre 2018: 45.9 ESR se actualiza él solo a 60.2 ESR y deja de funcionar java  
[ESR automatic update - URGENT](https://support.mozilla.org/es/questions/1214886)  
Parece que una manera en ubuntu es  
[I need to stop firefox updating!!!](https://support.mozilla.org/es/questions/1198037)  
`sudo apt-mark hold firefox` pero eso dejaría de actualizar firefox desde el sistema, no internamente, y aplicaría a cualquiera  Compruebo que tras hacerlo con una versión de firefox 45.9 instalada en un directorio se sigue actualizando internamente.  
Otra opción en mismo enlace, pero no aplica a firefox 45  
>Type **about:preferences#advanced**\<enter> in the address bar. Under **Advanced, ** Select **Update.   
**[v56+] Type **about:preferences#general**\<enter> in the address bar. Select **Update.  
**Select **Never Check For Updates.**  
Also turn off **Use a background service to install updates**  

Otra opción más [Stop automatic updates](https://support.mozilla.org/es/questions/1003777)  
`about:config to disable automatic updating:` 
`app.update.auto - false` 
`app.update.enabled - false` 
`app.update.silent - false` 

Ojo: esto puede aplicar al firefox no ESR en el mismo linux  

Otra opción vista en 2020 (se usa en algunas simulaciones java de PhET) CheerPJ  
[Cheerpj leaningtech](https://www.leaningtech.com/pages/cheerpj)  

[CheerpJ Revitalizes Legacy Java Applications](https://www.i-programmer.info/news/80-java/13477-cheerpj-revitalizes-legacy-java-applications)  

[Cheerpj applet runner](https://github.com/leaningtech/cheerpj-appletrunner) 

####   Configurar plugin java en el navegador
Hay información en páginas como  [Manual Installation and Registration of Java Plugin for Linux](http://www.oracle.com/technetwork/java/javase/manual-plugin-install-linux-136395)  
Pero como se comenta dado que ni Chrome desde 2015 ni Firefox desde 2017 lo soportan es complicado, hay que usar versiones antiguas o windows.  
Comento para firefox y linux:  
Navegador:  [Firefox 45.9.0 ESR Linux x86_64 es-ES tar.bz2 en ftp.mozilla.org](https://ftp.mozilla.org/pub/firefox/releases/45.9.0esr/linux-x86_64/es-ES/firefox-45.9.0esr.tar.bz2)  
Se trata solamente de descomprimir:  
`bzip2 -d firefox-45.9.0esr.tar.bz2`  
`tar -xvf firefox-45.9.0esr.tar`  
Hay que crear enlace en .mozilla/plugins  
En java 8 linux 64 bits ruta es /usr/lib/jvm/java-8-oracle/jre/lib/amd64/libnpjp2.so  
Es algo similar a plugin visor pdf (fichero /etc/alternatives/nppdf.so), que es un enlace/etc/alternatives/mozilla-javaplugin.so -> /usr/lib/jvm/java-8-oracle/jre/lib/amd64/libnpjp2.so  
Tras arrancar, comprobar mirando en about:plugins, donde aparece algo así:  
```
Java(TM) Plug-in 11.181.2  
Archivo: libnpjp2.so  
Ruta: /usr/lib/jvm/java-8-oracle/jre/lib/amd64/libnpjp2.so 
Versión: 11.181.2  
Estado: Habilitado     
Next Generation Java Plug-in 11.181.2 for Mozilla browsers  
```

###   Descartes
Descartes inicialmente está pensado para matemáticas, pero se usa en recursos de física, similar a Geogebra  
[Descartes, presentación](http://recursostic.educacion.es/descartes/web/presentacion/presentacion_web)  

[Descartes Web 2.0, descripción](http://recursostic.educacion.es/descartes/web/DescartesWeb2.0/descripcionWeb2.0)  
Enlaces a instaladores e instrucciones para Windows XP, Vista y 7; Linux y Mac OS X  

[Instalador del Plug-in de Descartes y DescartesWeb 2.0](http://recursostic.educacion.es/descartes/web/DescartesWeb2.0/)  
Indica en junio 2014  
>AVISO IMPORTANTE: Se ha detectado que con la versión 7.13 de Java (la última que han liberado) se produce un error que impide la ejecución de las escenas. Se está analizando su resolución. Mientras, la única alternativa es desinstalar la versión 7.13 de Java e instalar una anterior, por ejemplo la 7.11.  

Y con la versión java 1.7.0_55 no funciona, indica  
`SecurityException`  
`Bad applet class name`  
Verificación  
[Verificación del funcionamiento del plug-in de DescartesWeb2.0](http://recursostic.educacion.es/descartes/web/DescartesWeb2.0/prueba_instalacion)  
Ejemplo recurso física    
[El movimiento circular. CIDEAD. cc-by-nc-sa](http://agrega.educacion.es/visualizar/es/es_2010091013_9202913/false) 

###   Modellus
Inicialmente para windows de 32 bits (versiones 2.5, 3 y 4) luego portado a linux y Mac como Modellus X.0x (en julio 2014 v0.5RC)  
[http://modellus.co/index.php/es/](http://modellus.co/index.php/es/)  
La versión 0.5RC necesita java 8, y la instalación en linux necesita JavaFx y acceso a la librería libGlass.soEn linux se inicia con ./ModellusX en el directorio de instalación $HOME/ModellusX

##   Visualización Flash
Con el uso de HTML5 el uso de flash es algo a extinguir, en principio a finales de 2020.  
[Flash Player End of Life - community.adobe.com](https://community.adobe.com/t5/Flash-Player/Flash-Player-End-of-Life/td-p/10206952?profile.language=es&pageRefresh=true)  
Ahí se cita
   * Adobe [Flash & the Future of Interactive Content](https://theblog.adobe.com/adobe-flash-update/) 
   * Chrome: [Flash Roadmap - The Chromium Projects](https://www.chromium.org/flash-roadmap)  
   * Firefox: [Firefox Roadmap for Flash End-of-Life ](https://blog.mozilla.org/futurereleases/2017/07/25/firefox-roadmap-flash-end-life/)  and  [Plugin Roadmap for Firefox - Plugins | MDN](https://developer.mozilla.org/en-US/docs/Plugins/Roadmap) 
   * Microsoft [The End of an Era – Next Steps for Adobe Flash](https://blogs.windows.com/msedgedev/2017/07/25/flash-on-windows-timeline/) 
   * Apple [Adobe Announces Flash Distribution and Updates to End](https://webkit.org/blog/7839/adobe-announces-flash-distribution-and-updates-to-end/) 
   * Facebook [Migrating Games from Flash to Open Web Standards on Facebook](https://developers.facebook.com/blog/post/2017/07/25/Games-Migration-to-Open-Web-Standards/)   
Pero hay muchas cosas hechas con flash, y lo habitual era verlas desde navegadores. Lo hecho en flash mientras los navegadores lo soporten puede funcionar, pero hay que asegurarse de configurarlo y no siempre puede funcionar.  
También se pueden instalar versiones antiguas de navegadores.  
Una recopilación de uso configurando navegadores aquí  
[VerFlash](https://fisquiweb.es/VerFlash.htm)  
(en 2020 se actualiza  [FlashVer2.pdf](https://fisquiweb.es/FlashVer/FlashVer2.pdf) )  
(en 2021 se actualiza  [FlashVer4.pdf](https://fisquiweb.es/FlashVer/FlashVer4.pdf) )  

Otra opción más (por probar) es un complemento de firefox  
[http://mozilla.github.io/shumway/](http://mozilla.github.io/shumway/)  
Los ficheros tienen extensión .swf y se pueden descargar y ejecutar localmente (a veces no por dependencias)  
Hay apps para ejecutarlos en en móvil  
[https://play.google.com/store/apps/details?id=com.webgenie.swf.play](https://play.google.com/store/apps/details?id=com.webgenie.swf.play)  
Hay aplicaciones libres/ en linux como gnash, lightspark ...  
[Gnash - wikipedia.org](https://es.wikipedia.org/wiki/Gnash)   

[Running gnash on Ubuntu 20.04 (in Docker with X11 forwarding)](https://raymii.org/s/tutorials/Running_gnash_on_Ubuntu_20.04.html)  

Otro tema aparte son herramientas de conversión de flash a HTML5  
[https://developers.google.com/swiffy/](https://developers.google.com/swiffy/)  
Visto en 2020  
[cheerpxflash leaningtech](https://www.leaningtech.com/pages/cheerpxflash)  
El 31 diciembre 2020 termina oficialmente y se escriben varios artículos  
[twitter InternetMolaba/status/1344594747898392576](https://twitter.com/InternetMolaba/status/1344594747898392576)  
En 2017 @Adobe anuncio que el 31 de diciembre de 2020 dejaría de dar soporte al mítico Flash . El día ha llegado #byebyeFlash . Durante años Flash ha sido el símbolo de la multimedia y el vídeo en Internet.  
[https://mailchi.mp/bonillaware/flash-eol?e=4171108600](https://mailchi.mp/bonillaware/flash-eol?e=4171108600)  

[Hasta siempre, Flash Player: Adobe confirma que dejará de funcionar a finales de 2020 ](https://www.xataka.com/aplicaciones/siempre-flash-player-adobe-confirma-que-dejara-funcionar-a-finales-2020)  

[Adobe dejará de dar apoyo y actualizar Flash Player a partir del 1 de enero: cómo preparar tu PC para el fin de este mítico software](https://www.businessinsider.es/adobe-flash-no-sera-seguro-2021-como-desinstalarlo-pc-781313)  
Hilo de 2018 con la historia  
[twitter InternetMolaba/status/1012458129718685696](https://twitter.com/InternetMolaba/status/1012458129718685696)  
\[1] La web no ha sido siempre colorista y movida. En los comienzos de Internet todo era medio gris y estaba quieto poder ver vídeos, escuchar audios o usar interactivos supuso una revolución gracias en parte, a nuestro protagonista de hoy. Hablemos de Flash. ¡Let’ GO!  
[twitter InternetMolaba/status/1012466182115512320](https://twitter.com/InternetMolaba/status/1012466182115512320)  
\[y 15] Y ya para acabar una batería de memes y chistes de flash en diferentes épocas. La verdad es que visto con el tiempo hemos conseguido cogerle cariño. LARGA VIDA A (el recuerdo de) FLASH   
[RIP Adobe Flash 1996-2020](https://www.microsiervos.com/archivo/internet/rip-adobe-flash)  
Que enlaza  
[Flash Animations Live Forever at the Internet Archive](http://blog.archive.org/2020/11/19/flash-animations-live-forever-at-the-internet-archive/)  
y que cita  
[https://ruffle.rs/](https://ruffle.rs/) 

###   Ruffle
[https://ruffle.rs/](https://ruffle.rs/)  
Es código abierto  
[https://github.com/ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle)  

[twitter educaplus_org/status/1344631779781705729](https://twitter.com/educaplus_org/status/1344631779781705729)  
Ruffle está en desarrollo (AS1 y AS2 sobre el 70% y AS3 solo un 5%) pero es una buena alternativa frente a otras como CheerpX for Flash que son de pago y a un precio prohibitivo (más de 10000 € ANUALES)  
Hay dos formas de utilizarlo. 
El que tiene los recursos adapta su web (es lo que yo he hecho) para que se emulen vía webassembly y el usuario final no tiene que hacer nada o se sirven los swf tal cual y el usuario final instala la extensión para el navegador.  
En 2020 pruebo  
[ruffle_nightly_2020_12_31_firefox.xpi](https://github.com/ruffle-rs/ruffle/releases/download/nightly-2020-12-31/ruffle_nightly_2020_12_31_firefox.xpi)  
y puedo ejecutar sin problemas  
[Iniciación interactiva a la materia, Introducción: Arquímedes y la corona de Hierón](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/indice.htm)  
Puede haber complementos de navegadores que los usen internamentePor ejemplo en Chrome, buscando por ruffle aparece  
[FlashPlayer - SWF to HTML](https://chrome.google.com/webstore/detail/flashplayer-swf-to-html/nodnmpgjlnclahkmgjiinfjklgbbgecg)  
que indica *"The extension uses two open-source Flash to JS libraries (Ruffle and SWF2JS) as its emulation engine."*

###   Macromedia flashplayer como ejecutable windows portable
Un único ejecutable portable: no hay que instalar. Al abrirlo se elige el .swf a ejecutar  
[flashplayer_32_sa_debug.exe en macromedia.com](https://fpdownload.macromedia.com/pub/flashplayer/updaters/32/flashplayer_32_sa_debug.exe)  
Funciona en Linux con wine  
Por su interés guardo una copia local del fichero en [drive.fiquipedia media/flash](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/flash/flashplayer_32_sa_debug.exe)


###   Basilisk portable con Flash 
 [Basilisk Portable With Flash Player - archive.org](https://archive.org/details/basilisk-portable-with-flash)  
