---
aliases:
  - /home/pruebas-premios-extraordinarios/
---

# Pruebas premios extraordinarios

En 2019 están publicados en  [https://www.educa2.madrid.org/web/innovacioneducativa/actividadesculturalesyartisticas](https://www.educa2.madrid.org/web/innovacioneducativa/actividadesculturalesyartisticas)  

De momento juntos Premios Extraordinarios Educación Secundaria Obligatoria y Premios Extraordinarios Bachillerato, si crece se pueden separar.

En Bachillerato sí hay examen de Física y de Química, similares a EvAU, ver [convocatoria Premios Extraordinarios de Bachillerato (2018-2019)](https://gestionesytramites.madrid.org/cs/Satellite?c=CM_ConvocaPrestac_FA&cid=1354770326157&noMostrarML=true&pageid=1142687560411&pagename=ServiciosAE%2FCM_ConvocaPrestac_FA%2FPSAE_fichaConvocaPrestac&vest=1142687560411) 
   *  [enlace a los exámenes de la convocatoria correspondiente al curso 2017/2018](https://www.educa2.madrid.org/web/educamadrid/principal/files/8f9c96d3-2c95-41d6-a9f0-548131b55380/PREMIOS/Bachillerato/Ex%C3%A1menesPremiosExtraordinariosBachillerat2018.zip?t=1531988791731) 
   *  [enlace a los exámenes de la convocatoria correspondiente al curso 2018/2019](https://www.educa2.madrid.org/web/educamadrid/principal/files/8f9c96d3-2c95-41d6-a9f0-548131b55380/PREMIOS/Bachillerato/Ex%C3%A1menesPPEEBtoWeb.zip?t=1566895852056) 
   *  [enlace a las pruebas orales de los exámenes de la convocatoria correspondiente al curso 2018/2019](https://www.educa2.madrid.org/web/educamadrid/principal/files/8f9c96d3-2c95-41d6-a9f0-548131b55380/PREMIOS/Bachillerato/PruebasOrales.zip?t=1566896076531)
   
    
[Convocatoria Premios Extraordinarios de Educación Secundaria Obligatoria (2018-2019)](https://gestionesytramites.madrid.org/cs/Satellite?cid=1354779395705&c=CM_ConvocaPrestac_FA&noMostrarML=true&pageid=1142687560411&pagename=ServiciosAE%2FCM_ConvocaPrestac_FA%2FPSAE_fichaConvocaPrestac&vest=1142687560411) 
   *  [enlace a los exámenes de la convocatoria correspondiente al curso 2017/2018](https://www.educa2.madrid.org/web/educamadrid/principal/files/8f9c96d3-2c95-41d6-a9f0-548131b55380/PREMIOS/ESO/Ex%C3%A1menesPremiosExtraordinariosESO_2018.zip?t=1535991564392) 
   *  [enlace a los exámenes de la convocatoria correspondiente al curso 2018/2019](https://www.educa2.madrid.org/web/educamadrid/principal/files/8f9c96d3-2c95-41d6-a9f0-548131b55380/PREMIOS/ESO/ExPPEESOWeb2019.zip?t=1566896531920) 
  
