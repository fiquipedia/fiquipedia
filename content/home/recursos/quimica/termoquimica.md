
# Termoquímica

Relacionado con  [termodinámica](/home/recursos/termodinamica) , pero más simplificado y centrado en reacciones químicas.  
Se trata en 1º Bachillerato (antes de LOMCE estaba en 2ºBachillerato, y hay ejercicios asociados en  [PAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) )  

[TERMOQUÍMICA. LIBRO PRINCIPAL.pdf - unaquimicaparatodos](https://unaquimicaparatodos.com/wp-content/uploads/2017/01/6.-TERMOQUI%CC%81MICA.-LIBRO-PRINCIPAL.pdf)  

[1º Bachillerato Internacional. Ries Jovellanos EJERCICIOS DE TERMOQUÍMICA.](https://matematicasiesoja.files.wordpress.com/2015/03/termoquimica-1303210110.pdf)  

[Tema 3. Termoquímica. Ejercicios selectividad (96/97 a 08/09)- upo.es](https://www.upo.es/cms1/export/sites/upo/ponencia_quimica/pruebas/documentos/TERMOQUIMICA.doc)  


[QUÍMICA GENERAL, PROBLEMAS RESUELTOS, Dr. D. Pedro A. Cordero Guerrero, TERMOQUÍMICA](http://matematicasjjp.webcindario.com/termoquimica_resueltos_1.pdf)  

[Ejercicios de termoquímica con solución - elortegui.org](http://ciencia.elortegui.org/datos/1BACHFYQ/ejer/resueltos/Ejercicios%20termoquimica%20con%20solucion.pdf)  

[Examenes termoquímica 1º Bachillerato - profesor10demates](https://www.profesor10demates.com/2016/02/examenes-termoquimica-1-bachillerato.html)  
 
 [Endo o exotérmico](https://www.elcel.org/es/experiencias/endo-o-exotermico/)  
 Es un juego en el que los alumnos deben conocer el carácter endo y exotérmico de algunas reacciones químicas, como la electrólisis del agua o la formación de ozono, y tener una gran agilidad mental para colocar sus fichas antes que el resto de jugadores.  
 
 [twitter Biochiky/status/1017681028171870208](https://twitter.com/Biochiky/status/1017681028171870208)  
 [![](https://pbs.twimg.com/media/Dh-HgDjX4AE8BDJ.jpg "") ](https://pbs.twimg.com/media/Dh-HgDjX4AEsUo-?format=jpg)  

[EXPLOSIVOS, PROPELENTES Y PIROTECNIA, NIKOLA B. ORBOVIC](https://www.acapomil.cl/postgrado/extension/pdf/2_EXPLOSIVOS,%20PROPELENTES%20Y%20PIROTECNIA_.pdf)  
2.4776KNO3 + 4C + S = 3.194CO2 + 0.806CO2 + 1.2388N2 + 0.2388K2O +K2S + calor  
2CH3C6H2(NO2)3= 12CO + 2CH4+ H2+ 3N2 + calor  

[La química de la pólvora](https://www.uv.es/uvweb/master-chemistry/en/master-s-degree-chemistry/la-quimica-de-la-polvora-1285949129052/GasetaRecerca.html?id=1285960614806)  
la ecuación simplificada de la combustión de la pólvora, sería:  
10KNO3 + 8C +3S → 2K2CO3 + 3K2SO4 + 6CO2 + 5N2  

[Petardos, cohetes y mucha químicaPetards, coets i molta química](https://publicacions.iec.cat/repository/pdf/00000216/00000093.pdf)  
energía liberada por gramo de pólvora (1 300 J)  Firecrackers, rockets and a lot of chemistry  
Fernando Ignacio de Prada Pérez de Azpeitia / IES Las Lagunas. Rivas (Madrid)  
 
Los ficheros se veían antes anexados a esta página, ahora se ven en
[drive.fiquipedia recursos/quimica/termoquimica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/quimica/termoquimica)

[Periodic Graphics: Fuels for space travel. Chemical educator and Compound Interest blogger Andy Brunning explores the fuels that propel spacecraft. ](https://cen.acs.org/physical-chemistry/astrochemistry/Periodic-Graphics-Fuels-space-travel/100/i43)
![](https://acs-h.assetsadobe.com/is/image//content/dam/cen/100/43/WEB/10043-feature4-graphic.jpg/?$responsive$&wid=700&qlt=90,0&resMode=sharp2)  

[El combustible en la Fórmula 1; el cianuro en el consomé](https://www.motorpasion.com/formula1/el-combustible-en-la-formula-1-el-cianuro-en-el-consome)  
> Antes de 1989, el Reglamento Técnico de la Fórmula 1 permitía utilizar cualquier tipo de combustibles, así que los proveedores tenían libertad para mezclar sustancias químicas capaces de generar detonaciones desmedidas. Algunas combinaciones eran tan osadas que incluían carburantes de avión, reactores y cohetes. En varias sesiones clasificatorias, algunos motores Honda RA167E de 6 cilindros y BMW M12/13 de 4 cilindros llegaron a utilizar fusiones químicas tan poderosas que, tras culminar las tandas, fueron desarmados y depurados de inmediato para evitar su destrucción. El carburante resultaba tan abrasivo que literalmente deshacía los elementos internos del motor. Sin duda, fue una época inolvidable ya que se podía observar a un propulsor de 1500 cc generar más de 1200 Hp, girar a más de 18000 rpm e impulsar un monoplaza a más de 330 kph.  
in embargo, los vapores producto de la combustión, resultaban muy tóxicos tanto para quienes manipulaban las mezclas como para los espectadores. Así que en 1989, y una vez eliminado el sistema turbo, la FIA también decidió reglamentar el carburante. Desde esa temporada, y hasta nuestros días, la gasolina utilizada en la Fórmula 1 ha experimentado una serie de modificaciones para cumplir con las normativas sobre la emisión de gases contaminantes. De allí que el combustible empleado en la Fórmula 1 y el comercial mantengan **una semejanza de aproximadamente un 99% en su composición química. Pero ese 1% restante resulta tan trascendente como una gota de cianuro en una cazuela de consomé.**  

## Tablas de valores termoquímicos

Son datos útiles para plantear problemas. A veces citados en otros enlaces junto a reacciones.  
  
[Búsqueda de datos de especies por Fórmula Química. National Institute of Standards and Tecnology - nist.gov](https://webbook.nist.gov/chemistry/form-ser/)  
  
[Anexo:Tablas de entalpías de formación de compuestos - wikipedia](https://es.wikipedia.org/wiki/Anexo:Tablas_de_entalp%C3%ADas_de_formaci%C3%B3n_de_compuestos)  

Algunas constantes (Selected thermochemical values, Table 1. Standard molar Enthalpy change of formation at 25 °C, Table 2. Standard molar Gibbs free energy change of formation at 25 °C)  
[Selected thermochemical values - vaxasoftware](http://www.vaxasoftware.com/doc_eduen/qui/dtermoq.pdf)  
Licenciamiento: esta tabla está incluida dentro de unos  [recursos que se indican libres / abiertos para educación](/home/recursos/recursos-educativos-abiertos) . No garantizan exactitud.  

[Compendio de Propiedades. Tabla de Entalpía de Formación, Energía Libre de Gibbs y Entropía de Formación de Compuestos Inorgánicos](https://drjvazque.files.wordpress.com/2020/03/tabla-termoquimica-4.pdf)  
EIQ, BUI, TQ Fabio Germán Borgogno. Neuquén, Neuquén, Argentina. Versión Primera - Junio de 2010.

[STANDARD THERMODYNAMIC PROPERTIES OF CHEMICAL SUBSTANCES](https://www.update.uu.se/~jolkkonen/pdf/CRC_TD.pdf)  

[Propiedades Termodinámicas de Substancias Seleccionadas - hyperphysics](http://hyperphysics.phy-astr.gsu.edu/hbasees/Tables/therprop.html)  

 [Simulación matemática del funcionamiento de un quemador acoplado a un secador](https://riunet.upv.es/bitstream/handle/10251/103843/Castro%3BTom%C3%A1s%3BOrtol%C3%A1%20-%20Simulaci%C3%B3n%20matem%C3%A1tica%20del%20funcionamiento%20de%20un%20quemador%20acoplado%20a%20un%20secador.pdf?sequence=1)  
 Tabla 1. Tabla con la fórmula química, reacción de combustión, masa molecular (Mr), calor específico (Cp), calor latente de combustión (ΔG) y temperatura de llama (T llama) \[4] \[5]. *   
 Temperaturas que hay que hallar experimentalmente.  
 Celulosa C6H10O5  
 Hemicelulosa C6H10O6  
 Lignina C31H34O11  
 Aceite / Ácido oléico C18H34O2  
 Fructosa C6H12O6 (misma fórmula que glucosa)  
 
 La entalpía de combustión de glucosa es aproximadamente -2800 kJ/mol, que se puede aproxima a la variación de energía de Gibbs de la tabla pasada a kJ/mol. Por ejemplo para fructosa 14800*180/1000=2664  
   
   
# Energías de enlace

[7.5: Fortaleza de los enlaces iónicos y covalentes - libretexts.org](https://espanol.libretexts.org/Quimica/Libro%3A_Qu%C3%ADmica_General_(OpenSTAX)/07%3A_Enlace_Quimico_y_Geometria_Molecular/7.5%3A_Fortaleza_de_los_enlaces_ionicos_y_covalentes)     
> Tabla 7.5.1: Energías de enlace (kJ/mol)  

# Standard

Sobre condiciones normales, ver [Condiciones - fikasi](https://sites.google.com/view/fikasi/condiciones)  
Ahí se cita que RAE define [condiciones normales](https://dle.rae.es/condici%C3%B3n#Ckv0h1e)  
> 1. f. pl. Fís. y Quím. Circunstancias estándares, establecidas por convenio, que se toman como referencia para definir el estado físico de un cuerpo y que corresponden a cero grados Celsius y una atmósfera de presión.  

[Quantities, Units and Symbols in Physical Chemistry - IUPAC ](https://iupac.org/wp-content/uploads/2019/05/IUPAC-GB3-2012-2ndPrinting-PDFsearchable.pdf)  
>(12) The symbol <sup>⦵</sup> or ◦ is used to indicate standard. They are equally acceptable. Definitions of standard states are discussed in Section 2.11.1 (iv), p. 61  

>(iv) Standard states \[1.j] and \[81]  
The standard chemical potential of substance B at temperature T, μ B <sup>⦵</sup>(T ), is the value of the chemical potential under standard conditions, specified as follows. Three differently defined standard states are recognized.  

Asociado a electroquímica  
> (17) The standard potential is the value of the equilibrium potential of an electrode under standard conditions.   

## Ley de Hess

Aquí me centro en cálculo de entalpías de una reacción en función de otras reacciones que no son de formación, un tipo de ejercicios que a veces es escaso, ya que buscando ejercicios de "ley de Hess" suelen limitarse al caso de cálculo entalpías de reacción a través de entalpías de formación que solo es un caso concreto de la ley de Hess.  
  
Los ejercicios pueden estar en otras recopilaciones. Se ponen enlaces concretos  

[EvAU Madrid Química](https://www.fiquipedia.es/home/pruebas/pruebasaccesouniversidad/pau-quimica/): 2015-Sep-B4-d, 2012-Sep-A4  

Ejercicios 11 y 12 de [TERMOQUÍMICA. LIBRO PRINCIPAL.pdf - unaquimicaparatodos](https://unaquimicaparatodos.com/wp-content/uploads/2017/01/6.-TERMOQUI%CC%81MICA.-LIBRO-PRINCIPAL.pdf)  

Ejercicio 9 de [Ejercicios de termoquímica con solución - elortegui.org](http://ciencia.elortegui.org/datos/1BACHFYQ/ejer/resueltos/Ejercicios%20termoquimica%20con%20solucion.pdf)  


