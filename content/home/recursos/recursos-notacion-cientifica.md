
# Recursos notación científica
  
 En general la notación científica se entiende como la expresión de constantes y resultados mediante potencias de 10.   
 Incluyo conjuntamente aspectos relacionados como el separador decimal. Además de las definiciones básicas, pongo enlaces a vídeos.  
Puede haber  [recursos asociados a la medida](/home/recursos/recursos-medida)   
  
Historias de Nuestra HistoriaCómo un ataque pirata en 1794 impidió que los Estados Unidos adoptaran el sistema métrico  
 Por Félix Casanova [Cómo un ataque pirata en 1794 impidió que los Estados Unidos adoptaran el sistema métrico | Historias de nuestra Historia](https://hdnh.es/pirata-estados-unidos-sistema-metrico/) 

## Notación científica
 Se suele hablar de "notación científica normalizada", que implica expresar cualquier cifra como mantisa · 10n, siendo la mantisa un número de valor absoluto entre 1 y 10, y el exponente un número entero (positivo o negativo)  
 Ver  [Scientific notation - Wikipedia](http://en.wikipedia.org/wiki/Scientific_notation)   

[twitter ivanquifis/status/1712761152143331485](https://twitter.com/ivanquifis/status/1712761152143331485)  
Comparto actividad de NOTACIÓN CIENTÍFICA para 2º o 3º ESO, con dos ejercicios, uno con coevaluación, y otro gamificado en formato puzzle.  
📚 Inspirado en:  [Pasatiempos y juegos en clase de matemátias - anagarciaazcarate.wordpress](https://anagarciaazcarate.wordpress.com/about/)  
🔗 Descarga: [carpeta Google Drive Ficha notación científica](https://drive.google.com/drive/folders/1P5GErMJvY_OssYZ5AKQFmJyYm10gkCnn)  

  
Enlace interesante  
 Scientific Notation in Everyday Life  
 [Question Corner -- Scientific Notation in Everyday Life](http://www.math.toronto.edu/mathnet/plain/questionCorner/scinot.html)   
University of Toronto Mathematics Network, Question Corner and Discussion Area  
Se comenta el enlace de la notación científica con la precisión.  

[Notación científica. Serie Investigando | Investigando: La actividad científica - intef.es](https://descargas.intef.es/cedec/proyectoedia/fisica_quimica/contenidos/actividad_cientifica/notacin_cientfica.html)  

Incluye enlace a actividad "Tamaño en línea"

[CARTAS PARA LA ACTIVIDAD ‘TAMAÑO DE LÍNEA’ (pdf) - intef.es](https://descargas.intef.es/cedec/proyectoedia/fisica_quimica/contenidos/actividad_cientifica/tamano_en_linea_cartas.pdf)  

  
 [nc1c.swf](http://www.genmagic.org/mates2/nc1c.swf)   
Apuntes en flash  
 Roger Rey y Fernando romero  
  
 [971a6497-4644-4c2d-82de-945f97ada2fd](http://procomun.educalab.es/comunidad/procomun/recurso/notacion-cientifica/971a6497-4644-4c2d-82de-945f97ada2fd)   
Notación científica, Descartes, Rita Jiménez Igea, cc-by-nc-sa  
 [Notación científica  | Educaplus](http://www.educaplus.org/game/notacion-cientifica)   

## [Escalas](/home/recursos/escalas)  

## **Separador decimal**
Se incluye aquí un pequeño documento que resume el uso habitualmente incorrecto del separador decimal, con referencias.   
Normalmente muchos profesores lo usan y lo enseñan mal por tradición y simplemente desconocimientoPendiente ampliar y detallar con tema separador de millares, que se cita [Separador de millares - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Separador_de_millares)   
 [ISO/IEC 80000 - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/ISO/IEC_80000)  [ISO/IEC 80000 - Wikipedia](https://en.wikipedia.org/wiki/ISO_80000-1)  [](http://www.fundeu.es/consulta/miles-6498/)   
  
[Separador decimal.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-notacion-cientifica/Separador%20decimal.pdf)  


