
# Zinc

Se puede escribir zinc o cinc  
[Zinc - RAE](https://dle.rae.es/zinc)  

[twitter RAEinforma/status/1683449494862536705](https://twitter.com/RAEinforma/status/1683449494862536705)  
Consultas de la semana | ¿Se escribe «no hay tu tía» o «no hay tutía»?  
La forma canónica es «no hay tutía», pero se admite la variante «no hay tu tía». Tienen el sentido figurado de ‘no hay remedio’, porque la tutía o atutía (óxido de zinc) se usaba en ungüentos medicinales.  

[tutía - RAE](https://dle.rae.es/tut%C3%ADa)  

El zinc es asociable a la pila Daniel en redox.  

