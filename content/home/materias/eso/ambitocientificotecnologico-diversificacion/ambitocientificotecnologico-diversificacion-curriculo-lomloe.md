# Currículo Ámbito Científico Tecnológico Diversificación (LOMLOE)

LOMLOE implanta en 3º ESO en curso 2022-2023  
En Madrid se desarrola inicialmente 6 septiembre 2022 en Circular de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial en relación con la organización y el currículo de los ámbitos del programa de diversificación curricular de la Educación Secundaria Obligatoria durante el curso 2022-2023.  
CSV 1202871551640335491638  

En febrero 2023 se publica [ORDEN 190/2023, de 30 de enero, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se desarrolla la organización y el currículo del programa de diversificación curricular de la Educación Secundaria Obligatoria en la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/02/07/BOCM-20230207-7.PDF)  

## Introducción 

 El currículo del ámbito Científico-tecnológico del programa de diversificación curricular de la Comunidad de Madrid integra aquellos aspectos básicos correspondientes a los currículos de las diferentes materias de la Educación Secundaria Obligatoria que lo conforman: Matemáticas, Biología y Geología, Física y Química y Tecnología y Digitalización. Con ese diseño se pretende facilitar que el alumnado que cursa un programa de diversificación curricular adquiera, a través de las competencias específicas de este ámbito, las competencias básicas de la etapa educativa. De esta manera, el alumnado del programa de diversificación curricular podrá obtener el título de Graduado en Educación Secundaria Obligatoria, lo que facilitará su acceso a una formación académica posterior, su integración en la sociedad y, llegado el momento, en el mundo laboral. Además, y gracias al enfoque eminentemente práctico y de carácter instrumental recogido en los contenidos del presente ámbito, se dotará al alumno de un bagaje cultural científico y tecnológico adecuado para enfrentarse a situaciones de la vida cotidiana de un modo formado y crítico.

Los contenidos del ámbito Científico-tecnológico han sido agrupados en una serie de bloques, manteniéndose la estructura formal de los currículos de las materias de la Educación Secundaria Obligatoria que lo integran. Dicha organización permitirá al profesorado, por un lado, abordar los contenidos de una manera funcional, proporcionándole la flexibilidad necesaria para identificar conexiones entre los mismos y, por otro, establecer el puente necesario entre el ámbito Científico-tecnológico y las diferentes materias del correspondiente curso en la Educación Secundaria Obligatoria, ayudándole en la planificación de sus clases. Además, en ambos cursos, se ha creado un bloque de carácter transversal, “Proyecto y destrezas científicas”, en el que se recogen una serie de destrezas imprescindibles para comprender cómo se construye la ciencia, y que son comunes a las diferentes materias que constituyen este ámbito. Con su inclusión se pretende fomentar la aproximación experimental a los contenidos, así como dotar al profesorado de una guía útil para elaborar proyectos.

Debe señalarse que esta distribución y organización en bloques de contenidos no impone ninguna temporalización específica en su tratamiento en el aula, aunque dentro de cada bloque, como se verá, se sugiere un orden en la propia redacción de este currículo, a modo de orientación. En esta secuenciación se ha tenido en cuenta el carácter instrumental de los contenidos estudiados que, en ocasiones, por su propia naturaleza, deben abordarse progresivamente. Así, por ejemplo, carece de sentido la resolución de problemas en contextos de la vida cotidiana que impliquen la resolución de ecuaciones de segundo grado, si previamente no se ha reflexionado sobre la resolución de las mismas y el alumno no ha realizado un análisis crítico de sus soluciones.

Al término de este preámbulo, y tras la descripción de los bloques de contenidos que configuran el ámbito Científico-tecnológico, se incluye un ejemplo de actividad que involucra contenidos de varias materias. Esta propuesta, que puede utilizarse por el profesorado como modelo de trabajo en el aula, abarca contenidos matemáticos, físicos y tecnológicos, y permite que estos sean introducidos en el aula a través de un proyecto sencillo.

Los contenidos matemáticos del ámbito Científico-tecnológico se articulan en seis bloques. El primero de ellos, “Números y operaciones”, se caracteriza por la aplicación del conocimiento sobre numeración y cálculo en distintos contextos, y por el desarrollo de habilidades y modos de pensar basados en la comprensión, la representación y el uso flexible de los números y las operaciones. El bloque “Medida y geometría” se centra en la comprensión de los atributos mensurables de los objetos del mundo natural, entendiendo y eligiendo en cada situación las unidades más adecuadas, mientras que en el bloque “Geometría en el plano y el espacio” se abordan aspectos geométricos de nuestro mundo que, a través de ecuaciones y teoremas, se vuelven más comprensibles. En “Álgebra” se han reunido un conjunto de herramientas y procedimientos imprescindibles para trabajar y dotar al alumnado del lenguaje propio en el que se comunican las ideas matemáticas en particular y científicas en general. El bloque denominado “Estadística” comprende el análisis y la interpretación de datos, la elaboración de conjeturas y la toma de decisiones a partir de la información obtenida tras el estudio estadístico de los mismos. Por último, se ha incluido un bloque llamado “Actitudes y aprendizaje” en el que se integran una serie de destrezas y actitudes necesarias para entender y manejar las emociones, establecer y alcanzar metas, y aumentar la capacidad del alumnado para tomar decisiones responsables e informadas. Los contenidos y habilidades a que se hacen referencia en este bloque, dado su carácter transversal, deberían ser desarrollados a lo largo de todo el ámbito.

En Biología y Geología los contenidos del ámbito se han dividido, para el primer curso, en tres bloques: “Geología”, “Cuerpo humano” y “Salud y hábitos saludables”, y para el segundo, en cuatro: “La célula”, “Genética y evolución” “Geología” y “El planeta Tierra”.

Por su parte, los contenidos de la materia Física y Química se han agrupado en cuatro bloques con idénticas denominaciones para los dos cursos en los que se divide el ámbito. “La materia” es el primer bloque objeto de estudio en el cual se pretende abordar la estructura atómica de la materia, así como la nomenclatura orgánica e inorgánica. En el segundo bloque, “El cambio”, se estudian las reacciones químicas y los diferentes factores que influyen en las mismas. “La interacción” es el tercer bloque y aborda el análisis del movimiento de los cuerpos en relación con las fuerzas que actúan sobre ellos. Por último, el tercer bloque “La energía”, recoge las distintas formas y aplicaciones de la energía, el uso responsable de la misma y una introducción a la naturaleza eléctrica de la materia.

Finalmente, se ha establecido una correspondencia entre los contenidos de la materia de Tecnología y Digitalización del ámbito Científico-tecnológico y los contenidos de la materia del mismo nombre incluidos en el tercer curso de la Educación Secundaria Obligatoria. Con el desarrollo de este bloque se ha pretendido fomentar entre el alumnado el uso responsable de herramientas multimedia, así como el desarrollo formado de contenidos digitales, encaminados a la mejora y desarrollo de los proyectos planteados en el aula. De esta manera, y dada su relevancia en el mundo actual, se pretende contribuir a reforzar la competencia digital del alumnado.

Los contenidos que integran el bloque de Tecnología y Digitalización se han organizado en torno a cuatro bloques. El primer bloque, llamado “Proceso de resolución de problemas” aborda el desarrollo de habilidades y métodos que permiten avanzar desde la identificación y formulación de un problema técnico, hasta la solución constructiva del mismo y, todo ello, a través de un proceso planificado que busca la optimización de recursos y de soluciones. El bloque “Comunicación y difusión de ideas” implica el conocimiento de herramientas digitales para publicar y comunicar información relativa a proyectos; mientras que en el “Pensamiento computacional” se introduce al alumnado en la inteligencia artificial y la robótica. Finalmente, en el último bloque, denominado “Digitalización del entorno personal de aprendizaje”, se aborda el tema de la transmisión de datos y las herramientas de edición y creación de contenidos multimedia.

La metodología a seguir por el profesorado deberá adaptarse a cada grupo de alumnos, rentabilizándose al máximo los recursos tecnológicos disponibles en el aula. El aprendizaje, como ya se ha mencionado con anterioridad, debe plantearse de un modo esencialmente práctico, valorándose positivamente el diseño de actividades que integren contenidos de cada una de las materias que conforman este currículo, la correcta aplicación de los conceptos científicos estudiados y el uso correcto de las herramientas matemáticas pertinentes.

A modo de actividad, y con la finalidad de proporcionar al profesorado algún ejemplo práctico en el que se integren y trabajen de una manera coordinada los diferentes contenidos presentes en este currículo, se plantea la posibilidad de que el alumnado construya en grupos un cohete de agua. Para la realización de esta propuesta se necesitaría una botella de plástico llena de agua y cerrada con una válvula simple. La introducción de presión en la misma mediante una bomba de las empleadas para inflar las ruedas de una bicicleta hará que, en cierto momento, y respondiendo a la tercera ley de Newton, el prototipo de cohete despegue. Esta sencilla experiencia permite estudiar de un modo práctico las leyes de Newton y el lanzamiento parabólico. Además, y gracias a los conocimientos tecnológicos que vaya adquiriendo el alumnado, podrán implementarse cambios en el diseño del cohete tales como la incorporación de ojivas o alerones de diferentes formas y materiales, comprobándose cómo repercute su inclusión en la mejora del prototipo. En función del alumnado y de los recursos de que se dispongan, se podría realizar el diseño de estos elementos a través del uso de programas informáticos, llegándose incluso a la impresión 3D de los mismos. El vuelo también puede estudiarse matemáticamente, ya que el tiempo que permanece en el aire el cohete, gracias a las ecuaciones del movimiento estudiadas dentro de los contenidos de la materia de Física y Química, permite deducir la altura alcanzada por el dispositivo gracias a una sencilla ecuación de segundo grado.

Esta actividad, realizada en grupo y presentada por cada equipo al resto de sus compañeros, estimulará su curiosidad y proporcionará al alumnado hábitos de trabajo y herramientas necesarias para defender sus argumentos frente a los del resto, permitiéndoles situarse y reaccionar frente a problemas que se encontrarán más adelante en ciertas situaciones de la vida y en el mundo laboral.

## Competencias específicas.

 1. Reconocer los motivos por los que ocurren los principales fenómenos naturales, y ser capaz de explicarlos en términos de las leyes y teorías científicas adecuadas, utilizando con propiedad el lenguaje matemático y científico, y poniendo en valor la contribución de la ciencia en la cultura y el desarrollo de la sociedad.

El aprendizaje de las ciencias desde la perspectiva integradora del enfoque STEM tiene como base el reconocimiento de los fundamentos científicos de los fenómenos que ocurren en el mundo real. El alumnado debe ser competente para reconocer los porqués científicos de lo que sucede a su alrededor e interpretarlo a través de las leyes y teorías correctas. Esto posibilita que el alumnado establezca relaciones constructivas entre la ciencia, su entorno y la vida cotidiana, lo que le permitirá desarrollar habilidades para hacer interpretaciones de otros fenómenos diferentes, aunque no hayan sido estudiados previamente. Al adquirir esta competencia específica, se consigue despertar en ellos un interés por la ciencia y por la mejora del entorno y de la calidad de vida, así como se aprende a valorar el papel instrumental que desempeñan las matemáticas en el desarrollo de la ciencia.

Aspectos tan importantes como la conservación del medio ambiente o la preservación de la salud tienen una base científica, y comprender su explicación y sus fundamentos básicos otorga al alumnado un mejor entendimiento de la realidad, lo que favorece una participación activa en el entorno educativo y profesional como ciudadanos implicados y comprometidos con la sociedad.

Esta competencia específica se conecta con los siguientes descriptores recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo: CCL1, STEM1, STEM2, STEM4, CD1, CPSAA4, CC3.

2. Interpretar y modelizar en términos científicos problemas y situaciones de la vida cotidiana, aplicando diferentes estrategias, formas de razonamiento, herramientas matemáticas, tecnológicas y el pensamiento computacional, para hallar soluciones a los mismos, analizando críticamente su validez y su significado.

El razonamiento y la resolución de problemas se consideran destrezas esenciales no solo para el desarrollo de actividades científicas o técnicas, sino para cualquier otra actividad profesional, por lo que deben ser dos componentes fundamentales en el aprendizaje de las ciencias y de las matemáticas. Para resolver un problema, es esencial realizar una lectura atenta y comprensiva, interpretar la situación planteada, extraer la información relevante y transformar el enunciado verbal en una forma que pueda ser resuelta mediante procedimientos previamente adquiridos. Este proceso se complementa con la utilización de diferentes formas de razonamiento, tanto deductivo como inductivo, para obtener la solución. Para ello son necesarias la realización de preguntas adecuadas y la elección de estrategias que implican la movilización de conocimientos, la utilización de procedimientos y algoritmos. El pensamiento computacional juega también un papel central en la resolución de problemas ya que comprende un conjunto de formas de razonamiento como la automatización, el pensamiento algorítmico o la descomposición en partes. El análisis de las soluciones obtenidas potencia la reflexión crítica sobre su validez tanto desde un punto de vista estrictamente matemático como desde una perspectiva global.

El desarrollo de esta competencia fomenta un pensamiento más diverso y flexible, mejora la destreza del alumnado para resolver problemas en diferentes contextos, amplía la propia percepción sobre las ciencias y las matemáticas y enriquece y consolida los conceptos básicos, lo que repercute en un mayor nivel de compromiso, el incremento de la curiosidad y la valoración positiva del proceso de aprendizaje, favoreciendo su integración e iniciación profesional.

Esta competencia específica se conecta con los siguientes descriptores recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo: CCL2, STEM1, STEM2, CD1, CD2, CPSAA4, CE1.

3. Utilizar los métodos científicos realizando indagaciones y participando activamente en proyectos individuales o en equipo, destinados a desarrollar los razonamientos propios del pensamiento científico y a mejorar las destrezas en el uso de las metodologías científicas.

El desempeño de destrezas científicas conlleva un dominio progresivo en el uso de las metodologías propias del trabajo científico para llevar a cabo investigaciones e indagaciones sobre aspectos clave del mundo natural. Para el alumnado, el desarrollo de esta competencia específica supone alcanzar la capacidad de realizar observaciones sobre el entorno cotidiano, formular preguntas e hipótesis acerca de él y comprobar la veracidad de las mismas mediante el empleo de la experimentación, utilizando las herramientas y normativas que sean más convenientes en cada caso.

Además, desenvolverse en el uso de las metodologías científicas supone una herramienta fundamental en el marco integrador del trabajo colaborativo por proyectos propios de la labor científica. Por este motivo es importante que el alumnado desarrolle esta competencia específica a través de la práctica para que sea capaz de conservar las actitudes aprendidas tanto en sus futuros estudios como en el ejercicio de su profesión.

Esta competencia específica se conecta con los siguientes descriptores recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo: STEM1, STEM2, STEM3, CD1, CD3, CPSAA4, CPSAA5, CE1.

4. Analizar los efectos de determinadas acciones cotidianas sobre la salud, el medio natural y social, basándose en fundamentos científicos, para valorar la importancia de los hábitos que mejoran la salud individual y colectiva, evitan o minimizan los impactos medioambientales negativos y son compatibles con un desarrollo sostenible.

La actividad humana ha producido importantes alteraciones en el entorno con un ritmo de avance significativo. Algunas de estas alteraciones, podrían poner en grave peligro algunas actividades humanas esenciales, entre las que destaca la producción de alimentos.

Asimismo, el modelo de desarrollo económico actual ha favorecido la adopción de ciertos hábitos perjudiciales (como la dieta rica en grasas y azúcares, el sedentarismo y la adicción a las nuevas tecnologías), cada vez más comunes entre los ciudadanos del mundo desarrollado. Esto ha dado lugar a un aumento de la frecuencia de algunas patologías que constituyen importantes problemas de la sociedad actual.

Sin embargo, determinadas acciones y hábitos saludables y sostenibles (alimentación sana, ejercicio físico, interacción social, consumo responsable…) pueden contribuir a la preservación y mejora de la salud individual y colectiva y a frenar las tendencias medioambientales negativas anteriormente descritas. Por ello, es imprescindible para el pleno desarrollo del alumnado como ciudadano que este conozca y aplique los fundamentos científicos que justifican un estilo de vida saludable y sostenible.

Esta competencia específica se conecta con los siguientes descriptores recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo: STEM5, CD4, CPSAA2, CC4.

5. Interpretar y transmitir de un modo adecuado información y datos científicos, contrastando previamente su veracidad, utilizando correctamente el lenguaje verbal y el vocabulario científico y matemático necesario, con la finalidad de adquirir y afianzar conocimientos relativos al entorno natural y social.

En los ámbitos científicos, así como en muchas otras situaciones de la vida, existe un exceso de información que necesita ser seleccionada, interpretada y analizada antes de ser utilizada con unos fines concretos. En muchas ocasiones, la información de carácter científico se proporciona en formatos muy diversos, como enunciados, gráficas, tablas, modelos o diagramas, que es necesario comprender para trabajar de forma adecuada en la ciencia. Asimismo, el lenguaje matemático otorga al aprendizaje de la ciencia una herramienta potente de comunicación global, y los lenguajes específicos de las distintas disciplinas científicas se rigen por normas que es necesario comprender y aplicar.

Puesto que este tipo de comunicación se produce dentro y fuera de los ámbitos científicos, el alumnado debe ser competente no solo en la selección de información rigurosa y veraz, sino en la interpretación correcta de la información que se le proporciona, y en su transmisión a partir de una observación o un estudio, empleando con corrección distintos formatos, y teniendo en cuenta ciertas normas específicas de comunicación en las disciplinas científicas.

Esta competencia específica se conecta con los siguientes descriptores del recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo: CCL1, CCL2, CCL3, STEM4, CD1, CPSAA4, CC4, CCEC3.

6. Identificar las ciencias y las matemáticas implicadas en contextos diversos, interrelacionando conceptos y procedimientos para aplicarlos correctamente en situaciones de la vida cotidiana.

El conocimiento de las ciencias y de las matemáticas responde a una necesidad de la sociedad, así como a los grandes desafíos y retos de carácter multidisciplinar que la humanidad tiene planteados. Los contenidos del currículo correspondientes al Ámbito Científico-tecnológico dentro del programa de diversificación curricular deben ser valorados por el alumnado como una herramienta esencial para aumentar su competencia científica, al permitirle conectar su experiencia cotidiana con los conocimientos necesarios para juzgarlos con rigor científico.

Por lo tanto, es importante que el alumnado tenga la oportunidad de identificar y experimentar la aplicación de las ciencias y las matemáticas en diferentes contextos.

La conexión entre las ciencias y las matemáticas y otros ámbitos no debería limitarse a los saberes conceptuales, sino ampliarse a los procedimientos y actitudes científicos, de forma que puedan ser transferidos y aplicados a otros contextos de la vida real y a la resolución de problemas del entorno personal, social y, en un futuro, profesional.

Esta competencia específica se conecta con los siguientes descriptores recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo: STEM1, STEM2, STEM5, CD5, CPSAA5, CC4, CE1, CCEC2.

7. Analizar, tras la resolución de un problema, las soluciones obtenidas usando diferentes técnicas y herramientas, evaluando críticamente su validez y significado, a fin de verificar su idoneidad desde en el contexto planteado, así como su repercusión global.

El análisis de las soluciones obtenidas en la resolución de un problema potencia la reflexión crítica sobre su validez, tanto desde un punto de vista estrictamente matemático como desde una perspectiva global, valorando aspectos relacionados con la sostenibilidad, la igualdad de género, el consumo responsable, la equidad o la no discriminación entre otros. El razonamiento científico y matemático serán las herramientas principales para realizar esa validación, pero también lo son la lectura atenta, la realización de preguntas adecuadas, la elección de estrategias para verificar la pertinencia de las soluciones obtenidas según la situación planteada, la conciencia sobre los propios progresos y la autoevaluación.

El desarrollo de esta competencia conlleva procesos reflexivos propios de la metacognición como la autoevaluación y la coevaluación, la utilización de estrategias sencillas de aprendizaje autorregulado, el uso eficaz de herramientas digitales como calculadoras y hojas de cálculo, la verbalización o explicación del proceso y la selección entre diferentes métodos de comprobación de soluciones.

Esta competencia específica se conecta con los siguientes descriptores recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo: STEM1, STEM2, CD2, CPSAA4, CC3, CE3.

8. Desarrollar destrezas sociales para trabajar de forma colaborativa en equipos diversos con roles asignados que permitan potenciar el crecimiento entre iguales, valorando la importancia de romper los roles de género en la investigación científica y en las actividades grupales en general, para el emprendimiento personal y laboral.

El avance científico es producto del esfuerzo colectivo y, rara vez, del resultado del trabajo de un solo individuo. La ciencia implica comunicación y colaboración entre profesionales que, en ocasiones, se encuentran adscritos a diferentes disciplinas. En la generación de nuevos conocimientos es esencial que se compartan las conclusiones obtenidas y los procedimientos seguidos por un grupo de investigación con el resto de la comunidad científica. Estos conocimientos servirán de base para la construcción de nuevas investigaciones y descubrimientos.

Cabe destacar, además, que la interacción y colaboración son de gran importancia en diversos ámbitos profesionales y sociales, y no exclusivamente en un contexto científico. El trabajo en equipo tiene un efecto enriquecedor sobre los resultados obtenidos y a nivel del desarrollo personal de sus participantes, pues permite el intercambio de puntos de vista en ocasiones muy diversos. La colaboración implica movilizar las destrezas comunicativas y sociales del alumnado y requiere de una actitud tolerante y abierta frente a las ideas ajenas, valorando la importancia de romper los roles preestablecidos.

Por este motivo, aprender a trabajar en equipo es imprescindible para el desarrollo profesional y social pleno del alumnado como miembro activo de nuestra sociedad.

Esta competencia específica se conecta con los siguientes descriptores recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo: CCL5, CP3, STEM2, STEM4, CD3, CPSAA1, CPSAA3, CC1, CC2, CE2.

## Primer curso del Programa de Diversificación Curricular.

### Criterios de evaluación.

#### Competencia específica 1.

1.1. Realizar una aproximación de las fases del método científico para explicar los fenómenos naturales más relevantes como estrategia en la toma de decisiones fundamentadas.

1.2. Plantear hipótesis sencillas, expresadas mediante el lenguaje matemático, a partir de observaciones directas.

#### Competencia específica 2.

2.1. Elaborar representaciones que ayuden en la búsqueda de estrategias de resolución de una situación problematizada, organizando los datos dados y comprendiendo las preguntas formuladas.

2.2. Hallar las soluciones de un problema utilizando los datos e información aportados, así como las estrategias y herramientas más apropiadas.

2.3. Aplicar estrategias de cálculo para facilitar la resolución de problemas sencillos relacionados con la vida cotidiana.

2.4. Resolver problemas de la vida cotidiana en los que se precise el planteamiento y resolución de ecuaciones de primer y segundo grado y sistemas de dos ecuaciones lineales con dos incógnitas, aplicando técnicas de manipulaciones algebraicas.

2.5. Reconocer, representar y analizar las funciones lineales, utilizándolas para resolver problemas.

#### Competencia específica 3.

3.1. Plantear preguntas e hipótesis que puedan ser respondidas o contrastadas utilizando los métodos científicos, la observación, la información y el razonamiento, explicando fenómenos naturales y realizando predicciones sobre estos.

3.2. Diseñar y realizar experimentos sencillos y obtener datos cuantitativos y cualitativos sobre fenómenos naturales en el medio natural y en el laboratorio, utilizando con corrección los instrumentos, herramientas o técnicas más adecuadas a la hora de obtener resultados claros que respondan a cuestiones.

3.3. Describir las principales propiedades de la materia, utilizando la terminología técnica y específica apropiada.

3.4. Realizar mediciones de longitud, capacidad y masa con la precisión adecuada en función de la finalidad de la medida, utilizando las técnicas y herramientas más adecuadas en cada caso.

3.5. Interpretar los resultados obtenidos en proyectos de investigación, utilizando el razonamiento y, cuando sea necesario, herramientas matemáticas y tecnológicas.

#### Competencia específica 4.

4.1. Evaluar los efectos de determinadas acciones individuales sobre el organismo y el medio natural, proponiendo hábitos saludables y sostenibles basados en los conocimientos adquiridos y la información disponible.

4.2. Identificar los diferentes agentes geológicos y sus efectos sobre el relieve y el paisaje.

4.3. Identificar situaciones de salud y de enfermedad para las personas, valorando la influencia de los hábitos saludables en la mejora de la salud.

4.4. Describir los mecanismos encargados de la defensa del organismo, analizando los factores que influyen en su funcionamiento.

4.5. Identificar y clasificar las enfermedades infecciosas y no infecciosas más comunes en la población, reconociendo causas, prevención y, en su caso, tratamientos más frecuentes.

4.6. Entender la acción de las vacunas, antibióticos y otras aportaciones de la ciencia médica para el tratamiento y prevención de enfermedades infecciosas, valorando su importancia para el conjunto de la sociedad.

4.7. Diferenciar los nutrientes necesarios para el mantenimiento de la salud, relacionándolos con la dieta equilibrada.

4.8. Relacionar los procesos geológicos externos e internos con la energía que los activa y diferenciar unos de otros.

4.9. Detectar las necesidades tecnológicas, ambientales, económicas y sociales más importantes que demanda la sociedad, entendiendo la capacidad de la ciencia para darles una solución sostenible a través de la implicación de la ciudadanía.

#### Competencia específica 5.

5.1. Organizar y comunicar de forma sencilla información científica y matemática de forma clara de manera verbal, gráfica, numérica, etc., escogiendo en cada contexto el formato más adecuado.

5.2. Analizar e interpretar la información estadística que aparece en los medios de comunicación, valorando su representatividad y fiabilidad.

5.3. Utilizar la notación científica para representar y operar con números muy grandes o muy pequeños, decidiendo sobre la forma más adecuada para expresar las cantidades en cada caso.

5.4. Identificar los distintos tipos de números y utilizarlos para interpretar adecuadamente la información cuantitativa.

5.5. Practicar cambios de unidades de longitud, masa y capacidad.

5.6. Analizar e interpretar de forma sencilla información científica y matemática presente en la vida cotidiana manteniendo una actitud crítica.

5.7. Comparar magnitudes estableciendo su tipo de proporcionalidad.

5.8. Reconocer diferentes fuentes de energía, valorando su impacto en el medio ambiente.

5.9. Mostrar las ventajas e inconvenientes de las diferentes fuentes de energía, valorando aquellas que facilitan un desarrollo sostenible.

5.10. Comprender el comportamiento y la respuesta que presentan distintos sistemas materiales ante la aplicación de una fuerza, así como los efectos producidos por al variar la intensidad o el punto de aplicación.

#### Competencia específica 6.

6.1. Aplicar procedimientos propios de las ciencias y las matemáticas en situaciones diversas, estableciendo conexiones entre distintas áreas de conocimiento en contextos naturales, sociales y profesionales.

6.2. Identificar cada una de las técnicas experimentales que se van a realizar, seleccionando las que ofrecen mejor respuesta al problema planteado.

6.3. Manejar adecuadamente los materiales instrumentales del laboratorio, valorando la importancia de trabajar en condiciones adecuadas de higiene y seguridad.

6.4. Introducción a la programación de aplicaciones sencillas encaminadas al control de distintos dispositivos (impresoras 3D).

#### Competencia específica 7.

7.1. Comprobar y analizar la corrección y el sentido de las soluciones obtenidas tras la resolución de un problema.

7.2. Fabricar objetos o modelos mediante la manipulación y conformación de materiales, empleando herramientas y máquinas adecuadas, incluidas las impresoras 3D, aplicando los fundamentos de estructuras, mecanismos de electricidad y electrónica y respetando las normas de seguridad y salud correspondientes.

#### Competencia específica 8.

8.1. Asumir responsablemente una función concreta dentro de un proyecto científico, utilizando espacios virtuales cuando sea necesario, aportando valor, analizando críticamente las contribuciones del resto del equipo, respetando la diversidad y favoreciendo la inclusión.

8.2. Mostrar resiliencia ante los retos académicos, asumiendo el error como una oportunidad para la mejora y desarrollando un autoconcepto positivo ante las ciencias.

### Contenidos.

 A. Proyecto y destrezas científicas.

— Aproximación a las metodologías de la investigación científica: identificación y formulación de cuestiones, elaboración de hipótesis y comprobación experimental de las mismas.

• El método científico y sus etapas.

— Introducción a los entornos y recursos propios del aprendizaje científico: el laboratorio.

• Aproximación práctica al trabajo en el laboratorio científico.

• Reconocimiento del material básico de laboratorio.

• Uso correcto de los instrumentos de medida.

• Fundamentos básicos de eliminación y reciclaje de residuos.

• Descripción de normas básicas y elaboración y seguimiento de protocolos de seguridad en el laboratorio.

• Introducción al etiquetado de productos químicos y su significado.

— Iniciación al trabajo experimental mediante la realización de proyectos de investigación sencillos y de forma guiada.

— Adquisición del lenguaje científico necesario para expresar con propiedad los resultados correspondientes a un proyecto de investigación sencillo: unidades del Sistema Internacional y sus símbolos.

• Elección correcta de las unidades en que debe ser expresada una magnitud (múltiplos y submúltiplos, cambios de unidades, unidades del Sistema Internacional de Medida y sus símbolos)

• El proceso de medida. Medida indirecta de magnitudes.

— Representación e interpretación de los resultados correspondientes a un proyecto o trabajo experimental (elaboración de gráficos, uso de herramientas digitales destinadas al tratamiento de datos, etc.).

— Valoración de la cultura científica y de las aportaciones realizadas por científicos en los principales hitos históricos logrados por la ciencia que han contribuido al avance y mejora de la sociedad.

B. Números y operaciones

— Utilización y adaptación del conteo para resolver problemas de la vida cotidiana adaptando el tipo de conteo al tamaño de los números y al contexto del problema.

— Uso correcto y crítico de los números naturales, enteros, decimales y racionales. Resolución de operaciones combinadas con los mismos aplicando la prioridad de las operaciones aritméticas (potencias, raíces, multiplicaciones, divisiones, sumas y restas).

— Aplicación de los números naturales, enteros, decimales y racionales a la resolución de problemas y situaciones de la vida cotidiana.

— Estudiar la relación entre los números decimales y las fracciones: números decimales exactos y periódicos. Obtención de la fracción generatriz correspondiente a un número decimal.

— Operar correctamente con fracciones y decimales. Cálculo aproximado y redondeo. Cifras significativas. Error absoluto y relativo. Utilización de aproximaciones y redondeos en la resolución de problemas de la vida cotidiana con la precisión requerida por la situación planteada.

— Potencias de exponente entero. Significado y uso. Su aplicación para la expresión de números muy grandes y muy pequeños. Operaciones con números expresados en notación científica. Uso de la calculadora.

— Comprensión del significado de porcentajes mucho mayores que 100 y menores que 1. Aplicación a la resolución de problemas.

C. Medida y geometría

— Desarrollo de estrategias para la correcta representación sobre la recta numérica de números racionales e irracionales.

— Ordenación de números reales a partir de su representación gráfica en la recta numérica.

D. Geometría en el plano y el espacio.

— Aplicación de las principales fórmulas para obtener longitudes, áreas y volúmenes en formas planas y tridimensionales compuestas. Resolución de problemas geométricos variados.

— Determinación de figuras geométricas a partir de ciertas propiedades. Concepto de lugar geométrico.

— Estudio de traslaciones, simetrías y giros en el plano. Identificación de los elementos invariantes en cada uno de los movimientos.

— Identificación de los planos de simetría existentes en un poliedro.

E. Álgebra

— Conversión de diversas situaciones (con un máximo de dos variables) del lenguaje verbal al algebraico.

— Resolución de problemas de la vida cotidiana que requieran del empleo de ecuaciones de primer grado con una incógnita.

— Clasificación, conforme al valor de los coeficientes del polinomio asociado, de las ecuaciones de segundo grado en completas e incompletas. Aplicar los métodos de resolución más convenientes según corresponda.

— Estudio de diferentes métodos para resolver sistemas de dos ecuaciones lineales con dos incógnitas (sustitución, igualación, reducción y gráfico).

— Repaso de las operaciones básicas con polinomios: suma, resta y multiplicación. Introducción a la división de un polinomio entre un binomio.

— Cálculo del cuadrado de un binomio mediante el uso de las identidades notables.

— Diferencias entre las progresiones aritméticas y geométricas. Añadir correctamente términos a una sucesión dada, o bien construirla a partir de su término general.

— Concepto de función y análisis gráfico de sus propiedades más sencillas (crecimiento, extremos, etc.). Elaboración crítica de la tabla de valores correspondiente a la expresión analítica de una función.

— Representación gráfica de funciones lineales y cuadráticas.

F. Estadística

— Cálculo de las medidas de localización correspondientes a una distribución unidimensional (variable discreta) dada:

• Media, moda, mediana.

— Obtención de las correspondientes medidas de dispersión:

• Rango o recorrido, desviación típica y varianza.

— Descripción de experiencias aleatorias. Concepto de sucesos y espacio muestral. Adquisición del vocabulario matemático necesario para describir y cuantificar situaciones relacionadas con el azar.

— Cálculo de probabilidades mediante la regla de Laplace. Formulación y comprobación de conjeturas sobre el comportamiento de fenómenos aleatorios sencillos.

— Utilización de la probabilidad para tomar decisiones fundamentadas en diferentes contextos. Reconocimiento y valoración de las matemáticas para interpretar, describir y predecir situaciones inciertas.

G. Actitudes y aprendizaje

— Generar confianza en las propias capacidades para afrontar problemas, comprender las relaciones matemáticas y tomar decisiones a partir de ellas. Identificar el error como mecanismo de mejora del aprendizaje.

— Mostrar perseverancia y flexibilidad en la búsqueda de soluciones a los problemas planteados y en la mejora de las soluciones encontradas, valorando positivamente la contribución de las herramientas tecnológicas estudiadas para facilitar e interpretar los cálculos.

— Desarrollar técnicas cooperativas destinadas a optimizar el trabajo en equipo. Creación de agrupaciones flexibles con roles rotatorios a fin de trabajar la empatía, y para que el alumnado identifique sus puntos fuertes y debilidades.

H. Geología

— Manifestaciones de la energía interna de la Tierra. Actividad sísmica y volcánica.

• Origen y tipos de magmas.

— Transformaciones geológicas debidas a la energía interna del planeta Tierra.

— Transformaciones geológicas debidas a la energía externa del planeta Tierra.

— Uso de los minerales y las rocas: su utilización en la fabricación de materiales y objetos cotidianos.

— Relieve y paisaje: diferencias, su importancia como recursos y factores que intervienen en su formación y modelado.

I. Cuerpo humano

— Organización del cuerpo humano, células, tejidos y órganos.

— Importancia de la función de nutrición y los aparatos que participan en ella.

— Anatomía y fisiología básicas de los aparatos digestivo, respiratorio, circulatorio, excretor y reproductor.

— Análisis y visión general de la función de relación: receptores sensoriales, centros de coordinación y órganos efectores.

— Relación entre los principales sistemas y aparatos del organismo implicados en las funciones de nutrición, relación y reproducción mediante la aplicación de conocimientos de fisiología y anatomía.

J. Salud y hábitos saludables

— Concepto de enfermedades infecciosas y no infecciosas: diferenciación en base a su etiología.

— Funcionamiento básico del sistema inmunitario.

— Importancia de la vacunación en la prevención de enfermedades y en la mejora de la calidad de vida humana.

• Avances y aportaciones de las ciencias biomédicas.

— Valoración de la relevancia de los trasplantes y la donación de órganos.

— Educación afectivo-sexual desde la perspectiva de la igualdad entre personas y el respeto a la diversidad sexual. La importancia de las prácticas sexuales responsables. La asertividad y el autocuidado.

— La prevención de infecciones de transmisión sexual (ITS) y de embarazos no deseados. El uso adecuado de métodos anticonceptivos y de métodos de prevención de ITS.

— Valoración y análisis de la importancia del desarrollo de hábitos saludables encaminados a la conservación de la salud física, mental y social (alimentación saludable y actividad física, higiene del sueño, hábitos posturales, uso responsable de las nuevas tecnologías, ejercicio físico, control del estrés, etc.).

• Trastornos y alteraciones más frecuentes, conducta alimentaria, adicciones, trastornos del sueño. Prevención.

K. La materia

— El modelo cinético-molecular de la materia y su relación con los cambios de estado.

• Realización de experimentos de forma guiada relacionados con los sistemas materiales: conocimiento y descripción de sus propiedades, su composición y su clasificación. Mezclas y disoluciones. Concentración.

— Estructura atómica de la materia.

• Tabla periódica y propiedades de los elementos.

• Átomos e iones. Masa atómica y masa molecular. Isótopos.

— Principales compuestos químicos: su formación y sus propiedades físicas y químicas, así como la valoración de sus aplicaciones industriales, tecnológicas y biomédicas.

• Aproximación al concepto de mol.

— Nomenclatura: participación de un lenguaje científico común y universal formulando y nombrando sustancias simples, iones monoatómicos y compuestos binarios mediante las reglas de nomenclatura de la IUPAC.

L. El cambio

— Interpretación microscópica de las reacciones químicas: explicación de las relaciones de la química con el medio ambiente, la tecnología y la sociedad.

— Aplicación de la ley de conservación de la masa (Ley de Lavoisier) y de la ley de las proporciones definidas (Ley de Proust): aplicación de estas leyes como evidencias experimentales que permitan validar el modelo atómico-molecular de la materia.

M. La interacción

— Predicción de movimientos sencillos a partir de los conceptos de la cinemática, formulando hipótesis comprobables sobre valores futuros de estas magnitudes, validándolas a través del cálculo numérico, la interpretación de gráficas o el trabajo experimental.

• Concepto de posición, trayectoria y espacio recorrido.

— Las fuerzas como agentes de cambio: relación de los efectos de las fuerzas, tanto en el estado de movimiento o de reposo de un cuerpo como produciendo deformaciones en los sistemas sobre los que actúan.

• Fuerza y movimiento. Leyes de Newton.

• Cálculo gráfico de la resultante de varias fuerzas.

• Efectos de las fuerzas en situaciones cotidianas y de seguridad vial.

N. La energía

— Diseño y comprobación experimental de hipótesis relacionadas con el uso doméstico e industrial de la energía en sus distintas formas y las transformaciones entre ellas.

— Elaboraciones fundamentadas de hipótesis sobre el medio ambiente a partir de las diferencias entre fuentes de energía, renovables y no renovables. Concienciación sobre la necesidad del ahorro energético y conservación del medio ambiente.

• Uso racional de la energía.

• Tecnología sostenible. Aplicaciones de las tecnologías emergentes.

— Naturaleza eléctrica de la materia: electrización de los cuerpos y los circuitos eléctricos.

• La electricidad como movimiento de cargas eléctricas. Ley de Ohm. Fenómenos de atracción y repulsión.

• Circuitos eléctricos básicos. Asociación de resistencias.

• Aplicaciones de la electricidad en la vida diaria.

O. Proceso de resolución de problemas

— Estrategias, técnicas y marcos de resolución de problemas en diferentes contextos y sus fases.

— Estrategias de búsqueda crítica de información para la investigación y definición de problemas planteados.

— Análisis de productos y de sistemas tecnológicos: construcción de conocimiento desde distintos enfoques y ámbitos.

— Electricidad y electrónica básica para el montaje de esquemas y circuitos físicos o simulados:

• Funciones básicas de los principales componentes del circuito electrónico. Descripción a nivel cualitativo del comportamiento de los diodos y los transistores en un circuito.

• Simbología e interpretación. Conexiones básicas.

• Medida de magnitudes eléctricas fundamentales con el polímetro.

• Diseño y aplicación en proyectos.

• Cálculo de los valores de consumo y potencia eléctrica en proyectos y situaciones cotidianas.

— Introducción a la fabricación digital. Diseño e impresión 3D.

— Emprendimiento, perseverancia y creatividad para abordar problemas desde una perspectiva interdisciplinar.

— Respeto de las normas de seguridad e higiene.

P. Pensamiento computacional, programación y robótica

— Introducción a la inteligencia artificial:

• Sistemas de control programado. Computación física.

• Montaje físico y/o uso de simuladores y programación sencilla de dispositivos.

• Internet de las cosas.

— Fundamentos de la robótica:

• Componentes básicos: sensores, microcontroladores y actuadores.

• Montaje y control programado de robots de manera física y/o por medio de simuladores.

Q. Digitalización y comunicación de proyectos

— Adquisición del vocabulario técnico apropiado.

— Introducción al manejo de aplicaciones CAD (Computer Aideed Desing) en dos dimensiones para la representación de esquemas, circuitos, planos y objetos sencillos.

— Interpretación de planos de taller.

— Herramientas digitales para la publicación y difusión de documentación técnica e información multimedia relativa a proyectos.

— Conceptos básicos en la transmisión de datos: componentes (emisor, canal y receptor), ancho de banda (velocidad de transmisión) e interferencias (ruido).

— Principales tecnologías inalámbricas para la comunicación.

— Herramientas de edición y creación de contenidos multimedia: instalación, configuración y uso responsable.

— Respeto a la propiedad intelectual y a los derechos de autor.

## Segundo curso del Programa de Diversificación Curricular.

### Criterios de evaluación.

#### Competencia específica 1.

1.1. Realizar una aproximación de las fases del método científico para explicar los fenómenos naturales más relevantes como estrategia en la toma de decisiones fundamentadas.

1.2. Plantear hipótesis sencillas, expresadas mediante el lenguaje matemático, a partir de observaciones directas.

#### Competencia específica 2.

2.1. Elaborar representaciones que ayuden en la búsqueda de estrategias de resolución de una situación problematizada, organizando los datos dados y comprendiendo las preguntas formuladas.

2.2. Hallar las soluciones de un problema utilizando los datos e información aportados, así como las estrategias y herramientas más apropiadas.

2.3. Aplicar estrategias de cálculo para facilitar la resolución de problemas sencillos relacionados con la vida cotidiana.

2.4. Resolver problemas de la vida cotidiana en los que se precise el planteamiento y resolución de ecuaciones de primer y segundo grado y sistemas de dos ecuaciones lineales con dos incógnitas, aplicando técnicas de manipulaciones algebraicas.

2.5. Reconocer, representar y analizar las funciones lineales, utilizándolas para resolver problemas.

#### Competencia específica 3.

3.1. Plantear preguntas e hipótesis que puedan ser respondidas o contrastadas utilizando los métodos científicos, la observación, la información y el razonamiento, explicando fenómenos naturales y realizando predicciones sobre estos.

3.2. Diseñar y realizar experimentos sencillos y obtener datos cuantitativos y cualitativos sobre fenómenos naturales en el medio natural y en el laboratorio, utilizando con corrección los instrumentos, herramientas o técnicas más adecuadas a la hora de obtener resultados claros que respondan a cuestiones.

3.3. Describir las principales propiedades de la materia, utilizando la terminología técnica y específica apropiada.

3.4. Realizar mediciones de longitud, capacidad y masa con la precisión adecuada en función de la finalidad de la medida, utilizando las técnicas y herramientas más adecuadas en cada caso.

3.5. Interpretar los resultados obtenidos en proyectos de investigación, utilizando el razonamiento y, cuando sea necesario, herramientas matemáticas y tecnológicas.

#### Competencia específica 4.

4.1. Evaluar los efectos de determinadas acciones individuales sobre el organismo y el medio natural, proponiendo hábitos saludables y sostenibles basados en los conocimientos adquiridos y la información disponible.

4.2. Identificar los diferentes agentes geológicos y sus efectos sobre el relieve y el paisaje.

4.3. Identificar situaciones de salud y de enfermedad para las personas, valorando la influencia de los hábitos saludables en la mejora de la salud.

4.4. Describir los mecanismos encargados de la defensa del organismo, analizando los factores que influyen en su funcionamiento.

4.5. Identificar y clasificar las enfermedades infecciosas y no infecciosas más comunes en la población, reconociendo causas, prevención y, en su caso, tratamientos más frecuentes.

4.6. Entender la acción de las vacunas, antibióticos y otras aportaciones de la ciencia médica para el tratamiento y prevención de enfermedades infecciosas, valorando su importancia para el conjunto de la sociedad.

4.7. Diferenciar los nutrientes necesarios para el mantenimiento de la salud, relacionándolos con la dieta equilibrada.

4.8. Relacionar los procesos geológicos externos e internos con la energía que los activa y diferenciar unos de otros.

4.9. Detectar las necesidades tecnológicas, ambientales, económicas y sociales más importantes que demanda la sociedad, entendiendo la capacidad de la ciencia para darles una solución sostenible a través de la implicación de la ciudadanía.

#### Competencia específica 5.

5.1. Organizar y comunicar de forma sencilla información científica y matemática de forma clara de manera verbal, gráfica, numérica, etc., escogiendo en cada contexto el formato más adecuado.

5.2. Analizar e interpretar la información estadística que aparece en los medios de comunicación, valorando su representatividad y fiabilidad.

5.3. Utilizar la notación científica para representar y operar con números muy grandes o muy pequeños, decidiendo sobre la forma más adecuada para expresar las cantidades en cada caso.

5.4. Identificar los distintos tipos de números y utilizarlos para interpretar adecuadamente la información cuantitativa.

5.5. Practicar cambios de unidades de longitud, masa y capacidad.

5.6. Analizar e interpretar de forma sencilla información científica y matemática presente en la vida cotidiana manteniendo una actitud crítica.

5.7. Comparar magnitudes estableciendo su tipo de proporcionalidad.

5.8. Reconocer diferentes fuentes de energía, valorando su impacto en el medio ambiente.

5.9. Mostrar las ventajas e inconvenientes de las diferentes fuentes de energía, valorando aquellas que facilitan un desarrollo sostenible.

5.10. Comprender el comportamiento y la respuesta que presentan distintos sistemas materiales ante la aplicación de una fuerza, así como los efectos producidos por al variar la intensidad o el punto de aplicación.

#### Competencia específica 6.

6.1. Aplicar procedimientos propios de las ciencias y las matemáticas en situaciones diversas, estableciendo conexiones entre distintas áreas de conocimiento en contextos naturales, sociales y profesionales.

6.2. Identificar cada una de las técnicas experimentales que se van a realizar, seleccionando las que ofrecen mejor respuesta al problema planteado.

6.3. Manejar adecuadamente los materiales instrumentales del laboratorio, valorando la importancia de trabajar en condiciones adecuadas de higiene y seguridad.

6.4. Introducción a la programación de aplicaciones sencillas encaminadas al control de distintos dispositivos (impresoras 3D).

#### Competencia específica 7.

7.1. Comprobar y analizar la corrección y el sentido de las soluciones obtenidas tras la resolución de un problema.

7.2. Fabricar objetos o modelos mediante la manipulación y conformación de materiales, empleando herramientas y máquinas adecuadas, incluidas las impresoras 3D, aplicando los fundamentos de estructuras, mecanismos de electricidad y electrónica y respetando las normas de seguridad y salud correspondientes.

#### Competencia específica 8.

8.1. Asumir responsablemente una función concreta dentro de un proyecto científico, utilizando espacios virtuales cuando sea necesario, aportando valor, analizando críticamente las contribuciones del resto del equipo, respetando la diversidad y favoreciendo la inclusión.

8.2. Mostrar resiliencia ante los retos académicos, asumiendo el error como una oportunidad para la mejora y desarrollando un autoconcepto positivo ante las ciencias.

### Contenidos

A. Proyecto y destrezas científicas.

— Aproximación a las metodologías de la investigación científica: identificación y formulación de cuestiones, elaboración de hipótesis y comprobación experimental de las mismas.

• El método científico y sus etapas.

— Introducción a los entornos y recursos propios del aprendizaje científico: el laboratorio.

• Aproximación práctica al trabajo en el laboratorio científico.

• Reconocimiento del material básico de laboratorio.

• Uso correcto de los instrumentos de medida.

• Fundamentos básicos de eliminación y reciclaje de residuos.

• Descripción de normas básicas y elaboración y seguimiento de protocolos de seguridad en el laboratorio.

• Introducción al etiquetado de productos químicos y su significado.

— Iniciación al trabajo experimental mediante la realización de proyectos de investigación sencillos y de forma guiada.

— Adquisición del lenguaje científico necesario para expresar con propiedad los resultados correspondientes a un proyecto de investigación sencillo: unidades del Sistema Internacional y sus símbolos.

• Elección correcta de las unidades en que debe ser expresada una magnitud (múltiplos y submúltiplos, cambios de unidades, unidades del Sistema Internacional de Medida y sus símbolos)

• El proceso de medida. Medida indirecta de magnitudes.

— Representación e interpretación de los resultados correspondientes a un proyecto o trabajo experimental (elaboración de gráficos, uso de herramientas digitales destinadas al tratamiento de datos, etc.).

— Valoración de la cultura científica y de las aportaciones realizadas por científicos en los principales hitos históricos logrados por la ciencia que han contribuido al avance y mejora de la sociedad.

B. Números y operaciones

— Utilización y adaptación del conteo para resolver problemas de la vida cotidiana adaptando el tipo de conteo al tamaño de los números y al contexto del problema.

— Uso correcto y crítico de los números naturales, enteros, decimales y racionales. Resolución de operaciones combinadas con los mismos aplicando la prioridad de las operaciones aritméticas (potencias, raíces, multiplicaciones, divisiones, sumas y restas).

— Aplicación de los números naturales, enteros, decimales y racionales a la resolución de problemas y situaciones de la vida cotidiana.

— Estudiar la relación entre los números decimales y las fracciones: números decimales exactos y periódicos. Obtención de la fracción generatriz correspondiente a un número decimal.

— Operar correctamente con fracciones y decimales. Cálculo aproximado y redondeo. Cifras significativas. Error absoluto y relativo. Utilización de aproximaciones y redondeos en la resolución de problemas de la vida cotidiana con la precisión requerida por la situación planteada.

— Potencias de exponente entero. Significado y uso. Su aplicación para la expresión de números muy grandes y muy pequeños. Operaciones con números expresados en notación científica. Uso de la calculadora.

— Comprensión del significado de porcentajes mucho mayores que 100 y menores que 1. Aplicación a la resolución de problemas.

C. Medida y geometría

— Desarrollo de estrategias para la correcta representación sobre la recta numérica de números racionales e irracionales.

— Ordenación de números reales a partir de su representación gráfica en la recta numérica.

D. Geometría en el plano y el espacio.

— Aplicación de las principales fórmulas para obtener longitudes, áreas y volúmenes en formas planas y tridimensionales compuestas. Resolución de problemas geométricos variados.

— Determinación de figuras geométricas a partir de ciertas propiedades. Concepto de lugar geométrico.

— Estudio de traslaciones, simetrías y giros en el plano. Identificación de los elementos invariantes en cada uno de los movimientos.

— Identificación de los planos de simetría existentes en un poliedro.

E. Álgebra

— Conversión de diversas situaciones (con un máximo de dos variables) del lenguaje verbal al algebraico.

— Resolución de problemas de la vida cotidiana que requieran del empleo de ecuaciones de primer grado con una incógnita.

— Clasificación, conforme al valor de los coeficientes del polinomio asociado, de las ecuaciones de segundo grado en completas e incompletas. Aplicar los métodos de resolución más convenientes según corresponda.

— Estudio de diferentes métodos para resolver sistemas de dos ecuaciones lineales con dos incógnitas (sustitución, igualación, reducción y gráfico).

— Repaso de las operaciones básicas con polinomios: suma, resta y multiplicación. Introducción a la división de un polinomio entre un binomio.

— Cálculo del cuadrado de un binomio mediante el uso de las identidades notables.

— Diferencias entre las progresiones aritméticas y geométricas. Añadir correctamente términos a una sucesión dada, o bien construirla a partir de su término general.

— Concepto de función y análisis gráfico de sus propiedades más sencillas (crecimiento, extremos, etc.). Elaboración crítica de la tabla de valores correspondiente a la expresión analítica de una función.

— Representación gráfica de funciones lineales y cuadráticas.

F. Estadística

— Cálculo de las medidas de localización correspondientes a una distribución unidimensional (variable discreta) dada:

• Media, moda, mediana.

— Obtención de las correspondientes medidas de dispersión:

• Rango o recorrido, desviación típica y varianza.

— Descripción de experiencias aleatorias. Concepto de sucesos y espacio muestral. Adquisición del vocabulario matemático necesario para describir y cuantificar situaciones relacionadas con el azar.

— Cálculo de probabilidades mediante la regla de Laplace. Formulación y comprobación de conjeturas sobre el comportamiento de fenómenos aleatorios sencillos.

— Utilización de la probabilidad para tomar decisiones fundamentadas en diferentes contextos. Reconocimiento y valoración de las matemáticas para interpretar, describir y predecir situaciones inciertas.

G. Actitudes y aprendizaje

— Generar confianza en las propias capacidades para afrontar problemas, comprender las relaciones matemáticas y tomar decisiones a partir de ellas. Identificar el error como mecanismo de mejora del aprendizaje.

— Mostrar perseverancia y flexibilidad en la búsqueda de soluciones a los problemas planteados y en la mejora de las soluciones encontradas, valorando positivamente la contribución de las herramientas tecnológicas estudiadas para facilitar e interpretar los cálculos.

— Desarrollar técnicas cooperativas destinadas a optimizar el trabajo en equipo. Creación de agrupaciones flexibles con roles rotatorios a fin de trabajar la empatía, y para que el alumnado identifique sus puntos fuertes y debilidades.

H. Geología

— Manifestaciones de la energía interna de la Tierra. Actividad sísmica y volcánica.

• Origen y tipos de magmas.

— Transformaciones geológicas debidas a la energía interna del planeta Tierra.

— Transformaciones geológicas debidas a la energía externa del planeta Tierra.

— Uso de los minerales y las rocas: su utilización en la fabricación de materiales y objetos cotidianos.

— Relieve y paisaje: diferencias, su importancia como recursos y factores que intervienen en su formación y modelado.

I. Cuerpo humano

— Organización del cuerpo humano, células, tejidos y órganos.

— Importancia de la función de nutrición y los aparatos que participan en ella.

— Anatomía y fisiología básicas de los aparatos digestivo, respiratorio, circulatorio, excretor y reproductor.

— Análisis y visión general de la función de relación: receptores sensoriales, centros de coordinación y órganos efectores.

— Relación entre los principales sistemas y aparatos del organismo implicados en las funciones de nutrición, relación y reproducción mediante la aplicación de conocimientos de fisiología y anatomía.

J. Salud y hábitos saludables

— Concepto de enfermedades infecciosas y no infecciosas: diferenciación en base a su etiología.

— Funcionamiento básico del sistema inmunitario.

— Importancia de la vacunación en la prevención de enfermedades y en la mejora de la calidad de vida humana.

• Avances y aportaciones de las ciencias biomédicas.

— Valoración de la relevancia de los trasplantes y la donación de órganos.

— Educación afectivo-sexual desde la perspectiva de la igualdad entre personas y el respeto a la diversidad sexual. La importancia de las prácticas sexuales responsables. La asertividad y el autocuidado.

— La prevención de infecciones de transmisión sexual (ITS) y de embarazos no deseados. El uso adecuado de métodos anticonceptivos y de métodos de prevención de ITS.

— Valoración y análisis de la importancia del desarrollo de hábitos saludables encaminados a la conservación de la salud física, mental y social (alimentación saludable y actividad física, higiene del sueño, hábitos posturales, uso responsable de las nuevas tecnologías, ejercicio físico, control del estrés, etc.).

• Trastornos y alteraciones más frecuentes, conducta alimentaria, adicciones, trastornos del sueño. Prevención.

K. La materia

— El modelo cinético-molecular de la materia y su relación con los cambios de estado.

• Realización de experimentos de forma guiada relacionados con los sistemas materiales: conocimiento y descripción de sus propiedades, su composición y su clasificación. Mezclas y disoluciones. Concentración.

— Estructura atómica de la materia.

• Tabla periódica y propiedades de los elementos.

• Átomos e iones. Masa atómica y masa molecular. Isótopos.

— Principales compuestos químicos: su formación y sus propiedades físicas y químicas, así como la valoración de sus aplicaciones industriales, tecnológicas y biomédicas.

• Aproximación al concepto de mol.

— Nomenclatura: participación de un lenguaje científico común y universal formulando y nombrando sustancias simples, iones monoatómicos y compuestos binarios mediante las reglas de nomenclatura de la IUPAC.

L. El cambio

— Interpretación microscópica de las reacciones químicas: explicación de las relaciones de la química con el medio ambiente, la tecnología y la sociedad.

— Aplicación de la ley de conservación de la masa (Ley de Lavoisier) y de la ley de las proporciones definidas (Ley de Proust): aplicación de estas leyes como evidencias experimentales que permitan validar el modelo atómico-molecular de la materia.

M. La interacción

— Predicción de movimientos sencillos a partir de los conceptos de la cinemática, formulando hipótesis comprobables sobre valores futuros de estas magnitudes, validándolas a través del cálculo numérico, la interpretación de gráficas o el trabajo experimental.

• Concepto de posición, trayectoria y espacio recorrido.

— Las fuerzas como agentes de cambio: relación de los efectos de las fuerzas, tanto en el estado de movimiento o de reposo de un cuerpo como produciendo deformaciones en los sistemas sobre los que actúan.

• Fuerza y movimiento. Leyes de Newton.

• Cálculo gráfico de la resultante de varias fuerzas.

• Efectos de las fuerzas en situaciones cotidianas y de seguridad vial.

N. La energía

— Diseño y comprobación experimental de hipótesis relacionadas con el uso doméstico e industrial de la energía en sus distintas formas y las transformaciones entre ellas.

— Elaboraciones fundamentadas de hipótesis sobre el medio ambiente a partir de las diferencias entre fuentes de energía, renovables y no renovables. Concienciación sobre la necesidad del ahorro energético y conservación del medio ambiente.

• Uso racional de la energía.

• Tecnología sostenible. Aplicaciones de las tecnologías emergentes.

— Naturaleza eléctrica de la materia: electrización de los cuerpos y los circuitos eléctricos.

• La electricidad como movimiento de cargas eléctricas. Ley de Ohm. Fenómenos de atracción y repulsión.

• Circuitos eléctricos básicos. Asociación de resistencias.

• Aplicaciones de la electricidad en la vida diaria.

O. Proceso de resolución de problemas

— Estrategias, técnicas y marcos de resolución de problemas en diferentes contextos y sus fases.

— Estrategias de búsqueda crítica de información para la investigación y definición de problemas planteados.

— Análisis de productos y de sistemas tecnológicos: construcción de conocimiento desde distintos enfoques y ámbitos.

— Electricidad y electrónica básica para el montaje de esquemas y circuitos físicos o simulados:

• Funciones básicas de los principales componentes del circuito electrónico. Descripción a nivel cualitativo del comportamiento de los diodos y los transistores en un circuito.

• Simbología e interpretación. Conexiones básicas.

• Medida de magnitudes eléctricas fundamentales con el polímetro.

• Diseño y aplicación en proyectos.

• Cálculo de los valores de consumo y potencia eléctrica en proyectos y situaciones cotidianas.

— Introducción a la fabricación digital. Diseño e impresión 3D.

— Emprendimiento, perseverancia y creatividad para abordar problemas desde una perspectiva interdisciplinar.

— Respeto de las normas de seguridad e higiene.

P. Pensamiento computacional, programación y robótica

— Introducción a la inteligencia artificial:

• Sistemas de control programado. Computación física.

• Montaje físico y/o uso de simuladores y programación sencilla de dispositivos.

• Internet de las cosas.

— Fundamentos de la robótica:

• Componentes básicos: sensores, microcontroladores y actuadores.

• Montaje y control programado de robots de manera física y/o por medio de simuladores.

Q. Digitalización y comunicación de proyectos

— Adquisición del vocabulario técnico apropiado.

— Introducción al manejo de aplicaciones CAD (Computer Aideed Desing) en dos dimensiones para la representación de esquemas, circuitos, planos y objetos sencillos.

— Interpretación de planos de taller.

— Herramientas digitales para la publicación y difusión de documentación técnica e información multimedia relativa a proyectos.

— Conceptos básicos en la transmisión de datos: componentes (emisor, canal y receptor), ancho de banda (velocidad de transmisión) e interferencias (ruido).

— Principales tecnologías inalámbricas para la comunicación.

— Herramientas de edición y creación de contenidos multimedia: instalación, configuración y uso responsable.

— Respeto a la propiedad intelectual y a los derechos de autor.

Segundo curso del Programa de Diversificación Curricular

Criterios de evaluación

Competencia específica 1.

1.1. Justificar la contribución de la ciencia a la sociedad, y la labor de los hombres y mujeres dedicados a su desarrollo, entendiendo la investigación como una labor colectiva en constante evolución fruto de la interacción entre la ciencia, la tecnología, la sociedad y el medio ambiente.

1.2. Plantear hipótesis sencillas a partir de observaciones directas o indirectas recopiladas por distintos medios.

1.3. Planificar métodos y procedimientos experimentales sencillos de diversa índole para refutar o no sus hipótesis.

1.4. Interpretar enunciados de problemas matemáticos sencillos organizando los datos dados y estableciendo las relaciones básicas y directas entre ellos.

Competencia específica 2.

2.1. Aplicar los conocimientos científicos en la resolución de problemas de situaciones de la vida cotidiana.

2.2. Emplear herramientas tecnológicas adecuadas en la representación, la resolución de problemas y la comprobación de las soluciones.

Competencia específica 3.

3.1. Elaborar informes de ensayos en los que se incluye el procedimiento seguido, los resultados obtenidos y las conclusiones finales.

Competencia específica 4.

4.1. Relacionar, empleando fundamentos científicos, la preservación de la biodiversidad, la conservación del medio ambiente y la protección de los seres vivos con el desarrollo sostenible y la calidad de vida.

4.2. Identificar las reacciones químicas principales y describir los componentes principales y la intervención de la energía en las mismas.

4.3. Conocer los fenómenos de contaminación y los principales causantes, valorando las medidas que promueven evitarlos.

4.4. Reconocer y valorar el papel del agua en la existencia y supervivencia de la vida en el planeta, valorando las medidas de ahorro en su consumo.

4.5. Analizar las implicaciones positivas de un desarrollo sostenible, analizando su impacto en la economía y la sociedad.

4.6. Relacionar la estructura atómica de un elemento con su posición en la tabla periódica, con sus propiedades fisicoquímicas y con el tipo de enlace que forma al combinarse con otros elementos.

4.7. Valorar el papel de las mutaciones en la diversidad genética, comprendiendo la relación entre mutación y evolución.

4.8. Comprender la relevancia de la energía en la sociedad actual e identificar y desarrollar hábitos de consumo responsables.

Competencia específica 5.

5.1. Emplear y citar de forma adecuada fuentes fiables, seleccionando la información científica relevante en la consulta y creación de contenidos para la mejora del aprendizaje propio y colectivo.

5.2. Utilizar instrumentos adecuados para medir ángulos, longitudes, áreas y volúmenes, seleccionando los más adecuados en cada caso.

5.3. Identificar y representar gráficamente la función cuadrática y la función exponencial aplicando métodos sencillos de representación.

5.4. Extraer la información de gráficas que representen los distintos tipos de funciones asociadas a situaciones reales.

5.5. Elaborar e interpretar tablas y gráficos estadísticos.

5.6. Discriminar los movimientos cotidianos en función de su trayectoria y su celeridad.

5.7. Realizar cálculos sencillos de velocidades, espacios recorridos y tiempos en movimientos con aceleración constante.

5.8. Describir la relación causa efecto en distintas situaciones para encontrar la relación entre fuerzas y movimiento.

Competencia específica 6.

6.1. Utilizar correctamente las identidades notables en las operaciones con polinomios.

6.2. Obtener valores a partir de una expresión algebraica.

6.3. Resolver ecuaciones de primer y segundo grado sencillas de modo algebraico y gráfico.

Competencia específica 7.

7.1. Comprobar la corrección de las soluciones correspondientes a un problema, así como su coherencia en el contexto planteado.

7.2. Conocer y aplicar las herramientas digitales básicas para obtener y comprobar la corrección matemática de las soluciones obtenidas en la resolución de un problema.

Competencia específica 8.

8.1. Emprender, de forma guiada y de acuerdo a la metodología adecuada, proyectos científicos colaborativos orientados a la mejora y a la creación de valor en la sociedad.

8.2. Trabajar en equipo para alcanzar soluciones consensuadas a los problemas, cuestiones y ejercicios científicos planteados.

Contenidos

A. Proyecto y destrezas científicas

— Diseño sencillo del trabajo experimental y emprendimiento de proyectos de investigación: estrategias en la resolución de problemas mediante el uso de la experimentación y el tratamiento del error mediante la indagación, la deducción, la búsqueda de evidencias y el razonamiento lógico-matemático, haciendo inferencias válidas de las observaciones y obteniendo conclusiones que vayan más allá de las condiciones experimentales para aplicarlas a nuevos escenarios.

• Uso correcto del lenguaje científico y matemático: manejo adecuado de distintos sistemas de unidades y sus símbolos.

— Empleo de diversos recursos de aprendizaje científico, tales como el laboratorio o los entornos virtuales, utilizando de forma correcta los materiales, sustancias y herramientas tecnológicas, y atendiendo a las normas de uso de cada espacio, asegurando y protegiendo así la salud propia y comunitaria, la seguridad en redes y el respeto hacia el medio ambiente.

• Desarrollo integral de un proyecto de investigación sencillo, que abarque desde los estadios iniciales correspondientes al diseño y justificación del mismo hasta el análisis crítico de los resultados obtenidos.

• Utilización correcta del material de laboratorio y de los instrumentos de medida pertinentes.

• Aplicación responsable de las normas de seguridad en el laboratorio.

— Estrategias de interpretación y producción de información científica en diferentes formatos y a partir de diferentes medios: desarrollo del criterio propio basado en lo que el pensamiento científico aporta a la mejora de la sociedad para hacerla más justa, equitativa e igualitaria.

B. Números y operaciones

— Resolución de situaciones y problemas de la vida cotidiana en los que sea conveniente el empleo de estrategias útiles para realizar recuentos sistemáticos (diagrama de árbol, técnicas de combinatoria, etc.).

— Expresión correcta de cantidades mediante el empleo de distintos tipos de números reales. Realización de estimaciones en contextos diversos, acotando correctamente el error cometido.

— Profundización en la resolución de operaciones combinadas cada vez más complejas que contengan números enteros, decimales y racionales, aplicando correctamente la prioridad de las operaciones involucradas.

— Estudio de las propiedades de los números irracionales. Aplicación de las mismas a cálculos sencillos.

— Identificación de números irracionales relevantes, tales como el número ? o la proporción aurea.

C. Medida y geometría

— Aplicación de los métodos para una correcta representación de los números irracionales sobre la recta real.

— Estudio del significado de los diferentes tipos de intervalos (abiertos, cerrados o mixtos). Representación de los mismos sobre la recta real, así como de intervalos formados por la unión o intersección de un par de ellos.

D. Geometría en el plano y el espacio

— Modelización de elementos geométricos de la vida cotidiana con herramientas tecnológicas tales como programas de geometría dinámica, realidad aumentada, etc.

— Consolidación de estrategias para descomponer correctamente cuerpos y figuras geométricas diversas y poder obtener así sus áreas y volúmenes. Aplicación a la resolución de problemas geométricos variados.

E. Álgebra

— Resolución de problemas de la vida cotidiana que requieran del empleo de ecuaciones de primer y segundo grado con una incógnita. Evaluación crítica de las soluciones obtenidas.

— Aplicación de los métodos estudiados para la resolución de sistemas de ecuaciones lineales. Aplicación a la resolución de problemas en contextos reales.

— Introducción a la resolución de sistemas de ecuaciones no lineales sencillos.

— Operaciones combinadas con polinomios: suma, resta, multiplicación y división.

— Factorización de polinomios de segundo grado completos resolviendo la ecuación asociada. Aplicar el procedimiento en sentido inverso, construyendo ecuaciones a través de la multiplicación de binomios que respondan a situaciones concretas y le permitan al alumnado desarrollar enunciados una vez conocidas las soluciones del problema.

— Análisis de las propiedades y aplicación de los métodos para representar gráficamente funciones lineales y cuadráticas.

— Representar sobre el plano cartesiano funciones definidas a trozos formadas, bien por una función lineal y una constante, bien por dos funciones lineales. Introducción del concepto de continuidad.

— Construcción comparativa de las tablas de valores correspondientes a una función lineal y a una función exponencial, diferencia del crecimiento en ambos casos. Aplicación en ejemplos de la vida cotidiana y modelización mediante crecimientos exponenciales.

— Uso de las tecnologías de la información para el análisis conceptual y reconocimiento de propiedades de las funciones, así como para su representación.

F. Estadística

— Cálculo de las medidas de centralización correspondientes a una distribución unidimensional (variable continua) dada. Estudio del concepto de marca de clase:

• Media.

— Obtención de las correspondientes medidas de dispersión y posición:

• Rango o recorrido, desviación típica, varianza, moda, mediana y cuartiles.

— Gráficos estadísticos: representación mediante diferentes tecnologías (calculadora, hoja de cálculo, aplicaciones...) y elección del más adecuado según el contexto.

— Probabilidad: cálculo, aplicando la regla de Laplace y técnicas de recuento, a experimentos simples y compuestos sencillos (mediante diagramas de árbol, tablas…).

— Utilización de la probabilidad para tomar decisiones fundamentadas en diferentes contextos. Reconocimiento y valoración de las matemáticas para interpretar, describir y predecir situaciones inciertas.

G. Actitudes y aprendizaje

— Estrategias tanto de fomento de la curiosidad, la iniciativa y la perseverancia como de la flexibilidad cognitiva en el aprendizaje de las matemáticas: apertura a cambios de estrategia y transformación del error en oportunidad de aprendizaje.

— Desarrollar actitudes inclusivas y de aceptación de la diversidad presente en el aula, utilizando ésta como un exponente más de la diversidad social.

H. Genética y evolución

— Función biológica de la mitosis, la meiosis y sus fases.

— Destrezas de observación de las distintas fases de la mitosis al microscopio.

— Modelo simplificado de la estructura del ADN y del ARN y relación con su función y síntesis.

— Estrategias de extracción de ADN de una célula eucariota.

— Estudio sencillo de las etapas de la expresión génica y de las características del código genético.

— Relación entre las mutaciones, la replicación del ADN, el cáncer, la evolución y la biodiversidad.

— Fenotipo y genotipo: definición y diferencias.

— Análisis del proceso evolutivo de una o más características concretas de una especie determinada a la luz de la teoría neodarwinista y de otras teorías con relevancia histórica (lamarckismo y darwinismo).

— La evolución humana y el proceso de hominización.

I. Geología

— Análisis de la estructura y dinámica de la geosfera. Métodos de estudio.

• Determinar las capas que conforman el interior del planeta en función de su composición y de su mecánica, y reconocer las discontinuidades y zonas de transición.

— Estudio de los efectos globales de la dinámica de la geosfera desde la perspectiva de la tectónica de placas.

• Teoría de la tectónica de placas y tipos de bordes de placas litosféricas.

• Relación de la distribución de la actividad sísmica y volcánica con la dinámica del interior de la Tierra.

— Procesos geológicos externos e internos: diferencias y relación con los riesgos naturales. Medidas de prevención y mapas de riesgos.

— Interpretación de cortes geológicos sencillos.

J. El planeta Tierra

— Descripción del origen del universo y de los componentes del sistema solar.

— Hipótesis sobre el origen de la vida en la Tierra.

— Discusión sobre las principales investigaciones en el campo de la astrobiología.

— Ecología y sostenibilidad. Impacto en la economía y en la sociedad.

— Estudio de las funciones de la atmósfera y la hidrosfera y su importancia para los seres vivos.

• Análisis de los principales contaminantes medioambientales y su relación con los problemas causados.

• Valoración de las acciones que favorecen la conservación del medio ambiente.

K. La materia

— Sistemas materiales: resolución de problemas y situaciones de aprendizaje diversas sobre las disoluciones y los gases, entre otros sistemas materiales significativos.

• Leyes de los gases.

• Disoluciones.

— Modelos atómicos: desarrollo histórico de los principales modelos atómicos clásicos y descripción de las partículas subatómicas, estableciendo su relación con los avances de la física y la química.

— Estructura electrónica de los átomos: configuración electrónica de un átomo y su relación con la posición del mismo en la tabla periódica y con sus propiedades fisicoquímicas.

— Compuestos químicos: su formación, propiedades físicas y químicas y valoración de su utilidad e importancia en otros campos como la ingeniería o el deporte.

• El enlace químico: iónico, covalente y metálico.

• Compuestos químicos de especial interés.

— Cuantificación de la cantidad de materia: cálculo del número de moles de sistemas materiales de diferente naturaleza, manejando con soltura las diferentes formas de medida y expresión de la misma en el entorno científico.

• Masa atómica y molecular.

• Concepto de mol. Constante de Avogadro.

• Concentración molar de una disolución.

— Nomenclatura inorgánica: denominación de sustancias simples, iones y compuestos químicos binarios y ternarios mediante las normas de la IUPAC.

— Introducción a la nomenclatura de los compuestos orgánicos: denominación de compuestos orgánicos monofuncionales a partir de las normas de la IUPAC como base para entender la gran variedad de compuestos del entorno basados en el carbono.

• Compuestos orgánicos de interés industrial y biológico.

L. El cambio

— Reacciones químicas: ajuste de reacciones químicas y realización de predicciones cualitativas y cuantitativas basadas en la estequiometría, relacionándolas con procesos fisicoquímicos de la industria, el medioambiente y la sociedad.

• Ajuste de reacciones químicas.

• Cálculos estequiométricos sencillos.

• Reacciones químicas de especial interés.

— Descripción cualitativa de reacciones químicas de interés: reacciones de combustión, neutralización y procesos electroquímicos sencillos, valorando las implicaciones que tienen en la tecnología, la sociedad o el medioambiente.

— Factores que influyen en la velocidad de las reacciones químicas.

M. La interacción

— Predicción y comprobación, utilizando la experimentación y el razonamiento matemático, de las principales magnitudes, ecuaciones y gráficas que describen el movimiento de un cuerpo, relacionándolo con situaciones cotidianas y con la mejora de la calidad de vida.

• Movimiento rectilíneo y uniforme.

• Movimiento rectilíneo uniformemente acelerado.

— La fuerza como agente de cambios en los cuerpos: principio fundamental de la Física que se aplica a otros campos como el diseño, el deporte o la ingeniería.

— Carácter vectorial de las fuerzas: uso del álgebra vectorial básica para la realización gráfica de operaciones con fuerzas y su aplicación a la resolución de problemas relacionados con sistemas sometidos a conjuntos de fuerzas.

— Principales fuerzas del entorno cotidiano: reconocimiento del peso, la normal, el rozamiento, la tensión o el empuje, y su uso en la explicación de fenómenos físicos en distintos escenarios.

— Ley de Hooke.

— Ley de la gravitación universal: atracción entre los cuerpos que componen el universo.

— Fenómenos eléctricos y magnéticos: experimentos sencillos que evidencian la relación con las fuerzas de la naturaleza.

— Fuerzas y presión en los fluidos: efectos de las fuerzas y la presión sobre los líquidos y los gases, estudiando los principios fundamentales que las describen.

N. La energía.

— La energía: formulación y comprobación de hipótesis sobre las distintas formas y aplicaciones de la energía, a partir de sus propiedades y del principio de conservación, como base para la experimentación y la resolución de problemas relacionados con la energía mecánica en situaciones cotidianas.

• Energía cinética y energía potencial.

• Energía mecánica. Conservación de la energía mecánica.

— Transferencias de energía: el trabajo y el calor como formas de transferencia de energía entre sistemas relacionados con las fuerzas o la diferencia de temperatura.

— La luz y el sonido como ondas que transfieren energía. Aplicaciones.

• Concepto de onda. Características y propiedades.

• Utilización de la energía del Sol como fuente de energía limpia y renovable.

— La energía en nuestro mundo: estimación de la energía consumida en la vida cotidiana mediante la búsqueda de información contrastada, la experimentación y el razonamiento científico, comprendiendo la importancia de la energía en la sociedad, su producción y su uso responsable.
