# LOMLOE (Ley Orgánica por la que se Modifica la Ley Orgánica de Educación)  

## Legislación estatal
[BOE-A-2020-17264 Ley Orgánica 3/2020, de 29 de diciembre, por la que se modifica la Ley Orgánica 2/2006, de 3 de mayo, de Educación.](https://www.boe.es/buscar/act.php?id=BOE-A-2020-17264)  
Una manera más práctica de verlo es  
- Ver LOE consolidada en BOE [BOE-A-2006-7899 Ley Orgánica 2/2006, de 3 de mayo, de Educación.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899)  
- [Texto de LOE con LOMLOE publicado en web ministerio diciembre 2020 tras aprobación en senado](https://educagob.educacionyfp.gob.es/dam/jcr:f92577f1-f2b4-4135-8f4a-c41e80628954/loe-con-lomloe-texto.pdf)  
- [Página LOMLOE en web ministerio](https://educagob.educacionyfp.gob.es/lomloe/ley.html)  


[Real Decreto 205/2023, de 28 de marzo, por el que se establecen medidas relativas a la transición entre planes de estudios, como consecuencia de la aplicación de la Ley Orgánica 3/2020, de 29 de diciembre, por la que se modifica la Ley Orgánica 2/2006, de 3 de mayo, de Educación.](https://www.boe.es/buscar/act.php?id=BOE-A-2023-7939)  

## Madrid

[Circular de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial sobre la ordenación y la organización de las enseñanzas de educación secundaria obligatoria en el curso académico 2022-2023](https://www.educa2.madrid.org/web/educamadrid/principal/files/d7f00e27-b9d6-46d8-af57-bd026dd592e5/CircularinformativaESO.pdf?t=1654065675040)  

## [Currículo LOMLOE](/home/legislacion-educativa/lomloe/curriculo)  

## [Evaluación LOMLOE](/home/legislacion-educativa/lomloe/evaluacion)  

## [Acceso universidad LOMLOE](/home/legislacion-educativa/lomloe/acceso-universidad)  

Esta web de [Enrique Gerrero](https://twitter.com/kikeguerrerot/) recopila en 2022 normativa estatal y de las distintas comunidades autónomas sobre LOMLOE

[El site de kike](https://sites.google.com/view/elsitedekike/inicio)  
Por ejemplo Madrid
[El site de kike - Madrid](https://sites.google.com/view/elsitedekike/comunidades-aut%C3%B3nomas/madrid)   

José Manuel Martínez comparte tabla resumen  
[twitter jmanuelmmtnez/status/1552612749003325443](https://twitter.com/jmanuelmmtnez/status/1552612749003325443)  
Legislación para el curso 2022-2023 en la Comunidad de Madrid (ESO y Bachillerato). [Adjunto el enlace al documento en PDF](https://drive.google.com/file/d/1F2PAF31I74cNilIRcf1NHNwdPjlHo8RS/view?usp=sharing)  #LOMLOE #claustrovirtual  
![](https://pbs.twimg.com/media/FYv9EIGXkAAYHRv?format=jpg)  

[Primer año de la ley de educación en las aulas: del lío con las evaluaciones a la burocracia interminable - eldiario.es](https://www.eldiario.es/sociedad/primer-ano-ley-educacion-aulas-lio-evaluaciones-burocracia-interminable_1_10393992.html)  

[Ángel Pérez Pueyo: "Lo que fue una buena idea se está convirtiendo en un nuevo fracaso" - magisnet.com](https://www.magisnet.com/2023/08/angel-perez-pueyo-lo-que-fue-una-buena-idea-se-esta-convirtiendo-en-un-nuevo-fracaso/)  
