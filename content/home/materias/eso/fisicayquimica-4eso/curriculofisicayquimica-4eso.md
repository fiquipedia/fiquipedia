
# Currículo Física y Química 4º ESO (LOE)

Ver DECRETO 23/2007 en  [Legislación](/home/legislacion-educativa) Este currículo deja de estar en vigor en el curso 2016/2017 según el calendario de implantación LOMCE. Ver  [Currículo Física y Química 4º ESO (LOMCE)](/home/materias/eso/fisicayquimica-4eso/curriculo-fisica-y-quimica-4-eso-lomce) Esta página es estática y sin enlaces: para ver enlaces a recursos ver  [Contenidos currículo con enlaces a recursos: Física y Química 4º ESO](/home/materias/eso/fisicayquimica-4eso/contenidos-del-curriculo-con-enlaces-a-recursos-4-eso-fisica-y-quimica)  

Ver  [comparación currículo Física y Química 4º ESO](/home/materias/eso/fisicayquimica-4eso/comparacion-curriculo-fisica-y-quimica-4-eso)
 

##   Objetivos
Los objetivos son de etapa, no específicos de curso, para el área de  [Ciencias de la Naturaleza](/home/materias/eso/ciencias-de-la-naturaleza-1y2eso) 

##  Contenidos 


###  Bloque 1. Introducción al trabajo experimental 

   * Las magnitudes y su medida. El Sistema Internacional de unidades. Carácter aproximado de la medida. Notación científica. Redondeo
   * Aparatos de medida. Medida de masas: balanzas. Medidas de volumen. Medidas de longitud: regla y calibrador. Medidas de tiempo: cronómetro.
   * El trabajo en el laboratorio. Formulación de hipótesis y diseños experimentales. Análisis e interpretación de resultados experimentales.
   * La comunicación científica: el informe científico. Reglas y ejemplos

###  Bloque 2. Fuerzas y movimiento. 

####  Iniciación al estudio del movimiento. 

   * Movimiento y sistema de referencia. Trayectoria y posición. Desplazamiento y espacio recorrido. Velocidad y aceleración.
   * Estudio del movimiento rectilíneo y uniforme. Estudio del movimiento rectilíneo y uniformemente acelerado.
   * Análisis de los movimientos cotidianos.

####  Las fuerzas y el equilibrio. 

   * Las fuerzas y sus efectos estáticos.
   * Composición y descomposición de fuerzas.
   * Equilibrio de fuerzas.
   * Fuerzas en los fluidos. Concepto de presión. Presiones hidrostática y atmosférica. Aplicaciones
   * El principio de Pascal y la multiplicación de la fuerza.
   * El principio de Arquímedes y la flotación de barcos y globos. Tensión superficial.

####  Las fuerzas y el movimiento. 

   * Las leyes de la Dinámica y la superación de la física del sentido común.
   * Tratamiento cualitativo de la fuerza de rozamiento.
   * La ley de la Gravitación universal y la culminación de la primera de las revoluciones científicas. El peso de los cuerpos y su caída. El movimiento de planetas y satélites.

###  Bloque 3. Energía, trabajo y calor 

####  Trabajo, potencia y energía mecánica. 

   * Concepto de trabajo. Unidades. Trabajo mecánico. Aplicación a máquinas y herramientas. Concepto de Potencia.
   * La energía mecánica y sus formas. El trabajo como transferencia de energía mecánica. La conservación de la energía mecánica.

####  Calor y energía térmica. 

   * Concepto de temperatura. Energía térmica.
   * Transferencia de energía por efecto de diferencias de temperatura.
   * Conservación y degradación de la energía. Efectos del calor sobre los cuerpos.

####  La energía y las ondas: luz y sonido 

   * Concepto de onda. Tipos y características de las ondas.
   * Transferencia de energía sin transporte de materia.
   * La luz y el sonido. Propiedades de su propagación. Espectro lumínico y espectro acústico.

###  Bloque 4. Estructura y propiedades de las sustancias. 

####  El átomo y las propiedades de las sustancias. 

   * La estructura del átomo.
   * El sistema periódico de los elementos químicos.
   * Clasificación de las sustancias según sus propiedades. Estudio experimental.
   * El enlace químico: enlaces iónico, covalente y metálico.
   * Interpretación de las propiedades de las sustancias.
   * Introducción a la formulación y nomenclatura inorgánica según las normas de la IUPAC.

####  Las reacciones químicas. 

   * Tipos de reacciones químicas.
   * Relaciones estequiométricas y volumétricas en las reacciones químicas.
   * Calor de reacción. Concepto de exotermia y endotermia.
   * Velocidad de una reacción química. Factores que influyen.

###  Bloque 5. Iniciación a la estructura de los compuestos de carbono. 

####  La química de los compuestos de carbono. 

   * El carbono como componente esencial de los seres vivos. El carbono y la gran cantidad de compuestos orgánicos. Características de los compuestos de carbono.
   * Descripción de los compuestos orgánicos más sencillos: Hidrocarburos y su importancia como recursos energéticos. Alcoholes. Ácidos orgánicos.

####  Polímeros sintéticos. 

   * Fabricación y reciclaje de materiales plásticos.
   * Macromoléculas: importancia en la constitución de los seres vivos.
   * Valoración del papel de la química en la comprensión del origen y desarrollo de la vida.

###  Bloque 6. La contribución de la ciencia a un futuro sostenible 

####  El desafío medioambiental. 

   * El problema del incremento del efecto invernadero: causas y medidas para su prevención.
   * Cambio climático
   * Contaminación sin fronteras.
   * Agotamiento de recursos.
   * Reducción de la biodiversidad.

####  Contribución del desarrollo tecno-científico a la sostenibilidad. 

   * Importancia de la aplicación del principio de precaución y de la participación ciudadana en la toma de decisiones.
   * Energías limpias.
   * Gestión racional de los recursos naturales.
   * Valoración de la educación científica de la ciudadanía como requisito de sociedades demócratas sostenibles.
   * La cultura científica como fuente de satisfacción personal

##   Criterios de evaluación
1. Aplicar correctamente las principales ecuaciones, explicando las diferencias fundamentales de los movimientos MRU, MRUA y MCU. Distinguir claramente entre las unidades de velocidad y aceleración, así como entre magnitudes lineales y angulares.  
2. Identificar las fuerzas por sus efectos estáticos. Componer y descomponer fuerzas. Manejar las nociones básicas de la estática de fluidos y comprender sus aplicaciones. Explicar cómo actúan los fluidos sobre los cuerpos que flotan o están sumergidos en ellos mediante la aplicación del Principio de Arquímedes.  
3. Identificar las fuerzas que actúan sobre un cuerpo, generen o no aceleraciones. Describir las leyes de la Dinámica y aportar a partir de ellas una explicación científica a los movimientos cotidianos. Determinar la importancia de la fuerza de rozamiento en la vida real. Dibujar las fuerzas que actúan sobre un cuerpo en movimiento, justificando el origen de cada una, e indicando las posibles interacciones del cuerpo en relación con otros cuerpos.  
4. Identificar el carácter universal de la fuerza de la gravitación y vincularlo a una visión del mundo sujeto a leyes que se expresan en forma matemática.  
5. Diferenciar entre trabajo mecánico y trabajo fisiológico. Explicar que el trabajo consiste en la transmisión de energía de un cuerpo a otro mediante una fuerza. Identificar la potencia con la rapidez con que se realiza un trabajo y explicar la importancia que esta magnitud tiene en la industria y la tecnología.  
6. Relacionar la variación de energía mecánica que ha tenido lugar en un proceso con el trabajo con que se ha realizado. Aplicar de forma correcta el Principio de conservación de la energía en el ámbito de la mecánica.  
7. Identificar el calor como una energía en tránsito entre los cuerpos a diferente temperatura y describir casos reales en los que se pone de manifiesto. Diferenciar la conservación de la energía en términos de cantidad con la degradación de su calidad conforme es utilizada. Aplicar lo anterior a transformaciones energéticas relacionadas con la vida real.  
8. Describir el funcionamiento teórico de una máquina térmica y calcular su rendimiento. Identificar las transformaciones energéticas que se producen en aparatos de uso común (mecánicos, eléctricos y térmicos).  
9. Explicar las características fundamentales de los movimientos ondulatorios. Identificar hechos reales en los que se ponga de manifiesto un movimiento ondulatorio. Relacionar la formación de una onda con la propagación de la perturbación que la origina. Distinguir las ondas longitudinales de las transversales y realizar cálculos numéricos en los que interviene el periodo, la frecuencia y la longitud de ondas sonoras y electromagnéticas.  
10. Indicar las características que deben tener los sonidos para que sean audibles. Describir la naturaleza de la emisión sonora.  
11. Utilizar la teoría atómica para explicar la formación de nuevas sustancias a partir de otras preexistentes. Expresar mediante ecuaciones la representación de dichas transformaciones, observando en ellas el Principio de conservación de la materia.  
12. Diferenciar entre procesos físicos y procesos químicos. Escribir y ajustar correctamente las ecuaciones químicas correspondientes a enunciados y descripciones de procesos químicos sencillos y analizar las reacciones químicas que intervienen en procesos energéticos fundamentales.  
13. Explicar las características de los ácidos y de las bases y realizar su neutralización. Empleo de los indicadores para averiguar el pH.  
14. Explicar los procesos de oxidación y combustión, analizando su incidencia en el medio ambiente.  
15. Explicar las características básicas de los procesos radiactivos, su peligrosidad y sus aplicaciones.  
16. Escribir fórmulas sencillas de los compuestos de carbono, distinguiendo entre compuestos saturados e insaturados.  
17. Conocer los principales compuestos del carbono: hidrocarburos, petróleo, alcoholes y ácidos.  
18. Justificar la gran cantidad de compuestos orgánicos existentes así como la formación de macromoléculas y su importancia en los seres vivos  
19. Enumerar los elementos básicos de la vida. Explicar cuáles son los principales problemas medioambientales de nuestra época y su prevención.  
20. Describir algunas de las principales sustancias químicas que se aplican en diversos ámbitos de la sociedad: agrícola, alimentario, construcción e industrial.  

