---
aliases:
  - /home/pruebas-de-acceso-a-grado-medio/normativa-y-curriculo-pruebas-acceso-grado-medio/
---

# Normativa y Currículo Pruebas Acceso Grado Medio

##  [](/) Normativa
Real Decreto 1147/2011, de 29 de julio, por el que se establece la ordenación general de la formación profesional del sistema educativo. 
 [http://www.boe.es/buscar/act.php?id=BOE-A-2011-13118](http://www.boe.es/buscar/act.php?id=BOE-A-2011-13118)  
 Ver Artículo 17. Prueba de acceso a los ciclos formativos de grado medio y Artículo 21. Disposiciones comunes para los cursos y las pruebas de acceso.  
 
ORDEN 4879/2008, de 21 de octubre, de la Consejería de Educación, por la que se regulan las pruebas de acceso a ciclos formativos de Formación Profesional y el curso de preparación a las mismas.  
[http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&idnorma=6435&word=S&wordperfect=N&pdf=S](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&idnorma=6435&word=S&wordperfect=N&pdf=S)  
Los anexos solamente se visualizan en la versión en pdf  
[http://www.bocm.es/boletin/CM_Orden_BOCM/2008/11/10/2008-11-10_03112008_0004.pdf](http://www.bocm.es/boletin/CM_Orden_BOCM/2008/11/10/2008-11-10_03112008_0004.pdf)  
El anexo con los contenidos de la prueba de acceso a grado medio es "ANEXO 5 PRUEBAS DE ACCESO A CICLOS FORMATIVOS DE FORMACIÓN PROFESIONAL  
Contenidos y criterios de evaluación de las pruebas de acceso a ciclos formativos de grado medio "  
(son 5 páginas: se anexa aquí un pdf que tiene solamente esas páginas del anexo 5)  

Orden 431/2013 de 19 de febrero, de la Consejería de Educación, Juventud y Deporte, por la que se modifica la Orden 4879/2008, de 21 de octubre, por la que se regulan las pruebas de acceso a ciclos formativos de Formación Profesional y el curso de preparación de las mismas  
[http://www.madrid.org/fp/admision_pruebas/pruebas_acceso/O20130431_Pruebas_Acceso2013.pdf](http://www.madrid.org/fp/admision_pruebas/pruebas_acceso/O20130431_Pruebas_Acceso2013.pdf)  
Sigue indicando que los contenidos son los de los anexos 5 (grado medio) y 6 (grado superior). Modifica la regulación de pruebas ya que indica "Se derogan el capítulo V y los Anexos 9, 10, 11, 12, 14.a), 14.b) y 14.c"  

[http://www.madrid.org/fp/admision_pruebas/pruebas_acceso.htm](http://www.madrid.org/fp/admision_pruebas/pruebas_acceso.htm) 

##  [](/) Currículo
La parte Científico-Técnica en las Pruebas de Acceso a Ciclos de Grado Medio indica:

###  [](/) Bloque 1. Materia y energía.
Sistemas materiales.  
- Los cambios de posición en los sistemas materiales.
- Movimiento rectilíneo uniforme y uniformemente variado. Concepto de aceleración.
- Representación gráfica de movimientos sencillos.  
Las fuerzas y sus aplicaciones.
- Las fuerzas como causa del movimiento, los equilibrios y las deformaciones (ecuación y unidades en el S.I.).
- Masa y peso de los cuerpos. Atracción gravitatoria.  
- Estudio cualitativo del Principio de Arquímedes. Aplicaciones sencillas.  
La energía en los sistemas materiales.  
- La energía como concepto fundamental para el estudio de los cambios. Cambio de posición, forma y estado.  
- Análisis y valoración de las diferentes fuentes de energía, renovables y no renovables.  
- Problemas asociados a la obtención, transporte y utilización de la energía.  
- Toma de conciencia de la importancia del ahorro energético.

###  [](/) Bloque 2. Transferencia de energía.
Calor y temperatura.  
- Interpretación del calor como forma de transferencia de energía.  
- Distinción entre calor y temperatura. Los termómetros.  
- El calor como agente productor de cambios. Reconocimiento de situaciones en las que se manifiesten los efectos delcalor sobre los cuerpos.  
- Propagación del calor. Aislantes y conductores.

###  [](/) Bloque 3. Materiales de uso técnico.
- Materiales de uso habitual: clasificación general. Materiales naturales y transformados.  
- La madera: constitución. Propiedades y características. Maderas de uso habitual. Identificación de maderas naturales y transformadas. Derivados de la madera: papel y cartón. Tableros artificiales. Aplicaciones más comunes de las maderas naturales y manufacturadas.  
- Materiales férricos: el hierro. Extracción. Fundición y acero. Obtención y propiedades características: mecánicas, eléctricas, térmicas. Aplicaciones.  
- Metales no férricos: cobre, aluminio. Obtención y propiedades características: mecánicas, eléctricas, térmicas. Aplicaciones.  
- Distinción de los diferentes tipos de metales y no metales.  
- Técnicas básicas e industriales para el trabajo con metales. Tratamientos. Manejo de herramientas y uso seguro de las mismas.

###  [](/) Bloque 4. La vida en acción.
Las funciones vitales.  
- Las funciones de nutrición: Obtención y uso de materia y energía por los seres vivos.  
- Nutrición autótrofa y heterótrofa.  
- La fotosíntesis y su importancia en la vida de la Tierra.  
- La Respiración en los seres vivos.  
- Las funciones de relación: percepción, coordinación y movimiento.  
- Las funciones de reproducción: La reproducción sexual y asexual.

###  [](/) Bloque 5. El medio ambiente natural.
- Conceptos de Biosfera, ecosfera y ecosistema.- Identificación de los componentes de un ecosistema.- Influencia de los factores bióticos y abióticos en los ecosistemas.- El papel que desempeñan los organismos productores, consumidores y descomponedores en el ecosistema. Cadenas y redes tróficas.

###  [](/) Bloque 6. Las personas y la salud.
Promoción de la salud.  
- El concepto de organismo pluricelular. La organización general del cuerpo humano: la célula, tejidos, órganos, sistemas y aparatos.  
- El concepto de salud y el de enfermedad.  
- Principales agentes causantes de enfermedades infecciosas.  
- La lucha contra dichas enfermedades. Sistema inmunitario. Vacunas  
- Enfermedades no infecciosas. Causas, remedios y prevención.  
- Estudio de factores y hábitos relacionados con la salud en nuestra Comunidad Autónoma. La promoción de la salud y de estilos de vida saludables.  
- Dietas saludables y equilibradas. Prevención de las enfermedades provocadas por malnutrición. La conservación, manipulación y comercialización de los alimentos. Las personas y el consumo de alimentos.  
- Estilos de vida para una salud cardiovascular.  
- Factores que repercuten en la salud mental en la sociedad actual.  
- Las sustancias adictivas: el tabaco, el alcohol y otras drogas. Problemas asociados.  
- Actitud responsable ante conductas de riesgo para la salud.

##  [](/) Criterios de evaluación
1. Relacionar el concepto de energía con la capacidad de realizar cambios estableciendo la relación entre causa y efecto.  
2. Conocer diferentes formas y fuentes de energía renovables y no renovables, sus ventajas e inconvenientes y algunos de los principales problemas asociados a su obtención, transporte y utilización.  
3. Conocer el principio de conservación de la energía y aplicarlo en algunos ejemplos sencillos.  
4. Comprender la importancia del ahorro energético y el uso de energías limpias para contribuir a un futuro sostenible.  
5. Resolver problemas sencillos aplicando los conocimientos sobre el concepto de temperatura y su medida, el equilibrio y desequilibrio térmico, los efectos del calor sobre los cuerpos y su forma de propagación.  
6. Conocer y relacionar las funciones vitales de los seres vivos.  
7. Diferenciar entre la nutrición de seres autótrofos y heterótrofos.  
8. Conocer las características y los tipos de reproducción.  
9. Identificar los elementos fundamentales que intervienen en la función de relación.  
10. Identificar los componentes y las interrelaciones que se establecen en un ecosistema.  
11. Explicar cambios en los procesos de los seres vivos y en la dinámica de la Tierra, con efectos observables.  
12. Establecer relaciones entre las diferentes funciones del organismo humano y los factores que tienen una mayor influencia en la salud, como son los estilos de vida.  
13. Conocer los conceptos relacionados con los mecanismos de defensa corporal en la lucha contra la enfermedad.  
14. Conocer los conceptos relacionados con la salud y la prevención de la enfermedad y valorar su importancia sobre la salud: reproducción, sexualidad, hábitos tóxicos, ejercicio físico y alimentación.  
15. Conocer las propiedades mecánicas, eléctricas y térmicas de los materiales.  
16. Relacionar dichas propiedades con la aplicación de cada material en la fabricación de objetos comunes. 


