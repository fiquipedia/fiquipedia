
# Recursos Energía

Página en borrador: desde concepto de energía en general, a tipos de energía, a fuentes de energía (renovables/no renovables), más asociado a ciencias naturales /CMC e incluso primaria...  

Puede haber recursos asociados al  [movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio) , ya que se trata conjuntamente cinemática, dinámica y energía.  
Pendiente crear páginas separadas por tipo de fuente de energía. Para energía nuclear puede haber cosas en Física Nuclear de 2º Bachillerato.  

También se puede asociar a [Recursos Física y Química y Medio Ambiente](/home/recursos/recursos-fisica-quimica-y-medio-ambiente) 

[La energía 3ESO - newton.cnice.mec.es](http://newton.cnice.mec.es/materiales_didacticos/energia/)  

[Trabajo y energía 4ESO (pdf 25 páginas) - educacion.es/secundaria/edad](http://recursostic.educacion.es/secundaria/edad/4esofisicaquimica/4quincena6/impresos/quincena6.pdf)  

[Calor y energía 4ESO (pdf 35 páginas) - educacion.es/secundaria/edad](http://recursostic.educacion.es/secundaria/edad/4esofisicaquimica/4quincena7/impresos/quincena7.pdf)  

[Los cambios en la naturaleza: concepto de energía (pdf 13 páginas) - educarex.es](http://www.educarex.es/pub/cont/com/0019/documentos/pruebas-acceso/contenidos/modulo_IV/ciencias_de_la_naturaleza/4nat04.pdf)  

[Trabajo y energía mecánica 4ESO - blog.educastur.es/eureka _waybackmachine_](http://web.archive.org/web/20190424175722/http://blog.educastur.es/eureka/4%C2%BA-fyq/trabajo-y-energia-mecanica/)  

[![](https://img.youtube.com/vi/GOrWy_yNBvY/0.jpg)](https://www.youtube.com/watch?v=GOrWy_yNBvY "Thermodynamics and the End of the Universe: Energy, Entropy, and the fundamental laws of physics. Physics Videos by Eugene Khutoryansky")

[La energía y sus formas - luisamariaarias.wordpress.com](https://luisamariaarias.wordpress.com/cono/tema-4-la-energia/la-energia-y-sus-formas/)  
Se recopilan enlaces a recursos, algunos interactivos  

[Tipos de energías renovables, resumen](http://erenovable.com/tipos-de-energias-renovables-resumen/)  

[http://iesmh.edu.gva.es/ptebar/FUENTES DE NERGIA.htm](http://iesmh.edu.gva.es/ptebar/FUENTES%20DE%20NERGIA.htm)  IES Miguel Hernández   

[twitter OperadorNuclear/status/1515086762682597381](https://twitter.com/OperadorNuclear/status/1515086762682597381)  
Toda nuestra energía es de origen nuclear. #ApuntesOperador  
![](https://pbs.twimg.com/media/FQarYLPWUAMDLCU?format=jpg)  


## Fuentes de energía / vectores de energía

[L'énergie, les énergies fossiles et renouvelables](https://www.notre-planete.info/ecologie/energie/)  
![](https://www.notre-planete.info/actualites/images/energie/energies_flux.png)  

Es importante distinguir lo que son tipos de energía (formas en las que se puede presentar, por ejemplo cinética y potencial gravitatoria) de fuentes de energía ("lugares" donde está almacenada en la naturaleza y se puede recuperar en distintos tipos, por ejemplo combustibles fósiles y solar) y vectores de energía (no presentes en la naturaleza, son formas de almacenar y transportar energía, por ejemplo el hidrógeno generado por electrólisis para su consumo posterior y la electricidad obtenida en una central hidroeléctrica para su consumo inmediato)  

## Energía Solar

[![](https://img.youtube.com/vi/TLyzQCX0hRc/0.jpg)](https://www.youtube.com/watch?v=TLyzQCX0hRc "This blooming sunflower collects energy from the sun - TechInsider")  

[Energía solar integrada en tejas](https://www.dyaqua.it/invisiblesolar/_it/coppo-invisible-solar-fotovoltaico-integrato-per-centri-storici.php)  

## Simulaciones
 [https://phet.colorado.edu/en/simulations/category/physics/work-energy-and-power](https://phet.colorado.edu/en/simulations/category/physics/work-energy-and-power)  
 
 Energía en la pista de patinaje: conceptos básicos   
[https://phet.colorado.edu/es/simulation/energy-skate-park-basics](https://phet.colorado.edu/es/simulation/energy-skate-park-basics)   
[https://phet.colorado.edu/sims/html/energy-skate-park-basics/latest/energy-skate-park-basics_es.html](https://phet.colorado.edu/sims/html/energy-skate-park-basics/latest/energy-skate-park-basics_es.html)  
[![Energía en la pista de patinaje: conceptos básicos Captura de pantalla](https://phet.colorado.edu/sims/html/energy-skate-park-basics/latest/energy-skate-park-basics-600.png "Energía en la pista de patinaje: conceptos básicos Captura de pantalla") ](https://phet.colorado.edu/sims/html/energy-skate-park-basics/latest/energy-skate-park-basics_es.html)   
   * Conservación de Energía
   * Energía Cinética
   * Energía Potencial
   
 [Formas y cambios de energía - phet.colorado.edu](https://phet.colorado.edu/es/simulations/energy-forms-and-changes)  (HTML5)  
 [![](https://phet.colorado.edu/sims/html/energy-forms-and-changes/latest/energy-forms-and-changes-900.png)](https://phet.colorado.edu/es/simulations/energy-forms-and-changes)  
 
 Simulation 2D: Energy Conservation in a Spring (bungee jump) [https://soundphysics.ius.edu/?page_id=229](https://soundphysics.ius.edu/?page_id=229)  
 
[Energía - edumedia-sciences.com](https://www.edumedia-sciences.com/es/node/64-energia)  (son de pago, tienen previsualización)  
[Amusement Park Physics Interactive - learner.org](https://www.learner.org/series/interactive-amusement-park-physics/)  
![](https://www.learner.org/wp-content/interactive/parkphysics/images/header.gif)  

## Ejercicios energía 
Se ponen ejercicios de energía de elaboración propia, en la versión inicial se ponen conjuntamente en un único fichero varios bloques (trabajo, conservación de energía, potencia) pero puede que por volumen se separan en varios ficheros.  
En pruebas libres ESO y acceso Grado Medio también hay ejercicios sencillos de energía.  

Los ficheros se veían inicialmente anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia home/recursos/energia](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/energia)  

## Rendimiento

[La carga inalámbrica en móviles tiene un problema gordo: necesita casi un 50% más de energía que cargar el móvil con cable - xataka.com](https://www.xataka.com/moviles/carga-inalambrica-moviles-tiene-problema-gordo-necesita-casi-50-energia-que-cargar-movil-cable-1)  
![](https://i.blogs.es/d5a568/power/1366_2000.jpg)  

