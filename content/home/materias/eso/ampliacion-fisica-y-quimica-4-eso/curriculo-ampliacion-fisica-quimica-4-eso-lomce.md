
# Currículo Ampliación Física y Química 4º ESO (LOMCE)

Ver [ORDEN 2160/2016, de 29 de junio, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid.](http://www.bocm.es/boletin/CM_Orden_BOCM/2016/07/01/BOCM-20160701-7.PDF)   

## Introducción
La presente materia está diseñada para su oferta en cuarto curso de la Educación Secundaria Obligatoria.  
Los cambios sociales experimentados en los últimos siglos se deben en gran parte a los logros conseguidos por la ciencia y por la actividad de los científicos, sobre todo en aspectos relacionados con la salud, el medio ambiente y el desarrollo tecnológico.  
En un mundo cada vez más tecnificado, los ciudadanos deben tener competencia científica. La competencia científica es importante para comprender los problemas ambientales, médicos, económicos y de otro tipo a los que se enfrentan las sociedades modernas, que dependen enormemente del progreso tecnológico y científico. Además, el rendimiento de los mejores alumnos de un país en las materias científicas tiene repercusiones en el papel que el mismo desempeñe el día de mañana en el sector de las tecnologías avanzadas y en su competitividad internacional en general. Por el contrario, las deficiencias en competencia matemática y científica pueden tener consecuencias negativas para las perspectivas laborales y económicas de los individuos, así como para su capacidad de participar plenamente en la sociedad.  
La Física y Química, junto con el resto de las materias que componen el conocimiento científico, aparece hoy en día como imprescindible para una sociedad, pues:  
— Forma parte de la cultura general, si por cultura entendemos el conjunto de conocimientos científicos, históricos, literarios y artísticos.  
— Proporciona las bases para comprender el desarrollo social, económico y tecnológico que caracteriza el momento actual que ha permitido al hombre alcanzar a lo largo del tiempo una mayor esperanza y calidad de vida.  
— Proporciona un evidente enriquecimiento personal porque despierta y ayuda a la formación de un espíritu crítico.— Es modeladora de valores sociales, precisamente por su propio carácter social.  
— Proporciona las bases del conocimiento y la práctica del método científico.  
— Permite a las personas intervenir con criterios propios en muchos de los grandes temas presentes en la sociedad actual: cambio climático, conservación del medio ambiente, biotecnología, energías renovables, etcétera.  
— Es la base de un gran número de salidas profesionales.  
La materia Ampliación de Física y Química permite a los alumnos profundizar en contenidos que se abordan de forma más general en la Física y Química de cuarto curso y estudiar otros que le serán de utilidad para estudios posteriores. En cualquiera de los casos, esta materia enriquecerá tanto a los alumnos que finalizan sus estudios en esta etapa, como a aquellos que los continuarán en la secundaria postobligatoria.  
La idea de que la Física y la Química, como todas las ciencias, tiene implicaciones con la tecnología y la sociedad debe ponerse de manifiesto en la metodología, planteando cuestiones teóricas y prácticas mediante las que el alumno comprenda que uno de los objetivos de la ciencia es determinar las leyes que rigen la naturaleza. El proceso de adquisición de una cultura científica, además del conocimiento y la comprensión de los conceptos, implica el aprendizaje de procedimientos y el desarrollo de actitudes y valores propios del trabajo científico. La realización de actividades prácticas y el desarrollo de algunas fases del método científico permitirán alcanzar habilidades que servirán de motivación para lograr nuevos conocimientos y poner en práctica métodos del trabajo experimental.  
Los contenidos de la materia quedan distribuidos en cuatro bloques:  
— La actividad científica.  
— Las fuerzas y sus efectos.  
—Energía: sus formas y sus transferencias.  
— El átomo y los cambios químicos.

## Contenidos

### Bloque 1. La actividad científica
— Las magnitudes y su medida. El sistema internacional de unidades. Carácter aproximado de la medida. Errores absolutos y relativos. Notación científica. Redondeo.  
— Aparatos de medida. Medida de masas: balanzas monoplato y balanzas de dos platillos. Medidas de volumen. Medidas de longitud: regla y calibre. Medidas de tiempo: cronómetro.  
— Magnitudes directamente proporcionales. Representaciones gráficas de magnitudes directamente proporcionales.  
— El método científico. El trabajo en laboratorio. Formulación de hipótesis y diseños experimentales. Análisis e interpretación de resultados experimentales.  
— Investigación científica: Labor colectiva e interdisciplinar  
— Proyecto de investigación.

### Bloque 2. Las fuerzas y sus efectos
— Fuerzas y movimientos: MRU. Aceleración. MRUA. Cálculo de la aceleración. Ecuaciones de los movimientos rectilíneos. Representaciones gráficas de los movimientos rectilíneos.  
— Fuerza gravitatoria: caída libre. Ecuaciones de movimiento.  
— Fuerzas y deformaciones: Ley de Hooke. Determinación experimental de la ley de Hooke.  
— Fuerza y presión en los fluidos: Principio de Pascal. Aplicaciones. Principio de Arquímedes. Condiciones de flotación de los cuerpos.

### Bloque 3. Energía: sus formas y sus transferencias
— Energía térmica: calor. Efectos del calor. Calor específico. Calor absorbido o cedido con variación de temperatura. Temperatura de equilibrio.  
— Ondas: propiedades de la luz. Refracción. Ley de Snell. Índice de refracción.  
— Corriente eléctrica: Ley de Ohm. Asociaciones de resistencias. Circuitos eléctricos.

### Bloque 4. El átomo y los cambios químicos
— Introducción al laboratorio de química: el vidrio.  
— Elementos: configuración electrónica. Salto electrónicos y sus implicaciones energéticas. Espectroscopía a la llama. Identificación de elementos.  
— Uniones entre átomos: metales, no metales, semimetales y gases nobles. Tipos de enlace: iónico, covalente y metálico. Propiedades de las sustancias.  
— Reacciones químicas: ecuación química. Reacciones de descomposición, combustión y de obtención de gas.  
— Reacciones químicas: saponificación. Reciclaje de aceite usado. Propiedades del jabón.  
— Ácidos y bases: concepto de ácido y base según la teoría de Arrhenius. Escala de pH. Indicadores. Reacción de neutralización. Ácidos y bases en la vida diaria.  
— Disoluciones: separación de los componentes de una disolución. Cristalización y destilación.  
— Polímeros: importancia industrial. Tipos de polímeros. Plásticos, problemas medioambientales.

## Criterios de evaluación y estándares de aprendizaje evaluables


### Bloque 1. La actividad científica
1. Relacionar magnitudes y su unidad.  
1.1. Maneja las unidades.  
2. Emplear con corrección los múltiplos y submúltiplos.  
2.1. Maneja los múltiplos y submúltiplos.  
3. Calcular el error absoluto y relativo.  
3.1. Entiende el carácter aproximado de la medida.  
3.2. Calcula errores absolutos y relativos.  
4. Utilizar adecuadamente el redondeo y expresar los resultados en notación científica.  
4.1. Expresa con corrección los resultados de las medidas.  
5. Manejar aparatos de medida de uso habitual en el laboratorio.  
5.1. Realiza medias de masas, volúmenes y longitudes.  
5.2. Expresa los resultados correctamente  
6. Determinar la sensibilidad de aparatos de medida.  
6.1. Identifica la sensibilidad de distintos aparatos de medida de volúmenes y utiliza el aparato más adecuado según la medida a realizar.  
7. Realizar representaciones gráficas interpretando los resultados.  
7.1. Realiza gráficas a partir de tablas de datos en papel milimetrado.  
7.2. Realiza gráficas utilizando las TIC.  
8. Interpretar la proporcionalidad directa entre dos magnitudes.  
8.1. Interpreta gráficas.  
8.2. Resuelve cuestiones relativas a la proporcionalidad entre variables.  
9. Explicar los procesos que corroboran una hipótesis y la dotan de valor científico.  
9.1. Conoce el método científico inductivo y deductivo.  
10. Conocer el método que siguieron científicos relevantes en la elaboración de leyes.  
10.1. Diseña y planifica una experiencia aplicando el método científico imitando a Galileo y determinando las variables que afectan al período de oscilación de un péndulo.  
11. Describir algún hecho histórico relevante en el que se manifieste la comunicación entre la comunidad científica.  
11.1. Conoce el célebre episodio de la historia de la ciencia ocurrido en Londres en 1697.  
12. La curva cicloide.  
12.1. Describe las propiedades de la curva cicloide: Braquistócrona y tautócrona.  
13. Elaborar y defender un proyecto de investigación utilizando las TIC.  
13.1. Elabora y defiende un proyecto de investigación sobre un tema de interés científico, utilizando las TIC.

### Bloque 2. Las fuerzas y sus efectos
1. Interpretar gráficas de movimientos.  
1.1. Analiza e interpreta gráficas de MRU.  
1.2. Analiza e interpreta gráficas de MRUA.  
2. Explicar las diferencias fundamentales de los MRU y MRUA.  
2.1. Conoce las diferencias entre MRU y MRUA.3. Distinguir experimentalmente un MRU de un MRUA.  
3.1. Identifica experimentalmente un MRU y un MRUA.  
3.2. Toma datos y los representa gráficamente con corrección.  
4. Aplicar correctamente las principales ecuaciones del movimiento.  
4.1. Utiliza las ecuaciones matemáticas de caída libre.  
5. Calcular el tiempo de reacción.  
5.1. Calcula el tiempo de reacción.  
5.2. Interpreta expresiones como distancia de seguridad.  
6. Relacionar fuerza y deformación de los cuerpos elásticos.  
6.1. Conoce la relación entre la fuerza y la deformación de un cuerpo elástico.  
7. Interpretar la ley de Hooke.  
7.1. Interpreta la ley de Hooke.  
7.2. Comprueba experimentalmente la relación fuerza/alargamiento.  
7.3. Resuelve actividades y problemas sobre la ley de Hooke.  
7.4. Conoce las aplicaciones prácticas de la ley de Hooke.  
7.5. Conoce el dinamómetro.  
8. Interpretar experimentalmente el Principio de Pascal y el Principio de Arquímedes.  
8.1. Construye un ludión.  
8.2. Avanza hipótesis y las comunica fundadamente acerca de los resultados de la experiencia.  
8.3. Interpreta experimentalmente el Principio de Pascal y el Principio de Arquímedes.  
9. Conocer aplicaciones de estos principios.  
9.1. Conoce el fundamento de la prensa hidráulica, el gato y el elevador.  
9.2. Conoce el fundamento de la flotabilidad de los barcos.

### Bloque 3. Energía: sus formas y sus transferencias
1. Identificar el calor como forma de transferencia de energía.  
1.1. Distingue la aceptación coloquial de calor de su significado científico.  
2. Reconocer las propiedades características y generales de la materia.  
2.1. Reconoce el calor específico como una propiedad característica.  
3. Interpretar el significado del calor específico.  
3.1. Interpreta el significado del calor específico.  
4. Determinación experimental de la temperatura de equilibrio y del calor específico.  
4.1. Determina experimentalmente el calor específico de un sólido por el método del equilibrio térmico.  
5. Comprender las leyes de la refracción.  
5.1. Entiende las leyes de la refracción.  
6. Conocer la ley de Snell.  
6.1. Interpreta la ley de Snell.  
7. Determinar experimentalmente el índice de refracción de distintas sustancias.  
7.1. Determina experimentalmente el índice de refracción del agua, glicerina, etanol y aceite hidratante.  
7.2. Interpreta el significado del índice de refracción.  
8. Comprender la importancia de la electricidad en el mundo actual.  
8.1. Conoce los múltiples usos de la electricidad en la actualidad.  
9. Identificar los elementos de un circuito y su función.  
9.1. Realiza el montaje de un circuito eléctrico.  
9.2. Maneja el voltímetro y el amperímetro.  
10. Conocer la ley de Ohm.  
10.1. Comprende y utiliza la ley de Ohm.  
11. Identificar las distintas asociaciones de resistencias.  
11.1. Realiza montajes de resistencias en paralelo y en serie.  
12. Resolver problemas de circuitos.  
12.1. Resuelve problemas y cuestiones de corriente eléctrica.

### Bloque 4. El átomo y los cambios químicos
1. Conocer la materia prima constituyente del vidrio.  
1.1. Conoce los componentes básicos del vidrio.  
2. Manejar técnicas experimentales para elaborar capilares y tubos acodados.  
2.1. Maneja el material experimental con precaución.  
3. Reconocer diferentes usos para el vidrio.  
3.1. Conoce aplicaciones del vidrio como la fibra óptica.  
4. Justificar hábitos de reciclaje.  
4.1. Respeta y preserva el entorno reciclando tarros y botellas de vidrio.  
4.2. Toma conciencia del ahorro de energía y materia prima, reduciendo los residuos y lacontaminación del aire por reciclado de vidrio.  
5. Determinar la configuración electrónica de los elementos.  
5.1. Conoce y escribe la configuración electrónica de los átomos.  
6. Conocer los fundamentos teóricos de la espectroscopía.  
6.1. Interpreta los saltos electrónicos por absorción o emisión de energía.  
7. Identificar los elementos por espectroscopía a la llama.  
7.1. Identifica los metales de algunas sales por el color de la llama.  
7.2. Sitúa los elementos en la TP.  
8. Interpretar los distintos tipos de enlace entre los elementos en función de su posición en la TP.  
8.1. Distingue entre metales, no metales, semimetales y gases nobles, justificando los diferentes enlaces químicos.  
9. Explicar las propiedades de una sustancia a partir de su enlace químico.  
9.1. Identifica las principales propiedades de las sustancias iónicas, sustancias covalentes y metálicas.  
9.2. Determina la conductividad de las sustancias en estado sólido y en disolución.  
9.3. Comprueba la solubilidad de las sustancias.  
9.4. Observa su aspecto físico.  
10. Diferenciar cambios físicos y químicos.  
10.1. Diferencia cambios físicos de químicos.  
11. Distinguir reactivos y productos.  
11.1. Lee ecuaciones químicas con corrección e identifica los estados físicos de las sustancias que intervienen.  
12. Identificar el estado físico de las sustancias que intervienen en una reacción.  
12.1. Escribe ecuaciones químicas indicando el estado físico de las sustancias.  
13. Conocer las implicaciones energéticas de las reacciones químicas.  
13.1. Distingue reacciones exotérmicas y endotérmicas.  
14. Conocer las normas de seguridad en el manejo de reactivos y en la realización de los procedimientos.  
14.1. Conoce y aplica las normas de seguridad en la realización de las reacciones experimentales.  
14.2. Manipula con precaución los reactivos.  
15. Conocer la importancia del reciclaje del aceite ya usado.  
15.1. Toma conciencia de la necesidad de reciclar aceite usado.  
15.2. Valora la obtención de jabón como un método de reciclaje conocido desde la antigüedad y utilizado todavía en la actualidad.  
16. Buscar información de recetas de fabricación de jabón.  
16.1. Pregunta a abuelos, padres o vecinos por recetas de jabón valorando los conocimientos y la experiencia de los mayores.  
16.2. Busca información usando las TIC para la obtención de jabón.  
17. Conocer las características de la saponificación.  
17.1. Manipula la sosa con precaución y reconoce su disolución como exotérmica.  
17.2. Reconoce la lentitud de la reacción.  
17.3. Identifica la glicerina como segundo producto.  
18. Conocer las propiedades del jabón que le hacen útil para la limpieza.  
18.1. Conoce la parte liposoluble e hidrosoluble del jabón.  
19. Reconocer el carácter ácido o básico de una sustancia.  
19.1. Relaciona la composición química de una sustancia con su carácter ácido o básico según la teoría de Arrhenius.  
20. Conocer e interpretar la escala de pH.  
20.1. Conoce la escala de pH.  
20.2. Conoce la existencia de indicadores.  
20.3. Conoce la escala de colores del indicador universal.  
21. Identificar ácidos y bases utilizando indicadores y pH-metro.  
21.1. Determina la acidez o basicidad de una sustancia por su pH.  
21.2. Maneja el pH-metro.21.3. Interpreta los colores de un indicador.  
22. Interpreta la reacción entre ácidos y bases como una neutralización.  
22.1. Comprende la neutralización de un ácido con una base.  
23. Comprender la importancia de ácidos y bases en nuestra vida.  
23.1. Identifica el carácter ácido y básico de sustancias de la vida diaria.  
23.2. Comprende las técnicas de acidificación para conservación de alimentos.  
24. Conocer los fundamentos teóricos de la cristalización y la destilación como métodos para separar los componentes de una disolución.  
24.1. Conoce los fundamentos teóricos de las técnicas experimentales de separación de componentes de una disolución.  
24.2. Comprende el concepto de solubilidad.  
25. Utilizar las técnicas de cristalización y destilación experimentalmente.  
25.1. Realiza la cristalización del sulfato de cobre.  
25.2. Realiza el montaje de la destilación.  
26. Reconocer la cristalización como técnica de obtención de sal del agua de mar.  
26.1. Reconoce la aplicación de la técnica de la cristalización en la obtención de sal del agua de mar.  
27. Comprender la importancia de los polímeros en el mundo actual  
27.1. Entiende la importancia actual de los polímeros.  
28. Diferenciar las propiedades de los distintos tipos de polímeros.  
28.1. Diferencia polímeros naturales y sintéticos; homopolímeros y copolímeros.  
28.2. Diferencia entre fibras, plásticos y elastómeros según sus propiedades y usos.  
29. Realizar reacciones de polimerización.  
29.1. Obtiene polímeros sintéticos y naturales experimentalmente.  
30. Conocer los problemas medioambientales que pueden surgir.  
30.1. Toma conciencia de la necesidad del reciclaje de los plásticos.  

