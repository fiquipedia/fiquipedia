
# Ejercicios

Borrador, muchos ejercicios están asociados a otros temas y en otras páginas  
Pendiente reorganizar: agrupar física, química, recursos comunes ...  
Fuente de ejercicios son  [pruebas acceso universidad](/home/pruebas/pruebasaccesouniversidad) ,  [pruebas libres graduado ESO](/home/pruebas/pruebas-libres-graduado-eso) ,  [pruebas acceso Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio) , pruebas acceso Grado Superior, ...  
Para temas de formulación, ir a la página de  [formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion)  

Con el tiempo se pueden separar ejercicios de actividades. Algunas pueden ser transversales a varios contenidos.  

Pendiente organizar diferenciación / similitud y hacer referencias cruzadas. La denominación como ejercicio, actividad o tarea es algo que puede depender del contexto y del docente, aunque en general se busca que el alumno tenga comprensión y sea competente en el uso de los contenidos adquiridos.

[Distinción entre ejercicio, actividad y tarea - intef.es](https://formacion.intef.es/tutorizados_2013_2019/pluginfile.php/49912/mod_imscp/content/5/b_distincin_entre_ejercicio_actividad_y_tarea.html)  
![](https://formacion.intef.es/tutorizados_2013_2019/pluginfile.php/49912/mod_imscp/content/5/B02_T1_Imagen_6.jpg)  


##  Ejercicios en la página
En esta página concreta pongo enlaces.  
En fiquipedia en general están dispersos: me centré primero en 2º Bachillerato, Química y Física, con apuntes y problemas por bloque y nivel para no usar libro de texto  
[EvAU Física](/home/pruebas/pruebasaccesouniversidad/paufisica)  
[EvAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica)   
Además de eso, tengo algunos ejercicios adicionales de ampliación de cosas, pero dispersos, por ejemplo  
[Ejercicios elaboración propia Física 2º BAchillerato](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato) y en otros sitios  
[ActividadesParticulas.pdf](/home/recursos/recursos-de-elaboracion-propia/ActividadesParticulas.pdf)  
Para el resto de cursos, en 2017 empiezo a ntentar hacer "pizarras" para sustituir la teoría y no usar libro de texto, el curso anterior hice 2º, 3º y 4º ESO y en 2018 hgo 1º Bachillerato  
[Pizarras Física y Química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel)  
Ejercicios asociados y soluciones tengo pero ponerlos con licenciamiento correcto lleva tiempo.  
He subido algunos enunciados de momento, de nuevo dispersos  
Por ejemplo en  
[Recursos cinemática](/home/recursos/fisica/cinematica)  
[Recursos dinámica](/home/recursos/fisica/dinamica) 

##  Enlaces a ejercicios en páginas externas

[NO ME SALEN - Creación de Ricardo Cabrera, un profe políticamente incorrecto. EJERCICIOS RESUELTOS Y CONTENIDOS TEÓRICOS DE FÍSICA Y BIOFÍSICA ](https://ricuti.com.ar/intro_NMS.html)  

Se comentan algunos en recursos educativos abiertos dentro de [libros](/home/recursos/libros)  
 [Enunciados de problemas 3º ESO](http://didacticafisicaquimica.es/enunciados-problemas-3oeso/)  
 [Enunciados de problemas 4º ESO](http://didacticafisicaquimica.es/enunciados-problemas-4oeso/)  
 [Enunciados de problemas 1º Bachillerato](http://didacticafisicaquimica.es/enunciados-de-problemas-1o-bachiller/)  
 [Enunciados de problemas 2º Bachillerato](http://didacticafisicaquimica.es/enunciados-de-problemas-2o-bachiller/)  

 [https://www.profesor10demates.com/2013/10/fisica-genera.html#](https://www.profesor10demates.com/2013/10/fisica-genera.html#) Muchos ejercicios por tipo y también resueltos  

 [https://sites.google.com/site/fqmanueldiaz/](https://sites.google.com/site/fqmanueldiaz/)  Manuel Díaz Escalera, licenciamiento no detallado aunque indica "Hojas de ejercicios que puedes imprimir o modificar libremente"Tiene miles de ejercicios, por curso y bloques. Problemas resueltos de física y química   
 
 [http://www.profesorparticulardefisicayquimica.es](http://www.profesorparticulardefisicayquimica.es) Ejercicios por nivel (3º, 4º ESO, 1º y 2º Bachillerato) y por bloques. Profesor: A. Zaragoza López  
 
 [https://ejerciciosdefisica.com/](https://ejerciciosdefisica.com/) "Problemas y Ejercicios de Física para ResolverEn Ejercicios de física, encontraras la información más completa y de calidad sobre la física, además de fichas de trabajo y materiales educativos gratuitos que contienen problemas resueltos y para resolver, ademas de un formulario de física en cada uno de los contenidos."  
 
 El Sapo Sabio. Ejercicios resueltos de Química  
 [http://www.elsaposabio.com/quimica/](http://www.elsaposabio.com/quimica/)  [](http://www.elsaposabio.com/quimica/)  
 [http://www.elsaposabio.com/fisica/](http://www.elsaposabio.com/fisica/)  
 
 [http://www.comoseresuelvelafisica.com/](http://www.comoseresuelvelafisica.com/)  Ejercicios de Fisica y Quimica resueltos paso a paso en video. Copyright © 2011  
 Canal es  [https://www.youtube.com/user/comoseresuelve](https://www.youtube.com/user/comoseresuelve) y cuenta de Twitter @institweet
 
  Lo habitual no es encontrar ejercicios aislados, sino asociados a enlaces de recursos donde al hay mismo tiempo  [apuntes](/home/recursos/apuntes)  y ejercicios  
  Por ejemplo  
  
  [http://www.juliweb.es/fisica/ejercicios/problemascinematica.pdf](http://www.juliweb.es/fisica/ejercicios/problemascinematica.pdf)  
  
 [Disoluciones ejercicios y problemas resueltos . Química - profesor10demates.blogspot.com.es](http://profesor10demates.blogspot.com.es/2013/04/disoluciones-ejercicios-y-problemas_28.html)   
    [http://jpcampillo.es/apuntes.html](http://jpcampillo.es/apuntes.html)  [http://jpcampillo.es/problemas/index.html](http://jpcampillo.es/problemas/index.html)  Apuntes y problemas ESO y Bachillerato, cc-by-nc Enlaces validados julio 2015 
    
 "No me salen", EJERCICIOS RESUELTOS Y CONTENIDOS TEÓRICOS DE FÍSICA Y BIOFÍSICA [http://neuro.qi.fcen.uba.ar/ricuti/intro_NMS.html](http://neuro.qi.fcen.uba.ar/ricuti/intro_NMS.html)  Ricardo Cabrera, cc-by-nc-nd  
  
 FÍSICA Y QUÍMICA EJERCICIOS Actividades de refuerzo o ampliación para: Física y Química : 3º ESO , 4º ESO y 1º Bachillerato. Biología y Geología : 3º ESO. [http://fyqicod5.blogspot.com.es/](http://fyqicod5.blogspot.com.es/)  ELISAIDA FEBLES  
 
 Ciencias Naturales 8° básico. CN08 AE 1.01 Caracterizar la estructura interna de la materia, basándose en los modelos atómicos desarrollados por los científicos a través del tiempo. [http://www.curriculumenlineamineduc.cl/605/w3-article-18573.html](http://www.curriculumenlineamineduc.cl/605/w3-article-18573.html) Ministerio de Educación Chile. Licenciamiento cc-by  
 
  PhyStorming [http://matrix.fis.ucm.es/phystorm/problemas](http://matrix.fis.ucm.es/phystorm/problemas)  Selección de problemas distribuidos en categorías.  
  
  [http://ejercicios-fyq.com](http://ejercicios-fyq.com)  
  
  Ejercicios Física y Química 3º ESO.  [http://chopo.pntic.mec.es/jmillan/ejercicios_3n.pdf](http://chopo.pntic.mec.es/jmillan/ejercicios_3n.pdf) Pdf 24 páginas con ejercicios de todos los bloques del curso IES "Rey Fernando VI" @ Jesús Millán Crespo  
  
  [http://www4.uva.es/goya/Intranet/Pages/SelAccClave.asp?p_Asignatura=3](http://www4.uva.es/goya/Intranet/Pages/SelAccClave.asp?p_Asignatura=3)  Universidad de Valladolid, Departamento de Física de la Materia Condensada Cristalografía y Mineralogía,Consulta de cuestiones y problemas física por clave Física 1 (Biólogos y Geólogos), 1er Cuatrimestre 2012, Cátedra L. Szybisz  
  
[http://materias.df.uba.ar/f1bygAa2013c1/files/2013/03/Guia-Completa.pdf](http://materias.df.uba.ar/f1bygAa2013c1/files/2013/03/Guia-Completa.pdf)  

[http://fqsecundaria.blogspot.com.es/search/label/Relaciones de problemas](http://fqsecundaria.blogspot.com.es/search/label/Relaciones%20de%20problemas) Diego Palacios Guías de trabajos prácticos, Andrea Paola Buccino  

[http://cms.iafe.uba.ar/abuccino/guias.htm](http://cms.iafe.uba.ar/abuccino/guias.htm)  


Tests por materia y nivel (mucho más que física y química, más que secundaria)  
[http://www.testeando.es/index.asp](http://www.testeando.es/index.asp)  

[http://6con02.com/ejercicios.html](http://6con02.com/ejercicios.html)  Mecánica, Física molecular y calor, Electromagnetismo , Oscilaciones y Ondas, Optica [http://www.fisica.ru/2017/theory/ejercicios.php](http://www.fisica.ru/2017/theory/ejercicios.php)  

[twitter LebreroPastor/status/1265295317941043201](https://twitter.com/LebreroPastor/status/1265295317941043201)  
Algunos me pedisteis si podía compartir los ejercicios "para evaluar en estado de alarma" en formato pdf. @fruzgz @quirogafyq @garlan2. Aquí os dejo el enlace:  
[carpeta ejercicios](https://drive.google.com/drive/folders/16tstwBKhDf8NJZt6hxmoSVsK5mcs1SKr)  

## Actividades
En LOMLOE Madrid llama actividades a las situaciones de aprendizaje, ver [Situaciones de aprendizaje](/home/legislacion-educativa/lomloe/curriculo-situaciones-de-aprendizaje) donde aparecen algunos ejemplos.  

[twitter quirogafyq/status/1295385431174008838](https://twitter.com/quirogafyq/status/1295385431174008838)  
Buenas!  
Un grupo de profes y yo hemos creado un grupo de trabajo para elaborar materiales de FyQ.  
Quiero compartir con vosotros la web donde los recogemos, van detallados por curso, resueltos e incluyen propuestas de evaluación.  
[Grupo de trabajo de física y química](https://sites.google.com/view/materialesfyq)  cc-by-nc  


[Collection of Solved Problems in Physics - physicstasks.eu ](http://physicstasks.eu/en)  
This collection of Solved Problems in Physics is developed by Department of Physics Education, Faculty of Mathematics and Physics, Charles University in Prague since 2006.  
The Collection contains tasks at various level in mechanics, electromagnetism, thermodynamics and optics. The tasks mostly do not contain the calculus (derivations and integrals), on the other hand they are not simple such as the use just one formula. Tasks have very detailed solution descriptions. Try to solve the task or at least some of its parts independently. You can use hints and task analysis (solution strategy).  
Difficulty level  
Level 1 – Lower secondary level   
Level 2 – Upper secondary level   
Level 3 – Advanced upper secondary level   
Level 4 – Undergraduate level   

[twitter ProfaDeQuimica/status/1268147781706883072](https://twitter.com/ProfaDeQuimica/status/1268147781706883072)  
ProfesFrikis, al fin os traigo la actividad que realizaré en junio con 2ºESO: La aventura de #rol de #FyQ "La bruma maldita", con #Magissa de @Edanna y @Nosolorol. 🥰  
MIL gracias, @rolerodehamelin , por tus consejos.  
[La bruma maldita (carpeta google drive)](https://drive.google.com/drive/folders/16W27d1MYSuZMqCdWswODji9_ehOufGGz)  
![](https://pbs.twimg.com/media/EZlcaRoWkAAFrzC?format=jpg)  
![](https://pbs.twimg.com/media/EZlceFzX0AA9xZs?format=jpg)  

## Generadores de ejercicios 

Enlazan con simulaciones  
[Generadores de Física, Estos generadores proponen ejercicios, cada vez con datos diferentes, y proporcionan ayudas hasta resolverlos.](https://fisicayquimicaes.wordpress.com/genradores-de-fisica/)  Vicente Martín Morales, Geogebra   
[Generadores de Química Estos generadores proponen ejercicios, cada vez con datos diferentes, y proporcionan ayudas hasta resolverlos.](https://fisicayquimicaes.wordpress.com/generadores-de-quimica/)  Vicente Martín Morales  

* [Estequiometría elemental](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/estequiometria_elemental_jquery.htm?t=1466098457722)
* [Estequiometría](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/estequiometria_jquery.htm?t=1466098473450)
* [Equilibrio químico](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/equilibrio_quimico_jquery.htm?t=1466098434752)
* [Igualar reacciones](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/igualacion_reacciones_jquery.htm?t=1466103169291)  
* [Disociación. Cálculo del pH](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/disociacion_jquery.htm?t=1466098417872)
* [Hidrólisis](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/hidrolisis_jquery.htm?t=1466103151912)
* [Reacciones Redox](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/redox_jquery.htm?t=1466103185856)


##  Bancos de preguntas para ejercicios
Con el tema de clases no presenciales es interesante ver ejercicios en formatos que se puedan reutilizar en cuestionarios online.Hay variantes de usar cloze, wiris, geogebra, bancos de preguntas ...  
  
[Bancos de preguntas en Moodle XML - docentesconeducacion.es](http://www.docentesconeducacion.es/viewtopic.php?f=92&t=9046)  
