
# Electroquímica

Es un bloque asociado a Química de 2º de Bachillerato  
Enlaza con estados de oxidación, ajustes redox, pilas y electrólisis  

[chemwiki.ucdavis.edu  Electrochemistry](http://chemwiki.ucdavis.edu/Analytical_Chemistry/Electrochemistry)  
The ChemWiki is a collaborative approach toward chemistry education where an Open Access textbook environment is constantly being written and re-written by students and faculty members resulting in a free Chemistry textbook to supplant conventional paper-based books. The development of the ChemWiki is currently directed by UC Davis Professor Delmar Larsen.  
cc-by-nc-sa  

[chem1.com All about electrochemistry](http://www.chem1.com/acad/webtext/elchem/index2.html)   
© 2005 by Stephen Lower - Simon Fraser University - Burnaby/Vancouver Canada. cc-by  

[fisiquimicamente Equilibrio y ajuste rédox, pilas galvánicas y electrolisis](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2bach/quimica/redox/)  

[http://group.chem.iastate.edu/Greenbowe/sections/projectfolder/simDownload/index4.html#electrochem](http://group.chem.iastate.edu/Greenbowe/sections/projectfolder/simDownload/index4.html#electrochem)  
Chemistry Experiment Simulations and Conceptual Computer Animations, Chemical Education Research Group,Department of Chemistry, Iowa State University  

Simulador pilas con distintos electrodos  
[http://salvadorhurtado.wikispaces.com/file/view/electroq.swf](http://salvadorhurtado.wikispaces.com/file/view/electroq.swf)  
Salvador Hurtado, cc-by  

Baterías  
[http://www.doitpoms.ac.uk/tlplib/batteries/index.php](http://www.doitpoms.ac.uk/tlplib/batteries/index.php)  
Pilas de combustible   
[http://www.doitpoms.ac.uk/tlplib/fuel-cells/index.php](http://www.doitpoms.ac.uk/tlplib/fuel-cells/index.php)  
University of Cambridge, cc-by-nc-sa  

Pila de limón y de patata, asociable a laboratorios  
[https://es.wikipedia.org/wiki/Pila_de_limón](https://es.wikipedia.org/wiki/Pila_de_lim%C3%B3n)  

En la pila de limón y similares, al ver Zn y Cu los alumnos suelen indicar que el Cu es el cátodo sin pensar en el medio ácido 

[Pilas Zinc-Cobre](https://www.ucm.es/data/cont/docs/76-2013-07-11-17_Copper-Zinc_batteries.pdf)  

> Notar que no se produce la reducción de Cu<sup>2+<sup> a Cu en la placa de Cu debido a que no hay iones Cu<sup>2+<sup> en la disolución


* [Generador ejercicios reacciones Redox](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/redox_jquery.htm?t=1466103185856) Vicente Martín Morales


[El nuevo 'combustible líquido eléctrico' más potente que las baterías de litio - elconfidencial.com](https://www-elconfidencial-com.cdn.ampproject.org/c/s/www.elconfidencial.com/amp/tecnologia/novaceno/2022-08-10/crean-combustible-almacena-electricidad-bateria_3473820/)  
> "El exclusivo formato de líquido de alta densidad energética de las baterías de flujo NEF permite utilizar los mismos fluidos en diferentes dispositivos, lo que significa que el fluido, cargado en la estación de recarga a partir de fuentes de energía renovables o de una red, puede utilizarse para repostar rápidamente vehículos, o para el almacenamiento estacionario y otras aplicaciones portátiles de gran tamaño", afirma Elena Timofeeva, jefa de operaciones, directora de investigación y desarrollo de Influit y profesora de investigación química en Illinois Tech. "El fluido descargado puede devolverse a una estación de recarga-reabastecimiento para ser recargado o cargarse dentro del dispositivo enchufándolo a una fuente de energía".   

[![](https://img.youtube.com/vi/8333g246i3/0.jpg)](https://www.youtube.com/watch?v=8333g246i3 "Darpa Nanoelectrofuel Flow Battery")  


## Ejemplos de reacciones 

[Making silver mirrors using chemistry - compoundchem.com](https://www.compoundchem.com/2017/09/06/silver-mirror/)  
![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2017/09/Making-Silver-Mirrors-using-Chemistry-Oct-2017.png?w=1323&ssl=1)  

[Making a mirror using silver nitrate and sugar - rsc.org](https://edu.rsc.org/experiments/making-a-mirror-using-silver-nitrate-and-sugar/822.article)  

[![](https://img.youtube.com/vi/atCtLsh4MBs/0.jpg)](https://www.youtube.com/watch?v=atCtLsh4MBs "Watch This Artist Create A Mirror Using Glass And Silver Nitrate. Tech Insider")   

Hilo muy largo con ejemplos reales   
[twitter Llorens_MRC/status/1686417196027727895](https://twitter.com/Llorens_MRC/status/1686417196027727895)  
Lo de este hilo debería ser "Electroquímica", pero yo no me atrevo a calificarlo más allá de "Electroalquimia". En todo caso es una asignatura pendiente de algo que llevo tiempo queriendo hacer pero nunca me puse en ello...  
El caso es que comenzó con un viejo SalesKit receptor regenerativo de VHF, al que decidí platearle la bobina para mejorar sus características, y recordé la simple fórmula para hacerlo: hilo de cobre, algo de plata, salfumán y una simple fuente de alimentación de 0-20 Volt  
Así pues, tras limpiar el cobre a conciencia he conectado la lámina de plata como ánodo y la bobina de cobre como cátodo, todo ello en un pequeño vaso de cristal borosilicado en donde he vertido el ácido clorhídrico al 10%...  
He ajustado la corriente inicial a 250-300 mA, pero por algún motivo ha disminuido de forma rápida... Aquí el burbujeo en el cátodo de cobre indica que se efectúa la reducción del ión plata+ con los electrones de la fuente, quedando plata metálica sobre el substrato de cobre..  
...Y eso es la teoría, claro está, porque el resultado fue decepcionante ...En más o menos media hora la bobina de cobre se recubrió de una gruesa capa de color granate, pero sin consistencia, que se deshacía nada más tocarla...  
...El caso es que al limpiar la bobina ha aparecido el cobre exactamente igual que antes del procedimiento...  Así que habrá que probarlo otra vez, porque alguna variable me ha jugado una mala pasada... y eso que en los tutoriales parece la mar de fácil...  
Vale, la primera experiencia "Electroalquímica" no ha salido bien, lo reconozco, pero como me gusta insistir reiniciaré con algo probablemente más fácil, intentando cubrir de zinc una pequeña moneda de 2 cts...  
Así pues, tomo un trozo de zinc de una vieja pila de 1,5 V como ánodo. El problema (eso un químico ya lo habría supuesto), es que el ácido ya reacciona directamente con este metal, burbujeando hidrógeno y formando cloruro de zinc y desapareciendo aún sin corriente...  
Así que por el método de prueba-error, voy disminuyendo con agua destilada la concentración de ácido hasta más o menos el 2%, con lo cual el ataque directo al zinc se limita bastante y puedo dar corriente y ver qué pasa...  
Y el resultado no ha sido malo... La moneda ha quedado recubierta de zinc de forma evidente, la cara frontal más que la otra, claro, mientras que el ánodo se ha comido casi completamente, aunque más por la reacción directa del ácido que por la transferencia de iones al cátodo...  
La deposición de zinc debe ser corta, porque es un metal que tiende a cristalizar muy rápido, quedando una superficie rugosa e incluso con dentritas, pero con poco tiempo y corriente, una vez pulida ligeramente la moneda no ha quedado mal... aquí comparada con otra original...  


## Electrolisis
Enlaza con laboratorio y con [hidrogeno](/home/recursos/quimica/hidrogeno) 

[![](https://img.youtube.com/vi/gZJEDe_HUcw/0.jpg)](https://www.youtube.com/watch?v=gZJEDe_HUcw "Water Electrolysis Kit(hydrogen and oxygen separated)")  
  
[twitter onio72/status/1485636481289011201](https://twitter.com/onio72/status/1485636481289011201)  
¿Algún profe de FQ que haya hecho esta experiencia: electrólisis de agua con NaCl en presencia de campo magnético?  
![](https://pbs.twimg.com/media/FJ4KepCXIAgfhVx?format=jpg)  

[Magnetoelectrolisis - mientrasenfisicas](https://mientrasenfisicas.wordpress.com/2016/10/20/magnetoelectrolisis/)
![](https://mientrasenfisicas.files.wordpress.com/2016/10/sin-tc3adtulo.jpg)  

[Movement of Ions During Electrolysis - racichemedcentral.com.au](https://racichemedcentral.com.au/icq/ancq-chemical-resources/movement-of-ions-during-electrolysis)  

[Effect of Magnetic Field on HER of Water Electrolysis on Ni–W Alloy](https://link.springer.com/article/10.1007/s12678-017-0382-x)  

[![](https://img.youtube.com/vi/6sAHryltCQ4/0.jpg)](https://www.youtube.com/watch?v=6sAHryltCQ4 "Electrolysis in a magnetic field")  

[![](https://img.youtube.com/vi/0KmqinCxBho/0.jpg)](https://www.youtube.com/watch?v=0KmqinCxBho "Petri Dish Electrolysis")  
 
[twitter Biochiky/status/1536073040651505664](https://twitter.com/Biochiky/status/1536073040651505664)  
Estudio de las reacciones REDOX con el Camaleón Químico. Se ve el cambio de color del KMnO4 según el manganeso va cambiando su e.o. por la presencia de la glucosa del chupa chups.   
[EL CAMALEÓN QUÍMICO - mediateca](https://mediateca.educa.madrid.org/video/e5z437rd8e8iplx1) vía @educamadrid
Fte: [Química de colores: reacciones redox con chupachups ](https://www.scienceinschool.org/es/article/2018/colourful-chemistry-redox-reactions-lollipops-es/)  

##  Cómics
Dentro de  [http://cationdesigns.blogspot.com.es/p/high-school-science-teacher-resources.html](http://cationdesigns.blogspot.com.es/p/high-school-science-teacher-resources.html)  

[![http://cationdesigns.blogspot.com.es/p/high-school-science-teacher-resources.html](http://2.bp.blogspot.com/-Mg4aUaVNqOQ/TwZQiVvrePI/AAAAAAAABfU/Vy-v5apZY_Y/s1600/Electromotive+Force+Series.jpg "http://cationdesigns.blogspot.com.es/p/high-school-science-teacher-resources.html") ](http://2.bp.blogspot.com/-Mg4aUaVNqOQ/TwZQiVvrePI/AAAAAAAABfU/Vy-v5apZY_Y/s1600/Electromotive+Force+Series.jpg)  
