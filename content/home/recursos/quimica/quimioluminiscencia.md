
# Recursos quimioluminiscencia

Puede estar asociada a bioluminiscencia

[Juan Carlos Cedrón, “El Luminol”, Revista de Química PUCP, vol.25, no.1-2.](http://revistas.pucp.edu.pe/index.php/quimica/article/view/4606/4583)

[Emma Welsh, “What is chemiluminescence?”, Science in school, 2011.](https://www.scienceinschool.org/2011/issue19/chemiluminescence)  
[Box 1: Luminol, a glow-in-the-dark chemical](https://www.scienceinschool.org/article/2011/chemiluminescence/#box1)  
![](https://www.scienceinschool.org/wp-content/uploads/2011/05/issue19chemiluminescence2_l.jpg)  
![](https://www.scienceinschool.org/wp-content/uploads/2011/05/issue19chemiluminescence5_xl.jpg)  

¡Cómo hacer LUZ LÍQUIDA! | La Quimioluminiscencia  
[![](https://img.youtube.com/vi/ZALMJMUl7vk/0.jpg)]((https://www.youtube.com/watch?v=ZALMJMUl7vk "¡Cómo hacer LUZ LÍQUIDA! | La Quimioluminiscencia")   

Reacción del luminol con hipoclorito sódico | Reacción de quimioluminiscencia  
[![](https://img.youtube.com/vi/Ij9PDVhe6OI/0.jpg)]((https://www.youtube.com/watch?v=Ij9PDVhe6OI "Reacción del luminol con hipoclorito sódico | Reacción de quimioluminiscencia")   

[The Chemiluminescence of Luminol - Declan Fleming - University of Bristol](https://www.chm.bris.ac.uk/webprojects2002/fleming/experimental.htm)

> Luminol Solution  
    Dissolve 4g Na2CO3 in 500cm3 H2O. (This brings the pH to ~11 which I found to be optimum for dissolving Luminol).  
    Add 0.2g Luminol and dissolve  
    Add 25g Sodium Bicarbonate and 0.2g Ammonium Carbonate (this re-buffers to ~10.5 which I found to be optimum for this reaction).  
    Dilute to 1dm3 (use a 25cm3 portion for each reaction)  
> Oxidising solution  
-With K3[Fe(CN)6]
    0.76g K3[Fe(CN)6]
    Dissolve in 25cm3 H2O2 5%

[Taller de Química - Reacciones. Quimioluminiscencia - madrimasd](https://www.madrimasd.org/cienciaysociedad/taller/quimica/reacciones/quimioluminiscencia/default.asp)

[Chemiluminescence of luminol: a cold light experiment - edu.rsc.org/](https://edu.rsc.org/resources/chemiluminescence-of-luminol-a-cold-light-experiment/823.article)  

[Chemiluminescence - the oxidation of luminol - edu.rsc.org/](https://edu.rsc.org/exhibition-chemistry/chemiluminescence-the-oxidation-of-luminol/2020040.article)  

[Luminol and Chemiluminescence - physicsopenlab.org](https://physicsopenlab.org/2019/02/06/luminol-2/)  
![](https://physicsopenlab.org/wp-content/uploads/2019/02/LuminolCompare.png)  

[Luminol-Based Chemiluminescent Signals: Clinical and Non-clinical Application and Future Uses, Appl Biochem Biotechnol. 2014 May; 173(2): 333–355. ](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4426882/)  

[Electroquimioluminiscencia del luminol usando electrodos de bajo costo, Anales de la Real Sociedad Española de Química Abril Junio 2004](https://dialnet.unirioja.es/descarga/articulo/885966.pdf)  

[Direct and Indirect Chemiluminescence: Reactions, Mechanisms and Challenges, Molecules. 2021 Dec; 26(24): 7664. ](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8705051/)  

[Chemiluminescence from the reaction between hypochlorite and luminol, J. Phys. Chem. 1975, 79, 2, 101–106](https://pubs.acs.org/doi/pdf/10.1021/j100569a002)  

[2.1: Luminol - libretexts.org](https://chem.libretexts.org/Bookshelves/Analytical_Chemistry/Supplemental_Modules_(Analytical_Chemistry)/Analytical_Chemiluminescence/2%3A_Chemiluminescence_Reagents/2.01%3A_Luminol)  

[The Role of Metal Ions and Metallo-Complexes in Luminol Chemiluminescence (Catalysis), Nekimken, Howard Lewis, 1986](https://www.ideals.illinois.edu/items/70485)

[El cambio de color de un láser verde al atravesar aceite de oliva virgen - francis.naukas.com](https://francis.naukas.com/2018/02/05/el-cambio-de-color-de-un-laser-verde-al-atravesar-aceite-de-oliva-virgen/)  
> a calidad de un aceite de oliva virgen se puede estudiar mediante espectrofluorimetría gracias a que contiene compuestos fluorescentes y luminiscentes. Entre los fluorescentes tenemos tocoferoles, pigmentos (clorofilas, carotenos y feofitinas), fenoles y vitamina E.  
![](https://francis.naukas.com/files/2018/02/Dibujo20180201-three-lasers-three-oils-photo-joaquin-sevilla1-580x244.jpg)  

[¿Cuál es el mecanismo de la fosforescencia? - curioseando.com](https://curiosoando.com/mecanismo-de-la-fosforescencia)  

[Inhibition of Bleach-Induced Luminol Chemiluminescence*](http://www.themurderofmeredithkercher.net/docupl/filelibrary/docs/forensics/2002-07-24-Article-Kent-et-al-Inhibition-bleach-luminol.pdf)  
> FIG. 1—Plot showing predicted reactivity of luminol (___), hydrogen peroxide (- - -), TRIS, and 1,2-diaminoethane with hypochlorite as a function of pH. Curves for the amines are based on equation 4 in the text, estimating the rate constants from Eq 3. Curves for luminol and H2O2 are derived from analogous equations using literature values 


## Diagrama Jablonski

[Jablonski diagram - goldbook.iupac.org](https://goldbook.iupac.org/terms/view/J03360)  

[Jablonski diagrama - wikipedia](https://en.wikipedia.org/wiki/Jablonski_diagram)  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/JablonskiSimple.png/772px-JablonskiSimple.png?20130610131925)  
> In molecular spectroscopy, a Jablonski diagram is a diagram that illustrates the electronic states and often the vibrational levels of a molecule, and also the transitions between them. The states are arranged vertically by energy and grouped horizontally by spin multiplicity. Nonradiative transitions are indicated by squiggly arrows and radiative transitions by straight arrows. The vibrational ground states of each electronic state are indicated with thick lines, the higher vibrational states with thinner lines.  

[Jablonski diagram - chem.libretexts.org](https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Spectroscopy/Electronic_Spectroscopy/Jablonski_diagram)  
> A Jablonski diagram is basically an energy diagram, arranged with energy on a vertical axis.  
> Every column usually represents a specific spin multiplicity for a particular species.  


[H. H. Jaffe and Albert L. Miller "The fates of electronic excitation energy" J. Chem. Educ., 1966, 43 (9), p 469 DOI:10.1021/ed043p469](https://pubs.acs.org/doi/10.1021/ed043p469)  

Conceptos:  
**Estados vibracionales y niveles de energía vibracionales**  
> Hence it is easy to see that the density of vibrational levels of polyatomic molecules is very great and extremely complex.  
In spite of the above discussion, the conventional pseudo-diatomic diagrams will continue to be drawn, but the reader should bear in mind the complexity.  

**Multiplicidad de spin**  
[Spin multiplicity - goldbook.iupac.org](https://goldbook.iupac.org/terms/view/M04062)  
>The number of possible orientations, calculated as 2 S+1, of the spin angular momentum corresponding to a given total spin quantum number (S), for the same spatial electronic wavefunction.  

[Multiplicity - wikipedia](https://en.wikipedia.org/wiki/Multiplicity_(chemistry))  
>In spectroscopy and quantum chemistry, the multiplicity of an energy level is defined as 2S+1, where S is the total spin angular momentum.States with multiplicity 1, 2, 3, 4, 5 are respectively called singlets, doublets, triplets, quartets and quintets.  


**Relajación vibracional**  
Proceso que no radia, se pasa a energía cinética. Se indica con línea curvada. Proceso muy rápido 10^-14 a 10^-11 s  
Ocurre entre niveles vibracionales, normalmente no se pasa a niveles electrónicos.  

**Conversión interna**  
Es una relajación vibracional entre estados vibracionales de un nivel y estados vibracionales de otro nivel, que puede ocurrir si se solapan

**Fluorescencia**  
Proceso que radia emitiendo luz. Se indica con línea recta. Proceso lento 10^-9 a 10^-7 s  

**Cruce intersistema** (Inter System Crossing (ISC))  
[Cruce intersistema - wikipedia](https://es.wikipedia.org/wiki/Cruce_intersistema)  
Proceso que no radia. El electrón pasa a otro estado con diferente muliplicidad: el espín del electrón excitado se invierte.  
Se representa con flecha "horizontal / lateral". Es un proceso lento.  
Es una transición prohibida según reglas electrónicas pero se permite débilmente por los estados vibracionales.  
![](https://es.wikipedia.org/wiki/Cruce_intersistema)  

|Transition |	Time Scale |	Radiative Process?|
|:-:|:-:|:-:| 
|Internal Conversion |	10-14 - 10-11 s |	no |
|Vibrational Relaxation |	10-14 - 10-11 s |	no |
|Absorption |	10-15 s |	yes |
|Phosphorescence |	10-4 - 10-1 s |	yes |
|Intersystem Crossing |	10-8 - 10-3 s |	no |
Fluorescence |	10-9 - 10-7 s |	yes|

## Bioluminiscencia

[ISBC International Society for Bioluminescence and Chemiluminescence](http://www.isbc-society.org/)  

[twitter Massimo20_ES/status/1286979507920887808](https://twitter.com/Massimo20_ES/status/1286979507920887808)  
Los montículos del Parque Nacional de las Emas en Brasil (altos hasta 7 metros), están habitados también por el piróforo, un escarabajo luminoso similar a la luciérnaga que pone sus huevos ahí. Cuando las larvas nacen, brillan para atraer insectos de los cuales alimentarse.
![](https://pbs.twimg.com/media/Edw_P8jWoAAHfrt?format=jpg)  

[Luces en el cielo y en el mar: la ciencia detrás del espectáculo natural - muyinteresante](https://www.muyinteresante.com/ciencia/65269.html)  
> La luminiscencia es un fenómeno físico en el cual un material emite luz cuando se le aplica una fuente de energía. Dependiendo del origen de esa fuente, se pueden identificar diferentes tipos de luminiscencia.   
Entre los más conocidos destacan la quimioluminiscencia (la luz se origina a partir de reacciones en sistemas químicos), la electroluminiscencia (la emisión de luz se produce cuando una muestra se somete a un campo eléctrico), la fluorescencia (un material irradiado con luz emite luz a una longitud de onda mayor), la fosforescencia (similar a la fluorescencia, pero el material sigue emitiendo luz durante un tiempo después de que se haya cesado la irradiación), la cátodoluminiscencia (se produce al bombardear algunos materiales con electrones de alta energía), la triboluminiscencia (la luz se genera cuando un objeto es sometido a fricción, ruptura o impacto mecánico) o la radioluminiscencia (algunos materiales emiten luz cuando se exponen a rayos gamma o rayos X).  
![](https://imagenes.muyinteresante.com/files/image_670_447/uploads/2024/07/02/6683c650255b2.jpeg) 

[5 animales que brillan en la oscuridad - muyinteresante](https://www.muyinteresante.com/naturaleza/23230.html)  
Aunque muchos afirman que esas hileras de colores cambiantes son producto también de la bioluminiscencia, sin embargo, no se trata de un fenómeno de emisión de luz, sino de la difracción. ...En efecto, esos cilios difractan la luz blanca que el ctenóforo recibe del ambiente, cambiando su coloración en función de la orientación de los cilios —y cambia también según desde dónde se le mire—.  
La bioluminiscencia de los ctenóforos es tenue, de un tono azul o verdoso, y solo es visible cuando se encuentra en una gran oscuridad.  
![](https://imagenes.muyinteresante.com/files/vertical_image_670/uploads/2022/10/13/6347e2aedd4d4.jpeg)  

[¿Cómo funciona la bioluminiscencia en la naturaleza? - nationalgeographic](https://www.nationalgeographic.es/animales/2019/05/como-funciona-la-bioluminiscencia-en-la-naturaleza)  


[Bioluminiscencia: combinando biología, química y biónica - scienceinschool.org](https://www.scienceinschool.org/es/article/2021/bioluminescence-combining-biology-chemistry-and-bionics/)  

[Los eventos de bioluminiscencia son uno de los fenómenos más vistosos del mar. También son una mala señal - xataka](https://www.xataka.com/ecologia-y-naturaleza/eventos-bioluminiscencia-uno-fenomenos-vistosos-mar-tambien-mala-senal)  

[Vivid biofluorescence discovered in the nocturnal Springhare (Pedetidae)](https://www.nature.com/articles/s41598-021-83588-0)  
[A new discovery of the bioluminescent terrestrial snail genus Phuphania (Gastropoda: Dyakiidae)](https://www.nature.com/articles/s41598-023-42364-y)  

[A study of marine luminiscence signatures NASA 1973](https://ntrs.nasa.gov/api/citations/19730016387/downloads/19730016387.pdf)  

[Luminiscence 1948 - nature](https://www.nature.com/articles/162423a0)  
