Proyecto para albergar la web [www.fiquipedia.es](www.fiquipedia.es) en GitLab pages  
La página está en http://fiquipedia.gitlab.io/fiquipedia que es donde apunta el dominio www.fiquipedia.es  
Puedes leer algo sobre FiQuiPedia aquí [FiQuiPedia: pasado, presente y futuro](https://algoquedaquedecir.blogspot.com/2020/10/fiquipedia-pasado-presente-y-futuro.html)

# Estructura del proyecto
Se trata de un proyecto de [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/): son páginas estáticas editadas en [markdown](https://markdown.es/) (extensión .md) que se generan automáticamente ante cambios con [GitLab CI/CD](https://docs.gitlab.com/ee/ci/index.html) según `.gitlab-ci.yml'   
Información sobre las carpetas del proyecto:  
- "content": contiene la página web en sí, los ficheros .md, además de los ficheros que estaban anexados inicialmente en sites (mayoría pdfs, de modo que fueran indexados por buscadores). En la imgración los pdfs solo se almacenan aquí como redirección.  
- "themes": contiene el tema hugo que es el usado inicialmente (no sé cómo se organiza en otros).  
- "public": contiene el fichero _redirects con las redirecciones a drive.fiquipedia (ver más información en ese proyecto) y robots.txt  

# Almacenamiento
Se apoya en el proyecto https://gitlab.com/fiquipedia/drive.fiquipedia/ que contiene los ficheros que antes migración estaban en Google drive por limitaciones espacio sites (inicialmente eran solo 100 MB), y que no eran indexados por buscadores. Ver más información en ese proyecto: los enlaces a Google Drive se sustituyen por enlaces a ese proyecto.  

Cada generación aumenta el Storage en una cantidad aproximadamente igual al Size del proyecto: se pueden eliminar jobs antiguos.

# Tema  / SSG 
La conversión de markdown a HTML se realiza con un SSG (Static Site Generator) y un tema.
Se inicia utilizando el tema beautiful hugo  https://pages.gitlab.io/hugo  
Más información en http://themes.gohugo.io/beautifulhugo/  
Para pruebas locales está en `localhost:1313/hugo/` tras ejecutar `hugo server`
Se pueden ver ejemplos en https://gitlab.com/pages  

# Migración
Se comenta en fichero separado  [README-migracion](https://gitlab.com/fiquipedia/fiquipedia/-/blob/master/README-migracion.md)

# Actualización
Se utiliza git desde línea de comandos para la versión inicial, pero desde navegador se puede editar o subir un fichero (de uno en uno) y se genera de nuevo la web estática al ser un proyecto CI/CD.

# Agradecimientos
Infinitas gracias a Joaquín por su ayuda en la migración, que incluye realizar https://github.com/joaquinOnSoft/gsites2md que ha permitido ahorrar un trabajo inmenso, como:  
- convertir los enlaces a drive a enlaces a ficheros locales del proyecto, opción `--replace`   
- cambiar descripción urls por texto amigable y que facilita visión móvil (por defecto en sites se pegaba url, se seleccionaba y se daba en icono enlace y quedaba con misma descripción que url, a veces muy larga), opción `--url`, complementada con la opción `--timeout` muy útil al haber tantos enlaces.  
