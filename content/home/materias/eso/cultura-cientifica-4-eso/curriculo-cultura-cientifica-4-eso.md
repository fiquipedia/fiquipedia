
### Currículo Cultura Científica 4º ESO

Ver DECRETO 48/2015 en  [legislación](/home/legislacion-educativa)  
Se encuentra en página 193 del pdf  
El decreto fija contenidos, criterios de evaluación y estándares de aprendizaje evaluables para la comunidad de Madrid.  
  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver Contenidos currículo con enlaces a recursos: 4º ESO Cultura Científica (LOMCE) (pendiente), pero se pueden ver [Recursos Cultura Científica 4º ESO](/home/recursos/recursos-por-materia-curso/recursos-cultura-cientifica-4-eso)     


Texto inicial sobre la materia de Cultura Científica en decreto Madrid, que cita Cultura Científica de 1º Bachillerato  


Tanto la ciencia como la tecnología son pilares básicos del bienestar de las naciones, y ambas son
necesarias para que un país pueda enfrentarse a nuevos retos y a encontrar soluciones para ellos. El
desarrollo social, económico y tecnológico de un país, su posición en un mundo cada vez más
competitivo y globalizado, así como el bienestar de los ciudadanos en la sociedad de la información y
del conocimiento, dependen directamente de su formación intelectual y, entre otras, de su cultura
científica. Que la ciencia forma parte del acervo cultural de la humanidad es innegable; de hecho,
cualquier cultura pasada ha apoyado sus avances y logros en los conocimientos científicos que se
iban adquiriendo y que eran debidos al esfuerzo y a la creatividad humana. Individualmente
considerada, la ciencia es una de las grandes construcciones teóricas del hombre, su conocimiento
forma al individuo, le proporciona capacidad de análisis y de búsqueda de la verdad.  
En la vida diaria estamos en continuo contacto con situaciones que nos afectan directamente, como
las enfermedades, la manipulación y producción de alimentos o el cambio climático, situaciones que
los ciudadanos del siglo XXI debemos ser capaces de entender. Repetidas veces los medios de
comunicación informan sobre alimentos transgénicos, clonaciones, fecundación in vitro, terapia
génica, trasplantes, investigación con embriones congelados, terremotos, erupciones volcánicas,
problemas de sequía, inundaciones, planes hidrológicos, animales en peligro de extinción, y otras
cuestiones a cuya comprensión contribuye la materia Cultura Científica. Otro motivo por el que la
materia Cultura Científica es de interés es la importancia del conocimiento y utilización del método
científico, útil no sólo en el ámbito de la investigación sino en general en todas las disciplinas y
actividades. Por tanto, se requiere que la sociedad adquiera una cultura científica básica que le
permita entender el mundo actual; es decir, conseguir la alfabetización científica de los ciudadanos.
Por ello esta materia se vincula tanto a la etapa de ESO como al Bachillerato. A partir de 4º de ESO,
la materia Cultura Científica establece la base de conocimiento científico, sobre temas generales
como el universo, los avances tecnológicos, la salud, la calidad de vida y los nuevos materiales. Para
1º de Bachillerato se dejan cuestiones algo más complejas, como la formación de la Tierra y el origen
de la vida, la genética, los avances biomédicos y, por último, un bloque dedicado a lo relacionado con
las tecnologías de la información y la comunicación.  


## Contenidos

### Bloque 1. Procedimientos de trabajo
1. Los métodos de la ciencia.  
- La investigación científica.  
2. La influencia de la ciencia en la evolución de las sociedades.  
- Condicionamientos históricos y sociales de la creación científica.  
3. Utilización de las Tecnologías de la Información y la Comunicación.  
4. Proyecto de Investigación.  

### Bloque 2. El Universo
1. La antigua astronomía.  
2. La investigación y la exploración del universo.  
- Los instrumentos de observación y exploración.  
3. El origen del universo.  
- El universo en expansión.  
- La teoría del Big Bang.  
4. Los niveles de agrupación en el universo.  
5. La evolución de las estrellas y el origen de los elementos.  
6. Los agujeros negros.  
7. El sistema solar.  
- El origen del Sol.  
- La formación de los planetas.  
8. La astrobiología.  

### Bloque 3. Avances tecnológicos y su impacto ambiental
1. Los recursos naturales.  
- Sobreexplotación de los recursos naturales.  
2. La utilización de los combustibles fósiles como fuente de energía.  
3. La energía eléctrica.  
- Centrales eléctricas.  
- Fuentes de energía renovable y no renovable.  
4. Contaminación, desertización, pérdida de biodiversidad y tratamiento de residuos.  
5. El cambio climático.  
6. Nuevas fuentes de energía no contaminantes.  
- La pila de combustible.  
7. Principios para una gestión sostenible del planeta.  
- Principales tratados y protocolos internacionales.  

### Bloque 4. Calidad de vida
1. Salud y enfermedad.  
- Factores personales, ambientales y genéticos.  
2. Explicación y tratamiento de la enfermedad a lo largo de la Historia.  
3. Las enfermedades infecciosas.  
- El tratamiento de las enfermedades infecciosas.  
- Los mecanismos de defensa.  
4. Las enfermedades tumorales y el cáncer.  
- Factores de riesgo.  
5. Las enfermedades endocrinas, nutricionales y metabólicas.  
- La obesidad.  
6. Las enfermedades cardiovasculares y las enfermedades del aparato respiratorio.  
- Factores de riesgo.  
7. Las enfermedades mentales.  
- Conductas adictivas.  
8. Estilos de vida saludables.  
9. Tratamiento de las enfermedades: medidas preventivas, fármacos y medicamentos.  
- Técnicas de diagnóstico y tratamiento.  

### Bloque 5. Nuevos materiales
1. Las materias primas.  
- Métodos de obtención.  
2. Los primeros materiales manufacturados.  
- Cerámica, vidrio y papel.  
3. Los metales y sus aleaciones.  
- La corrosión de los metales.  
4. Los polímeros.  
- Los polímeros sintéticos y el medio ambiente.  
5. La nanotecnología.  
- Enfoques y aplicaciones  
6. Los nuevos materiales en el campo de la electricidad y la electrónica.  
7. Los materiales y su influencia en el desarrollo de la humanidad.  
8. Ahorro, reutilización y reciclado de los materiales.

## Criterios de evaluación y estándares de aprendizaje evaluables. 4º ESO

### Bloque 1. Procedimientos de trabajo
1. Obtener, seleccionar y valorar informaciones relacionados con temas científicos de la actualidad.  
1.1. Analiza un texto científico, valorando de forma crítica su contenido.  
2. Valorar la importancia que tiene la investigación y el desarrollo tecnológico en la actividad cotidiana.  
2.1. Presenta información sobre un tema tras realizar una búsqueda guiada de fuentes de contenido científico, utilizando tanto los soportes tradicionales, como Internet.  
2.2. Analiza el papel que la investigación científica tiene como motor de nuestra sociedad y su importancia a lo largo de la historia.  
3. Comunicar conclusiones e ideas en distintos soportes a públicos diversos, utilizando eficazmente las tecnologías de la información y comunicación para transmitir opiniones propias argumentadas.  
3.1. Comenta artículos científicos divulgativos realizando valoraciones críticas y análisis de las consecuencias sociales de los textos analizados y defiende en público sus conclusiones.  

### Bloque 2. El Universo
1. Diferenciar las explicaciones científicas relacionadas con el Universo, el sistema solar, la Tierra, el origen de la vida y la evolución de las especies de aquellas basadas en opiniones o creencias.  
1.1. Describe las diferentes teorías acerca del origen, evolución y final del Universo, estableciendo los argumentos que las sustentan  
2. Conocer las teorías que han surgido a lo largo de la historia sobre el origen del Universo y en particular la teoría del Big Bang.  
2.1. Reconoce la teoría del Big Bang como explicación al origen del Universo.  
3. Describir la organización del Universo y como se agrupan las estrellas y planetas.  
3.1. Establece la organización del Universo conocido, situando en él al sistema solar.  
3.2. Determina, con la ayuda de ejemplos, los aspectos más relevantes de la Vía Láctea.  
3.3. Justifica la existencia de la materia oscura para explicar la estructura del Universo.  
4. Señalar qué observaciones ponen de manifiesto la existencia de un agujero negro, y cuáles son sus características.  
4.1. Argumenta la existencia de los agujeros negros describiendo sus principales características.  
5. Distinguir las fases de la evolución de las estrellas y relacionarlas con la génesis de elementos.  
5.1. Conoce las fases de la evolución estelar y describe en cuál de ellas se encuentra nuestro Sol.  
6. Reconocer la formación del sistema solar.  
6.1. Explica la formación del sistema solar describiendo su estructura y características principales.  
7. Indicar las condiciones para la vida en otros planetas.  
7.1. Indica las condiciones que debe reunir un planeta para que pueda albergar vida.  
8. Conocer los hechos históricos más relevantes en el estudio del Universo.  
8.1. Señala los acontecimientos científicos que han sido fundamentales para el conocimiento actual que se tiene del Universo.  

### Bloque 3. Avances tecnológicos y su impacto ambiental
1. Identificar los principales problemas medioambientales, las causas que los provocan y los factores que los intensifican; así como predecir sus consecuencias y proponer soluciones a los mismos.  
1.1. Relaciona los principales problemas ambientales con las causas que los originan, estableciendo sus consecuencias.  
1.2. Busca soluciones que puedan ponerse en marcha para resolver los principales problemas medioambientales.  
2. Valorar las graves implicaciones sociales, tanto en la actualidad como en el futuro, de la sobreexplotación de recursos naturales, contaminación, desertización, pérdida de biodiversidad y tratamiento de residuos.  
2.1. Reconoce los efectos del cambio climático, estableciendo sus causas.  
2.2. Valora y describe los impactos de la sobreexplotación de los recursos naturales, contaminación, desertización, tratamientos de residuos, pérdida de biodiversidad, y propone soluciones y actitudes personales y colectivas para paliarlos.  
3. Saber utilizar climogramas, índices de contaminación, datos de subida del nivel del mar en determinados puntos de la costa, etc., interpretando gráficas y presentando conclusiones.  
3.1. Extrae e interpreta la información en diferentes tipos de representaciones gráficas, estableciendo conclusiones.  
4. Justificar la necesidad de buscar nuevas fuentes de energía no contaminantes y económicamente viables, para mantener el estado de bienestar de la sociedad actual.  
4.1. Establece las ventajas e inconvenientes de las diferentes fuentes de energía, tanto renovables como no renovables.  
5. Conocer la pila de combustible como fuente de energía del futuro, estableciendo sus aplicaciones en automoción, baterías, suministro eléctrico a hogares, etc.  
5.1. Describe diferentes procedimientos para la obtención de hidrógeno como futuro vector energético.  
5.2. Explica el principio de funcionamiento de la pila de combustible, planteando sus posibles aplicaciones tecnológicas y destacando las ventajas que ofrece frente a los sistemas actuales.  
6. Argumentar sobre la necesidad de una gestión sostenible de los recursos que proporciona la Tierra.  
6.1. Conoce y analiza las implicaciones medioambientales de los principales tratados y protocolos internacionales sobre la protección del medioambiente.  

### Bloque 4. Calidad de vida
1. Reconocer que la salud no es solamente la ausencia de afecciones o enfermedades.  
1.1. Comprende la definición de la salud que da la Organización Mundial de la Salud (OMS).  
2. Diferenciar los tipos de enfermedades más frecuentes, identificando algunos indicadores, causas y tratamientos más comunes.  
2.1. Determina el carácter infeccioso de una enfermedad atendiendo a sus causas y efectos.  
2.2. Describe las características de los microorganismos causantes de enfermedades infectocontagiosas.  
2.3. Conoce y enumera las enfermedades infecciosas más importantes producidas por bacterias, virus, protozoos y hongos, identificando los posibles medios de contagio, y describiendo las etapas generales de su desarrollo.  
2.4. Identifica los mecanismos de defensa que posee el organismo humano, justificando la función que desempeñan.  
3. Estudiar la explicación y tratamiento de la enfermedad que se ha hecho a lo largo de la Historia.  
3.1. Identifica los hechos históricos más relevantes en el avance de la prevención, detección y tratamiento de las enfermedades.  
3.2. Reconoce la importancia que el descubrimiento de la penicilina ha tenido en la lucha contra las infecciones bacterianas, su repercusión social y el peligro de crear resistencias a los fármacos.  
3.3. Explica cómo actúa una vacuna, justificando la importancia de la vacunación como medio de inmunización masiva ante determinadas enfermedades.  
4. Conocer las principales características del cáncer, diabetes, enfermedades cardiovasculares y enfermedades mentales, etc., así como los principales tratamientos y la importancia de las revisiones preventivas.  
4.1. Analiza las causas, efectos y tratamientos del cáncer, diabetes, enfermedades cardiovasculares y enfermedades mentales.  
4.2. Valora la importancia de la lucha contra el cáncer, estableciendo las principales líneas de actuación para prevenir la enfermedad.  
5. Tomar conciencia del problema social y humano que supone el consumo de drogas.  
5.1. Justifica los principales efectos que sobre el organismo tienen los diferentes tipos de drogas y el peligro que conlleva su consumo.  
6. Valorar la importancia de adoptar medidas preventivas que eviten los contagios, que prioricen los controles médicos periódicos y los estilos de vida saludables.  
6.1. Reconoce estilos de vida que contribuyen a la extensión de determinadas enfermedades (cáncer, enfermedades cardiovasculares y mentales, etcétera).  
6.2. Establece la relación entre alimentación y salud, describiendo lo que se considera una dieta sana.  

### Bloque 5. Nuevos materiales
1. Realizar estudios sencillos y presentar conclusiones sobre aspectos relacionados con los materiales y su influencia en el desarrollo de la humanidad.  
1.1. Relaciona el progreso humano con el descubrimiento de las propiedades de ciertos materiales que permiten su transformación y aplicaciones tecnológicas.  
1.2. Analiza la relación de los conflictos entre pueblos como consecuencia de la explotación de los recursos naturales para obtener productos de alto valor añadido y/o materiales de uso tecnológico.  
2. Conocer los principales métodos de obtención de materias primas y sus posibles repercusiones sociales y medioambientales.  
2.1. Describe el proceso de obtención de diferentes materiales, valorando su coste económico, medioambiental y la conveniencia de su reciclaje.  
2.2. Valora y describe el problema medioambiental y social de los vertidos tóxicos.  
2.3. Reconoce los efectos de la corrosión sobre los metales, el coste económico que supone y los métodos para protegerlos.  
2.4. Justifica la necesidad del ahorro, reutilización y reciclado de materiales en términos económicos y medioambientales.  
3. Conocer las aplicaciones de los nuevos materiales en campos tales como electricidad y electrónica, textil, transporte, alimentación, construcción y medicina.  
3.1. Define el concepto de nanotecnología y describe sus aplicaciones presentes y futuras en diferentes campos.  

