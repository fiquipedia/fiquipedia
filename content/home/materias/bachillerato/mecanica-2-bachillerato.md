
# 2º Bachillerato Mecánica (LOGSE)

Es una materia LOGSE, que desapareció de la PAU en 2010, se puede ver en exámenes PAU de 2009 y anteriores, y se incluye aquí porque puede ser interesante como fuentes de problemas para 1º Bachillerato y de ESO, ya que incluye cinemática y dinámica.   
  
Real Decreto 117/2004, de 23 de enero, por el que se desarrolla la ordenación y se establece el currículo del Bachillerato.  
 [BOE.es - BOE-A-2004-2972 Real Decreto 117/2004, de 23 de enero, por el que se desarrolla la ordenación y se establece el currículo del Bachillerato.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2004-2972)   
  


##  [Pruebas de Acceso a la Universidad Mecánica](/home/pruebasaccesouniversidad/pau-mecanica) 

## Recursos mecánica
Problemas resueltos sólido rígido, Fundamentos Físicos © Ángel Franco García   
 [Problemas resueltos](http://www.sc.ehu.es/sbweb/ocw-fisica/problemas/problemas.xhtml#solido)   
FUNDAMENTOS FÍSICOS DE INGENIERÍA I (1228)  
Grado en Ingeniería Mecánica, Universidad Miguel Hernández.   
 [](http://umh1228.edu.umh.es/material/)   
 [FUNDAMENTOS FÍSICOS DE INGENIERÍA I (1228) &raquo; Teoría](http://umh1228.edu.umh.es/material/teoria/)  [FUNDAMENTOS FÍSICOS DE INGENIERÍA I (1228) &raquo; Problemas con solución](http://umh1228.edu.umh.es/material/problemas-con-solucion/)   
 [](http://umh1228.edu.umh.es/material/teoria/)   

## Currículo (LOGSE, derogado en LOE)  

### Introducción
  
La Mecánica teórica es la ciencia que estudia las leyes generales del movimiento de los cuerpos materiales en relación con las fuerzas que lo producen, estableciendo procedimientos y métodos generales de análisis y de resolución de problemas relacionados con esos movimientos.  
  
Sin embargo, la Mecánica, como asignatura del Bachillerato, tiene un enfoque de ciencia aplicada, estando más cercana a la tecnología que a las ciencias físicas.  
  
Del amplio campo de cuerpos materiales sobre los que están aplicadas fuerzas y movimientos, esta disciplina se centra en el estudio de los elementos mecánicos más significativos de estructuras y máquinas. En cuanto a su finalidad, se trataría de enseñar a los alumnos los conocimientos que les permitan acometer el análisis mecánico de los elementos de máquinas y estructuras, ya sea para modificarlos y que respondan a nuevos planteamientos, ya sea para justificar su construcción.  
  
Su valor formativo estriba:  
  
En la mejora del razonamiento lógico, ya que la cercanía y simplicidad de los elementos mecánicos hacen que su coherencia interna y el rigor lógico de su funcionamiento sean fácilmente asimilados.  
  
En la transferencia de conocimientos a situaciones reales, pues la fácil aplicación de leyes generales en el estudio y análisis de elementos concretos refuerza esa capacidad.  
  
En el análisis crítico, puesto que el análisis metódico estático, cinemático y dinámico de los elementos mecánicos desarrolla tal competencia.  
  
En la precisión del lenguaje en cuanto que el aumento del vocabulario específico y el rigor conceptual de sus términos enriquece la expresión y comprensión oral y escrita.  
  
En la comprensión del mundo que les rodea, porque el estudio de los elementos mecánicos es el de la historia de la Mecánica y, en parte, el de la historia de la ciencia.  
  
Los contenidos de esta materia se organizan en seis bloques. Un primer bloque sistematiza y esquematiza el estudio de las Uniones y Acciones Mecánicas en máquinas y estructuras. De Estática se estudia únicamente el equilibrio de los elementos de estructuras y máquinas, aislados del conjunto y situados en el plano ; no obstante, el tratamiento genérico del equilibrio permite un acercamiento previo al tema con mayor rigor formal. La Cinemática se centra en el estudio de la traslación y rotación de los elementos de máquinas y mecanismos. En una introducción al movimiento plano, se presenta el método del centro instantáneo de rotación para determinar velocidades en elementos y el de la composición de movimientos para mecanismos articulados sencillos. En la Dinámica se desarrolla fundamentalmente la rotación de sólidos alrededor de ejes de simetría fijos. Un interés particular tienen el principio de la conservación de la energía mecánica para la determinación de las acciones sobre máquinas y mecanismos y la aproximación al estudio de las vibraciones en las máquinas. La Resistencia de Materiales permite un acercamiento al estudio de la resistencia del sólido elástico.  
  
Se completaría la asignatura con una introducción a la Mecánica de Fluidos.  
  
La relación de contenidos tiene una presentación clásica y responde, en cierto modo, a la lógica de la disciplina ; será el profesor, no obstante, quien determine su secuencia en cada curso, en la programación de aula, a la vista de los ciclos formativos impartidos en el centro y de los conocimientos previos de los alumnos.  
  
El acercamiento a las leyes de la mecánica, es decir, al estudio de la relación entre las fuerzas y los movimientos que obran sobre los cuerpos, debe hacerse desde el análisis de los elementos reales de las estructuras y de las máquinas. No parece, por tanto, aconsejable que se aborden estos contenidos con un planteamiento de mecánica teórica o mecánica racional ; el enfoque de la disciplina debe ser el de mecánica aplicada. Por ello, la metodología aconsejable consistiría en el estudio de las fuerzas y movimientos en los elementos mecánicos, fundamentándolo en las leyes de la mecánica y justificando después, en la medida de lo posible, el por qué de su construcción. El estudio mecánico (estático, cinemático, dinámico y resistente) de los elementos que conforman las máquinas y las estructuras, es el que ha de guiar continuamente los procesos de enseñanza y de aprendizaje en el aula.  

### Objetivos:
  
1. Construir modelos del comportamiento de elementos, estructuras o sistemas mecánicos reales sometidos a distintas exigencias, mostrando en el esquema lo fundamental y omitiendo lo accesorio.    
2. Identificar en los sólidos rígidos y en los sistemas mecánicos más complejos, las acciones que en ellos concurren y su interrelación.  
3. Analizar y resolver problemas mediante la aplicación, en ejemplos reales, de las leyes de la Mecánica y de otras fórmulas derivadas de la experiencia, teniendo en cuenta los límites impuestos por esa misma realidad.  
4. Relacionar formas, dimensiones, materiales y, en general, el diseño de los objetos y sistemas técnicos, con las solicitaciones mecánicas a que están sometidos, justificando su construcción.  
5. Utilizar apropiadamente, en la comunicación y el intercambio de ideas y opiniones, los conceptos y el vocabulario específico en relación con la Mecánica.  
6. Manejar correctamente las unidades de medida de las diferentes magnitudes.  
7. Desarrollar, a través del razonamiento con las leyes de la Mecánica, una "intuición mecánica" básica.  

### Contenidos
  
1. Uniones y acciones mecánicas.  
Introducción al estudio de vectores. Geometría de masas, centro de masas, centro de gravedad, momento de inercia de una sección respecto a un eje, radio de inercia.  
Uniones mecánicas. Tipos, características, grados de libertad, articulaciones, empotramientos, deslizaderas, rótulas, apoyos, uniones helicoidales. Estudio y modelización de uniones mecánicas en mecanismos y sistemas materiales reales.  
Acciones sobre un sistema material. Fuerzas interiores y exteriores. Fuerzas a distancia y fuerzas de contacto:  
puntuales, distribuidas, de presión de líquidos, de rozamiento. Momento de una fuerza. Par de fuerzas. Estudio y modelización de acciones en mecanismos y sistemas materiales reales.  
Transmisión de fuerzas y momentos mediante uniones mecánicas perfectas. Uniones mecánicas reales, rozamiento.  
  
2. Estática.  
Equilibrio de un sistema de puntos materiales:  
condiciones universales de equilibrio.  
Equilibrio de un sólido rígido, libre o con uniones fijas, sometido a un sistema de fuerzas coplanarias.  
Condiciones universales de equilibrio. Discusión del rozamiento en el equilibrio de sistemas simples.  
Estudio estático de mecanismos planos con elementos articulados y deslizaderas. Cuadrilátero articulado, biela-manivela. Estudio estático de elementos articulados de bastidores y máquinas. Estudio estático de máquinas simples, poleas fijas y móviles, tornos y cabrestantes.  
Estructuras con elementos articulados ; determinación de tensiones.  
  
3. Cinemática.  
Cinemática del punto. Posición, velocidad y aceleración del punto en el plano. Movimientos lineal y circular.  
Expresiones intrínsecas y cartesianas.  
Cinemática del sólido. Movimiento de traslación.  
Traslación rectilínea uniforme y uniformemente acelerada.  
Patines o deslizaderas, paralelogramo articulado.  
Movimiento de rotación alrededor de un eje fijo. Rotación uniforme y uniformemente acelerada. Expresiones intrínsecas y angulares. Ruedas, engranajes, volantes.  
Movimiento helicoidal uniforme. Husillos.  
Movimiento plano. Centro instantáneo de rotación, determinación de velocidades.  
Composición de movimientos, velocidades absoluta, relativa y de arrastre.  
Aproximación al movimiento vibratorio simple.  
  
4. Dinámica.  
Dinámica del punto. Principio fundamental de la dinámica en el movimiento lineal y circular, en el plano, de un punto material ; ecuaciones del movimiento.  
Dinámica del sólido. Traslación en el plano. Principio fundamental. Ecuaciones del movimiento. Trabajo, energía y potencia. Cantidad de movimiento: su conservación en un sistema aislado.  
Dinámica del sólido. Rotación alrededor de un eje de simetría fijo. Principio fundamental. Ecuaciones del movimiento. Momento de inercia. Trabajo, energía y potencia. Momento cinético: su conservación en un sistema aislado. Efectos del movimiento giroscópico en ruedas, rotores y volantes.  
Análisis dinámico de máquinas y mecanismos.  
Determinación de las acciones sobre máquinas y mecanismos, teorema de la energía cinética y principio de conservación de la energía mecánica. Equilibrado de masas giratorias e introducción al equilibrado de masas alternativas. Rozamiento por deslizamiento y rodadura.  
Rendimiento en máquinas y mecanismos.  
El sólido elástico sometido a vibración. Resonancia.  
Fatiga. Amortiguadores. Velocidades críticas en árboles.  
  
5. Resistencia de materiales.  
Elasticidad y plasticidad de los materiales, ley de Hooke. Acciones entre dos secciones contiguas de material, esfuerzos. Esfuerzo de trabajo, coeficiente de seguridad.  
Tracción, compresión, cortadura. Flexión: fuerza cortante y momento flector ; esfuerzos. Vigas simplemente apoyadas y en voladizo sometidas a cargas puntuales y uniformemente distribuidas. Torsión en árboles circulares macizos y huecos. Pandeo, carga crítica, esfuerzos en puntales y en elementos esbeltos de máquinas y estructuras.  
Esfuerzos térmicos. Concentración de esfuerzos, efecto entalla. Fatiga.  
  
6. Introducción a la Mecánica de fluidos.  
Hidrostática, teorema de Pascal. Cinemática de fluidos perfectos incompresibles, teorema de Bernouilli.  
Fluidos reales, pérdida de carga. Movimiento de fluidos alrededor de un perfil, sustentación y resistencia.  

### Criterios de evaluación:  
1. Identificar uniones mecánicas en sistemas materiales reales y expresar sus características, y las fuerzas y momentos que transmiten.  
2. Identificar las acciones que ocurren sobre los sistemas materiales reales, expresándolas como fuerzas o momentos e indicando su valor, dirección y sentido.  
3. Aislar un elemento de un mecanismo, bastidor o máquina, con representación en el plano, identificar las fuerzas y momentos a él aplicados, plantear el equilibrio y calcular los valores desconocidos.  
4. Plantear el equilibrio y calcular el valor de las tensiones en elementos articulados de estructuras planas o de estructuras espaciales sencillas (reducibles fácilmente a planos).  
5. Identificar movimientos lineales y circulares en sistemas materiales reales y calcular, en puntos significativos de su funcionamiento, posiciones, velocidades y aceleraciones.  
6. Identificar y calcular, en el sistema de referencia seleccionado, las velocidades absoluta, relativa y de arrastre en el movimiento plano de un sistema articulado sencillo.  
7. Aplicar el principio fundamental de la dinámica a máquinas que giran, discutir el valor del momento de inercia en el funcionamiento del conjunto y relacionar las magnitudes de potencia, par y régimen de giro.  
8. Aplicar el principio de conservación de la energía mecánica a máquinas y mecanismos y, en general, a sistemas mecánicos reales sencillos, discutir la influencia del rozamiento y determinar valores de rendimientos.  
9. Relacionar el diseño de los diferentes elementos que componen una estructura o conjunto mecánico con su resistencia a diferentes solicitaciones (tracción, compresión, cortadura, flexión, torsión y pandeo) y emplear en el razonamiento los conceptos y el vocabulario apropiados.  
10. Relacionar, entre sí, cargas, esfuerzos y coeficiente de seguridad en elementos simplificados de estructuras o sistemas mecánicos reales sometidos a tracción, compresión y cortadura.  
11. Justificar la construcción de estructuras reales desde el punto de vista de sus solicitaciones aerodinámicas.  
12. Calcular los valores de las magnitudes puestas en juego en la circulación de fluidos perfectos incompresibles.   
 
