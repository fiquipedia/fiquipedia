
# Didáctica

Creo esta página para intentar recopilar información asociada a didáctica. Puede ser general o específica de ciencias / Física y Química, y puede ser información sobre metodologías concretas o bien general orientada a reflexionar con temas asociados a la docencia (desde tecnologías a disciplina)  
Inicialmente creo una página aislada, si crece la separaré en varias.  
Puede haber información relacionada en normativa, por ejemplo asociada a estructura del curriculo y situaciones de aprendizaje.  
También puede haber información sobre la didáctica en algún contenido / concepto página concreta de algún recurso.  

Pendiente poner referencias de blogs / cuentas de Twitter y libros asociados:  

[Héctor Ruiz Martín @hruizmartin](https://twitter.com/hruizmartin)  
[Juan Fernández@profesmadeinuk](https://twitter.com/profesmadeinuk), [Investigación docente](https://investigaciondocente.com/)

[twitter hruizmartin/status/1161918561960497152](https://twitter.com/hruizmartin/status/1161918561960497152)  
¿Sabíais que la ciencia ha investigado qué estrategias de estudio son más efectivas? La forma cómo aprende nuestro cerebro determina qué acciones promueven que recordemos mejor lo que aprendemos. Aquí os hablaré de una de estas estrategias.  
Antes de todo, es importante apreciar que aprender implica tres procesos necesariamente: debemos obtener la información (codificación), debemos conservarla (almacenamiento) y debemos ser capaces de recuperarla (evocación).  
¿Podemos afirmar que hemos aprendido algo si no tenemos la capacidad de recuperarlo de la memoria? A la práctica, no. De hecho, puede q el olvido no se deba tanto a la desaparición de la información aprendida como a la incapacidad de hallarla en nuestra memoria y evocarla.  
En fin, el hecho es que aprender algo incluye la capacidad de recuperarlo de la memoria. Al fin y al cabo, lo que evaluamos en los exámenes no es la codificación ni el mantenimiento, sino la evocación. Y ahí está la clave.  
A todos nos parece obvio q para aprender a ir en bicicleta hay que practicar yendo en bicicleta. Pero muchos alumnos no se percatan d q lo mismo sucede con los conocimientos académicos: para ser capaces de evocarlos hay que practicar su evocación.  
Estos alumnos creen q es suficiente con "absorberlos", que prestando atención los asimilarán y eso garantizará que los podrán evocar posteriormente. Así q estos alumnos estudian leyendo y releyendo los apuntes, por ejemplo, pero no tratando de recuperar lo leído de su memoria.  
El caso es q la ciencia ha aportado una cantidad ingente de evidencias q muestran q practicar la evocación de lo aprendido nos hace aprenderlo mejor (Karpicke 2008), o, por lo menos, mejora nuestra capacidad de recuperarlo y, por lo tanto, de demostrar que lo sabemos.  
Son abundantes los estudios q constatan q si tras una sesión de estudio realizamos una sesión en la q ponemos a prueba nuestra memoria (en vez de "reestudiar"), se obtienen mejores resultados en un examen posterior (Rowland 2014).  
Es decir, estudiar y luego evocar es mucho más beneficioso para la memoria que estudiar y reestudiar. En el sentido q incrementa nuestra capacidad de recordar lo aprendido en el futuro (Karpicke, 2008).  
Y esto no solo aplica a conocimientos factuales, sino también conceptuales. De hecho, la evocación nos obliga a dar estructura y sentido a lo que aprendemos. E incluso promueve la capacidad de transferencia (poder aplicar lo aprendido en nuevos contextos) (Karpicke, 2012).  
...
 

[Enseñanza explícita: ¿Qué es, por qué funciona y en qué condiciones?](https://investigaciondocente.com/2022/08/09/ensenanza-explicita-que-es-por-que-funciona-y-en-que-condiciones/)  

[Student-centered instruction and academic achievement: linking mechanisms of educational inequality to schools’ instructional strategy](https://www.tandfonline.com/doi/abs/10.1080/01425692.2015.1093409?s=03)  

[21 Conceptos sobre disciplina que me habría gustado oír cuando empecé en esto - archive.org](https://web.archive.org/web/20200317094449/https://blogcienciesnaturals.wordpress.com/2014/09/10/21-conceptos-sobre-disciplina-que-me-habria-gustado-oir-cuando-empece-en-esto/)  

[Interesante entrevista al periodista y divulgador Johann Hari: "Hemos perdido el superpoder de nuestra especie: nuestra capacidad de atención" ](https://threadreaderapp.com/thread/1649990211995160580.html)   

[La advertencia de Johann Hari: "Hemos perdido el superpoder de nuestra especie y no es solo por culpa del móvil"](https://www.elconfidencial.com/cultura/2023-04-23/entrevista-johann-hari_3616128/)  

[Cómo enseñar historia: sesiones y actividades](https://www.profesorfrancisco.es/2023/06/como-ensenar-historia.html)  

[Retrieval Practice: práctica de la evocación - investigaciondocente](https://investigaciondocente.com/2020/10/16/retrieval-practice-practica-de-la-evocacion/)  

[Twitter currofisico/status/1788519106171334832](https://x.com/currofisico/status/1788519106171334832)  
Solo escucho gamificaciones, rúbricas, canvas, geniallys...y echarse las manos a la cabeza con las "barbaridades" que aparecen en las pruebas escritas. ¿Realmente son barbaridades o forman parte del proceso? Para enseñar física no hace falta un doctorado, ni ser un erudito, pero si mucha formación didáctica (que de eso va el trabajo, no de desparramar conocimiento desde el púlpito)...y tal y como van los vientos es difícil de encontrar. Conocer el cómo enseñar o trabajar un tópico enfrentándonos a las ideas que ya tienen, o simplemente presentarlos de forma adecuada al estado evolutivo del alumno/a no es fácil, requiere paciencia, continuidad... ningún concepto tiene carácter final, es un proceso donde poco a poco damos capaz de barniz y hasta en ocasiones este barniz se desprende.  

Se citan dos libros

[Enseñando física: una guía para el no especialista.: 1 (Biblioteca de recursos didácticos) ISBN 8446005859](https://www.amazon.es/Ense%C3%B1ando-f%C3%ADsica-especialista-Biblioteca-did%C3%A1cticos/dp/8446005859)  

[Ciencia de los alumnos, la ISBN 978-8471701084](https://www.amazon.es/Ciencia-alumnos-MONTERO-ANTONIO-HIERREZUELO/dp/8471701081)  

[Las Aventuras Químicas de Sherlock Holmes: un recurso “elemental” en el aula de ciencias. REIDOCREA | ISSN: 2254-5883 | VOLUMEN 11. NÚMERO 3. PÁGINAS 28-43 ](https://www.ugr.es/~reidocrea/11-3.pdf)  
> Este artículo supone la continuación de trabajos previos centrados en el empleo de la narrativa como recurso didáctico en Física y
Química.  

[Aprendizaje basado en la investigación Marina P. Arrieta Dillon y M. Victoria Alcázar Montero. An. Quím., 117 (3), 2021, 203-2](https://analesdequimica.es/index.php/AnalesQuimica/article/view/1939/2398)  

[Un ejemplo de actividad de escape room sobre física y química en educación secundaria. Revista Eureka sobre Enseñanza y Divulgación de las Ciencias 18(2), 2205 (2021)](https://revistas.uca.es/index.php/eureka/article/view/6191/7428)  
> De hecho, muchos estudiantes de Secundaria confiesan que la asignatura de Física y Química les resulta aburrida y/o difícil en comparación con otras(Méndez-Coca 2015). La estructura de una clase de esta asignatura suele consistir en introducirlos términos y conceptos mediante la presentación de definiciones, seguido de una explicacióno lectura de un libro de texto y, por último, se resuelven problemas tipo. Aunque parece unesquema asentado, en ocasiones los alumnos no responden bien al mismo (Yuriev et al. 2016).   

[Fuerzas conservativas: su abordaje en libros de texto de física universitaria. VOLUMEN 33, NÚMERO EXTRA | Selección de Trabajos Presentados a REF | PP. 673-680 ISSN 2469-052X (en línea)](https://revistas.unc.edu.ar/index.php/revistaEF/article/download/35648/35774/125780)  

