
# Contenidos currículo con enlaces a recursos

Este es uno de los objetivos iniciales de la fiquipedia indicados en [planteamiento](/planteamiento). 
*"Poner el currículo/normativa de cada materia, **para la Comunidad de Madrid que es la que conozco,** y cada frase o bloque enlazará a una página donde haya información y recursos asociados.* 

"Además de que estén los currículos "estáticos", se crean páginas especiales donde están asociados a los contenidos del currículo enlaces a recursos.  

A base de buscar recursos he encontrado algo similar con el currículo de la comunidad valenciana en  [http://aprendiendociencias.wordpress.com](http://aprendiendociencias.wordpress.com) , con enlace a ejemplos como  [https://sites.google.com/site/compartimosciencia/cursos-anteriores/201011-4eso-iesmisteridelx/11-los-atomos-y-sus-enlaces](http://sites.google.com/site/compartimosciencia/cursos-anteriores/201011-4eso-iesmisteridelx/11-los-atomos-y-sus-enlaces)  

De momento he ido creando algunas páginas, falta añadir enlaces ...cuando cambie el currículo con nuevas leyes, habrá que rehacerlo, aunque se reutilizarían enlaces a recursos.  
En algunos cursos los contenidos están separados por bloques y se ponen enlaces por cada bloque (por ejemplo en Física de 2º de Bachillerato)  
* [Contenidos currículo Física y Química 3º ESO con enlaces a recursos](/home/materias/eso/fisica-y-quimica-3-eso/cotenidoscurriculoenlacesrecursos3esofisicayquimica) 
* [Contenidos currículo Física y Química 4º ESO con enlaces a recursos](/home/materias/eso/fisicayquimica-4eso/contenidos-del-curriculo-con-enlaces-a-recursos-4-eso-fisica-y-quimica) 
* [Contenidos currículo Física y Química 1º Bachillerato con enlaces a recursos](/home/materias/bachillerato/fisicayquimica-1bachillerato/contenidoscurriculoenlacesrecursosfisicaquimica1bachillerato)
*  [Contenidos currículo Física 2º Bachillerato con enlaces a recursos](/home/materias/bachillerato/fisica-2bachillerato/contenidoscurriculoenlacesrecursosfisica2bachillerato)
*  [Contenidos currículo Química 2º Bachillerato con enlaces a recursos](/home/materias/bachillerato/quimica-2bachillerato/contenidoscurriculoenlacesrecursosquimica2bachillerato)  
