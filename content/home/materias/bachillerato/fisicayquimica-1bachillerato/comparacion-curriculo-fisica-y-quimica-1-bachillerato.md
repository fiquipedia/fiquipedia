# Comparación currículo Física y Química 1º Bachillerato

La tabla inicialmente la hago para LOMCE vs LOE, pero la muevo a página separada para incluir en comparación LOMLOE

Esta tabla la inicio como un borrador y está en evolución; pongo fecha de última revisión.   

En la columna LOE / LOMLOE se indica el currículo para la comunidad de Madrid, a veces no idéntico al RD estatal. En LOMCE los estándares de aprendizaje y parte del currículo no variaba entre el estatal y autonómico.  

LOMCE:  
Algunas cosas como la ley de Hess no se indicaban explícitamente pero a menudo eran incluidas por profesorado y materiales.  
Cuando en una de las dos columnas indico "se cita xxx" suele querer decir que se cita en ese caso (LOE ó LOMCE) pero no en el otro. A veces indico "se citaba", "se añade", "desaparece" ...  
Creo al final un resumen LOE vs LOMCE  

LOMLOE: comienzo a poner ideas con borrador de octubre 2021  
Considero 4 sesiones semanales como en LOE y LOMCE. "Anexo IV. Horario escolar, expresado en horas, correspondiente a las 
enseñanzas mínimas para el Bachillerato." indica 87,5 horas, y considerando 175 días lectivos según [disposición adicional quinta LOE](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#daquinta) (aunque es para enseñanza obligatoria) lleva a 35 semanas, por lo que serían 2,5 sesiones a la semana, pero lo que indica la normativa estatal es mínimo.  
Al publicarse [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) en ANEXO I Organización del primer curso de Bachillerato página 19 del pdf se indican 4 horas semanales.  
En junio 2022 (todavía sin currículos autonómicos) se publica un documento que realiza una comparativa  
[Física y Química en la LOMLOE: Una mirada al nuevo currículo de ESO y Bachillerato (Luis Moreno, Almudena de la Fuente y Alejandro Rodríguez‐Villamil), 4-14, Boletín informativo del grupo especializado en Didáctica e Historia de la Física y la Química, común a las Reales Sociedades Españolas de Física y de Química, Nº 37, Junio 2022](https://gedh.rseq.org/wp-content/uploads/2022/06/Boletin-37-GEDH-red.pdf)  
Creo al final un resumen LOMCE vs LOMLOE en forma de tabla  

La cabecera de las tablas lleva a cada uno de los currículos  

**Tabla revisada 28 julio 2024**  
  
| **[LOE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculofisicayquimica-1bachillerato)** | **[LOMCE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomce)**   | **[LOMLOE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomloe)** |
|:-|:-|:-|
|  4 sesiones semanales   |  4 sesiones semanales |  4 sesiones semanales | 
|  Materia de modalidad en la modalidad de bachillerato de Ciencias y Tecnología |  Materia troncal en la modalidad de bachillerato de Ciencias   | Materia de modalidad dentro de la modalidad de Bachillerato de Ciencias y Tecnología |
|  9 objetivos (se usa número 1 a 9) asociados a la materia   |  14 objetivos (se usan letra desde a hasta n) asociados a la etapa de bachillerato   | |  [14 objetivos de etapa](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a25)  (se usa letra desde a hasta n)  | [15 objetivos de etapa](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a7) (se usa letra desde a hasta o)  |
|  9 bloques de contenidos fijados en decreto comunidad   |  8 bloques de contenidos complementables por la comunidad (Madrid no los ha modificado)   | 6 bloques de **saberes básicos** (A, B, C, D, E y F) |
|  12 criterios de evaluación fijados en decreto comunidad   |  Criterios de evaluación fijados a nivel estatal a nivel de cada uno de los 8 bloques | Criterios de evaluación fijados en 6 **competencias específicas** que tienen 17 apartados en total |
|  No había estándares de aprendizaje |  Estándares de aprendizaje fijados a nivel estatal a nivel de cada uno de los 8 bloques | Desaparecen los estándares de aprendizaje |
|  1. Contenidos comunes. |  Bloque 1. La actividad científica   | **No hay bloque de saberes básicos asociado** "no se contempla un bloque específico de contenidos comunes sobre las destrezas científicas básicas, sino que al haber sido adquiridas estas por los alumnos previamente deben ser trabajadas de manera transversal en todos los apartados de la materia" |
|  2. Estudio del movimiento. |  Bloque 6. Cinemática  <br>  -Se añade explícitamente "Movimiento circular uniformemente acelerado" en contenidos aunque luego no se cita MCUA en estándares  <br> Se citan componentes intrínsecas de la aceleración <br>**-Se añade MAS (que desaparece en  [Física de 2º de Bachillerato LOMCE](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce) )** | Saber básico **D. Cinemática** <br> Se mantiene MCUA <br> Se citan explícitamente **Relatividad de Galileo.**, **Composición de movimientos: tiro horizontal y tiro oblicuo.**  <br><br> Desaparece: <br> Cinemática MAS que vuelve a Física 2º como antes LOMCE |
|  3. Dinámica.  <br> Se cita Ley de gravitación universal  |  Bloque 7. Dinámica  <br> Se cita "Dinámica de cuerpos ligados" <br>**-Se añade MAS (que desaparece en [Física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce) )**  <br>**-Se añade explícitamente Kepler (que desaparece en  [Física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce) )**  <br>**-Se añade explícitamente "Fuerzas centrales. Momento de una fuerza y momento angular. Conservación del momento angular"** (que desaparece en  [Física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce) )**  <br>**-Se añade explícitamente "3.3. Estima el valor de la gravedad haciendo un estudio del movimiento del péndulo simple.")**  <br>-Se sigue citando Ley de gravitación universal (deja de citarse en  [Física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce) ): se elimina cierta duplicidad existente. | Saber básico **E. Estática y dinámica** <br> El propio nombre del saber básico añade **Estática** <br> Se dejan de citar explícitamente planos inclinados, que aparecían en LOE y LOMCE. Se deja de citar explícitamente "no inercial" que aparecía en LOMCE. Se cita la fuerza tensión pero no cuerpos ligados<br> Se cita explícitamente **sólido rígido**, **pares de fuerzas** y varias veces **centro de gravedad**<br>. Se deja de citar explícitamente momento angular, aunque se habla de momentos de fuerzas <br><br> Desaparece: <br> Gravitación, leyes de Kepler y MAS, que vuelven a Física 2º como antes LOMCE | 
|  4. La energía y su transferencia. Trabajo y calor.  <br> Se cita "Energía debida a la posición en el campo gravitatorio"   |  Bloque 4. Transformaciones energéticas y espontaneidad de las reacciones químicas  <br>**-Por títulos de bloques parece que en LOMCE las transformaciones de energía se centran en reacciones químicas, cuando en LOE parece más genérico asociado a cualquier tipo de transferencia de energía (típicamente se veía calor específico y calor latente): mi visión es que no se citan explícitamente aunque se pueden considerar incluidos tanto en LOE ("Transferencias de energía") como en LOMCE (" intercambios de calor y trabajo")**  <br>**-Se añade explícitamente entalpía, Ley de Hess**  <br>**-Se añade Entropía, Energía de Gibbs.**  <br>*-En mi opinión se han equivocado al poner este estándar de aprendizaje evaluable "5.1. Predice la variación de entropía en una reacción química dependiendo de la molecularidad y estado de los compuestos que intervienen." [https://twitter.com/FiQuiPedia/status/760861873218654209](https://twitter.com/FiQuiPedia/status/760861873218654209) ya que molecularidad no aplica en absoluto, ver [Molecularidad - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Molecularidad). Por intentar darle sentido, asumo que lo que se pretendía decir es "Predice la variación de entropía en una reacción química dependiendo del número de moles y estado de reactivos y productos que intervienen según su estequiometría" En definitiva, razonar variación de entropía si se reduce el número de moles gaseosos de reactivos a productos o aumenta, o si hay cambios progresivos o regresivos*  <br>   Bloque 8. Energía  <br> -Se citan explícitamente "Sistemas conservativos", que antes no aparecían hasta [Física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce)   <br>**-Se añade MAS (que desaparece en [Física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce) )** | Saber básico **F. Energía** <br> Madrid cita "interpretación gráfica del trabajo de una fuerza variable" <br> Se vuelve a citar explícitamente potencia <br> Se citan explícitamente sistemas conservativos y no conservativos <br> Se añade a nivel estatal "Variables termodinámicas de un sistema en función de las condiciones: **determinación** de las variaciones de temperatura que experimenta y las transferencias de energía que se producen con su entorno", que Madrid concreta citando primer y segundo principio de la termodinámica y clasificación de procesos termodinámicos. Eso se puede interpretar: <br> 1. Que hace referencia a parte general de termodinámica previa a termoquímica <br> 2 Que es "el efecto de calor sobre los cuerpos" que se desarrolla tratando calor específico, calor latente y dilatación <br> 3 Que se espera se traten las expresiones de U, Q, W y S en distintos procesos. Se cita el segundo principio que no se cita en termoquímica<br>Mi interpretación es que es 3: se cita **determinación**, la idea general de calor ya se cita en 4º ESO LOMLOE, y 2 (efecto del calor sobre los cuerpos) se cita en 2º, 3º y 4º ESO LOMLOE aunque sin citar cálculos, y asumo se trata con cálculos ya en 4º ESO. Aunque no esté en texto de saberes básicos, en texto inicial del currículo se indica *los aspectos básicos de termodinámica que les permitan entender el funcionamiento de sistemas termodinámicos simples* y eso confirma interpretación 3. En cualquier caso queda abierto a interpretación el nivel de profundidad: centrarse en máquinas térmicas y rendimientos, solo segundo principio cualitativo citando S pero sin cálculos de entropía ni ciclos como Carnot.<br>  Deja de citarse termoquímica a nivel estatal y se puede considerar que vuelve a Química 2º como antes LOMCE, sin embargo Madrid añade parte en saber básico **B. Reacciones químicas** al tiempo que se mantiene en Química de 2º: **Los sistemas termodinámicos en química. Variables de estado. Equilibrio térmico y temperatura.**, **Procesos a volumen y presión constantes. Concepto de Entalpía.**, **La ecuación termoquímica y los diagramas de entalpía.**, **Determinación experimental de la entalpía de reacción.**. **Entalpías de combustión, formación y de enlace. La ley de Hess.**. No es igual porque no aparece espontaneidad ni Gibbs en 1º como antes, que está ya solo en 2º.<br><br> Desaparece: <br> Energía en MAS que vuelve a Física 2º como antes LOMCE |
|  5. Electricidad.  <br> Se cita electrización  <br> Se cita "La corriente eléctrica. Ley de Ohm; aparatos de medida y asociación de resistencias. Aplicación al estudio de circuito. Efectos energéticos de la corriente eléctrica. Generadores de corriente." |  Bloque 7. Dinámica: Interacción electrostática: ley de Coulomb.  <br> Bloque 8. Energía: Diferencia de potencial eléctrico  <br> -Desaparece mención a electrización  <br> -Se cita explícitamente Ley de Coulomb  <br> -Desaparece mención explícita a corriente eléctrica y ley de Ohm: se limita a interacción electrostática dentro de dinámica y de energía   | **Deja de haber mención explícita a electricidad** |
|  6. Teoría atómico-molecular de la materia. |  Bloque 2. Aspectos cuantitativos de la química  <br> -Se añaden explícitamente "propiedades coligativas"  <br>*-En mi opinión se han equivocado al poner este estándar de aprendizaje evaluable **"5.2. Utiliza el concepto de presión osmótica para describir el paso de iones a través de una membrana semipermeable."** ya que en la ósmosis lo que pasa a través de la membrana es el disolvente, no los iones y el soluto. Se podría plantear una redacción alternativa como "5.2. Utiliza el concepto de presión osmótica para describir el paso de **disolvente** a través de una membrana semipermeable **debido a diferente concentración de soluto**".*   <br> -Se añade Espectroscopía y Espectrometría   | Asociable a parte de saber básico **B. Reacciones químicas**, donde se cita "Cálculo de cantidades de materia en sistemas fisicoquímicos concretos, como gases ideales y disoluciones y sus propiedades" <br> Deja de citarse "Espectroscopía y espectrometría", que no aparece explícitamente en [Química 2º Bachillerato LOMLOE](/home/materias/bachillerato/quimica-2bachillerato/curriculo-quimica-2-bachillerato-lomloe) donde solo se cita espectro, aunque en bloque A de 1º se cita "interacción con la radiación electromagnética" y en 1º Madrid **Los espectros atómicos** <br><br> Desaparece: <br> Deja de citarse explícitamente "Determinación de fórmulas empíricas y moleculares", aunque Madrid añade **relaciones estequiométricas ...en la composición de los compuestos...Composición centesimal de un compuesto** <br> Dejan de citarse propiedades coligativas (y ya no es necesaria la molalidad) y presión osmótica, y aunque a nivel estatal se citan "disoluciones y sus propiedades" Madrid al concretar solo cita explícitamente "Concentración de una disolución: concentración en masa, molaridad y fracción molar"  | 
|  7. El átomo y sus enlaces  <br> Se citaba "Formulación y nomenclatura de los compuestos inorgánicos, siguiendo las normas de la IUPAC." | **Desaparece** (pasa a estar en [Química de 2º de Bachillerato LOMCE](/home/materias/bachillerato/quimica-2bachillerato/curriculo-quimica-2-bachillerato-lomce) ): se elimina cierta duplicidad existente <br> Desaparece mención a formulación inorgánica que estaba en este bloque de átomo y enlaces en LOE, y ahora se cita en bloque 3 "Formular y nombrar correctamente las sustancias que intervienen en una reacción química dada." | Saber básico **A. Enlace químico y estructura de la materia** <br> Se cita explícitamente nomenclatura inorgánica <br> Se puede considerar que se vuelve a situación anterior a LOMCE en cuanto a enlace, no en la parte de átomo de la que solo se cita configuraciones electrónicas, tabla periódica y propiedades periódicas, sin modelos atómicos<br> En Madrid se añade explícitamente **las triadas de Döbereiner y las octavas de Newlands**, **radio atómico, energía de ionización y afinidad electrónica**, los tres tipos de enlace, estructuras de Lewis, polaridad, fuerzas intermoleculares | 
|  8. Estudio de las transformaciones químicas. |  Bloque 3. Reacciones químicas  <br> -Desaparece mención a velocidad de reacción y factores de los que depende, tipos de reacciones   | Saber básico **B. Reacciones químicas**, en Madrid se añade termoquímica (se comenta en esta tabla en otro bloque asociado a energía) <br> Madrid cita mol, ley de Dalton de las presiones parciales, fracción molar <br> Madrid añade **Ley de Lavoisier de conservación de la masa, ley de Proust de las proporciones definidas y ley de Dalton de las proporciones múltiples. Composición centesimal de un compuesto.** <br> Madrid añade **Riqueza de un reactivo. Reactivo en exceso.** (rendimiento y limitante ya estaban desde LOE) <br> Se añade Clasificación de las reacciones químicas, que Madrid detalle de manera similar a como estaba en 4º ESO LOMCE |
|  9. Introducción a la química del carbono.  <br> Se cita en contenidos simplemente "Isomería" |  Bloque 5. Química del carbono  <br> -Se cita en contenidos "Isomería estructural", pero en criterios de evaluación "diferentes tipos de isomería"; se puede interpretar que se siguen contemplando todos los tipos.  <br> -Se añade explícitamente cadena cerrada, aromáticos, funciones oxigenadas y nitrogenadas, cosa que no se detallaba en LOE; en cualquier caso no se llega a dar tanto detalle como en  [FQ 4º ESO LOMCE](/home/materias/eso/fisicayquimica-4eso/curriculo-fisica-y-quimica-4-eso-lomce)  donde se indica "alcoholes, aldehídos, cetonas, ácidos carboxílicos, ésteres y aminas", pero al ser un curso posterior se puede asumir que al menos debe incluir esos grupos funcionales.  <br> -Se añade diferenciar las diferentes estructuras que presenta el carbono en el grafito, diamante, grafeno, fullereno y nanotubos relacionándolo con sus aplicaciones. | Saber básico **C. Química orgánica**  <br>  Se citan explícitamente **polifuncionales**, tras aclarar en 4º ESO son solo monofuncionales <br> Solo se citan "hidrocarburos, compuestos oxigenados y compuestos nitrogenados" sin concretar tantos grupos funcionales como LOMCE hacía en 4º ESO <br> <br> Desaparece: <br> Ya no se cita isomería en saberes básicos, solo en texto inicial Madrid asociado a curso siguiente |


## LOE vs LOMCE resumido

Un resumen rápido es que se elimina átomo y enlace para "bajar" de 2Bach Termoquímica, Leyes de Kepler y MAS, y se añade espectroscopía. En electricidad se deja de citar ley de Ohm y se pasa a citar Coulomb y energía electrostática.  
No se hace una tabla para este resumen al ser muy breve.  
Sin embargo sí se añade al final una comparación LOE vs LOMLOE  


## LOMCE vs LOMLOE resumido

Un resumen rápido es que desaparece el bloque de actividad científica, añaden polifuncionales y desaparece isomería, se recupera átomo (parte de tabla y propiedades periódicas) y enlace, vuelven a 2º termoquímica (aunque Madrid lo mantiene también en 1º), leyes Kepler y MAS, y desaparece espectroscopía. Se añade  sólido rígido, estática y termodinámica.  
Este resumen es más claro con una tabla, donde "-" quiere decir que no aparece explícitamente en saberes básicos.  

| **[LOMCE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomce)**   | **[LOMLOE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomloe)** |
|:-|:-|
|Actividad científica | - <br>**"transversal"** |
|Nomenclatura inorgánica | Nomenclatura inorgánica |
|Nomenclatura orgánica (monofuncionales) | Nomenclatura orgánica **(polifuncionales)**  |
| Isomería | - |
| - | Tabla y propiedades periódicas |
| - | Enlace |
| Propiedades coligativas | - |
| Determinación de fórmulas |- |
| Espectroscopía |- |
| Reacciones | Reacciones|
| Termoquímica | Termoquímica <br>**No aparece en estatal, se mantiene parte en Madrid** |
| Cinemática | Cinemática |
| Dinámica | Dinámica |
| - | **Sólido rígido. Estática** |
| Gravitación, leyes Kepler | - |
| Electricidad. Coulomb. Energía | - |
| Energía | Energía |
| MAS | - |
| - | **Termodinámica** |


## LOE vs LOMLOE resumido

Un resumen rápido es que añaden polifuncionales y desaparece isomería, se pierde átomo manteniendo tabla y propiedades periódicas, Madrid mantiene termoquímica en 1º, desaparece gravitación y electricidad. Se añade  sólido rígido, estática y termodinámica. 

| **[LOE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculofisicayquimica-1bachillerato)**  | **[LOMLOE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomloe)** |
|:-|:-|
|Actividad científica | - <br>**"transversal"** |
|Nomenclatura inorgánica | Nomenclatura inorgánica |
|Nomenclatura orgánica (monofuncionales) | Nomenclatura orgánica **(polifuncionales)**  |
| Isomería | - |
| Átomo, tabla y propiedades periódicas | **-,** tabla y propiedades periódicas |
| Enlace | Enlace |
| Propiedades coligativas | Propiedades coligativas |
| Determinación de fórmulas | Determinación fórmulas |
| Espectroscopía |- |
| Reacciones | Reacciones|
| - | Termoquímica <br>**No aparece en estatal, se mantiene en Madrid** |
| Cinemática | Cinemática |
| Dinámica | Dinámica |
| - | **Sólido rígido. Estática** |
| Gravitación | - |
| Electricidad. Ohm. Energía |- |
| Energía | Energía |
| - | **Termodinámica** |


