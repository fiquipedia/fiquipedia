
# Recursos electricidad

Los conceptos básicos de electricidad se ven en 3º ESO: en 1º de Bachillerato se profundizan y en  [Física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato)  se ve  [campo eléctrico](/home/recursos/fisica/recursos-campo-electrico) .  
Conceptos como aislante/conductor, signos de cargas y atracción/repulsión y tipos electrización pueden servir como introducción previa a tratar los modelos atómicos por primera vez, ya que para entender modelo Thomson y experimento Rutherford es necesario entender conceptos básicos de carga eléctrica.  
  
Los tema de circuitos eléctricos simples (ley de Ohm) también entrarían aquí, enlazan con materias como tecnología.  
  
Página en borrador, empiezo a guardar algún enlace  
  
 [Guapo, lo tuyo no es corriente — Cuaderno de Cultura Científica](https://culturacientifica.com/2014/03/28/guapo-lo-tuyo-es-corriente/)   
Conceptos; corriente, voltaje, resistencia, potencia  


## Electricidad estática / electrización
  
 [1_electrosttica.html](http://www.iesdmjac.educa.aragon.es/departamentos/fq/asignaturas/fq3eso/materialdeaula/FQ3ESO%20Tema%204%20Propiedades%20electricas%20de%20la%20materia/1_electrosttica.html)   
Globos y Electricidad Estática  
 [Globos y electricidad estática - PhET](https://phet.colorado.edu/sims/html/balloons-and-static-electricity/latest/balloons-and-static-electricity_es.html)   
 ![](https://phet.colorado.edu/sims/html/balloons-and-static-electricity/latest/balloons-and-static-electricity-900.png)  
  
[Travoltage - PhET](https://phet.colorado.edu/sims/html/john-travoltage/latest/john-travoltage_es.html)   
![](https://phet.colorado.edu/sims/html/john-travoltage/latest/john-travoltage-900.png)  
  
 [Electroscope - rimstar.org](http://rimstar.org/equip/electroscope.htm)   
 ![](https://rimstar.org/equip/electroscope/electroscope_demonstration_an.jpg)  
 
 [Decades later, hair-raising photo still a reminder of lightning danger - nbcnews.com](https://www.nbcnews.com/healthmain/decades-later-hair-raising-photo-still-reminder-lightning-danger-6C10791362)  
 ![](https://media-cldnry.s-nbcnews.com/image/upload/t_fit-1240w,f_auto,q_auto:best/streams/2013/July/130729/6C8448108-Lightning_Mike_and_Sean_V6-S.jpg)  
> The photo has been reprinted, posted and passed around for decades: Two grinning brothers, hair standing on end, unaware that they were minutes away from being struck by lightning while climbing Moro Rock in California’s Sequoia National Park.   

## Material para demostraciones / juguetes ...
 [energy-stick](https://www.eurekakids.es/juguete/eurekakids/energy-stick)   
 
 [twitter cossettej/status/1469061276936683523](https://twitter.com/cossettej/status/1469061276936683523)  
 A plasma ball, a fluorescent bulb, a table, and a sprinkler head for a path to ground. This is one of my favorite electricity demos and it still amazes me every time.   
 
 [![](https://img.youtube.com/vi/ODbgKXFED5o/0.jpg)](https://www.youtube.com/watch?v=ODbgKXFED5o "Electrical conductivity with salt water")  

## Circuitos eléctricos
 [Simulaciones en categoría Electricidad, Imanes y Circuitos - phet.colorado.edu](https://phet.colorado.edu/es/simulations/filter?subjects=electricity-magnets-and-circuits&type=html,prototype)   
 
[Kit de Construcción de Circuitos: DC - phet.colorado.edu](https://phet.colorado.edu/es/simulations/circuit-construction-kit-dc)  
![](https://phet.colorado.edu/sims/html/circuit-construction-kit-dc/latest/circuit-construction-kit-dc-900.png)  

[Tough concepts made easy - electricty](https://www.toppr.com/ask/content/posts/effects-of-current/tough-concepts-made-easy-21983/)  
1 Electric flow analogy  
2 Heating Effect of Current  
3 System of Parallel Resistors  
4 System of Series Resistors
  
En el  [Currículo de 2º y 3º ESO Física y Química](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce)  se cita dentro del bloque de Energía como "Energía en circuitos eléctricos"  


[Electricidad y electrónica. Circuitos eléctricos, ley de Ohm, dispositivos electrónicos y aspectos industriales de la energía. 2 y 3 ESO - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2eso-3eso/electricidad-electronica/)  
 [Electricidad y electronica 2 y 3 ESO, poster (pdf) - fisiquimicamente](https://rodrigoalcarazdelaosa.me/recursos-fisica-quimica/apuntes/2eso-3eso/electricidad-electronica/electricidad-electronica-poster.pdf)   
 
 [Currents and DC circuits. KaiserScience. High school science resources & lessons](https://kaiserscience.wordpress.com/physics/electromagnetism/electric-currents/)  
 
## Superconductividad

Pongo inicialmente superconductividad junto a campo eléctrico, que es donde está inicialmente la ley de Ohm. Con el tiempo se puede reorganizar.  La superconductividad enlaza con materiales y química, con electrotecnia ...

[How do superconductors work? A physicist explains what it means to have resistance-free electricity  - theconversation.com](https://theconversation.com/how-do-superconductors-work-a-physicist-explains-what-it-means-to-have-resistance-free-electricity-202308)  

[Periodic Graphics: The science of superconductors - acs.org](https://cen.acs.org/materials/electronic-materials/Periodic-Graphics-science-superconductors/101/i31?sc=230918_sc_eng_tw_cen)    
![](https://s7d1.scene7.com/is/image/CENODS/10131-feature4-graphic?$responsive$&wid=700&qlt=90,0&resMode=sharp2)  
 
