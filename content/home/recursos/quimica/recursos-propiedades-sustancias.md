
# Recursos propiedades sustancias

Conocer las propiedades de las sustancias en función de los tipos de  [enlace químico](/home/recursos/quimica/enlace-quimico)  es algo que se ve a lo largo de varios cursos: Física y Química en 3º ESO, 4º ESO, 1º Bachillerato, y Química 2º Bachillerato.  
Inicialmente se suelen describir los tres tipos de enlaces (iónico, covalente y metálico) y dar propiedades asociadas que se memorizan. En 4º se empiezan a razonar, aunque no se entra en las fuerzas intermoleculares, cosa que sí se inicia en 1º de Bachillerato. Mi visión es que en Química de 2º de Bachillerato no se amplía mucho más.  

Incluyo aquí una tabla genérica pensada inicialmente para 1º de Bachillerato, que habría que reducir para ESO, donde se intentan incluir razonamientos para todas las propiedades. Incluyo la idea de sólidos amorfos, que no suele ser tratada, porque me parece relevante.  

[PropiedadesVsTipoSustanciaEnlaces.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/recursos-propiedades-sustancias/PropiedadesVsTipoSustanciaEnlaces.pdf)

En 2014 reviso la tabla para 2º de bachillerato, tras hacer los  [apuntes de química de 2º](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato)  del bloque de enlace y propiedades de las sustancias, y prácticamente la dejo intacta.  
En 2015 creo  [apuntes elaboración propia para 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso/apuntes-elaboracion-propia-3-eso)  y unos de ellos son de enlace, donde pongo una tabla muy reducida.  
En 2017 creo pizarras asociadas a  [materiales elaboración propia 4º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso/apuntes-fisica-y-quimica-4-eso-elaboracion-propia)  asociadas a enlace  

Al hablar de distintas propiedades del carbono según su estructura en ESO suele surgir a idea de diamante a partir de otro tipo de carbono, y cito esta idea que suele impactar a los alumnos  
[Cenizas humanas convertidas en diamantes - wtf.microsiervos.com](http://wtf.microsiervos.com/teletienda/cenizas-humanas-convertidas-en-diamantes.html)  


[twitter ChemistryTe/status/1581630805868445696](https://twitter.com/ChemistryTe/status/1581630805868445696)  
Vídeo con buretas de hexano, agua e isopropanol y una varilla con electricidad estática para mostrar polaridad  

Algunos enlaces:  
[http://www.gobiernodecanarias.org/educacion/3/usrn/lentiscal/1-cdquimica-tic/flashq/enlaceq/intermoleculares/Teoria-Fintermoleculares.htm](http://www.gobiernodecanarias.org/educacion/3/usrn/lentiscal/1-cdquimica-tic/flashq/enlaceq/intermoleculares/Teoria-Fintermoleculares.htm)  
Recopilación de applets, animaciones, propuesta didáctica y diseño Web: Grupo Lentiscal -2005  

La idea de diamante enlaza con formas alotrópicas de carbono que se trata en 1º Bachillerato (ver proyecto de investigación asociado)  

[twitter ccauqui/status/603302638600454145](https://twitter.com/ccauqui/status/603302638600454145)  
'A diamond is just a piece of charcoal that handled stress exceptionally well'  
![](https://pbs.twimg.com/media/CF9cjIRWYAA1cw8?format=jpg)  

[twitter Biochiky/status/1177090994602426369](https://twitter.com/Biochiky/status/1177090994602426369)  
Hoy tocan ejercicios de orgánica. Ese C bonito.  
![](https://pbs.twimg.com/media/EFXeBIrXUAEPIl1?format=jpg)  

Asociado a propiedades se pueden ver más cosas  

Propuesta de José Martín, blog Escepticismo en el aula  
[Aprendiendo Física desde los 5 años](https://misterioeducacionyciencia.blogspot.com/p/aprendiendo-f.html)  
Como el movimiento se demuestra andando, vamos a remangarnos y vamos a poner en práctica algunas cuestiones didácticas. ¿Qué os parece si organizamos unas sesiones de Física alrededor del agua? ...  
ACTIVIDADES  
[Actividades del bloque inicial](https://docs.google.com/file/d/0B9z7VZfsQtXNeHRHY2E3MDAwb0U/edit?usp=sharing)  
Actividades del bloque continuo:  
B.1.  [ Propiedades mecánicas I](https://docs.google.com/file/d/0B9z7VZfsQtXNTjd4VnJ5WHh0bFE/edit?usp=sharing)  (*peso, volumen y forma*)  
B.2.  [Propiedades mecánicas II](https://docs.google.com/file/d/0B9z7VZfsQtXNUjFLemliLTcwN1k/edit?usp=sharing)  (*volumen*)  
B.3.  [Propiedades mecánicas III](https://docs.google.com/file/d/0B9z7VZfsQtXNdm1lUDNycktUNDA/edit?usp=sharing)  (*comparación de volumen (tamaño) y forma*)  
B.4.  [Propiedades mecánicas IV](https://docs.google.com/file/d/0B9z7VZfsQtXNZV9MaWp0bHdhMVk/edit?usp=sharing)  (*densidad y flotación*)  
B.5.  [Propiedades mecánicas V](https://docs.google.com/file/d/0B9z7VZfsQtXNekpKNTJ5LVFuWGc/edit?usp=sharing)  (*el aire no se ve, no huele, no sabe a nada*)  
B.6.  [Propiedades mecánicas VI](https://docs.google.com/file/d/0B9z7VZfsQtXNRjNXWDdiTzh0aG8/edit?usp=sharing)  (*sonido, vibración*)  
B.7.  [Propiedades mecánicas VII](https://docs.google.com/file/d/0B9z7VZfsQtXNLXI2QVdMdmY4QWc/edit?usp=sharing)  (*sonido con recipientes, tonos*)  
B.8.  [Propiedades mecánicas VIII](https://docs.google.com/file/d/0B9z7VZfsQtXNT29fSm5UcDY1QlE/edit?usp=sharing)  (*interferencia de ondas, ruido*)  
B.9.  [Propiedades mecánicas IX](https://docs.google.com/file/d/0B9z7VZfsQtXNWTRxdFowN0Nxb1E/edit?usp=sharing)  (*ondas, amplitud de onda, olas*)  
B.10.  [Propiedades mecánicas X](https://docs.google.com/file/d/0B9z7VZfsQtXNbnhzOWRTRkR3Njg/edit?usp=sharing)  (*tensión superficial*)  
B.11.  [Propiedades mecánicas XI](https://docs.google.com/file/d/0B9z7VZfsQtXNVHBmS0FjUW5GRnM/edit?usp=sharing)  (capilaridad; agua: alimento de plantas)  
B.12.  [Propiedades mecánicas XII](https://docs.google.com/file/d/0B9z7VZfsQtXNUkZmMkU2cXFiRGM/edit?usp=sharing)  (*el jabón rompe la tensión superficial*)  
B.13.  [Propiedades térmicas I](https://docs.google.com/file/d/0B9z7VZfsQtXNSmloVnI0cFFOXzQ/edit?usp=sharing)  (*sensación de frío y calor, temperatura*)  
B.14.  [Propiedades térmicas II](https://docs.google.com/file/d/0B9z7VZfsQtXNZ0dERWlHX2ctZ2M/edit?usp=sharing)  (*hielo y vapor*)  
B.15.  [Propiedades térmicas III](https://docs.google.com/file/d/0B9z7VZfsQtXNSGRZdGdVQzd4dmM/edit?usp=sharing)  (*hielo y vapor, el ciclo del agua*)  
B.16.  [Propiedades ópticas I](https://docs.google.com/file/d/0B9z7VZfsQtXNQ01HMGMxdkxJMlU/edit?usp=sharing)  (*el agua es transparente*)  
B.17.  [Propiedades ópticas II](https://docs.google.com/file/d/0B9z7VZfsQtXNQTBrS1N4QXZQRlE/edit?usp=sharing)  (*el agua y el aire son transparentes; refracción de la luz*)  
B.18.  [Propiedades ópticas III](https://docs.google.com/file/d/0B9z7VZfsQtXNbTZVU050enhxaUU/edit?usp=sharing)  (*la luz y los colores*)  
B.19.  [Propiedades químicas I](https://docs.google.com/file/d/0B9z7VZfsQtXNLUcwSHlpektvdms/edit?usp=sharing)  (*mezclas y disoluciones*)  
B.20.  [Propiedades químicas II](https://docs.google.com/file/d/0B9z7VZfsQtXNZlc0UGItQk1nd1E/edit?usp=sharing)  (*sustancias inmiscibles*)  
B.21.  [Propiedades químicas III](https://docs.google.com/file/d/0B9z7VZfsQtXNbTQ2S0JOVEl4d2s/edit?usp=sharing)  (*oxidación lenta*)  
B.22.  [Propiedades químicas IV](https://docs.google.com/file/d/0B9z7VZfsQtXNQVJXalN5dHhGeTQ/edit?usp=sharing)  (*átomos, oxígeno*)  

 
