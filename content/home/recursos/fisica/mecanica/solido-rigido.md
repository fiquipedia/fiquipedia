
# Recursos sólido rígido

Enlaza con [cinemática](/home/recursos/fisica/cinematica), [dinámica](/home/recursos/fisica/dinamica) y [estática](/home/recursos/fisica/mecanica/estatica).  
El sólido rígido se encuadra dentro de sistemas de partículas.  
Enlaza con momento de fuerzas y momento angular.  
Dentro del [currículo de 1º Bachillerato LOMLOE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomloe/) enlaza con ingeniería y deporte  

Se trataba en la antigua materia de  [mecánica de 2º de Bachillerato](/home/materias/bachillerato/mecanica-2-bachillerato) , pero con LOE desapareció.  

[Problemas de Dinámica de sistemas. José Carlos Vilches Peña, con soluciones (no resolución)](https://sites.google.com/site/jvilchesp/fisica/cou/sistemas)  

[Sólido rígido - montes.upm.es](http://www2.montes.upm.es/dptos/digfa/cfisica/solido/solido_portada.html)  

[Sólido rígido - sc.ehu.es fisica](http://www.sc.ehu.es/sbweb/fisica/solido/solido.htm)  
[Sólido rígido - sc.ehu.es fisica3](http://www.sc.ehu.es/sbweb/fisica3/solido/portada.html)  
Curso Interactivo de Física en Internet, Ángel Franco  

[Sistemas simples de sólidos rígidos - sistemas simples se sólidos rígidos - laplace.us.es](http://laplace.us.es/wiki/index.php/Sistemas_simples_de_s%C3%B3lidos_r%C3%ADgidos)  

[Física-I, 1 Ingeniería Química](https://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/Temario-index.html)  [Javier Junquera unican.es](https://personales.unican.es/junqueraj/)  
[Dinámica de los sistemas de partículas - unican.es](https://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/9.Dinamica_de_los_sistemas_de_particulas.pdf)  
[Colecciones de problemas de sistemas de partícullas elaborada por el Prof. José Javier Sandonís](https://personales.unican.es/junqueraj/javierjunquera_files/fisica-1/Sistemas_de_particulas_problemas.html)  

[Sistemas equivalentes de fuerzas - laplace.us.es](http://laplace.us.es/wiki/index.php/Sistemas_equivalentes_de_fuerzas_(CMR))  

[Momento Angular del Sólido Rígido en la Rotación - lagrangianos](https://lagrangianos.blogspot.com/2015/12/momento-angular-del-solido-rigido-en-la.html)  

Enlaza con centro de gravedad y centro de masas

[Problemas resueltos Centro de masas - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica3/problemas/solido/cm/cm.html)    

[Javier Fdez Panadero: Busco un centro de gravedad permanente. Naukas Bilbao 2021 - eitb.eus](https://www.eitb.eus/es/divulgacion/naukas/videos/detalle/8339375/video-javier-fdez-panadero-busco-centro-de-gravedad-permanente-naukas-bilbao-2021/)  

This Weird Shape Rolls Uphill Instead of Down - The action lab  
[![](https://img.youtube.com/vi/o4xTbyfQgps/0.jpg)](https://www.youtube.com/watch?v=o4xTbyfQgps "This Weird Shape Rolls Uphill Instead of Down - The action lab")  

[Playing with metal and physics](https://imgur.com/ExrHMje)  
Vídeo asociado a centro de gravedad.   

[Prácticas de seguridad relativas a la estabilidad de buques pesqueros pequeños - fao.org](https://www.fao.org/3/i0625s/i0625s02b.pdf)  

[Rolling the Black Pearl Over: Analyzing the Physics of a Movie Clip. Carl E. Mungan and John D. Emery, U.S. Naval Academy, Annapolis, MD. The Physics Teacher, Vol. 49, M ay 2011](https://www.usna.edu/Users/physics/mungan/_files/documents/Publications/TPT19.pdf)  

[twitter fqsaja1/status/1626691025736462338](https://twitter.com/fqsaja1/status/1626691025736462338)  
Aquí os dejo una preciosa experiencia 🧐para presentar el momento de inercia y la energía cinética de rotación. A nivel cualitativo...o cuantitativo.  
Si lo hacéis y me lo contáis me daréis una alegría.  
[twitter David_Alb_astro/status/1626188509919817732](https://twitter.com/David_Alb_astro/status/1626188509919817732)  
Montage fait d'après une expérience vue au Science Museum de Londres, il y a longtemps, et qui m'avait bluffé. Les 2 roues ont presque la même masse. Écrous collés sur 2 CD.  

[Eso va a ser de la física - ccasanueva](https://ccasanueva.wordpress.com/2014/02/27/eso-va-a-ser-de-la-fisica/)  
![](https://ccasanueva.files.wordpress.com/2014/02/fig3.png)  

[Cuál es la mejor manera de columpiarse según la física - francis.naukas.com](https://francis.naukas.com/2023/04/23/cual-es-la-mejor-manera-de-columpiarse-segun-la-fisica/)  
![](https://francis.naukas.com/files/2023/04/D20230423-physical-review-e-aps-pre-10-1103-PhysRevE-107-044203-participant-pumping-swing-in-lab.jpg)  

Does a Falling Slinky Defy Gravity? - Veritasium  
[![](https://img.youtube.com/vi/uiyMuHuCFo4/0.jpg)](https://www.youtube.com/watch?v=uiyMuHuCFo4 "Does a Falling Slinky Defy Gravity? - Veritasium")  
Slinky Drop - Veritasium  
[![](https://img.youtube.com/vi/wGIZKETKKdw/0.jpg)](https://www.youtube.com/watch?v=wGIZKETKKdw "Slinky Drop - Veritasium")  
Slinky Drop Answer - Veritasium  
[![](https://img.youtube.com/vi/eCMmmEEyOO0/0.jpg)](https://www.youtube.com/watch?v=eCMmmEEyOO0 "Slinky Drop Answer - Veritasium")  
Slinky Drop Extended - Veritasium  
[![](https://img.youtube.com/vi/oKb2tCtpvNU/0.jpg)](https://www.youtube.com/watch?v=oKb2tCtpvNU "Slinky Drop Extended - Veritasium")  
Supersized Slow-Mo Slinky Drop - Veritasium  
[![](https://img.youtube.com/vi/JsytnJ_pSf8/0.jpg)](https://www.youtube.com/watch?v=JsytnJ_pSf8 "Supersized Slow-Mo Slinky Drop - Veritasium")  
The Most Mind-Blowing Aspect of Circular Motion - All Things Physics  
[![](https://img.youtube.com/vi/AL2Chc6p_Kk/0.jpg)](https://www.youtube.com/watch?v=AL2Chc6p_Kk "The Most Mind-Blowing Aspect of Circular Motion - All Things Physics")  
El vídeo combina ideas de sólido rígido con movimiento circular  


## Deporte

[Todo sobre el Centro de Gravedad en el Cuerpo Humano #Biomecánica - fisioterapia.blogspot](https://fissioterapia.blogspot.com/2015/09/todo-sobre-el-centro-de-gravedad-en-el.html)  

Un atleta usa la física para romper récords mundiales - Asaf Bar-Yosef TEDed  
[![](https://img.youtube.com/vi/RaGUW1d0w8g/0.jpg)](https://www.youtube.com/watch?v=RaGUW1d0w8g "Un atleta usa la física para romper récords mundiales - Asaf Bar-Yosef TEDed))")  

Physics 15 Torque (14 of 27) Body Mechanics: Ex. 2, F=? To Lift Dumbbell  
[![](https://img.youtube.com/vi/DSB7dXw5PjA/0.jpg)](https://www.youtube.com/watch?v=DSB7dXw5PjA "Physics 15 Torque (14 of 27) Body Mechanics: Ex. 2, F=? To Lift Dumbbell")  

[twitter TechnologyVide0/status/1641828688135086081](https://twitter.com/TechnologyVide0/status/1641828688135086081)  
This is how to lift things  
[twitter Rainmaker1973/status/1683834630783533059](https://twitter.com/Rainmaker1973/status/1683834630783533059)  
A brilliant visual demonstration of why you should lift weights using legs  
[devanthechiro - instagram](https://www.instagram.com/p/CuZxsGhRRAE/)  

 Manual Handling - using a mannequinn to show lifting technique   
[![](https://img.youtube.com/vi/s0tZOlXZzGw/0.jpg)](https://www.youtube.com/watch?v=s0tZOlXZzGw "Manual Handling - using a mannequinn to show lifting technique ")  

La figura se usa en demostraciones y se vende
[Figura humana de demostración para levantar objetos correctamente W19007](https://www.girodmedical.es/figure-de-demonstration-lever-correctement-w19007.html)  


