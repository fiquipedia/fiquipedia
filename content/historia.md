# Historia

Comienzo citando este post de 2020 donde se comentan ideas generales  
[FiQuiPedia: pasado, presente y futuro](https://algoquedaquedecir.blogspot.com/2020/10/fiquipedia-pasado-presente-y-futuro.html)

El logotipo lo creo en 2017
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/2017-06-23-LogoFiquipedia.png)

Coloco aquí ideas generales de la historia que tenía inicialmente en la página de inicio/home, para descargarla, aunque se pueden mezclar otras ideas como [licenciamiento](/licenciamiento) y [planteamiento](/planteamiento)

El empujón definitivo para esta página ha sido realizar dos cursos en octubre de 2011: el curso del Instituto de Tecnologías Educativas "Creación, reutilización y difusión de contenidos", que me ha animado a publicar materiales como mi proyecto final y empezar a darle forma y contenido pensando al tiempo en el [licenciamiento](/licenciamiento), y el curso de EducaMadrid "La competencia digital en secundaria" que me ha dado algunas ideas de clasificación, organización y utilización de recursos. En 2013 realizo el curso "Recursos Educativos Abiertos para la enseñanza de las Ciencias" del INTEF, con el objetivo de ampliar el apartado ya existente sobre Recursos Educativos Abiertos, compartir recursos que he localizado y creado y ampliar los recursos citados con nuevos enlaces.En esta página intento comentar algunas [referencias a FiQuiPedia](/referencias-a-fiquipedia)

En 2014 incluyo un [código QR](http://es.wikipedia.org/wiki/Código_QR) en la página principal que contenga la dirección de la web; el objetivo es que si alguien lo ve lo pueda "anotar y guardarse" la dirección fácilmente (hay quien sigue anotando "fikipedia" e incluso "frikipedia".  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/FiquipediaQR.png)

En 2015 decido incluir [códigos QR](http://es.wikipedia.org/wiki/Código_QR) en los pdf que elaboro de modo que contengan como información el enlace para descargar el pdf; el objetivo es que si alguien ve unos recursos en una pantalla o impresos, capturado el código QR pueda descargárselos fácilmente en móvil/tablet, y al tiempo ahorre papel. El tema es que los códigos QR implican varias cosas: la url debe ser estable, por lo que en las cosas que creo con él tengo que tener clara su colocación en la página web y mantenerla, y otra que debe visualizarse/imprimirse bien: al final decido acortar las urls con [goo.gl](http://goo.gl/) para evitar demasiada densidad de información y así tener seguimiento de urls, además de ponerlo con tamaño suficiente (pruebo 1,8x1,8 cm) y no cortarse al imprimirse (la mayoría de las impresoras no pueden imprimir hasta el mismo borde del papel, así que intento dejar un borde de medio centímetro, y tras probar varias posiciones, decido colocar los códigos QR en la esquina inferior derecha. Otra opción que permiten los códigos QR es enlazar información, por ejemplo desde unos apuntes enlazar otros poniendo códigos QR dentro del propio documento según se citan otros materiales. Otro motivo para incluir los códigos QR es que al enlazar a la url de fiquipedia, siempre contendrán el enlace más actualizado; me he encontrado versiones desactualizadas de algunos materiales subidas a sitios varios de internet, por ejemplo a scribd, y no hay mayor problema porque los subo con licenciamiento Creative Commons, pero como inicialmente no citaba fiquipedia (usaba la dirección de email de profesor que proporciona Madrid), el documento aislado no contenía una referencia clara a cómo obtener la versión actualizada, cosa que con el código QR sí pasará. Si en algún momento decidiera abandonar el uso de Google Sites (por ejemplo porque Google lo abandone como proyecto como ha hecho con tantos otros), es posible que las url dejasen de ser exactamente las mismas, y los pdfs antiguos dejarían de ser operativos, pero asumo que es complicado intentar conseguir algo totalmente estable a medida que pasa el tiempo.  
Ese comentario escrito en 2014 es premonitorio: en 2021 realizo la migración a GitLab Pages, pero consigo mantener las urls.

En 2024 Google anuncia que en 2025 dejará de estar definitivamente operativo su acortador de urls, que era el que usaba en códigos QR
[Google URL Shortener links will no longer be available](https://developers.googleblog.com/en/google-url-shortener-links-will-no-longer-be-available/)  



