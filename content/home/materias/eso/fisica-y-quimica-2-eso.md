# 2º ESO Física y Química

* [Currículo (LOMCE)](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso)  
* [Currículo (LOMLOE)](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso-lomloe)  
* [Comparación currículo Física y Química 2º ESO](/home/materias/eso/fisica-y-quimica-2-eso/comparacion-curriculo-fisica-y-quimica-2-eso)
* [Recursos](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso)  
* Currículo con enlaces a recursos (pendiente) 
