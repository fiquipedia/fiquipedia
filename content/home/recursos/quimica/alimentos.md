
# Recursos química de los alimentos

Muy relacionado con [recursos quimiofobia](/home/recursos/quimica/quimiofobia)  

Un recurso esencial son las infografías asociadas a [etiqueta "food chemistry" en compoundchem](https://www.compoundchem.com/category/food-chemistry/)  

También se puede asociar a alimentación saludable, composición de los alimentos  
[Información sobre el modelo Nutri-Score - aesan.gob.es](https://www.aesan.gob.es/AECOSAN/web/para_el_consumidor/seccion/informacion_Nutri_Score.htm)  
[sin azúcar org](https://www.sinazucar.org/)  

[Vol. 34 (2023): Número especial. Química y Alimentos - Revista Educación Química - septiembre 2023](https://www.revistas.unam.mx/index.php/req/issue/view/6229)  
[Abordaje experimental para la enseñanza y el aprendizaje de la reacción de Maillard en química de los alimentos](https://www.revistas.unam.mx/index.php/req/article/view/86124/75902)  
[Alimentos: de la quimiofobia a la quimiofilia](https://www.revistas.unam.mx/index.php/req/article/view/86131/75909)  

[xatakaciencia.com La química de los alimentos naturales: también llevan 'número E'](http://www.xatakaciencia.com/quimica/la-quimica-de-los-alimentos-naturales-tambien-llevan-numero-e)  

[midietacojea.com Lo "natural" de la Stevia (Quimifobia del E-960)](https://www.midietacojea.com/2013/07/08/lo-natural-de-la-stevia-quimifobia-del-e-960/comment-page-1/)  

[James Kennedy Monash category / chemophobia](https://jameskennedymonash.wordpress.com/category/chemophobia/)  
[James Kennedy Monash category / infographics/](https://jameskennedymonash.wordpress.com/category/infographics/)  

En año 2023 CompoundChem hace un calendario de adviento con alimentos típicos de Navidad de distintos países  
[The Chemistry Advent Calendar 2023 - compoundchem.com](https://www.compoundchem.com/2023advent/)  
Por ejemplo el día 23 es el turrón de almendras  
[Day 23: Spain: Turrón](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2023/12/023-%E2%80%93-Chemistry-Advent-2023-%E2%80%93-Turron.png?w=1276&ssl=1)  

Hay una serie de imágenes muy interesantes, que suelo imprimir y poner en aulas y pasillos de los centros (ver autores y licenciamiento para usarlas), muchas de ellas en inglés, su autor es James Kennedy Monash, profesor australiano.  

[![](https://jameskennedymonash.files.wordpress.com/2013/12/ingredients-of-a-banana-poster-4.jpeg)](https://jameskennedymonash.wordpress.com/2013/12/12/ingredients-of-an-all-natural-banana/ "Ingredients of an All-Natural Banana")  
[![](https://jameskennedymonash.files.wordpress.com/2013/12/ingredients-of-all-natural-blueberries-poster.jpeg)](https://jameskennedymonash.wordpress.com/2013/12/20/ingredients-of-all-natural-blueberries/ "Ingredients of an All-Natural Blueberries")  
[![](https://jameskennedymonash.files.wordpress.com/2014/07/ingredients-of-all-natural-cherries-english.jpeg)](https://jameskennedymonash.wordpress.com/2014/07/19/ingredients-of-all-natural-cherries/ "Ingredients of All-Natural Cherries")  
[![](https://jameskennedymonash.files.wordpress.com/2014/07/ingredients-of-all-natural-coffee.png)](https://jameskennedymonash.wordpress.com/2014/07/26/ingredients-of-an-all-natural-coffee-bean/ "Ingredients of an All-Natural Coffee Bean")  
[![](https://jameskennedymonash.files.wordpress.com/2014/08/ingredients-of-a-lemon-english.jpg)](https://jameskennedymonash.wordpress.com/2014/08/17/ingredients-of-an-all-natural-lemon/ "Ingredients of an All-Natural Lemon")  
[![](https://jameskennedymonash.files.wordpress.com/2014/08/ingredients-of-an-all-natural-pineapple-english.jpg)](https://jameskennedymonash.wordpress.com/2014/08/21/ingredients-of-an-all-natural-pineapple/ "Ingredients of an All-Natural Pineapple")  
[![](https://jameskennedymonash.files.wordpress.com/2014/08/ingredients-of-an-all-natural-strawberry-english.jpg)](https://jameskennedymonash.wordpress.com/2014/08/22/ingredients-of-an-all-natural-strawberry/ "Ingredients of an All-Natural Strawberry")  
[![](https://jameskennedymonash.files.wordpress.com/2018/08/natural-pesticides-in-cabbage.png)](https://jameskennedymonash.wordpress.com/2018/08/09/natural-pesticides-in-a-cabbage/ "Natural pesticides in a cabbage")  

[![](http://3.bp.blogspot.com/-oTTWLtu0AQo/Uczxe5U1JzI/AAAAAAAADnQ/CN4zg07bUKQ/s640/quimicaG.jpg)]((http://charlatanes.blogspot.com.es/2013/06/de-que-esta-hecha-tu-manzana.html "¿De qué está hecha tu manzana?")  

[![](http://archive.senseaboutscience.org/data/images/Chemicals/Chemicals_Infographic_Apples_LoveHearts_web.png)](http://archive.senseaboutscience.org/blog.php/96/who-says-your-diet-is-chemical-free.html "Who says your diet is chemical-free?")  

[El Azúcar. valerialaura cc-by-nc-sa - ocw.ehu.eus](https://ocw.ehu.eus/pluginfile.php/59656/mod_resource/content/0/T1%20EL%20AZUCAR.pdf)  


[Periodic Graphics: The science of making cheese. Chemical educator and Compound Interest blogger Andy Brunning explores the biochemistry of cheese. ](https://cen.acs.org/food/food-science/Periodic-Graphics-science-making-cheese/100/i19)  
![](https://acs-h.assetsadobe.com/is/image//content/dam/cen/100/19/WEB/10019-feature3-graphic.jpg/?$responsive$&wid=700&qlt=90,0&resMode=sharp2)  

[Tequila Chemistry - Reactions Infographic - acs.org](https://www.store.acs.org/eweb/ACSTemplatePage.aspx?site=ACS_Store&WebCode=storeItemDetail&parentKey=eac300ac-b6cd-45a8-a30e-969d7c509e37&catKey=3268039d-813d-4511-98a9-8d724bd29137)  
![](https://www.store.acs.org/eweb/images/ACSStore/402-101.jpg)  

[Why Does Your Coffee Taste & Smell So Delicious? - Reactions Infographic - acs.org](https://www.store.acs.org/eweb/ACSTemplatePage.aspx?site=ACS_Store&WebCode=storeItemDetail&parentKey=7f978c2b-16c8-4eaa-b0b1-7e87a0c646ef&catKey=3268039d-813d-4511-98a9-8d724bd29137)  
![](https://www.store.acs.org/eweb/images/ACSStore/402-102.jpg)  

[International Coffee Day: Arabica vs robusta - compoundchem.com](https://www.compoundchem.com/2018/09/30/arabica-robusta/)  
![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2023/10/Coffee-arabica-vs-robusta-2023.png?resize=1536%2C1086&ssl=1)  

[What Gives Beer its Bitterness & Flavour? The Chemistry of Beer - compoundchem.com](https://www.compoundchem.com/2014/07/10/beerchemicals/)  
![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2023/08/Beer-2023.png?resize=1448%2C2048&ssl=1)  

[Why is milk white? The chemistry of milk - compoundchem](https://www.compoundchem.com/2018/06/02/milk/)  
![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2018/06/The-chemistry-of-milk-v2.png?resize=768%2C543&ssl=1)  

[A Guide to Common Fruit Acids - compoundchem](https://www.compoundchem.com/2016/02/25/a-guide-to-common-fruit-acids/)  
![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2016/02/Guide-to-Fruit-Acids.png?resize=768%2C543&ssl=1)  

[Polyphenols & Antioxidants – The Chemistry of Tea - compoundchem](https://www.compoundchem.com/2014/02/01/polyphenols-antioxidants-the-chemistry-of-tea/)  
![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2014/02/The-Chemistry-of-Tea-v2.png?resize=768%2C543&ssl=1)  



