
# 4º ESO Ampliación Física y Química

* [Currículo (LOE)](/home/materias/eso/ampliacion-fisica-y-quimica-4-eso/curriculo-ampliacion-fisica-quimica-4-eso)  
* [Currículo (LOMCE)](/home/materias/eso/ampliacion-fisica-y-quimica-4-eso/curriculo-ampliacion-fisica-quimica-4-eso-lomce)  
A nivel de recursos son básicamente materiales de 4º ESO y de  [prácticas / experimentos / laboratorio](/home/recursos/practicas-experimentos-laboratorio)  
* LOMLOE: ver 4º ESO Proyecto en Investigación Científica e Innovación Tecnológica. Técnicas de laboratorio de Física y Química en [ESO](/home/materias/eso)  

Proyectos de ampliación (blog de alumnos)
[http://ampliacionfyqcuartob.blogspot.com.es/](http://ampliacionfyqcuartob.blogspot.com.es/) (enlace validado en 2021)  
