
# Contenidos del currículo con enlaces a recursos: 4º ESO Física y Química

Esta página está en construcción, es solamente un primer borrador. Pretende conseguir uno de los objetivos puestos en la página inicial de la web. La inicio antes de crear [FQ por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel)  
Para ver el currículo completo "estático", mirar  [Currículo 4º ESO Física y Química](/home/materias/eso/fisicayquimica-4eso/curriculofisicayquimica-4eso)  

La idea es poner recursos asociados a cada bloque de los  [contenidos](/home/materias/eso/fisicayquimica-4eso/curriculofisicayquimica-4eso) , asociándolos luego a objetivos y criterios de evaluación.  
Lo ideal sería que la propia página de contenidos tenga sobre cada contenido los enlaces a los recursos asociados: de momento los pongo aquí separados, y unirlos ya será un trabajo aparte. En general se enlazará a recursos según su tipo: por ejemplo recursos de la tabla periódica, aunque dentro de ese tipo, habrá que seleccionar los que apliquen al nivel de 4º ESO  
Ejemplos parecidos a lo que pretendo  
[Física y Química para 4ºESO - aprendiendociencias.wordpress.com](http://aprendiendociencias.wordpress.com/fisica-y-quimica-para-4%C2%BA-eso/)   
Aída Aivars, licenciamiento cc-by-nc-sa  
[Categoría 4ºESO - aprendiendociencias.wordpress.com](http://aprendiendociencias.wordpress.com/category/4-eso/)  (antes en  [Física y Química para 4ºESO - fyq4eso.wordpress.com/](http://fyq4eso.wordpress.com/) )  
[Curso 2010-11 IES Misteri d'Elx. FQ 4º ESO‎ > ‎11. Los átomos y sus enlaces - compartimosciencia](https://sites.google.com/site/compartimosciencia/cursos-anteriores/201011-4eso-iesmisteridelx/11-los-atomos-y-sus-enlaces)  (muy similar a la idea que busco, pero para currículo comunidad valenciana). Aída Aivars, licenciamiento cc-by-nc-sa  


Además de los que se enlacen aquí, puede haber recursos que apliquen en más sitios con otra catalogación:  [simulaciones](/home/recursos/simulaciones) ,  [apuntes](/home/recursos/apuntes) ,  [materiales autosuficientes](/home/recursos/materiales-autosuficientes) , ...  
Puede haber elementos reutilizables de 3º ESO, según el nivel de los alumnos y lo visto en el curso anterior.

###  Bloque 1. Introducción al trabajo experimental 
   * Las magnitudes y su medida. El Sistema Internacional de unidades. Carácter aproximado de la medida. Notación científica. Redondeo*
   * Aparatos de medida. Medida de masas: balanzas. Medidas de volumen. Medidas de longitud: regla y calibrador. Medidas de tiempo: cronómetro.  
   *Estos dos puntos enlazan con  [recursos magnitudes-medición-unidades](/home/recursos/recursos-medida)  y con  [recursos notación científica](/home/recursos/recursos-notacion-cientifica)* 
   * El trabajo en el laboratorio. Formulación de hipótesis y diseños experimentales. Análisis e interpretación de resultados experimentales.
   * La comunicación científica: el informe científico. Reglas y ejemplos  
   *Estos puntos enlazan con  [recursos método científico](/home/recursos/recursos-metodo-cientifico)  y  [recursos prácticas-experimentos-laboratorio](/home/recursos/practicas-experimentos-laboratorio)* 

###  Bloque 2. Fuerzas y movimiento. 

####  Iniciación al estudio del movimiento. (ver [cinemática](/home/recursos/fisica/cinematica)  )

   * Movimiento y sistema de referencia. Trayectoria y posición. Desplazamiento y espacio recorrido. Velocidad y aceleración.
   * Estudio del movimiento rectilíneo y uniforme. Estudio del movimiento rectilíneo y uniformemente acelerado.
   * Análisis de los movimientos cotidianos.

####  Las fuerzas y el equilibrio.  (ver [dinámica](/home/recursos/fisica/dinamica)  )

   * Las fuerzas y sus efectos estáticos.
   * Composición y descomposición de fuerzas.
   * Equilibrio de fuerzas.
   * Fuerzas en los fluidos. Concepto de presión. Presiones hidrostática y atmosférica. Aplicaciones
   * El principio de Pascal y la multiplicación de la fuerza.
   * El principio de Arquímedes y la flotación de barcos y globos. Tensión superficial. > Ver  [recursos fuerzas en fluidos](/home/recursos/fisica/fuerzas-en-fluidos) 

####  Las fuerzas y el movimiento. 

   * Las leyes de la Dinámica y la superación de la física del sentido común.
   * Tratamiento cualitativo de la fuerza de rozamiento.
   * La ley de la Gravitación universal y la culminación de la primera de las revoluciones científicas. El peso de los cuerpos y su caída. El movimiento de planetas y satélites.

###  Bloque 3. Energía, trabajo y calor 

####  Trabajo, potencia y energía mecánica. (ver [recursos energía](/home/recursos/energia)  )

   * Concepto de trabajo. Unidades. Trabajo mecánico. Aplicación a máquinas y herramientas. Concepto de Potencia.
   * La energía mecánica y sus formas. El trabajo como transferencia de energía mecánica. La conservación de la energía mecánica.

####  Calor y energía térmica. (ver [recursos calor](/home/recursos/calor)  )

   * Concepto de temperatura. Energía térmica.
   * Transferencia de energía por efecto de diferencias de temperatura.
   * Conservación y degradación de la energía. Efectos del calor sobre los cuerpos.

####  La energía y las ondas: luz y sonido 

   * Concepto de onda. Tipos y características de las ondas.
   * Transferencia de energía sin transporte de materia.
   * La luz y el sonido. Propiedades de su propagación. Espectro lumínico y espectro acústico. Ver  [recursos de simulaciones](/home/recursos/simulaciones) 

###  Bloque 4. Estructura y propiedades de las sustancias. 

####  El átomo y las propiedades de las sustancias. 

   * La estructura del átomo. [recursos estructura del átomo](/home/recursos/quimica/recursos-estructura-del-atomo)
   * El sistema periódico de los elementos químicos.
   * Clasificación de las sustancias según sus propiedades. Estudio experimental.
   * El enlace químico: enlaces iónico, covalente y metálico.
   * Interpretación de las propiedades de las sustancias.
   * Introducción a la formulación y nomenclatura inorgánica según las normas de la IUPAC.

####  Las reacciones químicas. 

   * Tipos de reacciones químicas.
   * Relaciones estequiométricas y volumétricas en las reacciones químicas.
   * Calor de reacción. Concepto de exotermia y endotermia.
   * Velocidad de una reacción química. Factores que influyen.

###  Bloque 5. Iniciación a la estructura de los compuestos de carbono. 

####  La química de los compuestos de carbono. 

   * El carbono como componente esencial de los seres vivos. El carbono y la gran cantidad de compuestos orgánicos. Características de los compuestos de carbono.
   * Descripción de los compuestos orgánicos más sencillos: Hidrocarburos y su importancia como recursos energéticos. Alcoholes. Ácidos orgánicos.

####  Polímeros sintéticos. 

   * Fabricación y reciclaje de materiales plásticos.
   * Macromoléculas: importancia en la constitución de los seres vivos.
   * Valoración del papel de la química en la comprensión del origen y desarrollo de la vida.

###  Bloque 6. La contribución de la ciencia a un futuro sostenible 

####  El desafío medioambiental. 

   * El problema del incremento del efecto invernadero: causas y medidas para su prevención.
   * Cambio climático
   * Contaminación sin fronteras.
   * Agotamiento de recursos.
   * Reducción de la biodiversidad.

####  Contribución del desarrollo tecno-científico a la sostenibilidad. 

   * Importancia de la aplicación del principio de precaución y de la participación ciudadana en la toma de decisiones.
   * Energías limpias.
   * Gestión racional de los recursos naturales.
   * Valoración de la educación científica de la ciudadanía como requisito de sociedades demócratas sostenibles.
   * La cultura científica como fuente de satisfacción personal
 
