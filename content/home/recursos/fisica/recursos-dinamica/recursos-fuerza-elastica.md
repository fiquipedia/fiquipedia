
# Recursos fuerza elástica

Aquí la fuerza elástica hace referencia a muelles, resortes, ley de Hooke, dinamómetros y [movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio).  
La parte asociable a "fuerza recuperadora" que se puede visualizar en ciertos casos como "elástica" (por ejemplo un péndulo) no se trata aquí.  

Ejercicios de elaboración propia limitados a ley de Hooke  
[Problemas Física Dinámica Hooke.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/fisica/recursos-dinamica/ProblemasFisica-Dinamica-Hooke.pdf)  
[Problemas Física Dinámica Hooke soluciones.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/fisica/recursos-dinamica/ProblemasFisica-Dinamica-Hooke-soluc.pdf)  

El [currículo de 2º y 3º ESO LOMCE](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce) se citaban muelles en estándares de aprendizaje evaluables  
>1.2. Establece la relación entre el alargamiento producido en un ***muelle** y las fuerzas que han producido esos alargamientos, describiendo el material a utilizar y el procedimiento a seguir para ello y poder comprobarlo experimentalmente. 
1.4. Describe la utilidad del **dinamómetro** para medir la **fuerza elástica** y registra los resultados en tablas y representaciones gráficas expresando el resultado experimental en unidades en el Sistema Internacional.  

En [currículo de 2º ESO LOMLOE](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso-lomloe/) 
> Medidas de fuerzas. Fuerzas y deformaciones.  

En [currículo de 1º Bachillerato LOMLOE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomloe/)  
>  La fuerza elástica. Ley de Hooke.

[![](https://img.youtube.com/vi/iN1beukMJJc/0.jpg)](https://www.youtube.com/watch?v=iN1beukMJJc "The First Hold & Release Bungee Jump | Damien Walters") 

[The Physics Of Bungee Jumping - real-world-physics-problems.com](https://www.real-world-physics-problems.com/physics-of-bungee-jumping.html)  

[![](https://img.youtube.com/vi/Cg73j3QYRJc/0.jpg)](https://www.youtube.com/watch?v=Cg73j3QYRJc "The Spring Paradox") 
This spring paradox is actually an analogy for Braess's Paradox which is about traffic. The surprising behaviour of the springs when the blue rope is cut is just like how journey times can actually go down when you close a major road, even with the same number of journeys being made.  

## Recursos laboratorio virtual / simulaciones
Medida de la constante elástica de un muelle.   
Versión 1 (applet java) [Medida de la constante elástica de un muelle](http://www.sc.ehu.es/sbweb/fisica/dinamica/trabajo/muelle/muelle.htm)   
Versión 2 (vídeos flash)  
Procedimiento estático  [Medida de la constante elástica de un muelle. Procedimiento estático](http://www.sc.ehu.es/sbweb/fisica_/dinamica/trabajo/muelle/laboratorio/muelle_lab.html)   
Procedimiento dinámico  [Medida de la constante elástica de un muelle. Procedimiento dinámico](http://www.sc.ehu.es/sbweb/fisica_/dinamica/trabajo/muelle/laboratorio1/muelle1_lab.html)   
Versión 3 (HTML5) [El muelle elástico](http://www.sc.ehu.es/sbweb/fisica3/oscilaciones/cte_muelle/cte_muelle.html) Curso Interactivo de Física en Internet © Ángel Franco García   
Laboratorio interactivo con vídeo  
Determinación de la constante elástica de un resorte:Procedimiento dinámico [Determinación de la constante elástica de un resorte: Procedimiento dinámico](http://www.dfists.ua.es/experiencias_de_fisica/index04.html)   
Determinación de la constante elástica de un resorte: procedimiento dinámico [![Determinación de la constante elástica de un resorte: procedimiento dinámico - YouTube](https://img.youtube.com/vi/Mqx8HmU2FYM/0.jpg)](https://www.youtube.com/watch?v=Mqx8HmU2FYM)  (11 min)  


### Muelles
 [Spring and wave java applet](http://teleformacion.edu.aytolacoruna.es/FISICA/document/applets/Hwang/ntnujava/springWave/springWave_s.htm)   
Movimientos Vibratorio Armónico Simple y Circular Uniforme  
[Simple Harmonic Motion and Uniform Circular Motion](http://teleformacion.edu.aytolacoruna.es/FISICA/document/applets/Hwang/ntnujava/shm/shm_s.htm)   
(Url mirror en España),  
Autor: Fu-Kwun Hwang, Dept. of physics, National Taiwan Normal University   
Traducción: José Villasuso  
  
Masas y resortes: intro, PhET, HTML5, Universidad de colorado.   
[![](https://phet.colorado.edu/sims/html/masses-and-springs-basics/latest/masses-and-springs-basics-900.png "Masas y Resortes: Intro")](https://phet.colorado.edu/es/simulations/masses-and-springs-basics)   

Masas y resortes, PhET, HTML5, Universidad de colorado.   
[![](https://phet.colorado.edu/sims/html/masses-and-springs/latest/masses-and-springs-900.png "Masas y resortes (PhET)") ](https://phet.colorado.edu/sims/html/masses-and-springs/latest/masses-and-springs_es.html) 
 
  
[Muelle Oscilante](http://www.walter-fendt.de/html5/phes/springpendulum_es.htm)   
© Walter Fendt, 24 Mayo 1998  
© Traducción: Juan Muñoz, 9 Marzo 1999  
*Permite ver velocidad, aceleración, energías con el tiempo*  
  
 [Determinación de la constante elástica de un muelle - labvirtual.webs.upv.es](http://labvirtual.webs.upv.es/04muelle.html)   

### Ley de Hooke
Los recuros sobre ley de Hooke "estática" intentaré moverlos a página separada, porque son utilizables en cursos anteriores a ver el MAS, aunque a veces simulaciones ven ambas cosas al tiempo  
  
  
Ley de Hooke  
 [index.html](http://schools.matter.org.uk/Content/HookesLaw/index.html)   
No trata MAS, simplemente ley de Hooke con muelle  
Materials Teaching Educational Resources © 1999 MATTER Project, The University of Liverpool  
  
Fuerza debida a un muelle y M.A.S.  
 [Spring Force and SHM](http://teleformacion.edu.aytolacoruna.es/FISICA/document/applets/Hwang/ntnujava/springForce/springForce_s.htm)   
(Url mirror en España),  
Autor: Fu-Kwun Hwang, Dept. of physics, National Taiwan Normal University   
Traducción: José Villasuso  
  
 [Ley de Hooke  | Educaplus](http://www.educaplus.org/play-119-Ley-de-Hooke.html)   
  
 [Ley de Hooke v2  | Educaplus](http://www.educaplus.org/game/ley-de-hooke-v2)   
  
 [hookes-law_es.html](https://phet.colorado.edu/sims/html/hookes-law/latest/hookes-law_es.html)   
  
José Antonio Navarro, simulación sencilla flash (nivel ESO)  
http://janavarro.iespadremanjon.es/images/flash/dinamica/ley_Hooke.swf (enlace no operativo octubre 2018, cambio a  [http://fq.iespm.es/](http://fq.iespm.es/) ) 
[IES Al-Ándalus &#8211; 41000557 Arahal (Sevilla)](http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=139&Itemid=199)  

[Laboratorio virtual: Ley de Hooke (4º ESO):](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq4eso/ley_hooke.swf)  
¡Practica con la ley de Hooke de los cuerpos elásticos!. ¿Serás capaz de dar con la escurridiza constante de elasticidad?Jose Antonio Navarro Dominguez   


