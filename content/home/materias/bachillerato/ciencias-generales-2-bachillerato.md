
# 2º Bachillerato Ciencias Generales

Se trata de una nueva materia en LOMLOE, que está en 2º de Bachillerato en modalidad General, en cierto modo sustituyendo a la materia [Cultura científica 1º Bachillerato (LOMCE)](/home/materias/bachillerato/cultura-cientifica-1-bachillerato) que junto a la cultura científica de 4º de ESO a su vez sustituyeron a [Ciencias para el Mundo Contemporáneo (LOE)](/home/materias/bachillerato/cienciasparaelmundocontemporaneo-1bachillerato) 

* [Currículo](/home/materias/bachillerato/ciencias-generales-2-bachillerato/curriculo-ciencias-generales-2-bachillerato-lomloe)  
* [Recursos](/home/recursos/recursos-por-materia-curso/recursos-ciencias-generales-2-bachillerato)   

Es relevante que se cita en [Artículo 12. Estructura de la prueba. Real Decreto 534/2024, de 11 de junio, por el que se regulan los requisitos de acceso a las enseñanzas universitarias oficiales de Grado, las características básicas de la prueba de acceso y la normativa básica de los procedimientos de admisión.](https://www.boe.es/buscar/act.php?id=BOE-A-2024-11858#a1-4)  

> 2. De conformidad con lo previsto en los artículos 10, 11, 12 y 13 del Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato, la materia específica obligatoria de modalidad a la que se hace referencia en el apartado anterior será:  
...
c) Para la modalidad General: **Ciencias Generales.**  
...  
>4. Cuando el título de Bachiller se haya obtenido de acuerdo con lo establecido en el artículo 23 del Real Decreto 243/2022, de 5 de abril, el alumnado se examinará de la materia que no hubiera escogido previamente al optar, conforme al apartado 1.b), entre Historia de España e Historia de la Filosofía. No obstante, podrá sustituir esta materia por:  
a) **Ciencias Generales**, si el título obtenido es el de Técnico o Técnica de Formación Profesional.


