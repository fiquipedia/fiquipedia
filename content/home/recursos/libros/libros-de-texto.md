
# Libros de texto

## **Nota inicial sobre licenciamiento**
>>**Nota inicial importante: si has llegado a esta página buscando libros para descargar, ese no es el objetivo de esta página. El objetivo es tener una relación de libros relacionados con Física y Química con sus datos (nivel, autor, editorial, ISBN...) y, solamente si existen/se conocen, incluir enlaces: el lugar enlazado desde aquí será el responsable de cumplir con el copyright (si es que el material lo tiene y si es que ese sitio almacena material y lo permite descargar)**

## Normativa sobre libros de texto

[Disposición adicional cuarta. Libros de texto y demás materiales curriculares. - LOE](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#dacuarta)  

> 1. En el ejercicio de la autonomía pedagógica, corresponde a los órganos de coordinación didáctica de los centros públicos adoptar los libros de texto y demás materiales que hayan de utilizarse en el desarrollo de las diversas enseñanzas.  
> 2. La edición y adopción de los libros de texto y demás materiales no requerirán la previa autorización de la Administración educativa. En todo caso, éstos deberán adaptarse al rigor científico adecuado a las edades de los alumnos y al currículo aprobado por cada Administración educativa. Asimismo, deberán reflejar y fomentar el respeto a los principios, valores, libertades, derechos y deberes constitucionales, así como a los principios y valores recogidos en la presente Ley y en la Ley Orgánica 1/2004, de 28 de diciembre, de Medidas de Protección Integral contra la Violencia de Género, a los que ha de ajustarse toda la actividad educativa.  
> 3. La supervisión de los libros de texto y otros materiales curriculares es competencia de las administraciones educativas y constituirá parte del proceso ordinario de inspección que ejerce la Administración educativa sobre la totalidad de elementos que integran el proceso de enseñanza y aprendizaje, que debe velar por el respeto a los principios y valores contenidos en la Constitución y a lo dispuesto en la presente ley.  

[RESOLUCIÓN de 10 de agosto de 2022, de la Viceconsejería de Organización Educativa, por la que se aprueba el Plan Anual de Actuación de la Inspección Educativa para el curso 2022-2023 - bocm.es](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/08/25/BOCM-20220825-4.PDF)  
> 4.3.2. Actuación de atención preferente de supervisión de los libros de texto y materiales curriculares adoptados por los centros docentes por la aplicación de la Ley Orgánica 3/2020, de 29 de diciembre, por la que se modifica la Ley Orgánica 2/2006, de 3 de mayo, de Educación:  
Correspondencia con Objetivos del Plan General Plurianual de Actuación:  
— Aumentar la eficiencia en la organización y en el funcionamiento de centros, programas y servicios educativos.  
— Evaluar la práctica docente para su mejora.  
Objetivos de la actuación:  
— Garantizar el cumplimiento de la norma en el procedimiento de adopción de los libros de texto y materiales didácticos.  
— Supervisar los libros de texto y otros materiales didácticos como elementos integrantes del proceso de enseñanza y aprendizaje.  
— Comprobar su ajuste al currículo, el rigor científico y la adecuación del nivel de profundización a las edades de los alumnos.  
— Comprobar que los conocimientos incluidos por el gobierno de la Comunidad de Madrid en los diferentes currículos, en ejecución de su potestad para regular un 40 % de los mismos, se contienen, de manera efectiva, en los libros de texto.  
— Velar por el respeto a los principios y valores contenidos en la Constitución, para evitar cualquier tipo de adoctrinamiento.  

Se puede ver cita adoctrinamiento, enlaza con [#YoAdoctrino y PIN parental](https://algoquedaquedecir.blogspot.com/2019/08/yoadoctrino.html)  


## Los libros de texto que se tratan en esta página
El concepto de "libro de texto" es el de libro tradicional para uso por los alumnos en el aula (y que sirve de guión y base de actividades para algunos profesores), también se incluyen libros para profesorado: solucionarios, recursos, fichas, libros de ejercicios ...  
Se ponen aquí libros de texto tradicionales asociados a materias relacionadas con Física y Química (se pueden incluir libros asociados a ámbitos científicos de Diversificación, FP Básica, Educación de Adultos..): a veces se pueden usar otro tipo de libros y de recursos  
Al ordenarlos por nivel lo hago según el nombre del nivel según la reforma educativa correspondiente ... (BUP, COU, ESO, ...) Aunque solamente tienen contenido parcial de física y química, también se incluyen libros de ámbitos científico-tecnológicos (diversificación LOE / programas enriquecimiento LOMCE / PCPI LOE, FP Básica LOMCE ...)  
Intento agrupar por editorial dentro de cada nivel, lo que puede ser útil.  
  
Para libros más generales, no dentro de la categoría de libros de texto y ya nivel universitario, ver  [libros](/home/recursos/libros)   
  
Por ejemplo en 2015 empiezo a poner aquí libros de texto asociados a física y química según LOMCE, ya que según calendario de implantación comienza 3º ESO y 1º Bachillerato en el curso 2015/2016, las editoriales empiezan a enviar muestras e información, y muchos centros se plantearán cambiar de libros. Es imposible hacer una revisión completa de cada libro, de algunos comento simplemente que son LOMCE, y por mirar algo rápido, reviso cómo tratan la formulación según normas IUPAC 2005, ya que es algo que antes de 2015 hacían bien pocos.  
  
Los libros son de materias asociadas a la especialidad de física y química: fundamentalmente física y química, pero también otras como CMC (1º Bachillerato LOE), Cultura científica (LOMCE), FP Básica (LOMCE) ....  
  
Los libros asociados a formulación, aunque la editorial los considere "libros de texto" y "asociados a un curso concreto", los pongo separados en la página sobre  [libros](/home/recursos/libros)  (cuando haya muchos ya los separaré)  

## Mi opinión sobre libros de texto tradicionales y su uso  

Creo que el libro de texto es algo tradicional y ligado a cierto profesorado, que se basa mucho en seguir un libro de texto como guión de clase; seguro que a muchos les sonarán frases del estilo ¿Por qué página íbamos?¿Y qué páginas del libro entran? "Eso no entra porque no está en el libro"  
No es que pretenda que las editoriales se queden sin trabajo, pero personalmente siempre que puedo no uso libros de texto; a veces hay clases y alumnos que necesitan otro tipo de materiales, y en esta página intento compartir materiales cc-by de elaboración propia, que son los que yo uso en clase, tanto "apuntes", como "ejercicios", como enlaces a recursos (vídeos, simulaciones ...). Un libro de texto tradicional lo veo condenado a quedar obsoleto, a no ajustarse a un profesor; cada uno tiene que crear o adaptar sus propios materiales, y si todos los profesores compartieran públicamente lo que crean, creo que los libros de texto tradicionales tendrían poco hueco.  
Otro tema importante es el coste; he estado en centros públicos donde el coste de los libros de texto es excesivo para ciertos alumnos y familias, y aunque haya un programa de intercambio de libros a veces los libros no valen de un año para otro, o tienen los huecos para realizar actividades buscando indirectamente evitar su reutilización.   
  
¿Por qué entonces esta página de libros de texto? Porque sigue siendo muy habitual que se usen, y a veces es tan triste como que los centros dan una lista sin ISBN, y a veces para el mismo ISBN hay varias variante de libros. Muchas veces se ve mal desde inspección que en las programaciones didácticas no se cite algún libro de texto como referencia ...  
A veces se llega a un centro, como interino y como funcionario, y uno se encuentra con que está fijado previamente el usar libros de texto y los alumnos ya los tienen   
Pongo información para que sea útil a alguien una vez recopilada y organizada, aunque para mi sea de escasa utilidad ...  

## Normativa y enlaces sobre libros de texto tradicionales y su uso  

 [http://www.xarxatic.com/la-libertad-de-catedra-y-los-libros-de-texto/](http://www.xarxatic.com/la-libertad-de-catedra-y-los-libros-de-texto/)   
Se cita sentencia Tribunal Constitucional 1981  
  
 [http://www.xarxatic.com/razones-para-usar-libros-de-texto/](http://www.xarxatic.com/razones-para-usar-libros-de-texto/)   
  
 [http://www.xarxatic.com/algunas-cosas-que-tengo-claras-despues-de-unos-anos-de-docencia/](http://www.xarxatic.com/algunas-cosas-que-tengo-claras-despues-de-unos-anos-de-docencia/)   
Artículo interesante que cita varias veces los libros de texto  
  
 [http://www.xarxatic.com/los-libros-de-texto-como-salvavidas-de-nuestros-peores-docentes/](http://www.xarxatic.com/los-libros-de-texto-como-salvavidas-de-nuestros-peores-docentes/)   
  
Contra la acriticidad del “libro de texto”  
 [ Educar | Información Confiable Sobre Su Salud, Finanzas, Familia y Vida](http://www.educar.org/articulos/contralaacriticidad.asp)   
  
 [http://www.xarxatic.com/el-turbio-negocio-de-los-libros-de-texto/)](http://www.xarxatic.com/el-turbio-negocio-de-los-libros-de-texto/)   
 [http://www.xarxatic.com/de-libros-de-texto-y-otras-zarandajas/](http://www.xarxatic.com/de-libros-de-texto-y-otras-zarandajas/)   
 [http://www.xarxatic.com/una-propuesta-para-los-libros-de-texto/](http://www.xarxatic.com/una-propuesta-para-los-libros-de-texto/)   
 ¿Por qué los libros de texto contribuyen al fracaso escolar?   
 [¿Por qué los libros de texto contribuyen al fracaso escolar? | ¡No al Fracaso Escolar!](https://fracasoacademico.wordpress.com/2014/05/03/por-que-los-libros-de-texto-contribuyen-al-fracaso-escolar/)   
Citas iniciales  
*“El libro de texto ha sido uno de los elementos omnipresentes en la escuela; un dispositivo tan consustancial a una forma de entender el proceso de enseñanza-aprendizaje que quizá algunos profesores no sabrían qué hacer sin él, se encontrarían desvalidos, no sabrían qué enseñar ni cómo hacerlo. Porque, **demasiado a menudo, el maestro se refugia detrás del libro de texto** y acaba haciendo él mismo lo que luego va a exigir a sus alumnos; recitar el manual. Y así, **los libros de texto han llegado a ser como la prótesis imprescindible para suplir las carencias culturales y científicas de ciertos enseñantes**”.* 
Jaume Trilla Bernet, Catedrático de la Facultad de Pedagogía de la Universidad de Barcelona. La aborrecida escuela, Editorial Laertes.   
*“A mi juicio, son las técnicas de marketing y nunca la calidad de los mismos, lo que explica el uso masivo de los libros de texto (…). (…) **jamás deben probar las editoriales que los libros de textos han sido sometidos a un proceso de investigación** propia, ni mucho menos a explicitar los efectos secundarios de cada marca. (…). Esta política liberal salvaje que se produce en la comercialización de los libros de texto siempre me ha parecido monstruosa (…)”.–*  
Santiago Molina García, Catedrático de Educación de la Universidad de Zaragoza. La escolarización obligatoria en el siglo XXI, Editorial La Muralla.  
*“**Subordinar el desarrollo de la tarea docente al libro de texto constituye un elemento de desprofesionalización**. Los profesores piensan que el texto debe adecuarse a los instrumentos de planificación de la enseñanza: proyecto curricular, programaciones, etc., pero posteriormente reconocen que, en la mayor parte de los casos, **es el libro de texto el que rige la vida de la clase**. El hecho de que el texto esté o no por encima del resto de elementos de planificación suscita numerosas contradicciones entre el profesorado, entre lo que debería ser y lo que realmente ocurre”.*  
Ana López Hernández, Libros de texto y profesionalidad docente, Avances en supervisión educativa, n6, 2007.  
*“**En los libros de texto no existe el concepto de sostenibilidad**, ni su contrario el de insostenibilidad. La ocultación de la gravedad de la crisis ecológica (y social) contemporánea es generalizada.” *Comisión de Educación Ecologistas en Acción.* [Informe_curriculum.pdf](http://www.oei.es/decada/portadas/Informe_curriculum.pdf)   

### Normativa de la comunidad de Madrid
Se indica  
"No utilizar libros de texto en aquellas asignaturas en las que, por su reducida dedicación horaria a los contenidos teóricos, su utilización sea puntual o esporádica, sustituyéndolos en la medida de lo posible por la elaboración de materiales propios o provenientes de otras fuentes de recursos educativos"  
  
RECOMENDACIÓN A LOS CENTROS EDUCATIVO SOBRE LIBROS Y MATERIAL ESCOLAR PARA EL CURSO 2011/2012, Madrid 10 de junio de 2011, Dirección General de Becas y Ayudas a la Educación, CONSEJERÍA DE EDUCACIÓN, Ref: 09/373340.9/11, CSV 0945775419529807608649  
 [](http://www.madrid.org/cs/Satellite?blobcol=urldata&blobheader=application/pdf&blobheadername1=Content-Disposition&blobheadervalue1=filename=Recomendacionlibrosdetexto2011-2012.pdf&blobkey=id&blobtable=MungoBlobs&blobwhere=1272078760517&ssbinary=true)   
  
RECOMENDACIÓN A LOS CENTROS ESCOLARES SOBRE LIBROS DE TEXTO PARA EL CURSO 2014-15, Junio 2014  
 [recomendaciones_librostexto.pdf](http://www.educa2.madrid.org/web/educamadrid/principal/files/66c3d0b3-9897-49ba-8567-2510edf2a286/recomendaciones_librostexto.pdf?t=1402398514193)   
  
En 2017 la Comunidad aprueba un programa de préstamo  
 
 [LEY 7/2017, de 27 de junio, de Gratuidad de los Libros de Texto y el Material Curricular de la Comunidad de Madrid.](http://www.bocm.es/bocm/Satellite?c=CM_Orden_BOCM&cid=1340490166585&idBoletin=1340490166370&idSeccion=1340490166398&language=es&pagename=Boletin%2FComunes%2FPresentacion%2FBOCM_popUpOrden)   

## Editoriales en España de libros de texto  

La idea es indicar aquí las editoriales habituales en España, con datos de contacto y enlaces donde ver su oferta actual de librosPor orden alfabético por neutralidad  

   * **Aljibe**  
 [Libros Pedagogía, Sociología, Educación Social, Psicología, LectoEscritura, Expresión Corporal, Dificultades Aprendizaje, Autismo, Logopedia, sobre Superdotados, Discapacidad Visual, Hiperactivid](http://www.edicionesaljibe.com/)   
   * **Altamar**  
 [http://www.altamar.es/](http://www.altamar.es/)   
   * **Anaya**  
 [http://www.anayamascerca.com/](http://www.anayamascerca.com/)   
 [Inicio - Anaya Educaci&oacute;n](http://www.anayaeducacion.es/)   
   * **Bruño**  
 [Inicio - Editorial Bruño](http://www.editorial-bruno.es/)   
 [Catálogo Bruño Física y Química ContextoDigital](http://www.editorial-bruno.es/catalogos/catalogos_educativos/BS00599101_9999984506.pdf)   
 En 2022 parece que forma parte de grupo anaya
   * **Casals**  
 [editorialcasals](http://www.editorialcasals.com/contacta/contacta.php)   
   * **Donostiarra**  
 [http://www.editorialdonostiarra.com/](http://www.editorialdonostiarra.com/) 
   * **Ecir**  
 [Editorial Tabarca Llibres, S.L.](http://www.ecir.com/)   
   * **Edebé**  
 [Atención al cliente - Contacta con Edebé](http://www.edebe.es/atencion_cliente/contacta-con-edebe.asp)   
En 2021 enlace a demos (algunos con flash)  
 [Espacio personal Edebé](http://epe2.edebe.com/etapa/secundaria)   
 [Espacio personal Edebé](http://epe2.edebe.com/etapa/bachillerato) 
   * **Edelvives**  
 [http://www.edelvives.com/](http://www.edelvives.com/)   
 [EDELVIVES #somoslink](http://www.somoslink.com/)   
   * **Editex**  
 [Editex - Libros de Texto, Material Didáctico y Literatura Juvenil ](http://www.editex.es/) 
   * **Marcombo**  
 [http://www.marcombo.com/](http://www.marcombo.com/)   
   * **Marfil**  
 [Editorial Tabarca Llibres, S.L.](http://www.editorialmarfil.com/)   
En 2014 es adquirida por Tabarca Llibres  
   * **McGraw Hill**  
 [index.html](http://www.mcgraw-hill.es/rep_locator/index.html) 
   * **Mcmillan**  
 [http://www.macmillan.es/](http://www.macmillan.es/)   
   * **Oxford**  
 [Contacto.aspx](http://www.oupe.es/es/InformacionGeneral/Paginas/Contacto.aspx)   
 [Oxford Inicia](http://oxfordinicia.es/)   
   * **Santillana**  
 [http://www.santillana.es/contacto/](http://www.santillana.es/contacto/)   
   * **SM**  
 [españa](http://www.grupo-sm.com/presencia/espa%C3%B1a)   
 [SMConectados. Herramientas, recursos didacticos y servicios educativos para profesores | smconectados.com](http://www.smconectados.com/)  SM Conectados 2.0, Servicio para profesores (materiales, recursos, ...)  
 [Inicio de sesión | SM Aprendizaje](http://www.smlir.com/)  Libro Interactivo en Red  
 [Inicio de sesión | SM Aprendizaje](http://smsaviadigital.com/)   
   * **Tabarca Llibres**  
 [Editorial Tabarca Llibres, S.L.](http://www.tabarcallibres.com/)   
   * **Vicens-Vives**  
 [Vicens Vives](http://www.vicensvives.com/)   

## Libros de texto libres
Enlaza con la idea de  [Recursos Educativos Abiertos](/home/recursos/recursos-educativos-abiertos)  
Hay iniciativas como [Didactica Física y Química](http://didacticafisicaquimica.es/)  que no solo tienen libros de texto.  
  
Ver  [LIBROS DE TEXTO DE FÍSICA Y QUÍMICA | Jaime Carrascosa Al&iacute;s | 2 updates | Research Project](https://www.researchgate.net/project/Libros-de-texto-de-Fisica-y-Quimica)   
Hay libros de texto varios niveles en pdf, por ejemplo (cuando lo consulto por primera vez son anteriores a LOMCE)  
 [Física y Química 3º ESO - Didactica Física y Química](http://didacticafisicaquimica.es/fisica-y-quimica-3-eso/)  (Revisado en Valencia 29-febrero-2016, ISBN: 84-688-7495-7, Depósito legal: V-3640-2003, PERMITIDA LA REPRODUCCIÓN TOTAL O PARCIAL DE ESTE LIBRO, CITANDO AUTORES Y FUENTE)  
 [Física y Química 4º ESO - Didactica Física y Química](http://didacticafisicaquimica.es/fisica-y-quimica-4o-eso/)   
 [Física y Química 1ºBachillerato - Didactica Física y Química](http://didacticafisicaquimica.es/fisica-y-quimica-1-bachillerato/)   
 [Física y Química 2º Bachillerato - Didactica Física y Química](http://didacticafisicaquimica.es/fisica-y-quimica-2-bachillerato/) Para los materiales se indica   
Los contenidos de esta obra se pueden reproducir total o parcialmente de forma libre y gratuita. Los autores no solo lo autorizamos expresamente sino que nos congratulamos de ello. Únicamente pedimos que se indique la fuente y que, por favor, siempre que sea posible, se colabore en su difusión dándolos a conocer a otras personas a las que también pudieran resultar útiles. Nuestro objetivo es contribuir, en lo que podamos, a la mejora de la enseñanza y aprendizaje de la Física y Química.  
En 2021 el listado de libros de texto dentro de  [Contenidos - Didactica Física y Química](https://didacticafisicaquimica.es/contenidos/)  es   
 [Física y Química de 2º de ESO. Libro del alumno](http://didacticafisicaquimica.es/?p=1504&preview=true)   
 [Física y Química de 2º de ESO. Secuencia de actividades](http://didacticafisicaquimica.es/?p=1519&preview=true)   
 [Física y Química de 2º de ESO. Libro del profesor](http://didacticafisicaquimica.es/?p=1528&preview=true)   
 [Física y Química de 3º de ESO](http://didacticafisicaquimica.es/fisica-y-quimica-3-eso/)   
 [Cuaderno de fichas Física y Química de 3º de ESO](http://didacticafisicaquimica.es/cuaderno-de-fichas-fisica-y-quimica-3-eso/)   
 [Física y Química de 3º de ESO – LOMCE](http://didacticafisicaquimica.es/fisica-y-quimica-de-3o-de-eso-lomce/)   
 [Física i Química de 3r d’ ESO](http://didacticafisicaquimica.es/fisica-i-quimica-3r-eso/)   
 [Quadern de fitxes de Física i Química de 3r d’ ESO ](http://didacticafisicaquimica.es/?p=838&preview=true%20%E2%80%8E)   
 [Física i Química de 3r d’ ESO – LOMCE](http://didacticafisicaquimica.es/?p=910&preview=true%20%E2%80%8E)   
 [Física y Química de 4º de ESO](http://didacticafisicaquimica.es/fisica-y-quimica-4o-eso/)   
 [Física i Química de 4t d’ESO](http://didacticafisicaquimica.es/?p=734&preview=true)   
 [Física y Química de 1º de Bachillerato](http://didacticafisicaquimica.es/fisica-y-quimica-1-bachillerato/)   
 [Física y Química de 1º de Bachillerato – LOMCE](http://didacticafisicaquimica.es/?p=816&preview=true)   
 [Física de 2º de Bachillerato](http://didacticafisicaquimica.es/fisica-y-quimica-2-bachillerato/)   

Otro ejemplo en 2022 con LOMLOE (aunque no cita licenciamiento) es [mainquifi](https://mainquifi.blogspot.com/)    
En 2022 compartía el libro de FYQ 1ºBACHILLERATO ADAPTADO A LA LOMLOE GRATIS (pdf, 128 MB, 245 páginas, pero en 2023 deja de funcionar ese enlace y comparte muestras parciales, y también se venden impresos en Amazon  

[LIBRO DE FÍSICA Y QUÍMICA 2°ESO LOMLOE CON SITUACIONES DE APRENDIZAJE Y RÚBRICAS (muestra de 6 de 9 unidades didáticas, 372 MB)](https://drive.google.com/open?id=1YJUURbY90bmOzB41tBO0Diz6-H_zSfH2&authuser=0)  
[LIBRO DE FÍSICA Y QUÍMICA 3° ESO LOMLOE CON SITUACIONES DE APRENDIZAJE Y RÚBRICAS (muestra de 6 de 9 unidades didáticas, 212 MB)](https://drive.google.com/open?id=1Er_MODoGJw9CtaQteVHxHscd1H-zSxQ8&authuser=0)  
[LIBRO DE FÍSICA Y QUÍMICA 4° ESO LOMLOE CON SITUACIONES DE APRENDIZAJE Y RÚBRICAS (muestra de 6 de 9 unidades didáticas, 223 MB)](https://drive.google.com/open?id=1zZac7Y4TyA-Q46x2HgVuLIwal4q1TBHJ&authuser=0)  
[LIBRO DE FÍSICA Y QUÍMICA 1° BACHILLERATO LOMLOE CON SITUACIONES DE APRENDIZAJE Y RÚBRICAS  (muestra de 7 de 10 unidades didáticas, 177 MB)](https://drive.google.com/open?id=1KtQ_hzdbQhFgwKFoVQm_2WM0XeE0OQ71&authuser=0)  
[LIBRO DE FÍSICA 2° BACHILLERATO LOMLOE CON SITUACIONES DE APRENDIZAJE Y RÚBRICAS (muestra de 4 de 6 unidades didáticas, 164 MB)](https://drive.google.com/open?id=1IVFmgrwAG6LM7f09YJgfmB-ZUZMDtpki&authuser=0)  
[3 ESO Nacional](https://drive.google.com/open?id=1XWjYwA0DhH30k5XuyPPzdY36Zs6F84a6&authuser=0)  
[PREPARA LA QUÍMICA DE 2ºBACHILLERATO Y SELECTIVIDAD: 2022/2023 (QUÍMICA 2ºBACHILLERATO Y SELECTIVIDAD) -mainquifi, amazon](https://www.amazon.es/dp/B0BF2Q722F)  
[PREPARA LOS ESTÁNDARES DE QUÍMICA - 2° de Bachillerato ySelectividad](https://app.box.com/s/snju01nqp5mpzpddeg50s1bqorcheul0)  
  
Ejemplos de libros en pdf  
Física. - 1º ed. - Buenos Aires : Ministerio de Educación de la Nación, 2011., ISBN: 978-950-00-0873-0  [fisica-1-serie-para-la-ensenanza-en-el-modelo-1-a-1](https://openlibra.com/en/book/fisica-1-serie-para-la-ensenanza-en-el-modelo-1-a-1) Física 2 / Silvia Stipcich y Graciela Santos. - 1a ed. - Buenos Aires : Educ.ar S.E.; Ministerio de Educación de la Nación; Buenos Aires: Educ. ar S.E., 2012, ISBN 978-987-1909-02-5  [fisica-2-serie-para-la-ensenanza-en-el-modelo-1-a-1](https://openlibra.com/en/book/fisica-2-serie-para-la-ensenanza-en-el-modelo-1-a-1)   

## Comparadores de precios de libros de texto
No comparan solamente libros de texto, suelen basarse en ISBN y comparar solamente en tiendas online, pero sirven para hacerse una idea de precios  
  
 [OKlibros: Busca, Compara y Ahorra en Libros de Texto](http://www.oklibros.com/)   
 [http://www.textolibros.com/](http://www.textolibros.com/)    
 [elcorteingles](http://www.elcorteingles.es/libros/generos/libros-de-texto)  
 [🥇 Compara, Busca y Compra LIBROS de Texto - 【Ahorra en Libros】](http://www.ahorraenlibros.com/) 

## Libros por nivel  

### ESO

#### 2º ESO  

**Aljibe**
   * Adaptación curricular. Ciencias Naturales. 2º de ESO; Montserrat Moreno Carretero; ISBN: 978-84-9700-401-5  

**Bruño**  

   * "Física y química ESO 2"; Rafael Jiménez Prieto y Pastora María Torres Verdugo;Editorial Bruño; ISBN 978-84-696-1315-3; Depósito legal:M 6239-2016
**Casals**
   * "Física y Química 2"; Marta Duñach Masjuan ,M. Duñach, E.M.Costafreda; Editorial Casals; ISBN 978-84-218-6087-8; Depósito legal B-2500-2016, Versión digital 978-84-218-5092-3
**Edebé**
   * "Física y Química 2. Bloque I. Química"; Editorial Edebé ISBN 978-84-683-1719-9 (Obra completa); Depósito legal B-4472-2016
   * "Física y Química 2. Bloque II. Física"; Editorial Edebé ISBN 978-84-683-1719-9 (Obra completa); Depósito legal B-4472-2016
   * 20219788468351957 [Edebé](https://econtent.edebe.com/eso2021/9788468351957_fyq2_cas/) 
**Edelvives**
   * "Física y Química ESO 2"; Editorial Edelvives; ISBN 978-84-140-0292-6; Depósito legal Z 381-2016  

**Editex**
   * "Física y Química 2º ESO"; Editorial Editex; 2016; ISBN 978-84-9078-754-0
**McGrawHill**
   * "Física y Química 2º ESO"; Enrique Andrés del Río, Francisco Larrondo Almeda, Ángel Rodríguez Cardona, Francisco Martínez Salmerón; Editorial McGrawHill; ISBN 978-84-486-0902-3
Oxford
   * "Física y Química 2 ESO. Volumen: materia y energía", Inicia dual, ISBN 978019050242-3, Depósito legal M-8158-2016
   * "Física y Química 2 ESO. Volumen: movimientos, fuerzas y el universo", Inicia dual, ISBN 978019050243-0, Depósito legal M-9722-2016
   * "Física y Química 2 ESO. Volumen: fenómenos eléctricos y magnéticos", Inicia dual, ISBN 978019050244-7, Depósito legal M-10818-2016
  
**Santillana**  
   * "Física y Química 2 ESO"; Serie investiga; Editorial Santillana; ISBN 978-84-680-1952-9; Depósito legal M-4313-2016  

**SM**  
   * "Física y Química 2 ESO"; José G.L. de Guereñu, Ángel González, Josefa Guitart, Josep Corominas; Editorial SM; ISBN 978-84-675-8681-7; Depósito legal M-03199-2016

#### 3º ESO  
  
**Aljibe**  

   * Adaptación curricular. Biología y Geología. Física y Química. 3º de ESO ; Montserrat Moreno Carretero; ISBN: 978-84-9700-457-2
**Anaya**
   * "Física y Química 3. Alumnado. On line. ESO"; Sabino Zubiaurre Cortés, Jesús María Arsuaga Ferreras, Ana María Morales Cas, Antonio Pérez Sanz; Editorial Anaya, ISBN 978-84-667-9605-7
   * "Física y Química 3"; Sabino Zubiaurre Cortés, Jesús María Arsuaga Ferreras, Ana María Morales Cas, Antonio Pérez Sanz; Editorial Anaya, ISBN 978-84-667-1411-2  
      *  [Índice: unidades, contenidos y competencias](http://www.anaya.es/catalogos/indices/ET01212401_9999980981.pdf)   
   * "Física y Química 3 ESO"; Proyecto "Aprender es crecer en conexión"; José Miguel Vílchez González, Ana María Morales Cas, Sabino Zubiaurre Cortés; Editorial Anaya; ISBN 978-84-678-5225-7 Depósito legal M-11318-2015  
*Es un libro adaptado a LOMCE, recibido en el centro en mayo 2015 un ejemplar promocional, son tres tomos "unidades 1 a 3", "unidades 4 a 6" y "unidades 7 a 9", e indica "La versión definitiva de este proyecto incluirá las últimas aportaciones del currículo de la comunidad autónoma". El ejemplar con ISBN no está separado en tres tomos.*  
   * "Física y Química 3 ESO. Propuesta didática y recursos fotocopiables"; Proyecto "Aprender es crecer en conexión"; José Miguel Vílchez González, Ana María Morales Cas, Sabino Zubiaurre Cortés; Editorial Anaya; 2015 (sin ISBN)
   * "Physics and Chemistry 3"; José Miguel Vílchez González, Ana María Morales Cas, Sabino Zubiaurre Cortés; Editorial Anaya, ISBN 978-84-678-5227-1  
Libro en inglés para centros bilingües  

**Bruño**
   * "ContextoDigital Física y Química 3 ESO", Editorial Bruño, ISBN 978-84-216-6748-4  
   * "ContextoDigital Física y Química 3 ESO - 3 volúmenes"; Editorial Bruño; ISBN 978-84-216-6827-6
   * "Física y Química 3 ESO"; Editorial Bruño, 2007, ISBN 978-84-216-2101-1  
   * "Cuadernos ESO Física y Química 3"; Editorial Bruño, ISBN 978-84-216-6481-0  
   * "Objetivo aprobar LOE: Física y Química 3 ESO"; Editorial Bruño; ISBN 978-84-216-6013-3  
*Sigue apareciendo con "LOE" en catálogo recibido en el centro en mayo 2015 donde aparecen ya libros LOMCE*  
   * "Código Bruño Física y Química ESO3"; Rafael Jiménez Prieto, Pastora Mª Torres Verdugo; Editorial Bruño, ISBN 978-84-696-0924-8; Depósito legal M-3744-2015  
*Es un libro adaptado a LOMCE, recibido en el centro en mayo 2015 un ejemplar promocional, indica "disponible en tres volúmenes". En tema "5 Elementos y compuestos. La tabla periódica." se ve el concepto de fórmula y se interpretan fórmulas dadas. En Anexo "Introducción a la nomenclatura y formulación inorgánica" está parcialmente adaptado a IUPAC 2005: se habla de óxidos como compuestos con O, sin mencionar combinaciones de O con halógenos. Sí que se cita fosfano. Al hablar de oxácidos se cita erróneamente ácido hiponitroso, que IUPAC 2005 ya no contempla.*
   * "Código Bruño Física y Química ESO3 - 3 volúmenes"; Rafael Jiménez Prieto, Pastora Mª Torres Verdugo; Editorial Bruño, ISBN 978-84-696-1019-0  
*Es un libro adaptado a LOMCE, aparece en catálogo recibido en el centro en mayo 2015*  
   * "Código Bruño Física y Química ESO3 Propuesta didáctica"; Rafael Jiménez Prieto, Pastora Mª Torres Verdugo; Editorial Bruño, ISBN 978-84-696-1020-6  
*Es un libro adaptado a LOMCE, aparece en catálogo recibido en el centro en mayo 2015*
   * "Código Bruño Física y Química ESO3 digital alumno"; Editorial Bruño, ISBN 978-84-696-1022-0  
Es un libro adaptado a LOMCE, aparece en catálogo recibido en el centro en mayo 2015
   * "Código Bruño Física y Química ESO3 digital profesor"; Editorial Bruño, ISBN 978-84-696-1023-7  
Es un libro adaptado a LOMCE, aparece en catálogo recibido en el centro en mayo 2015  

   * [Física y Química 3ºESO LOMLOE (enlace muestra 2022)](https://www.blinklearning.com/Cursos/c3960786_c388853742__Portada.php&page=index)  
   [Física y Química 3ºESO, proyecto 5 etapas, muestra](https://issuu.com/grupoanayasa/docs/fyq3e?fr=sODhhNDM5MDYyMDc)  
   El [Proyecto 5 Etapas](https://www.hablamosdeeducacion.es/comunidad-de-madrid/proyectos-educativos/proyecto-5-etapas) de Bruño Educación está basado en el modelo 5E de enseñanza de las ciencias  

**Casals**  

   * "Física y Química 3"; Marta Duñach Masjuan , Armand Gran , Mercè López Alfaro , Maria Dolors Masjuan Buxó , Norberto Pfeiffer León ; 2011, Editorial Casals, ISBN 978-84-218-4367-3
   * "Física y Química 3"; Pano Estudio , Marta Duñach Masjuan , Javier Hernández , Mercè López Alfaro , Maria Dolors Masjuan Buxó , Silvestre Penyarrroja , Norberto Pfeiffer León ; 2007, Editorial Casals, ISBN 978-84-218-3748-1
   * "Física y Química 3, M. Duñach, M.D. Masjuan, E.M. Costafreda, A. Hernández; 2015; Editorial Casals; ISBN 9788421854631, Depósito legal B-1091-2015  
*Comentario personal: el primer libro que veo en abril 2015 adaptado a LOMCE, formulación breve en tema 6 y sí adaptada a normas IUPAC 2005*  

**Ecir**  

   * "Física y Química 3 ESO"; Agustín Candel, Juan B. Soler i Juanjo Tent; 2007; Editorial Ecir; ISBN 9788498262247  
   
**Edebé**  
   * "Física y Química 3 ESO, Ciencias de la naturaleza"; 2012; Editorial Edebé; ISBN 978-84-683-0697-1 ( [Ficha del libro](http://www.edebe.com/educacion/ficha.asp?id=109483&etapa=13) )
      *  [Unidad de muestra "1. La medida. El método científico"](http://www.edebe.com/educacion/documentos/109483-0-529-109484.pdf)   
   * "Física y Química 3 ESO. Bloque I: Química", "Física y Química 3 ESO. Bloque II: Física"; 2015; Editorial Edebé; ISBN 978-84-683-2112-7 (Obra completa) Depósito legal B-8881-2015  
>Es un libro adaptado a LOMCE, recibido ejemplar de muestra (son dos libros separados con mismo ISBN) en el centro en mayo 2015  
Ficha del libro  [Edebe - Ficha del libro](http://www.edebe.com/educacion/ficha.asp?id=113671&etapa=13&ccaa=16)   
Incluye Programación de aula Comunidad de Madrid, Programación didáctica Comunidad de Madrid,  [Muestra del libro del alumno](http://www.edebe.com/educacion/clicks.asp?CodArt=113671&CodDocu=29888&ruta=/educacion/documentos/113671-0-529-UD01_FQ%203%20ESO_CAS.pdf&flg=N)  (unidad 1: El método científico)  

  
**Edelvives**
   * Proyecto #somoslink asociado LOMCE 2015  
   * "Física y Química 3 ESO", Proyecto #somoslink, 2015; Editorial Edelvives,  
Unidad de muestra "4 Reacciones químicas"  [FISICA_QUIMICA_3_ESO_U04_Reacciones_Quimicas.pdf](http://somoslink.com/pdf/FISICA_QUIMICA_3_ESO_U04_Reacciones_Quimicas.pdf)   
Libro profesor, guía didáctica, Unidad de muestra "4 Reacciones químicas"   
 [3_ESO_fisica_quimica_LP.pdf](http://somoslink.com/pdf/3_ESO_fisica_quimica_LP.pdf) #somoslink con la innovacción http://somoslink.com/pdf/FISICA_QUIMICA_3_ESO_INNOVACION.pdf  
#somoslink con PISA http://somoslink.com/pdf/FISICA_QUIMICA_3_ESO_PISA.pdf  
Recursos refuerzo  
 leyes reacciones  [FQ3_R-04-01.LEYES DE LAS REACCIONES.pdf](http://somoslink.com/RECURSOS/ESO/FIS/FQ3_R-04-01.LEYES%20DE%20LAS%20REACCIONES.pdf)   
vocabulario científico  [FQ3_R-04-02.VOCABULARIO CIENTIFICO.pdf](http://somoslink.com/RECURSOS/ESO/FIS/FQ3_R-04-02.VOCABULARIO%20CIENTIFICO.pdf)   
Recursos ampliación   
Estequiometría   
 [FQ3_A-04-01. ESTEQUIOMETRIA.pdf](http://somoslink.com/RECURSOS/ESO/FIS/FQ3_A-04-01.%20ESTEQUIOMETRIA.pdf) Cálculos con reacciones químicas  [FQ3_A-04-02. CALCULOS CON REACCIONES QUIMICAS.pdf](http://somoslink.com/RECURSOS/ESO/FIS/FQ3_A-04-02.%20CALCULOS%20CON%20REACCIONES%20QUIMICAS.pdf)   
Experiencia  [FQ3_A-04-03. EXPERIENCIA.pdf](http://somoslink.com/RECURSOS/ESO/FIS/FQ3_A-04-03.%20EXPERIENCIA.pdf)   
Actividades de Evaluación  [FQ3_E-04-02. EVALUACION.pdf](http://somoslink.com/RECURSOS/ESO/FIS/FQ3_E-04-02.%20EVALUACION.pdf)   
PROPUESTAS PARA LA INNOVACIÓN EDUCATIVA  
Muestras mayo 2015, indica que en septiembre 2015 habrá recursos específicos por asignatura  
PBL. Impacto ambiental   
 [FQ_PBL_IMPACTO AMBIENTAL.pdf](http://somoslink.com/RECURSOS/ESO/00_INNOVACION/FQ/FQ_PBL_IMPACTO%20AMBIENTAL.pdf) Paletas de Inteligencias Múltiples. Uniones entre átomos   
 [FQ_PIIMM_UNIONES ENTRE ATOMOS.pdf](http://somoslink.com/RECURSOS/ESO/00_INNOVACION/FQ/FQ_PIIMM_UNIONES%20ENTRE%20ATOMOS.pdf) Propuestas de Flipped Classroom. Flippeando el trabajo científico   
 [FQ_FC_FLIPPEANDO EL TRABAJO CIENTIFICO.pdf](http://somoslink.com/RECURSOS/ESO/00_INNOVACION/FQ/FQ_FC_FLIPPEANDO%20EL%20TRABAJO%20CIENTIFICO.pdf) 
**Editex**
   * "Física y Química 3 ESO"; Juan Luis Antón Bozal, Dulce María Andrés Cabrerizo; Editorial Editex; ISBN 978-84-9708-496-9; Depósito Legal M-15624-2015 ( [Ficha del libro](http://www.editex.es/publicacion/fisica-y-quimica-3%C2%BA-eso-lomce-818.aspx) )  
Unidad de muestra "4 Moléculas y cristales"  [Física y Química 3º ESO (LOMCE) | Digital book | BlinkLearning](https://www.blinklearning.com/coursePlayer/librodigital_html.php?idclase=17239079&idcurso=411939)   
Unidad de muestra "8 La reacción química"  [RecuperarFichero.aspx](http://www.editex.es/RecuperarFichero.aspx?Id=24605)   
Es un libro adaptado a LOMCE, aparece en catálogo recibido en el centro en mayo 2015  

**McGrawHill**
   * "Física y Química 3 ESO";Ángel R. Cardona, Jose Antonio García, Ángel Peña, Antonio Pozas, Antonio José Vasco; Editorial McGrawHill; ISBN 978-84-481-5384-7 ( [Ficha del libro](http://www.mcgraw-hill.es/html/8448153847.html) )  
      *  [Capítulo de muestra "4. La materia. Estados de agregación"](http://www.mcgraw-hill.es/bcv/guide/capitulo/8448153847.pdf) 
   * "ATD FÍSICA Y QUÍMICA. 4 . ESO. FICHAS ATENCIÓN A LA DIVERSIDAD"; José Antonio García Pérez; Editorial McGrawHill; ISBN 844816296X ( [Ficha del libro](http://www.mcgraw-hill.es/html/844816296X.html) )
   * "Física y Química 3º ESO"; Enrique Andrés del Río, Miguel Ángel Yuste Muñoz, Ángel Rodríguez Cardona, Antonio Pozas Margariños; Editorial McGrawHill; ISBN 978-84-481-9579-3; Depósito legal M-9765-2015  

**Oxford**  

   * "Física y Química 3 ESO", Proyecto Ánfora; I. Píñar Gallardo. Cuaderno de laboratorio: I. Píñar Gallardo; Editorial Oxford; 2007, ISBN ( [Ficha del libro](http://www.oupe.es/es/Secundaria/FisicaYQuimica/proyanforanacional/Paginas/proyanfora3fisicayquimica.aspx) )
   * "Física y Química 3 ESO", Proyecto Adarve, [proyadarvenacional.aspx](http://www.oupe.es/es/mas-areas-educacion/secundaria/fisica-y-quimica/proyadarvenacional/Paginas/proyadarvenacional.aspx)  (enlace sep2014)  
   * "Física y Química 3 ESO. Aprueba tus exámenes", Editorial Oxford, Varias ediciones:  
ISBN: 978-84-673-3062-5, Depósito Legal M-12803-2007  
ISBN: 978-84-673-3062-5, Depósito Legal M-40063-2008  
ISBN: 978-84-673-8446-8, Depósito Legal M-10186-2014  
*>>Opinión personal: el propio título del libro ya es poco afortunado, usa aprobar en lugar de aprender y usa exámenes en lugar de evaluar. Se trata de un "cuadernillo" con los huecos para hacer sobre él los ejercicios, por lo que en general no es reutilizable de un curso para otro (si se hace con lápiz es complicado borrar y que no quede rastro). A nivel de contenidos tiene un fallo importante, ya que no menciona en absoluto el concepto de mol, que es obligatorio en 3º LOE. La formulación no está adaptada a normas IUPAC de 2005.*
   * Adaptaciones Curriculares Física y Química - 3º ESO, 2012, ISBN 9788467370805  
 [adapcurricfisicayquimica3.aspx](http://www.oupe.es/es/mas-areas-educacion/secundaria/fisica-y-quimica/adapcurricfisicayquimica/adapcurricfisicayquimica3/Paginas/adapcurricfisicayquimica3.aspx)   
Incluye muestra "Elementos y compuestos"  [interior_adap_fisica3.pdf](http://www.oupe.es/es/mas-areas-educacion/secundaria/fisica-y-quimica/adapcurricfisicayquimica/adapcurricfisicayquimica3/Galeria%20documentos/interior_adap_fisica3.pdf)   
   * "Física y Química 3 ESO. Aprueba tus exámenes. Solucionario", Editorial Oxford, ISBN: 978-84-673-3063-2, descargable desde la web de la editorial  
 [solucionarios.html](http://promo.oupe.es/escuela_verano/solucionarios.html)   
 [SOLUC_APRUEBA_FQ_3_ESO.pdf](http://promo.oupe.es/escuela_verano/pdf/solucionarios/SOLUC_APRUEBA_FQ_3_ESO.pdf) 
   * "Física y Química 3º ESO, Proyecto Inicia Dual", Isabel Piñar Gallardo; Editorial Oxford, ISBN: 9788467386974  
*>Es un libro asociado a LOMCE, ejemplar de muestra que indica "avance de edición, edición de venta disponible a partir de la segunda quincena de junio" recibido en el centro en mayo de 2015, que indicaba como ISBN: 01905052*  
DEMO Física y Química 3º ESO DUAL. Unidad 3 El átomo  
 [curso2.php](https://www.blinklearning.com/coursePlayer/curso2.php?idcurso=393689)   
 [Entrar en la unidad | BlinkLearning](https://www.blinklearning.com/coursePlayer/librodigital_html.php?idclase=16583419&idcurso=393689)   
Índices en pdf de los tres volúmenes disponibles en  [3ºESO.rar](http://online.oupe.es/OXED/ccaa/madrid/indices/ESO/FISICAYQUIMICA/3%C2%BAESO.rar)   
      * "Física y Química ESO 3 La materia y sus cambios", Proyecto Inicia Dual, serie gamma, Isabel Piñar Gallardo; Editorial Oxford, ISBN 97884-673-9831-1, Depósito Legal M-16505-2015, Código de barras en cubierta de libro de alumno para uso exclusivo profesor 8435157422924 recibido en lote de 3 libros con pegatina común que pone ISBN 8435157420395
      * "Física y Química ESO 3 Fuerza y movimiento", Proyecto Inicia Dual, serie gamma, Isabel Piñar Gallardo; Editorial Oxford, Sin ISBN, Código de barras en cubierta de libro de alumno para uso exclusivo profesor 8435157422931 recibido en lote de 3 libros con pegatina común que pone ISBN 8435157420395
      * "Física y Química ESO 3 Electricidad y energía", Proyecto Inicia Dual, serie gamma, Isabel Piñar Gallardo; Editorial Oxford, ISBN 97884-673-9833-5, Depósito Legal M-16507-2015, Código de barras en cubierta de libro de alumno para profesor 8435157422948 recibido en lote de 3 libros con pegatina común que pone ISBN 8435157420395
  
**Santillana**  

   * "Física y Química 3 ESO", Proyecto Los caminos del saber, Editorial Santillana; ISBN 978 84 294 3027 1 ( [Ficha del libro](http://www.santillana.es/catalogo/obra-completa-fisica-y-quimica-3-eso/) )  
   * "Física y Química 3 ESO. Avanza" Adaptación Curricular, ISBN 9788468003801 ( [Ficha del libro](http://www.santillana.es/catalogo/adaptacion-curricular-fisica-y-quimica-3-eso/) )
   *  [Avanza; adaptaciones curriculares física y química 3 ESO, catálogo](http://www.santillana.es/content/attachment/Avanza-fisica-quimica.pdf) 
   * "Física y Química 3 ESO", Proyecto Saber Hacer, Serie Investiga; Mª Carmen Vidal Fernández, David Sánchez Gómez, José Luis de Luis García, Editorial Santillana, ISBN 978-84-680-1742-6, Depósito Legal: M-18636-2015  
*Es un libro adaptado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015, todavía sin ISBN (con EAN 8431300968110) e indica "Los contenidos podrán ser modificados en función del currículo oficial de la Comunidad Autónoma". En septiembre se recibe ya con ISBN*  
   * "Competencias para el siglo XXI. Física y Química 3 ESO. Biblioteca del profesorado", Proyecto Saber Hacer; Editorial Santillana*, 2015*  
*Es un libro asociado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015, sin ISBN (con **, EAN 8431300987883 (939113)**)*
   * "Día a día en el aula. Recursos didácticos. Física y Química 3 ESO. Biblioteca del profesorado", Proyecto Saber Hacer; Editorial Santillana*, 2015*  
*Es un libro asociado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015, sin ISBN (con **, EAN 8431300984936 (939120)**)*
**SM**
   * "Física y Química 3 ESO"; Julio Puente Azcutia, Mariano Remacha, Jesús Ángel Viguera Llorente, Mariano Remacha Lafuente; Editorial SM; ISBN 978-84-675-0792-8, Depósito legal: M-51900-2006  
   * "Física y Química 3 ESO. Proyecto Conecta 2.0", Julio Puente Azcutia, Jesus Ángel Viguera Llorente, Mariano Remacha Lafuente; Editorial SM; ISBN 978-84-675-3996-7; Depósito legal M-587-2011 ( [Recursos y ficha técnica](http://www.smconectados.com/SMC/fichaLibro.aspx?guid=3B31E250-9032-4B6C-A894-1C557990B61E) )  
      * LIR Alumno "Física y Química 3 ESO. Proyecto Conecta 2.0" ( [Muestra: unidad 3 "Mezclas, disoluciones y sustancias puras", páginas 44 a 63](http://previewlibros.grupo-sm.com/F8887BAD-1C86-4E6A-8EA8-D772EBC43108.html) )
   * "Física y Química 3º ESO, SAVIA-15", Fernando I. de Prada, Ana Cañas, Aureli Caamaño; Editorial SM, ISBN 9788467576375; Depósito legal M-02.982-2015  
*Es un libro adaptado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015*  

**Vicens Vives**  

   * "FQ3 Física y Química " proyecto Aula3D, Á. Fontanet Rodríguez, Mª J. Martínez de Murguía Larrechi; Editorial Vicens Vives , ISBN 978-84-682-3046-7, Depósito Legal B. 6111-2015

#### 4º ESO
**Anaya**  

   * "Física y Química 4"; Sabino Zubiaurre Cortés, Jesús María Arsuaga Ferreras, Ana María Morales Cas, Antonio Pérez Sanz; Editorial Anaya, ISBN 978-84-678-0255-9  
   * "Física y Química 4", Salvador Balibrea López, Manuel Reyes Camacho, Antonio Álvarez Alcántara, Alfredo Sáez Fernández; Editorial Anaya, ISBN 978-84-667-7116-0; Depósito Legal: NA-710-2008
   * “Física y Química 4. Recursos fotocopiables", Salvador Balibrea López, Manuel Reyes Camacho, Antonio Álvarez Alcántara, Alfredo Sáez Fernández; Editorial Anaya; ISBN 978-84-667-7117-7; Depósito Legal: M-32138-2008  
   * Ciencias Aplicadas a la actividad profesional, 978-84-698-1153-5

**Bruño**  
   * "ContextoDigital Física y Química 4 ESO", Editorial Bruño, ISBN 978-84-216-7068-2  
   * "ContextoDigital Física y Química 4 ESO - 3 volúmenes"; Editorial Bruño; ISBN 978-84-216-7135-1
   * "Física y Química 4 ESO"; Editorial Bruño, 2007, ISBN 978-84-216-5911-3  
   * "Cuadernos ESO Física y Química 4"; Editorial Bruño; ISBN 978-84-216-6482-7  
   * "Objetivo aprobar LOE: Física y Química 4 ESO"; Editorial Bruño; ISBN 978-84-216-6014-0  
*Sigue apareciendo con "LOE" en catálogo recibido en el centro en mayo 2015 donde aparecen ya libros LOMCE*  

**Casals**  

   * "Física y Química 4"; Marta Duñach Masjuan , Mercè López Alfaro , Maria Dolors Masjuan Buxó , Norberto Pfeiffer León; 2012, Editorial Casals, ISBN 978-84-218-4800-5  
   * "Física y Química 4"; Pano Estudio , Amadeu Blasco , Marta Duñach Masjuan , Pedro Espinosa Saenz , Mercè López Alfaro , Maria Dolors Masjuan Buxó , Norberto Pfeiffer León ; 2008, Editorial Casals, ISBN 978-84-218-3964-5
   * "Física y Química 4"; Marta Duñach Masjuan ,M. Duñach, E.M.Costafreda; Editorial Casals; ISBN 978-84-218-6090-8; Depósito legal B-8908-2016, Versión digital 978-84-218-5095-4  

**Edebé**
   * "Física y Química 4 ESO, Ciencias de la naturaleza"; 2012; Editorial Edebé; ISBN: 978-84-683-0550-9 ( [Ficha del libro](http://www.edebe.com/educacion/ficha.asp?id=830550&etapa=13) )
      *  [Unidad de muestra "1 Movimiento"](http://www.edebe.com/educacion/documentos/830550-0-529-eso_fq_4_cas_ud1.pdf) 
   * "Física y Química 4 ESO", ISBN: 9788468317205 (Obra completa, dos libros bloque I:Química y Bloque II:Física ), Depósito legal: B-9147-2016
   * "Física y Química 4 ESO. Programación y orientaciones didácticas", ISBN 9788468319407, Depósito legal: B-14927-2016  
   * 2021 9788468351995 [Edebé](https://econtent.edebe.com/eso2021/9788468351995_fyq4_cas/)   
   
**Edelvives**  
   * "Física y Química ESO 4"; Editorial Edelvives; ISBN 978-84-140-0380-4; Depósito legal Z 281-2016  
   
**Editex**
   * "Física y Química 4º ESO"; Editorial Editex; 2016; ISBN 978-84-9078-755-7
   * Ciencias aplicadas a la actividad profesional, 978-84-907-8762-5  

**McGrawHill**
   * "Física y Química 4º ESO"; Enrique Andrés del Río, Francisco Larrondo Almeda, Francisco Martínez Salmerón, Sergi Bolea Escrich; Editorial McGrawHill; ISBN 9788448608767
   * "Física y Química 4 ESO"; José Antonio García Pérez; Editorial McGrawHill; ISBN: 8448162951 ( [Ficha del libro](http://www.mcgraw-hill.es/html/8448162951.html) )  
      *  [Capítulo de muestra "2 Fuerzas"](http://www.mcgraw-hill.es/bcv/guide/capitulo/8448162951.pdf) 

**Oxford**  

   * "Física y Química 4 ESO", Proyecto Ánfora; I. Píñar Gallardo. Cuaderno de laboratorio: I. Píñar Gallardo; 2007; Editorial Oxford; ISBN 978-84-673-3859-1; Depósito legal NA-268-2008 ( [Ficha del libro](http://www.oupe.es/es/Secundaria/FisicaYQuimica/proyanforanacional/Paginas/proyanfora4fisicayquimica.aspx) )
   * Adaptaciones Curriculares Física y Química - 4º ESO, 2012, ISBN 9788467370843  
 [adapcurricfyq4.aspx](http://www.oupe.es/es/mas-areas-educacion/secundaria/fisica-y-quimica/adapcurricfisicayquimica/adapcurricfyq4/Paginas/adapcurricfyq4.aspx) Incluye muestra "Energía y las ondas"  [interior_adap_fisica4.pdf](http://www.oupe.es/es/mas-areas-educacion/secundaria/fisica-y-quimica/adapcurricfisicayquimica/adapcurricfyq4/Galeria%20documentos/interior_adap_fisica4.pdf) 
   * "Física y Química 4 ESO. Volumen: Física", Inicia dual, ISBN 9780190502539, Depósito legal M-10200-2016
   * "Física y Química 4 ESO. Volumen: Química", Inicia dual, ISBN 9780190502522, Depósito legal M-9719-2016
**Santillana**
   * "Física y Química 4 ESO", Proyecto Los caminos del saber; Editorial Santillana; ISBN 978 84 680 0033 6 (son 3 volúmenes cada uno con otro ISBN propio)  
*>>Opinión personal: ojeado que en edición de 2011 la formulación inorgánica sí está adaptada a normas IUPAC de 2005.*
   * "Física y Química 4 ESO"; Serie investiga; Editorial Santillana; ISBN 978-84-680-3790-5
   * "Ciencias aplicadas a la actividad profesional"; Serie investiva; Editorial Santillana; ISBN 978-84-680-3793-6; Depósito legal M-12889-2016  

**SM**
   * "Física y Química 4 ESO. Proyecto Conecta 2.0"; Editorial SM; ISBN 978-84-675-5323-9, Depósito legal: M-66-2012  
      * LIR Alumno "Física y Química 4 ESO. Proyecto Conecta 2.0" ( [Muestra: unidad 1 "Estudio del movimiento", páginas 16 a 39](http://previewlibros.grupo-sm.com/62E76B56-B2A6-4EDA-89DE-F78EAB4CA8FC.html) )
   * Plan de refuerzo de física y química, 4º ESO, cuaderno 1, movimientos y fuerzas; Editorial SM; ISBN 9788434871755, Depósito legal: M-17321-02
   * Plan de refuerzo de física y química, 4º ESO, cuaderno 2, fuerzas en sólidos y fluidos; Editorial SM; ISBN 9788434871762, Depósito legal: M-13140-2003
   * "Física y Química 4 ESO"; Ana Cañas, Jesús Ángel Viguera, Aureli Caamaño, Fernando I. de Prada; Editorial SM; ISBN 978-84-675-8698-5; Depósito legal M-03370-2016

### Diversificación Curricular  

**Editex**  
   * "Diversificación I ámbito científico-tecnológico (Edición 2011)"; Filomena González, Mercedes Sánchez, Rubén Solís: ISBN: 9788497719902  
Unidad de ejemplo  [RecuperarFichero.aspx](http://www.editex.es/RecuperarFichero.aspx?Id=20494) 
   * "Diversificación I ámbito científico-tecnológico"; Filomena González, Mercedes Sánchez, Rubén Solís; ISBN: 9788497713955
   * "Diversificación II ámbito científico-tecnológico (Edición 2012)"; Filomena González, Mercedes Sánchez, Rubén Solís; ISBN: 9788490032770  
Unidad de ejemplo  [RecuperarFichero.aspx](http://www.editex.es/RecuperarFichero.aspx?Id=21302)   
   * "Diversificación II ámbito científico-tecnológico"; Filomena González, Mercedes Sánchez, Rubén Solís; ISBN: 9788497713979

### Bachillerato

#### 1º Bachillerato
**Anaya**
   * "Física y Química 1 Bachillerato"; S. Zubiaurre, J.M. Arsuaga, J. Moreno, B. Garzón; Editorial Anaya, ISBN 978-84-667-7307-2
   * "Física y Química. CD-ROM de Recursos didácticos. CD-ROM de Evaluación." ; Garzón Sánchez, Benito / Arsuaga Ferreras, Jesús Maria / Zubiaurre Cortés, Sabino: Editorial Anaya Educación;  
ISBN: 9788466773096
   * "Física y Química. Orientaciones y recursos didácticos."; Moreno Sánchez, José / Arsuaga Ferreras, Jesús Maria / Zubiaurre Cortés, Sabino; Editorial Anaya Educación; ISBN: 9788466773089
   * "Física y Química. Solucionario."; Editorial Anaya Educación; ISBN: 9788466773102
   * "Física y Química 1º Bachillerato", Editorial Anaya, 2015.  
Código para descargar libro válido hasta 15/07/2015 CGVF-NFDF-MJS9-PDE3  
Unidades de muestra: 0 La investigacion científica y 1 Naturaleza de la materia (DEMO)  [Física y Química 1. Bachillerato. Anaya + Digital | Libro digital | BlinkLearning](https://www.blinklearning.com/coursePlayer/curso2.php?idcurso=400809)   
Índice del libro  [Física y Química 1. Bachillerato. Anaya + Digital | Libro digital | BlinkLearning](https://www.blinklearning.com/coursePlayer/librodigital_html.php?idclase=16592399&idcurso=400809)   

**Bruño**
   * "Física y Química 1 Bachillerato"; Miquel Sauret Hernández, Jacinto Soriano Minnocci; Editorial Bruño; ISBN 978-84-216-5979-3, Depósito legal: M-4862-2008
   * "Física y Química 1 Bachillerato. Solucionario"; Miquel Sauret Hernández, Jacinto Soriano Minnocci; ISBN 978-84-216-6202-1 Depósito legal: M-24907-2008
   * "Código Bruñño Física y Química 1 Bachillerato"; Miguel Sauret Hernández, Jacinto Soriano Minnocci; Editorial Bruño, ISBN 978-84-696-0935-4; Depósito legal: M-5902-2015  
*Es un libro adaptado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015*
   * "Código Bruño Física y Química 1 Bachillerato digital alumno"; Editorial Bruño, ISBN 978-84-696-1024-4  
Es un libro adaptado a LOMCE, aparece en catálogo recibido en el centro en mayo 2015  
   * "Código Bruño Física y Química 1 Bachillerato digital profesor"; Editorial Bruño, ISBN 978-84-696-1025-1  
Es un libro adaptado a LOMCE, aparece en catálogo recibido en el centro en mayo 2015  

**Casals**
   * "Física y Química 1 Bachillerato"; J.M.Dou, M.D.Masjuan, N.Pfeiffer, A.Travesset; Editorial Casals, ISBN 84-218-2155-5; Depósito legal B-28.799-99
**Edebé**
   * "Física y Química 1 Bachillerato"; Editorial Edebé; ISBN 978-84-236-8590-5; Depósito legal B.988-2010 ( [Ficha del libro](http://www.edebe.com/educacion/ficha.asp?id=8590&etapa=14) )  
Actualización formulación 2012 descargable en pdf  [8590-16-34-112034_Adenda_LA_FQ_1_BCH_CAS.pdf](http://www.edebe.es/educacion/documentos/8590-16-34-112034_Adenda_LA_FQ_1_BCH_CAS.pdf) 
   * "Física y Química 1 Bachillerato"; 2015; Editorial Edebé; ISBN 978-84-683-2059-5 Depósito legal B-6400-2015  
Es un libro adaptado a LOMCE, recibido ejemplar de muestra en el centro en mayo 2015  
Ficha del libro  [Edebe - Ficha del libro](http://www.edebe.com/educacion/ficha.asp?id=112872&etapa=14&ccaa=16)   
Incluye Programación de aula,  [Muestra del libro del alumno](http://www.edebe.com/educacion/clicks.asp?CodArt=112872&CodDocu=29913&ruta=/educacion/documentos/112872-0-529-112872_UD_03_FQ%201_LA_BACH_CAS.pdf&flg=N)  (unidad 3: Reacciones químicas)
   * "Física y Química 4 ESO. Libro del alumno"; Editorial Edbé; ISBN 978-84-683-1720-5 (Obra completa); Depósito legal: B.9147-2016  
  
**Editex**
   * "Física y Química 1 Bachillerato"; Juan Luis Antón Bozal, Dulce María Andrés Cabrerizo, 2015; Editorial Editex; ISBN 978-84-9708-508-9 ( [Ficha del libro](http://www.editex.es/publicacion/fisica-y-quimica-1%C2%BA-bachillerato-lomce-826.aspx) )  
Unidad de muestra "1 La actividad científica"  [Libro Digital | BlinkLearning](https://www.blinklearning.com/coursePlayer/librodigital_html.php?idclase=17201469&idcurso=411069)   
Unidad de muestra "12 Leyes de la dinámica"  [RecuperarFichero.aspx](http://www.editex.es/RecuperarFichero.aspx?Id=24621)   
>Es un libro adaptado a LOMCE, aparece en catálogo recibido en el centro en mayo 2015
   * "Cultura Científica 1 Bachillerato"; Dulce María Andrés Cabrerizo, 2015; Editorial Editex; ISBN 978-84-9708-507-2 ( [Ficha del libro](http://www.editex.es/publicacion/cultura-cientifica-1%C2%BA-bachillerato-lomce-828.aspx) )  
Unidad de muestra "1 Los pilares de la ciencia"  [Libro Digital | BlinkLearning](https://www.blinklearning.com/coursePlayer/librodigital_html.php?idclase=17129149&idcurso=409369)   
Unidad de muestra "1 Los pilares de la ciencia"  [RecuperarFichero.aspx](http://www.editex.es/RecuperarFichero.aspx?Id=24623)   
Es un libro adaptado a LOMCE, aparece en catálogo recibido en el centro en mayo 2015
**Edelvives**
   * "Física y Química 1 Bachillerato", José Vicente Morales Ruiz, Carlos José Arribas Puras, José Antonio Sánchez Manzanares; Editorial Edelvives; ISBN 84-263-3658-2; Depósito legal Z.962-97
**McGrawHill**
   * "Física y Química 1 Bachillerato"; POZAS.ANTONIO.GCIA.JOSE ANTONIO; Editorial McGrawHill, ISBN 8448180488, EAN 9788448180485 ( [Ficha del libro](http://www.mcgraw-hill.es/html/8448180488.html) )
      *  [Capítulo de muestra "2 Estructura atómica"](http://www.mcgraw-hill.es/bcv/guide/capitulo/8448180488.pdf) 

**Oxford**  
DEMO Física y Química 1º Bachillerato DUAL. Unidad 4 Estructura Atómica y Molecular  
 [Física y Química 1.º Bachillerato DUAL | Digital book | BlinkLearning](https://www.blinklearning.com/coursePlayer/curso2.php?idcurso=392689)   
 [Física y Química 1.º Bachillerato DUAL | Digital book | BlinkLearning](https://www.blinklearning.com/coursePlayer/librodigital_html.php?idclase=15901889&idcurso=392689) 
   * "Física y Química Bachillerato", Proyecto Inicia Dual, Mario Ballestero Jadraque, Jorge Barrio Gómez de Agüero; Editorial Oxford, ISBN 97884-673-9384-2, Depósito Legal M-16514-2015, Código de barras en cubierta de libro de alumno para profesor 8435157420579  
  
**Santillana**  

   * "Física y Química 1 Bachillerato", Proyecto Saber Hacer, Serie Investiga; Francisco Barradas Solas, Pedro Valera Arroyo, Mª del Carmen Vidal Fernández; Editorial Santillana; ISBN 978-84-680-1328-2 Depósito legal M-10528-2015  
*Es un libro adaptado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015*
   * "Competencias para el siglo XXI. Física y Química 1 Bachillerato. Biblioteca del profesorado", Proyecto Saber Hacer; Editorial Santillana*, 2015*  
*Es un libro asociado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015, sin ISBN (con **, EAN 8431300981997 (938784)**)*
   * "Día a día en el aula. Recursos didácticos. Física y Química 1 Bachillerato. Biblioteca del profesorado", Proyecto Saber Hacer; Editorial Santillana*, 2015*  
*Es un libro asociado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015, sin ISBN (con **, EAN 8431300986169 (939121)**)*
   * "Cultura científica 1 Bachillerato", Proyecto Saber Hacer, Serie Explora ; Francisco Anguita Virella, Mariano Carrión Vázquez, José Manuel Cerezo Gallego, Luis Fernández García, Ana Isabel Henche Ruiz, Antonio José Hidalgo Moreno, David Sánchez Gómez; Editorial Santillana; ISBN 978-84-680-1186-8 Depósito legal M-11654-2015  
*Es un libro adaptado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015*
   * "Día a día en el aula. Recursos didácticos. Cultura científica 1 Bachillerato. Biblioteca del profesorado", Proyecto Saber Hacer; Editorial Santillana*, 2015*  
*Es un libro asociado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015, sin ISBN (**EAN 8431300975866 (939817)**)*  
En 2016 visto ejemplar con ISBN 978-84-680-3105-7, Depósito legal M-25427-2015
   * *"Ciiencias para el mundo contemporáneo", ISBN 978-84-294-4366-0, Depósito legal: B-35.251-2008*  
  
**SM**
   * "Física y Química 1 Bachillerato", José Ignacio del Barrio Barrero, Montserrat Agustench Masdeu, Julio Puente Azcutia, Aureli Caamaño Ros; Editorial SM, ISBN 978-84-675-2516-8 ( [Ficha del libro](http://www.smconectados.com/SMC/fichaLibro.aspx?guid=67A6176A-9E0C-448C-AA82-530091DC1E32) )
   * "Física y Química 1º Bachillerato, SAVIA-15", Pablo Nacenta, Fernando I. de Prada, Julio Puente; Editorial SM, ISBN 978-84-675-7651-1; Depósito legal M-05.087-2015  
 Es un libro adaptado a LOMCE, ejemplar de muestra recibido en el centro en mayo 2015  

**Vicens Vives**  

   * Física y Química, Á. Fontanet Rodríguez, Mª J. Martínez de Murguía Larrechi; Editorial Vicens Vives , ISBN 978-84-316-8921-6, Depósito Legal: B: 33.964-2008  
   * "FQB Física y Química" proyecto Aula3D, Á. Fontanet Rodríguez, Mª J. Martínez de Murguía Larrechi; Editorial Vicens Vives , ISBN 978-84-682-3054-2, Depósito Legal B. 6114-2015  

#### 2º Bachillerato Física
Libros para física 2º Bachillerato  
 [Física Tomo 1 2010-2011 | Vector Euclidiano | Óptica](http://es.scribd.com/doc/37145063/FISICA-TOMO-1-2010-2011)   
 [Física Tomo 2 2010-2011 | Interacción fundamental | Gravedad](http://es.scribd.com/doc/37145235/FISICA-TOMO-2-2010-2011)   
Según currículo País Vasco. Valentín Laconcha Abecia. Doctor en ciencias físicas. Licenciamiento no detallado.  
  
De editoriales:  
  
**Anaya**  

   * Física 2; S. Zubiaurre, J. M. Arsuaga, J. Moreno, F. Gálvez; Editorial Anaya; ISBN 978-84-667-8263-0; Depósito Legal B.5768-2009
   * Física 2; J. F. Dalmau, M. Pérez, J. Satoca, F.Tejerina; Editorial Anaya; ISBN 978-84-667-2178-9; Depósito Legal BI-183-2003  

**Bruño**  

   * "Física 2, Ciencias de la Naturaleza y de la Salud, Tecnología"; Editorial Bruño; ISBN 84-216-3139-X, Depósito Legal: M-8674-2002  
   * "Física Bachillerato"; Editorial Bruño; ISBN 978-84-216-6449-0
   * "Física Bachillerato Solucionario"; Editorial Bruño; ISBN 978-84-216-6451-3
   * "Física 2"; Editorial Bruño; ISBN 978-84-696-1161-6; Depósito Legal: M-6271-2016  

**Edebé**  

   * "Física 2"; 2009; Editorial Edebé; ISBN 978-84-236-9283-5, Depósto Legal B.9884-2010 ( [Ficha del libro](http://www.edebe.com/educacion/ficha.asp?id=9283&etapa=14) )
   * "Física 2. Orientaciones y solucionario", ISBN 9788423695232 (no indica depósito legal, libro impreso de 208 páginas)  

**Edelvives**  
   * "Física y Química Bachillerato 2"; Editorial Edelvives; ISBN 978-84-140-0342-8 (obra completa teoría + práctica) ; Depósito legal Z 264 2016
   * "Física y Química Bachillerato 2. Teoría"; Editorial Edelvives; ISBN 978-84-140-0367-1; Depósito legal Z 264 2016
   * "Física y Química Bachillerato 2. Teoría"; Editorial Edelvives; ISBN 978-84-140-0368-8; Depósito legal Z 264 2016  

**Editex**  

   * Física 2º Bachillerato; Dulce Mª Andrés Cabrerizo, J.Luis Antón Bozal, Javier Barrio Pérez; Editorial Editex; ISBN 84-9771-064-9; Depósito legal M-8933-2003
   * Física 2º Bachillerato; Juan Luis Antón, Javier Barrio, Dulce Mª Andrés; ISBN: 9788497715256, Depósito Legal: M-10513-2009 ( [Ficha del libro](http://www.editex.es/publicacion/fisica-2%C2%BA-bachillerato-523.aspx) )  
Unidad de ejemplo:  [RecuperarFichero.aspx](http://www.editex.es/RecuperarFichero.aspx?Id=18588)   

**McGrawHill**
   * "Física Bachillerato 2"; Ángel Peña Sainz, José Antonio García Pérez; Editorial Mc Graw Hill; ISBN 844817027X, EAN 978-84-481-7027-1
      *  [Mapa del CD, contiene tests y resúmenes de todas las unidades del libro](http://www.mhe.es/bachillerato/fisica_quimica/844817027X/archivos/media/esp/mapa_cd.html) 
      *  [Capítulo de muestra "04 Fuerzas centrales. Comprobación de la segunda ley de Kepler"](http://www.mcgraw-hill.es/bcv/guide/capitulo/844817027X.pdf) 
   * "Física 2º Bachillerato"; Ángel Peña Sainz, José Antonio García Pérez; Editorial Mc Graw Hill; ISBN 978-84-486-0992-4  
   
**Oxford**  
   * "Física y Química 2 Bachillerato", Inicia dual, ISBN 9780190502584, Depósito legal M-13514-2016  
   
**SM**
   * Física 2; Julio Puente Azcutia, Cristóbal Lara López, Nicolás Romo Baldominos; Editorial SM; ISBN 8434891611, Depósito legal M-3489-2003  
   *  [Física 2, Proyecto conectados; Nicolás Romo Baldominos, Julio Puente Azcutia, Juan de Dios Alonso Polvorosa, Máximo Pérez Viana; Editorial SM; ISBN 97884-675-3468-9 Depósito Legal: M-9141-2009](http://www.smconectados.com/SMC/fichaLibro.aspx?guid=F9CC63CC-8C76-45A7-A471-08212C34E992) 
   * "Física 2 Bachillerato"; Pablo Nacenta, Nicolás Romo, José Luis Trueba, Julio Puente; Editorial SM; ISBN 978-84-675-8721-0; Depósito legal M-03716-2016

#### 2º Bachillerato Química
**Anaya**  

   * Química 2. Editorial Anaya; ISBN 978-84-667-2181-3; Depósito Legal: M-4.534-2003
   * Química 2; Editorial Anaya, ISBN 978-84-667-8267-8  

**Bruño**
   * "Química 2 Bachillerato"; Miquel Sauret Hernández; Editorial Bruño; ISBN 978-84-216-6466-7
   * "Química 2 Bachillerato solucionario"; Editorial Bruño; ISBN 978-84-216-6468-1
   * "Química2"; Editorial Bruño; ISBN 978-84-696-1163-0; Depósito Legal: M-6272-2016  
   
**Edebé**
   * "Química 2 Bachillerato"; Guadiel-grupo Edebé, Editorial Edebé; ISBN 978-84-8379-180-6
   * "Química 2 Bachillerato"; Editorial Edebé, 2009, ISBN 978-84-236-9282-8 ( [Ficha del libro](http://www.edebe.com/educacion/ficha.asp?id=9282&etapa=14) )   
   * "Química 2. Orientaciones y solucionario", ISBN 9788423694525 (no indica depósito legal, libro impreso de 175 páginas)
   * "Química 2. Libro del alumno", Editorial Edebé, ISBN 978-84-683-1723-6 (no indica depósito legal, libro impreso parcial 2016  

**Edelvives**  

   * "Física y Química Bachillerato 2"; Editorial Edelvives; ISBN 978-84-140-0340-4 (obra completa teoría + práctica) ; Depósito legal Z 376 2015
   * "Física y Química Bachillerato 2. Teoría"; Editorial Edelvives; ISBN 978-84-140-0365-7; Depósito legal Z 376 2015
   * "Física y Química Bachillerato 2. Teoría"; Editorial Edelvives; ISBN 978-84-140-0366-4; Depósito legal Z 376 2015  

**McGrawHill**
   * "Química 2 Bachillerato"; Cardona; Editorial McGrawHill, ISBN 844816962X, EAN 9788448169626 ( [Ficha del libro](http://www.mcgraw-hill.es/html/844816962X.html) )
      *  [Mapa del CD, contiene animaciones, esquemas, resúmenes de todas las unidades del libro](http://www.mhe.es/bachillerato/fisica_quimica/844816962X/archivos/media/esp/mapa_cd.html)   
      *  [Capítulo de muestra "05 Equilibrio químico"](http://www.mcgraw-hill.es/bcv/guide/capitulo/844816962X.pdf) 
   * "Química 2 Bachillerato. Solucionario";Antonio Pozas Magariños, Rafael Martín Sánchez, Ángel Rodríguez Cardona, y Antonio Ruiz Sáenz de Miera; ISBN 978-84-481-6964-0
   * "Química 2 Bachillerato", Ángel Rodríguez Cardona, 2008, ISBN-10: 8448157133, ISBN: 978-8448157135  
      * Capítulo de muestra "05 Equilibrio químico" [8448157133.pdf](http://www.mcgraw-hill.es/bcv/guide/capitulo/8448157133.pdf) 
   * "Química 2º Bachillerato"; Ángel Peña Sainz, José Antonio García Pérez; Editorial Mc Graw Hill; ISBN 978-84-486-0957-3  
   
**Santillana**  

   * "Química 2", ISBN 978-84-680-2677-0, Depósito legal M-7526-2016  

**SM**
   * "Química 2", Proyecto Conectados; José Ignacio del Barrio Barrero, Alicia Sánchez Soberón, Aureli Caamaño Ros, Ana Isabel Bárcena Martín; Editorial SM ( [Ficha del libro](http://www.smconectados.com/SMC/fichaLibro.aspx?guid=8BD06FB3-D3A9-4C95-A8F6-232240512D52) )  

### BUP

#### 2º BUP

   * "Física y Química Positrón", Alejandro L. Lasheras, Mª Pilar Carretero, Editorial Vicens-Vives, ISBN 84-316-1132-4, Depósito legal B.1.380-1986

#### 3º BUP

   * "Física y Química Tercer curso de Bachillerato", Agustín Pérez Botella, Editorial Marfil, ISBN 84-268-0265-6, Depósito legal V.2.716-1977

### COU

   * Física COU, Ediciones SM, ISBN 84-348-1043-3, Depósito legal M-899-85
   * Curso de Física COU; A. Peña Sainz / F. Garzo Pérez; McGraw-Hill, 1990; ISBN: 84-7615-519-0

### FP1

   * "Física y química 2", M.C. Gómez Medina, M.L.Medina Lloret, A.G.Vítores González, Editorial SM, ISBN 84-348-2674-7. Depósito Legal M 14557-1990

### FP Básica
**Altamar**  

   * Ciencias Aplicadas I. Ciencias de la Nauraleza, Editorial Altamar, ISBN 978-84-15309-86-4, Depósito Legal B-12465-2014
   * Ciencias Aplicadas I. Matemáticas, Editorial Altamar, ISBN 978-84-15309-85-7, Depósito Legal B-12464-2014  
   
**Anaya**
   * Ciencias Aplicadas I, Editorial Anaya, ISBN 978-84-678-3357-7; Depósito Legal: M-11455-2014  
   
**Donostiarra**  

   * Ciencias Aplicadas I, Editorial Donostiarra, ISBN 978-84-7063-485-7; Depósito Legal: SS-488-2014
  
**Editex**  

   * Ciencias Aplicadas I, Editorial Editex, ISBN 978-84-9003-362-3, Depósito Legal: M-14927-2014
   * Ciencias Aplicadas II, Editorial Editex, ISBN 978-84-9078-518-8
   * Cuaderno Matemáticas I, Editorial Editex, ISBN 978-84-9078-589-8, Depósito Legal: M-7070-2015  
   
**Marcombo**
   * "Ciencias Aplicadas I, Matemáticas I"; Elisa Núñéz, Elisa Rufino; Editorial Marcombo; ISBN 9788426721716; Depósito Legal: B-18631-2014
   * "Ciencias Aplicadas I, Matemáticas I. Libro del profesor"; Elisa Núñéz, Elisa Rufino; Editorial Marcombo; ISBN 9788426721655; Depósito Legal: B-18633-2014
  
**MCMILLAN**  

   * Ciencias Aplicadas 1. Ciencias de la Nauraleza, Editorial MACMILLAN Profesional, ISBN 978-84-15991-74-8, Depósito Legal M-15313-2014
   * Ciencias Aplicadas 1. Matemáticas, Editorial MACMILLAN Profesional, ISBN 978-84-15991-73-1, Depósito Legal M-13967-2014  
   
**Santillana**
   * Módulo de Ciencias Aplicadas I. Ciencias; Editorial Santillana; ISBN: 978-84-680-1856-0; Depósito Legal M-8874-2014
   * Módulo de Ciencias Aplicadas I. Matemáticas; Editorial Santillana; ISBN: 978-84-294-6490-0; Depósito Legal M-8873-2014

### PCPI
**Editex**
   * Matemáticas y ciencias. Cuaderno de Trabajo; ISBN 978-84-9771-301-6; Depósito Legal: M-15709-2011

