---
aliases:
  - /home/materias/eso/ambito-cientifico-matematico---pmar/
---

# Ámbito científico matemático - PMAR (LOMCE)

Se usan las siglas PMAR para hacer referencia al Programa de mejora del aprendizaje y del rendimiento, que es paralelo a 2º y 3º ESO y que surge en la LOMCE, en cierto modo sustituyendo a la Diversificación curricular, que es paralela a 3º y 4º ESO y asociada a LOE.  

## Normativa
Artículo 19 de Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.  
[Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a19)   
donde se cita  
>2.º) Ámbito de carácter científico y matemático, que incluirá al menos las materias troncales Biología y Geología, Física y Química, y Matemáticas.   
