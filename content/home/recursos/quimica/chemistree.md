
# Chemistree

Intento recopilar recursos asociados a "árboles de navidad químicos", con ejemplos.  
También incluyo cosas relacionadas como belenes y otros tipos de adornos navideños o ciencia relacionados con química  

Se pueden ver también calendarios de adviento químicos
[The Chemistry Advent Calendar 2020: Christmas chemistry at home - compoundchem.com](https://www.compoundchem.com/2020/11/30/chemadvent2020/)  
[Christmas Chemistry Advent - compoundchem.com](https://www.compoundchem.com/2017advent/)  
[Chemistry History Advent - compoundchem.com](https://www.compoundchem.com/2017advent/)  
[Periodic Table Advent - compoundchem.com](https://www.compoundchem.com/2017advent/)  

##  Chemistree
Se pueden buscar en Twitter  
[twitter search CHEMISTREE](https://twitter.com/search?f=tweets&q=CHEMISTREE)  
Se pueden [buscar imágenes en Google](https://www.google.com/search?q=chemistree)  

[Is This ‘Chemistree’ Picture Real? - snops.com](https://www.snopes.com/fact-check/chemistree-olive-tree/)  
![](https://www.snopes.com/tachyon/2020/12/Featured-Image-Templates109.png)  

[O chemistree - rsc.org](https://edu.rsc.org/feature/o-chemistree/3009663.article)  
![](https://d1ymz67w5raq8g.cloudfront.net/Pictures/480xAny/6/8/9/139689_IMG_4875-PW.jpg)  

[twitter CavokCanada/status/1070691263186436096](https://twitter.com/CavokCanada/status/1070691263186436096)  
![](https://pbs.twimg.com/media/DtvcBniWwAA8xng.jpg:large)  

[twitter barb_bal/status/1070689287077851136](https://twitter.com/barb_bal/status/1070689287077851136)  
Let me introduce you to the Blight group #chemistree for 2018! #RealTimeChem ⁦@DrBarryChem⁩ @unb ⁦@ChemistryUNB⁩ 🎄🎄  
[![](https://pbs.twimg.com/media/DtvaOtDXQAEaSwm.jpg "") ](https://pbs.twimg.com/media/DtvaOtDXQAEaSwm.jpg)  

[twitter BirleyCC_Sci/status/1070672680997785602](https://twitter.com/BirleyCC_Sci/status/1070672680997785602)  
[![](https://pbs.twimg.com/media/DtvLH-OW4AIxNzc.jpg "") ](https://pbs.twimg.com/media/DtvLH-OW4AIxNzc.jpg)  

[twitter IESGasparSanz/status/811909067555962880](https://twitter.com/IESGasparSanz/status/811909067555962880)  
[![](https://pbs.twimg.com/media/C0R6_N1XcAE7Gdh.jpg "") ](https://pbs.twimg.com/media/C0R6_N1XcAE7Gdh.jpg)  

[twitter chem13news/status/1069935952590766080](https://twitter.com/chem13news/status/1069935952590766080)  
Great #chemistrees in Dec 2018/Jan 2019 issue (in the mail & in process of getting online) -- while you wait, have your students colour-by-number (chemical) #chemistree  

[Colour-by-numbers chemis-tree - uwaterloo.ca](https://uwaterloo.ca/chem13-news-magazine/december-2015-january-2016/activities/colour-numbers-chemis-tree)  
[![](https://uwaterloo.ca/chem13-news-magazine/sites/ca.chem13-news-magazine/files/uploads/images/colour-by-numbers-christmas_0.jpg "") ](https://uwaterloo.ca/chem13-news-magazine/sites/ca.chem13-news-magazine/files/uploads/images/colour-by-numbers-christmas_0.jpg)  

[twitter solsch1560/status/1070201130397958144](https://twitter.com/solsch1560/status/1070201130397958144)  
Our Science department have created their very own 'Chemistree' for Christmas! 🎄 Our pupils have been challenged to name all of the elements and compounds decorating our tree to win a prize! #SolSchSci  
[![](https://pbs.twimg.com/media/DtoeQYgW0AEQM8D.jpg "") ](https://pbs.twimg.com/media/DtoeQYgW0AEQM8D.jpg)  

[twitter 4n6ghosthunter/status/1070476677787123712](https://twitter.com/4n6ghosthunter/status/1070476677787123712) @johndhodonoghue while it may be small our Chem club made tiny #chemistrees to go along with our big silver nitrate #chemistree  
[![](https://pbs.twimg.com/media/DtsY2_KU4AAwz9q.jpg "") ](https://pbs.twimg.com/media/DtsY2_KU4AAwz9q.jpg)  

[twitter WRBdB/status/1071388998717849605](https://twitter.com/WRBdB/status/1071388998717849605)  
chemistree Draw a design with permanent marker on copper sheet. Attach it as the cathode and place in zinc sulfate. Lots of fun with Y11 electrolysis.@johndhodonoghue @chem13news @RealTimeChem #chemed  

Redox Reaction: Holiday ChemisTree! Copper + Silver Nitrate (Holiday Chemistry)  
[![Redox Reaction: Holiday ChemisTree! Copper + Silver Nitrate (Holiday Chemistry)](http://img.youtube.com/vi/yO9sl60XAZo/0.jpg)](http://www.youtube.com/watch?v=yO9sl60XAZo "Redox Reaction: Holiday ChemisTree! Copper + Silver Nitrate (Holiday Chemistry)")
[https://youtu.be/yO9sl60XAZo](https://youtu.be/yO9sl60XAZo)  

[twitter Alexny_85/status/1605490775621480448](https://twitter.com/Alexny_85/status/1605490775621480448)  
¡Hala, como soy químico, os deseo Feliz Navidad con esta chulísima tabla periódica navideña!  
¡Y aprovecho para deciros que es un honor para mi teneros a todos en mi TL!  
Crédito: Science Notes  
![](https://pbs.twimg.com/media/FkfZWRGVsAIHcnj?format=jpg)  

[Chemical Knowledge for New Year’s Eve. Fireworks, sparkler, sparkling wine, champagne, … there is chemistry involved everywhere - chemistryviews.org](https://www.chemistryviews.org/details/news/11207233/Chemical_Knowledge_for_New_Years_Eve/)  


