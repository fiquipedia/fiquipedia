
# 2º Bachillerato Tecnología industrial

Es una materia que desaparece con LOMLOE, existía en LOMCE sin exámenes PAU. En LOGSE y LOE tenía exámenes en PAU (hasta 2016 inclusive)  
En Cataluña sigue teniendo exámenes PAU con LOMCE después de 2016.  
En LOMLOE se sustituye en cierto modo por [Tecnología e Ingeniería II](/home/materias/bachillerato/tecnologia-e-ingenieria-2bachillerato)  

No es una materia afin a Física y Química, pero trata Termodinámica que se incluye en 1º Bachillerato LOMLOE.  
* [Currículo](/home/materias/bachillerato/tecnologia-industrial-2bachillerato/curriculotecnologia-industrial-2bachillerato)  

De momento aquí algunos enlaces a recursos, quizá se separe cuando sea grande.  
En 2022 empiezo a almacenar [enunciados PAU Tecnología Industrial II de 2ºBachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/PAUxComunidades/tecnologiaindustrial)   

[TECNOLOGÍA INDUSTRIAL: APUNTES, PROBLEMAS Y CUESTIONES](https://tecnologiaselectividad.blogspot.com/)  
[Exámenes de selectividad de Tecnología Industrial - academianuevofuturo](https://academianuevofuturo.com/examenes-resueltos/selectividad/tecnologia-industrial/)  
[PAU Tecnología Industrial todas las comunidadades - muchosexamenes.com](https://www.muchosexamenes.com/acceso/pau-pruebas-acceso-universidad-selectividad/?media_category=tecnologia-industrial)  
[Exámenes de EBAU de Tecnología Industrial - examenesdepau.com](https://www.examenesdepau.com/examenes/tecnologia-industrial/)  

[Lección 10. Diagramas de fase - ocw.unican.es](https://ocw.unican.es/pluginfile.php/2225/course/section/2110/leccion_10.pdf)  

[Tema 5.- Diagramas de Equilibrio de Fases - ocw.uc3m.es](https://ocw.uc3m.es/pluginfile.php/1059/mod_page/content/18/diagramas_fase.pdf)  

[DIAGRAMAS DE FASE - udima](https://www.cartagena99.com/recursos/alumnos/apuntes/Manual%20Tema%208%20Diagramas%20de%20fase%20FORMATO2.pdf)  

[PROBLEMAS RESUELTOS - ieshuelin](https://ieshuelin.com/huelinwp/download/Tecnologia/Tecnologia%20industrial/1-T1-Materiales-problemas.pdf)  
