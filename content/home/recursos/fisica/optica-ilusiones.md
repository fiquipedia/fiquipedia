---
aliases:
  - /home/recursos/fisica/recursos-optica-ilusiones/
---

# Recursos ilusiones ópticas

Se pueden citar ideas diversas, ver  [Ilusión óptica - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Ilusi%C3%B3n_%C3%B3ptica) ,  [Pareidolia - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Pareidolia)  
También puede enlazar con trucos de magia  

En algunos casos enlaza con [óptica física](/home/recursos/fisica/recursos-optica-fisica) (espejismos) y con [óptica geométrica](/home/recursos/fisica/optica-geometrica)   

Reservo este hueco para ir citando algunas / ejemplos. Al crecer la página intento agrupar algunos en ciertas categorías  

[12 Mind-Bending Perceptual Illusions - nautil.us](https://nautil.us/12-mind_bending-perceptual-illusions-237228/)  
1. The Power of Top-Down Processing  
2. The Skye Blue Café Wall Illusion  
![](https://assets.nautil.us/15395_0e2e84a82d94dc94d5749d44d4c6c73b.png)  

Citado aquí  
[twitter mrnickharvey/status/1051396885431418880](https://twitter.com/mrnickharvey/status/1051396885431418880) All the horizontal lines are actually parallel and I can’t stop staring at them.  
 
3. Confetti  
![](https://assets.nautil.us/15396_5b46370c9fd40a27ce2b2abc281064de.png)  
4. The Rice Wave Illusion  
![](https://assets.nautil.us/15397_8e452e79cd525671ce2fb6997b73aaf9.png)  
5. The Tilted Road Illusion  
![](https://assets.nautil.us/15398_5edba33a1dcf1876318fb7013a0a8134.png)  
6. Lightness Illusion  
![](https://assets.nautil.us/Stewart6.gif)  
7. The Dynamic Ebbinghaus  
![](https://assets.nautil.us/Stewart7.gif)  
8. The Dynamic Müller-Lyer Illusion  
![](https://assets.nautil.us/Stewart8.gif)  

Ver también [github csaid/Illusion](https://github.com/csaid/Illusion) 
![](https://github.com/csaid/Illusion/raw/master/animation.gif)  

9. The Train Illusion  
![](https://assets.nautil.us/Stewart9.gif)  
10. Rotating Rings  
![](https://assets.nautil.us/Stewart10.gif)  
11. The Spinning Dancer  
![](https://assets.nautil.us/Stewart11.gif)  
12. The Starry Night  
![](https://assets.nautil.us/Stewart12.gif)  


 [twitter cashjim/status/934940647349370880](https://twitter.com/cashjim/status/934940647349370880) I could not believe this illusion I saw today, tweeted out by @pickover... so I coded it myself in @Scratch. This is a fascinating illusion.  [Contrast Speed / Footsteps Illusion - scracth.mit.edu](https://scratch.mit.edu/projects/188838060/)  … Both rectangles are moving at exactly the same speed. #illusion #perception  
    
 [Da igual cuánto fuerces tu cerebro, las líneas azules de esta increíble ilusión óptica no se mueven](https://es.gizmodo.com/da-igual-cuanto-fuerces-tu-cerebro-las-lineas-azules-d-1823730730)   
  
 [twitter AliceProverbio/status/1050672406543486977](https://twitter.com/AliceProverbio/status/1050672406543486977)  
 V5 is firing due to V4 saturation!  
 ![](https://pbs.twimg.com/media/DpS8yFsVsAAxdKV.jpg)  
 
[Akiyoshi's illusion pages](http://www.ritsumei.ac.jp/~akitaoka/index-e.html)  
Akiyoshi KITAOKA, Professor, Psychology, Ritsumeikan University, Osaka, Japan
studying visual perception, visual illusion, optical illusion, trompe l'oeil, 3D, etc

[twitter Rainmaker1973/status/1348593059764850690](https://twitter.com/Rainmaker1973/status/1348593059764850690)  
The ambiguous illusion objects invented by mathematician Kokichi Sugihara now include a Batman logo, printed and sold by 3DNY Design [source, more:  [2yHS4yB](https://buff.ly/2yHS4yB) ]  
  
[twitter ConchiLillo/status/1309004886911131649](https://twitter.com/ConchiLillo/status/1309004886911131649) Buenos días! Una ilusión óptica para el jueves, venga! Ésta es una variante de un clásico. Fíjate bien porque las líneas azules y rojas tienen siempre la misma longitud. Lo único que cambia es la orientación de las puntas de flecha   
  
[twitter ConchiLillo/status/1266266862402146309](https://twitter.com/ConchiLillo/status/1266266862402146309) Buenos días! Hoy toca #ilusiónÓptica👀 Cada grupo de piezas del mismo color se desliza solo en un sentido del movimiento. Primero mira la animación al completo y luego fíjate en el movimiento de un grupo de color   
  
[twitter ConchiLillo/status/1260497290449555457](https://twitter.com/ConchiLillo/status/1260497290449555457) Hoy os traigo un efecto óptico sencillo pero espectacular.Tu cerebro interpreta los contrastes oscureciendo o aclarando automáticamente el cuadro que se mueve con el dedo para entender el fondo en el que está. Ojo❗️Tu cerebro no te engaña,te ayuda a percibir los contrastes. Mola?  
 
[twitter ConchiLillo/status/1219231372914675720](https://twitter.com/ConchiLillo/status/1219231372914675720) Es una ilusión óptica muy conocida. La más famosa es la de la mujer bailando. Al ser una imagen q reconoce tu cerebro, pero plana, intenta "verla" en 3D y dependiendo de qué parte del cuerpo interprete que está más cerca de nosotros, puede "pensar" que va hacia un lado o al otro.  

 [twitter InertialObservr/status/1206066397454852098](https://twitter.com/InertialObservr/status/1206066397454852098) Illusion of The Year 2019 Winner   
  
It's based on a simple Lissajous curve, but uses clever shading/highlighting to create a 'double-axis' illusion  
[twitter ConchiLillo/status/1155039139185725440](https://twitter.com/ConchiLillo/status/1155039139185725440)  
Uso d la ilusión óptica de Munker para "dar color" a fotos en blanco y negro.Son fotos B&W con rayas rojas.Para nuestros conos, tienen rangos de contraste muy alejados. Los colores se interpretan diferente dependiendo del color q lo acompaña.Nuestro cerebro hace el resto.  
[noticia-color-son-bolas-ilusion-optica-impide-veas-todas-color-marron-20190618195556.html](https://www.europapress.es/desconecta/memes/noticia-color-son-bolas-ilusion-optica-impide-veas-todas-color-marron-20190618195556.html)   
        
3D Schröder Staircase  
 [![3D Schröder Staircase - YouTube](https://img.youtube.com/vi/5DYeAkx2IBo/0.jpg)](https://www.youtube.com/watch?v=5DYeAkx2IBo)   
del canal Best Illusion of the Year Contest  
[Best Illusion of the Year Contest - YouTube](https://www.youtube.com/channel/UC3vYLHD7SzqmLHYsY5NX4eg)   

[twitter tuidelescribano/status/1485167864244023298](https://twitter.com/tuidelescribano/status/1485167864244023298)  

[Peripheral Curved Line Illusion](https://michaelbach.de/ot/geom-periphCurving/)  
[148 Optical Illusions & Visual Phenomena by Michael Bach](https://michaelbach.de/ot/index.html)  

[![](https://img.youtube.com/vi/pSXe1ilLMh8/0.jpg)](https://www.youtube.com/watch?v=pSXe1ilLMh8 "Vanishing Liquid Trick (Milk Pitcher)")   
[![](https://img.youtube.com/vi/9DNAljHN610/0.jpg)](https://www.youtube.com/watch?v=9DNAljHN610 "How The Vanishing Liquid Trick Works")   

[twitter Rainmaker1973/status/1594406349928030214](https://twitter.com/Rainmaker1973/status/1594406349928030214)  
This rotating circles tricks you into believing the shaped are 3D, but they're not  
location, Marwell Zoo, Winchester, UK   
source: Jessie Younghusband School  

[![](https://img.youtube.com/vi/HC0GGkNZPgs/0.jpg)](https://www.youtube.com/watch?v=HC0GGkNZPgs "Dual Axis Illusion")   
Best Illusion of the Year Contest - 2019, Frank Force  

[twitter AkiyoshiKitaoka/status/1608769117775101952](https://twitter.com/AkiyoshiKitaoka/status/1608769117775101952)  
![](https://pbs.twimg.com/media/FlN--rhaUAIs6YZ?format=jpg)  

[Stacking Chairs: Local Sense and Global Nonsense](https://journals.sagepub.com/doi/full/10.1177/2041669517752372)  
![](https://journals.sagepub.com/cms/10.1177/2041669517752372/asset/images/large/10.1177_2041669517752372-fig2.jpeg)  

[![](https://img.youtube.com/vi/V8OwDJdmiew/0.jpg)](https://www.youtube.com/shorts/V8OwDJdmiew "Weird Videos - How A Plane Stopped In The Mid-Air? #opticalillusion #parallax")

### Ilusiones asociadas a imágenes secundarias por persistencia retina

[twitter Figensport/status/1663232473759727640](https://twitter.com/Figensport/status/1663232473759727640)  
- If you follow the pink circle you will see all pink.  
- The moving circle will be green if you focus on the +'in the middle.  
- If you focus long on the +'in the middle, the pink circles will disappear and only the rotating green circle will remain.  

[https://x.com/Rainmaker1973/status/1820021062387556496](https://x.com/Rainmaker1973/status/1820021062387556496)  
Stare at the red dot on her nose for 30 seconds, then look at a plain wall while blinking fast.  
It's called afterimage.  
![](https://pbs.twimg.com/media/GUIDgPBWAAAGtBS?format=jpg)  

### Ilusiones asociadas a ver/no ver ciertas cosas en la imagen 

[twitter Rainmaker1973/status/1731359049977933954](https://twitter.com/Rainmaker1973/status/1731359049977933954)  
This is called the Coffer illusion.  
In this image there are 16 circles. Can you find them?  
![](https://pbs.twimg.com/media/GAcFwc3XYAAtys-?format=jpg)  

[twitter Rainmaker1973/status/1747663746573517051](https://twitter.com/Rainmaker1973/status/1747663746573517051)  
This photo is an example of how optical illusions mess with your mind.   
First you see a rock floating in the air and then...  
![](https://pbs.twimg.com/media/GEDyt65WAAAnvTx?format=jpg)  

### Ilusiones asociadas a distorsión curvatura / líneas rectas

[The Remarkable "Curvature Blindness" Illusion](http://blogs.discovermagazine.com/neuroskeptic/2017/12/08/curvature-blindness-illusion/#.WjBhrfZrzQp)   
![](https://images.ctfassets.net/cnu0m8re1exe/HQnYk4k9N0pJzt0kQfeO4/fd22aecd4ae4f3a60796a6a2fe90419d/curvature-blindness-Takahashi-1.png?fm=jpg&fl=progressive&w=660&h=433&fit=pad)  

[twitter AkiyoshiKitaoka/status/1252391109327589380](https://twitter.com/AkiyoshiKitaoka/status/1252391109327589380)  
This image consists of squares aligned vertically or horizontally, but the alignments appear to tilt.  
![](https://pbs.twimg.com/media/EWFi4ZTU4AA1gKn?format=jpg)  

[twitter ConchiLillo/status/1377997198512828417](https://twitter.com/ConchiLillo/status/1377997198512828417)  
Atentos a esta ilusión óptica! Al añadir cruces blancas y grises a las intersecciones de los cuadros, parece que la imagen se distorsiona...Pero en realidad los cuadros siguen siendo los mismos 😏 Fíjate bien!  

[Twitter Rainmaker1973/status/1829139394873205216](https://x.com/Rainmaker1973/status/1829139394873205216)  
Find the curved line.  
This grid tricks you into thinking there's a curved line somewhere, but you can't find it. The purposefully placed gray lines will induce your peripheral vision to interpolate curved lines  
image by Lesha Porche  
[Lesha Porche Cobbles Optical Illusion](https://www.deviantart.com/leshasillustrations/art/Lesha-Porche-Cobbles-Optical-Illusion-901413506)  
![](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/c1358518-2385-47a1-bbfd-5384f7166ce9/dewof42-8bb0fea1-72e7-4794-aecd-1471ec99bbc6.jpg/v1/fill/w_863,h_926,q_70,strp/lesha_porche_cobbles_optical_illusion_by_leshasillustrations_dewof42-pre.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTM3NCIsInBhdGgiOiJcL2ZcL2MxMzU4NTE4LTIzODUtNDdhMS1iYmZkLTUzODRmNzE2NmNlOVwvZGV3b2Y0Mi04YmIwZmVhMS03MmU3LTQ3OTQtYWVjZC0xNDcxZWM5OWJiYzYuanBnIiwid2lkdGgiOiI8PTEyODAifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.zwsgBfWdVDBj5bUR9Qq57DCavtbwsjfwe5DoG70Yn8I)


### Ilusiones asociadas a perspectiva existente o percibida

[![The Illusion Only Some People Can See - Veritasium](https://img.youtube.com/vi/dBap_Lp-0oc/0.jpg)](https://www.youtube.com/watch?v=dBap_Lp-0oc "The Illusion Only Some People Can See - Veritasium")   

[La ilusión óptica o anamorfosis, nueva tendencia del diseño gráfico](https://www.pixartprinting.es/blog/ilusion-optica-anamorfosis/)  

[Jonty Hurwitz. Esculturas anamórficas - doctorojiplatico](https://www.doctorojiplatico.com/2013/01/jonty-hurwitz-esculturas-anamorficas.html)  
[Jonty Hurwitz](https://jontyhurwitz.com/)  
  
[twitter Rainmaker1973/status/1680485870049042434](https://twitter.com/Rainmaker1973/status/1680485870049042434)  
Anamorphosis is a distorted projection or perspective requiring the viewer to occupy a specific vantage point  

[The ‘Secret’ to Anamorphic Illusions](https://medium.com/@tqvinn/the-secret-to-anamorphic-illusions-853e3674209a)  
![](https://miro.medium.com/v2/resize:fit:640/format:webp/1*HAf-jx_uHhqhdx3HU6Qm1w.jpeg)  
![](https://miro.medium.com/v2/resize:fit:720/format:webp/1*JTTBKL0tp5kecZ77CxyYNg.jpeg)  
![](https://miro.medium.com/v2/resize:fit:720/format:webp/1*ya9ksjgkkxuNrFPITSxvIw.jpeg)  

[twitter Rainmaker1973/status/1659931986511933443](https://twitter.com/Rainmaker1973/status/1659931986511933443)  
Defending a castle, the Escher way  
source, Mehdi Alibeygi: https://buff.ly/3IehyFc  
![](https://pbs.twimg.com/media/FwkXc-NXsAADkjc?format=jpg)  

[![](https://img.youtube.com/vi/ZbfMlNvKBOY/0.jpg)](https://www.youtube.com/watch?v=ZbfMlNvKBOY "How to Draw a Cube 3D Trick art on Graph paper")   

[![](https://img.youtube.com/vi/FHJ3CMWnVxY/0.jpg)](https://www.youtube.com/watch?v=FHJ3CMWnVxY "Furniture Optical Illusions - Zach King Magic")   

[twitter Rainmaker1973/status/1734585145082982781](https://twitter.com/Rainmaker1973/status/1734585145082982781)  
The Ambiguous Ring, designed and discussed by Donald E. Simanek in 1996, is one of many impossible objects and is not the same as the Möbius strip, which is a perfectly possible object.   

[Perspective: Objects in Pictures Are Not Always As They Seem | PetaPixel](https://petapixel.com/2012/12/30/perspective-objects-in-pictures-are-not-always-as-they-seem/) Incluye vídeo de 44 s Assumptions  

[twitter Rainmaker1973/status/1835011955100115004](https://x.com/Rainmaker1973/status/1835011955100115004)  
Reverse perspective is a form of drawing where the objects depicted are placed between the projective point and the viewing plane.   
"Reverspective" is an optical illusion created by Patrick Hughes using this principle.

[Reverspective: las ilusiones en 3D de Patrick Hughes](https://www.dw.com/es/reverspective-las-ilusiones-en-3d-de-patrick-hughes/video-56474292)  

### Ilusiones asociadas a color / contraste

[twitter NovickProf/status/1107655643081502720](https://twitter.com/NovickProf/status/1107655643081502720)  
A four-color Munker illusion: Although they appear to be pink, yellow, orange, and nearly white, all four background ovals are the same color (RGB 255,236,196). Original png file is at  [AAAXFo4uWPG6ZEygQ5GY50tma](https://www.dropbox.com/sh/oi9wdd0uc5uc700/AAAXFo4uWPG6ZEygQ5GY50tma?dl=0.)  
![](https://pbs.twimg.com/media/D18u7pZVYAA3eGt.png)  

[This Photo Is Black And White. Here's The Science That Makes Your Brain See Colour - sciencealert.com](https://www.sciencealert.com/crazy-optical-illusion-makes-your-brain-see-colour-in-a-black-and-white-photo)  
![](https://www.sciencealert.com/images/2019-07/processed/015-colour-grid-optical-illusion-1_1024.jpg)  

[twitter Rainmaker1973/status/1379126798466875393](https://twitter.com/Rainmaker1973/status/1379126798466875393)  
The Splitting Color illusion by Mark Vergeer is about how we perceive colors. These identical, flickering colored stripes remain unchanged throughout the clip. However, different surroundings will make these stripes appear completely different [read more:  [Splitting Colors - YouTube](https://buff.ly/3dBt7Wb) ]  
[![Splitting Colors - YouTube](https://img.youtube.com/vi/n_5g9URYGo4/0.jpg)](https://www.youtube.com/watch?v=n_5g9URYGo4)   

[twitter  SteveStuWill/status/1773491918690996654](https://x.com/SteveStuWill/status/1773491918690996654)  
There's no red in this picture. Not one pixel. Don't believe me? Zoom in, my friend; zoom in.  
![](https://pbs.twimg.com/media/GJy1f3fbsAAWMnk?format=jpg)  
[This Coca-Cola optical illusion continues to boggle minds](https://www.creativebloq.com/news/coca-cola-optical-illusion-still-confusing) 
![](https://cdn.mos.cms.futurecdn.net/vHebj6WvUKQqzvKiSAP6NC-1920-80.jpg.webp)  

[twitter Rainmaker1973/status/1407262083666743298](https://twitter.com/Rainmaker1973/status/1407262083666743298)  
Stare at this image for a few seconds and it completely disappears. This effect has been attributed to the adaptation of neurons vital for perceiving stimuli in the visual system 
[Troxler's fading](https://en.wikipedia.org/wiki/Troxler's_fading)  
![](https://pbs.twimg.com/media/E4auy_rXoAcI6S5?format=jpg)

[Optical illusion: the Troxler effect](https://whyevolutionistrue.com/2018/04/10/optical-illustion-the-troxler-effect/)  

Incredible Shade Illusion!   
 [![Incredible Shade Illusion!](https://img.youtube.com/vi/z9Sen1HTu5o/0.jpg)](https://www.youtube.com/watch?v=z9Sen1HTu5o "Incredible Shade Illusion!")   
 
[twitter ConchiLillo/status/1362002342623780864](https://twitter.com/ConchiLillo/status/1362002342623780864) Hola! No los tengo recopilados en #hilos, pero intento recuperar alguno de los tuits y te los pongo por aquí. Gracias por el interés!! Por ejemplo, aquí tienes uno  
[twitter ConchiLillo/status/1345702121484726272](https://twitter.com/ConchiLillo/status/1345702121484726272) ¿Os apetece una ilusión óptica? Mira, aquí tenéis 10 cuadros que parecen de diferente color y brillo...pero en realidad son todos EXACTAMENTE IGUALES. Esta imagen ha sido compartida en Twitter por @AkiyoshiKitaoka  
![](https://pbs.twimg.com/media/EqzirfvW8AArNi0?format=png)  

### Ilusiones asociadas a combinación simultánea de varias imágenes / visión periférica 

[![](https://img.youtube.com/vi/VT9i99D_9gI/0.jpg)](https://www.youtube.com/shorts/VT9i99D_9gI "Shocking illusion - Pretty celebrities turn ugly!- TangenCognitionLab")  

[twitter Rainmaker1973/status/1778017193109688347](https://x.com/Rainmaker1973/status/1778017193109688347)  
The extinction illusion: the left side and the right side have the same number of black dots  
![](https://pbs.twimg.com/media/GKzJNbMWEAEVgbc?format=jpg)  
  
[Illusion of the year. Healing Grid](http://illusionoftheyear.com/2005/08/healing-grid/)  
![](http://illusionoftheyear.com/finalists_2005/Healing_Grid600.jpg)  

[twitter Rainmaker1973/status/1381585392307109891](https://twitter.com/Rainmaker1973/status/1381585392307109891)  
 There are 12 black dots at intersections in this image. Your brain won’t let you see them all at once. Here's why [read more: http://bit.ly/2qxnuj5] [Here&#39;s why you can&#39;t see all twelve black dots in this optical illusion - The Verge](https://www.theverge.com/2016/9/12/12885574/optical-illusion-12-black-dots)   
![](https://pbs.twimg.com/media/Eys_d3QW8AYGXTn?format=jpg)  

[twitter ConchiLillo/status/1358782622449688578](https://twitter.com/ConchiLillo/status/1358782622449688578)  
Imagen que demuestra que lo que ves por tu retina periférica es una ilusión, no es la realidad, lo crea tu cerebro. Si miras el punto central, todas las figuras adquieren inmediatamente la misma forma, pero si las miras (usas retina central) ves que son distintas  
Vía @IFLScience  
![](https://pbs.twimg.com/media/Ettdl_uXAAA9uKX?format=jpg)  

[twitter Rainmaker1973/status/1075722678189867008](https://twitter.com/Rainmaker1973/status/1075722678189867008)  
A tussle for supremacy between left and right halves of your brain is why yellow dots disappear, staring at the green one  
[Why do the dots disappear? &#8211; Why Evolution Is True](https://whyevolutionistrue.wordpress.com/2014/09/02/why-do-the-dots-disappear/)   

### Ilusiones asociadas a movimiento

La limitación de la visión humana de no distinguir más de cierto número de imágenes por segundo lleva a ilusiones de movimiento: la animación, la televisión ... y otros tipos.  
También hay ilusión de movimiento al modificarse la iluminación en imágenes estáticas.  

[twitter SteveStuWill/status/1294738150166654977](https://twitter.com/SteveStuWill/status/1294738150166654977)  
One of the most powerful motion illusions I've seen: The cubes appear to be rotating in opposite directions – but they're not actually moving at all…   
Credit: @jagarikin  

[twitter jagarikin/status/1612981322871615489](https://twitter.com/jagarikin/status/1612981322871615489)  
not moving at all  

Optical illusion that makes stationary cubes appear in motion goes viral || Optical illusion
[![](https://img.youtube.com/vi/x-mHnNDHDDE/0.jpg)](https://www.youtube.com/shorts/x-mHnNDHDDE "Optical illusion that makes stationary cubes appear in motion goes viral || Optical illusion - Simply Logical") 

[twitter aberron/status/1619343226883969026](https://twitter.com/aberron/status/1619343226883969026)  
Este cubo no encoge ni rota. Todo ocurre en tu cabeza.  
[twitter jagarikin/status/1618798223975677952](https://twitter.com/jagarikin/status/1618798223975677952)  
  
[This square isn’t actually rotating. Here’s why | World Economic Forum](https://www.weforum.org/agenda/2016/07/this-square-isn-t-actually-rotating-here-s-why)   
![](https://assets.weforum.org/editor/large_qcgTSTJaz1sqrHI6x3w7eUIk1PGcmk_7mP7MDAShmPQ.gif)  

[Barrier Grid Animation / Picket Fence Animation / Moire Animation / Scanimation / Lenticular Animation](https://kinegram.app/#)  
They go by many names, but they are all methods of creating animations in a analogue static way.   

[twitter jagarikin/status/1332906807350624259](https://twitter.com/jagarikin/status/1332906807350624259)   
¡Súper fácil! Un curso sobre cómo crear ilusiones ópticas que parecen moverse  

[Scanimation.org](https://scanimation.org/)  


