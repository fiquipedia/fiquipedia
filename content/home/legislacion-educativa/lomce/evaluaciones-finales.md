---
aliases:
  - /home/legislacion-educativa/evaluaciones-finales/
---

# Evaluaciones Finales, "reválidas" (surgen con LOMCE y desaparecen con LOMLOE)

Conocidas como "reválidas" Con la implantación de LOMCE y la aprobación de  [Real Decreto 310/2016](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-7337)  (ver en  [legislación educativa](/home/legislacion-educativa) ) se crean nuevos apartados para guardar la información  
En diciembre 2016  [Real Decreto-Ley 5/2016](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-11733)  realiza modificaciones importantes  
**En diciembre 2020 [Ley Orgánica 3/2020, de 29 de diciembre, por la que se modifica la Ley Orgánica 2/2006, de 3 de mayo, de Educación](https://www.boe.es/buscar/act.php?id=BOE-A-2020-17264)  (LOMLOE) deroga LOMCE y Real Decreto-Ley 5/2016, por lo que desaparecen las evaluaciones finales, pero queda esto como información histórica.**

##  Historia de la normativa sobre reválidas
2013 las fija en LOE en su calendario de implantación original  
[https://www.boe.es/buscar/act.php?id=BOE-A-2013-12886&b=14&tn=1&p=20131210#dfquinta](https://www.boe.es/buscar/act.php?id=BOE-A-2013-12886&b=14&tn=1&p=20131210#dfquinta)   
Disposición final quinta. Calendario de implantación.  
1. Las modificaciones introducidas en el currículo, la organización, objetivos, promoción y evaluaciones de Educación Primaria se implantarán para los cursos primero, tercero y quinto en el curso escolar 2014-2015, y para los cursos segundo, cuarto y sexto en el curso escolar 2015-2016.  
2. Las modificaciones introducidas en el currículo, la organización, objetivos, requisitos para la obtención de certificados y títulos, programas, promoción y evaluaciones de Educación Secundaria Obligatoria se implantarán para los cursos primero y tercero en el curso escolar 2015-2016, y para los cursos segundo y cuarto en el curso escolar 2016-2017.  
La evaluación final de Educación Secundaria Obligatoria correspondiente a la convocatoria que se realice en el año 2017 no tendrá efectos académicos. En ese curso escolar sólo se realizará una única convocatoria.  
3. Las modificaciones introducidas en el currículo, la organización, objetivos, requisitos para la obtención de certificados y títulos, programas, promoción y evaluaciones de Bachillerato se implantarán para el primer curso en el curso escolar 2015-2016, y para el segundo curso en el curso escolar 2016-2017.  
La evaluación final de Bachillerato correspondiente a las dos convocatorias que se realicen en el año 2017 únicamente se tendrá en cuenta para el acceso a la Universidad, pero su superación no será necesaria para obtener el título de Bachiller. También se tendrá en cuenta para la obtención del título de Bachiller por los alumnos y alumnas que se encuentren en posesión de un título de Técnico de grado medio o superior de Formación Profesional o de las Enseñanzas Profesionales de Música o de Danza, de conformidad, respectivamente, con los artículos 44.4 y 50.2 de la Ley Orgánica 2/2006, de 3 de mayo.

###  2015
 En 2015 el borrador del Real Decreto fue público en la web del Ministerio de Educación, pero se descartó (en ese borrador se proponía que fuese completamente tipo test)  
[Real Decreto que regula las características de las pruebas de evaluación final de Primaria, ESO y Bachillerato.](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2015/evaluaciones-finales.html) 

###  2016: Abril
 Crue Universidades Españolas y El MECD definen cómo será la prueba externa de Bachillerato  
 [http://www.crue.org/Documentos compartidos/Comunicados/20160427_NP_Prueba-Externa-Bachilletaro.pdf](http://www.crue.org/Documentos%20compartidos/Comunicados/20160427_NP_Prueba-Externa-Bachilletaro.pdf) 

###  2016: Mayo-Julio
En 2016 los borradores de proyecto de Real Decreto y Orden Ministerial no son públicos en la web del Ministerio de Educación, pero se pueden consultar oficiosamente en:  
[http://www.magisnet.com/pdf/Proyecto-de-orden.pdf](http://www.magisnet.com/pdf/Proyecto-de-orden.pdf)  (fecha interna del pdf es 29 abril 2016) y se anexa aquí hasta que haya una publicación oficial  
[http://www.magisnet.com/pdf/Proyecto-de-Real-Decreto.pdf](http://www.magisnet.com/pdf/Proyecto-de-Real-Decreto.pdf)  (fecha interna del pdf es 29 abril 2016) y se anexa aquí, ya que tiene un anexo **"Guías de corrección, codificación y calificación" y "Ficha individualizada de corrección, codificación y calificación para cada una de las preguntas o ítems de las pruebas**" que no está en el Real Decreto 310/2016 publicado en BOE. El anexo se edita y se poner por separado por su utilidad; permite intuir como serán las preguntas: "preguntas directamente asociadas a cada (o a varios) estándar de aprendizaje evaluable"  [https://drive.google.com/file/d/0B-t5SY0w2S8iR2NVQ0wyUm8wZ2M/view](https://drive.google.com/file/d/0B-t5SY0w2S8iR2NVQ0wyUm8wZ2M/view)  

No es legislación propiamente dicha, pero hay información asociada a dictámenes del Consejo Escolar de Estado de Mayo 2016, que citan los borradores no públicos de abril 2016
   *  [Al Proyecto de Real Decreto por el que se regulan las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato. D13. (12-05-2016)](https://www.educacionyfp.gob.es/dam/jcr:fda9d2d8-2f3b-4082-ad9c-541d0fa0424c/dictamen132016-pdf.pdf) 
   * Además del Consejo Escolar de Estado, también hay dictamen del Consejo de Estado sobre el proyecto de Real Decreto, se publicó en el BOE antes del RD 310/2016 [http://www.boe.es/buscar/doc.php?id=CE-D-2016-627](http://www.boe.es/buscar/doc.php?id=CE-D-2016-627) 
   *  [Al Proyecto de Orden Ministerial por el que se regulan las características, el diseño y el contenido de las pruebas de las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato para el curso 2016/2017. D14. (12-05-2016)](https://www.educacionyfp.gob.es/dam/jcr:c5d1709c-3b3d-4068-9f00-2f05516d6547/dictamen142016-pdf.pdf) 
   
 En julio se publica en BOE el Real Decreto 310/2016, de 29 de julio, por el que se regulan las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato.Fija una fecha que no es retocada, y que se incumple  
[Disposición adicional segunda. Características, diseño, contenidos, de las pruebas de las evaluaciones finales a realizar en el curso escolar 2016-2017.](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#da-2)  
 El titular del Ministerio de Educación, Cultura y Deporte establecerá, antes del 30 de noviembre de 2016, las características, el diseño y el contenido de las pruebas de las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato para el curso 2016/2017.

###  2016: octubre
En octubre se publica un nuevo borrador de orden, un documento de 193 páginas  
[http://www.magisnet.com/pdf/EvaluacionesFinales.pdf](http://www.magisnet.com/pdf/EvaluacionesFinales.pdf)  
Añade "Artículo 3. Matrices de especificaciones" que indica 
*"Las matrices de especificaciones de cada una de las materias incluidas en la evaluación final de Educación Secundaria Obligatoria son las recogidas en el Anexo I de la presente orden.*  
*Las matrices de especificaciones de cada una de las materias incluidas en la evaluación final de Bachillerato son las recogidas en el Anexo II de la presente orden"*  
en artículo 6.2 (ESO) *"Al menos el ***80 %** de los estándares de aprendizaje evaluados en cada una de las pruebas deberán seleccionarse de entre los definidos en la matriz de especificaciones de la materia correspondiente."*  
en artículo 6.3 (Bachillerato) *"Al menos el ***70 %** de los estándares de aprendizaje evaluados en cada una de las pruebas deberán estar entre los definidos en la matriz de especificaciones de la materia correspondiente."*

###  2016: noviembre 
El 15 de noviembre de 2016 se publica en web el ministerio proyecto de orden, con fecha 11.11.2016, aceptando aportaciones hasta 23.11.2016  
[https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2016/pruebas-evalucaciones-finales-eso-bachillerato.html](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2016/pruebas-evalucaciones-finales-eso-bachillerato.html) 
   *  [Proyecto de orden ministerial por la que se determinan las características, el diseño y el contenido de las pruebas de las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato para el curso 2016/2017, las fechas máximas para realizar las evaluaciones y resolver los procedimientos de revisión de las calificaciones obtenidas, y los cuestionarios de contexto](https://www.educacionyfp.gob.es/dam/jcr:42c6e551-2e34-409a-b830-36e794a02e6a/proyecto.pdf) 
      * Fecha de publicación: 15/11/2016​
      * Fecha límite para la presentación de aportaciones: 23​/11/2016
      * Remisión de aportaciones a: [novedades.inee@mecd.es](mailto:novedades.inee@mecd.es) 

15 de noviembre de 2016, en la votación del congreso el ministro dice que suspenderá los efectos académicos más allá de 2017.  
[twitter FiQuiPedia/status/798681786918273025](https://twitter.com/FiQuiPedia/status/798681786918273025)  
En la rueda de prensa del consejo de ministros del 18 de noviembre de 2016 se dice que la evaluación de 4º ESO será muestral (no la hacen todos los centros) y en la de 2º Bachillerato solamente entrarán las troncales de 2º  
[twitter FiQuiPedia/status/799672881697476608](https://twitter.com/FiQuiPedia/status/799672881697476608)  
El día 18 de noviembre se publica el Proyecto de Real Decreto-Ley de Medidas Urgentes para la Ampliación del Calendario de Implantación de la Ley Orgánica 8/2013, de 9 de diciembre, para la Mejora de la Calidad Educativa donde aparecen las modificaciones (muestral en 4º, "únicamente sobre materias troncales cursadas en segundo curso") y se añade (y alarga en el tiempo) un tema que surgió en un RD de 2015 (de primaria!) asociado a FPB y ESO.  
[https://drive.google.com/open?id=0B-t5SY0w2S8ibDRXWTd0WFhDN28](https://drive.google.com/open?id=0B-t5SY0w2S8ibDRXWTd0WFhDN28)  
En prensa comentarios  
[http://www.elmundo.es/sociedad/2016/11/18/582ed2ee468aeb053e8b46ad.html](http://www.elmundo.es/sociedad/2016/11/18/582ed2ee468aeb053e8b46ad.html)  
El 18 de noviembre el consejo de ministros nombra presidente el Consejo Escolar de Estado; para normativa de reválidas deben emitir dictamen, y presidente y vicepresidente se jubilaron en mayo y nombrarlos era imprescindible.  
El 23 de noviembre el presidente del Consejo Escolar de Estado toma posesión de su cargo  
24 noviembre 2016: CRUE y Méndez de Vigo firman un acuerdo  
[http://www.europapress.es/sociedad/educacion-00468/noticia-mendez-vigo-firma-crue-acuerdo-sistema-acceso-universidad-20161124094740.html](http://www.europapress.es/sociedad/educacion-00468/noticia-mendez-vigo-firma-crue-acuerdo-sistema-acceso-universidad-20161124094740.html)  
Ministerio de Educación, Cultura y Deporte y CRUE-Universidades Españolas ratifican su acuerdo sobre la Prueba de Evaluación de Bachillerato  
[https://www.educacionyfp.gob.es/en/prensa/actualidad/2016/11/20161124-piriz.html](https://www.educacionyfp.gob.es/en/prensa/actualidad/2016/11/20161124-piriz.html)  
29 noviembre 2016: reunión de la Comisión Permanente del Consejo Escolar Estado, no emite dictamen, se incumple plazo 30 nov de Disposición Adicional 2ª de RD 310/2016  
[https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#da-2](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#da-2)  
 [http://www.lainformacion.com/educacion/EDUCACION-LLEVARA-REVALIDAS-ESCOLAR-DICIEMBRE_0_976402876.html](http://www.lainformacion.com/educacion/EDUCACION-LLEVARA-REVALIDAS-ESCOLAR-DICIEMBRE_0_976402876.html) 

###  2016: diciembre 
El 2 de diciembre el consejo de ministros aprueba la orden ministerial (lo dice Méndez de Vigo el 6 de diciembre) y se la remite al Consejo Escolar de Estado para que haga dictamen, en principio el 13 de diciembre [twitter FiQuiPedia/status/805878213469728770](https://twitter.com/FiQuiPedia/status/805878213469728770)  
El 5 de diciembre se publica nueva versión orden, solamente Bachillerato y cita futuro Real Decreto-Ley  
[Proyecto de orden ministerial por la que se determinan las características, el diseño, el contenido, el marco general de la evaluación de Bachillerato para el acceso a la universidad, las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, para el curso 2016/2017](https://www.educacionyfp.gob.es/dam/jcr:be095e9d-5bc9-4331-995d-ece14f25adfb/evaluacion-bachillerato-acceso-universidad.pdf) 
   * Fecha de publicación: 05/12/2016​
   * Fecha límite para la presentación de aportaciones: 15/12/2016 (siete días hábiles, según el procedimiento de urgencia, art. 26.6 LG)
   * Remisión de aportaciones a: [ novedades.inee@mecd.es](mailto:%20novedades.inee@mecd.es)  (Instituto Nacional de Evaluación Educativa)
El 6 de diciembre Méndez de Vigo cita orden y Rea Decreto-Ley  
[twitter FiQuiPedia/status/806255350680850434](https://twitter.com/FiQuiPedia/status/806255350680850434)  
El 9 de diciembre el consejo de ministros aprueba el Real Decreto-Ley que modifica calendario de implantación LOMCE asociado a reválidas [Referencia del Consejo de Ministros, 9 de diciembre de 2016, APROBADO UN REAL DECRETO LEY DE MEDIDAS URGENTES PARA LA AMPLIACIÓN DEL CALENDARIO DE IMPLANTACIÓN DE LA LOMCE - lamoncloa.gob.es](http://www.lamoncloa.gob.es/consejodeministros/referencias/Paginas/2016/refc20161209.aspx#LOMCE)  
El 10 de diciembre se publica en BOE Real Decreto-Ley 5/2016  
[Real Decreto-ley 5/2016, de 9 de diciembre, de medidas urgentes para la ampliación del calendario de implantación de la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-11733)  
El 13 de diciembre el Consejo Escolar de Estado tiene comisión permanente y emite dictamen (se publica en su web el día 20 de diciembre)
   *  [Al Proyecto de Orden Ministerial por la que se determinan las características, el diseño, el contenido, el marco general de la evaluación de Bachillerato para el acceso a la Universidad, las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, para el curso 2016/2017. D15. (13-12-2016)](https://www.educacionyfp.gob.es/dam/jcr:6693f142-4e11-47c2-b92a-256acee0b4b4/dictamen152016-pdf.pdf) 
El 21 de diciembre el congreso vota y aprueba el Real Decreto-Ley 5/2016  
El 23 de diciembre se publica en BOE orden que regula evaluación final Bachillerato para curso 2016/2017: [Orden ECD/1941/2016, de 22 de diciembre, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, para el curso 2016/2017. ](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-12219)  
El 29 de diciembre se publica en BOE convalidación Real Decreto-Ley 5/2016 [Resolución de 21 de diciembre de 2016, del Congreso de los Diputados, por la que se ordena la publicación del Acuerdo de convalidación del Real Decreto-ley 5/2016, de 9 de diciembre, de medidas urgentes para la ampliación del calendario de implantación de la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-12461) 

###  2017: marzo
 [Proyecto de Orden Ministerial por la que se determinan características de las evaluaciones finales de Educación Primaria y ESO](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2017/evaluacion-final-educacion-primaria.html) 

###  2017: abril
 Resolución de 21 de marzo de 2017, de la Secretaría de Estado de Educación, Formación Profesional y Universidades, por la que se establecen las adaptaciones de la evaluación de Bachillerato para el acceso a la Universidad a las necesidades y situación de los centros españoles situados en el exterior del territorio nacional, los programas educativos en el exterior, los programas internacionales, los alumnos procedentes de sistemas educativos extranjeros y las enseñanzas a distancia, para el curso 2016-2017.  
 [http://www.boe.es/diario_boe/txt.php?id=BOE-A-2017-3949](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2017-3949) 

##  Aspectos comunes a ambas evaluaciones finales, ESO y Bachillerato

###  Cómo serán las evaluaciones
Hasta que haya información (ver disposición adicional segunda), aparte de tomar referencias anteriores a LOMCE ( [pruebas de acceso libre ESO](/home/pruebas/pruebas-libres-graduado-eso) ,  [PAU en Bachillerato](/home/recursos/recursospau/recursos-pau-genericos) ), la única referencia es lo que indica la normativa LOMCE, ver [artículo 4 Pruebas, unidades de evaluación y tipos de preguntas de Real Decreto 310/2016, de 29 de julio, por el que se regulan las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#a4)   
**Artículo 4. Pruebas, unidades de evaluación y tipos de preguntas.**
1. Las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato tendrán una duración de un máximo de cuatro días. Aquellas Administraciones educativas con lengua cooficial podrán establecer una duración de un máximo de cinco días.
2. Cada una de las pruebas de la evaluación final de Educación Secundaria Obligatoria tendrá una duración de 60 minutos, estableciéndose un descanso mínimo de 15 minutos entre sesión y sesión.  
Cada una de las pruebas de la evaluación final de Bachillerato tendrá una duración de 90 minutos, estableciéndose un descanso mínimo de 20 minutos entre sesión y sesión.  
Sin perjuicio de lo establecido en el artículo 8.2, los tiempos de descanso no podrán ser utilizados para ampliar el tiempo de realización de las pruebas por los alumnos con necesidades específicas de apoyo educativo a quienes se haya prescrito dicha medida.  
3. Se realizará una prueba por cada una de las materias objeto de evaluación. Cada una de las pruebas constará de las unidades de evaluación que sean convenientes, y cada unidad de evaluación de al menos dos preguntas basadas en el estímulo propuesto. En total, cada prueba comprenderá un número máximo de 15 preguntas o ítems.  
4. Las unidades de evaluación estarán contextualizadas en entornos próximos a la vida del alumnado, por ejemplo, situaciones personales, familiares, escolares y sociales, además de entornos científicos y humanísticos.  
5. Cada una de las pruebas contendrá preguntas abiertas y semiabiertas que requerirán del alumnado capacidad de pensamiento crítico, reflexión y madurez, especialmente en la evaluación final de Bachillerato.  
Además de estos tipos de preguntas, se podrán utilizar también preguntas de opción múltiple, siempre que en cada una de las pruebas el porcentaje de preguntas abiertas y semiabiertas alcance como mínimo el 50%.  
6. Las pruebas podrán realizarse en soporte papel y/o en formato digital.  
7. Las pruebas deberán resultar atractivas y motivadoras, para lo que deberán cuidarse las imágenes, tablas y gráficos empleados y otras características formales.  

**Disposición adicional segunda. Características, diseño, contenidos, de las pruebas de las evaluaciones finales a realizar en el curso escolar 2016-2017.**  
El titular del Ministerio de Educación, Cultura y Deporte establecerá, antes del **30 de noviembre de 2016,** las características, el diseño y el contenido de las pruebas de las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato para el curso 2016/2017.  
Ver historia de normativa de reválidas: borrador de orden de octubre de 2016, de noviembre, de diciembre y tema de matrices de especificaciones y 80% para ESO y 70% para Bachillerato  
El plazo del 30 de noviembre no se ha cumplido.

###  ¿En la evaluación final pueden entrar estándares de aprendizaje evaluables de cursos anteriores?
 Se trata por separado en ESO y Bachillerato. 

###  Concepto de "unidad de evaluación"
Se cita en artículo 4 de RD 310/2016 [artículo 4 de RD 310/2016](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#a4) sin definirse claramente   

> 3. Se realizará una prueba por cada una de las materias objeto de evaluación. Cada una de las pruebas constará de las **unidades de evaluación** que sean convenientes, y cada **unidad de evaluación** de al menos dos preguntas basadas en el estímulo propuesto. En total, cada prueba comprenderá un número máximo de 15 preguntas o ítems.  
4. Las ***unidades de evaluación** estarán contextualizadas en entornos próximos a la vida del alumnado, por ejemplo, situaciones personales, familiares, escolares y sociales, además de entornos científicos y humanísticos.  

En documento borrador "Guías de corrección, codificación y calificación" y "Ficha individualizada de corrección, codificación y calificación para cada una de las preguntas o ítems de las pruebas"  [https://drive.google.com/file/d/0B-t5SY0w2S8iR2NVQ0wyUm8wZ2M](https://drive.google.com/file/d/0B-t5SY0w2S8iR2NVQ0wyUm8wZ2M)  se puede ver como la "Ficha individualizada de corrección, codificación y calificación para cada una de las preguntas o ítems de las pruebas", se tiene un "TÍTULO DE LA UNIDAD DE EVALUACIÓN:"  

Mi visión es cada una de las 15 preguntas o ítems pueden ser realmente apartados de "preguntas generales", siendo cada una de estas "pregunta general" una "unidad de evaluación". Al tener un límite de al menos dos preguntas por unidad, y un máximo de 15 preguntas, hay un máximo de "7 preguntas generales" en una prueba (en caso de haber 6 unidades con 2 ítems y una unidad con 3), aunque no hay límite mínimo, y podría ser una única unidad con 15 ítems. Se puede aventurar que podrían ser  
- 5 "preguntas generales", cada una con tres apartados "a,b y c", que serían tres "preguntas o ítems", y así habría 15 ítems, el máximo
- 5 "preguntas generales", cada una con dos apartados "a y b", que serían dos "preguntas o ítems", y así habría 10 ítems, menos del máximo  
En  [Evaluaciones Finales Bachillerato](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-bachillerato)  intento poner ejemplos asociados a física

##  [Evaluaciones Finales ESO](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-eso) 


##  [Evaluaciones Finales Bachillerato](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-bachillerato) 


##  Posible paralización/derogación reválidas
LOMCE y reválidas es normativa vigente e implantada iniciado el curso 2016-2017, no se puede paralizar la implantación de algo ya implantado. Si acaso solamente sería paralizar la ejecución de reválidas de ESO y Bachillerato que por primera vez se harán en 2017, las de primaria además de implantadas ya se han realizado), y para ello habría que derogar normativa de una ley orgánica, derogar todo o parte es muy poco probable, requiere un gobierno no en funciones, voluntad y alternativas.  
Información concreta sobre votaciones en congreso para paralizar/derogar LOMCE en 2016 (ver hilo: se cita mayo abril 2016, 5 octubre 2016 congreso y 18 octubre 2016 senado) [twitter FiQuiPedia/status/783420060794757120](https://twitter.com/FiQuiPedia/status/783420060794757120)  
Con la publicación de un borrador de orden el 25 de octubre de 2016, antes de la huelga del día 26, y lo dicho en la sesión de investidura del día 27 de octubre de 2016, muchos medios dicen que se suspenden las reválidas, pero no es así  
[twitter FiQuiPedia/status/791583426558226433](https://twitter.com/FiQuiPedia/status/791583426558226433)  

El 15 de noviembre hay una votación en el congreso, que no paraliza nada  
[twitter FiQuiPedia/status/798671351229747200](https://twitter.com/FiQuiPedia/status/798671351229747200)  

El 18 de noviembre el ministro dice que el gobierno va a revocar la votación del 15 de noviembre  
[twitter FiQuiPedia/status/799672282025078785](https://twitter.com/FiQuiPedia/status/799672282025078785)  

El 18 de noviembre el ministro dice que va a modificar las reválidas vía Real Decreto-Ley: 4º ESO serán muestrales y 2º Bachillerato solamente troncales 2º, "suspendiéndolas" hasta que se alcance el pacto educativo  
[twitter FiQuiPedia/status/799672881697476608](https://twitter.com/FiQuiPedia/status/799672881697476608)  

El 10 de noviembre se aprueba Real Decreto-Ley 5/2016 que suspende / modifica "Hasta la entrada en vigor de la normativa resultante del Pacto de Estado social y político por la educación"  
[http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-11733](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-11733)  

En diciembre 2020 se derogan definitivamente con LOMLOE
