
# Recursos bioquimica

Relacionado con varios temas  
[recursos quimiofobia](/home/recursos/quimica/quimiofobia)  
[Química orgánica](/home/recursos/quimica-organica)  
[Biología y geología](home/recursos/biologia-y-geologia/)  

Puede enlazar con electroquímica y seres vivos, química en la digestión, química y medicinas, química y medioambiente pero la mayor parte de ello se clasifica dentro de bioquímica.  

El libro de referencia es [Lehninger Principles of Biochemistry, David L. Nelson, Michael M. Cox, ISBN 9781464126116](https://books.google.es/books/about/Lehninger_Principles_of_Biochemistry.html?id=wy6WDAEACAAJ)  

Un recurso esencial son las infografías asociadas a [etiqueta "biochemistry" en compoundchem](https://www.compoundchem.com/category/biochemistry/)  

[What is folic acid, and why is it important during pregnancy? - compoundchem.com](https://www.compoundchem.com/2021/09/21/folic-acid/)  
![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2023/09/Folic-acid-and-pregnancy-2023.png?resize=1536%2C1086&ssl=1

[What causes hangovers? A biochemical mystery - compoundchem.com](https://www.compoundchem.com/2016/01/01/hangover/)  
![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2023/08/Chemistry-of-a-hangover-2023.png?resize=1536%2C1086&ssl=1)  

[Periodic Graphics: What are our skeletons made of? Chemical educator and Compound Interest blogger Andy Brunning explores the biochemistry of our bones. ](https://cen.acs.org/biological-chemistry/biochemistry/Periodic-Graphics-skeletons-made/100/i38)  
![](https://acs-h.assetsadobe.com/is/image//content/dam/cen/100/38/WEB/10038-feature3-graphic.jpg/?$responsive$&wid=700&qlt=90,0&resMode=sharp2)  

[Periodic Graphics: The science of fear. Chemical educator and Compound Interest blogger Andy Brunning explains the physiology of fright ](https://cen.acs.org/biological-chemistry/neuroscience/Periodic-Graphics-Science-Fear/98/i40)  
![](https://cen.acs.org/content/dam/cen/98/40/WEB/09840-feature3-fear.jpg)  

[Atención farmacoterapéutica al paciente con enfermedad cardiaca isquémica - Servicio de Farmacia Hospitalaria. Hospital U. Infanta Sofía - Madrid](https://formacion.sefh.es/dpc/framework/atf-cardiovasculares/paciente-enfermedad-cardiaca-isquemica/documentos/paciente-enfermedad-cardiaca-isquemica.pdf)  
Cita algunos medicamentos como bisoprolol y el uso de nitroglicerina  

