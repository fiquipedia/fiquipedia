# Currículo Física y Química 1º Bachillerato (LOMCE)

Ver DECRETO 52/2015 en  [legislación](/home/legislacion-educativa)  que indica   
  
*Artículo 9 Materias y currículo*  
 El currículo del Bachillerato en los centros docentes de la Comunidad de Madrid se establece del modo siguiente:  
 a) Materias del bloque de asignaturas troncales: Los contenidos, criterios de evaluación y estándares de aprendizaje evaluables de las materias del bloque de asignaturas troncales son los del currículo básico fijados para dichas materias en el Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.   
 La Administración educativa de la Comunidad de Madrid podrá complementar los contenidos del bloque de materias troncales.  
  
La Comunidad de Madrid no ha complementado contenidos por lo que aplica el Real Decreto 1105/2014  
El pdf del RD 1105/2014 son 378 páginas y no tiene índice: Física y Química está en las páginas 88 a 104, y 1º de Bachillerato de la 100 a la 104.  
  
 Esta página es estática y sin enlaces: para ver enlaces a recursos ver Contenidos currículo con enlaces a recursos: Física y Química 1º Bachillerato (LOMCE) (pendiente)  

## Comparación para Física y Química de 1º Bachillerato de currículo LOMCE con  [currículo LOE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculofisicayquimica-1bachillerato) 

La tabla inicialmente la hago para LOMCE vs LOE, pero la muevo a página separada para incluir en comparación LOMLOE

Ver [comparación currículo Física y Química 1º Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/comparacion-curriculo-fisica-y-quimica-1-bachillerato)


## Currículo Física y Química 1º Bachillerato condicionado por laboratorio, TIC y simulaciones
 El currículo cita a veces laboratorio y simulaciones, por lo que si no hay laboratorio (por ejemplo por falta de desdoble es habitual) o no hay posibilidad de un aula de informática para todos los alumnos, hay ciertas partes del currículo que no se pueden cubrir, y se citan aquí.  
Bloque 1. La actividad científica  
  
1.5. Elabora e interpreta representaciones gráficas de diferentes procesos físicos y químicos a partir de los datos obtenidos en experiencias de laboratorio o virtuales y relaciona los resultados obtenidos con las ecuaciones que representan las leyes y principios subyacentes.  
2. Conocer, utilizar y aplicar las Tecnologías de la Información y la Comunicación en el estudio de los fenómenos físicos y químicos.  
2.1. Emplea aplicaciones virtuales interactivas para simular experimentos físicos de difícil realización en el laboratorio.  
  
2.2. Establece los elementos esenciales para el diseño, la elaboración y defensa de un proyecto de investigación, sobre un tema de actualidad científica, vinculado con la Física o la Química, utilizando preferentemente las TIC.  
Bloque 2. Aspectos cuantitativos de la química  
  
4.1. Expresa la concentración de una disolución en g/l, mol/l % en peso y % en volumen. Describe el procedimiento de preparación en el laboratorio, de disoluciones de una concentración determinada y realiza los cálculos necesarios, tanto para el caso de solutos en estado sólido como a partir de otra de concentración conocida.  
Bloque 4. Transformaciones energéticas y espontaneidad de las reacciones químicas2.1. Explica razonadamente el procedimiento para determinar el equivalente mecánico del calor tomando como referente aplicaciones virtuales interactivas asociadas al experimento de Joule.  
  
Bloque 6. Cinemática  
  
8.3. Emplea simulaciones virtuales interactivas para resolver supuestos prácticos reales, determinando condiciones iniciales, trayectorias y puntos de encuentro de los cuerpos implicados.  
  
Bloque 7. Dinámica3.1. Determina experimentalmente la constante elástica de un resorte aplicando la ley de Hooke y calcula la frecuencia con la que oscila una masa conocida unida a un extremo del citado resorte.  
  
*6.1. Comprueba las leyes de Kepler *a partir de tablas de datos astronómicos correspondientes al movimiento de algunos planetas.*9.1. *Diseña y describe experiencias que pongan de manifiesto el movimiento armónico simple (M.A.S) y determina las magnitudes involucradas.  

## Bloque 1. La actividad científica

### Contenidos
 Estrategias necesarias en la actividad científica.   
 Tecnologías de la Información y la Comunicación en el trabajo científico.   
 Proyecto de investigación.  

### Criterios de evaluación
 1. Reconocer y utilizar las estrategias básicas de la actividad científica como: plantear problemas, formular hipótesis, proponer modelos, elaborar estrategias de resolución de problemas y diseños experimentales y análisis de los resultados.  
 2. Conocer, utilizar y aplicar las Tecnologías de la Información y la Comunicación en el estudio de los fenómenos físicos y químicos.  

### Estándares de aprendizaje evaluables
1.1. Aplica habilidades necesarias para la investigación científica, planteando preguntas, identificando problemas, recogiendo datos, diseñando estrategias de resolución de problemas utilizando modelos y leyes, revisando el proceso y obteniendo conclusiones.  
 1.2. Resuelve ejercicios numéricos expresando el valor de las magnitudes empleando la notación científica, estima los errores absoluto y relativo asociados y contextualiza los resultados.  
 1.3. Efectúa el análisis dimensional de las ecuaciones que relacionan las diferentes magnitudes en un proceso físico o químico.  
 1.4. Distingue entre magnitudes escalares y vectoriales y opera adecuadamente con ellas.  
 1.5. Elabora e interpreta representaciones gráficas de diferentes procesos físicos y químicos a partir de los datos obtenidos en experiencias de laboratorio o virtuales y relaciona los resultados obtenidos con las ecuaciones que representan las leyes y principios subyacentes.  
 1.6. A partir de un texto científico, extrae e interpreta la información, argumenta con rigor y precisión utilizando la terminología adecuada.  
 2.1. Emplea aplicaciones virtuales interactivas para simular experimentos físicos de difícil realización en el laboratorio.  
 2.2. Establece los elementos esenciales para el diseño, la elaboración y defensa de un proyecto de investigación, sobre un tema de actualidad científica, vinculado con la Física o la Química, utilizando preferentemente las TIC.

## Bloque 2. Aspectos cuantitativos de la química

### Contenidos
Revisión de la teoría atómica de Dalton.  
 Leyes de los gases. Ecuación de estado de los gases ideales.  
 Determinación de fórmulas empíricas y moleculares.  
 Disoluciones: formas de expresar la concentración, preparación y propiedades coligativas.  
 Métodos actuales para el análisis de sustancias: Espectroscopía y Espectrometría.

### Criterios de evaluación
 1. Conocer la teoría atómica de Dalton así como las leyes básicas asociadas a su establecimiento.  
 2. Utilizar la ecuación de estado de los gases ideales para establecer relaciones entre la presión, volumen y la temperatura.  
 3. Aplicar la ecuación de los gases ideales para calcular masas moleculares y determinar formulas moleculares.  
 4. Realizar los cálculos necesarios para la preparación de disoluciones de una concentración dada y expresarla en cualquiera de las formas establecidas.  
 5. Explicar la variación de las propiedades coligativas entre una disolución y el disolvente puro.  
 6. Utilizar los datos obtenidos mediante técnicas espectrométricas para calcular masas atómicas.  
 7. Reconocer la importancia de las técnicas espectroscópicas que permiten el análisis de sustancias y sus aplicaciones para la detección de las mismas en cantidades muy pequeñas de muestras.  

### Estándares de aprendizaje evaluables
 1.1. Justifica la teoría atómica de Dalton y la discontinuidad de la materia a partir de las leyes fundamentales de la Química ejemplificándolo con reacciones.  
 2.1. Determina las magnitudes que definen el estado de un gas aplicando la ecuación de estado de los gases ideales.  
 2.2. Explica razonadamente la utilidad y las limitaciones de la hipótesis del gas ideal.  
 2.3. Determina presiones totales y parciales de los gases de una mezcla relacionando la presión total de un sistema con la fracción molar y la ecuación de estado de los gases ideales.  
 3.1. Relaciona la fórmula empírica y molecular de un compuesto con su composición centesimal aplicando la ecuación de estado de los gases ideales.  
 4.1. Expresa la concentración de una disolución en g/l, mol/l % en peso y % en volumen. Describe el procedimiento de preparación en el laboratorio, de disoluciones de una concentración determinada y realiza los cálculos necesarios, tanto para el caso de solutos en estado sólido como a partir de otra de concentración conocida.  
 5.1. Interpreta la variación de las temperaturas de fusión y ebullición de un líquido al que se le añade un soluto relacionándolo con algún proceso de interés en nuestro entorno.  
 5.2. Utiliza el concepto de presión osmótica para describir el paso de iones a través de una membrana semipermeable.  
 6.1. Calcula la masa atómica de un elemento a partir de los datos espectrométricos obtenidos para los diferentes isótopos del mismo.  
 7.1. Describe las aplicaciones de la espectroscopía en la identificación de elementos y compuestos.  

## Bloque 3. Reacciones químicas

### Contenidos
Estequiometría de las reacciones. Reactivo limitante y rendimiento de una reacción.  
 Química e industria.  

### Criterios de evaluación
1. Formular y nombrar correctamente las sustancias que intervienen en una reacción química dada.  
 2. Interpretar las reacciones químicas y resolver problemas en los que intervengan reactivos limitantes, reactivos impuros y cuyo rendimiento no sea completo.  
 3. Identificar las reacciones químicas implicadas en la obtención de diferentes compuestos inorgánicos relacionados con procesos industriales.  
 4. Conocer los procesos básicos de la siderurgia así como las aplicaciones de los productos resultantes.  
 5. Valorar la importancia de la investigación científica en el desarrollo de nuevos materiales con aplicaciones que mejoren la calidad de vida.  

### Estándares de aprendizaje evaluables
1.1. Escribe y ajusta ecuaciones químicas sencillas de distinto tipo (neutralización, oxidación, síntesis) y de interés bioquímico o industrial.  
 2.1. Interpreta una ecuación química en términos de cantidad de materia, masa, número de partículas o volumen para realizar cálculos estequiométricos en la misma.  
 2.2. Realiza los cálculos estequiométricos aplicando la ley de conservación de la masa a distintas reacciones.  
 2.3. Efectúa cálculos estequiométricos en los que intervengan compuestos en estado sólido, líquido o gaseoso, o en disolución en presencia de un reactivo limitante o un reactivo impuro.  
 2.4. Considera el rendimiento de una reacción en la realización de cálculos estequiométricos.  
 3.1. Describe el proceso de obtención de productos inorgánicos de alto valor añadido, analizando su interés industrial.  
 4.1. Explica los procesos que tienen lugar en un alto horno escribiendo y justificando las reacciones químicas que en él se producen.  
 4.2. Argumenta la necesidad de transformar el hierro de fundición en acero, distinguiendo entre ambos productos según el porcentaje de carbono que contienen.  
 4.3. Relaciona la composición de los distintos tipos de acero con sus aplicaciones.  
 5.1. Analiza la importancia y la necesidad de la investigación científica aplicada al desarrollo de nuevos materiales y su repercusión en la calidad de vida a partir de fuentes de información científica.  

## Bloque 4. Transformaciones energéticas y espontaneidad de las reacciones químicas

### Contenidos
Sistemas termodinámicos. Primer principio de la termodinámica. Energía interna. Entalpía. Ecuaciones termoquímicas.  
 Ley de Hess.  
 Segundo principio de la termodinámica. Entropía.  
 Factores que intervienen en la espontaneidad de una reacción química. Energía de Gibbs.  
 Consecuencias sociales y medioambientales de las reacciones químicas de combustión.  

### Criterios de evaluación
1. Interpretar el primer principio de la termodinámica como el principio de conservación de la energía en sistemas en los que se producen intercambios de calor y trabajo.  
 2. Reconocer la unidad del calor en el Sistema Internacional y su equivalente mecánico.  
 3. Interpretar ecuaciones termoquímicas y distinguir entre reacciones endotérmicas y exotérmicas.  
 4. Conocer las posibles formas de calcular la entalpía de una reacción química.  
 5. Dar respuesta a cuestiones conceptuales sencillas sobre el segundo principio de la termodinámica en relación a los procesos espontáneos.  
 6. Predecir, de forma cualitativa y cuantitativa, la espontaneidad de un proceso químico en determinadas condiciones a partir de la energía de Gibbs.  
 7. Distinguir los procesos reversibles e irreversibles y su relación con la entropía y el segundo principio de la termodinámica.  
 8. Analizar la influencia de las reacciones de combustión a nivel social, industrial y medioambiental y sus aplicaciones.  

### Estándares de aprendizaje evaluables
1.1. Relaciona la variación de la energía interna en un proceso termodinámico con el calor absorbido o desprendido y el trabajo realizado en el proceso.  
 2.1. Explica razonadamente el procedimiento para determinar el equivalente mecánico del calor tomando como referente aplicaciones virtuales interactivas asociadas al experimento de Joule.  
 3.1. Expresa las reacciones mediante ecuaciones termoquímicas dibujando e interpretando los diagramas entálpicos asociados.  
 4.1. Calcula la variación de entalpía de una reacción aplicando la ley de Hess, conociendo las entalpías de formación o las energías de enlace asociadas a una transformación química dada e interpreta su signo.  
 5.1. Predice la variación de entropía en una reacción química dependiendo de la molecularidad y estado de los compuestos que intervienen.  
 6.1. Identifica la energía de Gibbs con la magnitud que informa sobre la espontaneidad de una reacción química.  
 6.2. Justifica la espontaneidad de una reacción química en función de los factores entálpicos entrópicos y de la temperatura.  
 7.1. Plantea situaciones reales o figuradas en que se pone de manifiesto el segundo principio de la termodinámica, asociando el concepto de entropía con la irreversibilidad de un proceso.  
 7.2. Relaciona el concepto de entropía con la espontaneidad de los procesos irreversibles.  
 8.1. A partir de distintas fuentes de información, analiza las consecuencias del uso de combustibles fósiles, relacionando las emisiones de CO2, con su efecto en la calidad de vida, el efecto invernadero, el calentamiento global, la reducción de los recursos naturales, y otros y propone actitudes sostenibles para minorar estos efectos  

## Bloque 5. Química del carbono

### Contenidos
Enlaces del átomo de carbono.  
 Compuestos de carbono: Hidrocarburos, compuestos nitrogenados y oxigenados.  
 Aplicaciones y propiedades.  
 Formulación y nomenclatura IUPAC de los compuestos del carbono.  
 Isomería estructural.  
 El petróleo y los nuevos materiales.  

### Criterios de evaluación
(Nota: único caso en BOE con criterios de evaluación no numerados, pero se puede asumir que es numeración correlativa de cada párrafo, de 1 a 6, coincidiendo con estándares)  
Reconocer hidrocarburos saturados e insaturados y aromáticos relacionándolos con compuestos de interés biológico e industrial.  
 Identificar compuestos orgánicos que contengan funciones oxigenadas y nitrogenadas.  
 Representar los diferentes tipos de isomería.  
 Explicar los fundamentos químicos relacionados con la industria del petróleo y del gas natural.  
 Diferenciar las diferentes estructuras que presenta el carbono en el grafito, diamante, grafeno, fullereno y nanotubos relacionándolo con sus aplicaciones.  
 Valorar el papel de la química del carbono en nuestras vidas y reconocer la necesidad de adoptar actitudes y medidas medioambientalmente sostenibles.  

### Estándares de aprendizaje evaluables
1.1. Formula y nombra según las normas de la IUPAC: hidrocarburos de cadena abierta y cerrada y derivados aromáticos.  
 2.1. Formula y nombra según las normas de la IUPAC: compuestos orgánicos sencillos con una función oxigenada o nitrogenada.  
 3.1. Representa los diferentes isómeros de un compuesto orgánico.  
 4.1. Describe el proceso de obtención del gas natural y de los diferentes derivados del petróleo a nivel industrial y su repercusión medioambiental.  
 4.2. Explica la utilidad de las diferentes fracciones del petróleo.  
 5.1. Identifica las formas alotrópicas del carbono relacionándolas con las propiedades físico-químicas y sus posibles aplicaciones.  
 6.1. A partir de una fuente de información, elabora un informe en el que se analice y justifique a la importancia de la química del carbono y su incidencia en la calidad de vida  
 6.2. Relaciona las reacciones de condensación y combustión con procesos que ocurren a nivel biológico.  

## Bloque 6. Cinemática

### Contenidos
Sistemas de referencia inerciales. Principio de relatividad de Galileo.  
 Movimiento circular uniformemente acelerado.  
 Composición de los movimientos rectilíneo uniforme y rectilíneo uniformemente acelerado.  
 Descripción del movimiento armónico simple (MAS).  

### Criterios de evaluación
1. Distinguir entre sistemas de referencia inerciales y no inerciales.  
 2. Representar gráficamente las magnitudes vectoriales que describen el movimiento en un sistema de referencia adecuado.  
 3. Reconocer las ecuaciones de los movimientos rectilíneo y circular y aplicarlas a situaciones concretas.  
 4. Interpretar representaciones gráficas de los movimientos rectilíneo y circular.  
 5. Determinar velocidades y aceleraciones instantáneas a partir de la expresión del vector de posición en función del tiempo.  
 6. Describir el movimiento circular uniformemente acelerado y expresar la aceleración en función de sus componentes intrínsecas.  
 7. Relacionar en un movimiento circular las magnitudes angulares con las lineales.  
 8. Identificar el movimiento no circular de un móvil en un plano como la composición de dos movimientos unidimensionales rectilíneo uniforme (MRU) y/o rectilíneo uniformemente acelerado (M.R.U.A.).  
 9. Conocer el significado físico de los parámetros que describen el movimiento armónico simple (M.A.S) y asociarlo a el movimiento de un cuerpo que oscile.  

### Estándares de aprendizaje evaluables
1.1. Analiza el movimiento de un cuerpo en situaciones cotidianas razonando si el sistema de referencia elegido es inercial o no inercial.  
 1.2. Justifica la viabilidad de un experimento que distinga si un sistema de referencia se encuentra en reposo o se mueve con velocidad constante.  
 2.1. Describe el movimiento de un cuerpo a partir de sus vectores de posición, velocidad y aceleración en un sistema de referencia dado.  
 3.1. Obtiene las ecuaciones que describen la velocidad y la aceleración de un cuerpo a partir de la expresión del vector de posición en función del tiempo.  
 3.2. Resuelve ejercicios prácticos de cinemática en dos dimensiones (movimiento de un cuerpo en un plano) aplicando las ecuaciones de los movimientos rectilíneo uniforme (M.R.U) y movimiento rectilíneo uniformemente acelerado (M.R.U.A.).  
 4.1. Interpreta las gráficas que relacionan las variables implicadas en los movimientos M.R.U., M.R.U.A. y circular uniforme (M.C.U.) aplicando las ecuaciones adecuadas para obtener los valores del espacio recorrido, la velocidad y la aceleración.  
 5.1. Planteado un supuesto, identifica el tipo o tipos de movimientos implicados, y aplica las ecuaciones de la cinemática para realizar predicciones acerca de la posición y velocidad del móvil.  
 6.1. Identifica las componentes intrínsecas de la aceleración en distintos casos prácticos y aplica las ecuaciones que permiten determinar su valor.  
 7.1. Relaciona las magnitudes lineales y angulares para un móvil que describe una trayectoria circular, estableciendo las ecuaciones correspondientes.  
 8.1. Reconoce movimientos compuestos, establece las ecuaciones que lo describen, calcula el valor de magnitudes tales como, alcance y altura máxima, así como valores instantáneos de posición, velocidad y aceleración.  
 8.2. Resuelve problemas relativos a la composición de movimientos descomponiéndolos en dos movimientos rectilíneos.  
 8.3. Emplea simulaciones virtuales interactivas para resolver supuestos prácticos reales, determinando condiciones iniciales, trayectorias y puntos de encuentro de los cuerpos implicados.  
 9.1. Diseña y describe experiencias que pongan de manifiesto el movimiento armónico simple (M.A.S) y determina las magnitudes involucradas.  
 9.2. Interpreta el significado físico de los parámetros que aparecen en la ecuación del movimiento armónico simple.  
 9.3. Predice la posición de un oscilador armónico simple conociendo la amplitud, la frecuencia, el período y la fase inicial.  
 9.4. Obtiene la posición, velocidad y aceleración en un movimiento armónico simple aplicando las ecuaciones que lo describen.  
 9.5. Analiza el comportamiento de la velocidad y de la aceleración de un movimiento armónico simple en función de la elongación.  
 9.6. Representa gráficamente la posición, la velocidad y la aceleración del movimiento armónico simple (M.A.S.) en función del tiempo comprobando su periodicidad.  

## Bloque 7. Dinámica

### Contenidos
La fuerza como interacción.  
 Fuerzas de contacto. Dinámica de cuerpos ligados.  
 Fuerzas elásticas. Dinámica del M.A.S.  
 Sistema de dos partículas.  
 Conservación del momento lineal e impulso mecánico.  
 Dinámica del movimiento circular uniforme.  
 Leyes de Kepler.  
 Fuerzas centrales. Momento de una fuerza y momento angular. Conservación del momento angular.  
 Ley de Gravitación Universal.  
 Interacción electrostática: ley de Coulomb.  

### Criterios de evaluación
1. Identificar todas las fuerzas que actúan sobre un cuerpo.  
 2. Resolver situaciones desde un punto de vista dinámico que involucran planos inclinados y /o poleas.  
 3. Reconocer las fuerzas elásticas en situaciones cotidianas y describir sus efectos.  
 4. Aplicar el principio de conservación del momento lineal a sistemas de dos cuerpos y predecir el movimiento de los mismos a partir de las condiciones iniciales.  
 5. Justificar la necesidad de que existan fuerzas para que se produzca un movimiento circular.  
 6. Contextualizar las leyes de Kepler en el estudio del movimiento planetario.  
 7. Asociar el movimiento orbital con la actuación de fuerzas centrales y la conservación del momento angular.  
 8. Determinar y aplicar la ley de Gravitación Universal a la estimación del peso de los cuerpos y a la interacción entre cuerpos celestes teniendo en cuenta su carácter vectorial.  
 9. Conocer la ley de Coulomb y caracterizar la interacción entre dos cargas eléctricas puntuales.  
 10. Valorar las diferencias y semejanzas entre la interacción eléctrica y gravitatoria.  

### Estándares de aprendizaje evaluables
1.1. Representa todas las fuerzas que actúan sobre un cuerpo, obteniendo la resultante, y extrayendo consecuencias sobre su estado de movimiento.  
 1.2. Dibuja el diagrama de fuerzas de un cuerpo situado en el interior de un ascensor en diferentes situaciones de movimiento, calculando su aceleración a partir de las leyes de la dinámica.  
 2.1. Calcula el modulo del momento de una fuerza en casos prácticos sencillos.  
 2.2. Resuelve supuestos en los que aparezcan fuerzas de rozamiento en planos horizontales o inclinados, aplicando las leyes de Newton.  
 2.3. Relaciona el movimiento de varios cuerpos unidos mediante cuerdas tensas y poleas con las fuerzas actuantes sobre cada uno de los cuerpos.  
 3.1. Determina experimentalmente la constante elástica de un resorte aplicando la ley de Hooke y calcula la frecuencia con la que oscila una masa conocida unida a un extremo del citado resorte.  
 3.2. Demuestra que la aceleración de un movimiento armónico simple (M.A.S.) es proporcional al desplazamiento utilizando la ecuación fundamental de la Dinámica.  
 3.3. Estima el valor de la gravedad haciendo un estudio del movimiento del péndulo simple.  
 4.1. Establece la relación entre impulso mecánico y momento lineal aplicando la segunda ley de Newton.  
 4.2. Explica el movimiento de dos cuerpos en casos prácticos como colisiones y sistemas de propulsión mediante el principio de conservación del momento lineal.  
 5.1. Aplica el concepto de fuerza centrípeta para resolver e interpretar casos de móviles en curvas y en trayectorias circulares.  
 6.1. Comprueba las leyes de Kepler a partir de tablas de datos astronómicos correspondientes al movimiento de algunos planetas.  
 6.2. Describe el movimiento orbital de los planetas del Sistema Solar aplicando las leyes de Kepler y extrae conclusiones acerca del periodo orbital de los mismos.  
 7.1. Aplica la ley de conservación del momento angular al movimiento elíptico de los planetas, relacionando valores del radio orbital y de la velocidad en diferentes puntos de la órbita.  
 7.2. Utiliza la ley fundamental de la dinámica para explicar el movimiento orbital de diferentes cuerpos como satélites, planetas y galaxias, relacionando el radio y la velocidad orbital con la masa del cuerpo central.  
 8.1. Expresa la fuerza de la atracción gravitatoria entre dos cuerpos cualesquiera, conocidas las variables de las que depende, estableciendo cómo inciden los cambios en estas sobre aquella.  
 8.2. Compara el valor de la atracción gravitatoria de la Tierra sobre un cuerpo en su superficie con la acción de cuerpos lejanos sobre el mismo cuerpo.  
 9.1. Compara la ley de Newton de la Gravitación Universal y la de Coulomb, estableciendo diferencias y semejanzas entre ellas.  
 9.2. Halla la fuerza neta que un conjunto de cargas ejerce sobre una carga problema utilizando la ley de Coulomb.  
 10.1. Determina las fuerzas electrostática y gravitatoria entre dos partículas de carga y masa conocidas y compara los valores obtenidos, extrapolando conclusiones al caso de los electrones y el núcleo de un átomo.  

## Bloque 8. Energía

### Contenidos
Energía mecánica y trabajo.  
 Sistemas conservativos.  
 Teorema de las fuerzas vivas.  
 Energía cinética y potencial del movimiento armónico simple.  
 Diferencia de potencial eléctrico.  

### Criterios de evaluación
1. Establecer la ley de conservación de la energía mecánica y aplicarla a la resolución de casos prácticos.  
 2. Reconocer sistemas conservativos como aquellos para los que es posible asociar una energía potencial y representar la relación entre trabajo y energía.  
 3. Conocer las transformaciones energéticas que tienen lugar en un oscilador armónico.  
 4. Vincular la diferencia de potencial eléctrico con el trabajo necesario para transportar una carga entre dos puntos de un campo eléctrico y conocer su unidad en el Sistema Internacional.  

### Estándares de aprendizaje evaluables
1.1. Aplica el principio de conservación de la energía para resolver problemas mecánicos, determinando valores de velocidad y posición, así como de energía cinética y potencial.  
 1.2. Relaciona el trabajo que realiza una fuerza sobre un cuerpo con la variación de su energía cinética y determina alguna de las magnitudes implicadas.  
 2.1. Clasifica en conservativas y no conservativas, las fuerzas que intervienen en un supuesto teórico justificando las transformaciones energéticas que se producen y su relación con el trabajo.  
 3.1. Estima la energía almacenada en un resorte en función de la elongación, conocida su constante elástica.  
 3.2. Calcula las energías cinética, potencial y mecánica de un oscilador armónico aplicando el principio de conservación de la energía y realiza la representación gráfica correspondiente.  
 4.1. Asocia el trabajo necesario para trasladar una carga entre dos puntos de un campo eléctrico con la diferencia de potencial existente entre ellos permitiendo el la determinación de la energía implicada en el proceso.  
  
