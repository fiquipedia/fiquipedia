
# Physics Masterclasses

Jornadas dirigidas a alumnos entre 15 y 19 años. En 2013 entre febrero y marzo, con una videoconferencia con los moderadores del CERN.  
 [International Masterclasses - hands on particle physics](http://physicsmasterclasses.org/)   
 [International Masterclasses - hands on particle physics](http://www.physicsmasterclasses.org/index.php?cat=country&page=sp)   
 [International Masterclasses - hands on particle physics](http://www.physicsmasterclasses.org/index.php?cat=schedule)   
Ejemplo en noticias 2013 (IX Jornadas Europeas de Clases Magistrales en Física de Partículas)  
Cantabria:  [Veinte alumnos de Bachillerato disfrutar&#225;n de las IX Clases Magistrales de F&#237;sica de Part&#237;culas del IFCA ](http://www.europapress.es/cantabria/innpulsa-00775/noticia-veinte-alumnos-bachillerato-disfrutaran-ix-clases-magistrales-fisica-particulas-ifca-20130301122427.html)   
Madrid:  [Masterclass de F&iacute;sica de Part&iacute;culas para Estudiantes de Bachillerato en el CIEMAT](http://wwwae.ciemat.es/masterclasses/)  "El Jueves 14 de Marzo de 2013 el CIEMAT participará en las Masterclasses Internacionales para Estudiantes de Bachillerato (Hands on Particle Physics) organizadas por el International Particle Physics Outreach Group (IPPOG). Treinta y seis (36) estudiantes de 2º de bachillerato pasaréis la mañana en el CIEMAT para realizar una práctica de análisis de datos reales tomados en el experimento CMS del acelerador LHC del CERN. ...."  
  
En Madrid además de las masterclass "originales" se celebran otras similares durante la semana de la ciencia, en noviembre.  
  
Para entender lo que pasa al reconstruir la masa de una partícula a partir de los datos medidos de sus productos de desintegración:  
 [http://fbarradass.wordpress.com/la-fisica-simplificada](http://fbarradass.wordpress.com/la-fisica-simplificada/)   
  
 [International Masterclasses - hands on particle physics](http://physicsmasterclasses.org/index.php?cat=teachers_educators)   

## CMS 
 [cms.html](http://cms.physicsmasterclasses.org/cms.html)   
  
 [cmssp.html](http://cms.physicsmasterclasses.org/cmssp.html)   
Hay dos ejemplos  
Clase Magistral de CMS. Ejercicio J/Ψ.  [cmsjpsisp.html](http://cms.physicsmasterclasses.org/pages/cmsjpsisp.html)   
(Actualización 2016  [cms-masterclass-2016-documentation](https://quarknet.i2u2.org/page/cms-masterclass-2016-documentation) )  
Clase Magistral de CMS. Ejercicio WZH.  [cmswzsp.html](http://cms.physicsmasterclasses.org/pages/cmswzsp.html)   
  
La página de datos públicos de CMS   
 [Cern Authentication](http://cms.web.cern.ch/content/cms-public-data)   
  
Visualizadores de eventos  
 [CMS 3D Event Display](https://www.i2u2.org/elab/cms/event-display)  (antiguo)  
 [iSpy WebGL](https://www.i2u2.org/elab/cms/ispy-webgl)  (nuevo)  

## ATLAS
 [wpath.htm](http://atlas.physicsmasterclasses.org/en/wpath.htm)  [zpath.htm](http://atlas.physicsmasterclasses.org/en/zpath.htm) 

## 
 [MINERVA](http://atlas-minerva.web.cern.ch/atlas-minerva/index.php?lang=es)  [Atlantis](http://atlantis.web.cern.ch/atlantis/)   


## ALICE
 [ALICE MasterClass](http://www.physicsmasterclasses.org/exercises/ALICE/MasterClassWebpage.html)  [ALICE MasterClass](http://www-alice.gsi.de/masterclass/)  [](http://www-alice.gsi.de/masterclass/) 

## LHCb
 [LHCb International Physics Masterclasses](http://lhcb-public.web.cern.ch/lhcb-public/en/LHCb-outreach/masterclasses/en/)   

## Convocatorias Madrid

### 2016
 [Masterclass Internacional de Física de Partículas &#34;IPPOG&#34;  (February 16, 2018 - March 8, 2018): Descripción · Indico](https://indico.cern.ch/event/232052/)   
*La Masterclass Internacional de Física de Partículas para estudiantes de Bachillerato tendrá lugar en Madrid en dos centros: CIEMAT el 1 de Marzo e IFT (CSIC-UAM) el 14 de Marzo. En esta actividad estudiantes de 2º de bachillerato pasarán el día en el CIEMAT o en el IFT (aproximadamente 40 en cada centro) para realizar una práctica de análisis de datos reales tomados en los experimentos CMS o ATLAS del acelerador LHC del CERN. El ejercicio se llevará a cabo en ordenadores de estos centros. Previamente impartiremos un seminario sobre física de partículas, aceleradores, detectores y, muy importante, explicaremos en qué consiste la práctica y cómo se llevará a cabo. El ejercicio práctico estará guiado por investigadores del CIEMAT (miembros de CMS) o del IFT. Para terminar discutiremos los resultados obtenidos y habrá un concurso con preguntas sobre física de partículas en el que todos los estudiantes podrán demostrar lo que han asimilado.*  
  
La inscripción de estudiantes deben llevarla a cabo los/as profesores/as del instituto accediendo a este formulario. Esta es conjunta para los dos centros, teniendo que especificarse si se desea asistir a un centro específico o si es indiferente. Dado que el número de plazas es limitado posiblemente solo serán admitidos uno o dos estudiantes por instituto, por tanto los debéis inscribir por orden de interés en la física de partículas.   
  
En el formulario de inscripción no se pueden poner más de 3 alumnos, y es obligatorio indicar el NIF de los alumnos.  

### 2017
Inscripción durante febrero, celebrada en CIEMAT en marzo y en IFT en abril.  
 [masterclass2017](https://workshops.ift.uam-csic.es/masterclass2017)   

### 2018
 [Masterclass Internacional de Física de Partículas &#34;IPPOG&#34;  (February 16, 2018 - March 8, 2018): Descripción · Indico](https://indico.cern.ch/event/232052/)   
Inscripción hasta 8 febrero, celebrada en IFT 16feb y CIEMAT en marzo.  
 [Masterclass Internacional de Física de Partículas &#34;Semana de la Ciencia&#34;  (November 14, 2018): Descripción · Indico](https://indico.cern.ch/event/754221/) 

## Iniciativas similares pero no asociadas a detectores CERN  

### Neutrinos: IceCube Masterclasses
En 2014 comienza una iniciativa similar "IceCube Masterclasses" inspirada en la misma idea, aunque esta segunda no disponible en España  
 [masterclass](http://icecube.wisc.edu/masterclass)   

### Cazadores de rayos gamma
En 2016 iniciativa similar con rayos gamma de IFAE / FECYT, planteada como un concurso   
 [https://www.cazadoresderayosgamma.com/es](http://www.cazadoresderayosgamma.com/)   
  
*Cazadores de Rayos Gamma nace con la idea de acercar los jóvenes a los fenómenos más energéticos y extremos del Universo y a las herramientas que se usan para estudiarlos. Para ello, hemos creado esta plataforma con un formato y diseño que busca la innovación en todo momento: la combinación de web, python y material audiovisual.*  
  
La aplicación web está orientada a los estudiantes de centros de Secundaria, pero todo el mundo es bienvenido a disfrutar de la aventura que encierra. Incluye material educativo para conocer el trabajo que se desarrolla en el campo de la astronomía Gamma, información de los telescopios que se han diseñado y construido en centros de investigación del estado, y una estructura para obtener, manejar y analizar una serie de datos y sacar conclusiones sobre qué es aquello que están observando y cómo se relaciona con lo que han aprendido del Universo más extremo.   
  
Los mensajes y el entorno están adaptados a las edades a las que se dirige el proyecto, pero no se trata simplemente de un divertimento. La complejidad del proyecto aumentará a medida que se embarquen en la caza de gammas, pero la componente de investigación se mezcla con el juego, el aprendizaje y el reto que supone un buen enigma. 

### Eventos LIGO
 [https://losc.ligo.org/events/](https://losc.ligo.org/events/) 

## BAM (Big Analysis of Muons)
In April and May 2020, QuarkNet and IPPOG offered the "Big Analysis of Muons in CMS" on behalf of International Masterclasses. This year, we have a new Big Analysis of Muons (BAM) on May 24, 25, and 26. BAM features the choice of ATLAS or CMS measurements. This is a chance for your students to have a masterclass experience with authentic data, all online. Learn more about it at  [Big Analysis of Muons 2021 | Quarknet](https://quarknet.org/content/big-analysis-muons-2021) .   

