
# 1º Bachillerato Ciencias para el Mundo Contemporáneo

En 2013 la LOMCE hace desaparecer Ciencias para el Mundo Contemporáneo 1º Bachillerato (en LOE era obligatoria); en cierto modo se puede ver como que sus contenidos pasan a estar en dos optativas: [Cultura Científica en 4º ESO](/home/materias/eso/cultura-cientifica-4-eso/curriculo-cultura-cientifica-4-eso)  y  [Cultura Científica 1º Bachillerato](/home/materias/bachillerato/cultura-cientifica-1-bachillerato/curriculo-cultura-cientifica-1-bachillerato)  
* [Currículo](/home/materias/bachillerato/cienciasparaelmundocontemporaneo-1bachillerato/curriculocienciasparaelmundocontemporaneo-1bachillerato)  
* [Recursos](/home/recursos/recursos-por-materia-curso/recursos-ciencias-para-el-mundo-contemporaneo)  
