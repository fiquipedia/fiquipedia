
# Apuntes formulación

Esta página pretende centralizar  [apuntes](/home/recursos/apuntes)  y  [materiales autosuficientes](/home/recursos/materiales-autosuficientes)  propios de [formulación](/home/recursos/quimica/formulacion). (muchas veces incluyen ejercicios, a veces se pueden separar)  
En la página de  [libros ](/home/recursos/libros) se colocan libros relativos a formulación  
En esta página se usa el término "formulación", aunque en general lo más correcto sería "nomenclatura"; por mantener enlaces no modifico todo, es un tema de detalle y se asume que se sabe de qué se está hablando. Ver en  [formulación](/home/recursos/quimica/formulacion)  el apartado "Qué y cuando usar: Formular, Formulación, Nombrar, Nomenclatura, Nomenclatura y formulación"

##  <a name="TOC-Apuntes-elaboraci-n-propia"></a> Apuntes formulación elaboración propia
Son muy breves, miniapuntes, hojas guía. Fijo una limitación de máximo 2 caras de folio, por lo que son teoría y no tienen ejemplos o muy pocos, que son imprescindibles.  
También hay recursos de elaboración propia de "pizarras" para explicar la formulación, ver [pizarras por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel/), a veces también accesibles desde apuntes de un nivel en concreto, por ejemplo [apuntes elaboración propia 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso/apuntes-elaboracion-propia-3-eso)  
También es importante tener claro el concepto de  [número de oxidación](/home/recursos/quimica/estados-de-oxidacion)  

Algunos enlaces directos aquí. Los ficheros estaban inicialmente se veían anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia recursos-apuntes-formulacion](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion). Aunque la página se llame de apuntes, también desde aquí se enlazaban algunos ejercicios de elaboración propia.   

**Inorgánica**  
* [Formulación inorgánica (adaptada normas IUPAC 2005): binarios y ternarios simples](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/NomenclaturaInorganicaBinariosTernariosSimples.pdf)  
* [Formulación inorgánica (adaptada normas IUPAC 2005): oxácidos](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/NomenclaturaInorganicaOxacidos.pdf)  
* [Formulación inorgánica (adaptada normas IUPAC 2005): oxisales neutras y sales ácidas](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/NomenclaturaInorganicaOxisalesNeutrasSalesAcidas.pdf)  

**Orgánica**  
* [Formulación orgánica (adaptada normas IUPAC 1993, se citan normas IUPAC 2013): formulación orgánica básica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/FormulacionOrganicaBasica.pdf)  
* [Formulación orgánica (adaptada normas IUPAC 1993, se citan normas IUPAC 2013): formulación orgánica básica (Anexo I: nombres comunes, aromáticos)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/FormulacionOrganicaBasica-AnexoI.pdf)  

Para ejercicios, en 2021 creo ficheros con una recopilación de inorgánica y orgánica de  [EvAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) que puede servir para repasar  

También se pueden ver ejercicios 2021 de Química orgánica realizados para acceso universidad Murcia: Relación mínima de nombres, Soluciones exámenes 2011-2020 y Ejercicios propuestos, además de un fichero de resumen de teoría. 

* [FQ2-HojaFormulacion.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/FQ2-HojaFormulacion.pdf)  
* [FQ1-HojaFormulacionInorganica.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/FQ1-HojaFormulacionInorganica.pdf)  
* [FQ1-HojaFormulacionInorganica-soluc.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/FQ1-HojaFormulacionInorganica-soluc.pdf)  

* [ProblemasQuímica-Orgánica.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/ProblemasQuímica-Orgánica.pdf)  
* [ProblemasQuímica-Orgánica-soluc.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/ProblemasQuímica-Orgánica-soluc.pdf)  
* [AlfabetosNomenclaturaOrganica.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/AlfabetosNomenclaturaOrganica.pdf)  

También algunos materiales sobre dados  
* [DadosFormulacion-Profesor.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/DadosFormulacion-Profesor.pdf)  

##   Apuntes formulación generales, orgánica e inorgánica

   * [Apuntes Formulación y nomenclatura de Química (inorgánica y orgánica) - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/formulacion-nomenclatura-quimica/)  
   En la sección de alcanos incluye este diagrama Hidrocarburos y productos petrolíferos  
   ![](https://fisiquimicamente.com/media/formulacion-nomenclatura-quimica-organica/hidrocarburos-petroleo.svg)      
   * Formulación y Nomenclatura de Química Inorgánica y Orgánica  
   [http://www.alonsoformula.com/](http://www.alonsoformula.com/)  
   Carlos Alonso. Material con licenciamiento no detallado. Incluye actualización inorgánica IUPAC 2005
   * [Formulación y nomenclatura](https://issuu.com/clasesparticularesenmontevideo/docs/formulaci__n_y_nomenclatura_en_qu__)  
   [Formulacion y Nomenclatura en Quimica Inorganica 23-12-05](https://es.scribd.com/document/376987167/Formulacion-y-Nomenclatura-en-Quimica-Inorganica-23-12-05)  
   Apéndice: Formulación y nomenclatura en Química Inorgánica. Resumen de 15 páginas, cuadro resumen de 2 páginas, con 5 páginas de enunciados y soluciones de "Ejercicios de formulación inorgánica propuestos en las Pruebas de Acceso a la Universidad (PAU) del Distrito Universitario de Canarias".  
   [FORM-organica](https://es.scribd.com/document/48732414/FORM-organica)  
   Apéndice: Formulación de química orgánica. Resumen de 18 páginas, cuadro resumen de 1 página, con 6 páginas de enunciados y soluciones de "Ejercicios de formulación orgánica propuestos en las Pruebas de Acceso a la Universidad (PAU). Distrito Universitario de Canarias."
   * Materiales didácticos de formulación orgánica e inorgánica  
   FORMULACIÓN INORGÁNICA no operativo en 2021  
   Materiales didácticos para el estudio de Formulación Inorgánica dirigido a los estudiantes de química de primer curso de la EUITA, realizado por las profesoras María Bejarano Bravo y Mª del Carmen Florido Fernández. 2001/2002  
   FORMULACIÓN ORGÁNICA no operativo en 2021  
   Materiales didácticos para el seminario "Formulación Orgánica" celebrado los días 19 y 20 de mayo de 2003 en la EUITA, dirigido a los estudiantes de química de primer curso, realizado por las profesoras Teresa Grarcía-Muñoz Martínez y Mª Carmen Florido Fernández
   * [Formulación y nomenclatura de Química Orgánica Recomendaciones y nombres preferidos de la IUPAC de 2013, Rodrigo Alcaraz de la Osa](https://rodrigoalcarazdelaosa.me/apuntes-formulacion-nomenclatura-quimica/organica/) 
   * [Blog de Física y Química IES Juan de la Cierva](http://aula44.files.wordpress.com/2009/07/cuaderno-de-formulacic3b3n-y-nomenclatura-quc3admica.pdf)  
   Incluye inorgánica y orgánica, y parece incluir apuntes escaneados de algún libro
   * [Profesor en línea, Nomenclagtura química](http://www.profesorenlinea.cl/Quimica/Nomenclatura_quimica.html) 
   * [Apuntes inorgánica y orgánica. IES Diego Conde Porcelos](http://iesdiegoporcelos.centros.educa.jcyl.es/sitio/index.cgi?wid_item=82&wid_seccion=21) 
   * [Ejercicios maravillosos de formulación orgánica e inorgánica](https://www.academia.edu/45582851/Ejercicios_maravillosos_de_formulaci%C3%B3n_org%C3%A1nica_e_inorg%C3%A1nica)  
   Documento escrito e imaginado por J.F.G.H. (Juan F. González Hernández) bajo Creative Commons. Copiar y distribuir libremente indicando su origen.
   * [Lecciones interactivas de química](https://www3.gobiernodecanarias.org/medusa/ecoescuela/recursosdigitales/2015/03/22/lecciones-interactivas-de-quimica/)
   *  [http://lenguajequimico.blogspot.com.es/](http://lenguajequimico.blogspot.com.es/)  
   En este blog damos las convenciones más recientes de la IUPAC sobre el Lenguaje Químico Inorgánico y Orgánico. Jesús Marcos Segura Martín
   * [!Por las barbas de Darwin! categoría Química Inorgánica](https://porlasbarbasdedarwin.com/category/quimica-inorganica/)  
   Samuel Escudero  
   [QUÍMICA INORGÁNICA [01] – Consideraciones previas](https://porlasbarbasdedarwin.com/2019/01/23/quimica-inorganica-01-consideraciones-previas/)  
   ![](https://lasbarbasdedarwin.files.wordpress.com/2019/02/plbdd_qi_01.png)  
   ![](https://lasbarbasdedarwin.files.wordpress.com/2019/02/plbdd_qi_02.png)  
   [QUÍMICA INORGÁNICA [02] – Nomenclatura compuestos binarios](https://porlasbarbasdedarwin.com/2019/01/30/quimica-inorganica-02-nomeclatura-compuestos-binarios/)  
   ![](https://lasbarbasdedarwin.files.wordpress.com/2019/02/plbdd_qi_03.png)  
   ![](https://lasbarbasdedarwin.files.wordpress.com/2019/02/plbdd_qi_04.png)  
   [QUÍMICA INORGÁNICA [03] – Nomenclatura compuestos ternarios](https://porlasbarbasdedarwin.com/2019/03/20/quimica-inorganica-03-nomeclatura-compuestos-ternarios/)  
   ![](https://lasbarbasdedarwin.files.wordpress.com/2019/05/plbdd_qi_05.png)  
   ![](https://lasbarbasdedarwin.files.wordpress.com/2019/05/plbdd_qi_06.png)  
   [Por las barbas de Darwin! QUÍMICA INORGÁNICA Resumen](https://lasbarbasdedarwin.files.wordpress.com/2021/11/plbdd_formulacion_inorganica_.pdf)  

## Apuntes formulación inorgánica

   * [Diapositivas inorgánica - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/formulacion-nomenclatura-quimica/inorganica/diapositivas/)  
   * [Póster inorgánica - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/formulacion-nomenclatura-quimica/inorganica/formulacion-nomenclatura-inorganica-poster.pdf)  

   * [Proyecto EDIA. Recurso de Física y Química para 3º de Secundaria. “El arte de formular”](https://cedec.intef.es/proyecto-edia-recurso-de-fisica-y-quimica-para-3o-de-secundaria-el-arte-de-formular/)  
   Proyecto / situación de aprendizaje: al final se realiza en grupo un concurso de formulación e individualmente una guía de formulación
   
   * [Resumen de las normas IUPAC 2005 de nomenclatura de Química Inorgánica para su uso en enseñanza secundaria y recomendaciones didácticas - 5 Otros materiales - rseq.org](https://rseq.org/wp-content/uploads/2018/09/5-OtrosMateriales.pdf) incluye "Guía de nomenclatura Univ. Rioja", con anexo en página 44 en la que hay ejercicios. 

	
## Apuntes formulación orgánica

   * Aplicación interactiva que propone test de formulación orgánica y los corrige (flash)  
   [Enlaces a aplicaciones informáticas útiles (flash, scripts, etc). elaboradas por José Antonio Navarro.](http://web.archive.org/web/20180705110131/http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=139&Itemid=199) (waybackmachine)  
   Los enlaces dejan de estar operativos en octubre 2018 y pasan a estar en [fq.iespm.es/](http://fq.iespm.es/), pero luego vuelven a etar no operativos, y al desaparecer el soporte de flash parece que ya no lo vuelven a estar.  
   Como **sí está adaptado normas IUPAC 2005** y se puede ejecutar aunque sea flash, y al ser una aplicaicón interactiva que corrige y  considero muy útil, en 2021 creo copia local en fiquipedia del fichero .swf y del exe de adobe que permite utilizarlo aunque el navegador no lo soporte: los materiales citan la autoría original de José Antonio Navarro, ver en blog del autor  [clasesfisicayquimica.blogspot.com](http://clasesfisicayquimica.blogspot.com/) que en 2021 remite a  [https://fq.iespm.es/](https://fq.iespm.es/), pero ya no están los archivos allí  
- [Test de Formulación Orgánica (4º ESO).](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/flash/TestOrganica4ESO-exe-y-swf.zip)   
- [Test de Formulación Orgánica (Bachillerato).](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/flash/TestOrganica1Bachillerato-exe-y-swf.zip)  
   * [Nomenclatura orgánica - eduvers.org](https://eduvers.org/es/fisica-y-quimica/nomenclatura-organica)  
   * [Diapositivas orgánica - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/formulacion-nomenclatura-quimica/organica/diapositivas/)  
   * [Formulación y nomenclatura de Química Orgánica. Recomendaciones y nombres preferidos de la IUPAC de 2013](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/formulacion-nomenclatura-quimica/organica/)  
   * [Póster orgánica - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/formulacion-nomenclatura-quimica/organica/formulacion-nomenclatura-organica-poster.pdf)  
   * [www.nomenclaturaquimica.com](https://www.nomenclaturaquimica.com/)  
   Web sobre contenidos de nomenclatura química adaptados a las últimas recomendaciones de la IUPAC, con los nombres preferidos en Química Orgánica (PIN)  
   Tiene "app multilingüe de ejercicios sobre nombres preferidos en química orgánica", ew web con menú lateral, por ejemplo alcanos  
   [nqcom-org.netlify.app](https://nqcom-org.netlify.app/index.html)  
   * [Orgánica. IES La Sisla - quifi.es](https://quifi.es/index.php/formulacion-inorganica-bachillerato-11/bloque-0-formulacion-organica). Incluye resumen y exámenes   
   * [Semana de la ciencia 2022. IES Pedro Duque Leganés - fisicayquimicaweb.es](https://fisicayquimicaweb.es/Semana-Ciencia/)  
[Nomenclatura química orgánica. Roberto Alonso](https://fisicayquimicaweb.es/data/documents/FORMULACION/index.html)  
Practicar y jugar, formular y nombrar.  
"PRACTICAR": Test para repasar las distintas familias de compuestos orgánicos  
"JUGAR": Test con preguntas mezcladas y para enviar la puntuación de la tabla de clasificaciones  
   * [Normas IUPAC de formulación y nomenclatura química Efraim Reyes, Garikoitz Beobide, Uxue Uria, Oscar Castillo, Luisa Carrillo, Sonia Pérez-Yáñez, Javier Cepeda, Liher Prieto y Jose L. Vicario](https://web-argitalpena.adm.ehu.es/listaproductos.asp?IdProducts=UCF00213106)  
   Páginas: 358, Fecha de edición: 2021, ISBN/ISSN: 978-84-1319-310-6  
   [SOLUCIONARIO PDF. Normas IUPAC de formulación y nomenclatura química. Efraim Reyes, Garikoitz Beobide, Uxue Uria, Oscar Castillo, Luisa Carrillo, Sonia Pérez-Yáñez, Javier Cepeda, Liher Prieto y Jose L. Vicario](https://web-argitalpena.adm.ehu.es/bxnn/UCPDF22EBAZ.pdf)  
   * [Formulación y nomenclatura de Química Orgánica - aprendiendo física con Berto Tomás](https://docs.google.com/document/d/1RV1fCLFnO_TVD4Om59ooWrVxHadEqc-7/edit)  
   * [Introducción a la Nomenclatura IUPAC de Compuestos Orgánicos](http://docentes.umss.edu.bo/Bioquimica/jquiroga/temas/nomenclatura/iupac-nomenclatura-organica.pdf)  
   (36 páginas) "En este documento seguiremos las recomendaciones de 1979 que son las más ampliamente adoptadas en los libros de texto actuales. ". © Dr. Eugenio Alvarado, Escuela de Química, Universidad de Costa Rica, Agosto 22 , 2000. Incluye bastantes ejemplos/ejercicios al final con soluciones
   * [Química orgánica, quimife](http://quimife.wordpress.com/quimica-organica/)  
   [Resumen orgánica.pdf, quimife](http://quimife.files.wordpress.com/2010/04/resumen-organica.pdf)  
   Licenciamiento no detallado  
   * [Formulación y Nomenclatura Química Orgánica ](http://chopo.pntic.mec.es/jmillan/for_org.pdf)  
   Departamento de Física y Química IES “Rey Fernando VI”. © diciembre de 2012 Jesús Millán Crespo
   * Apuntes Química Orgánica I. TEMA 2.- Tipos de compuestos orgánicos. Tipos de compuestos y clasificación. Compuestos acíclicos, cíclicos y policíclicos. Grupos funcionales. Reglas básicas de nomenclatura.  
   [Apuntes Química Orgánica I. Tema 2](http://web.usal.es/~frena/MoberlyQFS/documents/tema2.pdf)  
   Fernando Tomé Escribano. Universidad de Salamanca
   * [Apuntes química orgánica IES Rafael Puga Ramón](http://www.iespugaramon.com/ies-puga-ramon/resources/formorganica1258217062345.pdf;jsessio) 
   * Ejercicios orgánica  
   [Ejercicios de formulación Química Orgánica, Alcanos (III)](http://www.100ciaquimica.net/fororg/ejer/mhidro3.htm) 
   * QUÍMICA INDUSTRIAL Brevísima guía de nomenclatura de compuestos orgánicos  
   [http://www.unlu.edu.ar/~qui10192/qi0020301.htm](http://www.unlu.edu.ar/~qui10192/qi0020301.htm)  
   Universidad Nacional de Luján, Dto. de Ciencias Básicas
   * Alcoholes, Aldehídos y cetonas  
   [http://equipo5quimica607.blogspot.com.es/](http://equipo5quimica607.blogspot.com.es/)  
   Universidad Nacional Autónoma de México Escuela Nacional Preparatoria Equipo 5 (septiembre 2012)
   * Enlaces a ejemplos, ejercicios, apuntes  
   [ http://html.rincondelvago.com/compuestos-organicos.html](http://html.rincondelvago.com/compuestos-organicos.html) 
   * Blog con apuntes química orgánica  
   [nomenclaturandoando](http://nomenclaturandoando.wordpress.com/) 
   * [Tabla prioridades química orgánica, vaxasoftware](http://www.vaxasoftware.com/doc_edu/qui/tpforga.pdf) 
   * [IES Los Olmos, Formulación orgánica 1º Bachillerato  ](http://edu.jccm.es/ies/losolmos/FISICAYQUIMICA/1BACHILLER/FORMULACI%C3%93NORGANICA1bh.pdf) 
   * [Nomenclatura Orgánica (pdf) - romanfyq](http://romanfyq.wikispaces.com/file/view/Nomenclatura_Org%C3%A1nica.pdf)  
   Licenciamiento cc-by-sa
   * [Universidad Politécnica de Madrid. Apoyo para la preparación de los estudios de ingeniería y arquitectura. TEMA 1 NOMENCLATURA QUÍMICA ORGÁNICA (II). GRUPOS FUNCIONALES](http://ocw.upm.es/apoyo-para-la-preparacion-de-los-estudios-de-ingenieria-y-arquitectura/quimica-preparacion-para-la-universidad/contenidods/Material_de_clase/Tema1/tema_1_nomenclatura_organica_ii_grupos_funcionales_.pptm) 
   * [IES Anamaría Matute, apuntes y ejercicios formulación orgánica ](http://www.iesanamariamatute.com/departamentos/dpto_fisica_quimica/fisica_apuntes_ejercicios.asp) 
   * [IES Sardineira. Nomenclatura orgánica](http://centros.edu.xunta.es/iesasardineira/web_CS/qo/nomenclatura/nomenorgan/nomorgan.php)  
   Apuntes por grupos: Hidrocarburos, hologenados, oxigenados, nitrogenados (trata aminas no primarias), con azufre, organometálicos, y Ejercicios  
   © Mariano Pazos Afonso. Curso 2009-10 
   *  [Profesor en línea, radicales](http://www.profesorenlinea.cl/Quimica/Radicales.html)  
   Se comenta para radicales "ilideno"  
   * [Por las barbas de Darwin! QUÍMICA ORGÁNICA Resumen](https://lasbarbasdedarwin.files.wordpress.com/2021/11/plbdd_formulacion_organica.pdf)  
   *  ["La Química Orgánica Transparente" LECCIONES. Departamento química orgánica UAM. ](http://www.uam.es/departamentos/ciencias/qorg/docencia_red/qo/l00/lecc.html) 
   *  [Ejercicios orgánica Departamento de Física y Química del IES Teror, Gran Canaria.](http://iesteror.wordpress.com/2010/02/25/ejercicios-de-formulacion-organica/)  
   [Más ejercicios orgánica Departamento de Física y Química del IES Teror, Gran Canaria.](http://iesteror.wordpress.com/2010/03/07/mas-ejercicios-de-formulacion-organica/)  
   [Formulación orgánica ejercicios con solución, Departamento de Física y Química del IES Teror, Gran Canaria.](http://iesteror.files.wordpress.com/2010/03/formulacion-organica-ejercicios-con-solucion.pdf) 
   *  [Nomenclatura y formulación](http://www.quimicaorganica.net/nomenclatura-y-formulacion.html)  
   Apuntes básicos Copyright © 2013, Química Orgánica.
   *  [Formulación química orgánica IES Padre Majón](http://fisicaquimica.iespadremanjon.es/index.php/2013-04-25-17-35-45/finish/79-documentos-1-bachillerato/472-formulacion-quimica-organica)  
   Departamento física y química IES Padre Manjón
   * [NOMENCLATURA DE COMPUESTOS ORGANICOS Parte II: Ácidos Carboxílicos y sus derivados, Aldehídos y Cetonas, Alcoholes, Aminas, Prioridades. Laboratorio de Química Orgánica. Facultad de Ciencias. 2004](http://deymerg.files.wordpress.com/2011/03/nomenclatura-compuestos-orgc3a1nicos.pdf)  
   17 páginas, tabla jerarquías en página 16
   *  [Formulación orgánica IES Diego Porcelos](http://iesdiegoporcelos.centros.educa.jcyl.es/sitio/upload/formulacion_organica_para_la_pagina_web.pdf)  
   56 páginas en total. Adaptado normas 1993, página 7 compara 1979 y 1993
   *  [FORMULACIÓN Y NOMENCLATURA ORGÁNICA. 4º ESO Y 1º Y 2º BACHILLERATO. DEP. FÍSICA Y QUÍMICA. I.E.S. “JUAN DE ARÉJULA”. LUCENA (CÓRDOBA)](http://www.iesjuandearejula.com/descargas/actividades/165.pdf)  
   52 páginas, no adaptado normas 1993
   * [Formulación orgánica, Pablo Osorio](http://www.posorioquimica.blogspot.com.es/p/formulacion-organica_16.html)  
   [Formulación Orgánica.pdf](https://www.dropbox.com/s/48ydob86qaqktnh/1.%20Formulaci%C3%B3n%20Org%C3%A1nica.pdf)  
   Pablo Osorio. *Aviso: Los temas elaborados por P. Osorio son difundidos públicamente a través de la cuenta de Twitter @QuimicaPau siendo una herramienta para ayudar a los estudiantes, teniendo íntegramente su autoría y de tal modo consta, de manera que no se consentirá bajo ningún concepto el plagio y/o distribución sin consentimiento del propietario. 26/Enero/2014*  
   Junio 2014 sí sigue normas IUPAC 1993
   * QUÍMICA DEL CARBONO. Autora: Mª José Morcillo Ortega. Curso 0 Química UNED, OCW  
   [http://ocw.innova.uned.es/quimicas/](http://ocw.innova.uned.es/quimicas/)  
   Ficha 12 Compuestos del Carbono. Su representación  
   Ficha 13 Hidrocarburos  
   Ficha 14 Isomería  
   Ficha 15 Principales funciones orgánicas  
   Ficha 16 Formulación y nomenclatura orgánica  
   Ficha 17 Principales Tipos de Reacciones Orgánicas  
   Septiembre 2014 no sigue totalmente normas IUPAC 1993: 2-hexanona en lugar de hexan-2-ona
   *  [http://www.quimicaorganica.org/](http://www.quimicaorganica.org/) 
    Germán Fernández, incluye app para android
   * [Graphic Design & Organic Chemistry: a visual approach by Brian Zhao A nonprofit chemistry book that teaches you to think using design and narrative.](https://www.kickstarter.com/projects/384723486/graphic-design-and-organic-chemistry-a-free-colleg/)  
   *  [Molecularfigures2.doc](http://www.nmr2.buffalo.edu/resources/humor/matr/Molecularfigures2.doc) 
   *  [branched-enynenynols](https://anthonycrasto.wordpress.com/2013/10/29/branched-enynenynols/)  
   “Old MacDonald Named a Compound: Branched Enynenynols” that was originally published in the J. Chem. Ed. 74 (1997) 782, about what would happen if ‘Old MacDonald’ were a chemist, and made molecules that have the shapes of animals.  
   [Old MacDonald Named a Compound: Branched Enynenynols](http://pubs.acs.org/doi/abs/10.1021/ed074p782)  
   Dennis Ryan Department of Chemistry, Hofstra University, Hempstead, NY 11590 J. Chem. Educ., 1997, 74 (7), p 782 DOI: 10.1021/ed074p782 Publication Date (Web): July 1, 1997 DOI: 10.1021/ed074p782  
   [Old MacDonald Named a Compound: Branched Enynenynols](http://www.chm.bris.ac.uk/sillymolecules/JCE74_p782.pdf)  
   [Small world - futilitycloset](https://www.futilitycloset.com/2020/02/11/small-world-13/) 
   * Nanoputianos  
   [NanoPutian - Wikipedia](https://en.wikipedia.org/wiki/NanoPutian)  
   [Small Business - futilitycloset](https://www.futilitycloset.com/2014/08/30/small-business/) 
   *  [ORGANIC CONVENTIONS](http://www.chemguide.co.uk/basicorg/convmenu.html#top)  
   Understanding Chemistry  
   ORGANIC CONVENTIONS (How to draw organic molecules, An introduction to naming organic molecules, More organic names, Naming aromatic compounds, The use of curly arrows) © Jim Clark 2000 (modified slightly 2008)
   *  [Química Orgánica I (1311), Unidad 2 Nomenclatura de Compuestos Orgánicos](http://organica1.org/rafael/nomenclatura.pdf)  
   M. en C. Israel Velázquez Martínez M. en C. Alicia Hernández Campos Dr. Rafael Castillo Bocanegra
   *  [Formulación y nomenclatura en Química Orgánica - ejercicios-fyq.com](https://ejercicios-fyq.com/Formulacion_organica/index.html)  
   Incluye ejercicios  
   [35 ejercicios de formulación y nomenclatura orgánica](https://ejercicios-fyq.com/Formulacion_organica/35_ejercicios_de_formulacin_y_nomenclatura_orgnica.html) 
   *  [US Química orgánica ejercicios resueltos](https://www.studocu.com/es/document/universidad-de-sevilla/quimica-organica-i/ejercicios-obligatorios/ejercicios-resueltos/352004/view) 
   * [UPO guía nomenclatura_organica_2014x1x.pdf](https://www.upo.es/cms1/export/sites/upo/ponencia_quimica/noticias/documentos/guia_nomenclatura_organica_2014x1x.pdf)  

   
   * [Formulación esqueletal en orgánica, ausetute](https://www.ausetute.com.au/skeletal.html)  
   A skeletal formula does NOT explicitly show  
   ⚛ carbon atoms  
   ⚛ hydrogen atoms attached directly to carbon atoms  
   ⚛ bonds between carbon atoms and hydrogen atoms  
   A skeletal formula does explicitly show  
   ⚛ bonds between carbon atoms  
   ⚛ bonds to functional groups  
   ⚛ functional groups

   * [Manual de nomenclatura orgánica. Universidad Nacional Autónoma de México, INFOCAB PB200617](https://drive.google.com/file/d/1QEXksZmpvouZdXLsagchSCkD_xtslvh0/view) pdf 513 páginas  

   * [Estructuras desarrolladas, semidesarrolladas, esqueletal y 3D - chemdo.org](https://chemdo.org/a-level-home/alkanes/)  

   * [Química orgánica - eltamiz](https://eltamiz.com/elcedazo/series/quimica-organica/)  
   Conceptos generales y formulación   
   * [[Química Orgànica] VII-Compuestos Polifuncionales - eltamiz](https://eltamiz.com/elcedazo/2016/01/23/quimica-organica-vii-compuestos-polifuncionales/)  
   

### Ejercicios
Algunos mezclados con apuntes, así que pueden no citarse aquí pero sí en sección apuntes  
[twitter onio72/status/1459440324594610177](https://twitter.com/onio72/status/1459440324594610177)    
204 preguntas de formulación (elección múltiple) de sales ternarias para importar a Moodle  

[Formulación inorgánica y orgánica. 1 Bachillerato. M. Magdalena Homar Pons. Aprendiendo Física con Berto Tomás](https://drive.google.com/file/d/1ujM1yemLauwmGZMgBjF5mciEySM5PLw1/view)  

[Bancos de preguntas Moodle nomenclatura - Enrique García de Bustos Sierra](https://www.educa2.madrid.org/web/enrique.garciadebustos/lafisicaesfacil/recursos/moodle)  

[HIDROCARBUROS | 4.º ESO EJERCICIOS NOMENCLATURA. ALBA LÓPEZ VALENZUELA](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/formulacion-nomenclatura-quimica/organica/hidrocarburos-4eso-ejercicios.pdf)  
[QUÍMICA ORGÁNICA: GRUPOS FUNCIONALES | GRADOS UNIVERSITARIOS. ALBA LÓPEZ VALENZUELA](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/formulacion-nomenclatura-quimica/organica/grupos-funcionales-ejercicios.pdf)  


[twitter alfredo24404099/status/1624322793201364992](https://twitter.com/alfredo24404099/status/1624322793201364992)  
Buenas.... por aquí os dejo un nuevo Kahoot para 3º eso, de práctica y repaso de FORMULACION INORGANICA BINARIA. Contiene 100 preguntas, algunas estresantes... por si os ayuda.  
[FORM INORG _ BINARIA](https://create.kahoot.it/share/form-inorg-binaria/2022078e-1d24-4a74-a654-b4c4d6d6b73e)  

[FQ3 Formulación: Identificar sustancias por su fórmula - geogebra](https://www.geogebra.org/m/tbxzysej)  
[FQ3 Formulación: Identificar sustancias por su fórmula (II) (puntúa)- geogebra](https://www.geogebra.org/m/ycmwwyn7)  

[twitter currofisico/status/1620874835554607107](https://twitter.com/currofisico/status/1620874835554607107)  
Comparto esta relación, en formato visu, de 316 compuestos de química del carbono que han sido propuestos en las cuestiones de formulación química en las pruebas de acceso a la Universidad de Andalucía (periodo de 2008 a 2022).  
[VISU_FORMULACION_ORGÁNICA.pdf](https://drive.google.com/file/d/1fcCGijBYM9UE51EI2AMWZvnru8IPQwnO/view)  

[Naming compounds Walk Around](http://passionatelycurioussci.weebly.com/google-form-walk-arounds.html#compounds)  

[Naming Organic Compounds - chemistry.msu.edu](https://www2.chemistry.msu.edu/faculty/reusch/virttxtjml/nomen1.htm)  
Sección "Practice Problems. Seven questions concerning nomenclature are presented here."  

[QUÍMICA ORGÁNICA. 250 EJERCICIOS RESUELTOS. CECILIO MÁRQUEZ SALAMANCA. UNIVERSIDAD DE ALICANTE, 2008 - ua.es](https://rua.ua.es/dspace/bitstream/10045/6984/7/LIBRO1_1.pdf)  

[EJEMPLOS DE FORMULACIÓN Y NOMENCLATURA ORGÁNICA - lapalmafq2](https://lapalmafq2.weebly.com/uploads/3/7/5/3/37532953/ejemplosformulas.pdf)  Página IES La Palma  

[Formulación IES La Palma](https://lapalmafq2.weebly.com/formulacioacuten.html)  

###   Apps sobre formulación (inorgánica / orgánica)  
Inicialmente ponía apps aquí, pero decido poner todas las [apps de química](/home/recursos/quimica/apps-quimica)  juntas en una única página, y crear allí categorías. Hay apps que tienen más cosas que formulación o combinar varias cosas, como puede ser formulación y mecanismos de orgánica.

###   Apuntes formulación inorgánica


####   Guía Nomenclatura de Química Inorgánica para estudiantes de secundaria y bachillerato (Real Sociedad Española de Química)
Real Sociedad Española de Química creó un Grupo de Trabajo para la elaboración de una Guía Nomenclatura de Química Inorgánica para estudiantes de secundaria y bachillerato en noviembre de 2015, y se publicaron materiales en mayo 2016. Intervienen varias personas, entre ellas Salvador Olivares que se cita aquí. Los materiales los referencio desde la página de apuntes de formulación  

[Resumen de las normas IUPAC 2005 de nomenclatura de Química Inorgánica para su uso en enseñanza secundaria y recomendaciones didácticas](https://rseq.org/mat-didacticos/resumen-de-las-normas-iupac-2005-de-nomenclatura-de-quimica-inorganica-para-su-uso-en-ensenanza-secundaria-y-recomendaciones-didacticas/)

####  [Guía Breve para la Nomenclatura de Química Inorgánica (Real Sociedad Española de Química)](http://rseq.org/material-didactico/item/858-gu%C3%ADa-breve-para-la-nomenclatura-de-qu%C3%ADmica-inorg%C3%A1nica) 

####   Apuntes generales inorgánica

   * [Formulación en FisQuiWeb](http://fisquiweb.es/Formulacion/index.htm)  
   [Apuntes 3º ESO, 4º ESO, 1º Bachillerato](http://fisquiweb.es/Apuntes/Formulacion/formulacionB.htm)  
   Nomenclatura de Química Inorgánica. Recomendaciones de la **IUPAC de 2005**. IES Juan A. Suanzes. Avilés. Asturias. Licenciamiento no detallado., Diseño, mantenimiento y contenidos: Luis Ignacio García González  
   * [Nomenclatura inorgánica - eduvers.org](https://eduvers.org/es/fisica-y-quimica/nomenclatura-inorganica)
   * [Formulación y nomenclatura IUPAC 2005 - lamanzanadenewton](https://www.lamanzanadenewton.com/materiales/aplicaciones/lfq2/lmn_IUPAC2005.html)  
    Aplicación didáctica para el aprendizaje de las reglas de formulación y nomenclatura  en Química Inorgánica, actualizada según las recomendaciones 2005 de la IUPAC. Incluye actividades interactivas y pruebas de autoevaluación.   
    [Ficha didáctica Formulación y nomenclatura IUPAC 2005 - lamanzanadenewton](https://www.lamanzanadenewton.com/materiales/quimica/lmn_infoformulacion.html)  
   * [Visualizaciones en Química, nomenclatura: ejercicios online 2,3,4 ESO y 1º Bachillerato](https://www.uv.es/quimicajmol/nomenclatura/index.htm) 
   * [NOMENCLATURA QUÍMICA INORGÁNICA SEGÚN RECOMENDACIONES IUPAC 2005, DEPARTAMENTO DE FÍSICA Y QUÍMICA IES REY FERNANDO VI](http://chopo.pntic.mec.es/jmillan/for_inor_yago.pdf)  
   @ Jesús Millán Crespo
   * TEST DE FORMULACIÓN INORGÁNICA. Bachillerato. Ejercicios con autorrección  
   [Enlaces a aplicaciones informáticas útiles (flash, scripts, etc). elaboradas por José Antonio Navarro.](http://web.archive.org/web/20180705110131/http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=139&Itemid=199) (waybackmachine)  
   Los enlaces dejan de estar operativos en octubre 2018 y pasan a estar en [fq.iespm.es/](http://fq.iespm.es/), pero luego vuelven a etar no operativos, y al desaparecer el soporte de flash parece que ya no lo vuelven a estar.  
   Como **sí está adaptado normas IUPAC 2005** y se puede ejecutar aunque sea flash, y al ser una aplicaicón interactiva que corrige y  considero muy útil, en 2021 creo copia local en fiquipedia del fichero .swf y del exe de adobe que permite utilizarlo aunque el navegador no lo soporte: los materiales citan la autoría original de José Antonio Navarro, ver en blog del autor  [clasesfisicayquimica.blogspot.com](http://clasesfisicayquimica.blogspot.com/) que en 2021 remite a  [https://fq.iespm.es/](https://fq.iespm.es/), pero ya no están los archivos allí  
   - [Test de Formulación (2º ESO). Aplicación en Flash que propone ejercicios de formulación y los corrige. Nivel: 2º ESO. Compuestos binarios. Nomenclaturas: Sistematica.](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/flash/TestInorganica2ESO-exe-y-swf.zip)  
   - [Test de Formulación (3º ESO). Aplicación en Flash que propone ejercicios de formulación y los corrige. Nivel: 3º ESO. Compuestos binarios.](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/flash/TestInorganica3ESO-exe-y-swf.zip)  
   - [Test de Formulación Inorgánica (4º ESO). Aplicación en Flash que propone ejercicios de formulación inorgánica y los corrige. Nivel: 4º ESO. Hasta sales oxoácidas. ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/flash/TestInorganica4ESO-exe-y-swf.zip)  
   - [Test de Formulación Inorgánica (Bahillerato). Aplicación en Flash que propone ejercicios de formulación inorgánica y los corrige. Nivel: Bachillerato. Incluye iones y sales ácidas.](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/flash/TestInorganica1Bachillerato-exe-y-swf.zip)  
   Este programa tiene algunas limitaciones/errores:  
   - Nomenclatura nº oxidación (Stock): intenta asignar nombre cuando el oxígeno está a la izquierda, y no existe en Stock, así que "falla" indicando nombre solo " de "  
   - Nomenclatura nº oxidación (Stock): obliga a omitir el número de oxidación en romanos si solo hay un valor positivo: realmente omitir es opcional  
   - Nomenclatura nº oxidación (Stock): para H2O obliga a poner agua, no acepta óxido de hidrógeno que sería válido.  
   - Si se comienza por letra mayúscula a veces lo considera incorrecto, pero comenzar por minúscula no es obligatorio   
   - A veces da erróneos nombres cuando se aceptan varios. Por ejemplo son válidos telurio y teluro pero solo acepta teluro. Similar con zinc/cinc, yodo/iodo.     
   * [Introducción a la Formulación de Química Inorgánica, 100ciaquimica](http://www.100ciaquimica.net/forin/index.htm)  
   Material con copyright. Hoja de cálculo con más de 4000 ejercicios
   * [Apuntes formulación Inorgánica fisicayquimicamaite](http://fisicayquimicamaite.blogspot.com.es/2011/02/apuntes-formulacion-inorganica.html) Bastante completos
   * [Formulación y Nomenclatura. Química Inorgánica](http://www.eis.uva.es/%7Eqgintro/nomen/nomen.html)  (validado noviembre 2011) Teoría básica con ejemplos y ejercicios.
   * [Resumen de nomenclatura de química inorgánica. IES Dolmen de Soto.](http://iesdolmendesoto.org/wiki/images/0/02/Formulacion_inorganica_final_11.pdf)  
   Licenciamiento no detallado. Incluye apartados con título "Nomenclatura anterior a las recomendaciones de la IUPAC 2005"
   *  [Formulación y Nomenclatura de química inorgánica. IES Ruiz Gijón ](http://fisicayquimica.iesruizgijon.es/FisyQui1%C2%BABach/Formulacion_Inorganica_2005.pdf)  
   Licenciamiento no detallado. Muy similares a IES Dolmen de Soto. Incluye apartados con título "Nomenclatura anterior a las recomendaciones de la IUPAC 2005"
   * [Ejercicios fyq Formulación y nomenclatura](http://ejercicios-fyq.com/Formulacion_Inorganica/index.html)  Incluye actualización inorgánica IUPAC 2005  
   * Apuntes de formulación inorgánica 1º Bachillerato. Adaptado IUPAC 1990. 2011-2012 (30 páginas)  
   [Apuntes de formulación inorgánica 1º Bachillerato. Adaptado IUPAC 1990. 2011-2012 (30 páginas)](http://files.aprendefisicayquimica.com/200000128-710e872770/APUNTES%20FORMULACION%20INORGANICA%201%20BACH.pdf)  
   ARÁNZAZU GONZÁLEZ MÁRMOL. IES ORDEN DE SANTIAGO DPTO. FÍSICA Y QUÍMICA. Horcajo de Santiago (Cuenca)
   * [Formulación de química inorgánica interactiva, latizavirtual](http://www.latizavirtual.org/quimica/quim_ino.html)  
   J.Carlos Espinosa y José Manuel Romero. Material con licenciamiento no detallado. Incluye Teoría, Actividades, Evaluación. No adaptado a normas IUPAC 2005
   *  [http://quimife.wordpress.com/quimica-inorganica/](http://quimife.wordpress.com/quimica-inorganica/)  
   Licenciamiento no detallado
   * Tabla valencias y apuntes de formulación. No adaptados a normas IUPAC 2005  
   [Formulacion_Inorganica.pdf](http://www.juntadeandalucia.es/averroes/ipfacadiz/FISQUI/archivos/pdf/SoluQUI2/Formulacion_Inorganica.pdf) 
   * Combinaciones binarias. No adaptados a normas IUPAC 2005  
   [Combinaciones binarias](http://www.educared.org/global/anavegar3/premiados/ganadores/b/1046/home.htm)   
   Coordinador: Ignacio Gomara
   * [ESQUEMA GENERAL DE LA NOMENCLATURA INORGÁNICAdro.htm](http://www.acienciasgalilei.com/qui/formulacion-cuadro.htm) 
   * [Química Inorgánica. Universidad de A Coruña](http://www.udc.es/dep/qf/es/qidocencia.html)  
   * IES Ntra. Sra. de la Almudena, Madrid, Departamento de Física y Química. Normas IUPAC 2005 Simplificadas (4 páginas)  
   [IES Ntra. Sra. de la Almudena, Madrid, Departamento de Física y Química. Normas IUPAC 2005 Simplificadas (4 páginas)](http://almudenafyq.webcindario.com/Recursos/1BachFyQ/1BachFyQFormulacionInor2.pdf) 
   * [Edebé. Física y Química 1º Bachillerato. Formulación química. Actualización 2012](http://www.edebe.com/educacion/clicks.asp?CodArt=8590&CodDocu=26587&ruta=/educacion/documentos/8590-16-34-112034_Adenda_LA_FQ_1_BCH_CAS.pdf&flg=N)  
   Pdf descargable de la web de Edebé con la actualización de formulación, realizando en 2012 y que incluye normas 2005 (ver página 16 del pdf) © grupo edebé. Depósito Legal: B. 22902-2012
   * [Formulación inorgánica, IES Padre Manjón](http://fisicaquimica.iespadremanjon.es/index.php/2013-04-25-17-35-45/viewdownload/79-documentos-1-bachillerato/455-formulacion-inorganica) 
   *  [Formulacioń inorgánica IES Diego Porcelos](http://iesdiegoporcelos.centros.educa.jcyl.es/sitio/upload/formulacion_inorganica_para_la_web.pdf)  
   (No adaptados a IUPAC 2005)
   * [Formulación inorgánica Pablo Osorio](http://www.posorioquimica.blogspot.com.es/p/formulacion-inorganica_16.html)  
   [Formulación Inorgánica.pdf](https://www.dropbox.com/s/qfoxl1bodwq4hqq/2.%20Formulaci%C3%B3n%20Inorg%C3%A1nica.pdf)  
   Pablo Osorio. *Aviso: Los temas elaborados por P. Osorio son difundidos públicamente a través de la cuenta de Twitter @QuimicaPau siendo una herramienta para ayudar a los estudiantes, teniendo íntegramente su autoría y de tal modo consta, de manera que no se consentirá bajo ningún concepto el plagio y/o distribución sin consentimiento del propietario. 26/Enero/2014*  
   Junio 2014 no sigue totalmente normas IUPAC 2005: por ejemplo considera óxido de halógeno Cl2O
   * *FORMULACIÓN QUÍMICA INORGÁNICA. Autora: Mª Isabel Gómez del Río. Curso 0 Química UNED, OCW*  
   [http://ocw.innova.uned.es/quimicas/](http://ocw.innova.uned.es/quimicas/)  
   Ficha 8 Conceptos básicos  
   Ficha 9 Compuestos binarios  
   Ficha 10 Compuestos ternarios  
   Ficha 11 Compuestos cuaternarios  
   Septiembre 2014 no sigue totalmente normas IUPAC 2005: óxidos de halógenos y terminaciones -oso -ico para estados oxidación metales
   * *Formulació i nomenclatura de química inorgànica*  
   [http://clic.xtec.cat/db/act_es.jsp?id=3141](http://clic.xtec.cat/db/act_es.jsp?id=3141)  
   Esteve Hernández Gasió 14/09/05 , cc-by-nc-sa (Catalán, no actualizado con normas IUPAC 2005)
   * [Formulación de compuestos binarios (IUPAC)](http://www.iesfuentelucena.org/menus/departamentos%20didacticos/Fisicayquimica/PDF/Formulacioncompuestosbinariosiupac.pdf) 
   * [FQ1BACH Formulación Tipo de sustancia. Antonio González García - geogebra](https://www.geogebra.org/m/jmqfvddm)  
   * [FQ1BACH Formulación Tipo de sustancia (puntúa). Antonio González García - geogebra](https://www.geogebra.org/m/jzhjudjx)  
   * [FQ1BACH T0 Ayuda para nombrar oxoácidos. Antonio González García - geogebra](https://www.geogebra.org/m/xtsjc5ag)  
   * [FQ1BACH T0 Ayuda para formular oxoácidos. Antonio González García - geogebra](https://www.geogebra.org/m/rxvcvwtt)  

##   Juegos formulación
 A veces ciertos tests se planean como juegos  
 [Compuestos binarios, cerebriti](https://www.cerebriti.com/juegos-de-ciencias/compuestos-binarios#.W6gFnBhoTeQ)  
 
 Una opción es hacer piezas con goma eva / cartón con salientes / entrantes según los números de oxidación y luego ver qué compuestos son posibles.  
 Una imagen ilustrativa en la II Feria de la Ciencia en Rivas IES Las Lagunas 2018  
 [twitter FiQuiPedia/status/985099183836233729](https://twitter.com/FiQuiPedia/status/985099183836233729)  
 [![](https://pbs.twimg.com/media/DavGZvPWkAUj1mU.jpg "") ](https://pbs.twimg.com/media/DavGZvPWkAUj1mU.jpg)  
 
 [twitter pdQuimica/status/1050162141054033920](https://twitter.com/pdQuimica/status/1050162141054033920)  
 ¿Habéis jugado ya a la ruleta de los compuestos químicos? Maravillosa idea de aprender a formular. #formulación #química #ciencia #juego #gamificación #aprenderjugando  
 [![](https://pbs.twimg.com/media/DpLs5UcXcAQoOdl.jpg "") ](https://pbs.twimg.com/media/DpLs5UcXcAQoOdl.jpg)   
 Comentario: NO es correcto según normas IUPAC 2005 inorgánica llamar a HCO<sub>3</sub><sup>1-</sup> bicarbonato ni a HSO<sub>4</sub><sup>1-</sup> bisulfato.  
 
[!Por las barbas de Darwin! QUÍMICA INORGÁNICA – Ruletas de formulación](https://porlasbarbasdedarwin.com/2019/04/15/quimica-inorganica-ruletas-de-formulacion/)  Samuel Escudero  
![](https://lasbarbasdedarwin.files.wordpress.com/2020/01/plbdd_ruleta_quc3admicainorgc3a1nica_1.png)  
![](https://lasbarbasdedarwin.files.wordpress.com/2020/01/plbdd_ruleta_quc3admicainorgc3a1nica_2.png)  
 
 Visto con dados, en thingiverse hay más cosas  
 [https://www.thingiverse.com/thing:2788763](https://www.thingiverse.com/thing:2788763)  
 [![](https://cdn.thingiverse.com/renders/b3/9b/08/09/78/4ba300b4edb5ba116fbf64408aa55bab_preview_featured.JPG "") ](https://cdn.thingiverse.com/renders/b3/9b/08/09/78/4ba300b4edb5ba116fbf64408aa55bab_preview_featured.JPG)  
  
 [Juguemos a la ruleta y aprendamos el lenguaje químico](https://pledocienciaseberon.wordpress.com/2018/01/10/gamificando-en-las-clases-de-quimica/)  
 
 Se pueden buscar y encontrar recopilaciones de ideas sobre juegos  
 [LA EDUCACIÓN CIENTÍFICA HOY JUEGOS EDUCATIVOS. FyQ FORMULACIÓN](http://w3.recursostic.edu.es/newton/apls/juegos/images/juegos/publicaciones/eureka.pdf)  
 Rev. Eureka Enseñ. Divul. Cien., 2010, 7(2), pp. 559-565  
 Jesús Manuel Muñoz Calle Profesor de Física y Química del I.E.S. Fuente Juncal de Aljaraque (Huelva).  
 
Pulso 2008, 31. 197-217  
José Joaquín Esteve Castell** 
[Formulación química inorgánica en educación secundaria (Póquer de química)](https://dialnet.unirioja.es/descarga/articulo/2750865.pdf)  

[Juegos didácticos para la enseñanza de la nomenclatura y notación química de las sustancias inorgánicas, Angela Goulet Gorguet](http://www.eumed.net/libros-gratis/2013a/1287/1287.pdf)    
Editado por la Fundación Universitaria Andaluza Inca Garcilaso para eumed.net  
Derechos de autor protegidos. Solo se permite la impresión y copia de este texto para uso personal y/o académico. 
Este libro puede obtenerse gratis solamente desde  [Eudmed.net](https://www.eumed.net/libros-gratis/index.htm?/libros-gratis/index.html)  Cualquier otra copia de este texto en Internet es ilegal.  

[Temas para la educación. Nº 11 noviembre 2010, ISSN: 1989-4023 DIDÁCTICA DE LA QUÍMICA A TRAVÉS DE LOS JUEGOS](https://www.feandalucia.ccoo.es/andalucia/docu/p5sd7639.pdf)  

[twitter XiscoSolera/status/1103975047323156480](https://twitter.com/XiscoSolera/status/1103975047323156480) Avui els nostres alumnes de 3r d'ESO de @CSantAntoniAbat han començat a jugar al Dobble per aprendre els símbols i les valències dels elements químics. Gràcies @gogos20 per ensenyar-nos aquesta eina, s'ho han passat molt bé. @susimenso #gamificació @gamificatuaula  
[![](https://pbs.twimg.com/media/D1IbbpFWoAUE5HU.jpg "") ](https://pbs.twimg.com/media/D1IbbpFWoAUE5HU.jpg) pdf  
[Cartes Valències.pdf](https://drive.google.com/file/d/11lcAAChd7zhalXnPpMdv7Z-oYmQQBW_E/view)  

[twitter fruzgz/status/1308543077917876224](https://twitter.com/fruzgz/status/1308543077917876224)  
Comparto con #claustrovirtual una adaptación del trabajazo de @ProfaDeQuimica con sus dados químicos, en formato virtual con ruletas de cationes y aniones...a formular! https://cutt.ly/lfXLCLJ @pablofcayqca @juanfisicahr @fisquiris @fqsaja1 @cienciasag @NoeliaSandiego @alcarazr  
[ruleta formula inorg1.pdf](https://drive.google.com/file/d/1vPitWd7aQCZsb4KB6lRQiPtKQupOODHV/view) 

[Formula con Marvin. Aprende a nombrar y formular los compuestos orgánicos básicos. Pablo Ortega Rodríguez](https://fisiquimicamente.com/recursos-fisica-quimica/juegos-educativos/formulacion-nomenclatura-quimica/organica/formula-con-marvin/)  


####   Dados
Comentario general friki  
[Google Search adds multi-sided die to built-in rolling tool](https://9to5google.com/2019/08/15/google-search-roll-dice/)  

[twitter Molesyamperios/status/998594651639730176](https://twitter.com/Molesyamperios/status/998594651639730176)  
Cuando piensas que no será gran cosa y te sorprende el ... ¿Cuándo repetimos profe? Idea para repasar #quimicainorganica ¿Lo conocían? @juanfisicahr @ProfaDeQuimica @gamificatuaula @CaRoLCrEsPo_FyQ @NoeliaSandiego @SanmartinJuan @juanfratic  
[![](https://pbs.twimg.com/media/Ddu4hY6VwAE2MYS.jpg "") ](https://pbs.twimg.com/media/Ddu4hY6VwAE2MYS.jpg)  

Se trata de poner dados para cationes y dados para aniones  
[twitter Molesyamperios/status/998600679324373015](https://twitter.com/Molesyamperios/status/998600679324373015)  
Yo los compré de 16mm Yernea Official Store.. 10 unidades de cada color 1.62 € me interesaban diferentes colores para distinción + y -  

[twitter ProfaDeQuimica/status/998601817041891334](https://twitter.com/ProfaDeQuimica/status/998601817041891334)  
Y con dados de 10/12 caras, se pueden abarcar los cationes y aniones más comunes. 😊  

[twitter ProfaDeQuimica/status/1067426916255195136](https://twitter.com/ProfaDeQuimica/status/1067426916255195136)  
Así voy a pasar yo este trimestre los ratos muertos entre evaluaciones. 😉  
[![](https://pbs.twimg.com/media/DtBDHUFVsAEHbIo.jpg "") ](https://pbs.twimg.com/media/DtBDHUFVsAEHbIo.jpg)  

[twitter Biochiky/status/1076850091577827329](https://twitter.com/Biochiky/status/1076850091577827329)  
Listo para la formulación de los peques. A espera de los cubos de 12 caras para los de bachillerato. Idea de @bjmahillo (si no recuerdo mal)  
[![](https://pbs.twimg.com/media/DvG9cAZXcAAbsih.jpg "") ](https://pbs.twimg.com/media/DvG9cAZXcAAbsih.jpg)  

[twitter Biochiky/status/1084032405411975168](https://twitter.com/Biochiky/status/1084032405411975168)  
Ya están listos mis dados de 12 caras para la formulación inorgánica 1° bach y 2° bach.  
[![](https://pbs.twimg.com/media/DwtBtKeX0AEc9EW.jpg "") ](https://pbs.twimg.com/media/DwtBtKeX0AEc9EW.jpg)  

[twitter Biochiky/status/1084146684471508992](https://twitter.com/Biochiky/status/1084146684471508992)  
[![](https://pbs.twimg.com/media/DwupqEgXQAAFPvT?format=jpg "") ](https://pbs.twimg.com/media/DwupqEgXQAAFPvT?format=jpg)  [![](https://pbs.twimg.com/media/DwuyTp2WsAEbSW6?format=jpg&name=small "") ](https://pbs.twimg.com/media/DwuyTp2WsAEbSW6?format=jpg)  


[twitter MisterYeung/status/1084914204992262144](https://twitter.com/MisterYeung/status/1084914204992262144)  
Naming binary compounds can be a Bohr sometimes but not when you play chemistry Boggle. Make as many compounds as you can in a minute. 3d designed and printed this set. Love my 3D printer. #chemchat  
[![](https://pbs.twimg.com/media/Dw5jsn4VsAInivF.jpg "") ](https://pbs.twimg.com/media/Dw5jsn4VsAInivF.jpg)  
[![](https://pbs.twimg.com/media/Dw5jtj3VsAILPBN.jpg "") ](https://pbs.twimg.com/media/Dw5jtj3VsAILPBN.jpg)  

Los ficheros .stl para la impresora 3D son de pago  
[Chemistry Boggle for naming binary compoun3D Printer .stl files and answer sheet](https://www.teacherspayteachers.com/Product/Chemistry-Boggle-for-naming-binary-compoun3D-Printer-stl-files-and-answer-sheet-4311990)  
Pero también están compartidos de forma abierta [https://www.thingiverse.com/thing:2966841](https://www.thingiverse.com/thing:2966841)  
[![](https://cdn.thingiverse.com/renders/de/52/96/06/f3/41d9c75ae301b0aa848caaa5e5ba2703_preview_featured.jpeg "") ](https://cdn.thingiverse.com/renders/de/52/96/06/f3/41d9c75ae301b0aa848caaa5e5ba2703_preview_featured.jpeg)  
En septiembre 2019 se comparte más información  
[3D printed Boggle game with binary compounds](https://uwaterloo.ca/chem13-news-magazine/september-2019/feature/3d-printed-boggle-game-binary-compounds)  

[twitter ProfaDeQuimica/status/1085916377720836098](https://twitter.com/ProfaDeQuimica/status/1085916377720836098)  
Hoy he estrenado los dados químicos. Me han dicho que se les ha pasado la clase volando. La semana que viene, ¡otra vez!  
[![](https://pbs.twimg.com/media/DxHzKdUWwAA1zxu.jpg "") ](https://pbs.twimg.com/media/DxHzKdUWwAA1zxu.jpg)  [![](https://pbs.twimg.com/media/DxHzLNtWsAEAmS5.jpg "") ](https://pbs.twimg.com/media/DxHzLNtWsAEAmS5.jpg)  
[![](https://pbs.twimg.com/media/DxHzLxdUcAAQtMD.jpg "") ](https://pbs.twimg.com/media/DxHzLxdUcAAQtMD.jpg)  

[twitter ie_salud/status/1088377119611277314](https://twitter.com/ie_salud/status/1088377119611277314)  
Al rico anión. @guaytecno lo que no se le ocurra. #frikiprofes #fiquipedia @Gascanda ahora a por mi examen de StarWars de @bjmahillo Gracias!  
[![](https://pbs.twimg.com/media/DxqxBl_X0AA5PK_.jpg "") ](https://pbs.twimg.com/media/DxqxBl_X0AA5PK_.jpg)  

[twitter ProfaDeQuimica/status/1177338047001706496](https://twitter.com/ProfaDeQuimica/status/1177338047001706496)  
Recién escritos son taaaaan bonitos... Mañana los de 1° de Bachillerato echarán unas partidillas. #DadosQuímicos  
[![](https://pbs.twimg.com/media/EFa-tXFXUAAcV-W?format=jpg "") ](https://pbs.twimg.com/media/EFa-tXFXUAAcV-W?format=jpg)  

En el mismo hilo se comenta el planteamiento  
Este tipo de dados se encuentran fácilmente en AliExpress poniendo en el buscador "D12 blank dices". La tienda donde yo los compré está inactiva.  
Además de un mayor número de opciones, las caras son lo suficientemente grandes como para escribir iones poliatómicos holgadamente.  
He utilizado dos tipos de dados:  
- Blancos (aniones sencillos) y amarillos (cationes sencillos), que uso desde 3° hasta Bachillerato.
- Rojo (cationes algo más complejos), azul (oxoaniones) y verde (oxoaniones ácidos y peróxidos), que uso a partir de 4° o Bachillerato.  
A continuación os voy a enumerar en una lista los iones que he escrito en cada una de las caras, tratando de que hubiera variedad en los valores de las cargas positivas y negativas.  
DADOS BLANCOS:  
OH-  
H- (x2)  
Cl-  
Br-  
O2- (x2)  
Mg2-  
Ca2-  
P3-  
N3-  
Si4-  
DADOS AMARILLOS:  
Ag+  
Li+  
Na+  
K+  
Mg2+  
Ca2+  
Hg2+  
Zn2+  
Fe3+  
Co3+  
Ni3+  
Au3+  
DADOS ROJOS:  
Pb4+  
Pt4+  
Pd4+  
Al3+  
Cr3+  
Mn2+  
Hg2+  
Cd2+  
(NH4)+ (x2)  
H+ (x2)  
DADOS AZULES:  
(BrO2)-  
(PO4)3-  
(SO4)2-  
(CO3)2-  
(NO3)-  
(ClO)-  
(AsO3)3-  
(SiO4)4-  
(SeO3)2-  
(TeO2)2-  
(MnO4)-   
(IO3)-  
DADOS VERDES:  
(O2)2- (x3)  
(HPO3)2-  
(Cr2O7)2-  
(HSO)2-  
(HAs2O5)3-  
(HSiO4)3-   
(HSbO4)2-  
(HCO2)-  
(HBO3)2-  
(HSeO4)-  
La idea es que, por parejas (individualmente si el grupo es reducido y disponemos de suficiente material), se les reparten los dados básicos (uno blanco y uno amarillos) y los de color rojo, azul y verde irán rulando por cada puesto.  
La dinámica sería la siguiente:  
A cada alumno se le reparte la siguiente ficha. En la tabla deben anotar catión y anión obtenidos en cada tirada. Después se combinan para formar el compuesto químico correspondiente (deben escribir la fórmula y decir de qué tipo es, así como nombrarlo).  
[TABLA PARA RELLENAR DADOS QUÍMICOS.docx](https://drive.google.com/file/d/119a5IKih4FHEoEHWRPiGu5T4WljPbohz/view)  
Como ya he dicho antes, los dados rojos, verdes y azules deben cambiar de grupo o persona cuando se hayan utilizado durante dos o tres tiradas para que todos tengan la oportunidad de poder usarlos. Con el paso del tiempo iré comprando más para que todos usen un juego completo.  
Si se esmeran, en una sesión de 55 minutos pueden rellenar perfectamente la tabla completa.  
Llegados a este punto, a criterio del docente, se puede otorgar un premio (calificación extra, por ejemplo) a los tres que mayor número de compuestos bien formulados y nombrados entreguen.  
Siempre que he usado los dados químicos, el resultado ha sido muy bueno, ya que les motiva especialmente (es más ameno que simplemente rellenar fichas de formular/nombrar). Y si hay una bonificación, aún más.  
La desventaja fundamental es que, aunque los iones se escriben con rotulador indeleble, se acaban emborronando con el manoseo, por lo que tras cierto número de usos hay que borrar los dados con alcohol o acetona (acaban perdiendo el aspecto pulido y quedan mates) y reescribirlos.  
Estoy barajando la posibilidad de barnizarlos, ya que creo que el material con el que están hechos estos dados en concreto no es apto para el pirograbado. He visto otros de madera que parecen ser más duraderos (pero solo los he encontrado de 6 caras, no de 12).  

Dónde conseguir los dados / cuánto cuestanSe pueden buscar googleando, sin ánimo de publicidad, pongo unos enlaces usados en 2019  
 [Dodecaedros único color, fosforescentes](https://m.es.aliexpress.com/item/32897925080.html)  
 [6 caras plástico varios colores](https://m.es.aliexpress.com/item/32945113916.html)  
 [6 caras madera](https://m.es.aliexpress.com/item/32949930624.html)  
También se pueden hacer dados con madera y colorearlos, además de usar impresora 3D  

[twitter ProfaDeQuimica/status/1304033857906388992](https://twitter.com/ProfaDeQuimica/status/1304033857906388992)  
Alguien me ha favoriteado esta foto. No puedo usarlos este curso porque, al desinfectar, se borrarían. Alguien me dio la solución hace tiempo: que los construya el alumnado con una plantilla y cada uno tenga los suyos. Así que me he puesto manos a la obra: #ABJ #VueltaSegura  
[![](https://pbs.twimg.com/media/EhjbztQWAAIaKv6?format=jpg "") ](https://pbs.twimg.com/media/EhjbztQWAAIaKv6?format=jpg)  

Dados "avanzados" con cationes especiales y aniones poliatómicos (peróxidos, oxoaniones y oxoaniones ácidos).  RT para más difusión, ¡gracias! #ABJ #VueltaSegura #Formulación #DadosQuímicos  
[![](https://pbs.twimg.com/media/EhpAKpaXgAEMpJt?format=jpg "") ](https://pbs.twimg.com/media/EhpAKpaXgAEMpJt?format=jpg)  
Podéis descargar la plantilla imprimible en:  
[Plantilla datos inorgánicos.pdf](https://drive.google.com/file/d/11OxklQiz2pXDNfouU7vxHJ1iRXBBzYMf/view?usp=sharing)  
[twitter ProfaDeQuimica/status/1304425630545649664](https://twitter.com/ProfaDeQuimica/status/1304425630545649664)  
Visto el éxito ayer de la plantilla para #ABJ de formulación inorgánica con dados (de manera segura y sencilla), lanzo la correspondiente versión para #FormulaciónOrgánica.  
La propuesta didáctica está detallada en el hilo que cito a continuación.  
Espero que os sea de utilidad.Y la plantilla imprimible en PDF la podéis descargar de:  
[Plantilla dados orgánicos.pdf](https://drive.google.com/file/d/12tsCQxzkckeD9tCJdXq3G7SZeDR0nkhJ/view?usp=sharing)  
Si podéis difundir con un RT para que llegue a más profesores de FyQ os lo agradecería infinito (con dados, todo siempre es mejor).  

[twitter fisquiris/status/1309814861350789121](https://twitter.com/fisquiris/status/1309814861350789121)  
DADOS DE FORMULACIÓN INORGÁNICA . Adaptación digital de la versión física creada por @ProfaDeQuimica  
1) Clic a 1 dado catiónico y a 1 aniónico para detenerlos  
2) Formular el compuesto  
3) Nombrarlo #claustrovirtual #química #abj  
[Genial.ly presentation dados de formulacion inorganica](https://view.genial.ly/5f6f0dcb58c8610e14dc8db2/presentation-dados-de-formulacion-inorganica)  [![](https://pbs.twimg.com/media/Ei1lrBaXgAI2Asj?format=jpg "") ](https://pbs.twimg.com/media/Ei1lrBaXgAI2Asj?format=jpg)  
Tabla para rellenar los dados químicos:  
[Tabla para rellenar dados químicos](https://docs.google.com/document/d/1_P0A9lHM4ZTpvnqcluaLZtWUWDVh6KMYKd-8AgUb6tg/edit) 

###   Dados orgánica
 [twitter ProfaDeQuimica/status/1219356709350789124](https://twitter.com/ProfaDeQuimica/status/1219356709350789124)  
 Otra actividad con dados para formular.  
 Esta vez, FORMULACIÓN ORGÁNICA.  
 #Hilo va:  
 [![](https://pbs.twimg.com/media/EOwGc9gWoAEAtiR?format=jpg "") ](https://pbs.twimg.com/media/EOwGc9gWoAEAtiR?format=jpg)  
 DADOS CON RADICALES/MODIFICACIONES:  
 *Metilo (x2). 
 *Etilo.  
 *Propilo.  
 *Vinilo.  
 *Fenilo.  
 *Bromo.  
 *Cloro.  
 *Doble enlace.  
 *Triple enlace.  
 *Convertir la cadena principal en un cicloalcano.  
 *Convertir la cadena principal en un benceno.  
 DADOS CON GRUPOS FUNCIONALES:  
 *Ácido carboxílico.  
 *Éster.  
 *Amina (x2).  
 *Amida.  
 *Aldehído.  
 *Cetona.  
 *Alcohol (x2).  
 *Éter.  
 *Nitrilo.  
 *Nitroderivado.  
 INSTRUCCIONES:  
 1) Se lanza el D10 para averiguar la longitud de la cadena principal hidrocarbonada.  
 2) Se lanza el D12 de radicales 2 veces para añadir ramificaciones a la cadena principal.  
 El alumno elegirá a su gusto dónde localizarlas.  
 Si al lanzar ese dado aparece el modificador de cadena, lo obedece:  
*Si sale "Cambiar la cadena principal por un benceno", da igual lo que haya salido porque su compuesto va a ser un anillo de benceno.  
*Si sale "Ciclar la cadena principal", conserva el n° de C pero une extremos.  
3) Una vez ejecutados los modificadores y/o colocados los sustituyentes, lanza el D13 de grupos funcionales. Alternar en cada turno una tirada y dos para que tenga compuestos mono y difuncionales.  
En funciones no terminales, el alumno elige dónde localizarlas.  
*En caso de que salga un ÉSTER o un ÉTER, se volverá a tirar el D10 para ver la longitud del otro radical del éster.  
*En caso de que salga una amida o amina, volverá a tirar el D10 para ver si es primaria/secundaria/ternaria.  
** Si sale 0-3: 1aria.  
** Si sale 4-6: 2aria.  
** Si sale 7-9: 3aria.  
Y una vez seguidas todas las reglas, se habría terminado la fórmula del compuesto orgánico y el alumno debe nombrarlo siguiendo las reglas IUPAC.  
Una vez nombrado, comienza un turno nuevo y así sucesivamente.  
En mi clase tengo suficientes dados como para que trabajen individualmente pero pueden trabajar en parejas (más componentes suponen tiempos muertos de espera).  
Esta actividad está indicada para 4° ESO (realizando simplificaciones en las reglas o en los grupos funcionales si se estima oportuno), 1° de Bachillerato (curso óptimo) o 2° de Bachillerato sí el agobio de la temporalización lo permite.  
Voy a realizar una ficha para rellenar con estas instrucciones y, en cuanto la tenga, subiré aquí el link de descarga.  
Aquí tenéis la ficha que he trabajado con el alumnado en clase. Si el alumno lo ha estudiado bien, la tabla se le va a quedar corta en la sesión, por si queréis llevar varias copias.  
[Enlace google drive](https://drive.google.com/file/d/1pZZuq4yUM0zbRW9uQ0k8wGLBXVuvUcJ3/view) 

###   Juegos orgánica
 [twitter Molesyamperios/status/1139935723304476672](https://twitter.com/Molesyamperios/status/1139935723304476672)  
 Vi algo así en Pinterest y mi mente se puso a idear y mis manos a plastificar..Química orgánica en 4Eso¡Lo comparto con el claustro tuitero! @Biochiky @ProfaDeQuimica @bjmahillo @profedefyq @fyq_abel @CaRoLCrEsPo_FyQ @juanfisicahr @yglez @pablofcayqca @AlbaJimenezGom1 @profeFQ  
 [![](https://pbs.twimg.com/media/D9HdevuWkAES5yz.jpg "") ](https://pbs.twimg.com/media/D9HdevuWkAES5yz.jpg) 

##   Formulación, humor, memes química
Pendiente crear apartado separado, pongo algunos por varios sitios según la temática, y cito también en imágenes.  
Este hilo combina varios, el primero es sobre formulación  
[twitter DrLitos/status/1331978528699641856](https://twitter.com/DrLitos/status/1331978528699641856)  
Desde que descubrí el primer meme del Capitán América en el ascensor, pensé en preparar una colección de versiones biológico-lúdicas. A riesgo de ser expulsado de esta red social, aquí os traigo el resultado. Ha sido un placer.  
[![](https://pbs.twimg.com/media/Enwi8VoXcAEIGRr?format=jpg "") ](https://pbs.twimg.com/media/Enwi8VoXcAEIGRr?format=jpg) 

## Formulación con álbum

[USÓ EL ÁLBUM DEL MUNDIAL PARA UN EXAMEN DE QUÍMICA](https://lomasconectado.com/2022/10/03/uso-el-album-del-mundial-para-un-examen-de-quimica/)  
> Es docente de química y usó el formato del álbum del mundial para un examen recuperatorio en una secundaria  
> «El tema que estábamos trabajando era Compuestos Binarios. Ellos tenían que encontrar diferentes compuestos binarios en el álbum: tenían los cuadros con los nombres y ellos tenían que pegar figuritas con la formula molecular correspondiente».  
«Les di 12 figuritas a cada uno al azar y tenían que ir pegando la formula molecular según corresponda. Tenían que buscar la pagina con el mismo compuesto binario».   
![](https://lomasconectado.com/wp-content/webpc-passthru.php?src=https://lomasconectado.com/wp-content/uploads/2022/10/WhatsApp-Image-2022-10-03-at-14.15.08-edited-1536x1152.jpeg&nocache=1)  


