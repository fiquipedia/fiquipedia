[comentario]: # (el enlace #TOC-Mi-opini-n-personal-sobre-qu-nomenclatura-ense-ar es importante mantenerlo porque lo usa la RSEQ! https://twitter.com/FiQuiPedia/status/740795698426302465 http://bit.ly/24hT8yd y lo cito en referencias)  

# Formulación

1. [Introducción](#TOC-Introducci-n) 
    1. [Historia](#TOC-Historia)
2. [Qué y cuando usar: Formular, Formulación, Nombrar, Nomenclatura, "Nomenclatura y formulación"](#TOC-Qu-y-cuando-usar:-Formular-Formulaci-n-Nombrar-Nomenclatura-Nomenclatura-y-formulaci-n-)
3. [Análisis sobre formulación y nomenclatura IUPAC en Secundaria/Bachillerato](#TOC-An-lisis-sobre-formulaci-n-y-nomenclatura-IUPAC-en-Secundaria-Bachillerato)
    1. [Mi opinión personal sobre qué nomenclatura enseñar](#TOC-Mi-opini-n-personal-sobre-qu-nomenclatura-ense-ar)

## <a name="TOC-Introducci-n"></a> Introducción 
Esta página está asociada a información general sobre formulación y nomenclatura, por ejemplo normativa. Existe una página separada para  [apuntes / ejercicios de formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion)  
También hay información asociada en  [libros](/home/recursos/libros)  

Esto es un borrador: me gustaría tener totalmente claras las nomenclaturas que la IUPAC no admite pero en algún momento se utilizaron (entre qué fechas se utilizaron), las que se admiten ahora, y de esas las que son las recomendadas  
En 2013, las últimas normas de IUPAC para inorgánica son de 2005, y las últimas normas de IUPAC para orgánica de 1993.  

Un ejemplo es H<sub>2</sub>SO<sub>4</sub> que en 2005 tiene nomenclatura tradicional y sistemática de adición y de hidrógeno, con fórmula estructural [SO<sub>2</sub>(OH)<sub>2</sub>] 
En 2005 parece que las nomenclaturas sistemática "normal" (tetraoxosulfato de Hidrógeno) y Stock ( sulfato (VI) de hidrógeno) se abandonan por la IUPAC.  
Cito Red Book 2005 de IUPAC, página 11 (página 24 en pdf de 377 páginas)  

>IR-1.6.7 Names of inorganic acids  
The names of inorganic acids are dealt with separately in Chapter IR-8. 
Names described in Ref. 11 under the heading ‘acid nomenclature’, e.g. tetraoxosulfuric acid, trioxochloric(V) acid, **have been abandoned**. In addition, the format of the names described in Ref. 11 under the heading ‘hydrogen nomenclature’ has been changed so that ‘hydrogen’ is always attached directly to the second part of the name, and this part is always in enclosing marks. The charge number at the end of the name is the total charge.  

En página 124 (página 136 de pdf de 377 páginas)  

>Based on these considerations, the use of the word ‘acid’ in any new name in inorganic nomenclature is discouraged. However, a number of the existing ‘acid’ names are so commonly used (sulfuric acid, perchloric acid, etc.) that ***it would be unrealistic to suggest replacing them** altogether by systematic alternatives.  

Cito a Carlos Alonso de "alonsoformula.com".  

>(Los oxoácidos) es el único tipo de compuestos en el que permanece la nomenclatura antigua. La IUPAC propone una nueva nomenclatura, que aún está poco extendida dado que cuesta bastante deshabituarse de decir, por ejemplo ácido sulfúrico, que es un compuesto de uso frecuente, a decir dihidrogeno(tetraoxidosulfato), como propone la IUPAC. Ella misma admite como válida la nomenclatura tradicional en este tipo de compuestos.

### *Historia* <a name="TOC-Historia"></a>
 [thesciencewatcher.blogspot.com.es Breve historia de la nomenclatura y formulación químicas](http://thesciencewatcher.blogspot.com.es/2013/09/breve-historia-de-la-nomenclatura-y.html?view=classic&m=1) 

## Qué y cuando usar: Formular, Formulación, Nombrar, Nomenclatura, "Nomenclatura y formulación" <a name="TOC-Qu-y-cuando-usar:-Formular-Formulaci-n-Nombrar-Nomenclatura-Nomenclatura-y-formulaci-n-"></a>
En esta página se usa el término "formulación" y a veces "nomenclatura y formulación", aunque en general lo más correcto sería usar solamente"nomenclatura"; por mantener enlaces no modifico todo, es un tema de detalle y se asume que se sabe de qué se está hablando.  
A veces se usa "formulación" para asociar fórmula a un nombre dado (más correcto sería formular) y se usa "nomenclatura" para asociar nombre a una fórmula dada (más correcto sería nombrar)  
A veces para hablar de todo se usa simplemente el término "formulación", a veces "nomenclatura" y a veces "nomenclatura y formulación", aunque oficialmente lo correcto (según IUPAC) es decir solamente nomenclatura, que engloba tanto nomenclatura como formulación.  
En Red Book IUPAC 2005 (que en el propio título del libro usa únicamente "nomenclature" para hacer referencia a todo)  

>IR-1.3 AIMS OF CHEMICAL NOMENCLATURE  
The primary aim of chemical nomenclature is to provide methodology for assigning descriptors **(names and formulae)** to chemical species so that they can be identified without ambiguity, thereby facilitating communication  

El resumen es que lo correcto es usar nomenclatura, (no nomenclatura y formulación), para englobar nombrar y formular (a formular se podría asociar la palabra formulación)

## Análisis sobre formulación y nomenclatura IUPAC en Secundaria/Bachillerato <a name="TOC-An-lisis-sobre-formulaci-n-y-nomenclatura-IUPAC-en-Secundaria-Bachillerato"></a>
He encontrado gente que ya ha pensado cómo tratar el tema de la "cambiante" nomenclatura IUPAC. Incluyo varios enlaces.  
Yo pienso que el hecho de que haya cambiado y esté cambiado es en sí ya un contenido a explicar: a veces en función de cómo se nombra un compuesto en un documento se puede deducir en qué intervalo de fechas está escrito (de manera más real, con qué recomendaciones de la IUPAC que aplicaron o aplican en cierto intervalo de fechas está escrito).  
Cuando a los alumnos de bachillerato les digo que los libros de texto y los exámenes PAU no siguen las normas IUPAC de 1993 de orgánica, normas que desde 2010 son anteriores a que esos alumnos nacieran, abren los ojos como platos. 

### <a name="TOC-Mi-opini-n-personal-sobre-qu-nomenclatura-ense-ar"></a> **Mi opinión personal sobre qué nomenclatura enseñar** 
En mi opinión el problema no es la respuesta a esta pregunta; es la pregunta sobre qué nomenclatura enseñar lo que ya es un error (la idea original de este planteamiento sobre dónde está el error es de Salvador Olivares)  
La normativa oficial, Real Decretos a nivel estatal y Decretos (LOE: 23/2007 y 67/2008 de Madrid, LOMCE: RD1105/2014), todos ellos posteriores a 2005 :-), indican sin más la IUPAC, luego están delegando la responsabilidad de qué se deben enseñar a la IUPAC. Para mi la pregunta es, si no enseñamos la última nomenclatura de la IUPAC a los alumnos ¿para qué y quién las está haciendo la IUPAC? Se trata de que la usen las nuevas generaciones de químicos, y pensar/decir que en décadas se seguirá usando lo mismo/que lo anterior es lo que se usa y hay que mantenerlo lo veo tan irreal como que hace varias décadas pensasen lo mismo. No hace mucho comprobé que en libros no tan antiguos el ácido sulfúrico es SO<sub>4</sub>H<sub>2</sub>, y así lo han estudiado algunos profesores que ahora dan clase enseñándolo como H<sub>2</sub>SO<sub>4</sub>, pero que se resisten a adaptarse a nuevas normas IUPAC.  
Un tema importante es la estandarización de ciencia vía organismos internacionales, se puede hacer una analogía con el Sistema Internacional para que no haga cada uno lo que quiera: pensar en no seguir las últimas recomendaciones de IUPAC sería similar a plantearse no seguir las últimas recomendaciones Sistema Internacional, y volver a estar en una situación no realmente unificada en ciertos aspectos de la ciencia, sin un lenguaje común.  
Un motivo secundario es que en la PAU de Madrid, desde 2012, ya han indicado por escrito que usan las últimas recomendaciones de la IUPAC. (en otras comunidades como Andalucía se ha indicado desde 2011)  
En 2019 veo que PAU Canarias empieza a incluir nomenclatura de hidrógeno en oxácidos además de tradicional  

Frases de la normativa LOE de Madrid  
[Decreto 23/2007, de 10 de mayo, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo de la Educación Secundaria Obligatoria](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=4648&cdestado=P)  

>3º ESO  
"Fórmulas y nomenclatura de las sustancias más corrientes según las normas de la **IUPAC**."   
4º ESO  
"Introducción a la formulación y nomenclatura inorgánica según las normas de la **IUPAC**."  

[DECRETO 67/2008, de 19 de junio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=5081&cdestado=P)  
>1º Bachillerato  
"Formulación y nomenclatura de los compuestos inorgánicos, siguiendo las normas de la IUPAC... y saber formularlos y nombrarlos aplicando las reglas de la **IUPAC**"  

Frases en normativa LOMCE estatal  
[Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.](http://www.boe.es/boe/dias/2015/01/03/pdfs/BOE-A-2015-37.pdf)  

>**2º y 3º ESO:**  
"Formulación y nomenclatura de compuestos binarios siguiendo las normas **IUPAC**"  
"11. Formular y nombrar compuestos binarios siguiendo las normas **IUPAC**."  
"11.1. Utiliza el lenguaje químico para nombrar y formular compuestos binarios siguiendo las normas **IUPAC**."  
**4º ESO:**  
"Formulación y nomenclatura de compuestos inorgánicos según las normas **IUPAC**."  
"3. Agrupar por familias los elementos representativos y los elementos de transición según las recomendaciones de la **IUPAC**."  
"6. Nombrar y formular compuestos inorgánicos ternarios según las normas **IUPAC**."  
"6.1. Nombra y formula compuestos inorgánicos ternarios, siguiendo las normas de la **IUPAC**."  
"Formulación y nomenclatura **IUPAC** de los compuestos del carbono."  
**1º Bachillerato:**  
"Formulación y nomenclatura **IUPAC** de los compuestos del carbono."  
"1.1. Formula y nombra según las normas de la **IUPAC**: hidrocarburos de cadena abierta y cerrada y derivados aromáticos.  
2.1. Formula y nombra según las normas de la **IUPAC**: compuestos orgánicos sencillos con una función oxigenada o nitrogenada."  
"Nomenclatura y formulación orgánica según las normas de la **IUPAC.**"

Frases de normativa LOMLOE estatal y Madrid  
>**3º ESO**  
Nomenclatura: participación de un lenguaje científico común y universal formulando y nombrando sustancias simples, iones monoatómicos y compuestos binarios mediante las reglas de nomenclatura de la **IUPAC**.  
**4º ESO**  
Nomenclatura inorgánica: denominación de sustancias simples, iones y compuestos químicos binarios y ternarios mediante las normas de la **IUPAC**.  
Introducción a la nomenclatura orgánica: denominación de compuestos orgánicos monofuncionales a partir de las normas de la IUPAC como base para entender la gran variedad de compuestos del entorno basados en el carbono.  
**1º Bachillerato**  
...utilizando correctamente el lenguaje matemático, los sistemas de unidades, las normas de la **IUPAC** y ...  
3.2 Nombrar y formular correctamente sustancias simples, iones y compuestos químicos inorgánicos y orgánicos utilizando las normas de la **IUPAC**, como parte de un lenguaje integrador y universal para toda la comunidad científica.  
Reglas de la **IUPAC** para formular y nombrar correctamente algunos compuestos orgánicos mono y polifuncionales (hidrocarburos, compuestos oxigenados y compuestos nitrogenados).  
Nomenclatura de sustancias simples, iones y compuestos químicos inorgánicos mediante las normas establecidas por la **IUPAC** como herramienta de comunicación en la comunidad científica y reconocimiento de su composición y sus aplicaciones en la vida cotidiana.  
**Química 2º Bachillerato**  
...utilizar de forma correcta las normas de la **IUPAC** para nombrar y formular...  
3.1 Utilizar correctamente las normas de nomenclatura de la **IUPAC** como base de un lenguaje universal para la química que permita una comunicación efectiva en toda la comunidad científica, aplicando dichas normas al reconocimiento y escritura de fórmulas y nombres de diferentes especies químicas.  


### **FORMULACIÓN y NOMENCLATURA en QUÍMICA. Reunión coordinación Universidad de Sevilla (2011)**
"En la reunión de Coordinación mantenida con el profesorado del distrito universitario del ámbito de la Universidad de Sevilla, celebrada el 25/Enero/2011, se comentó que la Ponencia de Química ha constatado que uno de los asuntos que más preocupa es la Nomenclatura y Formulación de compuestos inorgánicos y orgánicos..."  
Se incluye  [el enlace original](http://estudiantes.us.es/descargas/Formulacion_y_Nomenclatura.pdf)  y  [se incluye también en esta página como anexo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/formulacion/2011-01-25-UniversidadSevilla-Formulacion_y_Nomenclatura.pdf).  
Incluye enlaces / material elaborado por  
- Dpto. de Física y Química del IES Juan A.Suanzes de Avilés (Asturias). Proyecto fisquiweb del que se aportan enlaces aquí en FiQuiPedia
- Gobierno de Canarias. Recopilación de applets, animaciones, propuesta didáctica y diseño Web: Grupo Lentiscal 
- 2005 [gobiernodecanarias.org FORMULACIÓN Y NOMENCLATURA DE QUÍMICA INORGÁNICA](http://web.archive.org/web/20090909013610/http://www.gobiernodecanarias.org/educacion/3/Usrn/lentiscal/1-CDQuimica-TIC/FicherosQ/apuntesformulacionOrganica.htm) *waybackmachine*  

Ver también [upo.es Ponencia de Química](https://www.upo.es/ponencia_quimica/orientaciones/) que incluye  
* [Guía sobre el uso de la nomenclatura de Química Inorgánica (elaborada por la ponencia)](https://www.upo.es/cms1/export/sites/upo/ponencia_quimica/orientaciones/documentos/PONENCIA_DE_QUxMICA-NOMENCLATURA_2005.pdf)  
* [Guía sobre el uso de la nomenclatura de Química Orgánica (elaborada por la ponencia)](https://www.upo.es/cms1/export/sites/upo/ponencia_quimica/orientaciones/documentos/guia_nomenclatura_organica_2014x1x.pdf)  
* [Errores más comunes en los exámenes de química, curso 2019-2020](https://www.upo.es/cms1/export/sites/upo/ponencia_quimica/orientaciones/documentos/Errores-comunes-detectados-en-los-examenes-19-20.pdf) que tiene un apartado de formulación:

> **Tema Formulación**  
• B, P, Si, As, Sb son, por defecto, ORTO y por ello no hay que indicarlo. Si se especifica, la respuesta será incorrecta.   
• Metal con un único estado de oxidación: no se ha de indicar este. Por ejemplo, Al(OH)<sub>3</sub> se  nombra  como  hidróxido  de  aluminio.  En  cambio,  si  se  opta  por  la  nomenclatura estequiométrica sí se deben utilizar los prefijos multiplicadores (trihidróxido de aluminio es correcto).  
• Un  error  común  es  situar  mal  en  el  nombre  del  compuesto  los  localizadores  de  las insaturaciones (= y ≡) y de determinados grupos funcionales (alcohol, cetona, amina). Butan-1-ol es correcto, 1-butanol no lo es.   
• A la vista de las contradicciones observadas en el libro azul de formulación orgánica de la IUPAC (1993) respecto a los localizadores redundantes, la ponencia dará por buena las dos respuestas. Es decir, a modo de ejemplo, se dará por válido tanto metilpropano 
como 2-metilpropano. No se deberán indicar solo en los casos muy obvios (1-clorometano es incorrecto) ni para indicar la posición de los grupos funcionales aldehídos y ácidos carboxílicos.  

### **Nomenclatura de Química Inorgánica. Recomendaciones de la IUPAC de 2005. Una adaptación del Libro Rojo a bachillerato (2011-2014) Salvador Olivares, IES Floridablanca**
El autor es Salvador Olivares Campillo IES Floridablanca. Miguel Hernández, 5. 30011 Murcia. 9 de noviembre y 5 de diciembre de 2011 y 18 enero 2014 (c) 2011-2014 Salvador OlivaresSe incluye  [el enlace original](http://www.um.es/sabio/docs-cmsweb/materias-pau-bachillerato/nomenclatura_en_quimica_inorganica._ies_floridablanca._s._olivares.pdf)  y  [se incluye también en esta página como anexo la versión de 2011](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/formulacion/2011-Nomenclatura%20de%20Qu%C3%ADmica%20Inorg%C3%A1nica.%20Recomendaciones%20de%20la%20IUPAC%20de%202005.%20Una%20adaptaci%C3%B3n%20del%20Libro%20Rojo%20a%20bachillerato.pdf)  y la  [versión revisada de 2014](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/formulacion/2014-Nomenclatura%20de%20Qu%C3%ADmica%20Inorg%C3%A1nica.%20Recomendaciones%20de%20la%20IUPAC%20de%202005.%20Una%20adaptaci%C3%B3n%20del%20Libro%20Rojo%20a%20bachillerato.pdf)  
"Resumen  
Esta guía selecciona y nombra completamente de acuerdo con las recomendaciones de la IUPAC de 2005 compuestos inorgánicos que se estudian en los dos cursos del actual bachillerato. Introduce los tres sistemas de nomenclatura básicos (sustitución, adición y composición), haciendo hincapié en el de composición y minimizando los otros dos a lo necesario por su dependencia de la estructura y por la complejidad que pueden alcanzar. También se da la nomenclatura de hidrógeno.  
Introducción  
¿Qué nomenclatura hay que dar en bachillerato? Esta pregunta, equivocada, y su respuesta, también equivocada, la he visto recientemente en la red. ..."

### **GUÍA SOBRE EL USO DE LA NOMENCLATURA DE QUÍMICA INORGÁNICA SEGÚN LAS ORIENTACIONES DE LA PONENCIA DE QUÍMICA DE ANDALUCÍA PARA LAS PRUEBAS DE ACCESO A LA UNIVERSIDAD (2011)**
Se aporta  [el enlace original](http://www.upo.es/ponencia_quimica/export/sites/ponencia_quimica/Descargas/PONENCIA_DE_QUxMICA-NOMENCLATURA_2005.pdf)  al tiempo que  [se incluye en esta página como anexo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/formulacion/PONENCIA_DE_QUxMICA-NOMENCLATURA_2005.pdf). La fecha interna de este pdf es de noviembre de 2011, a nivel de licenciamiento indica ©Ponencia de Química de Andalucía 2011, y habla de consideraciones provisionales para cursos 2011-2012 y 2012-2013. Es el que se ha utilizado por universidades públicas de Madrid al presentar su modelo en septiembre de 2012.

### GUÍA SOBRE EL USO DE LA NOMENCLATURA DE QUÍMICA PARA LAS PRUEBAS DE ACCESO A LA UNIVERSIDAD (Universidades públicas de Madrid, 2012)
En septiembre de 2012, las Universidades públicas de Madrid (Carlos III, Universidad Complutense, Universidad Rey Juan Carlos, Universidad de Alcalá de Henares, Universidad Politécnica) incluyen junto con el modelo unas recomendaciones de formulación donde se trata la normativa de IUPAC de 2005. En el modelo publicado en por la Universidad Politécnica en agosto de 2012 no se incluía, luego es una decisión de septiembre de 2012. Ver apartado de  [PAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) . La Universidad de Alcalá de Henares ofrece modelo y soluciones (4 páginas), criterios de corrección (1 página) y orientaciones (6 páginas que incluyen los comentarios de nomenclatura) en tres ficheros pdf separados:  [se incluye aquí como anexo el pdf con las orientaciones y lo relativo a nomenclatura](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/formulacion/2012-PAU-Qu%C3%ADmica-Orientaciones-uah.pdf).  
Parece que se ha tomado información de una ponencia de Andalucía de 2011 (ver en esta misma página), pero se les ha olvidado alguna cosa al copiar y pegar, como por ejemplo esta frase que creo que es esencial **"IMPORTANTE: Para la ponencia es válido cualquier sistema aceptado por la IUPAC, no obstante utilizará el sistema de nomenclatura de las columnas en gris."**  
Además de las normas de 2005 para inorgánica, se mencionan las normas de 1993 para orgánica; en general los exámenes seguían utilizando las normas anteriores de 1979. Frase literal **"La Comisión de Química utiliza la Nomenclatura de la IUPAC, siguiendo las últimas recomendaciones publicadas en 2005 para el caso de los compuestos inorgánicos, y las publicadas en 1993 para los compuestos orgánicos."**

#### Ejemplos resistencia universidades a seguir normas IUPAC 
Viendo el [acta de reunión coordinación Química de 2013](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/PAUxComunidades/quimica/madrid/2013-11-19-madrid-uah-acta.pdf) , envié este correo en 2014  

>*Leyendo el acta del 19 de noviembre de 2013 que se nos ha entregado quería comentar, con un espíritu totalmente constructivo, una frase:*  
*"Se recuerda que para la formulación de compuestos orgánicos el criterio a seguir, además de los de la IUPAC, se utilizarán las recomendaciones de la CA, que en lo que nos atañe no divide las palabras para incluir el número de la función"*  
*Entiendo que la frase quiere decir "... no divide las palabras para incluir el número ***localizador** de la función"*  
*El tema de la normativa de formulación/nomenclatura es recurrente: personalmente celebro que se incluyan junto al modelo las indicaciones explícitas sobre usar normativa IUPAC de 2005 para inorgánica y 1993 para orgánica.*  
*(aprovecho para comentar que la tabla ejemplos de inorgánica, que coinciden con los de un documento de Andalucía de 2011, creo que se ha omitido una frase inicial relevante que indica qué significan las columnas sombreadas "IMPORTANTE: Para la ponencia es válido cualquier sistema aceptado por la IUPAC, no obstante utilizará el sistema de nomenclatura de las columnas en gris" [GUÍA SOBRE EL USO DE LA NOMENCLATURA DE QUÍMICA INORGÁNICA SEGÚN LAS ORIENTACIONES DE  LA PONENCIA DE QUÍMICA DE ANDALUCÍA PARA LAS PRUEBAS DE ACCESO A LA UNIVERSIDAD ©Ponencia de Química de Andalucía 2011](https://www.upo.es/cms1/export/sites/upo/ponencia_quimica/orientaciones/documentos/PONENCIA_DE_QUxMICA-NOMENCLATURA_2005.pdf) )*  
*El comentario en sí que quería hacer: si la última nomenclatura de orgánica IUPAC es de 1993, que añade el detalle de incluir el localizador inmediatamente antes de lo que localiza, me parece un error no usar ese criterio.*  
*La parte de la normativa IUPAC 1993 que habla de posición del localizador * [Introduction. R-0.1.2 Position of locants - acdlabs.com](http://www.acdlabs.com/iupac/nomenclature/93/r93_42.htm)  
*La IUPAC indica en 1993 como ejemplo que lo que antiguamente era 1-ciclohexen-2-ol ahora es ciclohex-2-en-1-ol*  
*Siguiendo ese criterio de IUPAC de 1993, por ejemplo en el A3 del modelo 2014-2015, en lugar de 3-penten-2-ona sería pent-3-en-2-ona*  
*Yo personalmente enseño las normas de IUPAC de orgánica de 1993 y enseño a formular ese compuesto como pent-3-en-2-ona, ya que mi opinión sobre el uso IUPAC es esta  
*[fiquipedia.es Mi opinión personal sobre qué nomenclatura enseñar](/home/recursos/quimica/formulacion#TOC-Mi-opini-n-personal-sobre-qu-nomenclatura-ense-ar)*  
*Simplemente quiero comentar que el que se cite CA me parece asociado a cierta resistencia al cambio para usar "lo habitual" sin tener que adaptarse a un cambio que la IUPAC fijó hace más 20 años, antes de que nacieran los alumnos que se van a presentar a PAU!*  
*A diferencia de IUPAC, CA (asumo Chemical Abstracts de la American Chemical Society) no aparece en ningún lugar de normativa de Bachillerato, yo no lo enseño, y pienso, es solamente mi opinión, que es algo forzado recurrir a CA para "huir" de lo que dice IUPAC.*  
*Para ver en cierto modo lo absurdo que es para mí, se me ocurre una pregunta: las últimas normas CA creo que son de 2007 ¿qué normas CA son las que aplican / habría que considerar? ¿qué pasaría si también cambiasen las normas CA?*  

La respuesta recibida fue  

>*Agradezco sinceramente tus comentarios y voy a sugerir a la comisión que incluya la frase que “la comisión va a utilizar la nomenclatura de las columnas sombreadas en gris”.*  
*Sobre la nomenclatura de compuestos orgánicos, no te puedo decir en este momento si en la práctica vigente de Chemical Abstracts (CA) han cambiado esta normativa, lo que si te puedo asegurar que la comisión que elabora los repertorios tiene claro que el profesor puede utilizar cualquier tipo de nomenclatura inorgánica y orgánica que siga las normas vigentes de la IUPAC, lo único que en el documento aclaramos como vamos a redactar nuestros repertorios. Por supuesto, al alumno no se le va a penalizar porque utilice otras formas correctas de expresar los compuestos.*  

En 2017 se publican aclaraciones sobre EvAU de Química y se indica [2017-madrid-quimica-contenidos-nomenclatura.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/PAUxComunidades/quimica/madrid/2017-madrid-quimica-contenidos-nomenclatura.pdf)  
*Se mantienen igualmente las aclaraciones respecto a la nomenclatura de compuestos inorgánicos, adjuntando documento descriptivo. La nomenclatura de Química Orgánica se corresponderá con la recomendada por la IUPAC en 1993, **aunque se aceptará que el alumno utilice la anterior.**

### Recomendaciones sobre la nomenclatura de química inorgánica IUPAC 2005, Universitat de les Illes Balears
[Nomenclatura química inorgànica, recomanacions IUPAC 2005 pdf](http://estudis.uib.es/digitalAssets/257/257448_5nomenclatura_iupac.pdf)   
Treball elaborat pel Seminari de Química (Del Batxillerat a la Universitat) organitzat per la Direcció General d’Ordenació, Innovació i Formació Professional (2013).  
Autors: Gabriel Cànaves, Mercè Dopico, Enrique Gómez, Olga Lobón, Pau Maura, Miquel Palou, Rafel Perelló, Emma Sánchez, Antoni Salvà, Maria Torró i Agustí Vergés.Maig 2013  

### Guía Nomenclatura de Química Inorgánica para estudiantes de secundaria y bachillerato de la Real Sociedad Española de Química
Real Sociedad Española de Química creó un Grupo de Trabajo para la elaboración de una Guía Nomenclatura de Química Inorgánica para estudiantes de secundaria y bachillerato en noviembre de 2015, y se publicaron materiales en mayo 2016. Intervienen varias personas, entre ellas Salvador Olivares que se cita aquí. Los materiales los referencio desde la página de apuntes de formulación  
[Resumen de las normas IUPAC 2005 de nomenclatura de Química Inorgánica para su uso en enseñanza secundaria y recomendaciones didácticas](https://rseq.org/mat-didacticos/resumen-de-las-normas-iupac-2005-de-nomenclatura-de-quimica-inorganica-para-su-uso-en-ensenanza-secundaria-y-recomendaciones-didacticas/) 

### RSEQ Nomenclatura química y normas de la IUPAC en español

Publicado en web RSEQ el 1 diciembre 2022  
[Nomenclatura química y normas de la IUPAC en español](https://rseq.org/mat-didacticos/nomenclatura-quimica-y-normas-de-la-iupac-en-espanol/)  
En el siguiente libro se recogen las Guías Breves de Nomenclatura Química publicadas por la IUPAC en inglés y traducidas al español para que estén disponibles para el público. La descarga del presente libro es gratuita. También se dispone de una edición impresa que puede obtenerse a través de la Universidad de la Rioja.

Título: Nomenclatura Química y Normas de la IUPAC en Español

Año de Publicación: 2022

Autores: Efraím Reyes y Pascual Román

Editorial: Universidad de La Rioja, 2022

ISBN: 978-84-09-45312-2 y 978-84-09-45313-9

[Link](https://dialnet.unirioja.es/servlet/libro?codigo=873818)  

[Link de descarga](https://dialnet.unirioja.es/descarga/libro/873818.pdf)  

> © Logroño, 2022, Los autores. Publicado por la Universidad de La Rioja. Este trabajo
se distribuye bajo una licencia CC BY (http://creativecommons.org/licenses/by/4.0/)

### Indicacions sobre formulació inorgànica i orgànica, Cataluña
Documento (en catalán) que veo publicado en 2016 asociado a las PAU  
Se publica enlace drive que no funciona, localizo otros documentos  
[Indicacions sobre formulació inorgànica i orgànica](https://educaciodigital.cat/iesb7/moodle/pluginfile.php/72171/mod_resource/content/1/pau_quim2.pdf)  

### Relación mínima de nombres en formulación de Química Orgánica, Murcia 2017
Documento publicado en 2017 asociado a las PAU  
[Relación mínima de nombres en formulación de Química Orgánica, Murcia 2017](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/PAUxComunidades/quimica/murcia/2017-murcia-Relaci%C3%B3n_m%C3%ADnima_de_nombres_en_formulaci%C3%B3n_de_Qu%C3%ADmica_Org%C3%A1nica.pdf) 

## Documentación sobre nomenclatura no IUPAC
Alguien puede preguntar ¿pero para qué hay que usar nomenclatura no IUPAC? ¿es que acaso la hay y se usa?  
El tema no es trivial: además de la IUPAC está la ACS (American Chemical Society), que tiene sus propias reglas, que se llaman Chemical Abstract \[Services]  
[wikipedia.org Chemical Abstracts Service](http://es.wikipedia.org/wiki/Chemical_Abstracts_Service)  
Se puede leer algo de historia de CA / CAS aquí  
[cas.org CAS history](http://www.cas.org/about-cas/cas-history)  
Además de nomenclatura, la CAS identifica compuestos por "número de registro CAS", tiene una base de datos muy grande, y eso sí es habitual verlo asociado a compuestos  
[wikipedia.org Número de registro CAS](http://es.wikipedia.org/wiki/N%C3%BAmero_de_registro_CAS)  
Por ejemplo, el ácido sulfúrico tiene número de registro CAS 7664-93-9, que en cierto modo también se podría decir que es un "nombre CAS"  
[Naming and Indexing of Chemical Substances for Chemical Abstracts, 2007 Edition, A publication of Chemical Abstracts Service, Published by the American Chemical Society](https://www.cas.org/sites/default/files/documents/indexguideapp.pdf)  

En normas 2013 orgánica se cita CAS  
[Brief guide to the nomenclature of organic chemistry (IUPAC Technical Report)](https://www.degruyter.com/document/doi/10.1515/pac-2019-0104/html)  
> CAS maintains a registry of chemical substances collected from publications \[11]. **In the CAS system, compounds are named using methods similar to, but not identical with, those of IUPAC***. The most prominent difference is the use of ‘CA Index Names’, which in the index are cited in a special inverted order that was devised for the creation of alphabetical indexes of chemical names. CAS also uses conjunctive nomenclature, in which parent compounds are combined to make a new, larger parent compound.   
In the example below, the conjunctive parent name is benzeneacetic acid (corresponding substitutive name: phenylacetic acid), while the substitutive name recommended by IUPAC for this example is based on the longer chain parent compound propanoic acid.  
![](https://www.degruyter.com/document/doi/10.1515/pac-2019-0104/asset/graphic/j_pac-2019-0104_eq_38.jpg)  
Other differences include the position of locants and stereodescriptors, as well as some specific nomenclature procedures.  

> *** Note: ‘mercapto’ is no longer acceptable (but is still used by CAS).  

## Información sobre la pertenencia y expulsión de España en 2015 de la IUPAC
España ha sido expulsada de la IUPAC por no pagar las cuotas anuales*  
An. Quím., 111 (1), 2015, 12-15© 2015 Real Sociedad Española de Química  
El texto de la carta en la que se comunica la expulsión puede encontrarse en  [http://bit.ly/1MTfThb](http://bit.ly/1MTfThb)  
[España ha sido expulsada de la IUPAC por no pagar las cuotas anuales* Pascual Román Polo - analesdequimica.es](http://analesdequimica.es/index.php/AnalesQuimica/article/viewFile/721/805) 

## Documentación IUPAC (inorgánica, orgánica, ...libro rojo y otras publicaciones)
Documentación IUPAC en HTML, Recommendations on Organic & Biochemical Nomenclature, Symbols & Terminology etc.  
[http://www.chem.qmul.ac.uk/iupac/](http://www.chem.qmul.ac.uk/iupac/)  
  
[NOMENCLATURE OF INORGANIC CHEMISTRY. IUPAC Recommendations 2005. Red Book.](http://old.iupac.org/publications/books/rbook/Red_Book_2005.pdf)  pdf 377 páginas
  
[NOMENCLATURE OF INORGANIC CHEMISTRY. Second Edition. Definitive Rules 1970. Issued by the Commission of the Nomenclature of Inorganic Chemistry. London. BUTTERWORTHS](http://publications.iupac.org/pac/pdf/1971/pdf/2801x0001.pdf)  pdf 116 páginas
  
[NOMENCLATURE OF FUSED AND BRIDGED FUSED RING SYSTEMS. (IUPAC Recommendations 1998). Pure &App/. Chern., Vol. 70, No. 1, pp. 143-216, 1998.](http://www.iupac.org/publications/pac/1998/pdf/7001x0143.pdf) pdf 74 páginas  
[http://www.chem.qmul.ac.uk/iupac/fusedring/](http://www.chem.qmul.ac.uk/iupac/fusedring/) Versión web en HTML  

 
[Revised Nomenclature for Radicals, Ions, Radical Ions and Related Species (IUPAC Recommendations 1993) - old.iupac.org](https://old.iupac.org/publications/pac/1993/pdf/6506x1357.pdf)   
Radical and ions también en  [Recommendations 1993. R-5 Applications to Specific Classes of Compounds. R-5.8 Radicals and Ions - acdlabs.com](http://www.acdlabs.com/iupac/nomenclature/93/r93_24.htm)  
  
[Principles of Chemical Nomenclature, A GUIDE TO IUPAC RECOMMENDATIONS; G.J. LEIGH OBE, H.A. FAVRE, W.V. METANOMSKI; Edited by G.J. Leigh](http://old.iupac.org/publications/books/principles/principles_of_nomenclature.pdf) pdf 140 páginas

[GRAPHICAL REPRESENTATION STANDARDS FOR CHEMICAL STRUCTURE DIAGRAMS (IUPAC Recommendations 2008)](https://publications.iupac.org/pac/pdf/2008/pdf/8002x0277.pdf)  
Pure Appl. Chem., Vol. 80, No. 2, pp. 277–410, 2008.  
doi:10.1351/pac200880020277  


### IUPAC, Organic Chemistry, 1979

[International Union of Pure and Applied Chemistry. Organic Chemistry Division. Commission on Nomenclature of Organic Chemistry, Nomenclature of Organic Chemistry, Sections A, B, C, D, E, F, and H, 1979 ed., Pergamon Press, Oxford, 1979, 559 p.](https://www.acdlabs.com/iupac/nomenclature/79/r79_2.htm)  

[Nomenclatura de química orgànica: seccions A, B, C i H. Regles definitives de 1979 (Versión catalana, 2013)](http://libros.csic.es/product_info.php?products_id=650)  
Publicación: Barcelona : Consejo Superior de Investigaciones Científicas : Institut d'Estudis Catalans, 2013  Referencia CSIC: 12290 Otros datos: Traducción de: Nomenclature of organic chemistry; 2ª ed. corregida y aumentada

### Nomenclatura de Química Orgánica, Recomendaciones 2013: "PIN= preferred IUPAC names"
Cuando en 2015 todavía estoy en proceso de conseguir que la gente siga por fin las normas de 1993, me encuentro con que hay unas nuevas normas "de 2013". Había visto borradores (y los había citado hablando de la posición de localizadores), pero no sabía que se había publicado oficialmente.  

Realmente se trata de un proyecto iniciado bastante antes, en 2004 se publicó un primer borrador que aceptó comentarios hasta 2005, y posteriormente se publicó en 2013.  
El borrador es público y gratuito, pero el documento definitivo no.  
¿Cómo pretende la IUPAC que se sigan unas recomendaciones que no están disponibles de manera gratuita y pública?  
Se incluye aquí información, incluyendo unos párrafos introductorios que considero muy clarificadores  
Lo relevante es la aparición del PIN (Preferred IUPAC Name), que podríamos traducir como Nombre IUPAC Preferente. Otros nombres alternativos al "preferente" son válidos siempre que sigan las reglas y no sean ambiguos.  

*"For nomenclature purposes, a structure containing at least one carbon atom is considered to be an organic compound. The formation of a systematic name for an organic compound requires selection and then naming of a parent structure. This basic name may then be modified by prefixes, infixes, and, in the case of a parent hydride, suffixes, which convey precisely the structural changes required to generate the compound in question from the parent structure. In contrast to such systematic names, there are traditional names which are widely used in industry and academic circles. Examples are acetic acid, benzene and pyridine. Therefore, when they meet the requirements of utility and when they fit into the general pattern of systematic nomenclature, these traditional names are retained.*  
**A major new principle is elaborated in these Recommendations. The concept of ‘preferred IUPAC names’ is developed and systematically applied.** Up to now, the nomenclature developed and recommended by IUPAC has emphasized the generation of unambiguous names in accord with the historical development of the subject. In 1993, due to the explosion in the circulation of information and the globalization of human activities, it was deemed necessary to have a common language that will prove important in legal situations, with manifestations in patents, export-import regulations, environmental and health and safety information, etc. However, rather than recommend only a single ‘unique name’ for each structure, we have developed rules for assigning ‘preferred IUPAC names’, while continuing to allow alternatives in order to preserve the diversity and adaptability of the nomenclature to daily activities in chemistry and in science in general.  
**Thus, the existence of preferred IUPAC names does not prevent the use of other names to take into account a specific context or to emphasize structural features common to a series of compounds. Preferred IUPAC names belong to ‘preferred IUPAC nomenclature’ Any name other than a preferred IUPAC name, as long as it is unambiguous and follows the principles of the IUPAC recommendations herein, is acceptable as a ‘general’ IUPAC name, in the context of ‘general’ IUPAC nomenclature**. The concept of preferred IUPAC names is developed as a contribution to the continuing evolution of the IUPAC nomenclature of organic compounds. This book (Recommendations 2004) covers and extends the principles, rules and conventions described in two former publications: Nomenclature of Organic Chemistry, 1979 Edition and A Guide to IUPAC Nomenclature of Organic Compounds, Recommendations 1993. In a few instances, the 1979 rules and the 1993 recommendations have been modified to achieve consistency within the entire system. In case of divergence among the various recommendations, Recommendations 2004 prevail." 

[Current Project, Chemical Nomenclature and Structure Representation Division (VIII), Number: 2001-043-1-800, Title: Preferred names in the nomenclature of organic compounds](http://old.iupac.org/projects/2001/2001-043-1-800.html)  

Existe versión en pdf, de pago  
[Nomenclature of Organic Chemistry: IUPAC Recommendations and Preferred Names 2013](https://pubs.rsc.org/en/content/ebook/978-0-85404-182-4)  
Copyright Year:2014  
Print ISBN: 978-0-85404-182-4  
PDF eISBN: 978-1-84973-306-9  
DOI:10.1039/9781849733069-FP001  

Existen erratas públicas del libro de 2013 (última actualización consultada de la lista de erratas es de enero 2017)  
[Corrections to Nomenclature of Organic Chemistry. IUPAC Recommendations and Preferred Names 2013. *waybackmachine*](http://web.archive.org/web/20170313063039/http://www.chem.qmul.ac.uk/iupac/bibliog/BBerrors.html)  

**En 2021 se publican normas de 2013 en abierto dentro de [iupac.qmul.ac.uk World Wide Web material prepared by G. P. Moss](https://iupac.qmul.ac.uk/)**
Según esta página [What's Here and What's New - iupac.qmul.ac.uk](https://iupac.qmul.ac.uk/home/index.html)  

>Added 1 April 2022  
>Version 2 of the PDF of Nomenclature of Organic Chemistry (IUPAC Recommendations and Preferred IUPAC Names 2013), Royal Society of Chemistry, 2013, Edited by H A Favre and W H Powell.  
>Added 17 May 2021  
>Nomenclature of Organic Chemistry (IUPAC Recommendations and Preferred IUPAC Names 2013), Royal Society of Chemistry, 2013, Edited by H A Favre and W H Powell. ISBN 978-0-85404-182-4 contents list and errata and web version incorporating all changes and corrections.  




[Nomenclature of Organic Chemistry. IUPAC Recommendations and Preferred Names 2013. Prepared for publication by Henri A. Favre and Warren H. Powell, Royal Society of Chemistry, ISBN 978-0-85404-182-4](https://iupac.qmul.ac.uk/BlueBook/)  
Se puede descargar [Bluebook v2 en pdf, 60 MB, 1160 páginas](https://iupac.qmul.ac.uk/BlueBook/PDF/BlueBookV2.pdf) que permiten búsqueda.  




Presentación de ACDLabs sobre la nueva nomenclatura (ACDLabs son los que mantienen la página  [IUPAC Nomenclature of Organic Chemistry - acdlabs.com](http://www.acdlabs.com/iupac/nomenclature/)  donde están publicadas las normas de 1979 y de 1993, pero estas nuevas de 2013 de momento solamente están disponibles impresas y no las publican de momento.  

>NOTE: The most recent version of Nomenclature of Organic Chemistry. IUPAC Recommendations and Preferred Name 2013 (Blue Book) was published in December 2013. These rules supercede 1979 and 1993 recommendations.  
Currently these rules are only available in the print version due to copyright: Favre, Henri A. and Powell, Warren H. Nomenclature of Organic Chemistry. IUPAC Recommendations and Preferred Name 2013. Cambridge, UK: The Royal Society of Chemistry, 2013. ISBN 978-0-85404-182-4.  
It is our hope that we can publish the 2013 rules on this website once IUPAC permits reproduction of the text.  

[New IUPAC Organic Nomenclature: From Bottle Label to Update of Databases - acdlabs.com](https://www.acdlabs.com/download/publ/2014/acsf14_cinf.pdf)  
Andrey Yerin and all ACD/Labs team developing and supporting nomenclature tools 
ACS 248th National Meeting   
Division of Chemical Information  
San Francisco, USA, August 11, 2014  

> **The most principle innovation is the concept of preferred IUPAC name (PIN) selected by hierarchical sets of criteria.**  
There are many other changes compared to the Blue Book 1979.  
It takes ten pages just to list them. Lets see what ACD/Name provides for naming.  
The new Blue Book introduced many changes; the most significant of which include:  
* Less trivial names  
* **Length is senior to unsaturation**  
* More locants  
* More enclosing marks  
* Multiplication over substitution  
* Al, Ga, In and Tl are now ‘organic elements’  
* many other specific changes  
Of high importance is the fact that the number of criteria to select the best name has increased significantly. The nomenclature became more systematic but at the same time more difficult for humans.  

### Nomenclatura de Química Orgánica, Recomendaciones 2013: cadena principal
Además del PIN, es otro de los cambios principales: en resumen ACDlabs se indica **Length is senior to unsaturation**

En este hilo pongo alguna captura
[twitter FiQuiPedia/status/1383924582516592644](https://twitter.com/FiQuiPedia/status/1383924582516592644)  
Gracias a un comentario de @ProfaDeQuimica revisadas normas IUPAC orgánica 2013 y detectado que tendré que actualizar materiales. Comparto captura (de documento de pago de 1600 páginas) para ver que cambian criterios para elegir cadena principal.  
![](https://pbs.twimg.com/media/EzSvmW1VEAQHzbG?format=png)  

Además en documento original se indica 
[P-44.3.2 The principal chain has the greater number of skeletal atoms \[criterion (b) in P-44.3\]](https://iupac.qmul.ac.uk/BlueBook/P4.html#4403)    
> In acyclic parent sctructures the order of senioriy between unsaturation and length of chain given in earlier recommendations is reversed. Thus, the first criterion to be considered in choosing a preferred parent acyclic chain is the length of the chain; unsaturation is now the second.  
![](https://pbs.twimg.com/media/E-2pXAFXEAQvPhh?format=png)  

Ejemplos claros comparando con lo anterior:

[Brief Guide to the Nomenclature of Organic Chemistry - iupac.org](https://iupac.org/wp-content/uploads/2020/08/Organic-Brief-Guide-brochure_v1.0_06Feb2020.pdf)  
Que está en formato web aquí [Pure and Applied Chemistry | Volume 92: Issue 3, Brief guide to the nomenclature of organic chemistry (IUPAC Technical Report)](https://www.degruyter.com/view/journals/pac/92/3/article-p527.xml) 
> f. Criteria for chains   
f.1. Contains more atoms   
nine-atom chain is senior to eight-atom chain (even if it has fewer double bonds)  
Note: In earlier recommendations unsaturation was senior to chain length.  
![](https://pbs.twimg.com/media/E-2py_sX0AsSoUS?format=png)  

[New IUPAC Organic Nomenclature: From Bottle Label to Update of Databases - acdlabs.com](https://www.acdlabs.com/download/publ/2014/acsf14_cinf.pdf)  
> We did implement some earlier well-agreed upon changes, including the change of seniority of chain length to degree of unsaturation.
Was 2-ethylbut-1-ene  
PIN 3-methylidenepentane  
![](https://pbs.twimg.com/media/E-2p6fvXEAYMTkn?format=png)  

El tema tiene detalles, se puede ver este diagrama
[Brief Guide to the Nomenclature of Organic Chemistry - iupac.org](https://iupac.org/wp-content/uploads/2020/08/Organic-Brief-Guide-brochure_v1.0_06Feb2020.pdf)  
![](https://www.degruyter.com/document/doi/10.1515/pac-2019-0104/asset/graphic/j_pac-2019-0104_fig_001.jpg)  
Donde aparecen "Senior element", además de los grupos  
Sin citar éteres en tabla Table 3: Seniority order for characteristic groups de 4 CHARACTERISTIC GROUPS – Suffixes and prefixes indica "Note that, for nomenclature purposes, C-C multiple bonds are not considered to be characteristic groups (Section 5.4)"  	

Al hablar de prioridades en normas 2013 aparecen varias prioridades, pero las importantes son las de tabla [Table 4.4 Complete list of suffixes and functional replacement analogues (when present) for IUPAC preferred names, in decreasing order of seniority](https://iupac.qmul.ac.uk/BlueBook/P4.html#43) (las mismas que aparecen en documento resumen "Brief Guide"), no las de tabla [Table 4.1 General compound classes listed in decreasing order of seniority](https://iupac.qmul.ac.uk/BlueBook/P4.html#41) que va de 1 a 20 para sufijos y 21 a 43 para senior element, ya **IUPAC indica que se usan prioridades de grupos expresados como sufijos**  (eso enlaza con prioridad grupo éter)  
Normas indican [P-44.0 INTRODUCTION](https://iupac.qmul.ac.uk/BlueBook/P4.html#44)  
> The selection of a preferred parent structure is based on the seniority of classes (see P-41), which gives priority first to **characteristic groups expressed as suffixes**   

Volviendo a [P-44.3 SENIORITY OF ACYCLIC CHAINS (THE PRINCIPAL CHAIN)](https://iupac.qmul.ac.uk/BlueBook/P4.html#4403) y viendo qué más criterios que el citado b   

> When there is a choice for the principal chain, the following criteria are applied, in the order listed, until a decision is reached. The principal chain:  
(a) contains the greater number of heteroatoms of any kind;  
(b) has the greater number of skeletal atoms;  
(c) contains the greater number of the most senior acyclic heteroatom in the following sequence: O > S > Se > Te > N > P > As > Sb > Bi > Si > Ge > Sn > Pb > B > Al > Ga > In >Tl.   
> **Further criteria applicable to acyclic chains are found in P-44.4.**

[P-44.4 SENIORITY CRITERIA APPLICABLE TO RINGS, RING SYSTEMS, OR ACYCLIC CHAINS](https://iupac.qmul.ac.uk/BlueBook/P4.html#4404)  
> P-44.4.1 **If the criteria of P-44.1 through P-44.3, where applicable, do not effect a choice of a senior parent structure**, the following criteria are applied successively until there are no alternatives remaining.  

Ahí se priorizan insaturaciones y solo ante igualdad los dobles  

> (a) has the greater number of multiple bonds (P-44.4.1.1);  
> (b) has the greater number of double bonds (P-44.4.1.2);  

También es importante que en el diagrama aparece

> **Rings are senior to rings**

Se puede ver

[Guía Breve para la Nomenclatura en Química Orgánica Versión 1.0, diciembre de 2021 (correspondiente con la Version 1.1, June 2021 inglesa)](https://iupac.org/wp-content/uploads/2021/12/Guia-breu_CAT_7es_2_20211215.pdf)  
> d. Los ciclos tienen mayor jerarquía que las cadenas si se componen de los mismos elementos  
NOTA 2: En recomendaciones anteriores, la jerarquía dependía del número de átomos.  
[P-44.1.2.2 Systems composed of rings and chains (exclusive of linear phanes)](https://iupac.qmul.ac.uk/BlueBook/P4.html#44010202)  
> (1) Within the same class, a ring or ring system has seniority over a chain. 

## Sufijo -ilideno

El cambio de cadena principal en IUPAC 2013 hace que sea más habitual sustituyentes enlazados por doble enlace a la cadena principal. 
Se usa -ylidene  

Normas 2013: se recomienda metilideno (es PIN) aunque se acepta metileno para CH2=  

[P-56.4 THE ENDINGS ‘DIYL’ AND ‘YLIDENE’ VS. ‘YLENE’](https://iupac.qmul.ac.uk/BlueBook/P5.html#56)  
>Except for ‘methylene’, ‘ethylene’, and ‘phenylene’, the suffix ‘ylene’ previously used to describe divalent substituent groups in which the free valences do not form a double bond, i.e., –E– or E< was discarded in 1993 (ref. 2). Substituent groups in which the free valences form a double bond, i.e., E= were described by the suffix ‘ylidene’. The suffix ‘ylene’ was replaced by the suffixes ‘diyl’ to express the –E– or E< type of bonding, and ‘ylidene’ for E=, for example, ethane-1,2-diyl for –CH2-CH2– and ethylidene for H3C-CH=, respectively. However, the name ‘methylene’ is retained to describe the substituent group –CH2–; it is used in preferred IUPAC names rather than methanediyl. CAS still use the ‘ylene’ suffix to describe the ‘diyl’ and ‘ylidene’ types of bonds, especially ‘methylene’ for –CH2– and CH2=.  

[P-60.2 PRESENTATION OF NAMES.](https://iupac.qmul.ac.uk/BlueBook/P6.html)  
>For example, the prefix ‘methylene’ is ‘no longer recommended’ in IUPAC nomenclature to designate the =CH2 group.  

Se pone ejemplo  
> 3-methylidenehexane (PIN)   

[P-71.2.2.1 Specific method and retained names](https://iupac.qmul.ac.uk/BlueBook/P7.html)  

> Systematic names are the preferred IUPAC names. The retained names carbene or methylene, nitrene or aminylene and carbyne, can be used in general nomenclature, with full substitution.  

H2C2•  
methylidene (PIN)  
carbene  
methylene  

Normas 1993:

[Guide to Name Construction. R-4.1 General Principles - normas 1993](https://www.acdlabs.com/iupac/nomenclature/93/r93_317.htm)
> (a) that no characteristic group is expressed as a suffix (instead, a suffix such as "-yl" or "-ylidene", etc., is used);

Puede surgir la duda de si usar -ylene ó -ylidene y cómo traducir esos sufijos castellano.  

[Revised Nomenclature for Radicals, Ions, Radical Ions and Related Species (IUPAC Recommendations 1993) - old.iupac.org](https://old.iupac.org/publications/pac/1993/pdf/6506x1357.pdf)   

> RC-80.5. The operational suffices "-ylene" <sup>1Oc</sup>, "-ylidene"<sup>3c, 10d</sup>, and  "-ylidyne"<sup>1Oe</sup> used in  Sections A and B of the 1979 IUPAC Organic  Rules<sup>1</sup> are retained in these recommendations, but with restricted meanings. The suffix "-ylene" is used to describe the substituent groups -CH<sub>2</sub>- (methylene) , -CH<sub>2</sub>-CH<sub>2</sub>- (ethylene),   and -C<sub>6</sub>H<sub>4</sub>- (phenylene) only;  methylene is used only when two bonds from the CH<sub>2</sub> group are not attached to a single atom of a parent structure. The suffixes "-ylidene" and "-ylidyne" are used to describe groups in which two and three hydrogen atoms, respectively, have been removed from the same skeletal atom of a parent hydride and the resulting bonds are attached to a single atom of a parent structure, thereby describing the presence of a double or triple bond, respectively, between the substituting group and a parent structure or a  parent substituent.  


La referencias 3 y 10 están asociada a distintas secciones de referencia 1, que a su vez es 
[International Union of Pure and Applied Chemistry. Organic Chemistry Division. Commission on Nomenclature of Organic Chemistry, Nomenclature of Organic Chemistry, Sections A, B, C, D, E, F, and H, 1979 ed., Pergamon Press, Oxford, 1979, 559 p.](https://www.acdlabs.com/iupac/nomenclature/79/r79_2.htm)  

Se indica:  
Para 10c: `[c] Rules A-4.2, p. 14; A-4.3, p. 15; A-11.6, pp. 17-8; A-24.4, p. 30; and A-55, p. 45-6;`   
Para 3c: `[c]  Rule B-5.12,  pp. 70-1;`  
Para 10d: `[d] Rules A-4.1, p. 14; A-4.4, p. 15; A-4.5, pp. 15-6; A-11.5, p. 17; and A-24.3, p. 30;`  

Pongo enlaces de normas 1979:  
 
[Acyclic Hydrocarbons. Rule A-4. Bivalent and Multivalent Radicals (A-4.2, A-4.3, A-4.4, A-4.5) - acdlabs.com](https://www.acdlabs.com/iupac/nomenclature/79/r79_78.htm)  
[Monocyclic Hydrocarbons. Rule A-11. Unsubstituted Compounds and Radicals (A-11.5, A-11.6) - acdlabs.com](https://www.acdlabs.com/iupac/nomenclature/79/r79_60.htm)  
[Fused Polycyclic Hydrocarbons. Rule A-24. Radical Names from Trivial and Semi-trivial Names (A-24.3, A-24.4)](https://www.acdlabs.com/iupac/nomenclature/79/r79_74.htm)  
[Hydrocarbon Ring Assemblies. Rule A-55. Radicals for Identical Ring Systems](https://www.acdlabs.com/iupac/nomenclature/79/r79_159.htm)  


#### Nomenclatura Química Inorgánica IUPAC y futuros "preferred names" en inorgánica, similares a los PIN de orgánica 2013
Se cita en normas 2005, IR-8.1 Introduction and overview  
**In the future, IUPAC aims to select preferred names for inorganic species**, including the acids dealt with here, just as Ref. 1 does for organic species.  

y la referencia citada en 2005, en apartado IR-8.7 se puede entender como el borrador de la nomenclatura de 2013  
*Nomenclature of Organic Chemistry, IUPAC Recommendations, eds. W.H. Powell and H. Favre, Royal Society of Chemistry, in preparation.*

### Nomenclatura de Química Inorgánica, Recomendaciones de 2005, **Versión española**
Las normas IUPAC de inorgánica de 2005 están originalmente en inglés y son descargables en inglés pdf de forma gratuita.  
En 2007 se hizo una traducción al español por la Universidad de Zaragoza, que es citada por la IUPAC, pero en principio es de pago  
[Nomenclatura de Química Inorgánica. Recomendaciones de la IUPAC de 2005](http://old.iupac.org/publications/books/author/RedBook-spanish.html)  
*Miguel A. Ciriano y Pascual Román Polo*  
Editorial: Prensas Universitarias de Zaragoza, junio 2007  
ISBN 978-84-7733-905-2  
En 2015 se localiza en versión PDF descargable dentro de una página del Cidead, Ministerio de Educación  
[Cidead 3º ESO Nomenclatura y formulación inorgánica](http://recursostic.educacion.es/secundaria/edad/3esofisicaquimica/3quincena8/)  
Inicialmente se incluía en esta página entendiendo que si estaba publicada por el Ministerio no había problemas de derechos de autor, pero en 2016 se retira ya que Prensas de la Universidad de Zaragoza que tiene los derechos de explotación confirma que no se puede publicar y también es retirado el pdf de la web del Ministerio.    
[Panace@. Vol. IX, n.º 28. Segundo semestre, 2008, "Breve historia de la traducción del Libro rojo de 2005 de la IUPAC"](http://www.medtrad.org/panacea/IndiceGeneral/n28_tribuna-cirianoypolo.pdf) 

### Guías breves IUPAC (inorgánica, orgánica)
[Brief Guides to Chemical Nomenclature, International Union of Pure and Applied Chemistry, Division of Chemical Nomenclature and Structure Representation (Division VIII)](https://www.qmul.ac.uk/sbcs/iupac/BriefGuide/)  
 
[researchgate.net Brief guide to the nomenclature of inorganic chemistry, January 2015 Pure and Applied Chemistry 87(9), DOI: 10.1515/pac-2014-0718](https://www.researchgate.net/publication/282562216_Brief_guide_to_the_nomenclature_of_inorganic_chemistry)  
 
[iupac.org Brief Guide to the Nomenclature of Inorganic Chemistry, Version 1.3, November 2017](https://iupac.org/cms/wp-content/uploads/2018/05/Inorganic-Brief-Guide-V1-3.pdf)  pdf 4 páginas
 
[iupac.org Brief Guide to the Nomenclature of Organic Chemistry](https://iupac.org/wp-content/uploads/2020/08/Organic-Brief-Guide-brochure_v1.0_06Feb2020.pdf)  

[Guía Breve para la Nomenclatura en Química Inorgánica, castellano - iupac.org](https://iupac.org/wp-content/uploads/2016/07/20151106GuiaBreveNQIEspayol.pdf)  

[Guía Breve para la Nomenclatura en Química Orgánica, castellano - iupac.org](https://iupac.org/wp-content/uploads/2021/12/Guia-breu_CAT_7es_2_20211215.pdf)  
  
[Pure and Applied Chemistry | Volume 92: Issue 3, Brief guide to the nomenclature of organic chemistry (IUPAC Technical Report)](https://www.degruyter.com/view/journals/pac/92/3/article-p527.xml) 

### Formulación y nomenclatura orgánica: 1979 vs 1993
Aquí intento aclarar de manera oficial, con enlaces a documentación IUPAC, las diferencias entre la normativa IUPAC de 1979 y de 1993.  
Enlaces a versión HTML de la documentación IUPAC 1979 y 1993 en  [IUPAC Nomenclature of Organic Chemistry - acdlabs.com](http://www.acdlabs.com/iupac/nomenclature/) , que indica *"This HTML reproduction is as close as possible to the published version [see IUPAC, Commission on Nomenclature of Organic Chemistry. A Guide to IUPAC Nomenclature of Organic Compounds (Recommendations 1993), 1993, Blackwell Scientific publications, Copyright 1993 IUPAC]. If you need to cite these rules please quote this reference as their source."*  
El preámbulo tiene estos párrafos, que reflejan que los cambios se intentan hacer despacio, pero los seguirá habiendo.  

*Lastly, the Commission recognizes that for certain types of compounds, there is significant disagreement among chemists in different fields as to what should be the preferred nomenclature. This situation leads to an apparent lack of decisiveness in some of the recommendations in this document. This is unavoidable, because long experience has taught that formulating rules not having general support is a futile exercise; such rules will be widely ignored. Therefore, the Commission's policy is to offer critically examined alternatives, some of which may be new proposals, and to observe how they are accepted and used. If one of the alternatives subsequently becomes preferred to an overwhelming extent by the community of chemists, a future edition of recommendations can reflect that fact. *  
...
*Therefore, the Commission has initiated projects to formulate a comprehensive guide for selecting unique names that will, insofar as feasible, have good recognition value and general acceptance among chemists; the results will be presented in later publications. Further projects, with longer-range objectives of systematizing nomenclature of organic compounds, are also under way.*  

Los cambios de 1993 se pueden ver en documentación (ver más abajo apuntes de orgánica de 18 páginas de gobiernodecanarias.org con tabla inicial con ejemplos comparados muy clarificadora)  
El cambio más relevante entre 1979 y 1993 es la posición de los localizadores, y puede surgir la duda de si es una "recomendación" o es "obligatorio" ese cambio ¿están o no mal los nombres con el localizador al principio? ¿además de que esté bien Hex-2-eno, está bien decir 2-Hexeno?  
Según IUPAC, parece ser obligatorio; no se da opción, y se citan los nombres anteriores sin indicar que son válidos.  

[R-0.1.2 Position of locants](http://www.acdlabs.com/iupac/nomenclature/93/r93_42.htm)  
*Locants (numerals and/or letters) are placed **immediately before** the part of the name to which they relate, except in the case of traditional contracted forms (see R-2.5).  
Examples to R-0.1.2 Hex-2-ene (R-3.1.1) (**formerly** 2-Hexene)  
Cyclohex-2-en-1-ol (R-5.5.1.1) (**formerly** 2-Cyclohexen-1-ol)  
[R-0.0 Scope](http://www.acdlabs.com/iupac/nomenclature/93/r93_38.htm)  
This guide to IUPAC nomenclature of organic compounds provides an outline of the main principles of organic nomenclature as described in the 1979 edition of the IUPAC Nomenclature of Organic Chemistry and includes important changes agreed upon since its publication. **Differences from the 1979 edition have not been specifically highlighted. However, in many cases a name used in the 1979 edition, preceded by a word such as "formerly" or "previously", appears in parentheses following a name recommended herein.**  

En 1993 ya se decía que iba a haber más cambios: si miramos un texto más reciente (es borrador), dice lo mismo sobre localizadores.    
[iupac.org Preferred IUPAC Names. CHAPTER P-1 NOMENCLATURE OF ORGANIC COMPOUNDS. IUPAC Provisional Recommendations. September 2004 (provisional)](http://old.iupac.org/reports/provisional/abstract04/BB-prs310305/Chapter1.pdf)  
Es un borrador, pero es importante página 27 del pdf, sobre la posición de los localizadores (el cambio de 1979 a 1993 que hizo IUPAC y que cita los mismos ejemplos que en la normativa de 1993, pero ahora indicando "not" cuando antes decía "formerly")  
*"P-14.3.2 Position of locants*Locants (numerals and/or letters) are placed **immediately before** that part of the name to which they relate, except in the case of the traditional contracted names when locants are added at the front of their names.*  
Examples:  
hex-2-ene (PIN) (**not** 2-hexene)  
cyclohex-2-en-1-ol (PIN) (**not** 2-cyclohexen-1-ol)  
El tema de PIN se trata junto con las recomendaciones de 2013.

### Formulación inorgánica: "nomenclatura tradicional" 
Ver más abajo el apartado "¿Hay realmente tres nomenclaturas en inorgánica, tradicional, Stock y "Sistemática" tras las normas IUPAC de 2005?"  
Aquí intento aclarar de manera oficial, con enlaces a documentación IUPAC, cómo se define la "nomenclatura tradicional" según la IUPAC. La duda me surgió inicialmente con la nomenclatura de las sales ácidas, donde leyendo en varios sitios parece que hay "varios tipos de nomenclatura tradicional, alguna más antigua que otra" (por ejemplo poner prefijo hidrógeno al nombre de la sal tradicional, añadir la palabra ácido después para indicar que le quedan hidrógenos, o añadir el prefijo bi- en ciertos casos, por ejemplo NaHSO4 con varios nombres "tradicionales" como hidrogenosulfato de sodio, sulfato ácido de sodio ó bicarbonato de sodio), y que hace plantearse ¿hasta qué punto define la IUPAC la nomenclatura tradicional?  

Por ejemplo los nombres tradicionales de oxoácidos (hipo- -oso, per- -ico, orto-, meta-, piro-) están tratados en normativa IUPAC 1971  
*3.224—It is quite practicable to treat oxygen also in the same manner as other ligands (2.24), but it has long been customary to ignore the name of this element altogether in anions and to indicate its presence and proportion by means of a series of prefixes (hypo-, per-, etc., see Section 5) and sometimes also by the suffix -ite in place of -ate.*  
...  
*Concerning the use of prefixes hypo-, per-, etc., see the list of acids, table in 5.214. For all new compounds and even for the less common ones listed in the table in 3.224 or derived from the acids listed in the table in 5.214 it is preferable to use the system given in 2.24 and in Sections 5 and 7*  
...  
*5.21—Oxoacids*  
*For the oxoacids the ous-ic notation to distinguish between different oxidation states is applied in many cases. The -ous acid names are restricted to acids corresponding to the -ite anions listed in the table in 3.224.*  
*Further distinction between different acids with the same characteristic element is in some cases effected by means of prefixes. **This notation should not be extended beyond the cases listed below.**  
...  
*5.213—The prefixes ortho- and meta- have been used to distinguish acids differing in the "content of water". The following names are approved:*  
...  
*214—The following table contains the accepted names of the oxoacids ...For many of these acids systematic names are preferable,...*

#### Ejemplo: nomenclatura tradicional de sales ácidas
En el libro rojo de 2005 se menciona que la nomenclatura de las sales ácidas cambió en 1940!  
*The first comprehensive report of the inorganic commission, in 1940, had a major effect on the systematization of inorganic nomenclature and made many chemists aware of the necessity for developing a more fully systematic nomenclature. Among the significant features of this initial report were the adoption of the Stock system for indicating oxidation states, the establishment of orders for citing constituents of binary compounds in formulae and in names,**the discouragement of the use of bicarbonate, etc. in the names of acid salts**, and the development of uniform practices for naming addition compounds.*  

En el año 1971 (NOMENCLATURE OF INORGANIC CHEMISTRY. Second Edition. Definitive Rules 1970. Issued by the Commission of the Nomenclature of Inorganic Chemistry. London. BUTTERWORTHS) se cita el uso del prefijo "hidrógeno"  

*6.2. SALTS CONTAINING ACID HYDROGEN ("Acid" salts*)*  
*Names are formed by adding the word 'hydrogen', with numerical prefix where necessary, to denote the replaceable hydrogen in the salt. Hydrogen shall be followed without space by the name of the anion. Exceptionally, inorganic anions may contain hydrogen which is not replaceable. It is still designated by hydrogen, if it is considered to have the oxidation number +1, but the salts cannot of course be called acid salts.*  
Examples:  
1. NaHCO<sub>3</sub> sodium hydrogencarbonate  
2. LiH<sub>2</sub>PO<sub>4</sub> lithium dihydrogenphosphate  
3. KHS potassium hydrogensulfide  
4. NaHPHO<sub>3</sub> sodium hydrogenphosphonate  

### ¿Se puede seguir diciendo cloruro férrico, cloruro ferroso?
No desde 2005

[IUPAC 2005. Table III Suffixes and endings](http://old.iupac.org/publications/books/rbook/Red_Book_2005.pdf#page=264)  
> -ic: Ending formerly added to stems of element names to indicate a higher oxidation state, e.g. ferric chloride, cupric oxide, ceric sulfate. **Such names are no longer acceptable.**  
-ous: Ending formerly added to stems of element names to indicate a lower oxidation state, e.g. ferrous chloride, cuprous oxide, cerous hydroxide. **Such names are no longer acceptable.**  

### ¿Hay realmente tres nomenclaturas en inorgánica, tradicional, Stock y Sistemática tras las normas IUPAC de 2005?
Creo que si miramos muchos libros, muchos apuntes y preguntamos a muchos profesores en 2013 dirán todos que esta pregunta es una tontería: por supuesto que sí.He visto y sigo viendo hojas de ejercicios con tres columnas encabezadas "sistemática, Stock y tradicional".  
Creo que si digo que esos encabezados están mal estoy derrumbando costumbres que llevan mucho tiempo y se le pondrán los pelos como escarpias a mucha gente, pero mi opinión de resumen es clara (ver los apuntes de formulación elaborados por mí):  
**Opino que lo más correcto desde 2005 es decir que la nomenclatura tradicional y la nomenclatura Stock realmente no existen para la IUPAC**  
En 2005 la IUPAC establece varias nomenclaturas: de composición, de adición y de sustitución:  
- La nomenclatura tradicional no existe; la IUPAC simplemente admite ciertos nombres comunes de compuestos, pero eso no implica que haya una nomenclatura como tal. La propia IUPAC insiste en que solamente son válidos los nombres comunes que indica de forma explícita en tablas, por ejemplo para oxácidos  
- La nomenclatura de Stock no aparece mencionada en 2005, simplemente se menciona dentro de la nomenclatura de composición, la posibilidad de varios sistemas, entre los que está el estequiométrico con prefijos (que se asociaría a lo que se suele llamar "sistemática") y el sistema de indicar números de oxidación (que se asociaría a lo que se suele llamar "Stock")  

En [NOMENCLATURE OF INORGANIC CHEMISTRY. SECOND EDITION. DEFINITIVE RULES 1970, página 116](http://publications.iupac.org/pac/pdf/1971/pdf/2801x0001.pdf#page=116)  "Stock system" remite a 0.1 (oxidation number, no cita Stock) y 2.252  

En [NOMENCLATURE OF INORGANIC CHEMISTRY. IUPAC Recommendations 2005. Red Book., página 375](http://old.iupac.org/publications/books/rbook/Red_Book_2005.pdf#page=375)  se indica "Stock number (see oxidation number)"  

En [GoldBook, Stock number](https://goldbook.iupac.org/terms/view/S06020)  indica "obsolete"  

Quizá usar "Stock" para hablar de "números de oxidación" es algo que no aporta y se pasa a usar directamente "números de oxidación" sin llamarlo Stock.  

### "Stock" / números de oxidación: obligatoriedad de no indicarlo si "solo hay un valor"

Es una duda que me plantean en 2022, no me lo había planteado antes.  

> Una compañera dice que el libro Rojo no especifica en ningún sitio que no se deba indicar el número de oxidación de los metales con un solo valor. Por ejemplo, ella da por bueno cuando un alumno dice óxido de aluminio(III). El resto insistimos en que está mal y así lo debe indicar a sus alumnos, pero el caso es que no he encontrado el párrafo concreto donde se dice que no se debe hacer  

Tras mirarlo resumo y luego detallo aquí: es cierto que no es obligatorio omitirlo si solo tiene un número de oxidación habitual, y no hay que dar por incorrecto óxido de aluminio(III). Eso alegrará a muchos alumnos, pero no quita para que deban saber que se puede omitir y sepan formular un nombre que lo tenga omitido como óxido de aluminio.  
Enlaza con las nuevas normas de IUPAC de 2025 en las que definirán el PIN y espero que será con prefijos multiplicadores y sin números de oxidación.  


En [Red Book 2005 en inglés](http://old.iupac.org/publications/books/rbook/Red_Book_2005.pdf) efectivamente no he visto que lo cite explícitamente. La traducción oficial de la universidad de Zaragoza (no es pública) no dice nada distinto.  

Lo más relacionado
>IR-5.4.2.2 Use of charge and oxidation numbers  
> In nomenclature, the use of the charge number is **preferred** as the determination of the oxidation number is sometimes ambiguous and subjective. It is advisable to use oxidation numbers only when there is no uncertainty about their assignment.  
> ...  
> The oxidation number (see Sections IR-4.6.1 and IR-9.1.2.8) of an element is indicated by a Roman numeral placed in parentheses immediately following the name  

Los ejemplos 5, 20, 21 con K, 8, 22 con Na muestran que al nombrar omiten el (I), pero no se cita.

En IR-5.3.3.1 se indica " When there is no ambiguity, the charge number may be omitted, as in Example 1 below." pero es para aniones.

En [NOMENCLATURE OF INORGANIC CHEMISTRY. SECOND EDITION. DEFINITIVE RULES. 1970](http://publications.iupac.org/pac/pdf/1971/pdf/2801x0001.pdf#page=23)  
2.252 tiene ejemplos con K y Na similares que lo omiten, aunque veo un (II) en Ba.

> BaO<sub>2</sub> barium(II) peroxide

En [RESUMEN DE LAS NORMAS IUPAC 2005 DE NOMENCLATURA DE QUÍMICA INORGÁNICA PARA SU USO EN ENSEÑANZA SECUNDARIA Y RECOMENDACIONES DIDÁCTICAS. Normas actuales de la IUPAC sobre nomenclatura de Química Inorgánica. 2. NOMENCLATURA DE COMPOSICIÓN (NOMBRES ESTEQUIOMÉTRICOS). Compuestos binarios](https://rseq.org/wp-content/uploads/2018/09/6-DocumentoFinal-Todo.pdf#page=18)
>...
> b)...Además, la carga de algunos cationes **puede omitirse** cuando no hay duda, como es el caso de los alcalinos (grupo 1, siempre 1+) y los alcalinotérreos (grupo 2, siempre 2+), así como los elementos más comunes con número de oxidación único (caso del aluminio 3+, por ejemplo).  
> c) ... Este paso **puede omitirse** con los elementos más comunes con número de oxidación único, como los indicados en el apartado b)
	
Indica "puede omitirese", no indica obligatoriedad de omitir.  

La respuesta tras enviar el resumen es:   
> Por un lado dice que puede omitirse, por otro lado no hay ningún ejemplo utilizándolo y además creo recordar que en algún sitio dicen que el nombre debe ser lo más sencillo posible... Todo este unido a que los correctores de la EvAU seguramente lo den por malo... Me hace decantarme por la incorrección de su uso por redundancia de información. En cualquier caso no creo que sea conveniente darlo por bueno, ya que nadie lo usa y lleva a confusión.  
 
Mi respuesta:  

Entiendo que hay resistencia al cambio / tendencia a "lo que se ha hecho siempre". A mi me da igual mientras haya unos criterios a seguir. La normativa LOMCE, LOMLOE dice que sigamos criterios IUPAC.  

>creo recordar que en algún sitio dicen que el nombre debe ser lo más sencillo posible  
Referencia?

>no hay ningún ejemplo utilizándolo
[BaO2 barium(II) peroxide](http://publications.iupac.org/pac/pdf/1971/pdf/2801x0001.pdf#page=23)  

> los correctores de la EvAU seguramente den lo den por malo  
Si el nombre lo dan con números de oxidación, omitidos o no, el alumno lo debe saber formular.  
En EvAU correctores deben aceptar nombrar por composición estequiométrica con prefijos, luego no nombrando por números de oxidación no veo problema. Ya digo que en normas 2025 espero que nombrar con números de oxidación deje de ser PIN (normas 2005 indican "the use of the charge number is preferred as the determination of the oxidation number is sometimes ambiguous and subjective")  
Ya comento en mis apuntes  

>Ejemplos para reflexión final: ¿nombre tradicional AuPO<sub>3</sub> , CuFeS<sub>2</sub> , PbO<sub>2</sub> ? ¿Fosfito de oro (III) ó metafosfato de oro (I), Sulfuro de cobre (II) y hierro (II) ó de cobre (I) y hierro (III), Óxido de plomo (IV) o Peróxido de plomo (II)? Para mí muestran lo "absurdo" de poner en el nombre el estado de oxidación; realmente darlo no da información sobre la fórmula, y la nomenclatura sistemática es más útil en dar un nombre que describa la fórmula. 

La nomenclatura sistemática es lo más parecido a decir "hache dos ese o cuatro" para el sulfúrico en sentido de nombrar lo que hay.

>incorrección de su uso por redundancia de información  
En orgánica IUPAC sí detalla explícitamente cuando algo hay que omitirlo y la redundancia es correcta. Por ejemplo [IUPAC detalla explícitamente cuando omitir localizadores](https://iupac.qmul.ac.uk/BlueBook/P1.html#1403) y el hecho de que no lo haga explícitamente en inorgánica para omitir el número de oxidación me sugiere que no tiene por qué haber "incorrección por ser redundante"  

Por mi parte tras ver todo esto lo que no creo es que a partir de ahora pueda ni deba dar por malo óxido de aluminio(III).  

En agosto 2022 planteo una encuesta  
[twitter FiQuiPedia/status/1563524427219800064](https://twitter.com/FiQuiPedia/status/1563524427219800064)  
¿Darías por erróneo nombrar BaO2 como peróxido de bario (II) sin omitir el (II) aunque Ba "tenga solo" un número de oxidación?
(Sigue encuesta con otra pregunta)  
En la respuesta anterior, tu respuesta se ha basado en lo que has visto/se ha hecho siempre o en normativa (en este caso responde citando/enlazando algo que confirme que la normativa respalda tu respuesta anterior)  
Resultados (en 2ª puse tiempo diferente sin querer).  
1/3 no consideran erróneo poner número oxidación aunque sea único, 1/3 basa su respuesta en normativa, que indica que se PUEDE omitir, no obliga a omitirlo.  
Peróxido de bario(II) es correcto.  
![](https://pbs.twimg.com/media/FbQgtVfWAAAtRNp?format=jpg)  

Me citan la ponencia de Andalucía  
[twitter rcabreratic/status/1563620802339262466](https://twitter.com/rcabreratic/status/1563620802339262466)  
[Aquí las directrices que seguimos para nombrar los compuestos según la ponencia selectividad o PEvAU.](https://upo.es/cms1/export/sites/upo/ponencia_quimica/noticias/documentos/PONENCIA_DE_QUxMICA-NOMENCLATURA_2005.pdf)  
Según las recomendaciones de nuestra ponencia sería un error (pag 5).  

En ese momento interpreto que no consideran error
[twitter FiQuiPedia/status/1563631566823505920](https://twitter.com/FiQuiPedia/status/1563631566823505920)  
"Para la ponencia es válido cualquier sistema aceptado por la IUPAC, no obstante utilizará el sistema de nomenclatura de las columnas en gris"  
En gris indica peróxido de Bario sin (II), pero no indica que con (II) sea error si lo acepta/no prohíbe IUPAC.  

En septiembre me comentan que sí indica explícitamente la ponencia que es error  
[twitter adelcacho/status/1567556312799510529](https://twitter.com/adelcacho/status/1567556312799510529)  
> ¿Es correcto nombrar al Al(OH)<sub>3</sub> como hidróxido de aluminio(III)? No es correcto. El corrector dará la respuesta por incorrecta. En este caso no es necesario indicar el estado de oxidación del aluminio. La respuesta correcta es hidróxido de aluminio.  

[twitter FiQuiPedia/status/1567583340626153473](https://twitter.com/FiQuiPedia/status/1567583340626153473)  
Gracias. Ahí es claro.  
Entiendo que ese documento de Andalucía 2011 sea el que haya que aplicar allí, pero me quedo y aplicaré el documento de 2016 de @RSEQUIMICA y la ausencia de obligación de IUPAC.  
Intentaré que alguien ponencia lo argumente  

### Paréntesis
Aplica a evitar ambigüedad  

En Red Book IUPAC 2005

> IR-2.2.3Parentheses  
IR-2.2.3.1Use in formulae  
Parentheses are used in formulae in the following ways.  
(a) To enclose formulae for groups of atoms (the groups may be ions, substituent groups, ligands or molecules), to avoid ambiguity or when the group is being multiplied. In the latter case, a multiplicative subscript numeral follows the closing parenthesis. In the case of common ions such as nitrate and sulfate, parentheses are recommended but not mandatory.  

Al indicarse que se recomienda sin ser obligatorio en nitratos y sulfatos, se puede entender que es obligatorio en el resto de casos poner paréntesis cuando hay ambigüedad.  
Un ejemplo sería  
PbO<sub>2</sub> óxido de plomo(IV)  
Pb(O<sub>2</sub>)  peróxido de plomo(II)  

### <a name="prioridades-grupos-funcionales-organica"></a>Prioridades grupos funcionales orgánica

**1993**  
Sufijos y prefijos:  [IUPAC 93, Table 5 Suffixes and prefixes for some important characteristic groups in substitutive nomenclature](http://www.acdlabs.com/iupac/nomenclature/93/r93_296.htm)  

Prioridades: en normas 1993 van de 1 a 21, y éteres tienen prioridad 20 [IUPAC 93, Table 10 General classes of compounds in decreasing order of priority for choosing and naming a principal characteristic group](http://www.acdlabs.com/iupac/nomenclature/93/r93_326.htm)  


**2013**  
Sufijos: [Table 3.3 Basic preferred and preselected suffixes, in decreasing order of seniority for citation as the principal characteristic group (preferred suffixes are those that contain a carbon atom)](https://iupac.qmul.ac.uk/BlueBook/P3.html#33)  
P-33.2.2 Derived preferred and preselected suffixes  
 por ejemplo se indica   
–(C)O-OH	oic acid (preferred suffix)  

Prefijos: [P-35 PREFIXES CORRESPONDING TO CHARACTERISTIC GROUPS](https://iupac.qmul.ac.uk/BlueBook/P3.html#35)  

Prioridades: en normas 2013 separado en dos tramos de 1 a 20  y de 20 a 43 que indica "Classes denoted by the senior atom in heterane nomenclature" y éteres tienen prioridad 41 [Table 4.1 General compound classes listed in decreasing order of seniority](https://iupac.qmul.ac.uk/BlueBook/P4.html#41)  

Al hablar de prioridades en normas 2013 aparecen varias prioridades, pero las importantes son las de tabla [Table 4.4 Complete list of suffixes and functional replacement analogues (when present) for IUPAC preferred names, in decreasing order of seniority](https://iupac.qmul.ac.uk/BlueBook/P4.html#43) (las mismas que aparecen en documento resumen "Brief Guide"), no las de tabla [Table 4.1 General compound classes listed in decreasing order of seniority](https://iupac.qmul.ac.uk/BlueBook/P4.html#41) que va de 1 a 20 para sufijos y 21 a 43 para senior element, ya **IUPAC indica que se usan prioridades de grupos expresados como sufijos**  (ver en prioridades)  

Sin citar éteres en tabla Table 3: Seniority order for characteristic groups de 4 CHARACTERISTIC GROUPS – Suffixes and prefixes indica "Note that, for nomenclature purposes, C-C multiple bonds are not considered to be characteristic groups (Section 5.4)"  	 

La "heterane nomenclature" se comenta 
[P-11 SCOPE OF NOMENCLATURE FOR ORGANIC COMPOUNDS](https://iupac.qmul.ac.uk/BlueBook/P1.html)  
> The ending ‘ane’, characteristic of alkanes, was borrowed from methane, ethane, etc., and attached to terms forming the roots of the names of the various elements, for example sulfane, H2S; phosphane, PH3; silane, SiH4; alumane, AlH3. The resulting names constitute the basis of substitutive nomenclature; this treatment of parent hydrides is called generalized ‘ane’ nomenclature because all the rules applicable to alkanes are applicable to all hydrides of the elements of Groups 13, 14, 15, 16, and 17. The nomenclature of the carbon hydrides may be conveniently termed ‘carbane nomenclature’; whereas **the term ‘heterane nomenclature’ covers the hydrides of elements other than carbon**. Names of mononuclear parent hydrides are listed in Table 2.1 in Chapter P-2.  


### Nomenclatura del grupo éter
Lo cito en "Grupos funcionales en orgánica" (la prioridad del grupo éter pasa de 20 en 1993 a "41"/desaparecer en 2013) y en "Nomenclatura de Química Orgánica, Recomendaciones 2013: cadena principal" ya que se cita "senior element"   

Nombrar terminado en -éter es nomenclatura de clase funcional (antes radicofuncional), que también es la que se usa en ésteres (lo que pasa es que en castellano no parece igual al no terminar en oato, ya que se dice -oato de -ilo cuando en inglés es -il -oate  
[Brief guide to the nomenclature of organic chemistry (IUPAC Technical Report)](https://www.degruyter.com/view/journals/pac/92/3/article-p527.xml)  

Functional class names (formerly radicofunctional names) are preferred for esters and acid halides. For other compound classes (e.g. **ethers**, ketones, sulfoxides, and sulfones) **functional class names are still in use, although substitutive names are preferred.**
 Functional class names consist of one or more substituent names, ordered alphabetically, and followed by the compound class name (separated with spaces as required).  
 
Así CH<sub>3</sub>-CH<sub>2</sub>-O-CH<sub>2</sub>-CH<sub>3</sub> en nomenclatura de clase funcional sería dietiléter, pero en sustitución, sería etoxietano, por lo que no tendría sufijo.  

Por revisar que traducción oficial oxy a castellano es a oxi. No sería oxo que es asociado a grupo C=O  

[1979 List of Radical Names Beginning from "M"](https://www.acdlabs.com/iupac/nomenclature/79/r79_1043.htm)  
Methoxy  

[1993 R-5.5.4 Ethers and chalcogen analogues](https://www.acdlabs.com/iupac/nomenclature/93/r93_430.htm)  
Examples to R-5.5.4.1 incluye 1-Chloro-2-ethoxyethane

En normas 2013 se puede ver cómo los casos de metoxi y etoxi son casos admitidos, pero de manera general es R-iloxi, no R-oxi:  
[2013 P-63 HYDROXY COMPOUNDS, ETHERS, PEROXOLS, PEROXIDES, AND CHALCOGEN ANALOGUES](https://iupac.qmul.ac.uk/BlueBook/P6.html#6302)  
Se puede ver el ejemplo de radical penthyloxy y ver que se indican nombres P-63.2.2.2 Retained names  
methoxy  
ethoxy  
propoxy  
butoxy  
phenoxy  

Si se usa nomenclatura de reemplazamiento (Replacement nomenclature, distinta de la de sustitución que es substitution nomenclature) también conocida como 'Nomenclatura "a"' se nombra como "oxa", por ejemplo P-44.3.1 se puede ver 2,5,8,11,14-pentaoxahexadecane  
[1979 Rule B-4. Replacement Nomenclature (also known as "a" Nomenclature)](https://www.acdlabs.com/iupac/nomenclature/79/r79_976.htm)  
[1993 R-1.2.2 Replacement operation](https://www.acdlabs.com/iupac/nomenclature/93/r93_145.htm)  

Si se usa oxa sería posible llamar a dimetiléter, metoximetano, CH3-O-CH3, como 2-oxapropano  
Pero no es habitual, se puede ver ejemplo  
[P-41 Seniority order for classes](https://iupac.qmul.ac.uk/BlueBook/P4.html#41)  
CH3-O-CH2-S-CH3  
methoxy(methylsulfanyl)methane (PIN)  
(C > ether and sulfide)  

[P-63.2 E THERS AND CHALCOGEN ANALOGUES](https://iupac.qmul.ac.uk/BlueBook/P6.html#6302)  
> In substitutive nomenclature, when R is different from R′, **RH is chosen as parent hydride** and R′-O– is cited as a
substituent to it. Names of these substituent groups are described in Section P-63.2.2. Functional class nomenclature
uses substituent group names for R and R′.  

Lo confuso es que en tabla 10 1993 indica prioridad 20, pero no parece haber un sufijo en nomenclatura de sustitución.  

En normas 2013 visto que grupo éter desaparece de prioridad 20 (aparece con 41, pero ya no tiene prioridad en lista de sufijos para  elegir cadena principal); a nivel prioridad está dentro de lo que en 1993 era "grupo g" de elementos que solo se pueden nombrar como prefijos, y se ordenan entre ellos por orden alfabético.  

Hay que recordar esto de normas 2013  
[P-44.0 INTRODUCTION](https://iupac.qmul.ac.uk/BlueBook/P4.html#44)  
> The selection of a preferred parent structure is based on the seniority of classes (see P-41), which gives priority first to **characteristic groups expressed as suffixes**   

[Brief Guide to the Nomenclature of Organic Chemistry - iupac.org](https://iupac.org/wp-content/uploads/2020/08/Organic-Brief-Guide-brochure_v1.0_06Feb2020.pdf)  
![](https://www.degruyter.com/document/doi/10.1515/pac-2019-0104/asset/graphic/j_pac-2019-0104_fig_001.jpg)  
Sin citar éteres en tabla Table 3: Seniority order for characteristic groups de 4 CHARACTERISTIC GROUPS – Suffixes and prefixes indica "Note that, for nomenclature purposes, C-C multiple bonds are not considered to be characteristic groups (Section 5.4)"  	

Por lo tanto son más prioritarios dobles enlaces que éteres (al menos en 2013, en normas 1993 hay cierta ambigüedad al estar éter en tabla de prioridades)  
Probando con software de ACDLabs en 2022 (aplica normas 2013):
   
CH2=C(O-CH3)-C(O-CH3)-CH=CH2  
2,3-dimethoxypenta-1,3-diene  

Dibujando nombra 4,4-dietenil-3,3,5,5-tetrametoxiheptano: parece que son más prioritarios éteres para elegir cadena principal, pero es que la cadena es mayor y es criterio 2013.  
Dibujando nombra 4-(1,1-dimetoxipropil)-4-etenil-5,5-dimetoxihept-2-eno: ante dos cadenas de longitud 7, una con 4 grupos éter pero sin dobles y y otra con 2 grupos éter y 1 enlace doble, se elige la cadena con el doble.  
Dibujando nombra 4-(1,1-dimetoxipropil)-4-etenil-5,5,6-trimetoxihept-1,2-dieno: ante dos cadenas de longitud 7, una con 5 grupos éter pero sin dobles y otra con 3 grupos éter y 2 enlaces dobles, se elige la cadena con los dobles.  
Dibujando nombra 4-(1,1-dimetoxipropil)-4-etenil-5,5,6-trimetoxihept-2-eno: ante dos cadenas de longitud 7, una con 5 grupos éter pero sin dobles y otra con 3 grupos éter y 1 enlace doble, se elige la cadena con el doble.  
Dibujando nombra 4-(1,1-dimetoxipropil)-5,5,6-trimetoxihept-2-eno: ante dos cadenas de longitud 7, una con 5 grupos éter pero sin dobles y otra con 3 grupos éter y 1 enlace doble, se elige la cadena con el doble.  

Un ejemplo gráfico aquí  
[twitter FiQuiPedia/status/1579060540617658368](https://twitter.com/FiQuiPedia/status/1579060540617658368)  
Duda orgánica: ¿cómo nombrar este compuesto?
Éter tiene: 
[prioridad 20 en normas 1993](http://acdlabs.com/iupac/nomenclature/93/r93_326.htm)  
[prioridad 41 en normas 2013](https://iupac.qmul.ac.uk/BlueBook/P4.html#41)  
Ante 2 opciones misma longitud (7), tomaría principal 5 grupos éter sin dobles  > 3 grupos éter y 1 enlace doble
![](https://pbs.twimg.com/media/FenxNrSWIAAdvsM?format=jpg)  
Software de @ACDLabs lo nombra como 4-(1,1-dimethoxypropyl)-5,5,6-trimethoxyhept-2-ene y entiendo da prioridad a insaturaciones.
Veo cierta inconsistencia en que sea un [grupo "nombrado solo como sufijo"](http://acdlabs.com/iupac/nomenclature/93/r93_322.htm) pero tenga una prioridad de grupo superior a dobles  

Antes de revisar esto indico "Yo lo nombrarla por sustitución como 2,3,3,5,5-pentametoxi-4-(prop-1-enil)heptano", pero el nombre correcto es el de ACDLabs 4-(1,1-dimethoxypropyl)-5,5,6-trimethoxyhept-2-ene. 
 
Mi resumen es que, en nomenclatura de sustitución y teniendo en cuenta que "RH is chosen as parent hydride", los éteres son sustituyentes y siempre se nombran como sufijos, así que tienen misma "prioridad" que ramas. La cadena principal debe priorizar dobles a sustituyentes como sufijos. 


### Formulación del grupo hidróxido
¿Si según IUPAC (ya antes de 2005) se escribe el hidrógeno a la izquierda en H<sub>2</sub>O, por qué se escribe a la derecha en el grupo hidróxido OH<sup>-</sup> ? Realmente debería ser HO<sup>-</sup>, pero por tradición se mantiene y se acepta  
En normas IUPAC 2005  
*IR-4.4.3. Formulae for specific classes of compounds*  
IR-4.4.3.1 Binary species...Note that the formula for the hydroxide ion should be HO<sup>-</sup> to be consistent with the above convention.  
y luego en tabla IX Names of homoatomic, binary and certain other simple molecules, ions, compounds, radicals and substituent groups aparece como HO<sup>-</sup>

### Nombre del grupo H<sub>3</sub>O<sup>+</sup>
IUPAC en 2005 (IR-5.3.2.4 Heteropolyatomic cations y IR-6.4.1 Cations derived from parent hydrides by addition of one or more hydrons) nombra por sustitución oxidanio, y admite oxonio, pero indica que ya no se puede nombrar hidronio.

### Formulación de la carga de los iones: es A<sup>n+</sup> y no A<sup>+n</sup>
Se indica en normas IUPAC 2005  
*IR-3.2 INDICATION OF MASS, CHARGE AND ATOMIC NMBER USING INDEXES (SUBSCRIPTS AND SUPERSCRITS)*  
...right upper index charge  
A charge placed on an atom of symbol A is indicated as A<sup>n+</sup> or A<sup>n-</sup>, not as A<sup>+n</sup> or A<sup>-n</sup>.  
Además en IUPAC Green Book (tercera edición, borrador 2012) se indica en  
2.10 General Chemistry  
Al<sup>3+</sup> or Al<sup>+3</sup>  
3S<sup>2-</sup> or 3S<sup>-2</sup>  
...  
Al3+ is commonly used in chemistry and recommended by \[74]. The forms Al+3 and S-2, although widely used, are obsolete \[74], as well as the old notation Al<sup>+++</sup>, S<sup>=</sup>, and S<sup>--</sup>.donde la referencia 74 son las normas IUPAC 2005

### Símbolo para disolución acuosa
En IUPAC Green Book (tercera edición, borrador 2012), apartado 2.10.1 Other symbols and conventions in chemistry, (vi) States of aggregation se indica "aq aqueous solution"  
El tema de "ac" parece asociado a la traducción "disolución acuosa", pero ¿es oficial?  
Aunque no es concluyente, simplemente confirmar que sí aparece indicado que puede ser aq ó ac en libros en español, por ejemplo  
[Nomenclatura de química orgánica, Francisco González Alcaraz, EDITUM, 1991](https://books.google.es/books?id=Q_Z8RrSUuu8C)  
[Química. La ciencia básica: la ciencia básica, DOMINGUEZ REBOIRAS, MIGUEL ANGEL, Editorial Paraninfo, 2006](https://books.google.es/books?id=QM-Gj2K2ZKYC)  

### Vocales en la nomenclatura
Realmente es un tema propio de cada idioma, y está definido para inglés. Se ponen algunos enlaces:  
En orgánica:  [IUPAC 93, R-0.1.7 Elision and addition of vowels](http://www.acdlabs.com/iupac/nomenclature/93/r93_81.htm)  
En inorgánica se menciona en libro rojo 2005, IR-5.2 "The final vowels of multiplicative prefixes should not be elided (although ‘monoxide’, rather than ‘monooxide’, is an allowed exceptionbecause of general usage)."

En inorgánica, traducción oficial de libro rojo al castellano

> IR-2.7 ELISIONES  
> En general, no se hacen elisiones en las nomenclaturas de adición y de composición cuando se usan prefijos multiplicadores.  
> Ejemplo:  
> 1. tetraacuo ( no es tetracuo)  
> 2. monooxígeno ( no es monoxígeno)  
> 3. hexaóxido de tetraarsénico  
> Sin embargo, una excepción permitida es monóxido, en lugar de monoóxido debido a su uso generalizado.

Relacionado, a veces surge la duda de si usar aserniuro o asernuro, antimoniuro o antimonuro, antimonioso o antimonoso

>  R-5.3.3.2 Aniones monoatómicos
> El nombre de un anión monoatómico es el nombre del elemento (Tabla I) modificado para que lleve el indicador de anión ‘-uro’, que se forma, o bien reemplazando la terminación del nombre del elemento (‘eso’, ‘ico’, ‘io’, ‘o’, ‘ógeno’, ‘ígeno’, ‘ono’, u ‘oro’) por ‘uro’, o añadiendo directamente la terminación ‘-uro’ al nombre del elemento.  

Se indican algunos ejemplos que se ve que -io cambia por -uro

> 4. wolframio, wolframuro  
> 6. sodio, soduro  


>El nombre de todos los elementos, modificados de esta manera, se encuentra en la Tabla IX.

Esto también aplica a -ato: se puede ver antimonoso en documento Salvador Olivares, IES Floridablanca  
Aunque no sale en ejemplos, sería arsenato y no arseniato.  

En documentos breves 2020 traducidos al castellano sí se cita la omisión de vocales y se hace en los ejemplos  
[Guía Breve para la Nomenclatura en Química Orgánica - iupac.org](https://iupac.org/wp-content/uploads/2021/12/Guia-breu_CAT_7es_2_20211215.pdf)  

Dentro de apartado 5.2 
>...y la vocal “a” se elide cuando va seguida de una vocal.  

Aunque esto está en apartado 5.2 Hidruros progenitores monocíclicos   
En apartado 5.1 Hidruros progenitores acíclicos  
> Los nombres de las cadenas saturadas de carbono (alcanos) se componen del término numérico simple que indica el número de átomos de carbono (Tabla 2, **con la “a” elidida**) ... 

Como es una traducción parcial / una guía breve, es orientativo.  
Personalmente no considero error poner o dejar de poner esa vocal, pero parece ser que se puede extrapolar un criterio para el castellano de los ejemplos, que es no omitir la vocal si lo siguiente va a ser una consonante.    

Ejemplos:  
2-aminoetan-1-ol  (omitida o, no es 2-aminoetano-1-ol)  
7-bromo-6-hidroxiheptano-2,4-diona  (no omitida o, no es 7-bromo-6-hidroxiheptan-2,4-diona)  
3-etilocta-1,4-dieno  (no omitida a, no es 3-etiloct-1,4-dieno)  
1-(3-hidroxibutoxi)butan-1-ol  
3-\[(but-2-en-2-il)oxi]but-1-eno  
2-(1-bromo-2-hidroxietoxi)-2,2-dicloroetan-1-ol  


### Uso de prefijo mono: superfluo en binarios según IUPAC
El uso del prefijo mono en inorgánica a veces es confuso respecto a poder omitirse  
En inorgánica se menciona en libro rojo 2005, IR-5.2  
"Stoichiometric names may correspond to the empirical formula or to a molecular formula different from the empirical formula (compare Examples 3 and 4 below).  
Examples:  
1. HCl hydrogen chloride  
2. NO nitrogen oxide, or nitrogen monooxide, or nitrogen monoxide  
3. NO<sub>2</sub> nitrogen dioxide  
4. N<sub>2</sub>O<sub>4</sub> dinitrogen tetraoxide  
5. OCl<sub>2</sub> oxygen dichloride  
6. O<sub>2</sub>Cl dioxygen chloride  
7. Fe<sub>3</sub>O<sub>4</sub> triiron tetraoxide  
8. SiC silicon carbide  
9. SiCl<sub>4</sub> silicon tetrachloride  
10. Ca<sub>3</sub>P<sub>2</sub> tricalcium diphosphide, or calcium phosphide  
11. NiSn nickel stannide  
12. Cu<sub>5</sub>Zn<sub>8</sub> pentacopper octazincide  
13. Cr<sub>23</sub>C<sub>6</sub> tricosachromium hexacarbide  
Multiplicative prefixes need not be used in binary names if there is no ambiguity about the stoichiometry of the compound (such as in Example 10 above). **The prefix ‘mono’ is, strictly** speaking, superfluous and is only needed for emphasizing stoichiometry when discussing compositionally related substances, such as Examples 2, 3 and 4 above."  

Las recomendaciones de RSEQ de 2016 dicen en mi opinión algo confuso  

Página 16  
> El prefijo mono (1) no suele utilizarse salvo que su ausencia conduzca a confusiones. Por ejemplo, óxido de carbono no define exactamente un compuesto, ya que se conocen varios óxidos de carbono. Para evitar esta ambigüedad, CO debe llamarse monóxido de carbono. No son admisibles las contracciones salvo monóxido. Por ejemplo: pentaóxido es correcto y pentóxido es incorrecto.

Página 45  
> Utilización del prefijo mono
En el Libro Rojo (p. 71) se puede leer:  
_“El prefijo “mono” es, hablando estrictamente, superfluo y se necesita solamente para enfatizar la estequiometria cuando se comentan sustancias relacionadas por la composición”._  
Aclarando lo anterior se indica que el NO puede ser nombrado como óxido de nitrógeno, pero también como monóxido de nitrógeno ya que hay varios óxidos de nitrógeno más (sustancias relacionadas por la composición). Siguiendo este razonamiento el CO puede nombrarse como
monóxido de carbono o como óxido de carbono, el FeO como monóxido de hierro o como óxido de hierro, pero el Na2O se nombraría como óxido de disodio, no monóxido de disodio.  
No obstante esta norma, combinada con la que también aparece en la misma página 71, puede provocar algunas dudas:  
“Los prefijos multiplicadores no son necesarios en los nombres binarios si no hay ambigüedad sobre la estequiometría del compuesto”. (Ca3P2: difosfuro de tricalcio, o fosfuro de calcio).  
El fosfuro de calcio podría conducir a la fórmula CaP, considerando que el prefijo mono no se ha utilizado. Lo mismo ocurre con el óxido de aluminio (trióxido de dialuminio) o el óxido de sodio y otros (metales alcalinos, alcalino térreos, plata, cinc o aluminio) que de manera generalizada se considera que tienen un estado de oxidación “único”. No obstante, esto tampoco es cierto ya que se conocen algunos óxidos distintos de los que se tratan comúnmente. Por ejemplo el AgO. En este caso el óxido se nombra como monóxido de plata.  
Salvador Olivares \[2,4,5] propone para estos casos lo siguiente:  
_“… hay elementos para los que se deben de sobreentender determinados números de oxidación únicos cuando no hay ningún prefijo, pero si hay algún prefijo, estos mandan”._ 
Ejemplo:  
Al2O3: trióxido de dialuminio (nomenclatura preferida), óxido de aluminio (se considera que no hay ambigüedad); pero AlO: monóxido de aluminio.  


La idea de la “sistemática” (asociada a la nomenclatura de composición estequiométrica con prefijos) es que según se lee nombre se escribe fórmula y según se lee fórmula se escribe nombre.  
Lo que hace RSEQ (no IUPAC) de poner una regla para añadir o no mono según números de oxidación es desvirtuar la “sistemática”, porque implicaría que para nombrar por sistemática habría que conocer los estados de oxidación, que es precisamente lo que intenta evitar.  
La indicación de añadir monóxido de plata por el argumento de ser compuestos “que se tratan comunmente” hace referencia a añadir el mono en “sistemática” para evitar que haya ambigüedad **en caso de no indicar el tipo de nomenclatura del nombre**, pero supone “nombrar con prefijos no siguiendo reglas fijas según la fórmula sino dependiente de variantes existentes que dependen del número de oxidación”, y se considera desvirtuar la “sistemática”.  
La idea de omitir los prefijos en nomenclatura con prefijos, algo que permite IUPAC, si no hay ambigüedad sobre la estequiometría del compuesto” se considera de nuevo desvirtuar la “sistemática”  

### Sustituyentes dobles en orgánica
¿Cómo se llama la ramificación si está enlazada a la cadena principal con un doble enlace?  
[1979 Acyclic Hydrocarbons Rule A-4. Bivalent and Multivalent Radicals](https://www.acdlabs.com/iupac/nomenclature/79/r79_78.htm#a_4__1)  
The name "methylene" is retained for the radical CH<sub>2</sub>=
Methylidyne (triple)  
Ethylidene  (doble) 	
Ethylidyne (triple)  

[1979 List of Radical Names Beginning from "M"](https://www.acdlabs.com/iupac/nomenclature/79/r79_1043.htm)  
Methylene (doble)   
Methylidyne (triple)  

[IUPAC 93, R-2.5 Substituent Prefix Names Derived from Parent Hydrides](http://www.acdlabs.com/iupac/nomenclature/93/r93_271.htm)  
[R-9.1 Trivial and semisystematic names retained for naming organic compounds](https://www.acdlabs.com/iupac/nomenclature/93/r93_671.htm)  
[IUPAC 93 Table 19(b) Acyclic and monocyclic hydrocarbons. Substituent groups.](https://www.acdlabs.com/iupac/nomenclature/93/r93_682.htm)  
Methylene  
[Substituent. Methane substituents - en.wikipedia.org](https://en.wikipedia.org/wiki/Substituent#Methane_substituents)  
[Methylen group - en.wikipedia.org](https://en.wikipedia.org/wiki/Methylene_group)  

[2013 P-57 SELECTING PREFERRED AND PRESELECTED PREFIXES FOR SUBSTITUENT GROUPS](https://iupac.qmul.ac.uk/BlueBook/P5.html#57)  
methylidene  

[2013 ](https://iupac.qmul.ac.uk/BlueBook/P6.html#60)  
> Names that were recommended in the past but are not included in these recommendations are described parenthetically by the phrase ‘no longer recommended’. For example, the prefix ‘methylene’ is ‘no longer recommended’ in IUPAC nomenclature to designate the =CH2 group. For example, **the prefix ‘methylene’ is ‘no longer recommended’ in IUPAC nomenclature to designate the =CH<sub>2</sub> group.**  


### Dobles y triples enlaces al tiempo en orgánica: prioridad y nombre
A veces hay confusión: como dobles tienen prioridad sobre simples se asume erróneamente que triples tienen prioridad frente a dobles.Hay dos temas:
   * Tanto dobles como triples están en el mismo lote y tienen la misma prioridad dentro de la prioridad "decreciente por lotes", con letra g  
   [Guide to Name Construction, R-4.1 General Principles](https://www.acdlabs.com/iupac/nomenclature/93/r93_317.htm)  
   ...Insofar as the preceding rules leave a choice, the starting point and direction of numbering of a compound are chosen so as to give lowest locants to the following structural features (if present) considered successively in the order listed until a decision is reached. ...(f) unsaturation (ene/yne)
   * En caso de igualdad entre dobles y triples, tienen prioridad los dobles  
 [Characteristic (Functional) Groups, R-3.1.1 Suffixes denoting multiple bonds](https://www.acdlabs.com/iupac/nomenclature/93/r93_280.htm)  
 Numbers as low as possible are given to double and triple bonds as a set, even though this may at times give "-yne" a lower number than "-ene". If a choice remains, preference for low locants is given to the double bonds. 
   * Si hay al tiempo dobles y triples en la misma cadena son "-eninos", no "-inenos"  
 [Characteristic (Functional) Groups, R-3.1.1 Suffixes denoting multiple bonds](https://www.acdlabs.com/iupac/nomenclature/93/r93_280.htm)  
 The presence of both double and triple bonds is similarly denoted by endings such as "-enyne"

### Nombre y símbolo oficial del radical CH<sub>6</sub>-CH<sub>5</sub>-
Al radical asociado a que el benceno pierda un H se llama fenilo (bencenilo) y se suele representar como Ph-  
En wikipedia se indica que también se representa como φ  
Me gustaría encontrar una referencia oficial actual de IUPAC: he visto esta pero es de 1979  
[List of Radical Names Beginning from "P"](https://www.acdlabs.com/iupac/nomenclature/79/r79_1045.htm)  
Phenyl ![](https://www.acdlabs.com/iupac/nomenclature/79/images/bm24760.gif "")  

[Monocyclic Hydrocarbons, Rule A-13. Substituted Aromatic Radicals](https://www.acdlabs.com/iupac/nomenclature/79/r79_62.htm#a_13__1) 

### Cómo es posible que tras IUPAC 2005 en inorgánica se pueda usar permanganato pero no permangánico, cromato pero no crómico?
Las normas IUPAC 2005 no citan los nombres tradicionales oxoácidos de metales (Cr y Mn) en la tabla de nombres vulgares aceptados, así que no pueden ser utilizados, sin embargo las normas IUPAC 2005 sí citan nombres de oxoaniones tradicionales aceptados e incluyen cromato, dicromato y permanganato, así que esos sí pueden ser utilizados.  
...However, a number of the existing ‘acid’ names are so commonly used (sulfuric acid, perchloric acid, etc.) that it would be unrealistic to suggest replacing them altogether by systematic alternatives.  
...  
to list the ‘acid’ names that are still acceptable due to common usage and/or because they are needed in organic nomenclature (see Tables IR-8.1 and IR-8.2).  
...  
IR-8.4 ...Names such as permanganic acid, dichromic acid, etc., are not included in the present recommendations because they represent an area where it is difficult to systematize and decide what to include, and where the names are not needed for organic nomenclature, as opposed to the corresponding ‘acid’ names for acids of main group elements.  

La Real Sociedad Española de Química confirma que es así en su documentación.  
Únicamente se permiten los especificados (ver lista). De esta manera no se admiten nombres como ácido crómico o ácido permangánico.  
Ejemplos de errores  
...Presentar como aceptados un gran número de nombres vulgares que no lo están (ácido mangánico, ácido crómico,  
... el H<sub>2</sub>CrO<sub>4</sub> se puede derivar igual y no se llama ahora ácido crómico  
...Ya he dicho que H<sub>2</sub>CrO<sub>4</sub> no puede llamarse ácido crómico ni HMnO4 llamarse ácido permangánico  

Por si alguien hace la misma pregunta que me hizo una química a que se lo comenté: "**¿entonces, como se explica el nombre de permanganato si no viene de ácido permangánico?**"  
La respuesta es, según lo que dice la IUPAC, que los nombres vulgares aceptados no tienen según IUPAC ninguna regla reconocida para formar el nombre, IUPAC simplemente acepta los nombres que pone en las tablas tal cual los pone porque ese nombre ya existía y lo considera de uso común, como ocurre con amoniaco y agua, aunque en algunos casos "haya/se quieran ver reglas" para obtenerlos. Así que si IUPAC tiene en la tabla permanganato pero no ácido permangánico es lo que hay ...el currículo remite a normas IUPAC, así que es "lo que diga la IUPAC"

### Ácido hipofluoroso 

El compuesto HFO [aparece en wikipedia con nombre ácido hipofluoroso](https://en.wikipedia.org/wiki/Hypofluorous_acid), pero no está incluido en tabla IR8.1 Acceptable common names and systematic (additive) names for oxoacids and related structures

### Oxidano frente a agua y azano frente a amoniaco. La IUPAC no está loca.
Lo comento en mis apuntes  
Como lo he oído varias veces, aclaro que **la IUPAC no está loca y los nombres de amoniaco y agua por supuesto que se mantienen**; en las normas de 2005 oxidano y azano se proponen para nombrar derivados por sustitución (tabla IR-6.1 nota b). Son “hidruros progenitores” más que “tradicional”

### Haluros de oxígeno. ¿Ese cambio de normas IUPAC 2005 tiene algún sentido?
Las normas IUPAC 2005 tienen un cambio en binarios que afecta a fórmulas además de nombres: ya no existen óxidos de grupo 17, sino que son halogenuros/haluros de oxígeno.Hay quien no le ve sentido, pero intento comentar cómo lo veo (yo no soy químico) y que para mi sí tiene sentido de cara a formular y nombrar.  

Antes de las normas de 2005, la decisión de qué elemento se colocaba a la derecha o a la izquierda estaba definida de una manera **a medida** "según la práctica establecida"    
[NOMENCLATURE OF INORGANIC CHEMISTRY. SECOND EDITION. DEFINITIVE RULES 1970, página 20](http://publications.iupac.org/pac/pdf/1971/pdf/2801x0001.pdf#page=20)  
2.161—In the case of binary compounds between non-metals, in accordance with established practice, that constituent **should be placed first which appears earlier in the sequence**:  
Rn, Xe, Kr, B, Si, C, Sb, As, P, N, H, Te, Se, S, At, I, Br, Cl, O, F.  
[NOMENCLATURE OF INORGANIC CHEMISTRY. SECOND EDITION. DEFINITIVE RULES 1970, página 21](http://publications.iupac.org/pac/pdf/1971/pdf/2801x0001.pdf#page=21)  
2.22—If the electronegative constituent is monoatomic or homopolyatomic its name is modified to end in -ide. For binary compounds the name of the element standing later in the sequence in 2.161 is modified to end in -ide. **Elements other than those in the sequence of 2.161 are taken in the reverse sequence of Table IV** and the name of the element occurring last is modified to end in -ide.  If one element occurs only in Table IV and the other occurs also in the sequence of 2.161, that from the sequence of 2.161 is modified to end in -ide.  
[NOMENCLATURE OF INORGANIC CHEMISTRY. SECOND EDITION. DEFINITIVE RULES 1970, página 110](http://publications.iupac.org/pac/pdf/1971/pdf/2801x0001.pdf#page=110)  
Table IV. ELEMENT SEQUENCE  

En las normas de 2005, se define de manera unificada y más simple, usando una "electronegatividad de convenio según tabla VI". 
*IR-4.4.2 Ordering principles*  
*IR-4.4.2.1 Electronegativity*  
*If electronegativity is taken as the ordering principle in a formula or a part of a formula, the *atomic symbols are cited according to relative electronegativities, the least electronegative*element being cited first. For this purpose, Table VI* is used as a guide. By convention, the *later an element occurs when the table is traversed following the arrows, the more*electropositive is the element.*  

Esa nueva tabla VI se puede ver como una "sistematización" de la tabla IV de las normas anteriores, de modo que siempre se usa un único criterio (IUPAC mantiene excepciones, se puede ver comentario sobre formulación grupo hidróxido)  
Es ese convenio el que convierte a compuestos que anteriormente eran óxidos (por ejemplo Br<sub>2</sub>O en haluros OBr<sub>2</sub>)  
Intento responder algunas objeciones:  
- Eso está mal porque los compuestos con oxígeno son óxidos de toda la vidaComo indico en mis apuntes  
*COMPUESTOS BINARIOS: XxYy (formados solamente por dos tipos de elementos)*  
*A veces se dividen en [sub]categorías: hidruros (metálicos, volátiles...), hidrácidos, óxidos, sales (neutras, volátiles)... pero aquí se da visión global.*  
Creo que en muchos libros cuando se enseña formulación de binarios se enseñan subcategorías, pero al menos yo no lo hago así: "Yo enseño binarios. (punto)"  
Todos se nombran de manera unificada "lo de la derecha de lo de la izquierda" y lo explico: "¿Qué es un óxido? Lo que, al seguir el convenio de tabla VI, tiene el oxígeno a la derecha y al nombrarlo usamos la palabra óxido".  
Se puede pensar también ¿Por qué antes no era un óxido OF2 pero sí lo era Br2O, si ambos son combinaciones de O con halógeno? La respuesta es porque el F es más electronegativo en escala Pauling que O y el O lo es más que el Br (que en cierto modo intentaba reflejarse en la regla a medida de normas 1970)... pero el tema es que la IUPAC ha cambiado la ordenación para ser más sistemática, simplemente recorriendo la tabla VI de normas 2005.  
Para mi el objetivo de la formulación es nombrar y formular de manera sistemática, que con nombre podamos identificar fórmula y viceversa, no saber propiedades del compuesto, y el nuevo convenio de IUPAC creo que lo hace bien. Creo que una persona que no tenga sesgo / inercia (igual yo mismo que no soy químico), puede visualizar la simplificación.  
- Eso está mal porque deja de tener sentido la nomenclatura Stock para los haluros de oxígeno, ya que no siempre está el que actúa como metal a la izquierda.  
Pues sí, deja de tener sentido la nomenclatura indicando el número de oxidación del que actúa como metal con notación Stock, pero no siempre le veo sentido en general a esa notación, más allá de reconocer a nivel histórico la nomenclatura de documentos anteriores o de etiquetas reactivos.  
Br2O era óxido de bromo (I), y ahora OBr<sub>2</sub> sería "algo así" como "bromuro (I) de oxígeno": yo personalmente digo que no tiene sentido la notación Stock en esos casos.  
Como indico en mis apuntes asociado a Stock  
*Ejemplos para reflexión final: ¿nombre tradicional AuPO<sub>3</sub>, CuFeS<sub>2</sub>, PbO<sub>2</sub>? ¿Fosfito de oro (III) ó metafosfato de oro (I), Sulfuro de cobre (II) y hierro (II) ó de cobre (I) y hierro (III), Óxido de plomo (IV) o Peróxido de plomo (II)? Para mí muestran lo "absurdo" de poner en el nombre el estado de oxidación; realmente darlo no da información sobre la fórmula, y la nomenclatura sistemática es más útil en dar un nombre que describa la fórmula.*  
- Considerarlos haluros de oxígeno rompe con el sistema de formación de ácidos oxacidos y sales terciarias  
No lo veo: la formación de oxácidos se puede hacer de varias maneras, y las reacciones estequiométricamente serán correctas mientras el compuesto binario tenga la misma cantidad de oxígenos y de halógenos.  
Comento en mis apuntes  
*Historia: la mayor parte de los oxácidos se pueden obtener a partir del óxido del elemento central combinado con agua. Es un criterio que usa la nomenclatura tradicional, “a efectos de formulación”, porque además puede haber otros procesos de obtención (como dimerización). Antiguamente se llamaban “óxidos anhidros”/”anhídridos”, indicando que no tenían agua: eso está desaconsejado por IUPAC desde 1971, que pretende que el nombre de un compuesto no tenga información el método de obtención (puede haber varios), ni sus propiedades (intenta evitar usar “ácido” en sistemática).*

### Orden alfabético de los sustituyentes en orgánica 

En general es por orden alfabético de "la raíz": por ejemplo metil va antes que propil. Los mutiplicadores no afectan, si fuese dipropil, metil iría antes que dipropil. En el caso de "ramas de ramas" es distinto, cito párrafos exactos de normativa IUPAC que lo dicen. 

[Substitutive Nomenclature. General Procedure Rule C-16.3 (1979)](https://www.acdlabs.com/iupac/nomenclature/79/r79_883.htm)  

>16.31 - Simple prefixes (that is, those for unsubstituted substituents and for atoms) are first arranged alphabetically; multiplying affixes (if necessary) are then inserted and do not alter the alphabetical order already attained.  
>...  
>16.34 - The name of prefix for a substituted substituent is considered to begin with the first letter of its complete name.  
>...  
>(Dimethylpentyl as a complete substituent is alphabetized under "d").  


[Guide to Name Construction. R-4.1 General Principles (1993)](https://www.acdlabs.com/iupac/nomenclature/93/r93_317.htm)  
>(g) assemble the components into a complete name, using alphabetical order for all substitutive prefixes.
[Introduction. R-0.1.8 Order of prefixes (1993)](https://www.acdlabs.com/iupac/nomenclature/93/r93_93.htm)  
>(a) Simple prefixes (i.e., those describing atoms and unsubstituted substituents) are arranged alphabetically; multiplying affixes, if necessary, are then inserted and do not alter the alphabetical order already established.  
>(b) The name of a prefix for a substituted substituent is considered to begin with the first letters of its complete name.  

## Uso de guiones en orgánica  

[R-0.1.3 Punctuation (1993)](https://www.acdlabs.com/iupac/nomenclature/93/r93_45.htm)  
>Punctuation in chemical names is frequently of great importance, especially to avoid ambiguity.  
>R-0.1.3.1 Commas are used to separate locants that refer to the same part of a name, i.e., locants of a series, and to separate letters or letter combinations that denote fusion sites in names of fused ring systems.  
>...  
R-0.1.3.4 Hyphens separate:  
>(a) locants from the words or syllables of a name;  
>(b) adjacent locants referring to different parts of the name (but preferably parentheses should be inserted);  
>(c) the two parts of the designation for a primary fusion site in a name for a fused ring system;  
>(d) a stereodescriptor and the name.  

### Temas varios sobre nomenclatura no solamente asociados IUPAC


#### Estados de oxidación
Un tema muy importante de cara a formular es el tema de los estados de oxidación, por lo que le dedico  [una página propia](/home/recursos/quimica/estados-de-oxidacion) .

#### RAE, nombres, definiciones, tildes ...
A veces la IUPAC dice una cosa y la RAE otra (los nombres en castellano de los 4 elementos nombrados en 2016 fueron nombrados inicialmente de manera distinta)  
En RAE existe tanto yodo como iodo  [rae.es yodo](https://dle.rae.es/yodo) 
 En RAE no existe ión, sí ion  [rae.es ion](https://dle.rae.es/ion)  
 En la traducción de 2007 al español de las normas IUPAC 2005 se usa yoduro y se cita ion sin tilde  
 RAE admite amoníaco y amoniaco (con y sin tilde)  [rae.es amoniaco](https://dle.rae.es/amoniaco)  
 RAE admite cinc y zinc  [rae.es cinc](https://dle.rae.es/cinc) 

#### RAE y definición nucleido
En 2014 envío este correo a unidrae@rae.es  

> La definición actual de nucleido  
[rae.es nucleido](https://dle.rae.es/nucleido)  
nucleido.(De núcleo e ‒́ido).1. m. Fís. Núcleo atómico caracterizado por contener igual número de protones que de neutrones.  
Quería comentar que esa definición es incorrecta:  
La IUPAC (International Union for Pure and Applied Chemistry) sobre el término asociable "nucleide" dice que es preferible "nuclide"  
[old.iupac.org ACD V7_karol N](https://web.archive.org/web/20110811104428/http://old.iupac.org/ACD/V7_karol/N.html) *waybackmachine* Núclido no está en el diccionario, pero IUPAC lo define  
[goldbook.iupac.org nuclide](http://goldbook.iupac.org/N04257.html)  
Así que mi propuesta sería:  
- nucleido debería remitir a núclido  
- núclido: "Una especie atómica caracterizada por su número másico, número atómico, y estado de energía nuclear, siempre que la vida media de este estado sea lo suficientemente larga para ser observable"  


En 2016 la definición de nucleido es actualizada y similar a mi propuesta  
[rae.es nucleido](http://dle.rae.es/?id=QhKS4op)  
> *Especie atómica caracterizada por su número másico, su número atómico y el estado energético de su núcleo y con una vida media, en este estado, suficientemente larga para que sea observable.*  

Las definiciones antiguas se pueden consultar a través del  [Nuevo tesoro lexicográfico de la lengua española](http://ntlle.rae.es/ntlle/SrvltGUISalirNtlle) , y para 1992 era "Cuerpo simple cuyos átomos no solo tienen el mismo número de protones nucleares, sino también el mismo número de neutrones"

### Nombres de algunos elementos
Con la nomenclatura en 2016 de los elementos 113, 115, 117 y 118 surgió una confusión entre RAE, Fundeu y RSEQ.  
La RAE por error propuso nombre antes de contar con RSEQ.  
15 diciembre 2016  
[twitter FiQuiPedia/status/813776139982356480](https://twitter.com/FiQuiPedia/status/813776139982356480)  
[twitter FiQuiPedia/status/809402557760204800](https://twitter.com/FiQuiPedia/status/809402557760204800)  
Finalmente se aclararon.  
[lavozdegalicia.es Elementos químicos](https://www.lavozdegalicia.es/noticia/opinion/2017/07/01/elementos-quimicos/0003_201707G1P15992.htm)  

Asociado a Wolframio, hay polémica por cambio de criterio IUPAC en 2005.  
Aparece en algunos documentos posteriores que citan nombres  
[rseq.org Nombres y símbolos en español de los elementos aceptados por la IUPAC el 28 de noviembre de 2016 acordados por la RAC, la RAE, la RSEQ y la Fundéu](https://rseq.org/mat-didacticos/nombres-y-simbolos-en-espanol-de-los-elementos-aceptados-por-la-iupac-el-28-de-noviembre-de-2016-acordados-por-la-rac-la-rae-la-rseq-y-la-fundeu/)  
*7. Dar como preferida la denominación wolframio (va*riante volframio) para el elemento químico de número atómico 74, a pesar de que el nombre establecido en inglés por la IUPAC sea tungsten (español tungsteno). La RSEQ reivindica esa denominación por estar basada en el nombre que le dieron quienes primero aislaron este elemento, los hermanos Delhuyar, químicos riojanos.*  

Se pueden ver artículos en página de Pascual Román Polo, uno de los autores de la traducción oficial de las normas IUPAC 2005 de la Universidad de Zaragoza  
[http://www.ehu.eus/proman/datosautor.htm](http://www.ehu.eus/proman/datosautor.htm)  
Goya, Pilar; Martín, Nazario; **Román, Pascual**. W for tungsten and wolfram.*Nature Chemistry*, **2011, ***3*, 336. **DOI: **10.1038/nchem.1014. [PDF](http://www.ehu.eus/proman/documents/20110324WForTungstenWolframNatureChem2011-3-336.pdf)   
**Román Polo, Pascual **. El verdadero nombre del metal *tungsten* es: wolframio.* Apuntes de Ciencia y Tecnología***2006**, *18*, 25–31. [PDF](http://www.ehu.eus/proman/documents/20060320PRoman2006ACT18-25_000.pdf) [PDF, página 25](http://www.ehu.eus/proman/documents/20060320PRoman2006ACT18-25_000.pdf#page=25)  
**Román Polo, Pascual**. Wolframio, sí, tungsteno, no. *Anales de Química ***2005**, *101(2)*, 42–48. [PDF](http://www.ehu.eus/proman/documents/20050608WolframioSiTungstenoNoPascualRomanARSEQ_000.pdf)   
Goya, P.; **Román. P**. Wolfram vs. Tungsten. *Chemistry International***2005**, 27 (4), 26–27. [PDF](http://www.ehu.eus/proman/documents/20050714GoyaPWolframTungstenChemistryInternationalJuly05Articulo.pdf)   

2017 [Tungsteno o wolframio y las «guerras» de los nombres de los elementos - microsiervos.com](https://www.microsiervos.com/archivo/ciencia/tungsteno-o-wolframio.html) 

### Anhídridos

No se permiten en inorgánica según IUPAC (por ejemplo anhídrido carbónico), pero sí en orgánica  
[Specific Classes of Compounds, R-5.7.7 Anhydrides and their analogues](http://www.acdlabs.com/iupac/nomenclature/93/r93_534.htm) 

### <a name="grupos-funcionales-organica"></a>Grupos funcionales en orgánica

El concepto de grupo funcional enlaza con nomenclatura (ver prioridades de grupos funcionales) y con isomería.  
[functional group - IUPAC gold book](https://goldbook.iupac.org/terms/view/F02555)  
> Organic compounds are thought of as consisting of a relatively unreactive backbone, for example a chain of sp<sup>3</sup> hybridized carbon atoms, and one or several functional groups. **The functional group is an atom, or a group of atoms that has similar chemical properties whenever it occurs in different compounds**. It defines the characteristic physical and chemical properties of families of organic compounds.  

[2022, Murcia QUÍMICA ORGÁNICA, TEORÍA - ISOMERÍA](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/PAUxComunidades/quimica/murcia/2022-murcia-QU%C3%8DMICA%20ORG%C3%81NICA,%20TEOR%C3%8DA%202022.pdf)  
> A1) Isomería estructural de cadena  
Es aquella en la que sólo cambia la disposición de los átomos en el esqueleto carbonado, sin que se vea afectada la naturaleza o posición del grupo funcional (si es que lo hay).  
...  
Los isómeros de cadena suelen tener propiedades químicas muy similares, difiriendo algo más en sus propiedades físicas.   

> A3) Isomería estructural de función  
Es aquella en la que cambia el grupo funcional pero se mantiene la fórmula molecular del compuesto.  
Los isómeros de función son los que más difieren entre sí. Presentan propiedades físicas y químicas muy diferentes.  
Este tipo de isomería es típica de ciertas parejas de grupos funcionalesAlquenos y cicloalcanos:  
propeno y ciclo propano.

[A Brief Guide to Types of Isomerism in Organic Chemistry](http://www.compoundchem.com/2014/05/22/typesofisomerism/)   
Tiene como ejemplo de isomería estructural funcional but-2-eno y ciclobutano.  

A nivel de nomenclatura  
[Chapter P-3 CHARACTERISTIC (FUNCTIONAL) AND SUBSTITUENT GROUPS. Nomenclature of Organic Chemistry. IUPAC Recommendations and Preferred Names 2013.](https://iupac.qmul.ac.uk/BlueBook/P3.html)  
se suele considerar "grupos funcionales / característicos" los que no tienen solo C, por lo que excluirían dobles y triples  

### Ordenación alfabética de prefijos de la cadena principal en orgánica

En principio se indica que se ordenan alfabéticamente sin contar los prefijos multiplicadores: por ejemplo trimetil antes que dipropil.  
La definición está en  
[Guide to Name Construction, R-4.1 General Principles](https://www.acdlabs.com/iupac/nomenclature/93/r93_317.htm)  que indica sobre orden alfabético  
*...(g) assemble the components into a complete name, using ***alphabetical** order for all substitutive prefixes. *  
...  
*The various components having been selected, named and numbered, any necessary additive and subtractive modifications are made, and the complete name is assembled, prefixes being arranged in ***alphabetical** order.*  
 Otra referencia es  
 [Acyclic Hydrocarbons, Rule A-2. Saturated Branched-chain Compounds and Univalent Radicals](https://www.acdlabs.com/iupac/nomenclature/79/r79_36.htm)  
 ...If two or more side chains of different nature are present, they are cited in ***alphabetical** order .The **alphabetical** order is decided as follows:  
*(i) The names of simple radicals are first alphabetized and the multiplying prefixes are then inserted.*  
*(ii) The name of a complex radical is considered to begin with the first letter of its complete name.*  
*dimethylpentyl (as complete single substituent) is alphabetized under "d",*  
*thus 7-(1,2-Dimethylpentyl)-5-ethyltridecane*  
*(iii) In cases where names of complex radicals are composed of identical words, priority for citation is given to that radical which contains the lowest locant at the first cited point of difference in the radical.*  

Que es citado aquí de otra manera creo que más clara, aunque no es una referencia de IUPAC  
[IUPAC Alkane Nomenclature Rules in a Nutshell](https://www.stolaf.edu/depts/chemistry/courses/toolkits/247/js/names/anotes.htm)  
...If branches themselves are branched, then the complete name of the branch (with numbers) must be determined at this time. It is the *complete* name of the branch which is alphabetized. Thus, for example: 
   * **i**sopropyl comes before **s**ec-butyl
   * *4-(2,2-***d**imethylbutyl) comes before *3-***e**thyl
......These prefixes do NOT COUNT FOR ALPHABETIZING PURPOSES. For example: 
   * *3-***e**thyl comes before *2,2-di***m**ethyl
   * *4-***h**exyl comes before *2,3-di***i**sopropyl
   * *3-***e**thyl comes before *2,4-bis(2-***m**ethylbutyl)
 [](http://www.acdlabs.com/iupac/nomenclature/93/r93_534.htm) 

### Formulación en general / Apuntes

   * Creo una página aparte con recursos que se pueden categorizar como " [Apuntes de formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion) ", que incluye ejercicios, ejemplos, y algunos materiales elaborados por mí.

## InChI (IUPAC International Chemical Identifier)
 [http://www.inchi-trust.org/faq/](http://www.inchi-trust.org/faq/)  
 Copio un extracto (Copyright Inchl-trust)  
 *1.1 What is InChI?*  
Originally developed by the International Union of Pure and Applied Chemistry (IUPAC), the IUPAC International Chemical Identifier (InChI) is a character string generated by computer algorithm. It is a tool to be used in software applications designed and developed by those who choose to use it.  
The InChI algorithm turns chemical structures into machine-readable strings of information. InChIs are unique to the compound they describe and can encode absolute stereochemistry making chemicals and chemistry machine-readable and discoverable. A simple analogy is that InChI is the bar-code for chemistry and chemical structures.  
The InChI format and algorithm are non-proprietary and the software is open source, with ongoing development done by the community. A number of IUPAC working groups is currently creating standard for those areas of chemistry that are not yet handled by the InChI algorithm.  
1.2 What is the InChIKey?  
The InChIKey has been designed so that Internet search engines (such as Google, Yahoo, Bing, etc.) can search and find the links to a given InChI.  
To make the InChIKey the InChI string is subjected to a compression algorithm to create a fixed-length string of upper-case characters. While the InChI to InChIKey hash compression is irreversible, there are a number of InChI resolvers available to look up an InChI given an InChIKey.

Ver SMILES: Simplified Molecular-Input Line-Entry System
[SMILES - en.wikipedia.org](https://en.wikipedia.org/wiki/Simplified_molecular-input_line-entry_system)  
> In July 2006, the IUPAC introduced the InChI as a standard for formula representation. SMILES is generally considered to have the advantage of being more human-readable than InChI; it also has a wide base of software support with extensive theoretical backing (such as graph theory).  

## SMILES ((Simplified Molecular-Input Line-Entry System)

[SMILES - goldbook.iupac.org](https://goldbook.iupac.org/terms/view/ST06980)  

[IUPAC SMILES+ Specification](https://iupac.org/project/2019-002-2-024/)  
> The two most popular machine-readable chemical line notations are the IUPAC InChI (International Chemical Identifier) and SMILES (Simplified molecular-input line-entry system). The InChI is a standardized IUPAC chemical descriptor notation that has become essential for the chemistry community. SMILES are a chemical representation file format and play a distinct and complementary role for automated retrieval of structural information along-side InChI. However, unlike InChI, where the notation is standardized and well documented, there is no current standard specification for the SMILES file format. As a result, there are multiple SMILES dialects, implementations, and extensions available with no up-to-date authoritative descriptions of the format. This situation has proved to be a significant barrier for interoperability and accurate exchange of chemical structure information among researchers, chemical databases, cheminformatics toolkits, and software drawing tools.  

Es un formato "en línea" muy intuitivo para escribir compuestos, y muchos programas de dibujo químico lo aceptan como vía de entrada para indicar moléculas  
Ejemplos:  
CCCC  Butano  
CC(C)CC  2-metilbutano   
C=CCOC metoxiprop-2-eno  
CC(=O)C(=O)(O) ácido 2-oxopropanoico  
C1=CCC1 ciclobuteno  


## Libros formulación
Aparte de los recursos electrónicos IUPAC, en la  [página de recursos de libros](/home/recursos/libros)  intento centralizar algunos recursos impresos.

Los ficheros estaban inicialmente se veían anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia formulación](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/quimica/formulacion) 


