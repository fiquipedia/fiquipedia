# Recursos Aprendizaje Basado en Proyectos en Física y Química

Puede enlazar con pequeñas investigaciones (ver  [proyectos de investigación](/home/recursos/proyectos-de-investigacion) )  

[Pequeñas investigaciones dirigidas basadas en la resolución de problemas como alternativa alternativa a los trabajos de laboratorio tradicionales de Física y Química en 3º ESO. Trabajo de fin de máster. María Teresa García Segura](http://reunir.unir.net/bitstream/handle/123456789/3432/GARCIA%20%20SEGURA%2c%20MARIA%20TERESA.pdf?sequence=1)  
En página 56, dentro de propuesta de intervención, se cita "Investigación sobre cómo determinar la concentración de ácido acético en un vinagre comercial", que está temporalizado con 3 sesiones presenciales y una de laboratorio. El planteamiento requiere trabajo por parte del alumno fuera del centro antes de la siguiente sesión.

 [Manual sobre trabajo por proyectos: aprendizaje con sentido - webdelmaestrocmf](http://webdelmaestrocmf.com/portal/manual-trabajo-proyectos-aprendizaje-sentido/)  
 [Manual sobre trabajo por proyectos: aprendizaje con sentido (pdf)](/drive/TRABAJO-POR-PROECTOS-APRENDIZAJE-CON-SENTIDO.pdf)  
 [El método de proyectos como técnica didáctica (pdf)](/drive/TRABAJO-POR-PROECTOS-APRENDIZAJE-CON-SENTIDO.pdf)   

 [La enseñanza de ciencia naturales basada en proyectos (pdf, 226 páginas) - laboratoriogrecia.cl](http://laboratoriogrecia.cl/wp-content/uploads/2015/12/CS-Nats-y-Trabajo-por-Proyectos-Version-digital.pdf)   

[Mars in a box](https://marsinabox.eu/)  
> «Mars in a box» is an educational project aiming to introduce European secondary  school students to fundamental science through cutting-edge research on Mars. The initiative is part of RoadMap (ROle of and impAct of Dust and clouds in the Martian AtmosPhere: from lab to space),  a H2020 project led by a consortium of European scientific institutions, which aims to improve our multidisciplinary understanding of the Martian atmosphere.  

Formado por 8 experimentos  https://marsinabox.eu/experiments/  

