
# Láser

 [Laser - Tout est quantique](http://www.toutestquantique.fr/#laser)  
 [Lasers - Mapping Ignorance](http://mappingignorance.org/2016/06/23/lasers/)  
 
[El láser, la luz de nuestro tiempo. ISBN: 978-84-92997-10-7 - usal.es](https://laser.usal.es/alf/wp-content/uploads/2012/11/El_laser.pdf)   
 
Stimulated Emission - Bozeman Science   
 [![](https://img.youtube.com/vi/YHmGNDMV1cY/0.jpg)](https://www.youtube.com/watch?v=YHmGNDMV1cY "Stimulated Emission - Bozeman Science")  

How lasers work (in theory) - MinutoDeFísica  
 [![](https://img.youtube.com/vi/y3SBSbsdiYg/0.jpg)](https://www.youtube.com/watch?v=y3SBSbsdiYg "How lasers work (in theory) - MinutoDeFísica")  
 
Funcionamiento de un láser. ¿Cómo es por dentro? || Propiedades, aplicaciones, seguridad || OSAL   
 [![](https://img.youtube.com/vi/8UbnJ3b8YzY/0.jpg)](https://www.youtube.com/watch?v=8UbnJ3b8YzY "Funcionamiento de un láser. ¿Cómo es por dentro? || Propiedades, aplicaciones, seguridad || OSAL")  
 
### Simulaciones

[Lásers. Universidad de Colorado](http://phet.colorado.edu/es/simulation/lasers)  En 2023 solamente versión java  
Physics 2000  
 [index.html](http://www.maloka.org/fisica2000/lasers/index.html)   

   * Creando un láser. [lasers4.html](http://www.maloka.org/fisica2000/lasers/lasers4.html)   
Contiene pequeñas simulaciones
Il laser (italiano)  
 [laser_ita.htm](http://ww2.unime.it/weblab/ita/laser/laser_ita.htm)   
Universitá degli Studi Messina  
Original applet © 1997 by Sergey Kiselev and Tanya Yanovsky-Kiselev  
Adapted applet © 1998 by Carlo Sansotta for IFMSA WebLab  

