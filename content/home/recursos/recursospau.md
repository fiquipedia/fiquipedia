
# Recursos PAU

En 2017 PAU pasa a ser " [evaluación final bachillerato](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-bachillerato) " según LOMCE, nombre es PAU, EBAU, EvAU, EFB, ... según comunidad, pero se sigue centralizando información aquí como PAU  
Los recursos se pueden agrupar de distintas maneras:  
* [Recursos genéricos asociados a PAU](/home/recursos/recursospau/recursos-pau-genericos)  (sin distinción clara de materias)  
* [Recursos PAU acceso más de 25 años ](/home/recursos/recursospau/recursos-pau-mayores-25-anos) (información específica adicional a los recursos genéricos)  

Los recursos propios de cada materia, especialmente los de  [Física](/home/pruebas/pruebasaccesouniversidad/paufisica)  y  [Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) , están en sus propias páginas.  
En 2014 está iniciada la página  [PAU Electrotecnia](/home/pruebas/pruebasaccesouniversidad/pau-electrotecnia) , que es una materia asociada a la especialidad.  
En 2015 se inicia la página  [PAU Mecánica](/home/pruebas/pruebasaccesouniversidad/pau-mecanica), que es una materia que desapareció en 2009, asociada a la especialidad  
En 2023 se inicia la página [PAU Ciencias Generales](/home/pruebas/pruebasaccesouniversidad/pau-cienciasgenerales), que es una materia que surge con LOMLOE  
