# MRU (Movimiento Rectilíneo Uniforme)  

Esta es una subpágina de la página de [cinemática](/home/recursos/fisica/cinematica) para descargarla centralizando aquí recursos solo sobre MRU, que pueden incluir experiencias o [prácticas experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio) asociadas, por lo que puede haber información también, a veces duplicada, en otras páginas.  

Dado que el MRU es un caso de MRUA con a=0, puede haber recursos de MRUA que puedan utilizarse, por ejemplo gráficas espacio-tiempo y velocidad-tiempo en la que se pueda fijar a=0.

## Actividades

Puede haber asociadas a simulaciones y asociadas a experimentos  

[twitter onio72/status/1260570125062864899](https://twitter.com/onio72/status/1260570125062864899)  
Animación de Geogebra para trabajar la representación gráfica de posición frente al tiempo en MRU.  
[https://www.geogebra.org/m/wgppdvnm](https://www.geogebra.org/m/wgppdvnm)  
Pensada para integrar en Moodle como pregunta de cuestionario.  

[Gráficas r-t - geogebra](https://www.geogebra.org/m/gx52vbjt)  
Enrique García de Bustos Sierra  

[Cinemática 2º ESO – Ejercicios MRU, raizcudrada.es](http://www.raizcuadrada.es/cinematica-2o-eso-ejercicios-mru/)  

## Simulaciones

[MRU - Educaplus](https://www.educaplus.org/games/browse?search=mru)  

(Flash, enlace directo  [lab_mru_tabla_peplus.swf](http://www.educaplus.org/web/games/l/lab_mru_tabla_peplus.swf) )  

[Laboratorio de movimiento rectilíneo - educaplus](https://www.educaplus.org/game/laboratorio-de-movimiento-rectilineo)  

(Flash, enlace directo  [movimiento_1_dimension_tabla_peplus.swf](http://www.educaplus.org/web/games/m/movimiento_1_dimension_tabla_peplus.swf) )  

[Gráficas del movimiento  | Educaplus](http://www.educaplus.org/game/graficas-del-movimiento)  (HTML5)  
Incluye MRU y MRUA

[Gráfica de posición tiempo vehículo - geogebra.org](https://www.geogebra.org/m/mfunbR4s)  

[twitter JavierCayetan19/status/1497152141600960532](https://twitter.com/JavierCayetan19/status/1497152141600960532)  
Preparando una simulación para trabajar los diagramas x-t en el MRU. ¿A qué distancia está el coche de la casa?   
[Movimiento rectilíneo uniforme - Diagrama x-t - geogebra.org](https://www.geogebra.org/m/gxettyny)  

[twitter JavierCayetan19/status/1497152141600960532](https://twitter.com/JavierCayetan19/status/1497152141600960532)  
Está genial.  
¿Has probado a incluir un posible recorrido?  
A veces no entienden bien este tipo de gráficos y les cuesta asociarlos con un movimiento.  
(*) Se puede conectar con la geometría:  
Si no cambia la distancia, ¿está parado, recorre una circunferencia, hay más posibilidades?  

Pendiente de las gráficas e-t  
[Pendiente de las gráficas e-t | Educaplus](http://www.educaplus.org/movi/3_3et1.html)  
(Flash, enlace directo  [uniforme_graf_et1.swf](http://www.educaplus.org/movi/swf/uniforme_graf_et1.swf)   

Movimiento uniforme rectilineo. Jorge Lay Gajardo.  
[graficomur.swf](http://www.profisica.cl/images/stories/animaciones/graficomur.swf)  

Partículas con velocidad constante pero distinta magnitud y distinto sentido. Jorge Lay Gajardo.  
[3mur2profisica.swf](http://www.profisica.cl/images/stories/animaciones/3mur2profisica.swf)  

[IES Al-Ándalus &#8211; 41000557 Arahal (Sevilla)](http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=139&Itemid=199)  
[Laboratorio virtual: Movimiento Rectilíneo Uniforme (MRU). Tabla de valores y gráfica.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq4eso/mru_01.swf).  
[Laboratorio virtual: Práctica sobre Movimiento Rectilíneo Uniforme.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq4eso/practica_mru_01.swf).  
[Laboratorio virtual: Movimiento Rectilíneo Uniformemente Acelerado (MRUA).](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq4eso/mrua_01.swf).  
Jose Antonio Navarro Dominguez 

Ejercicios sencillos MRU  (flash)  
[veldiste2c.swf](http://genmagic.net/repositorio/albums/userpics/veldiste2c.swf)  
[ejer_gmru1.swf](http://genmagic.net/repositorio/albums/userpics/ejer_gmru1.swf)  

## Experimentos / laboratorio 

Ver [Prácticas / experimentos laboratorio](/home/recursos/practicas-experimentos-laboratorio/) y [Prácticas de laboratorio de elaboración propia](/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/)  

Se pueden plantear experiencias MRU con distintos elementos. Lo habitual es que permitan medir posición y velocidad a lo largo de la trayectoria.   
- Carritos de laboratorio y fotocélulas  
[Documento para el alumno, MRU](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMRU-Alumno.pdf)  

[Práctica Virtual MRUV - IES Valle del Saja (vídeo)](http://www.fqsaja.com/?portfolio_page=practica-virtual-mruv)  
[![](https://img.youtube.com/vi/PhfYb7y8cXY/0.jpg)](https://www.youtube.com/watch?v=PhfYb7y8cXY "MRUV corregido - IES Valle del Saja")  
[Práctica Virtual-MRUV - IES Valle del Saja (guion)](http://www.fqsaja.com/?p=9826)  
[Guion práctica virtual MRUV - IES Valle del Saja](https://drive.google.com/file/d/1ZC17GJcqX3h8rHlfdS43T9uBSkDrR8Oo/view)  

- Juguetes

12 junio 2019  
El objetivo era conseguir que seis juguetes con MRU (aproximadamente) empataran en una carrera. Han tenido que medir velocidades, calcular posiciones iniciales, representar x-t y v-t de los seis juguetes y finalmente (a la segunda): ¡EMPATE! Fotos y luego video. @IES_Modesto  
![](https://pbs.twimg.com/media/D82cSdJWkAAne-S.jpg)  

- Elemento que arde a velocidad constante: barria de incienso, papel con KNO3  

[MOVIMIENTO RECTILINEO UNIFORME (MRU) - riberadeltajo.es](http://riberadeltajo.es/nuevaweb/index.php/casilleros/fisica-y-quimica/casillero-de-mariano-benito-perez/eso/fisica-y-quimica-4-eso/148-fisica-y-quimica-4-eso-5-practicas-de-laboratorio/file)  
Estudiar el MRU de avance de un punto de ignición sobre una tira de papel impregnada con una disolución de Nitrato de Potasio, KNO3.  

- Elemento que cae a velocidad constante:  

   - Imán dentro de un tubo de cobre (es complicado visualizar y medir posición y tiempo, a no ser con ranura en el tubo y grabar en vídeo)  

[Práctica Virtual MRU](http://www.fqsaja.com/?p=9705)  

   - Nivel de líquido en una probeta de líquido que cae a ritmo constante, por ejemplo embudo de decantación  

   - Gota de agua bajando en aceite  

[Experimento   Movimiento Rectilíneo Uniforme (MRU), (gota agua en el aceite)](https://sites.google.com/view/experimentosvirgenvilla/fis-mec%C3%A1nica-fuerza-mov/fuerza-mov-y-energ%C3%ADa/efectos-de-fuerza/mov-rectilineo/m-r-u)  

![](https://lh5.googleusercontent.com/44BvwWHttXJwNsg4BKkPUn_O0j-HLsyYWC32kGDDQO_y6TirHUng501DV5p2tEpZ4RqPLsRewfrDa_baP4y-4ifosOdB16nph6emwbQDj7_1KagE=w1280)  

   - Objeto bajando en fluido
   Dado en glicerina  
[![](https://img.youtube.com/vi/ZtyFRdTvoGA/0.jpg)](https://www.youtube.com/watch?v=ZtyFRdTvoGA "Movimiento Rectilíneo Uniforme 2 - Sergio Paredes")  

Bola en agua 
[Práctica Virtual-MRUV versión II - IES Valle del Saja](http://www.fqsaja.com/?portfolio_page=practica-virtual-mru-version-ii)  
[](http://www.fqsaja.com/?p=9764)  
[Guion práctica virtual MRUV versión II - IES Valle del Saja](https://drive.google.com/file/d/1oSsmSv0xhd2IH8j-9CubdC6aYFpT8NlV/view?usp=sharing)  

   

