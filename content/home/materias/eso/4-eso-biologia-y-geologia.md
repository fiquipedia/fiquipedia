
# 4º ESO Biología y Geología

Según Real Decreto 1631/2006, ver en  [Legislación](/home/legislacion-educativa)   

## Contenidos

### Bloque 1. Contenidos comunes.
Actuación de acuerdo con el proceso de trabajo científico: planteamiento de problemas y discusión de su interés, formulación de hipótesis, estrategias y diseños experimentales, análisis e interpretación y comunicación de resultados.  
Búsqueda y selección de información de carácter científico utilizando las tecnologías de la información y comunicación y otras fuentes.  
Interpretación de información de carácter científico y utilización de dicha información para formarse una opinión propia, expresarse con precisión y tomar decisiones sobre problemas relacionados con las ciencias de la naturaleza.  
Reconocimiento de las relaciones de la biología y la geología con la tecnología, la sociedad y el medio ambiente, considerando las posibles aplicaciones del estudio realizado y sus repercusiones.  
Utilización correcta de los materiales e instrumentos básicos de un laboratorio y respeto por las normas de seguridad en el mismo.

### Bloque 2. La Tierra, un planeta en continuo cambio.

#### La historia de la Tierra:
El origen de la Tierra. El tiempo geológico: ideas históricas sobre la edad de la Tierra. Principios y procedimientos que permiten reconstruir su historia. Utilización del actualismo como método de interpretación.  
Los fósiles, su importancia como testimonio del pasado. Los primeros seres vivos y su influencia en el planeta.  
Las eras geológicas: ubicación de acontecimientos geológicos y biológicos importantes.  
Identificación de algunos fósiles característicos.  
Reconstrucción elemental de la historia de un territorio a partir de una columna estratigráfica sencilla.

#### La tectónica de placas y sus manifestaciones:
El problema del origen de las cordilleras: algunas interpretaciones históricas. El ciclo de las rocas.  
Pruebas del desplazamiento de los continentes. Distribución de volcanes y terremotos. Las dorsales y el fenómeno de la expansión del fondo oceánico.  
Interpretación del modelo dinámico de la estructura interna de la Tierra.  
Las placas litosféricas y sus límites. Interacciones entre procesos geológicos internos y externos. Formación de las cordilleras: tipos y procesos geológicos asociados.  
La tectónica de placas, una revolución en las Ciencias de la Tierra. Utilización de la tectónica de placas para la interpretación del relieve y de los acontecimientos geológicos.  
Valoración de las consecuencias que la dinámica del interior terrestre tiene en la superficie del planeta.

### Bloque 3. La evolución de la vida.

#### La célula, unidad de vida.
La teoría celular y su importancia en Biología. La célula como unidad estructural y funcional de los seres vivos.  
Los procesos de división celular. La mitosis y la meiosis. Características diferenciales e importancia biológica de cada una de ellas.  
Estudio del ADN: composición, estructura y propiedades. Valoración de su descubrimiento en la evolución posterior de las ciencias biológicas.  
Los niveles de organización biológicos. Interés por el mundo microscópico.  
Utilización de la teoría celular para interpretar la estructura y el funcionamiento de los seres vivos.

#### La herencia y la transmisión de los caracteres:
El mendelismo. Resolución de problemas sencillos relacionados con las leyes de Mendel.  
Genética humana. La herencia del sexo. La herencia ligada al sexo. Estudio de algunas enfermedades hereditarias.  
Aproximación al concepto de gen. El código genético. Las mutaciones.  
Ingeniería y manipulación genética: aplicaciones, repercusiones y desafíos más importantes. Los alimentos transgénicos. La clonación. El genoma humano.  
Implicaciones ecológicas, sociales y éticas de los avances en biotecnología genética y reproductiva.

#### Origen y evolución de los seres vivos:
Hipótesis sobre el origen de la vida en la Tierra. Evolución de los seres vivos: teorías fijistas y evolucionistas.  
Datos que apoyan la teoría de la evolución de las especies. Reconocimiento de las principales características de fósiles representativos. Aparición y extinción de especies.  
Teorías actuales de la evolución. Gradualismo y equilibrio puntuado.  
Valoración de la biodiversidad como resultado del proceso evolutivo. El papel de la humanidad en la extinción de especies y sus causas.  
Estudio del proceso de la evolución humana.

### Bloque 4. Las transformaciones en los ecosistemas.
La dinámica de los ecosistemas:Análisis de las interacciones existentes en el ecosistema: Las relaciones tróficas. Ciclo de materia y flujo de energía. Identificación de cadenas y redes tróficas en ecosistemas terrestres y acuáticos. Ciclos biogeoquímicos.  
Autorregulación del ecosistema: las plagas y la lucha biológica.  
Las sucesiones ecológicas. La formación y la destrucción de suelos. Impacto de los incendios forestales e importancia de su prevención.  
La modificación de ambientes por los seres vivos y las adaptaciones de los seres vivos al entorno. Los cambios ambientales de la historia de la Tierra.  
Cuidado de las condiciones medioambientales y de los seres vivos como parte esencial de la protección del medio natural.

## Criterios de evaluación
1. Identificar y describir hechos que muestren a la Tierra como un planeta cambiante y registrar algunos de los cambios más notables de su larga historia utilizando modelos temporales a escala.Se pretende evaluar la capacidad del alumnado para reconocer la magnitud del tiempo geológico mediante la identificación de los acontecimientos fundamentales de la historia de la Tierra en una tabla cronológica y, especialmente, a través de la identificación y ubicación de los fósiles más representativos de las principales eras geológicas y de otros registros geológicos tales como la datación estratigráfica, los tipos de rocas, las cordilleras y procesos orogénicos o las transgresiones y regresiones marinas.  
2. Utilizar el modelo dinámico de la estructura interna de la Tierra y la teoría de la Tectónica de placas para estudiar los fenómenos geológicos asociados al movimiento de la litosfera y relacionarlos con su ubicación en mapas terrestres.Se trata de evaluar la capacidad del alumnado para aplicar el modelo dinámico de la estructura interna de la Tierra y la teoría de la tectónica de placas en la explicación de fenómenos aparentemente no relacionados entre sí, como la formación de cordilleras, la expansión del fondo oceánico, la coincidencia geográfica de terremotos y volcanes en muchos lugares de la Tierra, las coincidencias geológicas y paleontológicas en territorios actualmente separados por grandes océanos, etc. También se debe comprobar si es capaz de asociar la distribución de seísmos y volcanes a los límites de las placas litosféricas en mapas de escala adecuada, y de relacionar todos estos procesos.  
3. Aplicar los postulados de la teoría celular al estudio de distintos tipos de seres vivos e identificar las estructuras características de la célula procariótica, eucariótica vegetal y animal, y relacionar cada uno de los elementos celulares con su función biológica.El alumnado ha de reconocer, empleando las técnicas adecuadas, la existencia de células en distintos organismos. Se trata de evaluar si es capaz de identificar las estructuras celulares en dibujos y microfotografías, señalando la función de cada una de ellas. Asimismo, debe entender la necesidad de coordinación de las células que componen los organismos pluricelulares.  
4. Reconocer las características del ciclo celular y describir la reproducción celular, señalando las diferencias principales entre meiosis y mitosis, así como el significado biológico de ambas.Se trata de comprobar que el alumnado reconoce la mitosis como un tipo de división celular asexual necesaria en la reproducción de los organismos unicelulares y que asegura el crecimiento y reparación del cuerpo en los organismos pluricelulares. También debe explicar el papel de los gametos y de la meiosis en la reproducción sexual. Se trata de comparar ambos tipos de división celular respecto al tipo de células que la sufren, a su mecanismo de acción, a los resultados obtenidos y a la importancia biológica de ambos procesos. Se puede considerar la utilización e interpretación de dibujos esquemáticos, modelos de ciclos celulares o fotografías de cariotipos.  
5. Resolver problemas prácticos de Genética en diversos tipos de cruzamientos utilizando las leyes de Mendel y aplicar los conocimientos adquiridos en investigar la transmisión de determinados caracteres en nuestra especie.Se pretende evaluar si el alumnado es capaz de diferenciar los conceptos básicos de genética y resolver problemas sencillos sobre la transmisión de caracteres hereditarios calculando porcentajes genotípicos y fenotípicos de los descendientes y reconociendo en estos resultados su carácter aleatorio. Se ha de valorar, asimismo, si aplica estos conocimientos a problemas concretos de la herencia humana, como la hemofilia, el daltonismo, factor Rh, color de ojos y pelo, etc.  
6. Conocer que los genes están constituidos por ADN y ubicados en los cromosomas, interpretar el papel de la diversidad genética (intraespecífica e interespecífica) y las mutaciones a partir del concepto de gen y valorar críticamente las consecuencias de los avances actuales de la ingeniería genética.Se pretende comprobar si el alumnado explica que el almacenamiento de la información genética reside en los cromosomas, interpreta mediante la teoría cromosómica de la herencia las excepciones a las leyes de Mendel y conoce el concepto molecular de gen, así como la existencia de mutaciones y sus implicaciones en la evolución y diversidad de los seres vivos. Se debe valorar también si utiliza sus conocimientos para crearse un criterio propio acerca de las repercusiones sanitarias y sociales de los avances en el conocimiento del genoma y analizar, desde una perspectiva social, científica y ética, las ventajas e inconvenientes de la moderna biotecnología (terapia génica, alimentos transgénicos, etc.).  
7. Exponer razonadamente los problemas que condujeron a enunciar la teoría de la evolución, los principios básicos de esta teoría y las controversias científicas, sociales y religiosas que suscitó.El alumnado debe conocer las controversias entre fijismo y evolucionismo y entre distintas teorías evolucionistas como las de Lamarck y Darwin, así como las teorías evolucionistas actuales más aceptadas. Se trata de valorar si el alumnado sabe interpretar, a la luz de la teoría de la evolución de los seres vivos, el registro paleontológico, la anatomía comparada, las semejanzas y diferencias genéticas, embriológicas y bioquímicas, la distribución biogeográfica, etc.  
8. Relacionar la evolución y la distribución de los seres vivos, destacando sus adaptaciones más importantes, con los mecanismos de selección natural que actúan sobre la variabilidad genética de cada especie.Se trata de valorar si el alumnado sabe interpretar, a la luz de la teoría de la evolución, los datos más relevantes del registro paleontológico, la anatomía comparada, las semejanzas y diferencias genéticas, embriológicas y bioquímicas, la distribución biogeográfica y otros aspectos relacionados con la evolución de los seres vivos.  
9. Explicar cómo se produce la transferencia de materia y energía a largo de una cadena o red trófica concreta y deducir las consecuencias prácticas en la gestión sostenible de algunos recursos por parte del ser humano.Se trata de valorar si el alumno es capaz de relacionar las pérdidas energéticas producidas en cada nivel con el aprovechamiento de los recursos alimentarios del planeta desde un punto de vista sustentable (consumo de alimentos pertenecientes a los últimos niveles tróficos) y las repercusiones de las actividades humanas en el mantenimiento de la biodiversidad en los ecosistemas (desaparición de depredadores, sobreexplotación pesquera, especies introducidas, etc.).  

