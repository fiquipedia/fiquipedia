[comentario]: # (se podrían pner aliases, por ejemplo reacciones-quimicas, ya que nombre inicial es recursos-cambios-quimicos, pero hay dispersas páginas, se intentan enlazar)  

# Recursos cambios químicos 

La diferenciación entre cambios químicos y físicos es algo básico que se introduce en varios niveles.  
Primero se introducen como cambios químicos y luego se habla de reacciones químicas.  
Aparece en  [Currículo Física y Química 2º ESO](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso)  
La idea de exotérmico y endotérmico se introduce en ESO y luego se trata con cálculos en  [termoquímica](/home/recursos/quimica/termoquimica)  

Hay recursos asociados en  
* [reacciones químicas vistosas](/home/recursos/quimica/reacciones-quimicas-vistosas)   
* [ajuste reacciones químicas](/home/recursos/quimica/recursos-ajuste-reacciones-quimicas)  
* [reacciones y cálculos estequiométricos](/home/recursos/quimica/recursos-reacciones-y-calculos-estequiometricos)  
* Ejercicios de  [Pruebas Libres Graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)  y  [Pruebas Acceso Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio)  
* [química y fuegos artificales](/home/recursos/quimica/recursos-quimica-fuegos-artificiales)  
* [química y arte](/home/recursos/quimica/recursos-quimica-y-arte)  
* [quimiofobia](/home/recursos/quimica/recursos-quimiofobia)  

Ver ejercicios sobre reacciones en [Pruebas libres graduado ESO](/home/pruebas/pruebas-libres-graduado-eso/)


[¿Es magia? No, son reacciones químicas. Proyecto EDIA - intef](https://descargas.intef.es/cedec/proyectoedia/fisica_quimica/contenidos/es_magia/index.html)  


Vídeo con conservación masa gráfica  
 [https://twitter.com/WRBdB/status/934443815699705856](https://twitter.com/WRBdB/status/934443815699705856)  
 Watch the mass change of burning iron wool. #chemed #scied @gochemistryvids  
 
 Cómic de  [http://cationdesigns.blogspot.com.es/p/high-school-science-teacher-resources.html](http://cationdesigns.blogspot.com.es/p/high-school-science-teacher-resources.html)  
 [![http://cationdesigns.blogspot.com.es/p/high-school-science-teacher-resources.html](http://2.bp.blogspot.com/--_shopZdi6g/TwZP-5OI2nI/AAAAAAAABfI/PiRDwlxcEz8/s1600/Evidence+of+Chemical+Change.jpg "http://cationdesigns.blogspot.com.es/p/high-school-science-teacher-resources.html") ](http://2.bp.blogspot.com/--_shopZdi6g/TwZP-5OI2nI/AAAAAAAABfI/PiRDwlxcEz8/s1600/Evidence+of+Chemical+Change.jpg)  
 
 [https://twitter.com/Biochiky/status/970916887579254784](https://twitter.com/Biochiky/status/970916887579254784)  
 [![](https://pbs.twimg.com/media/DXljwuoX4AA1mc4.jpg "") ](https://pbs.twimg.com/media/DXljwuoX4AA1mc4.jpg)  
 
 
[​Chemical Reactions Walk Around](http://passionatelycurioussci.weebly.com/google-form-walk-arounds.html#reactions)   
[Chemical Reactions Walk Around (pdf, 11 páginas)](https://passionatelycurioussci.weebly.com/uploads/5/8/6/6/58665899/chemical-reactions-walk-around.pdf)  


[Las mascarillas de oxígeno de emergencia de los aviones no están conectadas a una botella de O₂, sino a un generador químico](https://www.microsiervos.com/archivo/aerotrastorno/mascarillas-oxigeno-emergencia-aviones-generador-quimico-o2.html)  

[![](https://img.youtube.com/vi/KejaV70RWPk/0.jpg)](https://www.youtube.com/watch?v=KejaV70RWPk&t=93s "How Oxygen Masks Brought Down a Plane")
Segundo 93: 2NaClO3 -> 2NaCl + 3O2  

[![](https://img.youtube.com/vi/ZQl1TN5h1bA/0.jpg)](https://www.youtube.com/watch?v=ZQl1TN5h1bA "Reaction of Potassium Permanganate and Hydrogen Peroxide")
3H2O2 + 2KMnO4 → 3O2 + 2MnO2 + 2KOH + 2H2O  

[![](https://img.youtube.com/vi/qy159GJmYQs/0.jpg)](https://www.youtube.com/watch?v=qy159GJmYQs "What Happens When You Pee in the Pool?   ChemistryViews")  
 
## Reacciones químicas y colores

Enlaza con [química y fuegos artificales](/home/recursos/quimica/recursos-quimica-fuegos-artificiales) y [reacciones químicas vistosas](/home/recursos/quimica/reacciones-quimicas-vistosas)   


El caso del papel es algo que se puede asociar a método científico  
[Why Do Book Pages Turn Yellow Over Time? - livescience.com](https://www.livescience.com/63635-why-paper-turns-yellow.html)  
> Most paper is made from wood, which largely consists of cellulose and a natural wood component called lignin that gives land plant cell walls their rigidity and makes wood stiff and strong. Cellulose — a colorless substance — is remarkably good at reflecting light, which means we [perceive it as being white](http://jcb.rupress.org/content/jcb/53/3/695.full.pdf). This is why paper — including the pages of everything from sheet music to dictionaries — is usually white. [Why Is Cow's Milk White?](https://www.livescience.com/32501-why-is-cows-milk-white.html)  
> But when lignin is exposed to light and the surrounding air, its molecular structure changes. Lignin is a polymer, meaning it's built from batches of the same molecular unit bonded together. In the case of lignin, those repeating units are alcohols consisting of oxygen and hydrogen with a smattering of carbon atoms thrown in, Richardson said.

[The Chemistry of Paper Preservation: Part 2. The Yellowing of Paper and Conservation Bleaching - acs.org](https://pubs.acs.org/doi/abs/10.1021/ed073p1068)  

## Reacciones químicas y olores

A veces el olor se debe a presencia de sustancias volátiles, no a reacciones que las produzcan
[What Causes the Smell of New & Old Books? - compoundchem.com](https://www.compoundchem.com/2014/06/01/newoldbooksmell/)  
 
 
