
# Movimiento oscilatorio

## Recursos generales
Ver también asociado recursos de  [movimiento ondulatorio](/home/recursos/fisica/movimiento-ondulatorio)   
Ver también recursos de [Fuerza elástica: muelles, ley de Hooke, dinamómetros](/home/recursos/recursos-dinamica/recursos-fuerza-elastica)
  
Dinámica una partícula. Movimiento armónico simple  
 [mas.html](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/dinam1p/mas.html)   
Página realizada por Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM) - España.  
  
 [Movimiento Armónico Simple (M.A.S.) | Fisicalab](https://www.fisicalab.com/apartado/concepto-oscilador-armonico)   
  
 [Simple_Harmonic_Motion#Equation_of_Motion](http://mathforum.org/mathimages/index.php/Simple_Harmonic_Motion#Equation_of_Motion)   
  
 [Simple Harmonic Motion (SHM)](http://www.splung.com/content/sid/2/page/shm/)   
 
 [Movimiento Armónico Simple - recursostic.educacion.es/newton](http://recursostic.educacion.es/newton/web/materiales_didacticos/MAS/aulaMAS.pdf)  

## Fase inicial
Algunos recursos asociados a mostrar la idea de fase inicial, que es un concepto que cuesta visualizar inicialmente a los alumnos.En  [Movimiento Armónico Simple (M.A.S.) | Fisicalab](https://www.fisicalab.com/apartado/concepto-oscilador-armonico#grafica-posicion-mas)  hay una gráfica interactiva en la se puede variar A, frecuencia angular y fase inicial  
  
 [Movimiento Arm&oacute;nico Simple](http://www.sc.ehu.es/sbweb/fisica3/oscilaciones/mas/mas.html)  (versión HTML5 permite visualizar bien la idea de fase inicial de manera interactiva asociándola a muelle / MCU)

## Recursos vídeos / animaciones
  
 [!["¿Por Qué Todo OSCILA en el Universo? | El OSCILADOR ARMÓNICO"](https://img.youtube.com/vi/dpkt5MmlWZY/0.jpg)](https://www.youtube.com/watch?v=dpkt5MmlWZY "¿Por Qué Todo OSCILA en el Universo? | El OSCILADOR ARMÓNICO") Instituto de Física Teórica IFT  
 
[twitter MientrasEnFisic/status/799006610936987648](https://twitter.com/MientrasEnFisic/status/799006610936987648)   
-Dígame, como físico ¿qué sabe hacer?  
+Resolver el oscilador armónico  
-Y ya?  
+No, también aproximo cualquier problema al oscilador armónico   

Lo que Necesitas Saber sobre Ondas (al menos para Selectividad)   
 [![watch](https://img.youtube.com/vi/rKf92Vgx2ag/0.jpg)](https://www.youtube.com/watch?v=rKf92Vgx2ag) 8 primeros minutos son solamente sombre movimiento oscilatorio, 4 restantes de ondulatorio  
  
 [Physclips - Simple Harmonic Motion](http://www.animations.physics.unsw.edu.au/mechanics/chapter4_simpleharmonicmotion.html)   
© School of Physics - UNSW (Sydney, Australia). cc-by-nc-nd  
Lecciones con vídeos y animaciones  

## Recursos laboratorio no virtual
PRÁCTICAS DIGITALES DE FÍSICA (con apoyo de cámara de fotos)  
  
Ley de Hooke  [PDF82.htm](http://www.heurema.com/PDF82.htm) MOVIMIENTO ARMÓNICO SIMPLE  
[PDF6.htm](http://www.heurema.com/PDF6.htm)   

[![Sand pendulums - Lissajous patterns - part one // Homemade Science with Bruce Yeany - YouTube](https://img.youtube.com/vi/uPbzhxYTioM/0.jpg)](https://www.youtube.com/watch?v=uPbzhxYTioM)  

## Recursos apps para móvil
  
Physics - Oscillations (Free), Surendranath.B  
 [Physics - Oscillations (Free) - Apps on Google Play](https://play.google.com/store/apps/details?id=org.surendranath.oscillationstrial)  (versión gratuita, solamente primera de las animaciones) [Physics - Oscillations - Apps on Google Play](https://play.google.com/store/apps/details?id=org.surendranath.oscillations)  (versión de pago)Aunque no se indica explícitamente en octubre 2014, tienen como requisito una pantalla de al menos 7": es para tablet, no para móvil.  
  
Physics: Oscillations & Waves, PhysicsOne   
 [details](https://play.google.com/store/apps/details?id=appinventor.ai_au93e6mi.AppPhysicsWaves)  (de pago)  

## Recursos laboratorio virtual / simulaciones
  
M.A.S y movimiento circular uniforme  
 [M.A.S y movimiento circular uniforme](http://www.sc.ehu.es/sbweb/fisica_/oscilaciones/m_a_s/circular/oscila1.html)  (versión java)  
 *Permite visualizar bien la idea de fase inicial*  

 [Fase inicial movimiento armónico simple - geogebra.org](https://www.geogebra.org/m/j8sqvje7)  

 [Movimiento Arm&oacute;nico Simple](http://www.sc.ehu.es/sbweb/fisica3/oscilaciones/mas/mas.html)  (versión HTML5)  
  
 [Mechanics with animations and film clips: Physclips.](http://www.animations.physics.unsw.edu.au/labs/oscillations/oscillations_lab.html)   
School of Physics, Sydney, cc-by-nc-nd  
  
 [mas.htm](http://iesfgcza.educa.aragon.es/depart/fisicaquimica/fisicasegundo/mas.htm)   
Recopilación de recursos: simulaciones, exámenes ...  
Elaborado por Luis Ortiz de Orruño  
  
PRISMA. Programa de nuevas tecnologías  
El movimiento oscilatorio  
 [Movimiento oscilatorio](http://enebro.pntic.mec.es/~fmag0006/Prism500.html)   
Apuntes y applets  
  
SIMPLE HARMONICMOTION  
© Department of Physics, University of Guelph  
 [Q.shm.html](http://www.physics.uoguelph.ca/tutorials/shm/Q.shm.html)   
  
MAS. Jorge Lay Gajardo.  
 [mas_oct2006.swf](http://www.profisica.cl/images/stories/animaciones/mas_oct2006.swf)  
 
[Función periódica - noria - geogebra.org](https://www.geogebra.org/m/pphpkqns)   
Simulación orientada a matemáticas pero que permite ver la periodicidad y la relación con la proyección de un MCU   

[Energías del MAS - geogebra Enrique García de Bustos Sierra](https://www.geogebra.org/m/jtjppahd)   

### Péndulo
El péndulo doble enlaza con  [teoría del caos](/home/recursos/fisica/teoria-del-caos)   
  
 [El P&eacute;ndulo](http://www.walter-fendt.de/html5/phes/pendulum_es.htm) © Walter Fendt, 21 Mayo 1998  
© Traducción: Juan Muñoz, 9 Marzo 1999  
  
 [Laboratorio&#32;de&#32;P&#233;ndulo 2.03](http://phet.colorado.edu/sims/pendulum-lab/pendulum-lab_es.html)  (Flash) [pendulum-lab_es.html](https://phet.colorado.edu/sims/html/pendulum-lab/latest/pendulum-lab_es.html)  (HTML5)  
Universidad de colorado.  
*Permite ver vectores velocidad, aceleración, energías cinética y potencial*  
  
 [Ley del péndulo  | Educaplus](http://www.educaplus.org/play-130-Ley-del-p%C3%A9ndulo.html)   
 [Péndulo - influencia de g  | Educaplus](http://www.educaplus.org/play-129-P%C3%A9ndulo-influencia-de-g.html)   
  
Pendulum Studio, Voladd (péndulo en general, incluye péndulo doble)  [](https://play.google.com/store/apps/developer?id=Voladd)   
 [Pendulum Studio - Apps on Google Play](https://play.google.com/store/apps/details?id=com.vlvolad.pendulumstudio)   
 [Péndulo - influencia de g](http://www.educaplus.org/play-129-P%C3%A9ndulo-influencia-de-g.html) 


## Cosas distintas de muelles y péndulos
Puede ser interesante usar otros recursos, algunos los menciono en los  [apuntes de elaboración propia de 2º de bachillerato](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato)   
- Si realizamos un agujero que vaya de una cara de la Tierra al otro extremo pasando por su centro, y dejásemos caer un objeto por él, realizaría un MAS. Se puede trabajar este ejemplo en 2º de Bachillerato una vez vista la parte de gravitación. (ver en  [ejercios de elaboración propia de física de 2º de bachillerato](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato) )  
 [Viaje por el interior de la Tierra](http://www.sc.ehu.es/sbweb/fisica/celeste/tunel/tunel.htm#T%C3%BAnel%20por%20el%20interior%20de%20la%20Tierra)   
- Cuerpo en equilibrio flotando que se sumerge distancia A y se deja libre.  
- Si colocamos una bola en una botella de modo que la cierre herméticamente, y la desplazamos respecto de su posición de equilibrio, realizará un MAS. Al bajar la bola aumenta la presión en el interior, y al subir la bola disminuye la presión en el interior, por lo que se ve la idea de fuerza recuperadora que la lleva hacia el equilibrio. (Más información: método de Rüchardt y de Rinkel para medir coeficiente adiabático de un gas)  
- Circuito LC sin pérdidas. Si conectamos un condensador cargado a una bobina, el condensador empezará a descargarse a través de la bobina; la tensión disminuye y la corriente aumenta. Se llega a una expresión similar a un MAS donde V equivale a posición, L equivalente a masa, y 1/C equivale a la constante elástica.  
- Movimiento ideal de una carga eléctrica cerca de ciertas distribuciones de carga  
Un ejemplo es el modelo atómico de Thomson  
 [Modelo atmico de Kelvin-Thomson](http://www.sc.ehu.es/sbweb/fisica/elecmagnet/campo_electrico/atomo/atomo.htm#Movimiento%20del%20electr%C3%B3n%20en%20el%20%C3%A1tomo%20de%20Kelvin-Thomson)   
- Una esfera rodando sin rozamiento en el interior de una semicircunferencia>Se puede asociar a un patinador en una pista con conservación de energía mecánica.  
- Ciertas manchas solares ...  
 [Cómo ilustrar el movimiento armónico simple usando las manchas solares - La Ciencia de la Mula Francis](http://francis.naukas.com/2015/04/22/como-ilustrar-el-movimiento-armonico-simple-usando-las-manchas-solares)   
  
 [11058206.pdf](http://core.ac.uk/download/files/334/11058206.pdf)   
Notas de Clase: Física de Oscilaciones, Ondas y Óptica (Versión 02) 2013, NOTAS DE CLASE FISICA DE OSCILACIONES, ONDAS Y OPTICA Hernán Vivas C. Departamento de Física. Universidad Nacional de Colombia, Sede Manizales  
*"El propósito de este texto es múltiple: i) Organizar y actualizar un material de trabajo que permita a docentes y estudiantes una aproximación más real (en términos pedagógicos) al fenómeno de las oscilaciones. ..."*  
En página 7 se cita "péndulo simple, péndulo de torsión, circuito LC, péndulo físico, tubo en forma de U sección uniforme" y tiene ejemplos, por ejemplo " esfera sólida de   
radio r rueda sin deslizar en un canal cilíndrico de radio R"  

## Resonancia

[![](https://img.youtube.com/vi/tqSuJpJT8j0/0.jpg)](https://www.youtube.com/watch?v=tqSuJpJT8j0 "Rotura de Copa por Resonancia.  Dpto Física y Química IES Valle del Saja")

[![](https://img.youtube.com/vi/zNAEgUBss4A/0.jpg)](https://www.youtube.com/watch?v=zNAEgUBss4A "Copa rota por resonancia 2018.  Dpto Física y Química IES Valle del Saja")

[Resonancia dos diapasones - instagram](https://www.instagram.com/reel/Cnq1Mm3hvLn/?igshid=MDJmNzVkMjY%3D)  
