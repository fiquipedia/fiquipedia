# Acceso universidad LOMLOE 

Página creada para ir recopilando información. Se supone que la primera será en 2024, y debe definirse antes.

Enlaza con los cambios en [currículo LOMLOE](/home/legislacion-educativa/lomloe/curriculo) y en [evaluación LOMLOE](/home/legislacion-educativa/lomloe/evaluacion)  

Relacionado con [currículo LOMLOE concepto y estructura](/home/legislacion-educativa/lomloe/curriculo-concepto-estructura)  

### 14 febrero 2022
[La Selectividad se sustituirá por una prueba menos basada en contenidos y parecida a los exámenes de PISA - elpais.com](https://elpais.com/educacion/2022-02-14/la-selectividad-se-sustituira-por-una-prueba-menos-basada-en-contenidos-parecida-a-los-examenes-de-pisa.html)  

Al citar PISA, se pueden ver ejemplos en [Pruebas de evaluaciones internacionales](/home/pruebas/pruebas-evaluaciones-internacionales)   

[twitter ProfeJander/status/1493529443444346882](https://twitter.com/ProfeJander/status/1493529443444346882)  
Excelente idea. Propongo también construir edificios menos basados en cimientos y que se parezcan a la Torre de Pisa.  

## 10 abril 2022
[Expertos piden reformar Selectividad hacia un modelo que fomente el razonamiento - elcorreo.com](https://www.elcorreo.com/sociedad/educacion/expertos-piden-reformar-20220411211511-nt.html)  

> Creen que los cambios curriculares de la ESO y Bachiller «no tienen sentido» si la prueba de acceso a la universidad mantiene un enfoque «tan memorístico»

## 1 junio 2022
[¿Cómo sería una selectividad competencial? Así lo hacen otros países. La implementación del currículum competencial en la Educación provocaría una reforma inevitable en la Evaluación de Bachillerato para el Acceso a la Universidad (EBAU)](https://www.magisnet.com/2022/06/como-seria-una-selectividad-competencial-asi-lo-hacen-otros-paises/)  


## 10 junio 2022
[Cinco posibles cambios en la Selectividad y los obstáculos que se encuentran](https://elpais.com/educacion/2022-06-10/como-debe-ser-la-nueva-selectividad-cinco-posibles-cambios-en-los-examenes-y-los-obstaculos-que-los-frenan.html)  

Reformar la Evau, que está en revisión, es clave para crear un nuevo Bachillerato. Estas son las transformaciones que, según los expertos, debería experimentar la prueba y los problemas con que se encuentran

>Está previsto que la nueva Selectividad empiece a aplicarse en junio de 2024 y la estrene el alumnado que en septiembre empezará primero de Bachillerato. La intención del Ministerio de Educación es que las características de la nueva prueba se conozcan antes del inicio del próximo curso, para que estudiantes y profesorado sepan desde el principio de la etapa a qué prueba se enfrentarán al finalizarla. La Evaluación de Acceso a la Universidad (Evau) cambiará, pero seguramente no tanto como para impulsar una transformación profunda del Bachillerato, coinciden las fuentes consultadas para este artículo, algunas de las cuales piden no ser citadas. Primero, porque convertir la Selectividad en una prueba realmente competencial sería complejo. Y segundo, porque las inercias y los intereses de los actores implicados, profesorado, comunidades autónomas y sobre todo las Universidades, tienden de forma natural a frenarlo.

La dificultad de trasplantar PISA

Reducir el número de exámenes

Examen oral de inglés

Reforzar la parte competencial que ya tiene la prueba

El vuelco demográfico

## 21 junio 222

No es específicamente de acceso universidad, pero se habla de cómo evaluar Bachillerato en LOMLOE

[twitter FiQuiPedia/status/1539291398549258242](https://twitter.com/FiQuiPedia/status/1539291398549258242)  
He buscado la fuente para ver si era real  
[Telenotícies migdia - 21/06/2022 - ccma.cat/tv3](https://ccma.cat/tv3/alacarta/telenoticies/telenoticies-migdia-21062022/video/6165062/) min 22 a 25  
También de hoy sobre bachillerato competencial [Els matins - 21/06/2022 - ccma.cat/tv3](https://ccma.cat/tv3/alacarta/els-matins/els-matins-21062022/video/6165017/)  1h 21 min a 34 min  
Potabilizadora a químico/a:  
¿Qué cantidad de fluoruro por L de agua?  
Ni idea, pero te digo pros y contras.  
[twitter FiQuiPedia/status/1539295192087502848](https://twitter.com/FiQuiPedia/status/1539295192087502848)  
Convencional:  
(2 ejemplos recientes EvAU Madrid)  
Competencial: indique los pros y los contras de utilizar isótopos en diagnóstico.  

Médico a físico:  
¿Qué dosis debo inyectar a un paciente de 70 kg?  
Ni idea, pero te digo los pros y contras.  


## 23 julio 2022
[Menys memoritzar i més problemes reals: així seran els exàmens de batxillerat (i de les PAU)](https://www.ara.cat/societat/educacio/menys-memoritzar-mes-problemes-reals-aixi-seran-examens-batxillerat-pau_1_4442182.html)  	
Educació prepara, amb 50 professors, models d'exàmens competencials que s'enviaran aviat als centres  

## 27 julio 2022

[La nueva EBAU recogerá una prueba de madurez como principal novedad - educacionyfp.gob.es](https://www.educacionyfp.gob.es/prensa/actualidad/2022/07/20220727-nuevaebau.html)  

>El Ministerio propone así un modelo transitorio durante los cursos 2023-2024, 2024-2025 y 2025-2026, en los que la prueba se dividirá en cuatro ejercicios, todos ellos con una ponderación del 25%. Un ejercicio será sobre la materia de modalidad de prueba elegida por el alumno o alumna y dos de ellos serán de las materias comunes de Historia de la Filosofía e Historia de España. El cuarto ejercicio tendrá carácter general y evaluará las destrezas asociadas al ámbito lingüístico. En este ejercicio se pretende evaluar principalmente la madurez académica del alumnado.  
>Pasado el periodo transitorio, a partir del curso 2026-2027, este ejercicio general de madurez pasará a tener una ponderación del 75% y sustituirá a los ejercicios de las materias comunes. Se mantendrá, con una ponderación del 25%, el ejercicio específico sobre la materia de modalidad elegida por el estudiante. La prueba quedará así dividida en dos ejercicios: uno más globalizado y otro de materia, ambos con un enfoque competencial.  

[Presentación de la propuesta de nueva prueba de acceso a la universidad (simplificada)](https://www.educacionyfp.gob.es/dam/jcr:fe46dfb9-07a5-4482-8ebb-e070f157de77/presentaci-n-simplificada-evau.pdf)  

[El Gobierno propone que la nueva Selectividad incluya la mitad de exámenes y una prueba de madurez - eldiario.es](https://www.eldiario.es/sociedad/gobierno-cambia-selectividad-propone-tenga-examenes-crea-prueba-madurez-academica_1_9203644.html)  

[Las autonomías del PP critican la nueva Selectividad porque no prevé un examen único para todo el Estado - elpais.com](https://elpais.com/educacion/2022-07-27/las-autonomias-del-pp-critican-la-nueva-selectividad-porque-no-preve-un-examen-unico-para-todo-el-estado.html)  
Las competencias educativas están transferidas y no existe un bachillerato unificado, por lo que la evaluación conjunta es, a juicio del Gobierno, inviable. Ni Aznar ni Rajoy impulsaron un cambio en el modelo  

[La Comunidad de Madrid critica que la propuesta del Gobierno de la nación para acceder a la Universidad “no avanza hacia una prueba única para España”  - comunidad.madrid](https://www.comunidad.madrid/noticias/2022/07/27/comunidad-madrid-critica-propuesta-gobierno-nacion-acceder-universidad-no-avanza-prueba-unica-espana)  

## 28 julio 2022
[Selectividad: Desatar el nudo gordiano - elpais.com](https://elpais.com/educacion/2022-07-28/selectividad-desatar-el-nudo-gordiano.html)  
> Hemos asumido la retórica de las competencias, pero seguimos, sobre todo desde secundaria, impartiendo contenidos  

> Esto incendiará el debate sobre un problema derivado: si, examinando contenidos, el distinto nivel de exigencia de las comunidades ya comprometía la igualdad de oportunidades a escala nacional, evaluando competencias, más complejo e impreciso, puede empeorar.  

[La disparidad de resultados entre autonomías en la EVAU se origina en la escuela, no en el examen - elpais.com](https://elpais.com/educacion/2022-07-28/la-disparidad-de-resultados-entre-autonomias-en-la-evau-se-origina-en-la-escuela-no-en-el-examen.html)
> Las disparidades prosiguen en los exámenes de Selectividad (hay 17 modelos distintos), no solo por el contenido ―la forma de abordar la asignatura es distinta― o el tipo de prueba ―unas más memorísticas que otras―, sino también por los criterios de corrección. No están unificados, y eso sí intentará solucionarlo la nueva EVAU (Evaluación para el Acceso a la Universidad) propuesta el miércoles por el Gobierno.  

## 30 julio 2022
[Cómo medir la madurez en Selectividad y cómo se hace en el resto del mundo - huffingtonpost.es](https://www.huffingtonpost.es/entry/como-medir-la-madurez-en-selectividad-y-como-se-hace-en-el-resto-del-mundo_es_62e29845e4b07f83766cd7a8)  
> Así será la prueba de madurez académica  
> El ejercicio general de madurez del alumnado constará de un dossier que se completará con varios documentos sobre un mismo tema escogido por el propio estudiante. El trabajo podrá basarse en un tema de actualidad, científico, humanístico, entre otros.  
> La composición del examen estará dividida en tres partes. Primero un apartado destinado a “la lectura detenida y el análisis”. Después, uno segundo que constará de 15 o 20 preguntas “cerradas o semiconstruidas”. Y, por último, tres preguntas abiertas que servirán para evaluar “las destrezas asociadas al ámbito lingüístico” del alumno.  

## 31 julio 2022
[Una Selectividad que dé sentido a lo aprendido: compleja de diseñar e igual de exigente para los alumnos - elpais.com](https://elpais.com/educacion/2022-07-31/una-selectividad-que-de-sentido-a-lo-aprendido-compleja-de-disenar-e-igual-de-exigente-para-los-alumnos.html)  

## 1 agosto 2022
[Evaluar competencias - elpais.com](https://elpais.com/opinion/2022-08-01/evaluar-competencias.html)  
> Para hacerlo, el nuevo ejercicio que evaluará la madurez académica en la nueva Selectividad parte de un dosier sobre un tema concreto con documentos de todo tipo —textos, imágenes, material infográfico, gráficos— que se proporcionará a cada alumno y sobre el que se le harán varias preguntas para medir su “capacidad de pensamiento crítico, reflexión y madurez”, y que permitirá también a través de determinadas cuestiones valorar sus destrezas en el “ámbito lingüístico” (lenguas oficial, cooficial y extranjera).  

## 5 septiembre 2022

[NUEVA EBAU. PROPUESTA DEL MINISTERIO DE EDUCACIÓN - fsie.es](https://www.fsie.es/actualidad-comunicacion/2981-nueva-ebau-propuesta-del-ministerio-de-educacion)  
Se cita la propuesta de 27 julio 2022, pero además de la presentación simplificada de web ministerio incluye documento adicional  
[Propuesta EBAU texto completo (pdf)](https://www.fsie.es/documentos/ficheros/Propuesta_EBAU/Propuesta_EBAU_texto_completo.pdf)  

## 13 septiembre 2022
Consulta pública  
[Proyecto de Real Decreto por el que se establecen las características básicas de la prueba de acceso a la universidad](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/consulta-publica-previa/abiertos/2022/prd-acceso-universidad.html)  
Fecha de inicio: 13 de septiembre de 2022  
Fecha límite para la remisión de aportaciones: 3 de octubre de 2022  
Remision de aportaciones: tramitacion.sgoa@educacion.gob.es  
[Documento de consulta publica previa (pdf)](https://www.educacionyfp.gob.es/dam/jcr:e69264b3-2136-4612-8f35-9a59f5865d47/20220908-ficha-cpp-rd-pau.pdf)  

## 23 septiembre 2022
[No a la reforma de las pruebas de acceso a la Universidad - change.org](https://www.change.org/p/no-permitamos-que-la-propuesta-del-gobierno-se-lleve-a-la-pr%C3%A1ctica)  

## 11 octubre 2022
Se filtra un documento del Ministerio  
[Borrador propuesta NUEVO MODELO DE PRUEBA DE ACCESO A LA UNIVERSIDAD Y SU IMPLANTACIÓN GRADUAL (11 de octubre de 2022)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/legislacion-educativa/lomloe/2022-10-11-Propuesta%20prueba%20acceso%20universidad_V2.pdf)

## 20 octubre 2022
[Educación inicia un 'road show' por toda España para lograr un pacto en Selectividad - elmundo.es](https://www.elmundo.es/espana/2022/10/20/63503664e4d4d8485a8b45bb.html)  

## 2 noviembre 2022
[La idea de Madrid para cambiar la Selectividad que ha acabado por no defender ni el Ejecutivo regional - elpais.com](https://elpais.com/educacion/2022-11-02/los-responsables-educativos-entierran-la-propuesta-de-madrid-de-establecer-una-doble-prueba-de-selectividad.html)  

[Castilla y León insiste en trabajar en una EBAU única para asegurar la igualdad de oportunidades entre todos los jóvenes - jcyl.es](https://comunicacion.jcyl.es/web/jcyl/Comunicacion/es/Plantilla100Detalle/1281372051501/NotaPrensa/1285216802930/Comunicacion)  

[Andalucía lamenta "falta de receptividad" del Gobierno hacia sus propuestas para una Evau única en el país - lavanguardia.com](https://www.lavanguardia.com/local/sevilla/20221102/8591604/andalucia-lamenta-falta-receptividad-gobierno-propuestas-evau-unica-pais.html)  

## 18 noviembre 2022
[La Real Academia Española critica la reforma de la Selectividad planteada por el Gobierno - elpais.com](https://elpais.com/educacion/2022-11-18/la-rae-entra-en-el-debate-educativo-y-critica-la-reforma-de-la-selectividad-planteada-por-el-gobierno.html)  

## 25 noviembre 2022
[Selectividad: esta reforma está inmadura - elpais.com](https://elpais.com/opinion/2022-11-25/selectividad-esta-reforma-esta-inmadura.html)

## 2 diciembre 2022
[El Ministerio de Educación baraja aplazar la nueva EBAU - cadenaser.com](https://cadenaser.com/nacional/2022/12/02/el-ministerio-de-educacion-baraja-aplazar-la-nueva-ebau-cadena-ser/)  

[Educación aplazará la reforma de la Selectividad para conseguir “más consenso” - eldiario.es](https://www.eldiario.es/sociedad/educacion-aplazara-reforma-evau-pedirselo-15-comunidades-autonomas_1_9765373.html)  

## 3 diciembre 2022
[twitter Pilar_Alegria/status/1598991563505344512](https://twitter.com/Pilar_Alegria/status/1598991563505344512)  
El consenso y el acuerdo son importantes en todo, pero en #educación son fundamentales.  
Con la nueva #EBAU siempre trabajamos con ese planteamiento.  
Por eso y por aclarar, abro hilo  
En julio presentamos un documento abierto a las comunidades y universidades. Documento para debatir y mejorar.  
Desde entonces, hemos mantenido 20 reuniones y hemos recogido las sugerencias y mejoras que nos trasladaron.  
Ayer mantuvimos una reunión con las CCAA, 15 de ellas nos pedían posponer un año la aplicación de la nueva prueba.  
Recordemos: docentes y alumnos han comenzado este curso con la nueva ley educativa, por eso, entendemos que es positivo ampliar la aplicación gradual un año más  
¿Qué significa?   
La nueva #EBAU se desplegará en junio de 2028 y no en junio de 2027.  
Hasta ese momento las pruebas que realizarán los estudiantes serán similares a las que ya vienen haciendo. Y en 2024 se irán incluyendo pruebas competenciales de manera gradual.  
Vamos a seguir trabajando con todos para generar una prueba equiparable en todos los territorios y respetuosa con las competencias de cada uno.  
Una prueba también pensada para los chicos y chicas y en las nuevas formas de aprender y de enseñar.  
Mejorar, modular, flexibilizar planteamientos iniciales nunca me ha parecido negativo. Creo que escuchar a los demás es clave para gestionar  
El objetivo de todos es el mismo: mejorar la educación. Eso pasa por abandonar posiciones inamovibles, escuchar, respetarnos y consensuar  

## 8 diciembre 2022
[Otra vuelta a la nueva Selectividad - elpais.com](https://elpais.com/opinion/2022-12-08/otra-vuelta-a-la-nueva-selectividad.html)  

Análisis 
[twitter SalvadorrPons/status/1601111733397225473](https://twitter.com/SalvadorrPons/status/1601111733397225473)  
El País, en su edición de ayer, nos dedicó su editorial:  
Tamaño honor merece un hilo.  
El editorial defiende la reforma de las EBAU con tres argumentos:  
1. "La modificación de la Selectividad es un punto imprescindible de la reforma educativa (la Lomloe, que entró en vigor en 2021)". Obsérvese que no se explica por qué sino que simplemente se asume. Una vez se instala en la opinión pública la idea de que la reforma es "imprescindible" (cuando el examen actual comenzó a implantarse en 2020) se ha ganado la primera batalla.  
Supongo que, por razones de espacio, no han podido desarrollar la razón que aducía el borrador de la propuesta: porque el examen determina los contenidos que se vayan a enseñar de forma efectiva en Bachillerato.  
No se puede dejar que un Bachillerato depauperado se choque con la prueba anterior porque, en tal caso, las deficiencias del nuevo sistema quedarían en evidencia. Esto sí que explica por qué la reforma es imprescindible.  
2. "El nuevo enfoque de la enseñanza comporta el paso de un modelo tradicional, basado en exceso en los contenidos y la memoria, a otro de aprendizaje por competencias".  
Aquí tenemos el leit motiv de la reforma: la oposición memoria ~competencias.  
Esta oposición es falsa, [como ya he defendido en mi canal](https://youtube.com/watch?v=WvGY3hYlJ7g): en primer lugar, porque "competencial" es un término vacío; en segundo lugar, porque la enseñanza no era memorística hace dos años;  
por último, porque la enseñanza de lenguas siempre ha sido "competencial" (y nadie ha sido capaz de rebatir esto). Pero, ya se sabe, una mentira repetida cien veces se convierte en una verdad. El País, quién te ha visto y quien te ve…  
3. "…en línea con los países más avanzados". Aquí tenemos una argumentación que va de lo normal a la norma: si queremos ser un país avanzado, debemos hacer como ellos. Estas son las EBAU en Francia:  
[Baccalauréat, brevet, CAP, Parcoursup : le calendrier des épreuves 2023](https://www.education.gouv.fr/reussir-au-lycee/baccalaureat-brevet-cap-parcoursup-le-calendrier-des-epreuves-2023-341384)  
Y aquí tenemos información sobre el [Abitur alemán (por regiones) en Wikipedia](https://es.wikipedia.org/wiki/Abitur_en_Baviera)  
[Un enlace a las pruebas de Matemáticas](https://abiturma.de/mathe-abituraufgaben/alle-bundeslander/alle-jahre/alle-themen)  
[E información sobre la de Alemán](https://magazin.sofatutor.com/schueler/deutsch-abitur-themen/#allgemein)  
¿Por qué una prueba tan brillante se aplaza?  El País:  
"El desarrollo de esta transformación se ha retrasado por la falta de programas […] de formación del profesorado, un calendario precipitado […] y el escaso rodaje entre docentes y estudiantes de los exámenes competenciales"  
Es decir, no por deficiencias intrínsecas de la propuesta, sino por la incapacidad del sistema (formación, organización, mentalidad) para absorber tanta novedad.  
Señala El País que las críticas "deben tenerse en cuenta para que la prueba no rebaje ni el volumen de conocimiento ni el elevado nivel de exigencia esperable". En tal caso, se debería devolver el proyecto al cajón del que nunca debió salir.  
Me parece destacable que se hable del "volumen de conocimiento" y de "elevado nivel de exigencia". Aquí tenemos un ejemplo de la mezcla entre "argumentos de algodón" y "argumentos de mercado" [de la que hablé aquí](https://www.youtube.com/watch?v=jJ2pQ1B1C2Y)  
Y se cierra la argumentación con el siguiente argumento: "El gran desafío de la educación sigue siendo reforzar las capacidades de los alumnos para que puedan sacar lo mejor de sí en un mundo en constante cambio".  
La educación se enfrenta a un "desafío" (qué raro que no hable de "un reto"), no a un problema, y se alude al mundo en constante cambio como tarea por resolver. Este es uno de los argumentos del discurso seudopedagógico: cambia, todo cambia.  
Así, como "todo lo sólido se desvanece en el aire"(Berman) y, por tanto, estamos en un "mundo líquido" (Bauman), la educación debe cambiar (1'13"):  
[2 Mitos de la seudopedagogía](https://www.youtube.com/watch?v=4mVfvpOmqnc&list=PLmZYgYlIdnL7TXD_5OQkBzqHgXGR5wlT6)  
Muchas gracias a El País por este ejercicio de síntesis argumentativa, que bien merecería ser escogido para el ejercicio de comentario de texto, si nos dejan que siga existiendo, claro.  

## 13 diciembre 2022

[La ministra Pilar Alegría informa a los consejeros autonómicos de la ampliación de un año del periodo de implantación de la nueva prueba de acceso a la universidad](https://www.educacionyfp.gob.es/prensa/actualidad/2022/12/20221213-sectorial.html)  

[El Gobierno y la mayoría de autonomías deciden aplazar la nueva Selectividad frente a un PP que rompe los puentes de la negociación - elpais.com](https://elpais.com/educacion/2022-12-13/el-gobierno-y-la-mayoria-de-autonomias-deciden-aplazar-la-nueva-selectividad-frente-a-un-pp-que-rompe-los-puentes-de-la-negociacion.html)

[Educación rechaza la Selectividad única que pide el PP y avisa: modificará la prueba con o sin los populares - eldiario.es](https://www.eldiario.es/sociedad/educacion-rechaza-selectividad-unica-pide-pp-avisa-modificara-prueba-populares_1_9791420.html)  

## 17 diciembre 2022
[Antonio Amante: “Hay un catastrofismo en cómo se nos ve a los adolescentes que no entiendo” Con 19 años, el presidente de la Confederación Estatal de Asociaciones de Estudiantes y miembro del Consejo Escolar del Estado cree que el cambio previsto para la EVAU es necesario](https://elpais.com/educacion/2022-12-17/antonio-amante-la-selectividad-no-puede-seguir-consistiendo-en-vomitar-en-tres-dias-todos-los-contenidos-de-un-curso.html)  

[José Manuel Bar: “El PP piensa que es mejor un sistema educativo en el que se suspende más y se repite más” - eldiario.es](https://www.eldiario.es/sociedad/jose-manuel-bar-pp-piensa-mejor-sistema-educativo-suspende-repite_128_9796187.html)  

>  Vamos a empezar por lo más reciente, que es la Ebau (antigua Selectividad). El PP amenazó con abandonar las mesas de trabajo en la última reunión ante la reforma que está planteando Educación y usted les acusó de electoralismo. ¿No se están moviendo en términos educativos?  
La primera prueba es que sus declaraciones fueron desde Génova. Yo creo que para llegar a acuerdos a lo mejor habría que sentar a \[el presidente del PP, Alberto Núñez] Feijoó, más que a los consejeros de las comunidades. No sé si a Feijoó y a \[la presidenta de la Comunidad de Madrid, Isabel Díaz] Ayuso, pero creo que es una consigna de naturaleza política y que además viene de antiguo. Lo que pasa es que cuando lo sometes a la prueba del algodón, al 'vamos a trabajar, vamos a diseñar, vamos a hacerlo juntos', se rompen las costuras porque al PP nunca le gustó la ley y por tanto no le gusta el desarrollo normativo de la ley. Me recuerda bastante a lo que el PP está haciendo en otros ámbitos, y creo que lo vamos a ver con más intensidad a medida que se vayan acercando las elecciones porque es una estrategia no educativa, sino política.  
Explique qué quiere hacer el ministerio con esta reforma, además de adaptarla a la Lomloe. Cuénteselo a una familia que no está metida en el mundo educativo y se le pueden escapar los tecnicismos.  
Lo que no podemos es hacer un diseño en el que se enseña y se aprende de una manera, pero después se evalúa de una forma distinta. Eso no es una prueba válida. Hay que cambiar el chip. No pretendemos hacer unas pruebas de acceso a la universidad ni más fáciles ni más difíciles, sino distintas. El PP quiere recuperar la reválida, esa reválida que perdió por toda la oposición social que tuvo en su día. El diseño que pretenden no sólo es centralizador, en el que todas las comunidades tengan que hacer forzosamente lo mismo: es el diseño de la reválida en la que se evalúan otra vez todos los contenidos compartimentados por las materias que se hayan estudiado en el Bachillerato. Esto es precisamente lo que nosotros no queremos. Queremos evaluar una competencia de carácter general que nos asegure que alumnos y alumnas van a seguir cursando con éxito sus estudios universitarios, que tienen esa madurez académica general \[así, Prueba de Madurez, se llama la prueba en torno a la que se articulará el futuro examen].  
Pero se les acusa de bajar el nivel con su propuesta.  
No, no, no. Cuando se dice que se va a rebajar el nivel, ¿qué quiere decir? ¿Que en Bachillerato se va a enseñar menos? Porque a la Ebau no se va a aprender, a la Ebau se viene aprendido de casa. Si el Bachillerato cumple su función con éxito es independiente que la prueba sea más fácil o más difícil. El nivel se ha adquirido antes. La Ebau tal y como se concibe ahora está sometida a un montón de críticas. El profesorado en Bachillerato no tenía margen de autonomía porque estaba pendiente de que en qué iba a consistir la prueba. Es un entrenamiento para aprobar un determinado examen. Esto, que se estaba criticando desde hace tantos años, es lo que queremos cambiar.   
El PP debe decir con claridad si le parece demasiado fácil la Ebau, porque ahora mismo la está aprobando el 92%. ¿La quieren hacer más difícil? Explíqueselo a los alumnos. ¿Nos parece demasiado que aprueben el 92% y queremos hacer una Ebau más difícil por aquello del mantra de la cultura del esfuerzo? Explíqueselo con sinceridad a la comunidad educativa. Expliquen, por ejemplo, que no debe aprobar la Ebau más del 75%. Pero esto no lo dicen porque su modelo causó el rechazo de toda la comunidad educativa y muy particularmente de los estudiantes en su día.  
Desde los grupos políticos se hacen críticas que se pueden achacar a argumentos políticos, pero que haya grupos como la Red de Filosofía que también estén criticando, ¿no son críticas educativas?  
Nosotros vamos a ser muy sensibles a todas las propuestas que quieran llegar de esas asociaciones o grupos más corporativos, pero no nos han enviado ninguna. Estamos deseosos de recibirlas, y si llegan van a ser escuchadas. Me sorprende mucho que se critiquen los contenidos cuando nadie ha publicado los criterios de la prueba; se están trabajando todavía. Creo que se han precipitado un poco en la crítica. El debate está abierto, pero no queremos reeditar una reválida. Cuando se dice que no está determinado contenido, eso podría ser una crítica que tuviese sentido en el anterior modelo. Pero para disipar esas dudas hemos hecho un modelo transitorio, de ir haciendo más competenciales progresivamente las pruebas. No vamos a hacer un vuelco total en la Ebau: partiendo de la estructura actual por asignaturas vamos a ir dándole poco a poco el giro hacia un modelo más competencial y no tan basado en el embutir contenidos que se han de vomitar después en un examen.  

## 22 diciembre 2022
[La Selectividad mantendrá la actual estructura de exámenes por asignaturas hasta 2028 - elpais](https://elpais.com/educacion/2022-12-22/la-selectividad-mantendra-la-actual-estructura-de-examenes-por-asignaturas-hasta-2028.html)  

## 31 diciembre 2022
[La nueva selectividad: un modelo competencial y homologado con otras pruebas europeas - infolibre.es](https://www.infolibre.es/opinion/plaza-publica/nueva-selectividad-modelo-competencial-homologado-pruebas-europeas_129_1396445.html)  
 Mª Luz Martínez Seijo es secretaria de Educación y FP del PSOE.   
 
## 8 febrero 2023

[El nuevo decreto de selectividad aumenta la duración de los exámenes y cambia las reglas para revisar su corrección - elpais.com](https://elpais.com/educacion/2023-02-08/el-nuevo-decreto-de-selectividad-aumenta-la-duracion-de-los-examenes-y-cambia-las-reglas-para-revisar-su-correccion.html)  

[Educación propone aumentar la duración de los exámenes de Selectividad hasta pasar al nuevo modelo en 2027 - eldiario.es](https://www.eldiario.es/sociedad/educacion-propone-aumentar-duracion-examenes-selectividad-pasar-nuevo-modelo-2027_1_9934727.html)  

[Alegría fulmina la prueba de madurez y las preguntas test en la Selectividad y da más ventajas al alumnado - elmundo.es](https://www.elmundo.es/espana/2023/02/08/63e35d37e4d4d880508b4587.html)  
 
## 14 marzo 2023 
[Estudiantes de 50 centros educativos de diferentes comunidades autónomas participan en la prueba piloto de acceso a la universidad](https://www.lamoncloa.gob.es/serviciosdeprensa/notasprensa/educacion/Paginas/2023/140323-prueba-piloto-acceso-universidad.aspx)  

[¿Aprobarías la nueva EBAU? Así son los exámenes de selectividad que llegarán en 2024](https://cadenaser.com/nacional/2023/03/14/aprobarias-la-nueva-ebau-cadena-ser/)  


[Pruebas liberadas: Piloto de la Prueba de Acceso a la Universidad (PAU)](https://www.educacionyfp.gob.es/inee/evaluaciones-nacionales/piloto-prueba-acceso-universidad/pruebas-piloto-pau-liberadas.html)

## 16 marzo 2023
Se abre trámite de audiencia
[Proyecto real decreto por el que se regulan las condiciones para el acceso y la normativa básica de los procedimientos de admisión a las enseñanzas universitarias oficiales de grado](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2023/prd-acceso-normativa-grado.html)  

Se incluye [el borrador](https://www.educacionyfp.gob.es/dam/jcr:b39994b2-7842-4fd6-9754-4c7590551f18/rd-acceso-admision-universidad-b.pdf)  

> Artículo 14. Estructura de la prueba.  
>1. La prueba de acceso constará de cuatro o, en su caso, cinco ejercicios que versarán
sobre las siguientes materias:  
a) Lengua Castellana y Literatura II y, si la hubiere, Lengua Cooficial y Literatura II.  
b) Historia de España o Historia de la Filosofía, a elección del alumnado. Dicha elección deberá efectuarse en el momento de la inscripción en la prueba de acceso.  
c) Lengua Extranjera II cursada por el alumno o alumna.  
d) La materia específica obligatoria de 2.º de Bachillerato de la modalidad, y en su caso vía, cursada.  

>4. Cada uno de los ejercicios mencionados tendrá una duración de 105 minutos,
estableciéndose un descanso mínimo de 45 minutos entre el final de un ejercicio y el
principio del siguiente.  

## 31 mayo 2023
[Educación paraliza la nueva EBAU por "responsabilidad" ante la convocatoria anticipada de elecciones - europapress.es](https://www.europapress.es/sociedad/educacion-00468/noticia-educacion-paraliza-nueva-ebau-responsabilidad-convocatoria-anticipada-elecciones-20230531154152.html)  
[El Gobierno mantiene la nueva Selectividad para 2024, pero deja la aprobación del decreto para después de elecciones - elpais.com](https://elpais.com/educacion/2023-05-31/el-gobierno-mantiene-la-nueva-selectividad-para-2024-pero-deja-la-aprobacion-del-decreto-para-despues-de-elecciones.html)  
[El Gobierno paraliza la reforma de la Selectividad - eldiario.es](https://www.eldiario.es/sociedad/gobierno-paraliza-reforma-selectividad_1_10255934.html)  

## 31 agosto 2023
[El Gobierno en funciones aplaza la implantación de la nueva EBAU - educacionyfp.gob.es](https://www.educacionyfp.gob.es/prensa/actualidad/2023/08/20230831-aplazamientoebau.html)  
[El Gobierno aplaza la reforma de la EBAU y mantiene sin cambios la prueba para 2024 - eldiario.es](https://www.eldiario.es/sociedad/gobierno-aplaza-reforma-ebau-mantiene-cambios-prueba-2024_1_10477221.html)  
[La EBAU del curso 2023/2024 no se modifica y será igual que hasta ahora - abc.es](https://www.abc.es/sociedad/ebau-ano-modifica-igual-20230831103831-nt.html)  
[El Gobierno aplaza la reforma de la EBAU porque al estar en funciones excedería sus competencias  - cadenaser.com](https://cadenaser.com/nacional/2023/08/31/el-gobierno-aplaza-la-reforma-de-la-ebau-porque-al-estar-en-funciones-excederia-sus-competencias-cadena-ser/)  
[Educación no cambiará la prueba de Selectividad en 2024 y retrasa la implantación de la nueva EBAU un año - rtve.es](https://www.rtve.es/noticias/20230831/educacion-no-cambiara-selectividad-retrasa-ano-ebau/2454936.shtml)  

## 11 octubre 2023

[Trámite de audiencia ministerio de orden por la que se determinan las características, el diseño y el  contenido de la evaluación de Bachillerato para el acceso a la  Universidad. Aportaciones 11/10 a 3/11](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2023/ebau-2024.html)  
[Proyecto de Orden por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la universidad, y las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, en el curso 2023-2024.](https://www.universidades.gob.es/wp-content/uploads/2023/10/AIP21_Orden-EBAU-2023-24.pdf)  
[Los alumnos que realicen la prueba de acceso a la universidad tendrán el mismo número de exámenes que en cursos anteriores](https://www.educacionyfp.gob.es/prensa/actualidad/2023/10/20231011-ebau.html)  

## 24 enero 2024

[Feijóo abre la batalla de la educación contra el Gobierno y anuncia una Ebau común para las autonomías del PP - eldiario](https://www.eldiario.es/politica/feijoo-abre-batalla-educacion-gobierno-anuncia-ebau-comun-autonomias-pp_1_10862941.html)

## 25 enero 2024
[La EBAU común que propone el PP para sus comunidades: qué implicaría para alumnado, docentes y centros - maldita](https://www.newtral.es/ebau-pp-comunidades-autonomas/20240125/)  

## 26 enero 2024

[Orden PJC/39/2024, de 24 de enero, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la universidad, y las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, en el curso 2023-2024.](https://www.boe.es/buscar/act.php?id=BOE-A-2024-1471)

## 17 marzo 2024

[Las faltas de ortografía bajarán un 10% la nota en los exámenes de Selectividad a partir de 2025 en toda España - elpais](https://elpais.com/educacion/2024-03-17/las-faltas-de-ortografia-contaran-al-menos-un-10-en-los-examenes-de-selectividad-a-partir-de-2025-en-toda-espana.html)

## 11 junio 2024

[REAL DECRETO por el que se regulan los requisitos de acceso a las enseñanzas universitarias oficiales de Grado, las características básicas de la prueba de acceso y la normativa básica de los procedimientos de admisión. - lamoncloa.gob.es](https://www.lamoncloa.gob.es/consejodeministros/referencias/Paginas/2024/20240611-referencia-rueda-de-prensa-ministros.aspx#admitir)  

[La nueva prueba de acceso a la universidad tendrá estructura, características básicas y criterios de corrección mínimos comunes - educacionfpydeportes.gob.es](https://www.educacionfpydeportes.gob.es/prensa/actualidad/2024/06/20240611-pau.html)  

## 12 junio 2024

[Real Decreto 534/2024, de 11 de junio, por el que se regulan los requisitos de acceso a las enseñanzas universitarias oficiales de Grado, las características básicas de la prueba de acceso y la normativa básica de los procedimientos de admisión.](https://www.boe.es/buscar/act.php?id=BOE-A-2024-11858)  

## 9 septiembre 2024

[¿Qué es la PAU? Novedades de la Prueba de Acceso a la Universidad - lamoncloa.gob.es](https://www.lamoncloa.gob.es/serviciosdeprensa/notasprensa/educacion-fp-deportes/Paginas/2024/pau-prueba-acceso-universidad.aspx)  
