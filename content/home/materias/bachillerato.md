
# Materias Bachillerato

Enlaces a materias Bachillerato:  
* **[1º Bachillerato Física y Química](/home/materias/bachillerato/fisicayquimica-1bachillerato)**  
* **[2º Bachillerato Física](/home/materias/bachillerato/fisica-2bachillerato)**  
* **[2º Bachillerato Química](/home/materias/bachillerato/quimica-2bachillerato)**  
* [1º Bachillerato Ampliación Física y Química I (LOMCE)](/home/materias/bachillerato/ampliacion-fisica-quimica-1-bachillerato)  (LOMCE, Madrid)  
* [1º Bachillerato Ciencias para el Mundo Contemporáneo (LOE)](/home/materias/bachillerato/cienciasparaelmundocontemporaneo-1bachillerato) Desaparece con LOMCE  
* [1º Bachillerato Cultura Científica (LOMCE)](/home/materias/bachillerato/cultura-cientifica-1-bachillerato) Desaparece con LOMLOE 
* [2º Bachillerato Ciencias Generales (LOMLOE)](/home/materias/bachillerato/ciencias-generales-2-bachillerato)  
* [2º Bachillerato Desarrollo experimental (LOMCE)](/home/materias/bachillerato/desarrollo-experimental-2-bachillerato)  (LOMCE, Madrid)  
* [2º Bachillerato Electrotecnia (LOE)](/home/materias/bachillerato/electrotecnia-2bachillerato) Desaparece con LOMCE  
* [2º Bachillerato Mecánica (LOGSE)](/home/materias/bachillerato/mecanica-2-bachillerato)  
* [2º Bachillerato Tecnología Industrial II (LOE-LOMCE)](/home/materias/bachillerato/tecnologia-industrial-2bachillerato) Desaparece con LOMLOE  
* [2º Bachillerato Tecnología e Ingeniería II (LOMLOE)](/home/materias/bachillerato/tecnologia-e-ingenieria-2-bachillerato) Surge con LOMLOE
* [1º Bachillerato Técnicas experimentales en ciencias (LOE)](/home/materias/bachillerato/tecnicas-experimentales-en-ciencias-1bachillerato)  
* Principios fundamentales de electrónica (pendiente)  
* [Bachillerato Ciencia, Tecnología y Sociedad (LOGSE)](/home/materias/bachillerato/ciencia-tecnologia-y-sociedad-bachillerato)  

Al hablar de 1º y 2º Bachillerato se entiende el que surge con LOGSE; antes en LGE eran 1º, 2º y 3º BUP (Bachillerato Unificado Polivalente) y un curso de COU (Curso de Orientación Universitaria)  



Se incluyen las materias que los profesores de la especialidad de Física y Química pueden impartir en Bachillerato (según esta información  [Especialidades y materias afines. Tabla especialidades - materias](https://sites.google.com/site/especialidadesymateriasafines/home/especialidades-y-materias-afines-secundaria/tabla-especialidades-materias) )  

Existen algunas optativas en bachillerato en Madrid (ver normativa [RESOLUCIÓN de 7 de julio de 2008, de la Dirección General de Educación Secundaria y Enseñanzas Profesionales, por la que se establecen las materias optativas del Bachillerato en la Comunidad de Madrid](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=5164&cdestado=P) ) y ver ORDEN 2200/2017 donde aparece Ampliación de Física y Química I para 1º Bachillerato  

También es importante en Bachillerato aunque no exclusivo de Física y Química la idea de prelación y la idea de número de repeticiones (con LOMCE pasa a poder repetirse una única vez cada curso, ToDO revisar con LOMLOE y unificar con legislación)  


[Real Decreto 1105/2014 por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato. Artículo 32. Promoción.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a32)  
*Sin superar el plazo máximo para cursar el Bachillerato indicado en el artículo 26.3, los alumnos y alumnas podrán repetir cada uno de los cursos de Bachillerato una sola vez como máximo, si bien excepcionalmente podrán repetir uno de los cursos una segunda vez, previo informe favorable del equipo docente.*  

[Real Decreto 1105/2014 por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato. Artículo 26. Organización general.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a26)  
*3. Los alumnos y alumnas podrán permanecer cursando Bachillerato en régimen ordinario durante cuatro años.* 

## Prelación / continuidad
En la Física y Química en Bachillerato existe un concepto importante que es la prelación / continuidad

### LOMCE 
[Real Decreto 1105/2014 por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37)  
[Artículo 33. Continuidad entre materias de Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a33)  
> La superación de las materias de segundo curso que se indican en el anexo III estará condicionada a la superación de las correspondientes materias de primer curso indicadas en dicho anexo por implicar continuidad.  
[Anexo III Continuidad entre materias de Bachillerato](https://www.boe.es/boe/dias/2015/01/03/pdfs/BOE-A-2015-37.pdf#page=378)  

### LOMLOE 
[Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521)  
[Artículo 21. Promoción.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a2-3)  
> 2. La superación de las materias de segundo curso que figuran en el anexo V estará condicionada a la superación de las correspondientes materias de primer curso indicadas en dicho anexo por implicar continuidad.  
No obstante, dentro de una misma modalidad, el alumnado podrá matricularse de la materia de segundo curso sin haber cursado la correspondiente materia de primer curso, siempre que el profesorado que la imparta considere que reúne las condiciones necesarias para poder seguir con aprovechamiento la materia de segundo. En caso contrario, deberá cursar también la materia de primer curso, que tendrá la consideración de materia pendiente, si bien no será computable a efectos de modificar las condiciones en las que ha promocionado a segundo.  

[Anexo V Continuidad entre materias de Bachillerato](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#av)  



