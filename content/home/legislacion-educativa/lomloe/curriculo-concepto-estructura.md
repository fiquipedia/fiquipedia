# Currículo LOMLOE, concepto y estructura

Creo una página para ir recopilando información.  
Aquí pongo los aspectos generales de concepto y estructura del currículo en LOMLOE ver lo general en [Currículo LOMLOE](/home/legislacion-educativa/lomloe/curriculo) 

## Definición de currículo en normativa

El punto de partida es ver la definición de currículo en LOE (que ha ido variando en LOMCE y en LOMLOE)  
Artículo 6bis habla de competencias entre comunidades, no de competencias curriculares.  

[Artículo 6. Currículo.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a6)  

**LOMCE**  (crea estándares, y crea [Artículo 6 bis. Distribución de competencias.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a6bis))   
> 1. A los efectos de lo dispuesto en esta Ley Orgánica, se entiende por currículo la regulación de los elementos que determinan los procesos de enseñanza y aprendizaje para cada una de las enseñanzas.  
>2. El currículo estará integrado por los siguientes elementos:  
a) Los objetivos de cada enseñanza y etapa educativa.  
b) Las competencias, o capacidades para aplicar de forma integrada los contenidos propios de cada enseñanza y etapa educativa, con el fin de lograr la realización adecuada de actividades y la resolución eficaz de problemas complejos.  
c) Los contenidos, o conjuntos de conocimientos, habilidades, destrezas y actitudes que contribuyen al logro de los objetivos de cada enseñanza y etapa educativa y a la adquisición de competencias.  
Los contenidos se ordenan en asignaturas, que se clasifican en materias, ámbitos, áreas y módulos en función de las enseñanzas, las etapas educativas o los programas en que participen los alumnos y alumnas.  
d) La metodología didáctica, que comprende tanto la descripción de las prácticas docentes como la organización del trabajo de los docentes.  
e) **Los estándares y resultados de aprendizaje evaluables.**  
f) Los criterios de evaluación del grado de adquisición de las competencias y del logro de los objetivos de cada enseñanza y etapa educativa.  

**LOMLOE** (mantiene [Artículo 6 bis. Distribución de competencias.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a6bis))   
> 1. A los efectos de lo dispuesto en esta Ley, se entiende por currículo el conjunto de objetivos, competencias, contenidos, métodos pedagógicos y criterios de evaluación de cada una de las enseñanzas reguladas en la presente Ley.  
En el caso de las enseñanzas de formación profesional se considerarán parte del currículo los resultados de aprendizaje.
>2. El currículo irá orientado a facilitar el desarrollo educativo de los alumnos y alumnas, garantizando su formación integral, contribuyendo al pleno desarrollo de su personalidad y preparándoles para el ejercicio pleno de los derechos humanos, de una ciudadanía activa y democrática en la sociedad actual. En ningún caso podrá suponer una barrera que genere abandono escolar o impida el acceso y disfrute del derecho a la educación.
>3. Con el fin de asegurar una formación común y garantizar la validez de los títulos correspondientes, el Gobierno, previa consulta a las Comunidades Autónomas, fijará, en relación con los objetivos, competencias, contenidos y criterios de evaluación, los aspectos básicos del currículo, que constituyen las enseñanzas mínimas. Para la Formación Profesional fijará así mismo los resultados de aprendizaje correspondientes a las enseñanzas mínimas.
>4. Las enseñanzas mínimas requerirán el 50 por ciento de los horarios escolares para las Comunidades Autónomas que tengan lengua cooficial y el 60 por ciento para aquellas que no la tengan.
>5. Las Administraciones educativas establecerán el currículo de las distintas enseñanzas reguladas en la presente Ley, del que formarán parte los aspectos básicos señalados en apartados anteriores. Los centros docentes desarrollarán y completarán, en su caso, el currículo de las diferentes etapas y ciclos en el uso de su autonomía y tal como se recoge en el capítulo II del título V de la presente Ley. Las Administraciones educativas determinarán el porcentaje de los horarios escolares de que dispondrán los centros docentes para garantizar el desarrollo integrado de todas las competencias de la etapa y la incorporación de los contenidos de carácter transversal a todas las áreas, materias y ámbitos.  
Las Administraciones educativas podrán, si así lo consideran, exceptuar los cursos de especialización de las enseñanzas de Formación Profesional de estos porcentajes, pudiendo establecer su oferta con una duración a partir del número de horas previsto en el currículo básico de cada uno de ellos.
>6. Las Administraciones educativas revisarán periódicamente los currículos para adecuarlos a los avances del conocimiento, así como a los cambios y nuevas exigencias de su ámbito local, de la sociedad española y del contexto europeo e internacional.
>7. El Gobierno incluirá en la estructura orgánica del Ministerio de Educación y Formación Profesional una unidad que, en cooperación con las Comunidades Autónomas, desarrolle las funciones a las que se refieren los apartados tercero y cuarto de este artículo y contribuya a la actualización permanente de los currículos que constituyen las enseñanzas mínimas, sin perjuicio de lo previsto para la actualización de currículos de enseñanzas de formación profesional y enseñanzas de régimen especial.
>8. Los títulos correspondientes a las enseñanzas reguladas por esta Ley serán homologados por el Estado y expedidos por las Administraciones educativas en las condiciones previstas en la legislación vigente y en las normas básicas y específicas que al efecto se dicten.  
>9. En el marco de la cooperación internacional en materia de educación, el Gobierno, de acuerdo con lo establecido en el apartado 1 del artículo 6 bis, podrá establecer currículos mixtos de enseñanzas del sistema educativo español y de otros sistemas educativos, conducentes a los títulos respectivos.

## Conceptos y estructura currículo LOMLOE vs LOMCE vs LOE
Aparte de analizar materias concretas, lo interesante es ver el planteamiento global: en LOMCE vs LOE realicé una comparación en los que aparte de detalles propios de cada materia puse unas tabla que empezaban comparando ideas generales

* Nº de sesiones  
* Nº de objetivos y a qué estaban asociados (materia, etapa)  
* Nº de bloques de contenidos y dónde se fijaban/modificaban (estatal, autónomico)  
* Nº de criterios de evaluación y dónde se fijaban/modificaban (estatal, autonómico)  
* Estándares de aprendizaje, a qué estaban asociados y quién los fijaba (estatal, autonómico)

Y terminaba comparando bloques de contenidos  

De manera aproximada había contenidos, cada uno de ellos tenía ciertos criterios de evaluación y cada criterio de evaluación ciertos estándares de aprendizaje evaluables.   

Ahora se pueden mapear contenidos a saberes básicos (ver enlaces en apartado "Comparación currículos" de [currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)) pero hay cambios de fondo que son mayores.  


Al igual que LOMCE introdujo cosas nuevas (los estándares de aprendizaje), LOMLOE introduce términos nuevos, y aunque los estándares sí estaban en artículo 6, ahora estos nuevos no están en BOE, pero aparecen en documentos borradores y en proyectos de Real Decreto de enseñanzas mínimas:   
Se cita texto de varios documentos
* [PERFIL DE SALIDA DEL ALUMNADO AL TÉRMINO DE LA EDUCACIÓN BÁSICA, DOCUMENTO BASE MARCO CURRICULAR DE LA LOMLOE (marzo 2021)](https://www.magisnet.com/wp-content/uploads/2021/03/Perfil-de-salida-del-alumnado-al-te%CC%81rmino-de-la-Educacio%CC%81n-Ba%CC%81sica.pdf)  
* [PROPUESTA DE ESTRUCTURA CURRICULAR PARA LA ELABORACIÓN DE LAS ENSEÑANZAS MÍNIMAS (marzo 2021)](https://www.magisnet.com/wp-content/uploads/2021/03/Borrador-de-Propuesta-Curricular-1.pdf)  (pdf de 7 páginas, incluye definiciones y diagrama en página 7)  
![](https://i1.wp.com/josesande.com/wp-content/uploads/2021/10/image-1.png?w=592&ssl=1)  
* [Proyecto de real decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Infantil (octubre 2021)](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/abiertos/2021/prd-ordenacion-educacion-infantil.html)  Artículo 2. Definiciones.  
* [Proyecto RD de enseñanzas mínimas de Educación Infantil](https://educagob.educacionyfp.gob.es/dam/jcr:cc29fe27-3e03-4f12-bb35-5dfe2c63271f/proyecto-rd-ed-infantil-con-anexos-cge-.pdf) Artículo 2. Definiciones.  
* [Proyecto de real decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Primaria (PDF) (octubre 2021)](https://www.educacionyfp.gob.es/dam/jcr:cb5a4b85-292a-42a9-b2db-10aa7f830dac/proyecto-rd-educaci-n-primaria-con-anexos.pdf)  Artículo 2. Definiciones.  
* [Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/abiertos/2021/prd-ordenacion-ensenanzas-minimas-eso.html) Artículo 2. Definiciones.  
* [Borrador de RD de enseñanzas mínimas de Bachillerato](https://educagob.educacionyfp.gob.es/dam/jcr:916a7585-8ed6-4f16-975b-dc23895a2581/proyecto-rd-bachilleratocompleto-.pdf) Artículo 2. Definiciones.  

Intento enumerar y documentar los nuevos términos  

Si encuentro otras referencias, las intento citar
[Ideas clave de la LOMLOE - Raúl Diego](https://www.rauldiego.es/ideas-clave-de-la-lomloe/)  
En página 28 de 52 se habla de "Elementos del currículo" y tiene "Antiguos elementos vs Nuevos elementos"  

## Tabla resumen / asociación con lo anterior

Intento hacer una tabla que sea muy breve para ver "equivalencia" con lo anterior, que es lo conocido, aunque al abreviar es necesariamente incompleta  
Por ejemplo los objetivos en LOMCE eran de etapa, aunque en Bachillerato había objetivos de materia: los objetivos en LOMLOE vuelven a ser de etapa, aunque también se pueden asociar objetivos a competencias, como conseguir un perfil de salida (en ESO, en Bachillerato es desarrollarlas) que supone ciertas competencias clave y competencias específicas de área/materia  
Los objetivos en LOMLOE en propia ley [Infantil art13](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a13), [Primaria art17](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a17), [ESO art23](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a23), [Bachillerato art33](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a33) aunque se repiten en reales decretos de currículo.  
En el currículo de Madrid algunas de estas asociaciones son **literales**: Madrid al cambiar el currículo estatal no usa saberes básicos sino contenidos y no usa situaciones de aprendizaje sino actividades.  

Tampoco hay que perder de vista que las competencias clave están definidas en normativa oficial (ver [competencias](/home/legislacion-educativa/competencias)) como  

> Competencias clave  
> A efectos de esta Recomendación, **se definen las competencias como una combinación de conocimientos, capacidades y actitudes**, en las que:  
> d) los **conocimientos se componen de hechos y cifras, conceptos, ideas y teorías que ya están establecidos y apoyan la comprensión de un área o tema concretos**;  


| **LOMCE** | **LOMLOE** |
|:-:|:-:|
| [Elementos currículo](https://boe.es/buscar/act.php?id=BOE-A-2006-7899&b=14&tn=1&p=20131210#a6)<br> a) Objetivos<br> b) Competencias <br>c) Contenidos <br>d) Metodología<br>e) Estándares <br>f) Criterios de evaluación<br> [Definiciones](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a2) | [Elementos currículo](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a6) <br>objetivos<br>competencias<br> contenidos<br> métodos pedagógicos<br> ~~estándares~~ <br> criterios de evaluación <br> [Definiciones ESO](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a2), [Definiciones Bachillerato](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a2) | 
|Objetivos de etapa [ESO](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37&b=13&tn=1&p=20150103#a11), [Bachillerato](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a25) | Objetivos de etapa [ESO](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a7), [Bachillerato](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a7) |
|[Competencias](/home/legislacion-educativa/competencias) | - Enseñanza básica (Primaria y ESO): [Perfil de salida](/home/legislacion-educativa/lomloe/curriculo-perfil-de-salida) con competencias clave y descriptores operativos <br> - Bachillerato: se definen para cada una de las competencias clave un conjunto de descriptores operativos, que dan continuidad, profundizan y amplían los niveles de desempeño previstos al final de la enseñanza básica |
|Criterios de evaluación especificados con estándares de aprendizaje| [Criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion) formados por [competencias específicas](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas) asociadas a descriptores operativos |
|Contenidos | [Saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos) |
|Actividades | [Situaciones de aprendizaje](/home/legislacion-educativa/lomloe/curriculo-situaciones-de-aprendizaje) |
|[Titulación ESO depende superar evaluación final](https://boe.es/buscar/act.php?id=BOE-A-2006-7899&b=47&tn=1&p=20131210#a31) <br> _Reválidas, no se implantaron. Se basaban en estándares_ | [Titulación ESO depende adquirir competencias y alcanzar objetivos etapa](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a31) | 
|[Titulación Bachillerato depende superar evaluación final](https://boe.es/buscar/act.php?id=BOE-A-2006-7899&b=57&tn=1&p=20131210#a37) <br> _Reválidas, no se implantaron. Se basaban en estándares_ | [Titulación Bachillerato depende superar todas las materias](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a37) <br> _Salvo una no superada en ciertas condiciones_ |

## [PERFIL DE SALIDA](/home/legislacion-educativa/lomloe/curriculo-perfil-de-salida)  

## DESCRIPTORES OPERATIVOS  
> En cuanto a su dimensión aplicada, el Perfil de Salida de la Enseñanza Básica se concreta en la formulación de un conjunto de **Descriptores operativos** de las Competencias Clave en la Enseñanza Básica (ver apartado 5.1.), para cada una de las 8 competencias clave. **Estos descriptores se definen como los desempeños propios de cada una de las competencias clave para el aprendizaje permanente, que incorporan los conocimientos, las destrezas y las actitudes que el alumnado debería adquirir y desarrollar al término de la enseñanza básica**. Para contextualizar estos descriptores operativos y hacer más funcional su aplicación en el diseño curricular y su uso en el plano pedagógico, se han adaptado los marcos europeos que desarrollan la mayor parte de estas competencias clave a la realidad del sistema educativo español.  

## [COMPETENCIAS ESPECÍFICAS](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas) 

## [SABERES BÁSICOS](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos) 

## [CRITERIOS DE EVALUACIÓN](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion)  

## [SITUACIONES DE APRENDIZAJE](/home/legislacion-educativa/lomloe/curriculo-situaciones-de-aprendizaje) 

Aquí una editorial intenta explicar los nuevos conceptos  
[Nuevo currículo, nuevos desafíos educativos. Grupo SM](https://www.grupo-sm.com/es/nuevo-curriculo-nuevos-desafios-educativos), 

> COMPONENTES DEL CURRÍCULO Y ENFOQUES CURRICULARES  
¿Quieres conocer qué son los componentes claves del currículo?   
En esta sección descubrirás el significado de estos elementos:  
    Contenidos básicos  
    Situaciones de aprendizaje  
    Aprendizajes curriculares  
    Perfil de salida del alumnado  
    Criterios de evaluación   

[Nuevo currículo. Grupo SM](https://thrivu.grupo-sm.com/s/nuevo-curriculo?language=es)  

[![](https://img.youtube.com/vi/DTgk4ORTX7Q/0.jpg)](https://www.youtube.com/watch?v=DTgk4ORTX7Q "Webinar gratuito: Las claves de la evaluación competencial en Código abierto LOMLOE. Editorial Casals")   
Webinar a cargo de Roberto Bravo, docente en activo  y coautor del proyecto de Lengua Castellana y Literatura de Editorial Casals.  

[![](https://img.youtube.com/vi/IlfjPS8GkN4/0.jpg)](https://www.youtube.com/watch?v=IlfjPS8GkN4 "EDUcharla 22: La evaluación educativa: retos y novedades. INTEF")   
EDUcharla dedicada a la evaluación educativa y los cambios que se introducen en el nuevo marco legislativo.  

[![](https://img.youtube.com/vi/QaIrs2oN0mU/0.jpg)](https://www.youtube.com/watch?v=QaIrs2oN0mU "EDUcharla 23: La evaluación educativa II: formación e inclusión. INTEF")   
EDUcharla dedicada a la evaluación educativa y los cambios que se introducen en el nuevo marco legislativo.  

[![](https://img.youtube.com/vi/JUuXwbXK5fg/0.jpg)](https://www.youtube.com/watch?v=JUuXwbXK5fg "El currículum LOMLOE. Claves teóricas y experiencias de aula', Florencio Luengo Horcajo. Universidad de la Rioja")   

[![](https://img.youtube.com/vi/oWKUausjhHQ/0.jpg)](https://www.youtube.com/watch?v=oWKUausjhHQ "La evaluación en la LOMLOE #DeOtraManera. Edebé")   

[![](https://img.youtube.com/vi/aDOd1g4RTzc/0.jpg)](https://www.youtube.com/watch?v=aDOd1g4RTzc "La nueva estructura curricular: tu programación LOMLOE. @pablofcayqca")   


Cito aquí esto que combina cosas (y cito en más sitios)  
"Prepárate para la LOMLOE: cómo evaluar y planificar con Additio", un vídeo de 15 septiembre 2022  
[![](https://img.youtube.com/vi/rv-TZXcEYX8/0.jpg)](https://www.youtube.com/watch?v=rv-TZXcEYX8 "Prepárate para la LOMLOE: cómo evaluar y planificar con Additio")  
Webinar de 51 minutos, usa un producto concreto pero muestra ideas:   
- la relación entre [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion) y [competencias específicas](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas) (8:44)  
- la asociación entre actividades (que serían [situaciones de aprendizaje](/home/legislacion-educativa/lomloe/curriculo-situaciones-de-aprendizaje)) a [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion) (12:00)  
- la asociación entre actividades a [competencias específicas](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas) (ya que los [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion) están asociados a [competencias específicas](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas)) (17:25)  
- la asociación entre actividades a competencias claves (ya que las [competencias específicas](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas) están asociadas a las clave) (24:23)  
- asigna pesos en porcentaje a [competencias específicas](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas) (31:33)  
- asigna ponderación a criterios de evaluación (34:58) 
- Cita asociación entre unidades didácticas y situaciones de aprendizaje (35:49, 36:10) que de momento en app llaman unidades y citan que dudan si van a cambiar nombre en app  
- Cita que se pueden vincular contenidos o [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos)  en unidades (37:00), pero parece informativo, sin pesos  
- Cita que las [situaciones de aprendizaje](/home/legislacion-educativa/lomloe/curriculo-situaciones-de-aprendizaje) están compuestas de tareas de aprendizaje   

Comento ideas aquí  
[twitter FiQuiPedia/status/1571878668808470530](https://twitter.com/FiQuiPedia/status/1571878668808470530)  
Ejemplo FQ4ESO  
Criterio evaluación: 1.2 Resolver los problemas fisicoquímicos planteados mediante las leyes y teorías científicas adecuadas, razonando los procedimientos utilizados para encontrar las soluciones y expresando los resultados con corrección y precisión.  
Si asocio un peso a ese criterio, pero no sé de qué contenido (saber básico) estoy hablando, puede ser competente en energía, pero no en resto de física ni en química. Sin embargo sí cumple criterio y estaría evaluado positivamente en competencia específica y clave asociadas.  
Por ello en mi propuesta de programación pongo pesos a unidades didácticas centradas en bloques de saberes básicos, que es lo que temporalizo, y las actividades intentan evaluar ser competente en cada contenido, porque no veo evaluar ser "competente en vacío \[de contenido]"  
Hablo de Física y Química, donde las actividades son básicamente resolución de problemas que considero competenciales, y hay contenidos diferenciados, no solo entre Física y Química sino dentro de cada una. Quizá en otras materias es distinto.  

A partir de ahí surgen comentarios sobre Andalucía (respaldados por un documento oficial de instrucciones 2022-2023) que comento en [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion) y [competencias específicas](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas), y que también aplica a [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos)  


[![](https://img.youtube.com/vi/JLa0I4ora4M/0.jpg)](https://www.youtube.com/watch?v=JLa0I4ora4M "DESARROLLO DEL MARCO AUTONÓMICO DEL CURRÍCULUM. ASESORAMIENTO A LAS DIRECCIONES DE LOS CENTROS. Ciudad del conocimiento (dos hermanas). 3 octubre 2022. Junta de Andalucía. Primera parte. ")  3h 54min   

[![](https://img.youtube.com/vi/hcv3VbycKFg/0.jpg)](https://www.youtube.com/watch?v=hcv3VbycKFg "DESARROLLO DEL MARCO AUTONÓMICO DEL CURRÍCULUM. ASESORAMIENTO A LAS DIRECCIONES DE LOS CENTROS. Ciudad del conocimiento (dos hermanas). 3 octubre 2022. Junta de Andalucía. Segunda parte")  2h 50min   
    
[NOVEDADES DE LA EVALUACIÓN LOMLOE (Andalucía) - el loco de la mochila Blog de Juan Manuel López Esparrell](https://blogsaverroes.juntadeandalucia.es/ellocodelamochila/lomloe/)  
En formato hilo (concretado en Andalucía)  
[twitter LopezEsparrell/status/1579759344643895299](https://twitter.com/LopezEsparrell/status/1579759344643895299)  
La #LOMLOE y su desarrollo  ha traído algunas novedades al proceso de #evaluación. ¿Vemos algunas? ¿Me acompañas en este hilo?   
...Fuentes: Instrucciones 1/2022; Decreto 217/2022  
![](https://pbs.twimg.com/media/Fexuv-iXgAAM4ab?format=jpg)  

[twitter pbeltranp/status/1583743846462083072](https://twitter.com/pbeltranp/status/1583743846462083072)  
Interesantísimas las sesiones sobre LOMLOE en #Aragón desde el @cpjuandelanuza por @DavidGalindo75 y @SergioMJGR. Calendario de implantación, normativa y arquitectura curricular autonómica, PCE y programaciones.  
Infantil y Primaria:  
[Sesión informativa LOMLOE youtube](https://www.youtube.com/watch?v=E8tkkU6ATQ0)  

[Los nuevos elementos curriculares de la LOMLOE - raulsolbes.com](https://raulsolbes.com/2021/11/10/los-nuevos-elementos-curriculares-de-la-lomloe/)  
![](https://raulsolbes.com/wp-content/uploads/2021/11/Elementos_curriculo_LOMLOE-1024x575.png)  

[twitter PaulaBlooom/status/1555934642993242115](https://twitter.com/PaulaBlooom/status/1555934642993242115)  
Con la LOMLOE en la mano y los apuntes de Raúl Solbes que he referido en este tuite hice una pequeña presentación para explicar los elementos curriculares a mis compañeras. Con gusto comparto algunas diapositivas para que quien quiera pueda apoyarse en ellas (sigue)  

## Ideas combinando todo

Se podrían asociar "saberes básicos" a "contenidos" (así aparece en uno de los documentos citados de marzo 2021, y en la definición en el proyecto de RD indica "constituyen los contenidos  propios  de  una  materia  o  ámbito"), pero para los saberes básicos se asocia tanto a competencias que más que contenidos son "aquello en lo que se apoye las adquisición y evaluación competencias".   

> ...cuyo aprendizaje y evaluación se realizará de manera competencial.  

> ...cuyo aprendizaje es necesario para la adquisición de las competencias específicas.  

Artículo 6.3 (que es normativa básica según dispoción final quinta) cita explícitamente "contenidos" y luego en RD se citan en artículo 11 Currículo, pero a la hora de concretarlos pasan a ser saberes básicos, no contenidos, en anexo II "los contenidos, enunciados en forma de saberes básicos.  

Los criterios de evaluación, antes asociados a contenidos, en borrador LOMLOE están asociados a competencias específicas, cada competencia específica está asocida a unos descriptores del perfil de salida, que concreta ciertas competencias clave.  


El párrafo  
> El gran reto está en conseguir que estos criterios de evaluación, aunque se formulen para cada materia y 
ciclo  o  curso,  vayan  indisolublemente  unidos  a  los  descriptores  del  Perfil  de  salida,  a  través  de  las 
competencias  específicas,  de  tal  manera  que  **no  se  pueda  producir  una  evaluación  de  la  materia independiente de las competencias clave.**  

Me sugiere que se quiere centrar tanto en evaluar competencias, **no  se  pueda  producir  una  evaluación  de  la  materia independiente de las competencias clave.** que no se permite evaluar conocimiento si no se considera competencial. Considero que tener conocimiento concreto de contenidos y utilizarlo es ser competente, pero si cierto conocimiento no está plasmado para poder considerarse "competencias", no formará parte de la evaluación.  

La primera impresión es que poniendo saberes básicos, contenidos, poco concretos y difusos, eso lleva al absurdo de "poder ser competente en vacío" sin concocimientos.   

Resumido aquí  
[twitter jago2019jose/status/1448047187661836292](https://twitter.com/jago2019jose/status/1448047187661836292)  
Bueno, por fin una ley educativa va a permitir que los defensores del enfoque competencial y las nuevas pedagogías dejen de incumplir sistemáticamente la legislación vigente (eso que siempre decían que hacíamos los otros) ¡Felicidades! 😉 Ya podréis enseñar en el vacío legalmente  

Cito esto citado por jago2019jose de ["El menosprecio del conocimiento. Rosa Cañadell, Albert Corominas, Nico Hirtt"](https://icariaeditorial.com/nico-hirtt/4674-el-menosprecio-del-conocimiento.html)  
[Indice e introducción](https://icariaeditorial.com/index.php?controller=attachment&id_attachment=52)  

>Por su actualidad porque, aunque su origen se remonte a los años noventa, últimamente se está difundiendo de forma cada  vez más intensa y extensa la concepción de la enseñanza como herramienta  para  la  creación  y  renovación  de  lo  que  algunos economistas denominan «capital humano». De la mano de esta concepción  de  la  enseñanza,  cada  vez  van  abriéndose  camino  de  forma  más  general  teorías  supuestamente  pedagógicas  que menosprecian la adquisición de conocimientos y ensalzan la adquisición de competencias, preferiblemente transversales. Como si se pudiera «comunicar eficazmente» lo que se desconoce, o «trabajar en grupo» sin conocer el objeto de trabajo. El fenómeno ha alcanzado ya a la enseñanza universitaria, y cuenta con defensores no solamente entre dirigentes empresariales, sino también entre (ir)responsables universitarios, amén de múltiples «expertos». 

>Si un conocimiento no tiene lugar en la escuela mas que en la medida en que pueda ser utilizado en el desarrollo de una competencia, es decir, la realización de una tarea, eso excluye de entrada toda una categoría de conocimientos que, por razones diversas, no pueden ser objeto de una tarea en el conexto escolar. ... con mucha frecuencia, las tareas que deben realizar los alumnos en las disciplinas con fuerte contenido cognitivo quedan totalmente desconectadas del propio contenido del curso. Los alumnos pasan horas escribiendo un artículo acerca de ..., realizando un póster informativo sobre ..., presentando en forma de página web ..., preparar una conferencia con diapositivas... Algunos enseñantes tienen la impresión de que desagradable de que se trata de una pérdida de tiempo. Se equivocan. No comprenden que, simplemente, el objetivo no es que los alumnos dominen mejor la historia o la física, sino justamente que aprendan a hacer pósteres, páginas web y presentaciones "powerpoint", incluso -¡sobre todo!- sobre materias que no dominan y no dominarán jamás. 


Quizá la "concreción" de las **situaciones de aprendizaje** aporte algo de luz, pero me parece que alejarse tanto de los contenidos no tiene sentido, ya que no hay conocimiento sin contenidos.  

Se criticó la LOMCE por los estándares, que bajaban era demasiado enciclopédica con mucho detalle, generando algunos estándares absurdamente concretos/específicos, pero la concreción no la veo mal siempre que no se baje a detalles absurdos. Veo peor no bajar a ningún tipo de detalle que pueda generar una evaluación absurdamente abstracta/inespecífica pero que cumpla la letra de lo que dice el currículo. Que fuera del currículo se concrete lo muy abstracto que diga un currículo de mínimos puede llevar a un exceso de disparidad entre comunidades, centros y docentes. Eso puede suponer una "segregación de conocimiento": algunos alumnos tendrán más acceso a conocimiento que otros según comunidad, centro y docente. Siendo tan vago permite que un docente que viva en los mundos de Yupi y reniege de contenidos pueda no concretar nada y evaluar sin enseñar nada que no sean "competencias en vacío".     

- El planteamiento de la LOMCE era partir de reválidas/evaluaciones finales, a partir de ahí que es muy concreto se va a lo que se podía preguntar y se plasmaba en estándares, y criterios y contenidos que dieran cobertura a los estándares
- El planteamiento de la LOMLOE es partir de competencias, a partir de ahí que es muy genérico se habla de evaluarlas como la capacidad que el alumnado debe adquirir sobre un contenido en un contexto/aplicación/uso. 

Creo que se podría plantear que en Física y Química se es competente si se tiene capacidad para resolver problemas utilizando contenidos, y eso enlazaría con la competencia específica 3 del [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica), pero veo tan rebuscado el lenguaje que es complicado de ver.

## Mi resumen

(No pongo fecha, voy actualizando esto, puedes ver en gitlab la última fecha de actualización de esta pagína)
A ver si ejemplos reales de situaciones de aprendizaje y el "perfil de salida de 2º Bachillerato" aportan luz. Recuerdo que los materiales en mi web los planteé inicialmente "de arriba hacia abajo", partiendo del "punto de salida" que es la selectividad, claro y con contenidos y evaluación concreta y definida, y luego pensando como poco a poco se van introduciendo conceptos para llegar a ese punto. El "enganche" que se haga de lo que es "humo" en ESO a algo muy concreto como es la selectividad puede aclarar.   

Entrecomillo "perfil de salida" porque no hay como tal en 2º Bachillerato, es un concepto solo de educación básica. Tras ver el borrador a finales de octubre de currículo de Bachillerato, los saberes básicos en 2º Bachillerato, al menos en Física y en Química si tienen un nivel de concreción similar a LOE, por lo que igual el choque se produce en el salto entre la indefinición en 4º ESO y 1º Bachillerato.

Quizá la concreción de las situaciones de aprendizaje sea un truco final para "obligar" a seguir metodologías concretas / no permitir ciertas metodologías.

## Ámbitos

Además del programa de diversificación curricular que se comenta aparte, para ESO se indica 

> Artículo 8. Organización de los tres primeros cursos.
> 5. Los centros podrán establecer **agrupaciones en ámbitos de todas las materias de lostres primeros cursos de la etapa** en el marco de lo establecido a este respecto por sus respectivas Administraciones educativas.  


## Programa de diversificación curricular.

En LOMLOE desaparece PMAR y vuelve Diversificación curricular  
Aparece en el proyecto de real decreto de secundaria   

> Artículo 24. Programas de diversificación curricular  
> 2. La implantación de estos programas comportará la aplicación de una metodología
específica a través de una **organización del currículo en ámbitos de conocimiento**,
actividades prácticas y, en su caso, materias, diferente a la establecida con carácter
general, para alcanzar los objetivos de la etapa y las competencias establecidas en el
Perfil de salida.   

Además de los ámbitos lingüístico y social y cientítico-tecnológico (donde está Física y Química) como en LOE, surge como novedad un tercer ámbito práctico  

> 8. Las Administraciones educativas establecerán el currículo de estos programas en el
que se incluirán dos ámbitos específicos, uno de ellos con elementos de carácter
lingüístico y social, y otro con elementos de carácter científico-tecnológico y, al menos,
tres materias de las establecidas para la etapa no contempladas en los ámbitos
anteriores, que el alumnado cursará preferentemente en un grupo ordinario. **Se podrá establecer además un ámbito de carácter práctico**.   
> 9. El ámbito lingüístico y social incluirá, al menos, los aspectos básicos del currículo
correspondientes a las materias de Geografía e Historia, Lengua castellana y Literatura
y, si la hubiere, Lengua cooficial y Literatura. El ámbito científico-tecnológico incluirá, al
menos, los correspondientes a las materias de Matemáticas, Biología y Geología, **Física y Química**, y, en su caso, Tecnología y Digitalización. Cuando la Lengua extranjera no
se incluya en el ámbito lingüístico y social deberá cursarse como una de las tres materias
establecidas en el párrafo anterior. **En el caso de incorporarse un ámbito de carácter práctico, este podrá incluir los aspectos básicos del currículo correspondiente a Tecnología y Digitalización.**  

La mención a "especificar ... saberes básicos" parece que permite enlazar ámbitos con adaptaciones curriculares. Se supone que en el perfil de salida ya están "indicados" los saberes básicos mínimos, y "especificar" enlaza con concreción.   

> 10. En el marco de lo establecido por las Administraciones educativas, los centros
podrán organizar los programas de diversificación curricular teniendo en cuenta las
necesidades de su alumnado. Estos deberán **especificar la metodología, saberes básicos y criterios de evaluación** que garanticen el logro de las competencias establecidas en el Perfil de salida.


## La concreción del currículo

Una vez vista la poca concreción en el proyecto de Real Decreto de enseñanzas mínimas, queda ver cómo se concreta: ya comentado que dependerá de CCAA, centros y docentes, y puede generar desigualdad.   

A falta de ver la concreción de CCAA y ejemplos concretos de situaciones de aprendizaje, sí que el proyecto de Real Decreto lo deja muy abierto y en manos de los centros

[DOCUMENTO I: Proyecto de Real Decreto de Ordenación y enseñanzas mínimas de la ESO (PDF)](https://www.magisnet.com/wp-content/uploads/2021/10/21.10.06_RD_ESO.pdf)  

>Artículo 26. Autonomía de los centros.
>...
>2. Corresponde a las Administraciones educativas contribuir al **desarrollo y adaptación del currículo por parte de los centros**, favoreciendo la elaboración de modelos abiertos 
de  programación  docente  y  de  materiales  didácticos  que  atiendan  a  las  distintas 
necesidades  de  los  alumnos  y  alumnas  y  del  profesorado,  adecuándolo  así  a  sus 
diferentes realidades educativas.  
>...
>4. **Los centros fijarán la concreción de los currículos establecidos por la Administración educativa** y  la  incorporarán  a  su  proyecto educativo,  que  impulsará  y desarrollará  los 
principios, objetivos y metodología propios de un aprendizaje competencial orientado al 
ejercicio de una ciudadanía activa.   
>...
>6.  En  el  ejercicio  de  su  autonomía,  los  centros  podrán  adoptar  experimentaciones, 
innovaciones  pedagógicas,  programas  educativos,  planes  de  trabajo,  formas  de 
organización, normas de convivencia o ampliación del calendario escolar o del horario 
lectivo  de  materias  o  ámbitos,  en  los  términos  que  establezcan  las  Administraciones 
educativas y dentro de las posibilidades que permita la normativa aplicable, incluida la 
laboral, sin que, en ningún caso, suponga discriminación de ningún tipo, ni se impongan 
aportaciones a las familias ni exigencias para las Administraciones educativas.  

Al punto 26.6 le veo implicaciones:   
- Da carta blanca a "experimentaciones", y los experimentos (que incluso pueden ser de concreción de currículo) se deben hacer con control y con seguimiento real, sin ellos creo que puede dar alas a gurús y vende humos que innovan por innovar, que estarán perfectamente camuflados en la nube de humo que genera el galimatías de términos nuevos.   
- Teóricamente impide el bilingüismo implementado como grupos segregados entre sección y programa.   


En FQ se ve poca concreción, y por comentarios en otras materias no es igual   
[twitter SegundaNave/status/1447637198690529288](https://twitter.com/SegundaNave/status/1447637198690529288)  
Los 3 grupos de saberes básicos se estructuran en 79 "asuntos" que abordar. A 35 horas anuales me da +-2 saberes y cuarto por hora, a un alumnado con una madurez de 3ESO en el mejor de los casos. La mierda que fuman los que hayan redactado ese despropósito pega bien.  

En BG 
[twitter profesmadeinuk/status/1450512527507562504](https://twitter.com/profesmadeinuk/status/1450512527507562504)  
Competencias específicas del nuevo currículum de Biología. Resolver problemas es una competencia muy específica. La vida es un problema a resolver, desde luego. Menos mal que un geólogo avispado ha podido colar la última competencia sin que nadie se dé cuenta.  
![](https://pbs.twimg.com/media/FCFBdJvWEAIrvUt?format=jpg)  

Hilo comentando Matemáticas (no lo incluyo entero, pongo enlace y algunas ideas finales)  
[twitter pbeltranp/status/1452206768075988994](https://twitter.com/pbeltranp/status/1452206768075988994)  
¿Hacemos unos comentarios sobre el proyecto de currículo de Primaria de Matemáticas?  
...
Hay alguna cosilla más, pero creo que ya vale por ahora.  Lo veo mejor que el LOMCE. Me gustaría:  
- Más coherencia en el desarrollo entre materias.   
- Menos instrumentalización y menos vida cotidiana (repito: no quiero decir formalizar).  
- Énfasis en aprender a través de la RP.  
Habrá que esperar a los desarrollos autonómicos. Hay mucho margen para hacer cosas majas. Eso sí, el currículo no va a cambiar nada si no va acompañado de recursos, planes de desarrollo profesional, etc.  
En  particular, el margen que tienen los centros para el desarrollo curricular requeriría de figuras estructurales muy especializadas por materias: los «curriculum maker».  


## Cambios en la docencia al trabajar y evaluar por competencias  
Ver [competencias](/home/legislacion-educativa/competencias)  
La idea de centrar la educación en competencias en general en punto separado, aquí solo el tema del cambio que supone usarlas   

Tema de cambiar metodología para evaluar por competencias no es solo ponerlo en papel en la ley, supone cambiar la manera de trabajar y formar a profesores, además de dejarles más tiempo para preparar las clases, lo que enlaza con bajar ratios y reducir carga horaria.  

Conozco muchos profesores que para hacer programaciones solo saben copiar de BOE / BOCM / Editorial (de hecho con lomce un jefe de departamento, ya jubilado, me reconoció que fijaba libro para que la editorial le diese las programaciones hechas).  
Teniendo que hacer cada centro la concreción real, si no se hace y se copia y pega (puede ser estadísticamente significativo) pueden quedar unas programaciones tan poco concretas que no se diferencien de adaptaciones significativas para primaria.  

Además de la formación de profesores, está la formación de inspectores, que son los que _se supone_ que revisan las programaciones.  


Ideas reflejadas aquí 
[twitter aquimonroy/status/1451465864797167618](https://twitter.com/aquimonroy/status/1451465864797167618)  
Bueno, vamos a hablar un poquito de la propuesta curricular de la LOMLOE, aprovechando este artículo.   

21 octubre 2021
[Los nuevos currículos de Secundaria, una "revolución" que deja elegir a los docentes el contenido de las asignaturas](https://www.eldiario.es/sociedad/nuevos-curriculos-secundaria-revolucion-deja-elegir-docentes-contenido-asignaturas_1_8399075.html)  


Va hilo larguito  
...
Hay un profe en el artículo que se pregunta: "¿Qué tipo de exámenes hay que poner para comprobar estos criterios de evaluación?" "¿Qué tipo de libros de texto y materiales hay que diseñar para trabajar estas competencias?".
Esto le va a pasar a la MAYORÍA del profesorado.  
Esta transformación requiere un proceso personal de aprendizaje por parte del profesorado. No es como si la ley pone o quita a Quevedo. Es un cambio de concepción. Si no sabes trabajar así, no puedes hacerlo de un día para otro. NO SE PUEDE  
...


[twitter rcabreratic/status/1451577357425315841](https://twitter.com/rcabreratic/status/1451577357425315841)  
Evaluar por competencias a 150 alumnos, utilizando instrumentos variados requiere más de una hora al día. ¿Y eso no es una limitación? Crear un buen proyecto implica trabajar en casa casi tantas horas como en clase, ¿los tiene preparados la administración? En fin…  


[twitter pablofcayqca/status/1451628978972860420](https://twitter.com/pablofcayqca/status/1451628978972860420)  
Un análisis magnífico. Y yo añadiría, dentro de la formación del profesorado, el tiempo dedicado a formarse en resolución de problemas muy alejados de la realidad educativa para superar el procedimiento selectivo. ¿Y si los años de dedicación para preparar problemas (hablo de … mi especialidad, Física y Química), se dedicasen a formarse realmente en Didáctica?, ¿con qué herramientas contaríamos en clase para proponer situaciones de aprendizaje, quienes han/hemos pasado 2-3 horas diarias preparando problemas para un examen de oposiciones durante años?  
A ver si de una vez por todas, el @educaciongob y la @EducaAnd apuestan por un sistema de selección más realista y cercano al aula, lo que ganaríamos todos.  


[twitter SeattlePanda/status/1451585993220530201](https://twitter.com/SeattlePanda/status/1451585993220530201)  
"¿Qué tipo de exámenes hay que poner para comprobar estos criterios de evaluación?", se pregunta Sande. "¿Qué tipo de libros de texto y materiales hay que diseñar para trabajar estas competencias?". No lo ha entendido  

[twitter sande_jose/status/1451812576778760195](https://twitter.com/sande_jose/status/1451812576778760195)  
No solo lo he entendido, sino que la semana pasada elaboré y publiqué  un modelo de situación de aprendizaje lomloe "El mundial de globos de Ibai y Piqué", basada en el borrador.  

[Modelo Situación de aprendizaje LOMLOE «Economía y Emprendimiento» - josedande.com](https://josesande.com/2021/10/17/modelo-situacion-de-aprendizaje-lomloe-economia-y-emprendimiento/)  


Lo que hay que entender es la situación real del sistema educativo y que un cambio así no se puede hacer de forma improvisada y con prisas. Estamos casi en noviembre y no hay ni borrador de bachillerato. Las CCAA sacarán en junio/ julio sus currículos...  

[twitter aquimonroy/status/1451824610719174659](https://twitter.com/aquimonroy/status/1451824610719174659)  
Exacto. Gracias, Jose, eso es lo que yo quería decir. Y que esa pregunta se repetirá por lo largo y ancho de los institutos. La respuesta debería llegar antes, y no va a llegar, no hay tiempo.  

[twitter PascualGil1/status/1455183834769657871](https://twitter.com/PascualGil1/status/1455183834769657871)  
"Los vientos recientes que soplan en la enseñanza profundizan en esa anegación de los conocimientos en la ciénaga de las competencias, a la cual se arrojan ahora, además, grandes dosis de ideología destinada a crear ciudadanos felices a base de inculcarles ideas virtuosas."  

[twitter marcos_oaoa/status/1591608969918808064](https://twitter.com/marcos_oaoa/status/1591608969918808064)  
LOMLOE ¿Sí o No?  
(enfoque mates, eso sí)  
P.D.:Con este hilo expreso mi opinión y respeto cualquier otra que sea diferente  
Nota:Igual expreso cosas que en un café cara a cara sonarían o las diría diferente,ya que se por aquí se pierden cosas,pero allá voy  
1. He formado parte de la Comisión Autonómica de Canarias de Mates de Primaria.  
Lo he hecho porque me lo han pedido, porque he querido y con la máxima ilusión, honestidad y respeto.  
He aportado mi visión desde mi experiencia y he intentando aportar mi granito de arena, nada más/2  
2. El Ministerio de Educación es quien elabora la estructura general y los elementos curriculares. Las comunidades perfilamos y adaptamos el porcentaje que se nos permite. Hay cosas en la que tenemos margen (saberes básicos, por ejemplo) y otras que no podemos tocar/3  
3. El hecho de haber colaborado no me hace ser ni mejor ni peor docente. Tampoco me empodera ni hace que mi opinión valga más que la de otro docente. Me ha enriquecido y ha sido una experiencia personal y profesional positiva, nada más.  
Ahora vamos con las intenciones .../4  
4. Como todo en la vida hay luces y sombras: empiezo con las LUCES:  
a)Se añaden aspectos del pensamiento computacional, uso de la calculadora.Lo primero es lo más novedoso,lo segundo es increíble que hoy en día haya quien vea en la calculadora una enemiga.  
Pero ahí están/5  
b) Se crean competencias """""ESPECÍFICAS"""". No me darían las comillas. Es una muy buena idea, pero no estoy de acuerdo de cómo se han redactado y elegido (luego en sombras lo cuento). Sin embargo es un acierto que aparezcan. Creo que es bueno para la diáctica de las mates/6  
c) Se respira,sobre todo en los saberes básicos, unas matemáticas más comprensivas, donde se dota de significado a las cosas, evitando mecanismos y trucos.
Se detallan muchas cosas interesantes desde el punto de vista didáctico (al menos en el de Canarias)/7  
d) Se explicita la necesidad de hacer un esfuerzo por partr de toda la comunidad de transformar la mirada que tiene gran parte de la sociedad sobre la animadversión hacia la materia. El fallo como aprendizaje, el no pensar que las mates es para unos pocos, dar una visión más +/8  
SOMBRAS:
a) Las compentencias no son ESPECÍFICASSon muy ambiguas y abiertas.Creo que el profesorado no ve en ella una herramienta facilitadora.Han confundido flexibilidad con ambigüedad. Tampoco me parecen el lugar correcto que 2/8 (25%) sea sobre emociones e igualdad de género/9  
b) El hecho de no relacionar explícitamente los saberes básicos con criterios de evaluación hace que casi que cada centro pueda interpretarlos a su criterio (peligroso y complicado).  
Creo que han querido valorar procesos, pero no creo que el camino sea ese desvinculamiento/10  
c) La evaluación se hace infumable:inoperativa,poco funcional,aumenta la burocracia absurda,utópica y ridiculamente engorrosa
¿No es +fácil relacionar procesos cognitivos matemáticos con determinados saberes y ambos materializados en una verdadera Competenc ESPECÍFICA?Digo yo/11  
REFLEXIONES FINALES:
1. Es interesante que se siga apostando por una matemática de calidad donde la didáctica y la construcción del razonamiento lógico-matemático esté por encima de cualquier ley  
2. Los docentes nos formamos y nunca será suficiente Siempre queremos aprender+  
/12  
4. La sociedad siempre cambiará mucho más rapida que la educación, es normal.  
¿Pensamiento computacional?  
!Pues qué bueno!Ya hay un montón de gente debatiendo, preguntando, buscando cosas. Veremos materializado eso dentro de 20 años o más en las aulas, es así !pero ya empezó!  
/14  
5.Los curriculum deberían ser documentos que faciliten la labor a los docentes.Deberían poder ponerse enlaces a bibliografías,ejemplos,videos explicativos.En el año 2022 es  lamentable que todavía se impriman en papel y no haya un curriculum digitalizado con recursos enlazados/15  
6. Cada docente debe recordar que la prioridad número 1 es su alumnado y que no hay mejor regalo para la educación que un docente que quiere seguir aprendiendo y formándose para ofrecer una mejor versión de sí mismo a su alumnado.
Formarse. como hacemos, es el mejor regalo/16  
17. Es una tristeza que la formación inicial en la Universidad sea tan deficitaria en didáctica de las matemáticas y se sigan tirando cientos de horas a la basura. Es terrible que nadie de un golpe en la mesa para actualizar los planes de estudio !años a la basura!
/17  
REFLEXIÓN FINAL  
¿Saben que les digo?  
Que al alumnado les da igual las siglas.A ellos les importa un pimiento que se llame LOMLOE,LGES, KH7.  
A ellos lo que les importa es que tú domines la materia,que le transmitas pasión por tu asignatura. Así que eso siempre sera lo primero.
FIN  

## Adaptaciones curriculares   

Enlazando con la concreción, surge la idea de cómo se gestionarán ahora las adaptaciones curriculares.   
Hasta ahora se solía hablar de "un desfase curricular de x años", pero si es tan poco concreto, no lo veo.   
La poca concreción del currículo oficial, que es lo que se usará si centro y docente no concreta más, se queda en algo tan difuso y general que puede considerarse una adaptación curricular.  
Antes surgía la duda y no era nada claro/se gestionaba de manera diferente como tratar las calificaciones de las adaptaciones significativas (las notas con asterisco): ahora se pueden pensar que deben evaluar competencias de acuerdo a las capacidades de cada alumno y que con un currículo mínimo tan poco concretado siempre implicarán titulación.   

[twitter jago2019jose/status/1449087747063418889](https://twitter.com/jago2019jose/status/1449087747063418889)   
A vuelapluma, de los 23 "saberes básicos" de FyQ en 4ºESO, 21 son tratados hoy con total normalidad en 2ºESO. Al eludir el nivel de profundidad de los contenidos que debemos enseñar, la LOMLOE abre la puerta a q el nuevo 4ºESO sea una "adaptación significativa" d lo que era 2ºESO  

25 octubre 2021  
[Los alumnos con necesidades especiales podrán tener el título de la ESO aunque no superen los mínimos exigidos en la etapa - elmundo.es](https://www.elmundo.es/espana/2021/10/25/61729e93fdddff95a08b462e.html)  
El referente para que puedan graduarse será su adaptación curricular y no los objetivos generales establecidos para el conjunto el alumnado  

A esto le veo una derivada peligrosa, ya que dejar que el centro y docente concrete lo que supone titular pone en manos de centros y docentes privados con/si concierto la decisión de titular, y para esos centros los alumnos son hijos de sus clientes.  
Lleva a la idea que cité en [ESO parece pero ESO no es](https://algoquedaquedecir.blogspot.com/2018/04/eso-parece-pero-eso-no-es.html)  

> se abre la veda a que el título de ESO quede en manos de lo que "considere" un centro privado (con concierto o sin él), por lo que estadísticamente se está abriendo la posibilidad a que la gente pueda ir a "comprar" a su hijo el título de ESO.  

Igual alguien dice que no, que eso solo es para ACNEEs ... ejem, los centros privados ya han demostrado que convierten a alumnos en ACNEEs si les interesa.  Ver [Orientación concertada](https://algoquedaquedecir.blogspot.com/2018/04/orientacion-concertada.html)  

Tal y como lo están planteando, deja de ser un título de ESO y solo garantiza lo que en otro tiempo era un "certificado de escolaridad". Certifica que se ha estado años escolarizado en la educación obligatoria, no conocimientos   

## Enlaces y artículos   

Enlazo algunos artículos; algunos revisan o se centran en materias concretas pero plantean ideas generales:  
*   [Marta Ferrero: "En educación es mejor estar a la penúltima que a la última" - elmundo.es](https://amp.elmundo.es/espana/2021/10/01/6157329521efa0a7208b463a.html)  

>La Lomloe da mucha importancia a las nuevas metodologías...  
En educación es mejor estar a la penúltima que a la última. Si un profesor va a adoptar una nueva metodología, es mejor ponerla en marcha con todas las prevenciones y hacer primero un pilotaje. Se puede perder tiempo y dinero, y perjudicar a los estudiantes. Deberíamos participar en un debate informado sobre estos asuntos, pero nunca llegamos a comprobar si las leyes funcionan o no porque cada partido las cambia cuando llega al Gobierno.  

*  [La gran dificultad de evaluar en la LOMLOE - josesande.com](https://josesande.com/2021/10/13/la-gran-dificultad-de-evaluar-en-la-lomloe/)  
*  [Valoración del borrador de currículo de la asignatura "Tecnología y Digitalización" (1º a 3ºESO) presentado por el MEyFP a las comunidades ](https://aptear.blogspot.com/2021/10/valoracion-del-borrador-de-curriculo-de.html)  
*  [Los profesores, contra la ESO con perspectiva de género y sin repetidores de Alegría: "Es cortoplacista" - elespanol.com](https://www.elespanol.com/reportajes/20211014/profesores-perspectiva-genero-sin-repetidores-alegria-cortoplacista/618688919_0.html)  

>Para Borja Delgado, si bien es cierto que hay veces que los contenidos son muy extensos, hay un error de enfoque, ya que la reducción “así porque sí” no es positiva. En su opinión, lo que debería abordarse es una reorganización de los contenidos para evitar la repetición en cursos sucesivos, algo muy habitual: “En Biología y Geología estamos viendo la estructura de las células en los tres cursos de la ESO. Con verlo una vez y si conseguimos que el aprendizaje sea significativo y se mantenga…”, cuenta Borja.  

*  [Educación no detalla los hechos de Historia que deben estudiar los alumnos de ESO en toda España - elmundo.es](https://amp.elmundo.es/espana/2021/10/13/6167328221efa0950e8b45e5.html)  

*  Hilo comentando estructura general y caso de Física y Química  
[twitter pablofcayqca/status/1449055581994692608](https://twitter.com/pablofcayqca/status/1449055581994692608)  
Qué os parece si tiramos un hilo para ver qué cambios nos esperan con la llegada del nuevo Real Decreto en la ESO (según lo que aparece en el borrador). Aquí va una de #pildorillasdeprogramación  

*  ["La LOMLOE tiene muy preocupado a buena parte del profesorado" Juan Quílez Pardo Dr. Ciencias Químicas. Catedrático de Física y Química. Premio Tarea Educativa de la RSEQ](https://www.levante-emv.com/lectores/2022/02/17/lomloe-preocupado-buena-parte-profesorado-62824671.html)  

## Enlaces asociados a modificaciones currículo, competencias, metodologías 

De momento recopilatorio ideas, quizá reorganice o distribuya en otras páginas  

Ver [competencias](/home/legislacion-educativa/competencias)

## Implantación del currículo
Se pueden comentar muchas cosas, no solo que en algunos casos el curso 2022-2023 empezase sin currículo oficial, sino que en muchos casos se publicó con tan poca antelación que no ha habido tiempo de adaptarse

Durante la implantación en 2022 surgen ideas pro-LOMLOE y anti-LOMLOE, en muchos casos los segundos se interpretan como pro-LOMCE o pro-EGB o anti-inclusión. Creo que un aspecto innegable es las prisas sin recursos para un cambio real

[Balance desencantado de 2022](https://educarentiemposinciertos.blogspot.com/2022/12/balance-desencantado-de-2022.html)  

> Paradójicamente, al no tener todavía la programación según la reforma, con sus situaciones de aprendizaje de las que emanarán las competencias específicas, muchos docentes han recurrido, más que nunca, a lo que propone el libro de texto, que se ha "adaptado" a LOMLOE en un santiamén, casi milagrosamente. O sea, que la primera consecuencia de la ley, en el universo real, es que se use más la programación de las editoriales.  


