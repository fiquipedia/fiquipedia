
# Pruebas evaluaciones internacionales

Me centro en pruebas asociadas a ciencias  

[Evaluaciones internacionales. INEE (Instituto Nacional de Evaluación Educativa)](http://educalab.es/inee/evaluaciones-internacionales)  
* PISA (Programme for International Student Assessment) y PIAAC (Programme for the International Assessment of Adult Competencies)
   * [Preguntas liberadas PISA-PIAAC](http://educalab.es/inee/evaluaciones-internacionales/preguntas-liberadas-pisa-piaac)   
   * [Preguntas liberadas PISA como recursos didácticos de las Ciencias](http://educalab.es/inee/evaluaciones-internacionales/preguntas-liberadas-pisa-piaac/preguntas-pisa-ciencias)  
   * [PISA 2015. Ciencias. Preguntas liberadas](http://www.educacionyfp.gob.es/dctm/inee/internacional/pisa-2015/pisa-2015cienciaspreguntas-liberadas.pdf?documentId=0901e72b81c31419) 
* TIMSS (Trends in International Mathematics and Science Study)  
   * [Preguntas liberadas TIMSS](http://educalab.es/inee/evaluaciones-internacionales/timss/preguntas-liberadas)  
   * [TIMSS 2011. Preguntas liberadas de ciencias para 8º grado (2º ESO) ](http://www.educacionyfp.gob.es/dctm/inee/recursos/timssoctavogrado/pdf-web.timss-2011-preguntas-liberadas-ciencias.pdf?documentId=0901e72b81b034b6)  
   * [Preguntas liberadas de ciencias - Preguntas liberadas TIMSS y PIRLS](http://evaluacion.educalab.es/timsspirls/ciencias)  

También se coloca aquí información sobre pruebas de otros países, que pueden ser de acceso a la universidad del estilo de las [Pruebas Acceso Universidad](/home/pruebas/pruebasaccesouniversidad), o del estilo de [Pruebas de evaluaciones nacionales](/home/pruebas/pruebas-evaluaciones-nacionales)  

[EVALUACIóN DE DIAGNóSTICO PISA Y OTRAS EVALUACIONES - educarm](https://servicios.educarm.es/portal/admin/webForm.php?aplicacion=EDUCARM_MUNDO_MATEMATICO&mode=visualizaAplicacionWeb&web=158&ar=1111&liferay=1&zona=EDUCARM#this)  
Incluye evaluaciones de diagnóstico de las CCAA  


