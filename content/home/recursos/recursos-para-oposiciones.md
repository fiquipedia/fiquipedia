
1. [Problemas del práctico y resolución de elaboración propia](#TOC-Problemas-del-pr-ctico-y-resoluci-n-de-elaboraci-n-propia)
    1. [Problemas del práctico por comunidades](#TOC-Problemas-del-pr-ctico-por-comunidades)
    2. [Problemas del práctico por bloques](#TOC-Problemas-del-pr-ctico-por-bloques)
2. [Temarios oposiciones](#TOC-Temarios)
    1. [Temas de oposición](#TOC-Temas-de-oposici-n)
3. [Normativa sobre ingreso](#TOC-Normativa-sobre-ingreso)
4. [Programación y unidades didácticas](#TOC-Programaci-n-y-unidades-did-cticas)
5. [Academias / fuentes de temas de muestra / prácticos de muestra](#TOC-Academias-fuentes-de-temas-de-muestra-pr-cticos-de-muestra)
6. [Enlaces a foros / asociaciones](#TOC-Enlaces-a-foros-asociaciones)
7. [Criterios valoración tribunales](#TOC-Criterios-valoraci-n-tribunales)
8. [fientos](#TOC-Agradecimientos)


# Recursos para oposiciones
<a name="TOC-Problemas-del-pr-ctico-y-resoluci-n-de-elaboraci-n-propia"></a>
## Problemas del práctico y resolución de elaboración propia 

<a name="TOC-Problemas-del-pr-ctico-por-comunidades"></a>
## [Problemas del práctico por comunidad](/home/recursos/recursos-para-oposiciones/recursos-para-oposiciones-problemas-por-comunidad) 

* [Soluciones Opos FQ (recopilación en un único fichero)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/SolucionesOposFiquipedia.pdf) 

* [twitter ProfaDeQuimica/status/1580170669538836480](https://twitter.com/ProfaDeQuimica/status/1580170669538836480)  
DIFUSIÓN, POR FAVOR.  
Comparto los problemas que hice al preparar el práctico de la #oposición de Secundaria de #FyQ (licencia CC By-Nc-Nd) por si ayuda a otros opositores. ☺️ Obtuve plaza en Madrid en 2015 y quedé a las puertas en CLM.  
[enlace drive](https://drive.google.com/file/d/1GgODfaZJEUiQ0iQlNfmpdhoT3U8HzhPU/view)  

[copia local en gitlab](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/Pr%C3%A1cticos%20FyQ%20oposici%C3%B3n%20Leticia%20Cabezas%20(marca%20de%20agua).pdf)  

Descargar aquí:

Ideas sobre preparación en [OPOSICIONES A FÍSICA Y QUÍMICA - todocienciasblog](https://todocienciasblog.wordpress.com/oposiciones/)  
1.- Exposición de un tema  
ELABORAD VUESTROS PROPIOS TEMAS  
2.- Examen práctico (problemas)  
3-. Elaboración de la programación.  
4.- Presentación de la programación y de la unidad didáctica.  
5.- Fase de concurso.  

[Consejos para la oposición de profesor en la especialidad de Física y Química - fisicatabu.com](https://fisicatabu.com/oposiciones-fyq/consejos-para-la-oposicion-de-profesor-en-la-especialidad-de-fisica-y-quimica/)  

<a name="TOC-Problemas-del-pr-ctico-por-bloques"></a>      
## [Problemas del práctico por bloques](/home/recursos/recursos-para-oposiciones/recursos-para-oposiciones-problemas-por-bloques) 

<a name="TOC-Normativa-sobre-ingreso"></a>
##  Normativa sobre ingreso 

Para ser profesor de física y química en la educación pública hay que pasar por las oposiciones. (en la privada y privada-concertada funciona de manera distinta ...)  

Información sobre titulaciones que permiten ser profesor de física y química en  [Especialidades y materias afines](https://sites.google.com/site/especialidadesymateriasafines/)  

En cada convocatoria suele haber normativa específica.

La normativa que lo regula en 2016 es el Real Decreto 276/2007 Normativa anterior (se citan Agregados que se ven en enunciados antiguos de exámenes)
- [1977 Real Decreto por el que se regula el ingreso en los Cuerpos de Catedráticos Numerarios y de Profesores Agregados de Bachillerato.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1977-4044) 
- [1991 Real Decreto 574/1991, de 22 de abril, por el que se regula transitoriamente el ingreso en los Cuerpos de Funcionarios Docentes a que se refiere la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1991-9822)
- [1993 Real Decreto 850/1993, de 4 de junio, por el que se regula el ingreso y la adquisición de especialidades en los Cuerpos de Funcionarios Docentes a que se refiere la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1993-16874)
- [2004 Real Decreto 334/2004, de 27 de febrero, por el que se aprueba el Reglamento de ingreso, accesos y adquisición de nuevas especialidades en los cuerpos docentes que imparten las enseñanzas escolares del sistema educativo y en el Cuerpo de Inspectores de Educación.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2004-3715)
- [2007 Real Decreto 276/2007, de 23 de febrero, por el que se aprueba el Reglamento de ingreso, accesos y adquisición de nuevas especialidades en los cuerpos docentes a que se refiere la Ley Orgánica 2/2006, de 3 de mayo, de Educación, y se regula el régimen transitorio de ingreso a que se refiere la disposición transitoria decimoséptima de la citada ley.](https://www.boe.es/buscar/act.php?id=BOE-A-2007-4372) 

### Evolución histórica de cuerpos y normativa sobre ingreso
Antes de 1991 había dos cuerpos de profesores de Enseñanza Media (se llamaba Enseñanza Media y no Educación Secundaria) que eran el Cuerpo de Profesores Agregados de Enseñanza Media y el Cuerpo de Catedráticos de Enseñanza Media.  
Las oposiciones de Agregados eran únicas, se celebraban en Madrid, todos los años, se convocaban 20 o 30 plazas (en FyQ) en institutos de localidades concretas que aparecían en el BOE y con tribunales de profesores de toda España. Las oposiciones de Catedráticos también eran únicas, en Madrid, con tribunal formado por catedráticos de Universidad e Instituto, constaban de 5 ejercicios, todos eliminatorios sobre un temario de 150 temas (75 de Física y 75 de Química).  Los interinos se llamaban PNN’s (Profesores No Numerarios)  
Con las transferencias educativas empezaron a celebrarse en las Comunidades Autónomas.  En 1991 se eliminaron los cuerpos de Profesores Agregados y Catedráticos y se unieron un cuerpo único de Profesores de Secundaria

<a name="TOC-Temarios"></a>
## [Temarios oposiciones](/home/recursos/recursos-para-oposiciones/temarios-oposiciones) 

<a name="TOC-Temas-de-oposici-n"></a>
### Temas de oposición 
En la oposición se sacan al azar temas de los 75 temas del temario: en el periodo transitorio (2008 y 2010 en Madrid) se elegían 5 temas de 75, y a partir de 2012 se eligen 4 temas de 75.La elección al alzar de temas se hace en el tribunal 1, y luego se envía al resto de tribunales. Lo más habitual es que una persona del tribunal escriba en la pizarra los epígrafes de cada tema tal y como están en el BOE (es decir, no dicen únicamente el número de tema sino también el texto asociado), aunque en ocasiones puntuales los tribunales deciden hacer una copia impresa y entregársela a cada opositor. Comparto esas hojas de 2010 y de 2014  
[Los 5 temas de Física y Química Madrid 2010](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/Madrid/2010-06-25-Madrid-FQ-temas.pdf)  
[Los 4 temas de Física y Química Madrid 2014](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/Madrid/2014-06-25-Madrid-FQ-temas.pdf)  

En 2016 decido empezar a compartir algunos temas (solamente el tema 35), hay quien contacta para que los venda, pero no pretendo venderlos. **No hay una manera de conseguirlos todos, no los tengo elaborados de manera que pueda compartir, porque cuido licenciamiento; puedo usar cosas cogidas de libros para mi uso personal, pero no publicarlos así. Si puedo dedicarle tiempo iré elaborando alguno más de manera publicable y compartiendo de la misma manera que los problemas, aunque mis materiales están hechos para entenderlos yo.  
Como curiosidad mis "temas" los tengo cada uno en 2 caras de folio, pero con eso escribo en el examen más de 10 caras de folio, gran parte de lo que escribo no está como tal en lo que alguien llamaría "mi temario", sino que lo tengo en la cabeza tras haber hecho esos resúmenes en 2 caras.

Pongo carpeta compartida donde los colocaré si es que los hago: inicialmente comienzo por un único tema, el 35. **No tengo tiempo de ponerme con más, y es poco probable que ponga más** El tema 35 es un tema con el que tuve un 10 en 2014 pero no tuve plaza; en 2016 tras sacar plaza decidí ponerlo en limpio y publicarlo revisando licenciamiento. En 2023 me toca tribunal de oposición y paso a limpio y comparto también temas 10, 21 y 54 (en estabilización salen 10, 21, 44, 54 y 60). Tema 21 lo hice creo que en 2012 y tuve sobre un 9. Tema 10 lo hice en 2010 y creo recordar tuve sobre un 8.  

[OposicionesFQ/TemarioBOE93](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/OposicionesFQ/TemarioBOE93) 

[Comentarios sobre temas, cómo enfocarlos - docentesconeducacion](http://docentesconeducacion.es/viewtopic.php?f=135&t=2586&p=11803#p12000) 

Ver criterios valoración Madrid con comentarios sobre temas Criterios valoración temas 

Andalucía 2016  [2016-Andalucía-criterios tema tribunales.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/Andalucía/2016-Andalucía-criterios%20tema%20tribunales.pdf) 

En 2020 Leticia @profadequimica comparte varios temas  
[twitter ProfaDeQuimica/status/1228692845072125954](https://twitter.com/ProfaDeQuimica/status/1228692845072125954)  
Comparto mis temas propios de #oposición de Secundaria (Física y Química #FyQ) con licencia CC By-Nc-Nd por si ayuda a otros opositores.  Obtuve plaza en Madrid en 2015 (y me calificaron el tema 63 con un 9,2). Descargar aquí:  
[Temas propios FyQ Leticia Cabezas.pdf](https://drive.google.com/file/d/1-A-6JgTeQ7sOyeirs5mvK27zJuBTu-ia/view)  
No están todos los temas. Aquí podéis encontrar los siguientes: 1-12, 19, 37, 42, 50-55, 63, 66, 75. Es decir, un total de 24 temas del total de los 75 vigentes.

Dado el interés de esos materiales, y que incluyen autoría y licenciamiento, comparto una copia junto a mi tema 35. [OposicionesFQ/TemarioBOE93](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/OposicionesFQ/TemarioBOE93)  

[PREPARAR LA OPOSICIÓN A FÍSICA Y QUÍMICA](https://opositarafyq.blogspot.com/2019/) Blog que en 2019 comparte capturas de temas  

[twitter PericoCab/status/1604242858160128005](https://twitter.com/PericoCab/status/1604242858160128005)  
Dejo por aquí algunos de mis temas para las opos de FyQ (Casi todos de física), por si pudiera servir a algún compañero/a.  
[Carpeta con temas escaneados en pdf](https://drive.google.com/drive/folders/1qkDh7-zkhObWwh0nxwFitX8E1CYNlDcm)  

<a name="TOC-Programaci-n-y-unidades-did-cticas"></a>
##  [Programación y unidades didácticas](/home/recursos/recursos-para-oposiciones/programacion-y-unidades-didacticas) 

<a name="TOC-Academias-fuentes-de-temas-de-muestra-pr-cticos-de-muestra"></a>
##  Academias / fuentes de temas de muestra / prácticos de muestra 
Los temas son preparados por diversas academias, que normalmente ofrecen alguna "muestra" para intentar convencer de pagar por todos. La muestra a veces es parte de un tema o un tema completo. A veces entregan copias impresas de temas de muestra. Incluyo algunos ejemplos de enlaces, que se pueden considerar "publicitarios", pero no se trata de eso: menciono los que ofrecen muestras gratis. 
No almaceno recursos aquí: me limito a enlazar recursos en otras webs que son los responsables de respetar licenciamiento y derechos de autor.
Gustosamente incorporaré más si alguien me los envía o retiraré enlaces si se me argumenta y justifica.  
Pongo los nombres de academias por orden alfabético como criterio de neutralidad:

### Cede
 [cede sistema de acceso](https://www.cede.es/sistema-acceso/)  
 En marzo 2012 ofrece tema 01 del temario BOE93 [TEMA MUESTRA.pdf](https://www.serina.es/empresas/cede_muestra/106/TEMA%20MUESTRA.pdf)  
 En 2014 ofrece muestras prácticos de física y química [PRACTICO MUESTRA.pdf](https://www.serina.es/empresas/cede_muestra/106/PRACTICO%20MUESTRA.pdf)  
 En 2019 publica soluciones Física y Química 2018 Madrid [fisicamadrid2018.pdf](https://www.cede.es/examenes2018/fisicamadrid2018.pdf) 

### CenOposiciones

   [Descargas secundaria](https://www.cen.edu/descargas.php#secundaria)  
   En marzo 2012 ofrece tema 66 del temario BOE2011 (derogado en 2012)  

### CEVE
Academia antigua [Física y química: oposiciones al Cuerpo de profesores agregados de Institutos de bachillerato: problemas Autor CEVE. Editor Ceve, 1975](http://books.google.es/books?id=7y2CnQEACAAJ&dq=inauthor:%22CEVE.%22&hl=es&sa=X&ei=mZa2U-L5E8GH0AXntICACA&ved=0CD8Q6AEwBTgK)  

### Didacta21
Práctico de Madrid 2006 y Castilla y León 2006  [D21_EJERCICIOS_PRUEBAS_FYQ.pdf](http://www.didacta21.com/documentos/Cuerpos_Docentes/Profesores_de_Secundaria/Fisica_y_Quimica/D21_EJERCICIOS_PRUEBAS_FYQ.pdf)  
[d21_tema_14_fisica_y_quimica.pdf](http://www.didacta21.com/images/Documentos_provisionales/d21_tema_14_fisica_y_quimica.pdf)  
[Física y Química](https://web.archive.org/web/20161209101536/http://www.didacta21.com/index.php?option=com_content&view=article&id=607&Itemid=415) waybackmachine   

### Educalia 
 [Temarios oposiciones](http://www.e-ducalia.com/temarios-oposiciones.php)  
 En marzo 2012 ofrece tema 06, coincidente en temario BOE93 y BOE2011  [TEMA 6.pdf](https://web.archive.org/web/20150316144932/http://e-ducalia.com/files/201202131322591.TEMA%206.pdf) *waybackmachine*   
 Ejemplo 2014 ofrece "Muestra sesgada Nuevos supuestos prácticos Física y Química 2014-2015"  
 En 2016 ofrece muestra temas 2 y 52  [Fisica y quimica Tema 2 y 52.pdf](http://www.e-ducalia.com/files/201604291402020.Fisica%20y%20quimica%20Tema%202%20y%2052.pdf)
[SUPUESTOS PRÁCTICOS DE EDUCACIÓN SECUNDARIA / EOI](https://e-ducalia.com/supuestos-practicos-oposiciones-secundaria.php)  
[Mostra supòsits pràctics Física i Química - versió Catalunya](https://e-ducalia.com/files/muestra-sup-fiq-cat-pdf.pdf)  
[Mostra supòsits pràctics Física i Química - versió Catalunya 2021](https://e-ducalia.com/files/mostra-sup-fiq-cat-2021-pdf.pdf)  
[Muestra supuestos prácticos Física y Química](https://e-ducalia.com/files/muestra-supuestos-fyq-pdf.pdf)  


### El color de la Física
[Ejercicio de Física resuelto. Oposiciones de enseñanza secundaria: Convocatoria de 2015 en Castilla-La Mancha (I)](http://elcolordelafisica.blogspot.com/2021/03/ejercicio-de-fisica-resuelto.html) David Casas

### ElTemario.com

Antonio Abrisqueta García realizó unos temarios que, teniendo copyright, los ofrecía gratis y no tenía inconveniente de que estuvieran en la web.   
La web eltemario.com deja de estar operativa en 2018, aunque se puede ver cómo era en waybackmachine [eltemario.com](https://web.archive.org/web/20170607225657/http://www.eltemario.com/), donde no se guarda copia de los materiales.  
En 2021 la academia EPO tiene los derechos de temas, problemas, resoluciones y exámenes resueltos.   
[EPO Escuela de Preparación de Opositores](https://www.epodocentes.com/)  
[EPOline Estudios y Preparación Oposiciones online](https://www.eponline.es/)    

Algunas webs pueden tener almacenadas copias de estos materiales; aunque en algunos casos enlazo materiales y es el servidor enlazado el responsable de respetar los derechos de autor, en este caso no enlazo ninguna conociendo la historia del licenciamiento y los derechos de autor de esos materiales.  

[comentario]: # (en agosto 2021 elimino este enlace que citaba antes http://temariosgratis.oposicionesyempleo.com/temariofisicayquimica.zip) 

###  Fisiquímicamente
Rodrigo Alcaraz de la Osa / Jésica Sánchez Mazón [fisiquimicamente](https://fisiquimicamente.com/) #https://rodrigoalcarazdelaosa.me/recursos-fisica-quimica/oposiciones/ (antes de 2021)   
En 2018 compartida resolución aquí de ejercicio 1 Física Cantabria 2016 y ejercicio 1 Física Andalucía 2000, ya que entonces no tenía web  
En 2020 se eliminan en local aquí y se enlazan [fisiquimicamente oposiciones](https://fisiquimicamente.com/recursos-fisica-quimica/formacion-profesorado/oposiciones/)  

###  Kalium academia 
 [http://www.kaliumacademia.com/oposiciones/secundaria](https://web.archive.org/web/20171115034232/http://www.kaliumacademia.com/oposiciones/secundaria) *waybackmachine*  
 En 2021 ya no prepara Física y Química, solo inglés.  
 No localizados temas de muestra, pero sí algunos ejercicios con solución (solamente el resultado final), mezclados con otros niveles, no se dedican solamente a oposiciones.  
 En 2014 publican algunos problemas con resolución completa [http://www.slideshare.net/josemanuelbelmezmacias](http://www.slideshare.net/josemanuelbelmezmacias) 


###  MAD / Eduforma / Campus T12
Como más se conoce es como "MAD"  
En 2017 campust12 tenía su web [campust12.com](https://web.archive.org/web/20190906000341/http://www.campust12.com/) *waybackmachine* y ofrecía tema 43 del temario BOE93  [TM_FYQ.pdf](https://web.archive.org/web/20171013065723/http://www.campust12.com/temarios/TM_FYQ.pdf) *waybackmachine*  
En 2014 ofrece muestras ejemplos prácticos (enlace no funciona en 2017)  
   Tiene bastantes publicaciones   
* [http://books.google.es/books?id=eD67THAMuzcC](http://books.google.es/books?id=eD67THAMuzcC)  
Título Fisica Y Quimica. Profesores de Enseñanza Secundaria. Aplicaciones Didacticas. E-book  
Editor MAD-Eduforma  
ISBN 8466523502, 9788466523509 
* [http://books.google.es/books?id=354FXz7DApwC](http://books.google.es/books?id=354FXz7DApwC)  
Título Fisica Y Quimica. Profesores de Enseñanza Secundaria.temario Especifico Volumen Ii. E-book  
Editor MAD-Eduforma  
ISBN 8466509399, 9788466509398  
* [http://books.google.es/books?id=qx7Tw_7XxM4C](http://books.google.es/books?id=qx7Tw_7XxM4C)  
Título Fisica Y Quimica. Vol. Iii: Quimica I. Profesores de Educacion Secundaria. Temario Para la Preparacion de Oposiciones. Ebook  
Editor MAD-Eduforma  
ISBN 8466505539, 9788466505536  
* [http://books.google.es/books?id=uw7ld77p7IgC](http://books.google.es/books?id=uw7ld77p7IgC)  
Título Fisica Y Quimica. Profesores de Enseñanza Secundaria. Temario Practico. E-book  
Editor MAD-Eduforma  
ISBN 8466524622, 9788466524629

###  Magister

   [http://www.academiamagister.es/sec.htm](http://www.academiamagister.es/sec.htm)  
   En marzo 2012 ofrece tema 21 del temario BOE93  [temamu-fq.pdf](https://web.magister.com/wp-content/uploads/2019/10/tema-muestra-fq.pdf)  
   También ofrece el tema 49 del temario BOE2011  [temamu-fq-sust.pdf](http://www.academiamagister.es/temas/temamu-fq-sust.pdf)  
   En 2014 ofrece vídeos con algunos prácticos  [Ejemplo_de_Practico_de_fisica](http://www.academiamagister.es/practicos.html#Ejemplo_de_Practico_de_fisica)  
   En 2024 ofrece ejemplo prácticos [LOMLOE_FQ_E_RESUELTO_MUESTRA.pdf](https://web.magister.com/wp-content/uploads/2021/07/LOMLOE_FQ_E_RESUELTO_MUESTRA.pdf)  

###  Profe Luis Muñoz

[Profe Luis Muñoz - canal youtube](https://www.youtube.com/@luismunozmato3217)  
Profesor de secundaria en la Comunidad Autónoma de Galicia y preparador de oposiciones.  
Se resuelven problemas de las últimas convocatorias de oposiciones.  
El objetivo del canal es compartir conocimiento (no está monetizado)  

###  Tu academia en la nube

   *  [Tu academia en la nube](https://tuacademiaenlanube.com/) 
   *  [Tu academia en la nube - canal de youtube](https://www.youtube.com/tuacademiaenlanube)  canal con problemas resueltos
   *  [Tema 5 gratis](https://tuacademiaenlanube.com/contacto/tema-de-muestra-oposiciones-secundaria-fisica-y-quimica/)  Tema gratis (no descarga directa, hay que dar los datos)
   *  [Ejercicios Oposiciones Secundaria Física y Química 04 Ácido Base valoración y composición de una muestra.pdf](https://tuacademiaenlanube.com/wp-content/uploads/2020/05/Ejercicios-Oposiciones-Secundaria-Física-y-Química-04-Ácido-Base-valoración-y-composición-de-una-muestra.pdf)  ejercicio resuelto Castilla y León 2006

<a name="TOC-Enlaces-a-foros-asociaciones"></a>
##   Enlaces a foros / asociaciones 


   * Blog "OPOSICIONES: MIS CONSEJOS. FÍSICA Y QUÍMICA" [ http://opsfisquim.blogspot.com.es/](http://opsfisquim.blogspot.com.es/)  Consejos, temas, problemas, programación, prácticas, ... Autor: Agustín Sánchiz. Licenciamiento cc-by-nc-nd
   * Foro "Docentes con Educación" [http://docentesconeducacion.es/](http://docentesconeducacion.es/)  
   Varios usuarios comparten enunciados y soluciones ... resoluciones de cara a las oposiciones de 2016  [¡Ánimo Compañeros!](http://docentesconeducacion.es/viewtopic.php?f=92&t=3812)  
   [Andalucía](http://docentesconeducacion.es/viewtopic.php?f=92&t=4233)  
   [Aragón 2015](http://www.docentesconeducacion.es/viewtopic.php?f=92&t=3599)  
   [Asturias](http://docentesconeducacion.es/viewtopic.php?f=92&t=3757#p20617)  
   [Baleares](http://www.docentesconeducacion.es/viewtopic.php?f=92&t=4120)  
   [Cantabria](http://docentesconeducacion.es/viewtopic.php?f=92&t=4359)  
   [Castilla-La Mancha 2015](http://docentesconeducacion.es/viewtopic.php?f=92&t=3533&p=16101)  
   [Castilla y León 2015](http://www.docentesconeducacion.es/viewtopic.php?f=92&t=3569)  
   [Cataluña](http://docentesconeducacion.es/viewtopic.php?f=92&t=4253)  
   [Ceuta y Melilla](http://www.docentesconeducacion.es/viewtopic.php?f=92&t=4324)  
   [Galicia](http://www.docentesconeducacion.es/viewtopic.php?f=92&t=4018)  
   [Extremadura](http://docentesconeducacion.es/viewtopic.php?f=92&t=4271)  
   [Extremadura 2015](http://docentesconeducacion.es/viewtopic.php?f=92&t=3470)  
   [Madrid](http://docentesconeducacion.es/viewtopic.php?f=92&t=4181)  
   [Murcia](http://docentesconeducacion.es/viewtopic.php?f=92&t=4239)  
   [País Vasco](http://docentesconeducacion.es/viewtopic.php?f=92&t=4627)  
   [Rioja 2015](http://www.docentesconeducacion.es/viewtopic.php?f=92&t=3596)  
   [Valencia](http://www.docentesconeducacion.es/viewtopic.php?f=92&t=4125)  
   [Valencia 2015](http://www.docentesconeducacion.es/viewtopic.php?f=92&t=3585)  
   [Valencia 2016](http://docentesconeducacion.es/viewtopic.php?f=92&t=4579) 
   * Foro "Compartiendo conocimiento", de José Sande Trata temas generales de oposiciones que son útiles aunque sea de especialidad de economía [https://josesande.com/](https://josesande.com/) 
   * [Associacio per a l'Ensenyament de la Fisica y la Quimica - Curie. Enunciats d'exercicis d'oposicions](https://web.archive.org/web/20100726090003/http://curie.lacurie.org/materials/oposicions.htm) waybackmachine (operativo en 2015, deja de estar operativo en 2016, y el grupo de Google deja de ser público  [https://groups.google.com/forum/?nomobile=true#!forum/curie](https://groups.google.com/forum/?nomobile=true#!forum/curie) )

<a name="TOC-Criterios-valoraci-n-tribunales"></a>
##  Criterios valoración tribunales 
Deberían ser públicos, pero no suelen serlo. Información sobre tribunales y transparencia en blog  
[http://algoquedaquedecir.blogspot.com/2018/03/oposiciones-tribunales.html](http://algoquedaquedecir.blogspot.com/2018/03/oposiciones-tribunales.html)  
[http://algoquedaquedecir.blogspot.com/2018/03/oposiciones-transparencia.html](http://algoquedaquedecir.blogspot.com/2018/03/oposiciones-transparencia.html) 
[https://algoquedaquedecir.blogspot.com/2021/03/oposiciones-transparencia-valoracion.html](https://algoquedaquedecir.blogspot.com/2021/03/oposiciones-transparencia-valoracion.html)

###  2014 
 Madrid: Comparto los criterios de corrección en junio de 2014 en cuanto a tema, problemas, programación y unidad.  No hay ninguna garantía de que esos criterios sean válidos la próxima vez: por ejemplo indican 2 ejercicios de física y 2 de química, pero otras convocatorias han sido 3 de física y 3 de química.  
[Se incluye una foto](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/Madrid/2014-Madrid-FQ-Tribunal-Criterios.jpg) , y pongo aquí el texto (parcial)...
 
CRITERIOS E INDICADORES DE VALORACIÓN  
FÍSICA Y QUÍMICA  

CONSIDERACIONES GENERALES  
1. En las pruebas que deban realizarse por escrito, además de calificar la prueba de acuerdo con el contenido, se valorará la capacidad de redacción, manifestada en la exposición ordenada de las ideas, el correcto engarce sintáctico, la riqueza léxica y la matización expresiva. La ortografía será juzgada en su totalidad, letras, tildes y signos de puntuación, de acuerdo con las siguientes normas: Por cada falta de ortografía se deducirá medio punto de la calificación del ejercicio. Cuando se repita la misma falta de ortografía, se contará como una sola. Por la reiteración de errores de puntuación y faltas de acentuación se podrán deducir hasta tres puntos de la calificación del ejercicio, según la apreciación del corrector.
2. En el caso de problemas se valorará la adecuada estructuración y el rigor en el desarrollo de su resolución y la inclusión de pasos detallados así como la realización de diagramas, dibujos y esquemas. Se valorará así mismo la identificación de principios y leyes físicas involucradas así como la corrección de los resultados numéricos. un uso incorrecto de unidades, así como errores en la formulación, nomenclatura y lenguaje químico supondrá la calificación de cero en el correspondiente problema.
3. En aquellos ejercicios en los que el desarrollo del tema haya de ser oral, se valorará además los recursos didácticos y pedagógicos de los aspirantes en la exposición así como la coherencia, la fluidez verbal y la capacidad de manejar tiempos. 

Ejercicio práctico  
1. Consistirá en la resolución de cuatro problemas dos de Física y dos de Química. Cada uno de los cuales se calificará de 0 a 10 puntos. En caso de tener varios apartados la calificación de cada uno de ellos será la que figure en el texto.  
...

###  2015
Madrid: En 2015 el día del examen había una única hoja en cada tribunal que ponía esto, que se publica también en el tablón digital  
"**CRITERIOS DE CORRECCIÓN: ORTOGRAFÍA, SINTAXIS Y REGISTRO**  
Los criterios de corrección ortográficos elaborados para su seguimiento por todos los tribunales del procedimiento selectivo de ingreso al Cuerpo de Profesores de Enseñanza Secundaria, y procedimiento para la adquisición de nuevas especialidades por los funcionarios del citado Cuerpo, convocado mediante Resolución de 6 de Marzo de 2015, de la Dirección General de Recursos Humanos de la Consejería de Educación, Juventud y Deporte de la Comunidad de Madrid, serán los siguientes  
- La escritura incorrecta de una palabra supone la disminución de la nota en 1 punto, salvo que la única incorrección se deba a una tilde, en cuyo caso la disminución será de 0,5 puntos.
- La escritura de dos palabras como una sola supone una disminución de 0,5 puntos.
- La división de una palabra en dos dentro de un renglón supone una disminución de 0,5 puntos.
- La separación de dos vocales de una palabra, formen o no diptongo, al cambiar de reglón supone la disminución de un punto.
- La utilización de abreviaturas para expresar palabras, nexos, pronombres, terminaciones de adverbios, del tipo “tb” por también, “pq” en vez de porque o por qué, “q” en lugar de que o qué, adverbios que terminan en mente y se utiliza la “barra inclinada”, etc…, se penalizará con la disminución de 0,25 puntos.
- Las faltas cometidas en palabras que se repiten se contabilizarán una sola vez.

Estos criterios de corrección serán aplicados en todos los ejercicios escritos que realice el aspirante.

Madrid, mayo de 2015

LA COMISIÓN DE SELECCIÓN

"En el pie de folio físico, no en el tablón digital, indicaba "TRIBUNALES DE OPOSICIÓN FÍSICA Y QUÍMICA.COMISIÓN DE SELECCIÓN. CONVOCATORIA 2015."  
2015 En 2015 el día del examen había una única hoja en cada tribunal que ponía esto, que se publica también en el tablón digital "Comparto dos documentos recibidos que en teoría utilizaron internamente los tribunales de Madrid en 2015  
[Guión para valoración del tema](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/Madrid/2015-Madrid-GuionValoracionTema.jpg)  
[Guión para valoración de Programación Didáctica y Unidad Didáctica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/OposicionesFQ/Madrid/2015-Madrid-GuionValoracionPD-UD.jpg) 

###  2016
 Madrid: En 2016 publican criterios por primera vez en la web.
   *  [CRITERIOS DE CORRECCIÓN ORTOGRÁFICOS Y ASPECTOS FORMALES DE LA PROGRAMACIÓN DIDÁCTICA](http://www.madrid.org/cs/Satellite?blobcol=urldata&blobheader=application/pdf&blobheadername1=Content-disposition&blobheadername2=cadena&blobheadervalue1=filename=Criterios+comunes+ortogr%C3%A1ficos+y+formales.pdf&blobheadervalue2=language=es&site=PortalEducacionRRHH&blobkey=id&blobtable=MungoBlobs&blobwhere=1352908834651&ssbinary=true) *waybackmachine*   
   *  [Criterios de Física y Química](https://web.archive.org/web/20170923181954/http://www.madrid.org/cs/Satellite?blobcol=urldata&blobheader=application/pdf&blobheadername1=Content-disposition&blobheadername2=cadena&blobheadervalue1=filename=Criterios+-+2016+-+F%C3%8DSICA+Y+QU%C3%8DMICA.pdf&blobheadervalue2=language=es&site=PortalEducacionRRHH&blobkey=id&blobtable=MungoBlobs&blobwhere=1352909046068&ssbinary=true) *waybackmachine*   

###  2018
Se publican criterios en web de oposiciones Cataluña (que en 2018 se celebran en abril). 

Día 20 junio 2018 (3 días antes de las pruebas día 23) criterios en Madrid  
[Procedimientos selectivos para ingreso a los Cuerpos de Secundaria, Formación Profesional y Régimen Especial. Criterios de evaluación..>](http://www.madrid.org/cs/Satellite?c=EDRH_Generico_FA&cid=1354699557074&pagename=PortalEducacionRRHH%2FEDRH_Generico_FA%2FEDRH_generico)  
Se publican los criterios de evaluación de las especialidades convocadas en los procedimientos selectivos 2018.

Enlace directo a  [criterios Física y Química 2018](http://www.madrid.org/cs/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-disposition&blobheadername2=cadena&blobheadervalue1=filename%3D007+FISICA+Y+QUIMICA+CRITERIOS.pdf&blobheadervalue2=language%3Des%26site%3DPortalEducacionRRHH&blobkey=id&blobtable=MungoBlobs&blobwhere=1352956920396&ssbinary=true)  (pdf de 5 páginas: se citan detalles de calculadora por primera vez, y uso de regla)Temas de calculadora en página aparte de [ recursos calculadora](/home/recursos/recursos-uso-calculadora)  

A partir de 2018, tras lo realizado en [Oposiciones: transparencia enunciados](https://algoquedaquedecir.blogspot.com/2018/08/oposiciones-transparencia-enunciados.html) muchas comunidades publican. Algunos criterios se comparten junto a los enunciados en [Problemas del práctico por comunidad](/home/recursos/recursos-para-oposiciones/recursos-para-oposiciones-problemas-por-comunidad) 

<a name="TOC-Agradecimientos"></a>
##  Agradecimientos 
 Tras empezar a compartir problemas en 2014 y proponer que la gente me envíe enunciados, he recibido bastantes, y quiero agradecer desde aquí a todas las personas que comparten, tanto al enviármelos directamente a mi, como indirectamente al poner públicamente enunciados y soluciones en otros foros (ver enlaces a foros / asociaciones).  
En 2014 era algo raro tener enunciados originales, pero afortunadamente gracias a compartir entre todos y la búsqueda de [transparencia de enunciados](http://algoquedaquedecir.blogspot.com/2018/08/oposiciones-transparencia-enunciados.html)  ya se ve al contrario y se ve raro no disponer de ellos.  
Agradecimientos especiales: 
- Usuarios sleepylavoisier, basileia, jal (conozco sus nombres reales pero no los pongo mientras no me lo indiquen) del foro  [www.docentesconeducacion.es](http://www.docentesconeducacion.es/), por compartir resoluciones, comentarios y correcciones. También a otros usuarios que han compartido información en ese foro. 
- Antonio Abrisqueta García, que en 2015 y 2016 comparte numerosos enunciados originales de varias comunidades, además de comentarios y correcciones.
- Leticia Rodrigo, que en 2016 comparte enunciados originales
- J.A. que en 2016 comparte enunciados originales y resoluciones
- Felipe que en 2016 comparte enunciados
- druizpineda que en 2016 comparte enunciados
- Daniel Martínez Mora que en 2016 comparte enunciados
- Jesús García Martín que en 2018 comparte enunciados originales
- Rodrigo Alcaraz de la Osa que en 2018 comparte resoluciones en latex
- "Heisenberg" (foro docentesconeducacion.es) que en 2018 comparte enunciados originales
- Javier Perán Jódar. Javier Sánchez Pina. José A. Illán Ortuño. que en 2019 comparten enunciados con resoluciones
- Pedro Javier Díaz de Vega, que en 2022 comparte enunciados con soluciones  
- Roberto Palau García y Roberto Palau Lage, que en 2023 envían propuestas de correciones.  
- MartínMF, que en 2023 y 2024 comparte comentarios y correcciones.  
- Javier Serrano, que en 2023 y 2024 comparte comentarios y correcciones.   


