# Recursos uso calculadora

La calculadora científica es algo esencial, y en general veo que hay problemas en su manejo, por lo que intentaré colocar aquí recursos para aprender o practicar en su uso correcto, o temas asociados....  
Inicialmente la página surge para calculadoras "clásicas", como aparato, pero intento ir comentando todo  

También puede estar asociado a notación científica en la calculadora y ser reflexivo  
[twitter pmateo21/status/1583469141524369408](https://twitter.com/pmateo21/status/1583469141524369408)  
Les das una calculadora por primera vez en el curso, y lo primero que les preguntas es:  
¿Cuál es la última cifra del número que sale al multiplicar 6720576 por 347240280?  
Y caen COMO MOSCAS  
Esto sirve (además de para introducir la notación científica) para aprender cómo y cuándo usar la calculadora, así como sus limitaciones.  


##  Información general calculadoras
Calculator software and online resources  
[http://www.calculator.org/](http://www.calculator.org/)  
Incluye una base de datos de calculadoras con información e imágenes  

Un poco de humor para recordar la importancia de conocer y manejar bien la calculadoraLa calculadora estaba en radianes…  
[ingenierodelmonton.com La calculadora estaba en radianes… ¿A quién no le ha pasado?](https://ingenierodelmonton.com/post/59773317395/la-calculadora-estaba-en-radianes-a-qui%C3%A9n-no-le)  
[![](https://64.media.tumblr.com/468cfa314c24fde9112e9912176eb76a/tumblr_msckn7TOZo1sa0ryio1_500.jpg "") ](https://64.media.tumblr.com/468cfa314c24fde9112e9912176eb76a/tumblr_msckn7TOZo1sa0ryio1_500.jpg)  

[twitter memecrashes/status/1741979854268973495](https://twitter.com/memecrashes/status/1741979854268973495)  
![](https://pbs.twimg.com/media/GCzBcY1WkAAPWNe?format=jpg)  

## Marcas de calculadoras  
Según calculator.org las marcas principales son Casio, Canon, Citizen, HP (Hewlett-Packard), Sharp, TI (Texas Instruments)  
Pero voy colocando marcas y modelos según se las veo utilizar a alumnos  
En 2014 uno de los modelos más habituales utilizado por alumnos en educación secundaria es CASIO FX-82MS S-V.P.A.M. con tapa dura.  
En 2014 empieza a ser habitual la GAVAO FX-350MS, que es un clon de la CASIO FX-350MS, muy similar a la FX-82MS, también con tapa dura  
Hay otras réplicas similares no CASIO  
Kenko:  
[amazon.com Kenko Scientific Calculator fx-82LB](https://www.amazon.com/Kenko-KK-570MS-Scientific-Calculator-fx-82LB/dp/B0007Y7Q6A)
Jonius:  
[aliexpress.com Zhongcheng 350ms multifunctional trigonometric calculator](http://www.aliexpress.com/item/Cheap-amazing-Zhongcheng-350ms-multifunctional-trigonometric-calculator/1287569213.html)  
Catiga:  
[walmart.com catiga cs-183 2-line lcd display scientific calculator](https://www.walmart.com/ip/catiga-cs-183-2-line-lcd-display-scientific-calculator-suitable-for-school-and-business/825485822?wmlspartner=wlpa&selectedSellerId=916)  
Helect  
[amazon.es Helect Calculadora Científica de Ingeniería de 2 Líneas](https://www.amazon.es/Helect-H-1002-Calculadora-Cient%C3%ADfica-Muestran/dp/B01MCQMORK)  
ESTAR 209010  
[ebay.es CALCULADORAS CASIO FX 82 MS & ESTAR 209010](https://www.ebay.es/itm/CALCULADORAS-CASIO-FX-82-MS-ESTAR-209010-/324151141256) 

##  Uso de calculadora en pruebas acceso universidad
Puede haber comentarios en  [recursos PAU genéricos](/home/recursos/recursospau/recursos-pau-genericos)  

Vídeo junio 2021 de casio
[![¿Qué calculadoras están permitidas en la selectividad / EBAU 2021?](https://img.youtube.com/vi/vjAgwqo5o6I/0.jpg "¿Qué calculadoras están permitidas en la selectividad / EBAU 2021?")](https://www.youtube.com/watch?v=vjAgwqo5o6I)  
En general no se permiten calculadoras programables  

[madrimasd.org De los encerados a las calculadoras, 2 diciembre 2018](http://www.madrimasd.org/blogs/matematicas/2018/12/02/145992)  
La Federación Española de Sociedades de Profesores de Matemáticas (FESPM)  [ha hecho público un informe, ](http://apmcm.feemcat.org/wp-content/uploads/2018/10/INFORME_FESPM-DEFINITIVO.pdf) exigiendo que se suspenda la prohición del uso de calculadoras en los exámenes de acceso a la universidad. Su informe es razonado y convincente, mientras que las decisiones que se han tomado desde las universidades al respecto son bastante arbitrarias. Me remito al propio informe y recuerdo una de sus frases:  
“… el uso de las calculadoras en el Bachillerato supone la continuidad de un proceso que se inicia en Primaria y continúa en la Educación Secundaria Obligatoria. La competencia digital forma parte de las habilidades necesarias que todo ciudadano debe adquirir, según las recomendaciones del Parlamento y del Consejo Europeo.”

###  Madrid
En PAU 2016 de Madrid se publica este texto (Ficheros en carpetas drive.fiquipedia de PAU,  [2016 madrid nota aclaratoria calculadoras.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/PAUxComunidades/fisica/madrid/2016-madrid-nota_aclaratoria_calculadoras.pdf) )  
[uam.es nota aclaratoria calculadoras.pdf](http://www.uam.es/estudiantes/acceso/docs/acceso/bachfp/acuerdos_comision/nota_aclaratoria_calculadoras.pdf)  
>*NOTA ACLARATORIA SOBRE USO DE CALCULADORAS Y OTROS INSTRUMENTOS ELECTRÓNICOS*  
Según acuerdo previo de la comisión organizadora, actualizado para el curso 2015/16, en la PAU no está permitida la utilización por los estudiantes de teléfonos móviles o de cualquier otro dispositivo que permita la conexión inalámbrica. En este concepto estarían incluidos los dispositivos portátiles tipo tableta, los asistentes digitales personales (PDA), los relojes, gafas, pulseras y cualquier otro objeto que tenga capacidad de enviar o recibir mensajes a través de una pantalla.  
Se mantiene la autorización del uso de calculadoras científicas estándar con las siguientes características:  
- No podrán disponer de capacidades gráficas ni de cálculo simbólico.  
- Las pantallas no dispondrán de más de dos líneas de salida de información alfanumérica.  
- Solo podrán tener capacidad para almacenar los datos numéricos necesarios para cálculos estadísticos o intermedios.  

En 2017 se publica documento similar  [2017-madrid-normas-instrumentos-electronicos.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/PAUxComunidades/fisica/madrid/2017-madrid-normas-instrumentos-electronicos.pdf)  
Cada curso van publicando documentos: se puede buscar el últimoEn 2020  
[ucm.es 2019-01-23 CALCULADORAS PERMITIDAS EN EvAU en 2019](https://www.ucm.es/data/cont/docs/3-2019-01-23-CALCULADORAS%20PERMITIDAS%20EN%20EvAU%20en%202019_(2).pdf)  
Algunas no aparecen: clónicas, o algunas similares  
[edu-casio.es Diferencia entre la ClassWiz fx-82SP X y fx-82SP X II*](https://www.edu-casio.es/diferencia-entre-la-classwiz-fx-82sp-x-y-fx-82sp-x-ii/) 

###  Murcia
Incluye lista modelos [ACUERDO SOBRE EL USO DE CALCULADORAS EN LA PAU (junio y septiembre de 2016)](https://www.um.es/documents/877924/3224646/Calculadoras+(web).pdf/51fa395d-c9e9-4df7-8595-ae93878b3f13) 

###  Valencia
[Acord de la Comissió Gestora dels Processos d’Accés i Preinscripció sobre la utilització de calculadores en les Proves d’Accés a la Universitat (PAU)](http://www.ceice.gva.es/documents/161863209/162837127/Acord_Gestora_Calculadores.pdf/5f969e23-b510-4620-8956-38abc084378c) 

###  Zaragoza
Incluye lista de modelos [http://www.xavierre.es/docs/other/319.pdf](http://www.xavierre.es/docs/other/319.pdf) 

##  Documentación y manuales de calculadoras

###  Auchan (Alcampo en España)
En 2014 veo alumnos con la calculadora CS-08 Plus; los alumnos comentan que son muy baratas. Hay una imagen en wikipedia, pero no he encontrado documentación.  
[![](http://upload.wikimedia.org/wikipedia/commons/thumb/9/90/AUCHAN_CS-08_PLUS.jpeg/281px-AUCHAN_CS-08_PLUS.jpeg "") ](http://upload.wikimedia.org/wikipedia/commons/thumb/9/90/AUCHAN_CS-08_PLUS.jpeg/281px-AUCHAN_CS-08_PLUS.jpeg) 

###  CASIO
Enlace a todos los manuales  [Manuales CASIO](http://support.casio.com/manuallist.php?rgn=5&cid=004)  
Enlace directo a manual de FX-82MS:  [fx-82MS/83MS/85MS/270MS/300MS/350MS Manual](http://support.casio.com/manualfile.php?rgn=5&cid=004001013)  
Consulta planteada en 2020:  
>https://www.edu-casio.es/diferencia-entre-la-classwiz-fx-82sp-x-y-fx-82sp-x-ii/ veo que indica "Esta información es también aplicable a las calculadoras CASIO ClassWiz fx-991 SP X II y la CASIO ClassWiz fx-570 SP X II"Quería saber si aplica también a https://www.edu-casio.es/fx-350spx-ii-iberia/ respecto a https://www.casio-europe.com/es/productos/calculadora-escolar-y-grafica/calculadora-tecnico-cientifica/fx-350spx/  
_Hemos recibido su consulta sobre las diferencias entre los modelos de calculadora que tienen o no doble barra. Como indica el artículo al que hace referencia, la única diferencia entre ellas es el idioma euskera, las funciones matemáticas son las mismas. Esto es aplicable también al modelo fx-350SP X, pero no aparece en el artículo porque ya estaba descatalogada._

### HP Hewlett Packard

[hpcalcs.com/](https://hpcalcs.com/)  

[Calculadora científica HP 300s+ Guía de usuario](https://hpcalcs.com/downloads/HP300s+_User%20Guide_Espanol_SP.pdf)  

###  MILAN
 [milan.es calculadoras](https://www.milan.es/es/calculadoras)  
 Una calculadora habitual en 2014 es MILAN M240  
 [milan.es INSTRUCCIONS manual m240.pdf](http://www.milan.es/FitxersWeb/29604/8411574040194_PDF_INSTRUCCIONS_manual%20m240.pdf) 

##  Calculadoras online
Existen algunas calculadoras online, pero se cita simplemente [http://www.wolframalpha.com/](http://www.wolframalpha.com/)  que además de realizar cálculos permite mucho más: resolver, representaciones, comprobaciones,...

##  Apps calculadoras android
Aunque hay apps android que son emuladores, las apps puestas aquí son autónomas, no necesitan ROM, y como mucho "emulan" la apariencia ...  
Además hay calculadoras que hacen reconocimiento gráfico del enunciado / resolución paso a paso  
[play.google.com Calculadora ++ Sergey Solovyev](https://play.google.com/store/apps/details?id=org.solovyev.android.calculator&hl=es)  Sergey Solovyev  
Descubierta en 2015, muy buena interfaz y funcionalidad, además de poco tamaño (3 MB)  
Licenciamiento abierto, con acceso a código fuente [https://github.com/serso/android-calculatorpp](https://github.com/serso/android-calculatorpp)  

[fqcolindres Calculadoras científicas para android](http://fqcolindres.blogspot.com.es/2014/09/calculadoras-cientificas-para-android.html)  
  
[play.google.com Calculadora Clásica. Recados Franvi S.L.](https://play.google.com/store/apps/details?id=com.developstudios.casio)  
Imagen realista (cogida de Google Play)  
[![](https://lh5.ggpht.com/8C2Hz5VTe-7AzKMiSTKG3JbfvhWg4Hce9Hoq0l4cfDPy7fI6SZjV4hZy0OAi4Gd3nw=h900 "") ](https://lh5.ggpht.com/8C2Hz5VTe-7AzKMiSTKG3JbfvhWg4Hce9Hoq0l4cfDPy7fI6SZjV4hZy0OAi4Gd3nw=h900)  
Poco usable en versión noviembre 2014:  
no usa notación científica bien!  
1;EXP;9;= -> 1000000000 (bien, un uno y nueve ceros)  
1;EXP;10;= -> 1000000000 (bien, un uno y diez ceros)  
1;EXP;11;= -> 1000000000 (mal)  
...  
1;EXP;27;= -> 1000000000 (mal)  
1;EXP;28;= -> 1000000000 (mal)  
1;EXP;29;= -> -E- (mal)  
1;EXP;-9;= -> 0.000000001 (bien)  
1;EXP;-10;= -> 0.000000000 (mal)  
...  
1;EXP;-15;= -> 0.000000000 (mal)  
1;EXP;-16;= -> 0 (mal)  
1;EXP;10;*;1;EXP;10;= -> 1000000000 (mal)  
Si se pone "pi", indica 3.141592653 (mal) es 3.14159265358979323846... redondeando con ese número de decimales (caben más en "el display") sería 3,141592654  
  
[play.google.com Calculadora Científica Panecal. Appsys](https://play.google.com/store/apps/details?id=jp.ne.kutu.Panecal)  
  
[play.google.com Scientific Calculator. Scalea software](https://play.google.com/store/apps/details?id=com.scaleasw.powercalc)  
Imagen tomada de google play  
[![](https://lh6.ggpht.com/U_NyvaXsfBHokFUsl1vKJ-RsNejRdppqPw9DJZ-SMECnIm3P8UdP4iU9oHUnsUN4wl1n=h900 "") ](https://lh6.ggpht.com/U_NyvaXsfBHokFUsl1vKJ-RsNejRdppqPw9DJZ-SMECnIm3P8UdP4iU9oHUnsUN4wl1n=h900) 

###  Apps calculadoras con reconocimiento enunciado y/o resolución paso a paso 
MalMath: Step by step solver  
[play.google.com Malmath: Step by step solver](https://play.google.com/store/apps/details?id=com.malmath.apps.mm)  

Resolución paso a paso  
[play.google.com Photomath](https://play.google.com/store/apps/details?id=com.microblink.photomath)  
Reconocimiento gráfico con la cámara

##  Calculadoras "paso a paso escolar"
[play.google.com La calculadora de Alicia](https://play.google.com/store/apps/details?id=com.jpl.aliciacalc)  
La Calculadora de Alicia es una herramienta que realiza las operaciones de suma, resta, multiplicación, división, raíces cuadradas y descomposición factorial. Muestra el algoritmo completo, tal y como se realizan estas operaciones manualmente.  
[![](https://lh3.ggpht.com/sukn-t3kUe6ERmuyr3OPgEuEkpR1HBkS5wK2WO1GMMIwhvTI5NKtor2nTrpS4VmdOf8=w1366-h670 "") ](https://lh3.ggpht.com/sukn-t3kUe6ERmuyr3OPgEuEkpR1HBkS5wK2WO1GMMIwhvTI5NKtor2nTrpS4VmdOf8=w1366-h670)  

[play.google.com Сalculadora de divisiones Andrei Brusentsov](https://play.google.com/store/apps/details?id=andrei.brusentcov.schoolcalculator.free) 

##  Emuladores calculadoras
Algunas pueden ser apps android, pero no son calculadoras android... : la diferencia es que usan la ROM de la calculadora.... Las ROM no son de distribución libre, pero están publicadas en muchos sitios

###  Texas instruments
 [http://www.ticalc.org/](http://www.ticalc.org/)  
 [http://electronika2.tripod.com/indice_tir.htm](http://electronika2.tripod.com/indice_tir.htm) 

###  Hewlett-Packard
Droid48. Arnaud Brochard  
Emulator of the HP 48 scientific calculator, using RPN. Port of the X48 project  
[https://play.google.com/store/apps/details?id=org.ab.x48](https://play.google.com/store/apps/details?id=org.ab.x48) 

##  Calculadoras como programas informáticos en local
Algunas tienen interfaz web ... son muchas. Pongo algunos programas locales

###  SpeedCrunch
 [http://speedcrunch.org/](http://speedcrunch.org/)  
 Windows, OS X y Linux

###  A Chemical Calculator
 [http://gchemutils.nongnu.org/gchemcalc.html](http://gchemutils.nongnu.org/gchemcalc.html) 

##  Recursos didácticos sobre calculadoras
Se ponen algunos recursos, sin importar el nivel (hay algunos de primaria)  

El uso de la calculadora en el aula  
[http://www.actiludis.com/?p=3082&](http://www.actiludis.com/?p=3082&)  

Ejercicios para usar la calculadora  
[http://www.actiludis.com/?p=3476](http://www.actiludis.com/?p=3476)  
cc-by-nc-sa

###  Análisis de regresión lineal con calculadora
Por ejemplo lo permite calculadora FX-82MS, ver manual  
>"Cálculos de regresión  
Utilice la tecla `[mode]` para ingresar el modo REG cuando desea realizar cálculos estadísticos usando la regresión.  
REG : `[mode][3]`  
En el modo SD y modo REG, la tecla `[M+]` opera como la tecla `[DT]`  
Inicie siempre el ingreso de datos con `[shift][CLR][1][=]` para borrar la memoria estadística  
Ingrese los datos usando la secuencia de tecla siguiente `<datos x> \[ ' ] <datos y> [DT]`"

####  Funciones ocultas en CASIO fx-82MS
 [gaussianos.com Interesantes (y ocultas) funciones de la CASIO fx-82MS](http://gaussianos.com/interesantes-y-ocultas-funciones-de-la-casio-fx-82ms/) 

####  Prioridades y errores con calculadoras
 [gaussianos.com Jerarquía de las operaciones y «el síndrome del paréntesis invisible»](https://www.gaussianos.com/jerarquia-de-las-operaciones-y-el-sindrome-del-parentesis-invisible/)  
 [![https://gaussianos.com/images/operaciones.jpeg](https://gaussianos.com/images/operaciones.jpeg "") ](https://gaussianos.com/images/operaciones.jpeg)  
 
 [twitter FiQuiPedia/status/1076947713802100737](https://twitter.com/FiQuiPedia/status/1076947713802100737)  
 Me ha recordado esto visto en clase por alumnos: si se escribe √2e-2 la calculadora no está contemplando √2·10^-2, sino √(2e-2)= √2·10^-1.  
 La calculadora (casio fx-82MS) asume paréntesis/considera más prioritario "e-2" que √.  
 
 
