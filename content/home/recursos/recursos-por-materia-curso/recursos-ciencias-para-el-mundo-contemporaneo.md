
# Recursos Ciencias para el Mundo Contemporáneo 1º Bachillerato

La asignatura se conoce como CMC (en LOE era obligatoria), y en la LOMCE desaparece en cierto modo se puede ver como que sus contenidos pasan a estar en dos optativas:  [Cultura Científica en 4º ESO](/home/materias/eso/cultura-cientifica-4-eso/curriculo-cultura-cientifica-4-eso)  y  [Cultura Científica 1º Bachillerato](/home/materias/bachillerato/cultura-cientifica-1-bachillerato/curriculo-cultura-cientifica-1-bachillerato)   
  
Pendiente poner miles de enlaces, pero como con LOMCE desaparece la materia CMC antes de ponerlos, en general los iré poniendo ya en Cultura Científica, aunque algunos citen CMC, a no ser los propios libros y materiales que estén elaborados pensando en CMC, que pueden ser reutilizables tomando partes.  
  
 [ciencias-frente-creencias-religiosas.html](http://www.circuloesceptico.org/articulos/ciencias-frente-creencias-religiosas.html)  Enlace no operativo en 2016  
Artículo para el bloque "ciencia", lo considero interesante. Autor Juan Antonio Aguilera Mochón   
  
 [Ciencia vs. Religión. Enseñando la diferencia, resolviendo el conflicto | Sin Dioses](http://www.sindioses.org/sociedad/cienciavsreligion.html)   
  
SOLBES, J. (coord.), MARCO, D., TARÍN, F. y TRAVER, M. (2010). Ciencias para el mundo contemporáneo. Libro para el profesorado. Madrid, Ministerio de Educación.  
 [Ciencias para el mundo contemporáneo : libro para el profesorado](http://roderic.uv.es/handle/10550/46446)   
cc-by-nc  
  
 [Artículos Científicos ☑️ Curiosidades | CurioSfera-Ciencia.com](http://www.cienciasmc.es/)   
Web, wiki, descargas. Licenciamiento cc-by. Gobierno de Canarias. Francisco Martínez Navarro y Juan Carlos Turegano García  
  
 [Instituto Tecnológico de Canarias - Instituto Tecnológico de Canarias](http://www.itccanarias.org/web/difusion/recursos_didacticos/Energia/Documentacion/PDF/51891989-Ciencias-para-el-mundo-contemporaneo.pdf)  [Ciencias para el mundo contemporáneo by Agencia Canaria de Investigación, Innovación y Sociedad de la Información (ACIISI) - Issuu](https://issuu.com/cienciacanaria/docs/ciencias_para_el_mundo_contempor__n)   
**Ciencias para el Mundo Contemporáneo. Guía de Recursos Didácticos** (2010) es un libro on line para impartir la asignatura del mismo nombre en primer curso de bachillerato, elaborado por Francisco Martínez Navarro y Juan Carlos Turegano García. cc-by-nc-sa  
  
1º Bachillerato Curso 2013-2014  
 [indice.htm](http://ies.rayuela.mostoles.educa.madrid.org/Publicaciones/ApuntesCienciasMundoContemporaneo/indice.htm)   
  
NEFER: Física y Química  
 [blog-page_10.html](http://www.neferfisicayquimica.com/p/blog-page_10.html)   
En 2016 renombrado como Cultura Científica (sin aclarar si 4º ESO ó 1º Bachillerato) pero nombres de bloques son de CMC  
  
Ciencias para el mundo contemporáneo. 1º bachillerato. Bachillerato a distancia, CIDEAD  
 [16013](https://sede.educacion.gob.es/publiventa/ciencias-para-el-mundo-contemporaneo-1-bachillerato-bachillerato-a-distancia/bachillerato-ciencia/16013)   
Libro de pago, Unidad 1 muestra descargable  [PdfServlet](https://sede.educacion.gob.es/publiventa/PdfServlet?pdf=VP16013.pdf&area=E)   
  
