# Currículo Tecnología Industrial II 2º Bachillerato

Es una materia que desaparece con LOMLOE, donde hay una nueva materia en 2º Bachillerato "Tecnología e Ingeniería II"  

## LOE 
En la Comunidad de Madrid se desarrolla en [DECRETO 67/2008, de 19 de junio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Boletin_BOCM/2008/06/27/15200.pdf)  Página 68

### Contenidos 

1. Materiales.
— Estructura interna y propiedades de los materiales. Esfuerzos mecánicos.  
— Tipos de aleaciones metálicas.  
— Diagramas de equilibrios.  
— Técnicas de modificación de las propiedades: Tratamientos térmicos y tratamientos superficiales.  
— Proceso de oxidación y corrosión. Técnicas de protección.  
— Métodos de ensayo y medida de propiedades.  
— Procedimientos de reciclaje de materiales. Importancia social y económica de la reutilización de materiales.  
— Normas de precaución y seguridad en el manejo de materiales.  

2. Principios de máquinas.  
— Motores térmicos: Motores alternativos y rotativos. Descripción y principio de funcionamiento. Aplicaciones.  
— Motores eléctricos. Tipos. Principios generales de funcionamiento. Aplicaciones.  
— Circuito frigorífico y bomba de calor. Elementos. Principios de funcionamiento. Aplicaciones.  
— Energía útil. Potencia de una máquina. Par motor en el eje. Pérdidas de energía en las máquinas. Rendimiento.  

3. Sistemas automáticos.  
— Elementos que componen un sistema de control: Transductores, captadores, reguladores y actuadores. Diagramas de bloques.  
— Estructura de un sistema automático. Entrada, proceso, salida. Sistemas de lazo abierto. Sistemas realimentados de control. Comparadores. Función de transferencia. Respuesta dinámica. Estabilidad. Acciones básicas de control. Análisis de diseño automáticos de control sencillo. Montaje y experimentación de circuitos de control sencillos.  

4. Circuitos neumáticos y oleohidráulicos.  
— Técnicas de producción, conducción y depuración de fluidos. Caudal. Cálculo de fuerza y potencia. Pérdida de carga. Consumo de aire.
— Elementos de accionamiento, regulación y control. Simbología.  
— Circuitos característicos de aplicación. Interpretación de esquemas. Automatización de circuitos. Montaje e instalación de circuitos sencillos característicos.  

5. Control y programación de sistemas automáticos.  
— Tipos de señales y controles. Convertidores analógicos/digitales y digitales/analógicos. Captación y transmisión de datos.  
— Control analógico de sistemas. Circuitos lógicos combinacionales. Álgebra de Boole. Puertas y funciones lógicas. Procedimientos de simplificación de circuitos lógicos. Aplicación al control del funcionamiento de un dispositivo.  
— Circuitos lógicos secuenciales. Elementos. Diagrama de fases. Aplicación al control de un dispositivo de secuencia fija.  
— El ordenador como dispositivo de control. Ejemplo de simulación por ordenador.  
— Circuitos de control programado. Programación rígida y flexible. El microprocesador. El microcontrolador. El autómata programable.  Aplicación al control programado de un mecanismo. Estudio de un sistema de potencia por bloques.  

## LOMCE 

En la Comunidad de Madrid se desarrolla en [DECRETO 52/2015, de 21 de mayo, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato.](https://www.bocm.es/boletin/CM_Orden_BOCM/2015/05/22/BOCM-20150522-3.PDF) Página 83  
En LOMCE criterios de evaluación y estándares se fijan a nivel estatal en  [Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.](https://www.boe.es/boe/dias/2015/01/03/pdfs/BOE-A-2015-37.pdf) Página 360  

### Contenidos

Bloque 1. Materiales  
Estructura interna de los materiales.  
Propiedades de los materiales. Modificación de las propiedades.  
Materiales de última generación.  
Oxidación y corrosión. Tratamientos superficiales. Procedimientos de ensayo y medida.  
Procedimientos de reciclaje.  
Normas de precaución y seguridad en su manejo.  

Bloque 2. Principios de máquinas  
Elementos de máquinas. Condiciones de instalación.  
Motores térmicos: motores alternativos y rotativos. Aplicaciones.  
Motores eléctricos: tipos y aplicaciones.  
Circuito frigorífico y bomba de calor: elementos y aplicaciones.  
Energía útil. Potencia de una máquina. Par motor en el eje. Perdidas de energía en las  maquinas. Rendimiento.  

Bloque 3. Sistemas automáticos  
Elementos que conforman un sistema de control:  
Transductores  
Captadores  
Actuadores.  
Estructura de un sistema automático.  
Sistemas de lazo abierto.  
Sistemas realimentados de control. Comparadores.  
Experimentación en simuladores de circuitos sencillos de control.  
Técnicas de producción, conducción y depuración de fluidos.  
Elementos de accionamiento, regulación y control.  
Circuitos característicos de aplicación.  
Instrumentación asociada.  

Bloque 4. Circuitos y sistemas lógicos  
Circuitos lógicos combinacionales.  
Puertas y funciones lógicas.  
Procedimientos de simplificación de circuitos lógicos.  
Tipos: Multiplexores, decodificadores, circuitos aritméticos.  
Aplicación al control del funcionamiento de un dispositivo.  
Circuitos lógicos secuenciales.  
Biestables.  
Contadores.  
Registros.  
Memorias semiconductoras. Tipos.  
Instrumentación asociada.  

Bloque 5. Control y programación de sistemas automáticos  
Cronogramas de circuitos secuenciales.  
Programas de simulación de circuitos electrónicos: analógicos y digitales.  
Equipos de visualización y medida de señales.  
Técnicas de diseño de sistemas secuenciales.  
Microprocesadores y microcontroladores.  
Estructura interna.  
Evolución histórica.  
Aplicaciones.  

