
# Libros

**Nota inicial importante: si has llegado a esta página buscando libros para descargar, ese no es el objetivo de esta página. El objetivo es tener una relación de libros relacionados con Física y Química con sus datos (nivel, autor, editorial, ISBN...) y, solamente si existen/se conocen, incluir enlaces: el lugar enlazado desde aquí será el responsable de cumplir con el copyright (si es que el material lo tiene y si es que ese sitio almacena material y lo permite descargar)**  

Aparte de poner  [libros de texto](/home/recursos/libros/libros-de-texto)  de los que tradicionalmente se usan en el aula, intento poner libros en general relacionados con física y química  
En general no serán solo de física y química, sino también de ciencias.  
En general centrados en nivel pre universitario (Como soy profesor de secundaria, considero  [libros de texto](/home/recursos/libros/libros-de-texto)  los asociados a nivel ESO y Bachillerato: el resto de libros de física y química, con nivel universitario o más generales los pongo aquí)  
Además de libros tradicionales, creo un apartado sobre libros libres, sin copyright  
El concepto de libro está asociado a la idea antigua de editorial: ISBN, en papel ... puede haber "libros" electrónicos en internet, en html, agrupados o como grupos de pdf, no necesariamente en papel, pero ese tipo de recursos encajan más en  [apuntes](/home/recursos/apuntes).  
De momento es un cajón donde guardar un listado: con el tiempo la idea sería poner algún comentario sobre cada uno ...  
También es posible que haya libros citados en algún apartado concreto: por ejemplo libros de  [termodinámica](/home/recursos/termodinamica) , libros de  [relatividad](/home/recursos/fisica/recursos-fisica-relativista) ,  [prácticas / experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio)  ...

##  Libros con copyright
Siempre que es posible se intenta incluir el ISBN  
Siempre que es posible incluir una referencia a posible ebook, descarga parcial en googlebooks o en archive.org ... aunque no se ofrecen garantías sobre disponibilidad si no cumple el licenciamiento.  
Lo que nunca haré será albergar libros aquí, solamente se incluyen enlaces; es el lugar que lo alberga el responsable de cumplir con el copyright

## Otros buscadores y artículos científicos 
[Google Libros](https://books.google.es/) ofrece algunas previsualizaciones de libros.  
Sin ser libros, y sin salir a veces en buscadores como Google, se pueden ver artículos científicos y recursos en otros buscadores  
[Sci-Hub](https://www.sci-hub.st/)  


##  Libros generales física

   * Here’s How to Teach Yourself Physics and Math  
   [http://futurism.com/want-physicist-heres-guide/](http://futurism.com/want-physicist-heres-guide/)  
   Recopilación libros
   * Recopilación de Ángel Franco García  
   [http://www.sc.ehu.es/sbweb/fisica/Introduccion/fisica/biblio.htm](http://www.sc.ehu.es/sbweb/fisica/Introduccion/fisica/biblio.htm) 
   * [Física Para estudiantes desesperados. Volumen 1](https://zenodo.org/record/4630661#.YTDnb1uE5Ni)  
   This book is the first volume of a series.  
   It comprehends University Physics taught by the author in a 1 year freshman course for Engineering Physics students, at Universidad de Santiago de Chile.  
   Lautaro Vergara.  
   * Fundamentos de Física ; Raymond A. Serway, Jerry S. Faughn ; ISBN 970-686-375-3; vista previa disponible en [Google Libros](http://books.google.es/books?id=0NdVXzkxPmEC&printsec=frontcover&hl=es) 
   * FISICA FUNDAMENTOS Y APLICACIONES II, ROBERT M EISBERG , MCGRAW-HILL / INTERAMERICANA DE MEXICO, ISBN 9789684516342
   * FISICA CLASICA Y MODERNA, W.E. GETTYS , S.A. MCGRAW-HILL / INTERAMERICANA DE ESPAÑA, 1991, ISBN 9788476156353
   * FISICA PARA LA CIENCIA Y LA TECNOLOGIA (VOL. I) (6ª ED.), PAUL A. TIPLER; GENE MOSCA , REVERTE, 2010, ISBN 9788429144291
   * FISICA BASICA (T.1), ANTONIO FERNANDEZ-RAÑADA , ALIANZA EDITORIAL, 1993, ISBN 9788420606408
   * Física, Volumen 1: Mecánica (versión en español de "Fundamental University Physics, Volumen I: Mechanics"; Marcelo Alonso y Edward J. Finn; Editorial Addison-Wesley
   * Física General; S. Burbano de Ercilla, E. Burbano García, C. Gracia Muñoz; ISBN 978-84-95447-82-1; vista previa disponible en [Google Libros](http://books.google.es/books?id=BWgSWTYofiIC&lpg)  Hay ediciones anteriores "Física General" de S. Burbano de Ercilla. ISBN 84-7078-376-9, Depósito legal Z-1550-1986
   * Fisica Universitaria 11 Edicion Sears  
   https://archive.org/details/FisicaUniversitaria11EdicionSears (no disponible en 2020)  
   Se indica licenciamiento CC0 1.0 Universal, aunque está escaneado.  
   Fisica Universitaria 12va. Edicion Sears, Zemansky Vol. 1, Sears, Zemansky, ISBN 978-607-442-288-7  
   https://archive.org/details/FisicaUniversitaria12va.EdicionSearsZemanskyVol.1 (no disponible en 2020)  
   Fisica Universitaria 12va. Edicion Sears, Zemansky Vol. 2, , ISBN 978-607-442-304-4  
   https://archive.org/details/FisicaUniversitaria12va.EdicionSearsZemanskyVol.2 (no disponible en 2020)  
   Fisica Universitaria 12va. Edicion Sears, Zemansky Solucionario https://archive.org/details/FisicaUniversitaria12va.EdicionSearsZemanskySolucionario (no disponible en 2020)  
   Se puede descargar en pdf, epub ... Se indica licenciamiento CC0 1.0 Universal, aunque internamente indica "Adaptación autorizada de la edición en idioma inglés, titulada University Physics with Modern Physics 12ª ed., (capítulos 1-20) de Hugh D. Young, Roger A. Freedman; con la colaboración de A. Lewis Ford, publicada por Pearson Education, Inc., publicada como Addison-Wesley, **Copyright © 2008. Todos los derechos reservados.**"  
   * A veces se cita como "Young and Fredman"  
   University Physics with Modern Physics, 15th Edition  
   Hugh D Young, Carnegie Mellon University Roger A. Freedman, University of California, Santa Barbara  
   [https://www.pearson.com/us/higher-education/program/Young-Modified-Mastering-Physics-with-Pearson-e-Text-Standalone-Access-Card-for-University-Physics-with-Modern-Physics-15th-Edition/PGM2485469.html](https://www.pearson.com/us/higher-education/program/Young-Modified-Mastering-Physics-with-Pearson-e-Text-Standalone-Access-Card-for-University-Physics-with-Modern-Physics-15th-Edition/PGM2485469.html) 
   * Física para ciencias e ingeniería. SERWAY vol II (parte 4 electricidad y magnetismo, parte 5 luz y óptica, parte 6 física moderna) Derechos reservados McGrawHill, ISBN 970-10-3580-1 Obra completa, ISBN 970-10-3581-X Tomo I, ISBN 970-10-3582-8 Tomo II  
   [https://archive.org/details/S.e.r.w.a.y.-Vol2-Fisica.pdf](https://archive.org/details/S.e.r.w.a.y.-Vol2-Fisica.pdf) 
   * Física conceptual, Paul G. Hewitt, Pearson - Addison Wesley  
   [https://archive.org/details/FisicaHewitt](https://archive.org/details/FisicaHewitt) 
   * Física General, (C) Ignacio Martín Bragado. imartin@ele.uva.es, 12 febrero 2003  
   [http://fisicas.ucm.es/data/cont/media/www/pag-39686/fisica-general-libro-completo.pdf](http://fisicas.ucm.es/data/cont/media/www/pag-39686/fisica-general-libro-completo.pdf)  
   Incluye Bloque III Prácticas de laboratorio
   * Classical Mechanics; Herbert Goldstein, Columbia University, Charles P. Poole, Jr., University of South Carolina, John L. Safko, University of South Carolina; ISBN-10: 0201657023 • ISBN-13: 9780201657029, ©2002 • Addison-Wesley (Pearson)
   * Schaum's Outline of Theory and Problems of Physics for Engineering and Science (Schaum's Outlines), ISBN-13: 978-0070692541
   * Schaum's 3,000 Solved Problems in Physics (Schaum's Outlines), ISBN978-0071763462
   * Física para las ciencias de la vida; David Jou, Josep Enric Lllebot, Carlos Pérez; ISBN 84-481-1817-0
   * Mecánica para ingeniería: dinámica. Quinta Edición; Anthony Bedford, Wallace Fowler; PEARSON EDUCACIÓN, México, 2008 ISBN: 978-970-26-1278-0
   * Física Nuclear y de Partículas, Antonio Ferrer Soria, Universitat de València  
   1º Edición ISBN 84-370-5543-1, Depósito Legal V-591-2003 [https://books.google.es/books?id=CbEgyhh0y80C](https://books.google.es/books?id=CbEgyhh0y80C)  
   2ª Edición ISBN 9788437065687, Depósito Legal V-4897-2006  [https://books.google.es/books?id=zn0vUDrayO0C](https://books.google.es/books?id=zn0vUDrayO0C) 
   * Motion Mountain, The free physics textbook, An entertaining and free e-book on physics – the science of motion.  
   En inglés, algunas traducciones. [http://www.motionmountain.net/index.html](http://www.motionmountain.net/index.html)  
   Licenciamiento: "cc-by-nc-nd" con restricciones  [http://www.motionmountain.net/privacy.html](http://www.motionmountain.net/privacy.html)  
   Motion Mountain - The Adventure of Physics, is an e-book in pdf format that is copyright © 1990-2014 by Christoph Schiller and licensed under a cc-by-nc-nd, with the additional restriction that reproduction, distribution or use, in whole or in part, in any product or service, be it commercial or not, is not allowed without the written consent of the copyright owner.
   * INTRODUCTORY CLASSICAL MECHANICS * WITH PROBLEMS AND SOLUTIONS *; David Morin; Cambridge University Press
   * Cuestiones de Física (para los alumnos de primer curso de las facultades de ciencias y escuelas especiales); J. Aguilar, F. Senent; Ed. Reverté; ISBN: 84-291-4012-3 [https://books.google.es/books?id=VhgYuAy3Qu8C](https://books.google.es/books?id=VhgYuAy3Qu8C) 
   * Modern physics; Kenneth S. Krane; 3ª ed; JOHN WILEY & SONS, INC; ISBN 978-1-118-06114-5
   * 272 exámenes de física: resueltos y comentados : (primeros cursos de universidad); J. L. Torrent Franz,  [https://books.google.es/books?id=lWX1naEgN4EC](https://books.google.es/books?id=lWX1naEgN4EC) 


##  Libros problemas
Algunos se ponen dentro de subcategorías
   * 1000 problemas de física general; J.A. Fidalgo, M.R. Fernández; Ed Everest;  
   edición 8; ISBN 84-241-7603-0, Depósito legal LE 400-2001  
   edición 10; ISBN 978-84-241-7603-7
   * PROBLEMAS DE FISICA (27ª ED.), Ed Tebar, SANTIAGO BURBANO DE ERCILLA, ISBN 9788495447272
   * LA FÍSICA EN PROBLEMAS. Félix A. González. Editorial TEBAR FLORES, S.L. Albacete. 1995. ISBN 84-7360-141-6.
   * 100 PROBLEMAS DE MECANICA; VICTOR M. PEREZ GARCIA, LUIS VAZQUEZ MARTINEZ, ANTONIO FERNANDEZ-RAÑADA; ALIANZA EDITORIAL, 2008; ISBN 9788420686363
   * 100 PROBLEMAS DE ELECTROMAGNETISMO; Eloisa López Pérez, Felisa Núñez Cuber; Alianza Editorial, 1997; ISBN 9788420686356
   * 400 ejercicios resueltos de física universitaria, Gregorio Chenlo, ISBN 978-1073642359


##  Libros óptica
 Ver también  [recursos óptica](/home/recursos/fisica/recursos-optica) 
   * 100 PROBLEMAS DE OPTICA; Pedro M.Mejías, Rosario Martínez Herrero; Alianza editorial 1996; ISBN 9788420686325
   * Physics of Light and Optics  
   [http://optics.byu.edu/textbook.aspx](http://optics.byu.edu/textbook.aspx)  
   Libro descargable en pdf. Centrado en "óptica física" Brigham Young University, Department of Physics & Astronomy. Copyright Justin Peatross y Michael Ware
   * Fundamentals of Photonics  
   [http://spie.org/x17229.xml](http://spie.org/x17229.xml)  
   Conjunto de 10 "libros"/"módulos" que cubren óptica. Elaborado por SPIE "Internacional society for optics and photonics", inglés, varios autores por módulo. Licenciamiento no detallado pero indica "SPIE is providing **free and open access** to this material as a service to the optics community and the general public."


##  Libros termodinámica
 Ver también  [recursos termodinámica](/home/recursos/termodinamica) 
   * CURSO DE TERMODINAMICA, José Aguilar Peris, Editorial Pearson Educación, 1989, ISBN 9788420513829
   * 100 PROBLEMAS DE TERMODINAMICA; Julio Pellicer, José Antonio Manzanares; Alianza Editorial 1996; ISBN 9788420686318
   * Transferencia de calor; J.P. Holman, 8ª edición, 1ª en español; Editorial McGraw Hill; ISBN 84-481-2040-X


##  Libros generales química

   * Química, Décima edición; Raymond chang; Editorial McGrawHill; ISBN 978-607-15-0307-7 (ISBN edición anterior 970-10-6111-X, ISBN edición inglesa 978-007-351109-2
   * Principles of General Chemistry v. 1.0  
   [http://2012books.lardbucket.org/books/principles-of-general-chemistry-v1.0/index.html](http://2012books.lardbucket.org/books/principles-of-general-chemistry-v1.0/index.html)  
   Creative Commons
   * Química General; B.V. Nekrásov, Editorial MIR Moscú, Cuarta edición 1981
   * Química General; Petrucci, Harwood, Herring; Editorial Prentice Hall, Octava edición ISBN 84-205-3533-8
   * Química General: Principios y aplicaciones modernas; Ralph H. Petrucci / Geoffrey Herring / William Harwood; 10ª Edición; ISBN: 9788483228050
   * 1000 problemas de química general; José Antonio Fidalgo Sánchez y Manuel Ramón Fernández Pérez, Editorial Everest, Ed 11 ISBN 978-8424176044
   * Problemas y ejercicios de química general; N.L. Glinka, Editorial MIR Moscú / Editorial Latinoamericana, Primera reimpresión 1988
   * Química; Jerome L. Rosenberg, Lawrence M.Epstein, Peter J. Krieger; Colección SCHAUM, Editorial Mc Graw Hill; 9ª Edición, ISBN 978-970-10-6888-5, 2009
   * [La Química en problemas; J.M. Teijón, J.A. García, Y. Jiménez, I. Guerrero; Ed Tébar, ISBN 84-7360-226-9; 2006](https://books.google.es/books?id=f0VhOChqficC)  
   *  [http://www.juntadeandalucia.es/export/drupaljda/1337161060iniciacion_a_la_quimica_1.pdf](http://www.juntadeandalucia.es/export/drupaljda/1337161060iniciacion_a_la_quimica_1.pdf)  
INICIACIÓN A LA QUÍMICA. PREPARACIÓN PARA EL ACCESO A LA UNIVERSIDAD 
 Autores:  A. García Rodríguez. Catedrático de Química Inorgánica (Coordinador), M. García Vargas, Catedrático de Química Analítica (Coordinador), A. Navarrete Guijosa, Catedrático de Química Inorgánica (Coordinador), M. L. Quijano López, Prof. Titular de Química Orgánica (Coordinadora), P. Azuara Molina, Inspector de Educación, J.L. Ballesteros Olmo, Prof. Física y Química, C. Díaz Pérez de Perceval, Prof. Física y Química, M. Mayén Riego, Prof. Titular de Química Agrícola, J. A. Navío Santos, Prof. Titular de Química Inorgánica, J. Rincón González, Prof. Física y Química, P. Rodríguez Rubio, Catedrática de Escuela Universitaria   
 © Autores   
 Edita: Junta de Andalucia. Consejería de Innovación, Ciencia y Empresa. Distrito Único Andaluz.  
 I.S.B.N.: 978-84-8439-393-1
   * Química analítica cualitativa; Fernando Burriel Martí, Felipe Lucena Conde, Siro Arribas Jimeno; Ediciones Paraninfo 2001, ISBN 9788428312530  
   [https://books.google.es/books?id=QChYqMlUlL8C](https://books.google.es/books?id=QChYqMlUlL8C) 
   * Análisis químico cuantiativo 3ª Edición (sexta edición original); Daniel C. Harris; Editorial Reverté; ISBN 84-291-7224-6, Depósito Legal: B-29558-2006 (Edición original “Quantitative Chemical Analysis”, Sixth Edition, © 2003 W.H. Freeman and Company)  
   [https://books.google.es/books?id=H-_8vZYdL70C](https://books.google.es/books?id=H-_8vZYdL70C) 
   * Química física; Peter Atkins , Julio de Paula; Editorial Oxford University Press, 2006, ISBN 978-950-06-1248-7  
   [https://books.google.es/books?id=dVGP7pmCh10C](https://books.google.es/books?id=dVGP7pmCh10C) 
   * Shriver & Atkins' Inorganic Chemistry / Peter Atkins et al; Editorial McMillan; ISBN 978-0-19-923617-6  
   [https://books.google.es/books?id=tUmcAQAAQBAJ](https://books.google.es/books?id=tUmcAQAAQBAJ) 
   * Problemas resueltos de química para ingeniería; Ed.Paraninfo; ISBN 978-84-9732-293-5  
   [https://books.google.es/books?id=WB0E6Raux3oC](https://books.google.es/books?id=WB0E6Raux3oC) 
   * Resolución de problemas de química general; Editorial Reverté; Christopher J. WillisISBN: 9788429175264  
   [https://books.google.es/books?id=Llsd3VW7srYC](https://books.google.es/books?id=Llsd3VW7srYC) 
   * Resolución de problemas de química general; R. Nelson Smith,Conway Pierce,Miguel Gayoso Andrade  
   [https://books.google.es/books?id=sJDcZIWQ-RsC](https://books.google.es/books?id=sJDcZIWQ-RsC) 
   * Cálculos básicos en química física; Harold Eric Avery,D. J. Shaw  
   [https://books.google.es/books?id=hF1sJ4yG3VYC](https://books.google.es/books?id=hF1sJ4yG3VYC) 
   * Chemistry, The central science; Brown, LeMay, Bursten, Murphy; Prentice Hall; ISBN 978-0-321-69672-4
   * Problemas de química; Henry O. Daley,Robert F. O'Malley  
   [https://books.google.es/books?id=qniJqzwFzpYC ](https://books.google.es/books?id=qniJqzwFzpYC)  
   Second Edition  [https://books.google.es/books?id=LQwyfFc3pXsC](https://books.google.es/books?id=LQwyfFc3pXsC) 
   * Química: un proyecto de la American Chemical Society  
   [https://books.google.es/books?id=_FJ8ljXZD7IC](https://books.google.es/books?id=_FJ8ljXZD7IC) , Editorial Reverté, ISBN 978-84-291-7001-6
   * Química, 8ª edición, KENNETH W. WHITTEN, Ediciones Paraninfo, ISBN 9789706867988
   * Química, curso universitario; MAHAN, Editorial ADDISON-WESLEY; ISBN 9780201043730  
   [https://www.scribd.com/document/221684179/Mahan-quimica](https://www.scribd.com/document/221684179/Mahan-quimica)  ISBN 0-201-64419-3 
   * Problemas de química, Michell J. Sienko, ISBN 9788429174908
   * La química orgánica a través de ejercicios y problemas, T. A. Geissman, ISBN 9788420003757  
   * QUÍMICA CUÁNTICA. LA QUÍMICA CUÁNTICA EN 100 PROBLEMAS, LORNA ELIZABETH BAILEY CHAPMAN, MARIA DOLORES TROITIÑO NUÑEZ, ISBN 9788436266740
   * QUÍMICA CUÁNTICA, Ira N. Levine, ISBN 9788420530964  [books.google.es](https://books.google.es/books?id=jAgT7h4-7nsC)  
   


##  Química orgánica

   * QUÍMICA ORGÁNICA, Robert Thornton Morrison, Robert Neilson Boyd (Conocido como "Morrison y Boyd") 5ª edición, ISBN 968-444-340-4, 1998 de Addison Wesley Longman de México ( edición de 1985 es de Fondo Educativo Interamericano)  
   Análisis de la sexta edición  [http://pubs.acs.org/doi/abs/10.1021/ed069pA305.2](http://pubs.acs.org/doi/abs/10.1021/ed069pA305.2) 
   * Schaum Química orgánica, 3ª Ed; Herbert Meislich, Horward Nechamkin, Jacob Sharefkin, George Hademenos; Ed. McGraw Hill, ISBN: 958-41-0132-3, ISBN edición inglesa ISBN: 0-07-134165-X (SCHAUM'S OUTLINE OF THEORY AND PROBLEMS OF ORGANIC CHEMISTRY)
   * Organic Chemistry I for dummies, Arthur Winter, ISBN-13: 978-1118828076
   * Organic Chemistry I Workbook For Dummies, ISBN-13: 978-0470251515
   * Organic Chemistry II For Dummies, ISBN-13: 978-0470178157
   * Virtual Textbook of Organic Chemistry  
   [http://www2.chemistry.msu.edu/faculty/reusch/VirtTxtJml/intro1.htm](http://www2.chemistry.msu.edu/faculty/reusch/VirtTxtJml/intro1.htm)  
   This work is licensed under a Creative Commons License. Virtual Text of Organic Chemistry 1999 An interactive textbook covering the usual topics treated in a college sophomore-level course. Links are offered to advanced discussions of selected topics. William Reusch 
   * Tratado de Química Orgánica (Tomo I, 1ª parte) Química Orgánica Sistemática; F. Klages; Editorial Reverté; ISBN 84-291-7314-5Volumen 1, ISBN 84-291-7311-0 Tomo 1, [https://books.google.es/books?id=nhcGf55Gy58C](https://books.google.es/books?id=nhcGf55Gy58C) 
   * Tratado de Química Orgánica (Tomo I, 2ª parte) Química Orgánica Sistemática; F. Klages; Editorial Reverté; ISBN 84-291-7315-3 Volumen 2, ISBN 84-291-7311-0 Tomo 1, [https://books.google.es/books?id=5kJGwEBq5EoC](https://books.google.es/books?id=5kJGwEBq5EoC) 
   * Reacciones modernas de síntesis orgánica, Herbert.O. House, Editorial Reverté
   * PROBLEMAS RESUELTOS DE QUIMICA ORGANICA; FRANCISCO GARCIA CALVO-FLORES, Ediciones PARANINFO, 2007; ISBN 9788497324588
   * Química Orgánica (200 ejercicios resueltos); Márquez Salamanca, Cecilio; Universidad de Alicante. Departamento de Química Orgánica: [http://rua.ua.es/dspace/handle/10045/6984](http://rua.ua.es/dspace/handle/10045/6984) 
   * QUIMICA ORGANICA, Peter Voolhard, ISBN 978-8428214315


##  Libros generales ciencia / matemáticas en ciencia

   * Probability Theory: The Logic of Science by E. T. Jaynes
   * Tres primeros capítulos publicados online [http://bayes.wustl.edu/etj/prob/book.pdf](http://bayes.wustl.edu/etj/prob/book.pdf) 


##   [Libros de texto](/home/recursos/libros/libros-de-texto) 


##  Libros problemas selectividad

   * 1989 (COU, Anaya, ISBN 84-207-3648-1)
   * Selectividad COU Física Pruebas de 1990; Agustín Candel, José Satoca, Juan Bautista Soler, Juan José Tent; Editorial Anaya; ISBN 84-207-4037-3; Depósito Legal: M.46.491-1990
   * 1991 (COU, Anaya, ISBN 84-207-4542-1)
   * Pruebas de selectividad física, COU-Bachillerato LOGSE Distrito Universitario de Madrid COU (1995-1996), B.LOGSE (1996), Jacinto Soriano, editorial McGraw-Hill, Depósito legal: M.15.830-1998, ISBN 84-481-0904-X
   * Pruebas de selectividad física, COU-Bachillerato LOGSE Junio 1997 , Jacinto Soriano, editorial McGraw-Hill, Depósito legal: M.15.830-1998 (anexo al ISBN 84-481-0904-X)
   * 1998 (Anaya, ISBN 84-207-8389-7)
   * Selectividad COU Física Pruebas de 1997; José Satoca, Juan F. Dalmau Flores; Editorial Anaya; ISBN 84-207-8032-4; Depósito Legal: M.1.847-1998
   * Selectividad COU Física Pruebas de 1998; José Satoca, Juan F. Dalmau Flores; Editorial Anaya; ISBN 84-207-8389-7; Depósito Legal: M.995-1999
   * Selectividad LOGSE Física Pruebas de 1998; María Luz García Álvarez, María Paz Platero Muñoz; Editorial Anaya, ISBN 84-207-8863-5, Depósito Legal: M.1.817-1999
   * Selectividad COU Física Pruebas de 1999; José Satoca, Juan F. Dalmau Flores; Editorial Anaya; ISBN 84-207-9282-9; Depósito Legal: B.664-2000
   * Selectividad LOGSE Física Pruebas de 1999; María Luz García Álvarez, María Paz Platero Muñoz; Editorial Anaya, ISBN 84-207-9300-0, Depósito Legal: B.675-2000
   * Selectividad COU Física Pruebas de 2000; José Satoca, Juan F. Dalmau Flores; Editorial Anaya; ISBN 84-667-0021-8; Depósito Legal: B.51.789-2000
   * Selectividad LOGSE Física Pruebas de 2000; María Luz García Álvarez, María Paz Platero Muñoz; Editorial Anaya, ISBN 84-667-0012-9, Depósito Legal: B.48.552-2000
   * Selectividad LOGSE Física Pruebas de 2001; María Luz García Álvarez, María Paz Platero Muñoz; Editorial Anaya, ISBN 84-667-1302-6, Depósito Legal: B.44.308-2001
   * Selectividad Física Pruebas de 2002; María Luz García Álvarez, María Paz Platero Muñoz; Editorial Anaya; ISBN 84-667-2298-X; Depósito Legal: B.3.444-2003
   * Selectividad Física Pruebas de 2005; María Luz García Álvarez, María Paz Platero Muñoz; Editorial Anaya; ISBN 84-667-5020-7; Depósito Legal: B.653-2006
   * Relación de pruebas de acceso a la Universidad, Bliblioteca del profesorado, Preparación de la selectividad Física, Proyecto la casa del saber; Editorial Santillana, ISBN 978-84-294-7781-8, Depósito Legal: M-21935-2008


##  Libros formulación/nomenclatura
 Tanto inorgánica como orgánica. En general información de libros impresos adicional a recursos que aparecen en la  [página de recursos de formulación](/home/recursos/quimica/formulacion)  que son recursos IUPAC o electrónicos.  
 
   * NOMENCLATURA Y REPRESENTACION DE LOS COMPUESTOS ORGANICOS (En papel)  
   EMILIO QUIÑOA CABANA , MCGRAW-HILL / INTERAMERICANA DE ESPAÑA, S.A., 2005 ISBN 9788448143633 
   * "Nomenclatura y representación de los compuestos inorgánicos. Una guía de estudio y autoevaluación".  
   Emilio Quiñoa Cabana y Ricardo Riguera Vega, José Manuel Vila, 2ª edición ISBN: 84-481-4625-5, 2005 Colección Schaum; Editorial McGraw Hill 
   * "Nomenclatura y representación de los compuestos orgánicos. Una guía de estudio y autoevaluación". E. Quiñoa Cabana y R. Riguera Vega, 2ª edición ISBN: 84-481-4363-9, 2005 Colección Schaum; Editorial McGraw Hill
   * "Química orgánica, tercera edición"; Herbert Meislich, Howard Nechamkin, Jacob Sharefkin, George Hademenos; Editorial Schaum; ISBN: 958-41-0132-3 (ISBN edición inglesa ISBN: 0-07-134165-X)
   * "Nomenclatura y formulación química ESO, Química inorgánica, Química orgánica"; Editorial Bruño; ISBN 978-84-216-5723-2
   * "Nomenclatura y formulación química Bachillerato, Química inorgánica, Química orgánica"; Editorial Bruño; ISBN 978-84-216-5724-9-2
   * "Química del carbono, Nomenclatura y formulación (normas de la IUPAC 1993)"; Mariano Latorre Ariño, Ana Isabel Villa Ávila; Editorial Edelvives; ISBN 978-84-263-8909-1, Deposito Legal Z-450-2013
   * "Química inorgánica, Nomenclatura y formulación (normas de la IUPAC2005 )"; Mariano Latorre Ariño; Editorial Edelvives; ISBN 978-84-263-8908-4, Deposito Legal Z-449-2013
   * "Formulación inorgánica. Mejora del aprendizaje"; María del Carmen Vidal Fernández; Editorial Santillana; ISBN 978-84-680-8691-0 Depósito legal M-10023-2015  
   *Libro asociado a LOMCE, recibida muestra en el centro en mayo 2015*
   * "Formulación orgánica. Mejora del aprendizaje"; María del Carmen Vidal Fernández; Editorial Santillana; ISBN 978-84-680-8689-7 Depósito legal M-10022-2015  
   *Libro asociado a LOMCE, recibida muestra en el centro en mayo 2015*
   * "Formulación y nomenclatura químicas. Física y Química. 1 Bachillerato"; María del Carmen Vidal Fernández; Editorial Santillana  
   *Libro asociado a LOMCE, recibida muestra en el centro en mayo 2015, sin ISBN (EAN 8431300987999 (939445))*
   * [Nomenclatura de química orgánica; Francisco González Alcaraz; Universidad de Murcia, 1991, ISBN 84-7684-242-2](https://books.google.es/books?id=Q_Z8RrSUuu8C) 
   * [Nomenclatura de Química Inorgánica. Torredonjimeno: Madara, 2020. 202 p. ISBN: 978-84-09-22363-3. fisicayquimica.eu](https://www.fisicayquimica.eu/p/mis-libros.html) 
   * [Nomenclatura en Química Inorgánica ISBN 9788428355445, 2022](https://www.paraninfo.es/catalogo/9788428355445/nomenclatura-en-quimica-inorganica)  


##  Libros divulgación / lectura científica
 A veces vienen bien algunos libros que sin ser de texto aportan una lectura interesante: se pueden usar extractos en clase, o dar referencias a alumnos que los piden. Algunos pueden ser novelas que aproximan a conceptos científicos. El tema de ciencia ficción / hard sci-fi como género es aparte.
 
   * Una breve historia de casi todo, Bill Bryson  
   Libro divulgativo escrito por un no científico que suele escribir libros de viajes. Muy ameno
   * Los descubridores, Daniel J. Boorstin  
   Libro escrito por un erudito, director de bibliotecas. Repasa historia de descubrimientos, muy instructivo aunque no es un libro científico.
   * El Breviario del Señor Tompkis, George Gamov
   * 100 preguntas básicas sobre ciencia, Isaac Asimov  
   [http://www.librosmaravillosos.com/cienpreguntas/index.html](http://www.librosmaravillosos.com/cienpreguntas/index.html) 
   * Historia de la ciencia sin los trozos aburridos, Ian Crofton. Editorial Ariel, 2011, ISBN 9788434469587  
   [http://books.google.es/books/about/Historia_de_la_ciencia_sin_los_trozos_ab.html?id=LaFz-8GG0rsC](http://books.google.es/books/about/Historia_de_la_ciencia_sin_los_trozos_ab.html?id=LaFz-8GG0rsC) 
   * ¿POR QUE EL CIELO ES AZUL?: LA CIENCIA PARA TODOS; Javier Fernández Panadero, ISBN 9788495642349
   * ¿POR QUE LA NIEVE ES BLANCA?: LA CIENCIA PARA TODOS; Javier Fernández Panadero; ISBN 9788495642646
   * Como Einstein por su casa, Javier Fernández Panadero; [http://francis.naukas.com/2017/12/02/resena-como-einstein-por-su-casa-de-javier-fernandez-panadero/](http://francis.naukas.com/2017/12/02/resena-como-einstein-por-su-casa-de-javier-fernandez-panadero/)  [http://paginasdeespuma.com/wp-content/files_mf/1510573933PANADERO_EINSTEIN_I_Extracto.pdf](http://paginasdeespuma.com/wp-content/files_mf/1510573933PANADERO_EINSTEIN_I_Extracto.pdf) 
   * LOS CIENTIFICOS Y SUS LOCOS EXPERIMENTOS; Mike Goldsmith; ISBN 9788496751828
   * La física de lo imposible, Michio Kaku  
   [https://estudiarfisica.com/2010/06/07/fisica-de-lo-imposible-michio-kaku/](https://estudiarfisica.com/2010/06/07/fisica-de-lo-imposible-michio-kaku/)  
   [https://www.youtube.com/watch?v=yDtNWKz_Bqs](https://www.youtube.com/watch?v=yDtNWKz_Bqs) 
   * Hackers del espacio, Arturo Quirantes Sierra  
   Lea y descubra cómo la ISEE-3 se convirtió en el objetivo del primer intento privado de controlar una sonda interplanetaria. Sea testigo de cómo una sonda sin ordenador ni baterías, que ha recorrido más distancia que cualquier otra nave espacial en activo, fue despertada y recuperada para la investigación científica. Un pequeño consejo: recuerde que este libro no está basado en una historia real. Este libro es una historia real. Entre y disfrute este relato del mayor hacking espacial de la Historia. Bienvenidos a la era de la ciencia de cohetes ciudadana.
   * Historias Curiosas de la Ciencia, Cyril Aydon [https://books.google.es/books?id=_kErdGZ7ZZoC](https://books.google.es/books?id=_kErdGZ7ZZoC) 
   * 101 obras esenciales de divulgación científica, Jerónimo Tristante, DanielTorregrosa, [http://www.redbibliotecas.carm.es/RedBibliotecas/faces/noticia.jsp?idioma&pagina&id=2544](http://www.redbibliotecas.carm.es/RedBibliotecas/faces/noticia.jsp?idioma&pagina&id=2544)  [http://estaticocultura.carm.es/wbr/home/FIC20191204_021226.pdf](http://estaticocultura.carm.es/wbr/home/FIC20191204_021226.pdf) 
   * El Científico Que Derrotó A Hitler Y Otros Ensayos Sobre La Historia De La Ciencia, Alejandro Navarro Yáñez
   * ¡Que se le van las vitaminas!: Mitos y secretos que solo la ciencia puede resolver, Deborah García Bello
   * [YO QUIERO SER CIENTIFICO. Libro de divulgación que te animará y ayudará a decidir que científico quieres ser.](http://yoquierosercientifico.blogspot.com/)  Cada capítulo escrito por un científico. Descargable en pdf     
   * [LA RIDÍCULA IDEA DE NO VOLVER A VERTE - rosamontero.es](https://www.rosamontero.es/novela-ridicula-idea-no-verte.html)  
> Cuando Rosa Montero leyó el maravilloso diario que Marie Curie comenzó tras la muerte de su esposo, y que se incluye al final de este libro, sintió que la historia de esa mujer fascinante que se enfrentó a su época le llenaba la cabeza de ideas y emociones.   
   * [100 preguntas básicas sobre la ciencia, Isaac Asimov](http://www.gaiaciencia.com/2013/12/100-preguntas-basicas-sobre-la-ciencia/)  
   * [Respuestas sorprendentes a preguntas cotidianas, Jordi Pereyra](https://www.planetadelibros.com/libro-respuestas-sorprendentes-a-preguntas-cotidianas/305004)  
   * [ ¡EUREKA!: 50 DESCUBRIMIENTOS CIENTIFICOS QUE CAMBIARON AL MUNDO, ROCIO VIDAL](https://www.casadellibro.com/libro-eureka-50-descubrimientos-cientificos-que-cambiaron-al-mundo/9788417809775/11853786)   
   * [Cerca de las estrellas,  F. Fernández Peláez](https://editorialcirculorojo.com/31940/)  
   * [Isaac Asimov: La sensación de poder. Cuento](https://estoespurocuento.wordpress.com/2013/11/13/isaac-asimov-la-sensacion-de-poder-cuento/)  
   * [El asesinato de la profesora de ciencias, Jordi Sierra i Fabra. ISBN 9788467860597](https://madread.educa.madrid.org/info/el-asesinato-de-la-profesora-de-ciencias-00008456)    

Recopilaciones:  
[twitter EugenioManuel/status/1335295800142913536](https://twitter.com/EugenioManuel/status/1335295800142913536)   
Va una lista de los libros de divulgación científica en español de 2020 que merecen la pena. #LibrosDivulgaciónCientífica2020 Ha sido un año muy prolífico, así que dejad vuestro título y lo voy añadiendo en estos días, poco a poco. Si quieres una reseña, enviadme vuestros libros

[Revistas Investigación y Ciencia archivo completo 1976/2023](https://fisicamartin.blogspot.com/2024/02/revistas-investigacion-y-ciencia.html)  



##  Divulgación Física

   * De Arquímedes a Einstein: Los diez experimentos más bellos de la física, Manuel Lozano Leyva
   * [Física Recreativa I y II, Yakov I. Perelman - librosmaravillosos.com](http://www.librosmaravillosos.com/fisicarecreativa1/index.html) 
   * Física recreativa, Yakov Perelman, Editorial MIR Moscú, "Libro 1" y "Libro 2", traducción al español 1975 por ingeniero Antonio García Molina, Impreso en URSS, sin ISBN
   * Professor Povey's Perplexing Problems: Pre-University Physics and Maths Puzzles with Solutions
   * Conversaciones de Física con mi perro (How to teach physics to your dog), Chad Orzel, [https://books.google.es/books?id=o5z2CQAAQBAJ](https://books.google.es/books?id=o5z2CQAAQBAJ) 
   * Física de las noches estrelladas: Astrofísica, relatividad y cosmología, Eduardo Battaner
   * La puerta de los tres cerrojos, Sonia Fernández-Vidal [https://lapuertadelostrescerrojos.planetadelibros.com/](https://lapuertadelostrescerrojos.planetadelibros.com/) 
   * [Quantic love, Sonia Fernández-Vidal](http://quanticlove.com/index_sp.html) 
   * [Reseña Desayuno con partículas, Sonia Fernández-Vidal - cuentos-cuanticos.com _waybackmachine_](https://cuentos-cuanticos.com/2013/05/31/resena-desayuno-con-particulas/)   
   * [Quantum Space: Loop Quantum Gravity and the Search for the Structure of Space, Time, and the Universe, Jim Baggott](http://www.jimbaggott.com/books/quantum-space/)  


##  Divulgación química

   * La cuchara menguante (y otros relatos veraces de locura, amor y la historia del mundo a partir de la tabla periódica de los elementos), Sam Kean  
   Libro divulgativo de 2010, muy ameno. Traducción al español 2011 de editorial Ariel ISBN 978-84-344-1364-1
   * Tío tungsteno, Oliver Sacks  
   Libro sobre química, autobiográfico y muy entretenido. Oliver Sacks es más conocido por lo que ha hecho tras ser químico: es el doctor protagonista de la historia real de "Despertares"
   * Breve historia de la química, Isaac Asimov [http://www.librosmaravillosos.com/brevehistoriaquimica/index.html](http://www.librosmaravillosos.com/brevehistoriaquimica/index.html) 
   * Del mito al laboratorio, Daniel Torregrosa [https://naukas.com/2018/12/05/del-mito-al-laboratorio/](https://naukas.com/2018/12/05/del-mito-al-laboratorio/) 
   * Eso no estaba en mi libro de Historia de la Química, Alejandro Navarro [https://francis.naukas.com/2019/05/27/resena-eso-no-estaba-en-mi-libro-de-historia-de-la-quimica-por-alejandro-navarro/](https://francis.naukas.com/2019/05/27/resena-eso-no-estaba-en-mi-libro-de-historia-de-la-quimica-por-alejandro-navarro/) 
   * El Secreto de Prometeo y otras Historias sobre la Tabla Periódica de los Elementos, Alejandro Navarro Yáñez
   * El sistema periódico, Primo Levi [http://www.librosmaravillosos.com/elsistemaperiodico/index.html](http://www.librosmaravillosos.com/elsistemaperiodico/index.html) 
   * [Reseña: «Todo es cuestión de química» de Deborah García Bello - francis.naukas.com](http://francis.naukas.com/2016/03/05/resena-todo-es-cuestion-de-quimica-de-deborah-garcia-bello/) 
   * Mi vida es química (Móviles, café, emociones... Cómo puedes explicar todo con química), Mai Thi Nguyen-Kim
   * Guía de Venenos Mortíferos de Agatha Christie, Kathryn Harkup
   * La tabla periódica, Hugh Aldersey-Williams [https://www.xatakaciencia.com/libros-que-nos-inspiran/libros-que-nos-inspiran-la-tabla-periodica-de-hugh-aldersey-williams](https://www.xatakaciencia.com/libros-que-nos-inspiran/libros-que-nos-inspiran-la-tabla-periodica-de-hugh-aldersey-williams) 
   * Tortilla quemada - 23 raciones de quimica cotidiana, Claudi Mans
   * El Elemento Del Que Solo Hay Un Gramo, Sergio Parra Castillo
   * Cómo fueron descubiertos los elementos químicosm D.N. Trífonov y V.D. Trífonov
   * Había una vez el átomo, Gabriel Gellon
   * La química es la cuestión: El orden de la vida y el caos que llevamos encima, Helena González Burón
   * Química entre nosotros, Larry Young, Brian Alexander, et ál.

##  Ciencia ficción 

  Mi autor favorito es Greg Egan, que tiene los primeros capítulos de sus libros de acceso libre en inglés en su web [gregegan.net/](http://www.gregegan.net/). Cito algunos: cuarentena, el instante aleph, aparte de recopilación de relatos cortos como axiomático. 
  
  Referentes de hard sci-fi: 
  * Huevo de dragón y su secuela Estrellamoto, de Robert L. Forward  

##  Colección de RBA "Un paseo por el Cosmos"
Colección editorial RBA de libros divulgación, muchos con  [reseñas de Francisco R. Villatoro](http://francis.naukas.com/tag/resena/).  
La primera edición de la colección fue en 2015-2016 y la segunda en 2016-2017, con variaciones entre ellas.  
Según web en 2017  [Un paseo por el cosmos - rbacoleccionables.com _waybackmachine_](http://web.archive.org/web/20170606043645/http://tienda.rbacoleccionables.com/un-paseo-por-el-cosmos.html) son 70 libros (69 entregas ya que entrega 2 eran 2 libros)  
* Entrega 1: La materia oscura, Alberto Casas.  [http://francis.naukas.com/2015/09/19/resena-la-materia-oscura-por-alberto-casas/](http://francis.naukas.com/2015/09/19/resena-la-materia-oscura-por-alberto-casas/)  
* Entrega 2: Los agujeros negros, Antxon Alberdi.  [http://francis.naukas.com/2015/10/24/resena-los-agujeros-negros-de-antxon-alberdi/](http://francis.naukas.com/2015/10/24/resena-los-agujeros-negros-de-antxon-alberdi/)  
* Entrega 2bis: Espacio-tiempo cuántico, Arturo Quirantes  [http://francis.naukas.com/2015/11/21/resena-espacio-tiempo-cuantico-de-arturo-quirantes/](http://francis.naukas.com/2015/11/21/resena-espacio-tiempo-cuantico-de-arturo-quirantes/)  
* Entrega 3: El bosón de Higgs, David Blanco.  [http://francis.naukas.com/2015/10/31/resena-el-boson-de-higgs-de-david-blanco/](http://francis.naukas.com/2015/10/31/resena-el-boson-de-higgs-de-david-blanco/)  
* Entrega 4: Universos paralelos, José Rodríguez-Quintero  [http://francis.naukas.com/2015/10/31/resena-el-boson-de-higgs-de-david-blanco/](http://francis.naukas.com/2015/10/31/resena-el-boson-de-higgs-de-david-blanco/)  
* Entrega 5: El vacío y la nada, Enrique F. Borja.  [http://francis.naukas.com/2015/12/26/resena-el-vacio-y-la-nada-de-enrique-f-borja/](http://francis.naukas.com/2015/12/26/resena-el-vacio-y-la-nada-de-enrique-f-borja/)  
* Entrega 6: Los neutrinos, Juan Antonio Caballero.  [http://francis.naukas.com/2016/01/02/resena-los-neutrinos-de-juan-antonio-caballero/](http://francis.naukas.com/2016/01/02/resena-los-neutrinos-de-juan-antonio-caballero/)  
* Entrega 7: La flecha del tiempo, David Blanco  [http://francis.naukas.com/2016/02/13/resena-la-flecha-del-tiempo-de-david-blanco/](http://francis.naukas.com/2016/02/13/resena-la-flecha-del-tiempo-de-david-blanco/)  
* Entrega 8: Grandes estructuras del universo, Eduardo Battaner.  [http://francis.naukas.com/2016/01/30/resena-grandes-estructuras-del-universo-de-eduardo-battaner/](http://francis.naukas.com/2016/01/30/resena-grandes-estructuras-del-universo-de-eduardo-battaner/)  
* Entrega 9: Las constantes universales, Jesús Navarro.  [http://francis.naukas.com/2016/02/27/resena-las-constantes-universales-de-jesus-navarro/](http://francis.naukas.com/2016/02/27/resena-las-constantes-universales-de-jesus-navarro/)  
* Entrega 10: El Big Bang y el origen del universo, Antonio M. Lallena.  [http://francis.naukas.com/2016/02/27/resena-el-big-bang-y-el-origen-del-universo-de-antonio-m-lallena/](http://francis.naukas.com/2016/02/27/resena-el-big-bang-y-el-origen-del-universo-de-antonio-m-lallena/)  
* Entrega 11: Simetría y supersimetría, Francisco Pérez-Bernal.  [http://francis.naukas.com/2016/03/26/resena-simetria-y-supersimetria-de-francisco-perez-bernal/](http://francis.naukas.com/2016/03/26/resena-simetria-y-supersimetria-de-francisco-perez-bernal/)  
* Entrega 12: La evolución del universo, David Galadí-Enríquez.  [http://francis.naukas.com/2016/04/23/resena/](http://francis.naukas.com/2016/04/23/resena/)  
* Entrega 13: El modelo estándar de partículas, Mario E. Gómez Santamaría.  [http://francis.naukas.com/2016/07/02/resena-el-modelo-estandar-de-particulas-de-mario-e-gomez-santamaria/](http://francis.naukas.com/2016/07/02/resena-el-modelo-estandar-de-particulas-de-mario-e-gomez-santamaria/)  
* Entrega 14: La nanotecnología, Antonio Costa.  [http://francis.naukas.com/2017/01/28/resena-la-nanotecnologia-de-antonio-acosta/](http://francis.naukas.com/2017/01/28/resena-la-nanotecnologia-de-antonio-acosta/)  
* Entrega 15: El final del universo  
* Entrega 16: La teoría del caos, Alberto Pérez Izquierdo.  [http://francis.naukas.com/2017/04/01/resena-la-teoria-del-caos-de-alberto-perez/](http://francis.naukas.com/2017/04/01/resena-la-teoria-del-caos-de-alberto-perez/)  
* Entrega 17: La posibilidad de viajar en el tiempo  
* Entrega 18: El origen de la vida en la Tierra, Antonio Aguilera Mochón.  [http://francis.naukas.com/2016/10/22/resena-origen-la-vida-la-tierra-juan-antonio-aguilera-mochon/](http://francis.naukas.com/2016/10/22/resena-origen-la-vida-la-tierra-juan-antonio-aguilera-mochon/)  
* Entrega 19: Los rayos cósmicos  
* Entrega 20: Ciencia y consciencia, Eduardo Arroyo.  [http://francis.naukas.com/2016/10/01/resena-ciencia-y-consciencia-de-eduardo-arroyo/](http://francis.naukas.com/2016/10/01/resena-ciencia-y-consciencia-de-eduardo-arroyo/)  
* Entrega 21: La evolución estelar  
* Entrega 22: El sistema solar  
* Entrega 23: Los exoplanetas, Arturo Quirantes.  [http://francis.naukas.com/2016/10/01/resena-los-exoplanetas-arturo-quirantes/](http://francis.naukas.com/2016/10/01/resena-los-exoplanetas-arturo-quirantes/)  
* Entrega 24: El frío absoluto, Bruno Juliá Díaz.  [http://francis.naukas.com/2017/05/27/resena-frio-absoluto-bruno-julia-diaz/](http://francis.naukas.com/2017/05/27/resena-frio-absoluto-bruno-julia-diaz/)  
* Entrega 25: La vida no terrestre  
* Entrega 26: Quarks y gluones  
* Entrega 27: La materia extrema, Enrique Ruíz Arriola.  [http://francis.naukas.com/2017/02/18/resena-la-materia-extrema-de-enrique-ruiz-arriola/](http://francis.naukas.com/2017/02/18/resena-la-materia-extrema-de-enrique-ruiz-arriola/)  
* Entrega 28: Cuerdas y supercuerdas, José Edelstein y Gastón Giribet.  [http://francis.naukas.com/2017/01/21/resena-cuerdas-y-supercuerdas-de-edelstein-y-giribet/](http://francis.naukas.com/2017/01/21/resena-cuerdas-y-supercuerdas-de-edelstein-y-giribet/)  
* Entrega 29: La energía de las estrellas  
* Entrega 30: Grandes experimentos de la física (el título no orienta sobre su contenido: tiene 4 partes que son proyectos de fusión, LIGO, LHC y detección de neutrinos)  
* Entrega 31: La realidad cuántica  
* Entrega 32: De la simplicidad a la complejidad  
* Entrega 33: Las partículas elementales  
* Entrega 34: El principio antrópico  
* Entrega 35: Información y entropía  
* Entrega 36: La evolución  
* Entrega 37: Supernovas: la muerte de las estrellas  
* Entrega 38: La exploración y colonización del espacio  
* Entrega 39: La Tierra  
* Entrega 40: La gravedad cuántica  
* Entrega 41: La nucleosíntesis  
* Entrega 42: La física de la luz  
* Entrega 43: El universo holográfico  
* Entrega 44: Computación, teleportación y criptografía cuánticas  
* Entrega 45: Inteligencia artificial  
* Entrega 46: La observación de la Tierra desde el espacio  
* Entrega 47: El Sol  
* Entrega 48: La observación del espacio  
* Entrega 49: ¿Por qué hay algo en lugar de nada?  
* Entrega 50: La biofísica  
* Entrega 51: El agua en el cosmos  
* Entrega 52: La radiación de fondo de microondas  
* Entrega 53: Los robots del futuro  
* Entrega 54: Las galaxias  
* Entrega 55: Ciencia en el espacio  
* Entrega 56: La presencia humana más allá del sistema solar  
* Entrega 57: Robots en el espacio  
* Entrega 58: Los límites del universo  
* Entrega 59: Climatología planetaria  
* Entrega 60: Superconductividad y superfluidez  
* Entrega 61: El cielo nocturno  
* Entrega 62: Los límites de la computación  
* Entrega 63: Grandes moléculas en el cosmos  
* Entrega 64: Los fósiles cósmicos  
* Entrega 65: Las nubes y el polvo cósmico  
* Entrega 66: La paradoja de la física  
* Entrega 67: Más allá del modelo estándar de partículas  
* Entrega 68: Nuevas estructuras materiales  
* Entrega 69: Los cuerpos celestes extraños

##  Libros divulgación mecánica cuántica

   * Encuentros y conversaciones con Einstein y otros ensayos, Werner Heisenberg ISBN 84-206-1719-9, ALIANZA EDITORIAL 1980.
   * Entrelazamiento, El mayor misterio de la física; Amir D. Aczel ISBN 9788408008590


##  Libros consulta online

   * [La Ciencia para Todos - bibliotecadigital.ilce.edu.mx](http://bibliotecadigital.ilce.edu.mx/sites/ciencia/menu.htm)  
   Libros astronomía, biología, física, química .... Libros online que tienen su ISBN y copyright. La Ciencia para Todos es proyecto y propiedad del Fondo de Cultura Económica, al que pertenecen también sus derechos. Se publica con los auspicios de la Secretaria de Educación Pública y del Consejo Nacional de Ciencia y Tecnología. 
   *  [http://catalog.flatworldknowledge.com/](http://catalog.flatworldknowledge.com/)  
   Written by the industry's top authors, our textbooks are helping improve teaching and learning at more than 2,500 leading colleges and universities worldwide.  
   [http://catalog.flatworldknowledge.com/catalog/disciplines/21/titles?sort=discipline](http://catalog.flatworldknowledge.com/catalog/disciplines/21/titles?sort=discipline)  En octubre 2015 3 libros de química


##  Libros libres
Ver [Librería Ministerio de Educación](https://www.libreria.educacion.gob.es/)  
Con libros y recursos de descarga gratuita, con [catálogo](https://www.libreria.educacion.gob.es/catalogo/) por [colecciones](https://www.libreria.educacion.gob.es/colecciones/) y [materias](https://www.libreria.educacion.gob.es/materias)   

 Ver también  [REA (Recursos Educativos Abiertos)](/home/recursos/recursos-educativos-abiertos) 
   *  [Libros ciencia - openstax.org](https://openstax.org/subjects/ciencia)  © 1999-2024, Rice University. Salvo que se indique lo contrario, los libros de texto de este sitio están autorizados conforme a la Creative Commons Attribution 4.0 International License.    
   *  [Physics Library. LibreTexts project](https://phys.libretexts.org/)  
      [Physics Library Bookshelves](https://phys.libretexts.org/Bookshelves)  
   *  [Chemistry Library. LibreTexts project](https://chem.libretexts.org/)  
      [General Chemistry](https://chem.libretexts.org/Bookshelves/General_Chemistry)  
   *  [http://www.vidadigital.net/blog/2008/10/12/mdulo-8-fuentes-de-libros-de-texto-abiertos/](http://www.vidadigital.net/blog/2008/10/12/mdulo-8-fuentes-de-libros-de-texto-abiertos/)  Enlace validado en marzo 2014
   *  [http://www.vidadigital.net/blog/2009/11/07/libros-de-texto-abiertos-y-gratuitos/](http://www.vidadigital.net/blog/2009/11/07/libros-de-texto-abiertos-y-gratuitos/)  Enlace validado en marzo 2014
   *  [http://es.wikibooks.org/wiki/Portada](http://es.wikibooks.org/wiki/Portada)  Este proyecto está integrado al proyecto wikimedia y tiene por objetivo poner a la disposición de cualquier persona libros de texto, manuales, tutoriales u otros textos pedagógicos de contenido libre y de acceso gratuito Enlace validado en marzo 2014
   *  [http://cnx.org/](http://cnx.org/)  Connexions is: a place to view and share educational material made of small knowledge chunks called modules that can be organized as courses, books, reports, etc. Enlace validado en marzo 2014
   *  [http://oerconsortium.org/discipline-specific/#Physics](http://oerconsortium.org/discipline-specific/#Physics)  Open Educational Resources. Open Textbooks. Enlace validado en marzo 2014
   *  [http://www.collegeopentextbooks.org/opentextbookcontent/open-textbooks-by-subject/physics](http://www.collegeopentextbooks.org/opentextbookcontent/open-textbooks-by-subject/physics)  La mayoría en inglés, cada uno indica su licenciamiento. Enlace validado en marzo 2014
   *  [http://www.siyavula.com/](http://www.siyavula.com/)  [http://www.everythingscience.co.za](http://www.everythingscience.co.za)  En inglés, Sudáfrica.  Enlaces validados en marzo 2014 (en 2011 era  [http://www.fhsst.org/node/8097](http://www.fhsst.org/node/8097)  Free High School Science Texts, licenciamiento GFDL.)
   *  [http://chemwiki.ucdavis.edu/](http://chemwiki.ucdavis.edu/)  The ChemWiki is a collaborative approach toward chemistry education where an Open Access textbook environment is constantly being written and re-written by students and faculty members resulting in a free Chemistry textbook to supplant conventional paper-based books. The development of the ChemWiki is currently directed by UC Davis Professor Delmar Larsen. cc-by-nc-sa
   *  [http://didacticafisicaquimica.es/](http://didacticafisicaquimica.es/)  
   La parte asociada a libros de texto la comento en apartado "libros de texto libres" dentro de  [libros de texto](/home/recursos/libros/libros-de-texto)  
   Pero también tiene más materiales, y para todos se indica  
   Los contenidos de esta obra se pueden reproducir total o parcialmente de forma libre y gratuita. Los autores no solo lo autorizamos expresamente sino que nos congratulamos de ello. Únicamente pedimos que se indique la fuente y que, por favor, siempre que sea posible, se colabore en su difusión dándolos a conocer a otras personas a las que también pudieran resultar útiles. Nuestro objetivo es contribuir, en lo que podamos, a la mejora de la enseñanza y aprendizaje de la Física y Química. En 2021 el listado de matariales adicionales a libros de texto dentro de  [https://didacticafisicaquimica.es/contenidos/](https://didacticafisicaquimica.es/contenidos/)  es  
 
 [SCOAP3 Sponsoring Consortium for Open Access Publishing in Particle Physics - books](https://scoap3.org/scoap3-books/)  
 The SCOAP3 for Books initiative is a pilot that the SCOAP3 Governing Council (GC) has established at its May 2019 meeting to leverage the strong network of the Collaboration and the relationships with scholarly publishing to transition key textbooks and monographs in particle physics and neighboring fields to open access.   
 
   
 [Otros materiales educativos](http://didacticafisicaquimica.es/contenidos#materiales-complementarios)  
 [Del cambio climático hacia la construcción de un futuro sostenible.](http://didacticafisicaquimica.es/del-cambio-climatico-hacia-la-construccion-de-un-futuro-sostenible/)  
 [Revisión del cálculo matemático para Física 2º Bachillerato](http://didacticafisicaquimica.es/revision-del-calculo-matematico-para-fisica-2o-bachillerato/)  
 [No a los accidentes en el laboratorio](http://didacticafisicaquimica.es/no-a-los-accidentes-en-el-laboratorio/)  [](http://http://didacticafisicaquimica.es/no-a-los-accidentes-en-el-laboratorio/)  
 [Enunciados de problemas 3º ESO](http://didacticafisicaquimica.es/enunciados-problemas-3oeso/)  
 [Enunciados de problemas 4º ESO](http://didacticafisicaquimica.es/enunciados-problemas-4oeso/)  
 [Enunciados de problemas 1º Bachillerato](http://didacticafisicaquimica.es/enunciados-de-problemas-1o-bachiller/)  
 [Enunciados de problemas 2º Bachillerato](http://didacticafisicaquimica.es/enunciados-de-problemas-2o-bachiller/)  
 [Estructura del átomo: Modelos atómicos](http://didacticafisicaquimica.es/?p=1235&preview=true)  
 [Libros](http://didacticafisicaquimica.es/contenidos/#didactica)  
 [Curso básico de Didáctica de las Ciencias ](http://didacticafisicaquimica.es/curso-basico-de-didactica-de-las-ciencias/)  
 [Enseñanza de las ciencias en educación secundaria](http://didacticafisicaquimica.es/ensenanza-las-ciencias-educacion-secundaria/)  
 [Energía: La invención de un concepto fructífero](http://didacticafisicaquimica.es/?p=1010&preview=true)  
 [Cambios en la energía de los sistemas](http://didacticafisicaquimica.es/?p=1052&preview=true)  
 [Transmisión de energía. Ondas ](http://didacticafisicaquimica.es/?p=1081&preview=true)  
 [Papel de la energía en nuestras vidas ](http://didacticafisicaquimica.es/?p=1098&preview=true)  
 [Fuentes de energía ](http://didacticafisicaquimica.es/?p=1118&preview=true)  
 [Artículos](http://didacticafisicaquimica.es/contenidos/#didactica-articulos)  
 [Problemas que dificultan una mejor utilización de la Didáctica de las Ciencias en la formación del profesorado](http://didacticafisicaquimica.es/?p=1211&preview=true)  
 [Concepciones alternativas (I)](http://didacticafisicaquimica.es/concepciones-alternativas-i/)  
 [Concepciones alternativas (II)](http://didacticafisicaquimica.es/concepciones-alternativas-ii/)  
 [Concepciones alternativas (III)](http://didacticafisicaquimica.es/concepciones-alternativas-iii/)  
 [Concepciones sobre la situación del mundo](http://didacticafisicaquimica.es/concepciones-sobre-la-situacion-del-mundo/)  
 [Didáctica de las ciencias-disciplina emergente](http://didacticafisicaquimica.es/didactica-de-las-ciencias-disciplina-emergente/)  
 [Formación inicial del profesorado](http://didacticafisicaquimica.es/formacion-inicial-del-profesorado/)  
 [La sostenibilidad en el currículo de Física](http://didacticafisicaquimica.es/la-sostenibilidad-en-el-curriculo-de-fisica/)  
 [Sostenibilidad y laboratorios escolares](http://didacticafisicaquimica.es/sostenibilidad-y-laboratorios-escolares/)  
 [Diferencias entre preconcepciones](http://didacticafisicaquimica.es/?p=752&preview=true)  
 [Ideas alternativas en Dinámica](http://didacticafisicaquimica.es/?p=1156&preview=true)  
 [What to do about science misconceptions](http://didacticafisicaquimica.es/?p=1037&preview=true)  
 [Pupils’ learning – Scientific knowledge](http://didacticafisicaquimica.es/?p=1145&preview=true)  
 [Presentaciones en Power-Point](http://didacticafisicaquimica.es/contenidos/#powerpoint)  
 [Luz y visión](http://didacticafisicaquimica.es/luz-y-vision/)  
 [Naturaleza de la ciencia](http://didacticafisicaquimica.es/naturaleza-de-la-ciencia/)  
 [Errores conceptuales e ideas alternativas](http://didacticafisicaquimica.es/contenidos#errores-conceptuales)  
 [Comics](http://didacticafisicaquimica.es/comics/)  
 [Libros de texto](http://didacticafisicaquimica.es/libros-de-texto/)  
 [Prensa](http://didacticafisicaquimica.es/prensa/)  
 [Novelas](http://didacticafisicaquimica.es/novelas/)  
 [Otros](http://didacticafisicaquimica.es/otros/)  
 [Otros materiales didácticos](http://didacticafisicaquimica.es/contenidos/#otros)  
 [Contribución del cómic a la imagen de la ciencia](http://didacticafisicaquimica.es/contribucion-del-comic-a-la-imagen-de-la-ciencia/)  
 [Tratamiento didáctico de los errores conceptuales](http://didacticafisicaquimica.es/?p=1423&preview=true)  
 [Argumentación en las clases de Física y Química](http://didacticafisicaquimica.es/?p=1173&preview=true)  
 [Evaluación y Competencia Científica](https://didacticafisicaquimica.es/?p=1566&preview=true) 


##  Libros experimentos
Tambíen material en la página de  [experimentos/laboratorios](/home/recursos/practicas-experimentos-laboratorio)  

[http://homeschool.scienceprojectideasforkids.com/books/](http://homeschool.scienceprojectideasforkids.com/books/)  Janice VanCleave

##  Libros didáctica - docencia en general
 Tiempos de pruebas: los usos y abusos de la evaluación; Gordon, Stobart; Ed. Morata; Ministerio de Educación, Cultura y Deporte, 2011 - 240 páginas  
 [https://books.google.es/books/about/Tiempos_de_pruebas_los_usos_y_abusos_de.html?id=IiUbAgAAQBAJ](https://books.google.es/books/about/Tiempos_de_pruebas_los_usos_y_abusos_de.html?id=IiUbAgAAQBAJ) 

##  Libros didáctica - docencia ciencias
2020  
Student Misconceptions and Errors in Physics and Mathematics Exploring Data from TIMSS and TIMSS Advanced  
[https://link.springer.com/book/10.1007/978-3-030-30188-0#toc](https://link.springer.com/book/10.1007%2F978-3-030-30188-0#toc)   
