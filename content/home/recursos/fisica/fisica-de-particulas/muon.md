
# Muón

Enlaza con detectores: CMS son las siglas de Compact Moun Soleoid

16 abril 2021  
Cómo los Muones la están Liando Parda en la Física (ahora, rigurosamente contado)  
 [![Cómo los Muones la están Liando Parda en la Física (ahora, rigurosamente contado) - YouTube](https://img.youtube.com/vi/ynB3zFU2RHI/0.jpg)](https://www.youtube.com/watch?v=ynB3zFU2RHI)   

18 julio 2024  
[Adiós a la desviación de Muon g−2 del Fermilab en el momento magnético anómalo del muón - francis.naukas.com](https://francis.naukas.com/2024/07/18/adios-a-la-desviacion-de-muon-g%e2%88%922-del-fermilab-en-el-momento-magnetico-anomalo-del-muon/)  
