
# Dualidad onda corpúsculo

## Recursos generales
Puede haber recursos relacionados en  [recursos óptica física](/home/recursos/fisica/recursos-optica-fisica)  [Tout est quantique](http://www.toutestquantique.fr/#dualite)   
 [![https://upload.wikimedia.org/wikipedia/commons/9/9b/Wave-particle.jpg](https://upload.wikimedia.org/wikipedia/commons/9/9b/Wave-particle.jpg "https://upload.wikimedia.org/wikipedia/commons/9/9b/Wave-particle.jpg") ](https://upload.wikimedia.org/wikipedia/commons/9/9b/Wave-particle.jpg)   
 [File:Wave-particle.jpg - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Wave-particle.jpg) 

[![Ya, en serio, ¿Qué es la Luz?  QuantumFracture](https://img.youtube.com/vi/DkcEAz09Buo/0.jpg)](https://www.youtube.com/watch?v=DkcEAz09Buo "Ya, en serio, ¿Qué es la Luz?  QuantumFracture")  

## Aproximación a través de analogías en física clásica: "gotas danzantes", walking droplets , dotwaves...
Las gotas danzantes son una aproximación “visible en física clásica” a la dualidad onda corpúsculo y la onda piloto de la física cuántica.  
  
The pilot-wave dynamics of walking droplets, MITMathLab  
 [![The pilot-wave dynamics of walking droplets - YouTube](https://img.youtube.com/vi/nmC0ygr08tE/0.jpg)](https://www.youtube.com/watch?v=nmC0ygr08tE)   
  
 [Hydrodynamic quantum analogs](https://en.wikipedia.org/wiki/Hydrodynamic_quantum_analogs)   

 [DotWave.org](http://dotwave.org/)  Wave-Particle duality at human scale [Hydrodynamic quantum analogs - Wikipedia](http://en.wikipedia.org/wiki/Hydrodynamic_quantum_analogs)   
 Yves Couder . Explains Wave/Particle Duality via Silicon Droplets [Through the Wormhole]  
 [![Yves Couder . Explains Wave/Particle Duality via Silicon Droplets [Through the Wormhole] - YouTube](https://img.youtube.com/vi/W9yWv5dqSKk/0.jpg)](https://www.youtube.com/watch?v=W9yWv5dqSKk)  (ver de 1:28 a 1:33 idea onda piloto)  
MinutePhysics- The Wave/Particle Duality - Part 1  
 [![What is the Wave/Particle Duality? Part 1 - YouTube](https://img.youtube.com/vi/Q_h4IoPJXZw/0.jpg)](https://www.youtube.com/watch?v=Q_h4IoPJXZw)   
MinutePhysics- The Wave/Particle Duality - Part 2  
 [![watch](https://img.youtube.com/vi/_riIY-v2Ym8/0.jpg)](https://www.youtube.com/watch?v=_riIY-v2Ym8) 

## Experimento doble rendija  

Meme de la doble rendija (muñeco que mira y deja de mirar)  
[Un meme que explica mecánica cuántica](https://www.microsiervos.com/archivo/ciencia/meme-explica-mecanica-cuantica.html)  
![](https://img.microsiervos.com/images2021/meme-doble-rendija.jpg)

Dr. Quantum: Dobule Slit Experiment  
Vídeo de 5 min sacado de la película: "What The Bleep Do We Know!?: Down The Rabbit Hole"   
 [What the Bleep Do We Know!? | Feature Film](http://www.whatthebleep.com/)   
Muy visual "dibujos animados"  
Subido en youtube por varias personas, pero por licenciamiento podría ser borrado. Pongo enlace a la búsqueda [Dr+Quantum+Double+Slit+Experiment](https://www.youtube.com/results?search_query=Dr+Quantum+Double+Slit+Experiment) 

[twitter DivulgaMadrid/status/1588629075530485760](https://twitter.com/DivulgaMadrid/status/1588629075530485760)  
Hay gente utilizando este meme para argumentar que la mecánica cuántica es una teoría que está mal hecha. Están haciendo el ridículo, porque el meme es sólo una broma. En realidad, ni la mecánica cuántica predice eso ni el mundo funciona así. Abro hilo  
![](https://pbs.twimg.com/media/FgvxuOEWYAcJIhh?format=jpg)  
Por el hecho de mover los ojos y ponerlos en la dirección de las rendijas no va a ocurrir que, si se estaba produciendo un patrón de interferencia (dibujo de arriba), éste vaya a dejar de producirse (dibujo de abajo). Al mirar algo, no estamos enviado rayos al objeto observado.  
Luego no podemos modificar su comportamiento. Lo que sí ocurre cuando vemos algo es que hay luz que sale de ese algo y llega a nuestros ojos. Los niños aprenden esto de pequeños, pero hay gente en Twitter que parece que se les ha olvidado.  
Esto es clave. Hay luz que sale de ese algo. Pero, el hecho de que haya luz que salga de ese algo, es independiente de que pongamos los ojos en esa dirección para mirar o no.  
Ahora bien, ¿qué pasa cuando hay luz que sale de ese algo? Pues que esa luz está entrelazada cuánticamente con ese algo. Este es el punto importante que ignoran los que atacan a la mecánia cuántica, los que no aceptan los descubrimientos que se hicieron en Copenhague hace 96 años  
Cuando calculo la amplitud de probabilidad de que ese algo llegue a un punto concreto de la pantalla tras atravesar la doble rendija, tengo que sumar para todos los caminos. Simplificando mucho, tengo que sumar las amplitudes correspondientes a estos dos caminos.  
![](https://pbs.twimg.com/media/Fgv180RXEAQRuqp?format=jpg)  
Otra forma equivalente d expresar esto matemáticamente es decir que el estado cuántico de esa partícula inmediatamente antes de llegar a la pantalla es la suma(superposición cuántica) del estado asociado a haber pasado por la rendija 1, más el estado asociado a haber pasado por 2  
Pero, ¿qué pasa si la partícula, cuando está atravesando la doble rendija, emite luz? Entonces ahora esa luz también está en un estado cuántico superposición de haber sido emitida desde la rendija 1 más haber sido emitida desde la rendija 2.  
Pero este estado cuántico es un estado entrelazado. Eso significa que no se puede expresar como un producto (tensorial) de un estado asociado a la partícula, multiplicado por un estado asociado a la luz emitida por ésta.  
...  
Pero el meme con rigor científico ya no hace gracia. Así que vamos a seguir riéndonos usando el meme original, sabiendo que es una broma, y pasando vergüenza ajena cuando veamos a alguien tomárselo en serio.  
Si quieres este hilo en podcast, lo tienes aquí:  
[La decoherencia, o por qué no se puede hacer el experimento de la doble rendija con gatos puntuales - instituto ouróboros ](https://institutoouroboros.com/2022/10/23/la-decoherencia-o-por-que-no-se-puede-hacer-el-experimento-de-la-doble-rendija-con-gatos-puntuales/)	


## Difracción partículas
 Yves Couder . Explains Wave/Particle Duality via Silicon Droplets [Through the Wormhole]  
 [![Yves Couder . Explains Wave/Particle Duality via Silicon Droplets [Through the Wormhole] - YouTube](https://img.youtube.com/vi/W9yWv5dqSKk/0.jpg)](https://www.youtube.com/watch?v=W9yWv5dqSKk)  (ver de 2:40 a 3:10 idea difracción partículas)Single electron events build up to form an interference pattern in the double-slit experiments.  
 [index.html](http://www.hitachi.com/rd/portal/highlight/quantum/movie/index.html)   
A. Tonomura, J. Endo, T. Matsuda, T. Kawasaki and H. Ezawa, "Demonstration of Single-Electron Buildup of an Interference Pattern,"Amer. J. Phys. 57 (1989) pp.117-120.  
Demonstrated single-electron build-up of an interference pattern.  

  
