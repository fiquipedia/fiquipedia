---
aliases:
  - /home/recursos/recursos-matematicas/
---

# Recursos matemáticas

Las matemáticas son una de las materias que pueden acabar impartiendo con alta probabilidad los profesores de la especialidad de Física y Química, aunque legalmente no es correcto. Solamente es legal impartir por un docente de la especialidad de Física y Química desde el ámbito científico tecnológico de diversificación / Programa de Mejora del Aprendizaje y Rendimiento, de Educación de Adultos, de PCPI/FP Básica...  
Además es habitual que en física se necesiten ciertos conceptos matemáticos, que a veces los alumnos no han visto todavía en matemáticas:  
Se incluyen algunos recursos  
* [Trigonometría en 4º ESO](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso/apuntes-fisica-y-quimica-4-eso-elaboracion-propia/FQ4-Pizarras-Trigonometr%C3%ADa.pdf)  
* [Derivación en 1º Bachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato/FQ1B-Pizarras-Derivaci%C3%B3n.pdf)  
* [Integración en 2º Bachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato/FQ1B-Pizarras-Integraci%C3%B3n.pdf)  
* [Cálculo vectorial (vector se introduce en ESO)](/home/recursos/recursos-matematicas/calculo-vectorial) ...  

Para nivel superior a secundaria bachillerato se necesitan mucho las matemáticas, por ejemplo ecuaciones diferenciales  
Algo común y asociado son las funciones y las representaciones gráficas  
También se pueden ver  [recursos asociados a uso de la calculadora](/home/recursos/recursos-uso-calculadora)   
  
Incluyo aquí algunos enlaces a recursos, también puede haber algunos en  [cursos online / interactivos](/home/recursos/cursos-online-interactivos) , también puede haber en recursos sobre PAU, por ejemplo muchas veces se combinan páginas de física y matemáticas con exámenes PAU / Selectividad.  

[Map of mathematics, Domain of sciencie, Dominic Walliman](https://www.flickr.com/photos/95869671@N08/with/32264483720/)  
![](https://live.staticflickr.com/272/32264483720_c51bdde679_b.jpg "Map of mathematics Dominic Walliman")  
  
 [Wolfram|Alpha: Computational Intelligence](http://www.wolframalpha.com/)   
Útil para realizar cálculos, representaciones, comprobaciones...  

En Twitter se pueden ver hashtags donde se comparten ideas   
[#MTBoS math-twitter blog-o-sphere](https://twitter.com/hashtag/MTBoS)  
[#ITeachMath](https://twitter.com/hashtag/ITeachMath)  
[mtbos.org](https://mtbos.org/)    

[I fixed the meme that was posted on /r/physics yesterday. Difficulty of math vs Time ](https://www.reddit.com/r/physicsmemes/comments/cadgu2/i_fixed_the_meme_that_was_posted_on_rphysics/)  
![](https://i.redd.it/e6927md6ty831.jpg)  


[twitter pbeltranp/status/1578809116255846400](https://twitter.com/pbeltranp/status/1578809116255846400)  
Volviendo a lo de Twitter. Larsen utiliza esta imagen de  @sylviaduckworth para ilustrar la típica historia que evocan los que han participado del #MTBoS (o del #iteachmath).  
![](https://pbs.twimg.com/media/FekMC3SWQAUMpd1?format=jpg)  
MTBoS = Math-Twitter Blog-o-Sphere  
  
 [Matem&aacute;ticas con Derive](http://www.musat.net/)   
Página personal de Isaac Musat Hervás (Profesor de Matemáticas del Colegio Villaeuropa de Móstoles)  
 [ Matemáticas II | Mates a tu lado | EducaMadrid ](https://www.educa2.madrid.org/web/matesatulado/matematicas2)   
  
 [ThatQuiz](http://www.thatquiz.org/es/)   
Test de matemáticas (con algunas otras cosas), muy bien elaborados, posibilidad de darse de alta como profesor, crear tests y seguir el resultado en grupos de alumnos.  

[ Web de PEvAU, Selectividad de Matemáticas II y Matemáticas Aplicadas a las Ciencias Sociales de algunas Comunidades Autónomas. - iesayala.com](https://www.iesayala.com/selectividadmatematicas/)  
Olimpiadas Matemáticas (OME,IMO,OMI,AIME)  
Pruebas de Acceso a la Universidad para mayores de 25 años de la asignatura de Matemáticas de algunas Comunidades Autónomas.  
Germán Jesús Rubio Luna.  
  
 [Aprende Matematicas Online. Primaria, Secundaria ESO y Bachillerato.](http://www.amolasmates.es/)   
Apuntes y ejercicios por nivel.  
  
 [Material Didáctico - Superprof](http://www.vitutor.com/)   
Apuntes y ejercicios por nivel.  
@Vitutor 2012 Todos los derechos reservados   
  
 [Matemáticas Secundaria y Bachillerato | Apuntes, ejercicios, exámenes y artículos de matemáticas](http://lasmatematicas.eu/) Artículos, apuntes, ejercicios y exámenes de matemáticas © Pedro Castro Ortega   
  
 [http://www.studyit.org.nz/subjects/maths/](http://www.studyit.org.nz/subjects/maths/)   
  
Apuntes marea verde.  
 [Apuntes MareaVerde](http://www.apuntesmareaverde.org.es/)   
cc-by-nc-sa  
(En 2014 previstos también de física y química, pero todavía no elaborados)  
  
 [Blog de matemáticas de 5º | CEIP Santa María del Buen Aire](http://cplapueblamate5.wordpress.com/)   
  
 [Matematicas Visuales|Home](http://www.matematicasvisuales.com/)   
Roberto Cardil  
  
Recursos geogebra  
 [GeoGebra - the world’s favorite, free math tools used by over 100 million students and teachers](http://tube.geogebra.org/)   
At Desmos, our mission is to help every student learn math and love learning math. Explore and enjoy our collection of free digital math activities for you and your students.  
 [Desmos Classroom Activities](https://teacher.desmos.com/)   
  
 [EXAMENES GRADO SUPERIOR MATEMATICAS](http://apruebalasmates.blogspot.com.es/)   
  
 [lasmatematicas.es - Videos y ejercicios resueltos de matematicas](http://www.lasmatematicas.es/)   
Vídeos y recursos para varios niveles  
Dr. Juan Medina Molina  
Dpto de Matemática Aplicada y Estadística  
Escuela Técnica Superior de Ingeniería Industrial  
Universidad Politécnica de Cartagena   
  
 [BetterExplained &#8211; Math lessons that click](http://betterexplained.com/)   
Learn Right. Not Rote. Math explanations that click.  
A frustrating class sent me looking for the why behind the equations. This site helps you grasp tough concepts, from exponents and imaginary numbers to the Fourier Transform.   
Autor: Kalid Azad  

Taylor series | Chapter 11, Essence of calculus - 3Blue1Brown  
[![](https://img.youtube.com/vi/3d6DsjIBzJ4/0.jpg)](https://www.youtube.com/watch?v=3d6DsjIBzJ4 "Taylor series | Chapter 11, Essence of calculus - 3Blue1Brown")   

[Twitter ThePhysicsMemes/status/1856620873740083388?](https://x.com/ThePhysicsMemes/status/1856620873740083388)  
![]https://pbs.twimg.com/media/GcQK1_hakAAxpwq?format=jpg)  
  
 [https://www.youtube.com/user/profesorDonC/](https://www.youtube.com/user/profesorDonC/)   
  
 [Ejercicios de Matem&aacute;ticas](http://www.ematematicas.net)   
Ejercicios de matemáticas  
  
 [Matem&aacute;ticas ESO y Bachillerato ejercicios  ](http://www.vadenumeros.es)   
Matemáticas secundaria, bachillerato y selectividad.  
  
 [Programas Smartick de matemáticas y lectura](http://www.smartick.es/matematicas/enun.html)   
Ejercicios matemáticas online, distintos niveles (primaria)  
  
Ecuaciones Diferenciales (Ingeniería Química), Ion Zaballa Tejada  
 [ecu_dif.htm](http://www.ehu.eus/izaballa/Ecu_Dif/ecu_dif.htm)   
  
Antonio Omatos, cc-by-nc-sa [Matemáticas en la ESO ](http://eso.aomatos.com/)   
 [Ideas y Recursos para la clase de matemáticas ](http://recursosmates.aomatos.com/index.html)   
Recopilaciones  
https://twitter.com/lolamenting/status/1285510978445225989Ando recopilando enlaces útiles para la clase de Matemáticas. Iré completándolos estos días, sobre todo los específicos por temas (¿alguna sugerencia más?), pero dejo los que tengo por aquí:  
 [edit](https://docs.google.com/document/d/174DY7xK8rLVvVwqYCweQDZ0tO4Zluf8RiG2k-cTWPDw/edit)   
Pablo J. Triviño Rodríguez  
 [Pablo J. Triviño Rodríguez – Resources – GeoGebra](https://www.geogebra.org/u/ptrivino)   
  
El curso consta de cuatro ejes fundamentales que coincidirán con los apartados principales de esta web:
   1.  [Utilidades y recursos TICA: Desmos, Kahoot, Plickers,ThatQuiz, libros de GeoGebra](http://recursosmates.aomatos.com/recursos_tic.html) ...
   1.  [Proyectos matemáticos: restaurante matemático y otros guisos](http://recursosmates.aomatos.com/proyectos.html) 
   1.  [Juegos matemáticos para el aula: puzzles, bingos, cuatro en raya](http://recursosmates.aomatos.com/juegos_matemticos.html) 
   1.  [Ideas y recursos varios para el aula](http://recursosmates.aomatos.com/ideas_y_recursos_varios.html) : cine y matemáticas, matemáticas en tres actos, investigaciones matemáticas.
  
Berkeley Everett   
 [Math Visuals](https://mathvisuals.wordpress.com/)  [Fractions &#8211; Math Visuals](https://mathvisuals.wordpress.com/fractions/)   
  
Seeing Theory  
A visual introduction to probability and statistics  
 [Seeing Theory](http://students.brown.edu/seeing-theory/index.html)   
  
Aprender a enseñar matemáticas en la educación secundaria obligatoria  
Calvo Pesce, Cecilia · Deulofeu Piquet, Jordi · Jareño Ruiz, Joan · Morera Úbeda, Laura  
ISBN: 9788490774205  
ISBN Digital: 9788490779484  
 [aprender a ensenar matematicas en la educacion secundaria obligatoria ebook 2243 | didacticas recursos y aprendizaje 2 | ](https://www.sintesis.com/didacticas-recursos-y-aprendizaje-223/aprender-a-ensenar-matematicas-en-la-educacion-secundaria-obligatoria-ebook-2243.html)   
Web que acompaña libro  
 [Aprende Matemáticas Hoy](https://sites.google.com/site/aprendematematicashoy/home)   
 Representar funciones  
 [FooPlot | Graficador de Funciones Matematicas]https://pfortuny.net/fooplot.com/)   
  
 [FÍSICAYMATES](https://fisicaymates.com/)   
Blog enlazado con el canal de matemáticas de youtube/jmsreales. Aquí vas a encontrar ayuda sobre temas de matemáticas, curiosidades y todo el contenido del canal organizado para tener un acceso ágil a lo que buscas. Pero también hay mucho más. Puedes echar un ojo. Bienvenido.  
 [No todo es matemáticas](http://notodoesmatematicas.blogspot.com/)   
  
Competencias Matemáticas 1º ESOAutor: Álvaro Fernández y Pablo Triviñocc-by-sa [Competencias Matemáticas 1º ESO – GeoGebra](https://www.geogebra.org/m/dEV5qYNY)   
  
 [Pablo Jesús T. (Vídeos) (videos) | Mediateca de EducaMadrid](https://mediateca.educa.madrid.org/usuario/ptrivino/videos)     
  
 [Mini Unit Circle Twitter Storm! - mathhappens.org](https://www.mathhappens.org/mini-unit-circle-twitter-storm/)  
 [twitter AlexKontorovich/status/1227247510785314816](https://twitter.com/AlexKontorovich/status/1227247510785314816)  
 What is a “radian” anyway? It’s an equilateral triangle! (With one side curvilinear).   
(With many thanks to @MathHappensOrg!)  
![](https://www.mathhappens.org/wp-content/uploads/2020/02/Screen-Shot-2020-02-15-at-7.17.58-PM-814x946.png)  

[Ministerio de Educación y Ciencia, El lenguaje de funciones y gráficas](https://sede.educacion.gob.es/publiventa/el-lenguaje-de-funciones-y-graficas/pedagogia/1065)  


Find the Volume of Any Shape Using Calculus. Domain Of Science  
[![](https://img.youtube.com/vi/Lbg1M0PTy1A/0.jpg)](https://www.youtube.com/watch?v=Lbg1M0PTy1A "Find the Volume of Any Shape Using Calculus")   

Puede enlazar con dibujo, aplicaciones para dibujar fórmulas, uso de LaTEX ...
[Ecuation editor - codecogs.com](https://editor.codecogs.com/)  
[Editor ecucaciones online LaTEX - codecogs.com](https://www.codecogs.com/latex/eqneditor.php)  
[Mathcha.io](https://www.mathcha.io/)  También permite 

Teoría de grupos y porqué amo el número 808,017,424,794,512,875,886,459,904,961,710,757,005,754,368,000,000,000 - 3Blue1Brown  
[![](https://img.youtube.com/vi/mH0oCDa74tE/0.jpg)]((https://www.youtube.com/watch?v=mH0oCDa74tE "Teoría de grupos y porqué amo el número 808,017,424,794,512,875,886,459,904,961,710,757,005,754,368,000,000,000 - 3Blue1Brown")   

Animation vs. Math. Alan Becker  
[![](https://img.youtube.com/vi/B1J6Ou4q8vE/0.jpg)](https://www.youtube.com/watch?v=B1J6Ou4q8vE "Animation vs. Math")   

¿Cómo de seguro es la seguridad de 256 bits?  
[![](https://img.youtube.com/vi/S9JGmA5_unY/0.jpg)](https://www.youtube.com/watch?v=S9JGmA5_unY "¿Cómo de seguro es la seguridad de 256 bits?")   

Venga va... ¿Qué es un TENSOR? Mates Mike  
[![](https://img.youtube.com/vi/s4USUVBuFi0/0.jpg)](https://www.youtube.com/watch?v=s4USUVBuFi0 "Venga va... ¿Qué es un TENSOR? Mates Mike")   

What the HECK is a Tensor?!? The Science Asylum  
[![](https://img.youtube.com/vi/bpG3gqDM80w/0.jpg)](https://www.youtube.com/watch?v=bpG3gqDM80w "What the HECK is a Tensor?!? The Science Asylum")   




