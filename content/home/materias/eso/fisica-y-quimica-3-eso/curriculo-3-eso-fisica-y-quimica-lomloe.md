# Currículo 3º ESO Física y Química (LOMLOE)

En esta página se incluye el [currículo estatal (Real Decreto)](#Estatal) y el [currículo de Madrid (Decreto)](#Madrid)

# <a name="Estatal"></a> Estatal (conjunto para 1º, 2º y 3º ESO)
Revisado con [Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975) citado en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  
La normativa estatal fija los criterios de evaluación y los saberes básicos de manera conjunta para 1º, 2º y 3º ESO  

Se ponen aquí los criterios de evaluación y los saberes básicos y se referenciarán desde las páginas de otros cursos  
Si a nivel autonómico hay modificaciones, se intentarán ir reflejando.  

Ver  [comparación currículo Física y Química 3º ESO](/home/materias/eso/fisica-y-quimica-3-eso/comparacion-curriculo-3-eso-fisica-y-quimica)

Cambios en trámite audiencia noviembre 2021 respecto a borrador octubre:  
En competencia específica 1.1 cambia "Comprender" por "Identificar, comprender y explicar"  
En competencia específica 3.2 cambia "formulación y nomenclatura" por "nomenclatura"  
En competencia específica 4.1 cambia "de todos" por "de cada participante"  
En competencia específica 5.1 cambia "constructivas" por "constructivas y coeducativas", y "para los demás" por "para la comunidad"  
En competencia específica 6.1 cambia "Reconocer" por "Reconocer y valorar", "en construcción" por "en permanente construcción"  
Sin cambios en saberes básicos  

Cambios en BOE marzo 2022 respecto a trámite de audiencia noviembre 2021:  
Retoques mínimos en competencias específicas.  
Retoques mínimos en saberes básicos:  
En A entornos y normas se separan, se citan explícitamente símbolos SI.  
En B se deja de citar explícitamente formación de iones, se cita masa atómica y molecular.  
En C se cita electrización.  
En D se añade "Fenómenos gravitatorios, eléctricos y magnéticos: experimentos sencillos que evidencian la relación con las fuerzas de la naturaleza."  

## CURSOS DE PRIMERO A TERCERO

### Criterios de evaluación

#### Competencia específica 1.

1.1 Identificar, comprender y explicar los fenómenos fisicoquímicos cotidianos más relevantes a partir de los principios, teorías y leyes científicas adecuadas, expresándolos, de manera argumentada, utilizando diversidad de soportes y medios de comunicación.

1.2 Resolver los problemas fisicoquímicos planteados utilizando las leyes y teorías científicas adecuadas, razonando los procedimientos utilizados para encontrar las soluciones y expresando adecuadamente los resultados.

1.3 Reconocer y describir en el entorno inmediato situaciones problemáticas reales de índole científica y emprender iniciativas en las que la ciencia, y en particular la física y la química, pueden contribuir a su solución, analizando críticamente su impacto en la sociedad.

#### Competencia específica 2.

2.1 Emplear las metodologías propias de la ciencia en la identificación y descripción de fenómenos a partir de cuestiones a las que se pueda dar respuesta a través de la indagación, la deducción, el trabajo experimental y el razonamiento lógico-matemático, diferenciándolas de aquellas pseudocientíficas que no admiten comprobación experimental.

2.2 Seleccionar, de acuerdo con la naturaleza de las cuestiones que se traten, la mejor manera de comprobar o refutar las hipótesis formuladas, diseñando estrategias de indagación y búsqueda de evidencias que permitan obtener conclusiones y respuestas ajustadas a la naturaleza de la pregunta formulada.

2.3 Aplicar las leyes y teorías científicas conocidas al formular cuestiones e hipótesis, siendo coherente con el conocimiento científico existente y diseñando los procedimientos experimentales o deductivos necesarios para resolverlas o comprobarlas.

#### Competencia específica 3.

3.1 Emplear datos en diferentes formatos para interpretar y comunicar información relativa a un proceso fisicoquímico concreto, relacionando entre sí lo que cada uno de ellos contiene, y extrayendo en cada caso lo más relevante para la resolución de un problema.

3.2 Utilizar adecuadamente las reglas básicas de la física y la química, incluyendo el uso de unidades de medida, las herramientas matemáticas y las reglas de nomenclatura, consiguiendo una comunicación efectiva con toda la comunidad científica.

3.3 Poner en práctica las normas de uso de los espacios específicos de la ciencia, como el laboratorio de física y química, asegurando la salud propia y colectiva, la conservación sostenible del medio ambiente y el cuidado de las instalaciones.

#### Competencia específica 4.

4.1 Utilizar recursos variados, tradicionales y digitales, mejorando el aprendizaje autónomo y la interacción con otros miembros de la comunidad educativa, con respeto hacia docentes y estudiantes y analizando críticamente las aportaciones de cada participante.

4.2 Trabajar de forma adecuada con medios variados, tradicionales y digitales, en la consulta de información y la creación de contenidos, seleccionando con criterio las fuentes más fiables y desechando las menos adecuadas y mejorando el aprendizaje propio y colectivo.

#### Competencia específica 5.

5.1 Establecer interacciones constructivas y coeducativas, emprendiendo actividades de cooperación como forma de construir un medio de trabajo eficiente en la ciencia.

5.2 Emprender, de forma guiada y de acuerdo a la metodología adecuada, proyectos científicos que involucren al alumnado en la mejora de la sociedad y que creen valor para el individuo y para la comunidad.

#### Competencia específica 6.

6.1 Reconocer y valorar, a través del análisis histórico de los avances científicos logrados por hombres y mujeres de ciencia, que la ciencia es un proceso en permanente construcción y que existen repercusiones mutuas de la ciencia actual con la tecnología, la sociedad y el medio ambiente.

6.2 Detectar en el entorno las necesidades tecnológicas, ambientales, económicas y sociales más importantes que demanda la sociedad, entendiendo la capacidad de la ciencia para darles solución sostenible a través de la implicación de todos los ciudadanos.

### Saberes básicos

**A. Las destrezas científicas básicas.**

- Metodologías de la investigación científica: identificación y formulación de cuestiones, elaboración de hipótesis y comprobación experimental de las mismas.

- Trabajo experimental y proyectos de investigación: estrategias en la resolución de problemas y en el desarrollo de investigaciones mediante la indagación, la deducción, la búsqueda de evidencias y el razonamiento lógico-matemático, haciendo inferencias válidas de las observaciones y obteniendo conclusiones.

- Diversos entornos y recursos de aprendizaje científico como el laboratorio o los entornos virtuales: materiales, sustancias y herramientas tecnológicas.

- Normas de uso de cada espacio, asegurando y protegiendo así la salud propia y comunitaria, la seguridad en las redes y el respeto hacia el medio ambiente.

- El lenguaje científico: unidades del Sistema Internacional y sus símbolos. Herramientas matemáticas básicas en diferentes escenarios científicos y de aprendizaje.

- Estrategias de interpretación y producción de información científica utilizando diferentes formatos y diferentes medios: desarrollo del criterio propio basado en lo que el pensamiento científico aporta a la mejora de la sociedad para hacerla más justa, equitativa e igualitaria.

- Valoración de la cultura científica y del papel de científicos y científicas en los principales hitos históricos y actuales de la física y la química en el avance y la mejora de la sociedad.

**B. La materia.**

- Teoría cinético-molecular: aplicación a observaciones sobre la materia explicando sus propiedades, los estados de agregación, los cambios de estado y la formación de mezclas y disoluciones.

- Experimentos relacionados con los sistemas materiales: conocimiento y descripción de sus propiedades, su composición y su clasificación.

- Estructura atómica: desarrollo histórico de los modelos atómicos, existencia, formación y propiedades de los isótopos y ordenación de los elementos en la tabla periódica.

- Principales compuestos químicos: su formación y sus propiedades físicas y químicas, valoración de sus aplicaciones. Masa atómica y masa molecular.

- Nomenclatura: participación de un lenguaje científico común y universal formulando y nombrando sustancias simples, iones monoatómicos y compuestos binarios mediante las reglas de nomenclatura de la IUPAC.

**C. La energía.**

- La energía: formulación de cuestiones e hipótesis sobre la energía, propiedades y manifestaciones que la describan como la causa de todos los procesos de cambio.

- Diseño y comprobación experimental de hipótesis relacionadas con el uso doméstico e industrial de la energía en sus distintas formas y las transformaciones entre ellas.

- Elaboración fundamentada de hipótesis sobre el medio ambiente y la sostenibilidad a partir de las diferencias entre fuentes de energía renovables y no renovables.

- Efectos del calor sobre la materia: análisis de los efectos y aplicación en situaciones cotidianas.

- Naturaleza eléctrica de la materia: electrización de los cuerpos, circuitos eléctricos y la obtención de energía eléctrica. Concienciación sobre la necesidad del ahorro energético y la conservación sostenible del medio ambiente.

**D. La interacción.**

- Predicción de movimientos sencillos a partir de los conceptos de la cinemática, formulando hipótesis comprobables sobre valores futuros de estas magnitudes, validándolas a través del cálculo numérico, la interpretación de gráficas o el trabajo experimental.

- Las fuerzas como agentes de cambio: relación de los efectos de las fuerzas, tanto en el estado de movimiento o de reposo de un cuerpo como produciendo deformaciones en los sistemas sobre los que actúan.

- Aplicación de las leyes de Newton: observación de situaciones cotidianas o de laboratorio que permiten entender cómo se comportan los sistemas materiales ante la acción de las fuerzas y predecir los efectos de estas en situaciones cotidianas y de seguridad vial.

- Fenómenos gravitatorios, eléctricos y magnéticos: experimentos sencillos que evidencian la relación con las fuerzas de la naturaleza.

**E. El cambio.**

- Los sistemas materiales: análisis de los diferentes tipos de cambios que experimentan, relacionando las causas que los producen con las consecuencias que tienen.

- Interpretación macroscópica y microscópica de las reacciones químicas: explicación de las relaciones de la química con el medio ambiente, la tecnología y la sociedad.

- Ley de conservación de la masa y de la ley de las proporciones definidas: aplicación de estas leyes como evidencias experimentales que permiten validar el modelo atómico-molecular de la materia.

- Factores que afectan a las reacciones químicas: predicción cualitativa de la evolución de las reacciones, entendiendo su importancia en la resolución de problemas actuales por parte de la ciencia.

# <a name="Madrid"></a> Madrid (3º ESO)

Revisado con [Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-2.PDF) 

[Borrador Currículo ESO Madrid 19 abril 2022](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/legislacion-educativa/lomloe/2022-04-19-Madrid-ESO-Curr%C3%ADculo-Borrador.pdf)  
Cambia respecto a Real Decreto estatal que no separa por cursos, se marcan diferencias añadidas en negrita (en borrador 19 abril 2022 hay algunas indicaciones en rojo aunque algunas ya estaban en RD). Si hay alguna eliminación o cambio relevante se intenta comentar como texto. 

Se reordena para que bloque de cambio esté antes que energía. En RD cambio es E y energía C. En Madrid cambio es C y energía E.  

En bloque cambio se elimina "macroscópico" y párrafo "Los sistemas materiales: análisis de los diferentes tipos de cambios que experimentan, relacionando las causas que los producen con las consecuencias que tienen." **Está en 2ºESO**  
En bloque energía no aparece "La energía: formulación de cuestiones e hipótesis sobre la energía, propiedades y manifestaciones que la describan como la causa de todos los procesos de cambio.", "Efectos del calor sobre la materia: análisis de los efectos y aplicación en situaciones cotidianas." y "la obtención de energía eléctrica." **Está en 2ºESO** 

[Proyecto de decreto por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria. 23 mayo](https://www.comunidad.madrid/transparencia/sites/default/files/2022-05-23_decreto_eso-completo.pdf)  
En competencia específica 1.3 se elimina ", analizando críticamente su impacto en la sociedad"  
En competencia específica 3.3 se elimina "propia y colectiva" y "sostenible"   
En competencia específica 4.2 se elimina "propio y colectivo"  
En competencia específica 5.1 se cambia "Establecer interacciones constructivas y coeducativas, emprendiendo actividades de cooperación **y empleando estrategias propias del trabajo colaborativo**," por "Cooperar"  
En competencia específica 5.2 se elimina "y que creen valor para el individuo y para la comunidad"  
En competencia específica 6.1 se cambia "hombres y mujeres de ciencia" por "la humanidad"  
En competencia específica 6.2 se elimina " **para entender** la capacidad de la ciencia para darles solución sostenible a través de la implicación de todos los ciudadanos."  
En contenido A se añade al título "básicas"  
En contenido E se elimina "y la sostenibilidad", "renovables y no renovables", "sostenible" y "**• Análisis crítico del impacto en el medio ambiente de fuentes de energía renovables y no renovables.**  "  

Cambios [Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-2.PDF) vs proyecto  
Sin cambios en criterios de evaluación y en saberes básicos.  

## 3º ESO
### Criterios de evaluación.
#### Competencia específica 1.

1.1. Comprender y explicar los fenómenos fisicoquímicos cotidianos más relevantes a partir de los principios, teorías y leyes científicas adecuadas, expresándolos, de manera argumentada, utilizando diversidad de soportes y medios de comunicación.

1.2. Resolver los problemas fisicoquímicos planteados **en este curso** utilizando las leyes y teorías científicas adecuadas, razonando los procedimientos utilizados para encontrar la **solución o** soluciones y expresando adecuadamente los resultados.

1.3. **Identificar** en el entorno inmediato situaciones problemáticas reales de índole científica y emprender iniciativas en las que la ciencia, y en particular la física y la química, pueden contribuir a su solución.

#### Competencia específica 2.

2.1. Emplear las metodologías propias de la ciencia en la identificación y descripción de fenómenos a partir de cuestiones a las que se pueda dar respuesta a través de la indagación, la deducción, el trabajo experimental y el razonamiento lógico-matemático, diferenciándolas de aquellas pseudocientíficas que no admiten comprobación experimental.

2.2. Seleccionar, de acuerdo con la naturaleza de las cuestiones que se traten, la mejor manera de comprobar o refutar las hipótesis formuladas, diseñando estrategias de indagación y búsqueda de evidencias que permitan obtener conclusiones y respuestas ajustadas a la naturaleza de la pregunta formulada.

2.3. Aplicar las leyes y teorías científicas conocidas al formular cuestiones e hipótesis, siendo coherente con el conocimiento científico existente y diseñando los procedimientos experimentales o deductivos necesarios para resolverlas o comprobarlas.

#### Competencia específica 3.

3.1. Emplear datos en diferentes formatos para interpretar y comunicar información relativa a un proceso fisicoquímico concreto, relacionando entre sí lo que cada uno de ellos contiene, y extrayendo en cada caso lo más relevante para la resolución de un problema.

3.2. Utilizar adecuadamente las reglas básicas de la física y la química, incluyendo el uso de unidades de medida, las herramientas matemáticas y las reglas de **formulación y de** nomenclatura, consiguiendo una comunicación efectiva con toda la comunidad científica.

3.3. Poner en práctica las normas de uso de los espacios específicos de la ciencia, como el laboratorio de física y química, asegurando la salud, la conservación del medio ambiente y el cuidado de las instalaciones.

#### Competencia específica 4.
4.1. Utilizar recursos variados, tradicionales y digitales, mejorando el aprendizaje autónomo y la interacción con otros miembros de la comunidad educativa, con respeto hacia docentes y estudiantes y analizando críticamente las aportaciones de cada participante.

4.2. Trabajar de forma adecuada **y versátil** con medios variados, tradicionales y digitales en la consulta de información y la creación de contenidos, seleccionando **e interpretando** con criterio las fuentes más fiables y desechando las menos adecuadas y mejorando el aprendizaje.

#### Competencia específica 5.

5.1. Cooperar como forma de construir un medio de trabajo eficiente en la ciencia.

5.2. **Desarrollar, empleando** la metodología adecuada, proyectos científicos que involucren al alumnado en la mejora de la sociedad.

#### Competencia específica 6.

6.1. Reconocer y valorar, a través del análisis histórico de los avances científicos logrados por la humanidad, que la ciencia es un proceso en permanente construcción y que existen repercusiones mutuas de la ciencia actual con la tecnología, la sociedad y el medio ambiente.

6.2. **Analizar** en el entorno las necesidades tecnológicas, ambientales, económicas y sociales más importantes que demanda la sociedad.

### Contenidos.

**A. Las destrezas científicas básicas.**

- **Utilización de** metodologías de la investigación científica **para la** identificación y formulación de cuestiones, **la** elaboración de hipótesis y **la** comprobación experimental de las mismas. **Aplicación del método científico a experiencias sencillas.**
- **Empleo de** diversos entornos y recursos de aprendizaje científico, como el laboratorio o los entornos virtuales, **utilizando de forma correcta** los materiales, sustancias y herramientas tecnológicas **y atendiendo a las** normas de uso de cada espacio, asegurando y protegiendo así la salud propia y comunitaria, la seguridad en redes y el respeto hacia el medio ambiente.  
**• El trabajo en el laboratorio.**  
**• Estrategias de uso correcto de herramientas tecnológicas en el entorno científico.**  
**• Normas de seguridad en un laboratorio.**  
**• Identificación e interpretación del etiquetado de productos químicos.**  
**• Reciclaje y eliminación de residuos en el laboratorio.**  
- **Realización de** trabajo experimental **sencillo** y **de** proyectos de investigación **de forma guiada para desarrollar** estrategias en la resolución de problemas **mediante el uso de la experimentación**, la indagación, la deducción, la búsqueda de evidencias y el razonamiento lógico-matemático haciendo inferencias válidas de las observaciones y obteniendo conclusiones **para aplicarlas a nuevos escenarios.**
- **Uso del** lenguaje científico, **incluyendo el manejo adecuado de sistemas de unidades** y herramientas matemáticas básicas en diferentes escenarios científicos y de aprendizaje.  
**• Magnitudes derivadas. Sistema Internacional de Unidades. Cambio de unidades. Factores de conversión**  
**• Notación científica. Cifras significativas.**  
- Estrategias de interpretación y producción de información científica utilizando diferentes formatos y diferentes medios: desarrollo del criterio propio basado en lo que el pensamiento científico aporta a la mejora de la sociedad para hacerla más justa, equitativa e igualitaria.  
**• Registro de datos y resultados empleando tablas, gráficos y expresiones matemáticas.**  
**• Introducción a la elaboración de un informe científico.**  
**• Selección e interpretación de la información relevante de un texto de divulgación científica.**  

**B. La materia.**

- **Profundización en el** modelo cinético-molecular de la materia **y su relación** con los cambios de estado.  
**• Leyes de los gases.**  
**• Modelo cinético-molecular de la materia.**  
**• Cambios de estado de la materia.**  
**• Realización** de experimentos **de forma guiada** relacionados con los sistemas materiales: conocimiento y descripción de sus propiedades, su composición y su clasificación. **Mezclas y disoluciones. Concentración.**
- **Aplicación de los conocimientos sobre la** estructura atómica **de la materia** para entender **la formación de iones**, la existencia, formación y propiedades de los isótopos y ordenación de los elementos **de** la tabla periódica.  
**• Estructura atómica de la materia. Isótopos.**  
**• Tabla periódica y propiedades de los elementos.**  
**• Átomos e iones.** Masa atómica y masa molecular.  
**• Introducción al enlace químico.**  
- Principales compuestos químicos: su formación y sus propiedades físicas y químicas, así como la valoración de sus aplicaciones.  
**• Elementos y compuestos de especial interés con aplicaciones industriales, tecnológicas y biomédicas.**  
**• Aproximación al concepto de mol.**  
- Nomenclatura: participación de un lenguaje científico común y universal formulando y nombrando sustancias simples, iones monoatómicos y compuestos binarios mediante las reglas de nomenclatura de la IUPAC.

**C. El cambio.**  

- Interpretación microscópica de las reacciones químicas: explicación de las relaciones de la química con el medio ambiente, la tecnología y la sociedad.  
**• Ajuste de reacciones químicas sencillas.**  
- Aplicación de la ley de conservación de la masa **(Ley de Lavoisier)** y de la ley de las proporciones definidas **(Ley de Proust)**: aplicación de estas leyes como evidencias experimentales que **permitan** validar el modelo atómico-molecular de la materia.  
**• Cálculos estequiométricos sencillos.**  
- **Análisis de los** factores que afectan a las reacciones químicas: predicción cualitativa de la evolución de las reacciones, entendiendo su importancia en la resolución de problemas actuales por parte de la ciencia.  
**• Análisis cualitativo de la influencia de la temperatura y la concentración en una reacción química.**  

**D. La interacción.**

- Predicción de movimientos sencillos a partir de los conceptos de la cinemática, formulando hipótesis comprobables sobre valores futuros de estas magnitudes, validándolas a través del cálculo numérico, la interpretación de gráficas o el trabajo experimental.  
**• Tipos de magnitudes escalares y vectoriales.**  
**• Concepto de posición, trayectoria y espacio recorrido.**  
**• Velocidad media, velocidad instantánea y aceleración.**  
- Las fuerzas como agentes de cambio: relación de los efectos de las fuerzas, tanto en el estado de movimiento o de reposo de un cuerpo como produciendo deformaciones en los sistemas que actúan.  
**• Fuerza y movimiento.**  
**• Ley de Hooke.**  
**• Cálculo de la resultante de varias fuerzas.**  
- Aplicación de las leyes de Newton: observación de situaciones cotidianas o de laboratorio que permiten entender cómo se comportan los sistemas materiales ante la acción de las fuerzas y predecir **el efecto** de estas en situaciones cotidianas y de seguridad vial.  
**• Introducción a la Ley de la Gravitación Universal.**  
**• Maquinas simples.**  
- Fenómenos gravitatorios, eléctricos y magnéticos: experimentos sencillos que evidencian la relación con las fuerzas de la naturaleza.

**E. La energía.**

- Diseño y comprobación experimental de hipótesis relacionadas con el uso doméstico e industrial de la energía en sus distintas formas y las transformaciones entre ellas.
- Elaboración fundamentada de hipótesis sobre el medio ambiente, a partir de las diferencias entre fuentes de energía. Concienciación sobre la necesidad del ahorro energético y conservación del medio ambiente.  
**• Uso racional de la energía.**  
- Naturaleza eléctrica de la materia: electrización de los cuerpos y los circuitos eléctricos.  
**• La fuerza eléctrica: analogías y diferencias con la fuerza gravitatoria.**  
**• La electricidad como movimiento de cargas eléctricas. Ley de Ohm.**  
**• Circuitos eléctricos básicos. Asociación de resistencias.**  
**• Aplicaciones de la electricidad en la vida diaria.**  

