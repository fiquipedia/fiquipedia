
# Diagramas de Feynman

[elfactorciencia.wordpress.com ¿Qué son los Diagramas de Feynman?](https://elfactorciencia.wordpress.com/2013/01/25/que-son-los-diagramas-de-feynman/) 

[unican.es/vilarr Feynman.pdf](http://personales.unican.es/vilarr/Feynman.pdf)   
  
 [hyperphysics.phy-astr.gsu.edu Exchange Particles](http://hyperphysics.phy-astr.gsu.edu/hbasees/Particles/expar.html)   
  
 [cuentos-cuanticos.com Diagramas de Feynman para todos](https://cuentos-cuanticos.com/2011/11/07/diagramas-de-feynman-para-todos-1/)   
  
 [Diagrama de Feynman - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Diagrama_de_Feynman)   
  
 [zpath_feynman.htm](http://atlas.physicsmasterclasses.org/es/zpath_feynman.htm)   
  
 [Quantum Diaries](http://www.quantumdiaries.org/2010/02/14/lets-draw-feynman-diagams/)   
  
CompHEP: a package for evaluation of Feynman diagrams  
 [comphep:start [THEORY]](http://comphep.sinp.msu.ru/)   

