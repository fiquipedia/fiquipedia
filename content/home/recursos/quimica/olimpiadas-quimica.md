
# Olimpiadas de Química

Por completar, se pueden intentar recopilar en local problemas como en  [olimpiadas de física](/home/recursos/fisica/olimpiadas-de-fisica) aunque se enlazan recopilaciones.

## Olimpiadas Química orientación y resoluciones
* [Olimpiada Química orientaciones preparación](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/olimpiadas-quimica/oeq-local-madrid-orientaciones-preparaci%C3%B3n.pdf)
* [Olimpiada Química fase local Madrid, soluciones](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/olimpiadas-quimica/oeq-local-madrid-soluc.pdf)  


##  Información general
[rseq.org Olimpiada Española de Química](https://rseq.org/olimpiadas-de-quimica/)  

[Colegio Químicos Comunidad Valenciana](https://colegioquimicos.com/olimpiada-quimica/)  

[quimicosmadrid.org olimpiada química](https://web.archive.org/web/20171229002550/http://www.quimicosmadrid.org/index.php/eventos/olimpiada-quimica) *waybackmachine*  
[quimicosmadrid.org Información sobre la Olimpiada de Química 2020](https://quimicosmadrid.org/home/2020/02/informacion-sobre-la-olimpiada-de-quimica-2020/)  *El año pasado la RSEQ decidió que asumía en solitario la organización de las Olimpiadas de Química.*  

[losavancesdelaquimica.com olimpiadas científicas/](https://web.archive.org/web/20191215090828/http://www.losavancesdelaquimica.com/blog/category/olimpiadas-cientificas/) *waybackmachine*, parece que ahora tiene nueva web [Los Avances de la Química. Educación Científica (y algo de Historia …).](https://educacionquimica.wordpress.com/olimpiadas-cientificas/)  Bernardo Herradón García

 [RSEQ, Sección Territorial Madrid, Olimpiadas de Química](https://www.rseq-stm.es/categoria/olimpiadas-de-quimica/)  

##  Estructura de las olimpiadas
 Similar a la olimipiada física, con fases locales, fase nacional, y olimpiada internacional.  
 La olimpiada española se inicia en 1995  
 [rseq-stm.es Normativa de la Olimpiada de Química de Madrid 2020](http://www.rseq-stm.es/2020/01/normativa-de-la-olimpiada-de-quimica-de-madrid/)  
 [Programa para la Olimpiada de química de Madrid pdf](http://www.rseq-stm.es/wp-content/uploads/OQM_Programa-de-la-Olimpiada-de-Qu%C3%ADmica-de-Madrid.pdf)  
 [Bases / normas de la olimpiada de Química de Madrid pdf](http://www.rseq-stm.es/wp-content/uploads/OQM_Bases-de-la-olimpiada-de-Qu%C3%ADmica-de-Madrid.pdf) 

###  Mini-olimpiada química para 3º ESO
Lo veo por primera vez en 2015, Asturias  
[alquimicos.com miniolimpiada](http://www.alquimicos.com/olimpiadas/miniolimpiada)  
Se incluyen exámenes  

###  Fechas asociadas a las olimpiadas 2015
Se ponen las fechas para que sean orientativas para convocatorias posteriores  
Las olimpiadas de 2015 fueron las XXVIII nacionales y las 47ª internacional.  
Fase local de Madrid:
- Inscripción por fax al nº 915775137 o al correo electrónico: colquim@quimicosmadrid.org.  
- Se celebró viernes, 13 de marzo, a las 17,30 h en la facultad de Químicas de la Un. Complutense de Madrid.  
- La fase nacional se celebró en la Universidad Complutense de Madrid, el día 18 de abril de 2015.  
- La fase internacional se celebró del 20 al 29 de julio de 2015 en Bakú (Azerbayán)  [IChO 47th International Chemistry Olympiad. Baku, Azerbaijan 2015](http://www.icho2015.msu.az/)  
- La XX Olimpiada Iberoamericana de Química, se celebrará este año 2015 en Teresina (Brasil), los días 5 a 14 de septiembre.

###  Fechas asociadas a las olimpiadas 2017
 [quimicosmadrid.org Olimpiada Química 2017](https://web.archive.org/web/20180211003414/http://www.quimicosmadrid.org/index.php/eventos/olimpiada-quimica/oq-convocatoria-2017)  
 *Estimado/a profesor/a:*  
 Por medio de este correo, quiero poner en su conocimiento la convocatoria de la Olimpiada de Química de Madrid.  
 En la misma podrán participar los alumnos matriculados durante el curso 2016-17 en Bachillerato dentro del sistema educativo español.  
 La fase local de la Olimpíada de Química tendrá lugar el viernes 10 de marzo, a las 17:30 h, en el edifico C de la Facultad de Ciencias Químicas de la Universidad Complutense de Madrid, Ciudad Universitaria, s/n, 28040-Madrid, en convocatoria única para todos los centros de la Comunidad de Madrid adscritos a las Universidades de Alcalá, Autónoma de Madrid, Carlos III, Complutense, Politécnica y Rey Juan Carlos.  
 La inscripción de los alumnos se realizará mediante el envío de la ficha adjunta por fax (91 577 51 37) o e-mail ( [colquim@quimicosmadrid.org](mailto:colquim@quimicosmadrid.org) ) antes del lunes 6 de marzo.  
 Se debe rellenar una ficha por participante, pudiendo presentar cada centro un máximo de tres estudiantes. 

###  Fechas asociadas a las olimpiadas 2018
 [rseq.stm.es Olimpiada de Química de Madrid 2018](http://www.rseq-stm.es/2018/02/olimpiada-de-quimica-de-madrid-2/)  
 La fase local de la Olimpíada de Química tendrá lugar el viernes 2 de marzo de 2018, a las 17:30 h, en el edifico C de la Facultad de Ciencias Químicas de la Universidad Complutense de Madrid, Ciudad Universitaria, s/n, 28040-Madrid, en convocatoria única para todos los centros de la Comunidad de Madrid adscritos a las Universidades de Alcalá, Autónoma de Madrid, Carlos III, Complutense, Politécnica y Rey Juan Carlos.

###  Fechas asociada a las olimpiadas 2019
 [Olimpiada de Química de Madrid (convocatoria 2019)](https://www.educa2.madrid.org/web/revista-digital/convocatorias/-/visor/olimpiada-de-quimica-de-madrid-convocatoria-2019-)  
 La fase local tendrá lugar el viernes 15 de marzo, a las 17:30 h, en el edifico C de la Facultad de Ciencias Químicas de la Universidad Complutense de Madrid (Ciudad Universitaria s/n. Madrid)

###  Fechas asociadas a las olimpiadas 2020
 [rseq-stm.es Inscripción a la Olimpiada de Química de Madrid 2020](http://www.rseq-stm.es/2019/12/olimpiada-de-quimica-de-madrid-4/)  
 La fase local de la Olimpiada de Química de Madrid (OQM) tendrá lugar el viernes 6 de marzo de 2020, a las 17:30 h, en el edificio C de la Facultad de Ciencias Químicas de la Universidad Complutense de Madrid
 
###  Fechas asociadas a las olimpiadas 2021 
[Inscripción a la Olimpiada de Química de Madrid 2021 - rseq-stm.es](https://www.rseq-stm.es/2021/01/inscripcion-a-la-olimpiada-de-quimica-de-madrid-2021/)  
La olimpiada de Química de Madrid (OQM) se celebrará el viernes 5 de marzo de 2021, de manera presencial, a las 17:30 h.  
 
###  Fechas asociadas a las olimpiadas 2022
 [Olimpiada de Química de Madrid 2022 - rseq-stm.es](https://www.rseq-stm.es/2022/01/olimpiada-de-quimica-de-madrid-2022/)  

>La olimpiada de Química de Madrid (OQM) se celebrará el viernes 4 de marzo de 2022, de manera presencial, a las 17:30 h.  
>Para poder participar, el estudiante debe ser inscrito por su profesor preparador rellenando el siguiente formulario antes del 20 de febrero de 2022.  
 

##  Olimpiada nacional: fases locales y fase nacional

En [olimpiadasquimica.es problemas](https://web.archive.org/web/20171112223650/https://olimpiadasquimica.es/problemas) *waybackmachine*   había hay enlaces a información de fases locales, nacionales e internacionales  
En  [rseq.org Olimpiada Nacional de Química – Histórico](https://rseq.org/olimpiadas-de-quimica/olimpiada-nacional-de-quimica-historico/)  hay información de fases nacionales   

 Asturias  
   *  [alquimicos.com olimpiada regional](http://www.alquimicos.com/olimpiadas/olimpiada_regional) 

Castilla y León  
   *  [Enunciados de las cuestiones y de los problemas propuestos en las OLIMPIADAS DE QUÍMICA de Castilla y León. Desde el curso 1996-1997 hasta el 2012-2013 pdf](http://www.educa.jcyl.es/crol/es/recursos-educativos/olimpiada-quimica.ficheros/545400-Olimpiadas) 

Galicia  
   *  [colquiga.org PRUEBAS OLIMPIADA GALLEGA DE QUIMICA](https://www.colquiga.org/pruebas-anteriores) 

Madrid  
   *  [RSEQ Sección Territorial de Madrid](http://www.rseq-stm.es/)  
   
Valencia
   *  [uji.es EXÀMENS](http://www3.uji.es/~safont/olimpiada/examens/examens.html) 


###  Recopilación de cuestiones y problemas resueltos de distintas ediciones de olimpiadas de Fernando Latre David, Sergio Menargues Irlés, Amparo Gómez-Siurana
Se pueden ver en varios sitios:  
En 2022 la versión más actualizada en la web del colegio de Químicos de la Comunidad Valenciana 
[Colección de cuestiones y problemas resueltos de distintas ediciones de Olimpiadas Químicas. - colegioquimicos.com](https://colegioquimicos.com/problemas-resueltos/)  

En 2020 la versión más actualizada (y enlaces a las previas) en  
[uji.es MATERIAL DE PREPARACIÓ, Elaborat per Sergio Menargues i Amparo Gómez-Siurana, i Fernando Latre a les versions anteriors, de Fases Locals i Nacionals de l'Olimpíada Química](http://www3.uji.es/~safont/olimpiada/material/material.html)  

 Aparte del enlace se incluyen una copia local de los ficheros en la carpeta de drive  
 [drive.fiquipedia Olimpiadas Química](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/Olimpiadas%20Qu%C3%ADmica)
  La recopilación de problemas olimpiada de Química resueltos también se puede encontrar (en 2021 tienen los documentos de 2019) en Universidad de Alicante Departamento de Ingeniería Química Materiales  
  [diq.ua.es Universidad de Alicante Departamento de Ingeniería Química Materiales](https://diq.ua.es/es/materiales.html) 

##  Olimpiada Iberoamericana (OIAQ)
 [olimpiadasquimica.es olimpiada iberoamericana](http://web.archive.org/web/20171112234029/https://olimpiadasquimica.es/problemas/cat_view/2-problemas/8-olimpiada-iberoamericana) *waybackmachine*    
 [quimica.es Olimpiada iberoamericana de química](https://www.quimica.es/enciclopedia/Olimpiada_iberoamericana_de_qu%C3%ADmica.html)  
   
 [Olimpiadas argentinas de química. Materiales de estudio. Series de ejercitación](http://oaq.exactas.uba.ar/index.php?option=com_content&view=article&id=9&Itemid=126)  

##  Olimpiada internacional (IChO)
 [IChO 2015 Preparatory problems](http://icho2015.msu.az/problems/preparatory-problems/) 

##  Recopilación de problemas y soluciones fase local, nacional e internacional
Aparte de que se puedan localizar en alguno de los enlaces que se incluyen en la página, los problemas localizados y descargados se organizan en una carpeta que se comparte y se enlaza desde aquí
[drive.fiquipedia Olimpiadas Química](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/Olimpiadas%20Qu%C3%ADmica)



