---
aliases:
  - /home/recursos/fisica/optica/
---

# Recursos óptica

## Óptica en el currículo
El bloque  [óptica](/home/recursos/fisica/recursos-optica)  forma parte del  [currículo de física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculofisica2bachillerato) .  
Solamente se introducen algunas ideas sobre ondas y luz como onda en otros cursos, como   

## Separación entre "óptica geométrica" y "óptica física"
Se suele encontrar esta separación en algunos libros y apuntes.   
Yo comparto esa separación, y es un poco arbitraria, pero intento explicarla aquí (enlace válido en noviembre 2013)  
 [Óptica geométrica y óptica física | Óptica Geométrica ](http://s436787162.mialojamiento.es/ptica_geomtrica_y_ptica_fsica.html)   
  
Relacionado, esto enlaza con óptica geométrica y física  
 [![El mito de la recomposición de la luz blanca con dos prismas - YouTube](https://img.youtube.com/vi/VkZ1M_upCHM/0.jpg)](https://www.youtube.com/watch?v=VkZ1M_upCHM)   
El mito de la recomposición de la luz blanca con dos prismas  
Alejandro del Mazo Vivar (5 min)  
  
 [![Newton&#39;s Prism Experiment - YouTube](https://img.youtube.com/vi/uucYGK_Ymp0/0.jpg)](https://www.youtube.com/watch?v=uucYGK_Ymp0)   
Newton's Prism Experiment  
MIT (5 min)  

## Recursos óptica en general
Introducción a la óptica, presentación de Segundo de Bachillerato sobre Optica, curso 2006/2007  
 [Optica](http://www.slideshare.net/everwrest/optica-3848366)   
Esteban Benitez, Profesor en centro educativo Santa María Claret, licenciamiento no detallado  
  
Molecular Expressions. Science, optics & you.  
 [Molecular Expressions: Science, Optics and You](http://micro.magnet.fsu.edu/optics/index.html)   
© 1995-2013 by Michael W. Davidson and The Florida State University. All Rights Reserved.  
  
 [F2 OPTICA ~ FQ Libre](http://fqlibre.blogspot.com.es/p/f2-optica.html)   
 Presentación sobre óptica: espejos, lentes, leyes de snell, defectos refractivos... (pptx)  
 Hoja de ejercicios con soluciones sobre reflexión, refracción, lentes y espejos (docx)  
  
Examples > Physics >Optics  
 [Optics.html](http://www.wolframalpha.com/examples/Optics.html)   
Reflection & Refraction, Diffraction, Lasers,...  
Permite dibujar trazados de rayos  
  
 [Experimentos de fsica. ptica](http://www.madrimasd.org/cienciaysociedad/taller/fisica/optica/default.asp)   
Taller de física - óptica (combina óptica física y geométrica, e incluso efecto fotoeléctrico)  
 [<span>Principia</span>](http://principia.io/2015/03/26/ojos-para-lo-infinitesimal-ii-la-doma-de-la-luz.Ij1Ig/)   
 
[A Sundial displaying the time inside its shadow, with actual digits - thingiverse](https://www.thingiverse.com/thing:1068443)  

[IMÁGENES DE FENÓMENOS ÓPTICOS COTIDIANOS COMO APOYO DE LA DOCENCIA EN ÓPTICA. PROYECTO DE INNOVACIÓN Y MEJORA DE LA CALIDAD DOCENTE 2008 nº 35. Alfredo Luis Aina](https://webs.ucm.es/info/gioq/fenopt/index.htm)  
[Fenómenos Ópticos Cotidianos](https://fenomenos-opticos.blogspot.com/)  

[Collection of Solved Problems in Physics - optics - physicstasks.eu ](http://physicstasks.eu/en/physics/optics)  
This collection of Solved Problems in Physics is developed by Department of Physics Education, Faculty of Mathematics and Physics, Charles University in Prague since 2006.  
The Collection contains tasks at various level in mechanics, electromagnetism, thermodynamics and optics. The tasks mostly do not contain the calculus (derivations and integrals), on the other hand they are not simple such as the use just one formula. Tasks have very detailed solution descriptions. Try to solve the task or at least some of its parts independently. You can use hints and task analysis (solution strategy).  
Difficulty level  
Level 1 – Lower secondary level   
Level 2 – Upper secondary level   
Level 3 – Advanced upper secondary level   
Level 4 – Undergraduate level    

## Prácticas y talleres

Adicionales a los que puede haber en la página asociada a laboratorio  f
[Taller de Experimentos Sencillos de Óptica Experiencias Demostrativas de Óptica Física y Óptica Geométrica en Educación Básica y Media - researchgate.net](https://www.researchgate.net/publication/266138715_Taller_de_Experimentos_Sencillos_de_Optica_Experiencias_Demostrativas_de_Optica_Fisica_y_Optica_Geometrica_en_Educacion_Basica_y_Media)  

[Fundamentos de Óptica para Ingeniería Informática: PRÁCTICAS DE LABORATORIO - ua.es](https://rua.ua.es/dspace/bitstream/10045/11356/1/Practicas%20optica.pdf)  

## [Óptica física](/home/recursos/fisica/recursos-optica-fisica)

Ver también [Óptica atmosférica](/home/recursos/fisica/optica-atmosferica)  
Ver también [Óptica atmosférica](/home/recursos/fisica/optica-gnomonica)


## [Óptica geométrica](/home/recursos/fisica/optica-geometrica)

## [Óptica visión](/home/recursos/fisica/optica-vision)


## [Ilusiones ópticas](/home/recursos/fisica/recursos-optica-ilusiones)
