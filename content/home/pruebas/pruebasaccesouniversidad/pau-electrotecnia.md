---
aliases:
  - /home/pruebasaccesouniversidad/pau-electrotecnia/
---

# PAU Electrotecnia

Como se puede ver esta página está en construcción: la idea es que acabe quedado similar a las páginas de [PAU Física](/home/pruebas/pruebasaccesouniversidad/paufisica) y [PAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) , pero solamente he iniciado una recopilación parcial de enunciados de examen, en algunos casos incluyendo soluciones (no he revisado como de "oficiales" son, aunque alguna errata hay, por ejemplo 2012-SeptiembreA. Cuestión 2 indica valor final carga en μF en lugar de μC). La idea es agruparlos por bloques y resolver separando enunciados de soluciones. Los enunciados originales están a menudo escaneados, y aquí se ponen enunciados en modo texto, lo que facilita la búsqueda de ejercicios.  
Nunca he impartido la materia de electrotecnia, pero siendo una de las que oficialmente estaban asociadas a la especialidad de física y química, decidí realizar esta recopilación: cualquier persona que tenga más contacto con la materia y quiera hacer comentarios, se agradecerán.

##  **Enunciados y soluciones agrupados por bloques, elaboración propia (2002-2016)**
En 2017 PAU pasa a ser " [evaluación final bachillerato](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-bachillerato) " según LOMCE, nombre es PAU, EBAU, EvAU, EFB, ... según comunidad, pero se sigue centralizando información aquí como PAU  

Actualizados con examen modelo 2016 (última revisión 3 octubre 2015)(estos enunciados agrupan por bloques la [recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales) de electrotecnia](/home/recursos/recursospau/ficheros-enunciados-pau-electrotecnia)   

Nota 2015: en el currículo LOMCE (aprobado en  [RD 1105/2014](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2015-37) ), la materia de electrotecnia desaparece en 2º Bachillerato (es decir que el curso 2015-2016 será el último en el que se imparta en Madrid), por lo que de momento rebajo la prioridad a continuar con las resoluciones (que no toco desde agosto de 2014 y son solamente parciales), salvo el tema de recopilar enunciados completos y originales, ya que empezarán a desaparecer cuando la materia deje de impartirse y considero que es útil conservarlos.  

Sí que se guarda información en otras comunidades, que de alguna manera mantienen electrotecnia (Galicia, Cataluña ...) ver  la  [recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales) de electrotecnia](/home/recursos/recursospau/ficheros-enunciados-pau-electrotecnia)  

Nota 2024: en el currículo LOMLOE desaparece electrotecnia también en Cataluña y en cierto modo la materia que ocupa su lugar que también tiene pruebas PAU es Tecnología e Ingeniería II. Ver [PAU Tecnología e Ingenería](/home/pruebas/pruebasaccesouniversidad/pau-tecnologiaeingenieria) 
 
|  Bloque |  Enunciados |  Soluciones | 
|:-:|:-:|:-:|
| Conceptos eléctricos básicos y medidas (resistencias, condensadores, instrumentos de medida) |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursospau/ficheros-pau-electrotencia-por-bloques/E1-PAU-ConceptosElectricosBasicosMedidas.pdf)  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursospau/ficheros-pau-electrotencia-por-bloques/E1-PAU-ConceptosElectricosBasicosMedidas-soluc.pdf)  | 
| Circuitos corriente continua |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursospau/ficheros-pau-electrotencia-por-bloques/E2-PAU-Continua.pdf)  | pendiente  | 
| Conceptos electromagnetismo |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursospau/ficheros-pau-electrotencia-por-bloques/E3-PAU-ConceptosElectromagnetismo.pdf)  |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursospau/ficheros-pau-electrotencia-por-bloques/E3-PAU-ConceptosElectromagnetismo-soluc.pdf)  | 
| Circuitos corriente alterna (monofásica, trifásica) |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursospau/ficheros-pau-electrotencia-por-bloques/E4-PAU-Alterna.pdf)  |  pendiente | 
|  Máquinas (transformadores, motores) y circuitos prácticos y de aplicación |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursospau/ficheros-pau-electrotencia-por-bloques/E5-6-PAU-Maquinas.pdf)  |  pendiente |  


Nota importante: no se ofrecen garantías sobre la ausencia de erratas en las soluciones. Se agradecen comentarios sobre erratas y sobre todo, planteamientos alternativos, aclaraciones o posibles diagramas adicionales. Todos los documentos ponen en la cabecera la fecha de revisión y mail de contacto.  

Las pruebas de acceso a la universidad (PAU, también se siguen conociendo como selectividad) se rigen por unos contenidos que son básicamente los de Electrotecnia de 2º de Bachillerato.  
Asociado a eso, y por sencillez, se agrupan aquí los enunciados y soluciones por "bloques temáticos"  
Incluyo enunciados y soluciones por separado: creo que ponerlos juntos es una tentación demasiado fuerte, y tener los enunciados aislados los hace más útiles para entregar una copia a los alumnos.  
La asignación a bloques está hecha bajo mi criterio; electrotecnia es una materia asociada a la especialidad de física y química **que nunca he impartido**. Es relativamente fácil separar en bloques, aunque a veces una misma pregunta combina ideas de varias bloques, y un apartado de la pregunta se puede asociar a un bloque o a varios. La idea es que, en la secuencia habitual del curso, los ejercicios de un bloque temático se puedan hacer con lo conocido en ese momento.  

**Se comienza en agosto 2014:** 
- Inicialmente la agrupación en bloques es un borrador/agrupación provisional, algunos ejercicios/apartados pueden reubicarse en otros bloques una vez que en la solución se vea qué conceptos se utilizan. Por ejemplo un problema que había puesto inicialmente en el bloque de electromagnetismo por ser de cálculo de autoinducción de un solenoide lo he movido al bloque de alterna ya que enunciado utiliza el concepto de reactancia.  
- El primer bloque con soluciones en agosto 2014 es electromagnetismo, aparte de por ser el más corto, por ser el que enlaza directamente con física (hay algunos ejercicios que a priori podría no saberse de cuál de las dos materias son) y no requiere diagramas especiales respecto a los de física.  

Se incluyen enunciados y soluciones para cada bloque, para la Comunidad de Madrid.  

Todos estos recursos son de elaboración propia y tienen **licenciamiento Creative Commons Atribución (cc-by).**  
Las herramientas utilizadas también son libres:  [libreoffice](http://www.libreoffice.org/) ,  [inkscape](http://inkscape.org/) . .... pendiente encontrar un diagramador libre con iconos similares a los de los enunciados PAU.  
Pruebas iniciales, voy ordenando primero los que mejor me parecen (varias búsquedas en google, al final parece que me quedo con Scheme-it, que encontré buscando "electrical circuit drawing tool")  
[http://www.digikey.com/schemeit#](http://www.digikey.com/schemeit#)  : muy buen aspecto y completo. Obliga a registrarse para grabar pero no es de pago, en "Scheme-it Terms & Conditions" parece que simplemente indican que son los propietarios y pueden cambiarlo cuando quieran. Digi-Key es una empresa de distribuición de componentes, y parece que la idea es facilitar una herramienta de diagramas para luego hacer la lista de materiales y así pedírsela a ellos. Permite exportar a .png (bitmap, con baja calidad) y pdf (vectorial internamente)  
[https://www.draw.io/](https://www.draw.io/)  : edición online en navegador. Buena colección iconos eléctricos pero le falta terminación y no veo opción de conectores con ángulos rectos. Sí permite exportar .svg  
[https://www.circuitlab.com/](https://www.circuitlab.com/)  : muy buena pinta, pero es de pago, y versión evaluación no permite grabar. En resistencias inicialmente formato US pero también permite formato IEC.  
[http://pencil.evolus.vn/Default.html](http://pencil.evolus.vn/Default.html)  permite enlazar a librerías openclipart.org como  [http://openclipart.org/detail/191395/electricity_componant-by-abettik-191395](http://openclipart.org/detail/191395/electricity_componant-by-abettik-191395)  
[http://sourceforge.net/projects/tinycad/](http://sourceforge.net/projects/tinycad/)  windows pero funciona bien con wine sobre ubuntu, gran cantidad de iconos, variaciones para el mismo tipo  
[https://wiki.gnome.org/Apps/Dia](https://wiki.gnome.org/Apps/Dia)  : disponible ubuntu 14.04 sin problemas, pero iconos no muy similares  
[https://www.lucidchart.com/pages/examples/circuit-diagrams](https://www.lucidchart.com/pages/examples/circuit-diagrams)  no totalmente libre  
[https://es.libreoffice.org/descubre/draw/](https://es.libreoffice.org/descubre/draw/)  LibreOffice Draw más extensiones ... http://www.openoffice.org/fr/Documentation/Gallery/Divers/gallery_elec.zip (enlace no funciona en 2021)  
[https://code.google.com/p/freie-schaltzeichen/](https://code.google.com/p/freie-schaltzeichen/)  extensión libreoffice Draw desactualizada desde 2009 y solamente en alemán  
Por mirar  
http://opencircuitdesign.com/xcircuit/  
http://www.edrawsoft.com/electrical-drawing-software.php (versión de prueba)  
https://fritzing.org/download/ centrado en circuitos  
El conjunto de todos los ficheros se puede ver en  [drive.fiquipedia ficheros pau electrotenciapor bloques](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/PAUxComunidades/electrotecnia), pero se ponen aquí los enlaces de manera más legible  
Se ponen enlaces a versión pdf para facilitar la portabilidad.  
En estos ficheros utilizo fuentes libres: Liberation Sans en lugar de Arial y Liberation Serif en lugar de Times New Roman, espero que no haya problemas de visualización.

##   [Recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales)](/home/recursos/recursospau/ficheros-enunciados-pau-electrotecnia) 


##  Recursos externos con enunciados / soluciones
 [http://www.etsii.upct.es/antonio/html_demanda/doc_docencia/PAU.html](http://www.etsii.upct.es/antonio/html_demanda/doc_docencia/PAU.html)   
ETSII UPCT : Escuela Técnica Superior Ingeniería Industrial de la Universidad Politécnica de Cartagena  
Antonio Gabaldón  
Profesor del Dpto. de Ing. Eléctrica de la UP de Cartagena  
Pruebas de Acceso a la Universidad  
Tecnología Industrial II, Electrotecnia y Mecánica  

[PRUEBAS P.A.U. Electrotecnia - platea.pntic.mec.es](http://platea.pntic.mec.es/~jalons3/Electrotecnia/pau.htm)  
José Ignacio Alonso del Olmo  
Profesor de Tecnología en el Instituto de E.S. Julián Marías de Valladolid


Otra idea asociada a electrotecnia son enunciados de oposiciones de algunas especialidades de FP relacionadas con la electrotecnia.  
0590-124 Sistemas electrónicos  
0590-125 Sistemas electrotécnicos y automáticos  

###  Enunciados completos oficiales / originales
 Algunas páginas donde conseguir enunciados completos de electrotecnia (ver también  [Recursos PAU Genéricos](/home/recursos/recursospau/recursos-pau-genericos) ).  
 Siempre se intentan adjuntar los documentos originales (pdf, escaneados)  
 Los enunciados se pueden considerar de "dominio público", no se puede considerar que tengan derechos de autor: los elabora un grupo de personas de varias universidades, y son las mismas para toda la Comunidad de Madrid.  
 En general me centro solo en la Comunidad de Madrid. [http://www.aulataller.es/profesores-tecnologia/selectividad-PAU-bachillerato/madrid-PAU-selectividad.html](http://www.aulataller.es/profesores-tecnologia/selectividad-PAU-bachillerato/madrid-PAU-selectividad.html)  
 
 También incluyen exámenes de tecnología industrial [http://fermoya.com/electrotecnia/ejercicios/133-examenes-pau-electrotecnia.html](http://fermoya.com/electrotecnia/ejercicios/133-examenes-pau-electrotecnia.html)  
 Exámenes de Castilla La Mancha [LISTADO DE EXÁMENES DE SELECTIVIDAD. ELECTROTECNIA - iesboliches.org](http://www.iesboliches.org/tecnologia/index.php/universidad/155-universidad-de-malaga/universidad-07-listado-de-los-examenes-resueltos-de-selectividad/147-universidad-2012-13-listado-de-los-examenes-resueltos-de-selectividad#4)  
