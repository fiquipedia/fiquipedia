
# Recursos cifras significativas

Relacionado con  [recursos sobre medidas](/home/recursos/recursos-medida)  donde se trata la propagación de errores.  

[Test cifras significativas - educaplay](https://es.educaplay.com/recursos-educativos/587036-cifras_significativas.html)  


[twitter FiQuiPedia/status/1732331983211024727](https://twitter.com/FiQuiPedia/status/1732331983211024727)  
Visitante: ¿cuántos años tiene el tiranosaurio?  
Guía: 60000007 años  
Visitante: ¿cómo puede saberlo con tanta precisión?  
Guía: tenía 60 millones cuando empecé a trabajar aquí. Eso fue hace 7 años.  
![](https://pbs.twimg.com/media/GAp6wiyXgAAiISm?format=jpg)  

Me comentan que aparece en el libro "Aproxímate" de Javier Fernández Panadero, 2016, ISBN 9788483935590  
>Unos padres y sus hijos entran a un museo.  
>El guía les enseña un esqueleto y les dice:  
>- Este dinosaurio es de hace cien millones de años y una semana.  
>La familia alucina, claro.  
>- ¿Cómo que cien millones de años y una semana?  
>- Sí, es que cuando empecé a trabajar aquí me dijeron que el dinosaurio tenía cien millones de años de antigüedad ... y llevo una semana.  


[Iranian Coca Cola labels report not only volume but also volume uncertainty](https://www.reddit.com/r/mildlyinteresting/comments/7obe20/iranian_coca_cola_labels_report_not_only_volume/)  
En etiqueta de coca cola de Irán se indica 300,0 ml ± 7,5 ml

En comentarios se indica que la "e" europea significa eso  
[What does the e-sign mean on a label ?](http://www.food-info.net/uk/qa/qa-pr5.htm)  
[COUNCIL DIRECTIVE of 20 January 1976 on the approximation of the laws of the Member States relating to the making-up by weight or by volume of certain prepackaged products (76/211/EEC) ](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:01976L0211-20190726#tocId14)  
2.4. The tolerable negative error in the contents of a prepackage is fixed in accordance with the table below:  

[twitter martinbroadley/status/1103343875316424706](https://twitter.com/martinbroadley/status/1103343875316424706)  
![](https://pbs.twimg.com/media/D0_dZ4TXgAA9rwl?format=jpg)  
Agua mineral en Pakistán  

[twitter manullinas/status/1151832654913794049](https://twitter.com/manullinas/status/1151832654913794049)  
![](https://pbs.twimg.com/media/D_whpp9U0AEGH9y?format=jpg)  
Etiqueta de lata de pepsi en Teherán  
 
[twitter FiQuiPedia/status/1151888912543080450](https://twitter.com/FiQuiPedia/status/1151888912543080450)  
![](https://pbs.twimg.com/media/D_xU26bWwAAW7C8?format=jpg)  
  
Es un contenido muy importante, que se puede ver en primariaUso de cifras significativas  
 [![Uso de cifras significativas - YouTube](https://img.youtube.com/vi/L2zc0obqX_g/0.jpg)](https://www.youtube.com/watch?v=L2zc0obqX_g)  Cómo y por qué usar cifras significativas en lugar de decimales para "truncar" resultados. Nivel primaria y secundaria.  
Es algo que aparece explícitamente en currículo de  [Física y Química 4ºESO (LOMCE)](/home/materias/eso/fisicayquimica-4eso/curriculo-fisica-y-quimica-4-eso-lomce)   
>6. Expresar el valor de una medida usando el redondeo y el número de cifras significativas correctas.  
>6.1. Calcula y expresa correctamente, partiendo de un conjunto de valores resultantes de la medida de una misma magnitud, el valor de la medida, utilizando las cifras significativas adecuadas."  

También en  [Desarrollo experimental 2º Bachillerato (LOMCE Madrid)](/home/materias/bachillerato/desarrollo-experimental-2-bachillerato) Ahí aparecen temas relacionados con tratamiento datos experimentales y error, relacionado (pendiente separar a página aparte)  

>7. Discriminar datos experimentales siguiendo criterios de exactitud y precisión y usando el criterio Dixon.  
>7.1. Elimina datos obtenidos aplicando criterios científicos.  
>8. Manejar el criterio t de Student añadiendo el margen de error a las medidas.  
>8.1. Añade a cada resultado un margen de error."  

[Dixon's Q test - Wikipedia](https://en.m.wikipedia.org/wiki/Dixon%27s_Q_test)  [Prueba t de Student - Wikipedia, la enciclopedia libre](https://es.m.wikipedia.org/wiki/Prueba_t_de_Student)   
  
En las  [olimpiadas de Física](/home/recursos/fisica/olimpiadas-de-fisica)  dentro del bloque experimental se trata "Errores: absoluto y relativo. Cifras significativas. Propagación de errores."  

La realidad es que hay que realizar cálculos de propagación de errores. En modo práctico una regla muy básica/simplificada sería que el número de cifras significativas del resultado final no puede ser superior al peor número de cifras significativas de los datos de partida. Lo que hay que tener en cuenta es que a veces se dan valores de partida que se pueden asumir exactos (se indica 1 pero se puede entender 1,000). En [recursos para oposiciones](/home/recursos/recursos-para-oposiciones) se suele comentar sobre cifras significativas en la resolución.   

Este resumen sobre EvAU Física las cita [DivulgaMadrid: FÍSICA 2º BACHILLERATO: Cómo preparar el examen de la EvAU](http://divulgamadrid.blogspot.com/2018/05/como-preparar-fisica-evau.html) ...  
> Acuérdate de redondear con 3 cifras significativas (6 en los problemas de defecto de masa y en todos aquellos en los que haya que restar números parecidos, ya que eso hace que perdamos cifras significativas y tenemos que garantizar que al final quedan el menos 3 vivas). ....  

En EBAU CyL no se contemplan en los criterios de corrección  

[twitter RadiactivoMan/status/1073698393065709568](https://twitter.com/RadiactivoMan/status/1073698393065709568)  
Cierto que en ESO se ve de forma sucinta cifras sig. y errores, para inmediatamente olvidarlo. Todos los problemas de los libros de texto están planteados y resueltos haciendo caso omiso.  
En EBAU (en CyL) los criterios de corrección dicen... ¡Que no se tenga en cuenta! 😱

En Bachillerato Internacional sí  
[twitter RadiactivoMan/status/1073699630897074176](https://twitter.com/RadiactivoMan/status/1073699630897074176)   
 Un artículo sobre homeopatía me parece interesante para nivel avanzado   
 [Otra prueba de las bases físicas de la homeopatía (o no)](http://elprofedefisica.naukas.com/2016/02/15/otra-prueba-de-las-bases-fisicas-de-la-homeopatia-o-no/)   
  
*Por supuesto, si la sensibilidad del instrumento (lo mínimo que puede apreciar) es una cantidad mayor, ésta es la que te va a limitar la precisión. Es decir, no me importa que midan varias veces, hagan los números y obtengan un valor de 0,00003 ± 0,00016, porque si lo mínimo que puede apreciar tu instrumento es 0,001 el verdadero valor es 0,00003 ± 0,001; y eso en mi pueblo se redondea como 0,000 ± 0,001. Harto estoy de explicárselo a mis alumnos cada año, para que ahora ventan unos aprendices de científico a tomarme el pelo.*  
  
 [Otra prueba de las bases físicas de la homeopatía (o no) - elprofedefisica.naukas.com](http://elprofedefisica.naukas.com/2016/02/15/otra-prueba-de-las-bases-fisicas-de-la-homeopatia-o-no/)   
 [El complejo mundo de las cifras significativas - orbitalesmoleculares.com](https://www.orbitalesmoleculares.com/complejo-mundo-las-cifras-significativas/)   
Apartado "Cifras significativas" de la [Introducción de Resúmenes de Química General (1º Farmacia Plan antiguo), Ernesto de Jesús Alcañiz ](https://edejesus.web.uah.es/resumenes/QG/intro.pdf) en [Páginas de Ernesto de Jesús Alcañiz; Resúmenes de Química General (1º Farmacia Plan antiguo)](https://edejesus.web.uah.es/resumenes/QG.htm)   

--- 
**Cifras significativas**  
 Los números utilizados en química se dividen en exactos e inexactos. Números exactos son aquellos cuyos valores están fijados con precisión por definición (esto es, por acuerdo convencional entre los químicos). Por lo general tienen valores enteros. Por ejemplo, son números exactos los que relacionan dos cantidades dentro del mismo sistema de unidades: 1 m = 100 cm = 1000 mm = 1 1010 Å. No existe error o incertidumbre en estos números.   
 Los números inexactos proceden de medidas experimentales, que siempre introducen algún grado de error o incertidumbre. Algunas cantidades se conocen con gran exactitud, como la velocidad de la luz que es (2,99792458 ± 0,00000001) 10^8 m s–1, pero siempre existe la posibilidad de que experimentos posteriores obliguen a cambiar un valor aceptado.  
 En un trabajo muy exacto, se puede indicar los límites de error de una magnitud medida poniendo ±0,01, ±0,0002, etc., después del número. En un trabajo normal, se hace esto utilizando cifras significativas, esto es, por medio del número de dígitos que se utilizan al escribir un número. La elección del número de cifras significativas que se empleen estará basada en la estimación del máximo error probable de los datos iniciales sobre los que se basa el resultado. Escribir 3,6521 g (5 cifras significativas) implica 3,6521 ± 0,00005 g, es decir, que el valor verdadero está entre 3,65205 y 3,65215 g. Obviamente, si se ha determinado la masa en una balanza menos precisa, se indicará con menos cifras, tal como 3,65 g (3 cifras significativas).  
 Nótese que la posición de la coma no tiene importancia en la determinación del número de cifras significativas. Los números 365, 36,5, 3,65, 0,365 y 0,0365 tienen todos 3 cifras significativas. Los ceros situados delante no son significativos, pero sí lo situados detrás: 0,3650 tiene 4 cifras significativas. El valor verdadero está entre 0,36405 y 0,36505.  
 Puede presentarse cierta ambigüedad cuando se encuentran ceros al final de un número. ¿Se consideran cifras significativas o sirven meramente para fijar la posición de la coma? Así, la población de una ciudad se indica como 237 000. ¿Cómo se interpreta esto?¿Tres cifras significativas, cuatro, cinco o seis? Esta confusión se evita escribiendo el número en forma exponencial: 2,37 105 (tres cifras significativas), 2,370 105 (cuatro c. s.), etc.  
 El trabajo con números inexactos ocasiona muchos problemas, especialmente con la introducción de calculadoras. Los pequeños números que se presentan con luces coloreadas parecen muy impresionantes, pero ¿significan algo en realidad? La regla básica es muy sencilla: El cálculo no puede dar un resultado que sea más exacto que los datos sobre los que se basa. Así, es fácil de ver en una calculadora que 1,731 x 1,14 = 1,97334, y aritméticamente esto es correcto; pero como cálculo científico práctico está equivocado, porque la respuesta conlleva mayor grado de exactitud que el que está justificado por los datos iniciales. Se debería escribir 1,97, con sólo tres cifras significativas .  
 Cifras significativas en la adición y sustracción. La exactitud absoluta de la respuesta ha de estar relacionada con la exactitud absoluta de cada uno de los números utilizados. Se puede deducir su error del error absoluto del menos exacto de éstos:  
 15,51 + 0,065 + 0,001 = 15,58 0,6631 + 0,04113 + 0,0223 = 0,7265  
 En el primer caso, el número menos exacto sobre una base absoluta es 15,51 (exacto hasta la segunda cifra decimal), aunque los otros dos números tienen menos cifras significativas. El segundo ejemplo es similar. Nótese la manera de «redondear» que se ha empleado. Las sumas dan 15,576 y 0,72653, respectivamente. Cuando éstas se redondean al último dígito significativo, 15,576 es «redondeado por exceso» hasta 15,58 y 0,72653 es «redondeado por defecto» hasta 0,7265. Si el dígito siguiente a la última cifra significativa es 5 o mayor, se redondea por exceso; si es menor que 5, por defecto.   
 Cifras significativas en la multiplicación y división. Por lo general, la respuesta llevará igual número de cifras significativas que el número más pequeño de cifras significativas presentes en los datos iniciales. Así, en el ejemplo indicado al comienzo de esta sección, se debía escribir  
 1,731 x 1,14 = 1,97 ó 1,731 x 1,146 = 1,984.  
 (4 c.s.) (3 c.s.) (3 c.s.) (4 c.s.) (4 c.s.) (4 c.s.)  
 Lo mismo para la división:  
 1,731/1,14 = 1,52 1,731/1,146 = 1,510  
8,7285 x 1,133 10–6 x 5,81/(47,36 x 8,99 10–4) = 1,35 10–3.  
 El sentido común recomienda ligeras desviaciones de las reglas anteriores en ciertos casos:  
 99,3 x 1,079 = 107,1 porque el primer número está próximo a tener 4 cifras significativas (100,0).  
 103,8/1,108 = 93,7 por la razón contraria a la del caso anterior.  
 Cuando se efectúa un cálculo que exige varias etapas, cabe que a veces se introduzcan pequeños errores por redondeo de los resultados intermedios. Por ejemplo:  
 6,62/(40,0 x 0,200) = 0,8275 = 0,828 (3 c.s.)  
 Pero si el mismo cálculo se hace por etapas:  
 6,62/40,0 = 0,1655 = 0,166 (3 c.s.) 0,166/0,200 = 0,830 (3 c.s.)  
 Obviamente la primera contestación (0,828) es la correcta. Este problema se evita empleando más cifras de las significativas en los cálculos intermedios, y redondeando sólo las respuestas:  
 6,62/40,0 = 0,1655 = 0,166 (3 c.s.) 0,1655/0,200 = 0,828 (3 c.s.)  

---
  
[How Many Decimals of Pi Do We Really Need? - Edu News | NASA/JPL Edu](https://www.jpl.nasa.gov/edu/news/2016/3/16/how-many-decimals-of-pi-do-we-really-need/)  
>To start, let me answer your question directly. For JPL's highest accuracy calculations, which are for interplanetary navigation, we use 3.141592653589793.   

Se citan cifras significativas en [Libros Química 2ed 1.5 Incertidumbre, exactitud y precisión de las mediciones - openstax.org](https://openstax.org/books/qu%C3%ADmica-2ed/pages/1-5-incertidumbre-exactitud-y-precision-de-las-mediciones)  

 
