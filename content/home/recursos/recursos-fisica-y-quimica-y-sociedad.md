# Recursos Física y Química y Sociedad

Aparece en currículo explícitamente, por ejemplo en  [Currículo 3º ESO Física y Química](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce)  
>7.3. Defiende razonadamente la influencia que el desarrollo de la industria química ha tenido en el progreso de la sociedad, a partir de fuentes científicas de distinta procedencia.  

Algunos enlaces, pueden ser de Física o de Química  

¿Cómo sería la pandemia del COVID-19 sin una Industria Química potente?  
[https://javierguerreroruano.blogspot.com/2020/05/como-seria-la-pandemia-del-covid-19-sin.html](https://javierguerreroruano.blogspot.com/2020/05/como-seria-la-pandemia-del-covid-19-sin.html)   
 
