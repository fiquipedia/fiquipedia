---
aliases:
  - /home/pruebas-libres-graduado-eso/
---

# Pruebas Libres Graduado ESO

Relacionado con  [pruebas acceso Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio) , ámbito científico en diversificación, FP Básica, educación de adultosLas pruebas tienen una parte matemática / científico / tecnológica, y también pueden suponer un repertorio de ejercicios útiles, ya que una vez publicadas son de dominio público, sin derechos de autor.  
En 2015 empiezo a agrupar algunos ejercicios (los más asociados a física y química) y subir aquí los enunciados.Los agrupo en hojas que sean reutilizables, separando por bloques de contenidos aunque no coincidan con los del currículo.  
En 2017 separo "cinemática" de "dinámica y presión", que inicialmente tenía agrupados ya que tiene ejercicios que combinan ideas.

**Pendiente separar información sobre pruebas libres Título Bachiller**

[Normativa actualizada de aplicación en la regulación de pruebas libres (Graduado ESO y Título Bachiller)](https://www.comunidad.madrid/servicios/educacion/regulacion-pruebas-libres)


##  [Normativa y currículo Pruebas Libres Graduado ESO](/home/pruebas/pruebas-libres-graduado-eso/normativa-y-curriculo-pruebas-libres-graduado-eso) 

##  Enunciados agrupados por bloques, elaboración propia (parte científico - tecnológica)
Se agrupan en bloque que no coinciden con los contenidos del currículo, pero se trata de que las hojas sean más reutilizables. Algunos bloques son similares a los realizados agrupando enunciados de las  [pruebas de Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio) 

|  Bloque |  Enunciados | Soluciones |
|:-:|:-:|:-:
| Unidades |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-Unidades.pdf "Unidades")  | [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-Unidades-soluc.pdf "Unidades soluciones") |
| Materia y estados |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-materia-estados.pdf "Materia y estados")  | [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-materia-estados-soluc.pdf "Materia y estados soluciones") |
| Mezclas, elementos y compuestos |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-mezclas.pdf "Mezclas, elementos y compuestos")  | [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-mezclas-soluc.pdf "Mezclas, elementos y compuestos soluciones") |
| Átomo y enlace |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-%C3%A1tomo-enlace.pdf "Átomo y enlace")  | [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-%C3%A1tomo-enlace-soluc.pdf "Átomo y enlace soluciones")  |
| Reacciones químicas |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-reacciones.pdf "Reacciones químicas")  | [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-reacciones-soluc.pdf "Reacciones químicas soluciones") 
| Cinemática |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-cinem%C3%A1tica.pdf "Cinemática")  | [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-cinem%C3%A1tica-soluc.pdf "Cinemática soluciones") 
| Dinámica, presión |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-din%C3%A1mica-presi%C3%B3n.pdf "Dinámica, presión")  | [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-din%C3%A1mica-presi%C3%B3n-soluc.pdf "Dinámica, presión soluciones") |
| Energía |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-energ%C3%ADa.pdf "Energía")  | [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-energ%C3%ADa-soluc.pdf "Energía soluciones") |
| Electricidad |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-Electricidad.pdf "Electricidad")  | [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/pruebas-libres-graduado-eso/ESO-PruebaLibre-Electricidad-soluc.pdf "Electricidad soluciones") |


##  Recopilación local de enunciados completos / originales ámbito científico-tecnológico
Los enunciados se almacenan por separado en [drive.fiquipedia ESOLibrexComunidades](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/ESOLibrexComunidades)  

##  Recursos externos con enunciados/soluciones


###  Aragón
 [epa.educa.aragon.es Prueba para la obtención del título de Graduado en ESO, exámenes de años anteriores](https://epa.educa.aragon.es/educapermanente/pruebaeso/modelos_de_prueba.html) 

###  Madrid
 [http://cepaalcala.blogspot.com.es/](http://cepaalcala.blogspot.com.es/) CEPA "Don Juan I", Alcala de Henares. cc-by-nc-saEn 2014 tiene pruebas dese el año 2002, enunciados y solucionesIndica que las pruebas libres de Graduado en Secundaria (ESO) empezaron en Madrid en el año 2002, sustituyendo a las pruebas libres de Graduado Escolar  
