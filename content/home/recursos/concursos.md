
# Concursos

Puede haber algún concurso dirigido solo a docentes, se intentarán separar  

Se intentan poner por orden alfabético omitiendo a veces la palabra "concurso"

## CANSAT

[CANSAT - esero.es](https://esero.es/cansat-2/)  
> El desafío CanSat es una iniciativa de la Agencia Espacial Europea que desafía a estudiantes de toda Europa a construir y lanzar un mini satélite del tamaño de una lata de refresco.  
Un CanSat es una simulación de un satélite real, integrado dentro del volumen y la forma de una lata de refresco. El desafío para los estudiantes es adaptar todos los subsistemas principales que se encuentran en un satélite, como la energía, los sensores y un sistema de comunicación, dentro de este espacio tan reducido ¡No olvides incluir el paracaídas! El CanSat debe recuperarse sano y salvo.  

En CANSAT 2022-2023 el curso para profesores que participaron en la edición anterior CANSAT: LANZA TU SATÉLITE AL ESPACIO y/o en la Competición Regional indicaba que recibirán un kit con 2 sensores y módulo de comunicación radio.  

[CANSAT: TU SATÉLITE EN ÓRBITA: Curso 2023/2024- ESO y Bachillerato](https://innovacionyformacion.educa.madrid.org/node/647439)  
> Este curso está destinado a profesores de centros públicos que impartan docencia en el curso 2023/24 y quieran participar en la competición CANSAT. Los profesores seleccionados que participen por primera vez recibirán formación y la dotación de un kit con todo el material necesario para poder participar en CanSat.  
También podrán realizar el curso aquellos profesores que participaron en la edición anterior CANSAT: LANZA TU SATÉLITE AL ESPACIO y/o en la Competición Regional. En este caso, recibirán un acelerómetro digital.  
Durante esta formación, al profesor se le guiará en todos los aspectos técnicos y de programación para que sea capaz de construir y programar su CanSat.  

## Cazabulos

[cazabulos.es](https://www.cazabulos.es/)  
> Cazabulos anima a estudiantes de 1º y 2º de la ESO (así como a sus equivalentes en países latinoamericanos) a implicarse en la detección de bulos científicos en redes sociales, especialmente en TikTok.  
¡Con Cazabulos conseguirás que no te la cuelen!  
El Consejo Superior de Investigaciones Científicas (CSIC), a través de su Vicepresidencia Adjunta de Cultura Científica y Ciencia Ciudadana, y Big Van Ciencia impulsan este proyecto, que cuenta con la colaboración de la Consejería de Educación, Formación Profesional, Actividad Física y Deportes del Gobierno de Canarias.   

## CienciaClip
Un concurso de vídeos divulgativos de ciencia diseñados, producidos y protagonizados por estudiantes de Educación Secundaria (incluye bachillerato)   
 [http://cienciaclip.naukas.com/)](http://cienciaclip.naukas.com/) 

## Ciencia en Memes CSIC

[Participa en la VI edición de #CienciaenMemes, el concurso veraniego de humor científico](https://www.csic.es/es/agenda-del-csic/participa-en-la-vi-edicion-de-cienciaenmemes-el-concurso-veraniego-de-humor-cientifico)  

## Concursos CERN para estudiantes: CERNland, Beamline for schools
 [390-concursos-cern-para-escolares-y-estudiantes-de-secundaria](https://rsef.es/item/390-concursos-cern-para-escolares-y-estudiantes-de-secundaria)  
 [Home | A Beamline for Schools](https://beamlineforschools.cern/)  
 [Concurso CERN Fundación Príncipe de Asturias](http://www.cernland.net/concurso/) 
 
## CERTAMEN DE PROYECTOS EDUCATIVOS DE QUÍMICA. Facultad de Ciencias Químicas. UCM
[I CERTAMEN DE PROYECTOS EDUCATIVOS DE QUÍMICA. Facultad de Ciencias Químicas. UCM](https://quimicas.ucm.es/file/certamen-educativo_facultad-ciencias-quimicas_2022-23?ver)  
> Los proyectos podrán ser presentados por estudiantes de 3º, 4º de la ESO y 1º de Bachillerato de centros educativos de la Comunidad de Madrid.  

## [Concurso de Cohetes de Agua, Centro de Entrenamiento y Visitantes (NASA)](https://www.mdscc.nasa.gov/index.php?Section=Eventos&Id=69) 

## Concurso de cristalización en la escuela  

 [Inicio (Objetivos) | Concurso de Cristalización en la Escuela. (UAH, UAM, UCM) Madrid](https://www.ucm.es/concursocristalizacionmadrid/inicio)  
 [www.lec.csic.es/concurso/](http://www.lec.csic.es/concurso/) 

## CV Europass Científico - tecnológico 
¿Cómo habría sido el currículum de Albert Einstein? ¿Y el de Marie Curie o el de Arquímedes de Siracusa? Este concurso pretende dar a conocer el CV Europass entre estudiantes de FP, Educación Secundaria y Bachillerato, animándoles a reflexionar sobre cómo realizar un currículum vitae y cómo reflejar en él aptitudes, experiencias y conocimientos.  
¿Te animas a realizar el CV de algún personaje famoso del ámbito científico-tecnológico?  
[Concursos Europass - Iniciativas - Servicio Español para la Internacionalizaci&oacute;n de la Educaci&oacute;n](http://sepie.es/iniciativas/europass/concursos.html) 

## Detectives climáticos

[Detectives climáticos - esero](https://esero.es/concursos/detectives-climaticos/)  
> La ESA invita al profesorado y alumnado a unirse a Climate Detectives durante el curso escolar. En este proyecto estudiantes de entre 6 y 19 años adoptan el papel de Detectives Climáticos mientras aprenden sobre el medioambiente de la Tierra. Para ello, identificarán un problema climático local, lo investigarán utilizando imágenes de satélite reales o sus propias medidas terrestres y finalmente propondrán acciones para ayudar a reducir o monitorear el problema. Al final, los equipos compartirán sus resultados con la comunidad de Detectives Climáticos de la ESA en la plataforma del proyecto. De esta forma, otros podrán aprender de su trabajo y los estudiantes también pueden crear conciencia sobre el problema que han investigado.  


## Divulgación Gastrofísica
[Concurso de Divulgación Gastrofísica](https://estudiantes.rsef.es/Gastrofisica/)  

## [Éste es mi invento - ITEFI CSIC](https://www.openlab.itefi.csic.es/este-es-mi-invento)  

## Fotciencia

[FOTCIENCIA](https://www.fotciencia.es)  
> FOTCIENCIA es una iniciativa organizada por el Consejo Superior de Investigaciones Científicas (CSIC) y la Fundación Española para la Ciencia y la Tecnología (FECYT), con la colaboración de la Fundación Jesús Serra, Grupo Catalana Occidente. El objetivo de FOTCIENCIA es acercar la ciencia a la ciudadanía mediante la fotografía científica. También es objetivo de esta iniciativa promover entre la comunidad científica la importancia de divulgar su trabajo al conjunto de la sociedad.  

[Abierto el plazo de participación en FOTCIENCIA20. Las fotografías seleccionadas recibirán hasta 1500 euros y se incluirán en un catálogo y una exposición itinerante](https://www.csic.es/es/actualidad-del-csic/abierto-el-plazo-de-participacion-en-fotciencia20)  

[![](https://img.youtube.com/vi/W7lmWI_p1Ik/0.jpg)](https://www.youtube.com/watch?v=W7lmWI_p1Ik "FOTCIENCIA20 - ¡Abierto el plazo de participación!")  

## [La ingeniería en tus manos](https://laingenieriaentusmanos.com/)   

## [Náboj junior](https://junior.naboj.org/es/es/)  
> Náboj Junior (/náboi/) es una competición internacional de matemáticas y física diseñada para equipos (puede ser más de uno por centro) de hasta 4 estudiantes que cursen principalmente 2.º y 3.º de ESO (13-14 años). La competición dura 120 minutos. Al inicio de la competición, cada equipo recibe 6 problemas. Tan pronto como un equipo resuelva correctamente cualquiera de los problemas, recibirá uno nuevo. El equipo que resuelva, en el tiempo establecido, el mayor número de problemas correctamente saldrá victorioso.  

[Ejemplos de problemas](https://junior.naboj.org/es/es/problemas/)  

## Nanocientíficas

[I concurso 'Nanocientíficas en 60 segundos': descubre a una investigadora y crea tu vídeo](https://www.csic.es/es/agenda-del-csic/i-concurso-nanocientificas-en-60-segundos)  

## [Olimpiadas de física](/home/recursos/fisica/olimpiadas-de-fisica) 

## [Olimpiadas de química](/home/recursos/quimica/olimpiadas-quimica) 

## Olimpiada Científica Juvenil Internacional
La Olimpiada Científica Juvenil Internacional (IJSO, por sus siglas en inglés) es la única olimpiada internacional oficialmente reconocida cuyo público objetivo son estudiantes de la ESO, una etapa en la que las vocaciones científicas todavía están indefinidas y hay margen para descubrirse a uno mismo.  
[IJSO - quintescience.es](http://quintescience.es/ijso/)   

## Olimpiada Científica Turina para estudiantes de 4º de ESO
[II Olimpiada Científica Turina para estudiantes de 4º de ESO (curso 2022-2023)](https://turinadiario.blogspot.com/2023/01/olimpiada-cientifica-turina-22-23.html)  
[Soluciones de la I Olimpiada Científica Turina](http://turinadiario.blogspot.com/2022/04/soluciones-de-la-i-olimpiada-cientifica.html)  

## Olimpiada Científica de la Unión Europea
 [XIII Olimpiada Científica de la Unión Europea](http://educalab.es/-/xiii-olimpiada-cientifica-de-la-union-europea)   
 Los enlaces a euso no están operativo en 2022  

## [Olimpiadas telecomunicaciones](https://olimpiadasteleco.com/)  

> Desarrollar un prototipo programado en cualquier plataforma programable con el IDE de Arduino con el que se pongan de manifiesto cómo las telecomunicaciones ayudan al cumplimiento de los ODS 11: CIUDADES Y COMUNIDADES SOSTENIBLES

## OnZientzia  

Concurso de vídeos, con categorías vídeo de divulgación, vídeo en euskera, joven (menores de 18 años)  
 [index.asp](http://www.onzientzia.tv/index.asp?lang=ES) 

## Premio a la Tarea Educativa «Física y Química para el Desarrollo Sostenible» 
[El GEDH convoca el Premio a la Labor Educativa «Física y Química para el Desarrollo Sostenible»](https://gedh.rseq.org/el-gedh-convoca-el-premio-a-la-labor-educativa-fisica-y-quimica-para-el-desarrollo-sostenible/)  

## PROYECTOS de “QUÍMICA con EXCELENCIA” - quimicosmadrid.org

[I CONCURSO de PROYECTOS de “QUÍMICA con EXCELENCIA” - quimicosmadrid.org](https://quimicosmadrid.org/home/i-concurso-de-proyectos-de-quimica-con-excelencia/)  
[Bases del concurso](https://quimicosmadrid.org/home/wp-content/uploads/2023/10/Bases-Concurso.pdf)  

## Química, Sociedad y Medio Ambiente
 [Proyecto CEI “Energía Inteligente” &raquo; ALUMNOS DE SEGUNDO DE BACHILLERATO SE DAN CITA CON LA QUÍMICA EN LA URJC](http://www.campusenergiainteligente.es/actualidad/news/alumnos-de-segundo-de-bachillerato-se-dan-cita-con-la-quimica-en-la-urjc/)   

## Reacciona
[Reacciona - cac.es](https://www.cac.es/es/web/reacciona.html)  

PRESENTACIÓN Y OBJETIVOS DEL CONCURSO

La Sección Territorial de Valencia de la Real Sociedad Española de Química (RSEQ-STVAL) y la Ciutat de les Arts i les Ciències ofertan la duodécima edición del concurso Reacciona!, con el fin de incentivar a los estudiantes de 3º y 4º de Secundaria, Bachillerato y ciclos formativos de Formación Profesional Grado Medio de toda España, a presentar un material multimedia (duración máxima de 5 minutos) que exponga, de forma didáctica, creativa y original, algún proceso químico; en especial, aquellos cuya contribución al bienestar de la sociedad sea más patente o resulten más formativos e ilustrativos.

Esta iniciativa pretende incrementar la apreciación pública de la química como herramienta fundamental para satisfacer las necesidades de la sociedad, promover el interés por la química entre los jóvenes, y generar entusiasmo por el futuro creativo de la química. La primera edición del concurso se inició en 2011 con motivo del Año Internacional de la Química.


CALENDARIO

Plazo de inscripción: desde el 26 de septiembre hasta el 15 de febrero de 2023

Presentación de trabajos: del 15 de febrero al 24 de marzo de 2023

Evaluación del jurado y entrega de premios: 5 de mayo de 2023

## Spanish Space Design Competition

[Spanish Space Design Competition 2025](https://www.educa2.madrid.org/web/revista-digital/inicio/-/visor/spanish-space-design-competition-2025)  



El sábado 18 de enero de 2025 se va a celebrar la Spanish Space Design Competition 2025, una iniciativa de la Spanish Space Initiative (SPSIN), en colaboración con la Consejería de Educación, Ciencia y Universidades de la Comunidad de Madrid. Se trata de una competición educativa de excelencia, cuyo objetivo es que los estudiantes resuelvan problemas técnicos y sociales de forma creativa, enfrentando retos similares a los que se viven en proyectos reales del sector espacial. 

Esta competición busca recrear el entorno de trabajo de la industria aeroespacial actual. El evento se desarrollará en la Escuela Técnica Superior de Ingeniería Aeronáutica y del Espacio (UPM) de Madrid, el día 18 de enero de 2025 en horario de 8:00 a 21:00.

DESTINATARIOS

Estudiantes de 4º de ESO, 1º y 2º de Bachillerato, o alumnos de FP de entre 15-18 años (máximo 20 estudiantes por centro). 

DESARROLLO DE LA COMPETICIÓN

Los alumnos deben ir con un profesor o, si no es posible, presentar una autorización firmada por sus padres o tutores.

Los estudiantes trabajarán en equipos para diseñar colonias espaciales del futuro, enfrentándose a retos técnicos y sociales inspirados en la industria aeroespacial real.

El evento se desarrollará, en su gran mayoría, en inglés, y los proyectos serán presentados ante un jurado de expertos del sector, quienes evaluarán su creatividad, viabilidad y estructura.

PREMIOS

Los equipos ganadores representarán a España en la fase internacional, que se celebrará en el Kennedy Space Center, Florida, EE.UU.

## Tu experimento en un globo sonda

[Tu experimento en un globo sonda](https://innovacionyformacion.educa.madrid.org/proyectos/innovacion-educativa/experimento-un-globo-sonda)  
> La Consejería de Educación, Ciencia y Universidades de la Comunidad de Madrid, a través de la Dirección General de Bilingüismo y Calidad de la Enseñanza, propone a los docentes de centros de Secundaria, Bachillerato y Formación Profesional públicos de la Comunidad de Madrid participar en el proyecto STEM "Tu experimento en un globo sonda", cuyo objetivo es subir un experimento a la estratosfera (unos 30.000m) con las condiciones de espacio cercano que ello supone.    

## [Yincana virtual “Entre Matraces” - IQM - CSIC](https://entrematraces.iqm.csic.es/)

> Si eres estudiante de 4º de la ESO, 1º de Bachillerato o Ciclo Formativo de Grado Medio, te gusta la Química y tienes ganas de aprender… ¡Este es tu concurso! 


El plazo de inscripción comienza el lunes 30 de septiembre de 2024 a las 10.00h y finaliza el martes 15 de octubre de 2024 a las 24.00h.  
    La Yincana dará comienzo el lunes 14 de octubre de 2024.  
    La entrega de Premios tendrá lugar el viernes 17 de enero de 2025.  
    Prueba 1: Inicio 14/10; fin 22/10  
    Prueba 2: inicio 21/10; fin 29/10  
    Prueba 3: inicio 21/10; fin 29/10  
    Prueba 4: inicio 28/10; fin 5/11  
    Prueba 5; inicio 4/11; fin 12/11  
    Prueba 6: inicio 11/11; fin 19/11  
    Prueba 7: inicio 18/11; fin 26/11  
    Prueba 8: inicio 25/11; fin 3/12  
    Prueba 9: inicio 2/12; fin 17/12  



