
# Currículo Ciencias para el Mundo Contemporáneo 1º Bachillerato

En la Comunidad de Madrid se desarrolla en  [DECRETO 67/2008](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=5081&cdestado=P) , de 19 de junio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato, B.O.C.M. Núm. 152 de VIERNES 27 DE JUNIO DE 2008, páginas 10 a 12  
 
## Introducción
A partir de la segunda mitad del siglo XIX y a lo largo del siglo XX, la humanidad ha adquirido más conocimientos científicos y tecnológicos que en toda su historia anterior. La mayor parte de estos conocimientos han dado lugar a numerosas aplicaciones que se han integrado en la vida de los ciudadanos, quienes las utilizan sin cuestionar, en muchos casos, su base científica, la incidencia en su vida personal o los cambios sociales o medioambientales que se derivan de ellas.  
Los medios de comunicación presentan de forma casi inmediata los debates científicos y tecnológicos sobre temas actuales. Cuestiones como la ingeniería genética, los nuevos materiales, las fuentes de energía, el cambio climático, los recursos naturales, las tecnologías de la información, la comunicación y el ocio o la salud son objeto de numerosos artículos e, incluso, de secciones especiales en la prensa.  
Los ciudadanos del siglo XXI, integrantes de la denominada “sociedad del conocimiento”, tienen el derecho y el deber de poseer una formación científica que les permita actuar como ciudadanos autónomos, críticos y responsables. Para ello es necesario poner al alcance de todos los ciudadanos esa cultura científica imprescindible y buscar elementos comunes en el saber que todos deberíamos compartir. El reto para una sociedad democrática es que la ciudadanía tenga conocimientos suficientes para tomar decisiones reflexivas y fundamentadas sobre temas científico-técnicos de incuestionable trascendencia social y poder participar democráticamente en la sociedad para avanzar hacia un futuro sostenible para la humanidad.  
Esta materia, común para todo el alumnado, debe contribuir a da una respuesta adecuada a ese reto, por lo que es fundamental que la aproximación a la misma sea funcional y trate de responder a interrogantes sobre temas de índole científica y tecnológica con gran incidencia social. No se puede limitar a suministrar respuestas, por el contrario ha de aportar los medios de búsqueda y selección de información, de distinción entre información relevante e irrelevante, de existencia o no de evidencia científica, etcétera. En definitiva, deberá ofrecer a los estudiantes la posibilidad de aprender a aprender, lo que les será de gran utilidad para su futuro en una sociedad sometida a grandes cambios, fruto de las revoluciones científico-tecnológicas y de la transformación de los modos de vida, marcada por intereses y valores particulares a corto plazo, que están provocando  
graves problemas ambientales y a cuyo tratamiento y resolución pueden contribuir la ciencia y la tecnología.  
Además, “esta materia” contribuye a la comprensión de la complejidad de los problemas actuales y las formas metodológicas que utiliza la ciencia para abordarlos, el significado de las teorías y modelos como explicaciones humanas a los fenómenos de la naturaleza, la provisionalidad del conocimiento científico y sus límites. Asimismo, ha de incidir en la conciencia de que la ciencia y la tecnología son actividades humanas incluidas en contextos sociales,económicos y éticos que les transmiten su valor cultural. Por otra parte, el enfoque debe huir de una ciencia academicista y formalista, apostando por una ciencia no exenta de rigor, pero que tenga en cuenta los contextos sociales y el modo en que los problemas afectan a las personas de forma global y local.  
Estos principios presiden la selección de los objetivos, contenidos y criterios de evaluación de la materia, que están dirigidos a tratar de lograr tres grandes finalidades: Conocer algunos aspectos de los temas científicos actuales objeto de debate, con sus implicaciones pluridisciplinares y ser consciente de las controversias que suscitan; familiarizarse con algunos aspectos de la naturaleza de la ciencia y el uso de los procedimientos más comunes que se utilizan para abordar su conocimiento, y adquirir actitudes de curiosidad, antidogmatismo, tolerancia y tendencia a fundamentar las afirmaciones y las refutaciones.  
Los contenidos giran alrededor de la información y la comunicación, la necesidad de caminar hacia la sostenibilidad del planeta, la salud como resultado de factores ambientales y responsabilidad personal, los avances de la genética y el origen del universo y de la vida.  
Todos ellos interesan a los ciudadanos, son objeto de polémica y debate social y pueden ser tratados desde perspectivas distintas, lo que facilita la comprensión de que la ciencia no afecta solo a los científicos, sino que forma parte del acervo cultural de todos.  


## Objetivos
La enseñanza de las Ciencias para el Mundo Contemporáneo en el Bachillerato tendrá como objetivo el desarrollo de las siguientes capacidades:  
1. Conocer el significado cualitativo de algunos conceptos, leyes y teorías, para formarse opiniones fundamentadas sobre cuestiones científicas y tecnológicas, que tengan incidencia en las condiciones de vida personal y global y sean objeto de controversia social y debate público.  
2. Plantearse preguntas sobre cuestiones y problemas científicos de actualidad y tratar de buscar respuestas propias, utilizando y seleccionando de forma crítica información proveniente de diversas fuentes.  
3. Obtener, analizar y organizar informaciones de contenido científico y tecnológico, utilizar representaciones y modelos, hacer conjeturas, formular hipótesis y realizar reflexiones fundadas que permitan tomar decisiones fundamentadas y comunicarlas a los demás con coherencia, precisión y claridad.  
4. Adquirir un conocimiento coherente y crítico de las tecnologías de la información, la comunicación y el ocio presentes en su entorno, propiciando un uso sensato y racional de las mismas para la construcción del conocimiento científico, la elaboración del criterio personal y la mejora del bienestar individual y colectivo.  
5. Argumentar, debatir y evaluar propuestas y aplicaciones de los conocimientos científicos de interés social relativos a la salud, el medio ambiente, los materiales, las fuentes de energía, el ocio, etcétera, para poder valorar las informaciones científicas y tecnológicas de los medios de comunicación de masas y adquirir independencia de criterio.  
6. Poner en práctica actitudes y valores sociales como la creatividad, la curiosidad, el antidogmatismo, la reflexión crítica y la sensibilidad ante la vida y el medio ambiente, que son útiles para el avance personal, las relaciones interpersonales y la inserción social.  
7. Valorar la contribución de la ciencia y la tecnología a la mejora de la calidad de vida, reconociendo sus aportaciones y sus limitaciones como empresa humana cuyas ideas están en continua evolución y condicionadas al contexto cultural, social y económico en el que se desarrollan.  
8. Reconocer, en algunos ejemplos concretos, la influencia recíproca entre el desarrollo científico y tecnológico y los contextos sociales, políticos, económicos, religiosos, educativos y culturales en que se produce el conocimiento y sus aplicaciones.  
9. Garantizar una expresión oral y escrita correcta a partir de los textos relacionados con la materia.  
10. Diferenciar entre ciencia y otras actividades no científicas que nos rodean en nuestra vida cotidiana.  


## Contenidos


### 1. Contenidos comunes.
— Definición de Ciencia y Tecnología. Métodos Científicos. Pseudociencias.  
— Distinción entre las cuestiones que pueden resolverse mediante respuestas basadas en observaciones y datos científicos de aquellas otras que no pueden solucionarse desde la ciencia.  
— Búsqueda, comprensión y selección de información científica relevante de diferentes fuentes para dar respuesta a los interrogantes, diferenciando las opiniones de las afirmaciones basadas en datos.  
— Análisis de problemas científico-tecnológicos de incidencia e interés social, predicción de su evolución y aplicación del conocimiento en la búsqueda de soluciones a situaciones concretas.  
— Disposición a reflexionar científicamente sobre cuestiones de carácter científico y tecnológico para tomar decisiones responsables basadas en un análisis crítico en contextos personales y sociales.  
— Reconocimiento de la contribución del conocimiento científico-tecnológico a la comprensión del mundo, a la mejora de las condiciones de vida de las personas y de los seres vivos en general, a la superación de la obviedad y el dogmatismo científico, a la liberación de los prejuicios y a la formación del espíritu crítico.  
— Reconocimiento de las limitaciones y errores de la ciencia y la tecnología, de algunas aplicaciones perversas y de su dependencia del contexto social y económico, a partir de hechos actuales y de casos relevantes en la historia de la ciencia y la tecnología.  
— Conocimiento de algunos descubrimientos científico-tecnológicos que han marcado época en la historia de la ciencia y tecnología. Retos actuales de la ciencia. Cooperación internacional para el desarrollo tecnológico.  


### 2. Nuestro lugar en el Universo.
— El origen del Universo: Explicación en diferentes culturas. Teorías sobre su origen y evolución. La génesis de los elementos: Polvo de estrellas. Exploración del sistema solar: Situación actual.  
— La formación de la Tierra y la diferenciación en capas. Lylle y los principios de la geología. Wegener y la deriva de los continentes. La tectónica global: Pruebas y consecuencias de la misma.  
— El origen de la vida: De la síntesis prebiótica a los primeros organismos: Principales hipótesis.  
— Del fijismo al evolucionismo. Principales teorías evolucionistas. La selección natural darwiniana y su explicación genética actual.  
— Nuestro lugar en la escala biológica. De los homínidos fósiles al “Homo sapiens”. Los cambios genéticos condicionantes de la especie humana.  


### 3. Vivir más, vivir mejor.
— Definiciones de salud. Los determinantes de salud. La salud como resultado de los factores genéticos, ambientales y personales. Los estilos de vida saludables.  
— Las enfermedades infecciosas y no infecciosas. Enfermedades nutricionales de países ricos y países pobres: Obesidad y desnutrición. El uso racional de los medicamentos: Prescripción por principio activo. Trasplantes y solidaridad: Sus tipos, los problemas de rechazo y reflexión ética.  
— Los condicionamientos de la investigación médica: Intereses económicos, políticos y humanos. Grandes retos actuales de la investigación médica. Las patentes. La sanidad en los países de nivel de desarrollo bajo.  
— La revolución genética. El genoma humano. Las tecnologías del ADN recombinante y la ingeniería genética. Aplicaciones en la terapia de enfermedades humanas, en la producción agrícola y animal, en la biotecnología y en la medicina legal.  
— La reproducción asistida: Aspectos positivos y negativos. La clonación y sus aplicaciones. Las células madre: Terapia y controversia social. La Bioética: Riesgos e implicaciones éticas de la manipulación genética y celular.  


### 4. Hacia una gestión sostenible del planeta.
— La sobreexplotación de los recursos: Aire, agua, suelo, minerales, seres vivos y fuentes de energía. Energías renovables, no renovables y alternativas. Energía nuclear: Aplicaciones técnicas, médicas y energéticas. Tratamiento de los residuos radioactivos. El agua como recurso limitado: Necesidad biológica y bien económico.  
— Los impactos: La contaminación, la desertización. El aumento de residuos y la pérdida de biodiversidad. Los cambios climáticos: Causas y efectos.  
— Los riesgos naturales. Las catástrofes más frecuentes: Terremotos, inundaciones etcétera. Factores que incrementan los riesgos.  
— El problema del crecimiento ilimitado en un planeta limitado: Agotamiento de recursos. Producción de alimentos. Agricultura ecológica. Uso de fertilizantes. Principios generales de sostenibilidad económica, ecológica y social. Los compromisos internacionales y la responsabilidad ciudadana.  


### 5. Nuevas necesidades, nuevos materiales.
— La humanidad y el uso de los materiales. Localización, producción y consumo de materiales: Control de los recursos.  
— Algunos materiales naturales: Rocas, madera y fibras naturales. Los metales, riesgos a causa de su corrosión. El papel y el problema de la deforestación.  
— El desarrollo científico-tecnológico y la sociedad de consumo: Agotamiento de materiales y aparición de nuevas necesidades, desde la medicina a la aeronáutica. Los biocombustibles: Uso energético.  
— La respuesta de la ciencia y la tecnología. Nuevos materiales: Los polímeros. Nuevas tecnologías: La nanotecnología. Importancia y aplicaciones en el mundo actual.  
— Análisis medioambiental y energético del uso de los materiales: Reducción, reutilización y reciclaje. Basuras.  


### 6. La aldea global. De la sociedad de la información a la sociedad del conocimiento.
— Procesamiento, almacenamiento e intercambio de la información. El salto de lo analógico a lo digital. Su importancia y repercusión en la vida cotidiana.  
— Tratamiento numérico de la información, de la señal y de la imagen.  
— Internet, un mundo interconectado. Principales ventajas e inconvenientes. Compresión y transmisión de la información. Control de la privacidad y protección de datos. Su importancia en un mundo globalizado.  
— La revolución tecnológica de la comunicación: Ondas, cable, fibra óptica, satélites, ADSL, telefonía móvil, GPS, etcétera. Repercusiones en la vida cotidiana.  


## Criterios de evaluación
1. Obtener, seleccionar y valorar informaciones sobre distintos temas científicos y tecnológicos de repercusión social y comunicar conclusiones e ideas en distintos soportes a públicos diversos, utilizando eficazmente las tecnologías de la información y comunicación, para formarse opiniones propias argumentadas.  
2. Analizar algunas aportaciones científico-tecnológicas a diversos problemas que tiene planteados la humanidad, y la importancia del contexto político-social en su puesta en práctica, considerando sus ventajas e inconvenientes desde un punto de vista económico, medioambiental y social.  
3. Realizar estudios sencillos sobre cuestiones sociales con base científico-tecnológica de ámbito local, haciendo predicciones y valorando las posturas individuales o de pequeños colectivos en su posible evolución.  
4. Valorar la contribución de la ciencia y la tecnología a la comprensión y resolución de los problemas de las personas y de su calidad de vida, mediante una metodología basada en la obtención de datos, el razonamiento, la perseverancia, el espíritu crítico y el respeto por las pruebas, aceptando sus limitaciones y equivocaciones propias de toda actividad humana.  
5. Identificar los principales problemas ambientales, las causas que los provocan y los factores que los intensifican; predecir sus consecuencias y argumentar sobre la necesidad de una gestión sostenible de la Tierra, siendo conscientes de la importancia de la sensibilización ciudadana para actuar sobre los problemas ambientales locales.  
6. Conocer y valorar las aportaciones de la ciencia y la tecnología a la mitigación de los problemas ambientales mediante la búsqueda de nuevos materiales y nuevas tecnologías, en el contexto de un desarrollo sostenible.  
7. Conocer y diferenciar las enfermedades más frecuentes en nuestra sociedad, identificando algunos indicadores, causas y tratamientos más comunes, valorando la importancia de adoptar medidas preventivas que eviten los contagios, que prioricen los controles periódicos y los estilos de vida saludables sociales y personales.  
8. Conocer las bases científicas de la manipulación genética y embrionaria, valorar los pros y contras de sus aplicaciones y entender la controversia internacional que han suscitado, siendo capaces de fundamentar la existencia de un Comité de Bioética que defina sus límites en un marco de gestión responsable de la vida humana.  
9. Analizar las sucesivas explicaciones científicas dadas a cuestiones como el origen de la vida y del Universo; haciendo hincapié en la importancia del razonamiento hipotético-deductivo, el valor de las pruebas y la influencia del contexto social, diferenciándolas de las basadas en opiniones o creencias.  
10. Conocer las características básicas, las formas de utilización de los últimos instrumentos tecnológicos de información, comunicación, ocio y creación para obtener, generar y transmitir informaciones de tipo diverso, y las repercusiones individuales y sociales, valorando su incidencia positiva y negativa en los hábitos de consumo y en las relaciones sociales.  
11. Utilizar conceptos, leyes y teorías científicas para poder opinar de manera fundamentada y crítica sobre diferentes cuestiones cientifico-tecnológicas de incidencia en la vida personal, social, global y que sean, a su vez, objeto de discusión social y cuestión pública.  
12. Demostrar actitudes como la reflexión crítica, el antidogmatismo científico y el respeto a la vida y al medio ambiente.  
13. Identificar y analizar las actividades pseudocientíficas que aparecen en nuestra vida cotidiana.  
 
