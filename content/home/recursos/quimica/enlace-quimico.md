
# Enlace químico

En esta página se indican recursos asociados al enlace químico, muy relacionados con los  [recursos sobre propiedades sustancias](/home/recursos/quimica/recursos-propiedades-sustancias).  

También asociado a enlace químico está el  [diagrama de Lewis](/home/recursos/quimica/diagrama-lewis) , la [geometría molecular](/home/recursos/quimica/geometria-molecular), y [fuerzas intermoleculares](/home/recursos/quimica/fuerzas-intermoleculares) con páginas aparte.  

Pendiente poner aquí recursos para iónico, metálico, covalente ..

##  Apuntes
Apuntes de FisQuiWeb sobre enlaces. Distintos apuntes por nivel. Licenciamiento cc-by-nc-sa  
[fisquiweb.es apuntes](http://fisquiweb.es/Apuntes/apuntes.htm)  
* [2º ESO apuntes enlaces](http://fisquiweb.es/Apuntes/Apuntes2/Enlace2.pdf)  
* [3º ESO apuntes enlaces](http://fisquiweb.es/Apuntes/Apuntes3/Enlace3.pdf)  
* [4º ESO apuntes enlaces](http://fisquiweb.es/Apuntes/Apuntes4/Enlaces4.pdf)  
* [1º Bachillerato apuntes enlaces](http://fisquiweb.es/Apuntes/Apuntes1Bach/Enlaces1B.pdf)  
* [2º Bachillerato apuntes enlace iónico](http://fisquiweb.es/Apuntes/Apuntes2Qui/EnlaceIonico.pdf)  
* [2º Bachillerato apuntes enlace metálico](http://fisquiweb.es/Apuntes/Apuntes2Qui/EnlaceMetalico.pdf)  
* [2º Bachillerato apuntes enlace covalente](http://fisquiweb.es/Apuntes/Apuntes2Qui/EnlaceCovalente.pdf)  
* [2º Bachillerato apuntes enlace teoría cuántica](http://fisquiweb.es/Apuntes/Apuntes2Qui/EnlaceCuantico.pdf)  
* [2º Bachillerato apuntes interacciones de no enlace](http://fisquiweb.es/Apuntes/Apuntes2Qui/NoEnlace.pdf)  

  
[Resúmenes y Ampliaciones de Enlace Químico y Estructura de la Materia (1º Químicas) - uah.es](http://www2.uah.es/edejesus/resumenes/EQEM.htm)  
Páginas de Ernesto de Jesús Alcañiz. UNIVERSIDAD DE ALCALÁ. Departamento de Química Inorgánica.  [Copyright © 1995-200x Ernesto de Jesús Alcañiz](http://www2.uah.es/edejesus/copyright.htm).  

[ocw.uc3m.es QUIMICA DE MATERIALES TEMA3. EL ENLACE QUÍMICO](http://ocw.uc3m.es/ciencia-e-oin/quimica-de-los-materiales/Material-de-clase/tema-3.-el-enlace-quimico/skinless_view)  

[Teoria de enlace químico - quimitube.com](http://www.quimitube.com/teoria-de-enlace-quimico)  
© 2012 Quimitube Todos los derechos reservados.  
  
[Introductory Chemistry. Chemical Bond.](http://www.peoi.org/Courses/Coursesch/chemintro/contents/frame9.html)  © David W. Ball  

## Enlace iónico
Enlaza con estructura cristalinas y radio iónico  

[QUIMICA GENERAL Química Inorgánica Tema 4: Estado Sólido 4.4.- Sólidos Iónicos - El enlace Iónico Profesor: Rafael Aguado Bernal - ubu.es](https://riubu.ubu.es/bitstream/handle/10259.3/79/4.4.4%20(3)%20-%20S%F3lidos%20I%F3nicos%20-%20El%20enlace%20i%F3nico-3.pdf;jsessionid=1A16C0688D3DA9BD965FBAF1740F9973?sequence=10)  

##  Enlaces múltiples por encima del triple
Pendiente enlazar info sobre tipos orbitales moleculares en enlaces: sigma, pi, delta ...  
[wikipedia, Enlace quíntuple](https://es.m.wikipedia.org/wiki/Enlace_qu%C3%ADntuple)  
[wikipedia, Enlace séxtuple](https://es.m.wikipedia.org/wiki/Enlace_s%C3%A9xtuple) 

##  Born Haber
Asociado a enlace iónico, utiliza conceptos de entalpía de  [termoquímica](/home/recursos/quimica/termoquimica)  que se ven en 1ºBachillerato  
[http://chemistry.bd.psu.edu/jircitano/BHnotes.pdf](http://chemistry.bd.psu.edu/jircitano/BHnotes.pdf) 

[WORKSHEET-Born-Haber Cycle](https://www.cerritos.edu/chemistry/_includes/docs/Chem_111/Others/Worksheets/Answers/Worksheet%20Born-Haber%20Cycle%20Answer%20Key%20PDF.pdf)  

##  Cómics / imágenes 
 
 Algunos en  [https://wirdou.com/](https://wirdou.com/)  
![](https://wirdou.files.wordpress.com/2015/02/water-covalent-bond.jpg)  
![](https://wirdou.files.wordpress.com/2012/08/nacl.jpg)  
![](https://wirdou.files.wordpress.com/2012/10/chemical-bonds-ionic.jpg)  
![](https://wirdou.files.wordpress.com/2012/10/chemical-bonds-covalent.jpg)  
![](https://wirdou.files.wordpress.com/2012/10/chemical-bonds-metallic.jpg)  
![](https://wirdou.files.wordpress.com/2013/03/types-of-chemical-bonds.jpg)  
![](https://wirdou.files.wordpress.com/2017/02/gas-noble.jpg)  
![](https://wirdou.files.wordpress.com/2015/02/water-gollum.jpg)  
![](https://wirdou.files.wordpress.com/2015/02/water-polar-molecule.jpg)  
![](https://wirdou.files.wordpress.com/2015/02/the-water-molecule.jpg)  
![](https://wirdou.files.wordpress.com/2015/02/water-covalent-bond.jpg)  

[twitter CqQuimica/status/1438662732182601735](https://twitter.com/CqQuimica/status/1438662732182601735)  
![](https://scontent-mad1-1.cdninstagram.com/v/t51.2885-15/e35/s1080x1080/242215025_6338630002843852_6460403454408686463_n.jpg?_nc_ht=scontent-mad1-1.cdninstagram.com&_nc_cat=104&_nc_ohc=fo0f6QPbtNcAX_q-UDD&edm=AABBvjUBAAAA&ccb=7-4&oh=00_AT-UAK4nv09LfHvVz-gvPV7z72RYvfTjZKKOGYr3QK1LBA&oe=61D959CF&_nc_sid=83d603)  

[twitter CqQuimica/status/1439024705759969283](https://twitter.com/CqQuimica/status/1439024705759969283)  

##  Enlaces (de internet, no químicos :-)
El enlace molecular.  
[la-mecanica-cuantica.blogspot.com.es el enlace molecular](http://la-mecanica-cuantica.blogspot.com.es/2009/08/el-enlace-molecular.html)  
Blog "La mecánica cuántica", Armando Martínez Téllez  

[Organic chemistry. Bonding. About the MCAT *waybackmachine*](http://web.archive.org/web/20161115075932/http://www.aboutthemcat.org/organic-chemistry/bonding.php)  
© Sabali Co 2014  

[corinto.pucp.edu.pe unidad 3 enlace químico](http://corinto.pucp.edu.pe/quimicageneral/unidades/unidad-3-enlace-qu%C3%ADmico.html)  cc-by-nc-nd

## Enlace de hidrógeno
Simulación  
[3. Hydrogen Bonds: A Special Type of Attraction](https://www.labxchange.org/library/pathway/lx-pathway:9dc1a9dc-26cd-4dc0-a3a2-13a7c0960cfe/items/lx-pb:9dc1a9dc-26cd-4dc0-a3a2-13a7c0960cfe:lx_simulation:d352c29b)  

[Los enlaces de hidrógeno C–H···O explican por qué el agua y el aceite no se mezclan - francis.naukas.com](https://francis.naukas.com/2021/12/13/los-enlaces-de-hidrogeno-c-h%c2%b7%c2%b7%c2%b7o-explican-por-que-el-agua-y-el-aceite-no-se-mezclan/)  

[Hydrogen Bonding in Water - youtube](https://www.youtube.com/watch?v=aZ8JxFwR_nY)  


##  Enlace vibracional
[culturacientifica.com Un nuevo enlace químico por sustitución isotópica](https://culturacientifica.com/2014/10/29/un-nuevo-enlace-quimico-por-sustitucion-isotopica/)  

## Enlaces en el cemento
[twitter DrAbhayGoyal/status/1422991881626136582](https://twitter.com/DrAbhayGoyal/status/1422991881626136582)  
Finally out! In “The physics of cement cohesion” published today in Science Advances, we address the centuries-old question of what makes cement so strongly cohesive (1/8)
[sciencemag.org The physics of cement cohesion](https://advances.sciencemag.org/content/7/32/eabg5882)  

## Enlace metálico / teoría de bandas

[Teoría de bandas - textoscientificos.com](https://www.textoscientificos.com/quimica/inorganica/enlace-metales/teoria-bandas)  

[Una explicación sencilla de la teoría de bandas en los sólidos - francis.naukas.com](https://francis.naukas.com/2015/07/22/una-explicacion-sencilla-de-la-teoria-de-bandas-en-los-solidos/)  

[Teoría de bandas - es.wikipedia.org](https://es.wikipedia.org/wiki/Teor%C3%ADa_de_bandas)  
![](https://es.wikipedia.org/wiki/Banda_de_valencia)

[Teoría de bandas - studylib.es](https://studylib.es/doc/9009674/teor%C3%ADa-de-bandas.)  

[Enlace metálico - liceoagb.es](https://www.liceoagb.es/quimigen/enlace11.html)  
![](https://www.liceoagb.es/quimigen/imagenes/banda.jpg)  

![](https://www.liceoagb.es/quimigen/imagenes/banda3.jpg)  


##  Simulaciones
* [Construye una molécula - phet.colorado.edu](https://phet.colorado.edu/es/simulations/build-a-molecule)  
![](https://phet.colorado.edu/sims/html/build-a-molecule/latest/build-a-molecule-900.png)  
  [Construye una molécula. Simulación - phet](https://phet.colorado.edu/es/simulation/build-a-molecule)    
* Enlace iónico [http://www.hschickor.de/abioch/12nacl.html](http://www.hschickor.de/abioch/12nacl.html)  
* [educaplus.org Enlace iónico](http://www.educaplus.org/play-77-Enlace-i%C3%B3nico.html)  
* Enlaces [concurso.cnice.mec.es Iniciación interactiva a la materia, enlaces](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/enlaces/enlaces1.htm)  
    * [concurso.cnice.mec.es Iniciación interactiva a la materia, enlace iónico](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/enlaces/ionico.htm)  
    * [concurso.cnice.mec.es Iniciación interactiva a la materia, enlace covalente](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/enlaces/covalente.htm)  
    * [concurso.cnice.mec.es Iniciación interactiva a la materia, enlace metálico ](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/enlaces/metalico.htm)  
    
[www.mhe.es, tipos de enlaces: iónico, covalente, metálico (Flash)](https://www.mhe.es/bachillerato/fisica_quimica/844816962X/archivos/media/esp/unidad_2/2ani_U.2.swf)  
 
Enlaces, comparativa con perros y huesos, y visualización con jmol  
[liceoagb.es estructura molecular moléculas](http://www.liceoagb.es/quimiorg/moleculas.html)  
[chemdemos.uoregon.edu, bonding](https://chemdemos.uoregon.edu/search/content/bonding) 

###  Polaridad molecular 
 [phet.colorado.edu, Polaridad de la molécula](https://phet.colorado.edu/sims/html/molecule-polarity/latest/molecule-polarity_es.html) 


###  Modelos moleculares
Enlaza con  [geometría molecular](/home/recursos/quimica/geometria-molecular) , aunque a veces hay simulaciones solamente de enlace

##  Ejercicios / actividades
Ejercicios enlaces / Lewis PAU: selección para ESO  
Los contenidos asociados a enlaces se introducen en 3º ESO, y se repiten en 4º ESO, 1º Bachillerato y 2º Bachillerato profundizando (geometría, hibridación, enlaces intermoleculares,...)  
Se incluye una selección de los  [apartados / ejercicios PAU de Química que pueden utilizarse en ESO](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/enlace-quimico/Q3-PAU-EnlaceQuimicoPropiedadesSustancias-ESO.pdf) , ya que tratan sobre tipo de enlace, propiedades y diagramas de Lewis.  

[twitter bjmahillo/status/1049701074884530176](https://twitter.com/bjmahillo/status/1049701074884530176)  
Actividad de FyQ 4º ESO: los átomos buscan pareja 😍. Ya veremos si los alumnos son buenas celestinas... Idea copiada de @estapillao y @Biochiky.  
[![](https://pbs.twimg.com/media/DpFInuiWwAEqoTZ.jpg "") ](https://pbs.twimg.com/media/DpFInuiWwAEqoTZ.jpg)  

[twitter mamabish/status/1113513201868709889](https://twitter.com/mamabish/status/1113513201868709889)  
Home made department resource used for the first time with year 9 today. 3D printed ionic bonding! Love a marble @VParticle  
[![](https://pbs.twimg.com/media/D3P-WimWsAU0M1P.jpg "") ](https://pbs.twimg.com/media/D3P-WimWsAU0M1P.jpg)  

Incluye enlace a .stl para impresora 3D  
[Electron Config V2.stl](https://drive.google.com/file/d/1HWrrC8muCyP7RDsjA8hU9XtXjwLjlDuX/view)  

[twitter quirogafyq/status/1330491282389602305](https://twitter.com/quirogafyq/status/1330491282389602305)  
Buenas!!! Os comparto una actividad que hice hace nada en clase para repasar el tema de enlace químico. Se trata de un quién es quién. Os cuento! Abro mini-hilo!  
[miguelquiroga.es ¿quién-es-quién?](https://www.miguelquiroga.es/la-materia/qui%C3%A9n-es-qui%C3%A9n)  
Aprovechando las magníficas ilustraciones de @Wirdou he diseñado este quien es quien. La idea consiste en lo siguiente:  
Se hacen grupos de 4-6 personas con el alumnado y se les da a cada uno un cartel de modo totalmente aleatorio y secreto sin que vea que cartel le toca.  
Con una pinza, se lo coloca, mostrando su identidad al resto del grupo pero sin saber quién es.  
Haciendo preguntas cuya respuesta sea si/no, tienen que averiguar que tipo de enlace es.  
Las tarjetas están diseñadas para que por la parte de detrás les salgan sugerencias de preguntas como estas:  
¿Conduzco la electricidad y la temperatura?  
¿Tengo un punto de fusión y ebullición alto?  
¿Estoy formado por dos metales?  
¿Me disuelvo en agua?  
¿Soy duro?  

[Ionic Bonding Walk Around](http://passionatelycurioussci.weebly.com/google-form-walk-arounds.html#ionic)  


["Escape room" (test 6 preguntas) enlace químico - genial.ly](https://view.genial.ly/648893b25277730011aaca29/interactive-content-escape-room-enlace-quimico)  

[EJERCICIOS TEMA 5 ENLACE QUÍMICO FÍSICA Y QUÍMICA 3º ESO. IES “MARGARITA SALAS” (08-09)](https://profesorparticularcastellon.files.wordpress.com/2014/01/ejercicios-enlace.pdf)

##  ¿Metal y no metal excluyentes? ¿Tipo de enlace iónico y covalente excluyentes?
Dentro de la idea de propiedades periódicas, los elementos se suelen clasificar de manera sencilla como metales o no metales (así se hace en 3º ESO). La separación suele ser una línea diagonal en zigzag en la tabla periódica. La realidad es que entre metal y no metal hay un carácter intermedio, que es el de semimetales o metaloides. Suele ser una franja de elementos alrededor de la línea diagonal en zigzag, que varía según fuentes. ¿Qué tipo de enlace tiene AlCl3? iónico, covalente ? en 3º y 4º ESO sería totalmente correcto para un alumno responder que es iónico sin más, pero en bachillerato se puede intentar explicar algo más.  
Se puede mirar esta página de wikipedia (en inglés, la equivalente en español dice poco)  
[en.wikipedia.org, Metalloid#Aluminium](http://en.wikipedia.org/wiki/Metalloid#Aluminium)  
En los apuntes de 2º Bachillerato se cita la idea de porcentaje de carácter iónico/covalente, viendo el enlace iónico como un extremo de enlace covalente con mucha diferencia de electronegatividad. Se indica según criterio de libro de Chang que una diferencia menor o igual a 2 de electronegatividad es covalente, y mayor es iónico.  
Otro ejemplo sería BeCl<sub>2</sub> que también es covalente aunque a nivel ESO sería entre metal y no metal y se diría que es iónico.  

Además se puede plantear que en ciertos casos un compuesto químico tiene parte de sus enlaces covalentes y parte iónicos; por ejemplo CaCO<sub>3</sub>, en disolución se separa en sus iones, que son Ca<sup>2+</sup> y CO<sub>3</sub><sup>2-<sup>, pero el ión carbonato internamente tiene enlaces covalentes. 
