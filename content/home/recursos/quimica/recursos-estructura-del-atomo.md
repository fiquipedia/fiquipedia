---
aliases:
  - /home/recursos/quimica/recursos-estructura-del-tomo/
---

# Recursos estructura del átomo / modelos atómicos

[comentario]: # (No migro http://delicious.com/hunk/Recursos+FQ+átomo en 2015 no actualizado desde 2011 ... desisto de hacerlo, pendiente mover aquí todo lo que en su momento puse allí, si no desaparece antes) 
 
Filtrando por etiquetas adicionales, se pueden separar tipo de material (material autosuficiente, actividades. ...)  
Asociable a estructura del átomo se pueden ver también  [recursos sobre espectro](/home/recursos/recursos-espectro)  y recursos sobre  [física de partículas](/home/recursos/fisica/fisica-de-particulas): lo de física de partículas aplica porque a edades tempranas se introduce el átomo y sus constituyentes y el CERN hace recomendaciones para evitar ideas erróneas.  
También se pueden ver recursos sobre  [escalas y notación científica](/home/recursos/recursos-notacion-cientifica)  
También asociado a recursos  [configuración electrónica](/home/recursos/quimica/recursos-configuracion-electronica)  y  [orbitales](/home/recursos/orbitales)  
También asociado a  [recursos enlaces](/home/recursos/quimica/enlace-quimico)  


Una idea  
[Cuál es la frase más importante de toda la ciencia según el nobel de Física Richard Feynman y por qué - bbc.com](https://www.bbc.com/mundo/amp/noticias-53240219)  
>*"Creo que es la hipótesis atómica (o el hecho atómico, o como quieran llamarlo) que todas las cosas están hechas de átomos: pequeñas partículas que se mueven en movimiento perpetuo, atrayéndose entre sí cuando están a poca distancia, pero repeliéndose cuando se les trata de apretar una contra la otra "*
*.¿Por qué?" En esa sola frase hay una enorme cantidad de información sobre el mundo, si solo se aplica un poco de imaginación y pensamiento ".* 

[Concepto moderno del átomo. 3ºESO cidead - recursostic.educacion.es](http://recursostic.educacion.es/secundaria/edad/3esofisicaquimica/3quincena6/3q6_index.htm)  (isótopos y masa atómica)...cidead, cc-by-nc-sa
[Estructura del átomo y enlaces químicos. 4ºESO cidead - recursostic.educacion.es](http://recursostic.educacion.es/secundaria/edad/4esofisicaquimica/4quincena8/4q8_index.htm) cidead, cc-by-nc-sa   
[Cálculos de masas atómicas (masa ponderada). 3ºESO cidead - recursostic.educacion.es](http://recursostic.educacion.es/secundaria/edad/3esofisicaquimica/3quincena6/actividades6/masa_ponderada.htm)  cidead, cc-by-nc-sa   

[1 La estructura electrónica de los átomos. Ernesto de Jesús Alcañiz - uah.es](https://edejesus.web.uah.es/resumenes/EQEM/tema_193.pdf)

##  Modelos atómicos
[Modelos del Átomo del Hidrógeno  - phet.colorado.edu](https://phet.colorado.edu/es/simulations/hydrogen-atom)  En 2021 solo versión java, funciona con cheerpj  
 
Un poco de humor  
[https://xkcd.com/2100/](https://xkcd.com/2100/)  
[![](https://imgs.xkcd.com/comics/models_of_the_atom.png "") ](https://imgs.xkcd.com/comics/models_of_the_atom.png) 

[El átomo y los modelos atómicos. 3º ESO cidead - recursostic.educacion.es](http://recursostic.educacion.es/secundaria/edad/3esofisicaquimica/3quincena5/3q5_index.htm)  

[quimicafacil.net Los modelos atómicos fácilmente explicados](https://quimicafacil.net/humor/los-modelos-atomicos-facilmente-explicados/)  
[![](https://i0.wp.com/quimicafacil.net/wp-content/uploads/2020/04/Modelos-atomicos-muerte.jpg "") ](https://i0.wp.com/quimicafacil.net/wp-content/uploads/2020/04/Modelos-atomicos-muerte.jpg) 

[Atomic Structure Walk Around](http://passionatelycurioussci.weebly.com/google-form-walk-arounds.html#atomic)  
[Atomic Structure Walk Around (pdf, 13 páginas)](https://passionatelycurioussci.weebly.com/uploads/5/8/6/6/58665899/atomic_structure_walk_around.pdf)  

[twitter memecrashes/status/1477364495215382531](https://twitter.com/memecrashes/status/1477364495215382531)
![](https://pbs.twimg.com/media/FICnLQAXoAI4nz5?format=jpg)  

[twitter memecrashes/status/1696775229119341037](https://twitter.com/memecrashes/status/1696775229119341037)  
![](https://pbs.twimg.com/media/F4woEKAWQAAIPEe?format=jpg)  

[Beyond Bohr. Quantum Model of the Atom Beyond - perimeterinsitutue.ca](https://resources.perimeterinstitute.ca/products/beyond_bohr_poster?variant=42048283082930)  
![](https://cdn.shopify.com/s/files/1/1160/8438/products/Beyond_Bohr_Poster_EN_1500x1500.jpg?v=1655406332)  


##  Construcción de átomos
Incluye iones en algunos casos  
[![Construye un átomo - phet.colorado.edu](https://phet.colorado.edu/sims/html/build-an-atom/latest/build-an-atom-900.png)](https://phet.colorado.edu/es/simulation/build-an-atom) Usa niveles e indica exclusión Pauli
Simulación construcción, indica elemento/ion/estabilidad, no cita Pauli pero coloca en niveles, máximo 10 protones 
  
[Constructor de átomos - educaplus.org](http://www.educaplus.org/game/constructor-de-atomos)  
[Partículas de los átomos e iones - educaplus.org](http://www.educaplus.org/game/particulas-de-los-atomos-e-iones)  
[cnice.mec.es Iniciación interactiva a la materia, construir átomo](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/atomo/aconstruir.htm) 

##  Constructor de isótopos, masa atómica
[![Isótopos y masa atómica - phet.colorado.edu](https://phet.colorado.edu/sims/html/isotopes-and-atomic-mass/latest/isotopes-and-atomic-mass-900.png)](https://phet.colorado.edu/es/simulations/isotopes-and-atomic-mass "Isótopos y masa atómica - phet.colorado.edu")  
 

##  Modelo de Rutherford
Simulación del experimento de Rutherford, muy visual  
[http://www.deciencias.net/simulaciones/quimica/atomo/rutherford.htm](http://www.deciencias.net/simulaciones/quimica/atomo/rutherford.htm)  

[Rutherford. El modelo de átomo planetario - fisquiweb.es](http://fisquiweb.es/atomo/atomoII_B.htm)  
Simulación que permite rebotes y desvíos (flash)  

Simulación experimento Rutherford y comparación con Thomson  
[![Dispersión de Rutherford - phet.colorado.edu](https://phet.colorado.edu/sims/html/rutherford-scattering/latest/rutherford-scattering-900.png)](https://phet.colorado.edu/es/simulation/rutherford-scattering "Dispersión de Rutherford - phet.colorado.edu")  

[![Experimento de Rutherford (vídeo 15 s, inglés)](https://img.youtube.com/vi/sft5xx3mltM/0.jpg)](https://www.youtube.com/watch?v=sft5xx3mltM "Experimento de Rutherford (vídeo 15 s, inglés)")

[Simulación de la experiencia de Rutherford - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica_//cuantica/experiencias/rutherford/rutherford1.html) Nivel universitario  
  
[Rutherford Atomic Virtual Lab. Open Source Physics Singapore - play.google.com](https://play.google.com/store/apps/details?id=com.ionicframework.rutherfordapp857799)  

[Dispersión de Rutherford - www.walter-fendt.de](https://www.walter-fendt.de/html5/phes/rutherfordscattering_es.htm)  

Rutherford Nuclear Model Wasn't Inspired by the Gold Foil Experiment  
[![](https://img.youtube.com/vi/eoorE5nEQQg/0.jpg)](https://www.youtube.com/watch?v=eoorE5nEQQg "Rutherford Nuclear Model Wasn't Inspired by the Gold Foil Experiment")  
[On a diffuse reflection of the α-particles. H. Gegier and Ernest Marsden. Published:31 July 1909](https://royalsocietypublishing.org/doi/10.1098/rspa.1909.0054)  
[The scattering of α-particles by matter. Hans Geiger. Published:14 April 1910](https://royalsocietypublishing.org/doi/10.1098/rspa.1910.0038)  
[LXI. The laws of deflexion of a particles through large angles. Dr. H. Geiger. & E. Marsden](https://www.tandfonline.com/doi/full/10.1080/14786440408634197)  

Se puede asociar a colisiones y física de partículas  
[2. Colisiones: el método experimental. 2.1 CÓMO AVERIGUAR LA FORMA DE UN OBJETO INVISIBLE. LANZANDO PROYECTILES CONTRA ÉL: COLISIONES ELÁSTICAS ](http://palmera.pntic.mec.es/%7Efbarrada/aula/aula21.html)  
![](https://pbs.twimg.com/media/F8ura8hXoAAwYAD?format=jpg)  

Rutherford Scattering - FlinnScientific  
[![](https://img.youtube.com/vi/RszS2WlKscE/0.jpg)](https://www.youtube.com/watch?v=RszS2WlKscE "Rutherford Scattering - FlinnScientific")  


##  Modelo atómico de Bohr

[Chapter 4: Niels Bohr and the Quantum Atom - faqs.org](http://www.faqs.org/docs/qp/chap04.html)  

[Modelo atómico de Bohr - educaplus.org](http://www.educaplus.org/play-83-Modelo-at%C3%B3mico-de-Bohr.html)  
[Teoría de Bohr del Átomo de Hidrógeno - walter-fendt.de](https://www.walter-fendt.de/html5/phes/bohrmodel_es.htm)  

##  Modelo atómico cuántico - actual
[Modelo atómico mecanocuántico - educ.ar](https://www.educ.ar/recursos/40704/modelo-atomico-mecanocuantico)  
En el video se explica cómo, a partir de las propuestas de Luis de Broglie, Werner Heisenberg y Erwin Schrödinger, se creó el modelo atómico actual, también conocido como modelo orbital o modelo cuántico-ondulatorio.   

##  Vídeos sobre escala del átomo  
[![How Small Is An Atom? Spoiler: Very Small. (5 min) Kurz Gesagt - In a Nutshell](https://img.youtube.com/vi/_lNF3_30lUE/0.jpg "How Small Is An Atom? Spoiler: Very Small. (5 min) Kurz Gesagt - In a Nutshell")](https://www.youtube.com/watch?v=_lNF3_30lUE) 

[Jon Bergmann: Just how small is an atom? (5 min) - ted.com](https://www.ted.com/talks/just_how_small_is_an_atom) 

## Visualización del átomo
Enlaza con orbitales y física de partículas

[twitter martinmbauer/status/1532258418802708480](https://twitter.com/martinmbauer/status/1532258418802708480)  
This is the first series of "photographs" of a hydrogen atom. But how do you take "photos" at (sub)atomic scales ?    1/13  
The motivation for this thread is this visualisation of a proton I posted last week that received several disappointed comments that it is not made from real pictures or a simulation  2/13  
But lets first look at atoms. A hydrogen atom is a proton and an electron. The proton is in the center, the electron is...we don't know. Before we measure it we can only say something about the probability where to find it, but not where exactly. That's Quantum Mechanics 3/13  
![](https://pbs.twimg.com/media/FUMJ0bEXsAEHaiQ?format=png)  
To *look at* the electron in a hydrogen atom means to hit it with a photon. If the wavelength of the photon is too long it does not resolve the electron. If it is short enough to do so, it kicks the electron out of the hydrogen atom 5/13  
So you can only "look at" each atom once. But you can use a cloud of hydrogen atoms and keep kicking the electron out of different atoms many many times. It will not be at the same exact place, but always somewhere in the predicted region because QM is right!  6/13  
This is exactly how the pictures in the first tweet were produced. Electrons are knocked out of a beam of hydrogen atoms with a laser and then recorded on a screen very much like a picture tube in an old TV. 7/13  
Now for the proton. Even though a proton is made from quarks and gluons you can *never* ever knock them out of a proton. This is the strong force after all, not the electromagnetic force holding together hydrogen atoms  8/13  
Also the proton is 10.000 times smaller than the hydrogen atom, no laser is powerful enough to resolve its substructure. You need a collider. 9/13  
![](https://pbs.twimg.com/media/FUOoY5bXsAEoVu8?format=jpg)  
Now we can hit different protons with very energetic photons (or other particles, electrons, other protons...) and measure the outcome of this scattering process.  For example the angle by which electrons scatter off the proton.  For pp collisions there is debris 10/13  
Based on many measurements of many collisions and depending on the momentum transfer in these collisions the proton does not look like a billiard ball, but is better described as made from 3 quarks (high x), or a cloud of quarks and gluons (low x).  11/13  
In contrast to the hydrogen atom we can't solve the equations of QCD exactly. But we can describe what the proton looks like at different momentum transfers. One of the pioneers working on these equations is the latest Nobel prize laureate Giorgio Parisi (the P in DGLAP)  12/13  
Data from decades of measurements of proton scattering processes has been interpreted to produce the images physicists and artists @JLab_News and @ArtsatMIT have made. Here is a great video explaining in more detail how the animations were produced  13/13  
The first images of the hydrogen atom were produced by Aneta Stodolna and her team.   14/13  
[Hydrogen Atoms under Magnification: Direct Observation of the Nodal Structure of Stark States - aps.org](https://physics.aps.org/featured-article-pdf/10.1103/PhysRevLett.110.213001)  
[![](https://img.youtube.com/vi/XHYUSKD6V9s/0.jpg)](https://www.youtube.com/watch?v=XHYUSKD6V9s "What physicists do with light: Aneta Stodolna")  

##  Partículas constituyentes del átomo

Ver también en [física de partículas](/home/recursos/fisica/fisica-de-particulas)  

###  [Electrón](/home/recursos/fisica/fisica-de-particulas/electron)

###  Quarks, hadrones
 [twitter MolaSaber/status/928005553397358593](https://twitter.com/MolaSaber/status/928005553397358593)  
 ![](https://pbs.twimg.com/media/DODvuN-XUAEeBgT.png)  
 
##  Ejercicios
 [monografias.com Estructura atómica y tabla periódica](http://www.monografias.com/trabajos104/estructura-atomica-y-tabla-periodica/estructura-atomica-y-tabla-periodica.shtml)  
 
 [matematicasfisicaquimica.com Ejercicios Resueltos de Estructura del Átomo para Física y Química de Secundaria](http://www.matematicasfisicaquimica.com/fisica-quimica-eso/41-fisica-y-quimica-3o-eso/1090-ejercicios-resueltos-estructura-atomo-fisica-quimica-secundaria.html)  
 
[elsaposabio.com Protones, neutrones y electrones. Isótopos. Iones 02](http://www.elsaposabio.com/quimica/?p=780)  

Categoría: ESTRUCTURA ATÓMICA [http://www.elsaposabio.com/quimica/?cat=25](http://www.elsaposabio.com/quimica/?cat=25)   
  
[mestreacasa.gva.es Estructura atómica y Sistema Periódico. Ejercicios·FÍSICA Y QUÍMICA 1º DE BACHILLERATO](http://mestreacasa.gva.es/c/document_library/get_file?folderId=500015576934&name=DLFE-988909.pdf)  

[twitter quirogafyq/status/1088754518563659777](https://twitter.com/quirogafyq/status/1088754518563659777)  
Gamificando las clases de #Física y #Química de 2º de ESO. Hoy tocó configuración electrónica, sobra decir que fué un éxito. Esto de los dados es un vicio. Gracias a @ProfaDeQuimica y a @bjmahillo por las ideas. Como mola este claustro virtual. #gamificacion #gamification.  
[![](https://pbs.twimg.com/media/DxwHZrgX0AEt6mJ.jpg "") ](https://pbs.twimg.com/media/DxwHZrgX0AEt6mJ.jpg)  
[![](https://pbs.twimg.com/media/DxwHZrkXQAAZ4h6.jpg "") ](https://pbs.twimg.com/media/DxwHZrkXQAAZ4h6.jpg)  


Los ficheros se vevían inicialmente anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia quimica/recursos-estructura-del-atomo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/quimica/recursos-estructura-del-atomo)  

