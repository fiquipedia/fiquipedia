# Currículo 3º ESO Física y Química (LOMCE)

Para Madrid ver DECRETO 48/2015 en  [legislación](/home/legislacion-educativa)   
El decreto fija los criterios de evaluación y estándares de aprendizaje evaluables de manera conjunta para  [2º ESO](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso)  y 3º ESO  
Los contenidos son iguales para 2º ESO y 3º ESO en BOE, pero distintos en Decreto 48/2015  
  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver Contenidos currículo con enlaces a recursos: 3º ESO Física y Química (LOMCE) (pendiente)  

## Comparación para Física y Química de 3º ESO de currículo LOMCE con  [currículo LOE](/home/materias/eso/fisica-y-quimica-3-eso/curriculofisicayquimica-3eso) 

Ver  [comparación currículo Física y Química 3º ESO](/home/materias/eso/fisica-y-quimica-3-eso/comparacion-curriculo-3-eso-fisica-y-quimica)

## Currículo de 3º ESO condicionado por laboratorio, TIC y simulaciones  

El currículo cita a veces laboratorio y simulaciones, por lo que si no hay laboratorio (por ejemplo por falta de desdoble es habitual) o no hay posibilidad de un aula de informática para todos los alumnos, hay ciertas partes del currículo que no se pueden cubrir, y se citan aquí. Hay algunos para los que sí es posible (por ejemplo identificar material de laboratorio, o proponer un experimento), aunque usar el laboratorio sería lo ideal.  
  
Bloque 2. La materia  
3. ...resultados obtenidos en experiencias de laboratorio o simulaciones por ordenador.  
4.3. Realiza experiencias sencillas de preparación de disoluciones, describe el procedimiento seguido y el material utilizado, determina la concentración y la expresa en gramos por litro.  
10.2. Presenta, utilizando las TIC, las propiedades y aplicaciones de algún elemento y/o compuesto químico de especial interés a partir de una búsqueda guiada de información bibliográfica y/o digital.  
  
Estándares que se pueden hacer teóricamente, pero se pueden ver como asociables a laboratorio  
5.1. Diseña métodos de separación de mezclas según las propiedades características de las sustancias que las componen, describiendo el material de laboratorio adecuado.   
  
Bloque 3. Los cambios  
4. ... a través de experiencias sencillas en el laboratorio y/o de simulaciones por ordenador.  
4.1 ... comprueba experimentalmente que se cumple la ley de conservación de la masa.  
5. Comprobar mediante experiencias sencillas de laboratorio...  
Estándares que se pueden hacer teóricamente, pero se pueden ver como asociables a laboratorio  
*1.2. Describe el procedimiento de realización de experimentos* sencillos en los que se ponga de manifiesto la formación de nuevas sustancias y reconoce que se trata de cambios químicos.  
  
Bloque 4. El movimiento y las fuerzas  
2.1. Determina, *experimentalmente* o a través de aplicaciones informáticas,...  
10.2. **Construye**, y describe el procedimiento seguido para ello, una brújula elemental para localizar el norte utilizando el campo magnético terrestre.   
11.1. Comprueba y establece la relación entre el paso de corriente eléctrica y el magnetismo, **construyendo** un electroimán.  
 11.2. Reproduce los experimentos de Oersted y de Faraday, en el laboratorio o mediante simuladores virtuales,...  
 12.1. Realiza un **informe empleando las TIC** a partir de observaciones o búsqueda guiada de información que relacione las distintas fuerzas que aparecen en la naturaleza y los distintos fenómenos asociados a ellas.  
  
Estándares que se pueden hacer teóricamente, pero se pueden ver como asociables a laboratorio  
1.2. Establece la relación entre el alargamiento producido en un muelle y las fuerzas que han producido esos alargamientos, describiendo el material a utilizar y el procedimiento a seguir para ello y *pudiendo comprobarlo experimentalmente.*  
1.4. Describe la utilidad del dinamómetro para medir la fuerza elástica y *registra los resultados en tablas y representaciones gráficas, expresando el resultado experimental en unidades en el Sistema Internacional.*  
  
Bloque 5. Energía  
2. Identificar los diferentes tipos de energía puestos de manifiesto en fenómenos cotidianos y en experiencias sencillas realizadas en el laboratorio.   
 4. Interpretar los efectos de la energía térmica sobre los cuerpos en situaciones cotidianas y en experiencias de laboratorio.   
 9. ...en el laboratorio o mediante aplicaciones virtuales interactivas.  
 9.2. Construye... deduciendo de forma experimental las consecuencias de la conexión de generadores y receptores en serie o en paralelo.  
10.2. Construye, y describe el procedimiento seguido pare ello, una brújula elemental para localizar el norte utilizando el campo magnético terrestre.  

## Contenidos 3º ESO

### Bloque 1. La actividad científica
1. El método científico: sus etapas.  
2. Medida de magnitudes.  
- Sistema Internacional de Unidades.  
- Notación científica.  
3. Utilización de las tecnologías de la información y la comunicación.  
4. El trabajo en el laboratorio.  
5. Proyecto de Investigación  

### Bloque 2. La materia
1. Modelo cinético-molecular  
2. Leyes de los gases  
3. Estructura atómica. Isótopos.  
- Modelos atómicos.  
4. El sistema periódico de los elementos.  
5. Uniones entre átomos: moléculas y cristales.  
6. Masas atómicas y moleculares.  
7. Elementos y compuestos de especial interés con aplicaciones industriales, tecnológicas y biomédicas.  
8. Formulación y nomenclatura de compuestos binarios siguiendo las normas IUPAC  

### Bloque 3. Los cambios
1. La reacción química  
2. Cálculos estequiométricos sencillos  
3. Ley de conservación de la masa  
4. La química en la sociedad y el medio ambiente  

### Bloque 4. El movimiento y las fuerzas
1. Las fuerzas.  
- Efectos.  
- Velocidad media, velocidad instantánea y aceleración  
2. Las fuerzas de la naturaleza  

### Bloque 5. Energía
1. Electricidad y circuitos eléctricos. Ley de Ohm  
2. Dispositivos electrónicos de uso frecuente.  
3. Aspectos industriales de la energía.  
4. Fuentes de energía  
5. Uso racional de la energía  

## Criterios de evaluación y estándares de aprendizaje evaluables. 2º y 3º ESO

### Bloque 1. La actividad científica
1. Reconocer e identificar las características del método científico.  
1.1. Formula hipótesis para explicar fenómenos cotidianos utilizando teorías y modelos científicos.  
1.2. Registra observaciones, datos y resultados de manera organizada y rigurosa, y los comunica de forma oral y escrita utilizando esquemas, gráficos, tablas y expresiones matemáticas.  
2. Valorar la investigación científica y su impacto en la industria y en el desarrollo de la sociedad.  
2.1. Relaciona la investigación científica con las aplicaciones tecnológicas en la vida cotidiana.  
3. Conocer los procedimientos científicos para determinar magnitudes.  
3.1. Establece relaciones entre magnitudes y unidades utilizando, preferentemente, el Sistema Internacional de Unidades y la notación científica para expresar los resultados.  
4. Reconocer los materiales, e instrumentos básicos presentes del laboratorio de Física y en de Química; conocer y respetar las normas de seguridad y de eliminación de residuos para la protección del medioambiente.  
4.1. Reconoce e identifica los símbolos más frecuentes utilizados en el etiquetado de productos químicos e instalaciones, interpretando su significado.  
4.2. Identifica material e instrumentos básicos de laboratorio y conoce su forma de utilización para la realización de experiencias respetando las normas de seguridad e identificando actitudes y medidas de actuación preventivas.  
5. Interpretar la información sobre temas científicos de carácter divulgativo que aparece en publicaciones y medios de comunicación.  
5.1. Selecciona, comprende e interpreta información relevante en un texto de divulgación científica y transmite las conclusiones obtenidas utilizando el lenguaje oral y escrito con propiedad.  
5.2. Identifica las principales características ligadas a la fiabilidad y objetividad del flujo de información existente en internet y otros medios digitales.  
6. Desarrollar pequeños trabajos de investigación en los que se ponga en práctica la aplicación del método científico y la utilización de las TIC.  
6.1. Realiza pequeños trabajos de investigación sobre algún tema objeto de estudio aplicando el método científico, y utilizando las TIC para la búsqueda y selección de información y presentación de conclusiones.  
6.2. Participa, valora, gestiona y respeta el trabajo individual y en equipo.  

### Bloque 2. La materia
1. Reconocer las propiedades generales y características específicas de la materia y relacionarlas con su naturaleza y sus aplicaciones.  
1.1. Distingue entre propiedades generales y propiedades características de la materia, utilizando estas últimas para la caracterización de sustancias.  
1.2. Relaciona propiedades de los materiales de nuestro entorno con el uso que se hace de ellos.  
1.3. Describe la determinación experimental del volumen y de la masa de un sólido y calcula su densidad.  
2. Justificar las propiedades de los diferentes estados de agregación de la materia y sus cambios de estado, a través del modelo cinético-molecular.  
2.1. Justifica que una sustancia puede presentarse en distintos estados de agregación dependiendo de las condiciones de presión y temperatura en las que se encuentre.  
2.2. Explica las propiedades de los gases, líquidos y sólidos utilizando el modelo cinético-molecular.  
2.3. Describe e interpreta los cambios de estado de la materia utilizando el modelo cinético-molecular y lo aplica a la interpretación de fenómenos cotidianos.  
2.4. Deduce a partir de las gráficas de calentamiento de una sustancia sus puntos de fusión y ebullición, y la identifica utilizando las tablas de datos necesarias.  
3. Establecer las relaciones entre las variables de las que depende el estado de un gas a partir de representaciones gráficas y/o tablas de resultados obtenidos en, experiencias de laboratorio o simulaciones por ordenador.  
3.1. Justifica el comportamiento de los gases en situaciones cotidianas relacionándolo con el modelo cinético-molecular.  
3.2. Interpreta gráficas, tablas de resultados y experiencias que relacionan la presión, el volumen y la temperatura de un gas utilizando el modelo cinético-molecular y las leyes de los gases.  
4. Identificar sistemas materiales como sustancias puras o mezclas y valorar la importancia y las aplicaciones de mezclas de especial interés.  
4.1. Distingue y clasifica sistemas materiales de uso cotidiano en sustancias puras y mezclas, especificando en este último caso si se trata de mezclas homogéneas, heterogéneas o coloides.  
4.2. Identifica el disolvente y el soluto al analizar la composición de mezclas homogéneas de especial interés.  
4.3. Realiza experiencias sencillas de preparación de disoluciones, describe el procedimiento seguido y el material utilizado, determina la concentración y la expresa en gramos por litro.  
5. Proponer métodos de separación de los componentes de una mezcla.  
5.1. Diseña métodos de separación de mezclas según las propiedades características de las sustancias que las componen, describiendo el material de laboratorio adecuado.  
6. Reconocer que los modelos atómicos son instrumentos interpretativos de las distintas teorías y la necesidad de su utilización para la interpretación y comprensión de la estructura interna de la materia.  
6.1. Representa el átomo, a partir del número atómico y el número másico, utilizando el modelo planetario.  
6.2. Describe las características de las partículas subatómicas básicas y su localización en el átomo.  
6.3. Relaciona la notación AZX con el número atómico, el número másico determinando el número de cada uno de los tipos de partículas subatómicas básicas.  
7. Analizar la utilidad científica y tecnológica de los isótopos radiactivos.  
7.1. Explica en qué consiste un isótopo y comenta aplicaciones de los isótopos radiactivos, la problemática de los residuos originados y las soluciones para la gestión de los mismos.  
8. Interpretar la ordenación de los elementos en la Tabla Periódica y reconocer los más relevantes a partir de sus símbolos.  
8.1. Justifica la actual ordenación de los elementos en grupos y periodos en la Tabla Periódica.  
8.2. Relaciona las principales propiedades de metales, no metales y gases nobles con su posición en la Tabla Periódica y con su tendencia a formar iones, tomando como referencia el gas noble más próximo.  
9. Conocer cómo se unen los átomos para formar estructuras más complejas y explicar las propiedades de las agrupaciones resultantes.  
9.1. Conoce y explica el proceso de formación de un ion a partir del átomo correspondiente, utilizando la notación adecuada para su representación.  
9.2. Explica cómo algunos átomos tienden a agruparse para formar moléculas interpretando este hecho en sustancias de uso frecuente y calcula sus masas moleculares...  
10. Diferenciar entre átomos y moléculas, y entre elementos y compuestos en sustancias de uso frecuente y conocido.  
10.1. Reconoce los átomos y las moléculas que componen sustancias de uso frecuente, clasificándolas en elementos o compuestos, basándose en su expresión química.  
10.2. Presenta, utilizando las TIC, las propiedades y aplicaciones de algún elemento y/o compuesto químico de especial interés a partir de una búsqueda guiada de información bibliográfica y/o digital.  
11. Formular y nombrar compuestos binarios siguiendo las normas IUPAC.  
11.1. Utiliza el lenguaje químico para nombrar y formular compuestos binarios siguiendo las normas IUPAC.  

### Bloque 3. Los cambios
1. Distinguir entre cambios físicos y químicos mediante la realización de experiencias sencillas que pongan de manifiesto si se forman o no nuevas sustancias.  
1.1. Distingue entre cambios físicos y químicos en acciones de la vida cotidiana en función de que haya o no formación de nuevas sustancias.  
1.2. Describe el procedimiento de realización experimentos sencillos en los que se ponga de manifiesto la formación de nuevas sustancias y reconoce que se trata de cambios químicos.  
2. Caracterizar las reacciones químicas como cambios de unas sustancias en otras.  
2.1. Identifica cuáles son los reactivos y los productos de reacciones químicas sencillas interpretando la representación esquemática de una reacción química.  
3. Describir a nivel molecular el proceso por el cual los reactivos se transforman en productos en términos de la teoría de colisiones.  
3.1. Representa e interpreta una reacción química a partir de la teoría atómico-molecular y la teoría de colisiones.  
4. Deducir la ley de conservación de la masa y reconocer reactivos y productos a través de experiencias sencillas en el laboratorio y/o de simulaciones por ordenador.  
4.1. Reconoce cuáles son los reactivos y los productos a partir de la representación de reacciones químicas sencillas, y comprueba experimentalmente que se cumple la ley de conservación de la masa.  
5. Comprobar mediante experiencias sencillas de laboratorio la influencia de determinados factores en la velocidad de las reacciones químicas.  
5.1. Propone el desarrollo de un experimento sencillo que permita comprobar experimentalmente el efecto de la concentración de los reactivos en la velocidad de formación de los productos de una reacción química, justificando este efecto en términos de la teoría de colisiones.  
5.2. Interpreta situaciones cotidianas en las que la temperatura influye significativamente en la velocidad de la reacción.  
6. Reconocer la importancia de la química en la obtención de nuevas sustancias y su importancia en la mejora de la calidad de vida de las personas.  
6.1. Clasifica algunos productos de uso cotidiano en función de su procedencia natural o sintética.  
6.2. Identifica y asocia productos procedentes de la industria química con su contribución a la mejora de la calidad de vida de las personas.  
7. Valorar la importancia de la industria química en la sociedad y su influencia en el medio ambiente.  
7.1. Describe el impacto medioambiental del dióxido de carbono, los óxidos de azufre, los óxidos de nitrógeno y los CFC y otros gases de efecto invernadero relacionándolo con los problemas medioambientales de ámbito global.  
7.2. Propone medidas y actitudes, a nivel individual y colectivo, para mitigar los problemas medioambientales de importancia global.  
7.3. Defiende razonadamente la influencia que el desarrollo de la industria química ha tenido en el progreso de la sociedad, a partir de fuentes científicas de distinta procedencia.  

### Bloque 4. El movimiento y las fuerzas
1. Reconocer el papel de las fuerzas como causa de los cambios en el estado de movimiento y de las deformaciones.  
1.1. En situaciones de la vida cotidiana, identifica las fuerzas que intervienen y las relaciona con sus correspondientes efectos en la deformación o en la alteración del estado de movimiento de un cuerpo.  
1.2. Establece la relación entre el alargamiento producido en un muelle y las fuerzas que han producido esos alargamientos, describiendo el material a utilizar y el procedimiento a seguir para ello y poder comprobarlo experimentalmente.  
1.3. Establece la relación entre una fuerza y su correspondiente efecto en la deformación o la alteración del estado de movimiento de un cuerpo.  
1.4. Describe la utilidad del dinamómetro para medir la fuerza elástica y registra los resultados en tablas y representaciones gráficas expresando el resultado experimental en unidades en el Sistema Internacional.  
2. Establecer la velocidad de un cuerpo como la relación entre el espacio recorrido y el tiempo invertido en recorrerlo.  
2.1. Determina, experimentalmente o a través de aplicaciones informáticas, la velocidad media de un cuerpo interpretando el resultado.  
2.2. Realiza cálculos para resolver problemas cotidianos utilizando el concepto de velocidad.  
3. Diferenciar entre velocidad media e instantánea a partir de gráficas espacio/tiempo y velocidad/tiempo, y deducir el valor de la aceleración utilizando éstas últimas.  
3.1. Deduce la velocidad media e instantánea a partir de las representaciones gráficas del espacio y de la velocidad en función del tiempo.  
3.2. Justifica si un movimiento es acelerado o no a partir de las representaciones gráficas del espacio y de la velocidad en función del tiempo.  
4. Valorar la utilidad de las máquinas simples en la transformación de un movimiento en otro diferente, y la reducción de la fuerza aplicada necesaria.  
4.1. Interpreta el funcionamiento de máquinas mecánicas simples considerando la fuerza y la distancia al eje de giro y realiza cálculos sencillos sobre el efecto multiplicador de la fuerza producido por estas máquinas.  
5. Comprender el papel que juega el rozamiento en la vida cotidiana.  
5.1. Analiza los efectos de las fuerzas de rozamiento y su influencia en el movimiento de los seres vivos y los vehículos.  
6. Considerar la fuerza gravitatoria como la responsable del peso de los cuerpos, de los movimientos orbitales y de los distintos niveles de agrupación en el Universo, y analizar los factores de los que depende.  
6.1. Relaciona cualitativamente la fuerza de gravedad que existe entre dos cuerpos con las masas de los mismos y la distancia que los separa.  
6.2. Distingue entre masa y peso calculando el valor de la aceleración de la gravedad a partir de la relación entre ambas magnitudes.  
6.3. Reconoce que la fuerza de gravedad mantiene a los planetas girando alrededor del Sol, y a la Luna alrededor de nuestro planeta, justificando el motivo por el que esta atracción no lleva a la colisión de los dos cuerpos.  
7. Identificar los diferentes niveles de agrupación entre cuerpos celestes, desde los cúmulos de galaxias a los sistemas planetarios, y analizar el orden de magnitud de las distancias implicadas.  
7.1. Relaciona cuantitativamente la velocidad de la luz con el tiempo que tarda en llegar a la Tierra desde objetos celestes lejanos y con la distancia a la que se encuentran dichos objetos, interpretando los valores obtenidos.  
8. Conocer los tipos de cargas eléctricas, su papel en la constitución de la materia y las características de las fuerzas que se manifiestan entre ellas.  
8.1. Explica la relación existente entre las cargas eléctricas y la constitución de la materia y asocia la carga eléctrica de los cuerpos con un exceso o defecto de electrones.  
8.2. Relaciona cualitativamente la fuerza eléctrica que existe entre dos cuerpos con su carga y la distancia que los separa, y establece analogías y diferencias entre las fuerzas gravitatoria y eléctrica.  
9. Interpretar fenómenos eléctricos mediante el modelo de carga eléctrica y valorar la importancia de la electricidad en la vida cotidiana.  
9.1. Justifica razonadamente situaciones cotidianas en las que se pongan de manifiesto fenómenos relacionados con la electricidad estática.  
10. Justificar cualitativamente fenómenos magnéticos y valorar la contribución del magnetismo en el desarrollo tecnológico.  
10.1. Reconoce fenómenos magnéticos identificando el imán como fuente natural del magnetismo y describe su acción sobre distintos tipos de sustancias magnéticas.  
10.2. Construye, y describe el procedimiento seguido pare ello, una brújula elemental para localizar el norte utilizando el campo magnético terrestre.  
11. Comparar los distintos tipos de imanes, analizar su comportamiento y deducir mediante experiencias las características de las fuerzas magnéticas puestas de manifiesto, así como su relación con la corriente eléctrica.  
11.1. Comprueba y establece la relación entre el paso de corriente eléctrica y el magnetismo, construyendo un electroimán.  
11.2. Reproduce los experimentos de Oersted y de Faraday, en el laboratorio o mediante simuladores virtuales, deduciendo que la electricidad y el magnetismo son dos manifestaciones de un mismo fenómeno.  
12. Reconocer las distintas fuerzas que aparecen en la naturaleza y los distintos fenómenos asociados a ellas.  
12.1. Realiza un informe empleando las TIC a partir de observaciones o búsqueda guiada de información que relacione las distintas fuerzas que aparecen en la naturaleza y los distintos fenómenos asociados a ellas.  

### Bloque 5. Energía
1. Reconocer que la energía es la capacidad de producir transformaciones o cambios.  
1.1. Argumenta que la energía se puede transferir, almacenar o disipar, pero no crear ni destruir, utilizando ejemplos.  
1.2. Reconoce y define la energía como una magnitud expresándola en la unidad correspondiente en el Sistema Internacional.  
2. Identificar los diferentes tipos de energía puestos de manifiesto en fenómenos cotidianos y en experiencias sencillas realizadas en el laboratorio.  
2.1. Relaciona el concepto de energía con la capacidad de producir cambios e identifica los diferentes tipos de energía que se ponen de manifiesto en situaciones cotidianas explicando las transformaciones de unas formas a otras.  
3. Relacionar los conceptos de energía, calor y temperatura en términos de la teoría cinético-molecular y describir los mecanismos por los que se transfiere la energía térmica en diferentes situaciones cotidianas.  
3.1. Explica el concepto de temperatura en términos del modelo cinético-molecular diferenciando entre temperatura, energía y calor.  
3.2. Conoce la existencia de una escala absoluta de temperatura y relaciona las escalas de Celsius y Kelvin.  
3.3. Identifica los mecanismos de transferencia de energía reconociéndolos en diferentes situaciones cotidianas y fenómenos atmosféricos, justificando la selección de materiales para edificios y en el diseño de sistemas de calentamiento.  
4. Interpretar los efectos de la energía térmica sobre los cuerpos en situaciones cotidianas y en experiencias de laboratorio.  
4.1. Explica el fenómeno de la dilatación a partir de alguna de sus aplicaciones como los termómetros de líquido, juntas de dilatación en estructuras, etc.  
4.2. Explica la escala Celsius estableciendo los puntos fijos de un termómetro basado en la dilatación de un líquido volátil.  
4.3. Interpreta cualitativamente fenómenos cotidianos y experiencias donde se ponga de manifiesto el equilibrio térmico asociándolo con la igualación de temperaturas.  
5. Valorar el papel de la energía en nuestras vidas, identificar las diferentes fuentes, comparar el impacto medioambiental de las mismas y reconocer la importancia del ahorro energético para un desarrollo sostenible.  
5.1. Reconoce, describe y compara las fuentes renovables y no renovables de energía, analizando con sentido crítico su impacto medioambiental.  
6. Conocer y comparar las diferentes fuentes de energía empleadas en la vida diaria en un contexto global que implique aspectos económicos y medioambientales.  
6.1. Compara las principales fuentes de energía de consumo humano, a partir de la distribución geográfica de sus recursos y los efectos medioambientales.  
6.2. Analiza la predominancia de las fuentes de energía convencionales) frente a las alternativas, argumentando los motivos por los que estas últimas aún no están suficientemente explotadas.  
7. Valorar la importancia de realizar un consumo responsable de las fuentes energéticas.   
7.1. Interpreta datos comparativos sobre la evolución del consumo de energía mundial proponiendo medidas que pueden contribuir al ahorro individual y colectivo.  
8. Explicar el fenómeno físico de la corriente eléctrica e interpretar el significado de las magnitudes intensidad de corriente, diferencia de potencial y resistencia, así como las relaciones entre ellas.  
8.1. Explica la corriente eléctrica como cargas en movimiento a través de un conductor.  
8.2. Comprende el significado de las magnitudes eléctricas intensidad de corriente, diferencia de potencial y resistencia, y las relaciona entre sí utilizando la ley de Ohm.  
8.3. Distingue entre conductores y aislantes reconociendo los principales materiales usados como tales.  
9. Comprobar los efectos de la electricidad y las relaciones entre las magnitudes eléctricas mediante el diseño y construcción de circuitos eléctricos y electrónicos sencillos, en el laboratorio o mediante aplicaciones virtuales interactivas.  
9.1. Describe el fundamento de una máquina eléctrica, en la que la electricidad se transforma en movimiento, luz, sonido, calor, etc. mediante ejemplos de la vida cotidiana, identificando sus elementos principales.  
9.2. Construye circuitos eléctricos con diferentes tipos de conexiones entre sus elementos, deduciendo de forma experimental las consecuencias de la conexión de generadores y receptores en serie o en paralelo.  
9.3. Aplica la ley de Ohm a circuitos sencillos para calcular una de las magnitudes involucradas a partir de las dos, expresando el resultado en las unidades del Sistema Internacional.  
9.4. Utiliza aplicaciones virtuales interactivas para simular circuitos y medir las magnitudes eléctricas.  
10. Valorar la importancia de los circuitos eléctricos y electrónicos en las instalaciones eléctricas e instrumentos de uso cotidiano, describir su función básica e identificar sus distintos componentes.  
10.1. Asocia los elementos principales que forman la instalación eléctrica típica de una vivienda con los componentes básicos de un circuito eléctrico.  
10.2. Comprende el significado de los símbolos y abreviaturas que aparecen en las etiquetas de dispositivos eléctricos.  
10.3. Identifica y representa los componentes más habituales en un circuito eléctrico: conductores, generadores, receptores y elementos de control describiendo su correspondiente función.  
10.4. Reconoce los componentes electrónicos básicos describiendo sus aplicaciones prácticas y la repercusión de la miniaturización del microchip en el tamaño y precio de los dispositivos.  
11. Conocer la forma en la que se genera la electricidad en los distintos tipos de centrales eléctricas, así como su transporte a los lugares de consumo.  
11.1. Describe el proceso por el que las distintas fuentes de energía se transforman en energía eléctrica en las centrales eléctricas, así como los métodos de transporte y almacenamiento de la misma.  
 
