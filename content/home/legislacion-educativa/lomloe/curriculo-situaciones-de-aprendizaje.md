# Currículo LOMLOE: situaciones de aprendizaje 

Resumen rápido: se pueden traducir por "actividades" con cierto foco en que estén contextualizadas, sean significativas y relevantes.   
Hay quien plantea que se pueden traducir por "unidades didácticas", al entender que es un conjunto de sesiones en las que se tratan ciertos contenidos realizando esas actividades.   

La parte general de conceptos y estructura currículo en página separada  [Currículo LOMLOE concepto y estructura](/home/legislacion-educativa/lomloe/curriculo-concepto-estructura), donde se citan documentos de los que se cita texto 

[Real Decreto 217/2022 ESO Artículo 2. Definiciones.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a2)  
[Real Decreto 243/2022 Bachillerato Artículo 2. Definiciones.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a2)  
> f) Situaciones de aprendizaje: situaciones y actividades que implican el despliegue por parte del alumnado de actuaciones asociadas a competencias clave y competencias específicas y que contribuyen a la adquisición y desarrollo de las mismas.  

>Aunque el eje inspirador central del Perfil de salida es, sin duda, el marco de la Recomendación del 
Consejo de 2018, en su elaboración se han tenido también en cuenta los principales retos y desafíos 
globales a los que va a verse confrontado nuestro alumnado, cuyo abordaje demanda el despliegue 
de las competencias clave para acercarse a situaciones, cuestiones y problemas de la vida cotidiana, 
lo que, a su vez, proporcionará el necesario punto de apoyo para el diseño de **situaciones de aprendizaje** significativas  y  relevantes.   

[Borrador de RD de enseñanzas mínimas de la ESO](https://educagob.educacionyfp.gob.es/dam/jcr:366c73cd-b09e-4997-9260-2c51f9bf0d83/proyecto-rd-secundaria-completo-.pdf)  

Es curioso como precisamente en el borrador de currículo de Física y Química es el único sitio donde se citan "las distintas situaciones de aprendizaje que se proponen", aunque en el borrador no hay propuestas  

> Son estas competencias específicas las que justifican
cuáles son el resto de los elementos del currículo de la materia de Física y
Química en la Educación Secundaria Obligatoria, necesarios para responder con
precisión a dos de las necesidades curriculares del alumnado: los saberes
básicos de la materia y los criterios de evaluación de los mismos; ejemplificados
ambos **a través de las distintas situaciones de aprendizaje que se proponen**.
Todos ellos están definidos de manera competencial para asegurar el desarrollo
de las competencias clave más allá de una memorización de contenidos, porque
solo de esta forma el alumnado será capaz de desarrollar el pensamiento
científico para enfrentarse a los posibles problemas de la sociedad que le rodea
y disfrutar de un conocimiento más profundo del mundo.  


[Anexo III. Situaciones de aprendizaje](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#ai-3)  
   
La adquisición y el desarrollo de las competencias clave del Perfil de salida del alumnado al término de la enseñanza básica, que se concretan en las competencias específicas de cada materia o ámbito de la etapa, se verán favorecidos por metodologías didácticas que reconozcan al alumnado como agente de su propio aprendizaje. Para ello es imprescindible la implementación de propuestas pedagógicas que, partiendo de los centros de interés de los alumnos y alumnas, les permitan construir el conocimiento con autonomía y creatividad desde sus propios aprendizajes y experiencias. Las **situaciones de aprendizaje** representan una herramienta eficaz para integrar los elementos curriculares de las distintas materias o ámbitos mediante tareas y actividades significativas y relevantes para resolver problemas de manera creativa y cooperativa, reforzando la autoestima, la autonomía, la reflexión crítica y la responsabilidad.

Para que la adquisición de las competencias sea efectiva, dichas situaciones deben estar bien contextualizadas y ser respetuosas con las experiencias del alumnado y sus diferentes formas de comprender la realidad. Asimismo, deben estar compuestas por tareas complejas cuya resolución conlleve la construcción de nuevos aprendizajes. Con estas situaciones se busca ofrecer al alumnado la oportunidad de conectar y aplicar lo aprendido en contextos cercanos a la vida real. Así planteadas, las situaciones constituyen un componente que, alineado con los principios del Diseño universal para el aprendizaje, permite aprender a aprender y sentar las bases para el aprendizaje a lo largo de la vida, fomentando procesos pedagógicos flexibles y accesibles que se ajusten a las necesidades, las características y los diferentes ritmos de aprendizaje del alumnado.

El diseño de estas situaciones debe suponer la transferencia de los aprendizajes adquiridos por parte del alumnado, posibilitando la articulación coherente y eficaz de los distintos conocimientos, destrezas y actitudes propios de esta etapa. Las situaciones deben partir del planteamiento de unos objetivos claros y precisos que integren diversos saberes básicos. Además, deben proponer tareas o actividades que favorezcan diferentes tipos de agrupamientos, desde el trabajo individual al trabajo en grupos, permitiendo que el alumnado asuma responsabilidades personales y actúe de forma cooperativa en la resolución creativa del reto planteado. Su puesta en práctica debe implicar la producción y la interacción verbal e incluir el uso de recursos auténticos en distintos soportes y formatos, tanto analógicos como digitales. Las **situaciones de aprendizaje** deben fomentar aspectos relacionados con el interés común, la sostenibilidad o la convivencia democrática, esenciales para que el alumnado sea capaz de responder con eficacia a los retos del siglo XXI. 


[Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521)  

>[Artículo 2. Definiciones](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a2)  
>f) Situaciones de aprendizaje: situaciones y actividades que implican el despliegue por parte del alumnado de actuaciones asociadas a competencias clave y competencias específicas, y que contribuyen a la adquisición y desarrollo de las mismas


>[Anexo III. Situaciones de aprendizaje](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#ai-3)  
> La adquisición y el desarrollo de las competencias clave, que se describen en el anexo I de este real decreto y se concretan en las competencias específicas de cada materia, se verán favorecidos por metodologías que reconozcan al alumnado como agente de su propio aprendizaje. Para ello es imprescindible la implementación de propuestas pedagógicas que, partiendo de los centros de interés de los alumnos y alumnas y aumentándolos, les permitan construir el conocimiento con autonomía, iniciativa y creatividad desde sus propios aprendizajes y experiencias. Las **situaciones de aprendizaje** representan una herramienta eficaz para integrar los elementos curriculares de las distintas materias mediante tareas y actividades significativas y relevantes para resolver problemas de manera creativa y cooperativa, reforzando la autoestima, la autonomía, la iniciativa, la reflexión crítica y la responsabilidad.  
> Para que la adquisición de las competencias sea efectiva, dichas situaciones deben estar bien contextualizadas y ser respetuosas con las experiencias del alumnado y sus diferentes formas de comprender la realidad. Asimismo, deben estar compuestas por tareas complejas cuya resolución conlleve la construcción de nuevos aprendizajes y los prepare para su futuro personal, académico y profesional. Con estas situaciones se busca ofrecer al alumnado la oportunidad de conectar y aplicar lo aprendido en contextos de la vida real. Así planteadas, las situaciones constituyen un componente que, alineado con los principios del Diseño universal para el aprendizaje, permite aprender a aprender y sentar las bases para el aprendizaje a lo largo de la vida, fomentando procesos pedagógicos flexibles y accesibles que se ajusten a las necesidades, las características y los diferentes ritmos de aprendizaje del alumnado y que favorezcan su autonomía.  
> El diseño de estas situaciones debe suponer la transferencia de los aprendizajes adquiridos por parte del alumnado, posibilitando la articulación coherente y eficaz de los distintos conocimientos, destrezas y actitudes propios de esta etapa. Las situaciones deben partir del planteamiento de unos objetivos claros y precisos que integren diversos saberes básicos. Además, deben proponer tareas o actividades que favorezcan diferentes tipos de agrupamientos, desde el trabajo individual al trabajo en grupos, permitiendo que el alumnado asuma responsabilidades personales de manera autónoma y actúe de forma cooperativa en la resolución creativa del reto planteado. Su puesta en práctica debe implicar la producción y la interacción verbal e incluir el uso de recursos auténticos en distintos soportes y formatos, tanto analógicos como digitales. Las **situaciones de aprendizaje** deben fomentar aspectos relacionados con el interés común, la sostenibilidad o la convivencia democrática, esenciales para que el alumnado sea capaz de responder con eficacia a los retos del siglo XXI.


### Marzo 2022  
Surge una iniciativa de una editorial asociada a LOMLOE que precisamente se llama SITUACIONS (inicialmente en catalán)  
[Situacions: Analitza per comprendre i comprèn per actuar.](https://www.youtube.com/watch?v=QgXZKkUM5V4)  

[situacions vicensvives.com/](https://situacions.vicensvives.com/)  

[Situaciones Física i Química](https://situacions.vicensvives.com/fiq/projecte/)  
[twitter gogos20/status/1508769453349261319](https://twitter.com/gogos20/status/1508769453349261319)  
Tenía ganas de anunciaros un proyecto que llevamos trabajando @ProfaDeQuimica @fisquiris y @pablofcayqca hace más de un año en la editorial @vicensvives que por fin ha salido a la luz  

### Abril 2022
Borrador de decreto ESO Madrid cita situaciones aprendizaje explícitamente, ver [Currículo Física y Química ESO](/home/materias/eso/curriculo-fisica-y-quimica-eso)    
En trámite de audiencia de mayo las pasa a llamar "actividades".  

### Junio 2022

[Instrucción 12/2022, de 23 de junio, de la Dirección General de Ordenación y Evaluación Educativa, por la que se establecen aspectos de organización y funcionamiento para los centros que impartan educación primaria para el curso 2022/2023](https://www.juntadeandalucia.es/educacion/portals/delegate/content/a256976a-3933-4201-919c-0d69b19ce97d/Instrucci%C3%B3n%2012/2022,%20de%2023%20de%20junio,%20de%20la%20Direcci%C3%B3n%20General%20de%20Ordenaci%C3%B3n%20y%20Evaluaci%C3%B3n%20Educativa,%20por%20la%20que%20se%20establecen%20aspectos%20de%20organizaci%C3%B3n%20y%20funcionamiento%20para%20los%20centros%20que%20impartan%20educaci%C3%B3n%20primaria%20para%20el%20curso%202022/2023)  
En página 144 del pdf hay un "ESQUEMA DE SITUACIÓN DE APRENDIZAJE"  

### Julio 2022

Situaciones de aprendizaje en la LOMLOE.  Pablo - Física y Química  
[![](https://img.youtube.com/vi/EvBf0A15RGA/0.jpg)](https://www.youtube.com/watch?v=EvBf0A15RGA "Situaciones de aprendizaje en la LOMLOE.  Pablo - Física y Química")  
En minuto 7:30 se compara Unidad Didáctica vs Situación de Aprendizaje con ejemplo concreto FQ3ºESO  


[twitter pbeltranp/status/1549823384455970818](https://twitter.com/pbeltranp/status/1549823384455970818)  
Hasta hace poco no era consciente de que había cierto debate sobre a qué se le puede llamar situación de aprendizaje LOMLOE™️. Va un mini-hilo.  
- Que si no tiene contexto, no es situación de aprendizaje.  
- ¿Ein?  
Para que no lo tengáis que buscar, ya os lo pongo aquí. El Anexo III del RD 157/2022 de Enseñanzas Mínimas. Me centraré en las partes resaltadas, referentes al contexto y a la vida cotidiana. Y en Matemáticas.  
Aunque debo decir que dejo aparcada la cuestión, por haberse tratado en anteriores episodios, de la vida cotidiana. Solo señalar que sería preferible hablar de situaciones cercanas y significativas para el alumnado.  
Venga, vamos a verlo con un ejemplo. En efecto, las figuras geométricas se abstraen de formas y cuerpos del mundo real. Por eso, en el primer nivel de razonamiento (Van Hiele), podemos llegar a decir que un rectángulo es como una puerta o que un cuadrado es como una ventana.  
Pero cuando progresamos en los niveles de razonamiento y queremos pasar a trabajar clasificaciones de cuadriláteros, lo haremos con entidades abstractas, no con puertas o ventanas.  
Las actividades de clasificación de cuadriláteros y otras figuras con criterios exclusivos, primero, e inclusivos, después, ya os digo que bien planteadas son situaciones de aprendizaje en toda regla.  
El caso es que el término «contexto» en matemáticas no implica algo real necesariamente. Y, cercano, no implica algo de la vida cotidiana, sino que sea significativo. Evidentemente, existe un problema cuando introducimos capas de abstracción innecesarias.  
Si hay una escuela de didáctica centrada en el contexto, esa es la escuela holandesa, conocida como «Enseñanza Matemática Realista» (Realistic Mathematics Education).   
¿¿¿ R E A L I S T A ???  
Pues sí, porque en primer lugar, realista no implica real, sino que pueda ser imaginado.  
Toda esta cita, de una de las autoras más representativas de la EMR nos viene que ni pintada, pero subrayo la conclusión. Van Den Heuvel-Panhuizen, M. (2005). The role of contexts in assessment problems in mathematics. For the learning of mathematics, 25(2), 2-23.  
«El mundo de la fantasía o de los cuentos de hadas e incluso el mundo formal de las matemáticas \[¡anda!, como los cuadriláteros] pueden proporcionar contextos adecuados para un problema, siempre que sean reales en las mentes de los alumnos y puedan experimentarlos como tales.»  
Por si lo queréis leer completo.
[The role of contexts in assessment problems in mathematics](https://www.researchgate.net/publication/46674778_The_role_of_contexts_in_assessment_problems_in_mathematics)  
Bueno, que esa es otra, también os podéis pasar por el imprescindible @nrichmaths a ver cuántas tareas/problemas/juegos tienen contexto «real». Y luego, a ver quién tiene narices de decir que no son situaciones de aprendizaje válidas.  
Por cierto, en la web de @nrichmaths tenemos los retos de verano. Cada día, del 18 de julio al 2 de septiembre. [Aquí enlace para los retos de Primaria.](https://nrich.maths.org/primary-summer2022)  
Y [aquí los retos de verano de @nrichmaths para Secundaria.](https://nrich.maths.org/secondary-summer2022)  

[twitter PsicEduM/status/1551883836753158155](https://twitter.com/PsicEduM/status/1551883836753158155)  
En primer lugar, esta propuesta forma parte de la pedagogía diferenciada (PD), que no se refiere a un método ni dispositivo concreto, sino a personalizar los itinerarios de formación trabajando en grupos y dando apoyo a las interacciones sociocognitivas.  
La PD intenta desarrollar proyectos comunes que impliquen a todos los alumnos proponiendo situaciones cognitivamente movilizadoras, de cooperación y ayuda. En este punto se diferencia de la enseñanza individualizada porque considera que debemos permitir a través del trabajo y actividades propuestas, que todos accedan a una cultura escolar común, que todos participen y que todos progresen dentro de sus posibilidades.  
Y cómo logramos implementar una PD?  
En primer lugar, debemos tener claro que la PD implica crear secuencias didácticas que alternen tiempos de trabajo individual y colectivo (aquí tb podemos incluir la clase expositiva para clarificar conceptos, interrogar o establecer conexiones entre apr. previos y nuevos).  
En segundo lugar, es imprescindible tener en cuenta el concepto de objetivo- obstáculo: es el obstáculo lo que se convierte en el objetivo de trabajo, por lo tanto el trabajo a corto plazo se centra en detectar lo que dificulta el aprendizaje del alumno, y a partir de aquí agrupar a los alumnos que se encuentren con obstáculos similares (no han asimilado un determinado concepto o procedimiento) para proponerles un trabajo que les permita superar dichas dificultades.  
El trabajo por situaciones-problemas permite, en cierta medida, elegir y crear obstáculos. En lugar de detectarlos a través del fracaso, esta forma de enseñar toma la iniciativa y enfoca las situaciones de aprendizaje sobre los obstáculos cognitivos más probables que pueden encontrar nuestros alumnos antes, después o durante la secuencia (aquí resulta imprescindible la evaluación formativa).  
En otras palabras, debemos detectar aquello que se le resiste o se le puede resistir al alumnado (obstáculos y barreras) para proponer posteriormente, tareas individuales y/o grupales que les permita superar sus dificultades.  
Una diferencia con la propuesta más tradicional, es cambiar el orden de los factores siempre que sea posible (en vez de ofrecer unos contenidos y después actividades para movilizarlos, proponemos un problema que les obligue a ir en busca de los contenidos que les permita resolverlo). La función principal de una situación-problema es obligar a que los estudiantes identifiquen unos conocimientos o habilidades que les hacen falta para solucionar las situaciones o problemas propuestos, y se apropien de ellos para volver al problema con los medios para resolverlo. Este funcionamiento permite q el estudiante conecte directamente los conocimientos y habilidades con unos problemas y, así, los considere de entrada como recursos para plantear y resolverlo  
Finalmente comparto las características que debe tener una situación- problema según Astolfi (1993), uno de los precursores del término:  
![](https://pbs.twimg.com/media/FYloe6GWAAEJfKQ?format=jpg)  
![](https://pbs.twimg.com/media/FYloe6IWYAA89Ht?format=jpg)  
![](https://pbs.twimg.com/media/FYloe7UXoAIT6Cb?format=jpg)  
Para profundizar:  
La organización del trabajo, clave en toda pedagogía diferenciada. Philippe Perrenoud.  
Recuperar la pedagogía. Philippe Meirieu.  
🔚


[twitter signoresalieri/status/1569705807343755265](https://twitter.com/signoresalieri/status/1569705807343755265)  
El sentir de que las situaciones de aprendizaje son lo mismo que las unidades didácticas no hay quien lo pare, ¿eh? La culpa es una normativa tan mal redactada (que no lo deja claro), pero también del cómodo mantra de "esto es lo de siempre, solo han cambiado el nombre".  
[twitter MarcosSanzL/status/1569717229142384646](https://twitter.com/MarcosSanzL/status/1569717229142384646)  
Hasta los más convencidos (no de la ley, sino de la necesidad de cumplirla y de no hacer "lo de siempre") estamos encontrando dificultades a la hora de aterrizar una programación con criterios y saberes poco concretos, unas veces muy desconectados, otras muy solapados... uf  



### Agosto 2022

[twitter nsanrod/status/1562091010049675265](https://twitter.com/nsanrod/status/1562091010049675265)  
Revisando un poco la nueva normativa #LOMLOE y las nuevas Instrucciones de Andalucía, llego a las siguientes conclusiones sobre el tema de las situaciones de aprendizaje (SA) En el Anexo IV de las Instrucciones, viene un modelo/esquema de SA, que pongo más abajo. Por cierto,...  
...¿es este esquema de uso obligatorio? ¿Se puede modificar? ¿Se puede usar otro modelo @kikeguerrerot? Creo que este modelo de SA es mejorable, pero algo es algo. Sigo. En el apartado de "Identificación", hay un subapartado a completar llamado "Título o tarea" y el apartado de "Secuenciación didáctica" viene para completar las actividades y ejercicios que componen dicha SA. Para mí, aquí está la clave para entender qué es realmente una SA y si es lo mismo que Unidad Didáctica (UD) Todo dependerá del nº de tareas de la UD, ya que  SA es sinónimo de "Tarea", entendida como la  acción/es orientadas a la resolución de una situación problema, establecido dentro de  un contexto definido, en la que el alumno/a combinará  todos los saberes disponibles para elaborar un producto final relevante"  
Para llegar a elaborar ese producto final hay que ir dando pasos más cortos, para lo que tenemos las "Actividades" y éstas, a su vez, se irán descomponiendo en "Ejercicios". Por tanto, si una UD tiene una única tarea/SA a completar, la propia UD será la SA y ambas coincidirán.  
Sin embargo, también puede ocurrir que dependiendo de cómo me plantee el trabajo con el alumnado, en una misma UD haya varias tareas a realizar, con lo que cada una de ellas será una SA diferente y la UD estará compuesta por varias SA. Ejemplos:  
Ejemplo 1 (UD con una única SA): la tarea/SA sería la elaboración de un recetario con recetas saludables, para el trabajo de los hábitos de vida saludables y la función de nutrición. Ahí incluiríamos diferentes actividades y para cada actividad, diferentes ejercicios.  
Ejemplo 2 (UD con varias SA):  
Tarea/SA 1: elaboración de un recetario con recetas saludables, para el trabajo de los hábitos de vida saludables y la función de nutrición. Ahí incluiríamos diferentes actividades y para cada actividad, diferentes ejercicios.  
Tarea/SA 2: elaboración de un repositorio de vídeos en los que los alumnos/as expliquen diferentes tipos de actividades y ejercicios para la mejora de las capacidades física básicas, trabajando así la función de relación. Incluir también actividades y ejercicios.  
En el ejemplo 2, la UD estaría compuesta por cada tarea/SA.  
Otra cuestión sería la viabilidad de plantear las UD y/o tareas/SA de esta manera, el tiempo disponible para prepararlas, la formación que haría falta para ello, la cantidad de áreas que impartimos, sobre todo en primaria, etc., por todo el trabajo, organización, coordinación, etc. que ello supone.   
Este es mi planteamiento. Si alguien tiene otras ideas, encantado de escucharlas.   

[twitter luistimonbenite/status/1590391669912145920](https://twitter.com/luistimonbenite/status/1590391669912145920)  
En esta tabla de doble entrada se puede apreciar mejor la diferencia entre unidades, unidades didácticas y situaciones de aprendizaje. Me he basado en la normativa y en COMBAS Y PICBA. Como se puede apreciar las diferencias entre UDIS y SA son mínimas  
![](https://pbs.twimg.com/media/FhI0DboWYAAagGP?format=jpg)  


29 enero 2023  
[twitter pbeltranp/status/1619631617492013061](https://twitter.com/pbeltranp/status/1619631617492013061)  
Menudo jaleo todavía con lo de las situaciones de aprendizaje. Pienso que el origen puede estar en interpretaciones de corte generalista sin sensibilidad por la didáctica específica de cada materia.  
Los RD estatales las definen así (cito art 2 f de RD 217/2022):  
«Situaciones y actividades que implican el despliegue por parte del alumnado de actuaciones asociadas a competencias clave y competencias específicas y que contribuyen a la adquisición y desarrollo de las mismas.»  
Y punto. A lo largo del articulado se sigue resaltando esto. Que lo que realmente define a una SA es que moviliza competencias específicas y clave. Interesante lo del siguiente tuit, que ya menciona el Anexo III en el que igual estás pensando.  
Art 12-2. Para la adquisición y desarrollo, tanto de las c. clave como de las c. específicas, el equipo docente planificará SA en los términos que dispongan las administraciones educativas. Para facilitar al profesorado [...] se enuncian en el A. III orientaciones para su diseño.  
Que lo del Anexo III son orientaciones. Generales. De tareas que pueden ser consideradas como SA. Lo de que la planificación será en los términos que dispongan las administraciones educativas, lo dejamos, que ya sabemos que hay disparidad de interpretaciones.  
Yo leo lo del Anexo III y, a pesar de ser generales, pues tampoco me hace mucho daño. Tampoco veo que de ahí se desprenda que una SA tenga que ser obligatoriamente un proyecto. Sobre lo del contexto ya escribí.  
...  
Entiendo, como decía @SergioMJGR  el otro día, que haya materias en las que una SA sea bastante asimilable a un proyecto. Tecnología, por ejemplo, donde se plantea un producto final claro, etc. En Matemáticas también se pueden plantear proyectos, claro, no digo que no.  
...  
Cerrando ya. El foco debería estar en el diseño/selección/adaptación de tareas y en su gestión en el aula para que sean coherentes con las competencias específicas. Todo lo demás viene bastante solo, aunque no deja de ser un reto, claro.  


Respuestas interesantes aquí:  

[twitter profesmadeinuk/status/1643318922215563266](https://twitter.com/profesmadeinuk/status/1643318922215563266)  
Resulta muy ilustrativo este follón sobre el concepto que se supone el pilar de la programación y la didáctica del día a día en el aula. Seguro que hay varios responsables del follón, pero el último y máximo es la administración: quien lo propone sin desarrollarlo y explicarlo.  

[twitter A_IvanRodriguez/status/1643265754458882048](https://twitter.com/A_IvanRodriguez/status/1643265754458882048)  
Si tenéis un momento, me gustaría saber qué diferencias creéis que hay entre una U.D. y una situación de aprendizaje. @nolo14  @pbeltranp  @MarianaMorale19  @profesmadeinuk  @EfectoMcguffin  @bpalop  @tolobv  @OElwes  @dchicapardo  @iore_  @maganado  @maestroll11 @larotesmeyer  ¡Gracias!

[twitter pbeltranp/status/1643275667230826500](https://twitter.com/pbeltranp/status/1643275667230826500)  
Es como comparar el tocino con la velocidad. La definición prescriptiva de SA es esta q menciono en el tuit. Lo del anexo III son orientaciones GENERALISTAS de lo que PUEDE ser una SA (pq un proyecto puede q no movilice las comp. específicas de mates) 1/n

[twitter pbeltranp/status/1628495709082198018](https://twitter.com/pbeltranp/status/1628495709082198018)  
Exigir que las situaciones de aprendizaje deben tener un producto final es bastante incompatible con competencias específicas que básicamente describen PROCESOS.  
[twitter 	AlfonsoCortesA/status/1628517956878475264](https://twitter.com/AlfonsoCortesA/status/1628517956878475264)  
Creo que las tareas competenciales prácticas con su producto final (emitir un programa de radio) desde los criterios de evaluación es un buen camino para el trabajo por competencias, y a eso se refieren las situaciones de aprendizaje.
[twitter pbeltranp/status/1628519774761230336](https://twitter.com/pbeltranp/status/1628519774761230336)  
No, lo siento pero las situaciones de aprendizaje se refieren de forma prescriptiva a esto que adjunto (tomado del art. 2 del RD). Y no es poco. 1/n

[twitter pbeltranp/status/1628519774761230336](https://twitter.com/pbeltranp/status/1628519774761230336)  
En educación infantil que es mi campo, la unidad didáctica es una herramienta de programación en la que se establecen unos contenidos que llevan a alcanzar unos objetivos.  
La SA, parte de una vivencia, experiencia o realidad, le da más protagonismo a los alumnos/as.  

Se cita artículo 28 diciembre 2022
[Alejandro Tiana, coautor de la reforma educativa: “Estamos intentando cambiar la cultura escolar” - elpais](https://elpais.com/educacion/2022-12-28/alejandro-tiana-coautor-del-nuevo-sistema-de-aprendizaje-estamos-intentando-cambiar-la-cultura-escolar.html)  

> Introducimos ‘situaciones de aprendizaje’ porque en la escuela estamos muy acostumbrados a utilizar términos que están muy cargados de contenido, incluso inconsciente, como tareas, ejercicios, prácticas... Y esos términos a veces llevan a hacer más de lo mismo. **Utilizando situación de aprendizaje no pretendemos ninguna innovación rara**, sino decirle al docente: diseñe usted modelos aplicables al aula que favorezcan que los alumnos aprendan. Y eso lo puede hacer de maneras muy diferentes, no va en contra de hacer ejercicios, de hacer prácticas, ni de nada. Lo que decimos es: ponga usted el foco en eso. Yo diría, aunque no es exactamente lo que me ha preguntado…  

[twitter hruizmartin/status/1663504528069500929](https://twitter.com/hruizmartin/status/1663504528069500929)  
Es un grave error interpretar las “situaciones de aprendizaje” como un único contexto alrededor del cual deba girar todo el aprendizaje de unos conceptos y/o procedimientos. Hacer esto no ayuda a evitar lo que se conoce como «sobrecontextualización». Abro mini-hilo.  
La sobrecontextualización se da cuando las personas asociamos lo que aprendemos únicamente al contexto en que lo aprendemos, y somos incapaces de apreciar su relación o relevancia para resolver problemas en otros contextos.  
De hecho, este fenómeno es muy habitual, pues nuestro cerebro tiende a basarse en lo concreto. De ahí que muchas veces pidamos ejemplos para entender algo. Sin embargo, la comprensión realmente incrementa cuando nos dan múltiples ejemplos, no solo uno.  
En los hilos donde traté sobre la transferencia del aprendizaje subrayé que lograr aprendizajes transferibles a nuevas situaciones no es fácil; pero que una de las mejores maneras de contribuir a ello es usar múltiples contextos para abordar el mismo objeto de aprendizaje.  
Así que si deseamos que los alumnos sean capaces de abstraer lo aprendido para transferirlo a nuevas situaciones (algo fundamental cuando hablamos de competencias), entonces es necesario trabajar los mismos conceptos y procedimientos en múltiples “situaciones de aprendizaje”.  
Escribo este hilo tras la preocupación que me ha generado presenciar una formación sobre “situaciones de aprendizaje” en la que se ha asegurado que una “situación de aprendizaje” se caracteriza por ser un contexto que guía «todo un tema» o «varios temas».  
En los currículos que he consultado, afortunadamente, no se establece esa limitación. Así que entiendo que se trata de algún malentendido. Las “situaciones de aprendizaje” pueden guiar una sola clase o una sola actividad.  
Claro que podemos plantear también situaciones de aprendizaje más extensas, pero no debemos olvidar el riesgo de la sobrecontextualización del aprendizaje y procurar combinarlas con otras situaciones variadas y quizás más sencillas.  
Y sí, es interesante que las “situaciones de aprendizaje” sugieran plantear contextos significativos y empezar la secuencia por la pregunta que se quiere responder, y no por la respuesta. Pero de ahí a interpretar que deben ser el único contexto a emplear es desafortunado.  
En definitiva, lo más recomendable resulta trabajar con múltiples situaciones, que terminen abordando y movilizando unas mismas ideas o procedimientos. FIN.  
Aquí dejo los hilos sobre una cuestión tan importante y ampliamente investigada por las ciencias cognitivas como es la transferencia del aprendizaje, con sus referencias:  

[twitter pablofcayqca/status/1662916355052429313](https://twitter.com/pablofcayqca/status/1662916355052429313)  
Una de las situaciones más cómicas que ha traído consigo la LOMLOE ha sido la conceptualización de las SA y su diferenciación respecto de las UD. Incluso hay quien ha rebuscado en el "Génesis" para rescatar las UP. Pero, ¿qué son las SA, las UD y las UP?  
Una UP es una unidad de programación, es decir, una fracción de la planificación de la actuación docente.  
Una UD es una unidad didáctica, es decir, una unidad de programación en la que se concretan los elementos que intervienen en los procesos E/A durante un cierto periodo   
Esta definición tiene más años que el hilo negro (MEC, 1992), pero sigue totalmente vigente (teniendo en cuenta los nuevos elementos curriculares, eso sí): un periodo de tiempo en el que planificamos una serie de actividades para que los alumnos aprendan algo.  
Y hay muchas formas de presentar una unidad (organizar los aprendizajes). De hecho, en ese mismo documento (MEC, 1992) ya se nos decía esto  
Por tanto, en una unidad didáctica se pueden organizar los aprendizajes de manera que partan de un centro de interés, mantengan un determinado bloque temático y se dirijan hacia la elaboración de un producto (proyecto) o la resolución de un problema.  
Y, ¿qué es una unidad didáctica en la que los aprendizajes se han organizado de manera que partan de un centro de interés, mantengan un determinado bloque temático y se dirijan hacia la elaboración de un producto (proyecto) o la resolución de un problema?  
¡Exacto! Una situación de aprendizaje (SA).  
Así, cuando planteamos una UD desde una determinada temática que conecta con un contexto cercano y significativo para el alumnado y que le propone hacer o resolver algo con eso que aprende, estamos proponiendo una SA.  
Por ejemplo, cuando estudiamos campo gravitatorio, podríamos plantear al alumnado que realice los cálculos necesarios para colocar al James Webb en órbita (bueno, una aproximación aproximadamente aproximada): ¿Podrías poner al James Webb en órbita?  
  
[twitter eboixader/status/1687525394855972887](https://twitter.com/eboixader/status/1687525394855972887)  
¡Programación de SA en 5 min! Con 6 prompts \[en ChatGPT]. Adaptada a la legislación de la C. Valenciana. Igual que la que me costó el curso pasado más de 3 horas.  [Prompts para realizar la programación de una SA](https://docs.google.com/document/d/1IuZ04mJKfvt4C6M2Fec-n3mIKyoT2q24F73BawdJmBY/edit?usp=sharing) Mejorable. Tarea pendiente para mis compañer@s de @Fund_Flors
 y #claustrovirtual #Tutiempoparatualumnado  
 
[twitter starpy/status/1700187070675787957](https://twitter.com/starpy/status/1700187070675787957)  
Son exactamente "lo mismo" las Unidades didácticas y las Situaciones de Aprendizaje? 🤔  
No, son primas hermanas pero no hermanas hermanas gemelas.  
![](https://pbs.twimg.com/media/F5hHIFFXgAAl38K?format=jpg)  


[twitter currofisico/status/1788859206889615513](https://x.com/currofisico/status/1788859206889615513)  
Lo que veo en el área de física y química, y supongo que en las demás áreas lo mismo, es una deriva hacia una "tecnificación de la evaluación", ...rúbricas absurdas, idoceistas, trabajo cooperativo sin rumbo, uso de las tic sin un fin claro...¿No sabemos hacer situaciones de aprendizaje? Es como si nunca hubiéramos hecho nada y estuviéramos descubriendo la pólvora por primera vez. Lo llames situaciones de aprendizaje, o como te de la gana, lo que subyace a la dificultad del diseño es que no tenemos claro qué es lo que queremos que el alumnado aprenda cegados por el ruido normativo. Todos/as deseando que vengan las editoriales a evangelizarnos comercialmente con sus fantasticas propuestas. Pues hay cosas hechas hace años que son joyas, delicias de leer, estudiar e implementar...pero hay q echarle un rato.  
Se incluyen capturas de dos libros y de bibliografía; los cito en [Recursos situaciones aprendizaje](/home/recursos/situaciones-de-aprendizaje) 
- Ciencias de la naturaleza, Cuarto curso de Educación Secundaria Obligatoria, Ministerio de Educación y Ciencia, Edelvives  
- [Cinemática y dinámica ;Antonio Carmona García-Galán ... ](https://datos.bne.es/edicion/bimo0000134194.html)  ISBN 	84-87215-41-6   
Y dos capturas de bibliografía  
Un ejemplo [La "metodología de la superficialidad" y el aprendizaje de las ciencias March 1985 Enseñanza de las Ciencias Revista de investigación y experiencias didácticas 3(2):113-120](https://www.researchgate.net/publication/303471418_La_metodologia_de_la_superficialidad_y_el_aprendizaje_de_las_ciencias)  

## Documentación sobre diseño de situaciones de aprendizaje

[Diseño de situaciones de aprendizaje en física y química conforme a la LOMLOE. Antonio García-Carmona. Departamento de Didáctica de las Ciencias Experimentales y Sociales. Universidad de Sevilla. Ápice. Revista de Educación Científica, 7(1), 2023. Sección. Innovación educativa. DOI: https://doi.org/10.17979/arec.2023.7.1.9436. ISSN: 2531-016X](https://revistas.udc.es/index.php/apice/article/view/arec.2023.7.1.9436/g9436_pdf)  

Aparecen en normativa documentos (ver Andalucía)  

Recopilación de normativa sobre situaciones de aprendizaje realizada aquí por Kike Guerrero  
[Situaciones de aprendizaje - el site de Kike (bis)](https://sites.google.com/view/elsitedekikebis/inicio/situaciones-de-aprendizaje)  
> Espacio para encontrar las distintas formas en que las comunidades autónomas han normativizado este apartado de la LOMLOE.
Algunos desarrollos han considerado introducir situaciones en cada materia. Esto no se recoge en esta página, ya que sería demasiado extenso. Se reproducen los documentos/anexos que tienen un carácter general.  

[twitter oscarm1976/status/1563245696626540545](https://twitter.com/oscarm1976/status/1563245696626540545)  
Posible formato de Situación de aprendizaje inspirado en las instrucciones 12/2022, esta en formato editable para quien lo quiera usar y personalizarlo, opiniones y propuesta de mejora @kikeguerrerot @asanchezb74 @TeacherMigue  
[FORMATO SITUACIÓN APRENDIZAJE PRIMARIA](https://docs.google.com/document/d/1sQjT6NzLIzV0ct3yaXhUar5ErmGfPo2tC9KyNBIgkkw/edit)  


[Plantillas y documentos de apoyo para la programación docente ante la implantación de la Ley educativa LOMLOE en el curso 2022-23 - gobiernodecanarias.org](https://www.gobiernodecanarias.org/educacion/web/servicios/recursos-pedagogicos/programacion-docente-lomloe/index.html)  

> Ante la implantación de la ley educativa LOMLOE en el curso 2022-2023, con lo que ello conlleva de programar con el nuevo currículo para los cursos impares de Educación Primaria (1.º, 3.º, 5.º), Educación Secundaria (1.º y 3.º) y Bachillerato (1.º), se han elaborado unos documentos de apoyo para ayudar al profesorado en esa tarea. En ellos, se dan indicaciones sobre cómo consignar las competencias clave, que ahora se materializan en descriptores operativos. Además, se incluyen los nuevos elementos como son las competencias específicas, que indican los desempeños que el alumnado debe desplegar en cada etapa educativa y que se concretan y secuencian por niveles en los criterios de evaluación; los saberes básicos, que constituyen los contenidos propios del área, materia o ámbito. Por ello, se publican unas plantillas editables (odt) para diseñar situaciones de aprendizaje y programaciones didácticas para el presente curso escolar. Además, dichos recursos cuentan con un documento de orientaciones para que el profesorado sepa cómo rellenarlas, si tuviera alguna duda.  

* [ Orientaciones para el diseño de la programación didáctica LOMLOE. ](https://www.gobiernodecanarias.org/cmsweb/export/sites/educacion/web/servicios/_galerias/descargas/programacion-docente-lomloe/sept_plantilla-diseno-pd-lomloe-con-orientaciones.odt)  

* [ Plantilla de diseño de la programación didáctica LOMLOE. ](https://www.gobiernodecanarias.org/cmsweb/export/sites/educacion/web/servicios/_galerias/descargas/programacion-docente-lomloe/plantilla-disenno-pd-lomloe-vacia.odt)  

* [ Orientaciones para el diseño de una situación de aprendizaje LOMLOE. ](https://www.gobiernodecanarias.org/cmsweb/export/sites/educacion/web/servicios/_galerias/descargas/programacion-docente-lomloe/sept-plantilla-diseno-sa-lomloe-con-orientaciones.odt)  

* [ Plantilla de diseño de una situación de aprendizaje LOMLOE. ](https://www.gobiernodecanarias.org/cmsweb/export/sites/educacion/web/servicios/_galerias/descargas/programacion-docente-lomloe/plantilla-disenno-sa-lomloe-vacia.odt)  

[KIT 3 : Situaciones de aprendizaje - auladelfuturo.intef](https://auladelfuturo.intef.es/kit-aula-del-futuro/kit-de-herramientas-3/)  
>    3.1 Guía para escribir situaciones de aprendizaje  
    3.2 Competencias / Capacidades  
    3.3 Actividad para escribir o adaptar la narrativa de la situación de aprendizaje  
    
> Recordatorio: antes de trabajar en una situación de aprendizaje hay que haber realizado:  
    Identificar las tendencias trabajando con el Equipo de Innovación (KIT 1)  
    Reflexionar sobre la identidad del centro y su nivel de innovación (KIT 2)  


[KIT PARA EL DISEÑO DE SITUACIONES DE APRENDIZAJE - El loco de la mochila. Blog de Juan Manuel López Esparrell](https://blogsaverroes.juntadeandalucia.es/ellocodelamochila/lomloe/kit-para-el-diseno-de-situaciones-de-aprendizaje/)  
>Si bien la elaboración de situaciones de aprendizaje requiere su tiempo (como cualquier otra planificación), no es una tarea compleja en exceso. Además, **al exigir estas una fuerte contextualización y adecuación a las características concretas del grupo, no resulta adecuado copiarlas (de editoriales o de Internet), sino que debemos personalizarlas y adaptarlas a nuestro contexto educativo concreto**.  

[twitter IgnacioSnchezA4/status/1572591711104401409](https://twitter.com/IgnacioSnchezA4/status/1572591711104401409)  
¿Eres profesor y te lías con la LOMLOE?  
¿Te gustaría ver un ejemplo real de cómo programar y trabajar un curso completo a partir de situaciones de aprendizaje y evaluar criterios de forma eficaz?  
¿No tienes nada mejor que hacer hoy?  
Pues este es tu hilo, my friend  
...  
[twitter IgnacioSnchezA4/status/1572591721254453251](https://twitter.com/IgnacioSnchezA4/status/1572591721254453251)  
>... Y las «situaciones de aprendizaje» vienen a ser una especie de unidades didácticas tematizadas y convertidas en escenarios fuertemente relacionados con la vida real del alumnado. ...  

[twitter asturias4steam/status/1628722471439417345](https://twitter.com/asturias4steam/status/1628722471439417345)  
Lo de asumir que "situación de aprendizaje" es igual a "proyecto" es algo muy extendido. Esto de @jdomenechca arroja luz para desfacer el entuerto.  
[Situaciones de aprendizaje. Ideas para el despliegue curricular de las Ciencias - jordidomenechportfolio](https://jordidomenechportfolio.wordpress.com/2022/09/16/situaciones-de-aprendizaje-ideas-para-el-despliegue-curricular-de-las-ciencias/)  

[Lista de control de situaciones de aprendizaje - cedec.intef.es](https://cedec.intef.es/rubrica/lista-de-control-de-situaciones-de-aprendizaje/)  


## Ejemplos situaciones aprendizaje en el currículo 

[Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-2.PDF)  
**Educación física**  
Página 66  
> Como ejemplo de actividad, tomando como referencia el citado modelo de enseñanza comprensiva del deporte para su aplicación en el nivel curricular correspondiente a la unidad didáctica, se propone la práctica del juego modificado, incluyendo las variaciones que se consideren oportunas para averiguar el grado de conocimiento y destreza del alumnado (adaptación del espacio, de la meta, del material, del número de jugadores, etc.). En esta actividad deberá atenderse a la conciencia táctica para la apreciación del juego con el propósito de que el alumnado llegue a comprender los aspectos reglamentarios y sus implicaciones, la distribución de espacios y sus posibilidades de interacción, etc.  

**Física y Química**  
Página 104  
> A modo orientativo de cómo puede plantearse una actividad en el aula se presenta el siguiente ejemplo: con el fin de analizar la eficiencia energética en el entorno doméstico y escolar, el alumnado puede realizar, siguiendo los pasos propios del método científico, un estudio con propuestas viables para la mejora de la eficiencia energética en ambos entornos.  
> Esta actividad se podría desarrollar dentro del bloque «La energía» en cualquiera de los cursos de Educación Secundaria Obligatoria en los que se da la materia, contribuyendo a desarrollar las competencias específicas 5 y 6 de la misma.  


**Geografía e Historia**  
Página 124   
> A modo orientativo de cómo pueden ser planteadas las situaciones de aprendizaje anteriormente mencionadas, se presenta el siguiente ejemplo:
> La actividad que a continuación se describe tiene como objetivo crear una sencilla representación dramática que traslade al espectador, a través de un viaje temporal, a uno de los momentos más determinantes de la historia de la humanidad: el inicio de la globalización a raíz de la puesta en marcha del Galeón de Manila. El grupo-clase se dividirá en cuatro equipos y elegirá una de las siguientes áreas: documentación histórica, dirección artística, guion e interpretación.  
> Los cuatro equipos tendrán que colaborar entre ellos, pues documentación histórica, previa bibliografía y webgrafía facilitada por el docente, deberá ser el centinela del cumplimiento de aquellos aspectos pretéritos que se plasmen en la creación, ya sean artísticos o referentes al relato; dirección artística, asesorado por el equipo de documentación, tendrá que trabajar en el diseño de atuendos, objetos, escenarios y en la selección musical para desarrollar la obra; los miembros del equipo guionista deberán elaborar la narración y los diálogos, asesorados por documentación; dirección y los artistas que componen el equipo de interpretación habrán de comprometerse con las indicaciones del resto de equipos y materializar el guion. El contexto se desarrollará en el puerto de Manila a finales del siglo XVI y deberán aparecer personajes procedentes de distintos grupos sociales con diferentes ocupaciones (navegantes, comerciantes, burócratas, artesanos, soldados, religiosos, esclavos, etc.), de diversas procedencias (indígenas, chinos, malayos, hispanos) y diferentes tradiciones espirituales y creencias (politeístas, musulmanes, cristianos, taoístas), que plasmen la multiculturalidad y la riqueza de la
interculturalidad. Tras la representación, cada miembro del grupo-clase deberá exponer por escrito lo que ha supuesto a nivel social y emocional esta experiencia y lo que ha aprendido a través de ella. Esta actividad se podrá desarrollar en 2º de ESO tras haber estudiado los contenidos referentes a la monarquía hispánica y la colonización de América (bloque C). Asimismo, sería válida para 3º de ESO, retrasando al siglo XVIII el contexto histórico de la pieza teatral, y tras haber impartido el legado de España en América (bloque D). Además, también mantiene una estrecha relación con los contenidos dedicados a la globalización en 4º de ESO (bloque C). Por último, contribuye a desarrollar las competencias específicas 1, 2 y 6 de este currículo.  

[twitter FiQuiPedia/status/1531336534879789056](https://twitter.com/FiQuiPedia/status/1531336534879789056)  
Buscando referencias a "situaciones de aprendizaje" en  proyecto de decreto de currículo ESO @educacmadrid he visto esto de Geografía e Historia  
Si me parecía excesivo el detalle de algunos estándares, ¿tiene sentido que currículo baje a este detalle?  

[twitter NikoDimitratos/status/1563877902462189570](https://twitter.com/NikoDimitratos/status/1563877902462189570)  
Estimado claustro virtual: abro hilo sobre la LOMLOE.  
Podéis tirarme piedras si queréis.  
Actividad propuesta para la ESO.   
Sería para 2º de ESO, alumnos con 13 /14 años.  
El Galeón de Manila es "uno de los sucesos más importantes de la historia de la humanidad" 😳  
Hilo  

**Latín**  
Página 145  
> De esta forma, a modo de ejemplo, se podría proponer la siguiente situación: a partir de las áreas de interés del alumnado, se puede plantear un trabajo de investigación interdisciplinar sobre el origen etimológico de los tecnicismos más frecuentes en el vocabulario de las distintas ciencias (astronomía, medicina, física, química, matemáticas, historia, arte, etc.). Por grupos, los alumnos han de elaborar, preferentemente usando alguna herramienta digital, un «diccionario», que traduzca esas palabras al español y ofrecerlo como material de apoyo para sus compañeros de cursos inferiores. Este trabajo puede continuarse a través del contraste entre esas mismas palabras y sus equivalentes en otra lengua del repertorio de los alumnos, incluso en colaboración internacional con alumnado de otros centros de Europa, trabajando la cultura latina como punto de encuentro de las distintas realidades que conforman la actual identidad europea. Esta situación se puede relacionar con los bloques de contenido A, B y D, y, de forma destacada, con el B («Latín y plurilingüismo»), contribuyendo a desarrollar las competencias específicas 1, 2 y 5 de la materia.  

**Lengua extranjera**  
Página 184  
> A modo orientativo de cómo pueden ser planteadas diferentes actividades, se plantea la posibilidad de planificar una visita a una ciudad situada en uno de los países donde se hable la lengua objeto de aprendizaje. Los alumnos trabajarán en pequeños equipos de forma cooperativa y tendrán que organizar las tareas para conseguir el objetivo planteado para, posteriormente, exponer la visita organizada mediante medios digitales, informando al resto de equipos sobre el destino seleccionado, el marco temporal, el transporte utilizado, el alojamiento elegido y las actividades realizadas durante la visita. Los alumnos dispondrán para la realización de la tarea propuesta de los manuales utilizados habitualmente en clase, así como las herramientas digitales que les permitan acceder a materiales e informaciones auténticos.  
Esta situación se plantea para segundo curso de Educación Secundaria Obligatoria y relaciona los contenidos de este curso (describir lugares, expresar parcialmente el gusto o el interés, narrar acontecimientos pasados, presentes y futuros, expresar la opinión, etc.) con el desarrollo de las competencias específicas 1, 2, 4 y 6.  

**Música**  
Página 251  
> A fin de propiciar el aprendizaje competencial, desde un enfoque transversal e interdisciplinar,
se desarrollará una metodología didáctica que reconozca al alumnado como centro de su propio
aprendizaje. Las actividades propuestas fomentarán la autoestima, la autonomía, la reflexión
crítica y la responsabilidad. Asimismo, permitirán relacionar o utilizar los nuevos conocimientos y
habilidades aprendidos en la vida real. Se propiciará el uso de la tecnología, tan presente en
nuestra sociedad actual, como fuente de conocimiento y comunicación, guiado siempre por un
criterio que permita seleccionar el mejor material disponible según los objetivos educativos
propuestos. De esta forma, se pueden proponer actividades que favorezcan los distintos tipos de
agrupamientos, permitiendo que el alumnado asuma responsabilidades personales y actúe de
forma cooperativa. Por ejemplo, con el fin de trabajar los contenidos del bloque C (Músicas
populares urbanas desde los años 50 hasta la actualidad en España y en otros países), el
alumnado del grupo-clase se dividirá en seis equipos. Cada uno deberá elegir un estilo musical:
jazz, rock, pop, flamenco y folclore (se dará a elegir entre celta, country o cualquiera de los
desarrollados en el territorio español) e investigar los orígenes sociales de dicho estilo y las
influencias que han tenido como referentes sus principales compositores. De entre ellos, se
elegirá una canción representativa y el equipo tratará de versionarla en público a través de la
percusión, la voz o de ambas (acompañada, si se quiere, de una coreografía) como colofón a la
exposición pública apoyada en medios audiovisuales de dicha investigación. Esta actividad está
relacionada directamente con las competencias específicas 2, 3, 4 y 5.  


**Segunda lengua extranjera**  
Página 259  
> A modo orientativo de cómo pueden ser diseñadas diferentes actividades, se plantea la posibilidad de desarrollar en el aula una presentación por parte de los alumnos de una tradición y/o fiesta propia de los países en los que se habla la lengua extranjera, como parte del aprendizaje de esa lengua y su cultura. Los alumnos, una vez divididos en grupos, escogen una fiesta y/o tradición y, utilizando las herramientas digitales que permiten acceder a materiales auténticos, seleccionarán aquellos documentos que mejor reflejen la tradición o fiesta a presentar. A partir de esta selección, se podrá realizar una exposición oral por parte del grupo, adecuada al nivel del Marco Común Europeo de Referencia para las Lenguas en el que esté trabajando. Esta actividad desarrollaría las competencias específicas 1, 2, 3 y 6 de la materia.  

**Tecnología y digitalización**  
Página 271  
>  Una posible actividad para desarrollar en el aula en pequeños grupos de trabajo podría ser el diseño y construcción en equipo de un robot móvil programado para detectar y esquivar obstáculos, haciendo uso de algunos de los contenidos y competencias específicas trabajados en el tercer curso de la Educación Secundaria Obligatoria.  

**Tecnología**  
Página 280  
> Una posible actividad en el aula podría ser diseñar y construir un sistema de riego por goteo controlado desde la nube (Internet de las Cosas), y que sea capaz de monitorizar en tiempo real las condiciones de temperatura y humedad del suelo, haciendo uso de algunos de los contenidos y competencias específicas trabajados en la materia.  


[Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF)  

**Biología, Geología y Ciencias ambientales**  
Página 43  
> A modo de orientación, pueden plantearse actividades que traten los distintos bloques del
currículo de forma simultánea y transversal. Como sugerencia en relación al bloque de «Ecología
y sostenibilidad», podría plantearse que los alumnos abordaran el estudio de la estructura y los
factores bióticos y abióticos que afecten a una charca, o alguna otra zona, como puede ser un
parque cercano, en el que pueda plantearse la realización de un mapa de vegetación de una zona
concreta. Este tipo de actividades deberá ser apoyado por el planteamiento de hipótesis, la
investigación, la búsqueda de información y la realización de informes y exposiciones por parte del
alumnado, contribuyendo a desarrollar las competencias específicas 1, 2, 3 y 4 de la materia.  

**Ciencias generales**  
> Como orientación para la práctica docente se aporta la siguiente actividad: los alumnos, en
grupos, podrían investigar los planetas rocosos del sistema solar, realizar una geología
comparada de los mismos y establecer con criterios científicos posibilidades futuras de viajes
interplanetarios. A partir de unos objetivos claros y precisos, el alumnado deberá movilizar la
mayor cantidad posible de conocimientos de la materia y relacionar los contenidos de los bloques
de «Las fuerzas que rigen el universo y las fuerzas que nos mueven», «El sistema Tierra» y
«Biología para el siglo XXI». Tendrán que investigar en fuentes originales en diversos idiomas,
desarrollar ejemplos de futuros escenarios viables y buscar obras escritas y películas que traten 
sobre esta temática. La actividad planteada perseguiría como resultado final, la elaboración de un
producto en forma de informe escrito, investigación u obra audiovisual, y contribuiría a desarrollas
las competencias específicas 1, 2, 4 y 6 de la materia.  

**Coro y técnica vocal**  
Página 58  
> A modo de ejemplo para la práctica docente, se sugiere la siguiente actividad. En primero de
Bachillerato puede proponerse al alumnado el visionado de una interpretación de la canción
popular africana «Aya Ngena». A partir de esta pieza se propone el análisis de los elementos
estructurales de la música, respondiendo a las siguientes preguntas: ¿Cuáles son características
(melodía, ritmo, etc.)? ¿Cuántas voces se pueden diferenciar? ¿Se acompaña de danza? ¿Qué
significa la letra de la canción? ¿Quiénes participan? ¿Sus voces te parecen naturales o
impostadas? ¿Desempeñan todos los participantes el mismo rol? ¿Qué sentimientos crees que
transmite dicha música? ¿Se parece a algo que hayas escuchado antes o a algún tipo de música
actual? ¿Serías capaz de crear un ostinato rítmico para acompañarla? ¿Y una letra en español?  

**Cultura audiovisual**  
Página 64  
> En cuanto a la orientación de la metodología a emplear, en la materia de Cultura Audiovisual,
una actividad que podría plantearse, a modo de ejemplo en el aula, podría ser un trabajo en
grupos en el que cada grupo tuviera que realizar el proyecto de un relato audiovisual sobre un
mismo acontecimiento del entorno o de la vida del centro (desarrollo de jornadas culturales,
campeonatos deportivos u otras actividades extraescolares de especial relevancia), de tal forma
que cada grupo tenga que utilizar el lenguaje propio de un género o un estilo audiovisual
determinado: anuncio publicitario, noticia de informativo televisivo, documental, ensayo, formato
surrealista, historia dramática siguiendo la vivencia de un protagonista, humorístico, expresivo,
mudo, etc., todo ello respetando el derecho a la imagen y a la intimidad de las personas (Ley
Orgánica 1/1982, de 5 de mayo, de protección civil del derecho al honor, a la intimidad personal y
familiar y a la propia imagen, y normativa de desarrollo). El objetivo sería contrastar los recursos
expresivos que cada género o estilo requiere y los efectos que producen en el espectador dichos
procedimientos.  

**Dibujo artístico**  
Página 71  
> Como ejemplo de actividades para realizar con los alumnos de Dibujo Artístico I, se podría
plantear, en relación con los contenidos transversales y el bloque de la expresión gráfica y sus
recursos elementales, dentro del apartado de la línea, que el alumno realice ejercicios prácticos
de representación de objetos cotidianos siguiendo los contornos en lápiz o tinta, «dibujo ciego de
contornos» sin comprobar su dibujo, para desarrollar su agudeza y sensibilidad visual, pasando
posteriormente a un dibujo en el que sí se permitiría la comprobación de la relación entre la forma
dibujada y el objeto y la modificación del dibujo cuando sea necesario. Esta primera fase se
concluiría realizando dibujos de objetos cotidianos utilizando líneas de relieve. Como fuente de
inspiración se podrán analizar dibujos de Salvador Dalí, Henry Moore, Víctor Vasarely...
En segundo de Bachillerato, en la materia Dibujo Artístico II, y en relación con los contenidos
transversales y el tema del Dibujo y espacio, el alumnado podría realizar dibujos del natural en
espacios urbanos exteriores aplicando los principios elementales de la perspectiva cónica para
definir los puntos de fuga de las diferentes direcciones paralelas, valiéndose en estos dibujos
tanto de la observación detallada, como de su sensibilidad artística y espontaneidad.
Paralelamente se puede realizar un análisis de diferentes obras artísticas o artistas relacionado
con la perspectiva cónica (Piero della Francesca, El Lavatorio de Tintoretto, el Teatro Olímpico en
Vicenza, Canaletto, Piranesi, Giorgio de Chirico, La Città Nuova de Sant’Elia, Un mundo de
Ángeles Santos, Pearblossom Highway de David Hockney ...) Posteriormente y basándose en la
obra «Las ciudades invisibles» de Italo Calvino, los alumnos podrían realizar bocetos
preparatorios y un dibujo final en el que recrearan una perspectiva de una ciudad «invisible»
elegida.  

**Dibujo técnico**  
Página 82  
> En los procesos de enseñanza y aprendizaje debe potenciarse el pensamiento deductivo en
el alumnado y plantear actividades que faciliten asimilar los trazados geométricos. Por ejemplo,
una actividad para facilitar la comprensión del arco capaz como lugar geométrico sería analizar la
construcción de su trazado y ponerla en relación con los ángulos en la circunferencia y la
construcción de triángulos. De esta forma, el alumno podría intuir que si buscamos el lugar
geométrico en el que se observe un ángulo dado desde los extremos de un segmento, ese lugar
geométrico deberá ser un arco de circunferencia. Si queremos trazar ese arco de circunferencia
debemos conocer el centro, que deberá estar en la mediatriz del segmento, asimismo, desde los
extremos del segmento se debe observar un ángulo central de dicha circunferencia que deberá
ser el doble del ángulo inscrito. La construcción de estas deducciones, así como la
descomposición de un problema en problemas más pequeños permitirá la consecución de
aprendizajes significativos.  

**Dibujo ténico aplicado a las artes plásticas y al diseño**  
Página 90  
> De la misma manera que el alumnado de los conservatorios de música aprende a manejar los
instrumentos e interpretar obras sencillas a la vez (o incluso antes) que aprende los fundamentos
y el lenguaje musicales, es posible abordar el aprendizaje de los sistemas de representación del
espacio partiendo tanto del análisis de la realidad (obras de arte, ilustraciones, espacios
arquitectónicos) como de la propia experiencia creadora. Así, en el primer curso de Bachillerato, al
abordar por primera vez el aprendizaje del sistema diédrico, se mostrarían al alumnado ejemplos
concretos de planos de proyectos de diseño de objetos o de edificios sencillos, preferentemente
de autoría reconocida o muy cercanos a su experiencia, donde puedan apreciar plantas, alzados y
perspectivas, así como imágenes de los objetos finalmente construidos. Acto seguido, se les
presentarían objetos geométricos tridimensionales como por ejemplo poliedros sencillos, para que
traten de representar sus vistas en dos dimensiones, dejando intervenir su intuición.
Paralelamente a este trabajo y apoyándose en él, se irían introduciendo los conceptos abstractos
de plano (cada cara de un poliedro está contenida en uno), recta (cada arista de un poliedro está
formada por la intersección de dos planos) y punto (cada vértice de un poliedro resulta de la
intersección de dos o más aristas) cuya representación va a permitir llegar a la confección de
vistas diédricas exactas. A este trabajo preferentemente individual, le sucederían actividades
cooperativas de construcción de un modelo de diedro en tres dimensiones utilizando materiales de
uso sencillo como el cartón-pluma, sobre el que se colocarían y proyectarían diversos tipos de
volúmenes. Las líneas de proyección podrían resolverse mediante alambres o listones finos.
Sobre el modelo se continuarían trabajando otros conceptos y recursos (paralelismo,
perpendicularidad, proyecciones auxiliares, abatimientos...) para los que pueden utilizarse
papeles, cartulinas, acetatos transparentes etc. De esta manera sería fácil para el alumnado
asociar la lógica del sistema de representación con la necesidad de su uso a la hora de idear
objetos futuros. A la vez, se facilitaría la comprensión del espacio tridimensional y los conceptos
abstractos necesarios para la generación de ideas de objetos, sentándose las bases para seguir
trabajando contenidos de mayor complejidad y continuar aprendiendo otros sistemas de
representación.  

**Diseño**  
Página 97  
> Se propone, a modo de ejemplo, como posible actividad, una tarea sobre el contenido La
imagen de marca: el diseño corporativo. La aproximación de este tema a las áreas de interés del
alumnado se realizará en dos direcciones. La primera, a través de una actividad dedicada al
diseño de un escudo nobiliario que le represente. Así se introducirá la historia de las marcas, su
simbolismo y la necesidad humana de identificarse a través de una imagen. Ésta actividad será
individual. Para la segunda se trabajará en pequeños grupos y consistirá en elegir algunas de las
marcas con las que se suelen identificar y analizarlas: historia y evolución, autor, función-forma,
utilización del lenguaje visual, su importancia comercial y económica, sus valores, su poder de
comunicación y su coherencia asegurada a través del manual de identidad (descubrir sus
estructuras comunicativas, formales y semánticas). La última parte consistirá en redibujar con un
programa vectorial una de las marcas. De este modo se experimentará la lógica del diseño a
través del trazado de curvas y tangentes. Con esta actividad nos aproximamos al área de interés
del alumnado por las marcas y a través de ellas profundizamos en su función y su forma,
contextualizamos la realidad de la imagen de marca.  
> En una segunda parte proponemos el proyecto de diseño de una marca. Este avanzará a
través de la propuesta de actividades con diferentes agrupaciones que en cada caso favorecerán
un tipo de aprendizaje, autónomo e introspectivo o cooperativo asumiendo su responsabilidad 
dentro de un grupo. Estas diferentes agrupaciones nos van a aproximar a uno de sus objetivos,
contextualizar el aprendizaje en el mundo profesional. Las actividades se van a organizar
siguiendo los pasos que se dan en la vida real a la hora de abordar un proyecto de este tipo. Esta
actividad vendrá introducida a través del estudio sobre la creación de imagen corporativa, sus
aplicaciones y la presentación de grandes diseñadores españoles como Alberto Corazón y Cruz
Novillo. Así mismo se recordarán las fases que deben seguirse en un proyecto y las técnicas
creativas. Estos contenidos se irán introduciendo entre las fases y se irán recordando.
La propuesta viene dada por un briefing (instrucciones: características, tiempos, presupuesto,
etc.). Cada grupo de 3-5 alumnos recibe uno. Siguiéndolo se acometerán las fases propias de un
proyecto creativo: 1º Búsqueda de información: realizarán una presentación con todo el material
que recaben. 2º Aplicación de técnicas creativas: lluvias de ideas, mapa mental... quedará
plasmado en un resumen. 3º Realización de bocetos y selección: se alternará el trabajo individual
en su producción y grupal para su selección. 4º Diseño de la marca: el boceto elegido será
sometido a una depuración formal y conceptual, a una racionalización geométrica, se
determinarán sus colores (Pantone, CMYK), teniendo en cuenta su finalidad comunicativa y
funcional así como los soportes sobre los que será reproducido (impresión, audiovisuales, páginas
web, otros medios analógicos y virtuales). El trabajo será individual y se concretará en formato
papel y formato digital.
En una presentación sobre papel: bocetos en los que se refleje una evolución, realización del
logotipo final sobre una retícula utilizando las herramientas del dibujo técnico (trazado de
tangentes).  
> En una presentación digital se realizará una aproximación a un manual de identidad
corporativa. Aparecerá el logotipo trazado en un programa de dibujo vectorial en versiones para
diferentes contextos, positivo y negativo y en escala de grises. Se explicitarán los colores
utilizados (CMYK o pantone), así como la tipografía utilizada (si fuera el caso). En esta
presentación se incluirá una breve memoria que explique cómo se ajusta el proyecto a las
necesidades expuestas en el briefing a través de la utilización de las herramientas de la sintaxis
visual, de la psicología de la imagen, de la teoría y la psicología del color.  

**Economía**  
Página 104  
> Las metodologías activas son ideales para impartir la materia, ya que impulsan a que el
alumno sea el protagonista de su propio aprendizaje. Para llevarlas a cabo, se pueden desarrollar
actividades en las que se parta de la observación de la realidad económica que le rodea,
realizando una investigación en equipo sobre lo contemplado en la que se midan determinados
aspectos con herramientas económicas. A continuación, se realizaría un análisis de esos datos
económicos en el que, obteniendo conclusiones de forma reflexiva, se comunicarían al grupo-
clase mediante una exposición oral con ayuda de alguna herramienta multimedia. Finalmente, se
podría culminar con un debate guiado por el docente.  
> A modo orientativo de cómo pueden plantearse estas actividades que promueve la Economía,
se presenta el siguiente ejemplo:  
> Esta actividad tiene como objetivo descubrir la situación económica y laboral del entorno en el
que habita el alumnado. Para ello, tras agruparse en equipos, han de realizar unas entrevistas a
pie de calle y elaborar una pequeña encuesta de población activa de los ciudadanos de su barrio.
Han de seguir, en la medida de lo posible, la metodología de la Encuesta de Población Activa
(EPA), diferenciando según edad, género, nivel de estudios, además de añadir la categoría de
clase social. De igual manera, se han de clasificar las respuestas según los distintos tipos de
desempleo que hayan estudiado previamente en el aula. Los datos obtenidos se analizarán
utilizando hojas de cálculo, en las que se construirán gráficos. Posteriormente, se elaborará una
investigación acerca de los datos de desempleo que pueden encontrar en organismos de su
localidad, de la Comunidad de Madrid, en el INE o, incluso, en Eurostat. A través de la misma, se
podrá observar, comparar y analizar las diferencias territoriales que se producen a escala local,
regional, estatal y europea, así como las desigualdades existentes según los criterios ya
mencionados. Del mismo modo, indagarán sobre las distintas políticas de empleo existentes, tanto
activas como pasivas, para extraer conclusiones acerca de las soluciones más adecuadas para
solventar el problema de desempleo existente en su entorno. Con posterioridad, y manteniendo
los mismos equipos, confeccionarán una exposición oral al resto del grupo-clase sobre todos los
datos y conclusiones, valiéndose de la ayuda de herramientas multimedia. Por último, el profesor
moderará un debate que versará sobre las conclusiones obtenidas. Esta actividad se puede
relacionar con los bloques de contenido C, D y E. Además, contribuye a desarrollar las
competencias específicas 1, 2, 3, 5 y 6 de este currículo.  

**Economía, emprendimiento y actividad empresarial**  
Página 111  
> La metodología debe ser activa e implicar al alumno en su propio aprendizaje basándose en
situaciones reales y buscando un enfoque interdisciplinar. Se debe potenciar un aprendizaje
competencial y autónomo. Para conseguirlo, se pueden llevar a cabo tareas en las que se parta
de la observación por parte del alumno de la realidad económica que le rodea. A modo de
ejemplo, se plantea una actividad cuyo objetivo sería la investigación por parte de los alumnos de
los efectos económicos que se pueden derivar de una crisis como la que produjo la COVID-19, las
herramientas que se pueden utilizar para su superación y las lecciones que se pueden sacar para
otras crisis. Los alumnos, divididos en grupos, comenzarían investigando, en distintas fuentes
oficiales estadísticas y en medios de comunicación, los efectos económicos que produjo esta
crisis sanitaria en nuestro país y en otros países con distinto grado de desarrollo. Posteriormente
realizarían, por equipos, una exposición oral al resto del grupo-clase sobre todos los datos y
conclusiones obtenidos con la ayuda de alguna de las herramientas digitales, finalizando la
actividad con un debate guiado por el profesor. Esta actividad se puede relacionar especialmente
con los bloques de contenidos A.2, «Economía y otras disciplinas», y B, «Emprendimiento».
Además, contribuye a desarrollar las competencias específicas 1, 2, 3, y 6 de este currículo.  

**Educación Física**  
Página 117  
> A modo de ejemplo de actividad se plantea la posibilidad de llevar a cabo un juego de rol en el
aula. En él se asignarán diferentes papeles a cada uno de los estudiantes, desde futbolista
profesional de élite capaz de acaparar todas las portadas, técnico deportivo, utillero, presidente
del club de fútbol o periodista especializado en deportes. Con esta actividad se pretende generar
un espíritu crítico, analizando los mecanismos que intervienen en la toma de decisiones en
diferentes contextos de la práctica de la actividad física. El profesor será el encargado de ir
introduciendo en el juego diferentes elementos de discusión, tales como una conducta poco ética
y poco respetuosa con el entorno o con sus compañeros de la estrella del equipo. Por último, los
alumnos a los que les ha sido asignado el rol de periodista redactarán noticias en las que
analizarán el comportamiento de sus compañeros en los diferentes escenarios que haya
propuesto el profesor, recabando información entre los protagonistas de los mismos y respetando
las claves del estilo periodístico.  

**Empresa y diseño de modelos de negocio**  
Página 126  
> A modo orientativo de cómo pueden plantearse estas actividades que promueve la Economía,
se presenta el siguiente ejemplo:  
> La actividad descrita a continuación tiene como objetivo que el alumnado descubra, por sus
propios medios y en grupo, el entorno y la responsabilidad social corporativa de las empresas
cercanas. Para ello, se han de organizar equipos que investiguen acerca de medidas concretas
que hayan realizado empresas de su alrededor, de distintos tipos y tamaños, para alcanzar los
objetivos marcados en su política de RSC. A continuación, han de pensar acciones concretas que
mejoren los objetivos planificados en la misma, utilizando herramientas de organización de ideas,
como el Pensamiento Visual o Visual Thinking. Una vez decididas las acciones, simularán el
proceso de comunicación a los empresarios para convencerles de su aplicación, usando la
«narración de historias o storytelling» y «el discurso en el ascensor o elevator pitch». Cada equipo
grabará un vídeo o editará un podcast en el que se simule la presentación de esas acciones de
mejora a los directivos de la empresa y se expondrá en el aula ante sus iguales. Por último, y tras
cada exposición, se realizará un debate, dirigido por el docente, sobre la idoneidad, la viabilidad y
la eficiencia de las recomendaciones propuestas. Esta actividad se puede relacionar con los
bloques de contenido A y C. Además, contribuye a desarrollar las competencias específicas 1, 2, 4
y 5 de este currículo.  

**Filosofía**  
Página 131  
> En el marco de las actividades que pueden desarrollarse en el aula, se deben proponer
aquellas que favorezcan distintos tipos de agrupamientos, permitiendo que el alumnado asuma
responsabilidades personales y actúe de forma cooperativa. De esta forma, con el fin de trabajar
los contenidos del bloque B «Conocimiento y Realidad», a modo de ejemplo, se podría proponer
la siguiente tarea: tras la proyección de la película «Doce hombres sin piedad», de Sidney Lumet
(1957), los alumnos, en grupos de cinco, identificarán los argumentos falaces que mantienen los
personajes y que persiguen la condena a muerte del sospechoso de un crimen. Cada grupo se
ocupará de dos de los personajes y, además de identificar sus argumentos falaces, tratará de
determinar los verdaderos motivos que se ocultan tras ellos. Después del turno de exposiciones,
se producirá un debate en torno al siguiente tema: «Estando convencidos de la idoneidad de una
postura, ¿es válido utilizar cualquier procedimiento argumentativo?». A continuación, cada grupo
preparará la defensa de una postura sobre un tema polémico, incluyendo en sus discursos
argumentos falaces que deberán ser identificados por el resto de los grupos. Es conveniente que
el alumnado trate de incluir argumentos extraídos de los medios de comunicación, las redes
sociales, el cine o la televisión. Finalmente, y a modo de conclusión, cada alumno preparará una
disertación escrita sobre el siguiente tema: «La desinformación como instrumento de control
social». Esta actividad abordaría contenidos referidos a los tres bloques de la materia,
especialmente de los epígrafes B.1 «El problema filosófico del conocimiento y la verdad» y C.1
«La acción humana: filosofía, ética y política», contribuyendo a desarrollar las competencias
específicas 2, 3, 4, 5, 7 y 8 del currículo.  

**Física**  
Página 140  
>  Por tanto, la metodología más apropiada para trabajar esta materia se basará en el
planteamiento de actividades contextualizadas y estructuradas, que impliquen el desarrollo de
tareas de investigación en las que los contenidos estén integrados de una forma competencial y
que permitan al alumnado afianzar e integrar los contenidos de la disciplina. Como orientación
para la práctica docente se sugiere la siguiente actividad: a partir del trabajo interdisciplinar la
materia de Física con las materias de Matemáticas y Tecnología e Ingeniería, los alumnos crearán
una simulación que permita observar visualmente la evolución temporal de la ley de decrecimiento
exponencial. Se sugiere que ciertos valores sean parametrizados a fin de comprobar cómo afecta
a la velocidad de desintegración la constante de desintegración propia del material. El alumno
deberá, asimismo, implementar en la programación de la simulación el uso correcto de la función
aleatoria, dando cuenta del carácter estadístico del fenómeno radiactivo. Esta actividad contribuye
a desarrollar todas las competencias específicas de la materia.  

**Física y Química**  
Página 147  
> El enfoque STEM de la materia Física y Química establecerá, como forma de trabajo
preferente, experiencias de laboratorio, trabajo de campo y, en definitiva, las metodologías propias
de la física y la química. De esta forma, el alumnado asimilará mejor los contenidos ya que los
conectará con la realidad que les rodea. Para conseguir tales propósitos, se recomienda poner en
práctica actividades competenciales, basadas en situaciones reales y que busquen un enfoque
interdisciplinar.  
> De esta forma, se podría plantear trabajar de manera interdisciplinar los contenidos de los
bloques D y E, «Cinemática» y «Estática y dinámica» respectivamente, junto con el bloque A de la
asignatura Educación Física, llamado «Vida activa y saludable» – cuyo contenido recoge
«Prácticas de actividad física con efectos positivos sobre la salud personal y colectiva: la práctica
de la bicicleta como medio de transporte habitual» –, y con el bloque B, llamado «Materiales y
fabricación», de la materia Tecnología e Ingeniería I a través de la siguiente actividad: los
alumnos, divididos en grupos, analizarían las variables cinemáticas que intervienen en los
movimientos propios del ciclismo, investigando el efecto de cambiar la posición del centro de
masas del sistema bicicleta-cuerpo al modificar la altura del sillín de la bicicleta. También se
puede investigar qué sucede cuando se utiliza el freno delantero, el trasero o ambos en un
movimiento. Del mismo modo, se realizarían cálculos cinemáticos sobre otros deportes para que
comprendan e integren los contenidos adquiridos en la materia. Esta actividad contribuiría a
desarrollar las competencias específicas 1, 2, 3 y 5 de la asignatura.  

**Química**  
Página 353  
> A modo de ejemplo se podría proponer la siguiente actividad: un estudio de la industria del
vinagre basado en cuestiones como qué es, cómo se produce y cuál es su etiquetado, aporta un
contexto a los conceptos y problemas referentes a las secciones de ácido-base y química
orgánica, aparte de las cuestiones relacionadas con la materia de Biología. De esta forma, se
podrían desarrollar las competencias específicas 1, 3, 4 y 5 de la asignatura.  


## [Recursos situaciones aprendizaje](/home/recursos/situaciones-de-aprendizaje) 

