# Currículo LOMLOE: competencias específicas
Resumen rápido: se pueden traducir por "criterios de evaluación" (de hecho se enumeran una lista de competencias específicas dentro del apartado de currículo de criterios de evaluación). Su redacción usa infinitivo.  

[Real Decreto 217/2022 ESO Artículo 2. Definiciones.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a2)  
> c) Competencias específicas: desempeños que el alumnado debe poder desplegar en actividades o en situaciones cuyo abordaje requiere de los [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos) de cada materia o ámbito. Las competencias específicas constituyen un elemento de conexión entre, por una parte, el Perfil de salida del alumnado, y por otra, los [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos) de las materias o ámbitos y los [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion).  

[Real Decreto 243/2022 Bachillerato Artículo 2. Definiciones.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a2)  
 > c) Competencias específicas: desempeños que el alumnado debe poder desplegar en actividades o en situaciones cuyo abordaje requiere de los [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos) de cada materia. Las competencias específicas constituyen un elemento de conexión entre, por una parte, las competencias clave, y por otra, los [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos) de las materias y los [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion).

Enlaza con [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion)  

Un ejemplo concreto real de 2º ESO que muestra que puede no ser algo muy distinto  
LOMCE:  
* Estándar 4.1. Reconoce e identifica los símbolos más frecuentes utilizados en el etiquetado de productos químicos e instalaciones, interpretando su significado.  
* Estándar 4.2. Identifica material e instrumentos básicos de laboratorio y conoce su forma de utilización para la realización de experiencias respetando las normas de seguridad e identificando actitudes y medidas de actuación preventivas.  
LOMLOE: 
* Competencia específica 3.1 Identificar los símbolos más utilizados en el etiquetado de productos químicos y en las instalaciones de un laboratorio, interpretando su significado.
* Competencia específica 3.2. Conocer y respetar las normas de uso de uso de los espacios específicos de la ciencia, como el laboratorio de física y química, identificando los materiales e instrumentos básicos del mismo.



En [Ideas clave de la LOMLOE - Raúl Diego](https://www.rauldiego.es/ideas-clave-de-la-lomloe/) se indica en página 41 de 52  

> Formuladas en QUÉ + CÓMO + PARA QUÉ  

> Estructura sintáctica de la formulación  
> qué (infinitivo) + cómo (gerundio, a través de ...) + para qué (para + infinitivo)  

Y se pone un ejemplo, pero se puede comprobar mirando los de FQ que no se cumple siempre para todos.  


Se contectan / asocian a descriptores del [PERFIL DE SALIDA](/home/legislacion-educativa/lomloe/curriculo-perfil-de-salida)   


En [Currículo LOMLOE concepto y estructura](/home/legislacion-educativa/lomloe/curriculo-concepto-estructura) comento relacionado el vídeo "Prepárate para la LOMLOE: cómo evaluar y planificar con Additio" y luego comento en [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion)  de donde pongo solo una frase de normativa de Andalucía:  

> 5. En los cursos primero y tercero, **la totalidad de los criterios de evaluación contribuyen en la misma medida, al grado de desarrollo de la competencia específica, por lo que tendrán el mismo valor a la hora de determinar el grado de desarrollo de la misma**.  


Ver [competencias](/home/legislacion-educativa/competencias)  
>**Competencias específicas** 
Se entiende este como el segundo nivel de concreción de las competencias. Se formularán de manera que 
plasme la concreción de los descriptores de las competencias clave que se han recogido en el Perfil de 
salida para cada área (EP)/materia(ESO) de la etapa de la que se trate.  
Para cada competencia específica se proporcionará:   
- Una descripción de la misma y su vinculación con otras competencias y con el perfil del alumnado. 
- Orientaciones sobre situaciones de aprendizaje que favorecen su adquisición. 

>Los descriptores constituirán el marco referencial para la definición de las **competencias específicas** de las áreas o materias, que _identifican las capacidades y los principales tipos de actuaciones asociadas a ellas que los alumnos y alumnas deben poder desplegar en situaciones o actividades organizadas en torno a los [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos) propios del área o materia, o cuyo abordaje requiere de estos 
saberes, así como, en su caso, los criterios que deben cumplir las actuaciones desplegadas y las características de las situaciones y actividades en las que deben poder desplegarse._   

>c) Competencias específicas: Desempeños que el alumnado debe poder desplegar en 
actividades o en situaciones cuyo abordaje requiere de los [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos) de cada materia. 
Las competencias específicas constituyen un elemento de conexión entre, por una parte, 
el Perfil competencial del alumnado al término **del Bachillerato**, 
y por otra, los [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos) de las materias y los criterios de evaluación.  


[twitter rogerCampru/status/1564875090449375233](https://twitter.com/rogerCampru/status/1564875090449375233)  
![](https://pbs.twimg.com/media/FbeNkkoWYAEkXnP?format=jpg)  
[twitter pbeltranp/status/1563586494786576385](https://twitter.com/pbeltranp/status/1563586494786576385)  
¿Quieres saber más sobre la competencia matemática en el currículo LOMLOE de Primaria? Aquí el artículo que nos publicaron a @AngelAlsinaP y a un servidor en @margenes_rev hace un mes.  
Pd. Diría que interesa también para Secundaria.  
[La competencia matemática en el currículo español de Educación Primaria. Julio 2022. Márgenes Revista de Educación de la Universidad de Málaga 3(2):31-5](https://www.researchgate.net/publication/362349131_La_competencia_matematica_en_el_curriculo_espanol_de_Educacion_Primaria)  


[twitter fqmente/status/1566130271031885824](https://twitter.com/fqmente/status/1566130271031885824)  
![](https://pbs.twimg.com/media/FbwB5luXEAAkDDv?format=png)  
[Conexiones entre las competencias específicas de Física y Química y los descriptores del perfil de salida LOMLOE](https://fisiquimicamente.com/blog/2022/09/03/perfil-de-salida-lomloe/)  
> A continuación se presentan diagramas de Sankey interactivos mostrando las conexiones entre las competencias específicas de la materia de Física y Química1 y los descriptores operativos de las competencias clave en la enseñanza básica y bachillerato, tal y como se recoge en el R.D. 217/2022, de 29 de marzo (ESO) y el R.D. 243/2022, de 5 de abril (Bach), respectivamente.  

[twitter pbeltranp/status/1583210227016806400](https://twitter.com/pbeltranp/status/1583210227016806400)  
No se pueden relacionar competencias específicas con saberes. Un mismo saber, dependiendo de con qué tareas se trabaje (por no decir situaciones de aprendizaje) movilizará unas CE y otras. O ninguna.  



