## Currículo LOMLOE: saberes básicos
Resumen rápido: se pueden traducir por contenidos, que incluyen conocimientos, destrezas y actitudes.   

[Real Decreto 217/2022 ESO Artículo 2. Definiciones.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a2)  
[Real Decreto 243/2022 Bachillerato Artículo 2. Definiciones.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521#a2)  
> e) Saberes básicos: conocimientos, destrezas y actitudes que constituyen los contenidos propios de una materia y cuyo aprendizaje es necesario para la adquisición de las [competencias específicas](/home/legislacion-educativa/lomloe/curriculo-competencias-especificas).  

>**Saberes básicos/Contenidos**   
Se formulan integrando los diferentes tipos de saberes –conocimientos, destrezas y actitudes– evitando 
la forma de listado de hechos o conceptos.  
En el currículo básico, se presenta en bloques básicos de contenido. En los currículos concretos de cada 
materia, se desarrollarán dichos bloques en función de las demandas de los criterios de evaluación. 

(se citan saberes básicos en	 el texto citado al hablar de competencias específicas)    
>En definitiva, los descriptores que se formulan en el apartado 5.1. de este documento base permiten organizar los aprendizajes del currículo en torno a las diferentes áreas o materias y son los referentes para establecer los **saberes básicos** asociados a las mismas cuyo aprendizaje y evaluación se realizará de manera competencial. 

>...los contenidos enunciados en forma de saberes básicos.  

Ideas:  
[twitter sebasesrad/status/1583514432650366976](https://twitter.com/sebasesrad/status/1583514432650366976)  
¿Crees que hay unas asignaturas más lomloizables que otras?  
[twitter denyureypunto/status/1584046757641494530](https://twitter.com/denyureypunto/status/1584046757641494530)  
Sí. Sólo he visto satisfechos a algunos de matemáticas y lengua, y entre ellos los hay que no dan clase en secundaria o directamente implicados en la ley, y por tanto parte interesada. Me sorprendería que encontrases muchos profesores de hª de 2º eso contentos  



## Saberes básicos mínimos 

Surgen comentarios sobre Andalucía (respaldados por un documento oficial de instrucciones 2022-2023) que comento en [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion) y que también aplican a [saberes básicos](/home/legislacion-educativa/lomloe/curriculo-saberes-basicos), ya que allí definen saberes básicos mínimos   

> 3. En los cursos primero y tercero **se consideran saberes básicos mínimos, los saberes básicos que como mínimo están vinculados a cada criterio de evaluación y se han de trabajar de manera conjunta**. En los Anexos III, IV, V y VI se incluyen tablas en cada materia de los cursos primero y tercero, que presentan la vinculación de competencias específicas, con criterios de evaluación y saberes básicos. Así como la relación de criterios de evaluación y competencias específicas viene establecida en la normativa básica, la vinculación de saberes se presenta con la intención de orientar la planificación de los aprendizajes de manera que pueda ser ampliada siempre que se desarrollen todos los saberes mínimos.  

Eso enlaza también con la idea de que hay saberes básicos que no son mínimos, no tienen criterios de evaluación asociados y pueden no ser evaluados, además de que no hay oficialmente (sí la hay en Andalucía, no en Madrid) una vinculación de criterios de evaluación con saberes mínimos, por lo que se puede ser "competente en vacío", sin saber qué contenido se trabaja.    

La relación de Andalucía entre competencias específicas y saberes es cuestionable según las actividades / situaciones de aprendizaje
[twitter pbeltranp/status/1583210227016806400](https://twitter.com/pbeltranp/status/1583210227016806400)  
No se pueden relacionar competencias específicas con saberes. Un mismo saber, dependiendo de con qué tareas se trabaje (por no decir situaciones de aprendizaje) movilizará unas CE y otras. O ninguna.  


