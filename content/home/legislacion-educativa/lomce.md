# LOMCE (Ley Orgánica para la Mejora de la Calidad Educativa)  

## Legislación estatal

### Dictamen 36/2012 del Consejo Escolar de Estado al Anteproyecto de Ley Orgánica para la Mejora de la Calidad Educativa (LOCME).
 [dictamen0362012.pdf](http://www.educacionyfp.gob.es/dctm/cee/el-consejo/dictamenes/2012/dictamen0362012.pdf?documentId=0901e72b81458191) 

### CE-D-2013-172 Dictamen del Consejo de Estado sobre LOMCE
 [BOE.es - CE-D-2013-172](http://www.boe.es/buscar/doc.php?id=CE-D-2013-172) 

### Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa.
 [BOE-A-2013-12886 Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa.](https://www.boe.es/buscar/act.php?id=BOE-A-2013-12886)   
Realmente tiene un artículo único que modifica la LOE, luego se puede decir que a nivel de legislación "LOMCE es LOE actualizada", y sería consultar la LOE actualizada.

### Real Decreto 412/2014, de 6 de junio, por el que se establece la normativa básica de los procedimientos de admisión a las enseñanzas universitarias oficiales de Grado.
 [BOE.es - BOE-A-2014-6008 Real Decreto 412/2014, de 6 de junio, por el que se establece la normativa básica de los procedimientos de admisión a las enseñanzas universitarias oficiales de Grado.](https://www.boe.es/buscar/act.php?id=BOE-A-2014-6008) 

### Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato (LOMCE).
Asociado a LOMCE (modificación LOE)  
[BOE-A-2015-37 Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.](http://www.boe.es/buscar/act.php?id=BOE-A-2015-37)   
Versión pdf
[Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.](https://www.boe.es/boe/dias/2015/01/03/pdfs/BOE-A-2015-37.pdf)   


### Orden ECD/1361/2015, de 3 de julio, por la que se establece el currículo de Educación Secundaria Obligatoria y Bachillerato para el ámbito de gestión del Ministerio de Educación, Cultura y Deporte, y se regula su implantación, así como la evaluación continua y determinados aspectos organizativos de las etapas.
No es estatal (aplica a Ceuta y Melilla), pero la cito porque menciona explícitamente idea de "instrumentos de evaluación" en programaciones que aplican a otras comunidades  
[BOE-A-2015-7662 Orden ECD/1361/2015, de 3 de julio, por la que se establece el currículo de Educación Secundaria Obligatoria y Bachillerato para el ámbito de gestión del Ministerio de Educación, Cultura y Deporte, y se regula su implantación, así como la evaluación continua y determinados aspectos organizativos de las etapas.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-7662) 

### Orden ECD/462/2016, de 31 de marzo, por la que se regula el procedimiento de incorporación del alumnado a un curso de Educación Secundaria Obligatoria o de Bachillerato del sistema educativo definido por la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa, con materias no superadas del currículo anterior a su implantación.
 [BOE-A-2016-3229 Orden ECD/462/2016, de 31 de marzo, por la que se regula el procedimiento de incorporación del alumnado a un curso de Educación Secundaria Obligatoria o de Bachillerato del sistema educativo definido por la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa, con materias no superadas del currículo anterior a su implantación.](https://www.boe.es/buscar/act.php?id=BOE-A-2016-3229) 

### Orden ECD/563/2016, de 18 de abril, por la que se modifica la Orden EDU/849/2010, de 18 de marzo, por la que se regula la ordenación de la educación del alumnado con necesidad de apoyo educativo y se regulan los servicios de orientación educativa en el ámbito de gestión del Ministerio de Educación, en las ciudades de Ceuta y Melilla.
No es estatal (aplica a Ceuta y Melilla) pero la cito porque cita ideas que aplican a otras comunidades  
[BOE-A-2016-3772 Orden ECD/563/2016, de 18 de abril, por la que se modifica la Orden EDU/849/2010, de 18 de marzo, por la que se regula la ordenación de la educación del alumnado con necesidad de apoyo educativo y se regulan los servicios de orientación educativa en el ámbito de gestión del Ministerio de Educación, en las ciudades de Ceuta y Melilla.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-3772) 

### Real Decreto 310/2016, de 29 de julio, por el que se regulan las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato.
 [BOE-A-2016-7337 Real Decreto 310/2016, de 29 de julio, por el que se regulan las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337) 

### Orden ECD/1663/2016, de 11 de octubre, por la que se regulan las pruebas de acceso a la universidad de las personas mayores de 25 o de 45 años de edad, así como el acceso mediante acreditación de experiencia laboral o profesional, en el ámbito de la Universidad Nacional de Educación a Distancia.
 [BOE-A-2016-9514 Orden ECD/1663/2016, de 11 de octubre, por la que se regulan las pruebas de acceso a la universidad de las personas mayores de 25 o de 45 años de edad, así como el acceso mediante acreditación de experiencia laboral o profesional, en el ámbito de la Universidad Nacional de Educación a Distancia.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-9514) 

### Real Decreto-ley 5/2016, de 9 de diciembre, de medidas urgentes para la ampliación del calendario de implantación de la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa.
 [BOE-A-2016-11733 Real Decreto-ley 5/2016, de 9 de diciembre, de medidas urgentes para la ampliación del calendario de implantación de la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-11733) 

### Orden ECD/1941/2016, de 22 de diciembre, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, para el curso 2016/2017
 [BOE-A-2016-12219 Orden ECD/1941/2016, de 22 de diciembre, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, para el curso 2016/2017.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-12219) 

### Real Decreto 562/2017, de 2 de junio, por el que se regulan las condiciones para la obtención de los títulos de Graduado en Educación Secundaria Obligatoria y de Bachiller, de acuerdo con lo dispuesto en el Real Decreto-ley 5/2016, de 9 de diciembre, de medidas urgentes para la ampliación del calendario de implantación de la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa.
 [BOE-A-2017-6250 Real Decreto 562/2017, de 2 de junio, por el que se regulan las condiciones para la obtención de los títulos de Graduado en Educación Secundaria Obligatoria y de Bachiller, de acuerdo con lo dispuesto en el Real Decreto-ley 5/2016, de 9 de diciembre, de medidas urgentes para la ampliación del calendario de implantación de la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa.](http://boe.es/diario_boe/txt.php?id=BOE-A-2017-6250)  
 El dictamen del Consejo de Estado en  [BOE.es - CE-D-2017-421](http://boe.es/buscar/doc.php?id=CE-D-2017-421)   
 
## Legislación Comunidad de Madrid  

### [DECRETO 52/2015, de 21 de mayo, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato.](http://www.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=8937) 

### [DECRETO 48/2015, de 14 de mayo, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo de la Educación Secundaria Obligatoria.](http://www.madrid.org/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=8934&cdestado=P#no-back-button) 

### [ORDEN 1493/2015, de 22 de mayo, de la Consejería de Educación, Juventud y Deporte, por la que se regula la evaluación y la promoción de los alumnos con necesidad específica de apoyo educativo, que cursen segundo ciclo de Educación Infantil, Educación Primaria y Enseñanza Básica Obligatoria, así como la flexibilización de la duración de las enseñanzas de los alumnos con altas capacidades intelectuales en la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2015/06/15/BOCM-20150615-12.PDF) 

### [ORDEN 1459/2015, de 21 de mayo, de la Consejería de Educación, Juventud y Deporte, por la que se desarrolla la autonomía de los centros educativos en la organización de los Planes de Estudio de la Educación Secundaria Obligatoria en la Comunidad de Madrid.](http://www.bocm.es/boletin/CM_Orden_BOCM/2015/05/25/BOCM-20150525-20.PDF) 

### [ORDEN 1493/2015, de 22 de mayo, de la Consejería de Educación, Juventud y Deporte, por la que se regula la evaluación y la promoción de los alumnos con necesidad específica de apoyo educativo, que cursen segundo ciclo de Educación Infantil, Educación Primaria y Enseñanza Básica Obligatoria, así como la flexibilización de la duración de las enseñanzas de los alumnos con altas capacidades intelectuales en la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2015/06/15/BOCM-20150615-12.PDF) 

### [ORDEN 1513/2015, de 22 de mayo, de la Consejería de Educación, Juventud y Deporte, por la que se desarrolla la autonomía de los centros educativos en la organización de los planes de estudio del Bachillerato en la Comunidad de Madrid.](http://www.bocm.es/boletin/CM_Orden_BOCM/2015/05/25/BOCM-20150525-21.PDF) 

### [ORDEN 2160/2016, de 29 de junio, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid.](http://www.bocm.es/boletin/CM_Orden_BOCM/2016/07/01/BOCM-20160701-7.PDF) 

### [ORDEN 2398/2016, de 22 de julio, de la Consejería de Educación, Juventud y Deporte de la Comunidad de Madrid, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en la Educación Secundaria Obligatoria.](http://www.madrid.org/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=9459&cdestado=P#no-back-button) 

### [ORDEN 2582/2016, de 17 de agosto, de la Consejería de Educación, Juventud y Deporte de la Comunidad de Madrid, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en el Bachillerato.](http://www.bocm.es/boletin/CM_Orden_BOCM/2016/08/29/BOCM-20160829-5.PDF) 
Tiene "Capítulo III, Atención a la diversidad"  

### [CORRECCIÓN de errores de la Orden 2582/2016, de 17 de agosto, de la Consejería de Educación, Juventud y Deporte de la Comunidad de Madrid, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en el Bachillerato.](http://www.bocm.es/boletin/CM_Orden_BOCM/2016/10/06/BOCM-20161006-23.PDF) 

### [Orden 3357/2016, de 17 de octubre, de la Consejería de Educación, Juventud y Deporte, por la que se ordenan y organizan para las personas adultas las enseñanzas del Bachillerato en los regímenes nocturno y a distancia en la Comunidad de Madrid.](http://www.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=9536) 

### [ORDEN 3295/2016, de 10 de octubre, de la Consejería de Educación, Juventud y Deporte, por la que se regulan para la Comunidad de Madrid los Programas de Mejora del Aprendizaje y del Rendimiento en la Educación Secundaria Obligatoria.](http://www.bocm.es/boletin/CM_Orden_BOCM/2016/11/04/BOCM-20161104-16.PDF) 

### [ORDEN 47/2017, de 13 de enero, de la Consejería de Educación, Juventud y Deporte, por la que se desarrollan determinados aspectos de la evaluación final de Bachillerato para el acceso a la universidad.](http://www.bocm.es/boletin/CM_Orden_BOCM/2017/01/19/BOCM-20170119-1.PDF) 

### [ORDEN 2200/2017, de 16 de junio, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid.](http://www.bocm.es/boletin/CM_Orden_BOCM/2017/06/27/BOCM-20170627-15.PDF) 
 
### [Orden 2043/2018, de 4 de junio, de la Consejería de Educación e Investigación, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid para su implantación a partir de 2018-2019, y se modifica la Orden 2200/2017, de 16 de junio, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid, así como la Orden 1255/2017, de 21 de abril, de la Consejería de Educación, Juventud y Deporte, por la que se establece la organización de las enseñanzas para la obtención del título de Graduado en Educación Secundaria Obligatoria por personas adultas en la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2018/06/14/BOCM-20180614-16.PDF)  

### [Corrección de errores de la Orden 2043/2018, de 4 de junio, de la Consejería de Educación e Investigación, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid para su implantación a partir de 2018-2019, y se modifica la Orden 2200/2017, de 16 de junio, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid, así como la Orden 1255/2017, de 21 de abril, de la Consejería de Educación, Juventud y Deporte, por la que se establece la organización de las enseñanzas para la obtención del título de Graduado en Educación Secundaria Obligatoria por personas adultas en la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2018/06/25/BOCM-20180625-17.PDF)  
 

