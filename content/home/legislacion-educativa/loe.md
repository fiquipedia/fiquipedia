# LOE (Ley Orgánica de Educación)  

## Legislación estatal

### Ley Orgánica 2/2006, de 3 de mayo, de Educación (en su versión actualizada se puede considerar que no es LOE sino LOMCE)
 [BOE-A-2006-7899 Ley Orgánica 2/2006, de 3 de mayo, de Educación.](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899) 

### REAL DECRETO 1631/2006 Currículo de ESO (LOE)
 [BOE-A-2007-238 Real Decreto 1631/2006, de 29 de diciembre, por el que se establecen las enseñanzas mínimas correspondientes a la Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2007-238) 

### REAL DECRETO 1467/2007 Currículo de Bachillerato (LOE)
Real Decreto 1467/2007, de 2 de noviembre, por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas. [BOE-A-2007-19184 Real Decreto 1467/2007, de 2 de noviembre, por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas.](http://www.boe.es/buscar/act.php?id=BOE-A-2007-19184) 

### Real Decreto 1892/2008, de 14 de noviembre, por el que se regulan las condiciones para el acceso a las enseñanzas universitarias oficiales de grado y los procedimientos de admisión a las universidades públicas españolas.
 [BOE-A-2008-18947 Real Decreto 1892/2008, de 14 de noviembre, por el que se regulan las condiciones para el acceso a las enseñanzas universitarias oficiales de grado y los procedimientos de admisión a las universidades públicas españolas.](https://www.boe.es/buscar/act.php?id=BOE-A-2008-18947) 

### Orden EDU/2395/2009, de 9 de septiembre, por la que se regula la promoción de un curso incompleto del sistema educativo definido por la Ley Orgánica 1/1990, de 3 de octubre, de ordenación general del sistema educativo, a otro de la Ley Orgánica 2/2006, de 3 de mayo, de Educación.
 [BOE-A-2009-14503 Orden EDU/2395/2009, de 9 de septiembre, por la que se regula la promoción de un curso incompleto del sistema educativo definido por la Ley Orgánica 1/1990, de 3 de octubre, de ordenación general del sistema educativo, a otro de la Ley Orgánica 2/2006, de 3 de mayo, de Educación.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2009-14503) 

### Orden EDU/849/2010, de 18 de marzo, por la que se regula la ordenación de la educación del alumnado con necesidad de apoyo educativo y se regulan los servicios de orientación educativa en el ámbito de gestión del Ministerio de Educación, en las ciudades de Ceuta y Melilla.
 [BOE-A-2010-5493 Orden EDU/849/2010, de 18 de marzo, por la que se regula la ordenación de la educación del alumnado con necesidad de apoyo educativo y se regulan los servicios de orientación educativa en el ámbito de gestión del Ministerio de Educación, en las ciudades de Ceuta y Melilla.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2010-5493) 

### Real Decreto 132/2010 requisitos centros ESO y Bachillerato
Real Decreto 132/2010, de 12 de febrero, por el que se establecen los requisitos mínimos de los centros que impartan las enseñanzas del segundo ciclo de la educación infantil, la educación primaria y la educación secundaria.  
[BOE-A-2010-4132 Real Decreto 132/2010, de 12 de febrero, por el que se establecen los requisitos mínimos de los centros que impartan las enseñanzas del segundo ciclo de la educación infantil, la educación primaria y la educación secundaria.](http://www.boe.es/buscar/act.php?id=BOE-A-2010-4132) 

### Real Decreto 1146/2011 currículo ESO 
[BOE-A-2011-13117 Real Decreto 1146/2011, de 29 de julio, por el que se modifica el Real Decreto 1631/2006, de 29 de diciembre, por el que se establecen las enseñanzas mínimas correspondientes a la Educación Secundaria Obligatoria, así como los Reales Decretos 1834/2008, de 8 de noviembre, y 860/2010, de 2 de julio, afectados por estas modificaciones.](http://www.boe.es/buscar/act.php?id=BOE-A-2011-13117) 

### Real Decreto-ley 14/2012 recortes en gasto educativo
[BOE-A-2012-5337 Real Decreto-ley 14/2012, de 20 de abril, de medidas urgentes de racionalización del gasto público en el ámbito educativo.](https://www.boe.es/buscar/act.php?id=BOE-A-2012-5337)  

## Legislación Comunidad de Madrid  

### DECRETO 23/2007 Currículo de ESO (LOE)
Decreto 23/2007, de 10 de mayo, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo de la Educación Secundaria Obligatoria, B.O.C.M. Núm. 126 del 29 de mayo de 2007  
[HTML](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=4648&cdestado=P)  
[PDF, boletín completo](https://www.bocm.es/boletin/CM_Boletin_BOCM/20070529_B/12600.pdf)  
[PDF, currículo secundaria Madrid (DAT Capital)](http://www.madrid.org/dat_capital/loe/pdf/curriculo_secundaria_madrid.pdf) En pdf los anexos están a partir de la página 52 del pdf como imagen con el texto "apaisado", luego no se puede copiar el texto fácilmente, por lo que puede ser útil recurrir a la versión en HTML. Orientativo del pdf (el número de página es el que figura en el boletín, no el número de página del pdf):  
Página 52: Ciencias de la naturaleza. Introducción  
Página 53: Ciencias de la naturaleza. Contribución de la materia a la adquisición de las competencias básicas  
Página 54: Ciencias de la naturaleza. Objetivos  
Página 54: Ciencias de la naturaleza. Primer curso  
Página 55: Ciencias de la naturaleza. Segundo curso  
Página 57: Física y química. Tercer curso  
Página 58: Biología y geología. Tercer curso  
Página 60: Física y química. Cuarto curso  
Página 61: Biología y geología. Cuarto curso

### [RESOLUCIÓN de 27 de junio de 2007, de la Dirección General de Ordenación Académica, sobre la optatividad en la Educación Secundaria Obligatoria derivada de la Ley Orgánica 2/2006, de 3 de mayo, de Educación](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=4653&cdestado=P)  

### ORDEN 1029/2008 evaluación ESO
[ORDEN 1029/2008, de 29 de febrero, de la Consejería de Educación, por la que se regulan para la Comunidad de Madrid la evaluación en la Educación Secundaria Obligatoria y los documentos de aplicación.](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=4934&cdestado=P) 

### ORDEN 3320-01/2007 implantación y organización ESO
[ORDEN 3320-01/2007, de 20 de junio, del Consejero de Educación, por la que se regulan para la Comunidad de Madrid la implantación y la organización de la Educación Secundaria Obligatoria derivada de la Ley Orgánica 2/2006, de 3 de mayo, de Educación.](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=4654&cdestado=P) 

### DECRETO 67/2008 Currículo de Bachillerato (LOE)
[DECRETO 67/2008, de 19 de junio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato.](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&idnorma=6166&word=S&wordperfect=N&pdf=S#_ftnref4)  
Versión PDF
[DECRETO 67/2008, de 19 de junio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Boletin_BOCM/2008/06/27/15200.pdf)  

### ORDEN 3347/2008 Organización Bachillerato
[Orden 3347/2008, de 4 de julio, de la Consejería de Educación, por la que se regula la organización académica de las enseñanzas del Bachillerato derivado de la Ley Orgánica 2/2006, de 3 de mayo, de Educación](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=5131&cdestado=P) 

### ORDEN 3894/2008 Bachillerato nocturno y a distancia
[Orden 3894/2008, de 31 de Julio, de la Consejería de Educación, por la que se ordenan y organizan para las personas adultas las enseñanzas de bachillerato en los regímenes nocturno y a distancia en la Comunidad de Madrid.](http://www.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=5200)

### ORDEN 1931/2009 Evaluación y calificación Bachillerato
[ORDEN 1931/2009, de 24 de abril, de la Consejería de Educación, por la que se regulan para la Comunidad de Madrid la evaluación y la calificación en el Bachillerato y los documentos de aplicación.](http://gestiona.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=5684) 

### ORDEN 5451/2009 modificación de normas reguladoras del Bachillerato
[Orden 5451/2009, de 30 de noviembre, de la Consejería de Educación, de modificación de normas reguladoras del Bachillerato en la Comunidad de Madrid](http://gestiona.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=5242) 
  



