# Recursos sobre estrategias resolución problemas

En física y química la mayor parte de la evaluación es mediante problemas, se tiene la competencia para resolver problemas.  
Aparte de tener conocimientos teóricos y aplicarlos, y de haberse enfrentado y practicado ciertos tipos de problemas, es interesante mencionar ideas generales sobre estrategias de resolución de problemas  

Un tema relacionado lateral serían recursos para memorizar, ya que hay problemas que sin conocer ciertas fórmulas/expresiones no se pueden realizar.  
Pero poner fórmulas no es todo  
[twitter FiQuiPedia/status/1664949223568158722](https://twitter.com/FiQuiPedia/status/1664949223568158722)  
![](https://pbs.twimg.com/media/FxsWfvBXgAEIAAk?format=jpg)  
Un ejemplo relacionado a memorizar  
[twitter ITeachBoys92/status/1214630539136774144](https://twitter.com/ITeachBoys92/status/1214630539136774144)  
I really love simple but effective ideas, I magpied this emoji equation task from @MullerScience and used it with my yr 11's as a quick recall starter today! Much fun was had by all! It's available here:  
[SF Emoji equation recall starter - docs.google](https://docs.google.com/document/d/1BhsgTDfnMRxlBP4jG9aekJBYYKj0g-nSagve_fPwOSM/edit) 
\#science #teamscience #physics #edutwitter #ukedchat  
![](https://pbs.twimg.com/media/ENs8B-HW4AENxE9?format=jpg)  
(En 2023 el tuit no es visible ni la imagen, pero sí el documento)  

[Emoji Physics Equations](https://ankiweb.net/shared/info/302122151)  
![](https://dl15.ankiweb.net/shared/mpreview/302122151/0.png?1589991112)  
pressure = force ÷ area  (p = F / A)  

En recursos sobre tabla periódica tengo algunas ideas sobre memorizar, para memorizar fórmulas hay algunas ideas sencillas; con la Ley de Ohm a veces los alumnos usan "el triángulo", y a veces lo adaptan a expresiones de 3 variables (P=mg, d=m/v, F=ma, F=k·Δx, ...).  
El valor didáctico de esta regla del triángulo es tema aparte, ver este hilo  
[twitter FiQuiPedia/status/1234187205700132864](https://twitter.com/FiQuiPedia/status/1234187205700132864)  
Me lo enseñaron hace años alumnos con R=V/I que usaban en tecnología, diciendo que vale igual para v=e/t y para d=m/v. No les oculto su existencia, intento vean es truco memorizar, inútil si no se entiende. Las propias unidades (pe km/h y g/L) permiten entender la proporción.  
En problemas hay quien pasa de fórmulas y aplica proporciones, lo que en 2ESO digo veo correcto. Cuando se formaliza MRU con x=x0+vt (a la vez que se añade MRUA con término 1/2 at^2) creo que visualizan ya no valen atajos. Se lo intento asociar a ecuaciones rectas y parábolas.  
Mirando hoy un libro de FQ2ESO ISBN 9788468317199  
PD: no en libros, pero los a alumnos se lo he visto usar también para peso (P=m·g) y fuerza elástica (F=k·Δx)
![](https://pbs.twimg.com/media/ET5NuUEWAAg-s1t?format=png)  

El que los alumnos vean la misma ecuación despejada de maneras distintas como ecuaciones distintas a veces muestra falta de competencia matemática  

A veces hay otras reglas asociadas a matemáticas (un día vi una vaca vestida de uniforme ...)  
Ejemplo  
[twitterbiologiayarte/status/993549198103588864](https://twitter.com/biologiayarte/status/993549198103588864)  
![](https://pbs.twimg.com/media/DcnLrkbW4AEReX9.jpg)  

## Estrategias para problemas en general
Las estrategias para problemas generales pueden ser demasiado genéricas para problemas de física y química, pero a veces no está de más echar un vistazo a algunas ideas. En general resolver problemas suele tener una aproximación al planteamiento matemático, lo que ocurre también en física y química.  
	
[ESTRATEGIAS EN LA RESOLUCION DE PROBLEMAS.Rosa Viar Pérez, I.E.S. “Conde de Aranda” ALAGON, 9 de Noviembre de 2007 - unizar.es](https://ttm.unizar.es/2007-08/ESTRATEGIASI.pdf)  
  
Hay un tema de oposiciones de primaria que es TEMA 21. RESOLUCION DE PROBLEMAS. DIFERENTES CLASES Y MÉTODOS DE RESOLUCIÓN. PLANIFICACIÓN, GESTIÓN DE LOS RECURSOS, REPRESENTACIÓN, INTERPRETACIÓN Y VALORACIÓN DE LOS RESULTADOS. ESTRATEGIAS DE INTERVENCIÓN EDUCATIVA.  
Se pueden ver documentos asociados en varios lugares  
[Tema 21: Resolución de problemas. Clases y métodos. Planificación, gestión de recursos, representación, interpretación y valoración de resultados. Estrategias de intervención educativa - educativospara.com](https://www.educativospara.com/tema-21-resolucion-de-problemas-clases-y-metodos/)  

[Reglas de higiene matemática - lacienciaparatodos Javier Fernández Panadero](https://lacienciaparatodos.wordpress.com/2012/05/14/reglas-de-higiene-matematica/)  
> Cuando un estudiante sigue un procedimiento que no nos gusta para resolver un problema debemos darnos cuenta de que es culpa nuestra… lo más normal es que no anden en sus casas haciendo ecuaciones, aplicando la ley de Ohm… Lo que han aprendido mal lo han hecho de nosotros, con mucha frecuencia. Bien, pongámonos de acuerdo y empujemos todos en la misma dirección.  
> En mi instituto (el IES Vicente Aleixandre de Pinto) nos hemos juntado los dptos. de ciencias para acordar una manera común y coherente para la resolución de problemas matemáticos. Así no despistamos a los chavales y además incidimos todos en lo correcto. Un saludo para todos mis compañeros, agradeciendo su trabajo y su buena disposición.  
> Lo compartimos con vosotros por si os sirve.  
> Buenas prácticas  
>  1. Uso de tres cifras significativas, como regla general, en los cálculos y, sobre todo en los resultados finales, acompañando el número con sus unidades correspondientes.
> ...

[El método de Pólya para resolver problemas - us.es](https://www.glc.us.es/~jalonso/vestigium/el-metodo-de-polya-para-resolver-problemas/)  
[George Pólya](http://es.wikipedia.org/wiki/George_P%C3%B3lya)  presentó en su libro Cómo plantear y resolver problemas (en inglés,  [How to solve it](http://books.google.es/books?id=z_hsbu9kyQQC&lpg=PP1&pg=PR16#v=onepage&q&f=true) ) un método de 4 pasos para resolver problemas matemáticos. Dicho método fue adaptado para resolver problemas de programación, por Simon Thompson en  [How to program it](http://www.cs.kent.ac.uk/people/staff/sjt/Haskell_craft/HowToProgIt.html).  
Paso 1: Entender el problema  
Paso 2: Configurar un plan  
Paso 3: Ejecutar el plan  
Paso 4: Examinar la solución obtenida  

[Cuadernos de Investigación y Formación en Educación Matemática. 2008, Resolución de problemas, conceptos básicos](https://revistas.ucr.ac.cr/index.php/cifem/issue/view/761)  

[CUADERNOS DE INVESTIGACIÓN Y FORMACIÓN EN EDUCACIÓN MATEMÁTICA. 2006, Año 1, Número 1. RESOLUCIÓN DE PROBLEMAS. El Trabajo de Allan Schoenfeld. Hugo Barrantes](https://revistas.ucr.ac.cr/index.php/cifem/article/view/6971/6657)  

## Estrategias para problemas física
En el libro Sears Zemansky de Física Universitaria hay un índice inicial "ESTRATEGIAS PARA RESOLVER PROBLEMAS", y aparte de "Cómo resolver problemas de física", luego hay estrategias concretas para ciertos tipos.  
Un resumen breve de la estrategia general, para la que se indican 4 pasos:
   * IDENTIFICAR los conceptos pertinentes
   * PLANTEAR el problema
   * EJECUTAR la solución
   * EVALUAR la respuesta

 [Resolución de problemas, lafisicaesfacil, Enrique García de Bustos Sierra, licenciamiento no detallado](https://www.educa2.madrid.org/web/enrique.garciadebustos/lafisicaesfacil/-/visor/resolucion-de-problemas?_visor_WAR_cms_tools_backMessage=results&_visor_WAR_cms_tools_backUrl=%2Fweb%2Fenrique.garciadebustos%2Flafisicaesfacil%3Fp_p_id%3Dcommunity_content_browser_WAR_cms_tools%26p_p_lifecycle%3D1%26p_p_state%3Dmaximized%26p_p_mode%3Dview%26_community_content_browser_WAR_cms_tools_struts_action%3D%252Fcommunity_content_browser%252Fbrowser)  
 ![](https://www.educa2.madrid.org/web/educamadrid/principal/files/850260b2-306a-4182-be4c-15156bb5778e/Resoluci%C3%B3n%20de%20problemas.png)  
   
 [ Estrategias para resolver problemas de Física, Josep Lluís Rodríguez, uned - archive.org](https://web.archive.org/web/20160715115319/http://www.unedcervera.com/c3900038/estrategias/)  
 (en 2016 la última modificación es en 2003)   

## Estrategias para problemas química
Estrategias para problemas química  
[Resolución de Problemas en Química General. Metodología de Trabajo de los Alumnos. Nuñez*, María B. - Okulik*, Nora B. -Aguado*, María I. -Castro**, Eduardo A.∗ Dpto. de Química, Facultad de Agroindustrias - UNNE. - archive.org ](https://web.archive.org/web/20180417050417/http://www.unne.edu.ar/unnevieja/Web/cyt/cyt/exactas/e-048.pdf)  
  
Información extraída de la introduccioń de [química general - uah.es](http://www2.uah.es/edejesus/resumenes/QG/intro.pdf)  de [Resúmenes de Química General (1º Farmacia Plan antiguo), Ernesto de Jesús Alcañiz](http://www2.uah.es/edejesus/resumenes/QG.htm) 

>**Normas generales para la resolución de problemas (Extraídas del libro de problemas de C. J. Willis)**  
1 ¡Razonar positivamente! El alumno ha de convencerse de que el problema puede resolverlo un estudiante que tenga sus conocimientos. Evidentemente, si alguien se dice a sí mismo que el problema no tiene solución, se está complicando la vida. Es muy probable que aquello que preocupaba no intervenga en el cálculo, o pueda hallarse a partir de la información de que se dispone.  
2 Leer cuidadosamente la pregunta. Asombra saber los puntos que los estudiantes pierden en los exámenes por no haber leído la pregunta. No resulta útil buscar complicaciones calculando algo que no se pedía; además, no dan ningún punto por eso.  
3 Recordar las definiciones. Si un químico escribe: «La disolución tenía una concentración 1,85 molar» está diciendo que cada litro contenía 1,85 moles de soluto. Para comprender la frase hay que entender los términos «disolución», «mol» y «soluto». Sin una comprensión inmediata de los términos fundamentales no cabe esperar resolver ningún problema.  
4 Escribir una ecuación ajustada para cualquier reacción que se plantee. Para cualquier cálculo que implique una reacción química, se escribirá su ecuación, la cual suministra un gran volumen de información. Pero hay que asegurarse de que la ecuación está ajustada, porque el no hacerlo llevará con frecuencia a una respuesta incorrecta.  
5 Identificar lo que se pregunta. Se ha de releer la pregunta y determinar qué es lo que piden concretamente que se calcule. Se establecerá un símbolo para representar la magnitud a calcular. A veces el símbolo es evidente (V para volumen, T para temperatura, etc.). Otras veces se utilizarán símbolos generales como x, y, etc. Siempre se escribirá de modo claro lo que significa cada símbolo.  
6 Plantear una ecuación que permita calcular la incógnita. Evidentemente, esta etapa es clave en la resolución de un problema, y lo único que puede ayudar a hacerlo correctamente es la práctica. Una técnica sencilla y a menudo útil para plantear una ecuación es invertir el problema. Muchos estudiantes calculan con suma facilidad, por ejemplo, la molaridad a partir de la concentración en gramos por litro y la masa molar, pero son incapaces de calcular la masa molar a partir de la concentración en gramos por litro y la molaridad. Sin embargo, si se piensa un sólo instante se comprende que ambos son el mismo problema. El primero se resuelve a partir de la relación Molaridad = concentración en gramos por litro/Masa molar, mientras que el segundo sólo precisa de lareordenación de la relación anterior: Masa Molar = concentración en gramos por litro/Molaridad.  
7 Reducir el número de incógnitas de la ecuación. Si se llega a una ecuación con más de una incógnita será necesario buscar más información en los datos iniciales, en la ecuación química, etc.  
8 Aplicar los valores numéricos de las constantes, etc., y efectuar las operaciones necesarias para resolver la ecuación. Muchos problemas se complican al final debido a simples errores aritméticos, que, en gran parte, se deben a descuidos. Hay que conocer perfectamente las características de la calculadora usada y poner especial cuidado en las cifras significativas (ver siguiente apartado).  
9 Leer la respuesta. ¿Es razonable? Algunas veces no habrá forma de saber si la respuesta es correcta o errónea, pero muy a menudo se puede comprobar. Una masa molecular de 0,33 o un contenido en carbono del 170% son resultados imposibles. Es posible cerciorarse de si la respuesta concuerda con los datos iniciales o satisface la ecuación.  
10 ¡Leer la pregunta otra vez! ¿Se ha contestado por completo? ¿Se calculó realmente la magnitud que se pedía? ¿Había más de un apartado en el problema? ¿Se utilizaron todos los datos? (Si no se emplearon es necesario comprobar que no se necesitaban) ¿Se ha expresado la respuesta en las unidades apropiadas?¿Se han escrito las unidades al lado de las respuestas numéricas?   

(Donde se indica "ver siguiente apartado" se coloca en página aparte asociada a  [cifras significativas](/home/recursos/recursos-cifras-significativas) )   
