
# Movimiento ondulatorio

##  Recursos generales
De momento algunos enlaces separados de  [movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio) , aunque muchos coincidirán  
Ver  [apuntes de elaboración propia para física 2º bachillerato](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato)  

Asociado al movimiento ondulatorio está el sonido (para el que se pueden poner recursos separados) y  [recursos asociados al espectro](/home/recursos/recursos-espectro).  

[Ondas](http://www.catfisica.com/08ones/08ones.htm) Ondas  
[Sonido](http://www.catfisica.com/11so/11so.htm)  Sonido y ondas estacionarias  
Página libre de derechos de autor. Organizada por bloques (cinemática, dinámica...) Se pueden imprimir páginas de problemas de 2º de Bachillerato hechos con flash y páginas de teoría y problemas en pdf. Autor: Josep Maria Domènech i Roca. IES Ramon Muntaner. Figueres (Alt Empordà).  

[Vibraciones y ondas](http://sociedadelainformacion.com/departfqtobarra/ondas/index.htm)  
Jesús Ruiz Felipe  
Profesor de Física IES Cristóbal Pérez Pastor Tobarra (Albacete)   

[Ondas. Unidad completa.  1er Premio CNICE 2004. 1er Premio Educared 2006.](http://fisquiweb.es/MovOnd/IniOndas.htm)  
Incluye parte de "óptica física" según mi clasificación personal.  

[Las ondas sonoras](http://iesfgcza.educa.aragon.es/depart/fisicaquimica/fisicasegundo/ondas.htm) Recopilación recursos, animaciones, ... ondas y fenómenos ondulatoriosLuis Ortiz de Orruño Las ondas sonoras [ondas.htm](http://www.xtec.cat/centres/a8019411/caixa/ondas.htm)  

fisica2spp WIKI educativa para alumn@s de FISICA 2º Bachillerato  
[https://fisica2spp.wikispaces.com/UNIDAD+II](http://web.archive.org/web/20120607180758/http://fisica2spp.wikispaces.com/UNIDAD+II)  (waybackmachine, no operativo en 2021)  
Profesor: Crescencio Pascual Sanz, Colegio San Pedro Pascual  
Profesor: Juan José Serrano Pérez, Instituto de Ciencia Molecular, Universitat de València  
Licenciamiento cc-by-nc-sa   

[http://jair.lab.fi.uva.es/~manugon3/temas/ondas/principal.htm](http://web.archive.org/web/20130714123148/http://jair.lab.fi.uva.es/~manugon3/temas/ondas/principal.htm)  (waybakmachine, no operativo en 2021)  
En 2013 indica Fecha de la última modificación: 12/01/01  

[http://jair.lab.fi.uva.es/~manugon3/](http://web.archive.org/web/20050208082623/http://jair.lab.fi.uva.es/~manugon3/) (waybackmachine, no operativo en 2021)  
Esta es la (Septiembre 2013) Página provisional donde se encuentra información relativa a la docencia impartida por el Departamento de Física Aplicada (DTFA) de la Universidad de Valladolid (UVA) en la ETS de Ingeniería Informática.  


###  El signo delante de kx en la ecuación de onda
Es algo que se suele aprender mecánicamente sin razonar, y cuando se intenta ponerle sentido surgen dudas interesantes.Lo primero (creo que error habitual en bachillerato) es aclarar que es un signo delante de kx, no que k sea negativa (k SIEMPRE es positiva, como la longitud de onda)  
En mis apuntes indico  
*"Realmente signo delante kx decide sentido según sea el signo delante de ωt. La idea es que tiempo siempre es positivo, y siendo ωt positivo en expresión habitual, hace falta término kx “negativo” para que según avance x se tenga la misma fase que cuando avanza t. También se podrían utilizar relaciones trigonométricas transformar expresión onda con - delante ωt para ver expresión equivalente con signo +."*

### Sonic boom  

El problema del estampido sónico - Katerina Kaouri - TED-Ed  
 [![](https://img.youtube.com/vi/JO4_VHM69oI/0.jpg)](https://www.youtube.com/watch?v=JO4_VHM69oI "El problema del estampido sónico - Katerina Kaouri - TED-Ed")  

###  Radar
Al hablar de RADAR (RAdio Detection And Ranging, “detección y medición de distancias por radio”) se asume radio: a veces se habla de "radar de sonido" pero no es correcto, sería sonar o dispositivos que miden velocidad usando sonido, pero no radar.  
[Radares de tráfico basados en el sonido](https://www.xataka.com/otros/radares-de-trafico-basados-en-el-sonido) 

###  Decibelios
dB Math  
Training materials for wireless trainers  
The abdus salam International Centre for Theoretical Physics  
[Training materials for wireless trainers -  ppt video online download](http://slideplayer.com/amp/7091993/)  
Ejercicios decibelios: ver   
[Ejercicios elaboración propia Física 2º Bachillerato](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato) 

###  Interferencias
UAH. I.T.Telecomunicación. Fundamentos de Física II. Aspectos teóricos sobre interferencias,  

[Interferencias.pdf](http://www3.uah.es/mars/FFII/Interferencias.pdf)  
[1233493282711384069](https://twitter.com/MrJoeMilliano/status/1233493282711384069)  
Love this @Perimeter idea for relating light double slit interference patterns to constructive and destructive interference for waves. Simple, clear, and concrete. #iteachphysicsTerminale S - TP Maquette sur le principe des fentes d' Young  

[http://physicus.free.fr/terminale_S/terminale-S-TP-maquette-fentes-young.php](http://physicus.free.fr/terminale_S/terminale-S-TP-maquette-fentes-young.php) 

[![TS-TP-maquette-fentes-young-video.mpeg](http://img.youtube.com/vi/3e1e57cMg-4/0.jpg)](http://www.youtube.com/watch?v=3e1e57cMg-4 "TS-TP-maquette-fentes-young-video.mpeg")

###  Ondas estacionarias
 [Ondas estacionarias](http://fisquiweb.es/Videos/OndasEstacionarias/index.htm)  

 [Travelling Waves Laboratory](http://www.animations.physics.unsw.edu.au/labs/travelling_waves/waves_lab.html)  
 School of Physics, Sydney, cc-by-nc-nd  

 [Harmonic - Wikipedia](http://en.wikipedia.org/wiki/Harmonic#Harmonics_and_overtones)  
 Contiene imágenes animadas de ondas estacionarias y su representación molecular  
 
 Vibrating String  
 [Standing Waves on a String](http://hyperphysics.phy-astr.gsu.edu/hbase/waves/string.html)   
 Hyperphysics (© C. R. Nave, 2010)  
 
 [https://twitter.com/orbitalaika_tve/status/949980932882292738](https://twitter.com/orbitalaika_tve/status/949980932882292738)  
 Visualizando las vibraciones en las cuerdas de una guitarra gracias al efecto "rolling shutter" de los sensores CMOS presentes en la inmensa mayoría de cámaras de fotos y vídeo  
 Ojo: no muestra la vibración real a cámara lenta, el rolling shutter es un efecto, ver a partir de minuto 4 para la guitarra. Está relacionado con que oscile y cómo se oriente la cámara y su sensor CMOS  
 
 [![Why Do Cameras Do This? | Rolling Shutter Explained  - Smarter Every Day 172 - YouTube](https://img.youtube.com/vi/dNVtMmLlnoE/0.jpg)](https://www.youtube.com/watch?v=dNVtMmLlnoE) 
 
 [The Physics of Playing a Guitar Visualized: Metallica’s &quot;Nothing Else Matters” Viewed from Inside the Guitar |  Open Culture](https://www.openculture.com/?p=1027818) 

[381 Ondas con un cepillo de dientes eléctrico - fq-experimentos.blogspot.com](https://fq-experimentos.blogspot.com/2016/04/381-ondas-con-un-cepillo-de-dientes.html)  


###  Ecolocalización
 [Ecolocalización - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Ecolocalizaci%C3%B3n)  
 
 [Ecolocalización en Cetáceos, Jesús Vicente Poyato Jiménez](http://www.encuentros.uma.es/encuentros81/ecolocalizacion.html) 

###  Olas del mar

[Oceanography: waves. theory and principles of waves, how they work and what causes them by Dr J Floor Anthoni 2000](http://www.seafriends.org.nz/oceano/waves.htm)   

[Deep water wave wikimedia.org](https://commons.wikimedia.org/wiki/File:Deep_water_wave.gif)  
![](https://upload.wikimedia.org/wikipedia/commons/4/4a/Deep_water_wave.gif)  

###  Efecto Doppler
 [Efecto Doppler](http://laplace.us.es/wiki/index.php/Efecto_Doppler)  
 Laplace, cc-by-nc-sa, wiki del Departamento de Física Aplicada III, en la Escuela Técnica Superior de Ingenieros de la Universidad de Sevilla.  
 
[Physclips - The Doppler Effect](http://www.animations.physics.unsw.edu.au/waves-sound/Doppler/index.html) School of Physics, Sydney, cc-by-nc-nd  

[Efecto Doppler - fisicalab](https://www.fisicalab.com/apartado/efecto-doppler#contenidos)  

### Ecografías

[DOI: 10.1016/S1138-3593(07)73916-3 Metodología y técnicas. Ecografía: principios físicos, ecógrafos y lenguaje ecográfico - archive.org](https://web.archive.org/web/20220617175109/https://www.elsevier.es/es-revista-medicina-familia-semergen-40-articulo-metodologia-tecnicas-ecografia-principios-fisicos-13109445)  


####  Ejercicios Doppler
Problemas de ondas. Efecto Doppler José Antonio Diego Vives, cc-by-sa  

[problemas_efecto_dopler_1.pdf](http://www.elaula.es/files/problemas_efecto_dopler_1.pdf)  
Efecto Doppler, teoría con algunos ejercicios. Jorge Luis Alfaro Solís, Facultad de Física, Pontificia Universidad Católica de Chile  

[doppler.pdf](http://www.fis.puc.cl/~jalfaro/fiz0121/clases/doppler.pdf)   

Lic. IRMA CASTIBLANCO GUTIÉRREZ [Ejercicios Resueltos Efecto Doppler Ii PDF | Efecto Doppler | Procesamiento de la señal](https://es.scribd.com/document/351225108/EJERCICIOS-RESUELTOS-EFECTO-DOPPLER-II-pdf)  

La parte asociada a Doppler relativista se ve junto a  [recursos de física relativista](/home/recursos/fisica/recursos-fisica-relativista)   

[Las ondas acústicas tienen masa efectiva negativa - La Ciencia de la Mula Francis](https://francis.naukas.com/2019/03/13/las-ondas-acusticas-tienen-masa-efectiva-negativa/) 

##  Recursos de vídeos / animaciones
Lo que Necesitas Saber sobre Ondas (al menos para Selectividad)  
[![Lo que Necesitas Saber sobre Ondas (al menos para Selectividad) - YouTube](https://img.youtube.com/vi/rKf92Vgx2ag/0.jpg)](https://www.youtube.com/watch?v=rKf92Vgx2ag)  
8 primeros minutos son solamente sombre movimiento oscilatorio, 4 restantes de ondulatorio  

[https://twitter.com/becarioenhoth/status/993068653422809088](https://twitter.com/becarioenhoth/status/993068653422809088)  
Explicando con fichas de dominó por qué las ondas sonoras o de presión circulan más rápidamente por los sólidos que por los líquidos o gases #fisicafacil  

[Waves and Sound with animations and film clips: Physclips.](http://www.animations.physics.unsw.edu.au/waves-sound/)  
© School of Physics - UNSW (Sydney, Australia). cc-by-nc-nd  

Lecciones con vídeos y animaciones [twitte bjmahillo/status/972517109917016065](https://twitter.com/bjmahillo/status/972517109917016065)  
Hace unos días enseñé a los chicos de física de 2° Bach. este vídeo:  

[twitterTapasDeCiencia/status/962408431486492672](https://twitter.com/TapasDeCiencia/status/962408431486492672)  
Es el frikismo total, pero sirve para ver la diferencia entre M.A.S. y onda. Bromeaban con que les mande un trabajo práctico de ese tipo 😏. Total, podría ser...  

 "Oleaje militar"Soldiers marching in a too perfect formation  
 [![Soldiers marching in a too perfect formation](https://img.youtube.com/vi/kZF7QC3tsJY/0.jpg)](https://www.youtube.com/watch?v=kZF7QC3tsJY "Soldiers marching in a too perfect formation")  
[twitter ApuntesCiencia/status/1345316314802491392](https://twitter.com/ApuntesCiencia/status/1345316314802491392)  
 ¡#BuenosDías! En la limpieza por #ultrasonidos las ondas de presión generan burbujas de vacío (#cavitación) que colapsan a temperaturas y presiones altísimas, arrancando la suciedad de los objetos a limpiar • vía @ScienceIsNew #VideoCiencia
 
 [![Wave tank demonstration showing the impact of coastal defences on flood risk](https://img.youtube.com/vi/3yNoy4H2Z-o/0.jpg)](https://www.youtube.com/watch?v=3yNoy4H2Z-o "Wave tank demonstration showing the impact of coastal defences on flood risk")   

[twitter fqsaja1/status/1473275835075469319](https://twitter.com/fqsaja1/status/1473275835075469319)  
Pues es una suerte trabajar con chicos de #2ºBachiller tan motivados que como trabajo extra nos regalan al laboratorio esta máquina de dibujar ondas,   
Yo no la veo menos chula que la del MIT. Y vosotros??  
[![MIT Physics Demo -- Spray Paint Oscillator](https://img.youtube.com/vi/P-Umre5Np_0/0.jpg)](https://www.youtube.com/watch?v=P-Umre5Np_0 "MIT Physics Demo -- Spray Paint Oscillator")  

[twitter GeotechTips/status/1557415694878138368](https://twitter.com/GeotechTips/status/1557415694878138368)  
Conocer de física es importante para la vida.  
(vídeo en el que se sueltan varillas largas desde un camión y la perturbación se propaga al otro extremo haciendo saltar a un hombre)

[twitter a_freeparticle/status/1656678015647248390](https://twitter.com/a_freeparticle/status/1656678015647248390)  
p h a s e   s h i f t #ITeachPhysics  
(Credit goes to my colleague Mike Maloney for creating this elegant demo)  
If you're interested in simulations of this, check out my site: http://afreeparticle.com/pulserace.html #ITeachPhysics

###  Levitación / "haz tractor"
Manipulation of levitated objects using holographic acoustic elements 
[![](https://img.youtube.com/vi/Do94PgoeBQ4/0.jpg)](https://www.youtube.com/watch?v=Do94PgoeBQ4 "Manipulation of levitated objects using holographic acoustic elements")  
Levitación acústica en 3 ejes  
[![](https://img.youtube.com/vi/1sdUnIvit7U/0.jpg)](https://www.youtube.com/watch?v=1sdUnIvit7U "Levitación acústica en 3 ejes")  
Levitación Acústica en ULTRA SLOW MOTION   
[![](https://img.youtube.com/vi/pdfQmIGJJn4/0.jpg)](https://www.youtube.com/watch?v=pdfQmIGJJn4 "Levitación Acústica en ULTRA SLOW MOTION ")   
Levitation with Sound | Acoustic Levitation Explained   
[![](https://img.youtube.com/vi/GkbZU4Z91Tk/0.jpg)](https://www.youtube.com/watch?v=GkbZU4Z91Tk "Levitation with Sound | Acoustic Levitation Explained") 
 
### Litotricia

[Litotricia: tratamiento de las piedras del rinón - asssa.es](https://www.asssa.es/blog-entrada/litotricia-tratamiento-de-las-piedras-del-rinon-/)  
> El tratamiento con la popular “bañera” hace referencia a la litotricia extracorpórea por ondas de choque. Inicialmente el paciente se sumergía en una bañera cuya agua hacía de conductor de la energía vibratoria de las ondas de choque. Hoy la bañera se ha sustituido por una almohadilla de líquido conductor.   

[How does Lithotripsy Work? - gls-lithotripsy.com _waybackmachine_](https://web.archive.org/web/20100727043505/http://www.gls-lithotripsy.com/Howdoes.html)  
![](https://web.archive.org/web/20110711102030im_/http://www.gls-lithotripsy.com/Images/shockwave.gif)  

[Litotricia extracorpórea por ondas de choque - wikipedia.org](https://es.wikipedia.org/wiki/Litotricia_extracorp%C3%B3rea_por_ondas_de_choque)  

[Futuro de la litotricia extracorpórea por ondas de choque - revista.uromadrid.es](https://revista.uromadrid.es/futuro-de-la-litotricia-extracorporea-por-ondas-de-choque/)  
> La burst wave lithotripsy (BWL) podría convertirse en el futuro tratamiento de la litiasis sustituyendo a la litotricia extracorpórea por ondas de choque tal como la conocemos.  


[Shock Wave Therapy - shockwavetherapy.education](https://www.shockwavetherapy.education/)  
Enlaza fabricantes   
[Shockwave machine manufacturers](https://www.shockwavetherapy.education/index.php/j-stuff/manufacturer-links)  

[Kidney Stone Treatment: Shock Wave Lithotripsy - kidney.org](https://www.kidney.org/atoz/content/kidneystones_shockwave)  

[The Physics of Shock Wave Lithotripsy - imop.gr](https://www.imop.gr/sites/default/files/ch038.pdf)  
Robin O. Cleveland, PhD  
James A. McAteer, PhD  

> Figure 38-1B shows the amplitude spectrum 
of the shock pulse (that is, it displays the different 
frequency components in the pulse). We see that 
a lithotriptor shock wave does not have a dominant frequency or tone, but rather its energy is 
spread over a very large frequency range —this is a characteristic feature of a short pulse. 
It can be  seen that most of the energy in the shock wave is between 100 kHz and 1 MHz. 
This means that it is unlikely that a lithotriptor breaks kidney stones 
by exciting its  resonance — as  an  opera  singer might shatter a crystal glass.  

> SUMMARY 
Shock wave lithotripsy is a superb example of the
successful  transition  of  engineering  technology 
into the clinical area. We have outlined the underlying acoustic principles that describe (1) the generation of the shock pulse, (2)  focusing, (3) nonlinear distortion, (4) coupling of the shock source to 
the body, and (5) absorption of sound by the body.
The exact mechanisms by which shock waves can 
damage stones and tissue are still not fully understood, although it is likely that direct stresses and 
cavitation  are  dominant  in  stone  fragmentation, 
and  that  cavitation  is  dominant  in  tissue  injury. 
Improvements  in  lithotripsy,  whether  through 
improved  use  of  existing  lithotriptors  or  through
the development of new technologies, are likely to 
come only from an improved understanding of the
acoustics and the physics of this problem.  

## Erupción Hunga Tonga  

[![](https://img.youtube.com/vi/AcFropu7uWw/0.jpg)](https://www.youtube.com/watch?v=AcFropu7uWw "Hunga Tonga volcano eruption in 4K UHD with Shock Wave propagation") 

[twitter ecosdelfuturo/status/1484622041764581380](https://twitter.com/ecosdelfuturo/status/1484622041764581380)  
Otro ej. de la dificultad de la física aparentemente más básica. Tengo exactamente las mismas preguntas después de ver a gente hablando de onda de choque, ondas de gravedad, etc. ¿Qué tipos de ondas creó la erupción de Tonga, cuándo, hasta dónde viajaron y a qué velocidad?  

## Vídeos / experimentos  

Laboratorio Gorila - Sonido 4: velocidad del sonido  
[![](https://img.youtube.com/vi/2x0tGGrZEGk/0.jpg)](https://www.youtube.com/watch?v=2x0tGGrZEGk "Laboratorio Gorila - Sonido 4: velocidad del sonido")  

Encontrando la velocidad del sonido con un smartphone.  
[![](https://img.youtube.com/vi/Lgeg9vNBGZU/0.jpg)](https://www.youtube.com/watch?v=Lgeg9vNBGZU "Encontrando la velocidad del sonido con un smartphone.")  

[twitter Rainmaker1973/status/1575941394291638272](https://twitter.com/Rainmaker1973/status/1575941394291638272)  
Wave propagation in granular materials includes the possibility of emerging of localized solitary waves that propagate along one space direction only, with undeformed shape  
[Exact solutions of the hierarchical Korteweg–de Vries equation of microstructured granular materials](https://www.sciencedirect.com/science/article/abs/pii/S0960077908001379)  
[Vídeo rianki7](https://www.tiktok.com/@ryanki7/video/7148072558456212741)  


[twitter GregSchwanbeck/status/1658892848660455434](https://twitter.com/GregSchwanbeck/status/1658892848660455434)  
\#Physics teachers! Holding a lighter in front of a speaker is a great way for students to visualize:  
• what's happening to the air  
• the frequency & amplitude of #sound waves, and  
• the nature of longitudinal #waves in general.  

##  Recursos de simulaciones / laboratorios virtuales / apps para móvil
Simulación ondas  
[http://www.xtec.cat/~ocasella/applets/ones/appletsol2.htm](http://www.xtec.cat/%7Eocasella/applets/ones/appletsol2.htm)  
Tavi Casellas.cc-by-nc-nd   

[Wave Propagation - Javalab simulations](https://javalab.org/en/wave_propagation_en/)  

[wave-on-a-string](https://phet.colorado.edu/en/simulation/wave-on-a-string)  (HTML5)  

PRISMA. Programa de nuevas tecnologías  
[PRISMA. El movimiento ondularorio](http://enebro.pntic.mec.es/~fmag0006/Prism400.html)  
Apuntes y applets  

[Standing Waves, Medium Fixed At Both Ends | Zona Land Education](http://zonalandeducation.com/mstm/physics/waves/standingWaves/standingWaves1/StandingWaves1.html)  
Animaciones ondas estacionarias en instrumentos musicales  

Animación donde se visualiza bien la superposición de dos ondas propagándose en direcciones opuestas  
[Principio de Superposición de ondas](http://platea.pntic.mec.es/~cpalacio/ondas2.htm)  

[Movimiento ondulatorio](http://www.sc.ehu.es/sbweb/fisica_/ondas/ondas.html)  (applet java, muestra ondas longitudinales)  
Reflexión y transmisión  
[Propagaci&oacute;n del sonido en el oc&eacute;ano](http://www.sc.ehu.es/sbweb/fisica_/ondas/reflex_trans/oceano/oceano.html) 
Curso Interactivo de Física en Internet © Ángel Franco García  
Versión HTML5  
[Descripci&oacute;n de la propagaci&oacute;n](http://www.sc.ehu.es/sbweb/fisica3/ondas/pulso/pulso.html)  
[Movimiento ondulatorio arm&oacute;nico](http://www.sc.ehu.es/sbweb/fisica3/ondas/armonico/armonico.html)  (transversal y longitudinal)  

The Applet Collection. Piano para ver frecuencia asociada  
[Applet: Piano](http://web.archive.org/web/20200506003911/http://lectureonline.cl.msu.edu/~mmp/applist/sound/sound.html) (waybackmachine, no operativo en 2021)© W. Bauer, 1999  

Se representa cómo se genera una onda armónica transversal a partir de un movimiento armónico simple. Se pueden variar la longitud de onda y la frecuencia.  
[ondas.swf](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/ondas.swf)  
Página realizada por Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM) - España.  

Se representa cómo se genera una onda armónica longitudinal en un tubo de gas. Se pueden variar la longitud de onda y la frecuencia. Se dibujan en una gráfica la posición, la velocidad y la aceleración de las partículas en función del tiempo. 
[ondaslong.swf](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/ondaslong.swf) Página realizada por Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM) - España.

###  Ondas estacionarias
Se muestra una onda estacionaria en una cuerda, cuya longitud se puede variar. Mediante los mandos, podemos elegir el armónico que se dibuja y variar también la velocidad de propagación de la onda.  
[estacionarias.swf](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/estacionarias.swf)  
Página realizada por Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM) - España.  

[Modos de vibraci&oacute;n de una cuerda](http://www.sc.ehu.es/sbweb/fisica_/ondas/movimiento/estacionarias/estacionarias_lab.html)  
Curso Interactivo de Física en Internet © Ángel Franco García  
Laboratorio interactivo con vídeo  

Onda estacionaria como interferencia de onda incidente y reflejada  
[Standing wave (explanation by superposition with the reflected wave)](http://www.walter-fendt.de/html5/phen/standingwavereflection_en.htm)  (HTML5, inglés)  
[standinglongitudinalwaves_es.htm](http://www.walter-fendt.de/ph6es/standinglongitudinalwaves_es.htm)  (java, español)  
Standing Wave (Explanation by Superposition with the Reflected Wave)  
[standingwavereflection_en.htm](http://www.walter-fendt.de/ph6en/standingwavereflection_en.htm)  

[Física con ordenador. Ondas estacionarias](http://www.sc.ehu.es/sbweb/fisica/ondas/estacionarias/estacionarias.html)  
Material con derechos de autor. © Ángel Franco García -1998-2011

###  Doppler
Ejemplo de efecto Doppler. Ambulancia y persona [dopplereffect_es.htm](https://www.walter-fendt.de/html5/phes/dopplereffect_es.htm) © Walter Fendt, 25 febrero 1998 Traducción: Juan Muñoz, 1999 Última modificación: 13 diciembre 2017

###  Interferencias
Se dibujan dos ondas armónicas de igual frecuencia y longitud de onda y la onda resultante de su interferencia. Se pueden variar la diferencia de caminos desplazando uno de los focos, así como el desfase y la longitud de onda con los mandos.  
[caminos.swf](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/caminos.swf)  
Página realizada por Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM) - España.  

Interferencia dos fuentes (HTML5) [Interferencia de ondas producidas por dos fuentes (I)](http://www.sc.ehu.es/sbweb/fisica3/ondas/dosFuentes/dosFuentes.html)  

[Interferencia de dos Ondas Circulares o Esf&eacute;ricas](http://www.walter-fendt.de/html5/phes/interference_es.htm)  

[:: FisLab.net :: Tavi Casellas ::](http://www.xtec.cat/~ocasella/applets/interf/appletsol2.htm) Tavi Casellas.cc-by-nc-nd 

###  Batidos
The Applet Collection. Batidos [Applet: beats](http://web.archive.org/web/20200505180106/http://lectureonline.cl.msu.edu/~mmp/applist/beats/b.htm) (waybackmachine, no operativo en 2021) © W. Bauer, 1999

###  Decibelios
Sonómetro: medición de dB [Sonómetro : Sound Meter](https://play.google.com/store/apps/details?id=kr.sira.sound&hl=es)  
sonómetro (o SPL) aplicación es una muestra valores en decibelios por medida del ruido ambiental, displays medidos valores en dB en diversas formas.   

[Sonómetro (Sound Meter)](https://play.google.com/store/apps/details?id=com.gamebasic.decibel) 

###  Afinación instrumentos
 [Tuner - gStrings Free](https://play.google.com/store/apps/details?id=org.cohortor.gstrings)  
 gStrings is a chromatic tuner application measuring sound pitch and intensity.It will let you tune any musical instrument (violin, viola, violoncello, bass, guitar, piano, wind instruments, your own voice/singing).   
 
### Ultrasonidos para repeler
[Ultrasound mosquito repellents: Zapping the myth](https://www.bbc.com/news/magazine-20669080)   
> The misconception that mosquitoes are deterred by ultrasound has been around for nearly 40 years - at least one scientific review of an electronic repellent was published in 1974.   

[Electronic mosquito repellents for preventing mosquito bites and malaria infection (Review). Copyright © 2010 The Cochrane Collaboration. Published by John Wiley & Sons, Ltd](https://archive.lstmed.ac.uk/977/1/Electronic_mosquito_repellents.pdf)  

### Proyectos asociados a sonido

[Proyectos ingeniería de las ondas I](https://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1.htm)  
Relación de proyectos que dan ideas sobre posibilidades de tratamiento con sonido

PROYECTOS CURSO 2008-2009

[ Ruido oceánico y contaminación acústica ](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_08_09/io2/public_html)

[ Acústica arquitectónica](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_08_09/io3/public_html)

[ Tratamiento del audio digital: grabación y procesado ](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_08_09/io5/public_html)

[ Acústica arquitectónica](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_08_09/io6/public_html)

PROYECTOS CURSO 2007-2008

[ Ultrasonidos en Medicina ](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_07_08/io3/public_html)

[ DiseÃ±o y construcción de un preamplificador de micrófono](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_07_08/io4/public_html)

[ Codificación digital ](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_07_08/io5/public_html)

[ Acústica arquitectónica](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_07_08/io6/public_html)

PROYECTOS CURSO 2006-2007

[ Psicoacústica](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_06_07/io1/public_html)

[ La audición](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_06_07/io2/public_html)

[ Sismología ](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_06_07/io3/public_html)

[ Relaciones entre la música y las matemáticas](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_06_07/io5/public_html)

[ La terapia del sonido](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_06_07/io6/public_html)

[ Sondas batimétricas y cartografía submarina](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_06_07/io7/public_html)

PROYECTOS CURSO 2005-2006

[ Ultrasonidos: ecografías 2D, 3D y 4D](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_05_06/io1/public_html)

[ Acústica musical: fundamentos físicos de los instrumentos musicales](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_05_06/io2/public_html)

[ Análisis y síntesis de la voz ](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_05_06/io3/public_html)

[ Insonorización y control de ruido](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_05_06/io7/public_html)

[ Aislamiento acústico y control de ruido](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_05_06/io6/public_html)

[ Escucha 3D y holofonía](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_05_06/io5/public_html)

[ Ruidos y vibraciones en los sistemas de ventilación](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_05_06/io8/public_html)

[ La focalizacióon del sonido:Tecnología HyperSonicSound (HSS)](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_05_06/io4/public_html)

PROYECTOS CURSO 2004-2005

[ La acústica del piano](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_04_05/io4/public_html)

[ Aplicaciones de la reflexión del sonido](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_04_05/io9/public_html)

[ La audición humana](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_04_05/io1/public_html)

[ La audición y la voz en los seres vivos](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_04_05/io5/public_html)

[ Ultrasonidos en medicina ](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_04_05/io2/public_html)

[ Acústica arquitectónica](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_04_05/io6/public_html)

[ Sonido y ruido](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_04_05/io7/public_html)

[ La música electrónica ](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_04_05/io3/public_html)

[ Grabación y Reproducción de Sonido](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_04_05/io8/public_html)

PROYECTOS CURSO 2003-2004

[ Infrasonidos y ultrasonidos](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_03_04/infra_y_ultra/)

[ Sonificación](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_03_04/sonificacion/cabroa.html)

[ Reproducción y grabación](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_03_04/reprod_y_grab/)

[ Fenómenos acústicos a través del Csound](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_03_04/Csound)

[ Acústica subacuática](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_03_04/subacuatica)

[ Contaminación acústica](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_03_04/contaminacion)

PROYECTOS CURSO 2002-2003

[ Micrófonos y altavoces](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_02_03/micros_altavoces/index.htm)

[ Acustica arquitectonica](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_02_03/Acustica_arquitectonica/Acustica_arquitectonica.html)

[ La guitarra electronica](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_02_03/Guitarra/index.htm)

[ Reconocimiento automatica de la voz](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_02_03/Recon_voz/index.html)

[ Sonido musical](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_02_03/Sonido_musical/sonido_musical.htm)

PROYECTOS CURSO 2001-2002

[Estudios de Grabacion](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_01_02/estudios_de_grabacion/trabajo_io.html)

[Tarjetas Adquisicion y Procesado de Audio](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_01_02/tarjetas_adquisicion_proc_audio/index.html)

[Formatos de Audio Digital](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_01_02/formatos_audio_digital/index.htm)

[Digitalizacion y Compresion de Audio](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_01_02/digitalizacion_compresion_audio/index.html)

[Equipos Mezcla de Sonidos](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_01_02/equipos_mezcla_sonidos/principal.htm)

[Cine en Casa](http://www.lpi.tel.uva.es/~nacho/docencia/ing_ond_1/trabajos_01_02/cine_en_casa/index.html)
 
 
