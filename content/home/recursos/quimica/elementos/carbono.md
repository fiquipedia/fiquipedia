
# Carbono

El carbono enlaza con [química Orgánica](/home/recursos/quimica/quimica-organica), pero también con ideas comentadas en el proyecto [Formas de carbono](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/proyectos-de-investigacion/FQ1-ProyectoInvestigacionFormasCarbono.pdf)  donde se citan 

Grafito  

Diamante  

[Fullereno - wikipedia](https://es.wikipedia.org/wiki/Fullereno)    
Fullereno C 60  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Fullerene-C60.png/220px-Fullerene-C60.png)  
Fullereno C 540  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Fullerene_c540.png/250px-Fullerene_c540.png)  

Nanotubos  

Fibra de carbono  

[6. Formación de estructura grafítica en fibras de carbono mediante la pirólisis controlada de poli(acrilonitrilo). - researchgate](https://www.researchgate.net/figure/Figura-36-Formacion-de-estructura-grafitica-en-fibras-de-carbono-mediante-la-pirolisis_fig5_39352525)  
![](https://www.researchgate.net/profile/Juan-De-Damborenea/publication/39352525/figure/fig5/AS:669445668229125@1536619693634/Figura-36-Formacion-de-estructura-grafitica-en-fibras-de-carbono-mediante-la-pirolisis.png)  


