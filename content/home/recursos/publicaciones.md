
# Publicaciones

Publicaciones física  
 [Feria Madrid por la Ciencia y la Innovación | madrimasd](http://www.madrimasd.org/cienciaysociedad/feria/publicaciones/listado.asp?feria=6&area=3)   
Copyright© 2006 - 2011. Fundación para el Conocimiento madri+d.  
  
Physics Education  
 [ShieldSquare Captcha](http://iopscience.iop.org/0031-9120/)   
Physics Education is the international journal for everyone involved with the teaching of physics in schools and colleges. The articles reflect the needs and interests of secondary school teachers, teacher trainers and those involved with courses up to introductory undergraduate level.  
Comentarios sobre licenciamiento:  [ShieldSquare Captcha](http://iopscience.iop.org/page/copyright)   

