
# Recursos campo magnético

Ver apuntes bloque electromagnetismo en  [recursos de física de 2º de Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato)   
Ver también recursos asociados en  [apuntes](/home/recursos/apuntes)  y  [simulaciones](/home/recursos/simulaciones) Ver recursos asociados a  [producto vectorial](/home/recursos/recursos-matematicas/calculo-vectorial)   
Aunque se hable de campo magnético más asociado a Bachillerato, se incluyen también ideas de "magnetismo e imanes" en general que se trata en ESO  
De momento coloco aquí junto, aunque puede que vaya separando, por ejemplo lo asociado a imanes o generación de electricidad.

## Recursos generales
Curso Interactivo de Física en Internet © Ángel Franco García  
Electromagnetismo  [Electromagnetismo](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/elecmagnet.html)   
Campo magnético  [Electromagnetismo](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/elecmagnet.html#fuerzas)   
Además de ser un curso Incluye simulaciones que se incluyen el el apartado de simulaciones de esta página  
Campo Magnético 2º Bachillerato  
 [Campo magntico](http://recursostic.educacion.es/newton/web/materiales_didacticos/campmag/index.html)   
José Luis San Emeterio, cc-by-nc-sa  

[Magnetismo en el Aula. Comunidad de Madrid. CONSEJERÍA DE EDUCACIÓN. Dirección General de Ordenación Académica. Material didáctico para profesores de Educación Infantil y Primaria. Depósito Legal: M-17.257-2006 I.S.B.N.: 84-451-2837-X](http://www.madrid.org/bvirtual/BVCM001624.pdf)  

  
Universidad de Antioquia  
Magnetismo   
 [Capitulos](http://docencia.udea.edu.co/regionalizacion/irs-404/contenido/capitulo8.html) Fuentes de campo magnético  [Ejercicios](http://docencia.udea.edu.co/regionalizacion/irs-404/contenido/capitulo9.html)   
Ley de Faraday  [Ejercicios](http://docencia.udea.edu.co/regionalizacion/irs-404/contenido/capitulo10.html)   
Integrated Magnetics  
Magnet Frequently Asked Questions  
 [Magnets & Magnetism Frequently Asked Questions | Magnet FAQs](http://www.intemag.com/faqs.html)   

 [Usando el mando de la PlayStation para acordarse de la regla de la mano derecha.](http://www.cienciaonline.com/2013/11/24/usando-el-mando-de-la-playstation-para-acordarse-de-la-regla-de-la-mano-derecha/)   

Pendiente de buscar un meme sobre la regla de la mano derecha; la idea es que sale una mujer preguntando a un hombre "¿Qué sabes hacer con los dedos?", y el hombre responde "espera", y luego aparece la imagen de la mano con los dedos indicando en cada uno los valores de campo, velocidad y fuerza de la regla de la mano derecha 
  
 [magnet_portada.html](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/magnet/magnet_portada.html)   
  
 [Cyberphysics - The motor effect](http://www.cyberphysics.co.uk/topics/magnetsm/electro/Motor%20Effect.htm)   
Incluye regla mano izquierda de Fleming para motores "Fleming's Left Hand Motor Rule"  
  
San José State University. Magnetic Field  
 [Department of Physics and Astronomy | San Jose State University](http://www.physics.sjsu.edu/becker/physics51/mag_field.htm)   
Gráficos bien elaborados con copyright de Addison Wesley Longman Inc.  
  
Electronics Tutorial about Electromagnetism  
 [electromagnetism.html](http://www.electronics-tutorials.ws/electromagnetism/electromagnetism.html)   
Copyright © 1999 − 2013, Electronics-Tutorials.ws, All Right Reserved.   
  
 [campo-magnetico-creado-por-una-espira.html](http://elfisicoloco.blogspot.com.es/2013/02/campo-magnetico-creado-por-una-espira.html)   
  
 [movement_of_charged_particles.html](http://magnetic-field.99k.org/movement_of_charged_particles.html)   
Animación movimiento helicoidal  
  
 [OpenStax CNX](http://cnx.org/content/m31345/latest/)   
Motion of a charged particle in magnetic field, cc-by-sa  
  
 [Magnetismo5.htm](http://intercentres.edu.gva.es/iesleonardodavinci/Fisica/Magnetismo/Magnetismo5.htm)   
Comentario efecto Zeeman, enlaza con  [espectro](/home/recursos/recursos-espectro)   
  
 [First and Second Right-Hand Rules.swf](http://glencoe.com/sec/science/physics/ppp_09/animation/Chapter%2024/First%20and%20Second%20Right-Hand%20Rules.swf)   
  
IMANES Y ELECTROMAGNETISMO: APLICACIONES TECNOLÓGICAS (y 2)  
 [![ - YouTube](https://img.youtube.com/vi/kYU7lZHmwEo/0.jpg)](https://www.youtube.com/watch?v=kYU7lZHmwEo)   
En minuto 39 aparece la "levitación diamagnética de una rana"!  
  
A treatise on electricity and magnetism (1873)  
 [A treatise on electricity and magnetism : Maxwell, James Clerk, 1831-1879 : Free Download, Borrow, and Streaming : Internet Archive](https://archive.org/details/electricandmagne01maxwrich)   
  
 [camag.htm](http://iesfgcza.educa.aragon.es/depart/fisicaquimica/fisicasegundo/camag.htm)   
Recursos, animaciones  
Luis Ortiz de Orruño   
  
Curiosidad "La cosa" [“La cosa&quot; era un ingenioso micrófono ruso (1945) - La Aldea Irreductible](http://irreductible.naukas.com/2009/10/16/la-cosa-era-un-ingenioso-microfono-ruso-1945/)  [The Thing (listening device) - Wikipedia](https://en.wikipedia.org/wiki/The_Thing_(listening_device))   
Cómo funcionaba “La cosa”  [Fuente](http://www.audiodesignline.com/howto/173602214)   
 En realidad, “La cosa” utilizaba la  [inducción electromagnética](http://es.wikipedia.org/wiki/Inducci%C3%B3n_electromagn%C3%A9tica)  para transmitir una señal de audio, siendo activada a voluntad por las ondas electromagnéticas (microondas) de una fuente externa al edificio (una furgoneta aparcada frente a la embajada), lo que hacía que fuese casi imposible de detectar cuando no estaba activada.  
 
 [Ley de Lorentz - fisicalab.com](https://www.fisicalab.com/apartado/ley-de-lorentz)  
 
 [2º BACHILLERATO CAMPO MAGNÉTICO (2ª parte, Ampère e inducción). Hojas resumen / cheat sheet](https://drive.google.com/file/d/1-n7EA69Fc5m_PVMHyXamDUjl8lyYCL4x/view)  

### Ciclotrón
Enlaza con física nuclear por producción de isótopos  
[Cyclotron](http://hyperphysics.phy-astr.gsu.edu/hbasees/magnetic/cyclot.html)   
 [El primer acelerador de partículas; el ciclotrón de Lawrence. | A hombros de gigantes. Ciencia y tecnología](https://ahombrosdegigantescienciaytecnologia.wordpress.com/2015/08/08/el-primer-acelerador-de-particulas-el-ciclotron-de-lawrence/)   
 [Acelerador Ciclotrón 18/9MeV](http://cna.us.es/index.php/es/instalaciones/ciclo)  
 La descripción de los blancos dispuestos en los ocho puertos disponibles es la siguiente:  
 1.- Blanco de gran volumen (2 ml) con agua enriquecida en 18O en su interior ([18O]-H2O, pureza > 95% en 18O), para producir 18F en forma de [18F]-Fluoruro mediante la reacción 18O(p,n)18F.  
 2-3.- Dos blancos de volumen grande (2 ml), con las cavidades fabricadas en niobio y rellenos de agua enriquecida en 18O ([18O]-H2O, pureza > 95% en 18O). Están destinados también a la producción de 18F en forma de [18F]-Fluoruro mediante la reacción nuclear 18O(p,n)18F.  
4.- Blanco de 1,7 ml relleno de una mezcla agua-etanol para la obtención dentro del blanco de 13N en forma de [13N]-Amoniaco mediante la reacción 16O(p,α)13N.  
 5.- Blanco de 30 ml que contiene 18O2 y utilizado en la obtención de 18F en forma de [18F]-F2 mediante bombardeo con protones.  
 6.- Blanco gaseoso de 60 ml, en el que se bombardea una mezcla nitrógeno-oxígeno con protones para obtener 11C en forma de [11C]-CO2 mediante la reacción 14N(p,α)11C.  
 7.- Blanco de 60 ml relleno de una mezcla nitrógeno-oxígeno para la obtención de 15O en forma de [15O]-O2 mediante la reacción 14N(d,n)15O.  
 8.- En el último puerto existe una ventana de salida de haz en la que se ha instalado una línea que transporta el haz de partículas a una segunda sala blindada donde se ubica una cámara de reacción para la irradiación de materiales de interés tecnológico.  
 [technical-details](https://www.healthcare.siemens.com.co/molecular-imaging/cyclotron-chemistry-solution/eclipse-hp-cyclotron/technical-details) System Specifications
   * Energy: 11 MeV
   * Magnet: 4-sector azimuthally varying field, single strip coil, Power: 3kW , Mean Field: 12,000 Gaus (1.2 Tesla) , Valley to Hill Gap Ratio: 27:1 ° Magnetic Field @Room Boundary: < 1 Gauss (< 0.1 millitesla)
   * RF System: Four Dees, Fundamental Mode: 72MHz , Max. Amplifier Power: 10kW  
 
 Ciclotrón Características y principal funcionamiento Narración en español  
 [![](https://img.youtube.com/vi/qBLSqLbQYiw/0.jpg)](https://www.youtube.com/watch?v=qBLSqLbQYiw "Ciclotrón Características y principal funcionamiento Narración en español")  


### Espectrómetro de masas

Enlaza con isótopos que es un concepto asociado a átomo que se introduce en 3ºESO. El espectrómetro de masas se ve en Física y Química de 1º Bachillerato LOMCE.Ver  [recursos asociados al espectro](/home/recursos/recursos-espectro)   
  
Hyperphysics. Magnetic Field  
 [Magnetic field](http://hyperphysics.phy-astr.gsu.edu/hbase/magnetic/magfie.html#c1)   
Espectrómetro de masas, con buena ilustración del selector de velocidad  [Mass Spectrometer](http://hyperphysics.phy-astr.gsu.edu/hbasees/magnetic/maspec.html)   
  
 [Isotope Distribution Calculator, Mass Spec Plotter, Isotope Abundance Graphs](https://www.sisweb.com/mstools/isotope.htm)   
 
 [El espectrómetro de masas - ehu.es](http://www.sc.ehu.es/sbweb/fisica3/magnetico/espectrometro/espectrometro.html)  

### Resonancia magnética nuclear

[![](https://img.youtube.com/vi/75_xWBwBB9g/0.jpg)](https://www.youtube.com/watch?v=75_xWBwBB9g "How does an MRI machine work? - Lesics")  

### El flujo magnético cuando hay N espiras  

Como este tema me lo han comentado ya varias veces incluso profesores, además de intentar reflejarlo/mencionarlo en los apuntes de física y en las soluciones PAU (2013-Junio A. Pregunta 2 es el más reciente) cuando pueda, lo comento aquí:  
  
Si tenemos una bobina de N espiras (o N espiras apretadas) ¿depende de N el flujo y la fuerza electromotriz?  

#### Planteamientos que he recibido
A- "el número de espiras no afecta al flujo, sino que lo afecta tan solo el campo y la superficie de una espira que conforma la bobina.", por lo tanto si el campo es constante en toda la superficie de cada espira y la superficie de cada espira es constante, Φ=B·S·cos(α) siendo α el ángulo que forman los vectores B y S, pero es erróneo indicar Φ=N·B·S·cos(α)   
B- "Incluir el número de espiras para hallar el flujo, es absurdo, ya que no depende de estas pues la definición de flujo es "El número de líneas de fuerza que atraviesan la superficie S", por tanto da igual cuantas espiras tenga, el número de lineas que pasen por la primera será igual que las que pasen por la segunda."  
  
Sin embargo al tiempo que se dice eso se afirma eso se indica  
A- "No obstante el numero de espiras si que ha de tenerse en cuenta en la fuerza electromotriz que se define como el número de espiras por la derivada del flujo con respecto del tiempo"  
B- "Cabe destacar que luego para obtener la fuerza electromotriz, ahí si es necesario multiplicarlo por el número de espiras."  

#### Mi planteamiento  

Para mí es "curioso" que se diga que la N sí aparece en la fórmula de la fem pero no en la fórmula del flujo cuando ambos están directamente relacionados, y creo que es un tema de "interpetación"  
  
Si vamos al fondo del asunto ... creo que todos estaremos de acuerdo en las ecuaciones de Maxwell / Faraday:-), que simplificándolas a estos casos, se convierte de manera genérica en ε=−dΦ/dt  
A veces se cita como fórmula "habitual" ε=−N dΦ/dt, pero como hay que ir al fondo del asunto yo uso la "genérica"  
  
Ambas hablan de lo mismo, pero hay un detalle a tener en cuenta para no confundirse y tener ideas claras:  
  
Expresión1: ε=−dΦ/dt, donde ahí Φ es el flujo TOTAL por la superficie de la bobina.  
Expresión2: ε=−NdΦ/dt, donde ahí Φ es el flujo por la superficie de CADA espira de la bobina.  
  
El flujo en la expresión 1 incluye dentro el N (reordenando términos por claridad) Φ =B⋅N·S⋅cos α siendo S la superficie de CADA espira de la bobina.  
El flujo en la expresión 2 no incluye N, el N está fuera, sería Φ =B⋅S⋅cos α siendo S la superficie de CADA espira de la bobina.  
  
Al final la expresión para la fem es la misma en ambos casos, así que está la duda de si el flujo tiene la N o no ... que es lo que me comentan como "fallo".  
  
El enunciado dice explícitamente "bobina", no espira: "a) El fIujo magnético máximo que atraviesa la bobina."  
  
Creo que pensando en la propia ley de Faraday, habla del flujo en general, luego la expresión genérica ε=−dΦ/dt debe ser válida, y el flujo TOTAL de la bobina es N veces el de CADA espira, porque la bobina son N espiras.  
El tema es que la superfice TOTAL que atraviesa el campo magnético, en una bobina de N espiras, es aproximadamente N veces la superficie de CADA espira. Por eso se puede decir que Sbobina=N·Sespira, y ahí es donde sale el N en las ecuaciones.  
  
Creo que este dibujo para 3 espiras puede aclarar lo que intento reflejar.  
 [![wikipedia, cc-by, Michael Lenz](http://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Spulenflaeche.jpg/628px-Spulenflaeche.jpg "wikipedia, cc-by, Michael Lenz") ](http://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Spulenflaeche.jpg/628px-Spulenflaeche.jpg)   
 [File:Spulenflaeche.jpg - Wikipedia](http://en.wikipedia.org/wiki/File:Spulenflaeche.jpg) , cc-by Michael Lenz  
  
Más claro (inglés)  
 [Faraday's law of induction - Wikipedia](http://en.wikipedia.org/wiki/Faraday%27s_law_of_induction#Quantitative)   
  
*For a tightly wound coil of wire, composed of N identical loops, each with the same ΦB, Faraday's law of induction states that[20][21]*  
ε=−N dΦB/dt  
where N is the number of turns of wire and **ΦB is the magnetic flux in webers through a single loop.**  

### El flujo y su signo cuando la superficie es plana  

Es un tema que me han comentado varias veces, así que intento comentarlo aquí y así poder responder remitiendo a esta página.  
La definición de flujo supone una integral de un campo vectorial multiplicado escalarmente por el vector diferencial de superficie; si la magnitud vectorial es un campo, aplica a campo magnético (asociado a Ley de Faraday), pero también a gravitatorio y eléctrico (ley de Gauss)  
En los apuntes de campo eléctrico, que es cuando se introduce el concepto de flujo por primera vez, indico  
"Vector superficie S: módulo igual a área, dirección normal, y sentido hacia parte convexa superficie."  
El tema es que en una esfera que es habitual en Gauss, o en una superficie cerrada con caras planas donde sí hay una parte "exterior" parece claro el sentido del vector superficie, pero entra la duda cuando la superficie es plana, especialmente en problemas de inducción con superficies planas, por ejemplo Madrid 2018-Junio-A3 (uso ese ya que tiene sistema de referencia indicado en enunciado y así es más claro)El campo magnético tiene dirección de eje Z y sentido Z negativas.La superficie está en plano XY, de modo que vector superficie tiene dirección de eje Z ¿pero qué sentido de los dos posibles?  
a) Si se toma vector S en sentido de Z negativas, el ángulo entre campo y vector superficie será de 0º, y el flujo será positivo  
b) Si se toma vector S en sentido de Z positivas, el ángulo entre campo y vector superficie será de 180º, y el flujo será negativo.  
¿Son ambas soluciones correctas o solamente una, y por qué?  
Creo que la respuesta está en la definición y concepto de flujo: cualitativamente indica el número de líneas de campo que atraviesan la superficie, y atravesar una superficie tiene sentido. (Se puede ver el genial vídeo de QuantumFracture de la ley de Gauss)  
Si el flujo tiene signo positivo, quiere decir que en modo neto hay más líneas de campo que atraviesan la superficie a favor del sentido que hemos asignado para el vector superficie: en una esfera y campo eléctrico, con sentido de vector superficie hacia afuera al ser la parte convexa, flujo positivo es que salen más líneas de campo que entran. En una espira plana y campo magnético, flujo positivo es que salen más líneas de campo que entran respecto al sentido que hemos asignado al vector superficie. En el caso de Madrid 2018-Junio-A3 hay flujo en dos sentidos: el flujo del campo externo y el flujo del campo inducido (que se puede oponer o no), pero para Faraday usamos solo el campo externo y tenemos un único sentido de campo.En la opción a) el flujo positivo nos indica que campo va en mismo sentido que hemos elegido para vector superficie. En la opción b) el flujo negativo nos indica que campo va en sentido opuesto al que hemos elegido para vector superficie.   
Ambas respuestas dicen lo mismo y pueden ser correctas, y muestran que para que la respuesta sea completa en una superficie plana es necesario indicar el sentido del vector superficie. Lo veo análogo a decir si la gravedad es 9,8 m/s^2 ó -9,8 m/s^2: ambas pueden ser correctas en función de la referencia, que es necesario asociar junto a resultado.Lo habitual en problemas donde el campo magnético es uniforme es tomar el vector superficie de modo que forme un ángulo menor de 90º con el campo magnético, y esa elección creo que es tan natural que en resoluciones se omite citarla explícitamente (al menos yo no siempre lo hago), al igual que se suele omitir representar explícitamente el vector superficie.  
En el caso de una espira plana girando respecto a su diámetro en un campo B uniforme, el flujo va a ir alternando entre valores positivos y negativos.  

## Ejercicios
[Collection of Solved Problems in Physics - electricity-and-magnetism - physicstasks.eu ](http://physicstasks.eu/en/physics/electricity-and-magnetism)  
This collection of Solved Problems in Physics is developed by Department of Physics Education, Faculty of Mathematics and Physics, Charles University in Prague since 2006.  
The Collection contains tasks at various level in mechanics, electromagnetism, thermodynamics and optics. The tasks mostly do not contain the calculus (derivations and integrals), on the other hand they are not simple such as the use just one formula. Tasks have very detailed solution descriptions. Try to solve the task or at least some of its parts independently. You can use hints and task analysis (solution strategy).  
Difficulty level  
Level 1 – Lower secondary level   
Level 2 – Upper secondary level   
Level 3 – Advanced upper secondary level   
Level 4 – Undergraduate level    


 [induccion_problemas.html](http://www.sc.ehu.es/sbweb/fisica_//problemas/electromagnetismo/induccion/problemas/induccion_problemas.html)   
  
Estrategias para resolver problemas de campo magnético  
Josep Lluís Rodríguez  
 [estrategias_c_magnetico_1.html](http://www.unedcervera.com/c3900038/estrategias/estrategias_c_magnetico_1.html)   
 [estrategias_c_magnetico_2.html](http://www.unedcervera.com/c3900038/estrategias/estrategias_c_magnetico_2.html)   
 [estrategias_c_magnetico_3.html](http://www.unedcervera.com/c3900038/estrategias/estrategias_c_magnetico_3.html)   
(en 2016 la última modificación es en 2003)  



## Serie: Electromagnetismo (Cuaderno de Cultura Científica)  

 [Archivo de serie: Electromagnetismo — Cuaderno de Cultura Científica](https://culturacientifica.com/series/electromagnetismo/)   
[Trayectorias de las partículas cargadas en un campo magnético - culturacientifica.com](https://culturacientifica.com/2016/05/31/trayectorias-las-particulas-cargadas-campo-magnetico/)  

## Vídeos y animaciones
  
Visualización campo  
[https://twitter.com/becarioenhoth/status/993124997374906368](https://twitter.com/becarioenhoth/status/993124997374906368)   
9 Amazing Magnet Gadgets!  
[![9 Amazing Magnet Gadgets! - YouTube](https://img.youtube.com/vi/XvkEExdl-w4/0.jpg)](https://www.youtube.com/watch?v=XvkEExdl-w4)   
  
Ley de Faraday-Henry: experiencias de inducción electromagnética  UA - Universitat d'Alacant / Universidad de Alicante 
[![Ley de Faraday-Henry: experiencias de inducción electromagnética](https://img.youtube.com/vi/5E4nFAUrgMY/0.jpg)](https://www.youtube.com/watch?v=5E4nFAUrgMY "Ley de Faraday-Henry: experiencias de inducción electromagnética")  
[Ley de Faraday-Henry: Corriente eléctrica en una espira - dfists.ua.es](http://www.dfists.ua.es/experiencias_de_fisica/index14.html#video)  
[Ley de Faraday-Henry - hiru.eus](https://www.hiru.eus/es/fisica/ley-de-faraday-henry)  

[![Divergencia y rotor: el lenguaje de las ecuaciones de Maxwell, flujo de fluidos, y más](https://img.youtube.com/vi/rB83DpBJQsE/0.jpg)](https://www.youtube.com/watch?v=rB83DpBJQsE "Divergencia y rotor: el lenguaje de las ecuaciones de Maxwell, flujo de fluidos, y más")   

### MIT Physics 8.02 Electricity and Magnetism  

Gran cantidad de animaciones (no interactivas) junto a algunos applets  
Magnetostatics  
 [Magnetostatics Index](http://web.mit.edu/8.02t/www/802TEAL3D/visualizations/magnetostatics/index.htm)   
   * The magnetic field of a moving postive charge when the speed of the charge is small compared to the speed of light.  
 [8.02T &gt; ](http://web.mit.edu/8.02t/www/802TEAL3D/visualizations/magnetostatics/MovingChargePosMag/MovingChargePosMag.htm) 
Faraday's Law  
 [Faraday's Law Index](http://web.mit.edu/8.02t/www/802TEAL3D/visualizations/faraday/index.htm) 
   * Magnetic Inductance. This video and animation illustrate Faraday's Law. As a permanent magnet is moved back and forth in the vacinity of a coil of conducting wire, a current is induced in the coil (as measured by the ammeter in the video)  
 [8.02T &gt; ](http://web.mit.edu/8.02t/www/802TEAL3D/visualizations/faraday/inductance/inductance.htm) 

### Magnetismo natural
 [![MAGNETS: How Do They Work? - YouTube](https://img.youtube.com/vi/hFAOXdXZ5TM/0.jpg)](https://www.youtube.com/watch?v=hFAOXdXZ5TM) MAGNETS: How do they work?  

### Magnetismo y  [relatividad](/home/recursos/fisica/recursos-fisica-relativista) 
How Special Relativity Makes Magnets Work   
 [![How Special Relativity Makes Magnets Work - YouTube](https://img.youtube.com/vi/1TKSfAkWWN0/0.jpg)](https://www.youtube.com/watch?v=1TKSfAkWWN0)   
Vídeo de veritasium, recomendable  

### Efecto Einstein–de Haas 

[Einstein–de Haas effect - wikipedia](https://en.wikipedia.org/wiki/Einstein%E2%80%93de_Haas_effect)  

### Motores y aparatos 
 [Lesics Engineers Pvt Ltd](https://learnengineering.org/)  (apartado de máquinas eléctricas con motores)  
©2012-2013 Imajey. The content is copyrighted to LearnEngineering.org & Imajey Consulting Engineers Pvt. Ltd. and may not be reproduced without consent.Vídeos explicativos con animaciones muy claras.  
  
 [How speakers make sound - Animagraffs](https://animagraffs.com/loudspeaker/)  [How Hard Disk Drives Work - Animagraffs](https://animagraffs.com/hard-disk-drive/)   
 
 [Simple STEM / Homopolar Ghost](https://www.robives.com/project/homopolar-ghost/)  

### Generación de electricidad

[twitter Rainmaker1973/status/1533488821219622912](https://twitter.com/Rainmaker1973/status/1533488821219622912)  
F50-12V is a mini electricity generator, built with an incroporated turbine and electric generator that converts kinetic energy into electricity. The generator output is rectified and regulated for a maximum of 12V   
[F50-12V 10W DC Micro Hydro Generator - solecroshop](https://solectroshop.com/en/sensores-de-caudal/4708-sensor-de-caudal-de-agua-1-30l-min-yf-s201.html)  
[Yosoo DC generador de turbina de agua 12 V DC 10 W, herramienta hidroeléctrica - amazon](https://www.amazon.com/-/es/Yosoo-generador-turbina-herramienta-hidroel%C3%A9ctrica/dp/B00ZCBNNOC/)  


## Recursos laboratorios no virtuales
  
[Iniciación a la corriente alterna I (waybackmachine)](http://web.archive.org/web/20200802055846/http://www.heurema.com/PDF28.htm) Prácticas de Física 2º Bachillerato  
[Magnetismo. Prácticas - fisicayquimicaenflash (waybackmachine)](http://web.archive.org/web/20210727180203/http://fisicayquimicaenflash.es/autoinduccion/LAB_0.htm)   
Experiencia de Oersted  
Experiencias de Faraday Lenz"  
Ramón Flores-Martínez, cc-by-nc-nd

[twitter Hookean1/status/1543211643005550592](https://twitter.com/Hookean1/status/1543211643005550592)  
Physics teachers. This is a very cheap Lenz's Law demo. Just some magnets and catering foil! 🙂 #chatphysics #iteachphysics

[twitter fqsaja1/status/1493625851707015169](https://twitter.com/fqsaja1/status/1493625851707015169)  
Bueno... pues enseñando inventos magnéticos se me olvidaba el mal llamado motor "homopolar" 🧲 de Faraday.  
Dificultad: 0 ; Efectividad: 10.  
De nuevo, ley de LORENTZ  

[El Experimento de Oersted: Procedimientos - csic.es](https://museovirtual.csic.es/salas/magnetismo/mag8.htm)  

Ley de Faraday-Henry: experiencias de inducción electromagnética - UA  
 [![](https://img.youtube.com/vi/5E4nFAUrgMY/0.jpg)](https://www.youtube.com/watch?v=5E4nFAUrgMY "Ley de Faraday-Henry: experiencias de inducción electromagnética - UA")  

Cañón de Gauss / Gauss rifle PIE UCM 
 [![](https://img.youtube.com/vi/SiWBdv-fRMs/0.jpg)](https://www.youtube.com/watch?v=SiWBdv-fRMs "Cañón de Gauss / Gauss rifle PIE UCM")  


## Recursos laboratorios virtuales / simulaciones

   * Curso Interactivo de Física en Internet © Ángel Franco García  
Electromagnetismo  [Electromagnetismo](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/elecmagnet.html)   
Campo magnético  [Electromagnetismo](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/elecmagnet.html#fuerzas)   
Además de ser un curso Incluye varias simulaciones  
      * Fuerza magnética sobre conductor rectilíneo [Fuerza magn&eacute;tica sobre conductor rectil&iacute;neo](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/campo_magnetico/varilla/varilla.html) 
      * Fuerzas y momento sobre una espira [Fuerza y momento sobre una espira](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/campo_magnetico/momento/momento.html) 
      * El galvanómetro [El galvan&oacute;metro](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/campo_magnetico/galvanometro/galvanometro.html) 
      * Campo magnético producido por una corriente rectilínea indefinida [Campo magn&eacute;tico producido por una corriente rectil&iacute;nea indefinida](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/campo_magnetico/ampere/ampere.html) 
      * El solenoide y el toroide [El solenoide](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/campo_magnetico/magnetico/cMagnetico.html) 
Además de simulaciones incluye laboratorio con vídeo  
[Demostración de la ley de Faraday (I)](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/faraday/fem/fem_lab.html)  

   * Magnet Lab. Magnet High Magnetic Field Laboratory. Florida State Univesity - Los Alamos National Laboratory (antes 2016 en www.magnet.fsu.edu)  
 [interactive](https://nationalmaglab.org/education/magnet-academy/watch-play/interactive)   
© 1995–2013 National High Magnetic Field Laboratory  
Tutoriales interactivos, muy visuales (Concepts and Laws, Historical Instruments, Modern Instruments)  

      * Lorentz Force [Lorentz Force - MagLab](https://nationalmaglab.org/education/magnet-academy/watch-play/interactive/lorentz-force) 
      * Parallel Wires [parallel-wires](https://nationalmaglab.org/education/magnet-academy/watch-play/interactive/parallel-wires) 
      * Magnetic Field of a Solenoid [Magnetic Field of a Solenoid - MagLab](https://nationalmaglab.org/education/magnet-academy/watch-play/interactive/magnetic-field-of-a-solenoid) 
      * Magnetic Field around a Wire, I [Magnetic Field Around a Wire, I - MagLab](https://nationalmaglab.org/education/magnet-academy/watch-play/interactive/magnetic-field-around-a-wire-i) 
      * Magnetic Field around a Wire, II [Magnetic Field Around a Wire, II - MagLab](https://nationalmaglab.org/education/magnet-academy/watch-play/interactive/magnetic-field-around-a-wire-ii) 
      * Ørsted's Compass [Oersted's Compass - MagLab](https://nationalmaglab.org/education/magnet-academy/watch-play/interactive/orsted-s-compass) 

   * Electricity and Magnetism. Interactive Java Tutorials  
 [Molecular Expressions: Electricity and Magnetism - Interactive Java Tutorials](http://micro.magnet.fsu.edu/electromag/java/index.html)   
© 1995-2013 by Michael W. Davidson and The Florida State University. All Rights Reserved.  
(Relacionado con Magnet Lab, pero no son recursos idénticos, página inicialmente centrada en óptica y microscopios)  

      * Faraday's Magnetic Field Induction Experiment [Molecular Expressions: Electricity and Magnetism - Interactive Java Tutorials: Faraday's Magnetic Field Induction Experiment](http://micro.magnet.fsu.edu/electromag/java/faraday2/index.html)   

      * How A Hard Drive Works [Molecular Expressions: Electricity and Magnetism - Interactive Tutorials: How a Hard Drive Works](http://micro.magnet.fsu.edu/electromag/java/harddrive/index.html) 
      * Lenz's Law [Molecular Expressions: Electricity and Magnetism - Interactive Java Tutorials: Lenz's Law](http://micro.magnet.fsu.edu/electromag/java/lenzlaw/index.html) 
      * Transformer [Molecular Expressions: Electricity and Magnetism - Interactive Java Tutorials: Transformer](http://micro.magnet.fsu.edu/electromag/java/transformer/index.html) 

   * Animaciones de física  
 [varilla.swf](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/varilla.swf)   
Página realizada por Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM) - España.  

[Periodic table of mangetism - minutelabs.io](https://labs.minutelabs.io/PeriodicTable/#mag)  


### Movimiento imanes

   * Práctica de laboratorio. El acelerador de Gauss [El acelerador de Gauss](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/materiales/gauss/gauss.html)   
Práctica de laboratorio. Movimiento de un imán en un tubo metálico vertical  [Movimiento de un im&aacute;n en un tubo met&aacute;lico vertical](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/faraday/foucault1/foucault1_lab.html)   
Curso Interactivo de Física en Internet © Ángel Franco García  
Laboratorio interactivo con vídeo
   * Falling Magnet Applet  
 [ Falling Magnet Applet ](http://web.mit.edu/jbelcher/www/java/falling/falling.html)   
The core of our 4th order Runge-Kutta applet code is Copyright (c) 1997 by Christopher K. Oei, Incorporated. http://www.chrisoei.com; info@chrisoei.com Permission to use, display, and modify this code was granted by Christopher Oei, Inc. provided that: 1) this notice is included in its entirety; 2) Christopher Oei, Inc. is notified of the usage.   
 [ Falling Magnet Applet ](http://web.mit.edu/jbelcher/www/java/karolen/falling.html)   
Slower version of previous applet which shows the electric and magnetic vector fields of this system as the magnet falls
   * Levitating (Or Not) Magnet Applet  
 [ Levitating (Or Not) Magnet Applet ](http://web.mit.edu/jbelcher/www/java/lev_mag/lev_mag.html) 
   * Laboratorio electromagnético de Faraday  
 [faraday](http://phet.colorado.edu/es/simulation/faraday) 
   * Leyes de Faraday  
 [faradays-law](http://phet.colorado.edu/es/simulation/faradays-law)   
 
 [twitter fqsaja1/status/1662352815421894658](https://twitter.com/fqsaja1/status/1662352815421894658)  
 #demosdefísica #PhysicsDemos Ley de Lenz, Corrientes de Foucault...como prefiráis llamarlo. Un péndulo de Nd se enfrenta a una pletina de 3 kg de Cu. Las corrientes inducidas son tales que lo frenan de manera drástica incluso si es el Cu el que se mueve I❤️Physics #IteachPhysics  
 Comparamos el movimiento de rodadura y de deslizamiento de dos piezas cilíndricas similares sobre un plano inclinado. Una de Nd, otra de Al.Qué pena no poder compartir el tacto sedoso que transmite el Nd al acercarlo al Cu. Si os gusta, adaptáis o comentáis, por aquí nos vemos  


### Movimiento de cargas
[pInMagneticField.swf](http://www.kcvs.ca/site/projects/physics_files/particleMField/pInMagneticField.swf)   
The King's Centre for Visualizatión in Science (KCVS)  

[ciclotron.swf - acer.forestales.upm.es](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/ciclotron.swf)  
[thomson.swf](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/thomson.swf) 

#### Ciclotrón  

   * A cyclotron  
 [Cyclotron](http://physics.bu.edu/~duffy/HTML5/cyclotron.html)   
Andrew Duffy cc-by-sa
   * Cyclotron Simulator JavaScript Simulation Applet HTML5  
 [Cyclotron Simulator JavaScript Simulation Applet HTML5 - Open Educational Resources / Open Source Physics @ Singapore ](http://iwant2study.org/ospsg/index.php/interactive-resources/physics/05-electricity-and-magnetism/08-electromagnetism/706-chargeinnscyclotronwee#faqnoanchor) 
   *  [Cyclotron.swf](http://www.kcvs.ca/site/projects/physics_files/newCyclotron/Cyclotron.swf) 
   * Ciclotron  
 [Cyclotr&oacute;n java applet](http://teleformacion.edu.aytolacoruna.es/FISICA/document/applets/Hwang/ntnujava/cyclotron/cyclotron_s.htm)   
(Url mirror en España) Autor: Fu-Kwun Hwang, Dept. of physics, National Taiwan Normal University  
Traducción: José Villasuso Gato  

   * Cyclotron EdukiteLearning [![cyclotron - YouTube](https://img.youtube.com/vi/cNnNM2ZqIsc/0.jpg)](https://www.youtube.com/watch?v=cNnNM2ZqIsc)  (4 min)
   * Principle and Working of Cyclotron vnperceptions [![Principle and Working of Cyclotron - YouTube](https://img.youtube.com/vi/m2jp0klZHEE/0.jpg)](https://www.youtube.com/watch?v=m2jp0klZHEE)  (6 min)
   * Ciclotron 1 Susana De La Salud Quiles Casado [media UPV](https://media.upv.es/#/portal/video/67a7e540-acea-11e6-8b06-199ea5c4f362)  (10 s)  
   * Cyclotron in 3D Model [Cyclotron in 3D Model](https://www.compadre.org/osp/items/detail.cfm?ID=10527)  (java applet)


#### Espectrómetro de masas  

 [Mass spectrometer](http://physics.bu.edu/~duffy/HTML5/mass_spectrometer.html)  
 [massspectrometerv10_Simulation.xhtml](https://iwant2study.org/lookangejss/00workshop/2017FelixPaco/day4/ejss_model_massspectrometerv10/massspectrometerv10_Simulation.xhtml)  
 [Mass Spectrometer (Single Sector) - MagLab](https://nationalmaglab.org/education/magnet-academy/watch-play/interactive/mass-spectrometer-single-sector)   

### [Ondas electromagnéticas](/home/recursos/fisica/ondas-electromagneticas)

### Inducción
 [Magnetic Induction Lab](https://www.thephysicsaviary.com/Physics/Programs/Labs/MagneticFlux/index.html) 

[Prof. Joey Neilsen ‪@joeyneilsen.bsky.social‬](https://bsky.app/profile/joeyneilsen.bsky.social/post/3l6y5bugw3s2r)  
While I avoid planning class for tomorrow, how about a cool #Physics demo? ⚛️🧲  
Let's talk about Faraday's Law. What's Faraday's Law, precious? Well, it's the reason that a sheet of aluminum can bring a swinging magnet to a screeching halt.  

### Conceptos básicos magnetismo
Algunos relacionados con inducciónPhET interactive simulationes. Universidad de ColoradoImán y brújula  
 [magnet-and-compass](https://phet.colorado.edu/es/simulation/legacy/magnet-and-compass)  
 [magnets-and-electromagnets](https://phet.colorado.edu/es/simulation/legacy/magnets-and-electromagnets)  
 [faraday](https://phet.colorado.edu/es/simulation/legacy/faraday)  
 [generator](https://phet.colorado.edu/es/simulation/legacy/generator)  (varias pestañas y en cada una incluye una simulación)  
 [faradays-law](https://phet.colorado.edu/es/simulation/faradays-law) Magnetismo terrestre  
 [twitter PHDcomics/status/1209533506923028480](https://twitter.com/PHDcomics/status/1209533506923028480)  
 [![](https://pbs.twimg.com/media/EMkgTsUU8AAkW2F?format=jpg&name=small "") ](https://pbs.twimg.com/media/EMkgTsUU8AAkW2F?format=jpg&name=small)   
 
