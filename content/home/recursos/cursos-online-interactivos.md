---
aliases:
  - /home/recursos/cursos-online--interactivos/
---	
# Cursos online / interactivos

Los cursos online / interactivos pueden enlazar con otros tipos de materiales, como  [materiales autosuficientes](/home/recursos/materiales-autosuficientes) ,  [simulaciones](/home/recursos/simulaciones) , imágenes, vídeos ...  
Hay recursos que pueden ser cursos grabados, no interactivos, como por ejemplo las lecturas de Feyman...
   * [The Feynman Lectures on Physics - caltech.edu/](http://www.feynmanlectures.caltech.edu/)  
   [feynmanlectures.info/](http://www.feynmanlectures.info/)  
   Volume I: mainly mechanics, radiation and heat;  
   Volume II: mainly electromagnetism and matter,  
   Volumen III: quantum mechanics  
   Copyright © 1963, 2006, 2013 by the California Institute of Technology, Michael A. Gottlieb, and Rudolf Pfeiffer  
   Artículos relacionados: hay algunos vídeos  
   [Aprender Física con Richard Feynman: vídeos y libros al alcance de todo el mundo - microsiervos.com](http://www.microsiervos.com/archivo/ciencia/aprender-fisica-con-richard-feynman.html)  
   [Feynman "Messenger" lectures now available online - symmetrymagazine.org](http://www.symmetrymagazine.org/breaking/2009/07/15/feynman-messenger-lectures-now-available-online)  
   [research.microsoft.com/apps/tools/tuva/ (requiere SilverLight de Microsoft para verse)](http://research.microsoft.com/apps/tools/tuva/)  
   [Here are all of Feynman's Freely available Lectures at one place @PhysInHistory - threadreaderapp](https://threadreaderapp.com/thread/1684772760323555328.html)  
   * Física con ordenador  
   [http://www.sc.ehu.es/sbweb/fisica/](http://www.sc.ehu.es/sbweb/fisica/)  
   Material con derechos de autor. © Ángel Franco García -1998-2011.  
   Nivel alto, pero algunas partes aplicables a Bachillerato. Contiene  [simulaciones](/home/recursos/simulaciones) 
   *  [http://ocw.mit.edu/courses/physics/](http://ocw.mit.edu/courses/physics/)  Free Online Course Material - Physics  
   [http://ocw.mit.edu/courses/chemistry/](http://ocw.mit.edu/courses/chemistry/)  Free Online Course Material - Quemistry  
   Massachusetts Institute of Technology. Material con licenciamiento cc-by-nc-sa.  
   En inglés, cursos variados con vídeos, transcripciones, exámenes y soluciones
   * The Physics Classroom [http://www.physicsclassroom.com/](http://www.physicsclassroom.com/)  
   Apuntes, ejercicios, material para profesores. © 1996-2012 The Physics Classroom, Todos los derechos reservados.
   * ChemistryLand [http://www.chemistryland.com/home.html](http://www.chemistryland.com/home.html)  
   Cursos onlline química, algunos no completos. Licenciamiento no aclarado.  
   Un ejemplo interesante  [http://www.chemistryland.com/CHM130S/index.html](http://www.chemistryland.com/CHM130S/index.html)  CHM-130: Fundamental Chemistry
   * OpenCourseWare Universidad Politécnica de Madrid. Relación completa de asignaturas, organizadas por áreas de conocimiento  
   [http://ocw.upm.es/areas-de-conocimiento](http://ocw.upm.es/areas-de-conocimiento)  
   Licenciamiento cc-by (ver  [http://ocw.upm.es/about-ocw/terms-of-use](http://ocw.upm.es/about-ocw/terms-of-use) )
   * Oracle ThinkQuest Education Foundation, material de ciencias ---> pendiente revisar si clasificar de otra manera, son muchos tipos de materiales, sólo algún curso  
   [http://thinkquest.org/pls/html/f?p=52300:30:1439125431052426::::P30_CATEGORY_ID:CPJ_PHYSICAL_SCIENCE](http://thinkquest.org/pls/html/f?p=52300:30:1439125431052426::::P30_CATEGORY_ID:CPJ_PHYSICAL_SCIENCE)  
   Páginas web con cursos / recursos creados por estudiantes "Projects by Students for Students". Ejemplo  [http://library.thinkquest.org/C006669/data/Chem/newindex.html](http://library.thinkquest.org/C006669/data/Chem/newindex.html)  "ChemWorld, Interactive Chemical Tutorials"
   * OpenCourseWare Universidad de Oviedo  
   [http://ocw.uniovi.es/course/category.php?id=43](http://ocw.uniovi.es/course/category.php?id=43)  
   [Licenciamiento cc-by-nc-sa](http://ocw.uniovi.es/ocw/infoLegal.php) . Cursos nivel universitario, organizados en áreas de conocimiento y algunos asociables a física y química como "Química Física"
   * ENTORNO EDUCATIVO INTERACTIVO PARA EL APOYO DEL PROCESO DE ENSEÑANZA/APRENDIZAJE DE LA QUÍMICA ORGÁNICA  
   [http://www.uhu.es/quimiorg/](http://www.uhu.es/quimiorg/)  
   BASADO EN Jmol , JME y Hot Potatoes. Página mantenida por Agustín García Barneto Universidad de Huelva. Incluye muchos materiales, también webquest
   * Khan Academy  
   [http://www.khanacademy.org/](http://www.khanacademy.org/)  
   [http://www.khanacademy.org/science/physics](http://www.khanacademy.org/science/physics)  
   [http://www.khanacademy.org/science/chemistry](http://www.khanacademy.org/science/chemistry)  
   Dentro science: physics, chemistry, organic chemistry ... Multitud de vídeos, la mayoría en inglés. Licenciamiento ©2012 Khan Academy™ cc-by-nc-sa
   * Curso de nivelación en química  
   [http://www.virtual.unal.edu.co/cursos/sedes/medellin/nivelacion/uv00007/index.html](http://www.virtual.unal.edu.co/cursos/sedes/medellin/nivelacion/uv00007/index.html)  
   Curso con los contenidos básicos de fin 2º bachillerato / comienzo universidad  
   "Curso de contenido abierto y usted se pueden descargar y utilizar los materiales sin ningún costo. El curso en este momento no ofrece servicios de tutoría ... "  
   Copyright. Universidad Nacional de Colombia.
   * Vídeos de matemáticas, física y química  
   [http://www.unicoos.com/](http://www.unicoos.com/)  
   También tiene enlaces a recursos. Copyright © unicoos. Todos los derechos reservados 
   * OpenCourseWare Química Orgánica I  
   [http://ocwus.us.es/quimica-organica/quimica-organica-i/Course_listing](http://ocwus.us.es/quimica-organica/quimica-organica-i/Course_listing)  
   Universidad de Sevilla. Autores: Francisca Cabrera Escribano, Juan Vázquez Cabello. Licenciamiento cc-by-nc-sa.
   * Coursera  
   [https://www.coursera.org/](https://www.coursera.org/)  
   [https://www.coursera.org/courses?orderby=upcoming&cats=physics](https://www.coursera.org/courses?orderby=upcoming&cats=physics)  
   [https://www.coursera.org/courses?orderby=upcoming&cats=chemistry](https://www.coursera.org/courses?orderby=upcoming&cats=chemistry)  
   We are a social entrepreneurship company that partners with the top universities in the world to offer courses online for anyone to take, for free.
   * NPTEL National Programme on Technology Enhancement Learning  
   [http://nptel.iitm.ac.in/](http://nptel.iitm.ac.in/)  
   [http://nptel.iitm.ac.in/courses.php?disciplineId=115](http://nptel.iitm.ac.in/courses.php?disciplineId=115)  Física  
   Licenciamiento cc-by-nc-sa
   * Quimitube: clases de química online  
   [http://www.quimitube.com/](http://www.quimitube.com/) 
   * ELESAPIENS Learning & Fun. Unidades didácticas : Química [http://www.elesapiens.com/es/unidades-didacticas/?tag=quimica](http://www.elesapiens.com/es/unidades-didacticas/?tag=quimica)  
   Copyright.
   * Análisis Químico 2011  
   [http://ocw.um.es/ciencias/analisis-quimico](http://ocw.um.es/ciencias/analisis-quimico)  
   Natalia Campillo Seva, Departamento de Química Analítica, Facultad de Química, Universidad de Murcia, cc-by-nc-sa
   * Curso 0 Química. Uned  
   [http://ocw.innova.uned.es/quimicas/](http://ocw.innova.uned.es/quimicas/)  
   Mercedes de la Fuente (Coordinadora), Mª Isabel Gómez del Río, Mª José Morcillo Ortega, Octubre 2008. © 2008 Universidad Nacional de Educación a Distancia 
   * MOOCs para estudiantes ESO y Bachillerato, con alguno sobre química  
   [http://www.owlas.com/web/ajuste-de-reacciones-quimicas](http://www.owlas.com/web/ajuste-de-reacciones-quimicas) 
   * Albany College of Pharmacy and Health Sciences  
   [http://www.organicchem.org/](http://www.organicchem.org/)  
   Courses  
   Organic Chemistry I  
   Organic Chemistry II  
   Survey of Organic Chemistry  
   Martha A. Hass, PhD, Associate Professor of Chemistry
   * 1,700 Free Online Courses from Top Universities  
   [https://www.openculture.com/freeonlinecourses](https://www.openculture.com/freeonlinecourses)  
   [https://www.openculture.com/physics_free_courses](https://www.openculture.com/physics_free_courses)
   [https://www.openculture.com/chemistry-free-courses](https://www.openculture.com/chemistry-free-courses)
   * [Cursos IOC - Batxillerat - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/)  
   El Departament d’Educació, a causa de la situació actual d’excepcionalitat pels efectes del COVID-19, posa en obert els continguts i recursos educatius de l’Institut Obert de Catalunya (IOC). L’IOC vol, d’aquesta manera, posar a disposició materials a les persones que en vulguin fer ús mentre duri el tancament dels centres educatius.  
 
