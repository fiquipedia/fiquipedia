
# Recursos configuración electrónica

La configuración electrónica enlaza con  [orbitales](/home/recursos/orbitales)  y con  [tabla periódica](/home/recursos/quimica/recursos-tabla-periodica)  
La configuración electrónica es un ejemplo de contenido que se repite en varios cursos (3º y 4º ESO, 1º Bachillerato y 2º Bachillerato Química), y me parece interesante saber qué se añade en cada curso.

##  Diagrama de Moeller
Desde 3º se suele enseñar directamente el diagrama de Moeller, a veces llamada "regla del serrucho", sin que entiendan muy bien que son orbitales, ni qué significan los 4 números cuánticos.  
[![](http://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Electron_orbitals.svg/800px-Electron_orbitals.svg.png "") ](http://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Electron_orbitals.svg/800px-Electron_orbitals.svg.png)  
Diagrama de Moeller (izquierda) y forma de algunos orbitales. Fuente  [wikipedia](http://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Electron_orbitals.svg/800px-Electron_orbitals.svg.png) , public domain.  

Cuando se conocen los 4 números cuánticos (n,l,m y s), que es 1º Bachillerato, es interesante indicar que el diagrama de Moeller es realmente una manera práctica de ver la regla de Aufbau más la regla de Madelung (que combina n+l)  
[wikipedia Electron configuration, Aufbau principle and Madelung rule](http://en.wikipedia.org/wiki/Electron_configuration#Aufbau_principle_and_Madelung_rule)  

Es interesante la idea de poner la configuración basada en el gas noble anterior, puesto entre corchetes cuadrados, para no repetir todo.  
En muchos sitios se utiliza la idea de poner una cajita cuadrada por orbital con dos flechas simbolizando los spines opuestos.  
Interesante tener presente que hay excepciones al diagrama... (algunas obligatorias en 2º Bachillerato)  
Se incluye aquí una tabla periódica con configuración electrónica, cc-by-sa  
[![](http://upload.wikimedia.org/wikipedia/commons/6/66/Periodic_Table_of_Elements_showing_Electron_Shells_(2011_version).svg "") ](http://upload.wikimedia.org/wikipedia/commons/6/66/Periodic_Table_of_Elements_showing_Electron_Shells_%282011_version%29.svg)  
[commons.wikimedia.org periodic table of elements with electron shells](http://commons.wikimedia.org/wiki/Electron_shell#mediaviewer/File:Periodic_Table_of_Elements_showing_Electron_Shells_%282011_version%29.svg) 

##  Electrón más externo
En 2017 en mis apuntes de Química 2º Bachillerato indico 
*"Cuando se indica “electrón más externo” ó “último electrón”, se hace referencia al último que se pone según la regla de la construcción, de mayor energía, más fácil de extraer, no tiene necesariamente el número n mayor."*  
Pero la realidad es más complicada (pendiente revisar, también algunas resoluciones de ejercicios para comentarlo, revisando cómo lo consideran correctores EvAU)  
THE ORDER OF FILLING 3d AND 4s ORBITALS  
[chemguide.co.uk THE ORDER OF FILLING 3d AND 4s ORBITALS](https://www.chemguide.co.uk/atoms/properties/3d4sproblem.html)  
If you stop and think about it, that has got to be wrong. As you move from element to element across the Periodic Table, you are adding extra protons to the nucleus, and extra electrons around the nucleus.  
The various attractions and repulsions in the atoms are bound to change as you do this - and it is those attractions and repulsions which govern the energies of the various orbitals.  

Shriver and Atkins' Inorganic Chemistry  
[https://books.google.es/books?id=tUmcAQAAQBAJ&pg=PA18](https://books.google.es/books?id=tUmcAQAAQBAJ&pg=PA18)  
"In atoms from Ga (Z=31) onwards, the 3d orbitals lie well below the 4s orbital in energy, and the outer-most electrons are unambigously those of the 4s and 4p subshells"

##  Simulaciones / ejercicios interactivos sobre configuración electrónica

[Electronic Dungeon - fisicayquimicaweb.es](https://fisicayquimicaweb.es/data/documents/ELECTRONICDUNGEON/index.html)  
![](https://fisicayquimicaweb.es/gallery_gen/3d004dc753106c57794594a1b4a6f95a_317x190.png)  

[keithcom atoms](http://www.keithcom.com/atoms/index.php)  
![](http://www.keithcom.com/atoms/legend.png)
 
[concurso.cnice.mec.es Inicación interactiva a la materia, configuración electrónica](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/atomo/celectron.htm)  
 
 En [http://www.ptable.com/#Orbital](http://www.ptable.com/#Orbital)  se puede ver la configuración electrónica de cada elemento  
 
 [e-ducativa.catedu.es configuraciones electrónicas](http://e-ducativa.catedu.es/44700165/aula/archivos/repositorio/4750/4841/html/21_configuraciones_electrnicas.html)  
 
 [educaplus.org tabla periódica con configuración electrónica](http://www.educaplus.org/sp2002/configuracion.html)  
 [educaplus.org game configuracion electronica](http://www.educaplus.org/game/configuracion-electronica)  
 
 [http://herramientas.educa.madrid.org/tabla/anim/configuracion4.swf](http://herramientas.educa.madrid.org/tabla/anim/configuracion4.swf) 

##  Ejercicios configuración electrónica PAU: selección para ESO
Los contenidos asociados a configuración electrónica se introducen en 3º ESO, y se repiten en 4º ESO, 1º Bachillerato y 2º Bachillerato profundizando (números cuánticos, orbitales, Hund...)  
Se incluye una selección de los  [ejercicios/apartados de PAU de Química que pueden utilizarse en ESO](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/recursos-configuracion-electronica/Q2-PAU-EstructuraAt%C3%B3micaClasificacionPeri%C3%B3dicaElementos-ESO.pdf) , ya que tratan sobre configuración electrónica.

##  Actividades / juegos
 [twitter bjmahillo/status/1044991184224104454](https://twitter.com/bjmahillo/status/1044991184224104454)  
 Jugamos a la quiniela en física y química de 4º ESO para repasar los orbitales y las configuraciones electrónicas. Idea original de @bertaocana para trabajar morfoxintaxis.  
 [![](https://pbs.twimg.com/media/DoCMuV3XcAAdT8L?format=jpg&name=900x900 "") ](https://pbs.twimg.com/media/DoCMuV3XcAAdT8L?format=jpg&name=900x900)  
 
 [twitter DenmanChem/status/1051835485558099969](https://twitter.com/DenmanChem/status/1051835485558099969)  
 Electron Configuration Battleship was a HIT! #chemchat 🛳🚢⛴  
 [![](https://pbs.twimg.com/media/DpjeyrZU0AAD_Tf.jpg "") ](https://pbs.twimg.com/media/DpjeyrZU0AAD_Tf.jpg)  
 
 [https://fisquiweb.es/experienciasnolab.htm](https://fisquiweb.es/experienciasnolab.htm) La actividad pretende introducir el principio de construcción de la estructura electrónica de los átomos relacionando los niveles o pisos del hotel y sus habitaciones con los niveles energéticos del átomo.  
 [![](https://fisquiweb.es/ExpSinLab/ConfElec/ConfElecLogo.png "") ](https://fisquiweb.es/ExpSinLab/ConfElec/ConfElecLogo.png)  
 
 [[![](https://pics.me.me/3d10-1s2-2s2-2p6-3s2-3p6-biotecno-memes-3d10-4s2-46238487.png "")](https://me.me/i/3d10-1s2-2s2-2p6-3s2-3p6-biotecno-memes-3d10-4s2-d46d8680d34f49b6846bcaf68474133f)  
 
Los ficheros se veían inicialmente anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia quimica/recursos-configuracion-electronica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/quimica/recursos-configuracion-electronica)  

