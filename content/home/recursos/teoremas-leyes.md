# Teoremas y leyes

Esta página surge en 2018 cuando veo que modelo 2019 de Física Madrid cita "Teorema de Ampère" pero debería decir "Ley de Ampère"  
Lo comento en Twitter  
[twitter FiQuiPedia/status/1068912875119525888](https://twitter.com/FiQuiPedia/status/1068912875119525888)  
Subida mi resolución 2019-Modelo-A3 EvAU Física Madrid:  
"Como primer comentario se podría aclarar que enunciado habla de “Teorema de Ampère” pero debería decir “Ley de Ampère”. Los teoremas, como el de Pitágoras, se demuestran, y una ley se comprueba mediante experimentación."  

Pero tras hacerlo me surgen más dudas sobre en qué más puntos se usa teorema y ley, y aclarar conceptos generales de ciencia como teorías, modelos, principios ...  

[La diferencia entre un teorema y una ley - i-ciencias.com](https://www.i-ciencias.com/pregunta/10839/la-diferencia-entre-un-teorema-y-una-ley#answer-35703)  

Quizá de modo simple se podría decir que teoremas son matemáticos y leyes son físicas, pero no todo es tan simple, porque algunos teoremas matemáticos se pueden aplicar a la física y dan lugar a leyes que relacionan magnitudes físicas.

##  Teorema de Gauss
Aparece como teorema en  [Currículo Física 2º Bachillerato LOMCE](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce) 3 veces: criterios 5 y 6 y estándar 6.1 (aparecía 1 vez en LOE)  
[Teorema de la divergencia - wikipedia](https://es.wikipedia.org/wiki/Teorema_de_la_divergencia) 
[Ley de Gauss - wikipedia](https://es.wikipedia.org/wiki/Ley_de_Gauss)  
Si se habla de que la ley de Gauss, en su formulación diferencial o integral es una las 4 leyes de Maxwell, no sería correcto decir teorema  
Creo que lo correcto sería "Ley de Gauss"

##  Teorema de las fuerzas vivas / teorema trabajo-energía
Aparece como teorema en  [Currículo Física y Química 1º Bachillerato (LOE)](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculofisicayquimica-1bachillerato)  
Tiene una demostración en base a definición matemática de energía cinética y trabajo; en este caso lo veo correcto  
[Teorema de la energía cinética - wikipedia](https://es.wikipedia.org/wiki/Teorema_de_la_energ%C3%ADa_cin%C3%A9tica)  

El origen del nombre parece histórico
[Vis viva - wikipedia](https://es.wikipedia.org/wiki/Vis_viva)  
> El nombre de fuerza viva se conserva por razones históricas. Fue asignado por Leibniz a aquellas fuerzas que producen movimiento, en contraposición a las que él llamaba fuerzas muertas, que no dan lugar a movimiento alguno (por ejemplo, el peso de un cuerpo situado sobre un tablero horizontal).   

[Vis viva - en.wikipedia](https://en.wikipedia.org/wiki/Vis_viva)  
> Vis viva (from the Latin for "living force") is a historical term used to describe a quantity similar to kinetic energy in an early formulation of the principle of conservation of energy.   



A veces es citado como teorema trabajo-energía

[Kinetic Energy and Work-Energy Theorem - libretexts.org](https://phys.libretexts.org/Bookshelves/University_Physics/Book%3A_Physics_(Boundless)/6%3A_Work_and_Energy/6.4%3A_Work-Energy_Theorem)  
> The work-energy theorem states that the work done by all forces acting on a particle equals the change in the particle’s kinetic energy.  

[Trabajo y energía - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica/dinamica/trabajo/energia/energia.htm)  
> El teorema del trabajo-energía indica que el trabajo de la resultante de las fuerzas que actúa sobre una partícula modifica su energía cinética.  


##  Teorema del momento lineal y teorema de conservación
Aparecen como teoremas en  
[1º Bachillerato Ampliación Física y Química I (LOMCE)](/home/materias/bachillerato/ampliacion-fisica-quimica-1-bachillerato)  
[Cantidad de movimiento, Conservación - wikipedia](https://es.wikipedia.org/wiki/Cantidad_de_movimiento#Conservaci%C3%B3n)  
[Ley de conservación - wikipedia](https://es.wikipedia.org/wiki/Ley_de_conservaci%C3%B3n)  
En este caso no lo veo tan claro, pero creo que serían leyes físicas.  

Hablar de conservación me lleva a pensar en el Teorema de Noether, que enlaza teorema con leyes de conservación  
[Teorema de Noether - wikipedia](https://es.wikipedia.org/wiki/Teorema_de_Noether) 

##  Teorema de Thevenin
Aparece en  [Currículo Electrotecnia 2º Bachillerato](/home/materias/bachillerato/electrotecnia-2bachillerato/curriculoelectrotecnia-2bachillerato)  
[Teorema de Thévenin - wikipedia](https://es.wikipedia.org/wiki/Teorema_de_Th%C3%A9venin)  

