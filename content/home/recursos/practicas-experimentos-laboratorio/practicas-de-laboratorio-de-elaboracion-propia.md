
# Prácticas de laboratorio de elaboración propia

Aquí se ponen solo las prácticas de elaboración propia, adicionales a las que [prácticas y experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio) generales y otras en [prácticas con el smartphone](/home/recursos/practicas-experimentos-laboratorio/practicas-experimentos-laboratorio-smartphone).  
De momento no pone una tabla de documentos, sino que se van enlazando en cada apartado. Los ficheros se veían inicialmente anexados a esta página, ahora se pueden ver en [drive.fiquipedia prácticas experimentos laboratorio de elaboración propia](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia) 

Algunas pueden clasificarse como "actividades" o "experimentos" en lugar de "prácticas de laboratorio" si son informales y se pueden hacer en el aula o en casa, o incluso con el móvil, y pueden estar clasificadas de otra manera y colocadas en otro lugar. Por ejemplo la actividad de tiempo de reacción con MRUA y la actividad de reacciones químicas con Beaker.  

##  Planteamiento
En 2015-2016 tengo por primera vez una vacante completa en la que hay desdobles de laboratorio, y se plantean realizar prácticas de laboratorio para 3º ESO (ya LOMCE), 4º ESO y 1º de Bachillerato, realizando 10 prácticas en total en cada nivel a lo largo del curso.  
Mi experiencia con laboratorios de física y química hasta este momento es muy limitada (fundamentalmente debido a falta de desdobles no las había), y aparte de recopilar información general sobre prácticas, me planteo realizar unas prácticas propias por lo que creo esta página. La idea de crearlas es aplicar una serie de ideas:  
- Las prácticas deben estar vinculadas a currículo (contenidos, criterios de evaluación, estándares de aprendizaje) de cursos concretos para validar que son adecuadas, más en un momento donde empieza la implantación de LOMCE.  
- Crear por separado un documento para el profesor y otro para el alumno, y hacer prácticas genéricas para que puedan ser válidas/adaptables a distintos niveles.  
En 2017-2018 vuelvo a tener prácticas asociadas a Ampliación de Física y Química de 4º ESO, con condicionamientos especiales: el espacio limitado del centro hace que las 2 sesiones semanales se tengan que dar directamente en el laboratorio, e imparto a los mismos alumnos Física y Química de 4º ESO sin desdobles para laboratorio. Por ello casi todas las sesiones serán salvo algunas explicaciones prácticas de laboratorio, aunque no tan formales ni limitadas a una única sesión.

##  Guión de práctica para el profesor
El guión del profesor debe ser genérico, se trata de un documento orientativo, centrado en unos objetivos a cumplir, permitiendo ajustar la práctica al centro y grupo de alumnos: puede que no haya ciertos materiales en el laboratorio, o puede que la misma práctica se aplique a varios niveles. Por ejemplo, la misma práctica se puede hacer en 1º de Bachillerato y en 3º ESO si los alumnos de 1º de Bachillerato es la primera vez que van al laboratorio. 

##  Guión de práctica para el alumno
El guión del alumno debe estar concentrado en un máximo de 2 caras de folio, e idealmente una única cara.  
Puede que se les de una copia impresa del guión, pero también se debe contemplar que los alumnos vayan al laboratorio simplemente con bolígrafo y papel, y **"el guión" esté escrito en la pizarra o se les deja en funda de plástico para reutilizarla, que no se la queden,** se les explica, y ellos se limitan a tomar datos y anotaciones. También se puede entregar un único guión por grupo, y no por alumno.  
Los guiones de prácticas también se pueden tener en un aula virtual o enviar por correo electrónico en pdf, si se dispone de las direcciones de los alumnos, lo que se consigue tras la primera práctica si la entrega es por mail.  
Los guiones de alumnos típicos suelen incluir un apartado de "Objetivo", "Fundamento teórico", "Lista de material", "Procedimiento"; esos puntos pueden tratarse con explicaciones del profesor en el laboratorio y en la pizarra (los laboratorios la suelen tener).  
Para los guiones de prácticas que estén disponibles en una url como las de esta página, se puede dar a los alumnos un código QR y se pueden descargar el pdf de su guión ahorrando papel.

##  Informe de práctica
Aunque se entregue impreso un guión al alumno, considero poco adecuado que el guión tenga un hueco para el nombre del alumno y rellenando datos sea lo que entregue: debe entregar un informe de práctica, y lo ideal es que lo entregue por correo electrónico, no en papel; la ventaja es que se ahorra papel, permite comprobar una mínima competencia TIC en los alumnos (puede parecer simple, pero anexar documentos a un correo, validar que es enviado... no todos lo saben hacer y es una competencia básica), permite registrar la hora de entrega de los informes, y permite almacenar las prácticas fácilmente. Además permite tener un listado de direcciones de correo electrónico de los alumnos para enviarles información, como los guiones.  
El informe de práctica se les debe introducir como una versión simplificada de un Informe Científico, y que constará de los siguientes apartados:  
1. Título de la práctica y miembros del grupo de laboratorio que la han realizado (en caso de que la elaboración sea en grupo).  
2. Introducción / Resumen / Objetivos: se describe de manera general la práctica y qué finalidad se persigue al realizar la experiencia  
3. Materiales utilizados: descripción detallada indicando todos ellos y sus características  
4. Metodología / Procedimiento: descripción detallada de los pasos seguidos en la experimentación.  
5. Resultados: se indican las medidas realizadas, cálculos realizados y resultados, que en algún caso pueden darse en forma de tabla o gráfica.  
6. Conclusiones: qué podemos deducir del análisis de los resultados obtenidos. Se pueden responder a cuestiones planteadas.  
7. Bibliografía: se indica si se ha consultado en libros (incluido el libro de texto de clase) o en internet, dando la referencia.  

Comentarios y variaciones:  
- Punto 3: es importante que sepan reconocer las características de los materiales utilizados, como su sensibilidad  
- Punto 4: deben describir los pasos que han realizado, no necesariamente los que indique el guión. Describir errores o variantes puede ser interesante  
-Punto 5: en alguna práctica se les pueden plantear cuestiones concretas para que reflexionen, orientándoles sobre las conclusiones a obtener.  

En  [http://colegiocristorey.com/nenuca/nenina/quimicageneral/XCUADERNO LABORATORIO.pdf](http://colegiocristorey.com/nenuca/nenina/quimicageneral/XCUADERNO%20LABORATORIO.pdf)  por Nenina Martín Ossorio se plantea un cuaderno de prácticas, adicional al guión de prácticas.  
1. Actividades previas: antes de entrar al laboratorio  
2. Cuestiones previas: se pueden preguntar al entrar en el laboratorio  
3. Introducción: objetivos e información básica  
4. Sección experimental  
5. Resultados e interpretación de resultados  
6. Cuestiones posteriores: al finalizar la práctica  
7. Conclusiones  
8. Bibliografía  
9. Cuestiones para evaluación: 

##  Prácticas generales de introducción


###  Introducción al laboratorio: normas de seguridad y material
(práctica "0", no se genera informe de práctica como tal, a realizar solamente la primera vez que se va al laboratorio)  
[Documento para el alumno](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioSeguridadyMateriales-Alumno.pdf) 


###  Medidas, errores y tratamiento de datos
[Método científico y medida. Gotas en una moneda de agua](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMetodoCientificoMedidaMonedaAgua.pdf)  


Práctica común a varios niveles, se considera práctica inicial y se incluye en el guión una orientación para el informe de práctica.  
 [Documento para el alumno](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMedidaErrores-Alumno.pdf)  
 [Documento para el profesor](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMedidaErrores-Profesor.pdf) 

##  Prácticas física


###  Cinemática
Movimiento rectilíneo: MRU, MRUA, caída libre  

[Documento para el alumno, MRU](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMRU-Alumno.pdf)  
[Documento para el alumno, MRUA](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMRUA-Alumno.pdf)  
[Documento para el profesor](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMovimientoRectil%C3%ADneo-Profesor.pdf)  
[Documento para alumno, tiempo de reacción, caída libre](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorio-TiempoReacci%C3%B3n-Alumno.pdf)  

[Documento para el alumno, MRUA frenada con uso de vídeo opcional](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMRUA-frenada-video-Alumno.pdf)  


Tiro parabólico  
Una práctica sobre energía incluye un tiro parabólico.

###  Dinámica

[Segunda ley de Newton](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioDin%C3%A1micaSegundaLeyNewton.pdf)  

Inclinómetro, usando acelerómetro del móvil  
[Documento para el alumno, inclinómetro](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioInclin%C3%B3metroM%C3%B3vil-Alumno.pdf)  

Ley de Hooke, cálculo k estático, con alargamiento muelle  
[Documento para el alumno, ley de Hooke](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioHooke-Alumno.pdf)  
Se pueden ver  [proyectos de investigación](/home/recursos/proyectos-de-investigacion)  sobre ley de Hooke, se hace con laboratorio virtual / montaje en casa  

Rozamiento con dinamómetro en horizontal, rozamiento con rampa  
[Documento para el alumno, rozamiento](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioRozamiento-Alumno.pdf)  
[Documento para el profesor, rozamiento](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioRozamiento-Profesor.pdf)  

[Balanza y peso](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioDin%C3%A1micaBalanzaPeso.pdf)  

###  Presión
 [Documento para el alumno: fuerzas en fluidos: empuje y peso aparente, ludión/diablillo Descartes](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioFuerzasFluidos-Alumno.pdf)  
 
 Flotabilidad, Ley de gases ...

###  Conservación energía mecánica
Energía:  
Ideas: 
* Rampa, tiro parabólico y conservación de energía mecánica.  
* Péndulo, velocidad en punto inferior y altura máxima (es péndulo que ya se usa en bloque de MAS)  
  
[Documento para alumno 1º Bachillerato (tiro parabólico)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioEnerg%C3%ADa-Alumno.pdf)  
[Documento para profesor 1º Bachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioEnerg%C3%ADa-Profesor.pdf)   

[Documento para alumno ESO (tiro parabólico y péndulo)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioEnerg%C3%ADaESO-Alumno.pdf)  


###  MAS
Péndulo, conservación energía mecánica y periodo en función de longitud, pérdidas  
[Documento para alumno 1º Bachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMAS-Alumno.pdf) 

Otras ideas: cálculo k dinámico, con oscilaciones muelle

###  Ondas
En resolución propia de [ejercicio oposición 1994 Cataluña A7](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-para-oposiciones/1994-Catalu%C3%B1a-ProblemaA7.pdf) , se incluyen referencias sobre prácticas y material para laboratorio.

###  Óptica
 [Documento para alumno, índice refracción](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorio%C3%8DndiceRefraccion-Alumno.pdf) 

##  Prácticas química


###  Disoluciones
Práctica para 3º ESO y 1º Bachillerato, con distinto tratamiento. El documento de profesor es común a ambos niveles intentando agrupar todas las ideas  
[Documento para el alumno 3º ESO](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioDisoluciones-3ESO-Alumno.pdf)  
[Documento para el alumno 1º Bachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioDisoluciones-1Bach-Alumno.pdf)  
[Documento para el profesor](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioDisoluciones-Profesor.pdf) 

####  Disoluciones y cristalización
 [Documento para el alumno](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioSolubilidadCristalizaci%C3%B3n-Alumno.pdf) 

####  Destilación
 [Documento para alumno ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioDestilaci%C3%B3n-Alumno.pdf) 

###  Análisis de sustancias


####  Propiedades sustancias
 [Propieades sustancias. Documento para el alumno 4º ESO](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioPropiedadesSustancias-FQ4-Alumno.pdf)  
 [Propiedades sustancias. Documento para el profesor](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioPropiedadesSustancias-Profesor.pdf) 
 [Propieades sustancias. Densidad](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioPropiedadesSustancias.Densidad.pdf)  


####  Espectroscopía
Práctica para 1º Bachillerato, ya que aparece en currículo LOMCE explícitamente  
[Documento para el alumno 1º Bachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioEspectroscop%C3%ADa-1Bach-Alumno.pdf)  
[Documento para el profesor](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioEspectroscop%C3%ADa-Profesor.pdf) 

###  Reacciones químicas
El tipo de prácticas es muy amplio. En el documento de profesor se intentan recoger ideas generales, es un borrador y lo será durante mucho tiempo: se irán añadiendo ideas a medida que se concreten más documentos de prácticas concretas para alumnos.  
[Documento para el profesor](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas-Profesor.pdf)  

En algunos casos pueden enlazar con estequiometría y moles, más que reacciones 

[Determinación fórmula hidrato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas.DeterminacionFormulaHidrato.pdf)  

Asociada a [Prácticas con el smartphone](/home/recursos/practicas-experimentos-laboratorio/practicas-experimentos-laboratorio-smartphone)  
[Actividad reacciones químicas Beaker](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/ActividadQuimicaReaccionesQuimicasBeaker.pdf)  


####  Precipitación
Se plantea una práctica para 1º Bachillerato de precipitación para hacer cálculos estequiométricos de reactivo limitante  
[Documento para el alumno 1º Bachillerato, precipitación](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas.Precipitaci%C3%B3n-1Bach-Alumno.pdf) 

####  Liberación gases
 [Documento para alumno (liberar H2 combinando metal con HCl)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas.LiberarGas-HCl-H2-Alumno.pdf) 

####  Valoración ácido-base
 [Documento para alumno (valoración vinagre comercial)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas.ValoracionVinagre-Alumno.pdf)  
 (la práctica de disoluciones  [Documento para el alumno 1º Bachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioDisoluciones-1Bach-Alumno.pdf)  también está asociada a valoración ácido base)

####  Hidrólisis agua
 [Documento para alumno (Hidrólisis del agua)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas.HidrolisisAgua-Alumno.pdf) 

####  Saponificación
 [Documento para alumno (saponificación)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas.Saponificacion-Alumno.pdf) 

####  Catalizadores
 [Documento para alumno (catalizadores)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas.Catalizadores.pdf) 

####  Polímeros
 [Documento para alumno (concepto de polímero, búsqueda información)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas.Polimeros.pdf)  
 [Documento para alumno (esterificación)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioReaccionesQuimicas.Esterificacion.pdf) 

###  Termología
 [Documento para el alumno](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioTermolog%C3%ADa-Alumno.pdf)  (4º ESO / 1º Bachillerato)  
 [Documento para el profesor](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioTermolog%C3%ADa-Profesor.pdf) 

###  Electroquímica
Se pueden plantear pilas de limón, de patata ... en 2º Bachillerato realizada práctica magistral, pero con cálculos, en el patio: cada alumnno aporta una moneda de 1 céntimo, se pesan todas, se añade ácido nítrico en exceso, y cuando termina la reacción se pesa de nuevo. 


