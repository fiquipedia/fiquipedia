# MRUA (Movimiento Rectilíneo Uniformemente Acelerado)  

Esta es una subpágina de la página de [cinemática](/home/recursos/fisica/cinematica) para descargarla centralizando aquí recursos solo sobre MRUA, que pueden incluir experiencias o [prácticas experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio) asociadas, por lo que puede haber información también, a veces duplicada, en otras páginas.  

Dado que el MRU es un caso de MRUA con a=0, puede haber recursos relacionados de [MRU](/home/recursos/fisica/cinematica/mru)  
También puede haber recursos relacionados con la distancia de seguridad  

Pueden aplicar algunos [recursos de gravitación]((/home/recursos/fisica/recursos-gravitacion), por ejemplo distinta altura saltada en distintas planetas con misma velocidad inicial

Jorge Lay Gajardo  
[muag2profisica2dic05.swf](http://www.profisica.cl/images/stories/animaciones/muag2profisica2dic05.swf) 

## Simulaciones

[Encuentros MRU y MRUA - geogebra.org](https://www.geogebra.org/m/fsn3ymfe)  

[Gráficas del movimiento  | Educaplus](http://www.educaplus.org/game/graficas-del-movimiento)  (HTML5)  
Incluye MRU y MRUA  

[Movimiento con Aceleración Constante - walter-fendt.de](http://www.walter-fendt.de/html5/phes/acceleration_es.htm)  
Gráficas a-t, v-t, x-t  

[twitter lafisicaesfacil/status/1698308122899546489](https://twitter.com/lafisicaesfacil/status/1698308122899546489)  
Hola, profes de FyQ! Os comparto una herramienta con la que podéis crear aleat. gráficas v-t para practicar a reconocer movimientos. La misma gráfica permite ver r-t, v-t y a-t, y tiene un código para volver a ella:  
[Gráficas v-t - geogebra](https://www.geogebra.org/m/gx52vbjt)  

## Actividades

[twitter fqsaja1/status/1592155204577026048](https://twitter.com/fqsaja1/status/1592155204577026048)  
Actividad de distancia de seguridad para 4º ESO  
Realizada por nuestra compañera @BlancoSoraya seguro que resulta de utilidad para los que impartís el nivel. Si es así, difundidlo. Se agradece.  
Gracias a Soraya por toda su implicación en el Dpto  
[DISTANCIA DE SEGURIDAD 4º ESO](https://drive.google.com/file/d/1qx2AyRO3TTVwB_k0cDOywHYcoc8vc_Hw/view)  

## Experimentos / laboratorio
Ver [Prácticas / experimentos laboratorio](/home/recursos/practicas-experimentos-laboratorio/) y [Prácticas de laboratorio de elaboración propia](/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/), por ejemplo  
[Documento para el alumno, MRUA](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia/PracticaLaboratorioMRUA-Alumno.pdf)  



