
# 2º Bachillerato Desarrollo experimental (LOMCE)

En mayo 2018 es borrador, pero tiene ya definido currículo   
  
[Proyecto de Orden de la Consejería de Educación e Investigación, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid para su implantación a partir de 2019-2020, y se modifica la Orden 1255/2017, de 21 de abril, de la Consejería de Educación, Juventud y Deporte, por la que se establece la organización de las enseñanzas para la obtención del título de Graduado en Educación Secundaria Obligatoria por personas adultas en la Comunidad de Madrid](http://www.madrid.org/es/transparencia/normativa/proyecto-orden-consejeria-educacion-e-investigacion-que-se-aprueban-materias-libre-1)   
  
Proyecto de orden de la Consejería de Educación e Investigación, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid. Fecha: 20 Abril 2018 [Memoria del análisis de impacto normativo](http://www.madrid.org/es/transparencia/sites/default/files/regulation/documents/main_11.pdf)  [Proyecto de orden](http://www.madrid.org/es/transparencia/sites/default/files/regulation/documents/orden_5.pdf)   
  
Es oficial en junio 2018 [ORDEN 2043/2018, de 4 de junio, de la Consejería de Educación e Investigación, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid para su implantación a partir de 2018-2019, y se modifica la Orden 2200/2017, de 16 de junio, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid, así como la Orden 1255/2017, de 21 de abril, de la Consejería de Educación, Juventud y Deporte, por la que se establece la organización de las enseñanzas para la obtención del título de Graduado en Educación Secundaria Obligatoria por personas adultas en la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2018/06/14/BOCM-20180614-16.PDF)   

## Introducción
La presente materia está diseñada para su oferta en segundo curso del Bachillerato.  
La materia Desarrollo Experimental permite al alumnado familiarizarse con el manejo y la utilización del material y los instrumentos más habituales en un laboratorio de química  
al mismo tiempo que facilita el conocimiento de los aspectos teóricos básicos relacionados con estos conceptos, y dota al alumnado de las herramientas precisas para ejecutar una investigación autónoma relacionada con los procesos químicos tratados.  
Se pretende asimismo que, ante la divulgación de cualquier trabajo científico, los alumnos sepan redactar un informe que incluya el procedimiento llevado a cabo en el laboratorio, los resultados obtenidos, el tratamiento y presentación adecuados de los mismos, y las conclusiones. También se persigue que el alumnado adquiera unos hábitos de trabajo correctos tanto por las normas de trabajo y como por la seguridad en el laboratorio.La materia Desarrollo Experimental permite a los alumnos relacionar algunos de los contenidos conceptuales de la Química con los procedimientos experimentales que dieron lugar a esos contenidos o que permitieron comprobarlos. Al mismo tiempo, el aprendizaje de estos procedimientos y técnicas experimentales, propios del trabajo científico, son útiles para todo el alumnado del Bachillerato independientemente de los estudios que quieran continuar al finalizar esta etapa.Los contenidos de la materia quedan distribuidos en cuatro bloques:  

   * Metodología de trabajo
   * Técnicas generales
   * Técnicas especificas
   * Proyecto de investigación en el laboratorio

## Contenidos

### Bloque 1.Metodología de trabajo
- Conocimiento del material de laboratorio  
   * Elementos de sujeción, material volumétrico (graduado y aforado), recipientes y materiales específicos (refrigerantes,kitasato, etc.).  
- Normas de seguridad  
   * Normas de seguridad.
   * Manipulación de sustancias químicas.
   * Normas de comportamiento en el laboratorio.
   * Protocolos de actuación en caso de accidente.
- Eliminación y tratamiento de residuos  
   * Tipos de residuos y su manipulación.
   * Gestión adecuada de los residuos de laboratorio.
- Tratamiento matemático de datos fisicoquímicos  
   * Conceptos de error absoluto y relativo.
   * Concepto de cifras significativas. Método adecuado para el redondeo.
   * Conceptos de precisión y exactitud.
   * Fuentes y tipos de error: de escala, aleatorios y sistemáticos.
- Preparación de disoluciones  
   * Formas de expresar una concentración: molaridad, molalidad, tanto por 100 y fracción molar.
   * Preparación de disoluciones: sólido-líquido, líquido-líquido y gas-líquido.
- Punto de ebullición de una sustancia (Determinación de pureza).  
   * Concepto de punto de ebullición.
   * Uso del tubo de Thiele.


### Bloque 2. Técnicas generales
- Solubilidad y precipitación  
   * Fuerzas que intervienen en la disolución de una sustancia.
   * Formación de sólidos amorfos y cristalinos.
   * Estudio experimental de la influencia de factores como la temperatura, la naturaleza de las sustancias y la concentración en la disolución o precipitación de una sustancia.
   * Concepto de producto de solubilidad.
- Técnicas de filtración  
   * Tipos de filtros (simple, pliegues, lana de vidrio y embudo Buchner).
- Equilibrios ácido-base  
   * Medidores de pH: papel pH, indicadores y pH-metro.
   * Preparación de un tampón y comprobación de su utilidad.
   * Ácidos y bases relevantes a nivel industrial y de consumo.
- Valoraciones ácido-base  
   * Concepto de reacción de neutralización.
   * Concepto de patrón primario.
   * Determinación de la concentración de diferentes disoluciones mediante valoración ácido base incluyendo productos comerciales para confirmar la veracidad de sus etiquetas.
- Equilibrios redox  
   * Uso de los potenciales redox para predecir una reacción.
   * Ordenación experimental de diversos iones según su potencial redox.
- Valoraciones redox.  
   * Cálculo de masas equivalentes. Indicadores redox
   * Cálculos de equivalentes redox en diversos procesos.
   * Aplicar las leyes de la estequiometría a la reacciones redox.
   * Realización de experiencias sencillas de laboratorio. Determinación de la concentración de una disolución.
   * Importancia de las reacciones de oxidación-reducción en la vida cotidiana.


### Bloque 3. Técnicas específicas
- Cromatografía en capa fina  
   * Técnica de identificación de sustancias de una mezcla según su polaridad.
- Destilación sencilla y fraccionada  
   * Técnica de separación de mezclas según su punto de ebullición.
   * Concepto de azeótropo.
- Cristalización y purificación  
   * Ejecución de diversas cristalizaciones (sulfato de cobre, lluvia de oro, árbol de plata) y desarrollo de estructuras más complejas (ADP).
- Cinética química: reloj de yodo.  
   * Estudio del orden de reacción y de los factores que afectan a la velocidad de una reacción.
- Separación e identificación de especies   
   * Establecer una ruta para identificar cationes presentes en una disolución.
   * Reacciones de identificación.
   * Composición de un acero.
- Obtención de fármacos   
   * Obtención de la aspirina.
   * Obtención del trihidróxido de aluminio.

### Bloque 4. Proyecto de investigación en el laboratorio
- Investigación bibliográfica.  
- Iniciativa y coherencia en la investigación.  
- Orden en la ejecución de los experimentos.  
- Tratamiento adecuado de los datos obtenidos y en la emisión de conclusiones.  
- Elaboración de un informe científico.  

## Criterios de evaluación y estándares de aprendizaje evaluables

### Bloque 1. Metodología de trabajo
1. Describir el material de laboratorio.  
1.1. Diferencia las características del material y los aparatos de medida y su uso.  
2. Conocer el margen de error en la medición según el aparato utilizado.  
2.1. Elige el instrumento adecuado a cada medición atendiendo a la precisión requerida en cada proceso.  
3. Comprender las principales normas de seguridad a respetar y seguir en un laboratorio.  
3.1. Respeta las normas de conducta y orden a seguir en un laboratorio durante las prácticas.  
3.2. Reconoce e identifica los símbolos más frecuentes utilizados en el etiquetado de productos químicos e instalaciones, interpretando su significado.  
3.3. Sigue la secuencia de actuación correcta durante un simulacro de accidente en el laboratorio.  
4. Valorar la necesidad de conocer el impacto que produce en la naturaleza los materiales usados y los residuos generados en el laboratorio.  
4.1. Recicla y elimina los residuos adecuadamente atendiendo a su composición.  
5. Aprender a utilizar correcta y adecuadamente granatarios, balanzas analíticas y material volumétrico.  
5.1. Es capaz de preparar disoluciones de una concentración determinada.  
6. Entender las magnitudes que se miden con aparatos como pH-metros o de potenciales eléctricos; medida de puntos de fusión, termómetros o sensores térmicos.  
6.1. Extrae valores exactos medidos con aparatos como pH-metros, termómetros o sensores.  
7. Discriminar datos experimentales siguiendo criterios de exactitud y precisión y usando el criterio Dixon.  
7.1. Elimina datos obtenidos aplicando criterios científicos.  
8. Manejar el criterio t de Student añadiendo el margen de error a las medidas.  
8.1. Añade a cada resultado un margen de error.  
9. Plantear alguna experiencia sencilla de laboratorio donde se confirme una hipótesis o se corrija un error experimental.  
9.1. Corrige datos obtenidos experimentalmente atendiendo a experimentos paralelos que eliminen ciertos errores experimentales.  
10. Entender el fenómeno de la fusión y sus aplicaciones en la sociedad.  
10.1. Calcula correctamente el punto de fusión de un sólido usando el tubo de Thiele.  
10.2. Identifica una sustancia según su punto de fusión, así como un relativo grado de pureza.  
11. Plantear métodos eficaces para realizar experiencias sencillas de laboratorio.  
11.1. Ejecuta una práctica de laboratorio en grupo en el tiempo y condiciones establecidas.  
11.2. Plantea una secuencia de actuación optimizando recursos.  
11.3. Toma conciencia del valor del método científico como manera de trabajar rigurosa y sistemática, útil no sólo en el ámbito de las ciencias.  

### Bloque 2. Técnicas generales
1. Conocer y explicar los principales conceptos de solubilidad, destacando su relación inversa con la precipitación.  
1.1. Predice el resultado de experimentos sencillos entre diversas sustancias, atendiendo a su tipo de enlace.  
1.2. Aplica el concepto de Producto de Solubilidad a la preparación de una disolución.  
2. Discriminar cuándo usar cada tipo de filtro atendiendo a las sustancias que se desean separar.  
2.1. Elige el tipo de filtro adecuado a las sustancias que se le proponen.  
3. Conocer y comprender las distintas técnicas para medir el pH de una disolución.  
3.1. Mide el pH de una disolución con una batería de indicadores y con el pH-metro.  
4. Entender la importancia de los tampones en los seres vivos y en los ecosistemas.  
4.1. Prepara un tampón y mide su eficacia ante una variación externa de pH.  
5. Comprender la paridad entre un oxidante y un reductor.  
5.1. Prevé el resultado de una batería de reacciones entre diferentes iones oxidantes o reductores.  
6. Comprender los principales conceptos en las reacciones de oxidación-reducción y relacionarlos con numerosos procesos que ocurren en nuestra vida diaria.  
6.1. Muestra interés por conocer y estudiar algunos procesos redox de nuestra vida diaria y su importancia, tanto a nivel biológico como industrial, en beneficio de la sociedad.  
6.2. Valora el impacto medioambiental que puede producir un mal uso de las sustancias oxidantes o reductoras.  
7. Realizar experiencias sencillas para conocer la concentración de una disolución dada.  
7.1. Calcula la concentración de una disolución mediante una valoración ácido-base.  
7.2. Calcula la concentración de una disolución mediante una volumetría redox.  
8. Investigar la veracidad de los componentes que se muestran en las etiquetas de los productos alimentarios.  
8.1. Diseña un método para confirmar los datos que aparecen en las etiquetas de los productos alimenticios, comprobando su veracidad.  
9. Entender la importancia de los sistemas de control de calidad a la hora de sacar un producto a la venta.  
9.1. Investiga cómo afecta a nuestra vida diaria, la calidad de los productos que consumimos.  

### Bloque 3. Técnicas específicas
1. Comprende los mecanismos que permiten separar diversas sustancias según su polaridad y la del disolvente.  
1.1. Separa e identifica sustancias por cromatografía en capa fina.  
2. Conocer como separar diversos compuestos atendiendo a su distinto punto de ebullición.  
2.1. Entiende el mecanismo seguido para separar distintos compuestos según su punto de ebullición, y el grado máximo de separación que se puede obtener en una destilación.  
3. Domina el concepto de red tridimensional y la diferencia entre sólido amorfo y cristalino.  
3.1. Obtiene redes cristalinas de diversas sales.  
3.2. Predice la composición, estructura y forma de un cristal.  
3.3. Purifica una sustancia mediante el proceso de cristalización y recristalización.  
3.4. Mide el grado de pureza de un cristal a partir de su punto de fusión.  
4. Determinar el orden de una reacción.  
4.1. Obtiene el orden de reacción atendiendo a varios experimentos sencillos.  
4.2. Analiza los factores que afectan a la velocidad de reacción, especialmente la temperatura.  
4.3. Valora la importancia industrial que ha supuesto conocer estos factores a la hora de optimizar los recursos del planeta.  
5. Conocer la secuencia y el orden adecuado para identificar una especie presente en una disolución de varios componentes.  
5.1. Reflexiona críticamente sobre la importancia que tienen los ensayos de identificación de sustancias en medicina, en control de calidad y alimentación o para la policía científica.  
5.2. Relaciona la composición entre los distintos tipos de acero y sus aplicaciones en la vida real.  
6. Llevar a cabo, a escala de laboratorio, la fabricación de fármacos como la aspirina o el trihidróxido de aluminio, siguiendo una secuencia adecuada.  
6.1. Sintetiza un fármaco con la riqueza suficiente para ser admitido por la farmacopea.  
6.2. Evalúa de forma crítica la utilización que la sociedad hace de los fármacos, siendo consciente de los recursos que se consumen en su fabricación y el impacto que los residuos deja en el medio ambiente.  

### Bloque cuatro. Proyecto de investigación en el laboratorio
1. Plantear un proyecto de investigación en el laboratorio.  
1.1 Ejecuta un proceso de investigación de forma coherente, partiendo de una hipótesis y llegando a unas conclusiones.  
1.2 Plantea incógnitas, comprobables científicamente, de interés para nuestra sociedad.  
2. Llevar a cabo un proceso de experimentación en el laboratorio.   
2.1. Extrae datos cuantificables a partir de un experimento o secuencia de ellos.  
2.2. Toma las medidas de precaución adecuadas a la hora de ejecutar una práctica.  
2.3. Trata los residuos producidos de una forma adecuada y segura.  
2.4. Elige los aparatos y equipos óptimos y los utiliza del modo adecuado.  
3. Recoger, ordenar y exponer adecuadamente los datos obtenidos tras un experimento.  
3.1. Tabula y representa los resultados de modo que sean fácilmente manejables, legibles e interpretables.  
3.2. Expone con claridad los resultados numéricos obtenidos y su relación con el objetivo del experimento.  
3.3. Indica los márgenes de error tanto del proceso como de los resultados.  
4. Obtener una conclusión tras una experimentación.  
4.1. Extrae una conclusión que verifique la hipótesis inicial o bien le dirija a una nueva hipótesis.  
5. Hacer una exposición adecuada tras un proceso de investigación.  
5.1. Realiza un informe final de todo el proceso experimental y bibliográfico seguido, así como de sus conclusiones.  
5.2. Valora la pulcritud y el rigor en el trabajo, tanto de laboratorio como teórico.  
 
