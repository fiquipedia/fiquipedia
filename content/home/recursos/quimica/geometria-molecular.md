
# Geometría molecular

Puede haber parte relacionada con [isomería](/home/recursos/quimica/isomeria) y  [biología](/home/recursos/biologia-y-geologia)  molecular, y ciertos tipos de [formulación](/home/recursos/quimica/formulacion).

##  General

 [Symmetry Tutorial. Introduction - symotter.org](https://symotter.org/tutorial/intro)  
 Welcome to the world of symmetry! Symmetry plays an central role in the analysis of the structure, bonding, and spectroscopy of molecules. In this tutorial, we will explore the basic symmetry elements and operations and their use in determining the symmetry classification (point group) of different molecules.

[Molecular Geometry - chemistry.tutorvista.com *waybackmachine*](http://web.archive.org/web/20180126114729/http://chemistry.tutorvista.com/inorganic-chemistry/molecular-geometry.html)  

##  Teoría Repulsión Pares de Electrones de Valencia
Teoría Repulsión Pares de Electrones de Valencia, suele aparecer como RPECV en libros pero tiene otros nombres (TRePEV, teoría RPECV o teoría VSEPR = Valence Shell Electron Pair Repulsion Theory)  
Incluyo dos enlaces a la wikipedia con los diagramas (hay más de los que se suelen ver en bachillerato en clase).  
La página de la wikipedia en inglés es más completa.  
[TREPEV - es.wikipedia.org](http://es.wikipedia.org/wiki/TREPEV)  
[VSEPR theory - en.wikipedia.org](http://en.wikipedia.org/wiki/VSEPR_theory)  
[VSEPR - chemistry-drills.com](http://www.chemistry-drills.com/VSEPR.php)  
Incluyo una imagen resumen, en la wikipedia en español pero con texto en inglés  
![](http://upload.wikimedia.org/wikipedia/commons/a/a9/VSEPR_geometries.PNG "http://upload.wikimedia.org/wikipedia/commons/a/a9/VSEPR_geometries.PNG")  

![](http://www.compoundchem.com/wp-content/uploads/2014/11/VSEPR-Shapes-of-Molecules.png "http://www.compoundchem.com/wp-content/uploads/2014/11/VSEPR-Shapes-of-Molecules.png") 

##  Modelos moleculares

###  Modelos físicos
De madera, de plástico, algunos con imanes ...  
[Modelos Estructuras Moleculares Química Orgánica e Inorgánica - practicaciencia.com](https://practicaciencia.com/juguetes-educativos-y-kits-de-quimica-para-ninos/1062-modelos-estructuras-moleculares-quimica-organica-e-inorganica.html) 

###  Visualización moléculas 3D
Se pueden visualizar en 3 dimensiones, de manera interactiva, buscando el compuesto en  [3DChem.com - Chemistry, Structures & 3D Molecules a visual and interactive website showcasing the beautiful world of chemistry](https://www.3dchem.com/search.html)   
Para amoniaco  [3D structure of NH3](http://www.3dchem.com/3dinorgmolecule.asp?ID=1128)  
Para metano  [3D structure of CH4](http://www.3dchem.com/3dinorgmolecule.asp?ID=143)  
Listado compuestos 3D con carbono  [Collection of structures for the element C](http://www.3dchem.com/element.asp?selected=%20C)   

[Cheminformatic Tools and Databases for Pharmacology. MOLDB. Browse / Search the database by text or by chemical substructure](https://chemoinfo.ipmc.cnrs.fr/MOLDB/search.html)  
[Cheminformatic Tools and Databases for Pharmacology. LEA3D offers a facility to screen or to design small molecules](https://chemoinfo.ipmc.cnrs.fr/LEA3D/index.html)  

[ChemEd DL Chemical Education Digital Library](http://chemeddl.org)  
En septiembre 2021 indica _We are currently migrating (07/07/2021). Please come back soon for more inforamtion on what services are avaiable and the timeline for recovery. We apologize for the inconvenience!_  
[ChemEd DL Chemical Education Digital Library - Sulfuric Acid H2SO4](http://www.chemeddl.org/resources/models360/models.php?pubchem=1118) 
 Permite visualizar 3D de varias maneras, dipolos enlace y molecular. También permite [orbitales moleculares](/home/recursos/orbitales)  
 
[ChemSpider](http://www.chemspider.com/)  
ChemSpider is a free chemical structure database providing fast text and structure search access to over 32 million structures from hundreds of data sources.  
[Structure Search - chemspider.com](http://www.chemspider.com/StructureSearch.aspx)   

[MolView](http://molview.org/)  
MolView is an intuitive, Open-Source web-application to make science and education more awesome! MolView is mainly intended as web-based data visualization platform. You can use MolView to search through different scientific databases including compound databases, protein databases and spectral databases, and view records from these databases as interactive visualizations using WebGL and HTML5 technologies.  

[CoolMolecules: A Molecular Structure Explorer](https://www.stolaf.edu//depts/chemistry/mo/struc/explore.htm)  
Permite buscar por átomos, enlaces, forma ...  
En 2021 indica _All of the 962 structures in the full database are actual, experimentally- determined structures._  

###  Geometría molecular oxácidos
Se incluyen ejemplos con enlaces que muestra Lewis + 3D  
A veces los oxácidos se muestran como combinaciones de óxidos + moléculas de agua, y el origen histórico de algunos nombres provenía de número de moléculas de agua según ciertos métodos de obtención. La realidad es que son compuestos estables porque los átomos dentro de la molécula con los enlaces que se forman tienen las configuraciones adecuadas y creo que lo mejor para nombres tradicionales es memorizar la fórmula asociada al nombre y no su método de obtención.  
Para la misma fórmula hay varias estructuras, intento poner algunas y luego revisar  

La estructura enlaza con [formulacion](/home/recursos/quimica/formulacion) explicando cómo es la molécula y por qué no se puede simplificar a veces (por ejemplo el ácido ciclotrifosfórico)  

HClO  [http://www.chemspider.com/Chemical-Structure.22757.html](http://www.chemspider.com/Chemical-Structure.22757.html)   (hipocloroso)  
HClO<sub>2</sub>  [http://www.chemspider.com/Chemical-Structure.22861.html](http://www.chemspider.com/Chemical-Structure.22861.html)  (cloroso)  
HClO<sub>3</sub>  [http://www.chemspider.com/Chemical-Structure.18513.html](http://www.chemspider.com/Chemical-Structure.18513.html)  (clórico)  
HClO<sub>4</sub>  [http://www.chemspider.com/Chemical-Structure.22669.html](http://www.chemspider.com/Chemical-Structure.22669.html)  (perclórico)  
H<sub>5</sub>IO<sub>6</sub>  [http://www.chemspider.com/Chemical-Structure.23622.html](http://www.chemspider.com/Chemical-Structure.23622.html)  (ortoperyódico)  
H<sub>2</sub>SO<sub>2</sub>  [http://www.chemspider.com/Chemical-Structure.4574173.html](http://www.chemspider.com/Chemical-Structure.4574173.html)  (sin tradicional desde IUPAC 2005)  
H<sub>2</sub>SO<sub>3</sub>  [http://www.chemspider.com/Chemical-Structure.1069.html](http://www.chemspider.com/Chemical-Structure.1069.html)  (sulfuroso)  
H<sub>2</sub>SO<sub>4</sub>  [http://www.chemspider.com/Chemical-Structure.1086.html](http://www.chemspider.com/Chemical-Structure.1086.html)  (sulfúrico)   
H<sub>2</sub>S<sub>2</sub>O<sub>5</sub>  [http://www.chemspider.com/Chemical-Structure.26062.html](http://www.chemspider.com/Chemical-Structure.26062.html)  (disulfuroso)  
H<sub>2</sub>S<sub>2</sub>O<sub>7</sub>  [http://www.chemspider.com/Chemical-Structure.56433.html](http://www.chemspider.com/Chemical-Structure.56433.html)  (disulfúrico)  
H<sub>6</sub>TeO<sub>6</sub>  [http://www.chemspider.com/Chemical-Structure.56436.html](http://www.chemspider.com/Chemical-Structure.56436.html)  (ortotelúrico)  
H<sub>2</sub>N<sub>2</sub>O<sub>2</sub>  [http://www.chemspider.com/Chemical-Structure.55636.html](http://www.chemspider.com/Chemical-Structure.55636.html)  (sin tradicional desde IUPAC 2005)  
HNO<sub>2</sub>  [http://www.chemspider.com/Chemical-Structure.22936.html](http://www.chemspider.com/Chemical-Structure.22936.html)  (nitroso)  
HNO<sub>3</sub>  [http://www.chemspider.com/Chemical-Structure.919.html](http://www.chemspider.com/Chemical-Structure.919.html)  (nítrico)  
HPO<sub>2</sub>  [http://www.chemspider.com/Chemical-Structure.21100.html](http://www.chemspider.com/Chemical-Structure.21100.html)  (sin tradicional desde IUPAC 2005)  
HPO<sub>3</sub>  [http://www.chemspider.com/Chemical-Structure.495.html](http://www.chemspider.com/Chemical-Structure.495.html)  ( [metafosfórico](https://es.wikipedia.org/wiki/%C3%81cido_metafosf%C3%B3rico) , asociado a ciclotrifosfórico (HPO<sub>3</sub>)<sub>3</sub>)  
H<sub>3</sub>PO<sub>3</sub>  [http://www.chemspider.com/Chemical-Structure.97036.html](http://www.chemspider.com/Chemical-Structure.97036.html)  (fosforoso)  
H<sub>3</sub>PO<sub>4</sub>  [http://www.chemspider.com/Chemical-Structure.979.html](http://www.chemspider.com/Chemical-Structure.979.html)  (fosfórico)  
H<sub>4</sub>P<sub>2</sub>O<sub>5</sub>  [http://www.chemspider.com/Chemical-Structure.52083463.html](http://www.chemspider.com/Chemical-Structure.52083463.html)  (disfosforoso)  
H<sub>4</sub>P<sub>2</sub>O<sub>7</sub>  [http://www.chemspider.com/Chemical-Structure.996.html](http://www.chemspider.com/Chemical-Structure.996.html)  (difosfórico)  
H<sub>4</sub>P<sub>2</sub>O<sub>6</sub>  [http://www.chemspider.com/Chemical-Structure.22943.html](http://www.chemspider.com/Chemical-Structure.22943.html)  (hipodisfosfórico)  
H<sub>5</sub>P<sub>3</sub>O<sub>10</sub>  [http://www.chemspider.com/Chemical-Structure.958.html](http://www.chemspider.com/Chemical-Structure.958.html)  (trifosfórico)  
H<sub>2</sub>CO<sub>3</sub>  [http://www.chemspider.com/Chemical-Structure.747.html](http://www.chemspider.com/Chemical-Structure.747.html)  (carbónico)  
H<sub>4</sub>SiO<sub>4</sub>  [http://www.chemspider.com/Chemical-Structure.14236.html](http://www.chemspider.com/Chemical-Structure.14236.html)  (silícico)  
H<sub>2</sub>SiO<sub>3</sub>  [http://www.chemspider.com/Chemical-Structure.14085.html](http://www.chemspider.com/Chemical-Structure.14085.html)  (metasilícico)  
H<sub>6</sub>Si<sub>2</sub>O<sub>7</sub>  [http://www.chemspider.com/Chemical-Structure.55684.html](http://www.chemspider.com/Chemical-Structure.55684.html)  (disilícico)  
H<sub>3</sub>BO<sub>3</sub>  [http://www.chemspider.com/Chemical-Structure.7346.html](http://www.chemspider.com/Chemical-Structure.7346.html)  (bórico)  
HBO<sub>2</sub>  [http://www.chemspider.com/Chemical-Structure.22900.html](http://www.chemspider.com/Chemical-Structure.22900.html)  (metabórico)  
H<sub>2</sub>CrO<sub>4</sub>  [http://www.chemspider.com/Chemical-Structure.22834.html](http://www.chemspider.com/Chemical-Structure.22834.html)  (sin tradicional desde IUPAC 2005)  
H<sub>2</sub>Cr<sub>2</sub>O<sub>7</sub>  [http://www.chemspider.com/Chemical-Structure.24302.html](http://www.chemspider.com/Chemical-Structure.24302.html)  (sin tradicional desde IUPAC 2005)  
H<sub>2</sub>MnO<sub>3</sub>  [http://www.chemspider.com/Chemical-Structure.17616677.html](http://www.chemspider.com/Chemical-Structure.17616677.html)  (sin tradicional desde IUPAC 2005)  
H<sub>2</sub>MnO<sub>4</sub>  [http://www.chemspider.com/Chemical-Structure.4907197.html](http://www.chemspider.com/Chemical-Structure.4907197.html)  (sin tradicional desde IUPAC 2005)  
HMnO<sub>4</sub>  [http://www.chemspider.com/Chemical-Structure.374116.html](http://www.chemspider.com/Chemical-Structure.374116.html)  (sin tradicional desde IUPAC 2005)

### Ejercicios


[Vsepr Theory Worksheet](http://test.elderlaw-newyork.com/vsepr-theory-worksheet.html)  
![](http://worksheets.us/images/hybridization-review-worksheet/hybridization-review-worksheet-11.png)  


###  Geometría molecular con realidad aumentada
Ver en [recursos Realidad Virtual](/home/recursos/recursos-realidad-virtual) 

##  Simulaciones
[![](https://phet.colorado.edu/sims/html/molecule-shapes-basics/latest/molecule-shapes-basics-900.png)](https://phet.colorado.edu/es/simulations/molecule-shapes-basics "Formas de la molécula. Introducción - phet.colorado.edu")  

[![](https://phet.colorado.edu/sims/html/molecule-shapes/latest/molecule-shapes-900.png)](https://phet.colorado.edu/es/simulations/molecule-shapes "Formas de la molécula - phet.colorado.edu")  

[![](https://phet.colorado.edu/sims/html/build-a-molecule/latest/build-a-molecule-900.png)](https://phet.colorado.edu/es/simulations/build-a-molecule "Construye una molécula - phet.colorado.edu")  


