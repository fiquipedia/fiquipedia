
# Seguridad laboratorio

Ademś de normas generales de uso de laboratorio, está relacionado con propiedades sustancias químicas y reacciones químicas concretas   

## Pictogramas  

ECHA (European Chemicals Agency)  
Classification, Labelling and Packaging (CLP)  
[Understanding CLP - echa.europa.eu](https://echa.europa.eu/en/regulations/clp/understanding-clp)  
[CLP Pictograms- echa.europa.eu](https://echa.europa.eu/en/regulations/clp/clp-pictograms)  

United Nations Economic Commission for Europe (UNECE)   
[Globally Harmonized System of Classification and Labelling of Chemicals (GHS) - unece.org](https://unece.org/about-ghs)   
[GHS pictograms - unece.org](https://unece.org/transport/dangerous-goods/ghs-pictograms)  

[![](https://static.wixstatic.com/media/66bfed_681df88e735a4eaaab2e5d16eea8f2ba~mv2.png/v1/fill/w_980,h_577,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/66bfed_681df88e735a4eaaab2e5d16eea8f2ba~mv2.png)](https://drive.google.com/drive/folders/1NDRYYRO93xZiSHmhS1hx6tb1MpbMWmWp)  

[Etiquetado de productos químicos Reglamento CLP A](https://umivaleactiva.es/dam/web-corporativa/Documentos-prevenci-n-y-salud/Riesgo-Qu-mico/9.-Reglamento-CLP-A-2018.pdf)  
[Etiquetado de productos químicos Reglamento ClP B](https://umivaleactiva.es/dam/web-corporativa/Documentos-prevenci-n-y-salud/Riesgo-Qu-mico/10.-Reglamento-CLP-B-2018.pdf)  

[Higiene industrial. Prevención de riesgos laborales. Riesgo químico - umivaleactiva.es](https://umivaleactiva.es/prevencion-y-salud/prevencion/higiene-industrial)  

[PICTOGRAMAS DE PELIGROSIDAD - quimicaweb.net](http://www.quimicaweb.net/ciencia/paginas/laboratorio/pictogramas.html)  

## Reacciones con productos de limpieza
Es algo que enlaza con normas de seguridad y pictogramas 

[Intoxicaciones con productos químicos, ¿qué hacer y cómo prevenirlas? - umivaleactiva.es](https://umivaleactiva.es/blog/prevencion-y-habitos-saludables/noticia-prevencion/dynacontent/intoxicaciones-productos-qu-micos-que-hacer-y-prevenirlas)  

**Productos de limpieza que nunca deben mezclarse**  

![](https://umivaleactiva.es/dam/jcr:2d3117fb-57b1-419a-8bd4-ffa04d866b08/Productos%20de%20limpieza%20que%20nunca%20deben%20mezclarse%20mini.2022-02-17-11-20-41.jpg)  

Lejía y amoniaco. Su combinación produce un gas llamado cloramina. Su inhalación puede causar daños severos en el sistema respiratorio, además de ardor en los ojos.

Alcohol gel y Lejía. Su mezcla produce cloroformo y ácido clorhídrico, ambos muy tóxicos. Inhalar sus vapores puede producir daños en ojos, piel, pulmones, riñones, hígado y sistema nervioso.

Lejía y vinagre. Su mezcla produce gas cloro. Puede provocar quemaduras graves en los ojos y en las vías respiratorias.

Bicarbonato y vinagre. Pueden causar una explosión si los mezclas en un recipiente cerrado.

Vinagre y agua oxigenada. Generan ácido paracético que, en concentraciones altas, irrita y daña la piel, ojos, garganta, nariz y pulmones.

Lejía y agua oxigenada. Forman cloratos o percloratos, que pueden provocar una explosión.

Lejía y agua caliente. Si se diluye lejía en agua caliente, se evapora el cloro y ya no desinfecta, generándose emanaciones que pueden causar intoxicación e irritación de las mucosas.

Lejía y lavavajillas. Produce gas cloro, que puede causar problemas respiratorios y oculares.

[Productos de limpieza que nunca deben mezclarse](https://umivaleactiva.es/dam/web-corporativa/Documentos-prevenci-n-y-salud/Riesgo-Qu-mico/Productos-de-limpieza-que-nunca-deben-mezclarse.pdf)  
[8 medidas para prevenir la intoxicación con productos de limpieza](https://umivaleactiva.es/dam/web-corporativa/Documentos-prevenci-n-y-salud/Riesgo-Qu-mico/Prevenci-n-de-intoxicaciones-con-productos-de-limpieza.pdf)  

## Seguridad con otros productos

[Seguridad de los productos cosméticos. Cuaderno del alumno. - aragon.es](https://www.aragon.es/documents/20127/674325/cosmeticos_alumno.pdf/03b94c4d-c32f-1bad-0d9c-3466b824d1c4)  

[Estudio de los riesgos generados por agentes químicos en la fabricación de explosivos - upct.es](https://repositorio.upct.es/handle/10317/3621)  


