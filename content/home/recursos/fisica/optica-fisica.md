---
aliases:
  - /home/recursos/fisica/recursos-optica-fisica/
---

# Recursos óptica física

La óptica física está dentro del bloque  [óptica](/home/recursos/fisica/recursos-optica)  del  [currículo de física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculofisica2bachillerato) .Ver apuntes elaboración propia en  [recursos física 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato) La separación entre óptica física y geométrica es un criterio personal comentado en  [recursos óptica](/home/recursos/fisica/recursos-optica)   
 A veces puede haber cosas asociables a ambas, como el dioptrio plano tratable de ambas maneras.  
Puede haber recursos relacionados en  [recursos movimiento ondulatorio](/home/recursos/fisica/movimiento-ondulatorio)  y  [recursos espectro](/home/recursos/recursos-espectro)   

## Recursos generales

   * Plataforma e-ducativa aragonesa. Unidades Didácticas ESPAD Física 2º BACHILLERATO. Propagación de la luz  
 ESPAD_BACH_FI2_U5_T1_contenidos  
 [ Tema 1. Propagación de la luz ](http://e-ducativa.catedu.es/44700165/aula/archivos/repositorio//3000/3236/html/index.html) 
   *  [FisicaNet - Fen&oacute;menos ondulatorios de la luz AP11 [F&iacute;sica - Ondas - Optica - Electromagnetismo]](http://www.fisicanet.com.ar/fisica/ondas/ap11_luz.php)   
 Autor: Leandro Bautista
   *  [Diffraction-and-polarization.aspx](http://www.kshitij-school.com/Study-Material/Class-12/Physics/Diffraction-and-polarization.aspx)   
 [Interference-of-light-waves.aspx](http://www.kshitij-school.com/Study-Material/Class-12/Physics/Interference-of-light-waves.aspx)   
 Copyright © 2012-2013 Kshitij Education India Private Limited
   * El físico loco. Óptica  
 [optica.htm](http://elfisicoloco.blogspot.com.es/p/optica.htm)   
 Apuntes. Incluye óptica geométrica. Javier Sánchez
   *  [waves-and-optics](https://www.khanacademy.org/science/physics/waves-and-optics)   
 Clases en vídeo
   * Física 2000. Polarización  
 [index.html](http://www.maloka.org/fisica2000/polarization/index.html) 
  
 [Óptica física, fenómenos ondulatorios, David Matellano](https://mediateca.educa.madrid.org/documentos/cqnb3jvyvayy4chx)  cc-by-nc-sa David Matellano    
 
 [twitter currofisico/status/1409061065774743552](https://twitter.com/currofisico/status/1409061065774743552)  Francisco José Martínez Ruiz (Curro)   
 Chuletillas de óptica 2º bachillerato. La figuras sobredimensionan el concepto de 4 o 5 carillas A4, pero así han quedado. Disculpen gazapos...están en revisión.   
 [OPTICA_CHEAT_SHEET.pdf](https://drive.google.com/file/d/1FjU9AwFAkkjDxK7PCGDG0aMBOeiCV-zO/view)  

### Punto de Aragó/Poisson
 [el-punto-de-poisson-es-la-prueba-de-que.html](http://resolviendolaincognita.blogspot.com.es/2010/12/el-punto-de-poisson-es-la-prueba-de-que.html)   
 [Punto de Arago - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Punto_de_Arago)   

### Medida de la velocidad de la luz
  
 [La determinación de la velocidad de la luz, Ole Rømer y una luna perezosa](https://www.xataka.com/espacio/como-una-luna-perezosa-fue-fundamental-en-la-determinacion-de-la-velocidad-de-la-luz)  
[340.º aniversario de la determinación de la velocidad de la luz - google.com/doodles](https://www.google.com/doodles/340th-anniversary-of-the-determination-of-the-speed-of-light)   

[How Did We Measure The Speed Of Light? - scienceabc.com](https://www.scienceabc.com/innovation/how-did-we-measure-the-speed-of-light.html)  

[![The measurement of the speed of light conducted by Fizeau](https://img.youtube.com/vi/7JpDOhUzkME/0.jpg)](https://www.youtube.com/watch?v=7JpDOhUzkME "The measurement of the speed of light conducted by Fizeau")   
   
[![Measuring the speed of light the old fashioned way: Replicating the Fizeau Apparatus - YouTube](https://img.youtube.com/vi/YMO9uUsjXaI/0.jpg)](https://www.youtube.com/watch?v=YMO9uUsjXaI "Measuring the speed of light the old fashioned way: Replicating the Fizeau Apparatus - YouTube")   

[![Measuring the Speed of Light - YouTube](https://img.youtube.com/vi/Ik5ORaaeaME/0.jpg)](https://www.youtube.com/watch?v=Ik5ORaaeaME "Measuring the Speed of Light - YouTube")   
Students measure the speed of light using a rapidly rotating mirror and a long hallway.  

También se puede medir con un microondas y queso, mediante ondas estacionarias ...

## Propagación curvada de la luz / espejismos
Enlaza con [ilusiones ópticas](home/recursos/fisica/recursos-optica-ilusiones)  

['Hovering ship' photographed off Cornish coast by walker - bbc.com](https://www.bbc.com/news/uk-england-cornwall-56286719)  
![](https://ichef.bbci.co.uk/news/976/cpsprodpb/15EB2/production/_117387798_apex_hovering_ship_illusion_03.jpg)  

[![](https://img.youtube.com/vi/CrgKUFbwNf0/0.jpg)](https://www.youtube.com/watch?v=CrgKUFbwNf0 "Fata Morgana—Boats That Float In The Air")   

Espejismo en inglés es "mirage"  
[Atmospheric Refraction - hyperphysics.phy-astr.gsu.edu](http://hyperphysics.phy-astr.gsu.edu/hbase/atmos/mirage.html)  


## Recursos vídeos / animaciones
 [Light with animations and film clips: Physclips.](http://www.animations.physics.unsw.edu.au/light/)   
 [Physclips - The Nature of Light](http://www.animations.physics.unsw.edu.au/light/nature-of-light/)   
 [Physclips - Interference](http://www.animations.physics.unsw.edu.au/light/interference/)   
 [Physclips - Diffraction](http://www.animations.physics.unsw.edu.au/light/diffraction/)   
 © School of Physics - UNSW (Sydney, Australia). cc-by-nc-nd  
 Lecciones con vídeos y animaciones  
  
Hay imágenes asociadas a refracción con piscinas y personas nadando  
[twitter carloselrojo/status/1293831878168977408](https://twitter.com/carloselrojo/status/1293831878168977408) Incluye vídeo  
En respuesta se cita 
![](https://pbs.twimg.com/media/EfSgHJzX0AAL3Sd?format=jpg)   
Otro ejemplo  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Refraction_of_light_in_a_swimming_pool_with_glass_walls.jpg/800px-Refraction_of_light_in_a_swimming_pool_with_glass_walls.jpg)  
[twitter matthen2/status/1252498504343142401](https://twitter.com/matthen2/status/1252498504343142401)  
an explanation of refraction. When a wave changes its speed and wavelength at a boundary, it must change its direction so that the amplitudes match up at the boundary [EWHDrIcUcAE3AZE.mp4](https://video.twimg.com/tweet_video/EWHDrIcUcAE3AZE.mp4)   

### CMY Cube
CMY Cubes use primary colours Cyan, Magenta & Yellow (CMY)  
[![CMY Cube](https://img.youtube.com/vi/-Z2WyLIu9Jg/0.jpg)](https://www.youtube.com/watch?v=-Z2WyLIu9Jg "CMY Cube")  CMY Cube   

[twitter MsCiencia2/status/1491377879816740870](https://twitter.com/MsCiencia2/status/1491377879816740870)  
Al verter agua en una caja de color amarillo, magenta y cian, por la refracción de la luz creará en su interior un cubo de colores  
La refracción es el fenómeno por el cual la luz que se propaga en forma de onda cambia de velocidad al pasar de un medio a otro  
Vía @ Bryanrojas_M  
(incluye vídeo)  

### Prismas en el metro de Copenhague

[KHR Architecture. Copenhagen Metro Skylight](https://khr.dk/en/projects-3/metro-ovenlight/)  
![](https://khr.dk/wp-content/uploads/2021/09/metro-ovanlys-prisma-768x768.jpg)  
![](https://khr.dk/wp-content/uploads/2021/09/metro-ovanlys-prisma3-768x512.jpg)  

[KHR Architecture. Copenhagen Metro](https://khr.dk/en/projects-3/kobenhavns-metro/)  
> Carefully placed skylights and prisms create a spatial connection between the two worlds above and below ground.  
![](https://khr.dk/wp-content/uploads/2021/09/koebenhavns-metro-wall-prismalight.jpg.webp)  

## Índices de refracción
 [https://refractiveindex.info/](https://refractiveindex.info/)   
 [inrefraccion.pdf](http://www.vaxasoftware.com/doc_edu/fis/inrefraccion.pdf)   
 [Anexo:Índices de refracción - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Anexo:%C3%8Dndices_de_refracci%C3%B3n)   

También se puede asociar la velocidad del sonido en medios a índice de refracción del sonido  
[S107: Velocidad del sonido en diferentes partes del cuerpo humano… ](https://jmirezmedical.wordpress.com/2014/01/26/s107-velocidad-del-sonido-en-diferentes-partes-del-cuerpo-humano/)  

| Tejido / materia | velocidad (m/s) |  
|:-:|:-:|  
|Grasa | 1450 |  
|Tejidos blandos | 1540 |  
|Cerebro | 1549 |  
|Hígado | 1549 |  
|Riñón | 1561 |  
|Músculo | 1585 |  
|Hueso craneal | 4080 |  

[OBTENCIÓN DE LA VELOCIDAD DEL SONIDO EN TEJIDOS MEDIANTE ULTRASONIDOS Y REDES NEURONALES PROFUNDAS](http://www.sea-acustica.es/fileadmin/Faro20/ID33.pdf)  

[Ultrasonidos (US) Diagnóstico por la imagen - uv.es](https://www.uv.es/vifranjo/SIM/temas/SIM_05_US.pdf)  


## Recursos laboratorios no virtuales
Ver en  [recursos de prácticas/experimentos/laboratorio](/home/recursos/practicas-experimentos-laboratorio)   
 GRUPO HEUREMA. EDUCACIÓN SECUNDARIA. ENSEÑANZA DE LA FÍSICA Y LA QUÍMICA  
 Subsección: Apuntes de Física en Bachillerato (16-19 años)   
 Apuntes de óptica con apartado de prácticas de laboratorio  
 [http://heurema.com/ApFBachill9.htm](http://heurema.com/ApFBachill9.htm)   
  
 Algunos asociados a mi clasificación como  [óptica geométrica](/home/recursos/fisica/optica-geometrica) sección: PRÁCTICAS DE FÍSICA  
 MEDIDA DE LA LONGITUD DE ONDA DE LA LUZ EMITIDA POR UN PUNTERO LÁSER  [PF12.htm](http://www.heurema.com/PF12.htm)     
 [Universidad de Valencia. DEMO 67 ÍNDICE DE REFRACCIÓN](https://www.uv.es/piefisic/w3demos/castellano/catalogo/demos/demo67/demo67.pdf)   
 [twitter cossettej/status/1336477756771733505](https://twitter.com/cossettej/status/1336477756771733505)  
 Callie and I have decided to start a video series called "Bedtime Science"   
 Check out our first episode: The Disappearing Test Tube  

[![ADD SUGER, BEND LIGHT - ENGLISH](https://img.youtube.com/vi/PAK_1C-Zqo0/0.jpg)](https://www.youtube.com/watch?v=PAK_1C-Zqo0 "ADD SUGER, BEND LIGHT - ENGLISH")   
[![](http://www.arvindguptatoys.com/arvindgupta/toys/Addsugarbendlight07.jpg)](http://www.arvindguptatoys.com/toys/Addsugarbendlight.html "Add Sugar Bend Light")

[Duna Láctea - madrimasd.org](https://www.madrimasd.org/cienciaysociedad/taller/fisica/optica/duna-lactea/default.asp)  
> Históricamente, llegar a calcular la velocidad de la luz fue bastante complicado. Sin embargo, con un microondas y lonchas de queso, podremos calcularla fácilmente y con muy buena aproximación.  

## Recursos laboratorio virtual / simulaciones  

   * Universidad de colorado  
 Torciendo la luz  [bending-light](http://phet.colorado.edu/es/simulation/bending-light)  Simulación Ley de Snell
   * Snell's Law [Snell&#x27;s law - Wolfram|Alpha](http://www.wolframalpha.com/input/?i=Snell%27s+law&lk=3) 
   * Refraction in a prism [refraction in a prism - Wolfram|Alpha](http://www.wolframalpha.com/input/?i=refraction+in+a+prism&lk=3)   

   * Programas Java de Física  
 Walter Fendt  
 Traducción: Prof. Ernesto Martin Rodriguez, Juan Muñoz, José Miguel Zamarro, Mario Alberto Gómez García  

      * Refracción y Refracción de la Luz  
 [Refracci&oacute;n y Refracci&oacute;n de la Luz](http://www.walter-fendt.de/html5/phes/refraction_es.htm)   
 © Walter Fendt, 20 Diciembre 1997  
 © Traducción: Juan Muñoz, 9 Marzo 1999
      * Reflexión y Refracción de Ondas (Explicación mediante el Principio de Huygens)  
 [Reflexi&oacute;n y Refracci&oacute;n de Ondas](http://www.walter-fendt.de/html5/phes/refractionhuygens_es.htm) 
      * Telescopio Astronómico de Refracción  
 [Telescopio Astron&oacute;mico](http://www.walter-fendt.de/html5/phes/refractor_es.htm)   

      * Interferencia de Luz por una Doble Rendija  
 [Interferencia de Luz por una Doble Rendija](http://www.walter-fendt.de/html5/phes/doubleslit_es.htm)   
 © Walter Fendt, 7 Octubre 2003
      * Difracción de Luz por una Rendija  
 [Difracci&oacute;n de luz por una rendija](http://www.walter-fendt.de/html5/phes/singleslit_es.htm)   
 © Walter Fendt, 11 Octubre 2003  


   * 700 Applets de física y química. Óptica física  
 [610.htm](http://perso.wanadoo.es/oyederra/2btf/610.htm)   
 Recopilación efectuada por Fernando Jimeno Castillo  

   * Enlaces utilizados en la unidad didáctica de óptica  
 [optica-enlaces.html](http://fqcolindres.blogspot.com.es/2013/01/optica-enlaces.html)   
 Gran cantidad de recursos: apuntes, simulaciones  
 Publicado por Jesús L. F. Gallo. Licenciamiento cc-by-nc-sa
   * La luz y sus propiedades. Óptica (Introducción, propiedades)  
 [La luz y sus propiedades | Educaplus](http://www.educaplus.org/luz/index.html)   
 Incluye applets refracción interactivos.  
 © 1998-2012, www.educaplus.org
   * Movimiento ondulatorio > Reflexión y transmisión [Movimiento ondulatorio](http://www.sc.ehu.es/sbweb/fisica_/ondas/ondas.html#reflex)   

      * La ley de Snell de la refracción [La ley de Snell de la refracci&oacute;n](http://www.sc.ehu.es/sbweb/fisica_/ondas/reflex_trans/snell/snell.html) 
      * El principio de Fermat [El principio de Fermat](http://www.sc.ehu.es/sbweb/fisica_/ondas/reflex_trans/snell/snell1.html) 
      * El prisma de vidrio [El prisma de vidrio](http://www.sc.ehu.es/sbweb/fisica_/ondas/reflex_trans/prisma/prisma.html) 
      * El arco iris [El arco iris](http://www.sc.ehu.es/sbweb/fisica_/ondas/reflex_trans/arcoIris/arcoIris.html) 
      * Propagación en medios no homogéneos. Espejismos [Propagaci&oacute;n en medios no homog&eacute;neos. Espejismos](http://www.sc.ehu.es/sbweb/fisica_/ondas/reflex_trans/espejismo/espejismo.html) 
 Movimiento ondulatorio > Interferencia y difracción  [Movimiento ondulatorio](http://www.sc.ehu.es/sbweb/fisica_/ondas/ondas.html#interferencia)   

      * Difracción producida por una rendija [Difracci&oacute;n producida por una rendija](http://www.sc.ehu.es/sbweb/fisica_/ondas/interfer/difraccion/difraccion.html) 
      * Difracción Fraunhofer producida por una abertura rectangular [Difracci&oacute;n Fraunhofer&nbsp;producida por una abertura rectangular](http://www.sc.ehu.es/sbweb/fisica_/ondas/interfer/difraccion1/difraccion1.html) 
      * Difracción Fraunhofer producida por una abertura circular [Difracci&oacute;n Fraunhofer&nbsp;producida por una abertura circular](http://www.sc.ehu.es/sbweb/fisica_/ondas/interfer/difraccion1/difraccion1_1.html) 
      * Varias rendijas, redes de difracción. Interferencia modulada por la difracción [Interferencia modulada por la difracci&oacute;n](http://www.sc.ehu.es/sbweb/fisica_/ondas/interfer/redes/redes.html)   

 Curso Interactivo de Física en Internet © Ángel Franco García
   * Óptica (4º ESO). José Luis San Emeterio Peña. cc-by-sa  
 [ptica](http://newton.cnice.mec.es/materiales_didacticos/optica/optobjetivos.htm)   
 Combina óptica física y geométrica
   *  [optica.htm](http://iesfgcza.educa.aragon.es/depart/fisicaquimica/fisicasegundo/optica.htm)   
 Recursos, animaciones (incluye parte de lo que yo clasifico como  [óptica geométrica](/home/recursos/fisica/optica-geometrica) )  
 Luis Ortiz de Orruño
   * Laboratorio virtual reflexión-refracción  
 [reflexion.swf](http://salvadorhurtado.wikispaces.com/file/view/reflexion.swf)   
 Salvador Hurtado Fernández. Licenciamiento cc-by  

   * Óptica en Bachillerato.  
 [http://acacia.pntic.mec.es/~jruiz27/contenidos.htm](http://acacia.pntic.mec.es/%7Ejruiz27/contenidos.htm)   
 Página realizada por Jesús Ruiz Felipe Profesor de Física del Instituto Cristóbal Pérez Pastor (Tobarra, Albacete)   
 Incluye óptica física y geométrica con algunos applets interactivos  

      * Teoría ondulatoria [ Interference ](http://acacia.pntic.mec.es/~jruiz27/interf/young.htm#2%20Teor%C3%ADa%20ondulatoria) 
      * Propagación de la luz: índice de refracción y camino óptico [reflexión total](http://acacia.pntic.mec.es/~jruiz27/light/refracciones.html) 
      * Reflexión de la luz: Ley de Snell. REFLEXIÓN TOTAL EN UN CHORRO DE AGUA. [reflexión total](http://acacia.pntic.mec.es/~jruiz27/light/refracciones.html#%C2%A05%20Reflexi%C3%B3n%20de%20la%20luz:%20Ley%20de%20Snell.) 
      * Dispersión [arcoiris](http://acacia.pntic.mec.es/~jruiz27/dispersion/arcoiris.html) 

   * PRISMA "Laboratorio virtual" (ondas, óptica física y geométrica)  
 [Laboratorio Virtual](http://enebro.pntic.mec.es/~fmag0006/)   
 Enlaces a simulaciones por niveles y bloques (incluye parte de lo que yo clasifico como  [óptica geométrica](/home/recursos/fisica/optica-geometrica) )  
 Refracción y reflexión  [Refracci&oacute;n y refexi&oacute;n](http://enebro.pntic.mec.es/~fmag0006/op_applet_16.htm)   
 Comprobación experimental de la ley de Snell. Índice de refracción de un líquido  [Óptica  -  Índice de refracción - Applet 1 - Laboratorio Virtual](http://enebro.pntic.mec.es/~fmag0006/op_applet_1.html)   
 Refracción de un tren de ondas luminosas  [Refraccion-applet 4](http://enebro.pntic.mec.es/~fmag0006/op_applet_4.htm)   
 Práctica 1 "La luz en la superficie de separación de dos medios: Reflexión y refracción. Comprobación experimental de la Ley de Snell."  [PRISMA - Práctica 1_0-  Programa de Nuevas Tecnologías  - M.E.C.](http://enebro.pntic.mec.es/~fmag0006/Prism100.html)   
 Práctica 2 ángulo límite "La luz en la superficie de separación de dos medios: Reflexión y refracción. Determinación del ángulo límite de la refracción"  [PRISMA - Práctica 2_0 -  Ángulo límite - Programa de Nuevas Tecnologías](http://enebro.pntic.mec.es/~fmag0006/Prism200.html) 
   * Simulación reflexión total  
 [Demonstration, Total Internal Reflection](http://www.kean.edu/~gkolodiy/physics/optics/totintrefl/)   
 Muy simple © Copyright 1997, Sergey Kiselev and Tanya Yanovsky-Kiselev
   * The Applet Collection  
 [applets.htm](http://lectureonline.cl.msu.edu/~mmp/applist/applets.htm)   
 Wave Phenomena  

      * Interference [cd371.htm](http://lectureonline.cl.msu.edu/~mmp/kap13/cd371.htm) 
      * Reflection, Refraction, and Diffraction [cd372.htm](http://lectureonline.cl.msu.edu/~mmp/kap13/cd372.htm) 
      * Two-Slit Interference [app.htm](http://lectureonline.cl.msu.edu/~mmp/kap27/Gary-Inter2/app.htm) 
      * Diffraction [app.htm](http://lectureonline.cl.msu.edu/~mmp/kap27/Gary-Diffraction/app.htm) 
      * Two-Slit with Diffraction [app.htm](http://lectureonline.cl.msu.edu/~mmp/kap27/Gary-TwoSlit/app.htm) 
      * Doppler Effect [d.htm](http://lectureonline.cl.msu.edu/~mmp/applist/doppler/d.htm) 

   * JOptics curso de óptica  
 [JOptics](http://www.ub.edu/javaoptics/index-es.html)   
 JOptics es un conjunto de recursos docentes dirigidos al aprendizaje de la Óptica Física a nivel universitario en el marco de la licenciatura de Física o la titulación en Óptica y Optometría.  
 Grupo de Innovación Docente en Óptica Física y Fotónica. Los contenidos de esta página están bajo una Licencia Creative Commons y Universitat de Barcelona.  
 El nivel es universitario, pero incluye una **Guía de uso para secundaria de JOptics**, incluyendo   
 [JOptics](http://www.ub.edu/javaoptics/secundaria/Sec_GuiaEs.html)   
 Experimento de Young  [JOptics](http://www.ub.edu/javaoptics/secundaria/Sec_YoungEs.html) 
   * General Physics Java Applets  
 [General Physics Animatons](http://surendranath.tripod.com/Apps.html)   
 Optics. Inglés. Licenciamiento no detallado.
      * Refraction through a prism [Prism.html](http://surendranath.tripod.com/Applets/Optics/Prism/Prism.html) 
      * Young's Double Slit Experiment [DS.html](http://surendranath.tripod.com/Applets/Optics/Slits/DoubleSlit/DS.html)   

      * Single Slit Diffraction [SS.html](http://surendranath.tripod.com/Applets/Optics/Slits/SingleSlit/SS.html)   

      * Double Slit Ineterference and Diffraction [DSID.html](http://surendranath.tripod.com/Applets/Optics/Slits/DoubleSlitID/DSID.html)   

      * Fermat's Principle [Fermat.html](http://surendranath.tripod.com/Applets/Optics/Fermat/Fermat.html) 
      * Huygens's Principle (Wave Front) [WaveFront.html](http://surendranath.tripod.com/Applets/Optics/Huygens/WaveFront.html) 

   * Láminas paralelas  
 Refraction of light at a boundary  
 [refraction2.html](http://schools.matter.org.uk/Content/Refraction/refraction2.html)   
 Materials Teaching Educational Resources © 1999 MATTER Project, The University of Liverpool
   * Reflection/Refraction  
 [JAVA APPLET](http://teleformacion.edu.aytolacoruna.es/FISICA/document/applets/Hwang/ntnujava/light/flashLight.html)   
 (Url de mirror en España)  
 Author¡GFu-Kwun Hwang, Dept. of physics, National Taiwan Normal University

[Refraction - vascak.cz](https://www.vascak.cz/data/android/physicsatschool/template.php?s=kv_lom_vlneni&l=en)  
[Reflection - vascak.cz](https://www.vascak.cz/data/android/physicsatschool/template.php?s=kv_odraz_vlneni&l=en)  
[Reflexión y refracción - fisicalab.com](https://www.fisicalab.com/apartado/reflexion-refraccion-luz) Incluye simulación  
[Onda electromagnética - vascak.cz](https://www.vascak.cz/data/android/physicsatschool/template.php?s=opt_vlna&l=es)  

Por qué la luz puede “desacelerarse” y por qué depende del color | Rompecabezas de óptica 3  
[![](https://img.youtube.com/vi/KTzGBJPuJwM/0.jpg)]((https://www.youtube.com/watch?v=KTzGBJPuJwM "Por qué la luz puede “desacelerarse” y por qué depende del color | Rompecabezas de óptica 3 - 3Blue1Brown")   

Answering refractive index questions from viewers | Optics puzzles 4  
[![](https://img.youtube.com/vi/Cz4Q4QOuoo8/0.jpg)]((https://www.youtube.com/watch?v=Cz4Q4QOuoo8 "Answering refractive index questions from viewers | Optics puzzles 4 - 3Blue1Brown")   



### Huygens

   * Huygens's Principle (Reflection & Refraction) [Huygens.html](http://surendranath.tripod.com/Applets/Optics/Huygens/Huygens.html) 
   * Reflexión y Refracción de Ondas (Explicación mediante el Principio de Huygens) [Reflexi&oacute;n y Refracci&oacute;n de Ondas](https://www.walter-fendt.de/html5/phes/refractionhuygens_es.htm)   

### Difracción
 [practica7.pdf](http://taller2.fisica.edu.uy/Repartidos/practica7.pdf)   
 [APUNTES SOBRE DIFRACCI&#211;N](http://www.unirioja.es/dptos/dq/fa/emo/amplia/amplia.html#ampliacion_red)  [1lw.html](http://www.physics.rutgers.edu/ugrad/labs/1lw.html)   
 [Fraunhofer Single Slit Diffraction](http://hyperphysics.phy-astr.gsu.edu/hbase/phyopt/sinslit.html)   
 [ Single and Double Slit Comparison](http://hyperphysics.phy-astr.gsu.edu/hbase/phyopt/sindoub.html#c1)  [Difraccin producida por una rendija](http://www.sc.ehu.es/sbweb/fisica/ondas/difraccion/difraccion.html)   
 [Interferencia modulada por la difraccin](http://www.sc.ehu.es/sbweb/fisica/ondas/redes/redes.htm)   

[DVD as an Optical Grating - physicstasks.eu](http://physicstasks.eu/1700/dvd-as-an-optical-grating)  
![](http://physicstasks.eu/media/01700/dvd.page.tagged.gif)  
[Diffraction on a CD - physicsexperiments.eu](http://physicsexperiments.eu/1704/diffraction-on-a-cd)  
![](http://physicsexperiments.eu/media/01704/dobra_do_sbirky.page.tagged.jpg)  

Difracción a través de una rendija - LabFisGen
 [![](https://img.youtube.com/vi/Xd1KUBj0NZk/0.jpg)](https://www.youtube.com/watch?v=Xd1KUBj0NZk "Difracción a través de una rendija - LabFisGen")  

[twitter Joaquin_Sevilla/status/1684966591627239430](https://twitter.com/Joaquin_Sevilla/status/1684966591627239430)  
Cacharrismo: hoja de papel albal, dos ranuras hechas con un cúter y laser de 3 €  
![](https://pbs.twimg.com/media/F2I0L1IWYAEl2jh?format=jpg)  
En la pared, el patrón de interferencia esperado. Si Young le antara la cabeza...  
![](https://pbs.twimg.com/media/F2I0ZmuXYAAKu2X?format=jpg)  

[¿Por qué el telescopio James Webb muestra las estrellas con 8 puntas? - bbc.com](https://www.bbc.com/mundo/noticias-62150429)
> La respuesta está en la difracción  
Cuando el James Webb registra la imagen de una estrella, la difracción de la luz (debida a la geometría hexagonal del espejo primario del telescopio) es la causante de un patrón típico en forma de "estrella de ocho puntas".

![](https://ichef.bbci.co.uk/news/754/cpsprodpb/1174A/production/_125889417_jwebb4.jpg)  

[Descubre Cómo Conseguir el Efecto Estrella del Sol en Tus Fotografías](https://www.dzoom.org.es/efecto-sol-estrellado-fotografia/)  
> ¿Alguna vez has visto una fotografía como la de la portada, en la que el sol parece la típica estrella que vemos en los dibujos animados? Ese efecto, conocido con nombres como starburst en inglés o simplemente sol con efecto estrella,...  

> El efecto estrella de una fuente de luz es el resultado de la difracción de la luz.   

> La apertura es sin duda el factor más importante para conseguir el efecto estrellado. Cuanto menor sea la apertura que empleemos (es decir, cuanto mayor sea el número f con el que configuremos la cámara), más evidente será el efecto.  
![](https://www.dzoom.org.es/wp-content/uploads/2019/07/diafragma-efecto-estrella-734x355.jpg)  

> Otro aspecto importante relacionado con las lentes es el número de hojas que tiene el diafragma. No todas las lentes tienen las mismas, y dependiendo del número que tengan saldrán más o menos rayos del efecto estrellado de las fuentes de luz.  
Se tiende a pensar que cuantas más palas tenga la lente, más explosión de estrellas podremos conseguir, pero esto no es del todo cierto.  
![](https://www.dzoom.org.es/wp-content/uploads/2019/07/hojas-diafragma-y-difraccion-734x395.jpg)  

[Why do I see a star-shaped glare around lights? - allaboutvision.com](https://www.allaboutvision.com/symptoms/starburst-lights/)  
> What causes starbursts around lights?  
Many different eye conditions can cause a starburst effect around lights:  
- Cataracts – A clouding of the eye’s natural lens that becomes more common with age. In addition to starbursts, cataracts can cause hazy vision and sensitivity to light.  
- Astigmatism – A vision problem that affects the way the cornea or lens is shaped. Astigmatism causes light to bend the wrong way and can affect your driving at night.  
- Glaucoma – Irreversible damage to the optic nerve caused by high fluid pressure inside the eye. With a timely diagnosis, glaucoma can often be managed before damage occurs or worsens.  
- LASIK eye surgery – The most common laser surgery used to correct refractive errors. Starburst vision can be a temporary side effect or, less commonly, a long-term result of a surgery complication.  


### Polarización

This demo surprised me (a lot) | Barber pole, part 1  
[![](https://img.youtube.com/vi/QCX62YJCmGk/0.jpg)]((https://www.youtube.com/watch?v=QCX62YJCmGk "This demo surprised me (a lot) | Barber pole, part 1 - 3Blue1Brown")   

But what is light? (Barber pole, part 2)  
[![](https://img.youtube.com/vi/aXRTczANuIs/0.jpg)]((https://www.youtube.com/watch?v=aXRTczANuIs "But what is light? (Barber pole, part 2) - 3Blue1Brown")   

   *  [Polarización de la luz | Educaplus](http://www.educaplus.org/luz/polarizacion.html)   
 © 1998-2012, www.educaplus.org

   * Polarizers [http://lectureonline.cl.msu.edu/~mmp/kap24/polarizers/Polarizer.htm](http://lectureonline.cl.msu.edu/%7Emmp/kap24/polarizers/Polarizer.htm) 
   * Polarización [JOptics](http://www.ub.edu/javaoptics/secundaria/Sec_PolarEs.html) 
   * [![Rapid Learning:  Physical Optics - Polarizing of Light - YouTube](https://img.youtube.com/vi/lZ-_i82s16E/0.jpg)](https://www.youtube.com/watch?v=lZ-_i82s16E "Rapid Learning: Physical Optics - Polarizing of Light ")   
   
  [Polarization of Light - fsu.edu](https://micro.magnet.fsu.edu/primer/lightandcolor/polarizedlighthome.html)   

[VARIACIONES DE INTENSIDAD DE LUZ POLARIZADA POR DOS LÁMINAS DE PLÁSTICO - rsefalicante.umh.es](http://rsefalicante.umh.es/LaboratorioLuz/Polarizacion/polarizacion.htm)   





