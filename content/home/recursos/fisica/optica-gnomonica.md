
# Recursos óptica gnomónica

[Gnomónica - wikipedia](https://es.wikipedia.org/wiki/Gnom%C3%B3nica)  
> Gnomónica se denomina a la ciencia encargada de elaborar teorías y reunir conocimiento sobre la división del arco diurno o trayectoria del Sol sobre el horizonte, mediante el empleo de proyecciones específicas sobre superficies. Esta ciencia es muy útil para el diseño y la construcción de los relojes de sol, así como en cartografía (proyección gnomónica).   

También pongo aquí algunos recursos asociados a relojes aunque no sean solares

[Reloj de Sol - wikipedia](https://es.wikipedia.org/wiki/Reloj_de_sol)  

Se puede asociar a óptica física, dentro de [óptica](/home/recursos/fisica/recursos-optica)  

[A31 La óptica y la gnomónica. Asociación Amigos de los Relojes de Sol (AARS) - relojesdesol.info](https://relojesdesol.info/node/841)  

[Shadows Pro](https://www.shadowspro.com/es/index.html)  
Shadows es un programa gratuíto desarrollado por François Blateyron y dirigido al diseño de relojes de sol y astrolabios  

[Solar Clock - nasa.gov](https://sdo.gsfc.nasa.gov/assets/docs/Solar_Clock.pdf)  

[Digital Sundial Mojoptix - thingiverse.com](https://www.thingiverse.com/thing:1068443)  
![](https://cdn.thingiverse.com/renders/e6/59/fe/a2/34/002_display_large.jpg)  

[Reloj de sol de papel. Como hacer un Reloj de Sol - sundialzone.com](https://www.sundialzone.com/es/relojdesol)  

## Otros relojes

[twitter 21xsara/status/1747149815111417905](https://twitter.com/21xsara/status/1747149815111417905)  
Fountain water clock  

[DIY Arduino: relojes increíbles](https://lab.fawno.com/2020/10/20/diy-arduino-relojes-increibles/)

[Mechanical-Digital Steel Ball Clock](https://www.instructables.com/Mechanical-Digital-Steel-Ball-Clock/)  

[Eptaora, un delicioso reloj digital de segmentos](https://www.microsiervos.com/archivo/tecnologia/eptaora-delicioso-reloj-digital-segmentos.html)  
