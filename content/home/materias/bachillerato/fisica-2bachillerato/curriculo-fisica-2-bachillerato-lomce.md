
# Currículo Física 2º Bachillerato (LOMCE)

Esta página es estática y sin enlaces: para ver enlaces a recursos ver Contenidos currículo con enlaces a recursos: Física 2º Bachillerato (LOMCE) (pendiente)Currículo Física 2º Bachillerato LOMCE en normativa  
  
Ver DECRETO 52/2015 en  [legislación](/home/legislacion-educativa)  que indica   
  
*Artículo 9 Materias y currículo*  
El currículo del Bachillerato en los centros docentes de la Comunidad de Madrid se establece del modo siguiente:  
a) Materias del bloque de asignaturas troncales: Los contenidos, criterios de evaluación y estándares de aprendizaje evaluables de las materias del bloque de asignaturas troncales son los del currículo básico fijados para dichas materias en el Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.   
La Administración educativa de la Comunidad de Madrid podrá complementar los contenidos del bloque de materias troncales.  
  
La Comunidad de Madrid no ha complementado contenidos por lo que aplica el Real Decreto 1105/2014  
El pdf del RD 1105/2014 son 378 páginas y no tiene índice: Física 2º de Bachillerato está en las páginas 104 a 110.  
  
**Se marcan en negrita los estándares principales, en matriz de especificaciones** de Orden ECD/1941/2016, de 22 de diciembre, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, para el curso 2016/2017. [http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-12219 ](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-12219)   
[BOE-A-2016-12219.pdf#page=29](http://www.boe.es/boe/dias/2016/12/23/pdfs/BOE-A-2016-12219.pdf#page=29)   
Porcentajes en matriz de especificaciones (suman un 100%, pero está asociado a "al menos el 70%", ver normativa)  
15% = Bloque 1. La actividad científica. Bloque 2. Interacción gravitatoria.   
30% = Bloque 1. La actividad científica. Bloque 3. Interacción electromagnética.   
35% = Bloque 1. La actividad científica. Bloque 4. Ondas. Bloque 5. Óptica geométrica.   
20% = Bloque 1. La actividad científica. Bloque 6. Física del siglo XX.   
  
Órdenes posteriores vuelven a incluir matrices de especificaciones.  


## Comparación para Física de 2º Bachillerato de currículo LOMCE con  [currículo LOE](/home/materias/bachillerato/fisica-2bachillerato/curriculofisica2bachillerato) 

La tabla inicialmente la hago para LOMCE vs LOE, pero la muevo a página separada para incluir en comparación LOMLOE

Ver [comparación currículo Física 2º Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/comparacion-curriculo-fisica-2-bachillerato)

## Introducción
Por su carácter altamente formal, la materia de Física proporciona a los estudiantes una eficaz herramienta de análisis y reconocimiento, cuyo ámbito de aplicación trasciende los objetivos de la misma. La Física en el segundo curso de Bachillerato es esencialmente académica y debe abarcar todo el espectro de conocimiento de la física con rigor, de forma que se asienten las bases metodológicas introducidas en los cursos anteriores. A su vez, debe dotar al alumno de nuevas aptitudes que lo capaciten para su siguiente etapa de formación, con independencia de la relación que esta pueda tener con la Física. El currículo básico está diseñado con ese doble fin.  
El primer bloque de contenidos está dedicado a la actividad científica. Tradicionalmente, el método científico se ha venido impartiendo durante la etapa de ESO y se presupone en los dos cursos de Bachillerato. Se requiere, no obstante, una gradación al igual que ocurre con cualquier otro contenido científico. En la Física de segundo curso de Bachillerato se incluye, en consecuencia, este bloque en el que se eleva el grado de exigencia en el uso de determinadas herramientas como son los gráficos (ampliándolos a la representación simultánea de tres variables interdependientes) y la complejidad de la actividad realizada (experiencia en el laboratorio o análisis de textos científicos).  
Asimismo, la Física de segundo rompe con la estructura secuencial (cinemática–dinámica–energía) del curso anterior para tratar de manera global bloques compactos de conocimiento. De este modo, los aspectos cinemático, dinámico y energético se combinan para componer una visión panorámica de las interacciones gravitatoria, eléctrica y magnética. Esta perspectiva permite enfocar la atención del alumnado sobre aspectos novedosos, como el concepto de campo, y trabajar al mismo tiempo sobre casos prácticos más realistas.  
El siguiente bloque está dedicado al estudio de los fenómenos ondulatorios. El concepto de onda no se estudia en cursos anteriores y necesita, por tanto, un enfoque secuencial. En primer lugar, se trata desde un punto de vista descriptivo y, a continuación, desde un punto de vista funcional. Como casos prácticos concretos se tratan el sonido y, de forma más amplia, la luz como onda electromagnética. La secuenciación elegida (primero los campos eléctrico y magnético, después la luz) permite introducir la gran unificación de la Física del siglo XIX y justificar la denominación de ondas electromagnéticas. La óptica geométrica se restringe al marco de la aproximación paraxial. Las ecuaciones de los sistemas ópticos se presentan desde un punto de vista operativo, con objeto de proporcionar al alumno una herramienta de análisis de sistemas ópticos complejos.  
La Física del siglo XX merece especial atención en el currículo básico de Bachillerato. La complejidad matemática de determinados aspectos no debe ser obstáculo para la comprensión conceptual de postulados y leyes que ya pertenecen al siglo pasado. Por otro lado, el uso de aplicaciones virtuales interactivas suple satisfactoriamente la posibilidad de comprobar experimentalmente los fenómenos físicos estudiados. La Teoría Especial de la Relatividad y la Física Cuántica se presentan como alternativas necesarias a la insuficiencia de la denominada física clásica para resolver determinados hechos experimentales. Los principales conceptos se introducen empíricamente, y se plantean situaciones que requieren únicamente las herramientas matemáticas básicas, sin perder por ello rigurosidad. En este apartado se introducen también los rudimentos del láser, una herramienta cotidiana en la actualidad y que los estudiantes manejan habitualmente.  
La búsqueda de la partícula más pequeña en que puede dividirse la materia comenzó en la Grecia clásica; el alumnado de 2º de Bachillerato debe conocer cuál es el estado actual de uno de los problemas más antiguos de la ciencia. Sin necesidad de profundizar en teorías avanzadas, el alumnado se enfrenta en este bloque a un pequeño grupo de partículas fundamentales, como los quarks, y lo relaciona con la formación del universo o el origen de la masa. El estudio de las interacciones fundamentales de la naturaleza y de la física de partículas en el marco de la unificación de las mismas cierra el bloque de la Física del siglo XX.  
Los estándares de aprendizaje evaluables de esta materia se han diseñado teniendo en cuenta el grado de madurez cognitiva y académica de un alumno en la etapa previa a estudios superiores. La resolución de los supuestos planteados requiere el conocimiento de los contenidos evaluados, así como un empleo consciente, controlado y eficaz de las capacidades adquiridas en los cursos anteriores.  
Esta materia contribuye de manera indudable al desarrollo de las competencias clave: el trabajo en equipo para la realización de las experiencias ayudará a los alumnos a fomentar valores cívicos y sociales; el análisis de los textos científicos afianzará los hábitos de lectura, la autonomía en el aprendizaje y el espíritu crítico; el desarrollo de las competencias matemáticas se potenciará mediante la deducción formal inherente a la física; y las competencias tecnológicas se afianzarán mediante el empleo de herramientas más complejas.  
  
## Currículo Física 2º Bachillerato condicionado por laboratorio, TIC y simulaciones
El currículo cita a veces laboratorio y simulaciones, por lo que si no hay laboratorio (por ejemplo por falta de desdoble es habitual) o no hay posibilidad de un aula de informática para todos los alumnos, hay ciertas partes del currículo que no se pueden cubrir, y se citan aquí.  
  
Introducción   
Por otro lado, el uso de aplicaciones virtuales interactivas suple satisfactoriamente la posibilidad de comprobar experimentalmente los fenómenos físicos estudiados.   
  
Bloque 1   
2.1. Utiliza aplicaciones virtuales interactivas para simular experimentos físicos de difícil implantación en el laboratorio.   
  
Bloque 2   
6.1. Utiliza aplicaciones virtuales interactivas para el estudio de satélites de órbita media (MEO), órbita baja (LEO) y de órbita geoestacionaria (GEO) extrayendo conclusiones.   
  
Bloque 3   
10.2. Utiliza aplicaciones virtuales interactivas para comprender el funcionamiento de un ciclotrón y calcula la frecuencia propia de la carga cuando se mueve en su interior.   
17.1. Emplea aplicaciones virtuales interactivas para reproducir las experiencias de Faraday y Henry y deduce experimentalmente las leyes de Faraday y Lenz.   
  
Bloque 4   
8.1. Experimenta y justifica, aplicando la ley de Snell, el comportamiento de la luz al cambiar de medio, conocidos los índices de refracción.   
15.1. Determina experimentalmente la polarización de las ondas electromagnéticas a partir de experiencias sencillas utilizando objetos empleados en la vida cotidiana.   
  
Estándares que se pueden hacer teóricamente, pero se pueden ver como asociables a laboratorio*17.1. Analiza los efectos de refracción, difracción e interferencia en casos prácticos sencillos.*  
  
Bloque 5   
2.1. Demuestra experimental y gráficamente la propagación rectilínea de la luz mediante un juego de prismas que conduzcan un haz de luz desde el emisor hasta una pantalla.  
  
Bloque 6   
21.1. Realiza y defiende un estudio sobre las fronteras de la física del siglo XXI.  
  
Estándares que se pueden hacer teóricamente, pero se pueden ver como asociables a laboratorio *1.2. Reproduce esquemáticamente el experimento de Michelson-Morley así como los cálculos asociados sobre la velocidad de la luz, analizando las consecuencias que se derivaron.*  
*8.1. Interpreta espectros sencillos, relacionándolos con la composición de la materia.*  


## Bloque 1. La actividad científica


### Contenidos
Estrategias propias de la actividad científica.Tecnologías de la Información y la Comunicación.  


### Criterios de evaluación
1. Reconocer y utilizar las estrategias básicas de la actividad científica.  
2. Conocer, utilizar y aplicar las Tecnologías de la Información y la Comunicación en el estudio de los fenómenos físicos.  


### Estándares de aprendizaje evaluables  

1.1. Aplica habilidades necesarias para la investigación científica, planteando preguntas, identificando y analizando problemas, emitiendo hipótesis fundamentadas, recogiendo datos, analizando tendencias a partir de modelos, diseñando y proponiendo estrategias de actuación.  
1.2. Efectúa el análisis dimensional de las ecuaciones que relacionan las diferentes magnitudes en un proceso físico.  
**1.3. Resuelve ejercicios en los que la información debe deducirse a partir de los datos proporcionados y de las ecuaciones que rigen el fenómeno y contextualiza los resultados.**  
**1.4. Elabora e interpreta representaciones gráficas de dos y tres variables a partir de datos experimentales y las relaciona con las ecuaciones matemáticas que representan las leyes y los principios físicos subyacentes.**  
2.1. Utiliza aplicaciones virtuales interactivas para simular experimentos físicos de difícil implantación en el laboratorio.  
2.2. Analiza la validez de los resultados obtenidos y elabora un informe final haciendo uso de las TIC comunicando tanto el proceso como las conclusiones obtenidas.  
2.3. Identifica las principales características ligadas a la fiabilidad y objetividad del flujo de información científica existente en internet y otros medios digitales.  
**2.4. Selecciona, comprende e interpreta información relevante en un texto de divulgación científica y transmite las conclusiones obtenidas utilizando el lenguaje oral y escrito con propiedad.**  


## Bloque 2. Interacción gravitatoria

### Contenidos
Campo gravitatorio.  
Campos de fuerza conservativos.  
Intensidad del campo gravitatorio.  
Potencial gravitatorio.  
Relación entre energía y movimiento orbital.  
Caos determinista.  

### Criterios de evaluación
1. Asociar el campo gravitatorio a la existencia de masa y caracterizarlo por la intensidad del campo y el potencial.  
2. Reconocer el carácter conservativo del campo gravitatorio por su relación con una fuerza central y asociarle en consecuencia un potencial gravitatorio.  
3. Interpretar las variaciones de energía potencial y el signo de la misma en función del origen de coordenadas energéticas elegido.  
4. Justificar las variaciones energéticas de un cuerpo en movimiento en el seno de campos gravitatorios.  
5. Relacionar el movimiento orbital de un cuerpo con el radio de la órbita y la masa generadora del campo.  
6. Conocer la importancia de los satélites artificiales de comunicaciones, GPS y meteorológicos y las características de sus órbitas.  
7. Interpretar el caos determinista en el contexto de la interacción gravitatoria.  


### Estándares de aprendizaje evaluables  

**1.1. Diferencia entre los conceptos de fuerza y campo, estableciendo una relación entre intensidad del campo gravitatorio y la aceleración de la gravedad.**  
**1.2. Representa el campo gravitatorio mediante las líneas de campo y las superficies de energía equipotencial.**  
**2.1. Explica el carácter conservativo del campo gravitatorio y determina el trabajo realizado por el campo a partir de las variaciones de energía potencial.**  
**3.1. Calcula la velocidad de escape de un cuerpo aplicando el principio de conservación de la energía mecánica.**  
**4.1. Aplica la ley de conservación de la energía al movimiento orbital de diferentes cuerpos como satélites, planetas y galaxias.**  
**5.1. Deduce a partir de la ley fundamental de la dinámica la velocidad orbital de un cuerpo, y la relaciona con el radio de la órbita y la masa del cuerpo.**  
5.2. Identifica la hipótesis de la existencia de materia oscura a partir de los datos de rotación de galaxias y la masa del agujero negro central.  
6.1. Utiliza aplicaciones virtuales interactivas para el estudio de satélites de órbita media (MEO), órbita baja (LEO) y de órbita geoestacionaria (GEO) extrayendo conclusiones.  
7.1. Describe la dificultad de resolver el movimiento de tres cuerpos sometidos a la interacción gravitatoria mutua utilizando el concepto de caos.  


## Bloque 3. Interacción electromagnética


### Contenidos
Campo eléctrico.  
Intensidad del campo.  
Potencial eléctrico.  
Flujo eléctrico y Ley de Gauss. Aplicaciones  
Campo magnético.  
Efecto de los campos magnéticos sobre cargas en movimiento.  
El campo magnético como campo no conservativo.  
Campo creado por distintos elementos de corriente.  
Ley de Ampère.  
Inducción electromagnética  
Flujo magnético.  
Leyes de Faraday-Henry y Lenz. Fuerza electromotriz.  


### Criterios de evaluación
1. Asociar el campo eléctrico a la existencia de carga y caracterizarlo por la intensidad de campo y el potencial.   
2. Reconocer el carácter conservativo del campo eléctrico por su relación con una fuerza central y asociarle en consecuencia un potencial eléctrico.   
3. Caracterizar el potencial eléctrico en diferentes puntos de un campo generado por una distribución de cargas puntuales y describir el movimiento de una carga cuando se deja libre en el campo.   
4. Interpretar las variaciones de energía potencial de una carga en movimiento en el seno de campos electrostáticos en función del origen de coordenadas energéticas elegido.   
5. Asociar las líneas de campo eléctrico con el flujo a través de una superficie cerrada y establecer el teorema de Gauss para determinar el campo eléctrico creado por una esfera cargada.   
6. Valorar el teorema de Gauss como método de cálculo de campos electrostáticos.   
7. Aplicar el principio de equilibrio electrostático para explicar la ausencia de campo eléctrico en el interior de los conductores y lo asocia a casos concretos de la vida cotidiana.   
8. Conocer el movimiento de una partícula cargada en el seno de un campo magnético.   
9. Comprender y comprobar que las corrientes eléctricas generan campos magnéticos.   
10. Reconocer la fuerza de Lorentz como la fuerza que se ejerce sobre una partícula cargada que se mueve en una región del espacio donde actúan un campo eléctrico y un campo magnético.   
11. Interpretar el campo magnético como campo no conservativo y la imposibilidad de asociar una energía potencial.   
12. Describir el campo magnético originado por una corriente rectilínea, por una espira de corriente o por un solenoide en un punto determinado.   
13. Identificar y justificar la fuerza de interacción entre dos conductores rectilíneos y paralelos.   
14. Conocer que el amperio es una unidad fundamental del Sistema Internacional.   
15. Valorar la ley de Ampère como método de cálculo de campos magnéticos.   
16. Relacionar las variaciones del flujo magnético con la creación de corrientes eléctricas y determinar el sentido de las mismas.   
17. Conocer las experiencias de Faraday y de Henry que llevaron a establecer las leyes de Faraday y Lenz.   
18. Identificar los elementos fundamentales de que consta un generador de corriente alterna y su función.  

### Estándares de aprendizaje evaluables  

**1.1.Relaciona los conceptos de fuerza y campo, estableciendo la relación entre intensidad del campo eléctrico y carga eléctrica.**  
**1.2. Utiliza el principio de superposición para el cálculo de campos y potenciales eléctricos creados por una distribución de cargas puntuales**  
**2.1. Representa gráficamente el campo creado por una carga puntual, incluyendo las líneas de campo y las superficies de energía equipotencial.** (orden respecto RD 1105/2014 cambia "equipotencial por "potencial")  
**2.2. Compara los campos eléctrico y gravitatorio estableciendo analogías y diferencias entre ellos.**  
3.1. Analiza cualitativamente la trayectoria de una carga situada en el seno de un campo generado por una distribución de cargas, a partir de la fuerza neta que se ejerce sobre ella.   
**4.1. Calcula el trabajo necesario para transportar una carga entre dos puntos de un campo eléctrico creado por una o más cargas puntuales a partir de la diferencia de potencial.**  
**4.2. Predice el trabajo que se realizará sobre una carga que se mueve en una superficie de energía equipotencial y lo discute en el contexto de campos conservativos.**  
5.1. Calcula el flujo del campo eléctrico a partir de la carga que lo crea y la superficie que atraviesan las líneas del campo.   
6.1. Determina el campo eléctrico creado por una esfera cargada aplicando el teorema de Gauss.   
7.1. Explica el efecto de la Jaula de Faraday utilizando el principio de equilibrio electrostático y lo reconoce en situaciones cotidianas como el mal funcionamiento de los móviles en ciertos edificios o el efecto de los rayos eléctricos en los aviones.   
**8.1. Describe el movimiento que realiza una carga cuando penetra en una región donde existe un campo magnético y analiza casos prácticos concretos como los espectrómetros de masas y los aceleradores de partículas.**  
**9.1. Relaciona las cargas en movimiento con la creación de campos magnéticos y describe las líneas del campo magnético que crea una corriente eléctrica rectilínea.**  
**10.1. Calcula el radio de la órbita que describe una partícula cargada cuando penetra con una velocidad determinada en un campo magnético conocido aplicando la fuerza de Lorentz.**  
10.2. Utiliza aplicaciones virtuales interactivas para comprender el funcionamiento de un ciclotrón y calcula la frecuencia propia de la carga cuando se mueve en su interior.   
**10.3. Establece la relación que debe existir entre el campo magnético y el campo eléctrico para que una partícula cargada se mueva con movimiento rectilíneo uniforme aplicando la ley fundamental de la dinámica y la ley de Lorentz.**  
**11.1. Analiza el campo eléctrico y el campo magnético desde el punto de vista energético teniendo en cuenta los conceptos de fuerza central y campo conservativo.**  
**12.1. Establece, en un punto dado del espacio, el campo magnético resultante debido a dos o más conductores rectilíneos por los que circulan corrientes eléctricas.**  
**12.2. Caracteriza el campo magnético creado por una espira y por un conjunto de espiras.**  
**13.1. Analiza y calcula la fuerza que se establece entre dos conductores paralelos, según el sentido de la corriente que los recorra, realizando el diagrama correspondiente.**  
14.1. Justifica la definición de amperio a partir de la fuerza que se establece entre dos conductores rectilíneos y paralelos.   
15.1. Determina el campo que crea una corriente rectilínea de carga aplicando la ley de Ampère y lo expresa en unidades del Sistema Internacional.   
**16.1. Establece el flujo magnético que atraviesa una espira que se encuentra en el seno de un campo magnético y lo expresa en unidades del Sistema Internacional.**  
**16.2. Calcula la fuerza electromotriz inducida en un circuito y estima la dirección de la corriente eléctrica aplicando las leyes de Faraday y Lenz.**  
17.1. Emplea aplicaciones virtuales interactivas para reproducir las experiencias de Faraday y Henry y deduce experimentalmente las leyes de Faraday y Lenz.   
**18.1. Demuestra el carácter periódico de la corriente alterna en un alternador a partir de la representación gráfica de la fuerza electromotriz inducida en función del tiempo.**  
**18.2. Infiere la producción de corriente alterna en un alternador teniendo en cuenta las leyes de la inducción.**  

## Bloque 4. Ondas


### Contenidos
Clasificación y magnitudes que las caracterizan.   
Ecuación de las ondas armónicas.   
Energía e intensidad.   
Ondas transversales en una cuerda.   
Fenómenos ondulatorios: interferencia y difracción reflexión y refracción.   
Efecto Doppler.   
Ondas longitudinales. El sonido.  
 Energía e intensidad de las ondas sonoras. Contaminación acústica.   
Aplicaciones tecnológicas del sonido.   
Ondas electromagnéticas.   
Naturaleza y propiedades de las ondas electromagnéticas.   
El espectro electromagnético.   
Dispersión. El color.  
Transmisión de la comunicación.  


### Criterios de evaluación
1. Asociar el movimiento ondulatorio con el movimiento armónico simple.   
2. Identificar en experiencias cotidianas o conocidas los principales tipos de ondas y sus características.   
3. Expresar la ecuación de una onda en una cuerda indicando el significado físico de sus parámetros característicos.   
4. Interpretar la doble periodicidad de una onda a partir de su frecuencia y su número de onda.   
5. Valorar las ondas como un medio de transporte de energía pero no de masa.   
6. Utilizar el Principio de Huygens para comprender e interpretar la propagación de las ondas y los fenómenos ondulatorios.   
7. Reconocer la difracción y las interferencias como fenómenos propios del movimiento ondulatorio.   
8. Emplear las leyes de Snell para explicar los fenómenos de reflexión y refracción.   
9. Relacionar los índices de refracción de dos materiales con el caso concreto de reflexión total.   
10. Explicar y reconocer el efecto Doppler en sonidos.   
11. Conocer la escala de medición de la intensidad sonora y su unidad.   
12. Identificar los efectos de la resonancia en la vida cotidiana: ruido, vibraciones, etc.   
13. Reconocer determinadas aplicaciones tecnológicas del sonido como las ecografías, radares, sonar, etc.   
14. Establecer las propiedades de la radiación electromagnética como consecuencia de la unificación de la electricidad, el magnetismo y la óptica en una única teoría.   
15. Comprender las características y propiedades de las ondas electromagnéticas, como su longitud de onda, polarización o energía, en fenómenos de la vida cotidiana.   
16. Identificar el color de los cuerpos como la interacción de la luz con los mismos.   
17. Reconocer los fenómenos ondulatorios estudiados en fenómenos relacionados con la luz.   
18. Determinar las principales características de la radiación a partir de su situación en el espectro electromagnético.   
19. Conocer las aplicaciones de las ondas electromagnéticas del espectro no visible.   
20. Reconocer que la información se transmite mediante ondas, a través de diferentes soportes.  


### Estándares de aprendizaje evaluables
**1.1. Determina la velocidad de propagación de una onda y la de vibración de las partículas que la forman, interpretando ambos resultados.**  
**2.1. Explica las diferencias entre ondas longitudinales y transversales a partir de la orientación relativa de la oscilación y de la propagación.**  
2.2. Reconoce ejemplos de ondas mecánicas en la vida cotidiana.   
**3.1. Obtiene las magnitudes características de una onda a partir de su expresión matemática.**  
**3.2. Escribe e interpreta la expresión matemática de una onda armónica transversal dadas sus magnitudes características.**  
**4.1. Dada la expresión matemática de una onda, justifica la doble periodicidad con respecto a la posición y el tiempo.**  
**5.1. Relaciona la energía mecánica de una onda con su amplitud.**  
**5.2. Calcula la intensidad de una onda a cierta distancia del foco emisor, empleando la ecuación que relaciona ambas magnitudes.**  
**6.1. Explica la propagación de las ondas utilizando el Principio Huygens.**  
**7.1. Interpreta los fenómenos de interferencia y la difracción a partir del Principio de Huygens.**  
**8.1. Experimenta y justifica, aplicando la ley de Snell, el comportamiento de la luz al cambiar de medio, conocidos los índices de refracción.**  
**9.1. Obtiene el coeficiente de refracción de un medio a partir del ángulo formado por la onda reflejada y refractada.**  
**9.2. Considera el fenómeno de reflexión total como el principio físico subyacente a la propagación de la luz en las fibras ópticas y su relevancia en las telecomunicaciones.**  
10.1. Reconoce situaciones cotidianas en las que se produce el efecto Doppler justificándolas de forma cualitativa.   
**11.1. Identifica la relación logarítmica entre el nivel de intensidad sonora en decibelios y la intensidad del sonido, aplicándola a casos sencillos.**  
12.1. Relaciona la velocidad de propagación del sonido con las características del medio en el que se propaga.   
**12.2. Analiza la intensidad de las fuentes de sonido de la vida cotidiana y las clasifica como contaminantes y no contaminantes.**  
13.1. Conoce y explica algunas aplicaciones tecnológicas de las ondas sonoras, como las ecografías, radares, sonar, etc.   
14.1. Representa esquemáticamente la propagación de una onda electromagnética incluyendo los vectores del campo eléctrico y magnético.   
14.2. Interpreta una representación gráfica de la propagación de una onda electromagnética en términos de los campos eléctrico y magnético y de su polarización.   
15.1. Determina experimentalmente la polarización de las ondas electromagnéticas a partir de experiencias sencillas utilizando objetos empleados en la vida cotidiana.   
15.2. Clasifica casos concretos de ondas electromagnéticas presentes en la vida cotidiana en función de su longitud de onda y su energía.   
16.1. Justifica el color de un objeto en función de la luz absorbida y reflejada.   
17.1. Analiza los efectos de refracción, difracción e interferencia en casos prácticos sencillos.   
18.1. Establece la naturaleza y características de una onda electromagnética dada su situación en el espectro.   
**18.2. Relaciona la energía de una onda electromagnética. con su frecuencia, longitud de onda y la velocidad de la luz en el vacío.**  
**19.1. Reconoce aplicaciones tecnológicas de diferentes tipos de radiaciones, principalmente infrarroja, ultravioleta y microondas.**  
19.2. Analiza el efecto de los diferentes tipos de radiación sobre la biosfera en general, y sobre la vida humana en particular.  
19.3. Diseña un circuito eléctrico sencillo capaz de generar ondas electromagnéticas formado por un generador, una bobina y un condensador, describiendo su funcionamiento.   
20.1. Explica esquemáticamente el funcionamiento de dispositivos de almacenamiento y transmisión de la información.  


## Bloque 5 Óptica Geométrica


### Contenidos
Leyes de la óptica geométrica.Sistemas ópticos: lentes y espejos.El ojo humano. Defectos visuales.Aplicaciones tecnológicas: instrumentos ópticos y la fibra óptica.

### Criterios de evaluación
1. Formular e interpretar las leyes de la óptica geométrica.   
2. Valorar los diagramas de rayos luminosos y las ecuaciones asociadas como medio que permite predecir las características de las imágenes formadas en sistemas ópticos.   
3. Conocer el funcionamiento óptico del ojo humano y sus defectos y comprender el efecto de las lentes en la corrección de dichos efectos.  
4. Aplicar las leyes de las lentes delgadas y espejos planos al estudio de los instrumentos ópticos.  


### Estándares de aprendizaje evaluables
**1.1. Explica procesos cotidianos a través de las leyes de la óptica geométrica.**  
2.1. Demuestra experimental y gráficamente la propagación rectilínea de la luz mediante un juego de prismas que conduzcan un haz de luz desde el emisor hasta una pantalla.   
**2.2. Obtiene el tamaño, posición y naturaleza de la imagen de un objeto producida por un espejo plano y una lente delgada realizando el trazado de rayos y aplicando las ecuaciones correspondientes.**  
**3.1. Justifica los principales defectos ópticos del ojo humano: miopía, hipermetropía, presbicia y astigmatismo, empleando para ello un diagrama de rayos.**  
**4.1. Establece el tipo y disposición de los elementos empleados en los principales instrumentos ópticos, tales como lupa, microscopio, telescopio y cámara fotográfica, realizando el correspondiente trazado de rayos.**  
**4.2. Analiza las aplicaciones de la lupa, microscopio, telescopio y cámara fotográfica considerando las variaciones que experimenta la imagen respecto al objeto.**  


## Bloque 6. Física del siglo XX


### Contenidos
Introducción a la Teoría Especial de la Relatividad.  
Energía relativista. Energía total y energía en reposo.  
Física Cuántica.   
Insuficiencia de la Física Clásica.  
Orígenes de la Física Cuántica. Problemas precursores.  
Interpretación probabilística de la Física Cuántica.  
Aplicaciones de la Física Cuántica. El Láser.  
Física Nuclear.  
La radiactividad. Tipos.  
El núcleo atómico. Leyes de la desintegración radiactiva.  
Fusión y Fisión nucleares.  
Interacciones fundamentales de la naturaleza y partículas fundamentales. Las cuatro interacciones fundamentales de la naturaleza: gravitatoria, electromagnética, nuclear fuerte y nuclear débil.  
Partículas fundamentales constitutivas del átomo: electrones y quarks.  
Historia y composición del Universo. Fronteras de la Física.  


### Criterios de evaluación
1. Valorar la motivación que llevó a Michelson y Morley a realizar su experimento y discutir las implicaciones que de él se derivaron.   
2. Aplicar las transformaciones de Lorentz al cálculo de la dilatación temporal y la contracción espacial que sufre un sistema cuando se desplaza a velocidades cercanas a las de la luz respecto a otro dado.   
3. Conocer y explicar los postulados y las aparentes paradojas de la física relativista.   
4. Establecer la equivalencia entre masa y energía, y sus consecuencias en la energía nuclear.   
5. Analizar las fronteras de la física a finales del s. XIX y principios del s. XX y poner de manifiesto la incapacidad de la física clásica para explicar determinados procesos.   
6. Conocer la hipótesis de Planck y relacionar la energía de un fotón con su frecuencia o su longitud de onda.   
7. Valorar la hipótesis de Planck en el marco del efecto fotoeléctrico.   
8. Aplicar la cuantización de la energía al estudio de los espectros atómicos e inferir la necesidad del modelo atómico de Bohr.   
9. Presentar la dualidad onda-corpúsculo como una de las grandes paradojas de la física cuántica.   
10. Reconocer el carácter probabilístico de la mecánica cuántica en contraposición con el carácter determinista de la mecánica clásica.   
11. Describir las características fundamentales de la radiación láser, los principales tipos de láseres existentes, su funcionamiento básico y sus principales aplicaciones.   
12. Distinguir los distintos tipos de radiaciones y su efecto sobre los seres vivos.   
13. Establecer la relación entre la composición nuclear y la masa nuclear con los procesos nucleares de desintegración.   
14. Valorar las aplicaciones de la energía nuclear en la producción de energía eléctrica, radioterapia, datación en arqueología y la fabricación de armas nucleares.   
15. Justificar las ventajas, desventajas y limitaciones de la fisión y la fusión nuclear.   
16. Distinguir las cuatro interacciones fundamentales de la naturaleza y los principales procesos en los que intervienen.   
17. Reconocer la necesidad de encontrar un formalismo único que permita describir todos los procesos de la naturaleza.   
18. Conocer las teorías más relevantes sobre la unificación de las interacciones fundamentales de la naturaleza.   
19. Utilizar el vocabulario básico de la física de partículas y conocer las partículas elementales que constituyen la materia.   
20. Describir la composición del universo a lo largo de su historia en términos de las partículas que lo constituyen y establecer una cronología del mismo a partir del Big Bang.   
21. Analizar los interrogantes a los que se enfrentan los físicos hoy en día.  


### Estándares de aprendizaje evaluables
1.1. Explica el papel del éter en el desarrollo de la Teoría Especial de la Relatividad.   
1.2. Reproduce esquemáticamente el experimento de Michelson-Morley así como los cálculos asociados sobre la velocidad de la luz, analizando las consecuencias que se derivaron.   
2.1. Calcula la dilatación del tiempo que experimenta un observador cuando se desplaza a velocidades cercanas a la de la luz con respecto a un sistema de referencia dado aplicando las transformaciones de Lorentz.   
2.2. Determina la contracción que experimenta un objeto cuando se encuentra en un sistema que se desplaza a velocidades cercanas a la de la luz con respecto a un sistema de referencia dado aplicando las transformaciones de Lorentz.   
**3.1. Discute los postulados y las aparentes paradojas asociadas a la Teoría Especial de la Relatividad y su evidencia experimental.**  
**4.1. Expresa la relación entre la masa en reposo de un cuerpo y su velocidad con la energía del mismo a partir de la masa relativista.**  
**5.1. Explica las limitaciones de la física clásica al enfrentarse a determinados hechos físicos, como la radiación del cuerpo negro, el efecto fotoeléctrico o los espectros atómicos.**  
**6.1. Relaciona la longitud de onda o frecuencia de la radiación absorbida o emitida por un átomo con la energía de los niveles atómicos involucrados.**  
**7.1. Compara la predicción clásica del efecto fotoeléctrico con la explicación cuántica postulada por Einstein y realiza cálculos relacionados con el trabajo de extracción y la energía cinética de los fotoelectrones.**  
8.1. Interpreta espectros sencillos, relacionándolos con la composición de la materia.   
**9.1. Determina las longitudes de onda asociadas a partículas en movimiento a diferentes escalas, extrayendo conclusiones acerca de los efectos cuánticos a escalas macroscópicas.**  
**10.1. Formula de manera sencilla el principio de incertidumbre Heisenberg y lo aplica a casos concretos como los orbítales atómicos.** *(orden respecto RD 1105/2014 corrige tilde "orbitales")*  
11.1. Describe las principales características de la radiación láser comparándola con la radiación térmica.   
11.2. Asocia el láser con la naturaleza cuántica de la materia y de la luz, justificando su funcionamiento de manera sencilla y reconociendo su papel en la sociedad actual.   
**12.1. Describe los principales tipos de radiactividad incidiendo en sus efectos sobre el ser humano, así como sus aplicaciones médicas.**  
**13.1. Obtiene la actividad de una muestra radiactiva aplicando la ley de desintegración y valora la utilidad de los datos obtenidos para la datación de restos arqueológicos.**  
**13.2. Realiza cálculos sencillos relacionados con las magnitudes que intervienen en las desintegraciones radiactivas.**  
**14.1. Explica la secuencia de procesos de una reacción en cadena, extrayendo conclusiones acerca de la energía liberada.**  
**14.2. Conoce aplicaciones de la energía nuclear como la datación en arqueología y la utilización de isótopos en medicina.**  
15.1. Analiza las ventajas e inconvenientes de la fisión y la fusión nuclear justificando la conveniencia de su uso.   
**16.1. Compara las principales características de las cuatro interacciones fundamentales de la naturaleza a partir de los procesos en los que éstas se manifiestan.**  
17.1. Establece una comparación cuantitativa entre las cuatro interacciones fundamentales de la naturaleza en función de las energías involucradas.   
18.1. Compara las principales teorías de unificación estableciendo sus limitaciones y el estado en que se encuentran actualmente.   
18.2. Justifica la necesidad de la existencia de nuevas partículas elementales en el marco de la unificación de las interacciones.   
**19.1. Describe la estructura atómica y nuclear a partir de su composición en quarks y electrones, empleando el vocabulario específico de la física de quarks.**  
19.2. Caracteriza algunas partículas fundamentales de especial interés, como los neutrinos y el bosón de Higgs, a partir de los procesos en los que se presentan.   
20.1. Relaciona las propiedades de la materia y antimateria con la teoría del Big Bang   
**20.2. Explica la teoría del Big Bang y discute las evidencias experimentales en las que se apoya, como son la radiación de fondo y el efecto Doppler relativista.**  
20.3. Presenta una cronología del universo en función de la temperatura y de las partículas que lo formaban en cada periodo, discutiendo la asimetría entre materia y antimateria.   
21.1. Realiza y defiende un estudio sobre las fronteras de la física del siglo XXI.  

