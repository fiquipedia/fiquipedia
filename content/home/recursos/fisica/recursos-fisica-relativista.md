
# Recursos física relativista

La física relativista está dentro del currículo de física de 2º de Bachillerato, donde se ve de manera introductoria, solamente la relatividad especial/restringida  
Miniapuntes de elaboración propia en  [recursos de física de 2º de Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato)   

Enlaza con [Recursos gravitación](/home/recursos/fisica/recursos-gravitacion) por la relatividad general  

## Recursos generales
  
[Relatividad sin fórmulas - eltamiz.com](http://eltamiz.com/relatividad-sin-formulas/) Copyright Pedro Gómez-Esteban  
>Esta serie intenta, a lo largo de sus diez artículos, servir de introducción razonada a la Teoría Especial de la Relatividad de Albert Einstein. Sin utilizar fórmulas, trata de mostrar cómo a partir de los dos supuestos de Einstein, toda su teoría aparece como consecuencia lógica e inevitable.  
  
[RELATIVIDAD ESPECIAL - rsefalicante.umh.es](http://rsefalicante.umh.es/relatividad.htm)  
Los materiales didácticos de este tema, obtuvieron el 1º Premio del Jurado en Ciencia en Acción 2005  

[BERT JANSSEN'S DOCENCIA. Docente en la Universidad de Granada sobre relatividad. Charlas.](https://ugr.es/~bjanssen/docencia.html#charlas)  

[Física universitaria volumen 3 Física Moderna Relatividad Introducción - openstax.org](https://openstax.org/books/f%C3%ADsica-universitaria-volumen-3/pages/5-introduccion)  
© 1999-2024, Rice University. Salvo que se indique lo contrario, los libros de texto de este sitio están autorizados conforme a la Creative Commons Attribution 4.0 International License.   

[La invariancia de la masa en Relatividad Especial - Bert Janssen](https://www.ugr.es/~bjanssen/text/masarelativista.pdf)  
> Se corrige un error que aparece con cierta frecuencia en la literatura divulgativa
sobre la Teoría de la Relatividad Especial: el hecho de que supuestamente la masa
de un objeto en movimiento aumenta con la velocidad relativa entre el observador
y el objeto. Explicaremos en este texto que es más correcto afirmar que lo que
aumenta es el momento y la energía del objeto, mientras que la masa es un invariante,
independientemente del movimiento del observador como del objeto mismo
  
Apuntes "Descubre la Relatividad"  
 [sr.pdf](http://www.ugr.es/~jillana/SR/sr.pdf)   
 José Ignacio Illana, Departamento de Física Teñorica y del Cosmos Universidad de Granada  
  
Berkeley Science Books-Calculus Without Tears  
 [Berkeley Science Books - Special Relativity](http://www.berkeleyscience.com/relativity.htm)     
  
 [Mass and Energy. Conversations About Science with Theoretical Physicist Matt Strassler](http://profmattstrassler.com/articles-and-posts/particle-physics-basics/mass-energy-matter-etc/mass-and-energy/)   
Matt Strassler, 2012, en inglés  
Dos páginas derivadas en español que citan esa fuente  
 [E es igual a m por c al cuadrado | La Ciencia y sus Demonios](http://lacienciaysusdemonios.com/2013/03/24/e-es-igual-a-m-por-c-al-cuadrado/)   
 [Energía, momento y masa | Stargazer](http://dotorqantico.wordpress.com/2012/04/03/energa-momento-y-masa/)   
  
 [Relativity](http://jinkser.com/relativity/relativity.htm)   
 
 [Las Teorías de la Relatividad, explicadas de forma sencilla en estos 8 vídeos - xataka.com](https://www.xataka.com/investigacion/teorias-relatividad-explicada-forma-sencilla-estos-8-videos)  
  
 [Special Relativity: Kinematics: Introduction to Relativistic Kinematics | SparkNotes](http://www.sparknotes.com/physics/specialrelativity/kinematics/summary.html)   
© 2013 SparkNotes LLC, All Rights Reserved  
  
Donald E. Fulton (EE) homepage w/engineering, physics, biology, chemistry & consumer essays  
 [special_relativity.htm](http://twinkle_toes_engineering.home.comcast.net/~twinkle_toes_engineering/special_relativity.htm)   
Inglés. Donald E. Fulton  
  
Minicurso: Relatividad Especial  
 [http://cuentos-cuanticos.com/minicursos-cuentos-cuanticos/minicurso-relatividad-especial/](http://cuentos-cuanticos.com/minicursos-cuentos-cuanticos/minicurso-relatividad-especial/)   
cuentos-cuanticos.com, cc-by-sa  
  
An Introduction to the Special Theory of Relativity  
 [Unit 56](http://astro.physics.sc.edu/selfpacedunits/Unit56.html)   
Copyright 1997/2004 by John L. Safko.  
  
An Introduction to the General Theory of Relativity  
 [Unit 57](http://astro.physics.sc.edu/selfpacedunits/Unit57.html)   
Copyright 1997/2004 by John L. Safko.  
  
 [Teoria de la relatividad especial y general](http://www.relatividad.org/bhole/relatividad.htm)   
© ángel Torregrosa Lillo  
  
 [Special Relativity Animated](http://math.ucr.edu/~jdp/Relativity/SpecialRelativity.html)   
Introduction to the Animations by John de Pillis  
  
GALILEO AND EINSTEIN.   
 [Galileo and Einstein Home Page](http://galileoandeinstein.physics.virginia.edu/)   
Overview and Lecture Index  [Galileo and Einstein: Lecture Index](http://galileoandeinstein.physics.virginia.edu/lectures/lecturelist.html)   
Lecturas en inglés, con algunas animaciones/simulaciones. Parte enlaza con el bloque de   
 [recursos gravitación](/home/recursos/fisica/recursos-gravitacion)  [Michelson-Morley Experiment: Experimento de Michelson-Morley](http://galileoandeinstein.physics.virginia.edu/lectures/Michelson-Morley_Sp.htm) Michael Fowler, UVa  
  
 [Otro Decálogo de Relatividad | Trinoceronte](https://trinoceronte.wordpress.com/2015/11/26/otro-decalogo-de-relatividad/)   
  
Relativity made relatively easy, Andrew Steane  
 [rel_intro.pdf](https://users.physics.ox.ac.uk/~smithb/website/coursenotes/rel_intro.pdf)   
  
 [the-space-doctors-big-idea-einstein-general-relativity](https://www.newyorker.com/tech/elements/the-space-doctors-big-idea-einstein-general-relativity)   
  
 [https://www.canaldeciencias.com/2013/04/12/plan-b-para-enAtender-la-relatividad-i-usando-dibujitos-y-metáforas/](https://www.canaldeciencias.com/2013/04/12/plan-b-para-enAtender-la-relatividad-i-usando-dibujitos-y-met%C3%A1foras/)   
  
¿Por qué Einstein no recibió el Premio Nobel por la teoría de la relatividad?  
 [¿Por qué Einstein no recibió el Premio Nobel por la teoría de la relatividad? - La Ciencia de la Mula Francis](http://francis.naukas.com/2018/03/17/einstein-nobel/)   
  
 [demostracin-emc2](https://es.slideshare.net/EzequielSkorepa1/demostracin-emc2)   
2005.Internacional de la física. Año Einstein. Historia de la ecuación: E = mc2Luis Miralles Conesa Universidad de Valencia [1372397.pdf](https://dialnet.unirioja.es/descarga/articulo/1372397.pdf) DIDÁCTICA DE LAS CIENCIAS EXPERIMENTALES Y SOCIALES. N.º 19. 2005, 75-89 (ISSN 0214-4379)  

[Theory of Relativity explained in 6 images!](https://abhinavppradeep.blogspot.com/2020/05/theory-of-relativity-explained-in-6.html)  
![](https://1.bp.blogspot.com/-KV8jI6IbwWY/XrjdamKqYXI/AAAAAAAAAk0/gQJzOdC5fh4YnpqorKE-O5FBymVOW-JwQCLcBGAsYHQ/s640/WhatsApp%2BImage%2B2020-05-11%2Bat%2B08.28.50.jpeg)  

[¡E=mc² está mal! - fisicatabu.com](https://fisicatabu.com/emc2-esta-mal/)  

[ La falsedad de la "masa relativista". Miriam Pousa - más ciencia por favor](http://mas-ciencia-por-favor.blogspot.com/2017/01/la-falsedad-de-la-masa-relativista.html)  

[What is gravitational lensing? - earthsky.org](https://earthsky.org/space/what-is-gravitational-lensing-einstein-ring/)  

### Serie: Teoría de la invariancia
 [Archivo de serie: Teoría de la invariancia — Cuaderno de Cultura Científica](https://culturacientifica.com/series/teoria-de-la-invariancia/)   

### Transformación de Lorentz
 [Las transformaciones o transformadas de Lorentz, deducci&oacute;n simple y uso](http://www.relatividad.org/bhole/lorentz.html)  [![Lorentz Transformation](https://img.youtube.com/vi/ltrans.html/0.jpg)](https://www.youtube.com/watch?v=ltrans.html)   
 [Lorentz Velocity Transformation](http://hyperphysics.phy-astr.gsu.edu/hbase/relativ/veltran.html)   
 [http://www.physicspages.com/2011/06/22/lorentz-transformations-in-three-dimensions/ ](http://www.physicspages.com/2011/06/22/lorentz-transformations-in-three-dimensions/)   
Chapter 3, The Lorentz transformation  
 [rel_A.pdf](https://users.physics.ox.ac.uk/~smithb/website/coursenotes/rel_A.pdf)   
 
 [28.- Composición de velocidades relativistas - nosemosnaide](https://nosemosnaide.wordpress.com/2018/07/10/28-composicion-de-velocidades-relativistas/)  
 
[twitter BTeseracto/status/1736083777212793204](https://twitter.com/BTeseracto/status/1736083777212793204)  
Sabemos que si conduces a 50km/h y te cruzas por el carril contrario a alguien que también va a 50km/h, su velocidad relativa a ti son 100km/h. Pero ¿qué pasa cuando esa suma parece mayor que la velocidad de la luz? Hablemos de la composición de velocidades relativistas.  
Nada en el Universo, que sepamos, puede superar la velocidad de los fotones en el vacío. Pero eso no nos dice nada acerca de las velocidades relativas. ¿Qué pasa si algo va al 60% de la velocidad de la luz y se cruza con otra cosa que va también al 60%?  
Os adelantamos ya de que no hemos roto la relatividad especial. Podríamos pensar que la suma nos daría un 120% de c o eso nos diría la intuición, pero realmente jamás parecerá que ninguna de ambas viaja a esa velocidad. Porque a la velocidad de la luz se la respeta.  
El tema es que en este caso debemos coger las ideas que nos parecen lógicas y tirarlas a la basura, lo cual a los físicos nos pasa con cierta frecuencia. La lógica nos dice que las velocidades se suman y restan según nuestra velocidad. Estas son las transformaciones de Galileo.  
Esto se creía así, hasta que Einstein nos hizo ver que el tiempo no era una variable completamente independiente como creíamos. Contrariamente a lo que pensábamos, depende de la velocidad del observador. Es decir, las transformaciones de Galileo no son válidas aquí.  
En relatividad no se pueden sumar y restar velocidades sin más para pasar de un sistema de referencia a otro. Esto debe hacerse mediante las transformaciones de Lorentz y no las de Galileo.  
Con esta herramienta, la composición de velocidades empieza a tener mucho más sentido. En esencia son muy similares a las de toda la vida, con la diferencia de un término que se vuelve más y más relevante a velocidades próximas a la luz.  
Así se resuelve el problema de la composición de velocidades, puesto que en este Universo nada, salvo la luz, puede ir a la velocidad de la luz. Volvamos al caso del 60% de c para dar un ejemplo concreto de esto.  
En ese caso concreto no se mediría una velocidad relativa del 1,2c como nos indican las transformaciones de Galileo. Sino de 0,88c. Es decir, un 88% de la velocidad de la luz. Vamos a mostrarlo:  
Empecemos por lo básico, tenemos dos sistemas de referencia, el P y el Q. El Q se mueve con una velocidad u en el eje x, por tanto, su posición será su velocidad por el tiempo que pase. Es decir, u*s. No hay sorpresas aquí.  
Pero para cambiar el sistema de referencia debemos recurrir a las transformaciones de Lorentz, que son la que se muestran en la imagen:  
La ecuación de la derecha hace referencia al tiempo, en concreto a cómo cambia el tiempo de un sistema a otro. La de la izquierda a la posición del sistema de referencia en el eje x. Es muy semejante al caso habitual, con la diferencia de que se añade el término gamma.  
Cómo podéis imaginar, este término gamma es muy recurrente en relatividad, ya que nos relaciona la velocidad de la luz con la del sistema en movimiento, y este es su significado:  
Pues ya tenemos el cambio de sistemas de referencia teniendo en cuenta las correcciones relativistas, ahora solo falta calcular la velocidad para este otro sistema. Y la velocidad no es más que la posición entre el tiempo.  
Con nuestra expresión en la mano, ahora solo es necesario aplicarla al caso en el que ambos se muevan a un 60% de la velocidad de la luz en sentidos opuestos. Aquí está el cálculo:  
Cómo ya decíamos, el resultado es el 88% de la velocidad de la luz.  
Y para los más curiosos un plus. Aquí podemos ver cómo a partir de esta expresión somos capaces de recuperar el resultado que vemos cuando nos subimos a nuestro coche.  

### Relatividad general

 [Relatividad General por todas partes | Siderofilia | SciLogs | Investigación y Ciencia](http://www.investigacionyciencia.es/blogs/astronomia/76/posts/relatividad-general-por-todas-partes-13675)   
Relatividad General por todas partes, artículo que cita varios fenómenos  
  
 [Cronología de un Milagro | Trinoceronte](https://trinoceronte.wordpress.com/cronologia-de-un-milagro/)   
Jorge Zuluaga, asociado a los 100 años, comentando el proceso final  

[Einstein y...la pizarra del Observatorio del Monte Wilson - experientiadocet.com](http://www.experientiadocet.com/2010/12/einstein-yla-pizarra-del-observatorio.html)  
![](http://i121.photobucket.com/albums/o227/Ctome/einstein.jpg)  
![](http://i121.photobucket.com/albums/o227/Ctome/3f50fd206f2fe543a6a8a3e687cf74c3.png)  
>En definitiva, que el pie de foto podría ser “El Dr. Einstein discutiendo (por lo de la interrogación) la posibilidad de que la constante cosmológica sea cero”.
  
[Elementos de relatividad general, Alonso Sepúlveda S, Instituto de Física, Universidad de Antioquia, Medellín, julio 2014, Revisado: noviembre 2015](https://trinoceronte.files.wordpress.com/2015/11/alonsosepulveda-relatividad-edicion2015.pdf)   

La Gravedad NO ES UNA FUERZA | El Principio de Equivalencia - QuantumFracture  
 [![](https://img.youtube.com/vi/7vhc-hMWclY/0.jpg)](https://www.youtube.com/watch?v=7vhc-hMWclY "La Gravedad NO ES UNA FUERZA | El Principio de Equivalencia - QuantumFracture")  
 
  
[Ask Ethan: Is Spacetime Really A Fabric? - forbes.com](https://www.forbes.com/sites/startswithabang/2018/08/11/ask-ethan-is-spacetime-really-a-fabric/#1315a2cf97fc)   
 [![Space - Warped by Gravity - 3D Animation - YouTube](https://img.youtube.com/vi/hH69B0Oc2Og/0.jpg)](https://www.youtube.com/watch?v=hH69B0Oc2Og) 
  
 [![](https://blogs-images.forbes.com/startswithabang/files/2018/08/ezgif-5-014fc9ef71.gif? "") ](https://blogs-images.forbes.com/startswithabang/files/2018/08/ezgif-5-014fc9ef71.gif?)   
What Is The Shape of Space? (ft. PhD Comics) (MinutoDeFísica, vídeo 3 min) [![What Is The Shape of Space? (ft. PhD Comics) - YouTube](https://img.youtube.com/vi/oCK5oGmRtxQ/0.jpg)](https://www.youtube.com/watch?v=oCK5oGmRtxQ)   

[physics.aps.org Relativity Gets Thorough Vetting from LIGO](https://physics.aps.org/articles/v9/52)  
![](https://physics.aps.org/assets/b9706da9-41f5-4bf7-950f-49ba70931f1f/e52_2_medium.png)  

[La danza de una estrella alrededor de un agujero negro vuelve a dar la razón a Einstein](https://astroaventura.net/cosmos/la-danza-de-una-estrella-alrededor-de-un-agujero-negro-vuelve-a-dar-la-razon-a-einstein/amp/)  
 Tras observar durante casi 30 años los movimientos de la estrella S2 orbitando en torno al agujero negro supermasivo del centro de nuestra galaxia, los astrónomos han confirmado que se mueve como predijo la relatividad general de Einstein. Su órbita tiene forma de rosetón, y no de elipse, como decía la teoría de la gravedad de Newton.   
![](https://astroaventura.net/wp-content/uploads/2020/04/La-danza-de-una-estrella-alrededor-de-un-agujero-negro-vuelve-a-dar-la-razon-a-Einstein.jpg)  

[twitter Rainmaker1973/status/1517775286120509440](https://twitter.com/Rainmaker1973/status/1517775286120509440)  
The Einstein field equations are a set of non-linear second order partial differential equations & yet, as they are often written, look very simple. This is because of the compact tensor notation. See what they look like without this notation  
![](https://pbs.twimg.com/media/FQ-iRDGXIAAAzhl?format=png)  

[Einstein Field Equations Fully Written Out: What Do They Look Like Expanded?](https://profoundphysics.com/einstein-field-equations-fully-written-out-what-do-they-look-like-expanded/)  

### Experimento de Hafele-Keating
 [Hafele–Keating experiment - Wikipedia](http://en.wikipedia.org/wiki/Hafele%E2%80%93Keating_experiment)   
  
 [![Hafele-Keating Experiment](https://img.youtube.com/vi/airtim.html/0.jpg)](https://www.youtube.com/watch?v=airtim.html)   
  
Historical photos and information about the Hafele-Keating experiment, Ben Crowell  
 [Historical photos and information about the Hafele-Keating experiment](http://www.lightandmatter.com/article/hafele_keating.html)   

[Los relojes atómicos que dieron la vuelta al mundo para demostrar la teoría de la relatividad - gizmodo.com](https://es.gizmodo.com/los-relojes-atomicos-que-dieron-la-vuelta-al-mundo-para-1783125217)  

### Doppler relativista
 [Ir tan rápido, tan rápido, que la luz roja de un semáforo se vea verde | Microsiervos (Curiosidades)](https://www.microsiervos.com/archivo/curiosidades/rapido-luz-roja-semaforo-verde.html)  [respuestas-xciv-a-que-velocidad-hay-que-conducir-para-ver-verde-la-luz-de-un-semaforo-en-rojo.html](https://cienciadesofa.com/2019/01/respuestas-xciv-a-que-velocidad-hay-que-conducir-para-ver-verde-la-luz-de-un-semaforo-en-rojo.html)   
 [Semáforos siempre verdes y el efecto Doppler relativista, CPI (Curioso pero inútil)](https://curiosoperoinutil.com/2006/03/08/semaforos-siempre-verdes-y-el-efecto-doppler-relativista/) 
 
### Viajar más deprisa que la velocidad de la luz

Enlaza con la [radiación Cherenkov](https://es.wikipedia.org/wiki/Radiaci%C3%B3n_de_Cherenkov)  que es viajar más rápido que la luz en ese medio, no que la velocidad de la luz en el vacío  

[Miguel Alcubierre Moya, empuje por curvatura](https://es.wikipedia.org/wiki/Miguel_Alcubierre#Empuje_por_curvatura)  
> Este "empuje por curvatura" se basa en un modelo matemático que permitiría viajar más rápido que la luz sin violar la relatividad general. En esta idea, construyó un modelo que podría transportar un volumen de espacio plano dentro de una "burbuja" de espacio curvo. Esta "burbuja", llamada espacio hiper-relativista local-dinámico, es impulsada por una expansión local del espacio-tiempo detrás de ella, y una contracción opuesta frente a ella, de modo que teóricamente la nave espacial se pondría en movimiento por las fuerzas generadas por el cambio realizado en el espacio-tiempo. Según cuenta el mismo Alcubierre, la idea se le ocurrió viendo la serie de ciencia ficción Star Trek.7​  


[Sobre la supuesta observación de una burbuja de curvatura de Alcubierre–White. Francisco Villatoro](https://francis.naukas.com/2021/12/10/sobre-la-supuesta-observacion-de-una-burbuja-de-curvatura-de-alcubierre-white/)  
> Lo primero, fabricar una burbuja warp de Alcubierre requiere cantidades de energía descomunales; una burbuja con unos metros de diámetro (para que quepa una persona dentro) necesitaría una energía negativa total equivalente a varias masas solares. Si dicha energía se acumulara en una región de pocos kilómetros acabaría formando un agujero negro. Lo segundo, la burbuja warp solo permite alcanzar velocidades superlumínicas en su interior; para un observador exterior la burbuja se mueve a velocidades sublumínicas. Para viajar a Plutón desde la Tierra habría que usar una burbuja tan grande como la órbita de Plutón.  
...  
En el año 2016 la revista Nature encargó a un escritor de ciencia ficción una pieza sobre el impacto de Star Trek en la ciencia (Sidney Perkowitz, «Science fiction: Boldly going for 50 years,» Nature 537: 165-166 (07 Sep 2016), doi: https://doi.org/10.1038/537165a). Quiso mencionar los motores de curvatura, por lo que le pidió a White un dibujo de su propuesta; Sonny contactó con el ilustrador gráfico Mark Rademaker para preparar esta figura que ilustra la métrica de Alcubierre con una especie de motor formado por un anillo de materia exótica que rodea un cilindro parecido a una nave de Star Trek (Rademaker ha creado espectaculares naves inspiradas en Star Trek, pero fuera del canon de la serie). Desde entonces White incluye dicha imagen en todos sus artículos (citando la pieza en Nature, como si citara un artículo científico, como si diera caché).  
...  
En resumen, la idea de las burbujas de curvatura de Alcubierre–White como medio de transporte interestelar es irrisoria.  

[![](https://img.youtube.com/vi/xH3GV0JLz0E/0.jpg)](https://www.youtube.com/watch?v=xH3GV0JLz0E "Naukas Bilbao 2013: Mario Herrera. Naves de ciencia y no de ficción.")   
A partir de minuto 4:30 habla de la idea de usar curvatura del espacio-tiempo, cita a Alcubierre en 5:25  


## Recursos cuestionarios
La masa relativista. Problema 2º Bachillerato  
 [cuestionario.htm](http://recursostic.educacion.es/newton/web/materiales_didacticos/problema_relatividad/cuestionario.htm)   
J.L. San Emeterio Peña, cc-by-nc-sa  

## TALLER DE RELATIVIDAD PARA ESTUDIANTES DE BACHILLERATO
Sergio Montañez [taller-de-relatividad-para-estudiantes_2.html](http://divulgamadrid.blogspot.com/p/taller-de-relatividad-para-estudiantes_2.html) 

## Recursos didáctica
ERRORES COMUNES SOBRE RELATIVIDAD ENTRE LOS PROFESORES DE ENSEÑANZA SECUNDARIA1ALEMAÑ BERENGUER, R.A.Instituto de Bachillerato Jaime 11. Alicante. ENSEÑANZA DE LAS CIENCIAS, 1997, 15 (3), 301-307  [13271842.pdf](https://core.ac.uk/download/pdf/13271842.pdf)   
LA RELATIVIDAD EN EL BACHILLERATO. UNA PROPUESTA DE UNIDAD DIDÁCTICAAlonso Sánchez, Manuel1 y Soler Selva, Vicent2 IES Leonardo da Vinci de Alicante IES Sixto Marco de Elx ENSEÑANZA DE LAS CIENCIAS, 2006, 24(3), 439–454  
 [02124521v24n3p439.pdf](https://ddd.uab.cat/pub/edlc/02124521v24n3/02124521v24n3p439.pdf) 
 
 [LA TEORÍA DE LA RELATIVIDAD Y SU DIDÁCTICA EN EL BACHILLERATO: ANÁLISIS DE DIFICULTADES Y UNA PROPUESTA DE TRATAMIENTO. HÉCTOR PÉREZ CELADA. UNIVERSITAT DE VALENCIA 2003](https://www.tdx.cat/bitstream/handle/10803/9633/perez.pdf)   


## Recursos vídeos / animaciones  

 [http://www.phys.unsw.edu.au/einsteinlight/](http://www.phys.unsw.edu.au/einsteinlight/)   
© School of Physics - UNSW (Sydney, Australia). cc-by-nc-nd  
Lecciones con vídeos y animaciones  
  
Extended Michelson-Morley Interferometer experiment. English version   
 [![watch](https://img.youtube.com/vi/7T0d7o8X2-E/0.jpg)](https://www.youtube.com/watch?v=7T0d7o8X2-E)   
Vídeo que lo muestra de manera muy gráfica  
Texto asociado:  
 [Extended Michelson-Morley Interferometer Experiment - Sepp Hasslberger](http://blog.hasslberger.com/2009/09/extended_michelsonmorley_inter.html)   
  
Stephen Hawking's Grand Design: Season 1, Episode 2 The Key to the Cosmos  
 [Watch Stephen Hawking's Grand Design Season 1 | Prime Video](http://www.amazon.com/Stephen-Hawkings-Grand-Design-Season/dp/B00ITYXKWE)   
Se trata experimento Michelson-Morley y experimento mental tren  

## Recursos laboratorio virtual / simulaciones
Physlet-based Material described in the May 2004 paper in  
The Physics Teacher: "Teaching Special Relativity Using Physlets®"  
 [default.html](http://webphysics.davidson.edu/physlet_resources/special_relativity/default.html)   
© 2004 by Mario Belloni, Wolfgang Christian, and Tim Gfroerer.  
  
Experimento de Michelson-Morley simulado  
 [Michelson-Morley Experiment](http://galileo.phys.virginia.edu/classes/109N/more_stuff/flashlets/mmexpt6.htm)  (Flash)  
This movie simulates the set-up used in the Michelson-Morley experiment, including the non-existent aether wind they were trying to detect!  
 [Michelson-Morley Experiment](http://galileoandeinstein.physics.virginia.edu/more_stuff/Applets/MichelsonMorley/michelsonmorley.html)  (javascript)  
  
Space-Time Lab. Caltech Physics Applets  
 [http://www.cco.caltech.edu/~phys1/java/phys1/Einstein/Einstein.html](http://www.cco.caltech.edu/%7Ephys1/java/phys1/Einstein/Einstein.html)   
  
Space and Time in Special Relativity  
 [Concept of Space and Time in Special relativity](http://teleformacion.edu.aytolacoruna.es/FISICA/document/applets/Hwang/ntnujava/relativity/relativity.html)   
(Url mirror en España),  
Autor: Fu-Kwun Hwang, Dept. of physics, National Taiwan Normal University  
  
La masa relativista. Problema 2º Bachillerato  
 [La masa relativista](http://recursostic.educacion.es/newton/web/materiales_didacticos/problema_relatividad/evaluacion.html)   
J.L. San Emeterio Peña, cc-by-nc-sa  
(En julio 2014 applet con java 1.7_55 indica "Bad applet class name")  
Incluye cuestionario de evaluación  

[Relativistic Space Sheep - minutelabs.io](http://labs.minutelabs.io/Relativistic-Space-Sheep/)  
We’ve sent the MinutePhysics sheep into space to investigate the effects of non-inertial reference frames! In our case, that’s a fancy way of saying: “accelerating rocket”. You’ll be able to see the Equivalence Principle in action. The Space Sheep will feel like they are under the effects of a gravitational field when the rocket is accelerating.  

### Simultaneidad  

 [http://science.sbcc.edu/~physics/flash/relativity/Simultaniety.swf](http://science.sbcc.edu/%7Ephysics/flash/relativity/Simultaniety.swf)   
David M. Harrison, Dept. of Physics, Univ. of Toronto, Copyright © 2002 - 2011 David M. Harrison, cc-by-nc-sa

### Dilatación tiempo
Ejemplo de Dilatación del Tiempo  
 [Ejemplo de Dilataci&oacute;n del Tiempo](http://www.walter-fendt.de/html5/phes/timedilation_es.htm)   
© Walter Fendt, 15 Noviembre 1997  
© Traducción: Juan Muñoz, 9 Marzo 1999  

### Contracción longitud
 [LengthContraction](http://science.sbcc.edu/~physics/flash/relativity/LengthContraction.html)   
David M. Harrison, Dept. of Physics, Univ. of Toronto, Copyright © 2002 - 2011 David M. Harrison, cc-by-nc-sa  
  
The Physics Classroom » Multimedia Studios » Einstein's Theory of Special Relativity » Length Contraction   
 [lc.cfm](http://www.physicsclassroom.com/mmedia/specrel/lc.cfm)   
© 1996-2014 The Physics Classroom, All rights reserved.   

