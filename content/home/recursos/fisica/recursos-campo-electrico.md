
# Recursos campo eléctrico

Hay algunos recursos que pueden estar mezclados en campo magnético, por ser globalmente electromagnetismo y por compartir ideas como campo o flujo.  
Aunque se hable de campo eléctrico más asociado a Bachillerato, se incluyen también ideas de "electricidad" en general que se trata en ESODe momento coloco aquí junto, aunque puede que vaya separando, por ejemplo lo asociado a ley de Ohm

## Recursos generales
Campo eléctrico, hyperphysics  
 [Electric field](http://hyperphysics.phy-astr.gsu.edu/hbasees/electric/elefie.html#c1)   
 [Gradiente. Campo Eléctrico por Voltaje](http://hyperphysics.phy-astr.gsu.edu/hbasees/electric/efromv.html)  
    
 [Educatina - Física](http://www.educatina.com/fisica/electromagnetismo)   
  
 [Física de 2º de Bachillerato: 5 Campo eléctrostático](http://fisica-2bto.blogspot.com.es/2008/01/campo-elctrico.html)   
  
 [caele.htm](http://iesfgcza.educa.aragon.es/depart/fisicaquimica/fisicasegundo/caele.htm)   
 Animaciones, vídeos  
 Luis Ortiz de Orruño   
  
Inland Empire Physics Education Consulting Group  
 [Gauss's Law](http://sdsu-physics.org/physics180/physics196/Topics/gaussLaw.html)   
Tony DiMaduro  
  
APLICACION DEL TEOREMA DE GAUSS EL CALCULO DE CAMPOS.  
 [campo_electrico](http://fisicayquimicaenflash.es/campoelec/camelec07.htm)   
Ramón Flores-Martínez, cc-by-nc-nd  
  
Estrategias para resolver problemas de campo eléctrico  
Josep Lluís Rodríguez  
 [estrategias_c_electrico_1.html](http://www.unedcervera.com/c3900038/estrategias/estrategias_c_electrico_1.html)  (no operativo en 2020) [estrategias_c_electrico_1.html](https://web.archive.org/web/20170731190405/http://www.unedcervera.com/c3900038/estrategias/estrategias_c_electrico_1.html)   
 [estrategias_c_electrico_2.html](http://www.unedcervera.com/c3900038/estrategias/estrategias_c_electrico_2.html)   
 (no operativo en 2020) [estrategias_c_electrico_2.html](https://web.archive.org/web/20170708140512/http://www.unedcervera.com/c3900038/estrategias/estrategias_c_electrico_2.html)   
(en 2016 la última modificación es en 2003)  
  
La idea de si los cuerpos se tocan, electromagnetismo en el contacto de materiales [When we touch something, what do we actually touch at an atomic level? - Quora](https://www.quora.com/When-we-touch-something-what-do-we-actually-touch-at-an-atomic-level)  [Things Can Actually Touch Each Other - Fact or Myth?](http://factmyth.com/factoids/things-can-actually-touch-each-other/)   

[Collection of Solved Problems in Physics - electricity-and-magnetism - physicstasks.eu ](http://physicstasks.eu/en/physics/electricity-and-magnetism)  
This collection of Solved Problems in Physics is developed by Department of Physics Education, Faculty of Mathematics and Physics, Charles University in Prague since 2006.  
The Collection contains tasks at various level in mechanics, electromagnetism, thermodynamics and optics. The tasks mostly do not contain the calculus (derivations and integrals), on the other hand they are not simple such as the use just one formula. Tasks have very detailed solution descriptions. Try to solve the task or at least some of its parts independently. You can use hints and task analysis (solution strategy).  
Difficulty level  
Level 1 – Lower secondary level   
Level 2 – Upper secondary level   
Level 3 – Advanced upper secondary level   
Level 4 – Undergraduate level    

[Trabajo experimental con potenciales y campos eléctricos Situación de aprendizaje diseñada para la materia de Física de 2.º de Bachillerato - intef](https://descargas.intef.es/recursos_educativos/ODES_SGOA/Bachillerato/FQ/3B.7_-_Campos_elctricos/index.html)  

### Visión sencilla / comparación con gravitación
La variación con masa / distancia / medio y comparación con gravitación se cita en currículo  
[twitter garlan2/status/1389107810798116865](https://twitter.com/garlan2/status/1389107810798116865) Dos interacciones básicas de la naturaleza (entre masas y entre cargas) presentan una simetría casi perfecta al intercambiar masas por cargas o viceversa, sin embargo existen diferencias fundamentales (ver diapositiva) que hacen posible nuestro universo. [![](https://pbs.twimg.com/media/E0cZJWPXIAIyok5?format=png "") ](https://pbs.twimg.com/media/E0cZJWPXIAIyok5?format=png)   
 [![](https://pbs.twimg.com/media/E0cgMUUWQAAIp6A?format=jpg "") ](https://pbs.twimg.com/media/E0cgMUUWQAAIp6A?format=jpg)   

### Jaula de Faraday
 [Michael Faraday: The Invention of Faraday Cage](http://www.juliantrubin.com/bigten/faradaycageexperiments.html)   
 [faraday](http://www.backyardbrains.cl/experiments/faraday)   
 [  El efecto &#8216;jaula de Faraday&#8217; explicado de una manera sencilla y clara](https://blogs.20minutos.es/yaestaellistoquetodolosabe/el-efecto-jaula-de-faraday-explicado-de-una-manera-sencilla-y-clara/)   
Algunos ejemplos de enlaces con textos que la citan junto a aplicaciones:  
 [Una funda para protegerse de las radiaciones del móvil | RTVE.es](http://www.rtve.es/noticias/20130702/joven-espanola-inventa-funda-para-protegerse-radiaciones-del-telefono-movil/703482.shtml) "Las fundas solo van apantalladas por una de las caras, si apantallamos todo se quedaría sin cobertura. Entonces el móvil respira por la otra parte. Es como media **jaula de Faraday**"  
 [Fundas protectoras/bloqueadoras de tarjetas RFID - microsiervos](https://www.microsiervos.com/archivo/gadgets/protector-bloqueador-de-tarjetas-rfid.html)   
 [¿Por qué no ocurre nada cuando cae un rayo sobre un avión?](https://es.gizmodo.com/por-que-no-ocurre-nada-cuando-un-rayo-cae-en-un-avion-1695453796)  [jaula-de-faraday](https://hipertextual.com/2015/09/jaula-de-faraday)  [](https://www.tiempo.com/ram/36255/campana-o-jaula-de-faraday-salvados-contra-los-rayos/)  [](http://www.circulaseguro.com/cuando-cae-un-rayo-sobre-el-coche/)   
 [](https://gesab.com/es/cases/cpd-game/) “Este nivel de seguridad, implicó considerar soluciones para aislamiento electromagnético de los recintos a través de sistema tipo **jaula de Faraday** mediante paneles metálicos, garantizando el aislamiento de la sala IT frente al centro de transformación del edificio, situado en un local adyacente.”  
 [capitulo-j-proteccion-sobretensiones.pdf#page=9](http://acceso.siweb.es/content/26140/Guia_Scheider/capitulo-j-proteccion-sobretensiones.pdf#page=9) La jaula mallada (**jaula Faraday**)Este principio se utiliza para edificios sensibles que alberguen ordenadores o equipos de producción con circuitos integrados. Consiste en la multiplicación simétrica del número de pletinas descendentes fuera del edificio. Se añaden enlaces horizontales si el edificio es alto, por ejemplo, cada dos pisos  
 [Ataque Tempest, el robo de guante blanco - Blog GMV](https://www.gmv.com/blog_gmv/language/es/tempest-el-robo-de-guante-blanco/) “También existen otras técnicas para prevenir ataques de este tipo, como son las **jaulas de Faraday** o los laberintos de radiofrecuencia, donde se forman zonas de completo aislamiento electromagnético basándose en las propiedades de un conductor en equilibrio electrostático.”  
 [](https://unitel-tc.com/apantallamiento-electromagnetico/) “Si buscamos una solución que nos proporciones satisfacer la gran mayoría de los requisitos, optaremos por las **Jaulas de Faraday** modulares.”  
 [Cables detrás de una jaula protectora y un signo de advertencia de alto voltaje Fotografía de stock - Alamy](https://www.alamy.es/foto-cables-detras-de-una-jaula-protectora-y-un-signo-de-advertencia-de-alto-voltaje-144491930.html)   
FARADAY CAGE / FOR HEALTHCARE FACILITIES / MAGNETICALLY SHIELDED [product-115626-778047.html](http://www.medicalexpo.com/prod/itel-telecomunicazioni/product-115626-778047.html)   
 [046.pdf](http://web.archive.org/web/20060324100513/http://www.tno.nl/kwaliteit_van_leven/preventie_en_zorg/kwaliteit_in_de_zorg/faraday_cages_in_health_c/046.pdf)   
 [Ataques Tempest - Security Art Work](https://www.securityartwork.es/2010/03/18/ataques-tempest/) No obstante existen otras medidas de prevención ante ataques Tempest. A continuación explicamos brevemente algunas de ellas:-Jaulas de Faraday y Laberintos de Radiofrecuencia: ...  
 [The Many Myths of EMPs and Faraday Cages | Preparedness Pro](http://www.preparednesspro.com/many-myths-emps-and-faraday-cages)   

## Problemas
  
 [electro.htm](http://www.sc.ehu.es/sbweb/fisica/examenes/electro/electro.htm)  (enlace no operativo en marzo 2020) [electro.htm](https://web.archive.org/web/20070930213558/http://www.sc.ehu.es/sbweb/fisica/examenes/electro/electro.htm)   
  
Sebastián Urrutia Quiroga  
 [](http://web.ing.puc.cl/~sgurruti/)  (no operativo en marzo 2020)  
Electricidad y Magnetismo  
Compilado de problemas resueltos  [http://web.ing.puc.cl/~sgurruti/wp-content/uploads/2015/03/Compilado.pdf](http://web.ing.puc.cl/%7Esgurruti/wp-content/uploads/2015/03/Compilado.pdf) ( [Compilado.pdf](https://web.archive.org/web/20160327091926/http://web.ing.puc.cl/~sgurruti/wp-content/uploads/2015/03/Compilado.pdf) )  
Compliado de problemas selección múltiple   
 [Dropbox - Compilado_Alternativas - Simplify your life](https://www.dropbox.com/sh/5ouwqu87zs0pnrx/AAC_NnrBjRhaosltZ6sexTCka?dl=0)   
UCLMGauss y condensadores  [EVAU Fisica curso 2018_19 modelo cuestiones Gauss y condensadores 181121.pdf - Google Drive](https://drive.google.com/file/d/1NT3dUD4GWqDeHOUYFIH0PFz1z_qHC9fV/view)   
  
 [Ejercicios EyM.pdf](http://www2.fisica.unlp.edu.ar/ionicos/F2CIBEX/practicas%202011/Ejercicios%20EyM.pdf)   
Animaciones de Física 2º Bachillerato en GeoGebra. Ejercicios resueltos con enunciados múltiples y escenas para practicar campo eléctrico. [Física 2º Bachillerato – GeoGebra](https://www.geogebra.org/m/cbnzc8km) Autor:Antonio González García  
  
FÍSICA Y QUÍMICA 1º Bachillerato FÍSICA ElectrostáticaProf. Jorge Rojo Carrascosa  
  
 [electrostatica.pdf](http://www.profesorjrc.es/apuntes/1%20bachillerato/fyq/electrostatica.pdf) 1.6. PROBLEMAS RESUELTOS  

## Vídeos

### Vídeo sobre campo
  
Gravedad vs. Fuerza Eléctrica | Electromagnetismo (1)  
Quantumfracture  
 [![Gravedad vs. Fuerza Eléctrica | Electromagnetismo (1) - YouTube](https://img.youtube.com/vi/IEP5IM4N7GE/0.jpg)](https://www.youtube.com/watch?v=IEP5IM4N7GE)   
El campo: una idea maravillosa | Electromagnetismo (2)  
Quantumfracture [![El Campo: Una Idea Maravillosa | Electromagnetismo (2) - YouTube](https://img.youtube.com/vi/dwZuKaexAJ0/0.jpg)](https://www.youtube.com/watch?v=dwZuKaexAJ0)   
Real World Telekinesis (feat. Neil Turok)  
MinutePhysics  
 [![Real World Telekinesis (feat. Neil Turok) - YouTube](https://img.youtube.com/vi/NMgcX8UNIGY/0.jpg)](https://www.youtube.com/watch?v=NMgcX8UNIGY)   

### Vídeos sobre Gauss
¿Podría estar hueca la Tierra? | La Ley de Gauss   
Quantumfracture  
 [![¿Podría estar hueca la Tierra? | La Ley de Gauss - YouTube](https://img.youtube.com/vi/XyX3pwCjobU/0.jpg)](https://www.youtube.com/watch?v=XyX3pwCjobU)   

### Jaula de Faraday
MIT Physics Demo -- Faraday's Cage (1 min 23 s) [![MIT Physics Demo -- Faraday&#39;s Cage - YouTube](https://img.youtube.com/vi/WqvImbn9GG4/0.jpg)](https://www.youtube.com/watch?v=WqvImbn9GG4) 

### Flujo de energía en cables eléctricos

The Big Misconception About Electricity. Veritasium. Enlaza con campo magnético.  
 [![The Big Misconception About Electricity](https://img.youtube.com/vi/bHIhgxav9LY/0.jpg)](https://www.youtube.com/watch?v=bHIhgxav9LY "The Big Misconception About Electricity")   

### Construcciones asociadas a campo eléctrico

I built an IONIC PLASMA THRUSTER (Best Design)  - Integza  
[![](https://img.youtube.com/vi/mnCmvxt2jn8/0.jpg)](https://www.youtube.com/watch?v=mnCmvxt2jn8 "I built an IONIC PLASMA THRUSTER (Best Design)  - Integza")   

## Recursos laboratorios virtuales / simulaciones
  
Physique et simulations numériques.   
 [Physique et simulation](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/mnelectricite.html)   
 Jean-Jacques ROUSSEAU. Faculté des Sciences exactes et naturelles. Université du Maine  
 Francés. Niveaux : Lycée, premier et second cycles.  

|  [Paires de charges](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/electri/dipole2.html)  |  [Dipôle](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/electri/dipole1.html)  |  [Charges dans un plan](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/electri/chargegene.html)  | 
|  [Ligne bifilaire](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/electri/biligne.html)  |  [Trois charges en triangle](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/electri/tripole.html)  |  [Quatre charges en carré](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/electri/tetrapole.html)  |   
FisLab.net Laboratorio Virtual de Física.   
Electrostática  
 [:: FisLab.net :: Tavi Casellas ::](http://www.xtec.cat/~ocasella/applets/elect/appletsol2.htm)   
Tavi Casellas, cc-by-nc-ndMayo 2018: versión jar ejecutable  [electrostàtica.jar](http://www.xtec.cat/~ocasella/applets/electrost%C3%A0tica.jar)   
  
Funciones básicas de un Osciloscopio  
 [simple oscilloscope](http://teleformacion.edu.aytolacoruna.es/FISICA/document/applets/Hwang/ntnujava/oscilloscope/oscilloscope_s.html)   
(Url mirror en España),   
Autor: Fu-Kwun Hwang, Dept. of physics, National Taiwan Normal University  
Traducción: José Villasuso Gato   
  
Dos cargas eléctricas iguales penden de sendas cuerdas. Se puede variar el valor de las masas y de las cargas y la animación muestra los valores numéricos de las fuerzas (peso, fuerza eléctrica y tensión) para que el sistema esté en equilibrio. También aparece el valor del ángulo y de la distancia.  
 [coulomb.swf](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/coulomb.swf)   
 Página realizada por Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM) - España.  
  
Prácticas de Física 2º Bachillerato  
 [Documento sin t&iacute;tulo](http://fisicayquimicaenflash.es/campos/camposlab00.htm)   
"Campos  
 ELECTRIZACIÓN POR FROTAMIENTO  
 CONSTRUCCION DE UN ELECTROSCOPIO  
 LINEAS EQUIPOTENCIALESGRADIENTE DEL POTENCIAL  
 CONDENSADORES"  
Ramón Flores-Martínez, cc-by-nc-nd  
  
 [IES Al-Ándalus &#8211; 41000557 Arahal (Sevilla)](http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=139&Itemid=199) **Interacción electrostática:** [Laboratorio virtual: Campo electrostático producido por una carga puntual.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/campo_electrostatico_vectores.swf)  [Laboratorio virtual: Líneas de campo electrostático.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/campoelectrico_lineas.swf)  [Laboratorio virtual: Movimiento de una partícula en un campo electrostático constante.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/campoelectricoconstante01.swf) Jose Antonio Navarro Dominguez 

### Visualizar campo eléctrico
[Cargas y Campos - PhET](https://phet.colorado.edu/es/simulation/charges-and-fields)  
[Cargas y Campos - PhET](http://phet.colorado.edu/sims/charges-and-fields/charges-and-fields_es.html)  
![](https://phet.colorado.edu/sims/html/charges-and-fields/latest/charges-and-fields-900.png)  
  
[3-D Electrostatic Field Simulation](http://www.falstad.com/vector3de/) java@falstad.com  
This java applet demonstrates various properties of vector fields. You can select from a number of vector fields and see how particles move if it is treated as either a velocity or a force field. This helps you visualize the field. En 2017 ya no es applet, sino javascript, indica Huge thanks to  [Bob Hanson and his team](https://chemapps.stolaf.edu/swingjs/site/swingjs/examples/)  for help converting this applet to javascript.

### Configuración de cargas
  
En esta animación se representa el vector campo eléctrico creado por cada una de las cargas, para ver el campo eléctrico resultante pulsa *play*. Puedes cambiar el valor y el signo de las cargas; también puedes desplazar las cargas sobre el eje X y el punto P sobre el eje Y.  
 [electro_probl.html](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/electro/electro_probl_files/electro_probl.html)   

### Jaula de Faraday
  
[Electrostática. Jaula de Faraday - montes.upm.es](http://www2.montes.upm.es/dptos/digfa/cfisica/electro/jaula.html)  
[![](http://www2.montes.upm.es/dptos/digfa/cfisica/electro/jaula_files/jaula_anim3.gif "") ](http://www2.montes.upm.es/dptos/digfa/cfisica/electro/jaula_files/jaula_anim3.gif)   


[An electrostatics paradox. Physics Education - iopscience](https://iopscience.iop.org/article/10.1088/1361-6552/ac3eb9/meta)  
> An electric charge located outside a closed metal box does not produce an electric field inside the box. On the other hand, an electric charge located inside the box can generate an electric field outside the box. A charge inside the box can therefore exert a force on a charge outside the box, but not vice-versa, in apparent contradiction of Newton's third law.  

[An electrostatics paradox + Comment on ‘An electrostatics paradox’ (pdf) - usna.edu](https://www.usna.edu/Users/physics/mungan/_files/documents/Publications/PhysEd12.pdf)  

### Movimiento de cargas
Permite ver solamente eléctrico / magnético o combinarlos  
 [thomson.swf](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/thomson.swf) Cathode Ray Tube  
 [Tim's Cathode Ray Tube Applet](http://www.physics.uq.edu.au/people/mcintyre/applets/cathoderaytube/crt.html)   
Tim McIntyre. The University of Queensland, 2004 

### Gauss
Ley de Gauss, permite comprobar el flujo para un área cerrada que se puede desplazar sobre una configuración de cargas dada  
 [Gauss' law](http://dl.uncw.edu/digilib/physics/x_physlets_01/physletprob/ch6_tutorial/6.3.fig_49.html)   

### Flujo
[Electric Flux - davidson.edu _waybackmachine_](http://web.archive.org/web/20140429094222/http://webphysics.davidson.edu/physlet_resources/bu_semester2/c03_flux.html)    
Simulación donde aparece superficie plana, campo uniforme y flujo pudiendo variar el ángulo que forman  
 [PHYS208 Flux Animation](http://www.physics.udel.edu/~watson/phys208/flux-animation.html)   

### Conceptos básicos electricidad
PhET interactive simulationes. Universidad de Colorado  
Electrización: electricidad estática con globos [balloons-and-static-electricity](https://phet.colorado.edu/es/simulation/balloons-and-static-electricity)  [balloons](https://phet.colorado.edu/es/simulation/balloons)   
Carga y descarga de electricidad estática de una persona [john-travoltage](https://phet.colorado.edu/es/simulation/john-travoltage)   
  
Conductores y aislantes, circuitos de continua: permite poner objetos y comprobar si conducen [circuit-construction-kit-dc](https://phet.colorado.edu/es/simulation/legacy/circuit-construction-kit-dc)   
 [circuit-construction-kit-dc-virtual-lab_es.html](https://phet.colorado.edu/sims/html/circuit-construction-kit-dc-virtual-lab/latest/circuit-construction-kit-dc-virtual-lab_es.html)   
  
Juego que permite visualizar ideas: el objetivo es "meter gol" con una partícula cargada colocando otrasHockey eléctrico  [electric-hockey](http://phet.colorado.edu/es/simulation/electric-hockey)   

### Ley de Ohm, resistencia
PhET interactive simulationes. Universidad de Colorado  
 [Resistencia en un alambre - phet-colorado.edu](https://phet.colorado.edu/es/simulation/resistance-in-a-wire)   
 ![](https://phet.colorado.edu/sims/html/resistance-in-a-wire/latest/resistance-in-a-wire-900.png)  
 
 [Ley de Ohm - phet-colorado.edu](https://phet.colorado.edu/es/simulation/ohms-law)   
 ![](https://phet.colorado.edu/sims/html/ohms-law/latest/ohms-law-900.png)  

 [Ley de Ohm - phet.colorado.edu (CheerpJ)](https://phet.colorado.edu/es/simulations/battery-resistor-circuit)   
 ![](https://phet.colorado.edu/sims/battery-resistor-circuit/battery-resistor-circuit-600.png)  
 

