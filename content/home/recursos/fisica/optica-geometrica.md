
# Recursos óptica geométrica

La óptica geométrica está dentro del bloque [óptica](/home/recursos/fisica/recursos-optica)  del  [currículo de física de 2º de Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/curriculofisica2bachillerato) .  
Ver [apuntes elaboración propia en recursos física 2º Bachillerato](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato)   

También puede enlazar con radio, por ejemplo asociado a antenas parabólicas.  
En algunos casos enlaza con [ilusiones ópticas](/home/recursos/fisica/recursos-optica-ilusiones) (hologramas) 


## Normativa sobre óptica geométrica
Ni el currículo LOE ni el LOMCE mencionan una norma concreta. Personalmente siempre he utilizado la norma DIN, pero me han comentado que hay otra "norma de Eugene Hecht"; mirando he visto que tiene libros, pero no lo veo como normativa como tal. [Hecht Zajac OCR Ch. 5 geometric optics -- paraxial theory.pdf](http://physics111.lib.berkeley.edu/Physics111/Reprints/Optics%20Hecht%20&%20Zajac/Hecht%20Zajac%20OCR%20Ch.%205%20geometric%20optics%20--%20paraxial%20theory.pdf)   
Sí que incluyo referencias a documentos que sí citaban norma DIN (*Optica Paraxial, 1962, Autor:Casas Peláez, Justiniano*)  

En Fisica Universitaria 12va. Edicion Sears, Zemansky Vol. 2, , ISBN 978-607-442-304-4, página 1159 se indican unas reglas sin mencionar ninguna normativa concreta  

> Reglas de signos  
Antes de seguir adelante, conviene presentar algunas reglas generales de signos. Éstas
quizá parezcan innecesariamente complicadas con respecto al caso simple de una imagen formada por un espejo plano; no obstante, nos proponemos expresar las reglas de
una forma que sea aplicable a todas las situaciones que encontraremos más adelante.
Éstas incluyen la formación de imágenes por una superﬁcie reﬂectante o refractiva,
plana o esférica, o por un par de superﬁcies refractivas que forman una lente. Las reglas
son las siguientes:  
>1. Regla de signos para la distancia de objeto: cuando el objeto está del mismo
lado de la superﬁcie reﬂectante o refractiva que la luz entrante, la distancia de
objeto s es positiva; en caso contrario, es negativa.  
>2. Regla de signos para la distancia de imagen: cuando la imagen está del mis-
mo lado de la superﬁcie reﬂectante o refractiva que la luz saliente, la distancia
de imagen sr es positiva; en caso contrario, es negativa.  
>3. Regla de signos para el radio de curvatura de una superﬁcie esférica:
cuando el centro de curvatura C está del mismo lado que la luz saliente, el radio
de curvatura es positivo; en caso contrario, es negativo.  


## Norma DIN óptica geométrica: 1335 2003-12  

Al tratar la teoría de óptica geométrica se habla de "normas DIN", pero he encontrado poca documentación que diga qué norma en concreto es, para ver la fuente original y validar las normas que se encuentran en libros. Decir norma DIN sin decir más es decir bien poco:  [DIN](http://es.wikipedia.org/wiki/DIN)  es acrónimo de Deutsches Institut für Normung, a veces erróneamente interpretado como Deutsche Industrie Norm ó Das Ist Norm. Todos conocemos una norma DIN, que es la  [DIN 476](http://es.wikipedia.org/wiki/DIN_476)  que fija tamaños normalizados de papel y que da nombre al "DIN A-4", de modo que decir norma DIN sin decir más es decir bien poco.  
La norma que realmente se cita es **"DIN 1335 Geometrische Optik - Bezeichnungen und Definitionen"**, (Óptica geométrica - Nomenclatura y definiciones) cuya última edición es de diciembre de 2003. Es un documento en alemán de 13 páginas que se puede obtener en pdf, pagando,  [en la web de DIN](http://www.nafuo.din.de/cmd?artid=65594676&contextid=nafuo&bcrumblevel=1&subcommitteeid=54768047&level=tpl-art-detailansicht&committeeid=54738899&languageid=en)   
La norma al describir las reglas básicas menciona DIN 1315 1982-08 Winkel; Begriffe, Einheiten (Ángulo; Conceptos, Unidades)  
No se pueden reproducir libremente debido a  [condiciones de copyright](http://www.din.de/cmd?level=tpl-unterrubrik&menuid=47561&cmsareaid=47561&menurubricid=59384&cmsrubid=59384&menusubrubid=59400&cmssubrubid=59400&languageid=en) , e incluso se aclara su uso educativo en  [Reproduction of DIN Standards for educational purposes](http://www.beuth.de/sixcms_upload/media/2247/Notice_of_copyright_4_2011.pdf) , donde se marcan en negrita frases que dejan claro que los estándares DIN no pueden reproducirse libremente:  
  
>Special terms and conditions:  
Educational purposes exist where a state or state-recognized educational establishment undertakes reproduction in order to provide students with educational material.  
>1 **A fee is due for all reproductions,** irrespective of their format, which reflect the original text of a DIN Standard either wholly or partially, and **irrespective of whether the text is given verbatim or substantially unaltered.** Examples of insubstantial modifications are the inclusion of the name of the educational establishment or a reference number, and the omission or modification of the frame enclosing the text.  
>2 In granting permission to make reproductions, DIN does not assume liability for their accuracy. Reproduction is strictly limited to the number of copies for which permission was originally granted. Any subsequent reproduction shall require a renewed application for reproduction and the permission of DIN.  
>3 If the copies are distributed to the pupils or students on loan and are returned to the teacher on completion of the work or at the end of term, DIN will demand no fee for reproduction.  
4 **If the copies are to remain the property of the students, the reproduction fee per copy will be 20 % of the price of the DIN Standard.** The fee for the reproduction of extracts is calculated as a proportion of the total number of pages which the document comprises.  
  
Viendo esto, y en especial la frase "**irrespective of whether the text is given verbatim or substantially unaltered**" lo que sí parece claro es que, con modificaciones sustanciales del texto, se puede reproducir con fines educativos: en general en los libros de texto suele haber un "resumen de normas / convenios DIN".  
  
Es también muy relevante que la norma indica en su portada: **Frühere Ausgaben (Ediciones Anteriores) DIN 1335: 1933-12, 1958-05, 1975-07, 1983-06.**  
Lo que quiere decir que la primera versión es nada menos que del año 1933: hasta donde sé los derechos de autor de la primera edición, pasados 70 años para Alemania, ya serían de dominio público (ver Germany en  [wikipedia.org List of countries' copyright lengths](http://en.wikipedia.org/wiki/List_of_countries%27_copyright_length) )  
  
La normativa es tan antigua, que se pueden encontrar referencias de su incorporación al sistema educativo en bachillerato en España en el curso 1960-61, en este enlace de la Red de Información Educativa del Ministerio de Educación  [Óptica paraxial](http://redined.mecd.gob.es/xmlui/handle/11162/73691)  (enlace validado en 2015)  
  
>*Optica Paraxial, 1962, Autor:Casas Peláez, Justiniano*  
Resumen: Se analiza la óptica paraxial dentro del sistema educativo español en la década de los años sesenta. Al ponerse en vigor con el curso 1960-61 las nuevas normas sobre Preuniversitario, a él pasó la Óptica geométrica, que se venía explicando en el Selectivo de las facultades de Ciencias. **La disposición al efecto señala que su explicación se debe hacer utilizando la notación contenida en las normas DIN 1,335**. Pero los escasos libros españoles de Óptica apenas han usado estas normas, por lo que se realizó este estudio a modo de compendio. Se señala que no se tiene la pretensión de que estos apuntes puedan ser la sustitución de un libro de texto del Bachillerato, ya que son demasiado densos para los alumnos. Se deja a los profesores la tarea de exponerla de forma adecuada a los alumnos, de ese nivel y edad. Por tanto el contenido es meramente matemático, y se obvia cualquier observación de tipo didáctico. Si se presenta el contenido acompañado de numerosas demostraciones matemáticas y experimentos. De hecho se considera imprescindible que los alumnos vean, acompañando a la explicación del profesor en clase, una colección de experiencias para que se den cuenta de que las cuestiones que se explican corresponden a realidades físicas. En líneas generales los principales puntos tratados son la óptica geométrica, los sistemas ópticos centrados, la óptica paraxial de los sistemas centrados, las ecuaciones de correspondencia y los sistemas compuestos  

## Normativa sobre símbolos en óptica paraxial
Se suelen utilizar unos símbolos de lentes, dioptrios y espejos en óptica paraxial, y me gustaría citar de dónde vienen esos símbolos, que he visto muchas veces, pero no conozco referencias.   
La norma DIN 1335 original y no tiene nada sobre eso.  
Unos ejemplos  
[twitter FiQuiPedia/status/715607370139889664](https://twitter.com/FiQuiPedia/status/715607370139889664)   
[![Símbolos óptica paraxial](https://pbs.twimg.com/media/Ce5ZHA2WsAA65YS.jpg:large "Símbolos óptica paraxial") ](https://pbs.twimg.com/media/Ce5ZHA2WsAA65YS.jpg:large)   
Agradezco a Jaume Escofet i Soteras, autor de materiales en  [Exercicis-Problemes](http://ocw.upc.edu/curs/26438/Exercicis-Problemes) , esta información que comparto, junto con el documento.   
"A mi parecer no existe una normativa oficial respecto a los símbolos lo que induce a confusiones.  
 Adjunto a este mail te envio  [un documento](/home/recursos/fisica/optica-geometrica/2016-04-04-JaumeEscofet-Optica%20Paraxial.pdf)  donde intento aclararte su procedencia (sobretodo en mi caso)"  

## Recursos elaboración propia  

Ver apuntes elaboración propia en  [recursos física 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato)  (apuntes y resolución ejercicios PAU)  
En diciembre 2013 se publica en agrega un REA elaborado por mí  
Ficha:  [Recurso Educativo Abierto de Óptica Geométrica para Física de 2º de Bachillerato](http://agrega.educacion.es/ODE2/es/es_2013120212_9122007)   
Previsualización:  [![](http://agrega.educacion.es/repositorio/02122013/0a/es_2013120212_9122007/lentes.png)](http://agrega.educacion.es/visualizar/es/es_2013120212_9122007/false)   

## Programas dibujo  
* [pst-optic – Drawing optics diagrams](https://ctan.org/pkg/pst-optic)  (es lo que uso en solucionario EvAU)  
* [damienBloch / inkscape-raytracing](https://github.com/damienBloch/inkscape-raytracing)  

## Recursos generales  

   * Alternativas en la introducción de conceptos de Óptica en BUP y COU (1992)  
 [Alternativas en la introducci&oacute;n de conceptos de &Oacute;ptica en BUP y COU](http://redined.mecd.gob.es/xmlui/handle/11162/84608)  (enlace validado en 2015)  
Resumen objetivo: Estudiar los efectos experimentales sobre el aprendizaje de tratamientos basados en la consideración de las ideas previas y/o la resolución de cuestiones cualitativas apropiadas y/o la utilización de unidades didácticas, sobre Óptica geométrica y física, a nivel medio, intentando lograr un aprendizaje lo más significativo posible en el sentido ausubeliano. Autores: Puey Bernués, María Lucía / Casas Peláez, Justiniano   

   * [Manual de óptica geométrica. Julio V. Santos Benito. Universidad de Alicante I.S.B.N. : 84-89522-99-5 (pdf 475 páginas)](https://rua.ua.es/dspace/bitstream/10045/136450/1/Santos-Benito-Manual-de-Optica-Geometrica.pdf)  

   * Óptica  
 [mcgrautop.htm](http://www.gobiernodecanarias.org/educacion/3/Usrn/fisica/mcgrautop.htm)   
Incluye óptica física y óptica geométrica. Apuntes y ejercicios. Licenciamiento no detallado. Páginas elaboradas por J. Trujillo (I.B. Joaquín Artiles)
   * Hojas resumen óptica geométrica  
 [iesleonardodavinci.net](http://fisicayquimica.iesleonardodavinci.net/index.php?option=com_content&task=view&id=147&Itemid=31)   
Departamento Física y Química IES Leonardo da Vinci, Majadahonda, Madrid.
   * Óptica  
 [optica.pdf](http://www.miraralcielo.com/malet%C3%ADn%20web/apuntes/optica.pdf)   
Apuntes. Copyright © Matías Vázquez Dereitos Reservados ISBN: 978-84-690-9821-9
   * Basic Geometrical Optics. SPIE Fundamentals of Photonics Module 1.3  
 [00 STEP Module 03.pdf](http://spie.org/Documents/Publications/00%20STEP%20Module%2003.pdf)   
Autor Leno S. Pedrotti para SPIE "Internacional society for optics and photonics", inglés. Licenciamiento no detallado pero indica "SPIE is providing free and open access to this material as a service to the optics community and the general public."
   * Óptica de la visión  
 [Optica De La Vision](http://www.slideshare.net/guestd9b09b/optica-de-la-vision-2001521)   
Dr. Máximo Terán G. 2009, licenciamiento no detallado, alguna imagen (página 21 de 38) hace mención a copyright Microsoft.
   * 2º Ing. Telecom. CAMPOS ELECTROMAGNÉTICOS – ÓPTICA (TEMA 1 – Óptica Geométrica).  
 [Optica - Tema 1 - Optica Geometrica - 2010-11.pdf](http://laplace.us.es/campos/optica/Optica%20-%20Tema%201%20-%20Optica%20Geometrica%20-%202010-11.pdf)   
Universidad de Sevilla, Copyright.  
Nivel universitario, incluye visión humana, instrumentos ópticos.
   * Plataforma e-ducativa aragonesa. Material física 2º Bachillerato  
 [Materiales de estudio 5. Luz y óptica ](http://e-ducativa.catedu.es/44700165/aula/programas.cgi?wAccion=verguia&wid_unidad=1371&id_curso=198)   

      *  [Tema 2: Óptica geométrica](http://e-ducativa.catedu.es/44700165/aula/archivos/repositorio//3000/3237/html/index.html) 
      *  [Tema 3: Instrumentos ópticos. El ojo humano](http://e-ducativa.catedu.es/44700165/aula/archivos/repositorio//3000/3239/html/index.html) 

   *  [Geometric-optics.aspx](http://www.kshitij-school.com/Study-Material/Class-12/Physics/Geometric-optics.aspx)   
Copyright © 2012-2013 Kshitij Education India Private Limited.
   * Plataforma e-ducativa aragonesa. Unidades Didácticas ESPAD Física 2º BACHILLERATO. Óptica geométrica  
ESPAD_BACH_FI2_U5_T3_contenidos  
 [ Tema 2. Óptica geométrica ](http://e-ducativa.catedu.es/44700165/aula/archivos/repositorio//3000/3237/html/index.html) 
   * Enlaces utilizados en la unidad didáctica de óptica  
 [&#160;&#160;fqcolindres: Óptica (enlaces)](http://fqcolindres.blogspot.com.es/2013/01/optica-enlaces.html)   
Gran cantidad de recursos: apuntes, simulaciones  
Publicado por Jesús L. F. Gallo. Licenciamiento cc-by-nc-sa
   * El físico loco. Óptica  
 [optica.htm](http://elfisicoloco.blogspot.com.es/p/optica.htm)   
Apuntes. Incluye óptica física. Javier Sánchez
   *  [waves-and-optics](https://www.khanacademy.org/science/physics/waves-and-optics)   
Clases en vídeo
   * OpenCourseWare UCM, tema óptica geométrica (en página 19 hay resumen de fórmulas)  
 [tema_optica_geometrica.pdf](http://ocw.upm.es/apoyo-para-la-preparacion-de-los-estudios-de-ingenieria-y-arquitectura/fisica-preparacion-para-la-universidad/contenidos/optica/tema_optica_geometrica.pdf) 
   * OPTICA, EUGENE HECHT , ADDISON-WESLEY, 1999, ISBN 9788478290253
   *  [](http://webs.ono.com/manoloruizrojas/)   
 [F2b_42_OP_OG.pdf](http://webs.ono.com/manoloruizrojas/PDF/F2b_42_OP_OG.pdf)   
Autores: María Dolores Marín Hortelano y Manuel Ruiz Rojas, profesores de Física y Química, desde Cieza - Murcia - España.  
En febrero 2016 indica Ultima actualización: Julio de 2009.
   * Curiosidad sobre tecnología de lentes  
 [Meet Philips Research - Research | Philips](http://www.research.philips.com/technologies/fluidfocus.html) 
   * Bienvenid@ a la web de FÍSICA de Julio V. Santos Benito Julio V. Santos Benito  
Catedrático jubilado  
Dpto. Física Aplicada  
Universidad de Alicante  
Manual de Óptica Geométrica  [MOG.html](http://www.fisicaenppt.esy.es/MOG.html) 
   * Apuntes de Óptica Física  
Artur Carnicer e Ignasi Juvells  
Universitat de Barcelona  
Departament de Física Aplica i Óptica  
8 de enero de 2003  
  
 [textguia_es.pdf](http://www.ub.edu/javaoptics/teoria/textguia_es.pdf) 
   * David Matellano, cc-by-nc-sa  
Óptica geométrica  [coyegspoj3gxceb5](https://mediateca.educa.madrid.org/documentos/coyegspoj3gxceb5)   
Los defectos del ojo  [oqa3dvx923r9yi7p](https://mediateca.educa.madrid.org/documentos/oqa3dvx923r9yi7p)   

[Óptica cheat sheet](https://drive.google.com/file/d/1FjU9AwFAkkjDxK7PCGDG0aMBOeiCV-zO/view)  
[EL OJO HUMANO Y SUS DEFECTOS. Trabajo voluntario “Física aplicada a la farmacia - ucm.es](https://www.ucm.es/data/cont/docs/136-2015-01-29-el%20ojo%20humano%20y%20sus%20defectos..pdf)  

[Telescopio - fisicalab](https://www.fisicalab.com/apartado/telescopio#contenidos)  
[Microscopio - fisicalab](https://www.fisicalab.com/apartado/microscopio#contenidos)  

## Hologramas

[![](https://img.youtube.com/vi/dY9qzgKobUE/0.jpg)](https://www.youtube.com/watch?v=dY9qzgKobUE "Increible holograma casero 3D iPhone - Android")   

[![](https://img.youtube.com/vi/5r5saH4XMp8/0.jpg)](https://www.youtube.com/watch?v=5r5saH4XMp8 "3d Hologram maker cool optical illusion - Incredible Science")   

[optigone](https://optigone.com/)  
[Optical Illusions vs. Mirage Phenomenon](https://optigone.com/index.php/optical-illusions/)  
Mirage se traduce como espejismo, se comenta en óptica física.  

## Recursos ejercicios y problemas
GRAU EN ÒPTICA I OPTOMETRIA > ÒPTICA GEOMÈTRICA I INSTRUMENTAL > Exercicis/Problemes  
 [Exercicis-Problemes](http://ocw.upc.edu/curs/26438/Exercicis-Problemes) Problemas de Introducción a la Óptica, Problemas de reflexión y refracción del rayo de luz, Problemas de lámina plano paralela y prismas, Problemas de representación óptica, Problemas de espejo plano, Problemas de dioptrio plano, Problemas de asociación de dioptrios, Problemas de espejo esférico, Problemas de asociación de dioptrio y espejo, Problemas de lentes delgadas, Problemas de asociación de lente delgada y espejo, Problemas de asociación de lente delgada y un conjunto de dioptrios planos, Problemas de sistemas ópticos, asociación de sistemas ópticos y lentes gruesas, Problemas de sistemas ópticos. Asociación de lentes delgadas y un conjunto de dioptrios planos, Problemas de Instrumentos Fotográficos.   
OpenCourseWare Iniciativa Digital Politècnica.Oficina de Publicacions Acadèmiques Digitals. © UPC. Universitat Politècnica de Catalunya ● BarcelonaTech. cc-by-nc-nd  
Jaume Escofet  
Bienvenid@ a la web de FÍSICA de Julio V. Santos Benito Julio V. Santos Benito  
Catedrático jubilado  
Dpto. Física Aplicada  
Universidad de Alicante  
 [Problemas_de_Óptica_Geométrica.pdf](http://www.fisicaenppt.esy.es/Problemas_de_%C3%93ptica_Geom%C3%A9trica.pdf)   
  
Problema/desafío asociado a dioptrio plano / ley de Snell (asociable también a óptica física)  
 [http://eltamiz.com/2011/04/13/desafios-fernando-y-el-cangrejito-solucion/](http://eltamiz.com/2011/04/13/desafios-fernando-y-el-cangrejito-solucion/)   

## Lentes gruesas

[Rev. Cubana Fis. 35, 32 (2018) LA ÓPTICA DE UN SISTEMA DE LENTES GRUESAS: SU APLICACIÓN AL OJO HUMANO](http://www.revistacubanadefisica.org/RCFextradata/OldFiles/2018/Vol.35_No.1/RCF_35_1_32.pdf)  
> En la presente contribución se hace la deducción de las fórmulas que rigen el comportamiento de una lente gruesa
y de un sistema de estas lentes, considerando el caso general de diferentes índices de refracción para todos los
medios. Luego se aplican estas ecuaciones para calcular los parámetros ópticos del ojo humano. El trabajo puede ser útil
como material didáctico para cursos avanzados de Óptica Geométrica y para cursos de Óptica Oftalmológica. Todas las
figuras han sido elaboradas por los autores.


## Recursos vídeos / animaciones
 [Light with animations and film clips: Physclips.](http://www.animations.physics.unsw.edu.au/light/)   
 [Physclips - Geometrical Optics](http://www.animations.physics.unsw.edu.au/light/geometrical-optics/)   
© School of Physics - UNSW (Sydney, Australia). cc-by-nc-nd  
Lecciones con vídeos y animaciones  

## [Recursos sobre la visión / ojo humano](/home/recursos/fisica/optica-vision)


## Recursos laboratorios no virtuales / experimentos  

GRUPO HEUREMA. EDUCACIÓN SECUNDARIA. ENSEÑANZA DE LA FÍSICA Y LA QUÍMICA  
Subsección: Apuntes de Física en Bachillerato (16-19 años) [ApFBachill9.htm](http://heurema.com/ApFBachill9.htm) Algunos asociados a mi clasificación como  [óptica física](/home/recursos/fisica/recursos-optica-fisica)  [](http://enebro.pntic.mec.es/~fmag0006/Prism300.html) PRISMA Practica nº 3 Lentes delgadas: Determinación de la distancia focal de una lente delgada  
 [PRISMA - Práctica 3_0 - Lentes delgadas- Programa de Nuevas Tecnologías](http://enebro.pntic.mec.es/~fmag0006/Prism300.html)  [dispositivos-opticos-caseros.html](http://www.cienciaenaccion.org/es/2013/experimento-207/dispositivos-opticos-caseros.html)  [Experimentos de fsica. ptica](http://www.madrimasd.org/cienciaysociedad/taller/fisica/optica/default.asp)   
Taller de física - óptica (combina óptica física y geométrica, e incluso efecto fotoeléctrico)  
¿De veras son convergentes y divergentes?  
Responsables:  
Juan Alonso Dehesa  
Jorge Barrio Gómez De Agüero  
José Rubio Lozano  
M.ª José Vila Gómez  
Fuente: VI Feria Madrid por la Ciencia  
 [Taller de F&iacute;sica -ptica. &iquest;De veras son convergentes y divergentes?](http://www.madrimasd.org/cienciaysociedad/taller/fisica/optica/convergentes-divergentes/)  
 
[![Amazing Water Trick - Amazing Science Tricks Using Liquid - YouTube](https://img.youtube.com/vi/G303o8pJzls/0.jpg)](https://www.youtube.com/watch?v=G303o8pJzls)   
 [Physics in a Glass: Reversing Arrows | PhysicsCentral](http://www.physicscentral.com/experiment/physicsathome/reversing-arrows.cfm)   
[twitter UniverseAndMore/status/1398294662096510982](https://twitter.com/UniverseAndMore/status/1398294662096510982) Someone should have probably checked the focal length of the glass sphere before putting this lamp on the market.   
This is why physics matters people!! #iteachphysics 
![](https://pbs.twimg.com/media/E2e85IpWYAYmLcd?format=jpg&name=900x900)  

En respuestas al hilo se citan otras situaciones, como edificiones que reflejan luz  
[Concave Mirrored Skyscraper Creates Death Ray, Melts Car](http://geekologie.com/2013/09/concave-mirrored-skyscraper-creates-deat.php)  
![](http://geekologie.com/2013/09/04/death-ray-building-london-1.jpg)  

[London skyscraper's 'deathray' reflection is melting cars, burning businesses, but also cooking eggs](https://nationalpost.com/news/london-skyscrapers-deathray-reflection-is-melting-cars-burning-businesses-but-also-cooking-eggs)  
![](https://smartcdn.prod.postmedia.digital/nationalpost/wp-content/uploads/2013/09/fo0906_skyscraper_deathray_c_ab1.jpg?quality=90&strip=all&w=564&type=webp)  
![](https://smartcdn.prod.postmedia.digital/nationalpost/wp-content/uploads/2013/09/179463790.jpg?w=564&quality=90&strip=all&type=webp)  

## Recursos apps para móvil
  
**Ray diagrams**, Audrius Meškauskas   
 [Ray diagrams - Apps on Google Play](https://play.google.com/store/apps/details?id=org.us.andriod)   
 [  | F-Droid - Free and Open Source Android App Repository](https://f-droid.org/repository/browse/?fdfilter=prism&fdid=org.us.andriod)   
Licenciamiento GPL / Public Domain ...  
 [Home · andviane/prism Wiki · GitHub](https://github.com/andviane/prism/wiki)   
  
**Ray Optics**, Shakti Malik  
 [Ray Optics - Apps on Google Play](https://play.google.com/store/apps/details?id=com.shakti.rayoptics)   
 [Ray Optics Pro - Apps on Google Play](https://play.google.com/store/apps/details?id=com.shakti.rayoptics.pro)  (versión pro de pago)  
  
**Qioptiq Lens Calculator**, Optical Software   
 [details](https://play.google.com/store/apps/details?id=net.opticalsoftware.calclensthin)   
  
Physics - Optics (Free)  
 [Physics - Optics (Free) - Apps on Google Play](https://play.google.com/store/apps/details?id=org.surendranath.opticstrial)  (versión gratuita)  
Incluye elementos de  [óptica física](/home/recursos/fisica/recursos-optica-fisica)   
 [Physics - Optics - Apps on Google Play](https://play.google.com/store/apps/details?id=org.surendranath.optics)  (versión de pago)  
  
Optics Physics, Learner's Series   
 [details](https://play.google.com/store/apps/details?id=learnersseries.physics.optics)   
  
Light Lens Simulator, OpenSourcePhysicsSG [](https://play.google.com/store/apps/dev?id=7915608832562607433)   
 [details](https://play.google.com/store/apps/details?id=com.ionicframework.lensapp223196)   

## Recursos asociados a "óptica" con radio

[twitter CamiloSua_/status/1537835600136511493](https://twitter.com/CamiloSua_/status/1537835600136511493)  
Antena parabólica (modelo offset)  
Diferente a la antena parabólica simétrica por la posición del foco y el sector de la parábola que ocupa.  
[Antena parabólica Offset - Camilo Sua Flórez - geogebra.org](https://geogebra.org/m/jak7t2fe)  

## Recursos laboratorios no virtuales

[twitter onio72/status/1526614166919909377](https://twitter.com/onio72/status/1526614166919909377)  
Algunos de mis alumnos de 2º de bachillerato han “tallado” lentes en gelatina neutra. No creo que lo olviden fácilmente.  
![](https://pbs.twimg.com/media/FS-ffVUXEAIIvQ6?format=jpg)  
![](https://pbs.twimg.com/media/FS-ffVTWUAoKroq?format=jpg)  

## Recursos laboratorios virtuales / simulaciones

   * Óptica Geométrica. Simulaciones Universidad de Colorado  
 [Óptica geométrica - phet.colorado.edu](http://phet.colorado.edu/es/simulation/geometric-optics)   
 [Óptica geométrica. Lente y espejo. - phet.colorado.edu (HTML5)](https://phet.colorado.edu/sims/html/geometric-optics/latest/geometric-optics_all.html?locale=es)  
 Url alternativa [Óptica geométrica. Lente y espejo. - phet.colorado.edu (HTML5)](https://phet.colorado.edu/sims/html/geometric-optics/latest/geometric-optics_es.html)  
 ![](https://phet.colorado.edu/sims/html/geometric-optics/latest/geometric-optics-900.png)  
Simulación interactiva lente que permite modificar posición, radio lente, índice de refracción, mostrar rayos ...  
Licenciamiento cc-by ó gnu-gpl  [licensing](http://phet.colorado.edu/en/about/licensing)   
 _[Óptica geométrica (flash) - phet.colorado.edu](http://phet.colorado.edu/sims/geometric-optics/geometric-optics_es.html)_ versión anterior flash, era la única opción antes de que crear la versión HTML5. Su uso requiere poner operativo flash con Ruffle o similar.  


   * [Lens Combinations- 2 Converging Lenses. Barb Newitt. - geogebra.org](https://www.geogebra.org/m/EB97GwWD)  
   This simulation shows a lens combination of 2 converging lenses.  Adjust the position of the orange circle to adjust the object position.  Adjust the position of the purple focal point circles to adjust the focal lengths of the two lenses.  Adjust the position of the purple Lens2 + to adjust the position of Lens 2.
   
   * [Formación de imágenes en lentes delgadas. Andrés Manceñido - geogebra.org](https://www.geogebra.org/m/EQgbp4mt)  
   * [Convex lenses - minutelabs.io](http://labs.minutelabs.io/Lenses/)  

   * [Physics > Optics (Examples) - wolframalpha.com](https://www.wolframalpha.com/examples/science-and-technology/physics/optics/)   
Incluye lentes y espejos  

   * [Banco óptico (2º Bachillerato). José Luis San Emeterio Peña. cc-by-sa - recursostic.educacion.es](http://recursostic.educacion.es/newton/version/n3/materiales_didacticos/bancooptico/index.html) 
   * [Óptica (4º ESO). José Luis San Emeterio Peña. cc-by-sa - cnice.mec.es](http://newton.cnice.mec.es/materiales_didacticos/optica/optobjetivos.htm)   
Combina óptica física y geométrica  

[Lente delgadas - fisicalab](https://www.fisicalab.com/apartado/lentes-delgadas) Incluye simulación   

   * Simulaciones geogebra Vicente Martín Morales, copyright  
 [telescopio_reflector.html](http://www.fisicayquimica.hol.es/documentos/geogebra/telescopio_reflector.html)   
 [dioptrio_esf_convexo.html](http://www.fisicayquimica.hol.es/documentos/geogebra/dioptrio_esf_convexo.html)   
 [dioptrio_esf_concavo.html](http://www.fisicayquimica.hol.es/documentos/geogebra/dioptrio_esf_concavo.html)   
 [dioptrio_esf_convexo_2.html](http://www.fisicayquimica.hol.es/documentos/geogebra/dioptrio_esf_convexo_2.html)   
 [refraccion.htm](http://www.fisicayquimica.hol.es/documentos/geogebra/refraccion.htm)   
 [espejos_esfericos.html](http://www.fisicayquimica.hol.es/documentos/geogebra/espejos_esfericos.html)   
 [lentes.html](http://www.fisicayquimica.hol.es/documentos/geogebra/lentes.html)   
 [anteojo_terrestre.html](http://www.fisicayquimica.hol.es/documentos/geogebra/anteojo_terrestre.html)   
 [telescopio.html](http://www.fisicayquimica.hol.es/documentos/geogebra/telescopio.html)   
 [dos_lentes.html](http://www.fisicayquimica.hol.es/documentos/geogebra/dos_lentes.html)   
 [arco_iris.html](http://www.fisicayquimica.hol.es/documentos/geogebra/arco_iris.html) 
   *  [optica.htm](http://iesfgcza.educa.aragon.es/depart/fisicaquimica/fisicasegundo/optica.htm)   
Recursos, animaciones (incluye parte de lo que yo clasifico como  [óptica física](/home/recursos/fisica/recursos-optica-fisica) )  
Luis Ortiz de Orruño
   * PRISMA "Laboratorio virtual" (ondas, óptica física y geométrica)  
 [http://enebro.pntic.mec.es/~fmag0006/](http://enebro.pntic.mec.es/%7Efmag0006/)   
Enlaces a simulaciones por niveles y bloques (incluye parte de lo que yo clasifico como  [óptica física](/home/recursos/fisica/recursos-optica-fisica) )
   * Óptica en Bachillerato.  
 [Optica Bachillerato](http://acacia.pntic.mec.es/~jruiz27/contenidos.htm)   
Página realizada por Jesús Ruiz Felipe Profesor de Física del Instituto Cristóbal Pérez Pastor (Tobarra, Albacete)   
Incluye óptica física y geométrica con algunos applets interactivos  

      * Espejos planos [espejosplanos](http://acacia.pntic.mec.es/~jruiz27/lentespejoss/espejosplanos.htm)  Se visualizan los rayos para cada punto de un objeto con volumen
      * Espejos esféricos. formación de imágenes por espejos esféricos [espejos.htm](http://acacia.pntic.mec.es/~jruiz27/lentespejoss/espejos.htm)  Permite cambiar lente/espejo y posiciones
      * Lentes delgadas [lentes.htm](http://acacia.pntic.mec.es/~jruiz27/lentespejoss/lentes.htm) 
      * Instrumentos ópticos [instrumentos](http://acacia.pntic.mec.es/~jruiz27/lentespejoss/2lentes/instrumentos.htm)  Permite combinar varias lentes y desplazarlas

   * 37 Lecciones de Física y Química [30 lecciones](http://platea.pntic.mec.es/~cpalacio/30lecciones.htm)   

      * Espejos [Espejos](http://platea.pntic.mec.es/~cpalacio/Optica%281%29Espejos.htm) 
      * Imágenes especulares [Construcci&oacute;n de im&aacute;genes en espejos](http://platea.pntic.mec.es/~cpalacio/Imag_Espejo.htm) 
      * Lentes [Ejemplo lentes](http://platea.pntic.mec.es/~cpalacio/lentes.htm) 
      * Banco Óptico [Banco Optico](http://platea.pntic.mec.es/~cpalacio/banco2.htm)  Aquí tienes un banco óptico de pruebas con el que puedes experimentar con todo tipo de elementos ópticos.Calcula distancias focales, imágenes, etc
© 2000-2001. Autor de la página Carlos Palacios  

   * 700 Applets de física y química. Óptica geométrica  
 [606.htm](http://perso.wanadoo.es/oyederra/2btf/606.htm)   
Recopilación efectuada por Fernando Jimeno Castillo
   * Telescopio Astronómico [Refracting astronomical telescope](https://www.walter-fendt.de/html5/phen/refractor_en.htm)   
© Walter Fendt, 8 Marzo 2000
   * Image Formation by Converging Lenses [Image formation by converging lenses](https://www.walter-fendt.de/html5/phen/imageconverginglens_en.htm)   
© Walter Fendt, December 23, 2008  

   * JOptics curso de óptica  
 [JOptics](http://www.ub.edu/javaoptics/index-es.html)   
JOptics es un conjunto de recursos docentes dirigidos al aprendizaje de la Óptica Física a nivel universitario en el marco de la licenciatura de Física o la titulación en Óptica y Optometría.  
Grupo de Innovación Docente en Óptica Física y Fotónica. Los contenidos de esta página están bajo una Licencia Creative Commons y Universitat de Barcelona.  
El nivel es universitario, pero incluye una **Guía de uso para secundaria de JOptics**, incluyendo   
 [JOptics](http://www.ub.edu/javaoptics/secundaria/Sec_GuiaEs.html)   
Trazado de rayos  [JOptics](http://www.ub.edu/javaoptics/secundaria/Sec_RayEs.html)   

   * General Physics Java Applets  
 [General Physics Animatons](http://www.surendranath.org/GPA/Menu.php)  (antes de 2019 http://surendranath.tripod.com/Apps.html)
      * Optics. Inglés. Licenciamiento no detallado.
      * Spherical Mirrors & Lenses Reflection & Refraction at Curved Surfaces [RRCS.html](http://www.surendranath.org/Applets/Optics/RRCS/RRCS.html)  Simulación java interactiva (se puede usar teclado) con lentes convergentes y espejos cóncavos y convexos.   
      * Spherical Mirrors & Lenses [Spherical Mirrors & Lenses](http://www.surendranath.org/GPA/Optics/CurvSurf/CurvSurf.html)  no usa java  
      * Optical Instruments [OptInstrmnts.html](http://www.surendranath.org/Applets/Optics/OptInstrmnts/OptInstrmnts.html)  Telescopio y microscopio   

   * Optics Bench. Optics Applet v4.1  
 [default.html](http://webphysics.davidson.edu/Applets/optics4/default.html)   
This applet allows users to simulate standard optic elements (lens, mirror, dielectrics, sources, apertures) and observe the ways that light rays propagate through these elements. The applet is designed to be scripted but may also be used to construct optical systems using buttons and a click and drag metaphor.  
Autores Mike Lee, Wolfgang Christian, licenciamiento no detallado
   * La luz y sus propiedades. Óptica (Lentes, Lab. lentes, Espejos, Lab. espejos)  
 [Laboratorio de Lentes | Educaplus.org](http://www.educaplus.org/luz/lente2.html)   
Incluye applets lente y espejo interactivos.  
© 1998-2012, www.educaplus.org
   * Laboratorio de física.  
Título: Características de un espejo esférico  
Descripción: Observación de algunas de las principales características de un espejo esférico  
 [Laboratorio de F&iacute;sica](http://iris.cnice.mec.es/fisica/)   
Eligiendo 2º Bachillerato, óptica y óptica geométrica 
   * Óptica paraxial  
 [Optica paraxial](http://www.sc.ehu.es/sbweb/fisica_/ondas/reflex_trans/optica/optica.html)   
Animación interactiva espejo y dioptrio  
Curso Interactivo de Física en Internet © Ángel Franco García
   * Optics dentro de U.C. Berkeley Physics Lecture Demonstrations. Applets  
 [Museum Informatics Project | Research IT](http://www.mip.berkeley.edu/physics/appletindex.html) 
   * Physique et simulations numériques. Optique géométrique  
 [Physique et simulation](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/mnoptigeo.html)   
Jean-Jacques ROUSSEAU. Faculté des Sciences exactes et naturelles. Université du Maine   
Francés. Niveaux : Lycée, premier et second cycles.  
Gran cantidad de simulaciones, se incluyen enlaces directos aparte del enlace general anterior  

      * Mirroirs (espejos)  


|  [Miroir plan](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/mirplan.html)  |  [Rotation d'un miroir plan](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/mirtournant.html)  |  [Images dans deux miroirs plans](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/miroirs2.html)  | 
|:-:|:-:|:-:|
|  [Télémètre à miroir](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/telemetre.html)  |  [Principe du sextant](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/sextant.html)  |  [Stigmatisme des miroirs](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/miroirs.html)  | 
|  [Miroirs sphériques](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/mirspher.html)  |  [Mesure de la vitesse de la lumière](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/mesurec.html)  |  [Trois miroirs plans orthogonaux](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/miroirs3.html)  | 
|  [Visualisation de caustiques](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/caustimir.html)  |  [Principe des projecteurs automobiles](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/phareauto.html)  |  [Principe du périscope](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/periscope.html)  | 
      * Dioptres (Dioptrios)  

|  [Réfraction](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/refrac2.html)  |  [Dioptre plan](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/refrac.html)  |  [Dioptre sphérique](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/diopsher.html)  | 
|:-:|:-:|:-:|
|  [Construction des rayons](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/huyghens.html)  |  [Points d'Young-Weierstrass](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/youngweir.html)  |  [Lame à faces parallèles](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/lamelle.html)  | 
|  [Indice d'une lame à faces parallèles](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/chaulnes.html)  |  [Mirages](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/mirage.html)  |  [Deux illusions liées à la réfraction](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/refracpara.html)  | 
      * Prismes (Prismas)  

|  [Prisme dispersif](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/prisme.html)  |  [Prisme de Pellin et Broca](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/broca.html)  |  [Prisme d'Abbe](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/prismeabbe.html)  | 
|:-:|:-:|:-:|
|  [Prisme à vision directe](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/prismevd.html)  |  [Réfractomètre de Pulfrich](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/pulfrich.html)  |  [Réfractomètre d'Abbe](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/abberefracto.html)  | 
|  [Prismes de Dove et d'Amici](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/doveamici.html)  |  [Prismes de Porro](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/porro.html)  |  [Prisme pentagonal](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/pentaprism.html)  | 
|  [Prisme de Littrow](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/littrow.html)  |  [Astigmatisme du prisme](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/prismeasti.html)  |  [Goniomètre à prisme](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/gonio.html)  | 
      * Lentilles (Lentes)  

|  [Types de lentilles](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/lentepai.html)  |  [Lentilles minces](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/lentille.html)  |  [Construction des rayons](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/images.html)  | 
|:-:|:-:|:-:|
|  [Aberrations géométriques](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/rayons.html)  |  [Aberrations chromatiques](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/aberchro.html)  |  [Doublet achromatique](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/achromat.html)  | 
|  [Focométrie](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/focobess.html)  |  [Méthode de Cornu](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/cornu.html)  |  [Méthode de Davanne et Martin](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/davanne.html)  | 
|  [Focomètre des lunetiers](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/fronto.html)  |  [Correction de l'astigmatisme](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/stiglent.html)  |  [Caustiques de lentilles convergentes](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/caustiques.html)  | 
|  [Lentilles boule et demi-boule](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/boule.html)  |  [Ménisque d'Amici](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/menisque.html)  |  [Lentille de Fresnel](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/echelon.html)  | 
|  [Aberrations géométriques en 3D](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/aberat3D.html)  |  [Lentille ellipsoïdale](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/lentelip.html)  |  [Lentille cylindrique](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/lenticylin.html)  | 
|  [Lentille mince en 3D](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/lentispher.html)  | 
      * Instrumentes (Instrumentos ópticos)  

|  [Doublet de lentilles minces](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/doublet2.html)  |  [Principe du microscope](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/microscope.html)  |  [Lunettes](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/lunettes.html)  | 
|:-:|:-:|:-:|
|  [Oculaires](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/oculaires.html)  |  [Zoom et téléobjectif](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/zoom.html)  |  [Lentille de Barlow](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/doubleur.html)  | 
|  [Télescope Cassegrain (2)](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/casseg.html)  |  [Télescope Newton](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/newton.html)  |  [Profondeur de champ](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/profcham.html)  | 
|  [Étude de diverses loupes](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/loupes.html)  |  [Lame de Schmidt](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/schmidt.html)  |   
 | 
      * Photométrie (Fotometría)  

|  [Notion d'angle solide](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/anglesolide.html)  |  [Grandeurs photométriques](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/grandphoto.html)  |  [Éclairement d'un écran](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/photoecran.html)  |
|:-:|:-:|:-:| 
|  [Loi de Beer-Lambert](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/loidebeer.html)  |  [Loi de Bouguer](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/bouguer.html)  |  [Photomètre de Lummer et Brodhun](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/photometre.html)  | 
|  [Indicatrice d'émission](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/diode.html)  | 
      * Autres (Otros)  

|  [Principe de Fermat (2)](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/fermat.html)  |  [Oeil simplifié](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/oeil.html)  |  [Arc en ciel](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/arcciel.html)  | 
|:-:|:-:|:-:|
|  [Persistance rétinienne](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/retine.html)  |  [Synthèse des couleurs (2)](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/addition.html)  |  [Couplage d'une fibre optique](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/coupleur.html)  | 
|  [Fibre optique à saut d'indice](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/optigeo/fibresaut.html)  |    

   * Kean University  

      * Image Formation by a Diverging Mirror [Exercise, Image Formation by a Diverging Mirror](http://www.kean.edu/~gkolodiy/physics/optics/dmirr/) 
      * Image Formation by a Converging Lens [Exercise, Image Formation by a Converging Lens](http://www.kean.edu/~gkolodiy/physics/optics/clens/) 
      * Image Formation by a Diverging Lens [Exercise, Image Formation by a Diverging Lens](http://www.kean.edu/~gkolodiy/physics/optics/dlens/) 
Trazado de rayos gráfico con sentido en las flechas, interactivo  
© Copyright 1997, Sergey Kiselev and Tanya Yanovsky-Kiselev
   * Simulación de lentes. wikisaber  
 [WikiSaber - La Web con Todo el Conocimiento de Nuestro Mundo](http://www.wikisaber.es/CentroDeRecursos/toolkits/physics/es/lenses/index.html) 
   * The world above the water, as seen by a fish  
 [The world above the water, as seen by a fish | UCLA ePhysics](http://ephysics.physics.ucla.edu/fish-eye)   
Applet by Fu-Kwun Hwang  
Virtual Physics Laboratory 
   * Visón dioptrio plano: objeto en el agua visto desde el aire  
 [index.php](http://www.phy.ntnu.edu.tw/ntnujava/index.php?topic=378.0)   
 [htmltag.php](http://www.phy.ntnu.edu.tw/ntnujava/htmltag.php?code=users.ntnu.fkh.objectinwater2_pkg.objectinwater2Applet.class&name=objectinwater2&muid=2) 
   * ÓPTICA GEOMÉTRICA Autor: academia INTESUM  
 [ÓPTICA GEOMÉTRICA – GeoGebra](https://www.geogebra.org/m/EWg2nWxU)   
