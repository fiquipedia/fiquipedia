
# Pruebas Libres FP

Información general en 
[Pruebas para la obtención directa del título de Formación Profesional (Pruebas libres) - todofp.es](https://www.todofp.es/pruebas-convalidaciones/pruebas/pruebas-obtencion-directa.html)  

Desde ahí se enlazan fechas de cada comunidad
[Fechas de las pruebas libres para la obtención de títulos de FP. Curso 2021-2022 - todofp.es](https://www.todofp.es/pruebas-convalidaciones/pruebas/pruebas-obtencion-directa/pruebas-libres/pruebas-libres-1921.html#ancla01-12)  

Para Madrid se enlaza
[RESOLUCIÓN de 12 de enero de 2021, de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial, por la que se convocan, en el curso 2020-2021, las pruebas para la obtención de los títulos de Técnico y Técnico Superior de Formación Profesional.](http://www.bocm.es/boletin/CM_Orden_BOCM/2021/01/20/BOCM-20210120-14.PDF)  

[Pruebas para obtención de títulos de Técnico y Técnico Superior de FP - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/pruebas-obtencion-titulos-tecnico-tecnico-superior-fp)  

Hay un apartado de pruebas pero no incluye ejemplos   
> Estructura de las pruebas  
Ejercicios teóricos y prácticos  
Las pruebas incluyen ejercicios teóricos y prácticos para eviendenciar que el alumnado matriculado ha alcanzado los distintos resultados de aprendizaje o capacidades terminales y  las competencias asociadas a cada módulo profesional.  
Pruebas  
Se elaborará una prueba para cada uno de los módulos profesionales. En algunos casos, se podrá dividir la prueba en varias partes que podrán realizarse en días diferentes y puede ser necesario haber superado el contenido de la parte anterior para realizar la siguiente.  
Contenidos  
Los contenidos se referirán a los currículos o planes de estudios de los ciclos formativos implantados en la Comunidad de Madrid y sus respectivas competencias profesionales.  

En 2023 consigo que se publiquen enunciados, ver copia local en [drive.fiquipedia Pruebas libres FP Madrid](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/Pruebas%20libres%20FP/Madrid) 
 
 
