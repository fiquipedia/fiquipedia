
# Recursos Cosmología

En esta página se intentan organizar recursos asociados a cosmología, sobre la que aparecen contenidos en Física de 2º Bachillerato con LOMCE.  
  
Tenía inicialmente recursos que pueden estar relacionados en  [recursos escala / notación científica](/home/recursos/recursos-notacion-cientifica)   
  
Nombro la página como Cosmología aunque no es citada como tal en LOMCE, y quizá sería más correcto hablar de "Cosmología física"  
 [Cosmología física - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Cosmolog%C3%ADa_f%C3%ADsica)  que utilizando la física intenta explicar el origen, evolución y destino del Universo.  
  
Se puede relacionar con recursos de  [gravitación](/home/recursos/fisica/recursos-gravitacion) (por ejemplo agujeros negros),  [física de partículas](/home/recursos/fisica/fisica-de-particulas), astrofísica y  [astronomía](/home/recursos/recursos-astronomia)   
  
En 2017 el objetivo inicial es hacer unos apuntes asociados a Física de 2º Bachillerato LOMCE.  

## Recursos generales
Cosmovisiones del s.XXI: Del Microverso subatómico al Multiverso y más allá [(PDF) Cosmovisiones del s.XXI: Del Microverso subat&oacute;mico al Multiverso y m&aacute;s all&aacute;](https://www.researchgate.net/publication/338113323_Cosmovisiones_del_sXXI_Del_Microverso_subatomico_al_Multiverso_y_mas_alla) Presentation (PDF Available) · December 2019DOI: 10.13140/RG.2.2.21819.77605Juan F. G. H.IES Velázquez  
El Multiverso y las otras dimensiones: un viaje matemático y filosóficoJohn D Jonzz [(PDF) El Multiverso y las otras dimensiones: un viaje matemático y filosófico | John D Jonzz - Academia.edu](https://www.academia.edu/36831590/El_Multiverso_y_las_otras_dimensiones_un_viaje_matem%C3%A1tico_y_filos%C3%B3fico)   
  
Cosmología, Origen, evolución y destino del Universo, Pedro J. Hernández  
 [Cosmologa Astrofsica](http://astronomia.net/cosmologia/cosmolog.htm#Contenidos)   
  
 [The Big Bang and the Big Crunch - The Physics of the Universe](http://www.physicsoftheuniverse.com/topics_bigbang.html)   
  
 [How the Big Bang Theory Works | HowStuffWorks](http://science.howstuffworks.com/dictionary/astronomy-terms/big-bang-theory.htm)   
  
 [¿Cual sería el destino del Universo? - Ciencia y educac... en Taringa!](https://www.taringa.net/posts/ciencia-educacion/16050810/Cual-seria-el-destino-del-Universo.html)   
 
 [uib.cat La teoria i les evidències del big-bang](http://dfs.uib.cat/apl/aac/fisicapbau/hbg28ty.htm)  
 
[¿Cuál fue el origen del universo? - nationalgeographic.es](https://www.nationalgeographic.es/espacio/cual-fue-el-origen-del-universo)  
Desde tiempos inmemoriales hemos tratado de conocer cómo se formó el universo. La respuesta más común se basa en la teoría del Big Bang, pero no es la única.  

[Origen y evolución del Universo. Cosmoeduca - iac.es](https://outreach.iac.es/cosmoeduca/universo/index.html)  
El material que ofrecemos sobre "Origen y evolución del Universo" consta de dos charlas y anexos que las complementan.  
Las charlas no son independientes, sino que están pensadas para comenzar por la Charla I antes de abordar la Charla II.  
Tanto las charlas como sus anexos consisten en diapositivas con guiones.   
[Guion charla I](https://outreach.iac.es/cosmoeduca/universo/guion-charla1.html)  
[Guion charla II](https://outreach.iac.es/cosmoeduca/universo/guion-charla2.html)  

## Fondo cósmico de microondas 

[Los datos del telescopio espacial Planck de la ESA sobre el fondo cósmico de microondas - francis.naukas.com](https://francis.naukas.com/2013/03/21/los-datos-del-satelite-planck-de-la-esa-sobre-el-fondo-cosmico-de-microondas/)   
 
 
## Materia oscura

[La historia de la materia oscura, Francisco R. Villatoro - naukas.com](https://naukas.com/2010/08/20/la-historia-de-la-materia-oscura/)  
[Cinco razones por las que pensamos que existe la materia oscura (waybackmachine)](http://web.archive.org/web/20190314155155/http://divulgame.org:80/2014/12/05/cinco-razones-por-las-que-pensamos-que-existe-la-materia-oscura/)  

Tuit de diciembre 2017 con imagen que resume tipos de candidatos  
[twitter emulenews/status/938117522825580545](https://twitter.com/emulenews/status/938117522825580545)   
![](https://pbs.twimg.com/media/DQTc56bX4AAJwYY?format=jpg)   
 
[forbes.com 5 Ways That Dark Energy Might Not Determine The Fate Of Our Universe, Ethan Siegel](https://www.forbes.com/sites/startswithabang/2021/08/16/5-ways-that-dark-energy-might-not-determine-the-fate-of-our-universe/amp/)  
![](https://thumbor.forbes.com/thumbor/711x536/https://specials-images.forbesimg.com/imageserve/5f63aa78f0f6a2963da67b21/How-radiation--matter--and-dark-energy-inflation-energy-densities-change-with/960x0.jpg)  

## Cronologías
A veces de los descubrimientos, observaciones, teoría o del Universo  
A veces de todo el universo, a veces formación de sistemas solares, formación de planetas  
  
A veces escala temporal: enlaza con calendario cósmico de Carl San  
  
Science Today: Simulating Solar System Formation | California Academy of Sciences  
  
 [![watch](https://img.youtube.com/vi/yXq1i3HlumA/0.jpg)](https://www.youtube.com/watch?v=yXq1i3HlumA)   
  
TIMELAPSE OF THE ENTIRE UNIVERSE (marzo 2018, melodysheep)  
 [![TIMELAPSE OF THE ENTIRE UNIVERSE - YouTube](https://img.youtube.com/vi/TBikbn5XJhg/0.jpg)](https://www.youtube.com/watch?v=TBikbn5XJhg)   
  
 [![http://particleadventure.org/images/history-of-the-universe-2015.jpg](http://particleadventure.org/images/history-of-the-universe-2015.jpg "http://particleadventure.org/images/history-of-the-universe-2015.jpg") ](http://particleadventure.org/images/history-of-the-universe-2015.jpg)   
  
 [history-of-the-universe-2015.jpg](http://particleadventure.org/images/history-of-the-universe-2015.jpg)   
  
 [![http://www.physicsoftheuniverse.com/images/bigbang_timeline.jpg](http://www.physicsoftheuniverse.com/images/bigbang_timeline.jpg "http://www.physicsoftheuniverse.com/images/bigbang_timeline.jpg") ](http://www.physicsoftheuniverse.com/images/bigbang_timeline.jpg)   
La imagen indica Copyight Addison Wesley  
 [bigbang_timeline.jpg](http://www.physicsoftheuniverse.com/images/bigbang_timeline.jpg)   
  
 [![https://insight.nokia.com/sites/default/files/wp-content/uploads/2014/05/The-Big-Bang-A-Timeline-of-Discovery-Flickr-Photo-Sharing-Mozilla-Firefo_2014-05-26_13-04-44.jpg](https://insight.nokia.com/sites/default/files/wp-content/uploads/2014/05/The-Big-Bang-A-Timeline-of-Discovery-Flickr-Photo-Sharing-Mozilla-Firefo_2014-05-26_13-04-44.jpg "https://insight.nokia.com/sites/default/files/wp-content/uploads/2014/05/The-Big-Bang-A-Timeline-of-Discovery-Flickr-Photo-Sharing-Mozilla-Firefo_2014-05-26_13-04-44.jpg") ](https://insight.nokia.com/sites/default/files/wp-content/uploads/2014/05/The-Big-Bang-A-Timeline-of-Discovery-Flickr-Photo-Sharing-Mozilla-Firefo_2014-05-26_13-04-44.jpg)   
 [Our blog | Nokia](https://insight.nokia.com/sites/default/files/wp-content/uploads/2014/05/The-Big-Bang-A-Timeline-of-Discovery-Flickr-Photo-Sharing-Mozilla-Firefo_2014-05-26_13-04-44.jpg)   
 [![http://www.ctc.cam.ac.uk/images/contentpics/outreach/cp_universe_chronology_large.jpg](http://www.ctc.cam.ac.uk/images/contentpics/outreach/cp_universe_chronology_large.jpg "http://www.ctc.cam.ac.uk/images/contentpics/outreach/cp_universe_chronology_large.jpg") ](http://www.ctc.cam.ac.uk/images/contentpics/outreach/cp_universe_chronology_large.jpg)   
 [cp_universe_chronology_large.jpg](http://www.ctc.cam.ac.uk/images/contentpics/outreach/cp_universe_chronology_large.jpg)   
 [![https://en.wikipedia.org/wiki/BICEP_and_Keck_Array#/media/File:History_of_the_Universe.svg](https://upload.wikimedia.org/wikipedia/commons/d/db/History_of_the_Universe.svg "https://en.wikipedia.org/wiki/BICEP_and_Keck_Array#/media/File:History_of_the_Universe.svg") ](https://upload.wikimedia.org/wikipedia/commons/d/db/History_of_the_Universe.svg)   
 [File:History of the Universe.svg - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:History_of_the_Universe.svg)   

[«Cronología del Big Bang y expansión del universo». Ponencia divulgativa en la Reunión Anual de Mensa España, 8 de Diciembre de 2012, Alcobendas. - estudiarfisica.com](https://estudiarfisica.com/2013/05/09/cronologia-del-big-bang-y-expansion-del-universo-ponencia-divulgativa-en-la-reunion-anual-de-mensa-espana-8-de-diciembre-de-2012-alcobendas/)  
Adrián Baños. Incluye transcripción  
https://www.youtube.com/watch?v=8RtlK2cNTp4
[![](https://img.youtube.com/vi/8RtlK2cNTp4/0.jpg)](https://www.youtube.com/watch?v=8RtlK2cNTp4 "Cronología del Big Bang Charla de Adrián Baños")   

## Escala y estructura del universo  
Ver página sobre escalas  

[the-universe-laminated](http://www.natgeomaps.com/the-universe-laminated)   
Lámina de pago, pero que permite zoom  
  
 [ Large Scale Structure in the Local Universe: The 2MASS Galaxy Catalog. Thomas Jarrett (IPAC/Caltech)](https://wise2.ipac.caltech.edu/staff/jarrett/papers/LSS/)   
 
 [All the Contents of the Universe, in One Graphic - visualcapitalist](https://www.visualcapitalist.com/composition-of-the-universe/)  
 ![](https://www.visualcapitalist.com/wp-content/uploads/2022/08/THE-COMPOSITION-OF-THE-UNIVERSE_4.png)  
  
 [100,000 Stars by Google Data Arts Team - Experiments with Google](https://www.chromeexperiments.com/experiment/100000-stars)   
 [100,000 Stars](http://stars.chromeexperiments.com/)   
  
 [twitterRainmaker1973b/status/938404295975882752](https://twitter.com/Rainmaker1973b/status/938404295975882752)  
 The size of  [#Hubble](https://twitter.com/hashtag/Hubble?src=hash) 's eXtreme Deep Field in relation to the rest of the night sky [Hubble Ultra-Deep Field - Wikipedia](https://en.wikipedia.org/wiki/Hubble_Ultra-Deep_Field)   
  
 [One Million Stars by Michael Chang - Experiments with Google](https://www.chromeexperiments.com/experiment/one-million-stars)   
  
![](https://d1o50x50snmhul.cloudfront.net/wp-content/uploads/2016/09/21110025/ns_milky_way_poster-thumb-800x609.jpg)  

 [You are here: A spectacular poster of our place in the Milky Way | New Scientist](https://www.newscientist.com/article/2123526-you-are-here-a-spectacular-poster-of-our-place-in-the-milky-way/)   
 [Versión pdf ](https://drive.google.com/file/d/0BwQ4esYYFC04VFczX1ByU01LaUk/view?usp=sharing)   
 [NS_MILKY_WAY_POSTER.jpg - Google Drive](https://drive.google.com/file/d/0BwQ4esYYFC04bm9VX3ZfMFJlLWc/view?usp=sharing)   

## Universo observable y tiempo
[twitter AstroAhura/status/1551573971216150529](https://twitter.com/AstroAhura/status/1551573971216150529)  
This is one of my favorite animations on how to think about galaxy images across the observable universe (if humans aged over 13.4 billion years!)  
From a 2013 episode of NHK's Cosmic Front on the first stars featuring @annafrebel, @astro_nino, @LordMartinRees, myself, and others  
Here's another gem from the episode giving a sense of how far back (lookback time in units of 100 Myr in lower-right) we're seeing these early galaxies  
A little dramatic but I like it  

## Constante de Hubble 

[The Webb Telescope Further Deepens the Biggest Controversy in Cosmology](https://www.quantamagazine.org/the-webb-telescope-further-deepens-the-biggest-controversy-in-cosmology-20240813/)  
![](https://www.quantamagazine.org/wp-content/uploads/2024/08/Hubble_Tension-crMarkBelan-Desktop-v2.svg)


## Medio interestelar

[twitter imartividal/status/1512045354765664256](https://twitter.com/imartividal/status/1512045354765664256)  
¡Hola de nuevo! Como hace tiempo que no publico un hilo (tengo la agenda copada con algo muy muy chulo que os contaré dentro de poco), acabo de prepararos un nuevo hilo ligerito. Esta vez, fliparemos con "el medio interestelar":  
Mucha gente piensa que el espacio entre las estrellas está "vacío", pero la realidad es bien distinta: en el "medio interestelar" puede haber mogollón de cosas muy interesantes. Necesitaríamos un hilo más largo que el pelo de Rapunzel para hablar de todas ellas:  
Desde plasma, materia oscura, gas molecular o polvo estelar, hasta el fondo cósmico de microondas, los rayos cósmicos... En otros hilos iremos hablando sobre estas maravillas, pero en éste nos centraremos solamente en... el gas interestelar (¡y os prometo que no os aburrirá!).  
Las estrellas (todas ellas, incluido el Sol) nacen en titánicos conglomerados de gas y polvo llamados "nubes moleculares". Buscad un poco por el ciberespacio y encontraréis fotos de algunas de estas nubes... Pero hay algo chocante sobre estos objetos que no muchos saben:  
Aunque en las fotos parece que esas nubes son muy densas, en realidad están prácticamente vacías: suele haber unos pocos cientos de átomos por centímetro cúbico. Eso es tan poquito, que cada átomo, moviéndose libremente por ahí, puede tardar siglos en chocar con algún compañero.  
Pero entonces, ¿por qué en las fotos parecen tan densas? ¡Pues porque esas regiones son enormes! ¡En unos pocos de esos píxeles, cabe todo un sistema solar! De ahí que a simple vista parezcan tan "llenas". ¡Hay un buen montón de átomos a través de cada píxel!  
Una de las emisiones de radio más interesantes que recibimos de estas nubes es la llamada "línea del hidrógeno" (o "HI"), producida cuando el electrón que orbita a un protón cambia la dirección en la que "rota" sobre su propio eje. Al hacer esto, libera un tenue rayito de luz.  
Cada átomo de hidrógeno, en solitario, tardaría unos 10 millones de años en emitir un solo "rayito" de luz en esa "línea HI". Habéis leido bien: 10 millones de años (o más) es el tiempo necesario entre rayito y rayito para un solo átomo de hidrógeno.  
¡Wow! ¿Y podemos detectar desde la Tierra esas emisiones producidas de forma tan poco frecuente? Hablamos de un sitio donde un átomo suele chocar una vez cada siglo, para emitir un rayito de luz que, de estar en solitario, tardaría millones de años en emitir...  
Pero claro, esas nubes son tan grandes, que incluso procesos tan difíciles de ocurrir como la emisión HI ocurren de forma bastante frecuente EN TODA LA NUBE. De hecho, desde la Tierra podemos captar estas débiles emisiones de radio sin problemas.  
Aquí os paso una figurita muy chula en la que se muestra cómo cambia la forma de la línea HI a lo largo de casi toda nuestra galaxia. A partir de esta figurita, podemos hacer cosas formidables, como medir la rotación de la Vía Láctea y detectar la presencia de materia oscura.  
![](https://pbs.twimg.com/media/FPvUo-XXMAIGA89?format=png)  
Esa figurita está hecha con una antena de solo un par de metros de diámetro. De hecho, la línea HI es tan fácil de detectar (¡incluso desde nubes en el otro lado de la Galaxia!), que podemos verla incluso si usamos un simple *cubo de pintura*, en lugar de un radiotelescopio.  
**Sí, habéis leido bien. Hay gente que ha llegado a detectar el HI galáctico usando un cubo de pintura. ¡Wow! MacGyver se tragaría el chicle del flipe: ¡detectar la presencia de materia oscura usando un cubo y un poco de electrónica de los chinos!**  
[Weighing the Milky Way? Yes we CAN! - astron.nl](https://www.astron.nl/dailyimage/main.php?date=20211117)  
![](https://www.astron.nl/dailyimage/pictures/20211117/paint-cans-observe-Milky-Way.png)  
(aunque, para hacerlo muy bien, se necesita "calibrar" el cubo de pintura y, dependiendo de la calidad que se busque, eso puede complicar un poquitín el experimento.)  
En esas nubes moleculares, no solamente vemos la línea HI. También podemos detectar emisión del llamado "hidrógeno ionizado", que se produce típicamente en regiones más calientes donde están formándose nuevas estrellas. Y, además, también podemos detectar moléculas  
Se ha llegado a detectar agua, glucosa, cafeina, diversos azúcares, alcoholes,... vamos, que nos podríamos hacer un buen carajillo usando solamente sustancias del medio interestelar. Ahí lo tenéis: ¡los ladrillos de la vida esparcidos entre las estrellas!  
![](https://pbs.twimg.com/media/FPvVPdlWUAwtB0-?format=png)  
Dentro de poco, en otro hilo, os hablaré de una nube molecular muy especial, de la que un colega mío publicó, hace años, un resultado que cuando os lo cuente os hará explotar el cerebro. Tiene que ver con el Big Bang y la expansión del Universo. ¡Manteneos a la escucha!  

[twitter imartividal/status/1667896478675746816](https://twitter.com/imartividal/status/1667896478675746816)  
Lo que hace a este astro tan especial es que la luz que forma ambas imágenes atraviesa densas nubes moleculares de la galaxia lente (o sea, "criaderos de estrellas" de aquel remoto lugar), que dejan impresas en la luz unas "huellas dactilares" de las moléculas que hay allá.  
![](https://pbs.twimg.com/media/FyWLUpNXsAAPnzl?format=png)  

## Evolución estelar
 [Diagrama de Hertzsprung-Russell - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Diagrama_de_Hertzsprung-Russell)   
 [![https://molasaber.files.wordpress.com/2014/11/evolucion-estelar_es.png?w=766&amp;h=1166](https://molasaber.files.wordpress.com/2014/11/evolucion-estelar_es.png?w=766&h=1166 "https://molasaber.files.wordpress.com/2014/11/evolucion-estelar_es.png?w=766&amp;h=1166") ](https://molasaber.files.wordpress.com/2014/11/evolucion-estelar_es.png?w=766&h=1166)   
 [Evolución estelar para todos los públicos &#8211; Mola Saber](https://molasaber.org/2014/11/18/evolucion-estelar-para-todos-los-publicos/)  [https://twitter.com/MolaSaber/status/1105195888203481089](https://twitter.com/MolaSaber/status/1105195888203481089) Tengo especial cariño a esta #infografía con la que empecé mi andadura en las redes y que ahora comparto mejorada.«La Evolución de las Estrellas», desde que nacen y, según su masa, en lo que se convierten hasta el final de su vida. Espero que os guste. [![](https://molasaber.files.wordpress.com/2014/11/poster-estrellas.jpg "") ](https://molasaber.files.wordpress.com/2014/11/poster-estrellas.jpg)   
 [poster-estrellas.jpg](https://molasaber.files.wordpress.com/2014/11/poster-estrellas.jpg)   
  
 [![http://pictoline.com/wp-content/uploads/2017/03/a-2.png](http://pictoline.com/wp-content/uploads/2017/03/a-2.png "http://pictoline.com/wp-content/uploads/2017/03/a-2.png") ](http://pictoline.com/wp-content/uploads/2017/03/a-2.png)   
 [](http://pictoline.com/6836-de-nubes-de-hidrogeno-a-enanas-blancas-y-hoyos-negros-la-vida-y-muerte-de-las-estrellas-en-un-solo-diagrama/)   

## Redshift de las galaxias e interpretación
En estándares LOMCE se indica  
*"20.2. Explica la teoría del Big Bang y discute las evidencias experimentales en las que se apoya, como son la radiación de fondo* y el efecto Doppler relativista."*   
Pero en mi opinión es un error: la evidencia experimental del Big Bang es el redshift de la luz proveniente de una galaxia es proporcional a la distancia a la que se encuentra (la ley de Hubble), pero no se interpreta necesariamente mediante Doppler.  
Referencias: [http://faii.etsii.upm.es/dfaii/Docencia/Material Docente/Introduccción a la Cosmología I/II-Ley de Hubble v1.pdf ](http://faii.etsii.upm.es/dfaii/Docencia/Material%20Docente/Introduccci%C3%B3n%20a%20la%20Cosmolog%C3%ADa%20I/II-Ley%20de%20Hubble%20v1.pdf)  apartado 4.1 *"La expansión del espacio **En realidad la ley de Hubble [3.2] afirma simplemente que el redshift de la luz que proviene de una galaxia distante es proporcional a su distancia. Su interpretación vía Doppler u otros efectos es un tema distinto.*  
Los teóricos comprobaron inmediatamente que estas observaciones podían ser explicadas por un mecanismo distinto para producir el redshift. Estas observaciones corroboraban el trabajo de Alexander Friedmann , que en 1922 había establecido las famosas ecuaciones que llevan su nombre, un conjunto de ecuaciones que gobierna la expansión del espacio en modelos homogéneos e isotrópicos del universo dentro del contexto de la Relatividad General. En estos modelos el espacio-tiempo es dinámico; el universo no es estático: o bien se expande o bien se contrae. La ley de Hubble que correlacionaba los redshifts y las distancias era precisamente la requerida por esos modelos de cosmología derivados de la rela tividad general, los modelos con métrica FRW (Friedmann-Robertson-Walker)."  
  
 [La expansión del Universo: fotones que se estiran - Naukas](http://naukas.com/2011/10/05/la-expansion-del-universo-fotones-que-se-estiran/)   
 [Desplazamiento al rojo](http://www.astronomia.net/cosmologia/redshift.htm)  Copyright 1996-2006  [Pedro J. Hernández](http://www.astronomia.net/cosmologia/creditos.htm)   
Por tanto es importante tener en cuenta que la  [interpretación Doppler](http://www.astronomia.net/cosmologia/Doppler.htm)  para el desplazamiento al rojo no es válida en general y es sólo una buena aproximación cuando z es significativamente menor que la unidad.  
Muchos autores quieren solucionar este asunto apelando a la  [fórmula relativista para el efecto Doppler](http://www.astronomia.net/cosmologia/metricRE.htm#f%C3%B3rmula%20relativista%20para%20el%20efecto%20Doppler)   
v = c (1- (1+z)-2)1/2  
válida para cualquier desplazamiento al rojo. Sin embargo, esto no es correcto. El universo en expansión está descrito por la  [Teoría General de la Relatividad](http://www.astronomia.net/cosmologia/metricRG.htm)  y no por la  [Teoría Especial de la Relatividad](http://www.astronomia.net/cosmologia/metricRE.htm) .  

## Principio cosmológico
 [Principio cosmológico - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Principio_cosmol%C3%B3gico)   
  
The cosmological principle is not in the sky (2017)  [1611.02139.pdf](https://arxiv.org/pdf/1611.02139.pdf)   

## Simulaciones evolución Universo
 [Simulaciones por ordenador de la evolución del cosmos - La Ciencia de la Mula Francis](http://francis.naukas.com/2011/07/07/simulaciones-por-ordenador-de-la-evolucion-del-cosmos/)   
  
 [DEUS consortium &#8211; Dark Energy Universe Simulations | Dark matter &amp; dark energy simulations](http://www.deus-consortium.org/)   

## Vídeos
 [Estrellas orbitando durante 20 años el agujero negro central de nuestra galaxia | Microsiervos (Ciencia)](http://www.microsiervos.com/archivo/ciencia/estrellas-orbitando-agujero-negro-galaxia.html)   
Ver en  [recursos escala / notación científica](/home/recursos/recursos-notacion-cientifica)   

Viaje a los limites del universo HD Es - Natgeo  
[![](https://img.youtube.com/vi/hpWuWdwt1Wc/0.jpg)](https://www.youtube.com/watch?v=hpWuWdwt1Wc "Viaje a los limites del universo HD Es - Natgeo")   

In the Blink of an Eye: Space in an Instant - melodysheep  
[![](https://img.youtube.com/vi/fdQyD8B_odY/0.jpg)](https://www.youtube.com/watch?v=fdQyD8B_odY "In the Blink of an Eye: Space in an Instant - melodysheep")   

## Espacio profundo

[![](https://img.youtube.com/vi/yDiD8F9ItX0/0.jpg)](https://www.youtube.com/watch?v=yDiD8F9ItX0 "Deep Field: The Impossible Magnitude of our Universe")   

[![](https://img.youtube.com/vi/2sUrauA0iq4/0.jpg)](https://www.youtube.com/watch?v=2sUrauA0iq4 "Hubble's UItra Deep Field in 3D is an amazing journey through space and time")   

## JWST (James Webb Space Telescope)  

[Imágenes del JWST en HD y con su ubicación en el cielo](http://web.wwtassets.org/specials/2022/jwst-release/)  

[![](https://img.youtube.com/vi/3JtSxxGsrrQ/0.jpg)](https://www.youtube.com/watch?v=3JtSxxGsrrQ "Cosas muy sencillas que nadie te ha explicado sobre el telescopio JAMES WEBB")   


## App para móvil
  
Cosmología física,  [Kirill Sidorov ](https://play.google.com/store/apps/developer?id=Kirill+Sidorov)   
Información para consulta, toma cosas de wikipedia  
 [Physical cosmology - Apps on Google Play](https://play.google.com/store/apps/details?id=com.do_apps.catalog_840)   
 
