
# Electrón

 [![El Blues del Electrón](https://img.youtube.com/vi/v9Zcfy9cb4I/0.jpg)](https://www.youtube.com/watch?v=v9Zcfy9cb4I)   
 
[Medida de la unidad fundamental de carga (Millikan) - sc.ehu.es (java)](http://www.sc.ehu.es/sbweb/fisica/elecmagnet/millikan/millikan.html) 

[Medida de la relación carga/masa (Thomson) - sc.ehu.es (java)](http://www.sc.ehu.es/sbweb/fisica/elecmagnet/thomson/Thomson.html) 

[Experimento de Millikan - ld-didactic.de](https://www.ld-didactic.de/software/524221es/Content/ExperimentExamples/Physics/AtomicAndNuclearPhysics/Millikan.htm)  

[Francis en MUY Interesante: Campos muy cuánticos y El vacío lleno de cuántica](https://francis.naukas.com/2023/12/25/francis-en-muy-interesante-campos-muy-cuanticos-y-el-vacio-lleno-de-cuantica/)  
> «Solo existe un único electrón en el universo, que se propaga por el espacio y el tiempo de tal forma que parece que está en muchos sitios de forma simultánea». En apariencia, esta frase carece de sentido, ya que sabemos que dos átomos separados están rodeados por electrones diferentes. Sin embargo, el físico Richard Feynman, padre de la electrodinámica cuántica en 1949, pronunció estas palabras en su discurso Nobel de 1965; relató cómo su director de tesis, John Wheeler, le deslumbró con esta idea en 1940: el campo cuántico del electrón es único.  

Electrons DO NOT Spin - PBS Space Time
[![](https://img.youtube.com/vi/pWlk1gLkF2Y/0.jpg)](https://www.youtube.com/watch?v=pWlk1gLkF2Y "Electrons DO NOT Spin - PBS Space Time
")   

[EL ELECTRÓN: 10 curiosidades que no te contaron en la escuela porque no era el momento (Parte I) - Ciencia llana -  Javier Guerrero Ruano](https://javierguerreroruano.blogspot.com/2022/09/el-electron-10-curiosidades-que-no-te.html)  

[EL ELECTRÓN: 10 curiosidades que no te contaron en la escuela porque no era el momento (Parte II) - Ciencia llana -  Javier Guerrero Ruano](https://javierguerreroruano.blogspot.com/2024/08/el-electron-10-curiosidades-que-no-te.html)  


