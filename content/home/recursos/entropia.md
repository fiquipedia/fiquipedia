# Entropía 

Es un concepto no limitado a Física ni a Química. Relacionado con [Termodinámica](/home/recursos/termodinamica) y [Termoquímica](/home/recursos/quimica/termoquimica) 

[![](https://img.youtube.com/vi/DxL2HoqLbyA/0.jpg)](https://www.youtube.com/watch?v=DxL2HoqLbyA "The Most Misunderstood Concept in Physics - Veritasium")

[Entropy Explained, With Sheep, From Melting Ice Cubes to a Mystery About Time, By Aatish Bhatia](https://aatishb.github.io/entropy/)  

[twitter AndrewM_Webb/status/1182208172100116480](https://twitter.com/AndrewM_Webb/status/1182208172100116480)  
\#ThermodynamicsThursday Simulating a system of gases, and using compressed image filesize as an ersatz measure of its thermodynamic entropy  
  
[Order, Disorder, and Entropy: Misconceptions and Misuses of Thermodynamics](http://osumarion.osu.edu/sciencecafe/October10ScienceCafe.ppt)  
Michael B. Elzinga, Kalamazoo Area Mathematics and Science Center, A Presentation for Science Café, The Ohio State University at Marion, October 5, 2010  

Removing the entropy from the definition of entropy: clarifying the relationship between evolution, entropy, and the second law of thermodynamics  
[http://www.evolution-outreach.com/content/6/1/30](http://www.evolution-outreach.com/content/6/1/30)  
Martin et al, cc-by  

Teaching and Learning of Energy in K – 12 Education (Google eBook)  
[http://books.google.es/books?id=SSu_BAAAQBAJ](http://books.google.es/books?id=SSu_BAAAQBAJ)  
Robert F. Chen, Arthur Eisenkraft, David Fortus, Josep Krajcik, Knut Neumann, Jeffrey C. Nordine, Allison Scheff  
Springer Science & Business Media, 9/4/2014  
Especialmente interesante capítulo "8. Energy Spreading or Disorder? Understanding Entropy from the Perspective of Energy"  
  
[Historical burdens on physics.24 Entropy](http://www.physikdidaktik.uni-karlsruhe.de/publication/Historical_burdens/24_Entropy.pdf)  
Georg Job, University of Hamburg  

DON’T BE AFRAID - IT IS JUST ENTROPY. CALLENDAR’S IDEA ABOUT ENTROPY  
[http://lsg.ucy.ac.cy/girep2008/papers/DON’T BE AFRAID - IT IS JUST ENTROPY CALLENDAR’S IDEA ABOUT ENTROPY.pdf](http://lsg.ucy.ac.cy/girep2008/papers/DON%E2%80%99T%20BE%20AFRAID%20-%20IT%20IS%20JUST%20ENTROPY%20CALLENDAR%E2%80%99S%20IDEA%20ABOUT%20ENTROPY.pdf)  
Michael Pohlig. Didaktik der Physik Universität 76128 Karlsruhe  

Thermodynamics of Chemical Equilibrium. All about entropy and free energy.   
chem1 virtual textbook, a reference text for General Chemistry  
Stephen Lower, Simon Fraser University  
[http://www.chem1.com/acad/webtext/thermeq/index.html](http://www.chem1.com/acad/webtext/thermeq/index.html)  

[laplace.us.es, Entropía](http://laplace.us.es/wiki/index.php/Entrop%C3%ADa)  

[laplace.us.es, Cálculos_de_entropía_(GIE)](http://laplace.us.es/wiki/index.php/C%C3%A1lculos_de_entrop%C3%ADa_(GIE))   
  
[![¿Qué es la entropía? Javier Santaolalla, Date un Vlog](https://img.youtube.com/vi/LetmPf0XLBk/0.jpg "¿Qué es la entropía? Javier Santaolalla, Date un Vlog")](https://www.youtube.com/watch?v=LetmPf0XLBk)  

Entropía asociada a desorden  
[twitter emollick/status/1400885452261974020](https://twitter.com/emollick/status/1400885452261974020)  
One of my favorite scientific figures is this one of the entropy levels of 100 world cities by the orientation of streets. The cities with most ordered streets: Chicago, Miami, & Minneapolis. Most disordered: Charlotte, Sao Paulo, Rome & Singapore. Paper: 
[Urban spatial order: street network orientation, configuration, and entropy](https://appliednetsci.springeropen.com/articles/10.1007/s41109-019-0189-1)
![](https://pbs.twimg.com/media/E3DxNT6XIAMIVR9?format=jpg)  

[Diagrama Termodinamica, Temperatura vs entropía - apredamosingenieria](https://apredamosingenieria.blogspot.com/2017/07/definicion-al-definirse-la-entropia.html)  


