
# Recursos Física 2º Bachillerato

## Apuntes
Aparte de que en la página de  [recursos - apuntes](/home/recursos/apuntes)  hay apuntes, coloco aquí algunos ya concretos de física de 2º de bachillerato, aparte de enlazar a  [apuntes de elaboración propia de física de 2º de bachillerato](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato) .  
También se pueden buscar  [libros](/home/recursos/libros)  asociados a 2º de bachillerato, o bien  [recursos de física](/home/recursos/fisica)  en general, o también  [páginas y blog personales](/home/recursos/paginas-blog-personales)  

[Física 2º Bach. Guías, problemas, selectividad - Berto Tomás](https://drive.google.com/drive/folders/1akqqtkjCwimxR618abh_NGmNa-i2cp7G)  
[Aprendiendo Física con Berto Tomás](https://www.aprendiendofisicaconbertotomas.com/)  

[2ºBach Física - profesorjrc.es](https://profesorjrc.es/2fisica.htm) Jorge Rojo Carrascosa   

[2ºBach Física - elortegui.org](http://ciencia.elortegui.org/datos/2BACHFIS/fisica2.htm)  

[Apuntes, Escritos y Ensayos Científicos, Apuntes Física 2 Bachillerato](http://www.escritoscientificos.es/apunfisi/inicio.htm)   
Felipe Moreno Romero. Prof. Educación Secundaria (Andalucía). cc-by-nc-sa  

[ocw.upm.es, Apoyo para la preparación de los estudios de Ingeniería y Arquitectura, Física (Preparación para la Universidad)](http://ocw.upm.es/course/fisica-prep-universidad)   
Incluye "Materiales de estudio y lectura básicos" que se pueden asociar a física de 2º, repasando y profundizando   
 
[(https://adriantxu.eus/fisica-para-selectividad/](https://adriantxu.eus/fisica-para-selectividad/)   
[https://sites.google.com/sanviatorpastoral.es/fisicabachiller/página-principal](https://sites.google.com/sanviatorpastoral.es/fisicabachiller/p%C3%A1gina-principal)   

[Temas Física 2º Bachillerato. Fernando Escudero Ramos. IES Fernando de los Ríos, Quintanar del Rey (Cuenca)](http://ies-fernandorios.centros.castillalamancha.es/content/f%C3%ADsica-y-qu%C3%ADmica)  


[Física (autoformació IOC) - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=40)  
[Física II (Bloc 1) ~ gener 2020 - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=105)  
[Física II (Bloc 2) ~ gener 2020 - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=106)  

[Física en context 1n Batxillerat](https://sites.google.com/a/xtec.cat/fisicaencontext/f%C3%ADsica-en-context/2n-batxillerat)  
 

[uib.cat GuiA Física](http://dfs.uib.cat/apl/aac/fisicapbau/). Licenciamiento *Els enunciats d'aquestes pàgines i les imatges i els vídeos sense modificar, retallar o refer es poden usar amb finalitat educativa, sense ànim de lucre, citant l'autor de les pàgines i posant en lloc visible l'adreça d'aquest lloc web. Usau: "GuiA Física · Antoni Amengual Colom · UIB url: dfs.uib.cat/apl/aac/fisicapbau/".*  
[uib.cat GuiA Física, temas](http://dfs.uib.cat/apl/aac/fisicapbau/indexT.htm)  El nivell i els temes dels problemes d'aquest lloc web són els de segon de Batxillerat. Els problemes són d'especial interès per a l'alumnat que prepara la prova de batxillerat d'accés a la Universitat.

Vídeos Física 2º Bachillerato, ceroentropía [Física 2º Bachillerato - YouTube](https://www.youtube.com/playlist?list=PLhJ3K3-ToQfrz2C9u_Pn3kKiT2zkqtCs7)  
 [sites.google.com Física y Química](https://sites.google.com/site/fisicayquimicabeatriz/) Beatriz Jiménez Mahíllo, apartados Física 2º   
 [Curro Físico: Fisica 2ºBachillerato](http://currofisico.blogspot.com/p/fisica-2bachillerato.html) Internet y audiovisuales, Ejercicios con resultado, Ejercicios con solución, Hojas de ejercicios tipo PAU, Algunas prácticas de laboratorio  
 [ http://www.elortegui.org/ciencia/datos/2BACHFIS/fisica2.htm](http://www.elortegui.org/ciencia/datos/2BACHFIS/fisica2.htm)   
fisica2spp WIKI educativa para alumn@s de FISICA 2º Bachillerato   
 [INTRODUCCIÓN](https://fisica2spp.wikispaces.com/INTRODUCCI%C3%93N)   
 [PROYECTO+WIKIFIS](https://fisica2spp.wikispaces.com/PROYECTO+WIKIFIS)   
 Profesor: Crescencio Pascual Sanz, Colegio San Pedro Pascual  
 Profesor: Juan José Serrano Pérez, Instituto de Ciencia Molecular, Universitat de València  
 Licenciamiento cc-by-nc-sa FisQuiWeb. Apuntes Física 2º Bachillerato  
 [apun2BFis.htm](http://web.educastur.princast.es/proyectos/fisquiweb/Apuntes/apun2BFis.htm)   
Licenciamiento cc-by-nc-sa  
  
 Materiales cc-by-nc-sa en mediateca @educamadrid de David Matellano  
 [David M. | Mediateca de EducaMadrid](https://mediateca.educa.madrid.org/usuario/dmatellano)   
 Incluye Física 2º Bachillerato  
  
 Plataforma e-ducativa aragonesa. Unidades Didácticas ESPAD Física 2º BACHILLERATO   
 [index.cgi](http://e-ducativa.catedu.es/44700165/sitio/index.cgi?wid_item=126&wid_seccion=18)   
Unidades por bloques  
    
 [http://webs.ono.com/maitetono/](http://webs.ono.com/maitetono/)   
 José Antonio S Noriega. IES Fernando I de Valencia de Don Juan. Materiales Física 2º (y algo 1º Bach y 4º ESO)  
  
 Aula virtual de física 2º Bachillerato. Ejercicios resueltos de todo el curso (gallego, enlace obtenido en  [batxillerat](http://aulainf.com/materials/batxillerat) , realizado en curso 2010-2011 por Sònia Pérez  
 Seminari de Física i Química, IES l’Arboç)  
 [Aula Virtual 2n Batxillerat | Bloc funcionant amb el WordPress](http://blocs.xtec.cat/aulavirtual2nbat/)   
  
 FÍSICA DE 2º DE BACHILLERATO. TEMARIO DE CASTILLA-LA MANCHA:  
 [Fsica 2 Bachillerato](http://www.sociedadelainformacion.com/departfqtobarra/)   
 Jesús Ruiz Felipe. Profesor de Física del Instituto Cristóbal Pérez Pastor Tobarra (Albacete)  
  
 2º Bto. Física. Prof. Luis Bermejo  
 [Colegio e Internado Agustinos de León](http://www.agustinosleon.com/index.php?option=com_content&view=article&id=174&Itemid=120)   
 © Copyright 2006-2013 Colegio Ntra. Madre del Buen Consejo. PP. Agustinos de León. Centro Bilingüe . Todos los derechos reservados.   
  
 GRUPO HEUREMA. EDUCACIÓN SECUNDARIA. ENSEÑANZA DE LA FÍSICA Y LA QUÍMICA  
 Subsección: Apuntes de Física en Bachillerato (16-19 años)  
 [ApFBachill11.htm](http://heurema.com/ApFBachill11.htm)   
La página comienza con enlaces a física nuclear pero también contiene el índice de todos los bloques  
  
 Rincón Didáctico Física y Química. Consejería de Educación y Cultura. Gobierno de Extremadura.  
 [fisica-de-2](http://rincones.educarex.es/fyq/index.php/2-bachillerato/fisica-de-2)   
  
 Departamento de Física y Química del IES Leopoldo Queipo de Melilla.  
 [Apuntes y ejercicios | Física y Química](https://fisicayquimicaqueipo.wixsite.com/misitio-1/apuntes-y-ejercicios)   
 Apuntes y ejercicios por bloques. D. Miguel Sánchez Cazorla. Licenciamiento no detallado  
  
 Física de Bachillerato. Wiki de Emiliano González Flores. Departamento de Física y Química IES Vicente Aleixandre - Pinto (Madrid)  
 [Portada](http://emgonzalez-valeix-pinto.wikispaces.com/Portada)   
  
 [lawebdejuan - Física](https://sites.google.com/site/lawebdejlopez/fisica)   
Física 2º Bachillerato por bloques  
 Juan Antonio López Gomar  
  
 IES Valentín Turenzo  
 [2-bachillerato-fisica.html](http://fqcolindres.blogspot.com.es/p/2-bachillerato-fisica.html)   
Enlaces a recursos por unidades, y apartado "cursos completos" con enlaces a otras webs. Licenciamiento cc-by-nc-sa  
  
 [F&iacute;sica 2&ordm; BACH.](http://www.rinconsolidario.org/ciencias/fisica2.htm#)   
Miguel Ángel Queiruga @queiruga  
  
 FIQUIMAKCC. Recursos de Física, Química y Matemáticas. Física 2º Bachillerato  
 [index.php](http://www.fiquimakcc.es/index.php?option=com_content&view=category&id=88&Itemid=481)   
 Kico Calvillo, Licenciado en Física  
  
 Pruebas de Acceso a la Universidad, Apuntes de Física, Colecciones de Problemas y cuestiones, Material de Clase (2º Bachillerato) Curso 2010-2011, Exámenes Curso 2010 - 2011   
 [fisica.htm](http://selectividad.intergranada.com/fisica.htm)   
  
 Departamento de física y química del IES Élaios, Zaragoza.  
 [Departamento de F&iacute;sica y Qu&iacute;mica del I.E.S.&Eacute;laios](http://ieselaza.educa.aragon.es/DepartamentoFQ.htm#f2bto)   
Apuntes, ejercicios ....  
  
 Apuntes por bloques...  
 [EL FÍSICO LOCO: FÍSICA 2º BACH](http://elfisicoloco.blogspot.com.es/p/fisica-2-bach.html)   
 Javier Sánchez  
  
 [2º BACHILLERATO(FÍSICA) &#8211; Amhuertas](http://amhuertas.wordpress.com/2%C2%BA-bachillerato/)   
 Antonio de la Muñoza de Huertas.   
  
 [fisica2_enc.htm](http://webs.ono.com/mariadoloresmarin/fisica2_enc.htm)   
 Recopilación de materiales para enseñar / aprender la Física de 2º de Bachillerato en la Región de Murcia.  
 Autores: María Dolores Marín Hortelano y Manuel Ruiz Rojas, profesores de Física y Química, desde Cieza - Murcia - España.  
 En julio 2014 indica Ultima actualización: Julio de 2009.  
  
 [https://sites.google.com/site/lafisicaesfacil/home/2o-de-bachillerato](http://sites.google.com/site/lafisicaesfacil/home/2o-de-bachillerato)   
 [enrique.garciadebustos](https://www.educa2.madrid.org/web/enrique.garciadebustos)   
Página realizada con Enrique García De Bustos Sierra, con recursos por bloques y unidades, licenciamiento no detallado.  
  
 Todos los apuntes de Física de 2º de Bachillerato  
 [http://sitovg.wordpress.com/2012/06/14/todos-los-apuntes-de-fisica-de-2o-de-bachillerato/](http://sitovg.wordpress.com/2012/06/14/todos-los-apuntes-de-fisica-de-2o-de-bachillerato/)   
 Por bloques y partes, indicando licenciamiento, por ejemplo gravitación por Felipe Moreno Romero cc-by-nc-sa (*sitio no disponible en julio 2014*)  
  
 [pmwiki.php](http://www.juntadeandalucia.es/averroes/~04001205/pmwiki/pmwiki.php?n=Fyq.F%edsicaParaSegundoDeBachillerato)   
cc-by-nc-sa  
 incluye apuntes y problemas por temas  [Update your browser to use Google Drive, Docs, Sheets, Sites, Slides, and Forms - Google Drive Help](https://docs.google.com/folder/d/0B-veG_ag_q8fQXhqTWViNlRmclE/edit)   
  
Departamento física y química IES Valle del Arlanza, Lerma, Burgos.   
 [print.cgi](http://iesvalledelarlanza.centros.educa.jcyl.es/sitio/print.cgi?wid_seccion=14&wid_item=44&wOut=print)   
 En 2015 apuntes y ejercicios física 2º bachillerato y química 2º bachillerato  
  
 [fisica-2-bachiller-ejercicios-resueltos.html](http://profesor10demates.blogspot.com.es/2014/09/fisica-2-bachiller-ejercicios-resueltos.html)   
  
 Recursos de un profesor de física y química de la enseñanza pública  
 [Física de 2º de Bachillerato](http://felixagm.es/fisica2bachillerato.html)   
 Copyright © 2013 fagm 1.99 All rights reserved.  
  
 [FÍSICA 2º BACH | La web de Chema Martín](https://chemamartin.wordpress.com/fisica-2o-bach/) Apuntes y ejercicios por temas, y se incluyen enunciados selectividad CyL  
Física 2n de Batxillerat, Angel Juan Martínez,  
 [F&iacute;sica 2n de Batxillerat. Materilas](http://www.ticfisquim.org/f2b/f2bm.htm)   
  
Fisica de 2º de Bachillerato - Colegio Montpellier. Leandro Bautista  
 [http://www.freewebs.com/fisicamontpe/](http://www.freewebs.com/fisicamontpe/)   
 (*sitio no disponible en julio 2014*)  

## Libros libres 
En general con los materiales de la página intento que no sea necesario un libro como tal: en 2018 he conseguido estar 4 cursos sin usar libro en Física de 2º de Bachillerato usando mis materiales, que comparto.  
En agosto 2018 se publica por  [@vielbein](https://twitter.com/vielbein)  una primera versión de libro completo para Física 2º Bachillerato.   
 [Dropbox - physics2bach.pdf - Simplify your life](https://www.dropbox.com/s/0ttyjow49l5p5cl/physics2bach.pdf?dl=0)  Tiene 363 páginas, y a veces es llamado "el tocho"  

## Recursos por bloques 
Es un borrador, la idea es intentar ir desglosando recursos por bloquesA veces hay un bloque inicial de repaso de física de 1º (cinemática, dinámica, energía y cálculo vectorial). Es algo que se asume conocido en Física de 2º y se asume conocido al entrar en estudios universitarios: una idea aproximada de lo que puede contener en Curso preparatorio de Matemáticas y de Física  [cursocero.pdf](http://www.uhu.es/gem/docencia/fisica-ccaa/descargas/cursocero.pdf)  Facultad de Ciencias Experimentales, Universidad de Huelva, del que incluyo un extracto de la introducción  
*En ocasiones, los profesores de Matemáticas y Física, nos quejamos de las dificultades que encontramos en nuestro trabajo y son muchas las veces que tenemos la sensación de predicar en el desierto. Recíprocamente, muchos de vosotros tenéis, de vez en cuando, la sensación de que el profesor habla para una especie de “super-alumno”. Aunque entrar en un análisis pormenorizado de las causas que provocan estos desencuentros va más allá de las pretensiones de esta breve introducción, sí hay una cosa que está clara: por lo general, vuestra formación de carácter matemático y/o físico es francamente deficitaria. Así las cosas, el riesgo de fracaso es alto pues, como suele decir Juan María Arzak,“para aprender a bordar, primero hay que saber coser”*  
  
 [Movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio)  (con LOMCE pasa a 1º Bachillerato)  
 [Movimiento ondulatorio](/home/recursos/fisica/movimiento-ondulatorio)   
 [Gravitación](/home/recursos/fisica/recursos-gravitacion)  (con LOMCE parte pasa a 1º Bachillerato)  
 [Electricidad](/home/recursos/fisica/recursos-campo-electrico)   
 [Magnetismo](/home/recursos/fisica/recursos-campo-magnetico)   
 [Óptica](/home/recursos/fisica/recursos-optica)   
 [Relatividad](/home/recursos/fisica/recursos-fisica-relativista)   
 [Mecánica cuántica](/home/recursos/fisica/recursos-fisica-cuantica)   
 [Física nuclear](/home/recursos/fisica/recursos-fisica-nuclear)   
 [Física de partículas](/home/recursos/fisica/fisica-de-particulas)  (nuevo con LOMCE)  
 [Cosmología](/home/recursos/fisica/recursos-cosmologia)  (nuevo con LOMCE)  
  
Enlaces con recursos, animaciones y bibliografía por bloques, asociado a libro física 2º de McGrawHill  
 [FÍSICA enlaces](http://www.mhe.es/bachillerato/fisica_quimica/844817027X/archivos/media/esp/enlaces.html)   
 [FÍSICA bibliografía](http://www.mhe.es/bachillerato/fisica_quimica/844817027X/archivos/media/esp/bibliografia.html)   

## Prácticas de laboratorio
 A veces los recursos de cada bloque pueden citar alguna, y en la página general de  [prácticas de laboratorio](/home/recursos/practicas-experimentos-laboratorio)  y de  [simulaciones](/home/recursos/simulaciones)  puede haber alguna que aplique.   
 Se ponen aquí enlaces concretos.  
  
Algunas muy sencillas en  
  
 Física General, (C) Ignacio Martín Bragado. imartin@ele.uva.es, 12 febrero 2003  
 [fisica-general-libro-completo.pdf](http://fisicas.ucm.es/data/cont/media/www/pag-39686/fisica-general-libro-completo.pdf)   
 Incluye Bloque III Prácticas de laboratorio que incluyen  
  
 Experiencia de Faraday. Inducción.  
 Experiencia de Oersted.  
 Uso elemental de un osciloscopio  
 Estudio de un péndulo.  

## Simulaciones física
En principio en la página de  [recursos de simulaciones](/home/recursos/simulaciones)   
En esta página de McGraw Hill España para un libro de física de 2º de Bachillerato se centralizan muchos enlaces a simulaciones  
 [FSICA](http://www.mhe.es/bachillerato/fisica_quimica/844817027X/archivos/media/esp/enlaces.html)   
  
 [fisica.htm](http://www.edu365.cat/batxillerat/ciencies_tecnologia/fisica.htm) Página de la Generalitat de Cataluña, en catalán, con recursos (fundamentalmente simulaciones) por bloques.  

## Apuntes elaboración propia
 Ver página separada  [apuntes elaboración propia física 2º Bachillerato](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato) .  

## Ejercicios elaboración propia
 Ver página separada  [ejercicios elaboración propia física 2º Bachillerato](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato) .  
 
## Memes
Pueden servir para ilustrar ideas, y a veces para entender errores.  
Aquí varios recopilados (si la cuenta tiene candado, pueden no verse)  
[twitter bjmahillo/status/1191360405152714752](https://twitter.com/bjmahillo/status/1191360405152714752)  
Tras el primer y desastroso examen de Física de 2° Bach. he mandado a los alumnos hacer memes con sus errores.   
Porque no se extrañan cuando:   
- Les salen planetas de masas de 60 kg.  
- Los periodos de satélites son de 0,3 s.  
- La gravedad de un planeta vale 20000 m/s2.  
Ya me han entregado los alumnos de Física de 2º Bach. los memes que han hecho con los fallos de su último examen. Espero que les haya servido para repasar los errores más frecuentes y no se vuelvan a repetir.  
Lo han hecho muy bien y nos hemos reído mucho.  
Aquí los mejores   


