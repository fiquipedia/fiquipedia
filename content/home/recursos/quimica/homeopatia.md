# Recursos homeopatía

La homeopatía se relaciona con [disoluciones](/home/recursos/quimica/disoluciones) y [concentración](/home/recursos/quimica/concentracion) y es un buen ejemplo para usar conocimiento científico para desmontar una pseudociencia.  
Hay más pseudociencias asociadas al agua que la homeopatía [H2O dot com Water-related pseudoscience fantasy and quackery](https://www.chem1.com/CQ/)  

[researchgate.net La aproximación crítica a las pseudociencias como ejercicio didáctico: Homeopatía y diluciones sucesivas](https://www.researchgate.net/publication/267451404_La_aproximacion_critica_a_las_pseudociencias_como_ejercicio_didactico_Homeopatia_y_diluciones_sucesivas)  
Se proponen tres actividades docentes con distintos grados de sofisticación, apropiadas para su implementación en aulas y laboratorios desde la ESO hasta los primeros cursos de Universidad. En todas ellas se trabajan las diluciones sucesivas y al mismo tiempo se repasan los conceptos de cantidad de sustancia, mol y concentración. Transversalmente, se incide en el fraude pseudocientífico de la homeopatía, que en años recientes suscita un interés preocupante. Se concluye que los conocimientos fundamentales de química son suficientes para evitar este tipo de engaños.  
...  
Unos conocimientos básicos de química, unidos a la actitud crítica que se deriva de una educación científica, son herramientas necesarias y suficientes para resistir a engaños como el de la homeopatía. Pensamos que es responsabilidad de los científicos y docentes no solamente el educar y ofrecer estas herramientas de defensa, sino el combatir activamente a las pseudociencias. Como hemos mostrado en este trabajo, es posible hacerlo mientras se enseñan conceptos básicos en cualquier currículum de química tales como el mol y la concentración de una disolución.  

[isqch.wordpress.com El número de Avogadro y la homeopatía](https://isqch.wordpress.com/2012/05/05/el-numero-de-avogadro-y-la-homeopatia/)  

[laverdad.es Desmontando la homeopatía con química básica](https://www.laverdad.es/ababol/ciencia/desmontando-homeopatia-quimica-20180320003831-ntvo.html)  
  
[dialnet.unirioja.es Química vs Homeopatía, 2006](https://dialnet.unirioja.es/descarga/articulo/2082924.pdf)  
Fernando Ignacio de Prada Pérez de Azpeitia  

En 2019 me planteo un proyecto de investigación donde intento realizar cálculos de conversión entre concentraciones "C" y molaridad, y para eso es necesario aclarar el punto de partida de C.  
No tengo claro si desde homeopatía está bien definido, pero en "Química vs Homeopatía, 2006" se cita  
*Para realizar las diluciones centesimales se toma 1 mL del *soluto y se mezcla con 99mL de disolvente, agitándolo para obtener un volumen de 100 mL de disolución, (concentración 1:100) tras lo cual se toma de nuevo un mL de la disolución resultante, se diluye y agita o "dinamiza" con otros 99 mL de disolvente (concentración 1:1.000).*  
Luego se puede plantear que una disolución "1 C" ó "1 CH" es realmente una disolución aproximada/cualitativamente "1 % en volumen", asumiendo volúmenes aditivos.Si consideramos un soluto sólido, podemos plantear 1 g en 99 g de agua, aproximada/cualitativamente "1 % en peso"  

[Vivimos un 'suicidio homeopático' en directo: ¿qué pasa si tomamos una caja entera de estos productos?](https://www.lasexta.com/programas/la-roca/vivimos-suicidio-homeopatico-directo-que-pasa-tomamos-caja-entera-estos-productos_20211017616c37c5cc1bb00001482db4.html)  
El divulgador Javier Fernández Panadero se ha tomado una caja entera de producto homeopático para demostrar demostrar que no produce ningún efecto.   

[![James Randi explica la homeopatía (subtitulado)](https://img.youtube.com/vi/drPNYLVsGQM/0.jpg)](https://www.youtube.com/watch?v=drPNYLVsGQM "James Randi explica la homeopatía (subtitulado)")   
Extracto de la conferencia de Princeton de 2001 en el que James Randi explica los fundamento y mecanismos de la homeopatía.
Subtitulado al castellano por Creareify.  

[![Homeopathic A&E (with subtitles)](https://img.youtube.com/vi/p2FT8kNCYMs/0.jpg)](https://www.youtube.com/watch?v=p2FT8kNCYMs "Homeopathic A&E (with subtitles)")   
Homeopathic A&E, from 'That Mitchell and Webb Look.' 
With English subtitles.  

[twitter MestreMolar/status/1603688791809589249](https://twitter.com/MestreMolar/status/1603688791809589249)  
En estos 2 siguientes tuits tenéis la actividad completa, por si os sirve:  
![](https://pbs.twimg.com/media/FkFydDDWAAA1wYU?format=jpg)  

![](https://pbs.twimg.com/media/FkFydDCXgAYwupz?format=jpg)  

