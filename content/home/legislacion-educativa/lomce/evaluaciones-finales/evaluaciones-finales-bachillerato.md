---
aliases:
  - /home/legislacion-educativa/evaluaciones-finales/evaluaciones-finales-bachillerato
---

# Evaluaciones Finales Bachillerato

ToDO: tabla de contenido con contenido previo a migración, pendiente implementar enlaces  
   1.  [1 Nombre de las Evaluaciones Finales de Bachillerato](#TOC-Nombre-de-las-Evaluaciones-Finales-de-Bachillerato)   
   1.  [2 Calendario de implantación y efectos de las pruebas](#TOC-Calendario-de-implantaci-n-y-efectos-de-las-pruebas)   
      1.  [2.1 Pruebas 2017-2018](#TOC-Pruebas-2017-2018)  
      1.  [2.2 Pruebas 2018-2019](#TOC-Pruebas-2018-2019)  
      1.  [2.3 Pruebas 2019-2020](#TOC-Pruebas-2019-2020)  
   1.  [3 Concepto de nota de corte con reválidas: Bachillerato + reválida es "como mínimo, del 60%"](#TOC-Concepto-de-nota-de-corte-con-rev-lidas:-Bachillerato-rev-lida-es-como-m-nimo-del-60-)  
   1.  [4 Obligatoriedad realizar la evaluación final para estudiantes provenientes Bachillerato LOE](#TOC-Obligatoriedad-realizar-la-evaluaci-n-final-para-estudiantes-provenientes-Bachillerato-LOE)  
      1.  [4.1 Transición LOE/LOMCE en 2º Bachillerato con materias pendientes 2º Bachillerato LOE: cursar LOE/LOMCE y/o reválida (relacionados)](#TOC-Transici-n-LOE-LOMCE-en-2-Bachillerato-con-materias-pendientes-2-Bachillerato-LOE:-cursar-LOE-LOMCE-y-o-rev-lida-relacionados-)  
      1.  [4.2 Transición LOE/LOMCE en PAU: mantenimiento de la nota PAU anterior a implantación reválidas, y acceso a universidad en el caso de no haber realizado/superado PAU](#TOC-Transici-n-LOE-LOMCE-en-PAU:-mantenimiento-de-la-nota-PAU-anterior-a-implantaci-n-rev-lidas-y-acceso-a-universidad-en-el-caso-de-no-haber-realizado-superado-PAU)  
      1.  [4.3 Análisis evaluación final para estudiantes provenientes Bachillerato LOE por comunidad](#TOC-An-lisis-evaluaci-n-final-para-estudiantes-provenientes-Bachillerato-LOE-por-comunidad)  
   1.  [5 Cómo serán las evaluaciones finales de Bachillerato](#TOC-C-mo-ser-n-las-evaluaciones-finales-de-Bachillerato) 
      1.  [5.1 Fechas de convocatoria ordinaria y extraordinaria anual de las evaluaciones finales de Bachillerato](#TOC-Fechas-de-convocatoria-ordinaria-y-extraordinaria-anual-de-las-evaluaciones-finales-de-Bachillerato)  
      1.  [5.2 Dónde se realizarán las evaluaciones finales de Bachillerato](#TOC-D-nde-se-realizar-n-las-evaluaciones-finales-de-Bachillerato)  
      1.  [5.3 ¿En la evaluación final de Bachillerato pueden entrar estándares de aprendizaje evaluables de 1º de Bachillerato?](#TOC-En-la-evaluaci-n-final-de-Bachillerato-pueden-entrar-est-ndares-de-aprendizaje-evaluables-de-1-de-Bachillerato-)  
      1.  [5.4 Quién corregirá y calificará las evaluaciones finales de Bachillerato](#TOC-Qui-n-corregir-y-calificar-las-evaluaciones-finales-de-Bachillerato)  
      1.  [5.5 ¿Serán las evaluaciones finales iguales para todas las comunidades autónomas?](#TOC-Ser-n-las-evaluaciones-finales-iguales-para-todas-las-comunidades-aut-nomas-)  
      1.  [5.6 Posibilidad de cursar materias específicas / materias no cursadas](#TOC-Posibilidad-de-cursar-materias-espec-ficas-materias-no-cursadas)  
   1.  [6 Detalles sobre evaluaciones finales Bachillerato publicados por distintas universidades](#TOC-Detalles-sobre-evaluaciones-finales-Bachillerato-publicados-por-distintas-universidades)  
      1.  [6.1 CRUE / G9](#TOC-CRUE-G9)  
      1.  [6.2 Andalucía](#TOC-Andaluc-a1)  
      1.  [6.3 Aragón](#TOC-Arag-n)  
      1.  [6.4 Asturias](#TOC-Asturias)  
      1.  [6.5 Baleares](#TOC-Baleares)  
      1.  [6.6 Canarias](#TOC-Canarias)  
      1.  [6.7 Cantabria](#TOC-Cantabria)  
      1.  [6.8 Castilla y León](#TOC-Castilla-y-Le-n1)  
      1.  [6.9 Castilla-La Mancha](#TOC-Castilla-La-Mancha1)  
      1.  [6.10 Cataluña](#TOC-Catalu-a)  
      1.  [6.11 Extremadura](#TOC-Extremadura1)  
      1.  [6.12 Galicia](#TOC-Galicia1)  
      1.  [6.13 Madrid](#TOC-Madrid1)  
      1.  [6.14 Murcia](#TOC-Murcia)  
      1.  [6.15 Navarra](#TOC-Navarra1)  
      1.  [6.16 País Vaso](#TOC-Pa-s-Vaso)  
      1.  [6.17 Rioja](#TOC-Rioja)  
      1.  [6.18 Valencia](#TOC-Valencia)  

Esta página la creo en 2016 para aclarar las evaluaciones LOMCE que se implantaban por primera vez en 2017.  
En 2018 hay normativa adicional, sin variaciones, ver [Proyecto de orden de evaluaciones de Bachillerato en el curso 2017-2018](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2017/evaluaciones-bachillerato-2017-2018.html) 

## Nombre de las Evaluaciones Finales de Bachillerato
 Anteriormente se hablaba de Selectividad y PAU (Pruebas de Acceso a Universidad), pero LOMCE habla de Evaluaciones Finales de Bachillerato (EFB)  
Como inicialmente condicionaban titular, se mencionan a veces como "reválidas"  
Con la confusión de 2016-2017, surgen varios nombres, lo comento en este hilo  
[twitter FiQuiPedia/status/819656600432603136](https://twitter.com/FiQuiPedia/status/819656600432603136)  
* EFB (Evaluación Final de Bachillerato) [Educación y la UR pactan la Evaluación Final de Bachillerato - unirioja.es](https://www.unirioja.es/apnoticias/servlet/Noticias?codnot=4654&accion=detnot)  
* PAU (Valencia decide mantener el nombre en 2017)  
* "La Prueba": nombre que usa el grupo G9 de universidades en un documentoEBAU (Evaluación de Bachillerato para el Acceso a la Universidad)Evaluación de Bachillerato  
* [https://www.educastur.es/estudiantes/acceso-a-estudios/universidad](https://www.educastur.es/estudiantes/acceso-a-estudios/universidad)  EVAU (Evaluación para la Admisión en la Universidad)  
* [EVAU, la nueva selectividad - cadenaser.com](http://cadenaser.com/emisora/2016/12/13/radio_zaragoza/1481626418_301054.html)  
[http://geografia.unizar.es/evau](http://geografia.unizar.es/evau)  
* PEBAU (Pruebas de Evaluación Final del Bachillerato para el Acceso a la Universidad)  
[Andalucía – Pruebas de Evaluación Final del Bachillerato para el Acceso a la Universidad (EBAU) - PAU (y EBAU) Latín y Griego](https://paulatinygriego.wordpress.com/2017/01/19/andalucia-pruebas-de-evaluacion-final-del-bachillerato-para-el-acceso-a-la-universidad-pebau/)  
Artículo en prensa sobre el nombre  [La Selectividad ahora se llama EBAU - elmundo.es](http://www.elmundo.es/sociedad/2017/02/10/589dc99e46163f97408b458f.html)  
* EvAU en Madrid  
* ABAU ( Avaliación de Bacharelato para o Acceso á Universidade) en Galicia  
* [http://ciug.gal/](http://ciug.gal/)  
* EAU (Evaluación para el Acceso a la Universidad) en País Vasco  
[http://www.ehu.eus/es/web/sarrera-acceso/batxilergoko-sarbide-probak](http://www.ehu.eus/es/web/sarrera-acceso/batxilergoko-sarbide-probak) 

## Calendario de implantación y efectos de las pruebas
 (En diciembre 2016 se aprueba  [Real Decreto-ley 5/2016](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-11733)  que en artículo 2 deroga la disposición final primera del Real Decreto 310/2016, y hace que las evaluaciones de Bachillerato no condicionen titular siendo solamente de materias de 2º  
>Hasta la entrada en vigor de la normativa resultante del Pacto de Estado social y político por la educación, la evaluación de bachillerato para el acceso a la Universidad regulada por el artículo 36 bis de la Ley Orgánica 2/2006, de 3 de mayo, no será necesaria para obtener el título de Bachiller y se realizará exclusivamente para el alumnado que quiera acceder a estudios universitarios. Versará exclusivamente sobre las materias generales cursadas del bloque de las asignaturas troncales de segundo curso y, en su caso, de la materia Lengua Cooficial y Literatura. Los alumnos que quieran mejorar su nota de admisión podrán examinarse de, al menos, dos materias de opción del bloque de las asignaturas troncales de segundo curso.  
La regulación para 2016/2017 se publica el 23 de diciembre de 2016  
[Orden ECD/1941/2016, de 22 de diciembre, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, para el curso 2016/2017.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-12219)  
La información siguiente está escrita antes de diciembre 2016:  
Ver información en  [Evaluaciones finales](/home/legislacion-educativa/lomce/evaluaciones-finales) , definidas en Real Decreto 310/2016, se realizarán por primera vez en 2017, y este primer año no condiciona titular Bachillerato, pero sí se tiene en cuenta para el acceso a la Universidad  
En 2017 habrá dos única convocatorias de Bachillerato (se cita una única para ESO en 2017)  
A veces se habla de "efectos académicos" que es una frase con trampa: suele hacer referencia a condicionar titular, pero el tema del acceso a la universidad, que en sí es un efecto académico de las pruebas, no se suele incluir.  
Ver disposición final de RD 310/2016  [https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#df](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#df) )  
>Disposición final primera. Calendario de implantación.  
>*2....La evaluación final de Bachillerato correspondiente a las dos convocatorias que se realicen en el año 2017 únicamente ***se tendrá en cuenta para el acceso a la Universidad**, pero su superación no será necesaria para obtener el título de Bachiller.

### Pruebas 2017-2018
 [Orden ECD/42/2018, de 25 de enero, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, para el curso 2017/2018.](http://www.boe.es/buscar/act.php?id=BOE-A-2018-984)  
 Vuelve a incluir matrices de especificaciones

### Pruebas 2018-2019
 [Orden PCI/12/2019, de 14 de enero, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, y las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas en el curso 2018-2019.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2019-395)  
Vuelve a incluir matrices de especificaciones  

Noticia 25 abril 2019  
[El TSJM rechaza el recurso de la FAPA que pedía la impugnación de la EvAU para el acceso a la universidad - lavanguardia.com](https://www.lavanguardia.com/local/madrid/20190425/461855997563/el-tsjm-rechaza-el-recurso-de-la-fapa-que-pedia-la-impugnacion-de-la-evau-para-el-acceso-a-la-universidad.html)  
El Tribunal Superior de Justicia de Madrid ha desestimado el recurso de la FAPA Francisco Giner de los Ríos contra la evaluación final de Bachillerato (EvAU) en la Comunidad de Madrid para poder acceder a los estudios universitarios, que sustituyó a la antigua Selectividad.   

### Pruebas 2019-2020
 [Orden PCM/139/2020, de 17 de febrero, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, y las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas en el curso 2019-2020.](https://www.boe.es/buscar/act.php?id=BOE-A-2020-2384)  
 Vuelve a incluir matrices de especificaciones  
 
Por covid19 se modifica en abril 2020
[Orden PCM/362/2020, de 22 de abril, por la que se modifica la Orden PCM/139/2020, de 17 de febrero, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, y las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas en el curso 2019-2020.](https://www.boe.es/buscar/act.php?id=BOE-A-2020-4576)  

La idea esencial del cambio está en artículo 6: se pueden mezclar opciones A y B 
> Para realizar el número máximo de preguntas fijado **todas** las preguntas deberán ser susceptibles de ser elegidas.  

### Pruebas 2020-2021
[Orden PCM/2/2021, de 11 de enero, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, y las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, en el curso 2020-2021.](https://www.boe.es/eli/es/o/2021/01/11/pcm2/con)  

### Pruebas 2021-2022
[Orden PCM/58/2022, de 2 de febrero, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la universidad, y las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, en el curso 2021-2022.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2022-1778)  

## Concepto de nota de corte con reválidas: Bachillerato + reválida es "como mínimo, del 60%" 
 El tema del mínimo 60% está en LOE modificada por LOMCE, artículo 38  
 [http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a38](http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a38)  
>La ponderación de la calificación final obtenida en el Bachillerato deberá tener un valor, ***como mínimo, del 60 %** del resultado final del procedimiento de admisión.  
Es confuso porque se confunde con el 60% asociado a nota de bachillerato (artículo 37)  
60% notas materias Bachillerato + 40% evaluación final = nota de etapa Bachillerato, "nota de acceso" (antes con PAU hasta 14, ahora con LOMCE hasta 10)  
mínimo 60% nota etapa Bachillerato + máximo 40% pruebas universidad = nota de admisión  

En mi opinión desaparece con claridad el concepto de nota de corte: antes una persona sabía que cierta nota en bachillerato+PAU era su "pasaporte" para ingresar en un universidad, donde conocía las notas de corte del año anterior. Sin embargo, con reválidas, una persona puede tener nota 10 en bachillerato+reválida, pero podría tener un 0 en el 40% restante, y su nota final de acceso sería un 6, y al revés, alguien podría tener un 5 en bachillerato+reválida, pero un 10 en el 40% restante, con lo que su nota final de acceso sería un 7, y adelantaría al caso anterior con nota final un 6.  
Hasta que las universidades no detallen qué harán con ese 40%, no está claro qué pasará y un estudiante no sabe si podrá entrar solamente con su nota de bachillerato+reválida. Ejemplo: en Andalucía bachillerato+reválida no será solamente al menos un 60%, sino que será el 100%, pero no hay garantías en otras comunidades  
[Andalucía mantendrá el sistema de Distrito Único con la LOMCE para garantizar la igualdad y transparencia en el acceso a la Universidad - us.es *waybackmachine*](http://web.archive.org/web/20210303052855/http://comunicacion.us.es/centro-de-prensa/institucional/andalucia-mantendra-el-sistema-de-distrito-unico-con-la-lomce-para)  
[El sistema universitario andaluz mantendrá el Distrito Único con la Lomce para garantizar acceso por méritos académicos - 20minutos.es](https://www.20minutos.es/noticia/2583134/0/sistema-universitario-andaluz-mantendra-distrito-unico-con-lomce-para-garantizar-acceso-por-meritos-academicos/) 

## Obligatoriedad realizar la evaluación final para estudiantes provenientes Bachillerato LOE
 En general hace que sea obligatoria para el acceso a la universidad ya que sustituye a la PAU, pero permite (según normativa julio 2016) que no sea obligatorio de forma excepcional para los que tienen materias no superadas LOE en 2º Bachillerato en 2016-2017 (y más casos según normativa diciembre 2016), aunque ello depende de normativa de cada comunidad, hago comentario general y luego concreto por comunidad  
 ¿Dónde aparece en BOE que ya no habrá PAU en 2017? El tema es que el hecho de que en  [Disposición Transitoria Única RD 310/2016](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#dt)  no se ofrezca la posibilidad de PAU para los que tengan no superadas LOE y titulen bachillerato en mayo 2017 lo está diciendo implícitamente. Opinión personal: es un tema calculado: ofrecer en 2017 al mismo tiempo PAU+reválida tiene asociado un coste económico de ofrecer dos pruebas, y un coste político, que supone una grieta a las reválidas, ya que si en las mismas fechas hay una PAU se puede cancelar reválidas y ofrecer PAU en el último momento, pero si no hay PAU en 2017 y solamente reválida no hay escapatoria.  
En  [Disposición transitoria única Real Decreto 412/2014](https://www.boe.es/buscar/act.php?id=BOE-A-2014-6008#dtunica)  se está diciendo que en "1. Sin perjuicio de lo dispuesto en la disposición adicional cuarta, para la admisión a los estudios universitarios oficiales de Grado en las Universidades españolas en los cursos académicos 2014-2015, 2015-2016 y ** 2016-2017 las Universidades podrán utilizar como criterio de valoración en los procedimientos de admisión la superación de las materias de la prueba de acceso a la universidad y la calificación obtenida en las mismas**" y alguien puede pensar que en 2017 la ley permitiría que las universidades hicieran la PAU pero:  
- en términos de acceso a la universidad, la admisión a los estudios uiniversitarios del curso 2016-2017 [universitario] es el asociado a la PAU realizada en 2016; para la universidad la prueba que se haga en junio de 2017 la asocian al curso 2017-2018  
- el texto de la normativa se cuida de decir explícitamente que se realizan esas PAU (que sí se han hecho en 2014, 2015 y 2016), así que incluso podría hacer referencia a notas de PAU de convocatorias anteriores a esos años (la redacción de las leyes no es casual, si la redacción del texto lo permite lo podrían haber hecho).  
En el proyecto de orden publicado el 5 de diciembre de 2016 se añade en disposición transitoria que aplica también a más alumnos que los repetidores (ver punto siguiente)  
>Se aplicará el mismo criterio al alumnado que obtuvo el título de Bachiller en el curso 2015-2016 y no tiene superada la prueba de acceso a la Universidad realizada al finalizar dicho curso. En ambos casos y cuando este alumnado no se presente a la prueba, la calificación para el acceso a estudios universitarios será la calificación final obtenida en Bachillerato.

### Transición LOE/LOMCE en 2º Bachillerato con materias pendientes 2º Bachillerato LOE: cursar LOE/LOMCE y/o reválida (relacionados)
Ver hilo de tuits asociados  
[twitter FiQuiPedia/status/776439525581070336](https://twitter.com/FiQuiPedia/status/776439525581070336)  
[Art 32.3 RD 1105/2014](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a32)  dice que pueden cursar solamente materias pendientes  
[Art 3.3 ECD/462/2016](https://www.boe.es/buscar/act.php?id=BOE-A-2016-3229#a3)  dice que pueden optar entre repetir curso entero o solo materias no superadas  
[Art 3.4 ECD/462/2016](https://www.boe.es/buscar/act.php?id=BOE-A-2016-3229#a3)  dice que las materias no superadas se cursan como LOMCE  
[Disposición transitoria ECD/462/2016](https://www.boe.es/buscar/act.php?id=BOE-A-2016-3229#dtunica)  dice que excepcionalmente en 2016-2017 pueden cursar materias no superadas como LOE si lo permiten “Administraciones educativas”  
[Disposición transitoria RD 310/2016](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#dt)  dice que “**no necesitará superar la evaluación**” quien curse dichas materias según el currículo del sistema educativo anterior, lo que aplica o no según lo que indique cada comunidad, por lo que sí pueden tener que hacerla. (Cuadra con  [Disposición adicional tercera](http://www.boe.es/buscar/act.php?id=BOE-A-2013-12886#datercera)  de LOMCE, punto 5 que indica *"Quienes no hubieran superado ninguna prueba de acceso a la universidad y hubieran obtenido el título de Bachiller con anterioridad a la implantación de la evaluación final de Bachillerato establecida en esta Ley Orgánica, ***podrán acceder directamente a las enseñanzas universitarias oficiales de grado,** si bien deberán superar los procedimientos de admisión fijados por las Universidades." por lo que se está permitiendo el acceso sin prueba (lo que se indica para procedimientos de admisión en la parte fuera de "al menos el 60%", y "con anterioridad a la implantación de la evaluación final de Bachillerato" indica antes de junio de 2017))  
[Respuesta del Ministerio a QUYSU_27049](https://t.co/lTSA43SJvk)  reconoce que si aplicase ese caso y no se realizase ninguna prueba, el 40% de nota asociada a esa prueba se obtendría de la nota de Bachillerato, pero al indicar ‘habrá cursado todo el Bachillerato “según el currículo del sistema educativo anterior”’ no está indicando que no es posible salvo “excepcionalmente” 

### Transición LOE/LOMCE en PAU: mantenimiento de la nota PAU anterior a implantación reválidas, y acceso a universidad en el caso de no haber realizado/superado PAU 
 [Disposición Adicional tercera de Real Decreto 412/2014](https://www.boe.es/buscar/act.php?id=BOE-A-2014-6008#datercera)  
 En punto 1 La nota de fase general se mantiene indefinidamente, la nota de la fase específica caduca a los 2 años (cuadra con  [Disposición adicional tercera](http://www.boe.es/buscar/act.php?id=BOE-A-2013-12886#datercera)  de LOMCE, punto 4 que indica "Aquellos alumnos y alumnas que hayan superado la Prueba de Acceso a la Universidad que establecía el artículo 38 de la Ley Orgánica 2/2006, de 3 de mayo, o las pruebas establecidas en normativas anteriores con objeto similar, **mantendrán la nota obtenida en su momento según los criterios y condiciones que establezca el Gobierno**, si bien podrán presentarse a los procedimientos de admisión fijados por las Universidades para elevar dicha nota.)  
No habla de las "PAU hecha con anterioridad a la LOMCE de 2013" sino de "PAU superada ...con anterioridad a su modificación por LOMCE"; la modificación por LOMCE de la PAU es suprimirla por las reválidas, cosa que ocurre en 2017. Por lo tanto esa frase aplica a todas las PAU LOE, incluyendo 2014,2015 y 2016, y sí conservarán la nota de la nota de la fase general, pero la nota de la específica dice que dura 2 años, así que "caduca" en 2016 y no valdrá en 2017.  
En punto 3 indica que si no se ha llegado a realizar nunca ninguna PAU "podrán acceder directamente", sin realizar reválidas. (Cuadra con  [Disposición adicional tercera](http://www.boe.es/buscar/act.php?id=BOE-A-2013-12886#datercera)  de LOMCE, punto 5 que indica *"Quienes no hubieran superado ninguna prueba de acceso a la universidad y hubieran obtenido el título de Bachiller con anterioridad a la implantación de la evaluación final de Bachillerato establecida en esta Ley Orgánica, ***podrán acceder directamente a las enseñanzas universitarias oficiales de grado,** si bien deberán superar los procedimientos de admisión fijados por las Universidades." por lo que se está permitiendo el acceso sin prueba (lo que se indica para procedimientos de admisión en la parte fuera de "al menos el 60%", y "con anterioridad a la implantación de la evaluación final de Bachillerato" indica antes de junio de 2017)) 

### Análisis evaluación final para estudiantes provenientes Bachillerato LOE por comunidad


#### Madrid
 [Disposición adicional 5ª ORDEN 2582/2016](http://www.bocm.es/boletin/CM_Orden_BOCM/2016/08/29/BOCM-20160829-5.PDF)  
Madrid dice que alumnos repetidores 2º se cierra expediente LOE y se abre LOMCE (*"3. Durante el año académico 2016-2017 se podrán dar los siguientes casos:....Solo a los alumnos repetidores de segundo se les cerrarán, mediante diligencia, el expediente académico y el historial académico LOE y se procederá a la apertura de los correspondientes expediente e historial LOMCE."*): Según eso Madrid no contempla lo que indica Disposición transitoria ECD/462/2016, los alumnos cursan materias no superadas de 2º con currículo LOMCE.  

Así lo había entendido hasta octubre, sin embargo en octubre 2016 se publican  INSTRUCCIONES SOBRE LA MATRÍCULA EN 2016-2017 DE ALUMNOS DE SEGUNDO CURSO DE BACHILLERATO PROCEDENTES DEL SISTEMA EDUCATIVO DERIVADO DE LA LOE EN LAS MATERIAS NO SUPERADAS DE SEGUNDO Y DE PRIMERO DE BACHILLERATO DEL CURRÍCULO ANTERIOR un pdf tiene fecha interna de 21 de octubre de 2016, CSV 1239697496787626764094  
Copia local  [https://drive.google.com/file/d/0B-t5SY0w2S8iNDUzX1BFOE9rbmc/view](https://drive.google.com/file/d/0B-t5SY0w2S8iNDUzX1BFOE9rbmc/view)  
y ahí se indica explícitamente que sí se permite cursar las no superadas como LOE y por lo tanto no tener que hacer reválida  
*y no opten por repetir el curso completo, sino sólo por matricularse en las materias pendientes, podrán, excepcionalmente en 2016-2017 terminar su Bachillerato en las siguientes condiciones:*  
*- No necesitarán superar la evaluación final para acceder a la universidad.*  
*- No necesitarán superar la evaluación final para obtener el título de Bachiller.*  
*- Sólo necesitarán matricularse de las materias pendientes, que recuperarán según el currículo LOE....*  
Al consultar se indica que en  [Disposición transitoria única ORDEN 2582/2016](http://www.bocm.es/boletin/CM_Orden_BOCM/2016/08/29/BOCM-20160829-5.PDF)  indica  
*Evaluación de materias pendientes durante los años académicos 2015-2016 y 2016-2017*  
*Excepcionalmente, durante los años académicos 2015-2016 y 2016-2017 el alumnado al que le sea de aplicación lo dispuesto en esta Orden será evaluado de las materias no superadas conforme al currículo que hubiera cursado.*  
y puede surgir la duda de a qué se refiere la frase "*alumnado al que le sea de aplicación lo dispuesto en esta Orden";* se trataría en principio de alumnado con materias pendientes e incluye a los alumnos de 2º de Bachillerato.  
Al mismo tiempo  [Disposición adicional octava 2582/2016](http://www.bocm.es/boletin/CM_Orden_BOCM/2016/08/29/BOCM-20160829-5.PDF)  indica  
*Procedimiento de incorporación del alumnado a un curso de Bachillerato del sistema educativo definido por la Ley Orgánica 8/2013, de 9 de diciembre, con materias no superadas del currículo anterior a su implantación*  
*4\. Para la recuperación de las materias no superadas será de aplicación el currículo establecido por el citado Decreto 52/2015, de 21 de mayo.*  
y ahí está diciendo currículo LOMCE, pero es una disposición similar a lo indicado en  [Art 3.4 ECD/462/2016](https://www.boe.es/buscar/act.php?id=BOE-A-2016-3229#a3) ; se indica por defecto LOMCE pero luego en disposición transitoria única se permite para 2016-2017 que sea como LOE.  

*Resumen*  
[twitter FiQuiPedia/status/794601769556934657](https://twitter.com/FiQuiPedia/status/794601769556934657)  
- Evaluar: según disposición transitoria única de ORDEN 2582/2016 da igual cómo se gestione administrativamente la matrícula, Madrid está diciendo desde agosto que las materias no superadas se evalúan según currículo que cursaron LOE, así que aplica  [Disposición transitoria RD 310/2016](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#dt) , y en 2017 no tienen que hacer ninguna prueba para acceder a la universidad. Si los currículos son similares la flexibilidad permite dar clases únicas y hacer exámenes únicos, no forzar dos exámenes distintos LOE y LOMCE.  
- Expendiente LOMCE con evaluación LOE: como se implanta 2016-2017 LOMCE ya solamente hay documentos de evaluación LOMCE, en cierto modo la "matrícula ha sido administrativamente con reglas LOMCE", pero debe ser evaluado de las no superadas con currículo LOE.  
- Cambio de pendientes en la matrícula: (por ejemplo historia de filosofía de 2º pendiente que pasa a no ser obligatoria por otra) en principio sí es posible, lo permite DISPOSICIÓN ADICIONAL OCTAVA punto 3 párrafo final de ORDEN 2582/2016, y ahí sí que el currículo a aplicar para ese cambio no se indica explícitamente, pero lo lógico es que sea LOE también.  

Puse resumen en noviembre 2016, pero el 9 de febrero de 2017 se publican nuevas instrucciones de matriculación  
[NUEVAS INSTRUCCIONES SOBRE LA MATRÍCULA EN EL AÑO ACADÉMICO 2016-2017 DE ALUMNOS DE SEGUNDO CURSO DE BACHILLERATO PROCEDENTES DEL SISTEMA EDUCATIVO DERIVADO DE LA LOE EN LAS MATERIAS NO SUPERADAS DE SEGUNDO Y DE PRIMERO DE BACHILLERATO DEL CURRÍCULO ANTERIOR](https://drive.google.com/file/d/0B-t5SY0w2S8ibjNZRDRMU3lJa2s/view)  
que van asociadas a normativa firmada por universidades y comunidad de Madrid, publicado en varias universidades  
ACUERDO DE LAS UNIVERSIDADES PÚBLICAS DE MADRID SOBRE PROCEDIMIENTOS DE ADMISIÓN PARA ESTUDIANTES CON EL TÍTULO DE BACHILLER, EQUIVALENTE U HOMOLOGADO, CURSO 2017-2018  
[Acuerdo - ucm.es*waybackmachine*](http://web.archive.org/web/20170212025711/https://www.ucm.es/data/cont/docs/3-2017-02-10-AcuerdoAdmision_2017_18_Madrid_6febrero%20VERSI%C3%93N%20FIRMA.pdf)  
[Acuerdo - UAH.es *waybackmachine*](http://web.archive.org/web/20171012232623/http://www.uah.es/export/sites/uah/es/admision-y-ayudas/.galleries/Descargas-Acceso/AcuerdoAdmision2017.pdf)  
[Acuerdo - urjc.es *waybackmachine*](http://web.archive.org/web/20171112230302/https://www.urjc.es/images/EstudiarURJC/Admision_matricula/grado/normativa/AcuerdoAdmision_2017_18_Madrid.pdf)

#### Andalucía
 [Artículo 19 Decreto 110/2016](http://www.juntadeandalucia.es/boja/2016/122/BOJA16-122-00016-11632-01_00094129.pdf)  no indica nada especial, se limita a citar BOE, luego implícitamente por defecto hay que cursar 2º LOMCE y realizar prueba en 2017 

#### Castilla y León
 [Artículo 2 de ORDEN EDU/774/2016](http://bocyl.jcyl.es/html/2016/09/07/html/BOCYL-D-07092016-2.do)  permite cursar todo LOE, y por lo tanto acceder a universidad sin prueba de acceso en 2017 (se indica explícitamente en artículo 4)

#### Castilla-La Mancha
 [Punto 2.13 de instrucciones de la viceconsejería de educación, universidades e investigación para la organización del final del curso 2015-2016 e inicio del curso 2016-2017 de los centros docentes que imparten educación secundaria y educación de personas adultas en la comunidad autónoma de Castilla-La Mancha](http://www.anpeclm.com/web/attachments/article/4150/INSTRUCCIONES%20SECUNDARIA%20Y%20EPA.pdf)  permiten cursar como LOE (por lo tanto acceder a universidad sin prueba de acceso en 2017, aunque no se diga explícitamente)

#### Galicia
Noticia en prensa que dice que se cursa LOMCE pero no hay obligación de reválida, lo que es inconsistente.  
[Los repetidores de 2.º de bachillerato están exentos de la reválida - lavozdegalicia.es](http://www.lavozdegalicia.es/noticia/galicia/2016/09/30/repetidores-2-bachillerato-exentos-revalida/0003_201609G30P16992.htm)  
Las instrucciones que se citan en principio serían estas, que confirman lo indicado  
[Aclaracións da Dirección Xeral de Educación - edu.xunta.gal](http://www.edu.xunta.gal/portal/sites/web/files/aclaracions_bacharelato_20160926.pdf) 

#### Extremadura
Noticia en prensa  
[Los repetidores extremeños de 2º de Bachillerato se libran de cursar asignaturas de la LOMCE - elconfidencialautonomico.com](http://www.elconfidencialautonomico.com/extremadura/repetidores-extremenos-Bachillerato-asignaturas-LOMCE_0_2829916997.html)  
[INSTRUCCIÓN Nº 29/2016, DE 30 DE NOVIEMBRE, DE LA SECRETARÍA GENERAL DE EDUCACIÓN, POR LA QUE SE REGULA, PARA EL CURSO 2016/2017, LA SITUACIÓN ACADÉMICA DEL ALUMNADO REPETIDOR DE SEGUNDO CURSO DE BACHILLERATO CON MATERIAS PENDIENTES](https://drive.google.com/file/d/0B-6YB49uZ8cUUnIwLTcwRWlBMGc/view)  
En artículo 10.bis de Instrucción nº 1812016 se permite cursar LOE y por lo tanto no hacer reválida.

#### Euskadi
 [Artículo 38 Evaluación final de Bachillerato y título de Bachiller de DECRETO 127/2016](https://www.euskadi.eus/y22-bopv/es/bopv2/datos/2016/09/1604054a.shtml)  no se comenta el caso de materias no superadas de curso anterior. Al no citarse nada de currículo LOE, se puede asumir que aplica lo que dice BOE y se cursará con LOMCE, y será obligatorio realizar reválida (similar a Madrid)

#### Navarra
 [Punto 7 REPETICIÓN DE 2º CURSO EN EL AÑO 2016/2017 de instrucciones Navarra 2016-2017](https://www.educacion.navarra.es/documents/27590/559378/Instrucciones+oferta+16-17+ESO+y+BACH+18+febrero+cast.pdf/61eb6368-a4fb-4112-8a8c-63159b580612)  indica que se cursa LOMCE, luego será obligatorio realizar reválida

## Cómo serán las evaluaciones finales de Bachillerato
Hasta que se concreten, la única referencia son las pruebas anteriores, que son las  [Pruebas de Acceso a la Universidad (PAU)](/home/pruebas/pruebasaccesouniversidad)  asociadas a LOE, por lo que usan otro currículo y no tienen en cuenta los estándares de aprendizaje. Ver documento anexo a borrador real decreto en  [Evaluaciones finales](/home/legislacion-educativa/lomce/evaluaciones-finales)  y apartado asociado a información común a ambas evaluaciones  
En septiembre 2016 el ministro de educación en funciones dice textualmente "se va a parecer mucho a la antigua PAU"  
[http://www.farodevigo.es/sociedad/2016/09/06/vigo-desvela-revalida-sera-similar/1528320.html](http://www.farodevigo.es/sociedad/2016/09/06/vigo-desvela-revalida-sera-similar/1528320.html)  
Tras lo anunciado en consejo ministros 18 noviembre 2016 sobre Real Decreto-Ley, parece que será igual a la PAU, y algunas universidades ya lo dicen así  
[http://www.levante-emv.com/comunitat-valenciana/2016/11/21/pruebas-acceso-universidad-seran-igual/1494933.html](http://www.levante-emv.com/comunitat-valenciana/2016/11/21/pruebas-acceso-universidad-seran-igual/1494933.html)  
En BOE de 10 de diciembre de 2016,  [Real Decreto-Ley 5/2016](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-11733)  se indica explícitamente  
*"en el caso del Bachillerato se realizará una prueba de características semejantes a la hasta ahora vigente Prueba de Acceso a la Universidad*  
En BOE 23 de diciembre de 2016 se concretan las matrices Orden ECD/1941/2016, de 22 de diciembre, por la que se determinan las características, el diseño y el contenido de la evaluación de Bachillerato para el acceso a la Universidad, las fechas máximas de realización y de resolución de los procedimientos de revisión de las calificaciones obtenidas, para el curso 2016/2017. [http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-12219](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2016-12219) 

### Fechas de convocatoria ordinaria y extraordinaria anual de las evaluaciones finales de Bachillerato
 En octubre 2016 no lo refleja explícitamente la normativa: se puede asumir que la convocatoria ordinaria será en junio como la anterior PAU.  
 Respecto a la convocatoria extraordinaria, hay que tener en cuenta que algunas comunidades hacían la convocatoria extraordinaria de PAU en septiembre (por ejemplo Madrid, Cataluña), y otras comunidades la hacían en julio (por ejemplo Valencia, Canarias). Según la normativa se puede entender que cada comunidad puede seguir eligiendo entre julio o septiembre  
 [ https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337&p=20160730&tn=1#a6](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337&p=20160730&tn=1#a6)  
>1. Anualmente se celebrarán dos convocatorias en cada Administración educativa: una ordinaria y otra extraordinaria. El Ministerio de Educación Cultura y Deporte fijará cada año los plazos máximos para la resolución de cada una de las convocatorias, que comprenderán, en todo caso, un periodo único para la convocatoria ordinaria **y dos, alternativos entre sí, para la convocatoria extraordinaria**, debiendo cada Administración educativa optar por el que mejor se adecúe a la planificación académica establecida para su ámbito territorial.En borradores orden de octubre y noviembre y proyecto diciembre 2016 sí se reflejan ambas fechas más claramente*Articulo 9. Fechas máximas para la realización de las evaluaciones  
>2. Las pruebas que constituyen la evaluación final de Bachillerato correspondientes a la convocatoria ordinaria del curso 2016-2017 deberán finalizar antes del día 10 de junio. Los resultados de las mismas deberán ser publicados antes del 24 de junio.  
>3. Las pruebas que constituyen la evaluación final de Bachillerato correspondientes a la convocatoria extraordinaria del curso 2016-2017 deberán finalizar:  
>a) Antes del día 8 de julio en el caso de que la Administración educativa competente determine celebrar la convocatoria extraordinaria en el mes de julio. En este caso, los resultados deberán ser publicados antes del 22 de julio.  
>b) Antes del día 9 de septiembre en el caso de que la Administración educativa competente determine celebrar la convocatoria extraordinaria en el mes de septiembre. En este caso, los resultados deberán ser publicados antes del 23 de septiembre.  

*Las fechas las indico asociadas a universidades, más adelante*

### Dónde se realizarán las evaluaciones finales de Bachillerato
 No se indica explícitamente en normativa, pero en principio seguirán siendo en las universidades como la PAU.  
 Sí que se menciona la posibilidad de que sean en los centros  
 [https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#a3](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#a3)  
\2. Los equipos directivos y el profesorado de los centros docentes participarán y colaborarán con las Administraciones educativas **en las evaluaciones finales que se realicen en sus centros**, en los términos establecidos en el artículo 144.1 de la Ley Orgánica 2/2006, de 3 de mayo, de Educación.  
Esta noticia indica lugar para La Rioja  [http://www.larioja.com/agencias/la-rioja/201611/02/evaluacion-final-bachillerato-sera-809773.html](http://www.larioja.com/agencias/la-rioja/201611/02/evaluacion-final-bachillerato-sera-809773.html) 

### ¿En la evaluación final de Bachillerato pueden entrar estándares de aprendizaje evaluables de 1º de Bachillerato?
 (Con Real Decreto-Ley 5-2016 se limita a materias de 2º Bachillerato)  
 Depende de qué materia  
 En el caso de física y química no debería haber de 1º, si se usa algo de es instrumental (por ejemplo en 2º Bachillerato usar leyes Kepler o termoquímica), pero no preguntar solamente por eso, no ser en lo que se centre la pregunta al ser materia con continuidad del último curso.  
 En cierto modo que el borrador de orden de octubre con la la matriz, que fija que es el 70% de lo que entra en reválida, cite cosas de 2º, confirma esa interpretación Bachillerato  
 [https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#a1-9](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#a1-9)  
Las materias que impliquen continuidad entre los cursos primero y segundo sólo computarán como una materia; **en este supuesto se tendrá en cuenta sólo la materia cursada en segundo curso.**

### Quién corregirá y calificará las evaluaciones finales de Bachillerato
 Según LOMCE profesorado externo al centro  
 El artículo 144.1 de LOE modificada por LOMCE dice [http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a29](http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a29)  
>La realización material de las pruebas corresponde a las Administraciones educativas competentes. Las pruebas serán aplicadas y **calificadas por profesorado del Sistema Educativo Español externo al centro.**

### ¿Serán las evaluaciones finales iguales para todas las comunidades autónomas?
Según LOMCE sí deberían ser iguales en todas las comunidades, aunque la redacción de leyes siempre se puede interpretar retorciendo  

Artículos 29.4 (ESO) y 36.bis.3 (Bachillerato) de LOE modificada por LOMCE dice [http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a29](http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a29)  
*4\. El Ministerio de Educación, Cultura y Deporte establecerá para todo el Sistema Educativo Español los criterios de evaluación y las características de las pruebas, y ***las diseñará y establecerá su contenido para cada convocatoria.***  
[http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a36bis](http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a36bis)  
*3\. El Ministerio de Educación, Cultura y Deporte, previa consulta a las Comunidades Autónomas, establecerá para todo el Sistema Educativo Español los criterios de evaluación y las características de las pruebas, y **las diseñará y establecerá su contenido para cada convocatoria.***  
En artículo 144.1 de LOE modificada por LOMCE dice [https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a144](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a144)  
*En concreto, las pruebas y los procedimientos de las evaluaciones indicadas en los artículos 29 y 36 bis **se diseñarán por el Ministerio de Educación, Cultura y Deporte, a través del Instituto Nacional de Evaluación Educativa.**  Dichas pruebas serán estandarizadas y se diseñarán de modo que permitan establecer valoraciones precisas y comparaciones equitativas, así como el seguimiento de la evolución a lo largo del tiempo de los resultados obtenidos.*  
En mi opinión eso se interpreta como que las evaluaciones finales son iguales para toda España, porque las hace el Ministerio. Eso implica básicamente que supone, salvo temas de calendario, la misma prueba en las 17 comunidades.  
Es la misma opinión que muestra el  [Dictamen 13 (12-05-2016) del Consejo Escolar del Estado al Proyecto de Real Decreto por el que se regulan las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato](http://www.educacionyfp.gob.es/dctm/cee/el-consejo/dictamenes/2016/dictamen132016.pdf?documentId=0901e72b8212fce3)  (opinión que no es vinculante) en el que reconociendo que "la fijación de las preguntas y el calendario" es algo que debe hacer el Ministerio y no las comunidades, indicó que no es posible hacer una modificación vía Real Decreto y Orden que contradiga una Ley Orgánica  
**Habida cuenta de la improcedencia de que se otorgue a las Comunidades Autónomas competencias del Estado en la fijación de las preguntas y el calendario de las pruebas dado que puede ser contrario al artículo 29.4 de la LOMCE y puede ahondar, aún más, en las diferencias existentes entre las 17 comunidades autónomas de nuestro territorio nacional, se propone la supresión del punto a) del apartado 1, del artículo 3:**  
y el Consejo Escolar del Estado tacha el siguiente texto asociado a que no "Corresponde a las Administraciones educativas"  
**a) Redacción de las pruebas y elaboración de las guías de codificación y corrección, en el marco de lo dispuesto en la orden ministerial anual que establezca su diseño y contenidos**  
En el BOE, RD 310/2016 tras ese comentario del Consejo Escolar del Estado, no se suprimió el punto a, sino que se le dio esta redacción:  
*a) Construcción y elaboración material de las pruebas en el marco de lo dispuesto en la orden ministerial anual.*  
En mi opinión que según LOMCE el Ministerio, para todo el Sistema Educativo Español, las diseñe y establezca su contenido para cada convocatoria y que según RD 310/2016 las comunidades autónomas "las construyan y elaboren" me parece una filigrana de duplicidad de tareas ignorando el dictamen del Consejo Escolar de Estado  
Desconozco detalles de las evaluaciones finales de 6º de primaria que se realizaron por primera vez en 2016, pero creo que sí han sido distintas por comunidad, y eso creo que refleja que sí van a ser distintas en ESO y Bachillerato por comunidad.  
En dictamen 15/2016 también indica Consejo Escolar de Estado que la orden no puede reducir al 70%, pero la orden publicada en BOE ignora ese comentario.

### Posibilidad de cursar materias específicas / materias no cursadas 
 En artículo 17 de RD310/2016 se indica  
 [https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#a1-9](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#a1-9)  
*a) Todas las materias generales ***cursadas** en ambos cursos de Bachillerato en el bloque de asignaturas troncales. En el supuesto de materias que impliquen continuidad, se tendrá en cuenta sólo la materia cursada en segundo curso.*  
*b) Dos materias de opción ***cursadas** en el bloque de asignaturas troncales, en cualquiera de los cursos, a elección del alumnado. Las materias que impliquen continuidad entre los cursos primero y segundo sólo computarán como una materia; en este supuesto se tendrá en cuenta sólo la materia cursada en segundo curso.*  
*c) Una materia del bloque de asignaturas específicas *** cursada** en cualquiera de los cursos que no sea Educación Física ni Religión, a elección del alumnado.  
Al indicar cursada, no se puede hacer lo que a veces se hacía antes de escoger una materia no cursada  
En Real Decreto-Ley 5/2016 se indica al modificar calendario LOMCE  
[https://www.boe.es/buscar/act.php?id=BOE-A-2016-11733#a1](https://www.boe.es/buscar/act.php?id=BOE-A-2016-11733#a1)  
*Versará exclusivamente sobre las materias generales ***cursadas** del bloque de las asignaturas **troncales** de segundo curso y, en su caso, de la materia Lengua Cooficial y Literatura. Los alumnos que quieran mejorar su nota de **admisión** podrán examinarse de, al menos, dos materias de opción del bloque de las asignaturas troncales de segundo curso.  
No aparece explícitamente "cursadas" al hablar de las troncales de opción, pero sí al hablar de troncales en general el acceso, así que solamente pueden ser asignaturas CURSADAS en acceso, y se deja abierto a que sean no cursadas en admisión.  
Pero hay más: los repetidores LOE no cursan necesariamente materias LOMCE, por lo que no tienen por qué cursar materias troncales de segundo curso LOMCE que son de las que se examinan si se presentan voluntariamente a la evaluación para subir nota (aunque tienen derecho de acceso sin presentarse) ¿cómo se compagina el solamente examinarse de materias cursadas LOMCE y ser repetidor LOE y no haberla cursado?  
En Real Decreto-Ley 5/2016 se indica al modificar calendario LOMCE  
[https://www.boe.es/buscar/act.php?id=BOE-A-2016-11733#a2](https://www.boe.es/buscar/act.php?id=BOE-A-2016-11733#a2)  
*Apartado 4.b) Podrá presentarse a la evaluación el alumnado que esté en posesión del título de Bachiller, así como los alumnos que se encuentren en alguna de las situaciones contempladas en la disposición adicional tercera del Real Decreto 310/2016, de 29 de julio.*  
[https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#da-3](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#da-3)  
*Las universidades podrán adoptar como procedimiento de admisión a las enseñanzas universitarias oficiales de Grado cualquiera de los previstos en el artículo 10 del Real Decreto 412/2014, de 6 de junio, y entre ellos, la evaluación de conocimientos de determinas materias relacionadas con las enseñanzas universitarias que pretendan cursarse.*  
*Con objeto de garantizar la objetividad de las pruebas y la utilización eficiente de recursos, las universidades podrán utilizar para esta evaluación la calificación obtenida en las materias correspondientes en la evaluación final de Bachillerato. A estos efectos, los estudiantes en posesión de los títulos establecidos en los artículos 9.1 y 9.2 del Real Decreto 412/2014, de 6 de junio, por el que se establece la normativa básica de los procedimientos de admisión a las enseñanzas universitarias oficiales de Grado, podrán participar en las pruebas de dichas materias en la evaluación final de Bachillerato y obtendrán una certificación oficial de la calificación obtenida.*  
[https://www.boe.es/buscar/act.php?id=BOE-A-2014-6008#a9](https://www.boe.es/buscar/act.php?id=BOE-A-2014-6008#a9)  
No se menciona nada concreto para los alumnos repitiendo LOE; en Madrid se resuelve con instrucciones de matriculación de febrero de 2017  
[https://drive.google.com/file/d/0B-t5SY0w2S8ibjNZRDRMU3lJa2s/view](https://drive.google.com/file/d/0B-t5SY0w2S8ibjNZRDRMU3lJa2s/view)  
Se indica  
*"2. No optar por repetir el segundo curso completo, sino solo recuperar las materias pendientes según el currículo LOE,...*  
*En el caso de que lo soliciten, se estará a lo que se recoge en la Instrucción segunda.2 del presente documento.*  
*... 2....; podrán, con carácter voluntario, ser matriculados de modo inmediato en las correspondientes materias, según la modalidad, con vistas a su presentación a la evaluación de Bachillerato para acceso a la Universidad, toda vez que según lo previsto en el citado Real Decreto-ley 5/2016, de 9 de diciembre, las materias generales del bloque de las asignaturas troncales de segundo curso sobre las que versará la evaluación de Bachillerato para el acceso a la Universidad deberán haber sido **cursadas**."*  
Desconozco cómo lo han resuelto otras comunidades  

### Propuestas de unidades de evaluación e ítems de evaluaciones finales bachillerato física  
A falta de que se definan (a 30 de noviembre de 2016 según indica RD 310/2016), en octubre 2016 intento concretar con unas propuestas de ejemplo usando preguntas PAU y sus apartados (más que una propuesta de reválida es un intento de "mapeo PAU-reválida"), los asocio a estándares; puede que no se cubran totalmente los estándares, pero son "asociables/relacionados"  
Todo esto surge como primera idea de borrador, pensando en cómo se plantearía el examen de bloque ondas de tipo reválida / asociado a estándares reutilizando preguntas PAU  
La pregunta de PAU Madrid 2006-Junio Cuestión 2  
Una onda sonora que se propaga en el aire tiene una frecuencia de 260 Hz.  
a) Describa la naturaleza de la onda sonora e indique cuál es la dirección en la que tiene lugar la perturbación, respecto a la dirección de propagación.  
b) Calcule el periodo de esta onda y su longitud de onda.   
Datos: velocidad del sonido en el aire v = 340 m s-1  
Se puede ver como una unidad de evaluación, en la que  
- Apartado a sería un ítem asociable a estándar de aprendizaje evaluable "2.1. Explica las diferencias entre ondas longitudinales y transversales a partir de la orientación relativa de la oscilación y de la propagación."  
- Apartado b sería un ítem asociable a estándar de aprendizaje evaluable "1.1. Determina la velocidad de propagación de una onda y la de vibración de las partículas que la forman, interpretando ambos resultados."  
La pregunta de PAU Madrid 2015-Modelo B. Pregunta 2.  
Una onda transversal que se propaga en una cuerda , coincidente con el eje X, tiene por expresión matemática: y (x , t)= 2 sen (7t - 4x) , donde x e y están expresadas en metros y t en segundos. Determine:  
a) La velocidad de propagación de la onda y la velocidad máxima de vibración de cualquier punto de la cuerda.  
b) El tiempo que tarda la onda en recorrer una distancia igual a la longitud de onda.  
Se puede ver como una unidad de evaluación, en la que  
- Apartado a sería un ítem asociable a estándar de aprendizaje evaluable "1.1. Determina la velocidad de propagación de una onda y la de vibración de las partículas que la forman, interpretando ambos resultados."  
- Apartado b sería un ítem asociable a estándar de aprendizaje evaluable "3.1. Obtiene las magnitudes características de una onda a partir de su expresión matemática. "  
En octubre 2016 hago unos primeros intentos de conseguir algo similar a lo que puede ser la desconocida reválida para el bloque de ondasRealizo modelos de ondas, gravitación y campo eléctrico: una vez que en febrero 2017 surge el modelo de EvAU 2017 dejan de tener sentido y dejo de compartirlos aquí.

## Detalles sobre evaluaciones finales Bachillerato publicados por distintas universidades
Tras publicarse en diciembre 2016 Real Decreto Ley 5/2016 y Orden ECD/1941/2016, algunas universidades comienzan a publicar detalles, comento por comunidades según voy conociendo. Me centro en universidades públicas.  
Separo información de CRUE y de G9

### CRUE / G9
Grupo de varias universidades  
Documento septiembre 2016 ADMISIÓN A ESTUDIOS OFICIALES DE GRADO CURSO 2017-18  
*Los procedimientos regulados en este acuerdo serán de aplicación para la admisión en los estudios universitarios oficiales de grado de las universidades del G-9 durante el período transitorio correspondiente al curso 2017-18.*  
[ADMISIÓN A ESTUDIOS OFICIALES DE GRADO CURSO 2017-18, CRUE - uniovi.es](http://www.uniovi.es/documents/31582/23538036/2016+09+07+Admisio%CC%81n+G9_definitivo.pdf/fec7815e-914c-4e5e-84d2-c7f45f7e328a)  
Reunión 17 enero CRUE  
[Acuerdos sobre la nueva Prueba de Evaluación de Bachillerato y Acceso a la Universidad - uniovi.es](http://www.uniovi.es/-/acuerdos-sobre-la-nueva-prueba-de-evaluacion-de-bachillerato-y-acceso-a-la-universidad) 

### Andalucía
[Los exámenes para el acceso a la universidad serán los próximos días 12, 13 y 14 de junio - diariosur.es](http://www.diariosur.es/universidad/201701/14/selectividad-20170113212852.html)  
[El examen de selectividad tendrá dos opciones y Filosofía será para subir nota - diariosur.es](http://www.diariosur.es/universidad/201701/24/examen-selectividad-tendra-opciones-20170123233136.html)  
[Los exámenes de selectividad tendrán preguntas tipo test, cortas y largas - diarusur.es](http://www.diariosur.es/universidad/201701/25/examenes-selectividad-tendran-preguntas-20170124234016.html)  
El 25 de enero se publica una nota informativa  
[Nota informativa sobre la evaluación de Bachillerato para acceso a la Universidad en el curso escolar 2016/17 - juntadeandalucia.es](http://www.juntadeandalucia.es/educacion/portals/web/ced/novedades/-/contenidos/detalle/nota-informativa-sobre-la-evaluacion-de-bachillerato-para-acceso-a-la-universidad-en-el-curso-escolar-2016)  
30 enero  
[La UJA celebra una jornada para resolver dudas sobre la próxima prueba de acceso a la universidad - 20minutos.es](http://www.20minutos.es/noticia/2946994/0/uja-celebra-jornada-para-resolver-dudas-sobre-proxima-prueba-acceso-universidad/)  
8 febrero [La US reúne a los directores de institutos para explicarles las nuevas Pruebas de Acceso y Admisión a la Universidad -us.es *waybackmachine*](http://web.archive.org/web/20210303053728/http://comunicacion.us.es/node/5067)  
14 febrero en la página donde se muestran exámenes de años anteriores indica "AVISOS: Los examenes se corresponden al curriculum L.O.E., no obstante se muestran porque muchos de los ejercicios pueden ser validos para el bachillerato L.O.M.C.E.", y se publican orientaciones para 2016/17

### Aragón
 En enero 2017 todavía no cita acceso LOMCE, pero tiene un texto asociado a diferenciar acceso y admisión que creo que con LOMCE es interesante tener en cuenta, lo copio aquí  
 [http://wzar.unizar.es/servicios/acceso/](http://wzar.unizar.es/servicios/acceso/)  
 *El ***acceso** y la **admisión** a las enseñanzas universitarias de Grado son dos procesos que están muy unidos y cuyos conceptos se suelen confundir a menudo. Por este motivo, y antes de que consultes estas páginas, queremos explicar la diferencia entre los mismos.*   
   * *El ***acceso** es el derecho que tiene una persona a solicitar admisión en una Universidad para realizar unos estudios de Grado específicos. Sólo pueden ejercitar este derecho quienes cumplen alguno de los requisitos de acceso a la Universidad que marca la legislación vigente.* 
   * *La ***admisión** supone la aceptación efectiva de un estudiante en unos estudios y en una Universidad determinados.* 
*Como habrás podido comprobar, para solicitar admisión en estudios universitarios de Grado, previamente se ha de cumplir con alguno de los requisitos de acceso. La información que incorporamos en esta web está clasificada en dos grandes apartados que se corresponden, precisamente, con los dos procesos que acabamos de resumir: el acceso y admisión. Esperamos que te resulte útil.*  
A 20 enero 2017 hay modelos de Geografía e Historia  
[http://geografia.unizar.es/evau](http://geografia.unizar.es/evau)  
El 27 enero 2017 se publica  [FAQs sobre la evaluación de Bachillerato para el acceso a la Universidad (EvAU) - 27/1/2017. FAQ UNIZAR *waybackmachine*](http://web.archive.org/web/20171003001600/http://www.educaragon.org/FILES/FAQ%20UNIZAR%20DEF.pdf)    
[Preguntas frecuentes sobre la evaluación de bachillerato para el acceso a la Universidad (EvAU). FAQ UNIZAR - csif.es](https://www.csif.es/sites/default/files/field/file/FAQ%20UNIZAR%20DEF.pdf)  
16 febrero 2017  [La UZ aprueba los nuevos parámetros de ponderación de acceso a la universidad - elperiodicodearagon.com](http://www.elperiodicodearagon.com/noticias/aragon/uz-aprueba-nuevos-parametros-ponderacion-acceso-universidad_1181383.html)  
[La UZ presenta cómo será la nueva selectividad - heraldo.es](http://www.heraldo.es/noticias/aragon/2017/02/16/la-presenta-valor-que-tendra-cada-prueba-nueva-selectividad-1159476-300.html)  
23 febrero 2017  
[ORDEN ECD/133/2017, de 16 de febrero, por la que se determina la organización y coordinación de la evaluación final de bachillerato para el acceso a la Universidad en la Comunidad Autónoma de Aragón, a partir del curso 2016/2017.](http://www.boa.aragon.es/cgi-bin/EBOA/BRSCGI?CMD=VEROBJ&MLKOB=948881223232) 

### Asturias 
En enero 2017 en educastur se cita como Evaluación de Bachillerato para el Acceso a la Universidad (EBAU)  
[Evaluación de Bachillerato para el Acceso a la Universidad (EBAU) *waybackmachine*](http://web.archive.org/web/20170115212012/http://www.educastur.es/-/evaluacion-de-bachillerato-para-el-acceso-a-la-universidad-ebau-)  
Artículo 21 enero 2017 donde duda sobre nombre EBAU ó EVAU  
[La lentitud en organizar la nueva prueba de acceso exaspera a la Universidad - lne.es](http://www.lne.es/asturias/2017/01/21/lentitud-organizar-nueva-prueba-acceso/2044836.html)  
El 26 enero 2017 se convocan reuniones coordinación EBAU  
El 1 febrero 2017 se publica [RESOLUCIÓN de 1 de febrero de 2017, de la Consejería de Educación y Cultura, por la que se organiza la prueba de evaluación de Bachillerato para el acceso a la Universidad correspondiente al año académico 2016-2017.](https://www.educastur.es/documents/10531/40496/2017_02_DOC020217-02022017145432.pdf/2c58e7ce-f9f3-4999-a322-1e99e5b179e7)  
[Organización Prueba Evaluación Bachillerato para acceso Universidad (EBAU) - educastur.es *waybackmachine*](http://web.archive.org/web/20170607074944/http://www.educastur.es/-/organizacion-prueba-evaluacion-de-bachillerato-para-acceso-universidad)  

### Baleares
[Acceso con el título de bachillerato o asimilado. Convocatoria 2017 - uib.es *waybackmachine*](http://web.archive.org/web/20170202140543/http://estudis.uib.es/es/grau/acces/batxiller)  
A 12 de enero 2017 indica  
*En breve actualizaremos la información para la admisión 2017-18.*  
A 7 febrero 2017 ya se indica información: parámetros ponderación y enlaces a materias en 2017, aunque por ejemplo en Física se indica "en preparación"
[Acceso con el título de bachillerato o equivalente » Materias PBAU » Física -ubi.es *waybackmachine*](http://web.archive.org/web/20170504221441/http://estudis.uib.es/es/grau/acces/batxiller/materies/fisica/)  

### Canarias
 [La ULL despeja dudas sobre el acceso a la Universidad tras la supresión de la PAU](http://eldia.es/canarias/2017-01-24/15-ULL-despeja-dudas-acceso-Universidad-supresion-PAU.htm)  
 30 enero se publica nota informativa con fechas y ponderaciones  
 [La Evaluación de Bachillerato para el Acceso a la Universidad será del 7 al 9 de junio - ulpgc.es](http://www.ulpgc.es/noticia/evaluacion-bachillerato-acceso-universidad-sera-del-7-al-9-junio)  

### Cantabria
 [http://web.unican.es/admision/acceso-a-la-universidad-estudios-de-grado](http://web.unican.es/admision/acceso-a-la-universidad-estudios-de-grado)  
 En la web en enero 2017 se indica  
 *Pruebas de acceso (Selectividad)​​​​​​​​​​​​​​​​ La Universidad de Cantabria hará público en breve el procedimiento de admisión que utilizará para el curso 2017-18.*  
El 7 febrero 2017 ya aparece información: ponderaciones, fechas 8, 9 y 10 de junio  
[http://web.unican.es/admision/acceso-a-la-universidad-estudios-de-grado/pruebas-de-acceso](http://web.unican.es/admision/acceso-a-la-universidad-estudios-de-grado/pruebas-de-acceso) 

### Castilla y León
27 enero 2017 se publica  
[Borrador de la orden para creación de la comisión organizadora de la evaluación de bachillerato para el acceso a la universidad de Castilla y León - aspescl.com](https://www.aspescl.com/index.php/comunicacion/aspes-informa/mesa-sectorial/1035-borrador-de-la-orden-para-creacion-de-la-comision-organizadora-de-la-evaluacion-de-bachillerato-para-el-acceso-a-la-universidad-de-castilla-y-leon?tmpl=component&print=1&layout=default&page=)  
31 enero 2017 se publica  [ORDEN EDU/33/2017, de 26 de enero, por la que se crea la Comisión organizadora de la evaluación de bachillerato para el acceso a la universidad de Castilla y León, y se establecen determinados aspectos de la evaluación, para el curso académico 2016‑2017.](http://bocyl.jcyl.es/html/2017/01/31/html/BOCYL-D-31012017-1.do)  
2 febrero 2017 se fijan fechas  
[La nueva Evaluación de Bachillerato para el Acceso a la Universidad se celebrará los días 13, 14 y 15 de junio y 11, 12 y 13 de septiembre - jcyl.es](http://www.comunicacion.jcyl.es/web/jcyl/Comunicacion/es/Plantilla100Detalle/1281372051501/_/1284705864718/Comunicacion?d=1)  
8 febrero 2017 se publica información, guía informativa EBAU CyL 2016-17  

### Castilla-La Mancha
27 enero 2017 se publican [convocatorias reuniones coordinación 2017 *waybackmachine*](http://web.archive.org/web/20161225211325/http://www.uclm.es/preuniversitario/orientadores/reuniones.asp)   
Por ejemplo en física fechas varían entre 14 y 17 febrero 2017  

### Cataluña
Cataluña en principio mantiene la PAU   
[Documents per a l'organització i la gestió dels centres Concreció i desenvolupament del currículum del batxillerat 27/09/2016 *waybackmachine*](http://web.archive.org/web/20161223172727/http://educacio.gencat.cat/documents/IPCNormativa/DOIGC/CUR_Batxillerat.pdf)  
*Per als alumnes que finalitzin segon de batxillerat el juny del 2017, el títol de batxillerat s’obtindrà amb la superació de totes les matèries cursades al centre. ***Es mantenen les PAU, com a referència d’accés i admissió a la universitat, amb la mateixa estructura que en els cursos anteriors i amb les matèries de modalitat que vincula l’OrdreEDU/340/2009, de 30 de juny, (DOGC núm. 5417, de 9.7.2009)***  
15 febrero 2017 La Generalitat da marcha atrás en los cambios de la selectividad  
[La Generalitat capea el temporal de la selectividad con dos cambios a última hora - elperiodico.com](http://www.elperiodico.com/es/noticias/sociedad/generalitat-cambios-prueba-selectividad-acceso-universidad-5838756)  
Nota enviada  [COMUNICAT A DIRECTORES/DIRECTORS DE CENTRE I COORDINADORES/COORDINADORS DE BATXILLERAT](http://estaticos.elperiodico.com/resources/pdf/9/6/1487146964569.pdf?_ga=1.212558098.744918809.1478701630)  
En la página  [Proves d'accés a la universitat ... Estructura de les PAU i qualificació de la prova *waybackmachine*](http://web.archive.org/web/20161113215422/http://universitatsirecerca.gencat.cat/ca/03_ambits_dactuacio/acces_i_admissio_a_la_universitat/proves_dacces_a_la_universitat_pau/estructura_de_les_pau_i_qualificacio_de_la_prova/)  se indica  
*Estructura de les PAU i qualificació de la prova*  
*[Comunicat *waybackmachine*](http://web.archive.org/web/20170705101211/http://universitatsirecerca.gencat.cat/web/ca/03_ambits_dactuacio/acces_i_admissio_a_la_universitat/.content/acces_i_admissio_a_la_universitat/proves_d_acces_a_la_universitat__pau/docs/informacio-centres-10feb_rev_-PAU.pdf)  als centres de batxillerat ampliant informació referent a la prova d'accés a la universitat (10 de febrer)*  
*[Nota informativa *waybackmachine*](http://web.archive.org/web/20170215003936/http://universitatsirecerca.gencat.cat/web/ca/03_ambits_dactuacio/acces_i_admissio_a_la_universitat/.content/acces_i_admissio_a_la_universitat/proves_d_acces_a_la_universitat__pau/docs/Nota_premsa_PAU-2017_SUR.pdf)  referent a les proves d'accés a la universitat 2017.*  
*Estem treballant en l'actualització de la informació d'aquesta pàgina. En breu estarà disponible. Disculpeu les molèsties.*  
*Data d'actualització: 15.02.2017*

### Extremadura
 Noticia 25 enero 2017  [La Junta pretende que la reválida de Bachillerato sea como la Selectividad - hoy.es](http://www.hoy.es/extremadura/201701/25/junta-pretende-revalida-bachillerato-20170125140152.html)  
 Publicado 31 enero 2017 [Resumen de las características de la Prueba de Evaluación de Bachillerato para el acceso a la Universidad (EBAU)](http://www.educarex.es/pub/cont/com/0048/documentos/RESUMEN_EBAU.pdf) 

### Galicia
 Noticia 19 enero 2017  [La nueva selectividad durará tres días y se celebrará la segunda semana de junio - farodevigo.es](http://www.farodevigo.es/galicia/2017/01/19/nueva-selectividad-durara-tres-dias/1607214.html)  
 Está prevista para los días 7, 8 y 9 -El alumnado elegirá tres materias para subir nota -Xunta, universidades y CIUG fijan la segunda prueba para 13, 14 y 15 de septiembre  
 El 26 enero 2017 se publican documentos sobre física  
[Circular Informativa nova](http://ciug.gal/PDF/circularfisica2017.pdf)  (actualizado 26/01/2017)  
[Orientacións Xerais para o profesorado nova](http://ciug.gal/PDF/orientacionsxeraisfisica2017.pdf)  (actualizado 26/01/2017)  
En marzo 2017 se publican documentos en  [http://ciug.gal/](http://ciug.gal/)  
Ponderacións das materias da parte voluntaria da ABAU 2017 [VER](http://ciug.gal/PDF/pondera17.pdf)  
(provisional pendente da publicación do DOGA)  
[Calendario ABAU 2017 e acceso 2017-2018](http://ciug.gal/PDF/dxefpiegal.pdf)  [ VER](http://ciug.gal/PDF/calendarioabau17.pdf)  
Instrucións para a realización, dentro do curso 2016/2017, da avaliación de bacharelato para o acceso á Universidade (ABAU) para curso 2017/2018  
(En trámite para a súa publicación no DOG) [VER](http://ciug.gal/PDF/dxefpiegal.pdf) 

### Madrid
Artículo 13 enero 2017 donde se entrevista a José Manuel Torralba, director general de Universidades de la Comunidad.  
[Así sera la nueva prueba de Bachillerato en Madrid - cadenaser.com](http://cadenaser.com/emisora/2017/01/13/radio_madrid/1484293235_637207.html)  
El 19 de enero se publica  [ORDEN 47/2017, de 13 de enero, de la Consejería de Educación, Juventud y Deporte, por la que se desarrollan determinados aspectos de la evaluación final de Bachillerato para el acceso a la universidad.](http://www.bocm.es/boletin/CM_Orden_BOCM/2017/01/19/BOCM-20170119-1.PDF)  
El 10 febrero 2017 se realiza una presentación de todas las universidades públicas  
[twitter torralba59/status/830096733455650817](https://twitter.com/torralba59/status/830096733455650817)  
Aunque esté en web de universadades concreta, está asociado a todas las universidades públicas   
Nota de prensa [Las 6 universidades públicas madrileñas acuerdan con la Comunidad de Madrid garantizar el distrito único en la nueva normativa que regula el Acceso a la Universidad - ucm.es](https://www.ucm.es/las-6-universidades-publicas-madrilenas-acuerdan-con-la-comunidad-de-madrid-garantizar-el-distrito-unico-en-la-nueva-normativa-que-regula-el-acceso-a-la-universidad)  
Acuerdo de las Universidades públicas de Madrid sobre procedimientos de admisión para Estudiantes con el título de Bachiller, equivalente u homologado, curso 2017-2018  
EMES, UAM enlaces no operativos en 2021 ni copia en waybackmachine   
[Acuerdo - uah.es *waybackmachine*](http://web.archive.org/web/20171012232623/http://www.uah.es/export/sites/uah/es/admision-y-ayudas/.galleries/Descargas-Acceso/AcuerdoAdmision2017.pdf)  
[Acuerdo - ucm.es](http://web.archive.org/web/20170212214541/http://ucm.es/data/cont/docs/3-2017-02-10-AcuerdoAdmision_2017_18_Madrid_6febrero%20VERSI%C3%93N%20FIRMA.pdf)  
[Acuerdo - uc3m.es *waybackmachine*](http://web.archive.org/web/20170723184757/http://www.uc3m.es/ss/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-Disposition&blobheadername2=Cache-Control&blobheadervalue1=attachment%3B+filename%3D%22Acuerdo_de_las_Universidades_P%C3%BAblicas_de_Madrid_sobre_procedimientos_de_admisi%C3%B3n_para_estudiantes_con_el_t%C3%ADtulo_de_Bachiller%2C_equivalente_u_homologado%2C_Curso_2017%2F2018.pdf%22&blobheadervalue2=private&blobkey=id&blobtable=MungoBlobs&blobwhere=1371553294785&ssbinary=true)   
[Acuerdo - urjc.es](http://web.archive.org/web/20171112230302/https://www.urjc.es/images/EstudiarURJC/Admision_matricula/grado/normativa/AcuerdoAdmision_2017_18_Madrid.pdf)   
[Acuerdo - upm.es](http://www.upm.es/sfs/Rectorado/Vicerrectorado%20de%20Alumnos/Acceso/1%20Acuerdo%20Admisi%C3%B3n%20-%206%20febrero%202017.pdf)  

El 14 febrero CCOO recurre la normativa de Madrid    
[CCOO recurre la prueba de acceso a Universidad de la Comunidad de Madrid por "injusta y antijurídica" - elmundo.es](http://www.elmundo.es/madrid/2017/02/14/58a2cf4a468aebdb168b45ad.html)  
[CCOO rechaza la evaluación final para el acceso a la universidad de 2017 por injusta y antijurídica - madrid.ccoo.es](http://www.madrid.ccoo.es/noticia:232742--CCOO_rechaza_la_evaluacion_final_para_el_acceso_a_la_universidad_de_2017_por_injusta_y_antijuridica)  
El 17 febrero en BOCM fechas oficiales [Resolución de 31 de enero de 2017, del Director General de Universidades e Investigación, por la que se da publicidad al Acuerdo de la Comisión Coordinadora de la Evaluación de Bachillerato para Acceso a la Universidad por el que se determinan las fechas de realización de las evaluaciones correspondientes al año 2017 - bocm.es](http://www.bocm.es/boletin/CM_Orden_BOCM/2017/02/17/BOCM-20170217-14.PDF) 

#### EMES / todas las universidades
En enero 2017 cita RD 310/2016 pero no RDL 5/2016 ni ECD/1941/2016 y cita esta frase  
*"Las Universidades de la Comunidad de Madrid actualmente no han definido los criterios de admisión para el próximo curso 2017/2018 en el distrito único de Madrid."*  
A 17 enero se actualiza y ya indica 
*Evaluación de Bachillerato para el acceso a la universidad y ECD/1941/2016*

#### UAH
1 febrero publican fechas de reuniones coordinación: Física 2 febrero, Química 16 febrero (enlace deja de estar operativo 7 febrero 2017)  
A 7 febrero publican normas uso calculadoras y criterios generales, y pasan a mencionar EvAU en lugar de selectividad, cambia enlace 
A 10 febrero publica nota resumen cálculo calificaciones  
[CalculoCalificaciones.pdf - uah.es](http://www.uah.es/export/sites/uah/es/admision-y-ayudas/.galleries/Descargas-Acceso/CalculoCalificaciones.pdf)  
A 17 febrero publica [ponderaciones](http://www.uah.es/export/sites/uah/es/admision-y-ayudas/.galleries/Descargas-Acceso/Ponderaciones.pdf) 

#### UAM
 A 25 enero 2017 en  [Información para futuros estudiantes](http://www.uam.es/estudiantes/acceso/acceso/bachfp/index.html)  ya aparece el texto EvAU en lugar de PAU  
 El 26 enero 2017 ya indica las fechas EvAU y comentario pendiente publicación BOCM  
[Acceso para estudiantes de Bachillerato y Formación Profesional - uam.es](https://www.uam.es/ss/Satellite/es/1242656108731/1242695563300/generico/generico/Acceso_para_estudiantes_de_Bachillerato_y_Formacion_Profesional.htm)  
 El 31 enero 2017 publica instancia para que profesores IES sean correctores  
 [Convocatoria de participación en la prueba de acceso](https://www.uam.es/ss/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-disposition&blobheadervalue1=attachment%3B+filename%3DCarta+participaci%C3%B3n+en+Tribunales+Prof+IES.pdf&blobkey=id&blobtable=MungoBlobs&blobwhere=1242782096477&ssbinary=true)  (Profesores IES)  [Instancia](https://www.uam.es/ss/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-disposition&blobheadervalue1=attachment%3B+filename%3DInstancia+Profesores+I+E+S+%28actualizado+2017%29.pdf&blobkey=id&blobtable=MungoBlobs&blobwhere=1242782096512&ssbinary=true)  (Profesores IES)  
 A 13 febrero 2017, además de acuerdo publica  
[Procedimientos de la prueba EvAU en las universidades públicas madrileñas - uam.es](http://web.archive.org/web/20210304231348/http://www.uam.es/estudiantes/acceso/docs/acceso/bachfp/acuerdos_comision/acuerdos_procedimiento_EvAU2017.pdf)   

#### UC3M 
A 12 enero 2017 se indica en    
[Grados > Solicitud de plaza > Selectividad > Reuniones de coordinación de materias - uc3m.es *waybackmachine*](http://web.archive.org/web/20170705123332/http://www.uc3m.es/ss/Satellite/Grado/es/TextoDosColumnas/1371215799249)  
*Estamos trabajando en el proceso de las pruebas de acceso a la Universidad para el curso 2017/2018. La información se publicará próximamente.*  
A 2 febrero 2017 se publican reuniones coordinación: Física 14 ó 16 febrero según sea campus Getafe o Colmenarejo  
A 13 febrero 2017 se publica en página aparte  [Grados > Solicitud de plaza > Admisión Grados 2017/2018 - uc3m.es *waybackmachine*](http://web.archive.org/web/20170610224915/http://www.uc3m.es/ss/Satellite/Grado/es/TextoDosColumnas/1371228713047/)  acuerdo universidades e información, con este texto  
*Una vez publicadas por parte del Ministerio de Educación las normativas sobre el acceso a la universidad para el próximo curso, y firmado por las Universidades Públicas de Madrid el acuerdo por el que se establecen las condiciones comunes de admisión en el Distrito de Madrid, la UC3M hace públicas estas páginas, para ayudar al alumnado de 2º de bachillerato, a su profesorado y a los equipos de orientación de los centros a conocer los aspectos más importantes de la admisión en la universidad.*  
Se incluye este documento [Preguntas frecuentes (FAQs)](http://www.uc3m.es/ss/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-Disposition&blobheadername2=Cache-Control&blobheadervalue1=attachment%3B+filename%3D%22Preguntas_frecuentes_%28FAQs%29.pdf%22&blobheadervalue2=private&blobkey=id&blobtable=MungoBlobs&blobwhere=1371553285103&ssbinary=true)  [](http://www.uc3m.es/ss/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-Disposition&blobheadername2=Cache-Control&blobheadervalue1=attachment%3B+filename%3D%22Preguntas_frecuentes_%28FAQs%29.pdf%22&blobheadervalue2=private&blobkey=id&blobtable=MungoBlobs&blobwhere=1371553285103&ssbinary=true)   
A 17 febrero publica ponderaciones [PONDERACIONES DE MATERIAS PARA LA ADMISIÓN EN GRADOS DE LA UNIVERSIDAD CARLOS III DE MADRID - CURSO 2017/2018 *waybackmachine*](http://web.archive.org/web/20170612071348/http://www.uc3m.es/ss/Satellite?blobcol=urldata&blobheader=application/pdf&blobheadername1=Content-Disposition&blobheadername2=Cache-Control&blobheadervalue1=attachment;+filename=%22Ponderaciones_de_materia.pdf%22&blobheadervalue2=private&blobkey=id&blobtable=MungoBlobs&blobwhere=1371553359673&ssbinary=true) 

#### UCM
10 febrero 2017 publica en  [https://www.ucm.es/evau-admision](https://www.ucm.es/evau-admision)  varios documentos  
[https://www.ucm.es/data/cont/docs/3-2017-02-10-EvAU y admisión curso 2017-2018.pdf](https://www.ucm.es/data/cont/docs/3-2017-02-10-EvAU%20y%20admisi%C3%B3n%20curso%202017-2018.pdf)  
[Presentación de la sesión para orientadores y equipos directivos celebrada 21/2/2017](https://drive.google.com/file/d/0BwGLrTSBBKflRTg1RGZSMDBhRms/view?usp=sharing)  
Ponderaciones publicadas en febrero 
#### UPM 
23 febrero actualiza página quitar PAU LOE e indicar EvAU, se incluye acuerdo universidades y otros documentos  
[Evaluación para el acceso a la Universidad (EvAU) - upm.es](http://www.upm.es/FuturosEstudiantes/Ingresar/Acceso/EvAU)  
[EVALUACIÓN PARA EL ACCESO A LA UNIVERSIDAD. EvAU – CURSO 2017/2018 – BACHILLERATO LOMCE *waybackmachine*](http://web.archive.org/web/20180328231531/http://www.upm.es/sfs/Rectorado/Vicerrectorado%20de%20Alumnos/Acceso/EsquemaEvAU%202017-2018.pdf)  
[Tabla de ponderaciones - upm.es *waybackmachine*](http://web.archive.org/web/20210225222558/http://www.upm.es/sfs/Rectorado/Vicerrectorado%20de%20Alumnos/Informacion/Preinscripcion/Documentos%20Nuevos/TablaPonderaciones2017-2018.pdf)  
#### URJC  
En enero 2017 ya tiene url propia y normativa actualizada LOMCE (Real Decreto Ley 5/2016 y Orden ECD/1941/2016)  
[Estudiantes de bachillerato y formación profesional (LOMCE) - urjc.es](http://www.urjc.es/estudiar-en-la-urjc/admision/407-estudiantes-de-bachillerato-y-formacion-profesional-lomce)  
La información de reuniones de coordinación está en otro enlace, y en febrero publican acta reunión física, con apartado para modelos de exámen  
[https://www.urjc.es/estudiar-en-la-urjc/admision/416-informacion-para-los-centros#actas-de-la-reunión](https://www.urjc.es/estudiar-en-la-urjc/admision/416-informacion-para-los-centros#actas-de-la-reuni%C3%B3n)  
Se publica presentación informativa [EvAU: EVALUACIÓN DE ACCESO A LA UNIVERSIDAD 2017 - urjc.es](http://web.archive.org/web/20170711010855/https://www.urjc.es/images/EstudiarURJC/Admision_matricula/PRESENTACION%20EvAU%202017_def.pdf)

### Murcia
Información 22 enero 2017, asociada a reunión CRUE en Murcia  
[La nueva 'Selectividad' - laopiniondemurcia.es](http://www.laopiniondemurcia.es/comunidad/2017/01/22/nueva-selectividad/799790.html)  
El 23 enero 2017 ya hay reuniones Física y Química  
[Vicerrectorado de Estudios > Contenido > Acceso > Bachillerato y Ciclos Formativos > EBAU Materias y coordinadores > Física - um.es](http://www.um.es/web/vic-estudios/contenido/acceso/pau/ebau-materias-coordinadores/fisica/-/asset_publisher/w8VAsnNsENag/content/convocatoria-reunion-pau-23-enero-2017?redirect=http%3A%2F%2Fwww.um.es%2Fweb%2Fvic-estudios%2Fcontenido%2Facceso%2Fpau%2Febau-materias-coordinadores%2Ffisica%3Fp_p_id%3D101_INSTANCE_w8VAsnNsENag%26p_p_lifecycle%3D0%26p_p_state%3Dnormal%26p_p_mode%3Dview%26p_p_col_id%3Dcolumn-1%26p_p_col_count%3D1)    
Convocatoria reunión PAU 23 ENERO 2017  
Estimado profesorado del Departamento de Física y Química:  
La Orden Ministerial ECD/1941/2016, de 22 de diciembre, de acuerdo con el Real Decreto-ley 5/2016, de 9 de diciembre, establece en su Artículo 12 que "las administraciones educativas, en colaboración con las universidades, organizarán la realización material de la evaluación de Bachillerato para el acceso a la Universidad"; y que "las universidades asumirán las mismas funciones y responsabilidades que tenían en relación con las Pruebas de Acceso a la Universidad, aunque cada administración educativa podrá delimitar el alcance de la colaboración de sus universidades en la realización de las pruebas."  
Mientras se formaliza el desarrollo de este apartado por la administración educativa regional, por las circunstancias de urgencia que concurren y en virtud de las conversaciones mantenidas con dicha administración, las universidades públicas de la región van a comenzar a organizar la coordinación de las materias de la evaluación de Bachillerato para el acceso a la Universidad.  
El lunes 23 de enero de 2017 celebraremos una reunión de coordinación de las P.A.U. en el Salón de Actos de la Facultad de Química (Salón Hermenegildo Lumeras):  
- a las 17:00 horas, para la materia de Química.  
- a las 18:00 horas, para la materia de Física.  
Cordialmente,  Natalia Campillo Seva  
Coordinadora de Química en las P.A.U.  
Antonio Guirao Piñera  
Coordinador de Física en las P.A.U.

### Navarra
A 24 enero 2017 en  [Grados/ Pruebas de acceso (PAU): Bachiller y FP, mayores 25, 40 y 45/ Bachiller y FP - unavarra.es *waybackmachine*](http://web.archive.org/web/20170120175257/http://www.unavarra.es/estudios/acceso-y-matricula/grados/pruebas-de-acceso-a-la-universidad/selectividad)  se indica  
*La información sobre el acceso a la universidad de estudiantes de Bachillerato y Formación Profesional para el curso 2017-18 se publicará en este apartado en cuanto esté disponible.*  
El 3 febrero 2017 en mismo enlace se publican fechas 7, 8 y 9 de junio de 2017, y estructura ya LOMCE en  [Evaluación de Bachillerato para el Acceso a la Universidad (EvAU) - unavarra.es *waybackmachine*](http://web.archive.org/web/20170617005207/http://www.unavarra.es/estudios/acceso-y-matricula/grados/pruebas-de-acceso-a-la-universidad/selectividad?opcion=2) 

### País Vaso
A 13 enero 2017  
[UPV/EHU > Acceso a la Universidad > Admisión para el curso 2017/18 - ehu.eus *waybackmachine*](http://web.archive.org/web/20170205204414/http://www.ehu.eus/es/web/sarrera-acceso/content/-/asset_publisher/0wHI/content/admision-en-la-upv-ehu-para-el-curso-2017-18-y-siguientes)  
*Admisión a partir de 2017*  
*Posible procedimiento y criterios de admisión a la UPV/EHU una vez desaparecida la Prueba de Acceso a la Universidad (Selectividad) y con la Prueba de Conjunto de Bachillerato. Advertimos que esta información está pendiente de su aprobación definitiva por lo que podría sufrir modificaciones.*  
Se citan varios documentos  
Documentos  
[Procedimiento de admisión en la UPV/EHU a partir de 2017 (pdf , 911,70 KB)](https://www.ehu.eus/documents/1940628/2034007/Procedimiento+de+admisi%C3%B3n+para+la+UPV+en+2014)  
[Criterios de admisión a los grados de la UPV/EHU a partir de 2017 (pdf , 135,86 KB)](https://www.ehu.eus/documents/1940628/2034007/LOMCE%28logo%29.pdf)  
[Adscripción Familias Profesionales de FP a Ramas de Conocimiento (pdf , 85,76 KB)](https://www.ehu.eus/documents/1940628/2034007/Relaciones+entre+t%C3%ADtulos+de+formaci%C3%B3n+profesional+superior+y+ramas+de+conocimiento+de+ense%C3%B1anzas+universitarias+de+grado.)   
Y el acuerdo MECD-CRUE de 27 abril 2016  
28 febrero 2017  [La UPV/EHU hace público el diseño de la Evaluación de Acceso a la Universidad - deia.eus *waybackmachine*](http://web.archive.org/web/20190415183410/https://www.deia.eus/2017/02/28/sociedad/euskadi/la-upvehu-hace-publico-el-diseno-de-la-evaluacion-de-acceso-a-la-universidad) 

### Rioja
Esta noticia da fechas para La Rioja  [http://www.larioja.com/agencias/la-rioja/201611/02/evaluacion-final-bachillerato-sera-809773.html](http://www.larioja.com/agencias/la-rioja/201611/02/evaluacion-final-bachillerato-sera-809773.html)  
Publicado 23 enero   
[Resolución de 16 de enero de 2017, de la Dirección General de Educación, por la que se dictan instrucciones en el ámbito de la Comunidad Autónoma de La Rioja sobre la prueba de evaluación de Bachillerato para el acceso a la Universidad (EBAU) para el curso 2016-2017](http://www.larioja.org/bor/es/ultimo-boletin?tipo=2&fecha=2017/01/23&referencia=4296704-6-HTML-506861-X)  
[La nueva prueba EBAU sustituye a las PAU/Selectividad - unirioja.es](http://www.unirioja.es/apnoticias/servlet/Noticias?codnot=4760&accion=detnot)  
[EBAU, la nueva prueba de acceso a la universidad - gradosunirioja.es](http://www.gradosunirioja.es/ebau-la-nueva-prueba-de-acceso-la-universidad)  
[Todo sobre la EBAU, la nueva prueba de acceso a la universidad en La Rioja - nuevecuatrouno.com](http://nuevecuatrouno.com/2017/01/23/asi-sera-la-ebau-la-nueva-prueba-acceso-la-universidad-la-rioja/) 

### Valencia
[Novedades Admisión a la Universidad 2017 - upv.es *waybackmachine*](http://web.archive.org/web/20170506123735/http://www.upv.es/entidades/SA/acceso/959365normalc.html)  
 Documento 13 diciembre 2016  
[Acuerdos de Comisión Gestora de los Procesos de Acceso y Preinscripción, que se han aprobado en relación al acceso y la admisión a la universidad para el curso 2017/18 *waybackmachine*](http://web.archive.org/web/20170619074450/https://www.upv.es/entidades/SA/acceso/U0740156.pdf)  
El documento indica que se mentiene el nombre de PAU  
"a) Les proves per a l'accés a la universitat seguiran cridant-se amb les sigles PAU. --> a) Laspruebaspara el acceso a la universidad seguirán llamándose con las siglasPAU."  
Se comenta que habrá reuniones en febrero 2017  
*S'ha començat ja la coordinació de les característiques dels exàmens de les 25 assignatures que formaran part de les PAU i la previsió és que es facen les reunions amb tot el professorat que està impartint les assignatures durant la primera setmana de febrer de 2017.*  
El 12 de enero de 2017 se publican ponderaciones para 2017-2018, y se habla de "fase voluntaria PAU"  
[twitter informacioUA/status/819511498041159680](https://twitter.com/informacioUA/status/819511498041159680)  
16 enero se publica información (se envía a centros)  
[La Universidad envía por fin a los institutos las normas de la nueva Selectividad - diarioinformacion.com](http://www.diarioinformacion.com/alicante/2017/01/16/universidad-envia-institutos-normas-nueva/1849531.html)  
Información oficial en web universidad  (enlace no operativo en 2021) indicaba  
*Acceso y admisión estudios universitarios Comunidad Valenciana. Curso 2017/18. (Publicada 13/01/2017)*  
La publicación del  [Real Decreto-ley 5/2016](https://www.boe.es/boe/dias/2016/12/10/pdfs/BOE-A-2016-11733.pdf) de modificación de la LOMCE en diciembre 2016 y los acuerdos de la Comisión Gestora de procedimientos de acceso y preinscripción universitaria de la Comunidad Valenciana (enlace no operativo en 2021) (Conselleria y universidades públicas) en enero 2017, concretan las carácteristicas del procedimiento de acceso y admisión para el próximo curso 2017/18.  
La evaluación final de bachillerato para el acceso a la universidad mantendrá, en la Comunidad Valenciana, la denominación de Prueba de Acceso a la Universidad (PAU).  
La prueba constará de dos fases: Obligatoria y voluntaria. La nota máxima que se podrá alcanzar será de 14 puntos.  
La fase obligatoria consta de 5 exámenes: Castellano, valenciano, Historia de España, lengua extranjera y troncal de modalidad. No es obligatorio haber cursado la troncal de modalidad y la lengua extranjera.  
La fase obligatoria se supera obteniendo 4 puntos. La nota de acceso será la media ponderada, 60% expediente bachillerato, 40% nota fase obligatoria. Habrá que obtener mínimo 5 puntos para superar la PAU.  
Fase voluntaria para mejorar nota en 4 puntos. Los estudiantes podrán examinarse de hasta un máximo de 4 asignaturas troncales de opción cursadas o no cursadas. Para que la asignatura pondere (sume) habrá que obtener un mínimo de 5 puntos [Ponderaciones *waybackmachine*](http://web.archive.org/web/20210306024957/https://web.ua.es/es/oia/documentos/documentos-ingreso/ponderaciones-2017-y-notas-de-corte-2016.pdf)  
Se mantienen dos convocatorias: Ordinaria junio (6, 7 y 8), extraordinaria julio (4, 5 y 6)  
Se mantiene el Distrito Abierto (la nota de acceso es el único criterio de admisión y válida para todas las universidades públicas españolas)  
Otros colectivos (FP, titulados, mayores, extranjeros) acceden en las mismas condiciones que anteriormente. Los estudiantes extranjeros NO UE deberán superar una prueba de acceso en la Universidad Nacional de Educación a Distancia (UNED).  

17 enero se publica en [https://sa.ua.es/es/selectividad/](https://sa.ua.es/es/selectividad/)  
[Ponderaciones para la preinscripción 2017-18](https://sa.ua.es/es/selectividad/documentos/ponderaciones-cv-2017-18.pdf?noCache=1484647781919)  (Publicada 17/01/2017)  
Acuerdo de la Comisión Gestora de los Procesos de Acceso y Preinscripción Universitaria del Sistema Universitario Valenciano sobre el acceso y la admisión a los estudios universitarios de Grado de las Universidad Públicas Valencianas en el curso 2017/2018 (enlace no operativo en 2021) (Publicada 17/01/2017)  
El fichero pdf tiene nombre interno "Microsoft Word - Document Ponderacions_2017 2018 versio ultima 9Gener2017.doc": 9G no tiene relación con grupo de universidades G9, Gener es enero en valenciano.  
El mismo 17 añaden documento de preguntas frecuentes, y más tarde retiran los documentos.  
Pantallazo  [twitter FiQuiPedia/status/821409705071443971](https://twitter.com/FiQuiPedia/status/821409705071443971)  
El motivo de retirarlo parece que es que CRUE lo indica  
[Educación hace públicos los criterios de la nueva selectividad - levante-emv.com](http://www.levante-emv.com/comunitat-valenciana/2017/01/18/educacion-publicos-criterios-nuevo-selectivo/1516717.html)  
*la comisión mixta entre la Conselleria de Educación y las cinco universidades públicas que coordina el acceso al Sistema Universitario Público Valenciano (SUPV) deberá rectificar las instrucciones enviadas este 9 de enero a los más de 600 institutos públicos y concertados de la Comunitat tras la decisión tomada ayer por la Conferencia de Rectores de Universidad Españoles (CRUE).*  

A 25 de enero se publica información en  
[Pruebas de acceso a la universidad PAU - uv.es](http://www.uv.es/uvweb/universidad/es/estudios-grado/admision/bachillerato/informacion-pruebas-1285852996917.html)  
[NOTA INFORMATIVA para estudiantes de PAU-2017 *waybackmachine*](http://web.archive.org/web/20170520010308/http://www.uv.es:80/graus/PAU/Nota_informativa_especificas.pdf)   

