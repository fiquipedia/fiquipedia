# Recursos Realidad Virtual

La realidad aumentada es algo que enlaza con recursos de  [apps para móvil](/home/recursos/recursos-apps)  y  [apps química](/home/recursos/quimica/apps-quimica)  y recursos de  [geometría molecular](/home/recursos/quimica/geometria-molecular)  
En inglés se referencia como AR/VR (Augmented Reality/Virtual Reality)  
Puede haber cosas como baker en Química que simula realidad sin ser en sí AR/VR.

A veces algunas [simulaciones](/home/recursos/simulaciones) se incluyen elementos de realidad virtual, por ejemplo en  [Tabla periódica / elementos químicos](/home/recursos/quimica/recursos-tabla-periodica)  

[3D Periodic Table. Google ArtsExperiments](https://artsexperiments.withgoogle.com/periodic-table/)  Permite ver los modelos de Bohr 3D, enlaza con AR  
[3D Periodic table. Experiments with Google](https://experiments.withgoogle.com/3d-periodic-table)  
![](https://lh3.googleusercontent.com/AVjtgfgjcrk6CNL4xt5h2NUluR1H1TuXmtWrvOfVSqi9VonfvA1yrwP6rljXlXzPmGXHpUHeT4RQG8hzJdDMp0NBRPAPGzho-Zb7)  

También asociados a [geometría molecular](/home/recursos/quimica/geometria-molecular) 

Pongo algunos enlaces de recopilaciones. Citan aplicaciones que se pueden repetir entre ellos  

[The Best 7 Free and Open Source Augmented Reality Software - goodfirms.co](https://www.goodfirms.co/blog/best-free-open-source-augmented-reality-software)  
1 [ARToolKit+](http://www.arreverie.com/artoolkit.html) (free and open source)  
2 [Mixare](http://www.mixare.org/static/index.html) (free and open source) **archivado en 2022**   
3 [Holokit](https://holokit.io/) (open source)   
4 [BRIO](https://www.goodfirms.co/software/brio) (free)  
5 [Adobe Aero](https://www.adobe.com/products/aero.html) (free)  
6 [Vuforia Engine](https://www.ptc.com/en/products/vuforia/vuforia-engine) (free)   
7 [ZapWorks Studio](https://zap.works/) (30 days free trial)   
[Augment](https://www.augment.com/)  (popular)   

[16 aplicaciones de Realidad Aumentada para educación](https://www.ayudaparamaestros.com/2021/01/16-aplicaciones-de-realidad-aumentada.html)  
[Diagrama](https://coggle.it/diagram/X-z_YSrTPp7dEb-u/t/16-aplicaciones-de-a5ba75c14f530e3db2bd6ffdfa76afde/33276308877fb38f8db46556b445fc8b27499be1cf85ff22afeebfad3d8140d5)  
https://chromville.com/  
https://zap.works/  
https://quivervision.com/education-portal  
http://www.zookazam.com/#home  
https://en.actionbound.com/  
https://www.zappar.com/  
https://vitotechnology.com/star-walk.html#sthash.5afGvA48.dpuf  
https://bodyplanet.es/  
https://studio.gometa.io/landing  
http://www.arloon.com/  
https://mergeedu.com/cube?cr=1710  
https://www.layar.com/  
https://www.augment.com/  
http://www.aumentaty.com/community/es/  
https://www.curiscope.com/  

[4 apps de Realidad Aumentada para educación que debes conocer](https://www.ayudaparamaestros.com/2020/07/4-apps-de-realidad-aumentada-para.html)  
Body planet  
QuiverVision  
Merge  
ZooKazam  


[Foxar.fr](https://foxar.fr/en/education)  
[![Foxar — Nouvelle version bêta!](https://img.youtube.com/vi/OmVdh5-LO1w/0.jpg "Foxar — Nouvelle version bêta!")](https://www.youtube.com/watch?v=OmVdh5-LO1w)  

[Zappar](https://www.zappar.com/)  

[CoSpaces](https://www.cospaces.io/)  


##  Merge Cube
[Merge cube - mergeedu.com](https://mergeedu.com/cube)  
![](https://cdn.shopify.com/s/files/1/1248/5527/products/Merge_Cube_Product_with_Logo_white_1200x.jpg?v=1608076506)  

Lo conozco en 2019  
El cubo se puede [comprar oficialmente](https://shop.mergeedu.com/products/merge-cube) pero también imprimir en papel y montar (algunos enlaces a pdf pueden fallar con el tiempo)   
[Merge Cube Printable](https://www.arvrinedu.com/post/merge-cube-printable)  
![](https://static.wixstatic.com/media/879cdc_e7de0aafc6ef412a86eed58afe123fcb~mv2_d_10171_12989_s_6_4_3.png/v1/fill/w_360,h_460,al_c,q_95/879cdc_e7de0aafc6ef412a86eed58afe123fcb~mv2_d_10171_12989_s_6_4_3.webp)  
[PDF - wixstatic.com](https://docs.wixstatic.com/ugd/879cdc_2146ac3eac0045dcb440d715042de3bd.pdf)  

Otra fuente  [PDF - eduescaperoom.com](https://eduescaperoom.com/mergecube-plantilla-imprimiblew)  

El cubo es el mismo para distintas aplicaciones, que son las que tienen la información de los objetos.  
Suelen ser apps voluminosas.  
Algunas son gratuitas 
* [Galactic Explorer for MERGE Cube - play.google.com](https://play.google.com/store/apps/details?id=com.MergeCube.TheSolarSystem&hl=es) Sistema Solar  
![](https://play-lh.googleusercontent.com/JLCKkZmJpfD5ymsSme_ckQC_0YeaXu7uYZsueh3AwIygiRhVi6IhWt1OEDnlfwl8tjkL=w720-h310)  
* [3D Museum Viewer for MERGE Cube - play.google.com](https://play.google.com/store/apps/details?id=com.MergeCube.MuseumViewer&hl=es) Esculturas  
![](https://play-lh.googleusercontent.com/UQz1ehdqYlPiQseKmE1eZ8rB6l2JljpFV6lhQb5WqW7pvLoCit44rNXottz82osGqw=w720-h310)  
* [AR Medical - play.google.com](https://play.google.com/store/apps/details?id=de.nextreality.medicalcube&hl=es) Órganos (pulmones, corazón...)  
![](https://play-lh.googleusercontent.com/FOflgk_a86LsPavcWXBq2T9oMtdS8yhjOqt_khYsF0LylIF1kAABxk9v55JA3H8Img=w720-h310)  
* [TH!NGS for MERGE Cube - play.google.com](https://play.google.com/store/apps/details?id=com.MergeCube.Things&hl=es) Una de las opciones es un cráneo con huesos.  
![](https://play-lh.googleusercontent.com/SKbzbEOXU8mOTp_BCdVMFkElBvsxI5QQvfRifTVxEo-ucQt6PiIBUcK7dTGelPlVqPqJ=w720-h310)  
* [Merge Object Viewer - play.google.com](https://play.google.com/store/apps/details?id=com.MergeCube.ObjectViewer&hl=es)  
Pendiente documentar cómo crear objetos  
![](https://play-lh.googleusercontent.com/-fxiRHQsuLLNbFfj2Q-TZkt0hpftAPP1E_7QIvkGkIpITpZsEt_DZVmUZgyzZ4BG4g=w720-h310)  
[twitter quirogafyq/status/1127285129678860289](https://twitter.com/quirogafyq/status/1127285129678860289)  
Por ahora llevo esto. Lo he hecho con Thinkercad. En cuanto tenga más experiencia y salga algo mejor te aviso, cuenta con ello  

Mergecube y cómo crear enigmas de Realidad Aumentada en tu Escape Room Educativo  
4 mayo, 2019 por Juanda y Nieves de EduEscapeRoom  
[Mergecube y cómo crear enigmas de Realidad Aumentada en tu Escape Room Educativo - eduescaperoom.com](https://eduescaperoom.com/mergecube-y-como-crear-enigmas-de-realidad-aumentada-en-tu-escape-room-educativo/)  

[The 10 Best VR Apps for Classrooms Using Merge VR’s New Merge Cube - edsurge.com](https://www.edsurge.com/news/2018-03-14-the-10-best-vr-apps-for-classrooms-using-merge-vr-s-new-merge-cube) 

##  Otras referencias 
[Daqri’s 4D Cubes Use Augmented Reality To Teach Kids The Periodic Table Of Elements](https://www.fastcompany.com/3014771/daqris-4d-cubes-use-augmented-reality-to-teach-kids-the-periodic-table-of-elem)  
App para iOS: con cubos por elemento, se ven propiedades, y juntándolos, se forman compuestos  
![](https://images.fastcompany.net/image/upload/w_937,c_limit,f_auto,q_auto,fl_lossy/fc/3014771-slide-s-1-4d-elements-blocks.jpg)  
[Elements 4D Interactive Blocks - kickstarter.com](https://www.kickstarter.com/projects/daqri/elements-4d-interactive-blocks)  
![](https://ksr-ugc.imgix.net/assets/011/504/355/489bdb80aa4cffb6d125364b38ab223b_original.jpg?ixlib=rb-4.0.2&crop=faces&w=1552&h=873&fit=crop&v=1463683694&auto=format&frame=1&q=92&s=6f813eebe00f9513cfcf3d833725aeba)   

Algunos recursos para construir los cubos  
[Elements 4D by DAQRI - commonsense.org](https://www.commonsense.org/education/app/elements-4d-by-daqri)  
[Create Your Own Augmented DAQRI 4D Element Cubes - instructables.com](https://www.instructables.com/id/Create-your-own-Augmented-DAQRI-4D-element-cubes/)  

[Apps de Realidad Aumentada para utilizar en tus clases  - blogs.upm.es](https://blogs.upm.es/observatoriogate/2019/03/29/apps-de-realidad-aumentada-para-utilizar-en-tus-clases/)   Alegría Blázquez 29 marzo, 2019  
![](https://blogs.upm.es/observatoriogate/wp-content/uploads/sites/13/2019/03/chemistry.jpg)  
Se cita de "RApp Chemistry (A): AR", enlazando play store que inicialmente indica versión obsoleta (luego indica inexistente) y remite  a y remite a  [RAppChemistry: AR](https://play.google.com/store/apps/details?id=com.RApp.Chemistry)  

[Realidad Aumentada en Química: tendiendo puentes entre lo abstracto y lo real - mascienciapf.blogspot.com](http://mascienciapf.blogspot.com/2015/07/realidad-aumentada-en-quimica-tendiendo.html)  

[Realidad Aumentada para la enseñanza de la fermentación en Primaria - mascienciapf.blogspot.com](http://mascienciapf.blogspot.com/2016/06/realidad-aumentada-para-la-ensenanza-de.html)  

[augmentedclass.com](http://www.augmentedclass.com/)  

Vídeo ejemplo combustión metano (en segundo 24)  
[![Augmented Class! #AugmentedReality in #Education - DEMO](https://img.youtube.com/vi/SpIlqnt1v-4/0.jpg "Augmented Class! #AugmentedReality in #Education - DEMO")](https://www.youtube.com/watch?v=SpIlqnt1v-4&t=24s)  

[twitter DelgadoTFer/status/1184204261556666368](https://twitter.com/DelgadoTFer/status/1184204261556666368)  
Como app de realidad aumentada puedes usar Mirage Make, que permite entre otras muchas cosas visualizar moléculas en 3D. Puedes encontrarla en inglés o en francés. Más info aquí:  
[mirage.ticedu.fr](http://mirage.ticedu.fr)  
[Mirage Make - mirage.ticedu.fr](http://mirage.ticedu.fr/?page_id=5034)  
[![Mirage Make : augmented reality editor](https://img.youtube.com/vi/5rsaUM8xZSE/0.jpg "Mirage Make : augmented reality editor")](https://www.youtube.com/watch?v=5rsaUM8xZSE)  

###  Geometría molecular con realidad aumentada
A veces también visualizar simplemente enlaces, no geometría real con ángulos correctos   

[VSEPR AR 4+ Joseph Schiarizzi - apple.com](https://apps.apple.com/es/app/vsepr-ar/id1441723400)  
Solo iOS
[VSEPR AR - apkpure.com](https://iphone.apkpure.com/vsepr-ar/com.fws.vseprar)  
![](https://iphone-image.apkpure.com/v2/app/4/b/a/4bad93671a80b8eb6c8626cbd0818d4a.jpg?h=500)  
  
[QuimicAR, CreativiTIC - play.google.com](https://play.google.com/store/apps/details?id=com.CreativiTIC.AugmentedClass&hl=es)  
![](https://play-lh.googleusercontent.com/JIYTaKD9HmrXmyi9XlCL3EHz0-_EBNo04wSwEql-5KFLUA5wW_go01OB9xwB7_P-8so=w720-h310)  


[Arloon Chemistry](http://www.arloon.com/apps/arloon-chemistry/)  
Acerca a los alumnos la experiencia de observar las moléculas en 3D y transportarlas hasta su escritorio con la Realidad Aumentada.  
[![Arloon Chemistry](https://img.youtube.com/vi/FVxFrsVe3GE/0.jpg "Arloon Chemistry")](https://www.youtube.com/watch?v=FVxFrsVe3GE)    
[![AR Chemistry Augmented Reality Education Arloon](https://img.youtube.com/vi/Qi3h18wJJiI/0.jpg)](https://www.youtube.com/watch?v=Qi3h18wJJiI "AR Chemistry Augmented Reality Education Arloon")  
Precisamente la geometría de H<sub>2</sub>O no es correcta en el vídeo  

[Arloon Chemistry - play.google.com](https://play.google.com/store/apps/details?id=com.Arloon.Chemistry.AR&hl=es)  (de pago)  

[AR VR Molecules Editor. Virtual Space OOO - itunes.apple.com](https://itunes.apple.com/us/app/ar-vr-molecules-editor/id1287093721?mt=8) 

Vídeo visto en  [twitter YujinAriza](https://twitter.com/YujinAriza)  
[twitter Studio_RTV/status/1070437396716642317](https://twitter.com/Studio_RTV/status/1070437396716642317)  
Having all of the atoms strewn about the work area to start is messy, so here's a spawn menu to create/clean up atoms. @YujinAriza helped with integration while I was traveling over the weekend

[twitter DelgadoTFer/status/1184206548446388232](https://twitter.com/DelgadoTFer/status/1184206548446388232) 
Os dejo un ejemplo aquí, para visualizar unos aminoácidos  
![](https://pbs.twimg.com/media/EG8lkFDXYAA4oL6?format=jpg&name=small)

