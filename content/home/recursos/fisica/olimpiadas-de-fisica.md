# Olimpiadas de Física

##  Orientaciones preparación fase local Madrid, elaboración propia
En 2015 resumo en un documento ideas de orientación / preparación y realizo y comparto soluciones de fase local Madrid.   

* [Olimpiada local Física Madrid, orientaciones de preparación](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/fisica/olimpiadas-de-fisica/oef-local-madrid-orientaciones-preparaci%C3%B3n.pdf)

##  Soluciones fase local Madrid, elaboración propia
* [Olimpiada local Física Madrid, soluciones](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/fisica/olimpiadas-de-fisica/oef-local-madrid-soluc.pdf)

Al realizar las soluciones de 2012 encuentro que ninguna de las 4 soluciones aportadas me parecen válidas. Si alguien que lee esto lo sabe o lo puede preguntar, me interesaría saber oficialmente si son erratas, y si es así, cómo se trató la corrección tipo test de esas preguntas (asumo que se ignoraron sin más).  
Recibido comentario "les han dicho de palabra durante la prueba que había alguna que otra errata en los enunciados". Sí que se puede ver en el enunciado de 2015 que la numeración de las preguntas tipo test fue errónea. En 2016 sí que corrigieron durante el examen un enunciado para indicar "protón" donde ponía "electrón"  

##  Información general
[rsef.es Olimpiada Española de Física](https://rsef.es/olimpiada-espanola-de-fisica)  
[Madrid.org, Educación Secundaria Obligatoria y Bachillerato - Olimpiadas de Física](http://www.madrid.org/cs/Satellite?c=CM_ContenComplem_FA&cc=1142408942823&cid=1142344984603&language=es&pageid=1142344693213&pagename=PortalEducacion%2FCM_Actuaciones_FA%2FEDUC_Actuaciones#fis)  (enlace validado en 2015)  
 [Olimpiada de Física 2015](http://web.archive.org/web/20180117150150/http://fisicabachilleratoexcelencia.blogspot.com.es/2014/12/olimpiada-de-fisica-2015.html) *waybackmachine*  Antigua página web de Física del IES San Mateo, que estuvo operativa hasta el curso 2014-2015.  
[Pozuelo y la Olimpiada de Física, Treinta jóvenes entrenan con un miembro de la Real Sociedad Española de Física](http://www.enpozuelo.es/noticia/4961/educacion/pozuelo-y-la-olimpiada-de-fisica.html)  
*El Ayuntamiento fomenta el estudio de la Física entre los estudiantes de 1º y 2º de Bachillerato de Pozuelo a través del programa de excelencia educativa “Jóvenes Talentos de la Física”.*
 
 La participación del profesorado está reconocida como una actividad de especial dedicación en Madrid, que tiene reconocimiento y debe tramitar el propio profesor, con un anexo firmado por secretario y director del centro.  
 [gestionesytramites.madrid.org Inscripción de actividades de formación permanente del profesorado](https://gestionesytramites.madrid.org/cs/Satellite?c=CM_Tramite_FA&cid=1142693999390&noMostrarML=true&pageid=1142687560411&pagename=ServiciosAE%2FCM_Tramite_FA%2FPSAE_fichaTramite&vest=1142687560411)  
  [gestiondgmejora.educa.madrid.org Resolución de 20 de diciembre de 2018, de la Dirección General de Becas y Ayudas al Estudio, por la que se publican las actividades de especial dedicación reguladas en el Decreto 120/2017, de 3 de octubre, del Consejo de Gobierno y en 
la Orden 2453/2018, de 25 de julio, de la Consejería de Educación e Investigación.](http://gestiondgmejora.educa.madrid.org/home/docs/2018-2019/RESOLUCION_ESPECIAL_DEDICACION_2019_20dic18.pdf)  
  En septiembre 2019 se plantea un curso de formación en CTIF Sur [LA PREPARACION DE LAS OLIMPIADAS DE FÍSICA, UNA METODOLOGÍA EN EL APRENDIZAJE DE LA FÍSICA EN BACHILLERATO](http://ctif.madridsur.educa.madrid.org/index.php?option=com_crif_cursos&view=unCurso&layout=default&id=1869&lista=pordepartamento&iddepartamento=5&orden=&cadenaBusqueda=&modalidades=&destinatarios=&areas=&niveles=&departamentos=&estados=&cursoacademico=0&resultadoBusquedaAvanzada=&ver_todas=&Itemid=57)  
   Información por Sergio Montañez en [divulgamadrid.blogspot.com PREPARACIÓN OLIMPIADA ESPAÑOLA DE FÍSICA](https://divulgamadrid.blogspot.com/p/preparacion-olimpiada-espanola-de-fisica.html) 

##  Estructura de las olimpiadas
 Se celebran primero unas fases locales, que no son simplemente a nivel autonómico: van a nivel de distritos universitarios.  
 Los mejores de las fases locales se presentan a una fase nacional, que cada convocatoria es en un sitio distinto: [rsef.es Olimpiadas nacionales celebradas](https://rsef.es/olimpiadas-nacionales-celebradas)  
 Los mejores de la fase nacional representan a España en la olimpiada internacional, que cada convocatoria es un sitio distinto  
[ipho past and future organizers](http://web.archive.org/web/20150801170622/http://ipho.phy.ntnu.edu.tw/past-and-future-organizers.html) *waybackmachine*   

Pongo información fechas olimpiadas: algunas recopiladas por año. *Elimino / reviso algunos enlaces*, que pasado el tiempo no aplican / pasa a aplicar para cualquier año.   

###  Fechas asociadas a las olimpiadas 2014
Se ponen las fechas para que sean orientativas para convocatorias posteriores  
Las olimpiadas de 2014 fueron las XXV nacionales y las XLV internacionales.  
Fase local de Madrid:
- Inscripción hasta 27 de febrero de 2014
- Se celebró el 7 de marzo de 2014: dos partes, unas 15 cuestiones tipo test y problemas con parte experimental (gráficas, propagación de errores)  
La fase nacional se celebró en La Coruña del 4 al 7 de abril de 2014  
La fase internacional se celebró del 13 al 21 de julio de 2014 en Astana, Kazakhstan 

###  Fechas asociadas a las olimpiadas 2015
Las olimpiadas de 2015 son las XXVI nacionales y las XLVI internacionales.  
A nivel internacional ya está fijado que serán del 4 al 13 de Julio en Mumbai, India  
La XX Olimpiada Iberoamericana de Física será en septiembre de 2015 en Cochabamba, Bolivia   

Fases locales en 2015 (por orden cronológico)  
Alicante: 31 de enero  
[Cataluña: 6 de febrero](http://scfis.iec.cat/olimpiada/2015/convocatoria.php)  
Castilla y León (Burgos, Valladollid con pruebas de distrito en Palencia, Segovia, Soria y Valladolid): 20 de febrero  
Extremadura: 20 de febrero   
Castilla-La Mancha: 22 de febrero *Alberto Nájera: "vamos a intentar que todo el proceso sea telemático, intentando huir de los papeles"*  (el texto era similar al que ahora aparece en [uclm.es olimpiada de física](https://www.uclm.es/departamentos/fisica-aplicada/olimpiada%20de%20fisica))
Madrid: 27 de febrero: durante el mes de febrero se informa de un cambio de fecha: pasa a ser el día 6 de marzo  
[Aragón: 27 de febrero](http://olimpiada_de_fisica.unizar.es/oafConvocatoriaActual.htm)  
[Valencia: 3 de marzo (preselección 28 octubre 2014)](http://www.uv.es/uvweb/delegacion-incorporacion-UV/es/cooperacion-secundaria/olimpiadas/fisica/bases-1285871000846.html)  
Murcia: 4 de marzo [País Vasco: 6 de marzo](http://www.ehu.eus/p200-content/es/contenidos/evento/20150306_olimpiada_fisica/es_olimpia/olimpiada_fisica.html?utm_source=feedburner&utm_medium=twitter&utm_campaign=Feed%3A+ehu%2Feventos+%28UPV%2FEHU+Eventos%29)  
[Andalucía: 13 de marzo](http://www10.ujaen.es/sites/default/files/users/fisica/Fase%20local%20olimpiada_fisica%202015.pdf)  
Navarra: 13 de marzo  
[Asturias: 21 de marzo](http://www.rsefas.org/wp-content/uploads/downloads/2015/02/Primera_Circular_OLIMPIADA-ASTURIANA-DE-F%C3%8DSICA-2015.pdf) 

Fase nacional: 10 a 13 de abril en Madrid

###  Fechas asociadas a las olimpiadas 2016
Las olimpiadas de 2016 son las XXVII nacionales y las XLVII internacionales.  
A nivel internacional ya está fijado que serán del 10 al 18 de Julio en Zurich, Suiza  
Fases locales en 2016 (por orden cronológico)  
[Valencia: 1 de marzo (preselección 27 octubre 2015)](http://www.uv.es/uvweb/delegacion-incorporacion-UV/es/cooperacion-secundaria/olimpiadas/fisica/bases-1285871000846.html)  
Madrid: 4 de marzo [Cataluña: 5 de febrero](http://scfis.iec.cat/olimpiada/2016/convocatoria.php)  
Salamanca: 12 de febrero  
[Extremadura: 19 de febrero](http://www.unex.es/organizacion/servicios-universitarios/servicios/comunicacion/eventos/pruebas-de-la-fase-local-de-la-xxvii-olimpiada-espanola-de-fisica#.VshsYELVO1E)  

Fase nacional: 22 a 25 de abril en Sevilla

###  Fechas asociadas a las olimpiadas 2017
Las olimpiadas de 2017 son las XXVIII nacionales y las XLVIII internacionales.  
Fases locales en 2017 (se publica información de fases locales 2017 en  [rsef.es olimpiada espanola de física](https://rsef.es/olimpiada-espanola-de-fisica).  
Ordeno por fecha  
Cataluña (Agrupación de Universidades de Cataluña): 3 de febrero.  [Más información](http://scfis.iec.cat/olimpiada/)  
Universidad de La Laguna: 10 de febrero Universidad Jaume I de Castellón: 10 de febrero.  [Más información](https://www.uji.es/serveis/use/base/orientacio/orientacioabans/olimpiades/olimpiadesuji/fisica/)  
Castilla-La Mancha: 12 de febrero   
[Baleares: 14 de febrero](https://periodicodeibiza.es/pitiusas/local/2017/02/14/248484/alumnos-ibiza-compiten-olimpiada-fisica-uib.html)  Islas Baleares (Universidad de Les Illes Balears): 14 de febrero  
Galicia (Universidades de A Coruña, Santiago y Vigo): 17 de febrero.  [Más información](https://olimpiada.webs.uvigo.es/)  
 Extremadura (Universidad de Extremadura): 17 de febrero  
 Universidad de Valencia y Politécnica de Valencia: 23 de febrero.  [Más información](http://www.uv.es/uvweb/delegacion-incorporacion-UV/es/cooperacion-secundaria/olimpiadas/fisica/bases-1285871000846.html)  
 Universidad de Valladolid: 24 de febrero.  
 Universidad de Las Palmas de Gran Canaria: 24 de febrero  
 Madrid (Agrupación de Universidades de la Comunidad de Madrid): 24 de febrero  
 La Rioja (Universidad de La Rioja): 24 de febrero.  [Más información](http://www.unirioja.es/facultades_escuelas/fct/actividades/olimpiada_fisica_17.shtml)  
 Aragón (Universidad de Zaragoza): 24 de febrero.  [Más información](http://fisicaaplicada.unizar.es/olimpiada-de-fisica/presentacion-oaf)  
 Universidad Miguel Hernández: 24 de febrero.  [Más información](http://estudios.umh.es/atencion-al-estudiante/mireu-una-mirada-a-la-umh/olimpiadas/)  
 Universidad de Alicante: 25 de febrero.  [Más información](http://blogs.ua.es/rsefalicante/2017/01/30/xxviii-olimpiada-de-fisica-2017-fase-local-de-alicante/)  
 [Murcia: 1 de marzo](https://www.um.es/OlimpiadaFisica/)  
 Almería: 2 de marzo  
 [León: 3 de marzo](http://www.ileon.com/universidad/071112/leon-y-ponferrada-acogen-en-marzo-la-fase-local-de-la-olimpiada-de-fisica)  
 Universidad de Salamanca: 3 de marzo.  [Más información](http://diarium.usal.es/olimpiadafisica)  
 País Vasco (Universidad del País Vasco): 3 de marzo.  [Más información](http://tp.lc.ehu.eus/olimpiadas/)  
 Universidad de León: 3 de marzo.  [Más información](http://blogs.unileon.es/olimpiadadefisica/)  
 Ceuta: 3 de marzo  
 Región de Murcia (Universidades de Murcia y Politécnica de Cartagena): 3 de marzo.  [Más información](http://www.um.es/OlimpiadaFisica/)  
 Cantabria: 4 de marzo  
 Navarra (Universidad Pública de Navarra): 10 de marzo.  
 Universidad de Burgos: 10 de marzo.  [Más información](http://wwww.ubu.es/departamento-de-fisica/actividades/olimpiadas-de-fisica/olimpiada-de-fisica-2017)  
 Asturias (Universidad de Oviedo): 11 de marzo.  [Más información](http://www.rsefas.org/) 
 
 Fase nacional: 31 de marzo al 3 de abril en Gerona

###  Fechas asociadas a las olimpiadas 2018
 Detallado en  [https://turinadiario.blogspot.com.es/2017/12/olimpiada-de-fisica-2018.html](https://turinadiario.blogspot.com.es/2017/12/olimpiada-de-fisica-2018.html) 

###  Fechas asociadas a las olimpiadas 2019
 La fase nacional será en Salamanca en abril [https://rsef.es/informacion-olimpiada-2019](https://rsef.es/informacion-olimpiada-2019) 

###  Fechas asociadas a las olimpiadas 2020
 La fase nacional será en Murcia [https://rsef.es/olimpiada-espanola-de-fisica](https://rsef.es/olimpiada-espanola-de-fisica)  
 Fase local Madrid 4 marzo 2020 [revistadefisica.es  Olimpiada Española de Física 2020, XXXI edición](http://revistadefisica.es/index.php/ref/article/download/2684/2154)  
_...La situación extraordinaria desencadenada en marzo a causa de la pandemia del coronavirus también afectó a las olimpiadas científicas. La Olimpiada de Física estaba programada del 23 al 26 de abril en Murcia. En los días previos a la declaración del estado de alarma, y ante la previsión de agravamiento de la crisis sanitaria, se decidió suspender la Olimpiada para las fechas previstas. A esas alturas, en marzo, prácticamente todo el trabajo organizativo estaba ya acabado: reserva de hoteles, diseño de las pruebas, aulas, etc....
En cuanto a las olimpiadas internacionales, se suspendió la 51.ª Inter-national Physics Olympiad que debía celebrarse en Vilna, Lituania, en el mes de julio. Esta olimpiada se ha pospuesto para 2021, tras el logro de trasladar todo el calendario confirmado de Bielorusia-2021 a Ecuador-2028._ 

## Fechas asociadas a las olimpiadas 2021

[Madrid 11 marzo, telemática](https://rsef.es/images/Fisica/ConvocatoriaOEF_MADRID2021.pdf)  

##  Olimpiada nacional: fases locales y fase nacional
En la web de la Real Sociedad Española de Física había enlaces a información de fases locales; se pone aquí un listado basado en su contenido en 2014 (se reordena alfabéticamente y se agrupan algunas por comunidades, revisando/actualizando algunos enlaces)  
En el momento de escribir esto solo está este enlace operativo [rsef.es Fases Locales OEF 2018](https://rsef.es/fases-locales) 

**Andalucía**   
   * Córdoba  [uco.es sobre las olimpiadas (de Física)](http://www.uco.es/organiza/departamentos/fisica/es/actividades/olimpiadas-de-fisica/sobre-las-olimpiadas) 
   * Jaén: [ujaen.es http://www10.ujaen.es/conocenos/departamentos/fisica/olimpiadadefisica](http://www10.ujaen.es/conocenos/departamentos/fisica/olimpiadadefisica) 
   
**Asturias:** [http://www.rsefas.org/](http://www.rsefas.org/)  
**Castilla-La Mancha:** [uclm.es Departamento de Física, Olimpiada de Física](https://www.uclm.es/departamentos/fisica-aplicada/olimpiada%20de%20fisica)  
**Castilla y León**  
   * Burgos: [ubu.es Departamento de Física › Actividades › Olimpiadas de Física](https://www.ubu.es/departamento-de-fisica/actividades/olimpiadas-de-fisica) 
   * León: [http://blogs.unileon.es/olimpiadadefisica/](http://blogs.unileon.es/olimpiadadefisica/) 
   * Salamanca: [http://diarium.usal.es/lld/](http://diarium.usal.es/lld/) 

**Cataluña:** [http://scfis.iec.cat/olimpiada/](http://scfis.iec.cat/olimpiada/)  
**Comunidad Valenciana**  
   * Alicante: [ciencias.ua.es XXXII Olimpiada Física](https://ciencias.ua.es/es/extension-universitaria/centros-de-educacion-secundaria/olimpiadas-cientificas/xxxii-olimpiada-fisica.html)
   [ua.es Histórico olimpiadas científicas](https://ciencias.ua.es/es/extension-universitaria/centros-de-educacion-secundaria/olimpiadas-cientificas/historico-olimpiadas.html)   
   * Miguel Hernández: [estudios.umh.es olimpiadas](https://estudios.umh.es/olimpiadas/) 
   * Valencia: [uv.es Olimpíades de Física 2013-14](http://www.uv.es/uvweb/fisica/es/activitats-batxillerat/olimpiada-fisica-1285851747826.html)   
   [uv.es olimpiadas de Física, bases (cooperación secundaria)](http://www.uv.es/uvweb/delegacion-incorporacion-UV/es/cooperacion-secundaria/olimpiadas/fisica/bases-1285871000846.html)  
   [uv.es olimpiadas de Física, bases (actividades secundaria)](https://www.uv.es/uvweb/fisica/es/actividades-secundaria/olimpiada-fisica/bases-1286277097559.html) 
**Extremadura**  
[ Organización Gobierno de la UEx Vicerrectorados Estudiantes, Empleo y Movilidad Funciones EBAU Coordinación EBAU 2023-24 Materias Física Olimpiadas Física ](https://www.unex.es/organizacion/gobierno/vicerrectorados/vicealumn/funciones/car_20050411_001/coordinacion-ebau-2023-24/materias/fisica/olimpiadas_fisica)  
**Galicia:**  
[usc.gal búsqueda olimpiadas física](https://www.usc.gal/gl/web/busqueda.html?q=olimpiadas%20f%C3%ADsica)  
[FisQuiMat Olimpiadas de Física](https://sites.google.com/site/smmfisicayquimica/fisica-bac2/fisica-olimpiadas), Moisés López Caeiro 
**Madrid:** [rsef.es fase local de madrid](http://web.archive.org/web/20130614211456/http://www.rsef.es/oef/index.php/fase-local-de-madrid) *waybackmachine*  
**Murcia:** [www.um.es/OlimpiadaFisica](http://www.um.es/OlimpiadaFisica)  
**Navarra:** [unavarra.es departamento ciencias, olimpiada física](http://www.unavarra.es/departamento-ciencias/tablon-de-anuncios?contentId=251435) incluye de años anteriores y soluciones.  
**Rioja:**  [http://www.unirioja.es/dptos/dq/fa/olimp/olimp.html](http://www.unirioja.es/dptos/dq/fa/olimp/olimp.html)  
**Zaragoza:** [http://olimpiada_de_fisica.unizar.es](http://olimpiada_de_fisica.unizar.es)  Se incluyen aquí webs adicionales con recopilaciones o información  

##  Olimpiada Iberoamericana
 [Olimpiada Iberoamericana de Física (OIbF) ](https://es.wikipedia.org/wiki/Olimpiada_Iberoamericana_de_Física)  
 Suele tener una web distinta para cada año asociada al país que la organiza  
 [Olimpiada Iberoamericana de Física 2019, El Salvador](https://oibf2019.gofisica.org/)   
 [Olimpiada Iberoamericana de Física 2019, El Salvador, pruebas](https://oibf2019.gofisica.org/pruebas/)  
 Relacionado [academia.edu Olimpíada Argentina de Física](http://www.academia.edu/9803340/Olimp%C3%ADada_Argentina_de_F%C3%ADsica) 
 [Olimpiadas Portugal 2020, con información local, internacional e iberoamericana](http://olimpiadas.spf.pt/oibf/oibf.shtml)  

##  Olimpiada internacional
[IPhO Problems and Solutions](https://www.physprob.com/)  This website was created and is maintained by Mihkel Kree.
[IPhO International Physics Olympiads](http://www.jyu.fi/tdk/kastdk/olympiads/)  
*IPhO wepage has been moved to http://ipho.phy.ntnu.edu.tw. This page is not updated anymore.**
[IPhO Lithuania 2021 problems](https://www.ipho2021.lt/en/ipho-problems/)  

[GRUPO HEUREMA. EDUCACIÓN SECUNDARIA, ENSEÑANZA DE LA FÍSICA Y LA QUÍMICA, sección: PROBLEMAS DE LAS OLIMPIADAS INTERNACIONALES DE FÍSICA](http://web.archive.org/web/20190715193256/http://heurema.com/OLIMPIFIS.htm) *waybackmachine*   
Desde 1967 a 2018, problemas resueltos en español, José Luis Hernández Pérez ; Agustín Lozano Pradillo, Ricardo David Fernández Cruz, Jaime Solá de los Santos 

Además de la internacional (IPhO) y la iberoamericana (OIbF), también hay olimpiadas asiáticas (APhO), y olimpiadas universitarias en Cuba   
[Vietnam gana medalla de oro en Olimpiada de Física de Asia](https://es.vietnamplus.vn/vietnam-gana-medalla-de-oro-en-olimpiada-de-fisica-de-asia/73425.vnp)  
[juventudrebelde.cu Premian a ganadores de Olimpiada Nacional Universitaria de Física](http://www.juventudrebelde.cu/cuba/2015-05-13/premian-a-ganadores-de-olimpiada-nacional-universitaria-de-fisica/)  

[APhO 2020](https://apho2020.tw/site/page.aspx?pid=280&sid=1316&lang=en)  

##  Recopilación de problemas y soluciones fase local, nacional e internacional
Aparte de que se puedan localizar en alguno de los enlaces que se incluyen en la página, los problemas localizados y descargados se organizan en una carpeta de GitLab drive.fiquipedia que se comparte y se enlaza desde aquí [drive.fiquipedia Olimpiadas Física](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/Olimpiadas%20F%C3%ADsica)

[rsef.es Problemas de las Olimpiadas Nacionales](https://rsef.es/problemas-de-la-oef)  

El contenido inicial de problemas de fases locales es, reorganizando nombres, el de [rsef.es Recopilación de problemas de las Fases Locales desde la edición 2004](http://web.archive.org/web/20150306102808/http://rsef.es/oef/index.php/problemas-propuestos-en-las-fases-locales) *waybackmachine*   

[IPhO Problems and Solutions - olimpicos.net](https://ipho.olimpicos.net/)  
Developed by Lucas Takayasu  

[Conteúdos de física, química, astronomia e muito mais, voltados para um bom desempenho em olimpíadas nacionais e internacionais!](https://olimpicos.net/)  

[Problemas de física - olimpicos.net](https://olimpicos.net/fisica/problemas-de-fisica/) 


## 100 RETOS Y DESAFÍOS DE FÍSICA 
[100 RETOS Y DESAFÍOS DE FÍSICA](https://eusal.es/index.php/eusal/catalog/book/978-84-1311-001-1)   
Para alumnos de Bachillerato que preparan la Olimpiada de Física y estudiantes de Grados de Ciencias   
ISBN: 978-84-1311-002-8   

### Pruebas Olimpiada Iberoamericana de Física

[Pruebas. Olimpiada Iberoamericana de Física](https://www.oibf.org/p/pruebas.html)  
[Carpeta de pruebas OIbF](https://drive.google.com/drive/folders/12Z5_0xc0eXhew4INyDHmJ44IJZGAce4v?usp=sharing)  
[Libro con todas las Pruebas de las OIbF desde 1991 hasta 2023 (versión 0.1)](https://drive.google.com/file/d/11KIV_psRHk9lLX5R_vG8zkehfwKIKg8Q/view?usp=sharing)  

## Olimpíades Locals de Física Illes Balears
[Olimpíades Locals de Física Illes Balears](http://dfs.uib.cat/apl/aac/OFisica/)  
Consultado en 2021 tiene de 2003 a 2008

## Física de competición  

[Física de competición - fseneca.es](https://fseneca.es/web/fisica-de-competicion)  
[Física de competición. Problemas avanzados para bachillerato, olimpiadas y concursos. Antonio Guirao Piñera, Luis Roca Zamora. cc-by-nc-nd, ISBN 978-84-125914-0-8, depósito legal MU 202-2022](https://fseneca.es/web/sites/web/files/Libro%20Olimpiadas%20fisica%20-%20digital.pdf)  




