
# Estados de oxidación

Un tema muy importante de cara a la  [formulación](/home/recursos/quimica/formulacion)  es el tema de los estados de oxidación, por lo que le dedico esta página.  
A esta página le he puesto de momento un nombre único "estados de oxidación", pero en ella trato conceptos relacionados: números de oxidación y valencias  
En general todos los términos son relativos a dar información sobre el estado de los electrones de un átomo que forma parte de un compuesto.

[Rusty Dungeon - fisicayquimicaweb.es](https://fisicayquimicaweb.es/data/documents/RUSTYDUNGEON/index.html)  
![](https://fisicayquimicaweb.es/gallery_gen/9dbd8fa90b0162bb5a8ad3cc65d0d0f5_317x190.png)  

[twitter onio72/status/1465083492262420483](https://twitter.com/onio72/status/1465083492262420483)  
Otra animación de  @geogebra  para ayudarme en la pizarra en clase de Física y Química de 4º de ESO. Formulación. Practicar determinando el estado de oxidación del elemento no metálico ( generalmente ). Es algo que les cuesta trabajo a los alumnos.  
[FQ4ESO Estado oxidación. Antonio González García](https://www.geogebra.org/m/vwdyq7ph)  

##  Distintos conceptos: relaciones y diferencias entre ellos

###  Valencia
En general a veces se habla de  [valencias](http://es.wikipedia.org/wiki/Valencia_%28qu%C3%ADmica%29)  y tablas de valencias ( [valencia es un término reconocido por la IUPAC](http://goldbook.iupac.org/V06588.html) ), cuando se enseña por primera vez en 3º de ESO (muchas veces los alumnos hablan de "aprenderse las valencias", yo mismo lo recuerdo así en mi época de instituto, y hay tablas periódicas que incluyen como dato las valencias, pero el concepto más correcto es el de estado de oxidación.  

La valencia hace referencia al máximo número de enlaces que forma, pensando en combinarlo con Hidrógeno o con Flúor. También se suele asociar al concepto de  [electrones de valencia](http://es.wikipedia.org/wiki/Electrones_de_valencia)  que son los electrones más externos y los que intervienen en los enlaces; la valencia de un elemento es el número de electrones que necesita o que le sobra para tener completo su último nivel. Por ejemplo los elementos del grupo 1 alcalinos (metales), tienen todos valencia 1 porque tienen un único electrón en su última capa y tienen facilidad para perderlo y convertirse en un ion positivo (catión). La valencia de los gases nobles es cero.  

La [definición de valencia por la IUPAC](http://goldbook.iupac.org/V06588.html)  de 1994 es: *"El máximo número de átomos univalentes (originalmente átomos de hidrógeno o cloro) que pueden combinarse con un átomo del elemento en consideración, o con un fragmento, o para el cual un átomo de este elemento puede ser sustituido."* muestra que las valencias realmente siempre son positivas, de modo que donde aparecen valores negativos (muchas "tablas de valencias") no son realmente valencias... Además la propia definición de valencia de la IUPAC implica que tiene un único valor ..  

[EL CONCEPTO DE VALENCIA: SU CONSTRUCCIÓN HISTÓRICA Y EPISTEMOLÓGICA Y LA IMPORTANCIA DE SU INCLUSIÓN EN LA ENSEÑANZA](https://www.scielo.br/j/ciedu/a/Xhbc9WnQkY8bpYX3CcBWBhD/?lang=es&format=pdf)  

###  Valencia frente a estado de oxidación
 [wikipedia.org Valence_(chemistry) Valence versus oxidation state](http://en.wikipedia.org/wiki/Valence_%28chemistry%29#Valence_versus_oxidation_state)  
También se pueden ver ejemplos mostrando que la valencia siempre es positiva y única como se ha indicado pero no el estado de oxidación.  
[wikipedia.org Valence_(chemistry) Examples](http://en.wikipedia.org/wiki/Valence_%28chemistry%29#Examples) 

###  Número de oxidación y estado de oxidación
Normalmente se habla de  [estado de oxidación](http://es.wikipedia.org/wiki/Estado_de_oxidaci%C3%B3n) , aunque a veces se usa el término número de oxidación; se suelen usar indistintamente ambos términos, aunque son distintos y la IUPAC contempla definiciones separadas:  [oxidation number (definición IUPAC, inglés)](http://goldbook.iupac.org/O04363.html)  y  [oxidation state (definición IUPAC, inglés)](http://goldbook.iupac.org/O04365.html).  

Estado de oxidación de un átomo en un compuesto es un número teórico que indica el número de electrones que el átomo habría perdido o ganado, la carga que tendría, si el compuesto fuera completamente iónico. El estado de oxidación de un átomo es una medida de lo oxidado que está en un compuesto, de modo que un mismo elemento puede tener un valor distinto en distintos compuestos. Su valor suelen ser enteros, aunque a veces pueden ser fraccionarios.  

Se pueden ver  [diferencias en la página de la wikipedia en inglés sobre el número de oxidación](http://en.wikipedia.org/wiki/Oxidation_number#Oxidation_number_versus_oxidation_state) , donde se indica que a menudo son numéricamente iguales y son intercambiables. Sin embargo hay algunas situaciones en las que no coinciden. Como diferencia se puede citar que los números de oxidación se representan con números romanos, mientras que los estados de oxidación se representan con números árabes, y que los números de oxidación pueden formar parte del nombre de un compuesto (se usan en química inorgánica), pero no los estados de oxidación.  

Comentario: en noviembre 2012  [la página de la wikipedia en español sobre número de oxidación](http://es.wikipedia.org/w/index.php?title=N%C3%BAmero_de_oxidaci%C3%B3n&redirect=no)  no contiene nada más que una redirección al estado de oxidación, y las dos páginas en inglés  [Oxidation_number](http://en.wikipedia.org/wiki/Oxidation_number)  (que tiene como nota **Not to be confused with oxidation state**) y " [Oxidation_state](http://en.wikipedia.org/wiki/Oxidation_state) " (que tiene como nota **Not to be confused with Oxidation number**) están ambas vinculadas a la misma página en español  [Estado_de_oxidación](http://es.wikipedia.org/wiki/Estado_de_oxidaci%C3%B3n) , lo que muestra lo confuso del tema y hasta qué punto se suelen intercambiar.  

El concepto número de oxidación está tratado (mezclado con estado de oxidación) en  [alonsoformula.com Número oxidación](http://www.alonsoformula.com/inorganica/numero_oxidacion.htm) 

###  Carga formal
Es un concepto que se puede relacionar con el estado de oxidación;  
[wikipedia.org Carga formal Carga formal vs. estado de oxidación](https://es.wikipedia.org/wiki/Carga_formal#Carga_formal_vs._estado_de_oxidaci%C3%B3n) 

### Estados de oxidación y números cuánticos

En general los números de oxidación más estables se asocian a "conseguir configuración de gas noble", pero se puede detallar más:  

Son estables las asociadas a capas "totalmente llenas / totalmente vacías ó 50% semillenas".

Por ejemplo Cl tiene -1 porque pasa de 3p5 a 3p6, pero también tiene +1, +3, +5 y +7  
+1: de 3s2 3p5 pasa a 3s1 3p5, una semillena  
+3: de 3s2 3p5 pasa a 3s1 3p3, ambas semillenas  
+5: de 3s2 3p5 pasa a 3s2 3p0, llena y vacía  
+7: de 3s2 3p5 pasa a 3s0 3p0, ambas vacías  



En EvAU modelo 2023 se pide configuración de Z=29 (cobre) que es una excepción, y también estado de oxidación más estable / ion más estable, y soluciones oficiales indican +1  
Sale poco: había salido 2013-Jun-Coi-A1 con Zn, 2002-Sep-C1 con Mn y 2000-Mod-C1 con Zn  

Se puede plantear razonar los estados más probables de cuarto periodo entre Z=21 (Sc) y Z=30 (Zn)  

+3 para Sc (inicialmente 4s2 3d1, pasa a 4s0 y 3d0)  
+4 para Ti (inicialmente 4s2 3d2, pasa a 4s0 y 3d0)  
+5 para V (inicialmente 4s2 3d3, pasa a 4s0 y 3d0)  
+5 para Cr (inicialmente 4s1 3d5, pasa a 4s1 y 3d0, semillena y vacía)  
+6 para Cr (inicialmente 4s1 3d5, pasa a 4s0 y 3d0)  
+5 para Mn (inicialmente 4s2 3d5, pasa a 4s2 3d0, llena y vacía)  
+2 para Mn (inicialmente 4s2 3d5, pasa a 4s0 3d5, vacía y semillena)  
+6 para Mn (inicialmente 4s2 3d5, pasa a 4s1 3d0, semillena y vacía)  
+7 para Mn (inicialmente 4s2 3d5, pasa a 4s0 y 3d0, ambas vacías)  
+2 para Fe (inicialmente 4s2 3d6, pasa a 4s1 3d5, ambas semillenas)  
+3 para Fe (inicialmente 4s2 3d6, pasa a 4s0 3d5 ,vacía y semillena)  
+2 para Co (inicialmente 4s2 3d7, pasa a 4s2 3d5, llena y semillena)  
+3 para Co (inicialmente 4s2 3d7, pasa a 4s1 3d5, ambas semillenas)  
+2 para Ni (inicialmente 4s2 3d8, pasa a 4s0 3d8, una vacía)  
+3 para Ni (inicialmente 4s2 3d8, pasa a 4s2 3d5, llena y semillena)  
+1 para Cu (inicialmente 4s1 3d10, pasa a 4s0 3d10, vacía y llena)  
+2 para Cu (inicialmente 4s1 3d10, pasa a 4s0 3d9, una vacía)  
+2 para Zn (inicialmente 4s2 3d10, pasa a 4s0 3d10, vacía y llena)  

[How to Write the Electron Configuration for Copper (Cu, Cu+, and Cu2+). Wayne Breslyn, umd.edu](https://terpconnect.umd.edu/~wbreslyn/chemistry/electron-configurations/configurationCopper-Cu.html)  

### Memorización de estados de oxidación

Ver tablas de estados en esta página  
A veces citados como valencias 
* [uv.es Valencias de los elementos químicos](https://www.uv.es/vicami/Juegos/HtmlJs/Valencias/juegoValencias.html)  
* [educaplay.com Test valencias Química](https://es.educaplay.com/recursos-educativos/2038386-test_valencias_quimica.html)  
* [liveworksheets.com Tabla de valencias](https://es.liveworksheets.com/is890013oj)  
* [Formulación inorgánica 04 tabla de valencias truco profesor10demates](https://www.youtube.com/watch?v=MhytizmGwUM)  
* [1library.co Ejercicios de valencia y número de oxidación ](https://1library.co/document/4yr6x5py-ejercicios-de-valencia-y-numero-de-oxidacion.html)  

En [Resumen de las normas IUPAC 2005 de nomenclatura de Química Inorgánica para su uso en enseñanza secundaria y recomendaciones didácticas, Secundaria](https://rseq.org/wp-content/uploads/2018/09/3-Secundaria.pdf) se indica para 2ºESO 
>3.  Lo que se desaconseja hacer (por ser incorrecto o pedagógicamente desaconsejable)   
...  
Recurrir a la memorización de tablas de números de oxidación.

##  Tablas de estados de oxidación
Se suelen presentar en 3º de ESO por primera vez y se habla de "tablas de valencias". Es algo que hay que memorizar para poder formular si no se usa sistemática, y normalmente a medida que aumenta el nivel (al pasar desde 3º a 4º, de 4º a 1º se van ampliando, hay que recordar que en 2º de bachillerato se supone que no se amplía más formulación a más elementos, solamente se amplía la orgánica)  

Un ejemplo de tres niveles de "tablas de estados de oxidación" con "NÚMEROS DE OXIDACIÓN MÁS USUALES DE ALGUNOS ELEMENTOS" se podía ver  ver en  
[educastur.princast.es fisquiweb](https://web.archive.org/web/20160311233759/http://web.educastur.princast.es/proyectos/fisquiweb/Apuntes/Formulacion/formulacionPDF.htm) *waybackmachine*, donde tienen  
[N1 = 3º ESO](https://web.archive.org/web/20121222070631/http://web.educastur.princast.es/proyectos/fisquiweb/Apuntes/Formulacion/Numoxid1.pdf)  *waybackmachine* (por ejemplo +2 ,+3 solamente Fe, Co, Ni)  
[N2 = 4º ESO](https://web.archive.org/web/20130228132137/http://web.educastur.princast.es/proyectos/fisquiweb/Apuntes/Formulacion/Numoxid2.pdf)  *waybackmachine* (por ejemplo +2 ,+3 se añade Cr, Mn respecto a lo anterior)  
[N3 = 1º Bachillerato](https://web.archive.org/web/20141107095855/http://web.educastur.princast.es/proyectos/fisquiweb/Apuntes/Formulacion/Numoxid_Bach.pdf) *waybackmachine* (por ejemplo para Cr y Mn se detalla respecto a lo anterior +2, +3, +6 (cromatos y dicromatos) Cr // +2 , +3, (+4), +6 (manganatos), +7 (permanganatos) Mn)  
*Al migrar a fisquiweb.es parece que ya no están esas tablas, se citan [fisquiweb.es números de oxidación](http://fisquiweb.es/Formulacion/formulas_informacion.htm). Pongo algunas en waybackmachine*

A mí personalmente me parece más gráfico una tabla de números de oxidación en forma de tabla periódica; no aprenderse la relación nº oxidación -> elementos que lo tienen, sino saber elemento -> números de oxidación que tiene. Así por ejemplo para los grupos alcalinos y alcalinotérreos es inmediato recordarlo.  
Las tablas periódicas completas suelen incluirlos, aunque lo habitual es una tabla periódica especial solamente con los números de oxidación  
Incorporo una tabla de elaboración propia, recogiendo ideas básicas y poniendo ambos formatos  

**[TablaValenciasEstadosOxidacionNumerosOxidacion.pdf FiQuiPedia](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/estados-de-oxidacion/TablaValenciasEstadosOxidacionNumerosOxidacion.pdf)**  

Algunos ejemplos:
   * Sencillo [alonsoformula.com tabla nº oxidación](http://www.alonsoformula.com/inorganica/taboa_numero_oxidacion.htm) 
   *  [quimica.uaq.mx NÚMEROS DE OXIDACIÓN DE LOS ELEMENTOS DE LA TABLA PERIÓDICA](https://quimica.uaq.mx/docs/TP_valencias.pdf)  
   *  [vaxasoftware.com Periodic table of the elements and valences (valencies) ](http://www.vaxasoftware.com/doc_eduen/qui/tpvalen.pdf)  (tabla periódica con números de oxidación)
   *  [vaxasoftware.com Table of valences (valencies)](http://www.vaxasoftware.com/doc_eduen/qui/valencia.pdf)  (tabla de valencias)  
   *  [educaplus.org estados de oxidación](http://www.educaplus.org/elementos-quimicos/propiedades/estados-oxidacion.html)  
   *  [Tabla periódica estados oxidación - fisiquimicamente](https://fisiquimicamente.com/blog/2020/08/23/tabla-periodica-de-los-elementos/tabla-periodica-elementos-estados-oxidacion.pdf)  
   
Es importante tener presente que se aprenden los más comunes, porque cada elemento puede tener muchos; en pocas tablas sale que el Oxígeno tiene número de oxidación +2 en el compuesto OF2. Por ejemplo podemos ver en  [wikipedia.org Anexo:Estados de oxidación de los elementos](http://es.wikipedia.org/wiki/Anexo:Lista_de_estados_de_oxidaci%C3%B3n_de_los_elementos)  que hay muchos (los estados más comunes aparecen en negrita) y ver que la Plata, un elemento al que casi siempre se le asocia como estado de oxidación solamente +1 (en negrita en la tabla), tiene en ciertos compuestos raros +2, +3 e incluso +4  

Una tabla tomada de  [compoundchem.com The Periodic Table of Oxidation States](http://www.compoundchem.com/2015/11/17/oxidation-states/)   
![](http://www.compoundchem.com/wp-content/uploads/2015/11/Periodic-Table-Of-Oxidation-States-Small-1024x724.png)  


