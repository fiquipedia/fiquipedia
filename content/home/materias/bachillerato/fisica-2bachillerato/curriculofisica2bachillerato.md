# Curriculo Física 2º Bachillerato (LOE)

En la Comunidad de Madrid se desarrolla en  [DECRETO 67/2008, de 19 de junio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato](http://gestiona.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&idnorma=6166&word=S&wordperfect=N&pdf=S) , B.O.C.M. Núm. 152 de VIERNES 27 DE JUNIO DE 2008, páginas 60 a 62 en formato pdf.  
A nivel estatal  [Real Decreto 1467/2007, de 2 de noviembre, por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas](http://www.boe.es/buscar/act.php?id=BOE-A-2007-19184#fisica)   
  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver  [Contenidos currículo con enlaces a recursos: Física 2º Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/contenidoscurriculoenlacesrecursosfisica2bachillerato)   
  
Este currículo deja de estar en vigor en 2016/2017 según calendario de implantación LOMCE, ver  [currículo Física 2º Bachillerato (LOMCE)](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce)   

Ver [comparación currículo Física 2º Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/comparacion-curriculo-fisica-2-bachillerato)

## Introducción
Esta materia requiere conocimientos incluidos en Física y Química.  
La Física contribuye a comprender la materia desde la escala más pequeña (átomos, partículas, etcétera), hasta la más grande, en el estudio del universo. Los últimos siglos han presenciado un gran desarrollo de las ciencias físicas que ha supuesto a su vez un gran impacto en la vida de los seres humanos. Así, nuestra sociedad dispone de industrias enteras basadas en las contribuciones de la Física que se proyectan sobre múltiples ámbitos de aplicación, como las telecomunicaciones, la instrumentación médica, las tecnologías de la información y la comunicación, etcétera. La Física ha tenido influencia en campos diversos, tales como el cambio social, el desarrollo de las ideas o el estudio del medio ambiente.  
Esta materia tiene carácter formativo y preparatorio. Las ciencias físicas, al igual que otras disciplinas científicas, constituyen un elemento fundamental de la cultura de nuestro tiempo, cultura que incluye no solo aspectos humanísticos, sino que participa también los conocimientos científicos y de sus implicaciones sociales.  
El currículo debe incluir aspectos como las complejas interacciones entre Física, tecnología, sociedad y ambiente, salir al paso de una imagen empobrecida de la ciencia, y contribuir a que los alumnos se familiaricen con la naturaleza de la actividad científica y tecnológica.  
Por lo que se refiere a su carácter preparatorio, debe incluir una gama de contenidos y enfoques que permitan abordar con éxito estudios posteriores, dado que, por su condición de disciplina básica, la Física forma parte de todas las enseñanzas superiores de carácter científico-tecnológico y de un número importante de familias de Formación Profesional de Grado Superior.  
Los contenidos comunes iniciales están destinados a familiarizar a los alumnos con las estrategias básicas de la actividad científica. El carácter transversal de estos contenidos iniciales debe ser tenido en cuenta en el desarrollo de toda la materia. El resto de los contenidos se estructura en tres grandes ramas: Mecánica, Electromagnetismo y Física moderna. Se profundiza en Mecánica con el estudio de la gravitación universal, que permitió unificar los fenómenos terrestres y celestes; se estudian también las vibraciones y las ondas en el movimiento de muelles, cuerdas, sonido, etcétera, que ponen de manifiesto la potencia de la mecánica para explicar el comportamiento de la materia. En el ámbito del Electromagnetismo se estudian los campos eléctricos y magnéticos, tanto variables como constantes, y, desde esta perspectiva, se introduce la cuestión de la  
naturaleza de la luz y se consideran los fundamentos de la Óptica Física y de la Óptica geométrica.  
La ciencia experimental es una actividad humana que comporta procesos de construcción del conocimiento sobre la base de la observación, el razonamiento y la experimentación. De acuerdo con la propia naturaleza de la Física, la simulación, en la medida de lo posible, del trabajo científico por parte de los alumnos constituye una valiosa orientación metodológica.  
Un correcto desarrollo de los contenidos precisa generar escenarios atractivos y motivadores para los alumnos, introducirlos, desde una perspectiva histórica, en diferentes hechos de especial trascendencia científica así como conocer la biografía científica de los principales investigadores que propiciaron la evolución y el desarrollo de la Física. Las lecturas divulgativas, las experiencias y las prácticas de laboratorio familiarizarán al alumnado, de un modo realista, con los métodos de la ciencia y con la problemática del quehacer científico.  
Por último, no hay que olvidar la utilización de las metodologías específicas que las tecnologías de la información y comunicación ponen al servicio de alumnos y profesores, ampliando los horizontes del conocimiento y facilitando su concreción en el aula o en el laboratorio.  

## Objetivos
La enseñanza de la Física en el Bachillerato tendrá como finalidad contribuir a desarrollar en el alumnado las siguientes capacidades:  
1. Adquirir y utilizar con autonomía conocimientos básicos de la Física, así como las estrategias empleadas en su construcción.  
2. Comprender los principales conceptos y teorías de la Física, su articulación en cuerpos coherentes de conocimiento y su vinculación a problemas de interés.  
3. Familiarizarse con el diseño y realización de experimentos físicos, utilizando instrumental básico de laboratorio, de acuerdo con las normas de seguridad de las instalaciones.  
4. Expresar con propiedad mensajes científicos orales y escritos, así como interpretar diagramas, gráficas, tablas, expresiones matemáticas y otros modelos de representación.  
5. Utilizar de manera habitual las tecnologías de la información y la comunicación para realizar simulaciones, tratar datos, y extraer y utilizar información de diferentes fuentes, evaluar su contenido, fundamentar los trabajos y adoptar decisiones.  
6. Resolver problemas que se planteen en la vida cotidiana, seleccionando y aplicando los conocimientos físicos apropiados.  
7. Comprender las complejas interacciones actuales de la Física con la tecnología, la sociedad y el ambiente, valorando la necesidad de preservar el medio ambiente y de trabajar para lograr un futuro sostenible y satisfactorio para el conjunto de la humanidad.  
8. Comprender que el desarrollo de la Física supone un proceso complejo y dinámico, con continuos avances y modificaciones, que ha realizado grandes aportaciones a la evolución cultural de la humanidad y que su aprendizaje requiere una actitud abierta y flexible frente a diversas opiniones.  
9. Reconocer los principales retos a los que se enfrenta la investigación en este campo de la ciencia.  
  
## Contenidos

### 1. Contenidos comunes.
— Utilización de estrategias básicas del trabajo científico: Planteamiento de problemas y reflexión sobre el interés de los mismos, formulación de hipótesis, estrategias de resolución, diseños experimentales y análisis de resultados y de su fiabilidad.  
— Búsqueda y selección de información; comunicación de resultados utilizando la terminología adecuada.  

### 2. Interacción gravitatoria.
— De las Leyes de Kepler a la Ley de la gravitación universal.  
Momento de una fuerza respecto de un punto y momento angular. Fuerzas centrales y fuerzas conservativas. Energía potencial gravitatoria.  
— La acción a distancia y el concepto físico de campo: El campo gravitatorio. Magnitudes que lo caracterizan: Intensidad de campo y potencial gravitatorio.  
— Campo gravitatorio terrestre. Determinación experimental de g. Movimiento de satélites y cohetes.  

### 3. Vibraciones y ondas.
— Movimiento oscilatorio: Movimiento vibratorio armónico simple. Elongación, velocidad, aceleración. Estudio experimental de las oscilaciones de un muelle. Dinámica del movimiento armónico simple. Energía de un oscilador armónico.  
— Movimiento ondulatorio. Tipos de ondas. Magnitudes características de las ondas. Ecuación de las ondas armónicas planas. Aspectos energéticos.  
— Principio de Huygens: Reflexión y refracción. Estudio cualitativo de difracción e interferencias. Ondas estacionarias. Ondas sonoras. Contaminación acústica: Sus fuentes y efectos.  
— Aplicaciones de las ondas al desarrollo tecnológico y a la mejora de las condiciones de vida. Impacto en el medio ambiente.  

### 4. Interacción electromagnética.
— Campo eléctrico. Magnitudes que lo caracterizan: Intensidad de campo y potencial eléctrico. Teorema de Gauss. Aplicación a campos eléctricos creados por un elemento continuo: Esfera, hilo y placa.  
— Magnetismo natural e imanes. Relación entre fenómenos eléctricos y magnéticos. Campos magnéticos creados por corrientes eléctricas. Fuerzas sobre cargas móviles situadas en campos magnéticos. Ley de Lorentz. Interacciones magnéticas entre corrientes rectilíneas. Experiencias con bobinas, imanes, motores, etcétera. Analogías y diferencias entre campos gravitatorio, eléctrico y magnético.  
— Inducción electromagnética. Leyes de Faraday y de Lenz. Producción de energía eléctrica, impacto y sostenibilidad. Energía eléctrica de fuentes renovables.  
— Aproximación histórica a la síntesis electromagnética de Maxwell.  

### 5. Óptica.
— Controversia histórica sobre la naturaleza de la luz: Los modelos corpuscular y ondulatorio. La naturaleza electromagnética de la luz: Espectro electromagnético y espectro visible. Variación de la velocidad de la luz con el medio. Fenómenos producidos con el cambio de medio: Reflexión, refracción, absorción y dispersión.  
— Óptica geométrica. Comprensión de la visión y formación de imágenes en espejos y lentes delgadas. Pequeñas experiencias con las mismas. Construcción de algún instrumento óptico.  
— Estudio cualitativo de la difracción, el fenómeno de interferencias y la dispersión. Aplicaciones médicas y tecnológicas.  

### 6. Introducción a la Física moderna.
— La crisis de la Física clásica. Principios fundamentales de la relatividad especial. Repercusiones de la teoría de la relatividad. Variación de la masa con la velocidad y equivalencia entre masa y energía.  
— Efecto fotoeléctrico y espectros discontinuos: Insuficiencia de la Física clásica para explicarlos. Hipótesis de Planck. Cuantización de la energía. Hipótesis de De Broglie. Dualidad onda corpúsculo. Relaciones de indeterminación. Aportaciones de la Física moderna al desarrollo científico y tecnológico.  
— Física nuclear: Composición y estabilidad de los núcleos. Energía de enlace. Radiactividad. Tipos, repercusiones y aplicaciones. Reacciones nucleares de fisión y fusión, aplicaciones y riesgos.  

## Criterios de evaluación
1. Utilizar correctamente las unidades, así como los procedimientos apropiados para la resolución de problemas.  
2. Analizar situaciones y obtener información sobre fenómenos físicos utilizando las estrategias básicas del trabajo científico.  
3. Valorar la importancia de la Ley de la gravitación universal. Aplicarla a la resolución de problemas de interés: Determinar la masa de algunos cuerpos celestes, estudio de la gravedad terrestre y del movimiento de planetas y satélites. Calcular la energía que debe poseer un satélite en una órbita determinada, así como la velocidad con la que debió ser lanzado para alcanzarla.  
4. Construir un modelo teórico que permita explicar las vibraciones de la materia y su propagación. Deducir, a partir de la ecuación de una onda, las magnitudes que intervienen: Amplitud, longitud de onda, período, etcétera. Aplicar los modelos teóricos a la interpretación de diversos fenómenos naturales y desarrollos tecnológicos.  
5. Explicar las propiedades de la luz utilizando los diversos modelos e interpretar correctamente los fenómenos relacionados con la interacción de la luz y la materia.  
6. Valorar la importancia que la luz tiene en nuestra vida cotidiana, tanto tecnológicamente (instrumentos ópticos, comunicaciones por láser, control de motores) como en química (fotoquímica) y medicina (corrección de defectos oculares).  
7. Justificar algunos fenómenos ópticos sencillos de formación de imágenes a través de lentes y espejos: Telescopios, microscopios, etcétera.  
8. Usar los conceptos de campo eléctrico y magnético para superar las dificultades que plantea la interacción a distancia.  
9. Calcular los campos creados por cargas y corrientes rectilíneas y las fuerzas que actúan sobre las mismas en el seno de campos uniformes, justificando el fundamento de algunas aplicaciones: Electroimanes, motores, tubos de televisión e instrumentos de medida.  
10. Explicar la producción de corriente mediante variaciones del flujo magnético, utilizar las Leyes de Faraday y Lenz, indicando de qué factores depende la corriente que aparece en un circuito.  
11. Conocer algunos aspectos de la síntesis de Maxwell como la predicción y producción de ondas electromagnéticas y la integración de la óptica en el electromagnetismo.  
12. Conocer los principios de la relatividad especial y explicar algunos fenómenos como la dilatación del tiempo, la contracción de la longitud y la equivalencia masa-energía.  
13. Conocer la revolución científico-tecnológica que, con origen en la interpretación de espectros discontinuos o el efecto fotoeéctrico entre otros, dio lugar a la Física cuántica y a nuevas tecnologías.  
14. Aplicar la equivalencia masa-energía para explicar la energía de enlace y la estabilidad de los núcleos, las reacciones nucleares, la radiactividad y sus múltiples aplicaciones y repercusiones. Conocer las repercusiones energéticas de la fisión y fusión nuclear.  

