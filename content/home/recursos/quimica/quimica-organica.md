
# Química Orgánica

Aquí se ponen recursos asociados a mecanismos y reacciones. La  [formulación y nomenclatura](/home/recursos/quimica/formulacion)  de orgánica está aparte.  
En el apartado de  [libros](/home/recursos/libros)  también se ponen referencias a libros generales de orgánica
   * [Visualize Organic Chemistry](https://visualizeorgchem.com/)  Interactive 3D visualizations of organic reaction mechanisms, helping students and educators bridge the gap between written and real structures in organic chemistry.  
   * "La Química Orgánica Transparente" LECCIONES  
   [http://www.uam.es/departamentos/ciencias/qorg/docencia_red/qo/l00/lecc.html](http://www.uam.es/departamentos/ciencias/qorg/docencia_red/qo/l00/lecc.html)  
   UAM. Ernesto Brunet Catedrático de Química Orgánica
   * Recursos educativos de química orgánica. Universidad de Granada  
   [http://www.ugr.es/~quiored/](http://www.ugr.es/~quiored/)  
   Contenidos teórico-prácticos, Laboratorio de Química Orgánica 
   * Facultad de Quimica. Quimica Organica. Teoría  
   [http://organica1.org/teoria.html](http://organica1.org/teoria.html)  
   UNAM Universidad Nacional Autónoma de México. Dr. Carlos Rius Alonso
   * QUÍMICA ORGÁNICAENTORNO EDUCATIVO INTERACTIVO PARA EL APOYO DEL PROCESO DE ENSEÑANZA/APRENDIZAJE DE LA QUÍMICA ORGÁNICABASADO EN Jmol , JME y Hot Potatoes  
   [http://liceoagb.es/quimiorg/index.html](http://liceoagb.es/quimiorg/index.html)  
   Web creada y mantenida por Agustín García Barneto 
   * Organic Syntheses  
   [http://www.orgsyn.org/](http://www.orgsyn.org/)  
   Publicación científica con procedimientos detallados y contrastados de síntesis orgánica. Desde 1998 publicaciones (incluyendo las antiguas desde 1921) con acceso libre en internet
   * Química orgánica  
   [http://perso.wanadoo.es/oyederra/areas/qorganica.htm](http://perso.wanadoo.es/oyederra/areas/qorganica.htm)  
   Recopilación enlaces 
   *  [http://www.masterorganicchemistry.com/organic-1/](http://www.masterorganicchemistry.com/organic-1/)  
   Página con blog/posts sobre orgánica de libre acceso, además de fichas para estudio que son de pago
   * Organic Chemistry Animations  
   [http://www.chemtube3d.com/Main Page.html ](http://www.chemtube3d.com/Main%20Page.html)  
   University of Liverpool, cc-by-nc-sa
   *  [http://www.quimicaorganica.org/](http://www.quimicaorganica.org/)  
   Químicaorgánica.org es el resultado de la experiencia obtenida en la enseñanza de Química Orgánica a nivel Universitario, durante los últimos 13 años, en la Academia Minas de Oviedo.© 2007, Germán Fernandez, quimicaorganica.org. Todos los derechos reservados.
   * Intro Organic Chemistry. University of Alberta  
   [http://www.chem.ualberta.ca/~vederas/Chem_164/index.html](http://www.chem.ualberta.ca/~vederas/Chem_164/index.html)  
   Dr. John C. Vederas 
   * Understanding Chemistry  
   BASIC ORGANIC CHEMISTRY MENU  [http://www.chemguide.co.uk/orgmenu.html](http://www.chemguide.co.uk/orgmenu.html)  
   PROPERTIES OF ORGANIC COMPOUNDS MENU  [http://www.chemguide.co.uk/orgpropsmenu.html](http://www.chemguide.co.uk/orgpropsmenu.html)  
   ORGANIC MECHANISMS MENU  [http://www.chemguide.co.uk/mechmenu.html](http://www.chemguide.co.uk/mechmenu.html)  
   © Jim Clark 2000
   * Universidad de La Laguna. Facultad de química Departamento de Química Orgánica.Química Orgánica 2º 2009-2010  
   Profesor Antonio Galindo  
   [http://agalindo.webs.ull.es/](http://agalindo.webs.ull.es/) 
   * Química Orgánica  
   [http://www.quimicaorganica.net/](http://www.quimicaorganica.net/)  
   Copyright ©2005-2015, Germán Fernández.
   * Apuntes Química Orgánica  
   [http://www.sinorg.uji.es/apuntes.htm](http://www.sinorg.uji.es/apuntes.htm)  
   © 2011 Grupo de Síntesis Orgánica. Universidad Jaume I
   * QUÍMICA. 2º de Bachillerato.Unidad didáctica nº 9: Química orgánica  
   [http://fresno.pntic.mec.es/~fgutie6/quimica2/ArchivosHTML/Unidad_9.htm](http://fresno.pntic.mec.es/~fgutie6/quimica2/ArchivosHTML/Unidad_9.htm)  
   Teoría y ejercicios. © Fco. Javier Gutiérrez Rodríguez
   * OpenCourseWare Universidad de Zaragoza. Química Orgánica para ingenieros.  
   [http://ocw.unizar.es/ocw/course/view.php?id=18](http://ocw.unizar.es/ocw/course/view.php?id=18)  
    ©2015 Universidad de Zaragoza, autores y colaboradores. cc-by-nc-sa
   * Curso 0 UNED  
   [http://ocw.innova.uned.es/quimicas/](http://ocw.innova.uned.es/quimicas/)  
   © 2008 Universidad Nacional de Educación a DistanciaQUÍMICA DEL CARBONO.  
   Autora: Mª José Morcillo Ortega  
   Ficha 12 Compuestos del Carbono. Su representación  
   Ficha 13 Hidrocarburos  
   Ficha 14 Isomería  
   Ficha 15 Principales funciones orgánicas  
   Ficha 16 Formulación y nomenclatura orgánica  
   Ficha 17 Principales Tipos de Reacciones Orgánicas 
   * La química del carbono  
   [http://www.juntadeandalucia.es/averroes/recursos_informaticos/concurso1998/accesit8/main.htm](http://www.juntadeandalucia.es/averroes/recursos_informaticos/concurso1998/accesit8/main.htm)  
   Introducción histórica  
   Enlaces del carbono  
   Cadenas carbonadas  
   Isomería  
   Grupos funcionales.  
   Series homólogas  
   Las reacciones en Química Orgánica  
   Autor: Abelardo López Lorente. Accésit en el Concurso de Programas Informáticos Educativos y Páginas Web Educativas, 1998 
   * Química orgánica, Universidad de Sevilla [http://ocwus.us.es/quimica-organica/](http://ocwus.us.es/quimica-organica/) Copyright 2007, Autores y Colaboradores. cc-by-nc-sa
   *  [https://quizlet.com/18119245/organic-chemistry-reagents-flash-cards/](https://quizlet.com/18119245/organic-chemistry-reagents-flash-cards/) 
   * Organic Chemistry Portal  
   [http://www.organic-chemistry.org/](http://www.organic-chemistry.org/) 
   * [EJERCICIOS DE AUTO-EVALUACIÓN DE QUÍMICA ORGÁNICA. Asignatura: Química Orgánica(Codi: 820531) Escola d’Enginyeria de Barcelona Est (EEBE). Enero 2019 - upc.edu](https://upcommons.upc.edu/bitstream/handle/2117/191420/ejercicios_de_auto-evaluacion_de_quimica_organica_2019_iribarrenarmelin.pdf?sequence=1&isAllowed=y) PDF de 41 páginas  
   * [Química Orgánica (200 ejercicios resueltos); Márquez Salamanca, Cecilio; Universidad de Alicante. Departamento de Química Orgánica](http://rua.ua.es/dspace/handle/10045/6984) 
   * Mapas de reacciones orgánicas   
   cc-by-nc-nd  
   [Aromatic Chemistry Reactions Map - compoundchem.com (2014/02/17)](http://www.compoundchem.com/2014/02/17/organic-chemistry-reaction-map/)  
   ![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2014/02/Organic-reactions-map-2020-web.png?resize=768%2C543&ssl=1)  
   [Aromatic Chemistry Reactions Map - compoundchem.com (2014/02/23)](http://www.compoundchem.com/2014/02/23/aromatic-chemistry-reactions-map/)  
   ![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2014/02/Aromatic-Functional-Group-Interconversions-2015.png?resize=768%2C543&ssl=1)  
   [Reacciones de alquenos - wikipedia](https://es.wikipedia.org/wiki/Reacciones_de_alquenos)  
   ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Reacciones_alquenos_A.png/600px-Reacciones_alquenos_A.png)  
   
   [twitter jorgemo1999/status/1327581713271312384](https://twitter.com/jorgemo1999/status/1327581713271312384)  
   El origen de los nombres de los aminoácidos #hilo ...  
   Glicina. Por su sabor dulce. El prefijo gli- o glu- significan dulce. Del griego “glykýs”, dulce. Como glucosa por ejemplo.  
   Arginina. Fue aislado como cristales de color plata brillante. La palabra “árgyros” es plata en griego, de ahí el prefijo argi-....  
   
   [ Amino acid names and parlor games: from trivial names to a one-letter code, amino acid names have strained students’ memories. Is a more rational nomenclature possible? - willey.com](https://iubmb.onlinelibrary.wiley.com/doi/pdf/10.1016/S0307-4412(97)00167-2)  
 
   [Chemtymology. On the Etymology of Chemical Names. Asparagine, Aspartate, Glutamine and Glutamate](https://chemtymology.co.uk/2019/03/02/asparagine-aspartate-glutamine-and-glutamate/)   

   * [Virtual Textbook of Organic Chemistry - chemistry.msu.edu](https://www2.chemistry.msu.edu/faculty/reusch/virttxtjml/intro1.htm#contnt)  
   * [Chemistry 350: Organic Chemistry I - chem.libretexts.org](https://chem.libretexts.org/Courses/Athabasca_University/Chemistry_350%3A_Organic_Chemistry_I)  
   * [Introductory Chemistry – 1st Canadian Edition. Chapter 16. Organic Chemistry - opentextbc.ca](https://opentextbc.ca/introductorychemistry/part/chapter-16-organic-chemistry/)  
   * [FlexBooks CK-12 Chemistry - Intermediate. 25.0 Organic Chemistry - ck12.org](https://www.ck12.org/book/ck-12-chemistry-intermediate/section/25.0/)  
   * [Organic Chemistry Summary Sheets - masterorganicchemistry.com](https://www.masterorganicchemistry.com/summary-sheets/)  
     [Organic Chemistry Practice Questions - masterorganicchemistry.com](https://www.masterorganicchemistry.com/organic-chemistry-practice-problems/)     

###  Papel para escribir compuestos aromáticos
 [twitter quimicaPau/status/1079344163194368000](https://twitter.com/quimicaPau/status/1079344163194368000)  
 Queridos reyes magos. Necesito este papel para que mis bencenos dejen de ser patatas. Gracias  
 [![](https://pbs.twimg.com/media/DvqZysGWkAA8vca.jpg "") ](https://pbs.twimg.com/media/DvqZysGWkAA8vca.jpg)  
 
 [twitter rcabreratic/status/1079458211135176705](https://twitter.com/rcabreratic/status/1079458211135176705)  
 No hace falta que esperes a SSMM, lo puedes descargar gratuitamente e imprimir las copias que quieras de aquí: 😉  
[http://www.open.edu/openlearn/ocw/mod/resource/view.php?id=27292](http://www.open.edu/openlearn/ocw/mod/resource/view.php?id=27292)  
 [twitter rcabreratic/status/1079458211135176705](https://twitter.com/rcabreratic/status/1079458211135176705)   
 Leticia en el enlace que te dejo puedes crear tus propias plantillas grosor y longitud (recomiendo entre 8-10mm)  
 [http://gridzzly.com/](http://gridzzly.com/)  
 
 [Papel hexagonal – Cuaderno de química orgánica  - quimicafacil.net](https://quimicafacil.net/varios/papel-hexagonal-cuaderno-de-quimica-organica)  

## Aplicaciones para móvil
Se incluyen en  [apps química](/home/recursos/quimica/apps-quimica) , donde hay específicas de química orgánica

## Ejercicios
[QUÍMICA ORGÁNICA. 250 EJERCICIOS RESUELTOS. CECILIO MÁRQUEZ SALAMANCA. Profesor Titular UNIVERSIDAD DE ALICANTE, 2008](https://rua.ua.es/dspace/bitstream/10045/6984/7/LIBRO1_1.pdf)  

 [twitter Biochiky/status/1260264644993744898](https://twitter.com/Biochiky/status/1260264644993744898)  
 Entre que no cambio fechas de exámenes y estos ejercicios, creo que me he ganado el odio total por parte de mis alumnos, pero...¡¿y lo bonito que son?! Si yo los sufro en las oposiciones, ellos también  
![](https://pbs.twimg.com/media/EX1cApgXkAEhTgm?format=jpg)  
![](https://pbs.twimg.com/media/EX1cApZWAAAQ-Z8?format=jpg)  
 
 [twitter Biochiky/status/1518200558489849856](https://twitter.com/Biochiky/status/1518200558489849856)  
 ¿Me estoy ganado a pulso el odio de mis alumnos? No tengo pruebas pero tampoco dudas  
¿Será que echo de menos las oposiciones?  
![](https://pbs.twimg.com/media/FRG7JJ2XwAAd8bR?format=png)  
![](https://pbs.twimg.com/media/FRG7JJ2X0AA_rBn?format=png)  
![](https://pbs.twimg.com/media/FRG7JJ2X0AA_rBn?format=png)  

[Organic Chemistry Memes](https://es-la.facebook.com/ochemmemes)  
![](https://scontent-mad2-1.xx.fbcdn.net/v/t31.18172-8/15129084_1696329777345823_915432496763939879_o.jpg?_nc_cat=108&ccb=1-7&_nc_sid=9267fe&_nc_ohc=vS-q9TZypmgAX9Lq6KP&_nc_ht=scontent-mad2-1.xx&oh=00_AfD-blTCST46kh6kKktvVZHSLB4-BsCMtWEV4E2GBQUAuQ&oe=64920835)  

 
