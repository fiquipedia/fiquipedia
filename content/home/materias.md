
# Materias
 
* **[Materias de ESO](/home/materias/eso)**  
* **[Materias de Bachillerato](/home/materias/bachillerato)**  
* **[Materias de FP Básica](/home/materias/fpbasica)**  
* Materias de Educación de Adultos (**pendiente**, relacionado con ámbitos y materias de ciencias en FP Básica, diversificación curricular, pruebas de acceso a grado medio, pruebas para el graduado en ESO ...)  

Se crean subpáginas con esa agrupación de materias, que surge de cómo se organizan las materias que puede impartir un profesor de la especialidad de Física y Química.  
En principio las materias que se pueden impartir están limitadas por ley y son las denominadas afines, aunque realmente se pueden contemplar más  
Las materias oficialmente afines se pueden consultar en estas dos páginas  
- [Especialidades y materias afines. Tabla especialidades - materias](https://sites.google.com/site/especialidadesymateriasafines/home/especialidades-y-materias-afines-secundaria/tabla-especialidades-materias)  
- [Especialidades y materias afines. Tabla materia - especialidades](https://sites.google.com/site/especialidadesymateriasafines/home/especialidades-y-materias-afines-secundaria/tabla-materia-especialidades)  

* **[Ámbitos](/home/materias/ambitos)**  

