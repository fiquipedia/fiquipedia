
# Recursos Divulgacion Científica

Aunque me intento centrar en física y química, en general puede interesar la divulgación científica en general  
De nuevo esta página es un borrador, se trata de empezar a tener algo escrito  
Hay recursos asociados a divulgación muy variados, que a veces se combinan:  
  
- [Páginas / blogs personales](/home/recursos/paginas-blog-personales)   
- [Páginas ciencia / física / química](/home/recursos/recopilatorios-enlaces-recursos)   
- [Libros](/home/recursos/libros) y recursos [lectura](/home/recursos/lectura). Pueden ser de divulgación, aunque la ciencia ficción (en especial "la dura") también puede ser divulgativa  
- Charlas. Hay algunas que se mencionan como  [actividades fuera del centro](/home/recursos/recursos-de-actividades-fuera-del-centro) , pero hay muchas más, por ejemplo NAUKAS 2014 en Bilbao  [Bilbao 2014 Archives - Naukas](http://naukas.com/categorias/eventos/bilbao-2014/)   
- [Recursos historia de la ciencia](/home/recursos/historia-de-la-ciencia)  
- Puede haber recursos asociados a la transversalidad: intentar relacionar la física y química con otras disciplinas

ESTO ES FÍSICA - Dpto Física y Química IES Valle del Saja  
[![](https://img.youtube.com/vi/tLj-HV-XY8g/0.jpg)](https://www.youtube.com/watch?v=tLj-HV-XY8g "ESTO ES FÍSICA - Dpto Física y Química IES Valle del Saja")  
Documental que recrea en 16 capítulos las relaciones de la física con cada uno de los departamentos didácticos de un centro de secundaria. Un intento de enfocar la física partiendo de distintas materias.  


Hay cosas asociadas a ciencia de manera muy indirecta que pueden considerarse divulgación si acercan conceptos científicos al público general que no lo conocería de otra manera  
- Programas como [Órbita Laika](https://www.rtve.es/television/orbita-laika/)  
- Actividades específicas 

Ejemplo Halloween  
[twitter Elzo_/status/660915861809704962](https://twitter.com/Elzo_/status/660915861809704962)  
Cementerio de ideas científicas muertas  
![](https://pbs.twimg.com/media/CSwLd-EWoAA9HJF?format=jpg)  
![](https://pbs.twimg.com/media/CSwLeQsWsAUAmw1?format=jpg)  

Asociación con música / canciones / bailes:  
- Ver asociado [recursos Tabla Periódica / elementos químicos](/home/recursos/quimica/recursos-tabla-periodica) el vídeo "Chemical party" de la unión europea  
- [Barber's Science](https://www.youtube.com/c/BarbersScience)  
Barber’s Science es un proyecto de divulgación dedicado a recuperar las composiciones del gran John Moretropier, químico, músico y divulgador de los años 20 y 30 del siglo XX. El interdisciplinar grupo, formado por un bioquímico -Diego Salagre-, un galguero -Alberto Pérez-, un musicólogo -Ángel Estero- y un ingeniero de cables -Francisco Jesús Martínez- nace como escisión de los niños cantores de Viena actuando, entre otros, para Su Santidad, su alteza real Juan Froilán de Todos los Santos Urdangarín de Borbón y Rosalía. En la actualidad son el único cuarteto vocal que sigue cantando después de muertos. O no.  

Descomposición del peso en un plano inclinado

[twitter microBIOblog/status/1529106901895716865](https://twitter.com/microBIOblog/status/1529106901895716865)  
1/8 La semana de la ciencia también se celebra en primavera, al menos en #Pamplona. Pasea por sus calles y disfruta de "La ciencia en la calle". Una idea de @museoCCNN_unav @Zientzia  
Comenzamos con "La ciencia en una vacuna"  
![](https://pbs.twimg.com/media/FTh6MZFXsAE7AOx?format=jpg)  
2/8"La ciencia en una hoja de árbol"  
![](https://pbs.twimg.com/media/FTh65zuWYAQSPrf?format=jpg)  
3/8 "La ciencia en un código de barras"  
![](https://pbs.twimg.com/media/FTh7aChWAAIyZQ3?format=jpg)  
4/8 "La ciencia en un yogur"  
![](https://pbs.twimg.com/media/FTh71jfWAAAxC5t?format=jpg)  
5/8 "La ciencia en el agua del grifo"  
![](https://pbs.twimg.com/media/FTh8SuxWIAUIqP4?format=jpg)  
6/8 "La ciencia en una placa de cocina"  
![](https://pbs.twimg.com/media/FTh8vVnWAAEQNnk?format=jpg)  
7/8 Te puedes descargar la colección completa en castellano y euskera en:  
[Infográficos sobre ciencia - unav.edu](https://museodeciencias.unav.edu/actividades/ciencia-en-la-calle)  
8/8 Y otras muchas actividades desde el 24 de mayo al 7 de junio!  
La ciencia en la calle, charlas en los bares y talleres para familias en el día del medio ambiente:  
[Ciencia en la calle - unav.edu](https://museodeciencias.unav.edu/actividades/ciencia-calle-)  


Otra idea son calendarios  
[CALENDARIO CIENTÍFICO ESCOLAR - cscic](http://www.igm.ule-csic.es/calendario-cientifico)  
[Ya puedes descargar el Calendario científico escolar 2022](https://www.csic.es/es/actualidad-del-csic/ya-puedes-descargar-el-calendario-cientifico-escolar-2022)  
