# Recursos tecnología

Dentro de la especialidad de física y química no imparte normalmente tecnología salvo en lo asociado a diversificación.  

Hay contenidos comunes como termodinámica en 1º Bachillerato de Física y Química y en 2º Bachillerato de Tecnología e Ingeniería



* [areatecnlogia.com](https://www.areatecnologia.com/)  
Recursos, Conocimientos y Temas de Tecnologia  
* [AULA DE TECNOLOGÍAS. Blog de Tecnología e Ingeniería](http://auladetecnologias.blogspot.com.es/)  
cc-by-nc-sa María Amelia Tierno López  
* [Pelandintecno-Tecnología ESO. Blog de Tecnología de Pedro Landín. Sagrado Corazón de Placeres](http://pelandintecno.blogspot.com/)  
* [GearSketch, un simulador de engranajes para ese ingeniero mecánico que llevas dentro - microsiervos.com](http://www.microsiervos.com/archivo/ingenieria/gearsketch-simulador-engranajes.html)  
![](https://www.microsiervos.com/img/GearSketch.jpg)  
[http://www.gearsket.ch/](http://www.gearsket.ch/)  
* [Picuino - Recursos educativos libres para Tecnología en Educación Secundaria.](https://www.picuino.com/)  
Carlos Pardo Martín.
* [Planar mechanism simlation oluchukwu96](https://oluchukwu96.github.io/Kinematic-Simulator/)  
* [Mechanical Linkage Simulator](https://www.mechlinkages.com/)  

[aprendemostecnologia.org](https://aprendemostecnologia.org/)  
