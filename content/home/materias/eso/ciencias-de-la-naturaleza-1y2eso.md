
# 1º y 2º ESO Ciencias de la Naturaleza

Realmente es un área que engloba a Ciencias de la Naturaleza en 1º ESO y 2º ESO, tanto Física y Química como Biología y Geología en 3º y 4º ESO  

## Introducción
En la sociedad actual, la ciencia es un instrumento indispensable para comprender el mundo que nos rodea y los avances tecnológicos que se producen continuamente y que poco a poco van trasformando nuestras condiciones de vida, así como para desarrollar actitudes responsables sobre aspectos ligados a la vida, a la salud, a los recursos naturales y al medio ambiente. Por ello, los conocimientos científicos se integran en el saber humanístico, que debe formar parte de la cultura básica de todos los ciudadanos.  
  
Los conocimientos sobre ciencias de la naturaleza, adquiridos en la Educación Primaria deben ser afianzados y ampliados durante la etapa de secundaria obligatoria, incorporando también actividades prácticas obligatorias, propias del trabajo del naturalista y de la Física y química, enfocadas a la búsqueda de explicaciones. Las actividades prácticas deben convertirse en auténticos ¿contenidos prácticos¿, imprescindibles en estas materias.  
  
Los contenidos que se trabajan en esta materia deben estar orientados a la adquisición por parte del alumnado de las bases propias de la cultura científica, en especial en la unidad de los fenómenos que estructuran el mundo natural, en las leyes que los rigen y en la expresión matemática de esas leyes, de lo que se obtiene una visión racional y global de nuestro entorno que sirva de base para poder abordar los problemas actuales relacionados con la vida, la salud, el medio y las aplicaciones tecnológicas.  
  
En el currículo que se presenta, se han englobado en la materia de las Ciencias de la Naturaleza las materias de Biología y geología, y Física y química en los dos primeros cursos de esta etapa, con lo que se crea una unidad curricular y se mantiene así una aproximación de conjunto al conocimiento de los fenómenos naturales, integrando conceptos y subrayando las relaciones y conexiones entre los mismos. Se pretende que el alumno descubra la existencia de marcos conceptuales y procedimientos de indagación comunes a los diferentes ámbitos del saber científico. En tercer y cuarto curso, dada la madurez del alumno, se separan las dos materias para profundizar de un modo más especializado en los contenidos.  
  
Los contenidos seleccionados en los diferentes cursos obedecen a un orden creciente de complejidad y, por tanto, van asociados a la formación del alumnado al que van destinados. Los procedimientos que se introducen son aspectos del aprendizaje estrechamente relacionados con los conceptos y, por lo tanto, verdaderos contenidos prácticos del currículo. También se considera preciso desarrollar, de forma transversal, el método científico de estudio de la naturaleza, así como de las implicaciones que de él se infieren con la tecnología y la sociedad.  
  
El estudio de la Tierra en el Universo configura el primer curso. Tras comenzar con una visión general del Universo se sitúa en él a la Tierra como planeta y se estudian las características de la materia que la constituye para seguir con la introducción al conocimiento de la geosfera e iniciar el estudio de la diversidad de los seres vivos que en ella habitan.  
  
En el segundo curso es la Energía el núcleo principal, en torno al cual se estructuran los contenidos, sus diversas formas de transferencia, estudiando el calor, la luz y el sonido, así como los problemas asociados a la obtención y uso de los recursos energéticos. Se aborda la transferencia de energía interna que se produce en la Tierra, para estudiar a continuación las características funcionales de los seres vivos y las relaciones entre ellos y con el medio físico.  
  
Después de estudiar las Ciencias de la naturaleza desde un punto de vista general, en los cursos tercero y cuarto, con el fin de profundizar en el estudio de aspectos concretos, se considera necesario separar la Física y química, de la Biología y geología, no obstante mantener en común los contenidos relacionados con el método científico y el trabajo experimental.  
  
Los bloques de contenidos de la materia de Física y química se han distribuido de forma asimétrica entre los dos cursos. Así, teniendo en consideración los conocimientos matemáticos que poseen los alumnos, en el tercer curso predominarán los conceptos de Química sobre los de Física, y en cuarto los de Física sobre los de Química, para lograr al final de la etapa un conocimiento compensado y homogéneo de ambas.  
  
En concreto, en tercero se estudia la estructura de la materia macro y microscópicamente, como los principales elementos de la reactividad química. Se hace especial hincapié en la considerable repercusión que esta ciencia tiene en la sociedad actual. La Física que se estudia en este curso desarrolla conceptos energéticos, especialmente relacionados con la electricidad, por ser sencillos y con múltiples aplicaciones en su entorno.  
  
En este mismo curso, la Biología y geología estudia la estructura y función del cuerpo humano que, desde la perspectiva de la educación para la salud, establece la importancia de las conductas saludables y señala la relación de cada sistema orgánico con la higiene y prevención de sus principales enfermedades. Asimismo, se propone una visión integradora del ser humano con su entrono, mediante el estudio de las interacciones e interdependencias entre las personas y el medio ambiente, seguido de contenidos referidos al ciclo geológico y a la materia mineral.  
  
En cuarto curso, se pretende que los alumnos alcancen una preparación científica más general y cultural, suficiente para desenvolverse de manera adecuada en el mundo del siglo XXI.  
  
El currículo de Física, engloba los conceptos y aplicaciones de fuerzas y movimientos, estudiándose además las energías mecánica, calorífica y ondulatoria. La Química aborda, sobre todo, los cambios químicos, así como una introducción de los compuestos del carbono. Por último, el bloque La contribución de la ciencia a un futuro sostenible, permite analizar y tomar posición ante alguno de los grandes problemas globales con los que se enfrenta la humanidad.  
  
En lo referente a Biología y geología, en cuarto curso, se aborda con detalle la dinámica terrestre, con particular insistencia en el paradigma de la tectónica global y la historia del planeta; además, se profundiza en aspectos de Citología; y se introducen la Genética mendeliana y algunos temas relativos al conocimiento de los ecosistemas, y a la detección y prevención de problemas medioambientales.  
  
En todos los cursos se recogen conjuntamente, los contenidos que tiene que ver con las formas de construir la ciencia y de transmitir la experiencia y el conocimiento científico.  
  
A la hora de realizar las programaciones didácticas, se hace imprescindible la coordinación entre las materias de Biología y geología y las de Física y química, y de éstas con Matemáticas.  
  
En lo referente a la metodología, es importante transmitir la idea de que la Ciencia es una actividad en permanente construcción y revisión, con implicaciones con la tecnología y con la sociedad; plantear cuestiones tanto teóricas como prácticas, a través de las cuales el alumno comprenda que uno de los objetivos de la ciencia es dar explicaciones científicas de aquello que nos rodea.  
  
La realización de actividades prácticas adaptadas a cada nivel de enseñanza en la etapa, pondrá al alumno frente al desarrollo real de alguna de las fases del método científico, le proporcionará métodos de trabajo en equipo, le permitirá desarrollar habilidades experimentales y le servirá de motivación para el estudio. Esta formación es indispensable para todos los jóvenes, cualquiera que vaya a ser su orientación futura, pues tendrá que ser aplicada a todos los campos del conocimiento, incluso a los que no son considerados habitualmente como científicos.  
  
Por último, hay que tener presente la inclusión tanto de los temas puntuales, como de los grandes programas actuales que la ciencia está abordando. A este respecto, es importante la búsqueda de información, mediante la utilización de las fuentes adecuadas, sin olvidar las nuevas tecnologías de la información y la comunicación, en la medida en la que los recursos del alumnado y el centro lo permitan, así como su tratamiento organizado y coherente.  
  
Los criterios de evaluación que se establecen se corresponden con los bloques de contenidos que a continuación se indican para cada curso, más aquéllos que los profesores consideren oportunos, de acuerdo también con las programaciones didácticas y con el desarrollo de las actividades prácticas y los valores a los que se pretenda dar prioridad.  


## Contribución de la materia a la adquisición de las competencias básicas
La mayor parte de los contenidos de Ciencias de la naturaleza tiene una incidencia directa en la adquisición de la competencia en el conocimiento y la interacción con el mundo físico. Precisamente el mejor conocimiento del mundo físico requiere el aprendizaje de los conceptos y procedimientos esenciales de cada una de las ciencias de la naturaleza y el manejo de las relaciones entre ellos: de causalidad o de influencia, cualitativas o cuantitativas, y requiere asimismo la habilidad para analizar sistemas complejos, en los que intervienen varios factores. Pero esta competencia también requiere los aprendizajes relativos al modo de generar el conocimiento sobre los fenómenos naturales. Es necesario para ello lograr la familiarización con el trabajo científico, para el tratamiento de situaciones de interés, y con su carácter tentativo y creativo: desde la discusión acerca del interés de las situaciones propuestas y el análisis cualitativo, significativo de las mismas, que ayude a comprender y a acotar las situaciones planteadas, pasando por el planteamiento de conjeturas e inferencias fundamentadas y la elaboración de estrategias para obtener conclusiones, incluyendo, en su caso, diseños experimentales, hasta el análisis de los resultados.  
  
Algunos aspectos de esta competencia requieren, además, una atención precisa. Es el caso, por ejemplo, del conocimiento del propio cuerpo y las relaciones entre los hábitos y las formas de vida y la salud. También lo son las implicaciones que la actividad humana y, en particular, determinados hábitos sociales y la actividad científica y tecnológica tienen en el medio ambiente. En este sentido es necesario evitar caer en actitudes simplistas de exaltación o de rechazo del papel de la tecnociencia, favoreciendo el conocimiento de los grandes problemas a los que se enfrenta hoy la humanidad, la búsqueda de soluciones para avanzar hacia el logro de un desarrollo sostenible y la formación básica para participar, fundamentadamente, en la necesaria toma de decisiones en torno a los problemas locales y globales planteados.  
  
La competencia matemática está íntimamente asociada a los aprendizajes de las Ciencias de la naturaleza. La utilización del lenguaje matemático para cuantificar los fenómenos naturales, para analizar causas y consecuencias y para expresar datos e ideas sobre la naturaleza proporciona contextos numerosos y variados para poner en juego los contenidos asociados a esta competencia y, con ello, da sentido a esos aprendizajes. Pero se contribuye desde las Ciencias de la naturaleza a la competencia matemática en la medida en que se insista en la utilización adecuada de las herramientas matemáticas y en su utilidad, en la oportunidad de su uso y en la elección precisa de los procedimientos y formas de expresión acordes con el contexto, con la precisión requerida y con la finalidad que se persiga. Por otra parte en el trabajo científico se presentan a menudo situaciones de resolución de problemas de formulación y solución más o menos abiertas, que exigen poner en juego estrategias asociadas a esta competencia.  
  
El trabajo científico tiene también formas específicas para la búsqueda, recogida, selección, procesamiento y presentación de la información que se utiliza además en muy diferentes formas: verbal, numérica, simbólica o gráfica. La incorporación de contenidos relacionados con todo ello hace posible la contribución de estas materias al desarrollo de la competencia en el tratamiento de la información y competencia digital. Así, favorece la adquisición de esta competencia la mejora en las destrezas asociadas a la utilización de recursos frecuentes en las materias como son los esquemas, mapas conceptuales, etc., así como la producción y presentación de memorias, textos, etc. Por otra parte, en la faceta de competencia digital, también se contribuye a través de la utilización de las tecnologías de la información y la comunicación en el aprendizaje de las ciencias para comunicarse, recabar información, retroalimentarla, simular y visualizar situaciones, para la obtención y el tratamiento de datos, etc. Se trata de un recurso útil en el campo de las ciencias de la naturaleza y que contribuye a mostrar una visión actualizada de la actividad científica.  
  
La contribución de las Ciencias de la naturaleza a la competencia social y ciudadana está ligada, en primer lugar, al papel de la ciencia en la preparación de futuros ciudadanos de una sociedad democrática para su participación activa en la toma fundamentada de decisiones; y ello por el papel que juega la naturaleza social del conocimiento científico. La alfabetización científica permite la concepción y tratamiento de problemas de interés, la consideración de las implicaciones y perspectivas abiertas por las investigaciones realizadas y la toma fundamentada de decisiones colectivas en un ámbito de creciente importancia en el debate social.  
  
En segundo lugar, el conocimiento de cómo se han producido determinados debates que han sido esenciales para el avance de la ciencia, contribuye a entender mejor cuestiones que son importantes para comprender la evolución de la sociedad en épocas pasadas y analizar la sociedad actual. Si bien la historia de la ciencia presenta sombras que no deben ser ignoradas, lo mejor de la misma ha contribuido a la libertad del pensamiento y a la extensión de los derechos humanos. La alfabetización científica constituye una dimensión fundamental de la cultura ciudadana, garantía, a su vez, de aplicación del principio de precaución, que se apoya en una creciente sensibilidad social frente a las implicaciones del desarrollo tecnocientífico que puedan comportar riesgos para las personas o el medio ambiente.  
  
La contribución de esta materia a la competencia en comunicación lingüística se realiza a través de dos vías. Por una parte, la configuración y la transmisión de las ideas e informaciones sobre la naturaleza ponen en juego un modo específico de construcción del discurso, dirigido a argumentar o a hacer explícitas las relaciones, que solo se logrará adquirir desde los aprendizajes de estas materias. El cuidado en la precisión de los términos utilizados, en el encadenamiento adecuado de las ideas o en la expresión verbal de las relaciones hará efectiva esta contribución. Por otra parte, la adquisición de la terminología específica sobre los seres vivos, los objetos y los fenómenos naturales hace posible comunicar adecuadamente una parte muy relevante de las experiencia humana y comprender suficientemente lo que otros expresan sobre ella.  
  
Los contenidos asociados a la forma de construir y transmitir el conocimiento científico constituyen una oportunidad para el desarrollo de la competencia para aprender a aprender. El aprendizaje a lo largo de la vida, en el caso del conocimiento de la naturaleza, se va produciendo por la incorporación de informaciones provenientes en unas ocasiones de la propia experiencia y en otras de medios escritos o audiovisuales. La integración de esta información en la estructura de conocimiento de cada persona se produce si se tienen adquiridos en primer lugar los conceptos esenciales ligados a nuestro conocimiento del mundo natural y, en segundo lugar, los procedimientos de análisis de causas y consecuencias que son habituales en las ciencias de la naturaleza, así como las destrezas ligadas al desarrollo del carácter tentativo y creativo del trabajo científico, la integración de conocimientos y búsqueda de coherencia global, y la auto e interregulación de los procesos mentales.  
  
El énfasis en la formación de un espíritu crítico, capaz de cuestionar dogmas y desafiar prejuicios, permite contribuir al desarrollo de la autonomía e iniciativa personal. Es importante, en este sentido, señalar el papel de la ciencia como potenciadora del espíritu crítico en un sentido más profundo: la aventura que supone enfrentarse a problemas abiertos, participar en la construcción tentativa de soluciones, en definitiva, la aventura de hacer ciencia. En cuanto a la faceta de esta competencia relacionada con la habilidad para iniciar y llevar a cabo proyectos, se podrá contribuir a través del desarrollo de la capacidad de analizar situaciones valorando los factores que han incidido en ellas y las consecuencias que pueden tener. El pensamiento hipotético propio del quehacer científico se puede, así, transferir a otras situaciones.  


## Objetivos
La enseñanza de las Ciencias de la naturaleza en esta etapa tendrá como finalidad el desarrollo de las siguientes capacidades:  
  
1. Comprender y expresar mensajes con contenido científico utilizando el lenguaje oral y escrito con propiedad, así como comunicar a otros argumentaciones y explicaciones en el ámbito de la ciencia. Interpretar y construir, a partir de datos experimentales, mapas, diagramas, gráficas, tablas y otros modelos de representación, así como formular conclusiones.  
2. Utilizar la terminología y la notación científica. Interpretar y formular los enunciados de las leyes de la naturaleza, así como los principios físicos y químicos, a través de expresiones matemáticas sencillas. Manejar con soltura y sentido crítico la calculadora.  
3. Comprender y utilizar las estrategias y conceptos básicos de las ciencias de la naturaleza para interpretar los fenómenos naturales, así como para analizar y valorar las repercusiones de las aplicaciones y desarrollos tecnocientíficos.  
4. Aplicar, en la resolución de problemas, estrategias coherentes con los procedimientos de las ciencias, tales como la discusión del interés de los problemas planteados, la formulación de hipótesis, la elaboración de estrategias de resolución y de diseños experimentales, el análisis de resultados, la consideración de aplicaciones y repercusiones del estudio realizado y la búsqueda de coherencia global.  
5. Descubrir, reforzar y profundizar en los contenidos teóricos, mediante la realización de actividades prácticas relacionadas con ellos.  
6. Obtener información sobre temas científicos utilizando las tecnologías de la información y la comunicación y otros medios y emplearla, valorando su contenido, para fundamentar y orientar los trabajos sobre temas científicos.  
7. Adoptar actitudes críticas fundamentadas en el conocimiento para analizar, individualmente o en grupo, cuestiones científicas y tecnológicas.  
8. Desarrollar hábitos favorables a la promoción de la salud personal y comunitaria, facilitando estrategias que permitan hacer frente a los riesgos de la sociedad actual en aspectos relacionados con la alimentación, el consumo, las drogodependencias y la sexualidad.  
9. Comprender la importancia de utilizar los conocimientos provenientes de las ciencias de la naturaleza para satisfacer las necesidades humanas y para participar en la necesaria toma de decisiones en torno a problemas locales y globales del siglo XXI.  
10. Conocer y valorar las interacciones de la ciencia y la tecnología con la sociedad y el medio ambiente con atención particular a los problemas a los que se enfrenta hoy la humanidad, destacando la necesidad de búsqueda y aplicación de soluciones, sujetas al principio de precaución, que permitan avanzar hacia el logro de un futuro sostenible.  
11. Entender el conocimiento científico como algo integrado, que se compartimenta en distintas disciplinas para profundizar en los diferentes aspectos de la realidad.  
12. Describir las peculiaridades básicas del medio natural más próximo, en cuanto a sus aspectos geológicos, zoológicos y botánicos.  
13. Conocer el patrimonio natural de nuestra Comunidad Autónoma, sus características y elementos integradores, y valorar la necesidad de su conservación y mejora.  

## Contenidos 1º ESO Ciencias de la Naturaleza

### Bloque 1. Técnicas de trabajo.
- Familiarización con las características básicas del trabajo científico, por medio de: planteamiento de problemas, discusión de su interés, formulación de conjeturas, experimentación, etc., para comprender mejor los fenómenos naturales y resolver los problemas que su estudio plantea.  
- Utilización de los medios de comunicación y las tecnologías de la información para seleccionar información sobre el medio natural.  
- Interpretación de datos e informaciones sobre la naturaleza y utilización de dicha información para conocerla.  
- Reconocimiento del papel del conocimiento científico en el desarrollo tecnológico y en la vida de las personas.  
- Utilización cuidadosa de los materiales e instrumentos básicos de un laboratorio y respeto por las normas de seguridad en el mismo.  

### Bloque 2. La tierra en el Universo.

#### El Universo y el Sistema Solar.
- La observación del Universo: planetas, estrellas y galaxias.  
- La Vía Láctea y el Sistema Solar.  
- Características físicas de la Tierra y de los otros componentes del Sistema Solar.  
- Los movimientos de la Tierra: las estaciones, el día y la noche, los eclipses y las fases de la Luna.  
- Utilización de técnicas de orientación. Observación del cielo diurno y nocturno.  
- Evolución histórica de las concepciones sobre el lugar de la Tierra en el Universo: el paso del geocentrismo al heliocentrismo como primera y gran revolución científica.  
- Las capas de la tierra: Núcleo, Manto, Corteza, Hidrosfera, Atmósfera y Biosfera.  

#### La materia en el Universo.
- Propiedades generales de la materia constitutiva del Universo: definición de superficie, volumen, masa y densidad. Unidades (S.I.).  
- Estados en los que se presenta la materia en el universo: características y relación con la temperatura. Cambios de estado. Temperatura de fusión y de ebullición de una sustancia.  
- Reconocimiento de situaciones y realización de experiencias sencillas en las que se manifiesten las propiedades elementales de sólidos, líquidos y gases.  
- Identificación de sustancias puras y mezclas. Homogeneidad y heterogeneidad. Concepto de disolución y de suspensión. Ejemplos de materiales de interés y su utilización en la vida cotidiana.  
- Utilización de técnicas de separación de sustancias.  
- Átomos y moléculas. Símbolos y fórmulas.  
- Los elementos que forman el Universo. El hidrógeno y el helio.  

### Bloque 3. Materiales terrestres.

#### La atmósfera.
- Composición y propiedades de la atmósfera. Nitrógeno y oxígeno: abundancia y propiedades. Dióxido de carbono y ozono: implicaciones medioambientales. Variaciones en la composición del aire.  
- Reconocimiento del papel protector de la atmósfera, de la importancia del aire para los seres vivos y para la salud humana y de la necesidad de contribuir a su cuidado.  
- Fenómenos atmosféricos. Variables que condicionan el tiempo atmosférico. Distinción entre tiempo y clima.  
- Manejo de instrumentos para medir la temperatura, la presión, la velocidad y la humedad del aire.  
- Contaminantes atmosféricos: naturaleza, fuentes y dispersión.  
- Relación entre el aire y la salud.  

#### La hidrosfera.
- El agua en la Tierra (origen, abundancia e importancia) y en otros planetas.  
- El agua en la Tierra en sus formas líquida, sólida y gaseosa.  
- La molécula de agua: abundancia, propiedades e importancia. Estudio experimental de las propiedades del agua.  
- El agua del mar como disolución. Sodio, potasio y cloro: abundancia y propiedades.  
- El agua en los continentes.  
- El vapor de agua en la atmósfera.  
- El ciclo del agua en la Tierra y su relación con el Sol como fuente de energía.  
- Reservas de agua dulce en la Tierra: Importancia de su conservación.  
- El agua y la salud: la contaminación del agua y su depuración.  

#### La geosfera.
- Estructura interna de la Tierra.  
- La corteza terrestre: su superficie, composición química y elementos geoquímicos.  
- Composición química y petrológica de las capas de la Tierra.  
- Los minerales y las rocas: concepto de mineral y roca.  
- Tipos de rocas: sedimentarias, magmáticas y metamórficas. Importancia y utilidad de las rocas.  
- Utilidad, importancia y abundancia relativa de los minerales.  
- Observación, descripción y reconocimiento de los minerales y de las rocas más frecuentes.  
- Utilización de claves sencillas para identificar minerales y rocas.  
- Explotación de minerales y rocas.  

### Bloque 4. Los seres vivos y su diversidad.
- Factores que hacen posible la vida en la Tierra.  
- Los elementos bioquímicos.  
- El carbono; propiedades.  
- Características y funciones comunes de los seres vivos.  
- La diversidad de los seres vivos: ambientes, tamaños, formas y modos de alimentarse.  
- La teoría celular.  
- La diversidad como resultado del proceso evolutivo. Los fósiles y la historia de la vida.  

#### Clasificación de los seres vivos.
- Los cinco reinos.  
- Introducción a la taxonomía.  
- Utilización de claves sencillas de identificación de seres vivos.  
- Virus, bacterias y organismos unicelulares eucarióticos.  
- Hongos.  
- El reino vegetal; principales fila.  
- El reino animal; principales fila.  
- La especie humana.  
- Utilización de la lupa y el microscopio óptico para la observación y descripción de organismos unicelulares, plantas y animales.  
- Valoración de la importancia de mantener la diversidad de los seres vivos. Análisis de los problemas asociados a su pérdida.  

## Criterios evaluación 1º ESO Ciencias de la Naturaleza
1. Explicar la organización del Sistema Solar y las características de los movimientos de la Tierra y la Luna y sus implicaciones, así como algunas de las concepciones que sobre el sistema planetario se han dado a lo largo de la Historia.  
2. Situar y describir las capas internas y externas de nuestro planeta explicando la importancia de cada una de ellas.  
3. Establecer procedimientos para describir las propiedades de la materia que nos rodea, tales como la masa, el volumen, la densidad, los estados en los que se presentan y sus cambios. Valorar el manejo del instrumental científico. Utilizar modelos gráficos para representar y comparar los datos obtenidos.  
4. Realizar correctamente cálculos sencillos que incluyan la utilización de las diferentes unidades del SI, y manejar las diferentes unidades del sistema métrico decimal.  
5. Relacionar propiedades de los materiales con el uso que se hace de ellos y diferenciar entre mezclas y sustancias, gracias a las propiedades características de estas últimas y a la posibilidad de separar aquellas por procesos físicos como la filtración, decantación, cristalización, etc. aprovechando las propiedades que diferencian a cada sustancia de las demás.  
6. Diferenciar entre elementos y compuestos, átomos y moléculas, símbolos y fórmulas. Conocer las características de las partículas fundamentales del átomo.  
7. Explicar el átomo según el modelo planetario y establecer el criterio de materia neutra.  
8. Elaborar e interpretar gráficos y modelos sencillos sobre la estructura y dinámica atmosféricas, estableciendo relaciones entre las variables que condicionan el clima y los principales fenómenos meteorológicos.  
9. Reconocer la importancia de la atmósfera para los seres vivos, considerando las repercusiones de la actividad humana en la misma.  
10. Explicar, a partir del conocimiento de las propiedades del agua, el ciclo del agua en la naturaleza y su importancia para los seres vivos, considerando las repercusiones de las actividades humanas en relación con su utilización.  
11. Conocer la estructura interna de la Tierra y los componentes químicos de sus capas, y diferenciar claramente los conceptos de mineral y roca.  
12. Identificar las rocas y los minerales más frecuentes, en especial los que se encuentran en el entorno próximo, utilizando claves sencillas y reconocer sus aplicaciones más frecuentes. Conocer y valorar la importancia y los usos habituales de las rocas.  
13. Establecer los criterios que sirven para clasificar a los seres vivos e identificar los principales modelos taxonómicos a los que pertenecen los animales y plantas más comunes, relacionando la presencia de determinadas estructuras con su adaptación al medio.  
14. Conocer de forma operativa el concepto de biodiversidad. Valorar la importancia de la biodiversidad a escala mundial y en España.  
15. Explicar las funciones comunes a todos los seres vivos, teniendo en cuenta la teoría celular.  
16. Realizar correctamente experiencias de laboratorio, respetando las normas de seguridad.  

## Contenidos 2º ESO Ciencias de la Naturaleza

### Bloque 1. Técnicas de trabajo.
- Familiarización con las características básicas del trabajo científico, por medio de: planteamiento de problemas, discusión de su interés, formulación de conjeturas, diseños experimentales, etc., para comprender mejor los fenómenos naturales y resolver los problemas que su estudio plantea.  
- Utilización de los medios de comunicación y las tecnologías de la información y la comunicación para obtener información sobre los fenómenos naturales.  
- Interpretación de información de carácter científico y utilización de dicha información para formarse una opinión propia y expresarse adecuadamente.  
- Reconocimiento de la importancia del conocimiento científico para tomar decisiones sobre los objetos y sobre uno mismo.  
- Utilización correcta de los materiales e instrumentos básicos de un laboratorio y respeto por las normas de seguridad en el mismo.  

### Bloque 2. Materia y energía.

#### Sistemas materiales.
- Composición de la materia. Átomos y moléculas. Elementos y compuestos.  
- Formulación de compuestos binarios.  
- Escalas de observación macro y microscópica (unidades representativas: mega, año luz, micro).  
- Los cambios de posición en los sistemas materiales.  
- Movimiento rectilíneo uniforme y uniformemente variado. Concepto de aceleración.  
- Representación gráfica de movimientos sencillos.  

#### Las fuerzas y sus aplicaciones.
- Las fuerzas como causa del movimiento, los equilibrios y las deformaciones (ecuación y unidades en el S.I.)  
- Masa y peso de los cuerpos. Atracción gravitatoria.  
- Estudio cualitativo del Principio de Arquímedes. Aplicaciones sencillas.  

#### La energía en los sistemas materiales.
- La energía como concepto fundamental para el estudio de los cambios. Cambio de posición, forma y estado. Valoración del papel de la energía en nuestras vidas.  
- Trabajo y energía: análisis cualitativo e interpretación de transformaciones energéticas de procesos sencillos cotidianos.  
- Principio de conservación de la energía. Tipos de energía: cinética y potencial. Energía mecánica.  
- Análisis y valoración de las diferentes fuentes de energía, renovables y no renovables.  
- Problemas asociados a la obtención, transporte y utilización de la energía.  
- Toma de conciencia de la importancia del ahorro energético.  

### Bloque 3. Transferencia de energía.

#### Calor y temperatura.
- Interpretación del calor como forma de transferencia de energía.  
- Distinción entre calor y temperatura. Los termómetros.  
- El calor como agente productor de cambios. Reconocimiento de situaciones y realización de experiencias sencillas en las que se manifiesten los efectos del calor sobre los cuerpos.  
- Propagación del calor. Aislantes y conductores.  
- Valoración de las aplicaciones y repercusiones del uso del calor.  

#### Luz y sonido.
- La luz y el sonido como modelos de ondas.  
- Luz y visión: los objetos como fuentes secundarias de luz.  
- Propagación rectilínea de la luz en todas direcciones. Reconocimiento de situaciones y realización de experiencias sencillas para ponerla de manifiesto. Sombras y eclipses.  
- Estudio cualitativo de la reflexión y de la refracción. Utilización de espejos y lentes.  
- Descomposición de la luz: interpretación de los colores.  
- Sonido y audición. Propagación y reflexión del sonido.  
- Valoración del problema de la contaminación acústica y lumínica.  

#### La energía interna del planeta.
- Origen del calor interno terrestre.  
- Las manifestaciones de la energía interna de la Tierra: erupciones volcánicas y terremotos.  
- Interpretación del comportamiento de las ondas sísmicas y su contribución al conocimiento del interior de la Tierra.  
- Distribución de terremotos y volcanes y descubrimiento de las placas litosféricas.  
- Movimientos de los continentes  
- Valoración de los riesgos volcánico y sísmico y de su predicción y prevención.  
- El relieve terrestre. Continentes y fondos marinos.  
- La formación de rocas magmáticas y metamórficas. Identificación de estos tipos de rocas y relación entre su textura y origen.  

### Bloque 4. La vida en acción.   

#### Las funciones vitales.
- El descubrimiento de la célula.  
- Observación de células al microscopio.  
- Las funciones de nutrición: Obtención y uso de materia y energía por los seres vivos.  
- Nutrición autótrofa y heterótrofa  
- La fotosíntesis y su importancia en la vida de la Tierra.  
- La Respiración en los seres vivos.  
- Las funciones de relación: percepción, coordinación y movimiento.  
- Las funciones de reproducción: La reproducción sexual y asexual.  
- El mantenimiento de la especie. La reproducción animal y vegetal: analogías y diferencias.  
- Observación y descripción de ciclos vitales en animales y vegetales.  

### Bloque 5. El medio ambiente natural.
- Conceptos de Biosfera, ecosfera y ecosistema.  
- Identificación de los componentes de un ecosistema.  
- Influencia de los factores bióticos y abióticos en los ecosistemas.  
- Ecosistemas acuáticos de agua dulce y marinos.  
- Ecosistemas Terrestres: los biomas  
- El papel que desempeñan los organismos productores, consumidores y descomponedores en el ecosistema. Cadenas y redes tróficas.  
- Realización de indagaciones sencillas sobre algún ecosistema del entorno.  
- Ecosistemas característicos en nuestra Comunidad Autónoma.  

## Criterios de evaluación 2º ESO Ciencias de la Naturaleza
1. Interpretar los sistemas materiales como partes del Universo de muy distintas escalas, a los que la Ciencia delimita para su estudio, y destacar la energía como una propiedad inseparable de todos ellos, capaz de originarles cambios.  
2. Definir magnitudes como: velocidad, aceleración y fuerza; relacionarlas con una expresión matemática y unas unidades propias.  
3. Definir los conceptos y magnitudes que caracterizan el movimiento. Resolver problemas sencillos.  
4. Identificar las fuerzas en contextos cotidianos como causa de los cambios en los movimientos y de las deformaciones, así como su papel en el equilibrio de los cuerpos.  
5. Definir el concepto de peso como una fuerza y diferenciarlo del de masa. Distinguir con exactitud y diferenciar los conceptos de energía cinética y potencial, así como los de calor y temperatura.  
6. Utilizar el concepto cualitativo de energía para explicar su papel en las transformaciones que tienen lugar en nuestro entorno y reconocer la importancia y repercusiones para la sociedad y el medio ambiente de las diferentes fuentes de energías renovables y no renovables.  
7. Resolver problemas sencillos aplicando los conocimientos sobre el concepto de temperatura y su medida, el equilibrio y desequilibrio térmico, los efectos del calor sobre los cuerpos y su forma de propagación.  
8. Explicar fenómenos naturales referidos a la transmisión de la luz y del sonido y reproducir algunos de ellos teniendo en cuenta sus propiedades.  
9. Reconocer y valorar los riesgos asociados a los procesos geológicos terrestres y las pautas utilizadas en su prevención y predicción. Analizar la importancia de los fenómenos volcánicos y sismológicos, así como la necesidad de planificar la prevención de riesgos futuros.  
10. Analizar la incidencia de algunas actuaciones individuales y sociales relacionadas con la energía en el deterioro y mejora del medio ambiente.  
11. Relacionar el vulcanismo, los terremotos, la formación del relieve y la génesis de las rocas metamórficas y magmáticas con la energía interna del planeta, llegando a situar en un mapa las zonas donde dichas manifestaciones son más intensas y frecuentes.  
12. Establecer las características de las rocas metamórficas y magmáticas.  
13. Interpretar los aspectos relacionados con las funciones vitales de los seres vivos a partir de distintas observaciones y experiencias realizadas con organismos sencillos, comprobando el efecto que tienen determinadas variables en los procesos de nutrición, relación y reproducción.  
14. Definir los conceptos de nutrición celular y respiración aplicando los conocimientos sobre la obtención de energía.  
15. Diferenciar los mecanismos que tienen que utilizar los seres pluricelulares para realizar sus funciones, distinguiendo entre los procesos que producen energía y los que la consumen, llegando a distinguir entre nutrición autótrofa y heterótrofa, y entre reproducción animal y vegetal.  
16. Distinguir entre los conceptos de Biosfera y Exosfera explicando, mediante ejemplos sencillos, el flujo de energía en los ecosistemas.  
17. Identificar y cuantificar los componentes bióticos y abióticos de un ecosistema cercano, valorar su diversidad y representar gráficamente las relaciones tróficas establecidas entre los seres vivos del mismo.  
18. Caracterizar los ecosistemas más significativos de nuestra Comunidad Autónoma. Identificar los espacios naturales protegidos en nuestra Comunidad Autónoma y valorar algunas figuras de protección.  
19. Realizar correctamente experiencias de laboratorio, respetando las normas de seguridad  

