# Currículo Física 2º Bachillerato (LOMLOE)

En esta página se incluye el [currículo estatal (Real Decreto)](#Estatal) y el [currículo de Madrid (Decreto)](#Madrid)

## <a name="Estatal"></a> Estatal (Real Decreto)
Revisado con [Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521) citado en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  
  
Actualizada [comparación currículo Física 2º Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/comparacion-curriculo-fisica-2-bachillerato) con proyecto 9 diciembre 2021  

Ver también [comparación currículo Física 2º Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/comparacion-curriculo-fisica-2-bachillerato)

Algunos cambios diciembre 2021 vs octubre 2021: Física se sustituye por física, ciencia p física, habilidades por destrezas, ciudadanos críticos por ciudadanía critica, ... 

Algún cambio real decreto vs diciembre 2021: se añade ", sin perder la perspectiva del punto de vista medioambiental y de justicia social."   


La física, como disciplina que estudia la naturaleza, se encarga de entender y describir el universo, desde los fenómenos que se producen en el microcosmos hasta aquellos que se dan en el macrocosmos. La materia, la energía y las interacciones se comportan de forma distinta en las diferentes situaciones, lo que hace que los modelos, principios y leyes de la física que el alumnado ha de aplicar para explicar la naturaleza deban ajustarse a la escala de trabajo y a que las respuestas que encuentre serán siempre aproximadas y condicionadas por el contexto. Resulta adecuado que los alumnos y alumnas perciban la física como una ciencia que evoluciona, y reconozcan también que los conocimientos que implica la relacionan íntimamente con la tecnología, la sociedad y el medioambiente, lo que la convierte en una ciencia indispensable para la formación individual de cada estudiante de la modalidad de Ciencias y Tecnología, pues le permite formar parte activa de una ciencia en construcción a partir del análisis de su evolución histórica y de las destrezas que adquiere para observar, explicar y demostrar los fenómenos naturales.

Por otro lado, con la enseñanza de esta materia se pretende desmitificar que la física sea algo complejo, mostrando que muchos de los fenómenos que ocurren en el día a día pueden comprenderse y explicarse a través de modelos y leyes físicas accesibles. Conseguir que resulte interesante el estudio de estos fenómenos contribuye a formar una ciudanía crítica y con una base científica adecuada. La física está presente en los avances tecnológicos que facilitan un mejor desarrollo económico de la sociedad, que actualmente prioriza la sostenibilidad y busca soluciones a los graves problemas ambientales. La continua innovación impulsa este desarrollo tecnológico y el alumnado, que puede formar parte de esta comunidad científica, debe poseer las competencias para contribuir a él y los conocimientos, destrezas y actitudes que lleven asociados. Fomentar en el estudiante la curiosidad por el funcionamiento y conocimiento de la naturaleza es el punto de partida para conseguir unos logros que contribuirán de forma positiva en la sociedad.

El diseño de la materia parte de las competencias específicas, cuyo desarrollo permite al alumnado adquirir conocimientos, destrezas y actitudes científicos avanzados. Estas competencias no se refieren exclusivamente a elementos de la física, sino que también hacen referencia a elementos transversales que juegan un papel importante en la completa formación de los alumnos y alumnas. En este proceso no debe olvidarse el carácter experimental de esta ciencia, por eso se propone la utilización de metodologías y herramientas experimentales, entre ellas la formulación matemática de las leyes y principios, los instrumentos de laboratorio y las herramientas tecnológicas que pueden facilitar la comprensión de los conceptos y fenómenos. Por otro lado, estas competencias también pretenden fomentar el trabajo en equipo y los valores sociales y cívicos para lograr personas comprometidas que utilicen la ciencia para la formación permanente a lo largo de la vida, el desarrollo medioambiental, el bien comunitario y el progreso de la sociedad.

Los conocimientos, destrezas y actitudes básicas que ha adquirido el alumnado en la etapa de Educación Secundaria Obligatoria y en el primer curso de Bachillerato han creado en él una estructura competencial sobre la que consolidar y construir los saberes científicos que aporta la física en este curso. Los diferentes bloques de saberes básicos de la materia de Física de Bachillerato van enfocados a relacionar y completar a los de las enseñanzas de etapas anteriores, de forma que el alumnado pueda adquirir una percepción global de las distintas líneas de trabajo en física y de sus muy diversas aplicaciones. Aunque aparezcan presentados de este modo, en realidad la ordenación de los bloques no responde a una secuencia establecida para que el profesorado pueda trabajar de acuerdo a la temporalización más adecuada para las necesidades de su grupo concreto.

Los dos primeros bloques hacen referencia a la teoría clásica de campos. En el primero de ellos se abarcan los conocimientos, destrezas y actitudes referidos al estudio del campo gravitatorio. En él se presentan, empleando las herramientas matemáticas adecuadas, las interacciones que se generan entre partículas másicas y, en relación con algunos de los conocimientos de cursos anteriores, su mecánica, su energía y los principios de conservación. A continuación, el segundo bloque comprende los saberes sobre electromagnetismo. Describe los campos eléctrico y magnético, tanto estáticos como variables en el tiempo, y sus características y aplicaciones tecnológicas, biosanitarias e industriales.

El siguiente bloque se refiere a vibraciones y ondas, contemplando el movimiento oscilatorio como generador de perturbaciones y su propagación en el espacio-tiempo a través de un movimiento ondulatorio. Finalmente, presenta la conservación de energía en las ondas y su aplicación en ejemplos concretos como son las ondas sonoras y las ondas electromagnéticas, lo que abre el estudio de los procesos propios de la óptica física y la óptica geométrica.

Con el último bloque se muestra el panorama general de la física del presente y el futuro. En él se exponen los conocimientos, destrezas y actitudes de la física cuántica y de la física de partículas. Bajo los principios fundamentales de la física relativista, este bloque incluye modelos que explican la constitución de la materia y los procesos que ocurren cuando se estudia ciencia a nivel microscópico. Este bloque permitirá al alumnado aproximarse a las fronteras de la física y abrirá su curiosidad –el mejor motor para su aprendizaje– al ver que todavía quedan muchas preguntas por resolver y muchos retos que deben ser atendidos desde la investigación y el desarrollo de esta ciencia.

Para completar el aprendizaje competencial de esta materia, el currículo presenta los criterios de evaluación. Al referirse directamente a las competencias específicas, estos evalúan el progreso competencial del alumnado de forma significativa, pretendiendo una evaluación que vaya más allá de verter íntegramente contenidos teóricos o resultados, y justifican el saber útil sobre situaciones concretas de la naturaleza, es decir, van encaminadas a la adquisición de estrategias y herramientas para la resolución de problemas como elemento clave del aprendizaje significativo. La integración de aprendizajes en un contexto global permite, así, que el desarrollo científico del alumnado contribuya en su evaluación.

Con esta materia se busca, en definitiva, que los alumnos y alumnas generen curiosidad por la investigación de las ciencias y se formen para satisfacer las demandas sociales, tecnológicas e industriales que nos deparan el presente y el futuro cercano, sin perder la perspectiva del punto de vista medioambiental y de justicia social.

## Competencias Específicas  

Algunos cambios diciembre 2021 vs octubre 2021:   
En 3 se elimina "despertar la curiosidad por el conocimiento del universo"  
En 4 se elimina "la crítica en el análisis de las fuentes de información encontradas y utilizadas"  
En 5 se elimina "factores que intervienen, su justificación teórica y resolución"  

Algunos cambios Real Decreto vs diciembre 2021  
En competencia específica 2 se indica CPSAA2 en lugar CPSAA3  
En competencia específica 3 se indica CD3 en lugar CD2  
En competencia específica 4 se indica CPSAA4 en lugar CPSAA5  
En competencia específica 5 se CPSAA3.2 en lugar de CPSAA6  
En competencia específica 6 se pasan a poner algunos descriptores en minúsculas  


1. Utilizar las teorías, principios y leyes que rigen los procesos físicos más importantes, considerando su base experimental y desarrollo matemático en la resolución de problemas, para reconocer la física como una ciencia relevante implicada en el desarrollo de la tecnología, la economía, la sociedad y la sostenibilidad ambiental.

Utilizar los principios, leyes y teorías de la física requiere de un amplio conocimiento de sus fundamentos teóricos. Comprender y describir, a través de la experimentación o la utilización de desarrollos matemáticos, las interacciones que se producen entre cuerpos y sistemas en la naturaleza permite, a su vez, desarrollar el pensamiento científico para construir nuevo conocimiento aplicado a la resolución de problemas en distintos contextos en los que interviene la física. Esto implica apreciar la física como un campo del saber con importantes implicaciones en la tecnología, la economía, la sociedad y la sostenibilidad ambiental.

De esta forma, a partir de la comprensión de las implicaciones de la física en otros campos de la vida cotidiana, consigue formarse una opinión fundamentada sobre las situaciones que afectan a cada contexto, lo que es necesario para desarrollar un pensamiento crítico y una actitud adecuada para contribuir al progreso a través del conocimiento científico adquirido, aportando soluciones sostenibles.

Esta competencia específica se conecta con los siguientes descriptores: STEM1, STEM2, STEM3, CD5.

2. Adoptar los modelos, teorías y leyes aceptados de la física como base de estudio de los sistemas naturales y predecir su evolución para inferir soluciones generales a los problemas cotidianos relacionados con las aplicaciones prácticas demandadas por la sociedad en el campo tecnológico, industrial y biosanitario.
 
El estudio de la física, como ciencia de la naturaleza, debe proveer de la competencia para analizar fenómenos que se producen en el entorno natural. Para ello, es necesario adoptar los modelos, teorías y leyes que forman los pilares fundamentales de este campo de conocimiento y que a su vez permiten predecir la evolución de los sistemas y objetos naturales. Al mismo tiempo, esta adopción se produce cuando se relacionan los fenómenos observados en situaciones cotidianas con los fundamentos y principios de la física.

Así, a partir del análisis de diversas situaciones particulares se aprende a inferir soluciones generales a los problemas cotidianos, que pueden redundar en aplicaciones prácticas necesarias para la sociedad y que darán lugar a productos y beneficios a través de su desarrollo desde el campo tecnológico, industrial o biosanitario.

Esta competencia específica se conecta con los siguientes descriptores: STEM2, STEM5, CPSAA2, CC4.

3. Utilizar el lenguaje de la física con la formulación matemática de sus principios, magnitudes, unidades, ecuaciones, etc., para establecer una comunicación adecuada entre diferentes comunidades científicas y como una herramienta fundamental en la investigación.

El desarrollo de esta competencia específica pretende trasladar a los alumnos y alumnas un conjunto de criterios para el uso de formalismos con base científica, con la finalidad de poder plantear y discutir adecuadamente la resolución de problemas de física y discutir sus aplicaciones en el mundo que les rodea. Además, se pretende que valoren la universalidad del lenguaje matemático y su formulación para intercambiar planteamientos físicos y sus resoluciones en distintos entornos y medios.

Integrar al alumnado en la participación colaborativa con la comunidad científica requiere de un código específico, riguroso y común que asegure la claridad de los mensajes que se intercambian entre sus miembros. Del mismo modo, con esta competencia específica se pretende atender a la demanda de los avances tecnológicos teniendo en cuenta la conservación del medioambiente.

Esta competencia específica se conecta con los siguientes descriptores: CCL1, CCL5, STEM1, STEM4, CD3.

4. Utilizar de forma autónoma, eficiente, crítica y responsable recursos en distintos formatos, plataformas digitales de información y de comunicación en el trabajo individual y colectivo para el fomento de la creatividad mediante la producción y el intercambio de materiales científicos y divulgativos que faciliten acercar la física a la sociedad como un campo de conocimientos accesible.

Entre las destrezas que deben adquirirse en los nuevos contextos de enseñanza y aprendizaje actuales se encuentra la de utilizar plataformas y entornos virtuales de aprendizaje. Estas plataformas sirven de repositorio de recursos y materiales de distinto tipo y en distinto formato y son útiles para el aprendizaje de la física, así como medios para el aprendizaje individual y social. Es necesario, pues, utilizar estos recursos de forma autónoma y eficiente para facilitar el aprendizaje autorregulado y al mismo tiempo ser responsable en las interacciones con otros estudiantes y con el profesorado.

Al mismo tiempo, la producción y el intercambio de materiales científicos y divulgativos permiten acercar la física de forma creativa a la sociedad, presentándola como un campo de conocimientos accesible.

Esta competencia específica se conecta con los siguientes descriptores: STEM3, STEM5, CD1, CD3, CPSAA4.

5. Aplicar técnicas de trabajo e indagación propias de la física, así como la experimentación, el razonamiento lógico-matemático y la cooperación, en la resolución de problemas y la interpretación de situaciones relacionadas, para poner en valor el papel de la física en una sociedad basada en valores éticos y sostenibles.

Las ciencias de la naturaleza tienen un carácter experimental intrínseco. Uno de los principales objetivos de cualquiera de estas disciplinas científicas es la explicación de los fenómenos naturales, lo que permite formular teorías y leyes para su aplicación en diferentes sistemas. El caso de la física no es diferente, y es relevante trasladar a los alumnos y alumnas la curiosidad por los fenómenos que suceden en su entorno y en distintas escalas. Hay procesos físicos cotidianos que son reproducibles fácilmente y pueden ser explicados y descritos con base en los principios y leyes de la física. También hay procesos que, aun no siendo reproducibles, están presentes en el entorno natural de forma generalizada y gracias a los laboratorios virtuales se pueden simular para aproximarse más fácilmente a su estudio.
 
El trabajo experimental constituye un conjunto de etapas que fomentan la colaboración e intercambio de información, ambos muy necesarios en los campos de investigación actuales. Para ello, se debe fomentar en su desarrollo la experimentación y estimación de los errores, la utilización de distintas fuentes documentales en varios idiomas y el uso de recursos tecnológicos. Finalmente, se debe plasmar la información en informes que recojan todo este proceso, lo que permitiría a los estudiantes formar, en un futuro, parte de la comunidad científica.

Esta competencia específica se conecta con los siguientes descriptores: STEM1, CPSAA3.2, CC4, CE3.

6. Reconocer y analizar el carácter multidisciplinar de la física, considerando su relevante recorrido histórico y sus contribuciones al avance del conocimiento científico como un proceso en continua evolución e innovación, para establecer unas bases de conocimiento y relación con otras disciplinas científicas.
 
La física constituye una ciencia profundamente implicada en distintos ámbitos de nuestras vidas cotidianas y que, por tanto, forma parte clave del desarrollo científico, tecnológico e industrial. La adecuada aplicación de sus principios y leyes permite la resolución de diversos problemas basados en los mismos conocimientos, y la aplicación de planteamientos similares a los estudiados en distintas situaciones muestra la universalidad de esta ciencia.

Los conocimientos y aplicaciones de la física forman, junto con los de otras ciencias como las matemáticas o la tecnología, un sistema simbiótico cuyas aportaciones se benefician mutuamente. La necesidad de formalizar experimentos para verificar los estudios implica un incentivo en el desarrollo tecnológico y viceversa, el progreso de la tecnología alumbra nuevos descubrimientos que precisan de explicación a través de las ciencias básicas como la física. La colaboración entre distintas comunidades científicas expertas en diferentes disciplinas es imprescindible en todo este desarrollo.

Esta competencia específica se conecta con los siguientes descriptores: stem2, stem5, CPSAA5, ce1.	
  
## Criterios de evaluación  
  
### Competencia específica 1
1.1. Reconocer la relevancia de la física en el desarrollo de la ciencia, tecnología,
la economía, la sociedad y la sostenibilidad ambiental, empleando
adecuadamente los fundamentos científicos relativos a esos ámbitos.  
1.2. Resolver problemas de manera experimental y analítica, utilizando
principios, leyes y teorías de la física.  
### Competencia específica 2
2.1. Analizar y comprender la evolución de los sistemas naturales, utilizando
modelos, leyes y teorías de la física.  
2.2. Inferir soluciones generales a problemas generales a partir del análisis de
situaciones particulares y las variables de que dependen.  
2.3. Conocer aplicaciones prácticas y productos útiles para la sociedad en el
campo tecnológico, industrial y biosanitario, analizándolos en base a los
modelos, las leyes y las teorías de la física.  
### Competencia específica 3
3.1. Aplicar los principios, leyes y teorías científicas en el análisis crítico de
procesos físicos del entorno, como los observados y los publicados en distintos
medios de comunicación, analizando, comprendiendo y explicando las causas
que los producen.  
3.2. Utilizar de manera rigurosa las unidades de las variables físicas en diferentes
sistemas de unidades, empleando correctamente su notación y sus
equivalencias, así como la elaboración e interpretación adecuada de gráficas
que relacionan variables físicas, posibilitando una comunicación efectiva con
toda la comunidad científica.  
3.3. Expresar de forma adecuada los resultados, argumentando las soluciones
obtenidas, en la resolución de los ejercicios y problemas que se plantean, bien
sea a través de situaciones reales o ideales.  
### Competencia específica 4
4.1. Consultar, elaborar e intercambiar materiales científicos y divulgativos en
distintos formatos con otros miembros del entorno de aprendizaje, utilizando de
forma autónoma y eficiente plataformas digitales.  
4.2. Usar de forma crítica, ética y responsable medios de comunicación digitales
y tradicionales como modo de enriquecer el aprendizaje y el trabajo individual y
colectivo.  
### Competencia específica 5
5.1. Obtener relaciones entre variables físicas, midiendo y tratando los datos
experimentales, determinando los errores y utilizando sistemas de
representación gráfica.  
5.2. Reproducir en laboratorios, sean reales o virtuales, determinados procesos
físicos modificando las variables que los condicionan, considerando los
principios, leyes o teorías implicados, generando el correspondiente informe con
formato adecuado e incluyendo argumentaciones, conclusiones, tablas de datos,
gráficas y referencias bibliográficas.  
5.3. Valorar la física, debatiendo de forma fundamentada sobre sus avances y la
implicación en la sociedad desde el punto de vista de la ética y de la
sostenibilidad.  
### Competencia específica 6
6.1. Identificar los principales avances científicos relacionados con la física que
han contribuido a las leyes y teorías aceptadas actualmente en el conjunto de
las disciplinas científicas, como las fases para el entendimiento de las
metodologías de la ciencia, su evolución constante y su universalidad.  
6.2. Reconocer el carácter multidisciplinar de la ciencia y las contribuciones de
unas disciplinas sobre otras, estableciendo relaciones entre la física y la
Química, la Biología o las Matemáticas.   

## Saberes básicos 

Algunos cambios diciembre 2021 vs octubre 2021:  
A: Se cambia "sistema de dos masas" pasa a "sistema de masas"   
A: Se elimina "en el espacio e inferencia sobre la influencia que tendría en la trayectoria de otras masas que se encuentran en sus proximidades"   
A: Se añade "industria, **la tecnología, la economía** y en la sociedad"  
B: Se cambia _"geométricas, como hilos rectilíneos, espiras, solenoides o toros, y la interacción entre ellos o con cargas eléctricas libres presentes en su entorno."_ por "geométricas: rectilíneos, espiras, solenoides o toros. Interacción con cargas eléctricas libres presentes en su entorno." luego desparece la interacción entre hilos, que enlazaría con definición de Amperio.  
C: Se elimina "trazado de rayos", se habla solo de "formación de imágenes"  
C: Se elimina "teniendo en cuenta la atenuación y umbral de audición, así como las modificaciones de sus propiedades en función del 
desplazamiento del emisor y/o el receptor, y sus aplicaciones"  

Algunos cambios BOE abril 2022 vs diciembre 2021 (ver alegación 26 diciembre 2021 en [Currículo LOMLOE](/home/legislacion-educativa/lomloe/curriculo) )   
B: no se cambia (en alegación puse añadir interacción entre hilos)  
C: se añade "**Cambios en las propiedades de las ondas en función del desplazamiento del emisor y receptor.**" (lo puse en alegación)
D: se reescribe prácticamente entero y cambia del todo. Se añade 
- "**contracción de la longitud, dilatación del tiempo, energía y masa relativistas.**"  
- "Principio de incertidumbre **formulado en base al tiempo y la energía.**"  
- "**Modelo estándar en la física de partículas. Clasificaciones de las partículas fundamentales. Las interacciones fundamentales como procesos de intercambio de partículas (bosones). Aceleradores de partículas.**"  
- "estabilidad de isótopos"  
- "Radiactividad natural y **otros procesos nucleares**"   
- "**ingeniería, la tecnología**"
 
**A. Campo gravitatorio**  
- Determinación, a través del cálculo vectorial, del campo gravitatorio producido por un sistema de masas. Efectos sobre las variables cinemáticas y dinámicas de objetos inmersos en el campo.

- Momento angular de un objeto en un campo gravitatorio: cálculo, relación con las fuerzas centrales y aplicación de su conservación en el estudio de su movimiento.

- Energía mecánica de un objeto sometido a un campo gravitatorio: deducción del tipo de movimiento que posee, cálculo del trabajo o los balances energéticos existentes en desplazamientos entre distintas posiciones, velocidades y tipos de trayectorias.

- Leyes que se verifican en el movimiento planetario y extrapolación al movimiento de satélites y cuerpos celestes.

- Introducción a la cosmología y la astrofísica como aplicación del campo gravitatorio: implicación de la física en la evolución de objetos astronómicos, del conocimiento del universo y repercusión de la investigación en estos ámbitos en la industria, la tecnología, la economía y en la sociedad.

**B. Campo electromagnético**  
- Campos eléctrico y magnético: tratamiento vectorial, determinación de las variables cinemáticas y dinámicas de cargas eléctricas libres en presencia de estos campos. Fenómenos naturales y aplicaciones tecnológicas en los que se aprecian estos efectos.

- Intensidad del campo eléctrico en distribuciones de cargas discretas y continuas: cálculo e interpretación del flujo de campo eléctrico.

- Energía de una distribución de cargas estáticas: magnitudes que se modifican y que permanecen constantes con el desplazamiento de cargas libres entre puntos de distinto potencial eléctrico.

- Campos magnéticos generados por hilos con corriente eléctrica en distintas configuraciones geométricas: rectilíneos, espiras, solenoides o toros. Interacción con cargas eléctricas libres presentes en su entorno.

- Líneas de campo eléctrico y magnético producido por distribuciones de carga sencillas, imanes e hilos con corriente eléctrica en distintas configuraciones geométricas.

- Generación de la fuerza electromotriz: funcionamiento de motores, generadores y transformadores a partir de sistemas donde se produce una variación del flujo magnético.


**C. Vibraciones y ondas**  
- Movimiento oscilatorio: variables cinemáticas de un cuerpo oscilante y conservación de energía en estos sistemas.

- Movimiento ondulatorio: gráficas de oscilación en función de la posición y del tiempo, ecuación de onda que lo describe y relación con el movimiento armónico simple. Distintos tipos de movimientos ondulatorios en la naturaleza.

- Fenómenos ondulatorios: situaciones y contextos naturales en los que se ponen de manifiesto distintos fenómenos ondulatorios y aplicaciones. Ondas sonoras y sus cualidades. Cambios en las propiedades de las ondas en función del desplazamiento del emisor y receptor.

- Naturaleza de la luz: controversias y debates históricos. La luz como onda electromagnética. Espectro electromagnético.

- Formación de imágenes en medios y objetos con distinto índice de refracción. Sistemas ópticos: lentes delgadas, espejos planos y curvos y sus aplicaciones.

**D. Física relativista, cuántica, nuclear y de partículas**  
- Principios fundamentales de la Relatividad especial y sus consecuencias: contracción de la longitud, dilatación del tiempo, energía y masa relativistas.

- Dualidad onda-corpúsculo y cuantización: hipótesis de De Broglie y efecto fotoeléctrico. Principio de incertidumbre formulado en base al tiempo y la energía.

- Modelo estándar en la física de partículas. Clasificaciones de las partículas fundamentales. Las interacciones fundamentales como procesos de intercambio de partículas (bosones). Aceleradores de partículas.

- Núcleos atómicos y estabilidad de isótopos. Radiactividad natural y otros procesos nucleares. Aplicaciones en los campos de la ingeniería, la tecnología y la salud.

## <a name="Madrid"></a>  Madrid (Decreto)

Revisado con [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF)  
Física 2º Bachillerato en página 139 del pdf  

Cambios en [Proyecto de decreto 31 mayo](https://www.comunidad.madrid/transparencia/sites/default/files/04_2_2022-05-31_decreto_bachillerato-a.pdf)  

En texto inicial se introducen cambios  
Se cambia "alumnos y alumnas" por "alumnado"  
Se cambia "la tecnología, la sociedad y el medioambiente, lo que la convierte en una ciencia indispensable para la formación individual de cada estudiante de esta modalidad, pues le proporciona la capacidad de formar parte activa de una ciencia en construcción a partir del
análisis de su evolución histórica y de las destrezas que adquiere para observar, explicar y demostrar los fenómenos naturales." por "otras ciencias"  
Se eliminan dos párrafos completos  
> Por otro lado, con la enseñanza de esta materia se pretende desmitificar que la física sea algo complejo, mostrando que muchos de los fenómenos que ocurren en el día a día pueden comprenderse y explicarse a través de modelos y leyes físicas accesibles. Conseguir que resulte interesante el estudio de estos fenómenos contribuye a formar una ciudanía crítica y con una base científica adecuada. La física está presente en los avances tecnológicos que facilitan un mejor desarrollo económico de la sociedad, que actualmente prioriza la
sostenibilidad y busca soluciones a los graves problemas ambientales. La continua innovación impulsa este desarrollo tecnológico y el alumnado, que puede formar parte de esta comunidad científica, debe poseer las competencias para contribuir a él y los conocimientos, destrezas y actitudes que lleven asociados. Fomentar en el estudiante la curiosidad por el funcionamiento y conocimiento de la naturaleza es el punto de partida para conseguir unos logros que contribuirán de forma positiva en la sociedad.  
> El diseño de la materia parte de las competencias específicas, cuyo desarrollo da al alumnado la capacidad de adquirir conocimientos, destrezas y actitudes científicos avanzados. Estas competencias no se refieren exclusivamente a elementos de la física, sino que también hacen referencia a elementos transversales que juegan un papel importante en la completa formación de los alumnos y alumnas. En este proceso no debe olvidarse el carácter experimental de esta ciencia, por eso se propone la utilización de metodologías y herramientas
experimentales, entre ellas la formulación matemática de las leyes y principios, los instrumentos de laboratorio y las herramientas tecnológicas que pueden facilitar la comprensión de los conceptos y fenómenos. Por otro lado, estas competencias también pretenden fomentar el trabajo en equipo y los valores sociales y cívicos para lograr personas comprometidas que utilicen la ciencia para la formación permanente a lo largo de la vida, el desarrollo medioambiental, el bien comunitario y el progreso de la sociedad.  
Párrafo siguiente se reorganiza y se elimina "Aunque aparezcan presentados de este modo, en realidad la ordenación de los bloques no  responde a una secuencia establecida para que el profesorado pueda trabajar de acuerdo a la temporalización más adecuada para las necesidades de su grupo concreto."  
Se cambia "se abarcan los conocimientos, destrezas y actitudes referidos al estudio del" por "llamado"  
Se cambia "partículas másicas" por "los cuerpos debido a su masa"  
Se cambia "comprende los saberes sobre electromagnetismo" por "denominado «Campo electromagnético»"  
Se cambia "El siguiente bloque se refiere a vibraciones y ondas, contemplando" por "«Vibraciones y ondas» es el tercer bloque en el que se encuentra dividida la materia y se centra en el estudio"  
Se añade en último bloque ", «Física relativista, cuántica, nuclear y de partículas»,"  
Se cambian los dos últimos párrafos por otros dos donde aparece STEM y una propuesta de situación de aprendizaje.  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
Se cambia frase "Resulta adecuado que el alumnado perciba la física como una ciencia que evoluciona, y reconozca también que los conocimientos que implica relacionan íntimamente a la física con otras ciencias." por "También se destacará la importancia del respeto y el trabajo en equipo."  
Se cambia varias veces "situación de aprendizaje"/"situaciones de aprendizaje" por "actividad"/"actividades"  



La física, como disciplina que estudia la naturaleza, se encarga de entender y describir el universo, desde los fenómenos que se producen en el microcosmos hasta aquellos que se dan en el macrocosmos. La materia, la energía y las interacciones que se comportan de forma distinta en las diferentes situaciones, hace que los modelos, principios y leyes de la física que el alumnado ha de aplicar para explicar la naturaleza deban ajustarse a la escala de trabajo y a que las respuestas que encuentre sean siempre aproximadas y condicionadas por el contexto. **También se destacará la importancia del respeto y el trabajo en equipo.**  
Los bloques de contenidos en los que se encuentra estructurada la materia de Física de segundo curso de Bachillerato van enfocados a relacionar y completar a los adquiridos en la etapa de Educación Secundaria Obligatoria y a los de la materia Física y Química del primer curso de Bachillerato, de forma que el alumnado pueda adquirir una percepción global de las distintas líneas de trabajo en física y de sus diversas aplicaciones.  
Los dos primeros bloques hacen referencia a la teoría clásica de campos. En el primero de ellos, **llamado** «Campo gravitatorio», se estudiarán, empleando las herramientas matemáticas adecuadas para conferirle al bloque el rigor suficiente, las interacciones que se generan entre **los cuerpos debido a su masa** y, en relación con algunos de los conocimientos de cursos anteriores, su mecánica, su energía y los principios de conservación. A continuación, el segundo bloque **denominado «Campo electromagnético»**, describe los campos eléctrico y magnético, tanto estáticos como variables en el tiempo, y sus características y aplicaciones tecnológicas, biosanitarias e industriales.  
«Vibraciones y ondas» es el tercer bloque en el que se encuentra dividida la materia y se centra en el estudio del movimiento oscilatorio como generador de perturbaciones y su propagación en el espacio-tiempo a través de un movimiento ondulatorio. El estudio se completa con el análisis detallado de la conservación de la energía en las ondas y su aplicación en ejemplos concretos como son las ondas sonoras y las ondas electromagnéticas, lo que abre el estudio de los procesos propios
de la óptica física y la óptica geométrica.  
Con el último bloque **, «Física relativista, cuántica, nuclear y de partículas»,** se muestra el panorama general de la física del presente y el futuro. En él se exponen los conocimientos, destrezas y actitudes de la física cuántica y de la física de partículas. Bajo los principios fundamentales de la física relativista, este bloque explica cómo es la constitución de la materia y la descripción de los procesos que ocurren cuando se estudia ciencia a nivel microscópico. Este bloque permitirá al alumnado aproximarse a las fronteras de la física y abrirá su curiosidad (el mejor motor **del** aprendizaje) al ver que todavía quedan muchas preguntas por resolver y muchos retos que deben ser atendidos desde la investigación y desarrollo de esta ciencia.
**En este proceso no debe olvidarse el carácter experimental de la disciplina y dado que la materia de Física se encuentra comprendida dentro de las disciplinas STEM, se propone la utilización de metodologías y herramientas entre las que se encuentren la formulación matemática de leyes y principios, los instrumentos de laboratorio y las herramientas tecnológicas que puedan facilitar la comprensión de los conceptos y fenómenos. También se trabajarán valores tales como el respeto, el trabajo en equipo, el rechazo hacia actitudes que muestren cualquier tipo de discriminación y el compromiso con la sostenibilidad.**  
**Por tanto, la metodología más apropiada para trabajar esta materia se basará en el planteamiento de actividades contextualizadas y estructuradas, que impliquen el desarrollo de tareas de investigación en las que los contenidos estén integrados de una forma competencial y que permitan al alumnado afianzar e integrar los contenidos de la disciplina. Como orientación para la práctica docente se sugiere la siguiente actividad: a partir del trabajo interdisciplinar la materia de Física con las materias de Matemáticas y Tecnología e Ingeniería, los alumnos crearán una simulación que permita observar visualmente la evolución temporal de la ley de decrecimiento exponencial. Se sugiere que ciertos valores sean parametrizados a fin de comprobar cómo afecta a la velocidad de desintegración la constante de desintegración propia del material. El alumno deberá, asimismo, implementar en la programación de la simulación el uso correcto de la función aleatoria, dando cuenta del carácter estadístico del fenómeno radiactivo. Esta actividad contribuye a desarrollar todas las competencias específicas de la materia.**  

 
## Competencias Específicas  

Comparación proyecto Decreto Madrid frente a Real Decreto:  
En todas se añade "recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril"  
En texto inicial competencia específica 1 se cambia "la sostenibilidad ambiental" por "el medio ambiente", se elimina ", aportando soluciones sostenibles"  
En competencia específica 2 se cambia "capacidad" por "competencia"  
En competencia específica 3 se cambia "alumnos y alumnas" por "alumnado", se elimina "en la participación colaborativa"  
En texto inicial competencia específica 4 se elimina "en el trabajo individual y colectivo"  
En competencia específica 4 se cambia "capacidades" por "destrezas", se elimina ", así como medios para el aprendizaje individual y social. Es necesario, pues, desarrollar la capacidad de utilizar estos recursos de forma autónoma y eficiente para facilitar el aprendizaje autorregulado y al mismo tiempo ser responsable en las interacciones con otros estudiantes y con el profesorado."  
En texto inicial competencia específida 5 se elimina ", para poner en valor el papel de la física en una sociedad basada en valores éticos y sostenibles."  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
Al enumerar competencias específicas se añade un "y" antes de la final, antes había una coma entre todas.  


1. **Utilizar las teorías, principios y leyes que rigen los procesos físicos más importantes, considerando su base experimental y desarrollo matemático en la resolución de problemas, para reconocer la física como una ciencia relevante implicada en el desarrollo de la tecnología, la economía, la sociedad y el medio ambiente.**  
Utilizar los principios, leyes y teorías de la física requiere de un amplio conocimiento de sus fundamentos teóricos. La capacidad de comprender y describir, a través de la experimentación o la utilización de desarrollos matemáticos, las interacciones que se producen entre cuerpos y sistemas en la naturaleza permite, a su vez, desarrollar el pensamiento científico para construir nuevo conocimiento aplicado a la resolución de problemas en distintos contextos en los que interviene la
física. Esto implica apreciar la física como un campo del saber con importantes implicaciones en la tecnología, la economía, la sociedad y la sostenibilidad ambiental.  
De esta forma, a partir de la comprensión de las implicaciones de la física en otros campos de la vida cotidiana, **consigue** formarse una opinión fundamentada sobre las situaciones que afectan a
cada contexto, lo que es necesario para desarrollar un pensamiento crítico y una actitud **adecuada para** contribuir al progreso a través del conocimiento científico adquirido.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM1, STEM2, STEM3 y CD5.  
2. **Adoptar los modelos, teorías y leyes aceptados de la física como base de estudio de los sistemas naturales y predecir su evolución para inferir soluciones generales a los problemas cotidianos relacionados con las aplicaciones prácticas demandadas por la sociedad en el campo tecnológico, industrial y biosanitario.**  
El estudio de la física, como ciencia de la naturaleza, debe proveer de la competencia para analizar fenómenos que se producen en el entorno natural. Para ello, es necesario adoptar los modelos, teorías y leyes que forman los pilares fundamentales de este campo de conocimiento y que a su vez permiten predecir la evolución de los sistemas y objetos naturales. Al mismo tiempo, esta adopción se produce cuando se relacionan los fenómenos observados en situaciones cotidianas con los fundamentos y principios de la física.  
Así, a partir del análisis de diversas situaciones particulares se **aprende a** inferir soluciones generales a los problemas cotidianos, que pueden redundar en aplicaciones prácticas necesarias para la sociedad y que darán lugar a productos y beneficios a través de su desarrollo desde el campo tecnológico, industrial o biosanitario.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM2, STEM5, CPSAA2 y CC4.  
3. **Utilizar el lenguaje de la física con la formulación matemática de sus principios, magnitudes, unidades, ecuaciones, etc., para establecer una comunicación adecuada entre diferentes comunidades científicas y como una herramienta fundamental en la investigación.**  
El desarrollo de esta competencia específica pretende trasladar al **alumnado** un conjunto de criterios para el uso de formalismos con base científica, con la finalidad de poder plantear y discutir adecuadamente la resolución de problemas de física y discutir sus aplicaciones en el mundo **que les rodea**. Además, se pretende que valoren la universalidad del lenguaje matemático y su formulación para intercambiar planteamientos físicos y sus resoluciones en distintos entornos y medios.  
Integrar al alumnado con la comunidad científica requiere de un código específico, riguroso y común que asegure la claridad de los mensajes que se intercambian entre sus miembros. Del mismo modo, con esta competencia específica se pretende atender a la demanda de los avances tecnológicos teniendo en cuenta la conservación del medioambiente.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: CCL1, CCL5, STEM1, STEM4 y CD3.  
4. **Utilizar de forma autónoma, eficiente, crítica y responsable recursos en distintos formatos, plataformas digitales de información y de comunicación para el fomento de la creatividad mediante la producción y el intercambio de materiales científicos y divulgativos que faciliten acercar la física a la sociedad como un campo de conocimientos accesible.**  
Entre las destrezas que deben adquirirse en los nuevos contextos de enseñanza y aprendizaje actuales se encuentra la de utilizar plataformas y entornos virtuales de aprendizaje. Estas plataformas sirven de repositorio de recursos y materiales de distinto tipo y en distinto formato y son útiles para el aprendizaje de la física.  
Al mismo tiempo, la producción y el intercambio de materiales científicos y divulgativos permiten acercar la física de forma creativa a la sociedad, presentándola como un campo de conocimientos accesible.
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM3, STEM5, CD1, CD3 y CPSAA4.  
5. **Aplicar técnicas de trabajo e indagación propias de la física, así como la experimentación, el razonamiento lógico-matemático y la cooperación, en la resolución de problemas y la interpretación de situaciones relacionadas.**  
Las ciencias de la naturaleza tienen un carácter experimental intrínseco. Uno de los principales objetivos de cualquiera de estas disciplinas científicas es la explicación de los fenómenos naturales, lo que permite formular teorías y leyes para su aplicación en diferentes sistemas. El caso de la física no es diferente, y es relevante trasladar **al alumnado** la curiosidad por los fenómenos que suceden en su entorno y en distintas escalas. Hay procesos físicos cotidianos que son reproducibles fácilmente y pueden ser explicados y descritos con los principios y leyes de la física. También hay procesos que, aun no siendo reproducibles, están presentes en el entorno natural de forma generalizada y gracias a los laboratorios virtuales se pueden simular para aproximarse más fácilmente a su estudio.
El trabajo experimental constituye un conjunto de etapas que fomentan la colaboración e intercambio de información, **ambos** muy necesarios en los campos de investigación actuales. Para ello, se debe fomentar en su desarrollo la experimentación y estimación de los errores, la utilización de distintas fuentes documentales en varios idiomas y el uso de recursos tecnológicos. Finalmente, se debe plasmar la información en informes que recojan todo este proceso, lo que permitiría a los
estudiantes formar, en un futuro, parte de la comunidad científica.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM1, CPSAA3.2, CC4 y CE3.  
6. **Reconocer y analizar el carácter multidisciplinar de la física, considerando su relevante recorrido histórico y sus contribuciones al avance del conocimiento científico como un proceso en continua evolución e innovación, para establecer unas bases de conocimiento y relación con otras disciplinas científicas.**  
La física constituye una ciencia profundamente implicada en distintos ámbitos de nuestras vidas cotidianas y que, por tanto, forma parte clave del desarrollo científico, tecnológico e industrial. La adecuada aplicación de sus principios y leyes permite la resolución de diversos problemas basados en los mismos conocimientos, y la aplicación de planteamientos similares a los estudiados en distintas situaciones muestra la universalidad de esta ciencia.  
Los conocimientos y aplicaciones de la física forman, junto con los de otras ciencias como las matemáticas o la tecnología, un sistema simbiótico cuyas aportaciones se benefician mutuamente.
La necesidad de formalizar experimentos para verificar los estudios implica un incentivo en el desarrollo tecnológico y viceversa, el progreso de la tecnología alumbra nuevos descubrimientos que precisan de explicación a través de las ciencias básicas como la física. La colaboración entre distintas comunidades científicas expertas en diferentes disciplinas es imprescindible en todo este desarrollo.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM2, STEM5, CPSAA5 y CE1.  

## Criterios de evaluación 
Comparación proyecto Decreto Madrid frente a Real Decreto:   
En 1.1 se cambia "la sociedad y la sostenibilidad ambiental" por "etc"  
En 4.2 se elimina "y el trabajo individual y colectivo."  
En 6.2 se añade "geología" y se pasan nombres disciplinas a minúsculas  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
Se añaden comas en 4.2  

### Competencia específica 1.  
1.1. Reconocer la relevancia de la física en el desarrollo de la ciencia, la tecnología, la economía, **etc.**, empleando adecuadamente los fundamentos científicos relativos a esos ámbitos.  
1.2. Resolver problemas de manera experimental y analítica, utilizando principios, leyes y teorías de la física.  
### Competencia específica 2.  
2.1. Analizar y comprender la evolución de los sistemas naturales, utilizando modelos, leyes y teorías de la física.  
2.2. Inferir soluciones a problemas generales a partir del análisis de situaciones particulares y las variables de que dependen.  
2.3. Conocer aplicaciones prácticas y productos útiles para la sociedad en el campo tecnológico, industrial y biosanitario, analizándolos en base a los modelos, las leyes y las teorías de la física.  
### Competencia específica 3.
3.1. Aplicar los principios, leyes y teorías científicas en el análisis crítico de procesos físicos del entorno, como los observados y los publicados en distintos medios de comunicación, analizando, comprendiendo y explicando las causas que los producen.  
3.2. Utilizar de manera rigurosa las unidades de las variables físicas en diferentes sistemas de unidades, empleando correctamente su notación y sus equivalencias, así como la elaboración e interpretación adecuada de gráficas que relacionan variables físicas, posibilitando una
comunicación efectiva con toda la comunidad científica.  
3.3. Expresar de forma adecuada los resultados, argumentando las soluciones obtenidas, en la resolución de los ejercicios y problemas que se plantean, bien sea a través de situaciones reales o ideales.  
### Competencia específica 4.
4.1. Consultar, elaborar e intercambiar materiales científicos y divulgativos en distintos formatos con otros miembros del entorno de aprendizaje, utilizando de forma autónoma y eficiente plataformas digitales.  
4.2. Usar de forma crítica, ética y responsable medios de comunicación **,** digitales y tradicionales **,** como modo de enriquecer el aprendizaje.
### Competencia específica 5.
5.1. Obtener relaciones entre variables físicas, midiendo y tratando los datos experimentales, determinando los errores y utilizando sistemas de representación gráfica.  
5.2. Reproducir en laboratorios, reales o virtuales, determinados procesos físicos modificando las variables que los condicionan, considerando los principios, leyes o teorías implicados, generando el correspondiente informe con formato adecuado e incluyendo argumentaciones, conclusiones, tablas de datos, gráficas y referencias bibliográficas.  
5.3. Valorar la física, debatiendo de forma fundamentada sobre sus avances y la implicación en la sociedad desde el punto de vista de la ética y de la sostenibilidad.  
### Competencia específica 6.
6.1. Identificar los principales avances científicos relacionados con la física que han contribuido a **la formulación de** las leyes y teorías aceptadas actualmente en el conjunto de las disciplinas científicas, como las fases para el entendimiento de las metodologías de la ciencia, su evolución constante y su universalidad.  
6.2. Reconocer el carácter multidisciplinar de la ciencia y las contribuciones de unas disciplinas en otras, estableciendo relaciones entre la física y la química, la biología, **la geología** o las matemáticas.

## Contenidos

Comparación proyecto Decreto Madrid frente a Real Decreto:   
En Madrid no se llaman saberes básicos  
Se añaden contenidos en rojo, aquí se marcan en negrita  
En bloque A se reordenan párrafos  
En bloque B se elimina mención explícita a "motores"   
Bloque D se reordena y se amplía bastante. Se añaden "principios" que sería mejor "antecedentes" (en relatividad ya se usa término principios con otro significado)  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
En bloque B se añade mención explícita a "motores" (lo comenté en alegaciones)  


A. Campo gravitatorio.  
- **Estudio de la fuerza gravitatoria. Ley de Gravitación Universal.** Momento angular de un objeto en un campo gravitatorio: cálculo y relación con las fuerzas centrales.  
• **Intensidad del campo gravitatorio creado por una o varias masas.**  
- **Momento angular de una masa respecto a un punto**: cálculo **y** relación con las fuerzas centrales. Aplicación de **la** conservación del momento angular al estudio de**l** movimiento **de un cuerpo en un campo gravitatorio.**  
- Determinación, a través del cálculo vectorial, del campo gravitatorio producido por un sistema de masas. Efectos sobre las variables cinemáticas y dinámicas de objetos inmersos en el campo **gravitatorio.**  
• **Movimiento orbital de satélites, planetas y galaxias.**  
• **Líneas de campo gravitatorio.**  
- Energía mecánica de un objeto sometido a un campo gravitatorio: deducción del tipo de movimiento que posee, cálculo del trabajo o los balances energéticos existentes en desplazamientos entre distintas posiciones, velocidades y tipos de trayectorias.  
• **Carácter conservativo del campo gravitatorio. Trabajo en el campo gravitatorio. Velocidad de escape.**  
• **Potencial gravitatorio creado por una o varias masas. Superficies equipotenciales.**  
- Leyes que se verifican en el movimiento planetario y extrapolación al movimiento de satélites y cuerpos celestes.  
• **Leyes de Kepler.**  
- Introducción a la cosmología y a la astrofísica.  
• Aplicación del campo gravitatorio: implicación de la física en la evolución de objetos astronómicos, en el conocimiento del universo y la repercusión de la investigación en estos ámbitos en la industria, la tecnología, la economía y en la sociedad.  
• **Historia y composición del Universo.**  

B. Campo electromagnético.  
- **Estudios de los** campos eléctrico y magnético: tratamiento vectorial, determinación de las variables cinemáticas y dinámicas de cargas eléctricas libres en presencia de **uno o ambos** campos.  
• **Movimientos de cargas en campos eléctricos y/o magnéticos uniformes.**  
• Fenómenos naturales y aplicaciones tecnológicas en los que se aprecian estos efectos.  
- Intensidad del campo eléctrico en distribuciones de cargas discretas y continuas. **Ley de Coulomb.**  
• Cálculo e interpretación del flujo de campo eléctrico.  
• **Teorema de Gauss. Aplicaciones a esfera y lámina cargadas. Jaula de Faraday.**  
- Energía de una distribución de cargas estáticas: magnitudes que se modifican y permanecen constantes con el desplazamiento de cargas libres entre puntos de distinto potencial eléctrico.  
• **Carácter conservativo del campo eléctrico. Trabajo en el campo eléctrico.**  
• **Potencial eléctrico creado por una o varias cargas. Diferencia de potencial y movimiento de cargas. Superficies equipotenciales.**  
- Campos magnéticos generados por hilos con corriente eléctrica en distintas configuraciones geométricas: rectilíneos, espiras, solenoides o toros. **Intensidad del campo magnético. Fuerza de Lorentz. Fuerza magnética sobre una corriente rectilínea. Momento de fuerzas sobre una espira.**  
• Interacción con cargas eléctricas libres presentes en su entorno.  
• **Interacción entre conductores rectilíneos y paralelos.**  
• **Ley de Ampère.**  
- Líneas de campo eléctrico y magnético producido por distribuciones de carga sencillas, imanes e hilos con corriente eléctrica en distintas configuraciones geométricas.  
- **Flujo de campo magnético.** Generación de la fuerza electromotriz **inducida**: funcionamiento de motores, generadores y transformadores a partir de sistemas donde se produce una variación del flujo magnético.  
• **Ley de Faraday- Henry.**  
• **Ley de Lenz.**  
• **Generación de corriente alterna. Representación gráfica de la fuerza electromotriz en función del tiempo.**  

C. Vibraciones y ondas.  
- Movimiento oscilatorio: variables cinemáticas de un cuerpo oscilante. **Energía cinética y potencial del movimiento armónico simple** y conservación de energía en estos sistemas. **Representación gráfica en función del tiempo.**  
- Movimiento ondulatorio: gráficas de oscilación en función de la posición y del tiempo, ecuación de onda que lo describe y relación con el movimiento armónico simple.  
• **Velocidad de propagación y de vibración. Diferencia de fase.**  
• Distintos tipos de movimientos ondulatorios en la naturaleza.  
- Fenómenos ondulatorios: situaciones y contextos naturales en los que se ponen de manifiesto distintos fenómenos ondulatorios y aplicaciones.  
- **Estudio de las** ondas sonoras: **mecanismos de formación y velocidad de las mismas.**  
• Cualidades **del sonido. Intensidad sonora. Escala decibélica.**  
• Cambios en las propiedades de las ondas en función del desplazamiento del emisor y receptor: **el efecto Doppler.**  
• **Aplicaciones tecnológicas del sonido.**  
- Naturaleza de la luz: controversias y debates históricos **sobre los modelos ondulatorio y corpuscular**. La luz como onda electromagnética.  
• Espectro electromagnético. **Aplicaciones de ondas electromagnéticas del espectro no visible.**  
• **Velocidad de propagación de la luz. Índice de refracción.**  
• **Fenómenos luminosos: Reflexión y refracción de la luz y sus leyes. Estudio cualitativo de la dispersión, interferencia, difracción y polarización.**  
• **Aplicaciones tecnológicas de estos fenómenos.**  
- Formación de imágenes en medios y objetos con distinto índice de refracción. Sistemas ópticos: lentes delgadas, espejos planos y curvos. **Aplicaciones tecnológicas: el microscopio y el telescopio.**  
• **Óptica de la visión. Defectos visuales.**  

D. Física relativista, cuántica, nuclear y de partículas.  
1. **Principios de la Relatividad.**  
- **Sistemas de referencia inercial y no inercial.**  
- **La Relatividad en la Mecánica Clásica.**  
- **Limitaciones de la física clásica.**  
• **Experimento de Michelson-Morley.**
- **Mecánica relativista:** principios fundamentales de la relatividad especial y sus consecuencias.  
• **Postulados de Einstein.**  
• Contracción de la longitud y dilatación del tiempo.  
• Masa y energía relativistas.  
2. **Principios de la física cuántica.**  
- **Otras limitaciones de la física clásica: radiación del cuerpo negro,** efecto fotoeléctrico **y espectros atómicos. Trabajo de extracción y energía cinética de los fotoelectrones en el efecto fotoeléctrico.**  
- **Mecánica cuántica.**  
• Dualidad onda-corpúsculo y cuantización. Hipótesis de De Broglie.  
• Principio de incertidumbre formulado en base **a la posición y el momento lineal y** al tiempo y la energía.  
• **Aplicaciones de la física cuántica.**  
3. Núcleos atómicos.  
- Radiactividad natural y otros procesos nucleares.  
• **Tipos de radiaciones y desintegración radiactiva. Leyes de Soddy y Fajans.**  
- Núcleos atómicos y estabilidad de los isótopos.  
• **El núcleo atómico: fuerzas nucleares y energía de enlace.**  
• **Reacciones nucleares.**  
• **Leyes de la desintegración radiactiva. Actividad en una muestra radiactiva.**  
• **Efectos de las radiaciones. Riesgos y** aplicaciones en el campo de la ingeniería, la tecnología y la salud. **Datación de fósiles y medicina nuclear.**  
4. Física de partículas **e interacciones fundamentales.**  
- Modelo estándar en la física de partículas. Clasificaciones de las partículas fundamentales.  
- Las interacciones fundamentales como procesos de intercambio de partículas (bosones).  
- **Interacciones fundamentales: gravitatoria, electromagnética, nuclear fuerte y nuclear débil.**  
- **Aceleradores de partículas.**  
- **Fronteras y desafíos de la física.**  

