
# Materiales autosuficientes

## Ideas generales sobre materiales autosuficientes
  
*Se llama material autosuficiente al conjunto de recursos didácticos que contienen toda la información, secuencia y procesos necesarios para aprender un contenido específico de forma autónoma, sin la ayuda de un docente, tutor o asesor (definición de Elena Barberà y Mª José Rochera (2009), en Psicología de la Educación Virtual. Capítulo VII: Los entornos virtuales de aprendizaje basados en el diseño de materiales autosuficientes y el aprendizaje autodirigido. Editorial Morata)*  
Definición tomada del curso "La competencia digital en Secundaria" de Ángeles Puertas Serrano, Departamento TIC del CRIF Las Acacias de Madrid, Material con licencia cc-by-nc-sa  
  
Los materiales autosuficientes no son meros  [apuntes](/home/recursos/apuntes) , sino que tienen actividades para practicar y ejercitar lo aprendido. Los ejemplos tipo de materiales autosuficientes son actividades JClic, Hotpotatoes, actividades interactivas (páginas web, eXeLearning, ...)  
  
La clasificación a veces es difusa, de modo que hay recursos que se pueden clasificar como  [cursos online](/home/recursos/cursos-online-interactivos)  o como materiales autosuficientes.  

## Ejemplos de materiales autosuficientes

   * Iniciación interactiva a la materia  
 [Portada](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/index.html)   
Mariano Gaite Cuesta. Premio CNICE 2005. Material con derechos de autor. En su aviso legal indica "Todas las simulaciones, modelos interactivos, juegos y actividades interactivas contenidas en estos materiales son propiedad del autor de los mismos. No está permitido su uso en ningún modo fuera de estos materiales denominados "Iniciación interactiva a la materia", ni su inclusión en ningún otro material en soporte electrónico: aplicación informática, página web o cualquier otro, sin la previa autorización escrita del autor."
   * Unidades didácticas interactivas de Física y Química.  
 [VerEspecial.asp](http://www.fq.profes.net/comun/VerEspecial.asp?id_contenido=33748)   
Profes.net / Librosvivos.net. Editorial SM.   
   * Hyperphysics  
 [HyperPhysics Concepts](http://hyperphysics.phy-astr.gsu.edu/hbase/hph.html)  (inglés) y  [HyperPhysics Concepts](http://hyperphysics.phy-astr.gsu.edu/hbasees/hph.html)  (español "en marcha" en 2012)  
Gran cantidad de mapas conceptuales, curso /material para aprender y consultar.  
Material con copyright, Carl R. (Rod) Nave, Department of Physics and Astronomy, Georgia State University. Consultado en enero 2012.
   * Zona Land Education. Education in Physics and Mathematics  
 [Physics, Math Home | Zona Land Education](http://zonalandeducation.com/index.html)   
Copyright 2011. Todos los derechos reservados. Apuntes y animaciones VRML
   * Telesecundaria. Segundo Grado. Ciencias II (énfasis en física)  
 [index.html](http://arquimedes.matem.unam.mx/Vinculos/Secundaria/2_segundo/2_Fisica/index.html)   
 Telesecundaria. Tercer grado. Ciencias III (énfasis en química)  
 [index.html](http://arquimedes.matem.unam.mx/Vinculos/Secundaria/3_tercero/3_Quimica/INTERACTIVOS/index.html)   
 Instituto Latinoamericano de la Comunicación Educativa. Licenciamiento no detallado. Algunas partes podrían ser clasificables como "simulaciones"
   * Curso de física básica  
 [default.htm](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/default.htm)   
Cinemática, dinámica, termodinámica ... conceptos, cuestionarios, problemas resueltos y animaciones. Licenciamiento no detallado. Página realizada por Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM) - España.
   * La manzana de Newton  
 [principal.html](http://www.lamanzanadenewton.com/principal.html)   
Uso libre pero con restricciones. Rafael Jiménez y Mª Pastora Torres. Descrito en  [803-la-manzana-de-newton](http://recursostic.educacion.es/buenaspracticas20/web/es/difundiendo-buenas-practicas/803-la-manzana-de-newton) 
   * Física y Química 3º y 4º ESO. edad (Enseñanza Digital A Distancia).  
 [Física y Química](http://recursostic.educacion.es/secundaria/edad/index_fq.htm)   
Enlaces a cursos Newton. cc-by-nc-sa

### Química

   * Proyecto Ulloa (Recursos para química)  
 [Proyecto Ulloa](http://recursostic.educacion.es/ciencias/ulloa/web/)   
cc-by-nc-sa
   * Formulación y Nomenclatura de Química Inorgánica y Orgánica  
 [Formulacin de Qumica Inorgnica y Orgnica, de Carlos Alonso. Nomenclature of Inorganic and Organic Chemistry](http://www.alonsoformula.com/)  Carlos Alonso. Material con licenciamiento no detallado.
   * Formulación de química inorgánica interactiva  
 [quim_ino.html](http://www.latizavirtual.org/quimica/quim_ino.html)  J.Carlos Espinosa y José Manuel Romero. Material con licenciamiento no detallado. Incluye Teoría, Actividades, Evaluación.
  
