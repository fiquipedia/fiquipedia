
# Prácticas / experimentos / laboratorio con smartphone

Aquí se ponen solo las prácticas asociadas a smartphone, adicionales a las que [prácticas y experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio) generales.  

Los smartphone cada vez más se parecen a un PC: puede haber algunos elementos relacionados con aplicaciones para PC (por ejemplo análisis de vídeo con programa en PC y/o con app en smartphone)

Es una idea interesante cuando no es posible ir al laboratorio, y permite realizar algunas prácticas en casa.  
Asociados al [espectro](/home/recursos/recursos-espectro)  también se pueden hacer cosas con el móvil  

[14 sensores que encontrarás en tu móvil: cómo funcionan y para qué sirven - xataka.com](https://www.xataka.com/basics/sensores-que-encontraras-tu-movil-como-funcionan-sirven)  
 
[Tu iPhone o iPod Touch como péndulo para prácticas de Física - francis.naukas.com](http://francis.naukas.com/2013/04/29/tu-iphone-o-ipod-touch-como-pendulo-para-practicas-de-fisica/)  
 Francisco R. Villatoro  

[twitter LebreroPastor/status/1650769593684688896](https://twitter.com/LebreroPastor/status/1650769593684688896)  
Os dejo por aquí una breve recopilación de prácticas de laboratorio que se pueden hacer con el móvil. Las usamos como introducción a la investigación en 4º ESO.  
[Informe 06 Física con el móvil  IES Castilla. Soria](https://drive.google.com/file/d/1sIcPFGE-Y4nU3IY4MsZurZpRKQHeqyzg/view)  


Tracker Video Analysis and Modeling Tool  
written by Douglas Brown  
The Tracker Video Analysis and Modeling Tool allows students to **model and analyze the motion of objects in videos**.  
By overlaying simple dynamical models directly onto videos, students may see how well a model matches the real world.  
[Tracker Video Analysis and Modeling Tool for Physics Education](http://physlets.org/tracker/)  
[Tracker Video Analysis and Modeling Tool](http://www.opensourcephysics.org/items/detail.cfm?ID=7365)   
Opción online
[Tracker online](https://physlets.org/tracker/trackerJS/)  

Ejemplo  
[twitter ProfaDeQuimica/status/916234359589490688](https://twitter.com/ProfaDeQuimica/status/916234359589490688)  

[Sigmund Video Analysis](https://noragulfa.com/sigmund/)  
> This app is designed to allow the video tracking of a moving object, like a tossed ball. The basic idea is simple: load a movie, use an object in the video to set the scale, and then click on the moving object in successive video frames to get position vs. time data.  

Aunque indica app, la interfaz es web. Una vez cargado el vídeo y marcados los puntos, se lanza [nPlot](https://noragulfa.com/nPlot/)  

[VidAnalysis - play.google.com](https://play.google.com/store/apps/details?id=com.vidanalysis.free)  
![](https://play-lh.googleusercontent.com/BwatPLwswmnIX1TT2fdIIC_rU1hLb04lAY1lmCn8REj8tLaHnbITDFy-WjPVPYyAzGSF=w526-h296)  
![](https://play-lh.googleusercontent.com/fr0CZ6390AQdO3W_GfEXtsMnZkjUz0t-Frs_ooo8wwr7YMpKBMSNvc5ubCQ-SZ_iU6M=w526-h296)  
Fuente alternativa  
[VidAnalysis aptoide](https://vidanalysis-free.es.aptoide.com/app)  

[Kinovea](https://www.kinovea.org/)  
Kinovea is a video annotation tool designed for sport analysis.
It features utilities to capture, slow down, compare, annotate and measure motion in videos. 
Kinovea is completely free and open source.

[Vernier Graphical Analysis - play.google.com](https://play.google.com/store/apps/details?id=com.vernier.android.graphicalanalysis&hl=es)  
![](https://play-lh.googleusercontent.com/JtlzCB4hWAvea7uUXCLLjGI_Ajkhm_RU2WwNqZx7OcQhGxIHjoQkWXYDbh8RrPgDvBo=w526-h296)  

 
[Física: El principio de equivalencia o cómo resolver el problema de medir ángulos y aceleraciones reales con Smartphones - fisicamartin.blogspot.com.es ](https://fisicamartin.blogspot.com/2014/08/fisica-el-problema-de-medir-angulos-y_25.html)  
 Martín Monteiro, Cecilia Cabeza, Arturo C Martí  
   
 [Exploring phase space using smartphone acceleration and rotation sensors simultaneously - iopscience.iop.org](http://iopscience.iop.org/0143-0807/35/4/045013)  
 Martín Monteiro, Cecilia Cabeza, Arturo C Martí  
  
[Rotational energy in a physical pendulum - scitation.aip.org](http://scitation.aip.org/content/aapt/journal/tpt/52/3/10.1119/1.4865529)  
> ABSTRACT
Smartphone usage has expanded dramatically in recent years worldwide. This revolution also has impact in undergraduate laboratories where different experiences are facilitated by the use of the sensors usually included in these devices. Recently, in several articles published in the literature,1,2 the use of smartphones has been proposed for several physics experiments. Although most previous articles focused on mechanical experiments, an aspect that has received less attention is the use of rotation sensors or gyroscopes. Indeed, the use of these sensors paves the way for new experiments enabling the measurement of angular velocities. In a very recent paper the conservation of the angular momentum is considered using rotation sensors.3 In this paper we present an analysis of the rotational energy of a physical pendulum.  
 
[Angular velocity and centripetal acceleration relationship](http://scitation.aip.org/content/aapt/journal/tpt/52/5/10.1119/1.4872422)  
 Martín Monteiro, Cecilia Cabeza, Arturo C. Marti, Patrik Vogt, Jochen Kuhn  
 
Otra idea asociada a móvil:  
DECO, the cosmic-ray detector in your pocket  
WIPAC = Wisconsin IceCube Particle Astrophysics Center  
[http://wipac.wisc.edu/learn](http://wipac.wisc.edu/learn)  
[Convierte tu móvil en detector de rayos cósmicos- europapress.es](http://www.europapress.es/ciencia/laboratorio/noticia-fisicos-lanzan-app-hacer-movil-detector-rayos-cosmicos-20141007181015.html)  

[II Jornadas de Enseñanza de la Física para profesores de Educación Secundaria. 30 noviembre de 2017 - rsef.es](https://rsef.es/noticias-actividades-geef/item/1019-ii-jornadas-de-ensenanza-de-la-fisica-30-noviembre-de-2017)  
Incluye materiales  
[Experimentos con el acelerómetro del teléfono móvil: propuestas para ESO y Bachillerato. Manuel Iván González](https://rsef.es/images/Fisica/MIvanGonzalez.pdf)  
Ideas aparte de los experimentos mostrados:  
- MAS: colgar el móvil de un muelle en un soporte universal y hacerlo oscilar (oscilará en eje y, dará lectura en g y habrá que restar 1, luego multiplicar por 9,8 para tener datos en m/s^2)  
- El plano inclinado se puede usar como inclinómetro: con la lectura sin movimiento calcular el ángulo como g·sen(alfa)  
- Calibrar: aparte de en el móvil, se puede hacer procesando los datos si se ve que en cierta situación hay un valor constante que debería tener cierto valor (0 ó 1 por ejemplo), restando o sumando el ajuste para que de el valor asociado a estar calibrado.  
- Experimentos midiendo a en péndulo es complicado, porque a suele ser pequeña en régimen de pequeñas oscilaciones.  
- Con datos de gráficas a-t se puede calcular velocidad integrando (el ajuste es importante), y luego obtener distancia integrando otra vez (en esta segunda integración hay mucho error)  
- Se puede limpiar ruido en los datos haciendo una "media móvil", promediando cada dato con los 2 ó 3 anteriores y 2 ó 3 siguientes.  


[Otras dos maneras de usar el móvil en el aula y el laboratorio. Francisco Barradas Solas](https://rsef.es/images/Fisica/movil-labo-FBarradas.pdf)  

[Investigaciones sobre ondas con móvil. Patricio Gómez](https://rsef.es/images/Fisica/movil.pdf)  

[SMARTPHYSICS: Experimenta con los Sensores de tu Smartphone - lafisicadelgrel.blogspot.com.es](https://lafisicadelgrel.blogspot.com.es/2017/12/smartphysics-experimenta-con-los.html)   

[SmartPhysics - upv.es/](http://smartphysics.webs.upv.es/)  

[Un empollón en mi ascensor - fuga.naukas.com](https://fuga.naukas.com/2018/02/06/un-empollon-en-mi-ascensor/)  

[Experimento: Midamos el movimiento del ascensor con el móvil - lacienciaparatodos.wordpress.com ](https://lacienciaparatodos.wordpress.com/2020/02/21/experimento-midamos-el-movimiento-del-ascensor-con-el-movil/)   

[twitter physicstoolbox/status/1706811819635970414](https://twitter.com/physicstoolbox/status/1706811819635970414)  
Measuring the Capacitor Charge and Discharge with a LED and a Smartphone  
This arXiv pre-print paper presents the use of a circuit board and light emission from an LED to quantitatively study capacitors.  
![](https://pbs.twimg.com/media/F6_QOPbWkAAZv-2?format=jpg)  
[Measuring the capacitor charge and discharge with a LED and a smartphone](https://arxiv.org/abs/2305.13094)  
In this article, we present a simple, inexpensive, and effective method for measuring the capacitor charge and discharge processes using a light-emitting diode (LED) and the light meter of a smartphone. We propose a simple circuit in which the LED's brightness is linear on the capacitor's voltage, allowing us to use the smartphone to monitor the capacitor state accurately. The method is tested experimentally, giving highly satisfactory results. Its exceptional combination of accuracy, minimal requirements, and ease of setup makes it an excellent way to introduce undergraduate students to the concepts of electricity and electronics in any educational setting.   


## #LaboratorioGorila (Arturo Quirantes)  

[twitter elprofedefisica/status/1442428831864811523](https://twitter.com/elprofedefisica/status/1442428831864811523)  
Hilo oficial del #LaboratorioGorila

[Lista de distribución youtube](https://youtube.com/playlist?list=PLgMMj_N_55qyvxVCfaY5bsBuQlOchjINH)   

[Contacto email](mailto:laboratoriogorila@elprofedefisica.es)  

Experimento 0: PRESENTACIÓN  
Se explica cómo familiarizarse con el uso de un móvil para medir variables físicas de interés (intensidad de luz, sonido, aceleración, velocidad angular…)  

Vídeo: [![](https://img.youtube.com/vi/mYb2Jsumm_Y/0.jpg)](https://www.youtube.com/watch?v=mYb2Jsumm_Y)  
Hoja de resultados: [https://tiny.cc/r6fjuz](https://tiny.cc/r6fjuz)  

Experimento 1: CAÍDA LIBRE  
Un objeto cae al suelo, medimos y hallamos la aceleración. ¿Sencillo? No cuando necesitamos precisión de milisegundos. ¿Podremos conseguirlo con un móvil?  

Vídeo: [![](https://img.youtube.com/vi/tkoImaSy7IY/0.jpg)](https://www.youtube.com/watch?v=tkoImaSy7IY)  
Hoja de resultados: [http://tiny.cc/51wjuz](http://tiny.cc/51wjuz)  

Experimento 2: INTENSIDAD Y DISTANCIA  
La luz de una linterna se debilita conforma te alejas. ¿Cómo depende de la distancia? Descúbrelo en casa con un móvil, una luz y una regla.  

Vídeo: [![](https://img.youtube.com/vi/To8OVey_0l4/0.jpg)](https://www.youtube.com/watch?v=To8OVey_0l4)  
Hoja de resultados: [http://tiny.cc/3pdjuz](http://tiny.cc/3pdjuz)  

Experimento 3: MOVIMIENTO ARMÓNICO 2 (EL PÉNDULO)  
Todos habéis hecho experimentos con el péndulo en clase de prácticas, pero nunca de este tipo.  
(Y sí, he puesto primero el 2 ¿y qué?)  

Vídeo: [![](https://img.youtube.com/vi/uHs20kb6LMY/0.jpg)](https://www.youtube.com/watch?v=uHs20kb6LMY)  
Hola de resultados: [https://tiny.cc/24fjuz](https://tiny.cc/24fjuz)  

Experimento 4: MOVIMIENTO ARMÓNICO 1 (EL MUELLE)  
El muelle da juego cuando le cuelgas un objeto y lo sacas de su posición de equilibrio, y MUCHO más cuando usas un móvil para medirlo.  

Vídeo: [![](https://img.youtube.com/vi/ztIrsjkzGis/0.jpg)](https://www.youtube.com/watch?v=ztIrsjkzGis)  
Hoja de resultados: [https://tiny.cc/p4fjuz](https://tiny.cc/p4fjuz)  

Experimento 5: SONIDO 1 - CONCEPTOS BÁSICOS  
¿Tienes idea de lo que un móvil puede hacer con el sonido? Comienza a averiguarlo con este vídeo  

Vídeo: [![](https://img.youtube.com/vi/2GiGDN75j-8/0.jpg)](https://www.youtube.com/watch?v=2GiGDN75j-8)  
Hoja de resultados: [https://tiny.cc/rodjuz](https://tiny.cc/rodjuz)  

Experimento 6: SONIDO2 - INTERFERENCIA Y BATIDO  
¿Harto de que te expliquen el fenómeno de la interferencia de ondas? Pues saca el móvil del bolsillo y compruébalo tú mismo  

Vídeo: [![](https://img.youtube.com/vi/GZYvQp5Pd3A/0.jpg)](https://www.youtube.com/watch?v=GZYvQp5Pd3A)  
Hoja de resultados: [https://tiny.cc/6odjuz](https://tiny.cc/6odjuz)  

Experimento 7: SONIDO 3 - EFECTO DOPPLER  
Pues sí, también podemos medir el efecto Doppler con un móvil, e incluso determinar la velocidad del objeto que emite el sonido.   ¡Inténtalo!  

Vídeo: [![](https://img.youtube.com/vi/roagBdKhK_g/0.jpg)](https://www.youtube.com/watch?v=roagBdKhK_g)  

Experimento 8: SONIDO 4 - VELOCIDAD DEL SONIDO  
¿Cómo podemos medir algo que viaja a 340 metros por segundo? Con un móvil, por supuesto. Hacer palmas también ayuda  

Vídeo: [![](https://img.youtube.com/vi/2x0tGGrZEGk/0.jpg)](https://www.youtube.com/watch?v=2x0tGGrZEGk)  

Hoja de resultados: [https://tiny.cc/dmdjuz](https://tiny.cc/dmdjuz)  


