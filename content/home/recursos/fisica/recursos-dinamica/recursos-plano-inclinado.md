
# Recursos plano inclinado

El plano inclinado se trata de dos maneras en el currículo:  
  
- Como  [máquina simple](/home/recursos/fisica/maquinas-simples)  en 2º ESO LOMCE y 3º ESO LOMCE  
- Dentro de la dinámica como problema de representación y cálculo de fuerzas y aceleración: en currículo LOE no era obligatorio hasta 1º Bachillerato, pero con la LOMCE se introduce en 4º ESO como obligatorio. Es posible / habitual combinarlo con muelles y poleas  

A veces al hablar de plano inclinado surge la inclinación en porcentaje  
[La pendiente de una carretera - geogebra.es](https://geogebra.es/gauss/materiales_didacticos/eso/actividades/geometria/trigonometria/pendiente_carretera/actividad.html)  
![](https://geogebra.es/gauss/materiales_didacticos/eso/actividades/geometria/trigonometria/pendiente_carretera/img/carreteraconpendiente.png)  

[twitter cossettej/status/1554933373826527232](https://twitter.com/cossettej/status/1554933373826527232)  
Introducing forces on ramps with a class investigation trying to answer the question "What does an electronic balance measure?"  
Super simple and surprisingly effective demo to introduce a traditionally challenging topic 📐  
Blog Link: [The angled balance](https://passionatelycurioussci.weebly.com/blog/angled-balance)  
> Sometime in the middle/end of our forces unit, my students walk into the room with electronic balances set out on every table.  
I start class by asking the simple question "What does an electronic balance (or bathroom scale) measure?" and have the students discuss in pairs or groups before bringing the discussion to the full class.  
[![](https://img.youtube.com/vi/7WT6lk_XSgw/0.jpg)](https://www.youtube.com/watch?v=7WT6lk_XSgw "The Angled Balance - 1. Passionately Curious")   
[![](https://img.youtube.com/vi/6C-xbc0X6ug/0.jpg)](https://www.youtube.com/watch?v=6C-xbc0X6ug "The Angled Balance - 2. Passionately Curious")   

## Laboratorio y simulaciones

Ver páginas asociadas a [prácticas y experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio/) y a [simulaciones](/home/recursos/simulaciones/)   

[Determinación del coeficiente de rozamiento entre una lata de conservas y un plano inclinado. 4º ESO - miquelquiroga](https://miguelquiroga.es/materiales/determinacin-del-coeficiente-de-rozamiento-entre-una-lata-de-conservas-y-un-plano-inclinado)  

[Fuerzas en el plano inclinado - geogebra](https://www.geogebra.org/m/kRDTwEcv#material/CcSJy4Wx) Fernando Martínez  
[Descomposición de fuerzas en un plano inclinado. - geogebra](https://www.geogebra.org/m/kRDTwEcv#material/fwBuf7Hy)     Jesús Benayas Yepes  
[Plano Inclinado - geogebra](https://www.geogebra.org/m/kRDTwEcv#material/PGMXYVCR) Diego Borja, con una polea  

[Plano inclinado con una polea - geogebra](https://www.geogebra.org/m/kRDTwEcv#material/Rg3cJRe8)     Fernando Martínez   
Si se pone ángulo 0 sirve como plano horizontal con masa colgando.  

[Plano inclinado - 1 objeto - geogebra](https://www.geogebra.org/m/XB3S5nEB) Enrique García de Bustos Sierra  
[Plano inclinado - 2 objetos - geogebra](https://www.geogebra.org/m/pSapQcQm) Enrique García de Bustos Sierra  


 [Un bloque desliza a lo largo de un plano inclinado y deforma un muelle](http://www.sc.ehu.es/sbweb/fisica/dinamica/trabajo/plano_inclinado/plano_inclinado.htm)   
 [La rampa - PhET](https://phet.colorado.edu/es/simulations/the-ramp) HTML5  
 ![](https://phet.colorado.edu/sims/the-ramp/the-ramp-600.png)  
 [La rampa: fuerzas y movimiento - PhET](https://phet.colorado.edu/es/simulation/ramp-forces-and-motion) HTML5  
 ![](https://phet.colorado.edu/sims/motion-series/ramp-forces-and-motion-600.png)  
 [Inici | fislab](http://fislab.net/)  incluye en applets simulación plano inclinado donde variar parámetros (velocidad inicial, ángulo) y visualizar vectores (normal, peso...)  
Enlace directo (necesita java)  [:: FisLab.net :: Tavi Casellas ::](http://www.xtec.cat/~ocasella/applets/plaincl/appletsol2.htm) En mayo 2018 fislab pasa las simulaciones a ficheros .jar que funcionan sin las limitaciones de applets:  [plainclinat.jar](http://www.xtec.cat/~ocasella/applets/plainclinat.jar)   
  
 [Fuerzas y Movimiento en un Plano Inclinado | Fisicalab](https://www.fisicalab.com/apartado/fuerzas-planos-inclinados) Tiene apartado "Experimenta y Aprende" donde aparece diagrama y permite cambiar m, ángulo y coeficiente y da valores.  

[Descomposición del peso en un plano inclinado - educaplus.org](https://www.educaplus.org/game/descomposicion-del-peso-en-un-plano-inclinado)  
  
 [Plano Inclinado](http://www.walter-fendt.de/html5/phes/inclinedplane_es.htm)  (HTML5)  
GENMAGIC.ORG Banco de Objetos Multimedia Educativos  

[Plano inclinado. Generador de problemas](https://www.geogebra.org/m/lsGLrCg1)  Vicente Martín Morales, Geogebra 

Generador de planos inclinados / fuerzas  
 [GENMAGIC.ORG - Information](http://www.genmagic.net/repositorio/displayimage.php?album=11&pos=9)   
  
 [Leyes de Newton – GeoGebra](https://www.geogebra.org/m/zwgUVAvK) Leyes de Newton  
Autor: Jhon Atencio  
Dos masas unidas a través de una poleaPermite modificar y ver valores  

En cuanto a dibujarlos, yo suelo usar inkscape, en 2022 cito ideas en [imágenes vectoriales cliparts](/home/recursos/imagenes-vectoriales-cliparts)   

