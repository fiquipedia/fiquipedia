
# Pruebas evaluaciones nacionales

Me centro en pruebas asociadas a ciencias  

Algunas surgen asociadas a normativa estatal, y otras son propias de cada comunidad autónoma.  

[Evaluaciones nacionales. INEE (Instituto Nacional de Evaluación Educativa)](http://educalab.es/inee/evaluaciones-nacionales)  
En esa página actualizada en 2017 se citan  
    Evaluaciones en la LOMCE  
    Evaluaciones de Educación Infantil  
    Evaluaciones de Educación Primaria  
    Evaluaciones de Educación Secundaria  
    Evaluaciones de Diagnóstico de Ceuta y Melilla  
    Prueba piloto de la evaluación de la expresión oral en lengua inglesa en la PAU  

Sobre evaluaciones en LOMCE, ver [Evaluaciones Finales, "reválidas" (surgen con LOMCE y desaparecen con LOMLOE)](/home/legislacion-educativa/lomce/evaluaciones-finales)  

En evaluación 3º primaria no hay ciencias  
[Evaluación final 6º primaria](http://educalab.es/inee/evaluaciones-nacionales/evaluacion-6curso-primaria)  
[Evaluación final 4º ESO](http://educalab.es/inee/evaluaciones-nacionales/evaluacion/4eso)  

Por ejemplo en Pruebas oficiales evaluación final primaria del curso 2015/2016,  MECD (Ceuta, Melilla, centros en el exterior y CIDEAD –Centro para la Innovación y Desarrollo de la Educación a Distancia–), Castilla y León, Comunidad de Madrid y La Rioja,  [Prueba de competencias básicas en ciencia y tecnología](http://www.educacionyfp.gob.es/dctm/inee/evaluacionfinalprimaria/pruebasinee201617/cct20166epcienciatecnologia.pdf?documentId=0901e72b821b3cf1)   
pregunta 4 (6CT06) asociada a separación mezclas, pregunta 6 (6CT09) asociada a concepto densidad y flotación  

[EVALUACIóN DE DIAGNóSTICO PISA Y OTRAS EVALUACIONES - educarm](https://servicios.educarm.es/portal/admin/webForm.php?aplicacion=EDUCARM_MUNDO_MATEMATICO&mode=visualizaAplicacionWeb&web=158&ar=1111&liferay=1&zona=EDUCARM#this)  
Incluye evaluaciones de diagnóstico de las CCAA  
