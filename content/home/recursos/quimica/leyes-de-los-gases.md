
# Leyes de los gases

En el  [currículo oficial de 3º ESO LOE](/home/materias/eso/fisica-y-quimica-3-eso/curriculofisicayquimica-3eso)  no se citaban expresamente, es un contenido que se suele introducir en 3º ESO (citando Ley de Boyle y Ley de Charles y Gay-Lussac) junto con la [teoría cinético-molecular](/home/recursos/recursos-teoria-cinetica) , que sí es citada expresamente.  
Las leyes de los gases son un ejemplo de situación que puede ser explicada con esta teoría.  
En 4º ESO y en 1º Bachillerato se ve la ley de los gases ideales, y en Química de 2º se asume.  
Solamente se tratan gases ideales, no reales.  
Por "tradición" se suelen explicar primero las tres leyes clásicas (T=cte, V=cte, P=cte), y luego la ley de los gases ideales que unifica las tres.  

Física y Química 3º ESO:  
[Laboratorio virtual: Leyes de los gases según la TCM.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq3eso/mov_gas_tcm.swf) Jose Antonio Navarro Dominguez   

[Gas Laws Simulation - teachchemistry.org](https://teachchemistry.org/classroom-resources/the-gas-laws-simulation)  
AACT American Association of Chemistry Teachers  

[Relación entre presión, temperatura y volumen - geogebra](https://www.geogebra.org/m/vVku72Sg)  
  * [Relación de presión y volumen - geogebra](https://www.geogebra.org/m/vVku72Sg#material/cT4vpa5B)  
  * [Relación de temperatura y presión - geogebra](https://www.geogebra.org/m/vVku72Sg#material/ZBmQf3sS) >> Varía la cantidad de gas y la represantación no es clara   
  * [Relación de temperatura y volumen - geogebra](https://www.geogebra.org/m/vVku72Sg#material/WxzBzZrD)  

  
[Proyecto Ulloa (Recursos para Química). Los gases, 3º ESO (Flash)](http://recursostic.educacion.es/ciencias/ulloa/web/ulloa2/3eso/secuencia4/menu.html)  
cc-by-nc-sa   

[![](https://phet.colorado.edu/sims/html/gas-properties/latest/gas-properties-900.png)](https://phet.colorado.edu/es/simulation/gas-properties "Propiedades de los Gases") Propiedades de los Gases, PhET, HTML5

Recursos tomados de  [CienciaAragón 3.2. Estructura de la materia *waybackmachine*](http://www.catedu.es/cienciaragon/index.php?option=com_content&task=view&id=60&Itemid=44)  
Son ejecutables (ficheros .exe que también funcionan sobre linux con wine)  
Ejemplificación de las leyes de Avogadro, Boyle, Charles y Gay-Lussac. Quedan muy claras las variables que intervienen en cada ley, aunque sólo ejemplifica dos posiciones.  
[Ley de Avogadro (exe) *waybackmachine*](http://web.archive.org/web/20100618030904/http://iesdmjac.educa.aragon.es/PortalFQ/anima/AvogadroLaw.exe)  
[Ley de Avogadro (flash) *waybackmachine*](http://web.archive.org/web/20170930200123/http://www.dlt.ncssm.edu/tiger/Flash/gases/Avogadro'sLaw.html)  
[Ley de Boyle (exe) *waybackmachine*](http://web.archive.org/web/20150620014803/http://iesdmjac.educa.aragon.es/PortalFQ/anima/BoyleLaw.exe)   
[Ley de Boyle (flash) *waybackmachine*](http://web.archive.org/web/20170930200233/http://www.dlt.ncssm.edu/tiger/Flash/gases/BoylesLaw.html)  
[Ley de Charles (flash) *waybackmachine*](http://web.archive.org/web/20170930200242/http://www.dlt.ncssm.edu/tiger/Flash/gases/CharlesLaw.html)  
[Ley de Gay-Lussac (exe) *waybackmachine*](http://web.archive.org/web/20100618031146/http://iesdmjac.educa.aragon.es/PortalFQ/anima/Gay-LussacLaw.exe)  

[ChemDemos. Gas Laws - uoregon.edu](https://chemdemos.uoregon.edu/Topics/Gas-Laws)  
Se descargan zip con html y flash (swf)  
[Byole's Law - chem.iastate.edu *waybackmachine*](http://web.archive.org/web/20081210154019/http://www.chem.iastate.edu/group/Greenbowe/sections/projectfolder/flashfiles/gaslaw/boyles_law_new.html)  
[Charles Law - chem.iastate.edu (flash) *waybackmachine*](http://web.archive.org/web/20061203001236/http://www.chem.iastate.edu/group/Greenbowe/sections/projectfolder/flashfiles/gaslaw/charles_law.swf)  

[LEY DE BOYLE - gobiernodecanarias.org *waybackmachine*](http://web.archive.org/web/20150506141409/http://www.gobiernodecanarias.org/educacion/3/usrn/lentiscal/1-cdquimica-tic/FlashQ/0-1Gases/LeydeBoyle/LeydeBoyle.htm)  

[Leyes de los gases. Estudio experimental - fisquiweb.es](https://www.fisquiweb.es/Gases/index.htm)  

[Ley de Charles - ite.educacion.es](http://www.ite.educacion.es/w3/eos/MaterialesEducativos/mem2003/gases/lab_charles.html)  
[Ley de Boyle - ite.educacion.es](http://www.ite.educacion.es/w3/eos/MaterialesEducativos/mem2003/gases/lab_boyle.html)   

[Ley de Gay Lussac - juntadeandalucia.es (Flash) *waybackmachine*](http://web.archive.org/web/20130830144006/http://www.juntadeandalucia.es/averroes/recursos_informaticos/andared02/leyes_gases/ley_gaylussac.html)  


##  Ejercicios
[Leyes de los gases - ampliación (pdf)](http://cerezo.pntic.mec.es/~jrodr139/materiales/materiales_3eso/extra_gases.pdf)  
José Luis Rodríguez Blanco, IES Aramo  

Apuntes con ejercicios resueltos  
[Gases.pdf - fisquiweb.es](http://fisquiweb.es/Apuntes/Apuntes1Bach/Gases.pdf) 

##  Condiciones normales
Citado en apuntes termoquímica Química 2º Bachillerato  
[normal - goldbook.iupac.org](http://goldbook.iupac.org/N04211.html)  
[Standard Pressure and Temperature STP - goldbook.iupac.org](http://goldbook.iupac.org/S06036.html) 

##  Cómics
Dentro de  [High Scholl scienc Teacher Resources - cationdesigns.blogspot.com](http://cationdesigns.blogspot.com.es/p/high-school-science-teacher-resources.html)  
![](http://1.bp.blogspot.com/-ksmLBDGFXeQ/TwZPXSZMJiI/AAAAAAAABeo/LAbgaNaUAw8/s200/Gas+Laws2.jpg "Gas Law2")  

