# LOGSE (Ley Orgánica de Ordenación General del Sistema Educativo) 

## Legislación estatal
### Ley Orgánica 1/1990, de 3 de octubre de 1990, de Ordenación General del Sistema Educativo.
[Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo.](https://www.boe.es/buscar/doc.php?id=BOE-A-1990-24172) 

### Real Decreto 1007/1991 Currículo ESO (LOGSE)
[Real Decreto 1007/1991, de 14 de junio, por el que se establecen las enseñanzas mínimas correspondientes a la Educación Secundaria Obligatoria.](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-16422) 

### Real Decreto 1178/1992 Currículo Bachillerato (LOGSE)
[Real Decreto 1178/1992, de 2 de octubre, por el que se establecen las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/doc.php?id=BOE-A-1992-23405)  
> Artículo 11.  
1. Las Administraciones educativas fijarán las materias optativas del Bachillerato, así como el número de ellas que los alumnos deberán cursar en cada uno de los cursos del Bachillerato.  

[Real Decreto 1179/1992, de 2 de octubre, por el que se establece el currículo del Bachillerato (en el ámbito territorial de gestión del Ministerio de Educación y Ciencia.).](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1992-23406)  

[Orden de 12 de noviembre de 1992 por la que se dictan instrucciones para la implantación anticipada del Bachillerato establecido por la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo.](https://www.boe.es/buscar/doc.php?id=BOE-A-1992-25708)  

[Orden de 12 de noviembre de 1992 por la que se regula la evaluación y la calificación de los alumnos que cursan el Bachillerato establecido en la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo.](https://www.boe.es/buscar/doc.php?id=BOE-A-1992-25706)  

[Real Decreto 1700/1991, de 29 de noviembre, por el que se establece la estructura del Bachillerato.](https://www.boe.es/buscar/doc.php?id=BOE-A-1991-29052)  
> Art. 12.  
1. Las Administraciones educativas fijarán las materias optativas del Bachillerato, así como el número de ellas que los alumnos deberán superar en cada uno de los cursos del Bachillerato.  

[Resolución de 29 de diciembre de 1992, de la Dirección General de Renovación Pedagógica, por la que se regula el currículo de las materias optativas de Bachillerato establecidas en la Orden de 12 de noviembre de 1992 de implantación anticipada del Bachillerato definido por la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1993-2192)  
> ANEXO QUE SE CITA  
Ciencia, Tecnología y Sociedad  

