
# Bachillerato. Ciencia, tecnología y sociedad

Se trata de una materia optativa de Bachillerato LOGSE, por ver si aplica a ambos cursos de bachillerato y diferencias. 

En cierto modo es precursora de las materias  
[Ciencias para el Mundo Contemporáneo (LOE)](/home/materias/bachillerato/cienciasparaelmundocontemporaneo-1bachillerato)  
[Cultura científica 1º Bachillerato (LOMCE)](/home/materias/bachillerato/cultura-cientifica-1-bachillerato)  

Recursos y currículo de momento aquí  

[Ciencia, tecnología y sociedad. Materiales didácticos. Bachillerato. Luis Fernández González , Luis González Pérez. EAN 9788436926730 ](https://www.libreria.educacion.gob.es/libro/ciencia-tecnologia-y-sociedad-materiales-didacticos-bachillerato_173519/)  Descarga gratuita, tiene como anexo el currículo oficial  

[Ciencia, tecnología y sociedad. Materiales didácticos. Bachillerato. Luis Fernández González , Luis González Pérez , Ana Francisca Aguilar Sánchez , María Isabel Salazar Garteizgogeascoa. EAN 9788436924142](https://www.libreria.educacion.gob.es/libro/ciencia-tecnologia-y-sociedad-materiales-didacticos-bachillerato_148653/)  


[Resolución de 29 de diciembre de 1992, de la Dirección General de Renovación Pedagógica, por la que se regula el currículo de las materias optativas de Bachillerato establecidas en la Orden de 12 de noviembre de 1992 de implantación anticipada del Bachillerato definido por la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1993-2192)  
> ANEXO QUE SE CITA  
Ciencia, Tecnología y Sociedad  


1. Introducción

Mediante la actividad técnica, el ser humano modifica las sustancias materiales, los fenómenos naturales y el entorno con el fin de satisfacer sus necesidades y mejorar sus condiciones de vida. La técnica es una manifestación social, forma parte del tejido cultural de cada grupo humano en un momento determinado y sus productos reflejan las necesidades, aspiraciones y valores de una colectividad. A su vez, la actividad técnica es capaz de modificar la sociedad, vertebrarla de otro modo, cambiar sus niveles de riqueza y bienestar, transformar sus sistemas de valores y alterar, radicalmente incluso, el medio físico.

Por otra parte, la humanidad ha intentado siempre comprender e interpretar el mundo, buscando explicaciones a los fenómenos naturales y sociales.

El pensamiento filosófico y científico, empleando el razonamiento, la especulación y la prueba, ha creado conceptos, establecido leyes y teorías, imaginado y construido modelos ideales que permiten explicar, con razonable grado de certeza, el funcionamiento del mundo. La actividad científica y la sociedad en la que se desarrolla mantienen también una relación de interdependencia. La ciencia es una manifestación social condicionada, en cada momento histórico, por los problemas, valores y creencias vigentes y proporciona, a su vez, ideas capaces de alterar sustancialmente los conocimientos, valores y creencias que estructuran el tejido social.

Ciencia y técnica son realidades difíciles de separar. La ciencia -saber por qué- y la técnica -saber hacer- se han potenciado y fecundado mutuamente a lo largo de la historia. La actividad técnica, muchas veces empírica, ha proporcionado instrumentos de observación, experiencias y argumentos útiles para edificar el conocimiento científico. A su vez, la aplicación del conocimiento científico ha posibilitado un desarrollo vertiginoso de las técnicas, convirtiéndose en el factor más importante de la producción y haciendo menos necesaria la destreza manual. La fecundación de la técnica por la ciencia ha dado lugar a una actividad de síntesis, la tecnología, que caracteriza nuestro tiempo.

La tecnología -saber cómo y por qué hacer- persigue desarrollar soluciones prácticas a problemas y necesidades existentes, de un modo sistemático y ordenado. Para alcanzar sus fines, el tecnólogo emprende investigaciones y aplica tanto los conocimientos científicos como la experiencia técnica de que dispone. Es, pues, el producto de la simbiosis entre la investigación científica y las técnicas de producción. Las diversas tecnologías permiten producir, modificar y mejorar objetos, instrumentos, medios de producción, servicios, espacios y ambientes, para satisfacer las necesidades más variadas. Con el desarrollo tecnológico ha aumentado además, exponencialmente, la posibilidad de obtener nuevos conocimientos científicos y de aplicarlos para ofrecer más y mejores productos.

El desarrollo científico y tecnológico ha tenido un papel de gran importancia en muchas transformaciones sociales, no sólo en lo relativo a las condiciones materiales de vida, sino también en la propia organización interna y en los valores y creencias compartidas. Desde la perspectiva de los países desarrollados, la sociedad actual es mejor, más segura y confortable. Pero, a la vez que se ha producido este progreso, se han agrandado las desigualdades entre naciones en lo relativo a la producción y distribución de riqueza. El desarrollo tecnológico ha inducido también profundas transformaciones culturales y alterado las escalas de valores vigentes en distintos pueblos y culturas, ha influido decisivamente en la configuración del poder político, económico y militar de las naciones, ha acercado la posibilidad de agotamiento por extenuación de los recursos naturales del planeta y ha producido efectos desastrosos, no deseados, en el medio ambiente.

A menudo, el estudio de la dimensión científica y técnica de la evolución social está ausente de la enseñanza de las ciencias sociales.

Tampoco es habitual encontrar referencias a las condiciones y repercusiones sociales de una tecnología determinada en los programas de formación técnica. La fuerza de la tradición académica y del ambiente cultural, actuando sobre el diseño de los planes de estudios, ha hecho quizá que éstos aparezcan segmentados en exceso, segregados en dos ámbitos académicos injustificadamente estancos, uno humanista o de letras y otro científico y técnico.

La finalidad central de la materia Ciencia, Tecnología y Sociedad consiste en proporcionar a los estudiantes una ocasión para relacionar conocimientos procedentes de campos académicos habitualmente separados, un escenario para reflexionar sobre los fenómenos sociales y las condiciones de la existencia humana desde la perspectiva de la ciencia y la técnica, así como para analizar las dimensiones sociales del desarrollo tecnológico.

Es, pues, una materia con una clara voluntad interdisciplinar, integradora y abierta al tratamiento de cuestiones -el medio ambiente, los modelos de desarrollo económico y social, la responsabilidad política y las formas de control social, etc.- que no están claramente instalados en una disciplina académica concreta, pero que tienen un papel decisivo en la vida social.

De este carácter integrador y crítico nace el valor formativo de Ciencia, Tecnología y Sociedad, valor aplicable al currículo de cualquiera de las modalidades del Bachillerato. Una materia optativa que, a través del estudio de las interacciones mutuas de ciencia, tecnología y sociedad, trata de contribuir a la formación de ciudadanos capaces de comprender fenómenos de naturaleza compleja, reflexionar sobre ellos y elaborar juicios de valor propios, capaces también de tomar decisiones y participar activamente en la vida social.

Los contenidos de Ciencia, Tecnología y Sociedad abarcan un amplio campo temático, dado su carácter decididamente interdisciplinar, que puede concretarse en programaciones muy distintas, en función del contexto del centro docente, de las decisiones adoptadas en su Proyecto Curricular, de las modalidades del Bachillerato y de las peculiaridades del profesorado que la imparta. Dichos contenidos se han agrupado y ordenado alrededor de cinco núcleos o ejes temáticos.

El primer núcleo gira en torno a los conceptos centrales, su evolución histórica y sus implicaciones sociales inmediatas. El segundo está dedicado a estudiar la articulación de estos conceptos en el sistema tecnológico de la producción, de modo que permita analizar la acción humana intencionada, dirigida a modificar el medio. La actividad tecnológica y el desarrollo científico y técnico tienen efectos innegables sobre la sociedad, ejerciendo un papel, a veces determinante, en la modificación de múltiples aspectos de la vida social: el análisis de dichas repercusiones es el común denominador de los conocimientos del tercer núcleo. El cuarto núcleo de contenidos engloba aprendizajes relativos al control social de los fenómenos tecnológicos y al estudio de las relaciones existentes entre las fuerzas sociales y las distintas direcciones que pueden tomar el desarrollo tecnológico. Por último, como clave que cierra y da coherencia a los anteriores, aparece un núcleo de reflexión filosófica sobre la ciencia y la tecnología, una reflexión abierta a diferentes perspectivas: ética, antropológica, epistemológica, estética o de filosofía social.

Este modo de presentación no corresponde a un supuesto orden de importancia. Tampoco es imprescindible ni parece razonable abordar todos los contenidos de todos los núcleos. Por el contrario, es aconsejable seleccionar y concretar aquellos contenidos que, procedentes de los cinco núcleos, van a incorporarse a la programación de la enseñanza de la materia, adaptándolos y articulándolos en un discurso coherente y significativo para los estudiantes.

2. Objetivos generales

El desarrollo de esta materia ha de contribuir a que las alumnas y los alumnos desarrollen las siguientes capacidades:

1. Comprender la influencia de la ciencia y la técnica en la evolución de las sociedades, así como los condicionamientos históricos y sociales en la creación científica y tecnológica.

2. Analizar y valorar las repercusiones sociales, económicas, políticas y éticas de la actividad científica y tecnológica.

3. Aplicar los conocimientos científicos y tecnológicos adquiridos al estudio y valoración de problemas relevantes en la vida social.

4. Utilizar los conocimientos sobre las relaciones existentes entre ciencia, tecnología y sociedad para comprender mejor los problemas del mundo en que vivimos, buscar soluciones y adoptar posiciones basadas en juicios de valor libre y responsablemente asumidos.

5. Apreciar y valorar críticamente la capacidad potencial y las limitaciones de la ciencia y la tecnología para proporcionar mayor grado de bienestar personal y colectivo.

6. Adquirir una mayor conciencia de los problemas ligados al desarrollo desigual de los pueblos de todo el mundo y adoptar una actitud responsable y solidaria con ellos.

7. Analizar y evaluar críticamente la correspondencia entre las necesidades sociales y el desarrollo científico y técnico, valorando la información y participación ciudadanas como forma de ejercer un control democrático del mismo.

3. Contenidos

Ciencia, técnica y tecnología: Perspectiva histórica.

Evolución y <homo faber>. El papel de la técnica en el proceso de hominización.

El nacimiento del pensamiento y el método científicos.

Desarrollo e implicaciones de la Revolución Industrial.

Ciencia y técnica en el mundo actual. El desarrollo de la tecnología.

Historia social del desarrollo científico y técnico en algunos ámbitos característicos: conocimiento del universo, producción y aprovechamiento de energía, producción de alimentos, la salud, la información, el transporte y las comunicaciones, el hábitat, etc.

El sistema tecnológico.

La tecnología como sistema. Componentes del sistema tecnológico:

conocimiento, recursos técnicos, capital y contexto social.

El papel del conocimiento en el sistema tecnológico. La investigación científica. Ciencia aplicada. Investigación planificada (I + D).

Cantidad y calidad de los recursos técnicos disponibles: materiales y fuentes de energía, técnicas y herramientas, fuerza de trabajo.

La financiación de la tecnología. Costes de la investigación, producción y distribución. Interdependencia y colaboración tecnológica.

Necesidades y demandas sociales. Oportunidades de mercado. Calidad de vida, modos de vida y sistemas de valores.

Repercusiones sociales del desarrollo científico y técnico.

Transformaciones económicas: Industrialización, terciarización.

Desigualdades en el desarrollo económico.

Crecimiento demográfico: crecimiento de la población, control de mortalidad y de natalidad.

Efectos en la construcción social: estructura social, relaciones de producción, valores y hábitos. Las concepciones del mundo. Influencia en la vida cotidiana.

Impacto directo en el medio ambiente: vertidos, calentamiento, agotamiento de recursos y de la biodiversidad. Efectos indirectos: riesgos, subproductos y residuos. Valoración de casos significativos.

El control social de la actividad científica y tecnológica.

Prioridades sociales de investigación científica y desarrollo tecnológico.

Modelos de desarrollo.

Evaluación de la tecnología: Alcance y limitaciones.

El control del mercado y del Estado sobre la tecnología; su dimensión supranacional.

Desarrollo científico y técnico y poder político. Información y participación ciudadanas en la toma de decisiones.

El desarrollo científico y tecnológico: Reflexiones filosóficas.

Los mitos del progreso científico y técnico. Las dimensiones del progreso personal y social.

El problema de la racionalidad tecnológica. La correspondencia entre el fin y los medios. Crítica de la razón instrumental.

Desarrollo tecnológico y responsabilidad moral. El problema de la neutralidad científica y técnica.

La dimensión estética de la actividad tecnológica.

4. Criterios de evaluación

1. Identificar las características específicas de la ciencia, la técnica y la tecnología, diferenciando tales tipos de actividad y reconociendo su interdependencia.

Este criterio permite evaluar si el alumno ha adquirido los conceptos necesarios para establecer una comparación entre la ciencia, la técnica y la tecnología, reconociendo su mutua dependencia sin confundir tales ámbitos de la actividad humana.

2. Reconocer las relaciones existentes entre un logro científico o técnico relevante y el contexto social en el que se produce, identificando las necesidades y valores a los que responde.

Con este criterio se pretende comprobar la comprensión alcanzada por el alumno acerca de la dimensión social de la producción científica y técnica en una época determinada, de forma que identifique las circunstancias concretas que, en ese momento histórico, han favorecido la aparición o desarrollo de las mismas.

3. Explicar las causas determinantes que, en un momento dado, han supuesto el abandono o el retraso en la aplicación de algún descubrimiento científico o desarrollo técnico relevantes.

Complementario del anterior, este criterio permite evaluar en qué medida el alumno es capaz de identificar, al analizar un caso característico de una época histórica determinada, los condicionamientos que han operado sobre la producción científica y técnica, inhibiendo o retrasando su aparición y desarrollo.

4. Elaborar informes sobre las aplicaciones de un logro científico o tecnológico relevante en el mundo actual, evaluando críticamente sus consecuencias sociales o medioambientales.

Este criterio persigue evaluar la capacidad del alumno para recopilar, elaborar información y adoptar un juicio crítico sobre la aplicación de un conocimiento científico o un desarrollo técnico relevante y actual, identificando sus implicaciones en las condiciones de vida y, en su caso, las alteraciones del medio físico.

5. Exponer los hitos relevantes de la evolución de un ámbito concreto del desarrollo científico o técnico, indicando las principales consecuencias sociales derivadas de los mismos.

Mediante este criterio se puede comprobar el grado de conocimiento adquirido por el alumno a la hora de relacionar los avances científicos o técnicos de un ámbito concreto con las transformaciones sociales que han supuesto. Igualmente permite evaluar la competencia adquirida para organizar una exposición y extraer conclusiones del conjunto de datos manejados.

6. Analizar los rasgos que caracterizan el grado de desarrollo tecnológico de una sociedad determinada, a partir de un conjunto de datos significativos.

Lo que se pretende evaluar es la capacidad de relacionar los parámetros principales del sistema productivo de una sociedad determinada, relacionándolos entre sí para sacar conclusiones sobre su grado de desarrollo tecnológico. Deben, pues, proporcionarse datos relevantes, que caracterizan fundamentalmente una sociedad, y centrarse en un campo concreto de la actividad tecnológica, tal como las comunicaciones, la producción de alimentos o de bienes de consumo..., para extraer consecuencias generales sobre las posibilidades y las limitaciones de desarrollo de esa sociedad.

7. Argumentar y debatir acerca de los derechos de los ciudadanos a estar informados y participar en la toma de decisiones políticas sobre la investigación y las aplicaciones científicas y tecnológicas, tomando como referencia un caso concreto de relevancia social.

La aplicación de este criterio permite comprobar si el alumno es capaz de proponer y contrastar argumentos sobre el papel de los ciudadanos en el control social de las decisiones relacionadas con el desarrollo científico y tecnológico, centrándose en un caso o situación concreta que le resulte familiar y haya sido objeto de controversia social (energía nuclear, prolongación artificial de la vida, industria armamentista, ingeniería genética, etc.).

8. Analizar y enjuiciar críticamente las posibilidades y limitaciones de la ciencia y la tecnología en la búsqueda de soluciones a los problemas más acuciantes de la humanidad.

Este criterio sirve para evaluar la capacidad del alumno a la hora de emitir un juicio personal y crítico acerca del papel de la ciencia y la tecnología, como actividades que pueden contribuir a solucionar problemas que afectan al conjunto de la humanidad (explosión demográfica, desarrollo desigual de los pueblos, agotamiento de recursos, calentamiento de la atmósfera, pérdidas de biodiversidad, etc.), siempre que dichas actividades se pongan al servicio de los intereses colectivos.

9. Formular preguntas y plantear problemas de carácter filosófico sobre algunas dimensiones de la actividad tecnológica, proporcionando respuestas argumentadas y sometiéndolas a debate.

Con este criterio se pretende comprobar si el alumno es capaz de reflexionar sobre la tecnología adoptando un punto de vista específicamente filosófico, mediante el planteamiento de problemas relacionados con cuestiones como la racionalidad tecnológica, las dimensiones del progreso personal y social, la dimensión ética o estética de la actividad tecnológica, etc.

Asimismo, la aplicación del criterio permite evaluar la capacidad del alumnado para poner a prueba, mediante el diálogo con los demás, sus propios argumentos y/o conclusiones sobre los problemas analizados.
