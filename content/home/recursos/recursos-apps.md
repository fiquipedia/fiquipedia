
# Recursos apps

Aparte de enlaces que se puedan abrir desde un móvil / tableta ... también se ponen apps específicas relacionadas con ciencia  
En principio pongo para android que son las que puedo probar ... no para dispositivos apple  
Relacionado con  [Recursos simulaciones](/home/recursos/simulaciones) , con el tiempo puedo separar algunas en recursos apps Física y  [recursos apps Química](/home/recursos/quimica/apps-quimica)  
Algunos pueden estar asociados a recursos concretos: por ejemplo apps relacionadas con  [recursos movimiento ondulatorio](/home/recursos/fisica/movimiento-ondulatorio)  y  [recursos espectro](/home/recursos/recursos-espectro)  , apps relacionadas con  [recursos cinemática](/home/recursos/fisica/cinematica) , relacionadas con  [recursos física partículas](/home/recursos/fisica/fisica-de-particulas)  

Open Source Physics @Singapore using Easy Java/JavaScript Simulation (EjsS or Ejs) and Tracker.  
[Open Source Physics Singapore - play.google](https://play.google.com/store/apps/dev?id=7915608832562607433)  
Multitud de apps de física, algunas relacionadas con  [óptica geométrica](/home/recursos/fisica/optica-geometrica) , péndulo doble asociado a  [teoría del caos](/home/recursos/fisica/teoria-del-caos) , ...  
 
[Las mejores aplicaciones educativas en Android - Ámbito científico y matemático. Ministerio de educación, Observatorio tecnológico, Software educativo. Escrito por Alfonso Gaspar Noviembre de 2012](http://recursostic.educacion.es/observatorio/web/es/software/software-educativo/1070-las-mejores-aplicaciones-educativas-para-android?start=7) 

##  Apps de registro de datos de sensores de móviles
[Physics Toolbox Sensor Suite. Vieyra Software - play.google](https://play.google.com/store/apps/details?id=com.chrystianvieyra.physicstoolboxsuite)  
>esta aplicación utiliza entradas de los sensores del dispositivo para registrar y exportar datos en formato de valores separados por comas (CSV) a través de un archivo .csv. Los datos pueden registrarse en tiempo transcurrido en un gráfico o se muestran digitalmente. Los usuarios pueden exportar los datos para su posterior análisis en una hoja de cálculo. Esta aplicación tambien genera tonos, colores y un tiene un estroboscopio.  
[Physics Toolbox by Vieyra Software](https://www.vieyrasoftware.net/)  

[phyphox RWTH Aachen University - play.google](https://play.google.com/store/apps/details?id=de.rwth_aachen.phyphox)  

[Sensor Data Logger. Steppschuh - play.google](https://play.google.com/store/apps/details?id=net.steppschuh.sensordatalogger) 
>Sensor Data Logger is a dashboard for your device sensors. It plots charts that show the values of selected sensors in real-time, even from connected Android Wear devices.

##  Recursos apps Física
[Algodoo](http://www.algodoo.com/)  
> Algodoo is a unique 2D-simulation software from Algoryx Simulation AB. Algodoo is designed in a playful, cartoony manner, making it a perfect tool for creating interactive scenes. Explore physics, build amazing inventions, design cool games or experiment with Algodoo in your science classes. Algodoo encourages students and children’s own creativity, ability and motivation to construct knowledge while having fun. Making it as entertaining as it is educational. Algodoo is also a perfect aid for children to learn and practice physics at home.

[Design Simulation Technologies](http://www.design-simulation.com/)  
[Interactive Physics. Physics Simulation Software in the Classroom - design-simulation.com](
> Interactive PhysicsTM, the award-winning educational software from Design Simulation Technologies, makes it easy to observe, discover, and explore the physical world through exciting simulation. This easy-to-use program will support the most basic to complex topics in STEM education.   
[Interactive Physics. Flash simulation](http://www.design-simulation.com/IP/simulationlibrary/flash-simulations.php)  
 
 
##  Recursos apps juegos relacionados con Física
Es habitual que los alumnos tengan interés en videojuegos y es interesante mostrarles que su desarrollo suele implicar mucha física: mecánica, óptica ...  
Hay algunos juegos que usan directamente "principios físicos" y pueden ser interesantes incluso para niños  
El propio Angry Birds usa tiros parabólicos ..  
[Brain It On! - Physics Puzzles. Orbital Nine Games - play.google](https://play.google.com/store/apps/details?id=com.orbital.brainiton)  
[Rube's Lab - Physics Puzzle. Bouland Games - play.google](https://play.google.com/store/apps/details?id=com.onlinico.rubeslab)  
[Draw Lines Physics - Balls Puzzle Games. Yellow Bolt Studio - play.google](https://play.google.com/store/apps/details?id=com.ybs.drawLinesPuzzle)  
[Machinery - Physics Puzzle. WoogGames - play.google](https://play.google.com/store/apps/details?id=com.wooggames.machinery)  


