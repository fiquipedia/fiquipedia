
# 2º Bachillerato Tecnología e Ingeniería

Se trata de una nueva materia en LOMLOE de 2º de Bachillerato que  en cierto modo recupera la materia [Tecnología Industrial 2º Bachillerato (LOMCE)](/home/materias/bachillerato/tecnologia-industrial-2bachillerato) que desapareció con LOMLOE  

[Currículo](/home/materias/bachillerato/tecnologia-e-ingenieria-2-bachillerato/curriculo-tecnologia-e-ingenieria-2-bachillerato-lomloe)  


[PAU Tecnología e Ingeniería](https://www.fiquipedia.es/home/pruebas/pruebasaccesouniversidad/pau-tecnologiaeingenieria/)  

[Tecnología e ingeniería II - iesmurillo](https://sites.google.com/iesmurillo.es/tecnologia-industrial-2)  


[Ejercicios resueltos de Tecnología e Ingeniería II: Segundo de Bachillerato (Ciencias y Tecnología), José Otero Arias, ISBN 979-8387187483](https://www.amazon.es/dp/B0BYM4LRVX)  
> El libro consta de una completa colección de 320 ejercicios resueltos de esta materia de modalidad de segundo de Bachillerato de Ciencias y Tecnología, con diferentes enfoques y grados de dificultad, repartidos en nueve bloques temáticos y 248 páginas. 



