
# Vídeos

Esta página se creó en 2011 asociada al curso de licenciamiento, para poner vídeos con sus referencias, pero a partir de ese momento la mayor parte de los vídeos se asocian a un bloque de contenidos (Física, química ...) aunque hay páginas de vídeos generales  
Se ponen enlaces en general; se incrusta el vídeo aunque ralentizar la carga la página, ya que permite identificarlos mejor  
Se puede asociar también a cine como recurso didáctico, pero también está disperso: en los  [recursos de elaboración propia](/home/recursos/recursos-de-elaboracion-propia)  hay materiales de uso de cine de un curso realizado, y se pueden citar otras recopilaciones de recursos de cine de ciencia ficción (y literatura) como  [Ciencia para todos a través del cine y la literatura de ciencia ficción, Sergio L. Palacios](https://culturacientifica.com/2013/12/30/ciencia-para-todos-traves-del-cine-y-la-literatura-de-ciencia-ficcion/)   

[TeachFlix, High School, Science](https://teachflix.org/high-school/?category=Science)  


##  Cosmos (Carl Sagan)
A veces se suben vídeos en youtube pero se retiran porque tienen derechos de autor

 [https://www.youtube.com/playlist?list=PLKSi40WEKtMxykDBP8_vrC6bKXotys8KJ](https://www.youtube.com/playlist?list=PLKSi40WEKtMxykDBP8_vrC6bKXotys8KJ)  (validado en 2019)

## Beyond the Elements (David Pogue)

[Beyond the Elements](https://www.imdb.com/title/tt14973622/)
Reactions  
Indestructible  
Life  

En castellano en 
[La química de los elementos](https://ver.movistarplus.es/ficha/la-quimica-de-los-elementos/1919086?id=1919086)  

## Hunting the Elements (David Pogue)
[Hunting the Elements, NOVA T39.E8](https://www.imdb.com/title/tt2286853/)  

##  Youtube for schools
 Iniciativa surgida en diciembre de 2011, artículo del blog oficial de youtube  
 [http://youtube-global.blogspot.com/2011/12/opening-up-world-of-educational-content.html?](http://youtube-global.blogspot.com/2011/12/opening-up-world-of-educational-content.html?)  
 Enlace a youtube for schools  
 [http://www.youtube.com/schools?feature=inp-bl-paq](http://www.youtube.com/schools?feature=inp-bl-paq) 

##  **Experimento de la doble rendija**
 [Cookatoo.ergo.ZooM](http://commons.wikimedia.org/wiki/User:Cookatoo.ergo.ZooM) , 
 ['Young´s double slit experiment: video clip', Wikimedia Commons, 12 noviembre 2010](http://commons.wikimedia.org/wiki/File%3ADouble_slit.theora.ogv)   \[consultado 22 noviembre 2011]. Material bajo licencia  [Creative Commons Reconocimiento-CompartirIgual 3.0](http://www.creativecommons.org/licenses/by-sa/3.0) 

##  Prof. Walter Lewin  
[Prof. Walter Lewin, Massachusetts Institute of Technology OpenCourseWare, 8.01 Physics I Classical Mechanics Fall 1999](https://ocw.aprende.org/courses/physics/8-01-physics-i-classical-mechanics-fall-1999/video-lectures/) (consultado en 2021). Material bajo licencia  [Creative Commons Reconocimiento-NoComercial-CompartirIgual 3.0 Estados Unidos](http://creativecommons.org/licenses/by-nc-sa/3.0/us/)

##  Edutube.org
 [http://www.edutube.org/category/chemistry](http://www.edutube.org/category/chemistry)  
 Vídeos de uso educativo por categorías. Son enlaces externos, muchos de ellos alojados en youtube: el licenciamiento varía y no es claro.

##  The Known Universe (AMNH)
 American Museum of National History. Material con derechos de autor. Viaje virtual desde la Tierra hasta los confines del universo conocido, basado en datos reales obtenidos de observaciones.  [The Known Universe (AMNH)](https://www.amnh.org/explore/videos/space/the-known-universe) (en 2015 este enlace ya no lleva al vídeo directamente)  
[![](https://img.youtube.com/vi/17jymDn0W6U/0.jpg)](https://www.youtube.com/watch?v=17jymDn0W6U "The Known Universe by AMNH") 
 
 Uso; CMC “Nuestro lugar en el universo”, 4º ESO gravitación universal, Fis2º gravitación.

##  Powers of Ten / Potencias de 10
 Ray Eames y Charles Eames, 1968 (versiones posteriores), basado en el libro "Cosmic View" de Kees Boeke, 1957. Material con derechos de autor © 2010 Eames Office. Vídeo que muestra la escala en un viaje hacia el espacio y de regreso hacia el interior del átomo.  
[![](https://img.youtube.com/vi/0fKBhvDjuy0/0.jpg)](https://www.youtube.com/watch?v=0fKBhvDjuy0 "Powers of Ten™ (1977) Eames Office") 

## Canal Youtube del grupo de investigación Investiga, Construye, Crea de la Universidad de Alcalá de Henares @GrupoICCUAH

[Canal Youtube del grupo de investigación Investiga, Construye, Crea de la Universidad de Alcalá de Henares @GrupoICCUAH](https://www.youtube.com/@GrupoICCUAH)  

[Lista ¡Un video, Galileo! @GrupoICCUAH](https://www.youtube.com/watch?v=NqKgqTPFbuU&list=PLv4KVtP071cOBguakJ3LDggu9LnjeprzT&pp=iAQB)  

## Our Universe is SO big, it's mindblowing! BBC  

[Una nueva versión homenajea el clásico «Potencias de diez» con la tecnología y conocimientos actuales; es toda una lección de humildad](https://www.microsiervos.com/archivo/ciencia/nueva-version-potencias-diez-tecnologia-conocimientos-actuales.html)  

[![](https://img.youtube.com/vi/2iAytbmXYXE/0.jpg)](https://www.youtube.com/watch?v=2iAytbmXYXE "Our Universe is SO big, it's mindblowing! BBC") 


##  Images worth spreading: Cosmic Eye 
(iPad, iPhone, iPod)", vídeo generado a partir de una app  
[![](https://img.youtube.com/vi/8Are9dDbW24/0.jpg)](https://www.youtube.com/watch?v=8Are9dDbW24 "Cosmic Eye (Original HD Version)") 

###  Powers of Ten - Ultimate Zoom (micro-macro - Imax combined)
[![](https://img.youtube.com/vi/bhofN1xX6u0/0.jpg)](https://www.youtube.com/watch?v=bhofN1xX6u0 "Powers of Ten - Ultimate Zoom (micro-macro - Imax combined))") 

##  Tectónica de placas, 650 Million Years in under 2 minutes
Vídeo útil en CMC  
[![](https://img.youtube.com/vi/UvY13ZVIQJg/0.jpg)](https://www.youtube.com/watch?v=UvY13ZVIQJg "650 million years of plate tectonics in under 2 minutes")

##  Science on the Simpsons
 [Science on the Simpsons](https://web.archive.org/web/20160729190207/http://www.lghs.net/ourpages/users/dburns/ScienceOnSimpsons/Clips.html)  (waybackmachine, enlace no operativo en 2021) 
Material curioso catalogado por distintos conceptos físicos.  
"Science on the Simpsons These clips from The Simpsons cartoons are intended for use by science teachers". Dan Burns. 

## PhysicsHigh
 [@PhysicsHigh](https://www.youtube.com/@PhysicsHigh)  
Physics is a fascinating topic. In this channel I explore that content by providing explanations of various concepts explained in a way that a high school students can grasp. (though quite a number of my videos have been popular with undergraduate students!)  
As I result I  keep the maths to an appropriate level.
	
##  MinutePhysics- The BEST Science Online (Henry's List)
Video del canal youtube " [minutephysics](http://www.youtube.com/user/minutephysics?feature=watch) " con numerosos enlaces. 
[![The BEST Science Online (Henry's List)](https://img.youtube.com/vi/i8t53Ak_yrQ/0.jpg)](https://www.youtube.com/watch?v=i8t53Ak_yrQ)
Comentario al vído publicado el 07/02/2013 contiene esta lista de enlaces, algunos a otros vídeos

Subscribe - it's FREE!! [http://dft.ba/-minutephysics_sub](http://dft.ba/-minutephysics_sub)  
Music by Nathaniel Schroeder [http://www.soundcloud.com/drschroeder](httpshttp://www.soundcloud.com/drschroeder)   

This List Is Incomplete:  
[Big Red Button](http://inception.davepedu.com)  
[xkcd](http://xkcd.com/1131/)  
[xkcd what if?](http://what-if.xkcd.com/3/)  
[Saturday Morning Breakfast Cereal, SMBC Comics](https://www.smbc-comics.com/index.php?db=comics&id=2867)  
[Empirical Zeal](http://www.empiricalzeal.com/)
[Sean Carroll - preposterousuniverse, The World of Everyday Experience, In One Equation](http://www.preposterousuniverse.com/blog/2013/01/04/the-world-of-everyday-experience-in-one-equation/)  
[Terry Tao - A mathematical formalisation of dimensional analysis](https://terrytao.wordpress.com/2012/12/29/a-mathematical-formalisation-of-dimensional-analysis/)  
[It's Okay To Be Smart](https://www.itsokaytobesmart.com)  
[I ¶#@*ing Love Science](https://www.facebook.com/IFeakingLoveScience)  
[NASA Astronomy Picture of the Day](http://apod.nasa.gov/apod/astropix.html)  
[Radiolab](https://www.wnycstudios.org/podcasts/radiolab)  
[The Scale of the Universe](http://htwins.net/scale2)  
[Fake Science](https://fakescience.org/post/1292427190/high-fructose-corn-syrup)  

Youtube Channels  
[Veritasium](http://www.youtube.com/user/1veritasium)  
[Sixty Symbols](http://www.sixtysymbols.com/)  
[Periodic Videos](http://www.periodicvideos.com/)  
[Crash Course](http://www.youtube.com/user/crashcourse)  
[The Brain Scoop](http://www.youtube.com/thebrainscoop)  
[Smarter Every Day](http://www.youtube.com/destinws2)  
[Vi Hart](http://www.youtube.com/vihart)  
[George Hart](https://www.youtube.com/channel/UCTl0dASnxto6j2wlVs5Bs2Q)  
[Numberphile](http://www.numberphile.com/)  
[Vsauce](http://www.youtube.com/user/Vsauce)  
[TED-Ed](http://www.youtube.com/teded)  
[MinuteEarth](http://www.youtube.com/user/minuteearth)  

Thanks to [Perimeter Institute](http://www.perimeterinstitute.ca) for support. 

##  El universo mecánico

[El universo mecánico - Wikipedia](https://es.wikipedia.org/wiki/El_universo_mec%C3%A1nico)  
Incluye enlaces a youtube

[The Mechanical Universe. Lista youtube 56 capítulos. Caltech - youtube](https://www.youtube.com/watch?v=XtMmeAjQTXc&list=PL8_xPU5epJddRABXqJ5h5G0dk-XGtA5cZ)  

 [El universo mecánico - érase una vez Niels H. Abel y Evariste Galois](http://abelgalois.blogspot.com.es/2009/07/el-universo-mecanico-mechanical.html)  
El Documental [El Universo Mecánico [The Mechanical Universe] ](https://es.wikipedia.org/wiki/El_universo_mec%C3%A1nico) consta de 52 capítulos, "lecciones magistrales" [impartidas por David Goodstein ](http://www.its.caltech.edu/~dg/)en 1985 [[California Institute of Technology](http://www.caltech.edu/)].   

[twitter vielbein/status/1287847066413146112](https://twitter.com/vielbein/status/1287847066413146112)  
Por cierto chicos, os paso [mi transcripción personal junto a cosas propias de los 52 vídeos del Universo Mecánico]((https://en.calameo.com/books/0063271521f3048569929)  

##  Ecudemic: 23 Great Sources For Free Educational Videos Online
 [23 Great Sources For Free Educational Videos Online](https://web.archive.org/web/20130916110312/http://www.edudemic.com/free-educational-videos-online/)  (waybackmachine, no operativo en 2021)
 Katie Lepi
##  Varios
La cuna de Newton 
[![Newton's Cradle - Incredible Science](https://img.youtube.com/vi/0LnbyjOyEQ8/0.jpg)](https://www.youtube.com/watch?v=0LnbyjOyEQ8)  
[Seis vídeos imprescindibles para entender qué es el bosón de Higgs:](https://edurecu.wordpress.com/2011/12/15/seis-videos-imprescindibles-para-entender-que-es-el-boson-de-higgs-2/)  

[twitter ChemistryTe/status/1420454821736370178](https://twitter.com/ChemistryTe/status/1420454821736370178)  
Why do physicists live longer than chemists?  

