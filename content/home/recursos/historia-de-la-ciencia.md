
# Historia de la ciencia

En esta página se intentan poner recursos asociados a historia de la ciencia, o la ciencia a través de la historia.

Puede enlazar con recursos [divulgación científica](/home/recursos/recursos-divulgacion-cientifica) y recursos [lectura](/home/recursos/lectura), también con [efemérides](/home/recursos/recursos-efemerides-ciencia), con biografías y con [ciencia y mujer](/home/recursos/ciencia-y-mujer)  

[Recursos digitales sobre historia de la ciencia - Luis Moreno Martínez](https://www.educa2.madrid.org/web/luis.morenomartinez/entradas/-/blogs/recursos-digitales-de-historia-de-la-ciencia)  

[Historia de la Ciencia - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/historia-ciencia/)  
Biografías y principales contribuciones de los grandes científicos y científicas de la historia  

[Pasión por la química. Referentes históricos y actuales de la química recreativa e implicaciones en la enseñanza y la divulgación - metode.es](https://metode.es/revistas-metode/monograficos/pasion-por-la-quimica-recreativa.html)  

[La historia de la química en el currículo y los libros de texto de Educación Secundaria Obligatoria y Bachillerato (LOE). Un estudio desde la didáctica y la historia de la cienca - Tesis UAM, Moreno Martínez, Luis](https://repositorio.uam.es/handle/10486/681224)  

[GT de Historia de la Ciencia: Recursos - ua.es](https://ua-es.libguides.com/c.php?g=660258&p=4662434)  

[Fuentes en Historia y Comunicación de la Ciencia.](http://www.historia-ciencia-comunicacion.org/Joomla/index.php/mediateca/recursos)  

[Dpto. de Historia de la Ciencia - csic.es](https://cchs.csic.es/es/org-structure/dpto-historia-ciencia)  
