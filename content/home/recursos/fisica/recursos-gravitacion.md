
# Recursos gravitación

Página voluminosa, pendiente separar en varias 

La gravitación enlaza con teoría de relatividad general: ver [recursos física relativista](/home/recursos/fisica/recursos-fisica-relativista)  

## Gravitación en el currículo
 La gravitación es algo que está en el currículo (LOE, pendiente actualizar para LOMCE y LOMLOE) de:  

*  Física y Química 4º ESO:  
Bloque 2. Fuerzas y movimiento, subbloque Las fuerzas y el movimiento, apartado "**La ley de la Gravitación universal y la culminación de la primera de las revoluciones científicas. El peso de los cuerpos y su caída. El movimiento de planetas y satélites.**"
* Física y Química 1º Bachillerato:  
Bloque 3. Dinámica, apartados "Dinámica del movimiento circular uniforme. **Interacción gravitatoria: Ley de gravitación universal. Importancia de esta ley.**  
Estudio de algunas situaciones dinámicas de interés: **Peso**, fuerzas de fricción en superficies horizontales e inclinadas, fuerzas elásticas y tensiones."
* Física 2º Bachillerato:  
 Bloque completo "2. Interacción gravitatoria.  
- De las Leyes de Kepler a la Ley de la gravitación universal.  
 Momento de una fuerza respecto de un punto y momento angular. Fuerzas centrales y fuerzas conservativas. Energía potencial gravitatoria.  
- La acción a distancia y el concepto físico de campo: El campo gravitatorio. Magnitudes que lo caracterizan: Intensidad de campo y potencial gravitatorio.  
- Campo gravitatorio terrestre. Determinación experimental de g. Movimiento de satélites y cohetes."

## Recursos, enlaces, apuntes, ejercicios  

Ver apuntes de elaboración propia en  [recursos de física 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato) y en  [recursos de elaboración propia de 1º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato)   
Campo Gravitatorio. 2º Bachillerato  
 [Campo gravitatorio](http://recursostic.educacion.es/newton/web/materiales_didacticos/campo_gravitatorio/index.htm)   
 José Luis San Emeterio, cc-by-nc-sa  

[Interacción gravitatoria 2º Bachillerato - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2bach/fisica/interaccion-gravitatoria/)  
  
Animaciones de Física 2º Bachillerato en GeoGebra. Ejercicios resueltos con enunciados múltiples y escenas para practicar diferentes aspectos de campo gravitatorio. [Física 2º Bachillerato – GeoGebra](https://www.geogebra.org/m/cbnzc8km) Autor:Antonio González García  
Ejercicios resueltos Boletín 2 Campo gravitatorio y movimiento de satélites  
 [boletin_problemas_tema_2.pdf](http://www.juntadeandalucia.es/averroes/centros-tic/41008970/helvia/sitio/upload/boletin_problemas_tema_2.pdf)   
©Raúl González Medina 2011 Problemas Campo Gravitatorio  
  
 [RP5-C_Gravitatorio_sol.pdf](http://selectividad.intergranada.com/Bach/Fisica/Clase/RP5-C_Gravitatorio_sol.pdf)   
Interacción gravitatoria  
 [Gravitación](http://www.sociedadelainformacion.com/departfqtobarra/gravitacion/gravitacion.htm)   
 Jesús Ruiz Felipe Profesor de Física del Instituto Cristóbal Pérez Pastor Tobarra (Albacete)  
  
 [www.agustinosleon.com/descarga/apuntes/2o_bto_campogravitatorio.ppt](http://www.agustinosleon.com/descarga/apuntes/2o_bto_campogravitatorio.ppt) Apuntes ppt de 30 páginas. © Copyright 2006-2013 Colegio Ntra. Madre del Buen Consejo. PP. Agustinos de León. Centro Bilingüe . Todos los derechos reservados.   
Gravitación newtoniana. Barbol [Gravitaci&#243;n newtoniana](http://www.lawebdefisica.com/apuntsfis/gravi/) © 2003—2013, La web de Física  
Fuerzas centrales y órbitas gravitatorias  
 [cap5.pdf](http://w3.mecanica.upm.es/~goico/mecanica/libro/cap5.pdf) Nivel universitario  
Aeronaves y Vehículos Espaciales. Tema 8 – Mecánica Orbital  
 [Tema8.pdf](http://www.aero.us.es/AVE/archivos/Y0910/Tema8.pdf) Sergio Esteban Roncero, Francisco Gavilán Jiménez. Departamento de Ingeniería Aeroespacial y Mecánica de Fluidos. Escuela Superior de Ingenieros. Universidad de Sevilla. Curso 2009-2010  
    
 [Problemas.pdf](http://jpcampillo.es/onewebmedia/Problemas.pdf) Primer bloque incluye gravitación. Juan P. Campillo. cc-by-nc Enlace validado julio 2015  
  
 [boletin_problemas_tema_2.pdf](http://www.juntadeandalucia.es/averroes/centros-tic/41008970/helvia/sitio/upload/boletin_problemas_tema_2.pdf)   
  
 [RP5-C_Gravitatorio_sol.pdf](http://selectividad.intergranada.com/Fisica/2_bach/RP5-C_Gravitatorio_sol.pdf)   
  
 [cagra.htm](http://iesfgcza.educa.aragon.es/depart/fisicaquimica/fisicasegundo/cagra.htm) Recursos, animaciones,...Luis Ortiz de Orruño  [bloque-2-interaccic3b3n-gravitatoria-2-campo-gravitatorio-terrestre.pdf](http://sitovg.files.wordpress.com/2012/06/bloque-2-interaccic3b3n-gravitatoria-2-campo-gravitatorio-terrestre.pdf)   
 Felipe Moreno Romero cc-by-nc-sa **(no disponible julio 2014)**  
 [index.php](http://scripts.mit.edu/~srayyan/PERwiki/index.php?title=Module_3_--_Mechanical_Energy_of_Orbits) Expresión de energía mecánica para órbitas elípticas dentro del curso Mechanics: The Study of Motion  
Estrategias para resolver problemas de campo gravitatorio  
 Josep Lluís Rodríguez  
 [estrategias_gravitacion.html](http://www.unedcervera.com/c3900038/estrategias/estrategias_gravitacion.html)   
 (en 2016 la última modificación es en 2003)  
 [Campo gravitatorio](http://www.edured2000.net/fyq/monotematicos/Campo%20gravitatorio/CAMPO%20GRAVITATORIO.htm)   
Física Tabú, Adrián Castelo  
-  [Newton y la Ley de la Gravitación Universal: ¿cómo llegó a ella? &#8211; Física Tabú](https://fisicatabu.com/newton-y-la-ley-de-la-gravitacion-universal-como-llego-a-ella/)  sobre cómo llegó Newton a la ley de la gravitación universal.  
 -  [El viejo profesor &#8211; Física Tabú](https://fisicatabu.com/el-viejo-profesor/)  sobre un viejo profesor que habla de agujeros negros a un alumno.  
 -  [Espaciotiempo curvo&#8230; ¡sin mallas elásticas! &#8211; Física Tabú](https://fisicatabu.com/espaciotiempo-curvo-sin-mallas-elasticas/)  Sobre cómo explicar que el espaciotiempo es curvo sin analogías extrañas.  
 -  [Caída libre: respuestas de veinte siglos de física &#8211; Física Tabú](https://fisicatabu.com/caida-libre-respuestas-de-veinte-siglos-de-fisica/)  sobre cómo se ha explicado la caída libre a lo largo de la historia, desde aristóteles hasta Einstein  
 -  [Materia oscura: en búsqueda y captura &#8211; Física Tabú](https://fisicatabu.com/materia-oscura-en-busqueda-y-captura/)  Sobre la materia oscura de parte de un amigo que investiga en eso  

[Las 5 órbitas de los satélites](https://www.sutelco.com/blog/las-5-%C3%B3rbitas-de-los-sat%C3%A9lites)  
Cita órbitas LEO, MEO, GEO, HEO y SSO  (en LOMCE se citan solo LEO, MEO y GEO)  

[What Ocean Tides Teach Us About the Sun and Moon -profmattstrassler.com](https://profmattstrassler.com/2024/01/12/what-ocean-tides-teach-us-about-the-sun/)  
> The Moon has a four-week cycle; it is full every four weeks (actually every 29.5 days). But ocean tides exhibit a two-week cycle; they are large one week and then smaller the next.  
> What’s behind this pattern? And what does it tell us about the Sun and Moon that we wouldn’t otherwise easily know? Perhaps surprisingly, it tells us that the average mass density of the Sun — its mass divided by its volume — is only a little smaller than the average mass density of the Moon. Here’s why.

## Recursos educativos Agencia Espacial Europea (ESA)
 [ESA - Educational material from ESA](http://www.esa.int/Education/Educational_material_from_ESA) Un ejemplo es "Lift-off" Ejercicios de física y química para secundaria basados en datos reales de la Agencia Espacial Europea  [Lift-off_BR223_SP.pdf](http://esamultimedia.esa.int/docs/edu/LiftOff/Lift-off_BR223_SP.pdf)   
 [ESA - Space for Kids](http://www.esa.int/esaKIDSes/) 

## Vídeos
En general asociables a "Universo", "astronomía", no solamente gravitación.Hay algunos vídeos en la página  [recursos de vídeos](/home/recursos/videos) , y en la de  [notación científica](/home/recursos/recursos-notacion-cientifica) Alguno más [](http://www.erikwernquist.com/wanderers/)   
BALANÇA DE CAVENDISH [![watch](https://img.youtube.com/vi/v5yxhbn3zJ4/0.jpg)](https://www.youtube.com/watch?v=v5yxhbn3zJ4)   
Da série: Mecânica Clássica. Dinâmica. Experimento de Cavendish. Balança de Torção. Determinação da Constante de Gravitação Universal.  

[Video shows how high you would be able to jump on each planet](https://www.reddit.com/r/space/comments/mysioy/video_shows_how_high_you_would_be_able_to_jump_on/)   

## Agujeros negros
  
 6 Cosas que No Sabías sobre los Agujeros Negros, Instituto de Física Teórica IFT,   
 Narrado y Animado por: José Luis Crespo Cepeda (@quantumfracture)  
 [![6 Cosas que No Sabías sobre los Agujeros Negros - YouTube](https://img.youtube.com/vi/BGysGIoA49M/0.jpg)](https://www.youtube.com/watch?v=BGysGIoA49M)   
 Los Agujeros Negros, esos monstruos sutiles (José L. F. Barbón), Instituto de Física Teórica IFT  
 [![Los Agujeros Negros, esos monstruos sutiles (José L. F. Barbón) - YouTube](https://img.youtube.com/vi/tXYDBCC8Thw/0.jpg)](https://www.youtube.com/watch?v=tXYDBCC8Thw)   
  
 [los-agujeros-negros-y-la-fisica-del.html](http://divulgamadrid.blogspot.com.es/2014/09/los-agujeros-negros-y-la-fisica-del.html)   
 [Un agujero negro (artificial) para el tejado de cada hogar - La Ciencia de la Mula Francis](https://francis.naukas.com/2009/10/15/un-agujero-negro-artificial-para-el-tejado-de-cada-hogar/)   
 [https://twitter.com/Rainmaker1973/status/1061211989614256128](https://twitter.com/Rainmaker1973/status/1061211989614256128) 
 This is why we think there's a black hole in middle of our galaxy: the orbits of the stars around the center of the Milky Way  
 [Watch Stars Orbit The Milky Way’s Supermassive Black Hole](https://www.universetoday.com/133511/watch-stars-orbit-milky-ways-supermassive-black-hole/)   
Motion of "S2" and other stars around the central Black Hole [![watch](https://img.youtube.com/vi/u_gggKHvfGw/0.jpg)](https://www.youtube.com/watch?v=u_gggKHvfGw)   
 [S2 (star) - Wikipedia](https://en.wikipedia.org/wiki/S2_(star))   
 [Stars orbiting supermassive black hole show Einstein was right again](https://phys.org/news/2017-08-stars-orbiting-supermassive-black-hole.html)   
 [Scientists finally confirm the Milky Way has a supermassive black hole ](http://www.astronomy.com/news/2018/10/scientists-confirm-the-milky-way-has-a-supermassive-black-hole)   
 [https://twitter.com/BTeseracto/status/1359562863350849537](https://twitter.com/BTeseracto/status/1359562863350849537) Pocas cosas hay que despierten más nuestro interés y curiosidad que los agujeros negros. Por ello, hoy queremos hablar de cómo sería ver uno de estos de forma directa. Los créditos de las animaciones de este hilo son: NASA’s Goddard Space Flight Center/Jeremy Schnittman.  
 [NASA Visualization Shows a Black Hole’s Warped World | NASA](https://www.nasa.gov/feature/goddard/2019/nasa-visualization-shows-a-black-hole-s-warped-world)  
 [![](https://www.nasa.gov/sites/default/files/styles/ubernode_alt_horiz/public/thumbnails/image/bh_visualization.jpg "") ](https://www.nasa.gov/sites/default/files/styles/ubernode_alt_horiz/public/thumbnails/image/bh_visualization.jpg)   

 [twitter imartividal/status/1651243202253299717](https://twitter.com/imartividal/status/1651243202253299717)  
 ¡Lo hemos hecho! Hemos encontrado "el eslabón perdido" de la Astrofísica Observacional de agujeros negros: el nexo que une el corazón de un agujero negro (su "sombra") con el chorro relativista que, de forma fascinante, escapa de allí casi a la velocidad de la luz. ¡Dentro hilo!  
![](https://pbs.twimg.com/media/FupihdBWIAEGTzn?format=jpg)   
 


## Agujeros de gusano
[twitter GastonGiribet/status/1326477932785766402](https://twitter.com/GastonGiribet/status/1326477932785766402)  
En 1959, Charles Misner se preguntaba por cuáles serían las "condiciones iniciales para un agujero de gusano", y estudiaba las soluciones a las ecuaciones de Einstein que describirían un objeto tal. Su idea era pensar un par de partículas neutras de la manera en la que Wheeler había pensado un par de partículas cargadas: no como fuentes materiales, sino como la mera topología del espacio formando un túnel íntimo entre los miembros del par.  
Los agujeros de gusano [wormholes] como soluciones de las ecuaciones de Einstein fueron pensados por primera vez por Ludwig Flamm, en 1916, y más tarde por el mismo Einstein y Nathan Rosen (1935). En los 80s estas geometrías espaciotemporales tuvieron su revival: En 1987 Morris y Thorne escribieron un famoso artículo, que sería publicado en 1988 en American Journal of Physics. En él motivan la investigación así: "The description touches with Carl Sagan's novel Contact, which, unlike most science fictions novels, treats such \[interestellar] travel in a manner that accords with the best 1986 knowledge of the laws of physics". En su paper, Morris y Thorne advierten de lo extraña que debería ser la materia necesaria para formar un wormhole: "No known material has this \[...] property".  
El mismo año, 1988, Morris y Thorne, esta vez en colaboración con Yurtsever, escriben un paper más técnico que publican en [Physical Review Letters](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.61.1446) en el que afirman: "It is argued that, if the laws of physics permit an advanced civilization to create and maintain a wormhole in space for interstellar travel, then that wormhole can be converted into a time machine with which causality might be violatable", y, en relación con este riesgo de violación de la causalidad, discuten sobre un asunto específico de notable actualidad: "whether field theory enforces an averaged version of the weak energy condition." Hoy vemos estos temas regresar con rostros rejuvenecidos en los trabajos de Maldacena et al., en los que se ahonda en el problema de construir wormholes físicamente aceptables.  


## Satélites
 Un tema muy visual es ver satélites reales  
  
 En el vídeo de "The Known Universe" aparecen órbitas de satélites  
  
 [Stuff in Space - Rogue space systems corporation](https://sky.rogue.space/)   
  
 Ver satélites en Google Earth  
 [Real-time Satellites in Google Earth - Google Earth Blog](http://www.gearthblog.com/blog/archives/2008/09/realtime_satellites_in_googl.html)   
  
 [Interactive graphic: Every active satellite orbiting earth — Quartz](http://qz.com/296941/interactive-graphic-every-active-satellite-orbiting-earth/)   
 The world above us  
 This is every active satellite orbiting earth  
 By David Yanofsky and Tim Fernholz  
 There are more than 1,200 active satellites orbiting earth right now, taking pictures, relaying communications, broadcasting locations, spying on you, and even housing humans. Thanks to a database compiled by the Union of Concerned Scientists, we can show you each one, as of August 21, 2014.  
 The satellites are sized according to their launch mass and are colored by...  
  
 [CelesTrak](https://celestrak.com/) Visualizador  
 [CelesTrak Orbit Visualization](https://celestrak.com/cesium/orbit-viz.php?tle=/pub/TLE/catalog.txt&satcat=/pub/satcat.txt&referenceFrame=1)   

## Velocidad de escape
En 2017 actualizo apuntes y algún ejercicio para aclarar que velocidad de escape no siempre está asociada a la energía de escape  
 [Escape velocity - Wikipedia](https://en.wikipedia.org/wiki/Escape_velocity#From_a_rotating_body)   
 [Escape velocity - Wikipedia](https://en.wikipedia.org/wiki/Escape_velocity#From_an_orbiting_body)   
The velocity corresponding to the circular orbit is sometimes called the **first cosmic velocity**, whereas in this context the escape velocity is referred to as the **second cosmic velocity**. [[10]](https://en.wikipedia.org/wiki/Escape_velocity#cite_note-12)   
 [Operation Plumbbob - Wikipedia](https://en.m.wikipedia.org/wiki/Operation_Plumbbob)  
 During the **Pascal-B** nuclear test, a 900-kilogram (2,000 lb) steel plate cap (a piece of armor plate) was blasted off the top of a test shaft at a speed of more than 66 km/s (41 mi/s; 240,000 km/h; 150,000 mph). Before the test, experimental designer Robert Brownlee had estimated that the nuclear explosion, combined with the specific design of the shaft, would accelerate the plate to approximately six times Earth's  [escape velocity](https://en.m.wikipedia.org/wiki/Escape_velocity) . [[8]](https://en.m.wikipedia.org/wiki/Operation_Plumbbob#cite_note-brownlee-8)  The plate was never found, but Dr. Brownlee believes [[9]](https://en.m.wikipedia.org/wiki/Operation_Plumbbob#cite_note-register-9)  that the plate did not leave the atmosphere, as it may even have been vaporized by compression heating of the atmosphere due to its high speed. Vídeo. How a Manhole Cover Became the Fastest Manmade Object Ever, Half as Interesting  
 [![watch](https://img.youtube.com/vi/NSeL5c65v-g/0.jpg)](https://www.youtube.com/watch?v=NSeL5c65v-g)   
 
[cienciadesofa.com Respuestas (LXXXVI): ¿Podríamos movernos más rápido por el espacio a bordo de asteroides interestelares?](https://cienciadesofa.com/2018/01/respuestas-lxxxvi-podriamos-movernos-mas-rapido-por-el-espacio-a-bordo-de-asteroides.html)  

![](https://2.bp.blogspot.com/-eHjdYtGYSIw/WkTLwP2HS3I/AAAAAAAAI9c/OcHs7Hn465MGqZkl3qCJdmuEI6lOAZfdwCLcBGAs/s1600/velocidad%2Bde%2Bescape.jpg)  
 
## Lanzamiento de satélites 

[spinlauch.com](https://www.spinlaunch.com/)  

 [![California-based startup to hurl rockets into space with mechanical centrifuge](https://img.youtube.com/vi/ENZ_rQLCa9Q/0.jpg)](https://www.youtube.com/watch?v=ENZ_rQLCa9Q "California-based startup to hurl rockets into space with mechanical centrifuge")   

 [![Spinlaunch Suborbital Test Launch 22 Oct 2021](https://img.youtube.com/vi/YB6Nw4MKE2g/0.jpg)](https://www.youtube.com/watch?v=YB6Nw4MKE2g "Spinlaunch Suborbital Test Launch 22 Oct 2021")   

[SpinLaunch And The History Of Hurling Stuff Into Space - hackaday](https://hackaday.com/2022/06/24/spinlaunch-and-the-history-of-hurling-stuff-into-space/)  

## Pozos de gravedad  

 [xkcd: Gravity Wells](https://xkcd.com/681/)  [![](https://imgs.xkcd.com/comics/gravity_wells.png "") ](https://imgs.xkcd.com/comics/gravity_wells.png)   

## Cálculo de trayectorias
[NASA Ames Research Center. Trajectory Browser](http://trajbrowser.arc.nasa.gov/)   

### Asistencia gravitacional
 [El Sistema Solar - Asistencia gravitatoria - eltamiz.com](http://eltamiz.com/2010/01/28/el-sistema-solar-asistencia-gravitatoria/)   
  
 Concepto de asistencia gravitacional ...  
 [Asistencia gravitatoria - Wikipedia, la enciclopedia libre](http://es.wikipedia.org/wiki/Asistencia_gravitatoria)   
  
 [NASA Science. Solar System exploration. A gravity assist primer](https://solarsystem.nasa.gov/basics/primer/)   
  
 [¿Por qué debe pasar por la Tierra una nave que va a Júpiter? - Eureka](http://danielmarin.naukas.com/2013/10/08/por-que-debe-pasar-por-la-tierra-una-nave-que-va-a-jupiter/)   
  
 [ESA - Space for Kids - 'Asistencia gravitacional'](http://www.esa.int/esaKIDSes/SEMFX63AR2E_Liftoff_0.html)   

## Misiones espaciales 

Puede enlazar con asistencia gravitacional 

[Missions, Solar System - nasa.gov](https://solarsystem.nasa.gov/missions)  

 [![Rosetta – The story so far (extended)](https://img.youtube.com/vi/uHyYadYpjZg/0.jpg)](https://www.youtube.com/watch?v=uHyYadYpjZg "Rosetta – The story so far (extended)")   

[JAMES WEBB SPACE TELESCOPE orbit - nasa.gov](https://webb.nasa.gov/content/about/orbit.html)  

[Parker Solar Probe Enters the Magnetically Dominated Solar Corona](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.127.255101)  

[Ya puedes generar tus propios códigos como los del paracaídas de Perseverance - microsiervos.com](https://www.microsiervos.com/archivo/espacio/genrar-condigos-paracaidas-perseverance.html)  
[msg2mars](https://msg2mars.sjwarner.me/)  

From launch to landing, a space shuttle's solid rocket booster journey is captured, with sound mixed and enhanced by Skywalker Sound.  
[![](https://img.youtube.com/vi/527fb3-UZGo/0.jpg)](https://www.youtube.com/watch?v=527fb3-UZGo "Riding the Booster Never Sounded Better - NASA")  
 
[Orion has now reached the Moon, here's how it will get back to Earth on the 11th of December. - 9gag](https://9gag.com/gag/awZnmNW)  

## Visualización satélites / basura espacial
 A veces se visualizan por separado satélites / basura espacial  
 Puede enlazarse con el tema de momento lineal y choques (1º Bachillerato)  
  
 La ESA tiene su propio departamento  
 [ESA - kernel (1)](http://www.esa.int/Our_Activities/Operations/Space_Debris_Office2)   
 [ESA - Space debris spotlight](http://www.esa.int/Our_Activities/Operations/Space_debris_spotlight)   
  
 [ESA - Satellites vs Debris](https://www.esa.int/ESA_Multimedia/Images/2021/02/Satellites_vs_Debris)  [Stream ESA &amp; UNOOSA on: Satellites vs space debris by European Space Agency | Listen online for free on SoundCloud](https://soundcloud.com/esa/satellites-vs-debris)   
  
 [Basura espacial: peligro en cielo y tierra - alt1040.com](http://alt1040.com/2012/12/basura-espacial-peligro-cielo-tierra)   
  
 [El problema de la basura espacial - microsiervos.com](https://www.microsiervos.com/archivo/ecologia/basura-espacial-cerca-tierra.html)   

[Visualizing the World’s Space Debris by Country Responsible - visualcapitalist.com](https://www.visualcapitalist.com/cp/space-debris-by-country/)  


 [![](https://img.youtube.com/vi/pkfKnxX-L0k/0.jpg)](https://www.youtube.com/watch?v=pkfKnxX-L0k "20000 Days in Space")   
> Simulation of Space Debris orbiting Earth. Created by the Institute of Aerospace Systems of the Technische Universität Braunschweig and shown at the 3rd Braunschweig Lichtparcours from June 19th to September 30th, 2010. Also available as an interactive screen saver for windows and Linux at http://www.days-in-space.de.  
More information about our research at http://www.space-debris.de.  
Color Key:  
Red: Satellites (operational or defunct)  
Yellow: Rocket bodies  
Green: Mission Related Objects (bolts, lens caps, etc.)  
Blue: Solid rocket motor slag  
White: Fragments from explosion events  
  
 Página con la información y un vídeo que da una idea en tiempo real  
 Real-time Satellites in Google Earth  
 [Real-time Satellites in Google Earth - Google Earth Blog](http://www.gearthblog.com/blog/archives/2008/09/realtime_satellites_in_googl.html)   
  
 Fichero kmz con la información  
 [SatelliteDatabase.kmz](http://adn.agi.com/SatelliteDatabase/SatelliteDatabase.kmz)   
 Una vez cargado en Google Earth en la parte izquierda se ve que se puede filtrar información por  
 -Realtime updates  
 -Active satellites  
 -Inactive satellites  
 -Debris  
 -Rocket bodies  
 Dentro de estas categorías se puede filtrar por país ...  
 Pinchando sobre un satélite se obtienen datos, y se puede hacer que se dibuje su trayectoria.  
 Todos los que terminan en DEB son Debris, basura espacial, y las trayectorias no suelen ser órbitas.  
  
(Inclinar la vista en Google Earth con "shift+rueda ratón")  
  
 [Waste Hours Staring at This Real-Time 3D Map of Objects Orbiting Earth](http://gizmodo.com/waste-hours-staring-at-this-real-time-3d-map-of-objects-1716175884)   
Se puede enlazar con choques   
 [https://twitter.com/Wikingenieria/status/991418106726805505](https://twitter.com/Wikingenieria/status/991418106726805505) Esto es lo que le sucede a un bloque de aluminio cuando una pieza de plástico de 14 gramos lo golpea a 24.140 km/h en el espacio. [![](https://pbs.twimg.com/media/DcI5gcOW0AIC9S8.jpg "") ](https://pbs.twimg.com/media/DcI5gcOW0AIC9S8.jpg)   

[2021: La estación espacial es amenazada por escombros y los astronautas se ven obligados a refugiarse - gizmodocom](https://es.gizmodo.com/la-estacion-espacial-es-amenazada-por-escombros-y-los-a-1848061400)  

[twitter esaoperations/status/1461350763347603457](https://twitter.com/esaoperations/status/1461350763347603457)  
[When debris disaster strikes](https://esa.int/Safety_Security/Space_Debris/When_debris_disaster_strikes)  
After #spacedebris-creating events, such as occurred this week, the risk to missions must be reassessed.  
"This major fragmentation event will not only more than double the long-term #collisionavoidance needs for missions in orbit at similar altitudes but will significantly increase the probability of mission-terminating collisions at lower altitudes" Holger Krag, H/ESA #SpaceSafety  
"This major fragmentation event will not only more than double the long-term #collisionavoidance needs for missions in orbit at similar altitudes but will significantly increase the probability of mission-terminating collisions at lower altitudes" Holger Krag, H/ESA #SpaceSafety  
![](https://pbs.twimg.com/media/FEfBaGoVcAcFMA6?format=jpg)  


## Gravedad en juegos / humor
Muchos juegos necesitan contemplar bien la gravedad ...  
 Aparte de juegos, un ejemplo curioso  
 [Google Gravity](http://mrdoob.com/projects/chromeexperiments/google-gravity/)   

## Vuelos parabólicos /  microgravedad 

 [Vuelo parabólico - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Vuelo_parab%C3%B3lico)  [Vuelos en gravedad cero por 5.000 euros](https://www.abc.es/ciencia/abci-vuelos-gravedad-cero-euros-201105310000_noticia.html)   
ZERO-G, vuelo parabólico (3 min. Denis Gómez)  
 [![](https://img.youtube.com/vi/AGua3RGUOhA/0.jpg)](https://www.youtube.com/watch?v=AGua3RGUOhA)   

 [![](https://img.youtube.com/vi/FdRukUmNzB4/0.jpg)](https://www.youtube.com/watch?v=FdRukUmNzB4 "Cats in zero gravity: The extended and enhanced version.")   

## Gravitón
 ¿La gravedad es un efecto de la curvatura del espacio-tiempo o una fuerza por intercambio de gravitones?  
 [CPAN - Centro Nacional de Fsica de Partculas, Astropartculas y Nuclear](https://www.i-cpan.es/detallePregunta.php?id=22)   

## Materia oscura
Aparece explícitamente en currículo Física 2º Bachillerato LOMCEEs algo también asociado a  [Física de partículas](/home/recursos/fisica/fisica-de-particulas)   
 [En busca de la materia oscura en el LHC &#8211; Mola Saber](https://molasaber.org/2015/02/06/en-busca-de-la-materia-oscura-en-el-lhc/)   
  
Caos deterministaEs un contenido que surge en currículo LOMCE. Se trata en página aparte más general que trata la  [teoría del caos](/home/recursos/fisica/teoria-del-caos)   
Juegos de rol  
  
 [https://twitter.com/Chuso_Jar/status/1171790135282847744](https://twitter.com/Chuso_Jar/status/1171790135282847744)   
Fue idea de @OscarRecioColl y me propuso hacerlo con Coriolis. Al final lo estoy haciendo con Star Trek. Da igual da.  
 Voy a probar a enseñarles Gravitación haciendo q los estudiantes sean tripulantes de una nave estelar.  
 Astrónomo, geólogo, físico de atmósfera, médico, ...operador de sondas y operador de nave.  
 Cada uno en su especialidad utilizará la Ley de la gravitación universal para sacar información de los sistemas solares y planetas que visiten.  
 Hemos empezado por Inis, planeta de 1000 km de radio y masa 10^22.  
 Tendrán q calcular ...  
 velocidad orbital y periodo para colocar la nave a 500km sobre superficie, densidad del planeta para saber composición y gravedad en superficie para asegurar que un descenso es seguro.  
 [![](https://pbs.twimg.com/media/EEMI6JsXsAELwWE?format=jpg&name=900x900 "") ](https://pbs.twimg.com/media/EEMI6JsXsAELwWE?format=jpg&name=900x900)   

### Leyes Kepler
 [Leyes de Kepler - Wikipedia, la enciclopedia libre](https://es.m.wikipedia.org/wiki/Leyes_de_Kepler#/media/Archivo%3ASolarSystem_Radii_and_Period_(math).svg)   
Ley Titius-Bode [Ley_de_Titius-Bode](https://es.m.wikipedia.org/wiki/Ley_de_Titius-Bode)   

## Enlaces recursos laboratorios virtuales / simulaciones


[twitter onio72/status/1455288551973834754](https://twitter.com/onio72/status/1455288551973834754)  
Cálculo del vector fuerza gravitatoria entre dos masas situadas en puntos conocidos del plano. En la animación se pueden modificar las posiciones y los valores de las masas. Moviendo el cursor se ve paso a paso la resolución del problema. Física 2Bach  
[Ley de la Gravitación Universal (cálculo vectorial)](https://www.geogebra.org/m/qmcymndy)  


[twitter onio72/status/1455287967564582913](https://twitter.com/onio72/status/1455287967564582913)  
Modelo de ejercicio de gravitación consistente en calcular la fuerza total sobre una masa debida a otras dos situadas en puntos conocidos del mismo plano. Permite visualizar por separado los vectores fuerza, el resultado final y el desarrollo. Física 2Bach  
[Fuerza gravitatoria debida a dos masas](https://www.geogebra.org/m/EUU4xqaf)  


   
[Physique et simulations numériques - ressources.univ-lemans.fr](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/mndivers.html)   
 Jean-Jacques ROUSSEAU. Faculté des Sciences exactes et naturelles. Université du Maine  
 Francés. Niveaux : Lycée, premier et second cycles.  
 Mécanique céleste  

   *  [Système solaire](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/systsol.html) 
   *  [Mouvement de la Terre](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/mouveter.html) 
   *  [Système de Ptolémée](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/ptolemee.html) 
   *  [Satellite géostationnaire](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/satelstat.html) 
   *  [Satellites terrestres](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/satelgene.html) 
   *  [Les saisons](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/saisons.html) 
  
 Kean University. Dr George Kolodiy  
 EHW 5: Rotation, Gravity and Sattelites  [ehw4.htm](http://www.kean.edu/~gkolodiy/ph2091web/ehw4.htm)   
 EHW 4: Ch: 8-10: Rotation, Gravity and Sattelites  [ehw4.htm](http://coe.kean.edu/phys1000/ehw4.htm)   
  
 [LAW OF GRAVITY](http://www.kean.edu/~gkolodiy/physics/gravity/gravity.htm)   
Simulación interactiva ley de gravitación entre dos masas  
  
 [The Force of Gravity](http://www.kean.edu/~gkolodiy/physics/gravity/GravityHewitt.html)   
Simulación interactiva ley de gravitación, representación gráfica  
  
 [Gravity - projectile - www.kean.edu](http://www.kean.edu/~gkolodiy/physics/gravity/projectile.html)   
Lección con animaciones para ver una órbita como un caso concreto de lanzamiento  
  
 Física con ordenador. Dinámica celeste  
 [Din&aacute;mica celeste](http://www.sc.ehu.es/sbweb/fisica_/celeste/celeste.html)   
 Material con derechos de autor. © Ángel Franco García -1998-2011  
  
 Gravitación. Aplicación a satélites.Velocidad de escape.  
 [Satelites ](http://www.sociedadelainformacion.com/departfqtobarra/gravitacion/satelites/Keplersatelite.html)   
 Jesús Ruiz Felipe Profesor de Física del Instituto Cristóbal Pérez Pastor Tobarra (Albacete)  
  
 ANIMACIONES MODELLUS DE FÍSICA - 7 (CONCEPTOS DE GRAVITACIÓN)  
 [Animaciones07.htm](http://intercentres.edu.gva.es/iesleonardodavinci/Fisica/Animaciones/Animaciones07.htm)   
Licenciamiento cc-by-nc-sa  
  
 Mi sistema solar, permite poner más de 2 cuerpos (enlaza con  [caos determinista](/home/recursos/fisica/teoria-del-caos) )  
 [Mi sistema solar - PhET](https://phet.colorado.edu/sims/html/my-solar-system/latest/my-solar-system_es.html)   
 ![](https://phet.colorado.edu/sims/html/my-solar-system/latest/my-solar-system-900.png)  
 
 ([Versión antigua en flash](http://phet.colorado.edu/sims/my-solar-system/my-solar-system_es.html))
 
 [Gravedad y órbitas - PhET](https://phet.colorado.edu/sims/html/gravity-and-orbits/latest/gravity-and-orbits_es.html)  
 ![](https://phet.colorado.edu/sims/html/gravity-and-orbits/latest/gravity-and-orbits-900.png)  
  
 [IES Al-Ándalus &#8211; 41000557 Arahal (Sevilla)](http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=139&Itemid=199)  
 [Laboratorio virtual: Teoría de los epiciclos de Ptolomeo.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/mov_epiciclo.swf)  
 [Laboratorio virtual: Primera ley de Kepler.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/kepler1ley.swf)  
 [Laboratorio virtual: Segunda ley de Kepler.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/kepler2ley.swf)  
 [Laboratorio virtual: Movimiento orbital de un satélite en torno a la Tierra.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/gravitacion_sateliteartificial.swf)  
 [Laboratorio virtual: Campo gravitatorio creado por masas puntuales. Vectores.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/campo_gravitatorio_vectores.swf)  
 [Laboratorio virtual: Campo gravitatorio creado por varias masas puntuales. Líneas de campo.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/campogravitatorio_lineas.swf)  
 [Laboratorio virtual: Movimiento de una partícula en un campo gravitatorio.](http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fisica2bach/campo_gravitatorio_masaprueba.swf)  
 Jose Antonio Navarro Dominguez   

### Leyes de Kepler

 [Leyes de Kepler](https://phet.colorado.edu/es/simulations/keplers-laws)   
![](https://phet.colorado.edu/sims/html/keplers-laws/latest/keplers-laws-900.png)  

 [ Gradually Change the velocity of the cannon](http://www.kean.edu/~gkolodiy/physics/gravity/kepler1.html)   
 [ http://www.kean.edu/~gkolodiy/physics/gravity/kepler2.html](http://www.kean.edu/~gkolodiy/physics/gravity/kepler2.html)   
 [Kepler's Third law](http://www.kean.edu/~gkolodiy/physics/gravity/kepler3.html)  (permite "lanzar" un planeta y ver qué tipo de trayectoria describe)  
 Lecciones con animacionesMovimiento bajo las Leyes de Kepler  
 [Virtual Lab:Keppler Motion Java Applet](http://teleformacion.edu.aytolacoruna.es/FISICA/document/applets/Hwang/ntnujava/Kepler/Kepler_s.htm)   
 (Url mirror en España),  
 Autor: Fu-Kwun Hwang, Dept. of physics, National Taiwan Normal University  
 Traducción: José Villasuso  
 Opciones para ver las tres leyes de Kepler  
  
 Interactive Demonstrations for Astronomy  
 [Department of Physics and Astronomy | San Jose State University](http://www.physics.sjsu.edu/Tomley/kepler.html)   
 Kepler's First and Second Laws  
 Kepler's Third Law  
  
 Física con ordenador. Leyes de Kepler  
 [Las leyes de Kepler](http://www.sc.ehu.es/sbweb/fisica_/celeste/kepler/kepler.html)   
 Material con derechos de autor. © Ángel Franco García -1998-2011  

### Energía potencial gravitatoria y aplicaciones: satélites, velocidad escape, ...
 Satélites y Energía. Problema 2º Bachillerato.  
 [SATLITES Y ENERGA](http://recursostic.educacion.es/newton/web/materiales_didacticos/satelites_y_energia/index.html)   
 Mª del Mar Hijano Reyes, cc-by-nc-sa  
(En julio 2014 applet con java 1.7_55 indica "Bad applet class name")  
  
 Velocidad de escape. Recurso didáctico 2º Bachillerato  
 [Velocidad_escape](http://recursostic.educacion.es/newton/web/materiales_didacticos/vescape/velesca_1.html)   
 Inma Sevila Pascual, cc-by-nc-sa  
 (En julio 2014 applet con java 1.7_55 indica "Bad applet class name")  
  
 Montaña de Newton con cañón  
 [Newton's Mountain](http://galileoandeinstein.physics.virginia.edu/more_stuff/flashlets/NewtMtn/home.html)   
 Simulación que muestra la trayectoria de un cuerpo lanzado desde la Tierra en un punto situado a una altura determinada.  
Bala de cañón de Newton  
  
 [Bala de cañón de Newton - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Bala_de_ca%C3%B1%C3%B3n_de_Newton)   
  
Motores de cohetes, cálculos energía/momento/propulsión  
 [NASA Conducts SLS Booster Test for Future Artemis Missions | NASA](https://www.nasa.gov/exploration/systems/sls/nasa-conducts-sls-booster-test-for-future-artemis-missions.html)   
 [https://twitter.com/jurijkovalenok1/status/1344678450133684224](https://twitter.com/jurijkovalenok1/status/1344678450133684224) When hot gases escape from the nozzle at high speed, the rocket body gains momentum in the opposite direction. The law of conservation of momentum for the case with variable mass. #NASA #SpaceX #ElonMusk @elonmusk  
 [Tsiolkovsky rocket equation - Wikipedia](https://en.wikipedia.org/wiki/Tsiolkovsky_rocket_equation)   

### Campo gravitatorio
  
 Campo Gravitatorio Terrestre. Problema 2º Bachillerato  
 [CAMPO GRAVITATORIO TERRESTRE](http://recursostic.educacion.es/newton/web/materiales_didacticos/campo_gravitatorio_prob/index.html)   
 Mª Josefa Grima y Javier Soriano, cc-by-nc-sa  
Incluye applet con movimiento atravesando la Tierra  
  
 Algunas simulaciones de  [campo eléctrico](/home/recursos/fisica/recursos-campo-electrico)  sirven conceptualmente para el campo magnético si se hace una analogía con cargas negativas que crean campos atractivos, por ejemplo  
 [3-D Vector Field Simulation](http://www.falstad.com/vector3d/index.html)   
  
 Campo gravitatorio  
 [http://www.xtec.cat/~ocasella/applets/gravita/appletsol2.htm](http://www.xtec.cat/%7Eocasella/applets/gravita/appletsol2.htm)   
 Visualización campo, líneas de fuerza, superficies equipotenciales  
 Permite configuraciones de masas  
 Tavi Casellas. Licenciamiento cc-by-nc-nd  

### Conceptos básicos
Simulación para ver dependencia masa y distancia [gravity-force-lab_es.html](https://phet.colorado.edu/sims/html/gravity-force-lab/latest/gravity-force-lab_es.html)   
  
Salvador Hurtado, cc-by  
 [masa y peso](https://labovirtual5.blogspot.com.es/search/label/masa%20y%20peso)   
 Experimento Cavendish cualitativo  
 [Laboratorio Virtual 5: Ley de gravitación universal](https://labovirtual5.blogspot.com.es/search/label/Ley%20de%20gravitaci%C3%B3n%20universal)   
 [twitter MolaSaber/status/1273959391063048192](https://twitter.com/MolaSaber/status/1273959391063048192) Desconfiad de los remedios milagrosos, seguramente no funcionan y pueden ser peligrosos para la salud.   
  
![](https://pbs.twimg.com/media/Ea4DP2LXsAEAUKL?format=jpg)

[![](https://img.youtube.com/vi/D8H1RNtka6s/0.jpg)](https://www.youtube.com/watch?v=D8H1RNtka6s "How High You Could Jump on Different Planets in 3D")   

[twitter SeekersCosmos/status/1501430062557458434](https://twitter.com/SeekersCosmos/status/1501430062557458434)  
Perception of gravity in different celestial bodies in our solar system!   
  
