# Legislación educativa

[comentario]: # (ToDo importante tabla de contenido, aunque con migración empiezo a separar en varias páginas)  

Aquí se intentará poner la normativa, tanto a nivel europeo, estatal como autonómico (Madrid), para las distintas leyes (LOMLOE, LOMCE, LOE ...) fundamentalmente en lo relacionado con la especialidad de Física y Química, como por ejemplo la que fija el currículo.  
A veces se incluyen dictámenes (Consejo Escolar de Estado, Consejo de Estado) que no son propiamente legislación, pero aportan información sobre detalles de la elaboración de la normativa.  
Se puede agrupar de distintas maneras: estatal/autonómica , eso/bachillerato, currículo/evaluación ...  
Se incluye referencia a legislación no vigente (por ejemplo LOGSE y LOE en el momento en el que lo vigente es LOMCE) porque puede ser útil para comparar materias y también para tener información sobre materias desaparecidas (Mecánica desparece con LOE, Electrotecnia desaparece con LOMCE ...)  
Cuando se ponen enlaces a BOE, se intenta que sea a "legislación consolidada", que es una manera de verla que acumula correcciones y muestra última versión  
Cuando se ponen enlaces a normativa de BOCM, se pone enlace a BOCM en html o pdf, o a wleg que permite consultar legislación, y que a veces muestra también legislación consolidada/tiene la versión revisada con comentarios sobre modificaciones respecto a versiones iniciales.  
A veces se cita legislación de "territorio MEC" (Ceuta y Melilla que no tienen competencias autonómicas de educación) porque sirve de referencia en ciertos temas  
Inicialmente tenía toda la normativa en la misma página, pero reorganizo separando según cada ley. Las evaluaciones finales / "reválidas" asociadas las asocio a LOMCE.  
Puede haber legislación asociada a educación, como temas LGTBI  
Sobre algunos temas como ratios, horarios,... puede haber normativa dispersa entre normativa estatal y autonómica, órdenes, instrucciones ... a veces recopilo ideas en el blog.   
[Ratios en educación: normativa](https://algoquedaquedecir.blogspot.com/2018/12/ratios-en-educacion-normativa.html)  
[Horarios docentes secundaria](http://algoquedaquedecir.blogspot.com/2017/09/horarios-docentes-secundaria.html)   

[twitter tonisolano/status/1550439287769923584](https://twitter.com/tonisolano/status/1550439287769923584)  
En cambiar leyes somos expertos, así que no hay problema de cara al futuro. De hecho, somos expertos en cambiar leyes que no suponen ningún cambio real en las aulas, lo que no sé si es bueno o no. En realidad, es bueno para los buenos y malo para los malos, lo que deja todo igual  
Tendremos que acostumbrarnos por tanto a no confiar en leyes que nadie piensa cumplir, los que las redactan porque saben que son efímeras, y los que han de cumplirlas, porque no se las creen o les parecen inadecuadas u opinan que son malas. Y da igual cuando leas esto.  

Sobre sistema educativo, diagramas  

[Esquema navegable del Sistema Educativo - madrid.org](http://dgbilinguismoycalidad.educa.madrid.org/guiaparafamilias/esquema-navegacion-sistema-educativo.php)  


[twitter CreatiBrain/status/1550489238851952640](https://twitter.com/CreatiBrain/status/1550489238851952640)  
Deseando que llegue el día en que leerse una ley suponga un reto para mejorar, desde la realidad que tenemos, nuestra Escuela Pública. Y no suponga una capa de pintura a un sistema educativo que le chirrían las marchas. Gracias @Farohumor por tus viñetas.  
![](https://pbs.twimg.com/media/FYRxvt4WIAIy7h4?format=jpg)   

Subpáginas
* [LOMLOE](/home/legislacion-educativa/lomloe)  
    * [Evaluación LOMLOE](/home/legislacion-educativa/lomloe/evaluacion)  
    * [Currículo LOMLOE](/home/legislacion-educativa/lomloe/curriculo)  
    * [Currículo LOMLOE concepto y estructura](/home/legislacion-educativa/lomloe/curriculo-concepto-estructura)  
    * [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  
* [LOMCE](/home/legislacion-educativa/lomce)  
    * [Evaluaciones Finales, "reválidas" (surgen con LOMCE y desaparecen con LOMLOE)](/home/legislacion-educativa/lomce/evaluaciones-finales)  
    * [Evaluaciones Finales Bachillerato](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-bachillerato)  
    * [Evaluaciones Finales ESO](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-eso)  
* [LOE](/home/legislacion-educativa/loe)  

## Legislación europea

### [RECOMENDACIÓN DEL PARLAMENTO EUROPEO Y DEL CONSEJO de 18 de diciembre de 2006 sobre las competencias clave para el aprendizaje permanente (2006/962/CE)](http://eur-lex.europa.eu/legal-content/ES/ALL/?uri=CELEX:32006H0962)   

### [RECOMENDACIÓN DEL CONSEJO de 22 de mayo de 2018 relativa a las competencias clave para el aprendizaje permanente](https://eur-lex.europa.eu/legal-content/ES/TXT/PDF/?uri=CELEX:32018H0604(01))  
Se cita en currículo LOMCE ESO y Bachillerato

Ver [competencias](/home/legislacion-educativa/competencias)

## Legislación estatal

### [Orden ECD/65/2015, de 21 de enero, por la que se describen las relaciones entre las competencias, los contenidos y los criterios de evaluación de la educación primaria, la educación secundaria obligatoria y el bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-738) 

Ver [competencias](/home/legislacion-educativa/competencias)

### [LOMLOE](/home/legislacion-educativa/lomloe)

### [LOMCE](/home/legislacion-educativa/lomce) 

### [LOE](/home/legislacion-educativa/loe)

### [LOGSE](/home/legislacion-educativa/logse)  

### LGE

#### Ley 14/1970, de 4 de agosto, General de Educación y Financiamiento de la Reforma Educativa.
 [BOE-A-1970-852 Ley 14/1970, de 4 de agosto, General de Educación y Financiamiento de la Reforma Educativa.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1970-852) 

## Legislación Comunidad de Madrid

#### Legislación general
No asociable a una ley en concreta / aplicable a varias  

#### Instrucciones de inicio de curso
La comunidad de Madrid "legisla"/fija aspectos importantes a través de las "instrucciones de inicio de curso" que suele publicar en el mes de julio  
Hay instrucciones separadas para Bachillerato de excelencia, Bachillerato a distancia, Adultos ....  
Ejemplos (se aporta CSV para descargar de  [www.madrid.org/csv](http://www.madrid.org/csv) ), aunque pasados algunos años el documento no se puede recuperar vía CSV.

* [RESOLUCIÓN CONJUNTA DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y DE ORGANIZACIÓN EDUCATIVA POR LA QUE SE DICTAN INSTRUCCIONES SOBRE MEDIDAS ORGANIZATIVAS Y DE PREVENCIÓN, HIGIENE Y PROMOCIÓN DE LA SALUD FRENTE A COVID-19 PARA CENTROS EDUCATIVOS EN EL CURSO 2020-2021](https://www.educa2.madrid.org/web/educamadrid/principal/files/6aa7c315-e3ff-424a-b8e8-eea7f6b0105f/RESOLUCION_CONJUNTA_1_19466920.pdf?t=1594377148175) CSV: 0981150617359688986935  
* [INSTRUCCIONES DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y DE ORGANIZACIÓN EDUCATIVA, DE 9 DE JULIO DE 2020, SOBRE COMIENZO DEL CURSO ESCOLAR 2020-2021 EN CENTROS DOCENTES PÚBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID.](https://www.educa2.madrid.org/web/educamadrid/principal/files/7c653f50-b8ca-492b-9949-2b394a829334/instrucciones%20inicio%20de%20curso%2020%2021_2_19466891.pdf?t=1594377006513) CSV: 0981583806249335051204    
* [INSTRUCCIONES de las Viceconsejerías de Política Educativa y Ciencia y de Organización Educativa, de 5 de julio de 2019, sobre comienzo del curso escolar 2019- 2020 en centros docentes públicos no universitarios de la Comunidad de Madrid](http://www.comunidad.madrid/sites/default/files/doc/educacion/instrucciones_inicio_curso_2019_2020_firmado.pdf). CSV : 1038077242549437344108   
* [INSTRUCCIONES DE LAS VICECONSEJERÍAS DE EDUCACIÓN NO UNIVERSITARIA, JUVENTUD Y DEPORTE Y DE ORGANIZACIÓN EDUCATIVA SOBRE COMIENZO DEL CURSO ESCOLAR 2017-2018 EN CENTROS PÚBLICOS DOCENTES NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID](http://www.madrid.org/cs/Satellite?blobcol=urldata&blobheader=application/pdf&blobheadername1=Content-Disposition&blobheadervalue1=filename=InstruccionesInicioCurso1718.pdf&blobkey=id&blobtable=MungoBlobs&blobwhere=1352934784947&ssbinary=true) 1258427687262458706528  
* Instrucciones de la Dirección General de Educación Infantil, Primaria y Secundaria sobre las enseñanzas de Bachillerato a distancia para el curso 2017/2018 0999743164371485776473  
* Instrucciones de la Dirección General de Educación Infantil, Primaria y Secundaria sobre organización e incorporación al Programa de Excelencia en bachillerato en institutos de educación secundaria de la Comunidad de Madrid para el curso académico 2016-2017 054937888323224304982  
* Instrucciones de la Dirección General de Educación Infantil, Primaria y Secundaria sobre organización e incorporación al Programa de Excelencia en bachillerato en institutos de educación secundaria de la Comunidad de Madrid para el curso académico 2017-2018 1018409451230439105384  
* Instrucciones de la Dirección General de Educación Infantil, Primaria y Secundaria, sobre la organización y funcionamiento de los centros de educación de personas adultas, en el régimen presencial, semipresencial y a distancia, para el curso académico 2017/2018.0944401242200991875179  
* Instrucciones de la Dirección General de Educación Secundaria, Formación Profesional y Enseñanzas de Régimen Especial, para el curso académico 2015/2016, sobre organización y desarrollo de las enseñanzas para la obtención del título de Graduado en Educación Secundaria Obligatoria para personas adultas (Nivel II), en el régimen presencial y a distancia, así como sobre el Bachillerato a distancia en los Institutos de Educación Secundaria. 1295038843913508541248
* [Instrucciones de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial sobre organización y desarrollo del Programa de Excelencia en Bachillerato en institutos de educación secundaria de la Comunidad de Madrid para el curso académico 2022-
2023.](https://www.comunidad.madrid/transparencia/sites/default/files/regulation/documents/6-instrucciones_dgesfpre_progr_excel_bto_2022-2023_30292860.pdf)  
* [INSTRUCCIONES DE LAS VICECONSEJERÍAS DE POLÍTICA EDUCATIVA Y DE ORGANIZACIÓN EDUCATIVA, SOBRE COMIENZO DEL CURSO ESCOLAR 2023-2024 EN CENTROS DOCENTES PÚBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID.](https://www.comunidad.madrid/transparencia/sites/default/files/regulation/documents/resolucion_conjunta_instrucciones_inicio_de_curso_23-24.pdf)  
* [INSTRUCCIONES DE LA VICECONSEJERÍA DE POLÍTICA Y ORGANIZACIÓN EDUCATIVA, SOBRE COMIENZO DEL CURSO ESCOLAR 2024-2025 EN CENTROS DOCENTES PÚBLICOS NO UNIVERSITARIOS DE LA COMUNIDAD DE MADRID](https://www.comunidad.madrid/sites/default/files/instrucciones_inicio_curso_2024_2025.pdf) 

#### Otras instrucciones
[INSTRUCCIONES CONJUNTAS DE LA DIRECCIÓN GENERAL DE EDUCACIÓN INFANTIL Y PRIMARIA Y DE LA DIRECCIÓN GENERAL DE EDUCACIÓN SECUNDARIA, FORMACIÓN PROFESIONAL Y ENSEÑANZAS DE RÉGIMEN ESPECIAL, SOBRE LA APLICACIÓN DE MEDIDAS PARA LA EVALUACIÓN DE LOS ALUMNOS CON DISLEXIA, OTRAS DIFICULTADES ESPECÍFICAS DE APRENDIZAJE O TRASTORNO POR DÉFICIT DE ATENCIÓN E HIPERACTIVIDAD EN LAS ENSEÑANZAS DE EDUCACIÓN PRIMARIA, EDUCACIÓN SECUNDARIA OBLIGATORIA Y BACHILLERATO REGULADAS EN LA LEY ORGÁNICA 2/2006, DE 3 DE MAYO, DE EDUCACIÓN.](http://www.madrid.org/dat_capital/deinteres/impresos_pdf/IntruccionesEvAlumnosDislexia.pdf) 

#### LEY 7/2017, de 27 de junio, de Gratuidad de los Libros de Texto y el Material Curricular de la Comunidad de Madrid.
 [LEY 7/2017, de 27 de junio, de Gratuidad de los Libros de Texto y el Material Curricular de la Comunidad de Madrid.](http://www.madrid.org/wleg_pub/secure/normativas/contenidoNormativa.jsf?nmnorma=9839#no-back-button)  
 
### Ley maestra Madrid
[Ley 1/2022, de 10 de febrero, Maestra de Libertad de Elección Educativa de la Comunidad de Madrid.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-6768)  
Ver post [ Ley Maestra de Libertad de Elección Educativa](https://algoquedaquedecir.blogspot.com/2021/01/ley-maestra-de-libertad-de-ensenanza-de.html)  

### Atención a la diversidad
[DECRETO 23/2023, de 22 de marzo, del Consejo de Gobierno, por el que se regula la atención educativa a las diferencias individuales del alumnado en la Comunidad de Madrid.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/03/24/BOCM-20230324-1.PDF)   

[RESOLUCIÓN de 27 de abril de 2023, del Director General de Universidades y Enseñanzas Artísticas Superiores, por la que se establecen medidas y adaptaciones para los alumnos con dislexia en las pruebas de evaluación para el acceso a la Universidad.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/05/11/BOCM-20230511-18.PDF)  

[RESOLUCIÓN DEL DIRECTOR GENERAL DE UNIVERSIDADES Y ENSEÑANZAS ARTÍSTICAS SUPERIORES, POR LA QUE SE ESTABLECEN MEDIDAS Y ADAPTACIONES PARA LOS ALUMNOS CON DISLEXIA EN LAS PRUEBAS DE EVALUACIÓN PARA EL ACCESO A LA UNIVERSIDAD - uc3m.es](https://www.uc3m.es/pruebasacceso/media/pruebasacceso/doc/archivo/doc_resolucion-dislexia/resolucion-dislexia---dgueas---adaptacion-2023---firmada---27-04-2023.pdf)  

### Legislación asociada a Bachillerato de excelencia
Surge con LOE pero continua en LOMCE  
[DECRETO 63/2012, de 7 de junio, del Consejo de Gobierno, por el que se regula el Programa de Excelencia en Bachillerato en institutos de Educación Secundaria de la Comunidad de Madrid](http://www.madrid.org/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=7718&cdestado=P#no-back-button)  

[ORDEN 11995/2012, de 21 de diciembre, de la Consejería de Educación, Juventud y Deporte, de organización, funcionamiento e -incorporación al Programa de Excelencia en Bachillerato en institutos de Educación Secundaria de la Comunidad de Madrid](http://www.madrid.org/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerHtml&nmnorma=8042&cdestado=P#no-back-button) 

### [LOMCE](/home/legislacion-educativa/lomce)

### [LOE](/home/legislacion-educativa/loe)

### LOGSE

#### Decreto 47/2002 currículo Bachillerato (LOGSE)
[DECRETO 47/2002, de 21 de marzo, por el que se establece el currículo del Bachillerato para la Comunidad de Madrid.](http://gestiona.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=1452) 

#### Orden de 18 de septiembre de 1990 por la que se establecen las proporciones de profesionales/alumnos en la atención educativa de los alumnos con necesidades especiales 
 [BOE-A-1990-24063 Orden de 18 de septiembre de 1990 por la que se establecen las proporciones de profesionales/alumnos en la atención educativa de los alumnos con necesidades especiales.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-1990-24063) 

### Normativa acceso universidad Madrid
Hay normativa EvAU según el año. Hay normativa específica de adaptaciones  
[RESOLUCIÓN de 10 de marzo de 2010, de la Dirección General de Educación Secundaria y Enseñanzas Profesionales, por la que se establece el modelo y procedimiento para la elaboración del informe de propuesta de adaptación de la prueba de acceso a las enseñanzas universitarias de los estudiantes que presenten discapacidad.](http://www.madrid.org/wleg_pub/secure/normativas/contenidoNormativa.jsf?opcion=VerWord&idnorma=7450#no-back-button)  
[ORDEN PRE/1822/2006, de 9 de junio, por la que se establecen criterios generales para la adaptación de tiempos adicionales en los procesos selectivos para el acceso al empleo público de personas con discapacidad.](https://www.boe.es/buscar/doc.php?id=BOE-A-2006-10483) 

## Legislación otras comunidades con utilidad directa en Madrid

### Aragón

#### Ordenación Curricular / Normativa ESO, Bachillerato y otras enseñanzas
Como en LOMCE hay competencias (también en bachillerato, que no había en LOE), el mapeo de contenidos / criterios de evaluación/ estándares de aprendizaje evaluables a competencias es algo que se puede sacar de manera oficial del currículo de Aragón. Como criterios de evaluación/ estándares de aprendizaje evaluables son comunes a nivel estatal, lo que ha hecho Aragón vale para cualquier comunidad.  

Con LOMLOE cambian nombres de competencias, y página que era conjunta para ESO y BACHILLERATO la desdoblan en 2, donde parte de la materias ya son LOMLOE  
[NORMATIVA ESO - educaragon.es](https://educa.aragon.es/web/guest/-/normativa-eso)  
[NORMATIVA BACHILLERATO - educaragon.es](https://educa.aragon.es/web/guest/-/norma-bachillerato)  

[FÍSICA Y QUÍMICA ESO (pdf 20 páginas)](https://educa.aragon.es/documents/20126/521996/26+FISICA+Y+QUIMICA%281%29.pdf/94672315-9a20-7b1a-dbf7-e1c4ecffdb65?t=1578923092122)  

En 2018 detecto que Aragón también añade criterios de evaluación: en 2º ESO "Crit.FQ.5.7. Conocer la percepción, la propagación y los aspectos de la luz y del sonido relacionados con el medioambiente."

### Castilla-La Mancha

#### Currículo Física de 2º Bachillerato y competencias Gobierno sobre los estándares de aprendizaje
En 2017 detecto que en Castilla-La Mancha añaden estándares de aprendizaje que no están en el currículo estatal (Real Decreto 1105/2014)  
[https://twitter.com/FiQuiPedia/status/833327409999048705](https://twitter.com/FiQuiPedia/status/833327409999048705)  

Hasta ese momento yo consideraba, según  [artículo 3 de RD 1105/2014](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a3)  y  [artículo 6bis de LOE modificada por LOMCE](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a6bis)  que los estándares de aprendizaje evaluables de materias troncales eran competencia exclusiva del Gobierno: de hecho en Madrid no ponen estándares, se remiten a los estatales.  
Tras plantear la duda al Ministerio este indica que es legal lo que ha hecho Castilla-La Mancha, lo que abre la puerta a que comunidades autónomas e incluso centro complementen con nuevos estándares. No lo comparto lo que me indican (última respuesta de 28 de julio de 2017), pero es la interpretación del ministerio y a día de hoy lo que manda.  

Pongo enlace a carpeta con documentación en google drive: documentos editados para quitar datos personales (dirección), manteniendo nombre y apellidos y NIF, que estén en BOCM. [https://drive.google.com/open?id=0B-t5SY0w2S8icGNnNHpwRkpRelU](https://drive.google.com/open?id=0B-t5SY0w2S8icGNnNHpwRkpRelU)  
Pendiente crear carpeta en drive.fiquipedia, ConsutasMECDyMadrid/LOMCE-CompetenciasGobiernoEstándares  
