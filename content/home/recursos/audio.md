
# Recursos audio

(Esta página se creó inicialmente en 2011 como parte del curso que pedía incluir recursos con licenciamiento y no ha sido modificada desde entonces) 
Los recursos de audio en Física y Química no son habituales.  
En física puede haber audios asociados a ciertos bloques de contenidos, como las ondas sonoras en física de 2º de Bachillerato, y en general también puede haber audio asociado a grabaciones de interés, como son las lecturas de Feynman sobre física.
*  Efecto Doppler: coches de policía pasan con sirenas.Timbre ( [http://www.freesound.org/people/Timbre/](http://www.freesound.org/people/Timbre/) ), “Police cars pass by with sirens blaring.wav”,  [http://www.freesound.org/people/Timbre/sounds/71177/](http://www.freesound.org/people/Timbre/sounds/71177/) , [consultado 18 octubre 2011]. Material bajo  [licencia Creative Commons Reconocimiento-NoComercial 3.0 Unported](http://creativecommons.org/licenses/by-nc/3.0/) 

   *  Sirena DopplerJobro ( [http://www.freesound.org/people/jobro/](http://www.freesound.org/people/jobro/) ) “Doppler siren.mp3”,  [http://www.freesound.org/people/jobro/sounds/31920/](http://www.freesound.org/people/jobro/sounds/31920/) , [consultado 18 octubre 2011]. Material bajo  [licencia Creative Commons Reconocimiento 3.0 Unported](http://creativecommons.org/licenses/by/3.0/) 
