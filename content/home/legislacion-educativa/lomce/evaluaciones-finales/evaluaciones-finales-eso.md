---
aliases:
  - /home/legislacion-educativa/evaluaciones-finales/evaluaciones-finales-eso
---


# Evaluaciones Finales ESO

Esta página la creo en 2016 para aclarar las evaluaciones LOMCE que se implantaban por primera vez en 2017.  
En 2018 hay normativa adicional, sin variaciones, ver proyecto de orden y más detalles en  [LOMCE: evaluaciones finales 4º ESO](http://algoquedaquedecir.blogspot.com.es/2017/11/lomce-evaluaciones-finales-4-eso.html)  
Conocidas como "reválidas"  
Ver información en  [Evaluaciones finales](/home/legislacion-educativa/lomce/evaluaciones-finales) , definidas en Real Decreto 310/2016, , se realizarán por primera vez en 2017, sin efectos académicos.  
Para ESO en 2017 habrá una única convocatoria (en 2017 para Bachillerato sí habrá dos convocatorias)  

Ver disposición final de RD 310/2016  [BOE-A-2016-7337 Real Decreto 310/2016, de 29 de julio, por el que se regulan las evaluaciones finales de Educación Secundaria Obligatoria y de Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2016-7337#df) )  
>Disposición final primera. Calendario de implantación.  
>1. La evaluación final de **Educación Secundaria Obligatoria** se implantará en el curso escolar 2016-2017. La evaluación final de Educación Secundaria Obligatoria correspondiente a la convocatoria que se realice **en el año 2017 no tendrá efectos académicos. En ese curso escolar sólo se realizará una única convocatoria.**  

En diciembre 2016 se aprueba Real Decreto-ley 5/2016 que en artículo 2 deroga la disposición final primera del Real Decreto 310/2016, y hace que las evaluaciones de ESO pasen a ser muestrales y solamente de materias de 4º  
>Hasta la entrada en vigor de la normativa resultante del Pacto de Estado social y político por la educación la evaluación regulada en el artículo 29 de la Ley Orgánica 2/2006, de 3 de mayo, será considerada muestral y tendrá finalidad diagnóstica. Se evaluará el grado de adquisición de la competencia matemática, la competencia lingüística y la competencia social y cívica, teniendo como referencia principal las materias generales del bloque de las asignaturas troncales cursadas en cuarto de Educación Secundaria Obligatoria. Esta evaluación carecerá de efectos académicos.

## Cómo serán las evaluaciones finales ESO
Hasta que haya información (ver disposición adicional segunda), no hay referencias para ESO (quizá mirar las  [pruebas libres para graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)  asociadas a LOE, por lo que usan otro currículo y no tienen en cuenta los estándares de aprendizaje), solamente lo que indica la normativa; ver documento anexo a borrador real decreto en  [Evaluaciones finales](/home/legislacion-educativa/lomce/evaluaciones-finales)  y apartado asociado a información común a ambas evaluaciones

## Evaluaciones finales ESO a nivel estatal
En marzo 2017 se publica borrador de orden  
[Proyecto de Orden Ministerial por la que se determinan características de las evaluaciones finales de Educación Primaria y ESO](http://www.educacionyfp.gob.es/servicios-al-ciudadano-mecd/informacion-publica/audiencia-informacion-publica/abiertos/2017/evaluacion-final-educacion-primaria.html) Fecha de publicación: 22/03/2017​  
Fecha límite para la presentación de aportaciones: 11/04/2017  
Tras consultar al portal de transparencia, comparte:  
Acta reunión febrero  [2017-02-21-ActaComisionGeneralEducacion-Transparencia0070_001.pdf - Google Drive](https://drive.google.com/open?id=0B-t5SY0w2S8ia254b2NKMUNZTGc)  
Dictamen de Consejo Escolar Estado, antes de estar publicado en la web y con los votos particulares  
[2017-04-04-Dictamen 12-2017 Pruebas evaluación final Primaria y ESO-SinFirmasTrasNotificacion23ene2020.pdf - Google Drive](https://drive.google.com/open?id=0B-t5SY0w2S8iRjdVdkJYeWFfa00)  
Se publica en BOE 6 mayo, cuando 4 mayo ya empiezan en La Rioja  
[BOE-A-2017-4924 Orden ECD/393/2017, de 4 de mayo, por la que se regulan las pruebas de la evaluación final de Educación Secundaria Obligatoria, para el curso 2016/2017.](http://boe.es/diario_boe/txt.php?id=BOE-A-2017-4924) 

### Evaluaciones finales ESO 2018
En enero 2018 se publica  
[BOE-A-2018-1196 Orden ECD/65/2018, de 29 de enero, por la que se regulan las pruebas de la evaluación final de Educación Secundaria Obligatoria, para el curso 2017/2018.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2018-1196)  
En febrero 2018 se publica  
[BOE-A-2018-1484 Orden ECD/93/2018, de 1 de febrero, por la que se corrigen errores en la Orden ECD/65/2018, de 29 de enero, por la que se regulan las pruebas de la evaluación final de Educación Secundaria Obligatoria, para el curso 2017/2018.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2018-1484) 

## Evaluaciones finales ESO Madrid
Deberían estar definidas en BOE a 30 noviembre 2016 según RD 310/2016, y aunque con Real Decreto-Ley 5/2016 pasen a ser muestrales, se retrasa esa definición.  
El ministerio en enero no da fechas  
[https://twitter.com/FiQuiPedia/status/821379672856530945](https://twitter.com/FiQuiPedia/status/821379672856530945)  
En febrero Madrid fija fechas sin haber orden, fecha 17 mayo anterior a final de curso ¿tiene sentido una evaluación "final" antes del final de curso?  
[La evaluaci&#243;n externa de 4&#186; de ESO se desarrollar&#225; el 17 de mayo](http://www.europapress.es/madrid/noticia-evaluacion-externa-eso-desarrollara-17-mayo-20170215140957.html)  
El 17 febrero se publica  [PresentacionEvaluaciones2017.ppsx](http://www.madrid.org/dat_capital/Documentos/PresentacionEvaluaciones2017.ppsx)  de la que guardo copia aquí  [2017-02-17-DATCapital-PresentacionEvaluaciones2017.ppsx - Google Drive](https://drive.google.com/open?id=0B-t5SY0w2S8iM3kwdVZPUHZEWGs)  
El 17 abril en DAT Norte se pone información para recoger datos aplicadores  
[https://twitter.com/FiQuiPedia/status/853988767442587652](https://twitter.com/FiQuiPedia/status/853988767442587652)  
[Resolución de 8 de mayo de 2017, de las Viceconsejerías de Educación no Universitaria, Juventud y Deporte y de Organización Educativa, por la que se dictan instrucciones para la celebración de las pruebas correspondientes a la evaluación final de los alumnos de cuarto curso de Educación Secundaria Obligatoria, para el curso académico 2016-2017](http://www.bocm.es/boletin/CM_Orden_BOCM/2017/05/10/BOCM-20170510-20.PDF)  
Artículo con detalles pruebas (19 mayo 2017)  
¿Aprobarías la 'reválida' de 4º de la ESO?  
El amor en Tinder, el Brexit o los selfies, a examen Alumnos de 16 años se han examinado este viernes en Madrid de la prueba externa de la LOMCE: no servirá para titular.  
[¿Aprobarías la &#039;reválida&#039; de 4º de la ESO? El amor en Tinder, el Brexit o los selfies, a examen](http://www.elespanol.com/reportajes/20170519/217228438_0.html) 

### Evaluaciones Finales 4º ESO Madrid 2017, 2018
Cuadernillos de enunciados (sin soluciones) de 2017 publicados por Madrid en 2018 en [Evaluación 4º ESO en la Comunidad de Madrid. Última realizada Abril 2019](http://www.comunidad.madrid/servicios/educacion/evaluacion-4o-eso)  
Información de detalle (con soluciones) conseguida en 2017 y compartida aquí  [https://drive.google.com/open?id=0B-t5SY0w2S8iVVR3UXhfNTRTaXc](https://drive.google.com/open?id=0B-t5SY0w2S8iVVR3UXhfNTRTaXc) 

### Evaluaciones Finales 4º ESO 2017 MECD (Ceuta, Melilla y centros en el exterior), Aragón, Castilla y León, Castilla-La Mancha, Comunidad Foral de Navarra, Extremadura, Illes Balears y La Rioja
Son comunes, se pueden ver detalles en más detalles en  [LOMCE: evaluaciones finales 4º ESO](http://algoquedaquedecir.blogspot.com.es/2017/11/lomce-evaluaciones-finales-4-eso.html)  
Publicadas en único documento en enero 2018  
[https://twitter.com/educaINEE/status/953234160050692096](https://twitter.com/educaINEE/status/953234160050692096)  
Ya disponibles todas las pruebas de evaluación liberadas de 4º ESO 2017 con su respectiva guía para el profesorado en un único volumen en la página web del inee:  [Evaluación de Educación Secundaria Obligatoria (4º de ESO) - INEE | Ministerio de Educación y Formación Profesional](http://ow.ly/MYs730hFgup)  y en la del MECD:  [Ítems liberados del curso 2016-2017 - INEE | Ministerio de Educación y Formación Profesional](http://ow.ly/Mx1n30hFguR)  #Evaluación4ESO

## Evaluaciones finales ESO otras comunidades
Cataluña  
[¿Aprobarías el examen de matemáticas de las pruebas de ESO del 2017?](http://www.elperiodico.com/es/noticias/educacion/examen-matematicas-competencias-basicas-eso-2017-5837886)  
[¿Aprobarías el examen de Castellano de las pruebas de ESO del 2017?](http://www.elperiodico.com/es/noticias/educacion/examen-castellano-competencias-basicas-eso-2017-5837742)  

