# Factores de conversión
Se trata de un método de cambio de unidades  

[Factor de conversión - wikipedia.org](https://es.wikipedia.org/wiki/Factor_de_conversi%C3%B3n)  
> El factor de conversión o factor unidad es un método de conversión que se basa en multiplicar por una o varias fracciones en las que el numerador y el denominador son cantidades iguales expresadas en unidades de medida distintas, de tal manera, que cada fracción equivale a la unidad. Es un método muy efectivo para cambio de unidades y resolución de ejercicios sencillos dejando de utilizar la regla de tres. 
 
Los alumnos en secundaria suelen usar proporciones / "la regla de tres"; usar factores es un método alternativo, más compacto y más sistemático.  
  
Se pueden utilizar para cambiar unidades o también en cálculos estequiométricos.  
  
Los factores de conversión aparecían explícitamente en [currículo de física y química 1º Bachillerato LOE](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculofisicayquimica-1bachillerato), pero no en otros cursos LOE ni en ninguno LOMCE.  
En 2022 se añaden en [currículo 3º ESO Física y Química LOMLOE](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomloe)  
Algún profesor me ha dicho oralmente que se "exigían" en la PAU, pero no lo he visto documentado ni lo he podido validar con un corrector de PAU.  
  
El concepto de factor de conversión aparece en [Quantities, Units and Symbols in Physical Chemistry, Third Edition - IUPAC](https://iupac.org/wp-content/uploads/2019/05/IUPAC-GB3-2012-2ndPrinting-PDFsearchable.pdf) capítulo 7 Conversion of units, donde se cita como "quantity calculus"
  
 [FisQuiWeb. Factores de conversión](https://fisquiweb.es/FactoresConversion/Factores.htm)   
Algunos recursos/ejercicios:  
 [Factores de conversión - proyectodescartes.org](http://proyectodescartes.org/uudd/materiales_didacticos/Factores_conversion-JS/index.html)   
 [Factores de conversión - matematicasfisicaquimica.com](https://matematicasfisicaquimica.com/factor-de-conversion/)   
 [Factores de conversión - fq-3eso.blogspot.com.es](http://fq-3eso.blogspot.com.es/2015/06/factores-de-conversion.html)   
 [Ficha cambio de unidades - gobiernodecanarias.org](http://www3.gobiernodecanarias.org/medusa/ecoblog/adiamar/files/2012/11/fichascambiounidades.pdf)   
 Ejercicios con solución (no con resolución)  
 [Unidad 1: Magnitudes y Unidades. - fisicayquimica.iesruizgijon.es](http://www.fisicayquimica.iesruizgijon.es/fisyqui3eso/fisyqui3eso.html)  
[Tabla para los Factores de Conversión](http://www.fisicayquimica.iesruizgijon.es/fisyqui3eso/TablaFactoresConversion.pdf)  
[Cambios de unidades - Hoja nº 1](http://www.fisicayquimica.iesruizgijon.es/fisyqui3eso/EJERCICIOS%20FACTORES%20CONVERSION-HOJA%201.pdf)  
[Cambios de unidades - Hoja nº 2](http://www.fisicayquimica.iesruizgijon.es/fisyqui3eso/EJERCICIOS%20FACTORES%20CONVERSION-HOJA%202.pdf)  
[Cambios de unidades - Hoja nº 3](http://www.fisicayquimica.iesruizgijon.es/fisyqui3eso/EJERCICIOS%20FACTORES%20CONVERSION-HOJA%203.pdf)  
[Cambios de unidades - Hoja nº 4](http://www.fisicayquimica.iesruizgijon.es/fisyqui3eso/EJERCICIOS%20FACTORES%20CONVERSION-HOJA%204.pdf)  
 Ejercicios con resolución  
 [EJERCICIOS DE REPASO. CAMBIO DE UNIDADES - edu.xunta.gal](http://www.edu.xunta.gal/centros/iesmos/system/files/resuleto+de+cambio+unidades.pdf)   
Resolución en vídeo  
 [Cambio de unidades por factores de conversión - profesor10mates](http://profesor10demates.blogspot.com.es/2013/08/cambio-de-unidades-por-factores-de.html)   
    
 [109. Factores de conversión con Tesela2  - elcel.org](https://www.elcel.org/es/experiencias/109-factores-de-conversion-con-tesela2/)   

[Fichas cambio unidades jcabello.es (waybackmachine)](http://web.archive.org/web/20180619180706/http://www.jcabello.es/documentos/docfisyquim3/fichascambiounidades.pdf)  
3 fichas, cada una 4 páginas. Al final indica "Son 20 cambios a 0,5 puntos cada uno sería el 10, calcula que nota sacas y la compruebas cuando te corrija yo." 

[twitter bjmahillo/status/1224581547262664706](https://twitter.com/bjmahillo/status/1224581547262664706)   
Voy a usar este cartel para explicar los factores de conversión de aquí en adelante.  
[twitter RyTriGuy/status/1224462993326596097](https://twitter.com/RyTriGuy/status/1224462993326596097)  
Dimensional analysis review today with CP Chem ahead of our stoich unit, and a student said “OH! Conversion factors are like bananas in the elevator!!”   
After some laughs and confused looks, he showed us this picture. It’s a BRILLIANT example! @DohertySpartans  
![](https://pbs.twimg.com/media/EP4ql71X0AAvGWt?format=jpg)  

[twitter EugenioManuel/status/1259835087614607360](https://twitter.com/EugenioManuel/status/1259835087614607360)  
Damos mucha importancia al correcto uso de unidades y la expresión científica. También ofrecemos muchas actividades resueltas.  
![](https://pbs.twimg.com/media/EXvUoHgXgAATX0i?format=png)   

[Conversión de Densidad - online.unitconverterpro.com](https://online.unitconverterpro.com/es/conversion-de-unidad/conversor-alpha/density.php)  

[twitter onio72/status/1469194225300516865](https://twitter.com/onio72/status/1469194225300516865)  
He propuesto a mis alumnos de FQ2ESO fabricar con una botella de plástico un vaso medidor casero para poder tomar porciones de 250 gramos de diferentes alimentos. Creo que esta sencilla tarea puede ayudar a consolidar los conceptos de volumen, masa y densidad. ¿Qué os parece? ⁦   
![](https://pbs.twimg.com/media/FGOgW3mX0AMeTII?format=jpg)  

Se citan factores en [Libros Química 2ed 1.6 Tratamiento matemático de los resultados de las mediciones - openstax.org](https://openstax.org/books/qu%C3%ADmica-2ed/pages/1-6-tratamiento-matematico-de-los-resultados-de-las-mediciones)  

