
# Recursos física de partículas

Puede haber recursos relacionados en  [recursos física nuclear](/home/recursos/fisica/recursos-fisica-nuclear)  y  [recursos física cuántica](/home/recursos/fisica/recursos-fisica-cuantica)   

Las partículas subatómicas se incluyen como subpáginas, aunque algunas sencillas como electrones y protones no se suelen referenciar asociadas a física de partículas. Puede haber información relacionada en [recursos estructura atómica](/home/recursos/quimica/recursos-estructura-del-atomo/)  

[Twitter ThePhysicsMemes/status/1824156963665743906](https://x.com/ThePhysicsMemes/status/1824156963665743906)  
![](https://pbs.twimg.com/media/GVC1Fw7aEAICW5-?format=png)  

## Recursos física de partículas de elaboración propia
Ver en  [Ejercicios de elaboración propia de física de 2º Bachillerato](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato) , en materiales seminario física de partículas 2013-2014 dentro de  [recursos de elaboración propia](/home/recursos/recursos-de-elaboracion-propia) , y en  [recursos sobre Modelo Estándar](/home/recursos/fisica/recursos-modelo-estandar)  
En 2017 creo unos diagramas resumen pensando en los apuntes de física de 2º BachilleratoResumen tipos partículas elementales, tipos partículas compuestas, tipos de interacciones y unificaciones  
[ResumenParticulasInteracciones-propio.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/pdf/ResumenParticulasInteracciones-propio.pdf)  
Resumen interacciones entre partículas elementales  [Elementary-particle-interactions-es propio.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/pdf/Elementary-particle-interactions-es%20propio.pdf) 

## Ortografía física de partículas
Muon y pion son sin tilde  
 [Palabras como «guion», «truhan», «fie», «liais», etc., se escriben sin tilde | Español al día | Real Academia Española](https://www.rae.es/consultas/palabras-como-guion-truhan-fie-liais-etc-se-escriben-sin-tilde)   
Quark se puede escribir cuark  
Descubierto en este hilo  
[twitter FiQuiPedia/status/758566448050692096](https://twitter.com/FiQuiPedia/status/758566448050692096)   
[Tratamiento de los extranjerismos | Diccionario panhispánico de dudas](https://www.rae.es/dpd/ayuda/tratamiento-de-los-extranjerismos)   

## Física de partículas a nivel no universitario (en el instituto)
 
[Viaje al corazón de la materia. Física de partículas en el instituto por Francisco Barradas Solas ](http://palmera.pntic.mec.es/~fbarrada/)   
Incluye actividades en el aula, algunas utilizan fotos de cámaras de burbujas  
 [5. Análisis de colisiones de partículas mediante las leyes de conservación del momento lineal y la carga eléctrica](http://palmera.pntic.mec.es/~fbarrada/aula/aula51.html)   
  
Partículas para profesores de secundaria (Francisco Barradas Solas)  
 [partculas-para-profs](http://www.slideshare.net/fbsolas/partculas-para-profs)   
  
Masterclass Física de partículas en el instituto: ¡Investiga con el LHC!  
 [participa.php](http://www.i-cpan.es/bachillerato/participa.php)   
  
 [ apres-cern | EducaMadrid ](http://www.educa2.madrid.org/web/apres-cern/inicio)   
  
 [Contemporary Physics Education Project - CPEP](http://www.cpepweb.org/)   
 [Components of the Chart](http://www.particleadventure.org/spanish/cpep_componentss.html)   
Copyright en 1988-1999 Contemporary Physics Education Project. CPEP es una organización sin fines de lucro, de los profesores, educadores y físicos.   
  
 [CERNLand brings the excitement of CERN's research to your kids!](http://www.cernland.net/)  para niños (en 2017 en flash), en  [2866-cernland-el-cern-al-alcance-de-los-ninos](http://enmarchaconlastic.educarex.es/inicio/251-nuevo-emt/juegos-y-gamificacion/2866-cernland-el-cern-al-alcance-de-los-ninos)  se cita para 2°eso...  

[Esas maravillosas partículas - eltamiz.com](https://eltamiz.com/esas-maravillosas-particulas/)  

### Apps - juegos divulgativos
  
Particle identities [Particle Quiz](https://scoollab.web.cern.ch/sites/scoollab.web.cern.ch/files/ParticleGame/)   
Quantum 3: Learning QCD through Intuitive Play  
[[1901.00022] Quantum 3: Learning QCD through Intuitive Play](https://arxiv.org/abs/1901.00022)  
Una app para jugar a construir partículas con quarks usando colores y sabores  
[Quantum 3 - Apps on Google Play](https://play.google.com/store/apps/details?id=com.gellab.quantum3)   

## Física de Partículas para profesores de Bachillerato
 [http://wwwae.ciemat.es/~pablog/curso/curso.pdf](http://wwwae.ciemat.es/%7Epablog/curso/curso.pdf)   
Pablo García Abia, CIEMAT, Madrid  
  
A reappraisal of the mechanism of pion exchange and its implications for the teaching of particle physics, 2002  
 [Home | CERN Teacher Programmes](http://teachers.web.cern.ch/teachers/archiv/HST2002/feynman/Pion%20exchange.pdf)   
Se cuestiona si usar el modelo de "pelota de baloncesto" con piones de Yukawa para el nivel preuniversitario.  
  
Particle Physics in High School: A Diagnose Study   
 Paula Tuzón, Jordi Solbes   
Published: June 2, 2016  
 [Particle Physics in High School: A Diagnose Study](http://dx.doi.org/10.1371/journal.pone.0156526) 

## Física de Partículas "divulgativa"  

Boston University. CC104 From atoms to quarks.  
 [Prof Sulak's Lecture Notes](http://physics.bu.edu/cc104/)   
Lecture. Prof Sulak  

[Particle physics resources - UK Research and Innovation](https://www.ukri.org/what-we-do/public-engagement/public-engagement-stfc/engage-with-our-areas-of-science-and-technology-stfc/particle-physics/particle-physics-resources/)  

  
Resumen "popular" Nobel 2004 relacionado con fuerza nuclear fuerte  
 [The 2004 Nobel Prize in Physics - Popular information - NobelPrize.org](http://www.nobelprize.org/nobel_prizes/physics/laureates/2004/popular.html)   
  
The Higgs boson, explained simply (the real story)  
 [<span><a href="https://coherence.wordpress.com/" rel="home">Quantum Coherence</a></span>](http://coherence.wordpress.com/2012/07/08/the-higgs-boson-simply-explained/)   
  
 [material-didactico](http://projects.ift.uam-csic.es/outreach/index.php/es/material-didactico)   
Transparencias de charlas y cursos   
  
 [Colliding Helicopters:  What do we do at CERN? | International Particle Physics Outreach Group](http://ippog.web.cern.ch/resources/2011/colliding-helicopters-what-do-we-do-cern)   
This presentation describes CERN, particle physics and the LHC to younger audiences (featuring helicopters and dinosaurs!)  
 As it is aimed at 14-18 year olds, the presentation is rather basic.  
 Indeed it can also be easily adapted for younger audiences (e.g. 10-11) by omitting slides 14, 17, 18, 19, 26, 32 and 35.  
 Duration: 45-60 minutes, depending on the audience.  
 [Qué es un electrón en teoría cuántica de campos - La Ciencia de la Mula Francis](http://francis.naukas.com/2013/01/09/que-es-un-electron-en-teoria-cuantica-de-campos/)   
  
También hay cosas de humor  
 [xkcd: Unification](https://xkcd.com/1956/)   
 [![](https://imgs.xkcd.com/comics/unification.png "") ](https://imgs.xkcd.com/comics/unification.png)   

## Pósters / Carteles
En el enlace  [Contemporary Physics Education Project - CPEP](http://www.cpepweb.org/)  hay carteles que se pueden comprar, o descargar gratuitamente con resolución suficiente para imprimir en A3.En el CERN la mayoría de los carteles se pueden descargar  [Posters - CERN Document Server](https://cds.cern.ch/collection/Posters)  [material-didactico](http://projects.ift.uam-csic.es/outreach/index.php/es/material-didactico) 

##  [Recursos Modelo Estándar](/home/recursos/fisica/recursos-modelo-estandar)   

## Radiactividad
Se coloca junto a  [recursos de física nuclear](/home/recursos/fisica/recursos-fisica-nuclear) , aunque se relaciona a veces con cámara de niebla.  

## Cámara de niebla
Aparte de enlaces en recursos generales previos (por ejemplo física de partículas en el instituto), se incluyen otros propios  

[Una cámara de niebla casera - Francisco Barradas-Solas, julio de 2012](http://palmera.pntic.mec.es/~fbarrada/niebla_casera.html)   
Incluye enlaces  
[La cámara de niebla: Partículas de verdad. Premio en la modalidad de Experimentos del I Concurso de Divulgación Científica del CPAN - cpan.es](https://www.i-cpan.es/concurso/ganadores/55CamaraNiebla.pdf)  
[La camara de niebla. Manual de uso y construcción y cómic divulgativo. Francisco Barradas Solas y Paloma Alameda Meléndez](http://palmera.pntic.mec.es/~fbarrada/cloud_chamber_spanish.pdf)  

[Taller de cámaras de niebla. CONSTRUYA EN CASA SU PROPIA CÁMARA DE NIEBLA. CERN](https://indico.cern.ch/event/195014/contributions/361293/attachments/283543/396492/cloud_chamber_spanish.pdf)  
  
APRENDE: Experimento Cámara de Niebla  
 [![CYT-2. APRENDE: Experimento Cámara de Niebla - YouTube](https://img.youtube.com/vi/2LPz2EEfRfU/0.jpg)](https://www.youtube.com/watch?v=2LPz2EEfRfU)   
APRENDE: La cámara de niebla permite observar el movimiento de las partículas ionizantes. Este vídeo registra el funcionamiento de la cámara. Fecha: 2009. Duración: 1:48 minutos. Producción: Parque de las Ciencias.  
  
Física Moderna. Trazas nucleares. Radiactividad   
 [![Física Moderna. Trazas nucleares. Radiactividad - YouTube](https://img.youtube.com/vi/5yiPcrhRTQE/0.jpg)](https://www.youtube.com/watch?v=5yiPcrhRTQE)   
Este video es casi un "How To" sobre la construcción de una càmara de niebla. Duración: 6:22 minutos.   
  
 [how-to-build-your-own-particle-detector](https://www.symmetrymagazine.org/article/january-2015/how-to-build-your-own-particle-detector) 

## Laboratorios
Aparte de  [Physics Masterclasses](/home/recursos/fisica/fisica-de-particulas/physics-masterclasses)  [Lab 10: The Mass of the Top Quark](http://electron6.phys.utk.edu/phys250/Laboratories/mass_of_the_top_quark.htm)   

## Recursos para profesores  

Ver también  [Physics Masterclasses](/home/recursos/fisica/fisica-de-particulas/physics-masterclasses)   
  
 [Resources_for_Teachers](http://www.uslhc.us/higgs/Resources_for_Teachers)   
  
 [e-Labs](http://www.i2u2.org/)   
Interactions in Understanding the Universe (I2U2), an "educational virtual organization,"   
Incluye CMS e-Lab, Cosmic Ray e-Lab, LIGO e-Lab  
  
APPLICATIONS OF PARTICLE ACCELERATORs  
 [P00021907.pdf](http://cds.cern.ch/record/260280/files/P00021907.pdf)   
1994,O. Barbalat  
  
 [Para ver, leer y hacer (recursos) - Física de partículas en el instituto](https://sites.google.com/site/particulasenelinsti/cosas-utiles-recursos)   
Página de Paco Barradas  
  
Posters  
 [Posters - CERN Document Server](http://cds.cern.ch/collection/Posters?ln=en)   

## Charlas
 [charlas_participa.php](http://www.i-cpan.es/bachillerato/charlas_participa.php)   

## CERN: LHC, CMS, ATLAS, ...  

 [CMS_Slice_elab.swf](http://www.i2u2.org/elab/cms/graphics/CMS_Slice_elab.swf)   
Animación que muestra diferentes capas CMS y detección distintos tipos de partículas.  
  
Animación equivalente con ATLAS (flash)  [wpath_teilchenid1.htm](http://atlas.physicsmasterclasses.org/en/wpath_teilchenid1.htm)   
  
Estructura ATLAS   
 [atlas.htm](http://atlas.physicsmasterclasses.org/en/atlas.htm)   
  
 [LHC Guide - CERN Document Server](http://cds.cern.ch/record/1165534)   
CERN FAQ. LHC: the guide (English version)  
 © CERN Geneva: The use of posters requires prior authorization (from CERN copyright).  
The words CERN-Brochure-2009-003-Eng must be quoted for each use.   
  
 [0.pdf](http://indico.cern.ch/event/153896/material/1/0.pdf)   
Traducción Guía LHC al español de José Luis Suárez de la Fuente  
  
Processing LHC data (vídeo)  
 [Processing LHC data · CDS Videos · CERN](https://cds.cern.ch/record/1541893?ln=en)   
 
11 junio 2021
[El CERN ha hecho otro hallazgo asombroso: una partícula que oscila entre materia y antimateria puede ayudarnos a entender el origen del universo](https://www.xataka.com/investigacion/cern-ha-hecho-otro-hallazgo-asombroso-particula-que-oscila-materia-antimateria-puede-ayudarnos-a-entender-origen-universo)   

##  [Diagramas de Feynman](/home/recursos/fisica/fisica-de-particulas/diagramas-de-feynman) 

Se ponen enlaces a algunas partículas: las ordinarias como protón, electrón y neutrón pueden usarse fuera del ámbito habitual de Física de Partículas

## Partículas subatómicas

[twitter FIKASI1/status/1553433805427458048](https://twitter.com/FIKASI1/status/1553433805427458048)  
Me encanta las propuestas de @DrJeffWiener y su equipo para introducir la física de partículas en etapas tempranas y evitar ideas equivocadas. Os resumo algunas ideas.   
1) Hablar de sistemas de partículas para el caso de neutrones o protones y reservar el nombre de partícula únicamente para las elementales (electrones, quarks, etc.).  
2) Utilizar símbolos abstractos, como las tipografías de la imagen, para la representación gráfica de las partículas y los sistemas de partículas. De esta forma se evita utilizar esferas u otro tipo de figuras que puedan conducir a error.  
3) Hablar de "espacio del núcleo" en lugar de "núcleo" para incidir en la idea de zona espacial/localización. De esta forma es más sencillo introducir los protones/neutrones que se encuentran en la zona del núcleo.  
4) De la misma forma, podemos hablar de "espacio del orbital" en vez de "orbital" que facilita la idea de zona del espacio en el que es más probable encontrar electrones.  
5) Estas dos últimas sugerencias favorecen, además, la idea de vacío en los átomos.  
6) Bibliografía de la Unidad didáctica:   
[The subatomic structure of matter. Annotated learning unit - cern.ch](https://per.web.cern.ch/sites/default/files/units/JW_ElementaryParticles_LearningUnit.pdf)  

[Estructura atómica. Unidad didáctica CERN - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2eso-3eso/estructura-atomica/unidad-didactica-CERN.pdf)  

## [Electrón](/home/recursos/fisica/fisica-de-particulas/electron)

## Neutrón

[![](https://www.energy.gov/sites/default/files/styles/full_article_width/public/082312.jpeg?itok=GP-psvRv)](https://www.energy.gov/articles/photo-week-how-open-worlds-heaviest-door "Photo of the Week: How to Open the World's Heaviest Door") 
A special bearing in the hinge allowed a single person to open or close the concrete-filled door, which was used to shield the Rotating Target Neutron Source-II (RTNS-II) -- the world’s most intense source of continuous fusion neutrons.  

## Protón
[twitter martinmbauer/status/1529705943386275842](https://twitter.com/martinmbauer/status/1529705943386275842)  
Protons are not fundamental particles, but dynamical systems made of quarks and gluons. What a proton looks like depends on 2 quantities: the momentum at which you probe (Q^2) and the fraction of the proton momentum carried by each quark or gluon inside the proton (x)  1/6  
It is not true that each proton consists of exactly 3 quarks. A better way to phrase it would be to say that it has *at least* 3 quarks. Gluons split into quark-antiquark pairs and what you see for low x is that the proton looks more like a cloud of quarks and gluons 2/6  
At large x, the total proton momentum is carried by as few particles as possible and since there are *at least* 3, it looks more like the usual illustration  3/6  
Protons are not always spherical either. If you change the momentum, the proton changes shape and starts looking like a pancake at high Q^2 because of Lorentzian length contraction (units are missing but should be GeV^2) 4/6  
"Looking at the proton" at these energies really means colliding it with another particle (e.g. an electron). The  probability distributions of a probe particle to interact with a component inside the proton is given by the parton distribution functions 5/6  
These amazing visualisations have just been publishewd by @JLab_News and it is worth looking at all of it here  6/6  
[Now Presenting: A Visualization of the Proton - jlab.org](https://www.jlab.org/news/stories/now-presenting-visualization-proton)  
![](https://www.jlab.org/sites/default/files/styles/image_436xauto/public/220524_VisualizingProton_0.jpg?itok=nxYne_wG)  

[twitter atdotde/status/1529727492466130944](https://twitter.com/atdotde/status/1529727492466130944)  
Nice to look at. But I am not sure that making this look like the result of some simulation while it‘s actually an artist‘s animation (an informed artist though) does a good service. In the end it shows that thinking of a strongly coupled QFT in terms of particles is hard.  

[![](https://img.youtube.com/vi/G-9I0buDi4s/0.jpg)](https://www.youtube.com/watch?v=G-9I0buDi4s "Visualizing the Proton. Arts at MIT")  

## Quarks

Experimento SLAC de descubrimiento quarks  

[Discovery of the top quark - slac.stanford.edu](https://www.slac.stanford.edu/pubs/beamline/25/3/25-3-carithers.pdf)  
[The Discovery of Quarks SLAC PUB 5724 April 1992](https://www.slac.stanford.edu/pubs/slacpubs/5500/slac-pub-5724.pdf)  
Diagramas en páginas 29 y 30  
[The Discovery of the Point-Like Structure of Matter SLAC-PUB-8640 September 2000](https://www.slac.stanford.edu/pubs/slacpubs/8500/slac-pub-8640.pdf)  
En página 7 "Figure 5 Spectrometer facility at the Stanford Linear Accelerator. Each of the Spectrometers can be rotated about the target position to vary the angle of scatterin"  
[THE 1990 NOBEL PRIZE Finding The Quark](https://www.europhysicsnews.org/articles/epn/pdf/1990/11/epn19902111p208.pdf)  
En página 1 Fig. 1 — The two magnetic spectrometers used for the SLAC-MIT experiment. The 8 GeV
spectrometer is in the foreground and the 20 GeV unit is to the rear. The bulk of the detec
tors comprise shielding (weighing 450 tons for the 8 GeV device)  
[The Nobel Prize in Physics 1990 - nobelprize.org](https://www.nobelprize.org/prizes/physics/1990/illustrated-information/)  
![](https://www.nobelprize.org/uploads/2013/06/matter.jpeg)  
![](https://www.nobelprize.org/uploads/2018/06/slac.jpeg)  

## [Bosón de Higgs](/home/recursos/fisica/fisica-de-particulas/boson-de-higgs)

## [Muon](/home/recursos/fisica/fisica-de-particulas/muon)

## Bosón W
[Key Particle Weighs in a Bit Heavy, Confounding Physicists](https://www.snopes.com/ap/2022/04/08/key-particle-weighs-in-a-bit-heavy-confounding-physicists/)  
[El detector CDF II del Tevatron mide una masa del bosón W que está a 7 sigmas del modelo estándar - francis.naukas.com](https://francis.naukas.com/2022/04/08/el-detector-cdf-ii-del-tevatron-mide-una-masa-del-boson-w-que-esta-7-sigmas-del-modelo-estandar/)  

## Neutrinos
Recursos en este hilo de Twitter  [https://twitter.com/fisicagrel/status/1109824712086048769](https://twitter.com/fisicagrel/status/1109824712086048769)   
¿Qué pasa con los neutrinos? Instituto de Física Teórica IFT  
Alberto Casas nos proporciona algunos FUN FACTS sobre los neutrinos, las partículas más fantasmales y misteriosas que conocemos  
 [![¿Qué pasa con los neutrinos? - YouTube](https://img.youtube.com/vi/tMH1lO0QhFA/0.jpg)](https://www.youtube.com/watch?v=tMH1lO0QhFA)  (2:30 min)  
Neutrinos bajo tierra, CERNtripetas  
Descubrimos el experimento de neutrinos del CERN de la mano de Miguel Angel Garcia que trabaja en protoDUNE. 
[![Neutrinos bajo tierra](https://img.youtube.com/vi/HAEEwhWsIaA/0.jpg)](https://www.youtube.com/watch?v=HAEEwhWsIaA "Neutrinos bajo tierra")   

[cuentos-cuanticos.com Nno history, what’s NEeutriXT? — ¿Neutrinos?](https://cuentos-cuanticos.com/2014/11/13/neutrino-history-whats-next-neutrinos/)   
Los neutrinos se miran al espejo  
 [neutrinos_cpan.pdf](https://www.i-cpan.es/media/concurso2/59c823b95c14e/neutrinos_cpan.pdf)   
El nacimiento del neutrino  
  
 [El nacimiento del neutrino, IFIC UV](https://bloggy.ific.uv.es/bloggy/index.php/2019/03/22/el-nacimiento-del-neutrino/)   

[![Even Bananas 06: How big is a neutrino?](https://img.youtube.com/vi/EULHYBTaWtU/0.jpg)](https://www.youtube.com/watch?v=EULHYBTaWtU "Even Bananas 06: How big is a neutrino?")   

[IceCube observa neutrinos de alta energía asociados al plano galáctico de la Vía Láctea - francis.naukas](https://francis.naukas.com/2023/07/03/icecube-observa-neutrinos-de-alta-energia-asociados-al-plano-galactico-de-la-via-lactea/)
![](https://francis.naukas.com/files/2023/07/D20230703-science-magazine-10-1126-science-adc9818-plane-Milky-Way-Galaxy-photons-and-neutrinos-580x283.png)  

[A New Map of the Universe, Painted With Cosmic Neutrinos - .quantamagazine.org](https://www.quantamagazine.org/a-new-map-of-the-universe-painted-with-cosmic-neutrinos-20230629/)  
![](https://d2r55xnwy6nx47.cloudfront.net/uploads/2023/01/Seeing-With-NeutrinosbyMerrillSherman2_920-Desktop.svg)  

##  [Physics Masterclasses](/home/recursos/fisica/fisica-de-particulas/physics-masterclasses) 

## Recursos simulaciones
Applet: Quarks  
 [q.htm](http://lectureonline.cl.msu.edu/~mmp/applist/q/q.htm)   
Put the right combination of quarks and antiquarks together and see the resulting hadron.  
© G.D. Westfall, 1999

### Apps para móviles 
LHSee: información sobre LHC y visualizador de eventos (ATLAS)  
 [LHSee](https://play.google.com/store/apps/details?id=com.lhsee&hl=es)   
  
Physics: The Standard Model, Appocalypse [Physics: The Standard Model - Apps on Google Play](https://play.google.com/store/apps/details?id=standardmodel.namespace)   
  
 [App | WIPAC - Wisconsin IceCube Particle Astrophysics Center](https://wipac.wisc.edu/deco/app) DECO works by recording a camera image, called a sample, once every 1-2 seconds. The app analyzes the image to determine bright pixels.  
  
If enough bright pixels are found, the sample is considered a candidate for a high-energy particle interaction. A second analysis performs more thorough follow-up processing to determine if the candidate should be considered an event. Many events are due to cosmic-ray muons, but DECO can also detect electrons, gamma rays, and alpha particles (helium nuclei) produced by the decay of trace amounts of radioactive elements that occur naturally in the environment and in the materials of the phone.  
  
Only a small fraction of the samples turn out to be candidates, and a fraction of these are classified as events. For a typical device, you need to run the app for about 24 hours to get a few events.  

## Teoría de cuerdas  

La teoría de cuerda en 7 minutos, Instituto de Física Teórica  [Instituto de Física Teórica IFT - YouTube](https://www.youtube.com/user/IFTMadrid) , vídeo realizado por  [QuantumFracture - YouTube](https://www.youtube.com/user/QuantumFracture)   
 [![watch](https://img.youtube.com/vi/yd1jx1DkXb4/0.jpg)](https://www.youtube.com/watch?v=yd1jx1DkXb4)   
 [teoria-de-cuerdas-viva-y-coleando.html](https://lafisicadelgrel.blogspot.com.es/2017/01/teoria-de-cuerdas-viva-y-coleando.html)  [El ocaso de la teoría de cuerdas — Cuaderno de Cultura Científica](https://culturacientifica.com/2017/01/06/ocaso-la-teoria-cuerdas/)   

## Enlaces
  
Elementary Particles and the World of Planck Scale  
 [Elementary Particles and the World of Planck Scale](http://universe-review.ca/F15-particle.htm)   
  
Recopilatorio de enlaces dentro de la página de "Hands on Particle Physics Masterclasses" (ver en  [actividades fuera del centro para alumnos](/home/recursos/recursos-de-actividades-fuera-del-centro) )   
  
 [International Masterclasses - hands on particle physics](http://www.physicsmasterclasses.org/index.php?cat=country)   
  
 [Resources - site under development, static content since 1/8/2020 | International Particle Physics Outreach Group](http://ippog.web.cern.ch/resources)   
Recursos por categorías  

### General Particle Physics Links  


   *  [The Particle Adventure](http://particleadventure.org/)  (13 languages)
   *  [Antimatter webcasts: mirror of the universe](http://livefromcern.web.cern.ch/livefromcern/antimatter/) 
   *  [www.interactions.org - particle physics news and resources](http://www.interactions.org) 
   *  [CERN educational resources](http://education.web.cern.ch/education/) 
   *  [DESY research](http://www.desy.de/research/index_eng.html) 
   *  [Science at SLAC](http://www6.slac.stanford.edu/ExploringSLACScience.aspx) 
   *  [Fermilab education office](http://www-ed.fnal.gov/) 
   *  [Contemporary physics education project - Posters, Charts and more](http://www.cpepweb.org/) 
   *  [Particle Physics Education Sites Worldwide](http://particleadventure.org/other/othersites.html) 
   *  [CERN Courier - Latest news on Particle Physics](http://www.cerncourier.com/) 
   *  [Symmetry - A magazine from SLAC and Fermilab](http://www.symmetrymagazine.org/cms/) 
   *  [Labs and experiments in particle physics](http://www.fnal.gov/pub/ferminews/ahep.html) 
   *  [International Particle Physics Outreach Group](http://cern.ch/eppog/) 
   *  [String Theory](http://superstringtheory.com/) 
   *  [Quantum Universe](http://interactions.org/quantumuniverse/) 
   *  [Taking a closer look at LHC](http://www.lhc-closer.es/php/index.php?i=1&s=6&p=1&e=0) 
Enlaces en Fermilab  
 [Inquiring Minds](http://www.fnal.gov/pub/inquiring/othersites/index.html)   
  
**Other High Energy Physics Sites**  
**Accelerator laboratories:**  
[Brookhaven National Laboratory](http://www.bnl.gov/)  
[Stanford Linear Accelerator Center](http://www.slac.stanford.edu/)  
[LNS at Cornell University](http://www.lns.cornell.edu/)  
[CERN](http://public.web.cern.ch/Public/)  (Geneva, Switzerland)  
[DESY](http://www.desy.de/html/home/fastnavigator.html)  (Hamburg, Germany)  
[KEK](http://www.kek.jp/intra.html)  (Tsukuba, Japan)  
[JINR](http://www.jinr.dubna.su/)  (Dubna, Russia)  
[Other accelerator laboratories around the world](http://www-elsa.physik.uni-bonn.de/accelerator_list.html)   
  
**Research institutions:**  
[High energy physics institutes around the world (including universities)](http://www.hep.net/sites/directories.html)   

## Fronteras de la física
Aparece en el temario LOMCE, no solamente física de partículas, también  [cosmología](/home/recursos/fisica/recursos-cosmologia) . Se ponen aquí algunos enlaces  
 [List of unsolved problems in physics - Wikipedia](https://en.wikipedia.org/wiki/List_of_unsolved_problems_in_physics)   
Beyond the Standard Model [The Particle Adventure | Unsolved Mysteries | Beyond the Standard Model](http://www.particleadventure.org/beyond_start.html)   
  
Ciclo de Conferencias "La Frontera de la Física Fundamental"  
 [Ciclo de Conferencias "La Frontera de la Física Fundamental" - IftWorkShops | Workshops in the Institute of Theoretical Physics - UAM/CSIC](https://workshops.ift.uam-csic.es/151/Programa)   
 [5 Puzzles About The Universe That Keep Scientists Up At Night](https://www.forbes.com/sites/startswithabang/2021/04/19/5-puzzles-about-the-universe-that-keep-scientists-up-at-night/amp/)   
  
TOTEM y DZero publican el descubrimiento del odderon [TOTEM y DZero publican el descubrimiento del odderon - La Ciencia de la Mula Francis](https://francis.naukas.com/2021/03/10/totem-and-dzero-publican-el-descubrimiento-del-odderon/)   

### Violación de la universalidad leptónica
  
 [twitter VM_Lozano/status/1374323339272056832](https://twitter.com/VM_Lozano/status/1374323339272056832) Hoy el experimento LHCb ha anunciado evidencia de violación de la universalidad en el sector de los leptones, algo que no es lo que predice el Modelo Estándar de partículas. Pero, ¿qué significa realmente esto? ¿Es importante?

### Quinta fuerza
 [La quinta fuerza - La Ciencia de la Mula Francis](http://francis.naukas.com/2015/04/02/la-quinta-fuerza/)   
 [(https://cuentos-cuanticos.com/2016/05/27/a-las-puertas-de-una-quinta-interaccion/](https://cuentos-cuanticos.com/2016/05/27/a-las-puertas-de-una-quinta-interaccion/)   

