
# Taller de astronomía 

Materia que aparece con LOMCE  

[Orden 2043/2018, de 4 de junio, de la Consejería de Educación e Investigación, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid para su implantación a partir de 2018-2019, y se modifica la Orden 2200/2017, de 16 de junio, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid, así como la Orden 1255/2017, de 21 de abril, de la Consejería de Educación, Juventud y Deporte, por la que se establece la organización de las enseñanzas para la obtención del título de Graduado en Educación Secundaria Obligatoria por personas adultas en la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2018/06/14/BOCM-20180614-16.PDF)  

> Artículo 2
> Materias de libre configuración autonómica aprobadas

| MATERIA | CURSO | DEPARTAMENTO DIDÁCTICO RESPONSABLE DE SU IMPARTICIÓN | ESPECIALIDAD DOCENTE DEL PROFESORADO ENCARGADO DE IMPARTIRLA |
|:-:|:-:|:-:|:-:|
|Taller de Astronomía | Tercero de la ESO | Matemáticas | Matemáticas |  


TALLER DE ASTRONOMÍA  
3.º curso de ESO  
Introducción  

El interés formativo de la materia reside en su contribución a desarrollar en el alumno
el gusto por la ciencia, por el desarrollo tecnológico, el método científico, todo ello a través de contenidos tan atractivos para el adolescente como son los temas astronómicos.  
Su fin es ir abriendo las perspectivas del alumno haciéndole mirar mucho más allá de
su realidad inmediata: hacia las estrellas, hasta los límites del Sistema Solar, hasta el borde
de nuestra galaxia, incluso hasta los límites del Universo. A lo largo de este camino el alumno tendrá la oportunidad de hacerse las preguntas y disertaciones que los primeros Astrónomos, y en definitiva, la humanidad, intentando dar respuesta a muchos de los grandes in-
terrogantes que han ocupado el pensamiento de los seres humanos.  
A esta materia se le dará un enfoque de aprendizaje fundamentalmente activo, práctico y participativo, en la que se combinarán las explicaciones del profesor con debates suscitados en el aula, así como con el uso de las nuevas tecnologías, y también con salidas y
observaciones al aire libre.  
Se intentará en todo momento que el alumno aprenda los conocimientos explicados
por el profesor a través de actividades y observaciones prácticas. Por ello, se utilizará la mayor parte del tiempo para realizar muchos ejercicios en el aula y fuera de ella que relacionen la teoría con la práctica. Se intentará también, a través de estas prácticas, desarrollar en el alumno el concepto del método científico.  
Se usarán medios audiovisuales que apoyarán en todo momento a las explicaciones del
profesor. Estos medios podrán ser: proyecciones desde Internet con cañón y DVD’s, transparencias, presentaciones informáticas, etc. Se recomienda la alternancia entre las clases en el aula tradicional y clases en un aula de informática en la que se utilizarán los ordenadores como complemento a las explicaciones del profesor.  
También se recomienda el uso de los ordenadores para la búsqueda de información y
actualidad astronómica, accediendo a Internet. Prácticamente en todos los temas se reservará un tiempo para indagar en la actualidad astronómica relativa al tema que en ese momento se esté tratando, estando al día en todas las efemérides astronómicas que se vayan
produciendo.  
Se recomienda tener a mano en el aula de Informática programas de cálculo de Efemérides y simulación del movimiento diurno (por ejemplo, StarCalc, programa de acceso gratuito descargado de la red). Al finalizar cada tema, siempre surgirá la posibilidad de establecer un debate en el aula, procurando la participación activa del alumno.  
Se recomienda que el alumno haga el seguimiento de la asignatura a través de un cuaderno individual en el que deberá anotar las explicaciones del profesor, experiencias y actividades realizadas fuera y dentro del aula y cualquier actividad que se realice en el Taller. Esta será su herramienta fundamental de trabajo.  

Contenidos  

Bloque 1. Introducción a la Astronomía e historia:  
1. Aspectos de los que se ocupa la Astronomía.
2. Historia de la Astronomía:
- La Astronomía en la antigüedad (Prehistoria, Mesopotamia, Egipto...).
- Astronomía clásica (Grecia, Alejandría, Roma,..).
- La Astronomía en la Edad Media.
- Astronomía moderna.
- La era espacial.
- Actualidad.

Bloque 2. Coordenadas celestes:
1. Esfera y bóveda celeste. Movimientos aparentes.
2. Coordenadas horizontales: acimut y altura.
3. Coordenadas ecuatoriales: ascensión recta y declinación.

Bloque 3. Observaciones a simple vista: constelaciones y movimientos del Sol, la Luna y los planetas:
1. Observaciones a simple vista. Principales constelaciones en el hemisferio norte y en el hemisferio sur.
2. Uso de planisferios y software específico.
3. Movimientos de la Tierra: Rotación, traslación y precesión.
4. Movimiento aparente del Sol. Eclíptica y Zodíaco.
5. Movimiento de la Luna y sus fases.
6. Movimiento aparente de los planetas.

Bloque 4. Óptica y telescopios:
1. Breves nociones de óptica y tipos de telescopio.
2. Funcionamiento de un telescopio.

Bloque 5. Sistema Solar:
1. Leyes del movimiento planetario: Leyes de Kepler.
2. Características de los planetas y sus principales satélites.
3. La Luna: origen y formación. Geografía lunar.
4. Asteroides, cometas, Nube de Oort y cinturón de Kuiper.

Bloque 6. Las estrellas:
1. Formación, características, clasificación y evolución.
2. Observación de estrellas. Brillo y color.
3. Espectroscopía. Composición de las estrellas.
4. El Sol.

Bloque 7. La Vía Láctea y otras galaxias:
1. Evolución, estructura, dimensiones. Tipos de galaxias.
2. Nebulosas y cúmulos.
3. La Vía Láctea.

Bloque 8. Universo extragaláctico. Cosmología:
1. Cuásares, estrellas de neutrones, agujeros negros, materia oscura.
2. El origen del Universo: Big Bang.
3. Estructura y evolución del universo. Universo en expansión.

Bloque 9. Investigación y actualidad:
1. Distintas agencias espaciales: ESA, NASA...
2. Líneas de investigación actuales y proyectos futuros.

Criterios de evaluación y estándares de aprendizaje evaluables

Bloque 1. Introducción a la Astronomía e historia:
1. Conocer las distintas teorías que a lo largo de la historia han explicado los fenómenos astronómicos, así como los principales astrónomos y científicos implicados.  
1.1. Describe los principales acontecimientos relacionados con la Astronomía desde las antiguas civilizaciones hasta nuestros días.  
1.2. Conoce los personajes más notables que han contribuido al desarrollo y a la evolución y avance de la Astronomía.  
2. Reconocer el método científico seguido en la elaboración de una teoría.  
2.1. Argumenta razonadamente y con actitud crítica las teorías antiguas y actuales.  

Bloque 2. Coordenadas celestes:
1. Distinguir entre esfera y bóveda celeste y reconocer la necesidad de ambas representaciones.
1.1. Distingue claramente entre esfera y bóveda celeste.  
1.2. Identifica líneas y puntos relevantes en la esfera celeste y en la bóveda.  
2. Expresar la posición de una estrella y otros cuerpos celestes en coordenadas horizontales y ecuatoriales.  
2.1. Distingue las coordenadas horizontales y las ecuatoriales y la utilidad de cada una en cada caso.  
2.2. Calcula las coordenadas horizontales aproximadas de un cuerpo celeste, simplemente observando su posición aparente.  

Bloque 3. Observaciones a simple vista: constelaciones y movimientos del Sol, la Luna y los planetas:
1. Identificar los principales objetos astronómicos observables a simple vista.  
1.1. Identifica las principales constelaciones visibles desde nuestra latitud y las constelaciones del Zodíaco.  
2. Reconocer la utilidad de instrumentos como el planisferio en la observación del firmamento.  
2.1. Maneja el planisferio y software específico para realizar observaciones.  
3. Enumerar los movimientos de la Tierra en el espacio y su efecto.  
3.1. Distingue los movimientos de rotación y traslación de la Tierra y los relaciona con sus consecuencias inmediatas (día, noche, estaciones,...).  
4. Describir los movimientos aparentes del Sol, la Luna y los planetas en el cielo y en la esfera celeste.
4.1. Conoce y comprende las fases de la Luna y su movimiento alrededor de la Tierra.  
4.2. Conoce los distintos planetas y su posición habitual en el cielo.  

Bloque 4. Óptica y telescopios:  
1. Entender el funcionamiento de los telescopios y prismáticos.  
1.1. Posee las nociones básicas del funcionamiento y utilización de un telescopio.  
1.2. Maneja un telescopio a nivel inicial y realiza observaciones de la Luna y otros cuerpos celestes con él.  
2. Conocer los fundamentos básicos de la óptica.  
2.1. Describe los fundamentos básicos de la óptica.  

Bloque 5. Sistema Solar:  
1. Conocer las leyes de Kepler y valorar su importancia histórica.  
1.1. Conoce el significado de las leyes de Kepler y su implicación histórica.  
2. Enumerar y conocer las principales características de la Luna, los planetas y sus satélites.  
2.1. Describe las principales características de la Luna, su origen y su formación.  
2.2. Enumera las características principales de los planetas y los principales satélites de estos.  
3. Conocer las características de otros cuerpos del Sistema Solar como asteroides y cometas.  
3.1. Distingue entre asteroide y cometa y conoce las características de cada uno.  

Bloque 6. Las estrellas:  
1. Comprender el proceso de formación y evolución de una estrella.  
1.1. Comprende el proceso de formación y evolución de una estrella.  
2. Entender el fundamento de las reacciones nucleares que se producen en el interior de las estrellas.  
2.1. Entiende el proceso y las reacciones que se producen en el interior de las estrellas para producir luz y calor.  
3. Conocer los distintos sistemas de clasificación de las estrellas y el lugar que ocupa el Sol en cada uno de ellos.  
3.1. Conoce los rasgos identificativos de las estrellas y sabe clasificarlas en los distintos sistemas.  
3.2. Entiende y maneja el diagrama de Hertzsprung-Russel, valorando su importancia.  
4. Describir las características principales de nuestro Sol.  
4.1. Conoce las características fundamentales de nuestro Sol y establece la comparación del mismo con otras estrellas en cuanto a tamaño, luminosidad, edad y evolución.  

Bloque 7. La Vía Láctea y otras galaxias:  
1. Describir la estructura y dimensiones de nuestra galaxia, la Vía Láctea.  
1.1. Conoce la estructura espiral de la Vía Láctea.  
1.2. Sabe situar y entiende la posición que ocupa nuestro Sistema Solar en la Vía Láctea.  
1.3. Conoce los cuerpos y objetos que pueblan la Vía Láctea.  
2. Conocer otras galaxias y los tipos de galaxias que hay en el universo.  
2.1. Enumera otras galaxias y sus características.  
2.2. Entiende las dimensiones de las galaxias y la cantidad ingente de estrellas que existen en cada una y es consciente de la existencia de otros planetas fuera de nuestro Sistema Solar.  

Bloque 8. Universo extragaláctico. Cosmología:  
1. Describir los principales objetos alejados del Sistema Solar.  
1.1. Conoce objetos como cuásares, estrellas de neutrones, agujeros negros, materia oscura.  
1.2. Conoce los instrumentos y grandes telescopios que nos permiten la observación de estos objetos lejanos.  
2. Conocer la estructura del universo a gran escala, así como su origen y las teorías sobre su evolución.  
2.1. Comprende la estructura del universo, su grandeza, sus dimensiones y entiende que la observación del mismo nos lleva a su origen.  

Bloque 9. Investigación y actualidad:  
1. Conocer las distintas agencias espaciales en el mundo y sus estaciones.  
1.1. Conoce las distintas agencias espaciales en el mundo.  
2. Describir las líneas actuales de investigación en torno a la Astronomía y el conocimiento del universo.  
2.1. Sabe describir las principales líneas de investigación que se siguen actualmente en el mundo de la Astronomía.  
2.2. Conoce las futuras misiones que la NASA y la ESA y otras agencias espaciales están proyectando para un futuro y en busca de qué.  
