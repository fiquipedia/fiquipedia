# Currículo Cultura Científica 1º Bachillerato

Ver DECRETO 52/2015 en  [legislación](/home/legislacion-educativa)  que indica   
*Artículo 9*  
Materias y currículo  
El currículo del Bachillerato en los centros docentes de la Comunidad de Madrid se establece del modo siguiente: ...  
c) En el Anexo I del presente Decreto se establecen los contenidos de las materias del bloque de asignaturas específicas. Los criterios de evaluación y los estándares de aprendizaje evaluables de dichas materias son los del currículo básico fijados para dichas materias en el Real Decreto 1105/2014, de 26 de diciembre, por el que se establece el currículo básico de la Educación Secundaria Obligatoria y del Bachillerato.  
En el caso de que se curse una materia troncal como específica, su currículo será el correspondiente incluido en el Real Decreto 1105/2014, de 26 de diciembre.   
  
El pdf del RD 1105/2014 son 378 páginas y no tiene índice: Cultura Científica está en las páginas 296 a 300, y 1º de Bachillerato de la 299 a la 300.  
  
Hay que tener presente que hay  [Cultura Científica 4º ESO LOMCE](/home/materias/eso/cultura-cientifica-4-eso) , y que en LOE había relacionada  [Ciencias para el Mundo Contemporáneo 1º Bachillerato (CMC)](/home/materias/bachillerato/cienciasparaelmundocontemporaneo-1bachillerato) ; el currículo de CMC en cierto modo se puede decir que se desdobla en 4º y en 1º de CC.  
    
Esta página es estática y sin enlaces: para ver enlaces a recursos ver Contenidos currículo con enlaces a recursos: Cultura Científica 1º Bachillerato (LOMCE) (pendiente)  
  
## Contenidos (Decreto 52/2015)  

### Bloque 1. Procedimientos de trabajo
Herramientas TIC.  
Búsqueda de información.  
Trabajo en grupo.  
Blog.  
Debates.  

### Bloque 2. La Tierra y la vida
Estructura, formación y dinámica de la Tierra.  
El origen de la vida.  
Teorías de la evolución.  
Darwinismo y genética.  
Evolución de los homínidos.  


### Bloque 3. Avances en Biomedicina
Diagnósticos y tratamientos.  
Trasplantes.  
La investigación farmacéutica.  
Principios activos: Genéricos.  
Sistema sanitario.  
Medicina alternativa.  

### Bloque 4. La revolución genética
Los cromosomas.  
Los genes como bases de la herencia.  
El código genético.  
Ingeniería genética: transgénicos, terapias génicas.  
El Proyecto Genoma Humano.  
Aspectos sociales relacionados con la ingeniería genética.  
La clonación y sus consecuencias médicas.  
La reproducción asistida, selección y conservación de embriones.  
Células madre: tipos y aplicaciones.  
Bioética.  

### Bloque 5. Nuevas tecnologías en comunicación e información
Analógico frente a digital.  
Ordenadores: evolución y características.  
Almacenamiento digital de la información.  
Imagen y sonido digital.  
Telecomunicaciones: TDT, telefonía fija y móvil.  
Historia de Internet.  
Conexiones y velocidad de acceso a Internet. La fibra óptica.  
Redes sociales.  
Peligros de Internet.  
Satélites de comunicación.  
GPS: funcionamiento y funciones.  
Tecnología LED.  
Comunicaciones seguras: clave pública y privacidad. Encriptación de la información.  
Firma electrónica y la administración electrónica.  
La vida digital.  

## Criterios de evaluación (Real Decreto 1105/2014)  

### Bloque 1. Procedimientos de trabajo
1. Obtener, seleccionar y valorar informaciones relacionadas con la ciencia y la tecnología a partir de distintas fuentes de información.  
2. Valorar la importancia que tiene la investigación y el desarrollo tecnológico en la actividad cotidiana.  
3. Comunicar conclusiones e ideas en soportes públicos diversos, utilizando eficazmente las tecnologías de la información y comunicación para transmitir opiniones propias argumentadas.  

### Bloque 2. La Tierra y la vida
1. Justificar la teoría de la deriva continental en función de las evidencias experimentales que la apoyan.  
2. Explicar la tectónica de placas y los fenómenos a que da lugar.  
3. Determinar las consecuencias del estudio de la propagación de las ondas sísmicas P y S, respecto de las capas internas de la Tierra.  
4. Enunciar las diferentes teorías científicas que explican el origen de la vida en la Tierra.  
5. Establecer las pruebas que apoyan la teoría de la selección natural de Darwin y utilizarla para explicar la evolución de los seres vivos en la Tierra.  
6. Reconocer la evolución desde los primeros homínidos hasta el hombre actual y establecer las adaptaciones que nos han hecho evolucionar.  
7. Conocer los últimos avances científicos en el estudio de la vida en la Tierra.  

### Bloque 3. Avances en Biomedicina
1. Analizar la evolución histórica en la consideración y tratamiento de las enfermedades.  
2. Distinguir entre lo que es Medicina y lo que no lo es.  
3. Valorar las ventajas que plantea la realización de un trasplante y sus consecuencias.  
4. Tomar conciencia de la importancia de la investigación médico-farmacéutica.  
5. Hacer un uso responsable del sistema sanitario y de los medicamentos.  
6. Diferenciar la información procedente de fuentes científicas de aquellas que proceden de pseudociencias o que persiguen objetivos meramente comerciales.  

### Bloque 4. La revolución genética
1. Reconocer los hechos históricos más relevantes para el estudio de la genética.  
2. Obtener, seleccionar y valorar informaciones sobre el ADN, el código genético, la ingeniería genética y sus aplicaciones médicas.  
3. Conocer los proyectos que se desarrollan actualmente como consecuencia de descifrar el genoma humano, tales como HapMap y Encode.  
4. Evaluar las aplicaciones de la ingeniería genética en la obtención de fármacos, transgénicos y terapias génicas.  
5. Valorar las repercusiones sociales de la reproducción asistida, la selección y conservación de embriones.  
6. Analizar los posibles usos de la clonación.  
7. Establecer el método de obtención de los distintos tipos de células madre, así como su potencialidad para generar tejidos, órganos e incluso organismos completos.  
8. Identificar algunos problemas sociales y dilemas morales debidos a la aplicación de la genética: obtención de transgénicos, reproducción asistida y clonación.  

### Bloque 5. Nuevas tecnologías en comunicación e información
1. Conocer la evolución que ha experimentado la informática, desde los primeros prototipos hasta los modelos más actuales, siendo consciente del avance logrado en parámetros tales como tamaño, capacidad de proceso, almacenamiento, conectividad, portabilidad, etc.  
2. Determinar el fundamento de algunos de los avances más significativos de la tecnología actual.  
3. Tomar conciencia de los beneficios y problemas que puede originar el constante avance tecnológico.  
4. Valorar, de forma crítica y fundamentada, los cambios que internet está provocando en la sociedad.  
5. Efectuar valoraciones críticas, mediante exposiciones y debates, acerca de problemas relacionados con los delitos informáticos, el acceso a datos personales, los problemas de socialización o de excesiva dependencia que puede causar su uso.  
6. Demostrar mediante la participación en debates, elaboración de redacciones y/o comentarios de texto, que se es consciente de la importancia que tienen las nuevas tecnologías en la sociedad actual.  

## Estándares de aprendizaje evaluables (Real Decreto 1105/2014)

### Bloque 1. Procedimientos de trabajo
1.1. Analiza un texto científico o una fuente científico-gráfica, valorando de forma crítica, tanto su rigor y fiabilidad, como su contenido.  
1.2. Busca, analiza, selecciona, contrasta, redacta y presenta información sobre un tema relacionado con la ciencia y la tecnología, utilizando tanto los soportes tradicionales como Internet.  
2.1. Analiza el papel que la investigación científica tiene como motor de nuestra sociedad y su importancia a lo largo de la historia.  
3.1. Realiza comentarios analíticos de artículos divulgativos relacionados con la ciencia y la tecnología, valorando críticamente el impacto en la sociedad de los textos y/o fuentes científico-gráficas analizadas y defiende en público sus conclusiones.  

### Bloque 2. La Tierra y la vida
1.1. Justifica la teoría de la deriva continental a partir de las pruebas geográficas, paleontológicas, geológicas y paleoclimáticas.  
2.1. Utiliza la tectónica de placas para explicar la expansión del fondo oceánico y la actividad sísmica y volcánica en los bordes de las placas.  
3.1. Relaciona la existencia de diferentes capas terrestres con la propagación de las ondas sísmicas a través de ellas.  
4.1. Conoce y explica las diferentes teorías acerca del origen de la vida en la Tierra.  
5.1. Describe las pruebas biológicas, paleontológicas y moleculares que apoyan la teoría de la evolución de las especies.  
5.2. Enfrenta las teorías de Darwin y Lamarck para explicar la selección natural.  
6.1. Establece las diferentes etapas evolutivas de los homínidos hasta llegar al Homo sapiens, estableciendo sus características fundamentales, tales como capacidad craneal y altura.  
6.2. Valora de forma crítica, las informaciones asociadas al universo, la Tierra y al origen de las especies, distinguiendo entre información científica real, opinión e ideología.  
7.1. Describe las últimas investigaciones científicas en torno al conocimiento del origen y desarrollo de la vida en la Tierra.  

### Bloque 3. Avances en Biomedicina
1.1. Conoce la evolución histórica de los métodos de diagnóstico y tratamiento de las enfermedades.  
2.1. Establece la existencia de alternativas a la medicina tradicional, valorando su fundamento científico y los riesgos que conllevan.  
3.1. Propone los trasplantes como alternativa en el tratamiento de ciertas enfermedades, valorando sus ventajas e inconvenientes.  
4.1. Describe el proceso que sigue la industria farmacéutica para descubrir, desarrollar, ensayar y comercializar los fármacos.  
5.1. Justifica la necesidad de hacer un uso racional de la sanidad y de los medicamentos.  
6.1. Discrimina la información recibida sobre tratamientos médicos y medicamentos en función de la fuente consultada.  

### Bloque 4. La revolución genética
1.1. Conoce y explica el desarrollo histórico de los estudios llevados a cabo dentro del campo de la genética.  
2.1. Sabe ubicar la información genética que posee todo ser vivo, estableciendo la relación jerárquica entre las distintas estructuras, desde el nucleótido hasta los genes responsables de la herencia.  
3.1. Conoce y explica la forma en que se codifica la información genética en el ADN , justificando la necesidad de obtener el genoma completo de un individuo y descifrar su significado.  
4.1. Analiza las aplicaciones de la ingeniería genética en la obtención de fármacos, transgénicos y terapias génicas.  
5.1. Establece las repercusiones sociales y económicas de la reproducción asistida, la selección y conservación de embriones.  
6.1. Describe y analiza las posibilidades que ofrece la clonación en diferentes campos.  
7.1. Reconoce los diferentes tipos de células madre en función de su procedencia y capacidad generativa, estableciendo en cada caso las aplicaciones principales.  
8.1. Valora, de forma crítica, los avances científicos relacionados con la genética, sus usos y consecuencias médicas y sociales.  
8.2. Explica las ventajas e inconvenientes de los alimentos transgénicos, razonando la conveniencia o no de su uso.  

### Bloque 5. Nuevas tecnologías en comunicación e información
1.1. Reconoce la evolución histórica del ordenador en términos de tamaño y capacidad de proceso.  
1.2. Explica cómo se almacena la información en diferentes formatos físicos, tales como discos duros, discos ópticos y memorias, valorando las ventajas e inconvenientes de cada uno de ellos.  
1.3. Utiliza con propiedad conceptos específicamente asociados al uso de Internet.  
2.1. Compara las prestaciones de dos dispositivos dados del mismo tipo, uno basado en la tecnología analógica y otro en la digital.  
2.2. Explica cómo se establece la posición sobre la superficie terrestre con la información recibida de los sistemas de satélites GPS o GLONASS.  
2.3. Establece y describe la infraestructura básica que requiere el uso de la telefonía móvil.  
2.4. Explica el fundamento físico de la tecnología LED y las ventajas que supone su aplicación en pantallas planas e iluminación.  
2.5. Conoce y describe las especificaciones de los últimos dispositivos, valorando las posibilidades que pueden ofrecer al usuario.  
3.1. Valora de forma crítica la constante evolución tecnológica y el consumismo que origina en la sociedad.  
4.1. Justifica el uso de las redes sociales, señalando las ventajas que ofrecen y los riesgos que suponen.  
4.2. Determina los problemas a los que se enfrenta Internet y las soluciones que se barajan.  
5.1. Describe en qué consisten los delitos informáticos más habituales.  
5.2. Pone de manifiesto la necesidad de proteger los datos mediante encriptación, contraseña, etc.  
6.1. Señala las implicaciones sociales del desarrollo tecnológico.  
  
