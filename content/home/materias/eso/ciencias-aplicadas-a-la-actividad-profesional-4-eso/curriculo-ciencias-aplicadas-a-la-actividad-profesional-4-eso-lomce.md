
# Currículo ciencias aplicadas a la actividad profesional 4º ESO (LOMCE)

Ver DECRETO 48/2015 en  [legislación](/home/legislacion-educativa)   
El decreto fija contenidos, criterios de evaluación y estándares de aprendizaje evaluables para la comunidad de Madrid.  
  
Es una materia que es modificada en LOMCE respecto a LOE, ver  [Currículo ciencias aplicadas a la actividad profesional 4º ESO (LOE)](/home/materias/eso/ciencias-aplicadas-a-la-actividad-profesional-4-eso/curriculo-ciencias-aplicadas-a-la-actividad-profesional-4-eso)   
  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver Contenidos currículo con enlaces a recursos: 4º ESO Currículo ciencias aplicadas a la actividad profesional (LOMCE) (pendiente)  

## Contenidos

### Bloque 1. Técnicas instrumentales básicas
1. Laboratorio: organización, materiales y normas de seguridad.  
2. Utilización de herramientas TIC para el trabajo experimental del laboratorio.  
3. Técnicas de experimentación en física, química, biología y geología.  
4. Aplicaciones de la ciencia en las actividades laborales.  

### Bloque 2. Aplicaciones de la ciencia en la conservación del medio ambiente
1. Contaminación: concepto y tipos.  
2. Contaminación del suelo.  
3. Contaminación del agua.  
4. Contaminación del aire.  
5. Contaminación nuclear.  
6. Tratamiento de residuos.  
7. Nociones básicas y experimentales sobre química ambiental.  
8. Desarrollo sostenible.  

### Bloque 3. Investigación, Desarrollo e Innovación (I+D+i)
1. Concepto de I+D+i. Importancia para la sociedad. Innovación.  

### Bloque 4. Proyecto de investigación
1. Proyecto de investigación.  

## Criterios de evaluación y estándares de aprendizaje evaluables.

### Bloque 1. Técnicas instrumentales básicas
1. Utilizar correctamente los materiales y productos del laboratorio.  
1.1. Determina el tipo de instrumental de laboratorio necesario según el tipo de ensayo que va a realizar.  
2. Cumplir y respetar las normas de seguridad e higiene del laboratorio.  
2.1. Reconoce y cumple las normas de seguridad e higiene que rigen en los trabajos de laboratorio.  
3. Contrastar algunas hipótesis basándose en la experimentación, recopilación de datos y análisis de resultados.  
3.1. Recoge y relaciona datos obtenidos por distintos medios para transferir información de carácter científico.  
4. Aplicar las técnicas y el instrumental apropiado para identificar magnitudes.  
4.1. Determina e identifica medidas de volumen, masa o temperatura utilizando ensayos de tipo físico o químico.  
5. Preparar disoluciones de diversa índole, utilizando estrategias prácticas.  
5.1. Decide qué tipo de estrategia práctica es necesario aplicar para el preparado de una disolución concreta.  
6. Separar los componentes de una mezcla Utilizando las técnicas instrumentales apropiadas.  
6.1. Establece qué tipo de técnicas de separación y purificación de sustancias se deben utilizar en algún caso concreto.  
7. Predecir qué tipo biomoléculas están presentes en distintos tipos de alimentos.  
7.1. Discrimina qué tipos de alimentos contienen a diferentes biomoléculas.  
8. Determinar qué técnicas habituales de desinfección hay que utilizar según el uso que se haga del material instrumental.  
8.1. Describe técnicas y determina el instrumental apropiado para los procesos cotidianos de desinfección.  
9. Precisar las fases y procedimientos habituales de desinfección de materiales de uso cotidiano en los establecimientos sanitarios, de imagen personal, de tratamientos de bienestar y en las industrias y locales relacionados con las industrias alimentarias y sus aplicaciones.  
9.1. Resuelve sobre medidas de desinfección de materiales de uso cotidiano en distintos tipos de industrias o de medios profesionales.  
10. Analizar los procedimientos instrumentales que se utilizan en diversas industrias como la alimentaria, agraria, farmacéutica, sanitaria, imagen personal, etc.  
10.1. Relaciona distintos procedimientos instrumentales con su aplicación en el campo industrial o en el de servicios  
11. Contrastar las posibles aplicaciones científicas en los campos profesionales directamente relacionados con su entorno.  
11.1. Señala diferentes aplicaciones científicas con campos de la actividad profesional de su entorno.  

### Bloque 2. Aplicaciones de la ciencia en la conservación del medio ambiente
1. Precisar en qué consiste la contaminación y categorizar los tipos más representativos.  
1.1. Utiliza el concepto de contaminación aplicado a casos concretos.  
1.2. Discrimina los distintos tipos de contaminantes de la atmósfera, así como su origen y efectos.  
2. Contrastar en qué consisten los distintos efectos medioambientales tales como la lluvia ácida, el efecto invernadero, la destrucción de la capa de ozono y el cambio climático.  
2.1. Categoriza los efectos medioambientales conocidos como lluvia ácida, efecto invernadero, destrucción de la capa de ozono y el cambio global a nivel climático y valora sus efectos negativos para el equilibrio del planeta.  
3. Precisar los efectos contaminantes que se derivan de la actividad industrial y agrícola, principalmente sobre el suelo.  
3.1. Relaciona los efectos contaminantes de la actividad industrial y agrícola sobre el suelo.  
4. Precisar los agentes contaminantes del agua e informar sobre el tratamiento de depuración de las mismas. Recopila datos de observación y experimentación para detectar contaminantes en el agua.  
4.1. Discrimina los agentes contaminantes del agua, conoce su tratamiento y diseña algún ensayo sencillo de laboratorio para su detección.  
5. Precisar en qué consiste la contaminación nuclear, reflexionar sobre la gestión de los residuos nucleares y valorar críticamente la utilización de la energía nuclear.  
5.1. Establece en qué consiste la contaminación nuclear, analiza la gestión de los residuos nucleares y argumenta sobre los factores a favor y en contra del uso de la energía nuclear.  
6. Identificar los efectos de la radiactividad sobre el medio ambiente y su repercusión sobre el futuro de la humanidad.  
6.1. Reconoce y distingue los efectos de la contaminación radiactiva sobre el medio ambiente y la vida en general.  
7. Precisar las fases procedimentales que intervienen en el tratamiento de residuos.  
7.1. Determina los procesos de tratamiento de residuos y valora críticamente la recogida selectiva de los mismos.  
8. Contrastar argumentos a favor de la recogida selectiva de residuos y su repercusión a nivel familiar y social.  
8.1. Argumenta los pros y los contras del reciclaje y de la reutilización de recursos materiales.  
9. Utilizar ensayos de laboratorio relacionados con la química ambiental, conocer que es una medida de pH y su manejo para controlar el medio ambiente.  
9.1. Formula ensayos de laboratorio para conocer aspectos desfavorables del medioambiente.  
10. Analizar y contrastar opiniones sobre el concepto de desarrollo sostenible y sus repercusiones para el equilibrio medioambiental.  
10.1. Identifica y describe el concepto de desarrollo sostenible, enumera posibles soluciones al problema de la degradación medioambiental.  
11. Participar en campañas de sensibilización, a nivel del centro educativo, sobre la necesidad de controlar la utilización de los recursos energéticos o de otro tipo.  
11.1. Aplica junto a sus compañeros medidas de control de la utilización de los recursos e implica en el mismo al propio centro educativo.  
12. Diseñar estrategias para dar a conocer a sus compañeros y personas cercanas la necesidad de mantener el medioambiente.  
12.1. Plantea estrategias de sostenibilidad en el entorno del centro.  

### Bloque 3. Investigación, Desarrollo e Innovación (I+D+i)
1. Analizar la incidencia de la I+D+i en la mejora de la productividad, aumento de la competitividad en el marco globalizador actual.  
1.1. Relaciona los conceptos de Investigación, Desarrollo e innovación. Contrasta las tres etapas del ciclo I+D+i.  
2. Investigar, argumentar y valorar sobre tipos de innovación ya sea en productos o en procesos, valorando críticamente todas las aportaciones a los mismos ya sea de organismos estatales o autonómicos y de organizaciones de diversa índole.  
2.1. Reconoce tipos de innovación de productos basada en la utilización de nuevos materiales, nuevas tecnologías etc., que surgen para dar respuesta a nuevas necesidades de la sociedad.  
2.2. Enumera qué organismos y administraciones fomentan la I+D+i en nuestro país a nivel estatal y autonómico.  
3. Recopilar, analizar y discriminar información sobre distintos tipos de innovación en productos y procesos, a partir de ejemplos de empresas punteras en innovación.  
3.1. Precisa como la innovación es o puede ser un factor de recuperación económica de un país.  
3.2. Enumera algunas líneas de I+D+i que hay en la actualidad para las industrias químicas, farmacéuticas, alimentarias y energéticas.  
4. Utilizar adecuadamente las TIC en la búsqueda, selección y proceso de la información encaminadas a la investigación o estudio que relacione el conocimiento científico aplicado a la actividad profesional.  
4.1. Discrimina sobre la importancia que tienen las tecnologías de la información y la comunicación en el ciclo de investigación y desarrollo.  

### Bloque 4. Proyecto de investigación
1. Planear, aplicar, e integrar las destrezas y habilidades propias de trabajo científico.  
1.1. Integra y aplica las destrezas propias de los métodos de la ciencia.  
2. Elaborar hipótesis, y contrastarlas a través de la experimentación o la observación y argumentación.  
2.1. Utiliza argumentos justificando las hipótesis que propone.  
3. Discriminar y decidir sobre las fuentes de información y los métodos empleados para su obtención.  
3.1. Utiliza diferentes fuentes de información, apoyándose en las TIC, para la elaboración y presentación de sus investigaciones.  
4. Participar, valorar y respetar el trabajo individual y en grupo.  
4.1. Participa, valora y respeta el trabajo individual y grupal.  
5. Presentar y defender en público el proyecto de investigación realizado  
5.1. Diseña pequeños trabajos de investigación sobre un tema de interés cienfítico-tecnológico, animales y/o plantas, los ecosistemas de su entorno o la alimentación y nutrición humana para su presentación y defensa en el aula.  
5.2. Expresa con precisión y coherencia tanto verbalmente como por escrito las conclusiones de sus investigaciones.  
 
