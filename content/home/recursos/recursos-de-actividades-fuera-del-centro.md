---
aliases:
  - /home/recursos/recuros-de-actividades-fuera-del-centro/
---

# Recursos de actividades fuera del centro 

Además de reflejar aquí posibilidades de actividades extraescolares, se reflejan otro tipo de actividades.  
Esta página se centra en actividades para los alumnos, hay una página separada de  [recursos para profesores](/home/recursos/recursos-para-profesores)  desde donde se enlazarán posibles actividades fuera del centro para profesores: formación, cursos, ...  
En general son actividades que tienen o pueden tener algo de relación con la ciencia.  

## Actividades extraescolares (salir del centro)  

Típicamente se realizan durante días lectivos. Intentaré poner aquí actividades asociadas al área cercana a Madrid.  
Ordenadas por orden alfabético con el nombre asignado: siempre se pueden llamar de varias maneras y estarían en otro orden... salvo "otros" que está al final...  

### Big Van ciencia. El arte de contar la ciencia
 
[¿Quieres que Big Van Ciencia visite tu centro educativo?](https://www.bigvanciencia.com/institutos)  

### Centro de Astrobiología del INTA, asociado a la NASA
 [Visitas cab.inta-csic.es](https://cab.inta-csic.es/divulgacion/visitas/)   
A partir de 4º ESO y Bachillerato.  

### Centro de Información del Consejo de Seguridad Nuclear 
 [Cómo visitar el Centro de Información - CSN](https://www.csn.es/como-visitar-el-centro-de-informacion)   
> El Centro de Información, ubicado en la sede del Consejo de Seguridad Nuclear, pone sus instalaciones a disposición de los estudiantes de segundo ciclo de ESO, Bachillerato, Formación Profesional, enseñanzas universitarias, centros de educación de personas adultas, y otros colectivos que lo requieran.  
La visita tiene una duración prevista de hora y cuarenta y cinco minutos y consta de un recorrido guiado y la proyección de un vídeo. Se pueden programar dos visitas guiadas al día de lunes a viernes, para un máximo de 35 visitantes por grupo, en horario de 9:30 a 11:15 h y de 12:15 a 14:00 h.  
Para poder realizar la visita al Centro de Información es necesario concertarla previamente a partir del mes de septiembre de cada curso. En caso de estar interesado, rellene el [formulario](https://www.csn.es/SEVis/formulario?lang=es) de solicitud de visita.  
 
  
Correo enviado a los centros en septiembre 2014: (la información es similar a la que aparece en la web, pero se incluye aquí)  
  
Estimado/a director/a:  
Como en cursos anteriores, el Centro de Información del Consejo de Seguridad Nuclear (CSN) pone sus instalaciones a disposición de los estudiantes de segundo ciclo de ESO, Bachillerato, Formación Profesional, enseñanzas universitarias, centros de educación de personas adultas, y otros colectivos que lo requieran.  
Este Centro, ubicado en la sede del CSN, pretende dar un mayor impulso a una de las funciones que este organismo tiene encomendadas por su Ley de Creación: informar a la opinión pública.  
En este recinto se desarrollan de forma interactiva 29 módulos, algunos de los cuales están adaptados a personas con discapacidad sensorial. Como se explica en el tríptico adjunto, la exposición se encuentra dividida en cuatro ámbitos:  
● Historia de las radiaciones y fundamentos de la ciencia (radiación natural)  
● Usos y aplicaciones en la industria, medicina e investigación (radiación artificial)  
● Riesgos y servidumbres (dosis, residuos radiactivos)  
● El Consejo de Seguridad Nuclear (organismo regulador)  
La visita tiene una duración prevista de hora y media y consta de un recorrido guiado por el Centro, la proyección de un vídeo y una última parte dedicada a visita libre.  
Para el curso 2014-2015 se han programado dos visitas guiadas al día, de lunes a viernes, para un máximo de 35 alumnas/os por grupo, en horario de: 10:00 a 11:30 h y de 12:15 a 13:45 h  
También existe la posibilidad de realizar una visita no guiada de 1 hora, de lunes a viernes en horario de mañana de 10:30 a 11:30h y de 12:30 a 13:30h,y de lunes a jueves en horario de tarde de 16:00 a 17:00h.  
Para poder realizar la visita al Centro de Información es necesario concertarla previamente, bien mediante el formulario debidamente cumplimentado, que figura en la web en la dirección: (dirección ya no válida en 2022), o bien por fax, para lo que adjuntamos modelo de solicitud.  
Rogamos trasladen esta información a todos los departamentos que puedan tener interés en realizar esta visita: jefatura de estudios, física y química, biología, tecnología, ciencias del mundo contemporáneo, ciclos formativos (radiodiagnóstico, salud ambiental, medicina nuclear…), actividades extraescolares …  

  
En 2021 se ofrecen de manera virtual [Cómo visitar el centro de información - CSN](https://www.csn.es/como-visitar-el-centro-de-informacion) Las visitas al Centro de Información del Consejo de Seguridad Nuclear (CSN) permanecen suspendidas hasta que se normalice la situación provocada por la pandemia Covid-19.Estamos a la espera de que se puedan reanudar bajo adecuadas medidas de seguridad. Mientras tanto, les recordamos que tienen a su disposición la  [visita virtual al Centro de Información](https://www.csn.es/visita-virtual)  en esta misma web, así como un  [vídeo resumen de la visita guiada](https://www.csn.es/video-visita-guiada) .   

[Módulos interactivos](https://www.csn.es/varios/modulos-interactivos/)  

### Física en el parque de atracciones
 [Jornadas de la Física. Aprende física y mecánica a través de las atracciones | Parque de Atracciones de Madrid](https://www.parquedeatracciones.es/blog/jornadas-fisica)   
> Esta actividad está dirigida a los correspondientes de la programación de la asignatura de Física y Química de 4º ESO y 1º Bachillerato, siendo adaptable y ampliable a otros niveles y materias (Tecnología). El precio por participante será de 12,90€ y el grupo tiene que ser de mínimo 20 alumnos.   
 
### Central nuclear Trillo (Guadalajara) 

[Centrales Nucleares Almaraz-Trillo. Visítanos - cnat.es](https://www.cnat.es/visitanos.php)   

### Visita guiada Instituto de Ciencia de Materiales Madrid - CSIC  
[Visitas guiadas para alumnos de Bachillerato - icmm.csic.es](http://www.icmm.csic.es/es/divulgacion/visitas-guiadas.php)  
En septiembre 2022 indica  
>  El calendario de visitas para el curso 2022-2023 está completo.  
Las solicitudes de visita para el curso 2023-2024 se recogerán a través del siguiente formulario a partir del 1 de febrero de 2023.  

### Visita guiada Centro de Biología Molecular Severo Ochoa (CBM) - CSIC

Máximo 30 personas

Las visitas tienen lugar por la mañana, con el siguiente formato:  
    Charla de introducción al CBM.  
    Visita a un laboratorio: en grupos pequeños, de un máximo de 10 personas. Cada grupo será guiado por un miembro del laboratorio que visiten.   
    Visita a un servicio científico: el guía de cada grupo llevará a los estudiantes al servicio científico-técnico con el que más colabore su laboratorio, donde aprenderán sus características y funciones principales.  
    
En 2024 inscripción de 13 septiembre a 30 septiembre.  

Formulario indica “Fechas entre octubre y mayo en las que sería IMPOSIBLE realizar la visita (no se admite preferencia de fechas)”

### Jornadas puertas abiertas


#### Universidad de Alcalá de Henares
[https://puertasabiertas.uah.es/](https://puertasabiertas.uah.es/)   
Desde 9:30 hasta 13:50, varios itinerarios, C: Ingeniería y Arquitectura, D: Ciencias (Edif Medicina), E: Ciencias de la salud (Edif Química)  
En 2021 son virtuales  

### INTA, Torrejón
 [Visitar el INTA](https://www.inta.es/INTA/es/canal-educa/Visitar-el-INTA/)   
En 2010 ofrecía visitas guiadas indicando que alumnos debían cursar 4º de ESO o Bachiller.   

> Para el curso 2024/2025, las visitas se realizarán los martes y jueves.  
El grupo de escolares no podrá ser superior a 30 alumnos.  
El grupo debe ir acompañado por 2 profesores.  

### MDSCC (Madrid Deep Space Communications Complex), Centro INTA - NASA en Robledo de Chavela.
 [Centro de Entrenamiento y Visitantes INTA-NASA. Madrid Deep Space Communications Complex -MDSCC](https://www.mdscc.nasa.gov/index.php/page_newcev2/)   
 
En 2024 indica "Actualmente El Centro de Visitantes de MDSCC no realiza actividades destinadas al público"  
  
### Noche europea de los investigadores
 [La Noche Europea de los Investigadores en Madrid. 27 de noviembre de 2020.](http://www.madrimasd.org/lanochedelosinvestigadores/)   
Distintas instituciones participantes, algunas actividades requieren reserva.  
En 2014 la reserva comienza el 15 de septiembre  

### Obra Social La Caixa
CaixaForum Madrid  
 [caixaforummadrid_es.html](http://obrasocial.lacaixa.es/nuestroscentros/caixaforummadrid/caixaforummadrid_es.html)   
  
 [caixaforum-madrid](http://agenda.obrasocial.lacaixa.es/caixaforum-madrid?educativa=true)   
  
 [exposiciones_es.html](http://obrasocial.lacaixa.es/ambitos/exposicionesitinerantes/exposiciones_es.html)   
  
Cosmocaixa  
 [cosmocaixamadrid_es.html](http://obrasocial.lacaixa.es/nuestroscentros/cosmocaixamadrid/cosmocaixamadrid_es.html)   
(en febrero 2013 surge la noticia de que desaparece de Alcobendas, no se sabe si estará en otro sitio / estará en Madrid capital)  

### MUNCYT (Museo Nacional de Ciencia y Tecnología)  

 [Inicio - MUNCYT. Museo Nacional de Ciencia y Tecnología (es)](http://www.muncyt.es/)   
Muncyt Alcobendas  
En 2014 sustituye al Cosmocaixa de Alcobendas, en las mismas instalaciones (antes estaba junto al museo del ferrocarril)  
Desde la estación de cercanías de Alcobendas-San Sebastián de los Reyes y el Muncyt hay 1 km en línea recta.  

### Observatorio astronómico de Yebes (Guadalajara)
 [Observatorio de Yebes - ciencia.gob.es](https://www.ciencia.gob.es/Organismos-y-Centros/Infraestructuras-Cientificas-y-Tecnicas-Singulares-ICTS/Astronomia-y-Astrofisica/Observatorio-de-Yebes-OY.html)   
  
[Astro Yebes. Aula de astronomía](https://astroyebes.es/)   
[Astro Yebes. Instalaciones y recursos](https://astroyebes.es/instalaciones-y-recursos/observatorio-astronomico-de-yebes)  
[Astro Yebes. Escuela de cohetes](https://astroyebes.es/instalaciones-y-recursos/escuela-de-cohetes)  
Teléfono del ayuntamiento de Yebes que organiza 949290100  

### Potabilizadora (ETAP)  

Se puede dar varios enfoques: procesamiento del agua para su consumo (separación de mezclas, cloración ...), distribución (coste del tratamiento, abastecimiento ...), consumo resposable  
Se puede combinar con una visita a una depuradora, para combinar la idea de cómo se toma de la naturaleza y cómo se devuelve

#### ETAP de Mohernando 
(Asociado a zona Este Madrid)  
 [Visitas a la ETAP de Mohernando - Mancomunidad de Aguas del Sorbe](http://www.aguasdelsorbe.es/web/text.php?id_button=29&id_section=33)   
La MAS (Mancomunidad de Aguas del Sorbe) ofrece estas visitas guiadas a colectivos y asociaciones **del ámbito territorial abastecido**, subvencionando parte del transporte hasta la E.T.A.P.  
*La subvención del transporte solamente aplica a poblaciones abastecidas*  

### Depuradora (EDAR)  

Se puede combinar con una visita a una potabilizadora, para combinar la idea de cómo se toma de la naturaleza y cómo se devuelve  
[La EDAR de Guadalajara recibe visitas de grupos escolares interesados en su funcionamiento](https://nuevaalcarria.com/articulos/la-edar-de-guadalajara-recibe-visitas-de-grupos-escolares-interesados-en-su-funcionamiento)  
> La Estación Depuradora de Aguas Residuales (EDAR) de Guadalajara recibe periódicamente visitas de grupos de escolares interesados en el funcionamiento de la instalación. El gerente de Guadalagua, Andrés Müller, ha querido acompañar a los alumnos de 1º de ESO de Colegio Santa Cruz durante una de estas visitas.  
El Departamento de Biología de este centro escolar organiza todos los años diferentes visitas a la EDAR. No en vano, cada grupo de alumnos del Santa Cruz que pasa por este lugar debe hacer luego un trabajo sobre lo aprendido allí.  

### Visita la Central Hidroeléctrica y el Museo Bolarque (Guadalajara)

[Visita la Central Hidroeléctrica y el Museo Bolarque](https://www.fundacionnaturgy.org/museo-bolarque/programa-educativo-bolarque/visita-la-central-hidroelectrica-museo-bolarque/)  

### Aulas Didácticas de la Energía Iberdrola

[Centros educativos de energías renovables. Aulas Didácticas de la Energía](https://www.iberdrola.com/sostenibilidad/contra-cambio-climatico/concienciacion-medioambiental/centros-educativos-energias-renovables)  
* Higueruela (Albacete)  
* Villacañas (Toledo)  
* Maranchón (Guadalajara)
* Puertollano (Ciudad Real)
* Astudillo (Palencia)

### Museo Naval Madrid

[Museo naval - turismomadrid.es](https://turismomadrid.es/es/oferta-cultural/museos/museo-naval.html)  
[Museo naval - defensa.gob.es](https://armada.defensa.gob.es/ArmadaPortal/page/Portal/ArmadaEspannola/cienciaorgano/prefLang-es/01cienciamuseo)  

### Real Observatorio de Madrid 

[Real Observatorio de Madrid - ign.es](https://www.ign.es/web/visitas-al-real-observatorio-de-madrid)  

### CESAR (Cooperation through Education in Science and Astronomy Research)
ESAC Camino Bajo del Castillo, s/n., Urb. Villafranca del Castillo, 28692 Villanueva de la Cañada, Madrid.   
[Space Science Experience en ESAC](https://cesar.esa.int/index.php?Section=Space_Science_Experience_Info)  


### Canal Educa. Fundación Canal. Canal de Isabel II
Incluye ETAP y EDAR pero más tipos de actiividades  
 [https://www.canaleduca.com/actividades-presenciales/](https://www.canaleduca.com/actividades-presenciales/)   

### Química en acción. Universidad de Alcalá de Henares
[Facultad de Ciencias - UAH - Química en acción](https://ciencias.uah.es/facultad/quimica-accion.asp)  
[Ediciones de Química en acción - UAH](https://ciencias.uah.es/facultad/quimica-accion-ediciones.asp)  
Se detallan centros participantes desde 2000 a 2020 (fue en enero 2020)  

Nota de prensa de 2014 (se realizó en el mes de enero)  
[Nueva edición de 'Química en acción' para estudiantes de Bachillerato en la Universidad de Alcalá](https://portal.uah.es/portal/page/portal/servicio_comunicacion/sala_prensa/notas_prensa/2014/01/Qu%EDmica%20en%20acci%F3n)   
Se indica "Para más información, teléfono 91-885 4905 (decanato de la Sección de Química) y www.uah.es/quimica"  
En el curso 2014-2015 se abrió plazo de solicitud en diciembre, y se concedieron por orden de solicitud, sin conceder las últimas peticiones, dentro de plazo, de diciembre.  
Cuadernillo describiendo actividades en  [Cuadernillo.pdf](https://ciencias.uah.es/facultad/documentos/quimica-accion/Cuadernillo.pdf)   

### [UAH. Jornadas, cursos y talleres para estudiantes](https://www.uah.es/es/actividades-secundaria/jornadas-cursos-y-talleres-para-estudiantes/)

[Búsqueda del significado práctico de las leyes de Snell dentro del campo de la óptica](https://www.uah.es/es/actividades-secundaria/ultimas-publicaciones/Busqueda-del-significado-practico-de-las-leyes-de-Snell-dentro-del-campo-de-la-optica/)  
Taller dirigido a estudiantes de 2º bachillerato cursando la rama de Ciencia y Tecnología. 

[Diseño, simulación y medidas prácticas de una antena de televisión](https://www.uah.es/es/actividades-secundaria/ultimas-publicaciones/Diseno-simulacion-y-medidas-practicas-de-una-antena-de-television/)  
Taller dirigido a estudiantes de 1º bachillerato cursando la rama de Ciencia y Tecnología. 

### Visita al acelerador UAM

[Visita guiada al acelerador de Partículas de la UAM](https://www.madrimasd.org/notiweb/agenda/visita-guiada-acelerador-particulas-uam)  
Centro de Micro-Análisis de Materiales | Universidad Autónoma de Madrid  
El CMAM apoya la diversidad en la ciencia, en especial la iniciativa del 11 de febrero, Día Internacional de la Mujer y la Niña en la Ciencia. Por ello, participaremos una vez más, organizando una jornada de puertas abiertas para mostrar el acelerador de partículas de la Universidad Autónoma de Madrid. La visita está dirigida al público en general y a centros educativos. Podrás divertirte con nuestras investigadoras, que te contarán de primera mano su experiencia en la ciencia.  
En 2022 se realizó el día 3 febrero 2022   

### ALBA sincrotrón Barcelona

[Visita el ALBA](https://www.albasynchrotron.es/es/divulgacion/visita-alba)  
> Las visitas tienen una duración de 1 hora y 15 minutos aproximadamente. Para reservar un turno de visita se precisa ser un mínimo de 15 personas (no se ofrecen visitas individuales o familiares) y un máximo de 30 participantes. Si tu grupo es más numeroso, debes reservar otra sesión de visita. Encontrarás más detalles de la visita dentro del formulario específico para cada colectivo.  
Debido a la gran demanda, las visitas están dirigidas exclusivamente a grupos escolares (4t ESO, 1º y 2º de bachillerato), grupos universitarios, formación profesional e investigadore/as, así como a instituciones y asociaciones profesionales y culturales. Si quieres conocernos y no perteneces a ninguno de estos colectivos, te invitamos a participar en el ALBA Open Day (próxima edición en 2023).  

### 4º ESO+Empresa
Es una actividad que puede tener su vertiente científica si se elige un sitio especial, por ejemplo laboratorios/departamentos ciencias universitariosEn 2015 incluye CSIC, INTA, ...  
 [ 4eso+empresa | EducaMadrid ](http://www.educa2.madrid.org/web/4eso-empresa) Incluye una relación de "EMPRESAS QUE HAN COLABORADO CON EL PROGRAMA 4ESO+empresa"   
 [uah.es Programa 4º ESO + Empresa](https://uah.es/es/conoce-la-uah/organizacion-y-gobierno/servicios-universitarios/informacion-universitaria/4-eso-y-empresa/)   

### PROGRAMA INVESTIGA - IBM Skillsbuild

Edición 2023 - 2024   
La participación es totalmente GRATUITA  
EDICIÓN PRESENCIAL  
Con la colaboración del CONSEJO SUPERIOR DE INVESTIGACIONES CIENTÍFICAS - CSIC  

https://programainvestiga.org/  

### Otros ... (no necesariamente asociados a física y química / ciencias)  

 [http://festivaleducacine.com/](http://festivaleducacine.com/)   
20-24 enero en 2014. En esa convocatoria no hay relación directa con ciencia  

## Transporte para actividades extraescolares (salir del centro)
 [Núcleo de Cercanías de Madrid](http://www.renfe.com/viajeros/cercanias/madrid/catalago/viaja.html)   
Grupos escolares primaria / secundaria en cercanías renfe Madrid  

## Actividades "intraescolares" (sin salir del centro, "vienen" al centro)

### CPAN en el instituto, ciclo de charlas en IES  
[Charlas CPAN](https://www.i-cpan.es/es/content/charlas)  
> Algunos grupos CPAN tiene una página específica donde podrás reservar una charla directamente   
[Centro de Investigaciones Energéticas, Medioambientales y Tecnológicas (CIEMAT), Madrid](https://www.i-cpan.es/es/content/charlas-que-oferta-el-ciematf)  
Grupo de Física Experimental de Altas Energías del Instituto Universitario de Ciencias y Tecnologías Espaciales de Asturias (ICTEA), Oviedo  
Instituto de Física de Cantabria (IFCA), Cantabria  
Instituto de Física Corpuscular (IFIC), Valencia  
Instituto Gallego de Física de Altas Energías (IGFAE), A Coruña  
 

### Divulgación científica CIEMAT Física de Partículas
Como todos los cursos desde 2011, desde el CIEMAT convocamos el Ciclo de Charlas en Institutos de Educación Secundaria y Bachillerato. Aunque este ciclo está bien establecido y tenemos numerosas solicitudes, tenemos una importante novedad. Como sabéis, el ciclo tradicional estaba centrado en la física y astrofísica de partículas, cosmología y computación científica. Ahora lo hemos ampliado a **todas las áreas científico-técnicas del CIEMAT**, ofertando cerca de 300 charlas repartidas en 34 temas, englobados en 6 áreas temáticas: cultura científica, física fundamental y aceleradores de partículas, biología y biomedicina, energía y tecnología, medio ambiente y fusiónnuclear. Las charlas son para estudiantes de 1° y 2° de bachillerato y, en las charlas que así lo especifican, para 4° de ESO.   
  
La web con toda la información y con la solicitud de inscripción es  [charlas](https://agenda.ciemat.es/e/charlas) .  
**El plazo de inscripción es del 1 al 15 de octubre de 2019. **  
  
**Os animamos a participar y a compartir esta información con vuestras compañeras y compañeros de otros departamentos.**  

[Charlas CIEMAT-CPAN en IES y FP 2022-2023](https://agenda.ciemat.es/e/charlas)  

### Charlas divulgativas jóvenes nucleares
 [Charlas en colegios e institutos - jovenesnucleares.org](http://www.jovenesnucleares.org/blog/que-hacemos/charlas-colegios/) 

### Charlas divulgativas CSIC
[CATÁLOGO DE CONFERENCIAS CIENTÍFICAS DEL CSIC. Dirigidas al sistema educativo y otras entidades de la Comunidad de Madrid](https://delegacion.madrid.csic.es/conferencias)   
[Catálogo de Conferencias Científicas del CSIC dirigidas al Sistema Educativo de la Comunidad de Madrid](https://delegacion.madrid.csic.es/catalogo-de-conferencias-2/)  

[Charlas en centros educativos. Instituto de Ciencia de Materiales Madrid - CSIC](http://www.icmm.csic.es/es/divulgacion/charlas-de-divulgacion.php)  

### [Mujer e ingeniera va a tu centro](https://innovacionyformacion.educa.madrid.org/mujer_ingeniera)   

### Charlas 11 de Febrero. Día Internacional de la Mujer y la Niña en la Ciencia 

[11 de Febrero. Día Internacional de la Mujer y la Niña en la Ciencia](https://11defebrero.org/)  

Fechas de la edición 2023:  
    Del 1 de octubre al 11 de noviembre: solicitud de charlas por parte de los centros educativos.  
    Del 20 de noviembre al 10 de febrero: Inscripción de Centros11F y Actividades11F. Contacto con centros educativos e inscripción de charlas concertadas.  

### Ciencia ambulante
 [ciencia_ambulante.html](http://www.familymuseo.com/museo/ciencia_ambulante.html)   
Con Ciencia Ambulante se mantener un contacto real con centros de enseñanza, asociaciones culturales o cualquier entidad interesada en la comunicación científica. En sus sesiones monitores del centro virtual La Corrala de la Ciencia acudirán a los lugares que lo soliciten para realizar sesiones con demostraciones experimentales espectaculares.  

## [Concursos](/home/recursos/concursos)  

## Experiencias científico-espaciales en el Centro Europeo de Astronomía Espacial (ESAC) de la Agencia Espacial Europea (ESA) por el equipo CESAR (Cooperation through Education in Science and Astronomy Research)
[Space Science Experience en ESAC](https://cesar.esa.int/index.php?Section=Space_Science_Experience_Info)  

## Actividades de fin de semana


### Fin de semana científico MUNCYT
2013: 11 y 12 de Mayo, junto a museo del ferrocarril  
 [IV Finde Científico - 2013](http://www.muncyt.es/portal/site/MUNCYT/menuitem.8dbda8254659d9883c791a1801432ea0/?vgnextoid=f804290bffcab310VgnVCM1000001d04140aRCRD&vgnextchannel=4cff3efc68c5a210VgnVCM1000001034e20aRCRD)   
  
2014: 18 y 19 de octubre. En Alcobendas.  
V Finde Científico 2014  
 [detalle.do](http://www.fecyt.es/fecyt/detalle.do?elegidaNivel1=;SalaPrensa&elegidaNivel2=;SalaPrensa;AgendaEventos&elegidaNivel3=;SalaPrensa;AgendaEventos;EventosPropios&tc=agenda_eventos&id=2014_10_18_finde_cientifico)   
 [finde_cientifico_2014.pdf](http://www.madrid.org/dat_oeste/descargas/14_15/finde_cientifico_2014.pdf)   
  
2015: 23 y 24 de Mayo, en Alcobendas  
VI Finde Científico 2015  
 [](http://www.muncyt.es/portal/site/MUNCYT/menuitem.8dbda8254659d9883c791a1801432ea0/?vgnextoid=cdff4f105c93a410VgnVCM1000001d04140aRCRD&vgnextchannel=4cff3efc68c5a210VgnVCM1000001034e20aRCRD)   


## Jornadas científicas


### International Masterclasses. Hands on particle physics.
Ver  [Physics Masterclasses](/home/recursos/fisica/fisica-de-particulas/physics-masterclasses) , suele ser en febrero/marzo.  
  
Además el CPAN organiza un taller práctico o masterclass dirigido a alumnos de Bachillerato en el marco de la **Semana de la Ciencia. **  
 [participa.php](http://www.i-cpan.es/bachillerato/participa.php)   


### Semana de la ciencia
Visitas guiadas, jornadas de puertas abiertas, talleres, rutas científicas...  
 [Semana de la Ciencia y la Innovación](http://www.madrimasd.org/semanacienciaeinnovacion/ )   
En 2013 de 4 a 17 noviembre, con reservas a partir de 21 de octubre   
En 2014 de 3 a 16 noviembre, con reservas a partir de 20 de octubre   
En 2023 de 6 a 19 noviembre, con reservas a partir de 24 de octubre   

## Actividades de verano


### Campus Científico Verano (CCV)
Pensados para alumnos de 4º ESO y 1º Bachillerato.  
 [campus científicos de verano](http://www.campuscientificos.es)   
  
2015:  [convocatoria](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2015-1910)   

### Campus de Profundización Científica para estudiantes de Educación Secundaria Obligatoria
En 2014 es la V edición, se realiza en Soria para 80 alumnos de 4º ESO.  
 [campus-profundizacion-4-eso-2014.html](https://sede.educacion.gob.es/catalogo-tramites/becas-ayudas-subvenciones/extraescolares/campus-profundizacion-4-eso/campus-profundizacion-4-eso-2014.html)   
 [boe.es Resolución de 17 de marzo de 2014, de la Secretaría de Estado de Educación, Formación Profesional y Universidades, por la que se convocan ayudas para participar en el programa "Campus de Profundización Científica para estudiantes de Educación Secundaria Obligatoria" en Soria.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-4163)   
  
 [campus-profundizacion-cientifica-estudiantes-2016](http://www.educa.jcyl.es/es/programas/campus-profundizacion-cientifica-estudiantes-2016)   
  
