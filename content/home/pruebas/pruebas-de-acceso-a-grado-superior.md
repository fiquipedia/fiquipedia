# Pruebas de acceso a Grado Superior

Relacionado con [pruebas Acceso FP Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio) y con [pruebas Acceso Universidad](/home/pruebas/pruebasaccesouniversidad).

Información [todofp.es Pruebas de acceso a ciclos formativos de grado superior](https://www.todofp.es/pruebas-convalidaciones/pruebas/ciclos-grado-superior.html)
[todofp.es, Pruebas de acceso a ciclos formativos de grado superior, Modelos de exámenes de años anteriores ](https://www.todofp.es/pruebas-convalidaciones/pruebas/ciclos-grado-superior/modelos-examen-anteriores.html)

[Pruebas de acceso a ciclos formativos de Formación Profesional - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/pruebas-acceso-ciclos-formativos-formacion-profesional)  

[Pruebas de acceso a ciclos formativos de formación profesional, de artes plásticas y diseño y prueba sustitutiva del requisito académico de acceso a enseñanzas deportivas de régimen especial. - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/pruebas-acceso)  


##   Normativa y currículo Pruebas de Acceso Grado Superior
[Real Decreto 1147/2011, de 29 de julio, por el que se establece la ordenación general de la formación profesional del sistema educativo](https://www.boe.es/buscar/act.php?id=BOE-A-2011-13118)  

[DECRETO 187/2021, de 21 de julio, del Consejo de Gobierno, por el que se re-gulan las pruebas de acceso a ciclos formativos de formación profesional y a las enseñanzas profesionales de artes plásticas y diseño y la prueba sustitutiva de los requisitos académicos establecidos para el acceso a las enseñanzas deportivas de régimen especial y a las formaciones deportivas en período transitorio en la Comunidad de Madrid](http://www.bocm.es/boletin/CM_Orden_BOCM/2021/07/27/BOCM-20210727-1.PDF)  

[Normativa de FP: Pruebas de acceso a Ciclos Formativos - jccm.es](https://www.educa.jccm.es/es/fpclm/normativa-formacion-profesional/normativa-fp-pruebas-acceso-ciclos-formativos)  

##  Recursos externos con enunciados/soluciones

###  Andalucía
[juntadeandalucia.es Quiero información > Pruebas y procedimientos > Pruebas de acceso a ciclos formativos de Formación Profesional](https://www.juntadeandalucia.es/educacion/portals/web/formacion-profesional-andaluza/quiero-formarme/pruebas-y-procedimientos/pruebas-acceso)  

[juntadeandalucia.es     Temas > Estudiar >  Formación Profesional > Pruebas de acceso](https://www.juntadeandalucia.es/temas/estudiar/fp/pruebas-acceso.html)

[juntadeandalucia.es  Pruebas y procedimientos > Pruebas de acceso a ciclos > Ejercicios de años anteriores ](https://www.juntadeandalucia.es/educacion/portals/web/formacion-profesional-andaluza/quiero-formarme/pruebas-y-procedimientos/pruebas-acceso/ejercicios_anteriores) 

###  Aragón
 [aragon.es Pruebas de acceso a la enseñanzas de formación profesional](https://www.aragon.es/tramitador/-/tramite/participacion-pruebas-acceso-formacion-profesional) 

###  Asturias
 [Pruebas acceso ciclos formativos. GM-GS - educastur.es](https://www.educastur.es/estudiantes/fp/pruebas-acceso-gm-gs)   
 [Modelos de exámenes para pruebas de acceso a ciclos de FP de Grado Superior - educastur.es](https://www.educastur.es/-/modelos-de-examenes-para-pruebas-de-acceso-a-ciclos-de-fp-de-grado-superior)  

###  Canarias
 [gobiernodecanarias.org Pruebas de acceso > Acceso a los ciclos formativos de grado superior](https://www.gobiernodecanarias.org/educacion/web/formacion_profesional/acceso_ciclos_formativos/grado_superior/) 

###  Castilla La Mancha
[Pruebas de Acceso a Ciclos Formativos de Formación Profesional - Estructura, requisitos, modelos. › Grado Superior. Exámenes de años anteriores](https://www.educa.jccm.es/es/fpclm/estudios-formacion-profesional/pruebas-acceso-fp/pruebas-acceso-ciclos-formativos-formacion-profesional-estr/grado-superior/examenes-anos-anteriores) 

###  Madrid  
[sauce.pntic.mec.es Recursos en internet para orientación académica y profesional](http://sauce.pntic.mec.es/mbenit4/) © Manuel Benito Blanco  
[sauce.pntic.mec.es Formación profesional](http://sauce.pntic.mec.es/mbenit4/fprofesional.htm)  

Departamento de orientación Colegio Marqués del Vallejo, Valdemoro, Madrid  
Grado superior [orientacionjuncarejo.blogspot.com.es Exámenes Prueba Acceso Grado Superior COMUNIDAD de MADRID](http://orientacionjuncarejo.blogspot.com.es/p/examenes-prueba-acceso-grado-superior.html) 

###  Valencia
 [ceice.gva.es Pruebas de acceso a ciclos y cursos preparatorios Orientaciones Grado Superior](https://ceice.gva.es/es/web/formacion-profesional/orientaciones-grado-superior) 
 
