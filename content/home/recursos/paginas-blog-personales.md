---
aliases:
  - /home/recursos/pginas--blogs-personales/
---

# Páginas / blog personales / departamento / centro / comunidad ...

Inicialmente páginas personales, pero a veces las páginas no son realmente personales, sino de centro/departamento/comunidad ...  
Relacionado con  [Páginas ciencia / física / química / recopilatorios enlaces recursos](/home/recursos/recopilatorios-enlaces-recursos)   
También puede haber páginas que están enlazadas desde la  [página donde se centralizan apuntes](/home/recursos/apuntes)  o dentro de las páginas relativas a un curso en concreto o a un tema en concreto  

Puede haber algunas páginas que se puedan asociar por su contenido a [vídeos](/home/recursos/videos) en canales de youtube o de redes sociales 
  
Pendiente una ordenación aunque sea alfabética: la clasificación sería algo arbitraria, hay algunos que cubren muchas cosas al tiempo, distintos niveles de profundidad  

[FisQuiMat Recursos de Física, Química y Matemáticas](https://sites.google.com/site/smmfisicayquimica)   Moisés López Caeiro, cc-by-nc-nd
  
 [Profesor JRC. Recursos de Fsica y Qumica para ESO y Bachillerato](http://www.profesorjrc.es/)  Jorge Rojo Carrascosa, cc-by-nc-sa, recursos por niveles  
  
 [Recursos Miguel Quiroga](https://sites.google.com/view/eldelafisicaylaquimica/p%C3%A1gina-principal)  Miguel Quiroga Bóveda  
  
Educaplus.org  
 [Educaplus - Recursos educativos para la enseñanza de las ciencias](http://www.educaplus.org/)   
Sitio personal de Jesús Peñas Cano, profesor de Física y Química. En línea desde 1998 y su objetivo fundamental es compartir con todos, pero fundamentalmente con la comunidad educativa hispanohablante, los trabajos realizados por él para mejorar su práctica profesional como docente. Apartados de Matemáticas, Física, Química, Ed. Artística, Biología, Ciencias de la Tierra y Tecnología. Muchos materiales premiados en concursos. Dispone de un  [gestor de enlaces educativos](http://www.educaplus.org/directorio/index.php) . Para algunas funcionalidades necesita registro. En algunos puntos incluye publicidad.  
  
Física y Química en Internet  
 [fqdiazescalera.com](http://www.fqdiazescalera.com/)   
Materiales por curso (ejercicios, recursos en internet, selectividad), blog de experimentos y de laboratorio,   
  
Rafael Cabrera  
 [http://serendiphia.es/](http://serendiphia.es/) Canal de Física y Química. Recursos por nivel desde 2 ESO a 2 Bachillerato.  
  
 [http://www.disfrutalaciencia.es/](http://www.disfrutalaciencia.es/)   
Página personal Cayetano Gutiérrez Pérez, Catedrático de Física y Química y Divulgador Científico.  
Experimentos con canal de vídeo en youtube.  
  
Aprendiendo ciencias  
 [http://aprendiendociencias.wordpress.com/](http://aprendiendociencias.wordpress.com/)   
Materiales por curso, y para cada curso por bloques de contenidos.  
  
ENSEÑANZA DE LA FÍSICA Y LA QUÍMICA. EDUCACIÓN SECUNDARIA. GRUPO HEUREMA  
 [Heurema](http://heurema.com/)   
Incluye apuntes, prácticas.... Jaime Solá de los Santos, José Luis Hernández Pérez y Ricardo Fernández Cruz   
  
 [La web de Chema Martín | Materiales para clase, noticias científicas&#8230;](https://chemamartin.wordpress.com/) Se comenta en  [Recursos didácticos para la asignatura de Física | EDUCACIÓN 3.0](https://www.educaciontrespuntocero.com/recursos/recursos-para-fisica/95530.html) Cada año, este docente cuelga en su página web el temario de las asignaturas de Física de 2º de Bachillerato, Química de 2º de Bachillerato y Física y Química de 4º de ESO al ritmo al que avanza el curso. En cada una de las secciones, divide el contenido por bloques o temas en los que incluye ejercicios y apuntes. Además, recopila en una pestaña toda la información de cursos anteriores para aquellos que quieran repasar lo aprendido durante el año académico anterior.  
Rodrigo Alcaraz de la Osa  
 [FisiQuímicamente](https://rodrigoalcarazdelaosa.me)   
  
Física y Química en el Vizcaíno, Rafael Ruiz Guerrero. Presentaciones por curso y tema  
 [Física y Química en el Vizcaíno](http://fqrdv.blogspot.com.es/)  [Rafael Ruiz Guerrero presentations | SlideShare](https://es.slideshare.net/rafaruizguerrero)   
 
[BERT JANSSEN'S DOCENCIA. Docente en la Universidad de Granada sobre relatividad. Charlas.](https://ugr.es/~bjanssen/docencia.html#charlas)  
 
 
[IES Albero. Departamento Física y Química](https://www.iesalbero.es/department/fisica-y-quimica/)  
Ricardo Sánchez Montalvo, Jefe de Departamento. Materiales por curso desde 2º ESO hasta 2º Bachillerato  

[eboixader.com](http://eboixader.com)   
Ernesto Boixader Gil  
- La Clase DiNvertida: 30 años de docencia dan para esto, definir la metodología que mejor se adapta a mi persona
- Los libros digitales que utilizo en mis clases de Biología y Geología / Física y Química. Incluyendo la programación y la propuesta didáctica
- Mis recursos: incluyo los recursos que he creado y otros que utilizo de compañer@s del #ClaustroVirtual: Breakouts digitales, Frikiexamenes (pruebas escritas), Laboratorios Virtuales, Herramientas, Prácticas/juegos de laboratorio o de campo  y digitales, tableros de juego
- Blog: cuando quiero hablar conmigo mismo suelo escribir aquí, aunque reconozco que me cuesta bastante
- Webs de referencia: enlace a webs de compañeros/as del #claustrovirtual que comparten sus materiales, ideas, reflexiones, etc
- Contacto: si necesitas contactar conmigo para solicitar soluciones, recursos, invitarme a una cerveza, ... Es gratis

[MAINQUIFI](https://mainquifi.blogspot.com)  
> MAINQUIFI es un blog con vídeos para aprender MAtemáticas, INglés, QUÍmica y FÍsica de ESO y Bachillerato. Si no entiendes algún tema, búscalo aquí y mírate el vídeo. No te aseguramos que todo lo que aquí se explica esté bien (decide tú si te fías de los vídeos jajajaja). Trataremos de ir rectificando los fallos que nos vaya comentando la gente.  
Twitter [@MainquifiM](https://twitter.com/MainquifiM)  
  
FISICA II  
 [FISICA II](http://blogdefisicall.blogspot.com.es/)   
 Unidades por bloques. Raul Calvario Ramos. Licenciamiento no detallado.  
  
Fisilandia. Ayuda en física. Un lugar dedicado a la enseñanza de la física en secundaria  
 [FISILANDIA · Algo de física y algo de diversión](http://www.fisilandia.es/index.php/home)   
Apuntes, exámenes, artículos. Autor Gabriel Villalobos con licenciamiento cc-by-nc-nd  
  
Portal de apoyo a la Formación del Profesorado del CEP de la Sierra de Cádiz / Villamartín.  
 [Recursos](http://www.omerique.net/twiki/bin/view/Recursos)   
 [MateriaFQ](http://www.omerique.net/twiki/bin/view/Recursos/MateriaFQ)   
Recursos en forma de wiki, con categorías (en 2012 pocos asociados a Física y Química). O.M.E.R.I.Q.U.E. puede significar Organización de Materiales Educativos y Recursos Informáticos Que se Utilizan en la Enseñanza  
  
Recursos de física, química, ... por Vicente Martín Morales  
 [http://perso.wanadoo.es/vicmarmor/](http://perso.wanadoo.es/vicmarmor/)   
Autor Vicente Martín Morales. Departamento de Física y Química I.E.S. Vallecas Magerit. Incluye problemas ESO y Bachillerato por bloques, con algunos de PAU. En 2012 la última actualización es de 2004.  
 [http://ies.magerit.madrid.educa.madrid.org/fyqiesvm/](http://ies.magerit.madrid.educa.madrid.org/fyqiesvm/)   
 [http://www.fisicayquimica.hol.es/](http://www.fisicayquimica.hol.es/)   
Vicente Martín Morales. En 2014 actualizado, incluye por ejemplo PAU modelo física 2014.  
Incluye generador de problemas con geogebra!  
  
[ARTEAGA BACHILLERATO: FÍSICA 2º, QUÍMICA 2º Y FYQ 1º](https://arteagafisica.wordpress.com/)  
Blog de Carlos Martín Arteaga para trabajar la Física y la Química de 2º de Bachillerato y la FyQ de 1º  
  
  
 [http://www.thespectrumofriemannium.com/](http://www.thespectrumofriemannium.com/) Physmatics and the Polyverse in a nutshell! Mobilis in mobili!  
Blog en inglés, física y matemáticas, nivel alto  
El autor se identifica como "∂³Σx² = Amarashiki"  
Algunas categorías indicadas en la cabecera del blog: LOGS (entradas en el blog), Physmatics, Chemistry, Relativity, Information theory, Science, Quantum, Mathematics, Neutrinology, H.E.P....Un ejemplo curioso y asequible  
[http://www.thespectrumofriemannium.com/2015/04/13/log170-the-shortest-papers-ever-the-list/](http://www.thespectrumofriemannium.com/2015/04/13/log170-the-shortest-papers-ever-the-list/)   
 [http://www.thespectrumofriemannium.com/teaching/](http://www.thespectrumofriemannium.com/teaching/)  
 [http://www.thespectrumofriemannium.com/teaching/2bach/2bachnewteentochophysics/](http://www.thespectrumofriemannium.com/teaching/2bach/2bachnewteentochophysics/)   
  
WikiFis: Wiki educativa para alumnos de física  
 [PROYECTO+WIKIFIS](http://fisica2spp.wikispaces.com/PROYECTO+WIKIFIS)   
Centrado en física 2º Bachilllerato. Crescencio Pascual Sanz y uan José Serrano Pérez, Licenciamiento cc-by-nc-sa  
  
FisicaNet  
 [http://www.fisicanet.com.ar/](http://www.fisicanet.com.ar/)   
Matemática, física, química, biología y tecnología, apuntes, ejercicios y monografías. Copyright © 2007-2016 Fisicanet ® Todos los derechos reservados. (sí, en 2012 tiene copyright hasta el 2016 :-)  
  
Física y Química  
 [Física y Química](http://sites.google.com/site/javirobledanofq/Home)   
Página vista en junio 2012, proyecto similar a esta fiquipedia de Javier Robledano Arillo, Profesor de Secundaria. Licenciamiento no detallado, materiales por curso, incluye exámenes PAU  
En 2018 crea nueva web  [ Clasefyqrobledano | EducaMadrid ](https://www.educa2.madrid.org/web/clasefyq)   
  
  
Física y Química para todos  
 [ Física y Química para todos | EducaMadrid ](http://www.educa2.madrid.org/web/cesar.arenas/)   
César Arenas López. Incluye apuntes, ejercicios  
  
Física y Química  
 [jpcampillo.es](http://www.jpcampillo.es/)  (enlace validado julio 2015)  
Apuntes, problemas, PAU, simluaciones, ...cc-by-nc  
Juan P. Campillo  
  
Web de Julián Moreno Mestre   
 [jpcampillo.es](http://www.jpcampillo.es/)  (enlace validado agosto 2015)  
Matemáticas, Física, Química, apuntes y ejercicios carrera termodinámica  
Licenciamiento no detallado  
  
RECURSOS PARA EL PROFESORADO DE FÍSICA Y QUÍMICA Batiburrillo de todo para la materia  
 [Recursos para el profesorado de Física y Química](http://recursosfyq.blogspot.com.es/)   
 Aránzazu González Mármol  
  
 [http://www.aprendefisicayquimica.com/](http://www.aprendefisicayquimica.com/)   
Aránzazu González Mármol, IES Orden de Santiago, Horcajo de Santiago, Cuenca  
  
Materiales para el aula con licencia de uso libre.   
 [http://fqlibre.blogspot.com.es/](http://fqlibre.blogspot.com.es/)   
Licenciamiento cc-by-nc-sa, se incluyen materiales con su propio licenciamiento (y en algunos no indicado)  
Autores: Manuel Díaz Escalera  [fqmanuel](https://twitter.com/fqmanuel)  y José Luis Cebollada Gracia  [jlcebollada](https://twitter.com/jlcebollada)   
Información del proyecto  [Sobre el proyecto ~ FQ Libre](http://fqlibre.blogspot.com.es/p/blog-page.html)   
"Queremos ofrecer contenidos para todas las asignaturas de FQ, de manera que el profesor que lo desee pueda crear su propio libro de texto o sus hojas de trabajo sin más que recortar, pegar y citar al autor. Todo bajo licencia Creative Commons."  
  
  
Recursos física y química  
 [http://graviton.blogspot.com.es/](http://graviton.blogspot.com.es/)   
José Ramón Ramos. cc-by-nc-sa, aunque tiene pdf con apuntes y ejercicios que indican © José Ramón Ramos  
  
Actividades de física,química,biología y geología  
 [Amhuertas &#8211; Actividades,ejercicios y presentación de temas de física y química](http://amhuertas.wordpress.com/)   
Antonio de la Muñoza de Huertas.   
  
Blog de física, dibujo técnico y tecnología  
 [http://pacofisica.blogspot.com.es/](http://pacofisica.blogspot.com.es/)   
Francisco Pérez del Pozo. Profesor física dibujo técnico y tecnología  
  
 [http://fqcolindres.blogspot.com.es/](http://fqcolindres.blogspot.com.es/)   
Enlaces y complementos didácticos para el aula de Física y Química. cc-by-nc-sa  
  
 [6con02 - Física y Química - ESO, Bachillerato](http://www.6con02.com/)   
Esta Web está destinada a mis alumnos de E.S.O. y Bachillerato, como apoyo y ayuda en su aprendizaje de la Física y la Química. En este sitio encontraréis muchos de los contenidos teóricos vistos en clase, además de ejercicios, problemas y enlaces interesantes de Física y Química.  
Todos los nombres propios de libros, programas, sistemas y demás material didáctico y tecnológico que aquí se mencionan son marcas registradas y pertenecen a sus respectivas compañías, organizaciones y autores. Aparecen en este website solo con fines educativos.  
Mª Jesús D. A., cc-by-nc-sa  
  
Departamento física y química IES Valle del Arlanza, Lerma, Burgos.   
 [print.cgi](http://iesvalledelarlanza.centros.educa.jcyl.es/sitio/print.cgi?wid_seccion=14&wid_item=44&wOut=print)   
En 2015 apuntes y ejercicios física 2º bachilerato y química 2º bachillerato  
  
Departamento Ciencias Naturales IES Suel, Fuengirola, Málaga  
 [http://www.juntadeandalucia.es/averroes/~29701428/ccnn/](http://www.juntadeandalucia.es/averroes/~29701428/ccnn/)   
Incluye "Actividades interactivas" y "Material descargable" por cursos.  
  
FISICA y MATEMATICAS JM (Juntos Mejor)   
 [http://fisicajm.blogspot.com.es/](http://fisicajm.blogspot.com.es/)   
Roberto del Rey Granell & Marta Gutiérrez Pérez.  
  
 [http://salvadorhurtado.wikispaces.com/](http://salvadorhurtado.wikispaces.com/)   
Una wiki para compartir recursos para la enseñanza de las ciencias usando las "nuevas tecnologías"  
Una wiki creada por Salvador Hurtado Fernández, profesor de física y química en el I.E.S. Aguilar y Cano de Estepa (Sevilla  
Puede ver una aplicación del material de esta wiki en  [Laboratorio Virtual](http://labovirtual.blogspot.com)   
Usted es libre de distribuir,bajo licencia C.C., el contenido de esta wiki  
  
Fisquimed, todo para tu ciencia.  
 [fisquimed | ¡Todo para tu ciencia!](https://fisquimed.wordpress.com/)   
Recursos por nivel  
Blog Esteban Calvo Marín  
  
Eureka, ciencia más allá de la tiza    
 [http://blog.educastur.es/eureka/](http://blog.educastur.es/eureka/) Recursos por nivel  
Blog María Inmaculada López Fernández, profesora de Secundaria en el Principado de Asturias  

[fiquillados - Francisco Collados](https://franciscojuancolla.wixsite.com/fiquillados/)  
  
Innobator, Física y Química  
 [INNOBATOR  -  FÍSICA Y QUÍMICA 2020-2021](http://innobator.blogspot.com.es/)   
Este Blog pretende ser un complemento las clases que se imparten en aula. En él podéis publicar vuestros comentarios, dudas o sugerencias. A medida que el curso avance se irán publicando en él distintos materiales aportados por el profesor (ejercicios, exámenes, trabajos, fotos, prácticas, ...) o por los alumnos.  
POR MIGUEL, PROFESOR DEL DPTO. DE FÍSICA Y QUÍMICA DEL IES SAENZ DE BURUAGA.   
  
 [Fisica y Quimica IES MIGUEL CATALAN de Coslada](http://fiquimiguelcatalan.blogspot.com.es/)   
Centrado en 1º Bachillerato. IES Miguel Catalán de Coslada  
  
Aula de física y química  
 [Aula de Física y Química: Índice](http://fisica-quimica.blogspot.com.es/p/indice.html)   
Estáis en el lugar adecuado para aprender, enseñar o curiosear estas materias. Este blog incluye todo tipo de enlaces (animaciones, software, prácticas, webquest,vídeos, imágenes, diagramas...) a CONTENIDOS concretos de la materia que se puedan llevar al aula para mejorar la comprensión y la motivación hacia estas materias. Desde aquí podréis acceder a la RED DE BLOGS sobre esta materia.  
  
 [Todo es Física y Química](http://todoesfisicayquimica.blogspot.com.es/) Sara Alonso  
  
 [fisica-2-bachillerato.html](http://jvciesplayamar.blogspot.com.es/p/fisica-2-bachillerato.html) Blog de Física y Química. José Vegas Cano  
Apuntes, ejercicios  
  
 [Aprobar matemáticas profesor10](http://profesor10demates.blogspot.com.es/2013/10/fisica-general.html) APROBAR MATEMÁTICAS El blog del profesor10 para aprobar las matemáticas , física y química de secundaria 3º ,4º de ESO , 1º y 2º de bachillerato , selectividad , UNED y la universidad  
  
 [http://fqtablada.blogspot.com.es/](http://fqtablada.blogspot.com.es/) LOLA GUTIÉRREZ VILLARÁN PROFESORA DE FÍSICA Y QUÍMICA IES CARLOS HAYA. SEVILLA  
EN MI AFÁN POR MEJORAR LA COMPRENSIÓN DE CIERTOS CONTENIDOS EN EL AULA RELACIONADOS CON LA MATERIA DE FÍSICA Y QUÍMICA, PONGO A VUESTRO ALCANCE ESTE BLOG, QUE EN LA ACTUALIDAD ESTÁ EMPEZANDO COMO UNA FORMA FÁCIL DE ACCEDER A PRESENTACIONES power point, IMÁGENES, VIDEOS, PÁGINAS WEBS,..Y QUE CON EL TIEMPO SERÁ UN BANCO DE RECURSOS  
  
 [Física y Química. ESO y Bachillerato](http://www.fisicayquimicasegundocicloeso.blogspot.com.es/)   
Blog de Pedro Bejarano.  
  
 [http://fisicayquimica.com/](http://fisicayquimica.com/)   
Tu web de ciencias  
Juanjo Florido, actualmente profesor de Física y Química en el I.E.S. Alonso Quesada (Las Palmas de Gran Canaria, SPAIN)  
  
 [Física Divertida](http://depfisicayquimica.blogspot.com.es/)   
DEPARTAMENTO DE FÍSICA Y QUÍMICA DEL IES "Antonio Mª Calero" de Pozoblanco (CÓRDOBA) ESPAÑA  
BLOG DE FÍSICA Y QUÍMICA DIVERTIDAS  
  
 [http://cbasefis4eso.blogspot.com.es/](http://cbasefis4eso.blogspot.com.es/) Blog de trabajo de la asignatura de Física y Química de 4º ESO. COLEGIO BASE.  
  
Química y algo más.Un blog sobre química y otras ciencias  
 [Química y algo más – Un blog sobre química y otras ciencias](http://www.quimicayalgomas.com/)   
Patricio. ©Quimica y algo mas 2011. Todos los derechos reservados  
  
Química Orgánica. Blog creado para difundir artículos de química orgánica  
 [Química Orgánica](http://quimicaorganicaqu.blogspot.com.es/)   
  
FÍSICA EN EL RIHONDO  
"Estás en una página de carácter educativo y en construcción.  
Aquí poco a poco encontrarás apuntes y ejercicios de Física y Química para ESO."   
 [Fisica Rihondo. Ejercicios Física. Apuntes Física - www.fisicarihondo.jimdo.com](http://fisicarihondo.jimdo.com/)   
También bachillerato y matemáticas  
  
 [http://joseignaciomelero.blogspot.com.es/](http://joseignaciomelero.blogspot.com.es/)  [Física y Química 3ºESO](http://e3fyq.blogspot.com.es)   
José Ignacio Melero. Apuntes, actividades, vídeos ...  [about](https://www.youtube.com/user/melerojosei/about)   
  
Materiales de trabajo de Física y Química por (by)Nicolas Elortegui, cc-by-nc-sa  
 [Ciencia de cada da](http://www.elortegui.org/ciencia/datos/trabajo.html)   
  
Sitio Web de Héctor E. Medellín Anaya, cursos de física universitarios con recursos  
 [Cursos](http://galia.fc.uaslp.mx/~medellin/cursos.htm)   
  
Aula de ciencias, José Manuel Aranda Pérez, IES Baelo Claudia   
 [Aula de Ciencias](https://sites.google.com/site/arandajm/)   
  
Física y Química, IES La Sisla [Inicio](http://quifi.es/)   
  
  
Joaquín Recio Miñarro, Químicaweb  
 Profesor de Física y Química del I.E.S. Itaba (Teba - Málaga - España)  
 [http://www.quimicaweb.net/](http://www.quimicaweb.net/)   
  
  
Bernardo Herradón García  
 [http://www.losavancesdelaquimica.com/](http://www.losavancesdelaquimica.com/)   
 [Los Avances de la Química. Educación Científica (y algo de Historia)](https://educacionquimica.wordpress.com/)   
  
The Wonders of Physics  [The Wonders of Physics](http://sprott.physics.wisc.edu/wop.htm)  de Julien Clinton Sprott  [Julien Clinton Sprott](http://sprott.physics.wisc.edu/sprott.htm)   
 Academia donde se comparten los apuntes sin derechos de autor que llevan los alumnos, asociado a nivel universitario  
 [Recursos Academia apuntes exmenes ejercicios resueltos ejemplos](http://www.cartagena99.com/recursos/recursos_alumnos.php)   
  
 [Física y química](http://scienceisanadventure.blogspot.com.es/search/label/F%C3%ADsica%20y%20qu%C3%ADmica)   
  
Leticia Cabezas Bermejo [lacienciafriki](https://sites.google.com/site/lacienciafriki)   
  
Discover Sensors documents with possible links to Junior Cycle Specification  
Michael Kavanagh @mikekav66  
 [Discover Sensors documents  with possible links to Junior Cycle Specification](https://padlet.com/collabscience/q08w0lkhzhjj)   
Web personal de Física y QuímicaJosé Moreno Hueso  
 [fisicayquimica.eu: Home](https://www.fisicayquimica.eu/) Incluye libros  
 
