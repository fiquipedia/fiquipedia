---
aliases:
  - /home/recursos/quimica/recursos-disoluciones/
---

# Recursos disoluciones

Las disoluciones son algo que se suelen introducir  [Física y Química de 3º ESO](/home/materias/eso/fisica-y-quimica-3-eso)  al trabajar mezclas, y es un concepto básico que se utiliza en cursos posteriores.  
Se suele introducir inicialmente g/L, y añadiendo %masa, %volumen, y molaridad cuando se conoce ya el concepto de mol.  
La molalidad se suele asociar a  [propiedades coligativas](/home/recursos/quimica/recursos-propiedades-coligativas).  
Asociado a disoluciones se ve también la solubilidad.  
Ver también  [recursos concentración](/home/recursos/quimica/concentracion)  
Las disoluciones se suelen trabajar en  [prácticas de laboratorio](/home/recursos/practicas-experimentos-laboratorio).  
En pruebas  [libres de graduado en ESO](/home/pruebas/pruebas-libres-graduado-eso)  (bloque de mezclas, elementos y compuestos) y en  [pruebas de Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio)  (bloque energía y materia)  

[fisicanet.com.ar Apunte teórico AP02 Disolución y solubilidad](http://www.fisicanet.com.ar/quimica/soluciones/ap02_disoluciones_solubilidad.php)   

GRUPO HEUREMA. EDUCACIÓN SECUNDARIAENSEÑANZA DE LA FÍSICA Y LA QUÍMICA  
Subsección: Apuntes de Química en Bachillerato (16-19 años)  
[http://www.heurema.com/ApQBachill2.htm](http://www.heurema.com/ApQBachill2.htm)  
2.INTRODUCCIÓN  
2 Disoluciones. Medidas de la concentración. Determinación de fórmulas Problemas de disoluciones y estequiometría  
OBSERVACIÓN: Las introducciones corresponden a conocimientos de 1º de bachillerato que el alumno debe tener al iniciar el curso de 2º


How Water Dissolves Salt - Canadian Museum of Nature  
[![](https://img.youtube.com/vi/xdedxfhcpWo/0.jpg)](https://www.youtube.com/watch?v=xdedxfhcpWo "How Water Dissolves Salt - Canadian Museum of Nature")


##   Ejercicios
 [profesor10demates.blogspot.com.es Disoluciones ejercicios y problemas resueltos](http://profesor10demates.blogspot.com.es/2013/04/disoluciones-ejercicios-y-problemas_28.html)  

 [http://ejercicios-fyq.com/?-Disoluciones,20-](http://ejercicios-fyq.com/?-Disoluciones,20-)  
 
 [fisquiweb.es Disoluciones](http://fisquiweb.es/Apuntes/Apuntes1Bach/Disoluciones.pdf) Apuntes con ejercicios resueltos, e incluye hoja final con ejercicios y soluciones (algunos asociados a propiedades coligativas)  
 Un ejercicio resuelto de determinación fórmulas al final de  [fisquiweb.es Conceptos básicos de Química](http://fisquiweb.es/Apuntes/Apuntes1Bach/InicialesQuimica.pdf)  
 
 Ejercicios determinación de fórmulas, empírica y molecular (pendiente mover a página aparte)  
 [http://www.heurema.com/ApQBachill2.htm](http://www.heurema.com/ApQBachill2.htm)  
 
 [profesor10demates.com fórmula empírica y molecular](https://www.profesor10demates.com/2013/12/formula-empirica-y-molecular.html)  
 
 Aplicación útil (permite poner una fórmula y da porcentajes en masa) [http://www.chemcalc.org/](http://www.chemcalc.org/)  
 
3. Disoluciones 
* [Relaciones estequiométricas](http://www.heurema.com/ApuntesFQ/AQuimica/Introduccion2/RelacionesEsteq.pdf). Después del ajuste de las reacciones, se establecerán las relaciones masa volumen entre productos y reaccionantes  
* [RelaciConcent](http://www.heurema.com/ApuntesFQ/AQuimica/Introduccion2/RelacConcent.pdf). Conceptos necesarios para el manejo de las disoluciones. Medidas de la concentración  
* [Disoluciones 1](http://www.heurema.com/ApuntesFQ/AQuimica/Introduccion2/Disoluciones1.pdf). Problemas de disoluciones  
* [DisolucionesRQ](http://www.heurema.com/ApuntesFQ/AQuimica/Introduccion2/DISOLUCIONESPS.pdf). Problemas de disoluciones y reacciones químicas resueltos  
* Determinación de Fórmulas 
[heurema.com Determinación de fórmulas 1](http://www.heurema.com/TestQ16N.htm)  
[heurema.com Determinación de fórmulas 1 (soluciones)](http://www.heurema.com/TestQ/TestQ16-DetForm1/TQ12-DetFormIS.pdf)  
[heurema.com Determinación de fórmulas 2](http://www.heurema.com/TestQ17N.htm)  
[heurema.com Determinación de fórmulas 2 (soluciones)](http://www.heurema.com/TestQ/TestQ17-DetForm2/TQ12-DetForm2S.pdf)  
[heurema.com Determinación de fórmulas 3](http://www.heurema.com/TestQ18N.htm)
[heurema.com Determinación de fórmulas 3 (soluciones)](http://www.heurema.com/TestQ/TestQ18-DetForm3/TQ12-DetForm3S.pdf) 

## [Homeopatía](/home/recursos/quimica/homeopatia)  

##   Simulaciones
[![Sales y solubilidad](https://phet.colorado.edu/sims/soluble-salts/soluble-salts-600.png) "Sales y solubilidad"](https://phet.colorado.edu/es/simulation/soluble-salts)  (java vía cheerpj)  
 
 [![Concentración](https://phet.colorado.edu/sims/html/concentration/latest/concentration-600.png) "Concentración"](https://phet.colorado.edu/sims/html/concentration/latest/concentration_es.html)  (HTML5)  
 
 [![Molaridad](https://phet.colorado.edu/sims/html/molarity/latest/molarity-600.png) "Molaridad"](https://phet.colorado.edu/es/simulation/molarity)  (HTML5)  
 
 [![Soluciones de sal y azúcar](https://phet.colorado.edu/sims/sugar-and-salt-solutions/sugar-and-salt-solutions-600.png "Soluciones de sal y azúcar")](https://phet.colorado.edu/es/simulation/legacy/sugar-and-salt-solutions)  (java vía cheerpj)

##   Referencias solubilidad
Relacionado con [equliibrio químico](/home/recursos/quimica/equilibrio-quimico)
 
[chemwiki.ucdavis.edu E3. Solubility Constants for Compounds at 25°C](https://chem.libretexts.org/Ancillary_Materials/Reference/Reference_Tables/Equilibrium_Constants/E3._Solubility_Constants_for_Compounds_at_25C)  

[Solubility table - en.wikipedia.org](https://en.wikipedia.org/wiki/Solubility_table)  

[kentchemistry.com Solubility Curves](http://www.kentchemistry.com/links/Kinetics/SolubilityCurves.htm)   

[Solubility Curve  - chemistry.tutorvista.com (waybackmachine)](http://web.archive.org/web/20180126122444/http://chemistry.tutorvista.com/inorganic-chemistry/solubility-curve.html) 

## Preparación de disoluciones
Enlaza con laboratorio

[![COMO PREPARAR CORRECTAMENTE UNA DISOLUCIÓN | Técnicas de laboratorio](https://img.youtube.com/vi/BowV116ddIQ/0.jpg)](https://www.youtube.com/watch?v=BowV116ddIQ "COMO PREPARAR CORRECTAMENTE UNA DISOLUCIÓN | Técnicas de laboratorio")  Breaking Vlad


##   Referencias presión de vapor
 [laplace.us.es Propiedades del agua, Equilibrio agua-vapor](http://laplace.us.es/wiki/index.php/Propiedades_del_agua#Equilibrio_agua-vapor)  
 
[vaxasoftware.com Presión de vapor de agua líquida y hielo a varias temperaturas](http://www.vaxasoftware.com/doc_edu/qui/pvh2o.pdf)  
 
 
Los ficheros estaban inicialmente se veían anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia quimica/recursos-disoluciones](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/quimica/recursos-disoluciones)   
