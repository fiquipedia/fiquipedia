---
aliases:
  - /home/pruebas-libres-graduado-eso/normativa-y-curriculo-pruebas-libres-graduado-eso/
---

# Normativa y currículo pruebas libres graduado ESO

##  Normativa
[Orden 11997/2012, de 21 de diciembre, por la que se regulan las pruebas libres para la obtención del título de Graduado en Educación Secundaria Obligatoria destinadas a personas mayores de dieciocho años en la Comunidad de Madrid.](http://www.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=8045) derogada  
  
[Orden 1255/2017, de 21 de abril, de la Consejería de Educación, Juventud y Deporte, por la que se establece la organización de las enseñanzas para la obtención del título de Graduado en Educación Secundaria Obligatoria por personas adultas en la Comunidad de Madrid.](http://www.madrid.org/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=9770)  
  
[ORDEN 3888/2008, de 31 de julio, por la que se establece la organización de las enseñanzas para la obtención del título de Graduado en Educación Secundaria Obligatoria por personas adultas.](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=5216&cdestado=P) derogada   

Convocatoria curso 2013-2014  [http://www.bocm.es/boletin/CM_Orden_BOCM/2013/12/23/BOCM-20131223-10.PDF](http://www.bocm.es/boletin/CM_Orden_BOCM/2013/12/23/BOCM-20131223-10.PDF)  

## Información Madrid
[comunidad.madrid Pruebas libres para obtener el Título de Graduado en Educación Secundaria Obligatoria](https://www.comunidad.madrid/servicios/educacion/pruebas-obtencion-titulo-eso-mayores-18-anos)

##  Currículo Ámbito Científico-Tecnológico 

**Por revisar, está el de la normativa de 2008 derogada**

###  Introducción
El currículo del ámbito científico-tecnológico se ha realizado desarrollando los aspectos básicos de los currículos de las materias que lo conforman: Matemáticas, Ciencias de la Naturaleza y Tecnologías, recogidos en el Anexo del Decreto 23/2007, de 10 de mayo, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo de la Educación Secundaria Obligatoria, con una particular incidencia en aquellos de carácter instrumental.  
El presente currículo pretende facilitar que las personas adultas puedan adquirir la formación básica suficiente que les permita alcanzar los objetivos y las competencias básicas de la etapa, especialmente aquellos ligados a la adquisición de conocimientos científicos y tecnológicos. Todo ello con la finalidad de que obtengan el título de graduado en Educación Secundaria Obligatoria, lo que les facilitará, a su vez, su integración en la vida activa y en la sociedad.  
La metodología a seguir deberá adaptarse a cada grupo, rentabilizando al máximo los recursos disponibles. El aprendizaje debe plantearse de forma esencialmente práctica, integrando los procedimientos metodológicos de cada una de las materias mediante la aplicación de conceptos e instrumentos matemáticos en las Ciencias de la Naturaleza y en las Tecnologías.  
El trabajo en grupo de estos alumnos, ante situaciones y problemas que estimulen la curiosidad y la reflexión, les facilitará el desarrollo de hábitos de trabajo que les permitirán defender sus argumentos frente a los de sus compañeros, comparar distintos criterios y seleccionar la respuesta más adecuada.  
Es el profesor el que, teniendo siempre en cuenta las características de las personas adultas y mediante la programación de aula, deberá dar forma a los contenidos y objetivos propuestos en el presente currículo para su desarrollo en clase.

###  Objetivos
La enseñanza del ámbito científico-tecnológico en la Educación Secundaria Obligatoria para personas adultas tendrá como finalidad el desarrollo de las siguientes capacidades:  
1. Incorporar al lenguaje y a los modos de argumentación habituales las formas elementales de expresión científico-matemática con el fin de comunicarse de manera clara, concisa y precisa.  
2. Utilizar técnicas sencillas y autónomas de recogida de datos, familiarizándose con las que proporcionan las tecnologías de la información y la comunicación, sobre fenómenos y situaciones de carácter científico y tecnológico.  
3. Participar en la realización de actividades científicas y en la resolución de problemas sencillos.  
4. Utilizar los conocimientos adquiridos sobre las Ciencias de la Naturaleza para comprender y analizar el medio físico que nos rodea.  
5. Adquirir conocimientos sobre el funcionamiento del organismo humano para desarrollar y afianzar hábitos de cuidado y salud corporal.  
6. Aplicar con soltura y adecuadamente las herramientas matemáticas adquiridas a situaciones de la vida diaria.  
7. Utilizar procedimientos de medida y realizar el análisis de los datos obtenidos mediante el uso de distintas clases de números y la selección de los cálculos apropiados.  
8. Identificar las formas planas o espaciales que se presentan en la vida diaria y analizar las propiedades y relaciones geométricas entre ellas.  
9. Utilizar de forma adecuada los distintos medios tecnológicos (calculadoras, ordenadores, etcétera) tanto para realizar cálculos como para tratar y representar informaciones de índole diversa.  
10. Disponer de destrezas técnicas y conocimientos básicos para el análisis, diseño, elaboración y manipulación de forma segura y precisa de materiales, objetos y sistemas tecnológicos.  
11. Conocer y valorar las interacciones de la ciencia y la tecnología con la sociedad y el medio ambiente, incidiendo en la necesidad de búsqueda y aplicación de soluciones a los problemas a los que se enfrenta actualmente la humanidad.  
12. Reconocer y valorar las aportaciones de la ciencia y la tecnología para la mejora de las condiciones de vida de los seres humanos.  
13. Potenciar como valores positivos el esfuerzo personal y la autoestima en el propio proceso de aprendizaje.

##  MÓDULO DE CIENCIAS DE LA NATURALEZA I


###  Contenidos


####  Bloque 1. Contenidos comunes
- Familiarización con las características básicas del trabajo científico por medio de: Planteamiento de problemas, discusión de su interés, formulación de conjeturas, experimentación, etcétera, para comprender mejor los fenómenos naturales y resolver los problemas que su estudio plantea.  
- Utilización de los medios de comunicación y las tecnologías de la información para seleccionar información sobre el medio natural.- Interpretación de datos e información sobre la naturaleza y utilización de dicha información para conocerla.  
- Reconocimiento del papel del conocimiento científico en el desarrollo tecnológico y en la vida de las personas.  
- Utilización cuidadosa de los materiales e instrumentos básicos de un laboratorio y respeto por las normas de seguridad en el mismo.

####  Bloque 2. La Tierra en el Universo
- El Universo y el sistema solar.  
- Diferentes concepciones históricas sobre el Universo.  
- Características físicas de la Tierra. Los movimientos de la Tierra.  
- Las capas de la Tierra.

####  Bloque 3. La materia y los materiales terrestre
3.1. La materia:  
- Propiedades generales: Dimensiones, masa y densidad. Unidades del Sistema Internacional.  
- Estados en los que se presenta la materia.- Unidad y diversidad de la materia.  
3.2. La atmósfera:  
- La atmósfera: Composición y propiedades.  
- Fenómenos atmosféricos.  
- Variaciones en la composición del aire. Contaminación.  
- Implicaciones medioambientales.  
3.3. La hidrosfera:  
- El agua en la Tierra (origen, abundancia e importancia) y en otros planetas.  
- Propiedades del agua.  
- El agua de mar como disolución.  
- El ciclo del agua.  
- La contaminación del agua.  
- El agua y la salud.  
- Implicaciones medioambientales.  
3.4. La geosfera:  
- Estructura interna de la Tierra.  
- La corteza terrestre: superficie, composición química y elementos geoquímicos.  
- Elementos bioquímicos.  
- Los minerales y las rocas.

####  Bloque 4. Los seres vivos y su diversidad
- Factores que hacen posible la vida en la Tierra.  
- Los elementos bioquímicos. El carbono.  
- Características y funciones de los seres vivos. Su diversidad.  
- Clasificación de los seres vivos.

####  Bloque 5. Materia y energía
5.1. Los sistemas materiales y la energía:  
- La energía como propiedad de los sistemas materiales.  
- Variación de la energía en los sistemas naturales.  
- Tipos y fuentes de energía.  
5.2. Calor y temperatura:  
- Calor y temperatura: Interpretación del calor como forma de transferencia de energía.  
- Distinción entre calor y temperatura. Los termómetros.  
- El calor como agente productor de cambios.  
- Propagación del calor: Aislantes y conductores.  
5.3. Luz y sonido:  
- Las ondas como una forma de propagación de la energía.  
- La luz y el sonido como dos tipos diferentes de ondas.  
- Luz y visión: Los objetos como fuentes secundarias de luz.  
- Propagación de la luz. La reflexión y la refracción. Utilización de lentes y espejos. Descomposición de la luz.  
- Sonido y audición: Propagación y reflexión del sonido.  
- La contaminación acústica y lumínica.  
5.4. La energía interna del planeta:  
- Origen del calor interno terrestre.  
- Manifestaciones de la energía interna de la Tierra: Vulcanismo y terremotos.  
- Movimientos de los continentes. Fenómenos de los bordes de las placas litosféricas.  
- El relieve terrestre: Continentes y fondos marinos.

####  Bloque 6. La vida en acción
6.1. Las funciones de los seres vivos:  
- Las funciones de los seres vivos: Nutrición, nutrición autótrofa y heterótrofa.  
- La fotosíntesis y su importancia en la vida de la Tierra.  
- La respiración de los seres vivos.  
- Las funciones de reproducción.  
6.2. El medio ambiente:  
- El medio ambiente natural: Conceptos de biosfera, exosfera y ecosistema.  
- Ecosistemas terrestres y acuáticos.  
- El papel que desempeñan los organismos productores, consumidores y descomponedores en el ecosistema. Cadenas y redes tróficas.  
- Ecosistemas característicos de nuestra comunidad autónoma.

###  Criterios de evaluación
1. Explicar la organización del sistema solar y las características de los movimientos de la Tierra y de la Luna y sus implicaciones, así como algunas de las concepciones que sobre el Universo se han dado a lo largo de la historia.  
2. Situar y describir las capas internas y externas de nuestro planeta.  
3. Establecer procedimientos para describir las propiedades de la materia que nos rodea, tales como la masa, el volumen, la densidad, los estados en los que se presentan y sus cambios. Valorar el manejo correcto de instrumentos científicos sencillos. Utilizar modelos gráficos para representar y comparar los resultados obtenidos.  
4. Realizar correctamente cálculos sencillos que incluyan la utilización de las diferentes unidades del SI y manejar las diferentes unidades del sistema métrico decimal.  
5. Reconocer la importancia de la atmósfera para los seres vivos, considerando las repercusiones de la actividad humana en la misma.  
6. Explicar, a partir del conocimiento de las propiedades del agua, el ciclo del agua en la naturaleza y su importancia para los seres vivos, considerando las repercusiones de las actividades humanas en relación con su utilización.  
7. Conocer la estructura interna de la Tierra y los componentes químicos de sus capas. Diferenciar claramente los conceptos de mineral y roca.  
8. Identificar las rocas y los minerales más frecuentes, en especial los que se encuentran en el entorno próximo, utilizando claves sencillas. Reconocer sus aplicaciones más frecuentes.  
9. Conocer de forma operativa el concepto de biodiversidad. Valorar su importancia a escala mundial y en España.  
10. Establecer los criterios que sirven para clasificar a los seres vivos e identificar los principales modelos taxonómicos a los que pertenecen los animales y plantas más comunes, relacionando la presencia de determinadas estructuras con su adaptación al medio.  
11. Interpretar los sistemas materiales como partes del Universo de muy distintas escalas, a los que la ciencia delimita para su estudio. Destacar la energía como una propiedad inseparable de todos ellos, capaz de originarles cambios.  
12. Resolver problemas sencillos aplicando los conocimientos sobre el concepto de temperatura y su medida, el equilibrio y desequilibrio térmico, los efectos del calor sobre los cuerpos y su forma de propagación.  
13. Explicar fenómenos naturales referidos a la transmisión de la luz y del sonido. Reproducir algunos de ellos teniendo en cuenta sus propiedades.  
14. Reconocer y valorar los riesgos asociados a los procesos geológicos terrestres y las pautas utilizadas para su prevención y predicción. Analizar la importancia de los fenómenos volcánicos y sismológicos, así como la necesidad de planificar la prevención de riesgos futuros.  
15. Relacionar el vulcanismo, los terremotos, la formación del relieve y la génesis de las rocas metamórficas y magmáticas con la energía interna del planeta. Situar en un mapa las zonas donde dichas manifestaciones son más intensas y frecuentes.  
16. Interpretar los aspectos relacionados con las funciones vitales de los seres vivos a partir de distintas observaciones y experiencias realizadas con organismos sencillos, comprobando el efecto que tienen determinadas variables en los procesos de nutrición, relación y reproducción.  
17. Definir los conceptos de nutrición celular y respiración, aplicando los conocimientos sobre la obtención de energía.  
18. Diferenciar los mecanismos que tienen que utilizar los seres pluricelulares para realizar sus funciones, distinguiendo entre nutrición autótrofa y heterótrofa, y entre reproducción animal y vegetal.  
19. Caracterizar los ecosistemas más significativos de nuestra comunidad autónoma. Identificar los espacios naturales protegidos en nuestra comunidad autónoma y valorar algunas figuras de protección.20. Realizar correctamente experiencias de laboratorio, respetando las normas de seguridad.

##  MÓDULO DE CIENCIAS DE LA NATURALEZA II


###  Contenidos


####  Bloque 1. Introducción a la metodología científica
- Utilización de estrategias propias del trabajo científico como el planteamiento de problemas y discusión de su interés, la formulación y puesta a prueba de hipótesis y la interpretación de los resultados. El informe científico. Análisis de datos organizados en tablas y gráficos.  
- Búsqueda y selección de información de carácter científico utilizando las tecnologías de la información y comunicación y otras fuentes.  
- Interpretación de información de carácter científico y utilización de dicha información para formarse una opinión propia, expresarse con precisión y argumentar sobre problemas relacionados con la naturaleza. La notación científica.  
- Valoración de las aportaciones de las Ciencias de la Naturaleza para dar respuesta a las necesidades de los seres humanos y mejorar las condiciones de su existencia, así como para apreciar y disfrutar de la diversidad natural y cultural, participando en su conservación, protección y mejora.  
- Utilización correcta de los materiales, sustancias e instrumentos básicos de un laboratorio. Carácter aproximado de la medida. Sistema internacional de unidades. El respeto por las normas de seguridad en el laboratorio.

####  Bloque 2. Las personas y la salud
2.1. Las personas y la salud:  
- Organización general del cuerpo humano. La célula, tejidos, órganos, sistemas y aparatos.  
- El concepto de salud y de enfermedad. Los factores determinantes de la salud.  
- Sistemas inmunitarios. Vacunas. El trasplante y donación de células, órganos y sangre.  
- Primeros auxilios.2.2. La reproducción humana:  
- Los aparatos reproductores masculino y femenino.  
- Las enfermedades de transmisión sexual.  
- El ciclo menstrual. Relación con la fecundidad.  
- Fecundación, embarazo y parto.  
- Principales métodos anticonceptivos.  
- La respuesta sexual humana. Sexo y sexualidad. Salud e higiene sexual.  
2.3. Alimentación y nutrición humana:  
- Las funciones de nutrición.  
- El aparato digestivo. Principales enfermedades.  
- Hábitos alimenticios saludables. Dieta equilibrada.  
- Prevención de enfermedades provocadas por malnutrición.  
2.4. Los aparatos respiratorio, circulatorio y excretor:  
- Descripción y funcionamiento.  
- Hábitos saludables.  
- Enfermedades más frecuentes y su prevención.  
2.5. Las funciones de relación: Percepción, coordinación y movimiento.  
- La percepción: Los órganos de los sentidos, su cuidado e higiene.  
- La coordinación y el sistema nervioso: organización y función.  
- El control interno del organismo. El sistema endocrino.  
- Glándulas endocrinas y su funcionamiento. Sus principales alteraciones.  
- El aparato locomotor. Análisis de las lesiones más frecuentes y su prevención.  
2.6. La salud mental:  
- Factores que repercuten en la salud mental en la sociedad actual.  
- Las sustancias adictivas: El tabaco, el alcohol y otras drogas. Problemas asociados a su consumo.  
- Actitud responsable ante las conductas de riesgo para la salud.

####  Bloque 3. Energía y electricidad
3.1. El concepto de energía:  
- Energías tradicionales y energías alternativas.  
- Fuentes de energía renovables.  
- Conservación y degradación de la energía.  
3.2. Electricidad:  
- Propiedades eléctricas de la materia.  
- Las cargas eléctricas y su interacción.  
- La energía eléctrica. Conductores y aislantes. Circuitos eléctricos sencillos.  
- La electricidad en casa. El ahorro energético.

####  Bloque 4. Las fuerzas y los movimientos
- Estudio cualitativo de los movimientos rectilíneos: Desplazamiento, espacio recorrido, velocidad y aceleración.  
- La fuerza como causa de deformación de los cuerpos.  
- La fuerza como causa de los cambios de movimiento: Fuerza y aceleración. La fuerza peso.

####  Bloque 5. La actividad humana y el medio ambiente
- Los recursos naturales: Definición y clasificación.  
- Importancia del uso y gestión sostenible de los recursos hídricos.  
- La potabilización del agua y los sistemas de depuración.  
- Utilización de técnicas sencillas para conocer el grado de contaminación y depuración del aire y del agua.  
- Los residuos y su gestión. Valoración del impacto de la actividad humana en los ecosistemas. Análisis crítico de las intervenciones humanas en el medio.  
- Principales problemas ambientales de la actualidad.  
- Valoración de la necesidad de cuidar el medio ambiente y adoptar conductas solidarias y respetuosas con él.

####  Bloque 6. Transformaciones geológicas debidas a la energía externa de la Tierra
6.1. La energía de procedencia externa del planeta:  
- La energía solar en la Tierra.  
- La atmósfera como filtro de la energía solar: Su estructura y dinámica.  
- La presión atmosférica.- Interpretación de mapas meteorológicos sencillos.  
6.2. Agentes geológicos externos:  
- Origen de los agentes geológicos externos.  
- Alteraciones de las rocas producidas por la atmósfera: La meteorización.  
- Acción geológica del viento y del hielo.  
- Acción geológica de las aguas superficiales y subterráneas.  
- Aprovechamiento y sobreexplotación de acuíferos.  
- Dinámica marina: Corrientes, mareas y olas. Acción geológica del mar.  
6.3. La formación de las rocas sedimentarias:  
- Las rocas sedimentarias: Formación y clasificación.  
- Explotación y utilización del carbón, del petróleo y del gas natural. Impactos ambientales asociados a su obtención, transporte y consumo. Consecuencias de su agotamiento.

####  Bloque 7. Diversidad y unidad de estructura de la materia
7.1. La materia, elementos y compuestos:  
- La materia y sus estados de agregación: Sólido, líquido y gaseoso.  
- Teoría cinética de la materia y cambios de estado.  
- Sustancias puras y mezclas. Métodos de separación de mezclas. Disoluciones. Métodos de separación de disoluciones. Elementos y compuestos.  
7.2. Átomos y moléculas:  
- Estructura atómica. Partículas constituyentes del átomo.  
- Utilización de modelos.  
- Introducción al concepto de elemento químico.  
- Uniones entre átomos: Moléculas y cristales.  
- Fórmulas y nomenclatura de las sustancias más corrientes según las normas de la IUPAC.  
- Masas atómicas y moleculares. Concepto de isótopo. Aplicaciones de los isótopos radioactivos.

####  Bloque 8. Los cambios químicos
8.1. Las reacciones químicas:  
- Interpretación microscópica de las reacciones químicas.  
ç- Representación simbólica.  
- Ecuaciones químicas y su ajuste.  
- Realizaciones experimentales de algunos cambios químicos.  
- Reacciones de oxidación y de combustión.  
- Impactos medioambientales: Efecto invernadero, lluvia ácida, destrucción de la capa de ozono, contaminación de aguas y tierras.

###  Criterios de evaluación
1. Determinar los rasgos distintivos del trabajo científico a través del análisis contrastado de algún problema científico o tecnológico de actualidad y su influencia sobre la calidad de vida de las personas.  
2. Realizar correctamente experiencias de laboratorio propuestas a lo largo del curso, respetando las normas de seguridad.  
3. Describir las interrelaciones existentes en la actualidad entre sociedad, ciencia y tecnología.  
4. Describir la morfología celular y explicar el funcionamiento de los orgánulos más importantes.  
5. Describir los aspectos básicos del aparato reproductor, diferenciando entre sexualidad y reproducción.  
6. Conocer y comprender el funcionamiento de los métodos de control de natalidad. Valorar el uso de métodos de prevención de enfermedades de transmisión sexual.  
7. Explicar los procesos fundamentales de la digestión y asimilación de los alimentos, utilizando esquemas y representaciones gráficas. Justificar, a partir de ellos, los hábitos alimenticios saludables, independientes de prácticas consumistas inadecuadas. Analizar el consumo de alimentos de nuestra comunidad autónoma.  
8. Explicar la misión integradora del sistema nervioso ante diferentes estímulos, describir su funcionamiento, enumerar algunos factores que lo alteran y reflexionar sobre la importancia de hábitos de vida saludables.  
9. Explicar la función integradora del sistema endocrino conociendo las causas de sus alteraciones más frecuentes y valorar la importancia del equilibrio entre todos los órganos del cuerpo humano.  
10. Localizar los principales huesos y músculos que integran el aparato locomotor.  
11. Reconocer que en la salud influyen aspectos físicos, psicológicos y sociales. Valorar la importancia de los estilos de vida para prevenir enfermedades y mejorar la calidad de vida, así como las continuas aportaciones de las ciencias biomédicas. Analizar las influencias de algunos estilos de vida sobre la salud.  
12. Comprender el concepto de energía. Razonar ventajas e inconvenientes de las diferentes fuentes energéticas. Enumerar medidas que contribuyan al ahorro colectivo o individual de energía. Explicar por qué la energía no puede reutilizarse sin límites.  
13. Describir los diferentes procesos de electrización de la materia. Clasificar materiales según su conductividad. Indicar las diferentes magnitudes eléctricas y los componentes básicos de un circuito. Resolver ejercicios numéricos de circuitos sencillos. Saber calcular el consumo eléctrico en el ámbito doméstico.  
14. Diseñar y montar circuitos de corriente continua, respetando las normas de seguridad, en los que se pueden llevar a cabo mediciones de intensidad de corriente y de diferencias de potencial, indicando los valores con la precisión que corresponde al tipo de instrumento de medida utilizada.  
15. Identificar el papel de las fuerzas como causa de los cambios de movimiento y reconocer algunas de las principales fuerzas presentes en la vida cotidiana.  
16. Recopilar información procedente de fuentes documentales y de internet acerca de la influencia de las actuaciones humanas sobre diferentes ecosistemas: Efectos de la contaminación, desertización, disminución de la capa de ozono, agotamiento de recursos y extinción de especies. Analizar dicha información y argumentar posibles actuaciones para evitar el deterioro del medio ambiente y promover una gestión más racional de los recursos naturales. Estudiar algún caso de especial incidencia en nuestra comunidad autónoma.  
17. Relacionar los procesos geológicos externos e internos mediante la explicación del ciclo geológico y su representación esquemática.  
18. Relacionar la desigual distribución de la energía en la superficie del planeta con el origen de los agentes geológicos externos. Identificar las acciones de dichos agentes en el modelado del relieve terrestre.  
19. Reconocer las principales rocas sedimentarias, conocer su origen, clasificación y explotación.  
20. Describir las características de los estados sólido, líquido y gaseoso. Explicar en qué consisten los cambios de estado, empleando la teoría cinética, incluyendo la comprensión de gráficas.  
21. Diferenciar entre elementos, compuestos y mezclas. Explicar los procedimientos químicos básicos para su estudio. Describir las disoluciones. Efectuar correctamente cálculos numéricos sencillos sobre su composición. Explicar y emplear las técnicas de separación y purificación.  
22. Distinguir entre átomos y moléculas. Indicar las características de las partículas componentes de los átomos. Diferenciar los elementos.  
23. Formular y nombrar algunas sustancias importantes. Indicar sus propiedades.  
24. Discernir entre cambio físico y químico. Comprobar que la conservación de la masa se cumple en toda reacción química. Escribir y ajustar correctamente ecuaciones químicas sencillas.  
25. Explicar los procesos de oxidación y combustión, analizando su incidencia en el medio ambiente.  

