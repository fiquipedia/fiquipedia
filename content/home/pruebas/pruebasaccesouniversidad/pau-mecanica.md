---
aliases:
  - /home/pruebasaccesouniversidad/pau-mecanica/
---

# PAU Mecánica

La materia de mecánica desapareció en la PAU de 2010, ya que la LOE la derogaba, sin embargo como se comenta en la  [página sobre la materia de Mecánica de 2º de bachillerato](/home/materias/bachillerato/mecanica-2-bachillerato)  se considera útil recopilar exámenes PAU y hacer colecciones de problemas.  
Como se puede ver esta página está en construcción: la idea es que acabe quedado similar a las páginas de  [PAU Física](/home/pruebas/pruebasaccesouniversidad/paufisica)  y  [PAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) , pero solamente he iniciado una recopilación parcial de enunciados de examen, en algunos casos incluyendo soluciones.

## **Enunciados y soluciones agrupados por bloques, elaboración propia**
Pendiente agrupar ejercicios por bloques y resolverlos; la resolución en formato electrónico implica muchos gráficos y llevará mucho tiempo.De empezar no lo haría necesariamente para la comunidad de Madrid, ya que no es una materia vigente; quizá algunos problemas representativos de cada tipo de distintas comunidades ...

## [Recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales)](/home/recursos/recursospau/ficheros-enunciados-pau-mecanica) 
