# Referencias a FiQuiPedia

En mayo 2016 FiQuiPedia es citada por la  [Real Sociedad Española de Química](http://rseq.org/) en sus [materiales didácticos](https://rseq.org/recursos/), [Resumen de las normas IUPAC 2005 de nomenclatura de Química Inorgánica para su uso en enseñanza secundaria y recomendaciones didácticas](https://rseq.org/mat-didacticos/resumen-de-las-normas-iupac-2005-de-nomenclatura-de-quimica-inorganica-para-su-uso-en-ensenanza-secundaria-y-recomendaciones-didacticas/), [5-Otros materiales de interés](https://rseq.org/wp-content/uploads/2018/09/5-OtrosMateriales.pdf#page=2)  
 [![Real Sociedad Española de Química, Otros materiales de Interés, referencia a FiQuiPedia](https://pbs.twimg.com/media/CkfVwq9WYAEK5jT?format=jpg) ](https://twitter.com/FiQuiPedia/status/1176563161455050753)  
Enlaza a [http://bit.ly/24hT8yd](http://bit.ly/24hT8yd) que es mi opinión personal  

Pongo resto de referencias en orden cronológico (las primeras si no indican fecha / no es conocida). No cito referencias a FiQuiPedia no asociadas a Física y Química, asociadas a la actividad del blog [Algo queda que decir](http://algoquedaquedecir.blogspot.com.es/)  

[educarex.es, Junta de Extremadura,  Rincón didáctico, Biología y Geología, Algunas webs y blogs para Cultura Científica de 1º de Bachillerato](https://biologiaygeologia.educarex.es/index.php/novedades/2061-algunas-webs-y-blogs-para-cultura-cientifica-de-1-de-bachillerato)

**2012**  
[intef. Instituto de Tecnologías Educativas. Experiencias educativas. Creación, reutilización y difusión de contenidos ](http://web.archive.org/web/20180717110323/http://formacionprofesorado.educacion.es/version/v2/index.php/es/experiencias/278-creacion-reutilizacion-y-difusion-de-contenidos) (waybackmachine, enlace no operativo en 2021)

**18 mayo 2015**   
[magonia.com ¡Meta la sábana santa en clase de ciencias y enseñe física y pensamiento crítico!](https://magonia.com/2015/05/18/sabana-santa-clase-ciencias/)

**19 abril 2015**  
[CEDEC (Centro Nacional de Desarrollo de Sistemas No Propietarios) FiQuiPedia](http://cedec.intef.es/fiquipedia/) 

**2015**  
[educa.jcyl.es Portal de Educación de la Junta de Castilla y León, recursos educativos](http://www.educa.jcyl.es/crol/es/recursos-educativos/fiquipedia) 

**3 abril 2016**  
Recopilación de capturas de algunos tuits  
[twitter FiQuiPedia/status/716640244976713729](https://twitter.com/FiQuiPedia/status/716640244976713729)  
[![](http://pbs.twimg.com/media/CfIEgB4XEAI1G0K.jpg) ](http://pbs.twimg.com/media/CfIEgB4XEAI1G0K.jpg) 

**22 septiembre 2016**  
[procomun.educalab.es ANALISIS DE UN REA-WEB PARA FÍSICA Y QUÍMICA](https://procomun.educalab.es/ca/articulos/analisis-de-un-rea-web-para-fisica-y-quimica)  

**5 diciembre 2018**  
[Recursos didácticos para la asignatura de Física - educaciontrespuntocero.com](https://www.educaciontrespuntocero.com/recursos/recursos-para-fisica/95530)  
**[FiQuiPedia](https://www.fiquipedia.es)**  
>_Esta web, que homenajea en su nombre a la famosa enciclopedia online, pone a disposición de alumnado y profesorado una amplia selección de apuntes en formato PDF, ejercicios prácticos resueltos y experimentos para llevar al aula, así como numerosos enlaces a fuentes de información que resultarán de interés para sus visitantes._

**27 febrero 2019**  
[‘Frikiexámenes’ para implicar y reducir el estrés de los estudiantes - educaciontrespuntocero.com](https://www.educaciontrespuntocero.com/experiencias/frikiexamenes-estres-estudiantes/)  
_Enrique García (@FiQuiPedia) ha recopilado gran parte de estos “frikiexámenes” en su [web](../home/recursos/examenes/friquiexamenes)._  

[‘Frikiexámenes’ para reducir la ansiedad del alumno ante la prueba - informavalencia.com](https://www.informavalencia.com/2019/02/27/frikiexamenes-para-reducir-la-ansiedad-del-alumno-ante-la-prueba/)  

**28 noviembre 2019**
Desde Twitter se generan memes que citan FiQuiPedia, con varios hashtags #HomenajeAlDocente2019 #FiquipediaRules #adalidelosdatos #azotedelaverdad #justicierodelaeducaciónpública #Fiquipediaday  
Algunos están asociados a la actividad del blog, no a la página web  
Se realiza 28 noviembre pero surge el 13 noviembre 2019 con este tuit  
[twitter EnJarras/status/1194694311230791681](https://twitter.com/EnJarras/status/1194694311230791681)  
Propongo el próximo 28 de noviembre, martes,día de san Honesto de Nimes, como bien sabe @Siquiatro, como día de homenaje al compañero Enrique García @FiQuiPedia  
HILO  

Pongo algunos omitiendo los hashtags

[twitter alatorrefyq/status/1200195470821404673](https://twitter.com/alatorrefyq/status/1200195470821404673)  
¡Gracias! De tantos y por tanto. 
![](https://pbs.twimg.com/media/EKfzah-XkAAihuh?format=jpg)  

[twitter CaperuErinna/status/1200048427008217091](https://twitter.com/CaperuErinna/status/1200048427008217091)  
Muchísimas gracias por hacer lo que todos deberíamos hacer y no rendirte ante la burocracia. 
![](https://pbs.twimg.com/media/EKdtrmhXYAI-foE?format=jpg)  

[twitter ProfaDeQuimica/status/1200110813463371777](https://twitter.com/ProfaDeQuimica/status/1200110813463371777)  
@FiQuiPedia, ayer tuve una visión premonitoria... 
![](https://pbs.twimg.com/media/EKema0QWkAAApDO?format=jpg)  
[twitter ProfaDeQuimica/status/1200149764186296320](https://twitter.com/ProfaDeQuimica/status/1200149764186296320)  
![](https://pbs.twimg.com/media/EKfJ1_fXUAE7zOm?format=jpg)  
[twitter ProfaDeQuimica/status/1200126081522638850](https://twitter.com/ProfaDeQuimica/status/1200126081522638850)  
![](https://pbs.twimg.com/media/EKe0TopWsAAtgEj?format=jpg)  

[twitter mamenoula/status/1199947790090149889](https://twitter.com/mamenoula/status/1199947790090149889)  
Hoy muchos docentes agradecemos a @FiQuiPedia  su gran labor como adalid de la transparencia y garante de nuestros derechos. Muchísimas gracias, Enrique  
![](https://pbs.twimg.com/media/EKcSJeyWkAEDw7X?format=jpg)  

[twitter Biochiky/status/1200086811231608834](https://twitter.com/Biochiky/status/1200086811231608834)  
II. ¡Hoy toca homenaje a @FiQuiPedia!  
![](https://pbs.twimg.com/media/EKeQl1tXUAA9E3A?format=jpg)  
![](https://pbs.twimg.com/media/EKcOtxhXkAALsLw?format=jpg)  

[twitter denyureypunto/status/1200169722651828224](https://twitter.com/denyureypunto/status/1200169722651828224)  
Felicidades y gracias por todo. ¡Dale duro a la administración!  
![](https://pbs.twimg.com/media/EKfb_ufXYAMVTI1?format=jpg)  

[twitter elenadesociales/status/1200111122868789250](https://twitter.com/elenadesociales/status/1200111122868789250)  
![](https://pbs.twimg.com/media/EKems9wXYAAdxXi?format=jpg)  
[twitter elenadesociales/status/1200112177954410496](https://twitter.com/elenadesociales/status/1200112177954410496)  
![](https://pbs.twimg.com/media/EKenpxKX0AEzMlo?format=jpg)  

[twitter Gonprados/status/1200101481325699073](https://twitter.com/Gonprados/status/1200101481325699073)  
![](https://pbs.twimg.com/media/EKed7rvXsAYeXrx?format=jpg)  

[twitter MarisaTra/status/1199952921712087040](https://twitter.com/MarisaTra/status/1199952921712087040)  
En nuestro santoral tuitero no puede faltar @Fiquipedia.  
![](https://pbs.twimg.com/media/EKcWzSdWkAAE1PQ?format=png)  

**21 marzo 2020**  
[fespugtclm FiQuiPedia – Recursos didácticos sobre Física y Química – ESO, Bachillerato, EvAU, FP,…](https://educacion.fespugtclm.es/fiquipedia-recursos-didacticos-sobre-fisica-y-quimica-eso-bachillerato-evau-fp/)

**9 junio 2020**  
[magisnet EBAU 2020: Una selectividad distinta a todas las demás](https://www.magisnet.com/2020/06/ebau-2020-una-selectividad-distinta-a-todas-las-demas/)

>El año pasado, Cristina López Jarrega, que hoy cursa Medicina en la Universidad de Navarra, obtuvo un 13,5 en selectividad. Reconoce que en la recta final en las asignaturas más prácticas (Matemáticas, Física y Química) estuvo haciendo los exámenes de los últimos años en la Comunidad de Madrid, donde se presentaba, y en Castilla y León, “donde me dijeron que eran más complicados”. Aunque se centró en las cinco últimas ediciones, tenía a mano todos los exámenes desde 2001 en la web **FiQuiPedia**: “Yo creo que en Lengua, Inglés e Historia se mantiene el nivel, pero que en Física y Matemáticas eran más difíciles cuanto más antiguos”, relata.

**1 octubre 2020**  
[El País: Madrid gasta cerca de 17 millones de euros en materiales digitales para **Editorial Planeta, Informática El Corte Inglés y Odilo**. Los sindicatos y los directores de instituto lo tachan de “despilfarro innecesario” y critican la adjudicación “a dedo”](https://elpais.com/espana/madrid/2020-10-01/madrid-gasta-cerca-de-17-millones-de-euros-en-materiales-digitales-para-educamadrid.html)  
>_También cuentan con un ingente material gratuito que facilitan webs hechas por docentes como  [Fiquipedia](https://www.fiquipedia.es)  y  [Orientación Andújar](https://www.orientacionandujar.es/)  y vídeos de todo tipo en  [YoutubeEdu](https://www.youtube.com/channel/UCSSlekSYRoyQo8uQGHvq4qQ)._

Retoco el titular original del artículo que citaba "EducaMadrid". Ver post [EducaMadrid: qué y por qué](https://algoquedaquedecir.blogspot.com/2021/01/educamadrid-que-y-por-que.html), apartados "EducaMadrid no son recursos propietarios, 
EducaMadrid no es aulaPlaneta, edebé / iecisa / Madrid5e ... "

**[11 diciembre 2022**](https://github.com/rodrigoalcarazdelaosa/fisiquimicamente/commits/55c7ef185e81c14de60328781fa42b4506025bdd/content/es/enlaces-recursos-online/FiQuiPedia/index.md)  
[Enlaces de interés - fisiquimicamente](https://fisiquimicamente.com/enlaces-interes/recursos-online/)

**abril 2021**
[Reflexiones multidisciplinares para el tratamiento de la competencia artística y la formación cultural. Salido López, P.V. y Irisarri Juste, M.R. (coords.). (2021). Cuenca: Ediciones de la Universidad de Castilla-La Mancha. - uclm.es](https://publicaciones.uclm.es/reflexiones-multidisciplinares-para-el-tratamiento-de-la-competencia-artistica-y-la-formacion-cultural/)  
ISSN: 2697-049X; ISBN: 978-84-9044-440-5; Pedro V. Salido López y María Rosario Irisarri Juste (coords.)
[N.º 29: Reflexiones multidisciplinares para el tratamiento de la competencia artística y la formación cultural - uclm.es](https://ruidera.uclm.es/xmlui/handle/10578/28141)  

[13 De Ben-Hur a Los Vengadores: el cine en el aula de Física y Química. 167 pp. Leticia Isabel Cabezas Bermejo - doi.org](http://doi.org/10.18239/jornadas_2021.29.13)  

[13 De Ben-Hur a Los Vengadores: el cine en el aula de Física y Química. Cabezas Bermejo, Leticia Isabel - uclm.es (página 167 a 178 de pdf de 368 páginas)](https://ruidera.uclm.es/xmlui/bitstream/handle/10578/28142/Reflexiones%20multidisciplinares%20para%20el%20tratamiento%20de%20la.pdf?sequence=1&isAllowed=y#page=167)  
> Desde el curso 2016-2017, un grupo de docentes de secundaria que se han conectado a través 
de la red social Twitter, han trabajado de manera más activa en este tipo de formato de pruebas 
bautizándolos y etiquetándolos como #frikiexamen. Comparten capturas de los enunciados, 
licenciados como materiales Creative Commons, por si son de utilidad para otros docentes. 
El repositorio Fiquipedia (García, 2020) aglutina ejemplos compartidos por una veintena de 
docentes durante los últimos cursos.

> GARCÍA SIMÓN, E. (2020). FiQuiPedia: FriQuiExámenes. [Consulta: 7 de septiembre de 2020](http://www.fiquipedia.es/home/recursos/examenes/friquiexamenes)  

Tras encontrar esta referencia en 2021, veo que en 2019 hubo una comunicación con el mismo título, "De Ben-Hur a Los Vengadores: el cine en el aula de Física y Química", presentada conjuntamente por Antonio Ruiz Relea dentro de [VI Congreso Internacional de Competencias: Conciencia cultural y expresiones artísticas - blog.uclm.es](https://blog.uclm.es/didactica/2019/04/01/vi-congreso-internacional-de-competencias-conciencia-cultural-y-expresiones-artisticas/) que se celebró días 3, 4 y 5 abril 2019  
[VI Congreso Internacional de Competencias: Conciencia cultural y expresiones artísticas - eventos.uclm.es](https://eventos.uclm.es/24375/detail/vi-congreso-internacional-de-competencias_-conciencia-cultural-y-expresiones-artisticas.HTML)   
[twitter profedefyq/status/1439708977546268677](https://twitter.com/profedefyq/status/1439708977546268677)  

**mayo 2021**  
Citada desde la web de mi actual centro, primer centro definitivo no forzoso  
[IES Alkal'a Nahar, departamentos](https://www.educa2.madrid.org/web/centro.ies.alkalanahar.alcala/departamentos)  

**19 mayo 2021**  
[¿Qué páginas web ofrecen los mejores recursos para repasar química? - superprof.es](https://www.superprof.es/blog/paginas-repaso-quimica/)  

> La FiQuiPedia es, sin duda, uno de los recursos que deberás guardar en tus Favoritos ya que es una mina de oro para construir tus apuntes de química de cara a tu examen de acceso a la universidad. Encontrarás, asimismo, todo lo relativo a esta prueba, desde apuntes, ejercicios, exámenes, problemas...  
> Por supuesto, la desventaja que ofrece la anterior página web es su interfaz, poco interactiva. 

**1 junio 2021**  
Se cita en bibliografía  
[Friki-exámenes como herramienta de evaluación en Física y Química, Miguel Juanals Márquez. Máster Universitario en Profesor de Educación Secundaria Obligatoria y Bachillerato, Formación Profesional y Enseñanza de Idiomas. Universidad de Salamanca. Curso 2020-2021](https://gredos.usal.es/bitstream/handle/10366/146924/2021_TFM_MUPES_Friki-ex%C3%A1menes%20como%20herramienta%20de%20evaluaci%C3%B3n%20en%20F%C3%ADsica%20y%20Qu%C3%ADmica.pdf?sequence=1)  

> García Simón, E. (2021). Fiquipedia: Friquiexámenes. http://www.fiquipedia.es/home/recursos/examenes/friquiexamenes. Accedido 01-06-2021.

**21 octubre 2021**  
[Los nuevos currículos de Secundaria, una "revolución" que deja elegir a los docentes el contenido de las asignaturas](https://www.eldiario.es/sociedad/nuevos-curriculos-secundaria-revolucion-deja-elegir-docentes-contenido-asignaturas_1_8399075.html)  

> Lo resume así [el profesor Enrique García en un primer análisis de los currículos](https://www.fiquipedia.es/home/legislacion-educativa/lomloe/curriculo-concepto-estructura/): "Se criticó la LOMCE por los estándares, que era demasiado enciclopédica con mucho detalle, generando algunos estándares absurdamente concretos/específicos, pero la concreción no la veo mal siempre que no se baje a detalles absurdos. Veo peor no bajar a ningún tipo de detalle que pueda generar una evaluación absurdamente abstracta/inespecífica, pero que cumpla la letra de lo que dice el currículo".  

**marzo 2022**  
Se cita en el módulo 5 del curso de funcionarios en prácticas de Madrid  
[twitter FiQuiPedia/status/1504913625491910659](https://twitter.com/FiQuiPedia/status/1504913625491910659)  
Me acaba de pasar esto una persona que está  haciendo el curso de funcionarios en prácticas, primera noticia que tengo.   
![](https://pbs.twimg.com/media/FOKG92DWYAQL6Jk?format=jpg)  

**octubre 2022**
Enlazada por la Universidad de Murcia
[Asignatura Química. Curso académico 2022/2023. Código 1150. Titulación Grado en Biotecnología. Centro docente Facultad de Ciencias Experimentales. Curso 1 de Grado en Biotecnología](https://www.umh.es/contenido/Estudios/:asi_g_1150_O1/datos_es.html)  
![](https://pbs.twimg.com/media/Feem85RWAAAGOL5?format=png)  

Leticia Cabezas comparte problemas oposición y me cita  
[twitter ProfaDeQuimica/status/1580170669538836480](https://twitter.com/ProfaDeQuimica/status/1580170669538836480)  
DIFUSIÓN, POR FAVOR.
Comparto los problemas que hice al preparar el práctico de la #oposición de Secundaria de #FyQ (licencia CC By-Nc-Nd) por si ayuda a otros opositores. ☺️ Obtuve plaza en Madrid en 2015 y quedé a las puertas en CLM.  
[twitter FiQuiPedia/status/1580571178938552321](https://twitter.com/FiQuiPedia/status/1580571178938552321)  
He visto que me citas, muchas gracias!  
PD: el apartado (menos poblado) existía en septiembre 2014  
![](https://pbs.twimg.com/media/Fe9PhtaWAAEmt22?format=png)  

**diciembre 2022**
[Consejos para la oposición de profesor en la especialidad de Física y Química - fisicatabu.com](https://fisicatabu.com/oposiciones-fyq/consejos-para-la-oposicion-de-profesor-en-la-especialidad-de-fisica-y-quimica/)  
> Un lugar donde encontrar los problemas publicados en diversas comunidades a lo largo de muchas convocatorias es la genial página [FiQuiPedia](https://www.fiquipedia.es/home/recursos/recursos-para-oposiciones/recursos-para-oposiciones-problemas-por-comunidad/) (incluye soluciones propias, y además el año pasado los recopiló todos en un libro que encontrarás al final del enlace anterior).  


