
# Recursos Química

Desde la página [recursos](/home/recursos) se enlaza aquí para los asociados a Química  

Aparte de enlaces aparte para  [apuntes](/home/recursos/apuntes) ,  [simulaciones](/home/recursos/simulaciones) ,  [laboratorio/prácticas](/home/recursos/practicas-experimentos-laboratorio)  y por otros temas, se ponen aquí páginas generales sobre química que agrupan varios tipos de recursos  

[The Map of Chemistry - Dominic Walliman](https://dominicwalliman.com/post/160913843520/the-map-of-chemistry-poster-by-dominic-walliman)  
[![](https://m.media-amazon.com/images/I/81P8iyLbYGS._AC_SL1500_.jpg)](https://dominicwalliman.com/post/160913843520/the-map-of-chemistry-poster-by-dominic-walliman "Dominic Walliman The map of chemistry")  

En esta página irán recursos genéricos sobre química y enlaces a recursos específicos sobre química, como puede ser enlazar a  [recursos sobre la tabla periódica](/home/recursos/quimica/recursos-tabla-periodica) Se intentan poner como subpáginas los recursos asociados solamente a química, por lo que  [recursos sobre la tabla periódica](/home/recursos/quimica/recursos-tabla-periodica)  también debería estar accesible en este listado/índice:

## Listado de subpáginas Recursos Química

   *  [Alimentos](/home/recursos/quimica/alimentos)
   *  [Apps Química](/home/recursos/quimica/apps-quimica) 
   *  [Bioquímica](/home/recursos/quimica/bioquimica)
   *  [Chemistree](/home/recursos/quimica/chemistree) 
   *  [Cinética química](/home/recursos/quimica/cinetica-quimica) 
   *  [Concentración](/home/recursos/quimica/concentracion) 
   *  [Concepto de mol](/home/recursos/quimica/concepto-de-mol) 
   *  [Cristales](/home/recursos/quimica/cristales) 
   *  [Diagrama Lewis](/home/recursos/quimica/diagrama-lewis)
   *  [Electroquímica](/home/recursos/quimica/electroquimica) 
   *  [Elementos](/home/recursos/quimica/elementos)  
      *  [Carbono](/home/recursos/quimica/elementos/carbono)    
      *  [Hidrógeno](/home/recursos/quimica/elementos/hidrogeno) 
      *  [Hierro](/home/recursos/quimica/elementos/hierro) 
      *  [Oro](/home/recursos/quimica/elementos/oro)    
      *  [Plata](/home/recursos/quimica/elementos/plata)    
      *  [Zinc](/home/recursos/quimica/elementos/zinc)  
   *  [Enlace químico](/home/recursos/quimica/enlace-quimico) 
   *  [Equilibrio químico](/home/recursos/quimica/equilibrio-quimico) 
   *  [Estados de oxidación](/home/recursos/quimica/estados-de-oxidacion) 
   *  [Formulación](/home/recursos/quimica/formulacion) 
   *  [Fuerzas intermoleculares](/home/recursos/quimica/fuerzas-intermoleculares) 
   *  [Geometría molecular](/home/recursos/quimica/geometria-molecular) 
   *  [Isomería](/home/recursos/quimica/isomeria) 
   *  [Leyes de los gases](/home/recursos/quimica/leyes-de-los-gases) 
   *  [Olimpiadas de Química](/home/recursos/quimica/olimpiadas-quimica) 
   *  [Química Orgánica](/home/recursos/quimica/quimica-organica) 
   *  [Quimioluminiscencia](/home/recursos/quimica/quimioluminiscencia)
   *  [Reacciones químicas vistosas](/home/recursos/quimica/reacciones-quimicas-vistosas) 
   *  [Recursos ajuste reacciones químicas](/home/recursos/quimica/recursos-ajuste-reacciones-quimicas) 
   *  [Recursos cambios químicos](/home/recursos/quimica/recursos-cambios-quimicos) 
   *  [Recursos configuración electrónica](/home/recursos/quimica/recursos-configuracion-electronica) 
   *  [Recursos disoluciones](/home/recursos/quimica/recursos-disoluciones) 
   *  [Recursos estructura del átomo / modelos atómicos](/home/recursos/quimica/recursos-estructura-del-atomo) 
   *  [Recursos mezclas](/home/recursos/quimica/recursos-mezclas) 
   *  [Recursos programas dibujo químico](/home/recursos/quimica/recursos-programas-dibujo-quimico) 
   *  [Recursos propiedades coligativas](/home/recursos/quimica/recursos-propiedades-coligativas) 
   *  [Recursos propiedades sustancias](/home/recursos/quimica/recursos-propiedades-sustancias) 
   *  [Recursos quimiofobia](/home/recursos/quimica/quimiofobia) 
   *  [Recursos Química y Arte](/home/recursos/quimica/recursos-quimica-y-arte) 
   *  [Recursos química: fuegos artificiales, pirotecnia, ensayos a la llama...](/home/recursos/quimica/recursos-quimica-fuegos-artificiales) 
   *  [Recursos reacciones y cálculos estequiométricos](/home/recursos/quimica/recursos-reacciones-y-calculos-estequiometricos) 
   *  [Recursos Tabla Periódica / elementos químicos](/home/recursos/quimica/recursos-tabla-periodica) 
   *  [Recursos ácidos y bases](/home/recursos/quimica/recursos-acidos-bases) 
   *  [Seguridad laboratorio](/home/recursos/quimica/seguridad-laboratorio)  
   *  [Termoquímica](/home/recursos/quimica/termoquimica) 
   *  [Volumen molar](/home/recursos/quimica/volumen-molar) 
   
Con el listado/índice anterior deberían estar todos los enlaces a "recursos química por bloques" dentro de los distintos cursos, especialmente química de 2º de bachillerato  
También puede haber algún enlace no puesto aquí en  [recursos química de 2º de bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato)   

[Visualizaciones en Química - uv.es](https://www.uv.es/quimicajmol/index.html)   
(ESO, Bachillerato, formulación, simulaciones ...)  

   
[Las reacciones químicas - cnice.mec.es (Flash)](http://concurso.cnice.mec.es/cnice2005/35_las_reacciones_quimicas/curso/index.html)  
Rafael Jiménez Prieto. Pastora María Torres Verdugo. Licenciamiento no detallado.  
Animaciones muy visuales para ajuste reacciones.  

Chemical Portal. WebQC.Org online education free homework help chemistry problems questions and answer  
[http://www.webqc.org/](http://www.webqc.org/)  
Online Tools, ....  
Incluye por ejemplo una utilidad para realizar ajustes de reacciones  
[http://www.webqc.org/balance.php](http://www.webqc.org/balance.php)  


[Online Resources for Teaching and Learning Chemistry - chemcollective.org](http://chemcollective.org/home)  
[Online Resources for Teaching and Learning Chemistry - chemcollective.oli.cmu.edu/](https://chemcollective.oli.cmu.edu/) **url operativa en 2022 cuando dominio org no funcionaba**  
Inglés. Recursos de distintos tipos (laboratorios virtuales, tutoriales, cursos online, ...) cubriendo distintos temas (estequiometría, termoquímica, cinética...)  
Carnegie Mellon University. The ChemCollective site and its contents are licensed under a Creative Commons Attribution 3.0 NonCommercial-NoDerivs License.  
  
[The Chemical Thesaurus Reaction Chemistry Database](https://www.chemthes.com/)  

[Manual de Química Analítica - uia.mx](http://www.uia.mx/campus/publicaciones/quimanal/)  
Martha Mallen V. Separado por temas, incluye 5- Reacciones de Neutralización, 6- Reacciones de Precipitación, 7- Reacciones Redox ...  

Apuntes química, física, matemáticas ...  
[http://www.100ciaquimica.net/](http://www.100ciaquimica.net/)  
Copyright www.100ciaquimica.net.  

Química en todas partes.  
[http://quimicaeverywhere.blogspot.com.es/](http://quimicaeverywhere.blogspot.com.es/)  
Incluye materiales como pdf con 184 transparencias de todo el curso, ejercicios PAU seleccionados por bloques  
Carmen Hermira, IES Doctor Marañón  

An Introduction to Chemistry by Mark Bishop. Textbooks, tools (animations)...  
[http://preparatorychemistry.com/](http://preparatorychemistry.com/)  

La chimie à partir de zéro (francés)  
[http://www.siteduzero.com/sciences/tutoriels/la-chimie-a-partir-de-zero](http://www.siteduzero.com/sciences/tutoriels/la-chimie-a-partir-de-zero)  

This chemistry blog is aimed mainly at senior high school students or first year university students. It covers general chemistry topics required in Colleges and Universities. However, chemistry topics of general interest are going to be included.  
[http://chem-net.blogspot.com.es/](http://chem-net.blogspot.com.es/)  
  
[Interactive Chemistry - schools.matter.org.uk *waybackmachine*](http://web.archive.org/web/20160818100432/http://schools.matter.org.uk/InteractiveCD.htm)  
Materials Teaching Educational Resources © 1999 MATTER Project, The University of Liverpool 

 [http://beautifulchemistry.net/](http://beautifulchemistry.net/)  
 Beautiful Reactions  
 Beautiful Structures  
 Beautiful Apparatus: Reactions cannot be performed without chemical apparatus. Using computer graphics, we have visualized the apparatus used by Joseph Priestley (1733-1804) and Antoine Lavoisier (1743-1794), two famous scientists in the history of chemistry.  
 Copyright © 2014 Institute Of Advanced Technology at University of Science and Technology of China and Tsinghua University Press. All rights reserved  
 
 Chimica-online.it: risorse didattiche per lo studio online della Chimica  
 [http://www.chimica-online.it/](http://www.chimica-online.it/)  
 (Italiano, con ejercicios)  
 
MCAT (Medical College Admissions Test) son unos exámenes similares a MIR / EvAU de acceso a medicina que tienen parte de química, y se pueden buscar exámenes de ejemplo  
>The  (MCAT) is the standardized exam that all medical colleges in the United States require of prospective students.The MCAT does not test your ability to regurgitate facts, rather the ability to use that knowledge in an applied manner.  

[Free MCAT Practice Tests [2021 Updated]](https://www.test-guide.com/free-mcat-practice-tests.html)  
[MCAT Practice Questions: Chemistry](https://www.kaptest.com/study/mcat/mcat-practice-questions-chemistry/)  

Tutorías en línea para la asignatura de QUÍMICA IV ÁREA I  
[Proyecto INFOCAB SB 202507, Responsable académica: I. Q. Raquel Enríquez García, Tutorías en línea para la asignatura de QUÍMICA IV ÁREA I - *waybackmachine* ](http://web.archive.org/web/20161123203413/http://prepa8.unam.mx/academia/colegios/quimica/infocab/index.html)  
 Se diseñó con el objeto de tratar los temas que presentan mayor complejidad del programa de Química IV área I y sirva de apoyo didáctico para los alumnos.  
 
[chemwiki.ucdavis.edu](http://chemwiki.ucdavis.edu/)  
 The ChemWiki is a collaborative approach toward chemistry education where an Open Access textbook environment is constantly being written and re-written by students and faculty members resulting in a free Chemistry textbook to supplant conventional paper-based books. The development of the ChemWiki is currently directed by UC Davis Professor Delmar Larsen.  
 cc-by-nc-sa   
En 2021 pasa a ser [Chemistry libre texts](https://chem.libretexts.org/)   
 
[chemguide, Helping you to understand Chemistry](http://www.chemguide.co.uk/)    
 © Jim Clark 2009 (last modified August 2017)  
 
[The Cavalcade o' Chemistry](https://chemfiesta.wordpress.com/)  
The resources on this site were written between 1998 and 2018 by Ian Guch and are copyrighted. You may use these resources subject to the the Creative Commons Attribution-NonCommerical-ShareAlike 4.0 International license (CC BY-NC 4.0). 

[PROBLEMAS RESUELTOS DE QUÍMICA GENERAL ](http://www.matematicasypoesia.com.es/ProbQuimica/ProbQuiBasPreg.htm)  
 José Antonio Hervás  
  
[Introductory Chemistry. Chapter 1: What Is Chemistry?](http://www.peoi.org/Courses/Coursesch/chemintro/contents/foreword.html)  
 © David W. Ball Source. cc-by-nc-sa  
 
[QUÍMICA PRÁCTICA por bobquim](http://bob.webcindario.com/quimprac/)  
Química práctica. General, inorgánica, orgánica, Problemas de estequiometría y concentraciones.  
Análisis cuali y cuantitativo. Identificación de productos, Valoraciones, Calcinaciones, Precipitaciones ...Análisis Químico Instrumental (visible, ultravioleta, Infrarrojo, masas, cromatografía y otros muchos). Procesos químicos, Síntesis. Plásticos  
Copyright bobquim  

[Chemical science and chemical engineering materials. - *waybackmachine*](http://web.archive.org/web/20160109214804/http://baparu.yolasite.com/)  


[Aproximaciones (cinética química), 2011-I, Rafael Moreno Esparza](http://depa.fquim.unam.mx/amyd/archivero/05-aproximaciones_12804.pdf)  

[Física y Química - proyectodescartes.org](https://proyectodescartes.org/descartescms/fisica-y-quimica)  
Libros interactivos  
[Introducción a la Química - proyectodescartes.org](https://proyectodescartes.org/descartescms/fisica-y-quimica/icartesilibri/item/3814-introduccion-a-la-quimica)  
[Química - Volumen I, primera parte - proyectodescartes.org](https://proyectodescartes.org/descartescms/fisica-y-quimica/item/3486-quimica-volumen-i-primera-parte)  
[Química - Volumen I, segunda parte - proyectodescartes.org](https://proyectodescartes.org/descartescms/fisica-y-quimica/item/3487-quimica-volumen-i-segunda-parte)  

  
[Amigos de la Química, Marta Vitores - Canal Youtube](https://www.youtube.com/c/AmigosdelaQu%C3%ADmicaMartaVitores)  

RENA Red Escolar Nacional (Venezuela)  
[RENA Tercera Etapa Química *waybackmachine*](http://web.archive.org/web/20170201154850/http://www.rena.edu.ve/TerceraEtapa/Quimica/index.html)  
[RENA Cuarta Etapa Química *waybackmachine*](http://web.archive.org/web/20170520230006/http://www.rena.edu.ve/cuartaEtapa/quimica/index.html)  
Recursos desglosadas por "etapas", que son rangos de cursos/edades.  

[twitter Hookean1/status/1547132675164610562](https://twitter.com/Hookean1/status/1547132675164610562)  
Chemistry teachers. Happened upon [this Google Drive which contains lots of A-Level and GCSE resources](https://drive.google.com/drive/folders/1EsGkeUVepPSE_ZdaufRzbKGLar1J0pvx).  

[twitter Hookean1/status/1559167796399030272](https://twitter.com/Hookean1/status/1559167796399030272)  
🧪Chemistry teachers🧪 Please find below [all my chemistry shares up until August 2022. Thousands of ppts, booklets, worksheets](https://docs.google.com/spreadsheets/d/1Y8mVI6MchdnopEyS_SLYC32RUmXKM2YV/edit?rtpof=true&sd=true#gid=1693004565). Would appreciate a retweet, as I am mostly followed by physicists]  

[Studyit L3 Chemistry](https://studyit.govt.nz/Chemistry/level/13)  

[Revista de Educación Química - unam.mx](https://www.revistas.unam.mx/index.php/req)  

[Libros Química 2ed volumen 1 y 2 - openstax.org](https://openstax.org/subjects/ciencia#Quimica)   © 1999-2024, Rice University. Salvo que se indique lo contrario, los libros de texto de este sitio están autorizados conforme a la Creative Commons Attribution 4.0 International License.

