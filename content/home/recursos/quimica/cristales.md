
# Cristales

Los cristales enlazan con estructura de la materia, propiedades de sustancias (cristales iónicos, cristales covalentes, cristales de sustancias moleculares y cristales metálicos)   
También enlaza con geología con aspectos como las redes de Bravais: se citan en apuntes de enlace Química de 2º Bachillerato.   

[Redes de Bravais - wikipedia](https://es.wikipedia.org/wiki/Redes_de_Bravais)  
[Sistemas cristalinos - wikipedia](https://es.wikipedia.org/wiki/Sistema_cristalino)  

Creo página separada para recopilar enlaces como otras, inicialmente una imagen de los cristales de bismuto.  

[Bismuto - mundodoscristais.com](http://www.mundodoscristais.com/tag/bismuto/)  
![](http://www.mundodoscristais.com/wp-content/uploads/2015/07/mineral-bismuto-1.jpg)  
Se pueden comprar por internet.  

[Visualized: How Snowflakes are Formed - visualcapitalist.com](https://www.visualcapitalist.com/how-snowflakes-are-formed/)  
![](https://www.visualcapitalist.com/wp-content/uploads/2022/11/structure-of-snowflakes.png)  

[twitter FrauValenzuela/status/1689675829024890892](https://twitter.com/FrauValenzuela/status/1689675829024890892)  
![](https://pbs.twimg.com/media/F3LvNjIXoBQSRgl?format=jpg)  
Esta la podéis hacer en casa. Instrucciones:  
1. Se mezcla bien sal común con colorante alimentario. Yo usé un recipiente con tapa y agité hasta el agotamiento.  
2. Se prepara una disolución sobresaturada en agua. En unos 50 mL de agua se añade sal hasta que no se disuelva más...  
... después de agitar un rato.   
3. Se separa el sólido del líquido. Por filtración podría usarse un filtro de café o por decantación dejando reposar y trasvasar parte del líquido de la parte de arriba. Es mejor filtrar, porque siempre queda alguna partícula en suspensión.  
6. Recoger los cristales. Si dejas que lo hagan con las manos mancha, mejor cuchara. Se pueden lavar con la misma disolución saturada, no con agua.  
7. Disfrutar de los cristalitos.  

[Solución a problemas – cristalización - quimicafacil.net](https://quimicafacil.net/tecnicas-de-laboratorio/solucion-a-problemas-cristalizacion/)  

