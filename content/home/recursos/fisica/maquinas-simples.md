# Recursos máquinas simples

Concepto de máquina simple   
[Máquina simple - wikipedia.org](https://es.wikipedia.org/wiki/M%C3%A1quina_simple)  

Las máquinas aparecen en  [currículo 2º ESO LOMCE](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso)  y  [currículo 3º ESO LOMCE](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce)  (en Madrid en contenidos solamente de 2º), a veces se trataban algunas como palancas en 4º ESO LOE por citar currículo LOE equilibrio. 
>*Máquinas simples*  
*4. Valorar la utilidad de las máquinas simples en la transformación de un movimiento en otro diferente, y la reducción de la fuerza aplicada necesaria.*  
*4.1. Interpreta el funcionamiento de máquinas mecánicas simples considerando la fuerza y la distancia al eje de giro y realiza cálculos sencillos sobre el efecto multiplicador de la fuerza producido por estas máquinas.*  

No se citan explícitamente nombres de máquinas, pero se puede asumir palanca al hablar de eje de giro y efecto multiplicador, aunque es habitual tratar las máquinas simples clásicas: palanca, plano inclinado, torno, polea (y polipasto), cuña y tornillo.  
Se ponen enlaces o se crea una página separada para algún tipo de máquina: los planos inclinados y las poleas tienen enlace con problemas de Física de 1º Bachillerato  

[Máquinas simples (pdf) - fisquiweb.es](http://www.fisquiweb.es/Apuntes/Apuntes2/Maquinas.pdf)  

[Máquinas simples - edu.xunta.es](https://www.edu.xunta.es/espazoAbalar/sites/espazoAbalar/files/datos/1464947673/contido/2_mquinas_simples.html)  
Manuel Torres Búa cc-by-nc-sa  
 
[Mecanismos (pdf) cc-by-nc-sa - mestreacasa.gva.es](http://rec.mestreacasa.gva.es/webzip/0d170a55-cd76-436d-a6c2-c238d724ca10/pdfs/recursos.pdf)  

[Máquinas simples - palancapolea54.blogspot.com](http://palancapolea54.blogspot.com/2010/05/maquinas-simples_17.html) 

Se puede asociar a [sólido rígido](/home/recursos/fisica/mecanica/solido-rigido) y [estática](/home/recursos/fisica/mecanica/estatica).   

##   [Plano inclinado](/home/recursos/fisica/recursos-dinamica/recursos-plano-inclinado) 


##  Palancas
Son tratadas también en Tecnología y se pueden encontrar recursos asociados  
[Mecanismos - ejercicios palancas 3º ESO pdf - iesparquegoya.es](http://iesparquegoya.es/files/tecnologia/mecanismos/ejercicios%20palancas%203%C2%BA%20eso.pdf)  

[Ejercicios palancas 3º ESO - tecnoponteareas.blogspot.com.es](http://tecnoponteareas.blogspot.com.es/2014/03/ejercicios-de-palancas-3-eso.html)  

[Ejercicios mecanismos 3º ESO pdf - aliciadiazcobo.files.wordpress.com](https://aliciadiazcobo.files.wordpress.com/2014/02/ejercicios-de-mecanismos-3c2ba-eso.pdf)  

[La palanca - edu.xunta.es](https://www.edu.xunta.es/espazoAbalar/sites/espazoAbalar/files/datos/1464947673/contido/21_la_palanca.html)   
Manuel Torres Búa cc-by-nc-sa  

[twitter disfrutalacienc/status/1604092495771623424](https://twitter.com/disfrutalacienc/status/1604092495771623424)  
Al menos conocen la ley de la palanca.  
![](https://pbs.twimg.com/media/FkLhmrzWQAEU6Q-?format=jpg)  

Tipos de Palancas Anatómicas: 1º, 2º y 3º grado/género/clase | Biomecánica - Physiotopics  
[![](https://img.youtube.com/vi/iEf0YPJSEHI/0.jpg)](https://www.youtube.com/watch?v=iEf0YPJSEHI "Tipos de Palancas Anatómicas: 1º, 2º y 3º grado/género/clase | Biomecánica - Physiotopics") 

###  Simulaciones
 ![](https://phet.colorado.edu/sims/html/balancing-act/latest/balancing-act-900.png)  
 [Ley de equilibrio - phet.colorado.edu](https://phet.colorado.edu/sims/html/balancing-act/latest/balancing-act_es.html)  
 
 [Principio de la palanca - walter-fendt.de](http://www.walter-fendt.de/html5/phes/lever_es.htm)  
 
 Sencillo/gráfico, flash que funcionan con Ruffle
 [Equilibrios entre fuerzas (swf) - genmagic.net](http://www.genmagic.net/repositorio/albums/userpics/equili2c.swf)    
 [ Masa, fuerza y poleas (swf) - genmagic.net](http://genmagic.net/repositorio/albums/userpics/maspes4c.swf)  
 
 [Ejercicio dinámica. Sistema con dos masas que cuelgan de una polea ideal situada en lugares con diferentes valores de g. - educaplus.org](https://www.educaplus.org/game/ejercicio-de-dinamica-4)  
 
