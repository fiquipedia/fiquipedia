
# Recursos Física y Química y Medio Ambiente

Se crea apartado aparte porque enlaza con varias materias y varios cursosPongo nombre general "Medio Ambiente" pero enlaza con más ideas: ecología, sostenibilidad, reciclaje, huella de carbono, cambio climático, contaminación, residuos...   
Se puede asociar a [Energía](/home/recursos/energia) 


**Pendiente revisar con LOMLOE**  
Por ejemplo  
- [Física y Química 3º ESO LOMCE](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce)  
>Bloque 3 Los Cambios  
>7. Valorar la importancia de la industria química en la sociedad y su influencia en el medio ambiente.  
>7.1. Describe el impacto medioambiental del dióxido de carbono, los óxidos de azufre, los óxidos de nitrógeno y los CFC y otros gases de efecto invernadero relacionándolo con los problemas medioambientales de ámbito global.  
>7.2. Propone medidas y actitudes, a nivel individual y colectivo, para mitigar los problemas medioambientales de importancia global.  
>7.3. Defiende razonadamente la influencia que el desarrollo de la industria química ha tenido en el progreso de la sociedad, a partir de fuentes científicas de distinta procedencia.  
- [Ampliación de Física y Química 4º ESO (LOMCE)](/home/materias/eso/ampliacion-fisica-y-quimica-4-eso/curriculo-ampliacion-fisica-quimica-4-eso-lomce)  
>4. Justificar hábitos de reciclaje.  
>4.1. Respeta y preserva el entorno reciclando tarros y botellas de vidrio.  
>4.2. Toma conciencia del ahorro de energía y materia prima, reduciendo los residuos y la contaminación del aire por reciclado de vidrio.  
>>15. Conocer la importancia del reciclaje del aceite ya usado.  
>15.1. Toma conciencia de la necesidad de reciclar aceite usado.  
>15.2. Valora la obtención de jabón como un método de reciclaje conocido desde la antigüedad y utilizado todavía en la actualidad.  
>30. Conocer los problemas medioambientales que pueden surgir.  
>30.1. Toma conciencia de la necesidad del reciclaje de los plásticos.  
- [Cultura Científica 4º ESO](/home/materias/eso/cultura-cientifica-4-eso/curriculo-cultura-cientifica-4-eso)  
>Bloque 5. Nuevos materiales  
>4. Los polímeros.  
>- Los polímeros sintéticos y el medio ambiente.  
>2. Conocer los principales métodos de obtención de materias primas y sus posibles repercusiones sociales y medioambientales.  
>2.1. Describe el proceso de obtención de diferentes materiales, valorando su coste económico, medioambiental y la conveniencia de su reciclaje.  
>2.2. Valora y describe el problema medioambiental y social de los vertidos tóxicos.  
>2.3. Reconoce los efectos de la corrosión sobre los metales, el coste económico que supone y los métodos para protegerlos.  
>2.4. Justifica la necesidad del ahorro, reutilización y reciclado de materiales en términos económicos y medioambientales.   
- [Ciencias aplicadas a la actividad profesional 4º ESO (LOMCE)](/home/materias/eso/ciencias-aplicadas-a-la-actividad-profesional-4-eso/curriculo-ciencias-aplicadas-a-la-actividad-profesional-4-eso-lomce)  
>*Bloque 2. Aplicaciones de la ciencia en la conservación del medio ambiente**1. Contaminación: concepto y tipos.*  
>2. Contaminación del suelo.  
>3. Contaminación del agua.  
>4. Contaminación del aire.  
>5. Contaminación nuclear.  
>6. Tratamiento de residuos.  
>7. Nociones básicas y experimentales sobre química ambiental.  
>8. Desarrollo sostenible.  
- Química 2º Bachillerato LOE  
>— La corrosión de metales y su prevención. Residuos y reciclaje.  

También se puede enlazar con pensamiento científico en general, dado que hay negacionistas
[Cultura Científica 1ºº Bachillerato](/home/materias/bachillerato/cultura-cientifica-1-bachillerato/curriculo-cultura-cientifica-1-bachillerato)  
>*Bloque 1. Procedimientos de trabajo*
>1. Obtener, seleccionar y valorar informaciones relacionadas con la ciencia y la tecnología a partir de distintas fuentes de información.  
>2. Valorar la importancia que tiene la investigación y el desarrollo tecnológico en la actividad cotidiana.  
>3. Comunicar conclusiones e ideas en soportes públicos diversos, utilizando eficazmente las tecnologías de la información y comunicación para transmitir opiniones propias argumentadas.  


## Juegos
 [Misión Posible - Salvar la Tierra | CRUZ ROJA JUVENTUD](http://www.cruzroja.es/juego_cambio_climatico/mision_posible.html)   

## Simulaciones
Simulación sobre efecto invernadero [greenhouse](https://phet.colorado.edu/es/simulation/legacy/greenhouse)   

[Greenhouse effect - PhET](https://phet.colorado.edu/sims/html/greenhouse-effect/latest/greenhouse-effect_en.html)  

## Materiales generales
No acotados a cambio climático  
 [Recursos Educativos sobre Energía y Medio Ambiente - rinconeducativo.org](https://rinconeducativo.org/es/)  
 [RECURSOS EDUCATIVOS - Teachers For Future Spain](https://teachersforfuturespain.org/recursos-educativos/)   
 [El Proyecto - Consejería de Educación y Deporte](http://www.juntadeandalucia.es/educacion/portals/web/aldea/proyectos/ecoescuela/el-proyecto)   
 [https://www.educaclima.com/](https://www.educaclima.com/)   
 [https://www.fuhem.es/2019/03/22/nuevos-materiales-curriculares-con-mirada-ecosocial-e-interdisciplinar/](https://www.fuhem.es/2019/03/22/nuevos-materiales-curriculares-con-mirada-ecosocial-e-interdisciplinar/)   
 [Educación Primaria. Fridays for Future. Innovación Educativa: Movilízate por el clima | Blog Escuelas en red | EL PA&Iacute;S](https://elpais.com/elpais/2019/09/09/escuelas_en_red/1568058031_075820.html)  [global-express-actualidad-en-aula](https://www.oxfamintermon.org/es/educacion/noticia/global-express-actualidad-en-aula)   

## Cambio climático  

Se puede relacionar con el uso de energías: combustibles fósiles y energía nuclear (según crece puedo tener apartados y enlaces separados)  
También con sostenibilidad y el propio modelo de crecimiento del capitalismo  
[twitter jjnucleares/status/1073129927233413120](https://twitter.com/jjnucleares/status/1073129927233413120)  
Ya es hora de dejar atrás este tipo de escepticismosLa concentración de CO2 en la atmósfera está en su máximo de los últimos 400000 años (imagen)Existe un 97% de consenso científico sobre la responsabilidad antropogénica en el calentamiento global ⬇️  
[![](https://pbs.twimg.com/media/DuSF-hLX4AEEa4o.jpg "") ](https://pbs.twimg.com/media/DuSF-hLX4AEEa4o.jpg)   
Consensus on consensus: a synthesis of consensus estimates on human-caused global warmingPublished 13 April 2016 • © 2016 IOP Publishing LtdEnvironmental Research Letters, Volume 11, Number 4 [Consensus on consensus: a synthesis of consensus estimates on human-caused global warming - IOPscience](http://iopscience.iop.org/article/10.1088/1748-9326/11/4/048002)   
29 marzo 2019 Global Warming Changes in Annual Average Temperature Distribution (51 s)  
 [![Global Warming Changes in Annual Average Temperature Distribution - YouTube](https://img.youtube.com/vi/xWpTGbZhZfQ/0.jpg)](https://www.youtube.com/watch?v=xWpTGbZhZfQ)   
  
 [Diane Burrell&#039;s answer to Why do so many people blindly accept the global warming hypothesis without making any unbiased research? - Quora](https://www.quora.com/Why-do-so-many-people-blindly-accept-the-global-warming-hypothesis-without-making-any-unbiased-research/answer/Diane-Burrell-1)   

 [The Best Sites To Learn About Climate Change](http://larryferlazzo.edublogs.org/2009/12/04/the-best-sites-to-learn-about-climate-change/) cita  [The Best Sites To Introduce Environmental Issues Into The Classroom](http://larryferlazzo.edublogs.org/2008/07/19/the-best-sites-to-introduce-environmental-issues-into-the-classroom/)   
 [climate-change-activities#resources](	)   
 [Educational Activities – Climate Change: Vital Signs of the Planet](https://climate.nasa.gov/educationalActivities/)  
 
En 2022 surge la "ideología" del término "cambio climático" en los currículos LOMLOE  
[La Comunidad de Madrid elimina conceptos con "carga ideológica" (como 'ciudadanía resiliente' o 'emergencia climática') del currículo de Bachillerato y limita la titulación con suspensos](https://www.europapress.es/madrid/noticia-comunidad-elimina-conceptos-carga-ideologica-curriculo-bachillerato-limita-titulacion-suspensos-20220504130644.html)  

[twitter ecosdelfuturo/status/1522939357023379456](https://twitter.com/ecosdelfuturo/status/1522939357023379456)  
El término "Emergencia climática" tiene tanta carga ideológica como cualquier otra definición, sobre todo si implica estimar probabilidades.   
Después nos reíamos del estado de Indiana por legislar el valor de pi.  
Definición de emergencia climática  
[Puntos críticos climáticos: demasiado arriesgado apostar en su contra - investigacionyciencia](https://www.investigacionyciencia.es/noticias/puntos-crticos-climticos-demasiado-arriesgado-apostar-en-su-contra-18093)  
> Emergencia: hacer los números  
> Definimos emergencia (E) como el producto del riesgo y la urgencia. Los actuarios definen el riesgo (R) como la probabilidad (p) multiplicada por el daño (D). La urgencia (U) se define en las situaciones de emergencia como el tiempo de reacción ante una alerta (τ) dividido por el tiempo de que se dispone aún para evitar un mal resultado (T). Por lo tanto:  
> E = R x U = p x D x τ/T  
>Se está en una situación de emergencia si tanto el riesgo como la urgencia son altos. Si el tiempo de reacción es mayor que el tiempo de que se dispone aún (τ/T > 1), es que hemos perdido el control.  

[twitter ecosdelfuturo/status/1516865038878978051](https://twitter.com/ecosdelfuturo/status/1516865038878978051)  
(forma parte de un hilo, cita la misma definicón emergencia climática)  
Todo aquel que afirmen que no estamos ante una emergencia climática tendrá que mostrar que el riesgo es bajo y no es urgente una descarbonización profunda y rápida. Toda la ciencia que conocemos apuntan a lo contrario.  

[twitter VaryIngweion/status/1564993033636122626](https://twitter.com/VaryIngweion/status/1564993033636122626)  
Por último. Aunque a @FranHervias No le guste que se diga, el cambio climático que estamos sufriendo actualmente ES antropogenico. Negarlo es como decir que la tierra es plana o que la evolución biológica es mentira.  
Más información:  
[¿Somos los humanos responsables del cambio climático? - muyinteresante.es](https://www.muyinteresante.es/ciencia/articulo/somos-los-humanos-responsables-del-cambio-climatico-141653320398)  

[twitter VaryIngweion/status/1565049689266962435](https://twitter.com/VaryIngweion/status/1565049689266962435)  
Y para acabar con este hilo, lo que menos les gusta a los negacionistas del cambio climático como @FranHervias: las referencias científicas:  

[No evidence for globally coherent warm and cold periods over the preindustrial Common Era - nature](https://nature.com/articles/s41586-019-1401-2)  

[A global multiproxy database for temperature reconstructions of the Common Era - nature](https://nature.com/articles/sdata201788)  

[Consistent multidecadal variability in global temperature reconstructions and simulations over the Common Era - nature](https://nature.com/articles/s41561-019-0400-0)  

[IPCC Climate Change 2021 The Physical Science Basis](https://ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Full_Report.pdf)  PDF de 3949 páginas  

[¿Qué y quiénes están impulsando a las temperaturas globales a récords históricos en 2023? - tiempo.com](https://www.tiempo.com/ram/temperaturas-globales-a-records-historicos.html)  
Julio de 2023 ha sido el mes más cálido registrado en la Tierra en los registros modernos y ha venido precedido por otros tantos meses de records tèrmicos atmosféricos y marinos ¿Quién y por qué se están impulsando estos récords de temperaturas? ¿Cuánto contribuye el vapor de agua del volcán Hunga Tonga?  
![](https://services.meteored.com/img/article/temperaturas-globales-a-records-historicos-1692158753144_1024.png)  

[July 2023 Temperature Update - berkeleyearth.org](https://berkeleyearth.org/july-2023-temperature-update/)  
 
## Greenwashing

[Greenwashing: 9 ejemplos destacados recientes](https://thesustainableagency.com/es/blog/ejemplos-de-greenwashing/)  
> Greenwashing es la práctica de hacer que las marcas parezcan más sostenibles de lo que realmente son. Puede involucrar tácticas de marketing cínicas, trucos de relaciones públicas equivocados o simplemente cambiar el packaging de un producto existente, mientras se continua usando ingredientes o prácticas insostenibles. Es una forma de que las empresas parezcan que se preocupan al mismo tiempo que aumentan sus márgenes de beneficio, ya que son plenamente conscientes de que las personas con conciencia ecológica están dispuestas a gastar más dinero en productos sostenibles.  

## Ciudades sostenibles

[Estas son las diez ciudades más sostenibles del planeta - nationalgeographic](https://viajes.nationalgeographic.com.es/lifestyle/estas-son-las-diez-ciudades-mas-sostenibles-del-planeta_18332)  

[Shade is an essential solution for hotter cities - nature.com](https://www.nature.com/articles/d41586-023-02311-3)  
> One of the most effective ways to keep people cool is often neglected in urban planning. Cities must work to provide cover and reverse the ‘shade deserts’ common in low-income communities.   

[De una calle sin árboles a otra con ellos... Así cambia la temperatura en plena ola de calor - lasexta](https://www.lasexta.com/noticias/sociedad/de-una-calle-sin-arboles-a-otra-con-ellos-asi-cambia-la-temperatura-en-plena-ola-de-calor_201707145968cb0b0cf20d3cbe83e6c4.html)  

[Sombras en los desiertos urbanos: lecciones de dos calles de Córdoba para combatir las temperaturas extremas - elpais](https://elpais.com/clima-y-medio-ambiente/2023-08-13/sombras-en-los-desiertos-urbanos-lecciones-de-dos-calles-de-cordoba-para-combatir-las-temperaturas-extremas.html)  

[Da Sombra a Watsonville Para Ayudar a Reducir el Efecto de Isla de Calor Urbano - watsonvillecommunityforest](https://www.watsonvillecommunityforest.org/es/involucrese/da-sombra-a-watsonville)  

 
## Agotamiento del combustibles fósiles  / Decrecimiento

[The Oil Crash](https://crashoil.blogspot.com/)  Antonio Turiel 

 [Ciencia: Nos estafan con el diésel: la excusa es que contamina, pero en realidad se acaba - elconfidencia.com](https://www.elconfidencial.com/amp/economia/2018-12-10/pico-del-petroleo-antonio-turiel-diesel-contaminacion_1694062/)   
"Nos estafan con el diésel: la excusa es que contamina, pero en realidad se acaba"Algo pasa con el diésel. Durante el último año, y solo en español, se han publicado más de 600.000 noticias que contienen las palabras 'diésel' y 'contaminación'. El combustible estrella desde los noventa —recuerden aquel "diésel gustazo" de Salas y Summers— se ha transformado, a partir del 'dieselgate', en una catástrofe medioambiental contra la que hay que tomar medidas inmediatamente.  

['Petrocalipsis' Antonio Turiel: "El decrecimiento económico es inevitable, tendremos que decidir si lo hacemos por las buenas o por las malas" - publico.es](https://www.publico.es/entrevistas/petrocalipsis-antonio-turiel.html)   
 
[Programa para la Innovación Educativa, Aldea / Proyecto Terral / Recursos - juntadeandalucia.es](http://www.juntadeandalucia.es/educacion/portals/web/aldea/proyectos/terral/recursos)  
 
[El otoño de la civilización (y la ruptura de la cadena de suministros) - ctxt.es](https://www.ctxt.es/es/20210901/Firmas/37191/civilizacion-escasez-trigo-petroleo-gas-suministros-antonio-turiel-juan-bordera.htm)   

[La crisis del carbón - crashoil.blogspot.com](https://crashoil.blogspot.com/2021/10/la-crisis-del-carbon.html)  

[No es solo la luz: los problemas de suministro disparan los precios y paralizan las fábricas - elconfidencial.com](https://www.elconfidencial.com/economia/2021-09-18/problemas-suministro-disparan-precios-cierran-fabricas_3291264/)  
El mundo se enfrenta a una escasez de oferta no vista en medio siglo por las restricciones a la producción y la paralización del transporte marítimo, con efectos que ya se notan en las fábricas española  

[twitter EDocet/status/1452180934376185865](https://twitter.com/EDocet/status/1452180934376185865)  
¿Somos conscientes de que toda la tecnología del coche eléctrico se basa en una materia prima no renovable, el litio? ¿Y que cualquier tecnología no renovable tiene los días contados?  
Ir de cabeza al coche eléctrico es suicida, caro, medioambientalmente un desastre (minería y gestión de baterías usadas), limitado porque no alcanza al transporte de mercancías y no operativo por tiempo de carga / necesidad de puntos de recarga.  
Es mucho más interesante la tecnología del hidrógeno verde, que sí es renovable, usa la mecánica de los motores de combustión conocida, puede emplearse en transporte pesado y solo tiene el inconveniente de que hay que crear plantas de producción e hidrogeneras.  
Mientras llega esto último, en un país como España, donde el 70 % del parque automovilístico duerme en la calle y no tiene acceso a un punto de recarga durante la noche, parece más inteligente optar por los híbridos: bajas contaminación en ciudades y emisión de CO2.  
Esta sería una política de "transición ecológica" realista. Como es realista potenciar las nucleares de última generación mientras se desarrolla toda la capacidad de las renovables y como apoyo a éstas.  
Démonos cuenta de que el rechazo a las nucleares es más una posición ideológica que una basada en los hechos. Un prejuicio más que un análisis de los datos. España debería actuar YA con visión a largo plazo y con criterios ambientales informados y serios.  
La crisis energética que está comenzando ahora ya es inevitable, serán las decisiones que se tomen ahora las que condicionarán el futuro a medio y largo plazo. Permitidme que sea muy pesimista.  
Un par de enlaces interesantes. Primero las pruebas de un camión híbrido que usa hidrógeno y que han probado en un lugar conocido, el puerto de Los Ángeles, y no como el Nikola que fue en una cuesta abajo: Kenworth Zero Emission Cargo Transport (ZECT)  
Ver en [Hidrógeno](/home/recursos/quimica/hidrogeno) 

Y este artículo de Jesús Cacho que recoge, entre otras cosas, lo que se está haciendo en Europa:  
[La nuclear vuelve con fuerza al escenario energético - vozpopuli.com](https://www.vozpopuli.com/opinion/nuclear-escenario-energetico.html)  

[![](https://img.youtube.com/vi/NmNwINPFEDs/0.jpg)](https://www.youtube.com/watch?v=NmNwINPFEDs&t=1637s "Antonio Turiel. El futuro de la energía. Fundación Cultural 1 de Mayo")   

[twitter limites1972/status/1527259527241994246](https://twitter.com/limites1972/status/1527259527241994246)  
1/ A raíz de la entrevista al astrofísico Tom Murphy en "The Great Simplification" me he puesto a reflexionar en las causas por las que es tan difícil comunicar el cénit de la energía fósil y, en general, los límites al crecimiento. 👇🏽 HILO, me temo  
2/ Empecemos con aquellas causas relacionadas con cómo funciona nuestro cerebro: 1️⃣ No se nos da bien pensar en sistemas. Podemos entender relaciones causales simples pero no tanto si son complejas, con  interconexiones o bucles de retroalimentación.  
3/ Los límites del crecimiento es un asunto complejo que, como muchos otros, se entiende mejor si se hace desde la dinámica de sistemas, como hizo el equipo del MIT que preparó el informe original.  
4/ 2️⃣ Como solía decir Alber Bartlett, no se nos da bien entender la función exponencial, que es clave para comprender la peligrosa trayectoria de crecimiento, explotación y consumo de los recursos en el sistema capitalista actual.  
5/ 3️⃣ A algunas personas no se les dan bien las matemáticas y los números, en general. Esto hace posible problemas de "anumerismo" como, por ejemplo, dejarse llevar por noticias de descubrimientos de yacimientos con millones de barriles de petróleo que en realidad...  
6/ ...no equivalen mas que a unos cuantos días de consumo mundial. Esto no hace a estas personas más listas o tontas que las otras, por cierto, sino simplemente una inteligencia no tan orientada a los números.  
7/ 4️⃣ Somos menos racionales de lo que creemos. Nuestra razón se deja llevar por cosas como sesgos cognitivos, relatos y mitos que nos sirven para "entender" el mundo, prejuicios, estado de ánimo o, simplemente, cansancio. Y como dice Tom Murphy, mucha gente necesita...  
8/ ... *ver y tocar* las cosas, lo cual quizá fuera una ventaja evolutiva en su día pero dificulta la comunicación de temas abstractos como los límites del crecimiento. "Miro por la ventana, el sol brilla y hace buen tiempo, ¿qué me estás contando de cambio climático?"  
9/ Lo que nos lleva al siguiente grupo de causas: 5️⃣ Somos "ciegos a la energía" dice Nat Hagens. Como un pez que vive en el mar y no se da cuenta de que vive rodeado agua, la mayor parte de gente no nos damos cuenta de hasta que punto nuestro modo de vida...  
10/ ...depende del acceso a ingentes cantidades de energía fósil barata y abundante. En su día hice un hilo tratando de explicarlo. Esta falta de percepción de la importancia de la energía fósil barata y abundante hace que en general se menosprecien...  
Los combustibles fósiles (carbón, petróleo y gas) son el gran pilar sobre el que sustenta el modo de vida occidental. Aquí van unos datos que nos ayudan a recordar por qué estos recursos finitos son prácticamente insustituibles: 👇🏽 🧵  
11/ ...los problemas derivados de su pico de producción. Lo que nos lleva a la causa 6️⃣ La complejidad de nuestro sistema y la hiperespecialización asociada al mismo hacen que mucha gente brillante esté centrada en un nicho específico de conocimiento...  
12/ ... sin tener en cuenta una visión más global del conjunto o una visión con una perspectiva temporal más amplia como requiere el agotamiento de los recursos y los límites al crecimiento. 7️⃣ Cortoplacismo. A dos niveles, el primero tiene que ver con que la planificación...  
13/ ... se hace a muy corto plazo tanto en el sector público (4-8 años en las democracias liberales) como en el sector privado (año fiscal, trimestre o siguiente junta de accionistas). El segundo nivel tiene que ver con la muy errónea idea de que estos asuntos...  
14/ ... (cambio climático, cénit energético) no tendrán un impacto en nuestra generación sino en las de nuestros descendientes.  
Las razones expuestas hasta ahora podrían indicar que gente con un perfil más científico-técnico tendría más facilidad para entender estos temas. 👇🏽  
15/ Sin embargo, sabemos que esto no es así y que gente con muy buena formación científica tiene parecidos problemas para entender el atolladero en el que nos encontramos. Lo que nos lleva a 8️⃣ Mito del Progreso: tecno-optimismo. El impresionante desarrollo tecnológico...  
16/ ... de los 2-3 últimos siglos se ha producido por dos factores: (i) el ingenio humano de la mano de la Ciencia y (ii) el acceso a energía fósil abundante, barata y lista para usar. Los dos requisitos son imprescindibles para el desarrollo tecnológico y,...  
17/ ...sin embargo, el Mito del Progreso, nos ha convencido de que *sólo* la ciencia o *sólo* el ingenio humano son suficientes. Este antropo-optimismo o tecno-optimismo hace que mucha gente responda con un "algo inventarán" o cualquiera de sus variantes.  
18/ 9️⃣ Mito del Progreso - sesgo del triunfador. No sé siquiera si es un sesgo real, pero sería el siguiente: El ser humano ha conseguido transformar completamente el mundo. Hoy hacemos cosas que hace 150 años nos parecían imposibles...  
19/ ... ¡Incluso llegamos a la Luna! ¿No somos formidables? De nuevo, el ingenio humano es formidable pero tiene límites. La ciencia tiene límites (ya hablaremos de esto otro día). El triunfo pasado hace que creamos que todo lo podremos y que todo saldrá bien. Pero no es así...  
20/... en ningún sitio está escrito que todo vaya a salir bien y que siempre salgamos triunfantes. Es un falso sentido de seguridad proporcionado por dos siglos de energía abundante y barata. Necesitamos más humildad como especie. 🔟 Psicología - Hablar de estos temas...  
21/ ... está muy mal visto. Sobretodo en algunos círculos científicos y de divulgación. Se ven como temas pesimistas y hasta anticientíficos. Casi se consideran como ataques a la ciencia por poner en cuestión su "omnipotencia". Es la misma razón por la que...  
22/ ...en ambientes "escépticos" y de divulgación se suele hablar poco de cambio climático y de otros problemas ecológicos pero mucho de telescopios, sondas espaciales y robots. 1️⃣1️⃣ Miedo. Seamos honestos, incluso los que llevamos décadas hablando de estos temas...  
23/ ...sentimos en ocasiones el frío de mirar al abismo de un futuro muy oscuro. El miedo paraliza nuestro cerebro: "todo saldrá bien", "algo inventarán", "el ingenio humano", "si se invierte en ciencia". Sin embargo, una vez que se pasa la fase de negación y duelo...  
24/ ... y se aceptan las cosas como son (las que no podemos cambiar) podemos ponernos manos a la obra en lo que realmente sí se puede cambiar, por ejemplo nuestro modelo de crecimiento infinito. Y créanme que no conozco gente más activista que los que han aceptado esto.  

[twitter Shine_McShine/status/1555874108663029761](https://twitter.com/Shine_McShine/status/1555874108663029761)  
1/ Tras el (en mi opinión, desafortunado) hilo sobre el decrecimiento, @deborahciencia plantea el debate entre decrecentismo y "desarrollo sostenible".  
Hoy vengo a aportar un poco de evidencia empírica al respecto.  
Dentro hilo  
2/ Según la definición que da aquí Deborah, el desarrollo sostenible es aquel modelo que permite simultáneamente el crecimiento económico y la reducción en la utilización de recursos e impacto ambiental mediante la innovación científica y tecnológica.  

> El «desarrollo sostenible» lleva implícita una etiqueta de progreso y bienestar. También de crecimiento económico. La innovación científica y tecnológica se considera la clave para lograr hacer un uso responsable y eficiente de los recursos.  

3/ En ecología este fenómeno recibe el nombre de desacoplamiento; el desligar el crecimiento del PIB del uso de recursos y/o del impacto ambiental, que históricamente han estado fuertemente correlacionados.  
Hay dos tipos de desacoplamiento: relativo y absoluto.  
4/ El desacoplamiento relativo implica que si sube el PIB también sube la utilización de recursos y/o el impacto ambiental, pero sube menos que el PIB. Por ejemplo, si el PIB crece un 3% y el uso de recursos un 1,5% donde antes crecía un 3%, esto es un desacoplamiento relativo.  
5/ El desacoplamiento absoluto implica que el PIB puede crecer sin que crezca el uso de recursos o el impacto ambiental e incluso que disminuyan. Por ejemplo, si el PIB crece un 2% y las emisiones de gases de efecto invernadero caen un 1%, esto es un desacoplamiento absoluto.  
6/ La idea del desacoplamiento se remonta a los años 70, pero es a partir de los 90 cuando se le empieza a prestar atención. Según algunos economistas, a medida que las sociedades se desarrollan incrementan su impacto ambiental, PERO.  
7/ Pero al acanzar un umbral de desarrollo (medido en renta per cápita), se produce naturalmente un desacoplamiento y gracias a la innovación se empieza a crecer usando menos recursos y con menor impacto ambiental, lo que se conoce como "Curva Ambiental de Kuznets"  
8/ Este modelo fue adoptado como objetivo por varios países en la década de los 90, por los países de la OCDE en 2001 y por el Banco Mundial en 2012. En teoría, la meta marcada era lograr un desarrollo sostenible que posibilitara seguir creciendo sin incinerar el planeta.  
9/ Llegamos aquí a los informes del IPCC, donde advierten que para lograr mantenernos dentro de un rango de temperaturas que sean compatibles con el normal desarrollo de la actividad humana se tiene que actuar de forma rápida y contundente entre hoy y 2040.  
10/ Para lograr los objetivos marcados por el IPCC el desacoplamiento debe ser: absoluto, global, permanente, rápido y suficiente.  
Quedáos con esto que volveremos a ello más adelante.  
11/ Bien, sabiendo que el desacoplamiento lleva décadas siendo el objetivo de muchos países y un imperativo climático si queremos evitar la subida catastrófica de las temperaturas, ¿qué dice la evidencia empírica al respecto?  
12/ En un contundente y extenso informe desarrollado por Parrique et al. (2019) titulado "Decoupling Debunked" (Desacoplamiento Desmontado), los autores analizan las evidencias disponibles sobre el desacoplamiento. [Lo podéis leer aquí](https://eeb.org/wp-content/uploads/2019/07/Decoupling-Debunked.pdf)  
13/ Las conclusiones del informe son demoledoras: no hay evidencias empíricas de que se esté produciendo el desacoplamiento necesario a nivel global, y la hipótesis de que este vaya a permitir seguir creciendo sin aumentar la presión ambiental es poco realista.  
14/ Los autores analizan varios estudios que hablan de desacoplamiento, tanto relativo como absoluto, encontrándose que buena parte de los estudios que afirman que se ha logrado un desacoplamiento solo lo consideran a nivel local, sectorial o en una ventana temporal muy pequeña.  
15/ Por ejemplo, una economía de un país puede afirmar hacer reducido sus emisiones de GEI manteniendo el crecimiento económico, pero esto ocurre porque han trasladado sus fábricas a un tercer país (offshoring), no por innovación tecnológica (Hardt et al. 2018).  
16/ De la misma manera, muchos países o entes políticos afirman haber logrado un desacoplamiento absoluto en el uso de recursos porque han reducido la producción local, pero si se cuentan las importaciones el desacoplamiento desaparece (Wiedmann et al. 2012).  
17/ Así, los autores afirman que los casos de desacoplamiento absoluto que se han producido hasta la fecha solo lo han hecho a nivel local o regional, pero estos son compensados por un crecimiento en otras partes del mundo.  
No existen evidencias de desacoplamiento global.  
18/ Similarmente hay que considerar el marco temporal. Hay economías que parecen presentar un desacoplamiento, pero cuando se amplía la ventana de tiempo los periodos de desacoplamiento vienen seimpre seguidos de periodos de reacoplamiento o sobreacoplamiento (Zhang & Wang, 2013)  
19/ Los periodos observados de desacoplamiento obedecen en muchos casos a crisis económicas o reorganizaciones políticas y/o industriales y tienden a revertirse pasados unos años.  
No hay evidencias de desacoplamiento permanente.  
20/ El estudio también analiza si es posible lograr el desacoplamiento necesario a corto o medio plazo, solo para acabar concluyendo que es altamente improbable si no imposible (p.33).  
21/ Además del estudio citado, Vadén et al. (2020) publicaron un meta-análisis de 179 artículos científicos publicados en los últimos 30 años sobre el desacoplamiento, llegando a las mismas conclusiones:  
22/ "No hay evidencia de desacoplamiento absoluto de recursos (...) en toda la economía. No hay evidencia del tipo de desacoplamiento necesario para la sostenibilidad ecológica. En ausencia de pruebas sólidas, el objetivo del desacoplamiento se basa en parte en la fe."  
23/ De hecho, parece ser que, en el tema de los recursos, no sólo no hemos conseguido un desacoplamiento sino que desde el año 2000 se ha producido el efecto contrario: consumimos más recursos de lo que crece el PIB (IRS, 2016; Bithas & Kalimeris 2018).  
24/ Recordemos que, para lograr los objetivos climáticos propuestos, deberíamos estar hablandod e un desacoplamiento absoluto, global, permanente, rápido y suficiente (reducir un 5% las emisiones de GEI cada año de aquí al 2040). 
25/ Lo que nos demuestra la evidencia empírica es que esto no se ha producido pese a ser el objetivo de muchos países desde hace décadas, y no tiene visos de producirse, tampoco mediante "innovación científica y tecnológica" (cof cof paradoja de Jevons).  
26/ Al final, es pura lógica capitalista; si puedo crecer un 2% usando la mitad de los recursos que utilizo, ¿por qué no seguir usando esa cantidad de recursos para crecer un 4%? Al fin y al cabo el capitalismo busca la maximización del beneficio.  
27/ Las evidencias parecen bastante claras; el "desarrollo sostenible" no solo no es realista sino que es peligroso al tratarse de una ficción que puede demorar las actuaciones necesarias para evitar las peores consecuencias de la crisis climática.  
28/28 Espero haber arrojado algo de luz al debate, otro día si queréis hablaré del decrecimiento.  

[En contra de la opinión ampliamente extendida, la tecnología no nos salvará del colapso.](https://futurocienciaficcionymatrix.blogspot.com/2023/03/en-contra-de-la-opinion-ampliamente.html)  
[#InfografíaDPL | Electricidad que consumirá la industria TIC en 2030](https://dplnews.com/electricidad-que-consumira-la-industria-tic-en-2030/)  
![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjag1Y8oXJDQK1_unDuzj273sSpl2jak1TIkphYfLa2Ad6Tp87Sb3k3V_0vXpGskRmwZIH_4QDsZYHTW-6zGjXhFFqiPYfphTinbV2FVTdX_MAVgCTbw_VDDBV6VlJjXWANiX__-ErHWLaA_yuV5RuE_55uccTFj3OmCJfdwrPQsWeTVvkl4vNFcZU_xQ/w512-h640/dplnews_electricidad-consumo-TIC_mc250621.jpg)  

## Contaminación
 [Cómo conseguir que la superhidrofobia dure — Cuaderno de Cultura Científica](https://culturacientifica.com/2017/10/18/conseguir-la-superhidrofobia-dure/)   
 

[Contaminación del aire ambiente (exterior) - OMS](https://www.who.int/es/news-room/fact-sheets/detail/ambient-(outdoor)-air-quality-and-health)  
[WHO global air quality guidelines: particulate matter (‎PM2.5 and PM10)‎, ozone, nitrogen dioxide, sulfur dioxide and carbon monoxide](https://www.who.int/publications/i/item/9789240034228)  


## Películas / documentales / vídeos
 Hay documentales generales como "Big Picture", aparece visión global, incendios ...  
 [Big Picture](http://documentary.es/4594-the-big-picture-documental-online)   
Hay vídeos con intervenciones interesantes  
**15 diciembre 2018** 
 #SchoolStrike4Climate #ClimateStrike #Youth4ClimateGreta Thunberg full speech at UN Climate Change COP24 Conference  
 [![](https://img.youtube.com/vi/VFkQSGyeCWg/0.jpg)](https://www.youtube.com/watch?v=VFkQSGyeCWg "Greta Thunberg full speech at UN Climate Change COP24 Conference - YouTube")   
**25 diciembre 2019**  
[![](https://img.youtube.com/vi/RjsLm5PCdVQ/0.jpg)](https://www.youtube.com/watch?v=RjsLm5PCdVQ "'I want you to panic': 16-year-old issues climate warning at Davos") 

### Greta Thunberg
 [Greta Thunberg: La niña sueca que inspira huelgas por el clima en todo el mundo - BBC News Mundo](https://www.bbc.com/mundo/noticias-internacional-47242837)   

### Consumo y sostenibilidad
 [The Story Of Stuff](http://storyofstuff.org/)   
 [Comprar, tirar, comprar - rtve.es](http://www.rtve.es/television/documentales/comprar-tirar-comprar/)   

### Cambio climático
Una verdad incómoda (2006)  
[(http://www.imdb.com/title/tt0497116/](http://www.imdb.com/title/tt0497116/)   

 [Animations Show the Melting Arctic Sea Ice, and What the Earth Would Look Like When All of the Ice Melts](http://www.openculture.com/2017/05/what-the-earth-would-look-like-when-all-of-the-ice-melts.html)   
 [twitter WIRED/status/946063749609377794](https://twitter.com/WIRED/status/946063749609377794) There's no way to misinterpret the message of this map. And that's the point.  
 [How Quickly Climate Change Is Accelerating, in 167 Maps](https://www.wired.com/2016/08/quickly-climate-change-accelerating-167-maps/)   
  
 [Crisis climática | Los tuits de los científicos sobre el calor en el Ártico que te van a meter el miedo en el cuerpo  - El Salto - Edición General](https://www.elsaltodiario.com/cambio-climatico/tuits-cientificos-calor-artico-miedo-cuerpo)   
 
 [theguardian.com Shell and Exxon's secret 1980s climate change warnings, Newly found documents from the 1980s show that fossil fuel companies privately predicted the global damage that would be caused by their products.](https://www.theguardian.com/environment/climate-consensus-97-per-cent/2018/sep/19/shell-and-exxons-secret-1980s-climate-change-warnings)
 
 [theguardian.com Just 100 companies responsible for 71% of global emissions, study says. A relatively small number of fossil fuel producers and their investors could hold the key to tackling climate change](https://www.theguardian.com/sustainable-business/2017/jul/10/100-fossil-fuel-companies-investors-responsible-71-global-emissions-cdp-study-climate-change)
 
[twitter ProfMarkMaslin/status/1422457517494448137](https://twitter.com/ProfMarkMaslin/status/1422457517494448137)  
![](https://pbs.twimg.com/media/E72VkfsXMAM9rU2?format=jpg)
[Climate Dashboard](https://www.metoffice.gov.uk/hadobs/monitoring/dashboard.html)

[Frenar el cambio climático. Un reto de todos - aragon.es](https://www.aragon.es/-/frenar-el-cambio-climatico.-un-reto-de-todos)  

[Guía didáctica Terral. Proyecto de educación ambiental frente al cambio climático. - juntadeandalucia.es](https://www.juntadeandalucia.es/medioambiente/portal_web/web/temas_ambientales/educacion_ambiental_y_formacion_nuevo/aldea/programas/kiotoeduca/recursos_educativos/staticfiles/guia_didactica_Terral.pdf)  

[Guías didácticas de educación ambiental de la junta de Andalucía. 1. Educación Ambiental y Cambio Climático](http://aeclim.org/wp-content/uploads/2016/01/guia-didactica-ed-ambiental-y-cambio-climatico.pdf)  

[Actividades Didácticas sobre Cambio Climático. Centro Nacional de Educación Ambiental - CENEAM. Ministerio para la Transición Ecológica y el Reto Demográfico](https://www.miteco.gob.es/es/ceneam/recursos/mini-portales-tematicos/Cclimatico/actdida_cc.aspx)  

[Canvi climàtic a l’aula: recull de 8 guies didàctiques - fundesplai.org](https://escoles.fundesplai.org/blog/educacio-ambiental/canvi-climatic-a-laula-guies-didactiques/)  

[Guías Didácticas Ecozine Film Festival 2017 cambio climático - zaragoza.es](http://www.zaragoza.es/contenidos/medioambiente/cda/guia-didactica-2017-cambioclimatico.pdf)  


[Video: Climate Spiral](https://climate.nasa.gov/climate_resources/300/video-climate-spiral/)  
[![Climate Spiral](https://img.youtube.com/vi/jWoCXLuTIkI/0.jpg)](https://www.youtube.com/watch?v=jWoCXLuTIkI "Climate Spiral")   

[twitter migusant/status/1554726540193271808](https://twitter.com/migusant/status/1554726540193271808)  
Y tú, ¿qué tipo de retardista climático eres?  
![](https://pbs.twimg.com/media/FZN_ipGXkAACwWJ?format=jpg)  

### Reciclaje
“Mitos de Reciclajes. Falsos” Hazte Eco. 12min [![Acaba con los mitos del reciclaje. #HazteEco - YouTube](https://img.youtube.com/vi/xnN4qC97zhM/0.jpg)](https://www.youtube.com/watch?v=xnN4qC97zhM)   
“Reducir, recuperar, reciclar”. Campaña Medioambiental. 16min  
[![Reducir, Recuperar, Reciclar. Reportaje Medioambiental. - YouTube](https://img.youtube.com/vi/z8l0USIklko/0.jpg)](https://www.youtube.com/watch?v=z8l0USIklko)   
Campaña de Reciclaje. Diputación de Guadalajara. 13min [![3R - Campaña de reciclaje - YouTube](https://img.youtube.com/vi/i5SNWCJG4Tw/0.jpg)](https://www.youtube.com/watch?v=i5SNWCJG4Tw)   
La huella Ecológica: reciclar las latas. “Para Todos la 2” 9:16 min [](http://www.rtve.es/alacarta/videos/para-todos-la-2/para-todos-2-huella-ecologica-reciclar-latas/2126690/)   
Vídeos virales para concienciar [Cinco vídeos virales que te engancharán al reciclaje](http://elasombrario.com/cinco-videos-virales-que-te-engancharan-al-reciclaje/)   
Reciclaje briks, plásticos, latas, papel y cartón - Ecoembes - generacionnatura.org 3:34min [![Reciclaje briks, plásticos, latas, papel y cartón - Ecoembes - generacionnatura.org - YouTube](https://img.youtube.com/vi/YZHgtUev9g0/0.jpg)](https://www.youtube.com/watch?v=YZHgtUev9g0)   
“Enganchados a la red. Reciclaje electrónico”. Comando Actualidad 6min  
[http://www.rtve.es/alacarta/videos/comando-actualidad/comando-actualidad-enganchados-red-reciclaje-electronico/3348117/](http://www.rtve.es/alacarta/videos/comando-actualidad/comando-actualidad-enganchados-red-reciclaje-electronico/3348117/)   
“La otra vida de los envases” [La otra vida de los envases: qué ocurre cuando llegan a una planta de reciclaje | El HuffPost](http://www.huffingtonpost.es/2016/06/15/reciclaje-botella-plastico_n_10438584.html)   

[twitter ferranrosa/status/1198959087221956610](https://twitter.com/ferranrosa/status/1198959087221956610)  
Convé recordar el gran potencial de la prevenció (el no consum) per mitigar el #canviclimàtic, com il·lustra l'informe de @zerowasteeurope
 i @Eunomia_RandC.  
![](https://pbs.twimg.com/media/EKOO7elXsAAybDo?format=png)  

[twitter jaalgon/status/1194202441815920643](https://twitter.com/jaalgon/status/1194202441815920643)  
Hilo desde 2019 con artículos sobre cambio climático  
"...artículos climáticos que me vayan interesante, for the record (sin que quiera decir que los suscribo, ni enteros ni en parte)."  

### Huella de carbono
¿Qué es la huella de carbono? 2 min [![¿Qué es la huella de carbono? 2 min](https://img.youtube.com/vi/-oTSQzE5uGk/0.jpg)](https://www.youtube.com/watch?v=-oTSQzE5uGk)   
¿Cómo reducir la huella de carbono? 4 min [![¿Cómo reducir la Huella de carbono? - YouTube](https://img.youtube.com/vi/lZWdrVyNWWs/0.jpg)](https://www.youtube.com/watch?v=lZWdrVyNWWs)   
“La huella de Carbono”. Para todos la la 2. 10 min [http://www.rtve.es/alacarta/videos/para-todos-la-2/para-todos-2-huella-ecologica-huella-del-carbono/2197793/](http://www.rtve.es/alacarta/videos/para-todos-la-2/para-todos-2-huella-ecologica-huella-del-carbono/2197793/)   
Estrategia Huella Carbono Decathlon 2 min [![Estrategia Huella de Carbono. Decathlon Medio Ambiente. - YouTube](https://img.youtube.com/vi/ED8UW2_jBxo/0.jpg)](https://www.youtube.com/watch?v=ED8UW2_jBxo)   

[twitter LeftistWonk/status/1468806429855424514](https://twitter.com/LeftistWonk/status/1468806429855424514)  
Jeff Bezos' 9-minute joyride to the edge of space created more carbon emissions than 1 billion people produce in an entire lifetime  
[twitter htejero_/status/1468990013790724101](https://twitter.com/htejero_/status/1468990013790724101)  
Jeff Bezos emitió en sus 11 minutos de viaje espacial lo mismo que emite a lo largo de toda su vida cada una de las 1000 millones de personas más pobres del mundo. (Fuente: https://wir2022.wid.world)  
![](https://pbs.twimg.com/media/FGLmi0eWQAgHOeH?format=jpg)  

### ODS

[Los Objetivos de desarrollo sostenible y su relación con la química - quimicaysociedad.org](https://www.quimicaysociedad.org/los-objetivos-de-desarrollo-sostenible-y-su-relacion-con-la-quimica/)  
