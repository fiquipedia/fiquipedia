# Recursos calor y temperatura

El calor está asociado a [termodinámica](/home/recursos/termodinamica),  [energía](/home/recursos/energia)  y trabajo (y dentro de ellas a rendimiento) Es un concepto que se introduce conceptualmente en Física y Química en 2º ESO tras ver energía.  
Se ven efectos de calor (dilatación con problemas en 4º) y formas de transferencia (conducción, convección, radiación)  
Temperatura se ve antes, asociado a teoría cinética.  
En 4ºESO se asocia a calor específico, equilibrio térmico  
La radicación se cita en Física cuántica en 2º Bachillerato   


Se ponen aquí algunos recursos por separarlos, habrá en otras páginas asociadas al nivel (por ejemplo [recursos 4º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso) ), a otras categorías [apuntes](/home/recursos/apuntes), [simulaciones](/home/recursos/simulaciones), asociadas a otros conceptos como [termodinámica](/home/recursos/termodinamica), ...  

* [Calor y temperatura - Newton](http://recursostic.educacion.es/newton/web/materiales_didacticos/calor/calor-indice.htm)  cc-by-nc  
* [Calor y calorimetría - Universidad de Sevilla](http://laplace.us.es/wiki/index.php/Calor_y_calorimetr%C3%ADa)  
* [Serie de artículos "La dinámica del calor"](https://culturacientifica.com/series/la-dinamica-del-calor/)  
* [![](https://oeydms.weebly.com/uploads/8/9/5/5/8955843/5325370.jpg?1358360364 "https://oeydms.weebly.com/conduction-convection--radiation.html") ](https://oeydms.weebly.com/uploads/8/9/5/5/8955843/5325370.jpg?1358360364)  
* [Mechanisms of Heat Loss or Transfer](https://www.e-education.psu.edu/egee102/node/2053)  
Incluye animaciones y ejercicio interactivo con tipos de transferencia de calor  
* [Heat transfer (Flash)](https://www.pbslearningmedia.org/asset/lsps07_int_heattransfer/)  
Animaciones sencillas transferencia de calor

##   Kelvin
Es una de las unidades fundamentales del Sistema Internacional, se citan algunas referencias  
[SI base unit: kelvin (K)](https://www.bipm.org/en/si-base-units/kelvin)  
- El nombre es kelvin y símbolo K, no es "grados kelvin" ni "ºK" 
*The 13th CGPM (1967/68,  [Resolution 3](https://www.bipm.org/en/committees/cg/cgpm/13-1967/resolution-3) ) adopted the name kelvin, symbol K, instead of "degree Kelvin", symbol °K, and defined the unit of thermodynamic temperature as follows (1967/68,  [Resolution 4](https://www.bipm.org/en/committees/cg/cgpm/13-1967/resolution-4) ):The kelvin, unit of thermodynamic temperature, is the fraction 1/273.16 of the thermodynamic temperature of the triple point of water.*
- El tamaño de ºC y de K es el mismo 
* [Resolution 3](https://www.bipm.org/en/committees/cg/cgpm/13-1967/resolution-3) , mentioned above), the numerical value of the temperature difference being the same.  

- La conversión exacta entre ºC y K es con 273,15  
*t*/°C = *T*/K – 273.15. 

##   Transferencia de calor

[twitter operadornuclear/status/1154240754430205953](https://twitter.com/operadornuclear/status/1154240754430205953)  
Formas de transferencia de calor. #ApuntesOperador  
![](https://pbs.twimg.com/media/EASv1_OX4AAByff?format=jpg)  
 
[twitter Rainmaker1973/status/1233768904721420288](https://twitter.com/Rainmaker1973/status/1233768904721420288) 
Space Shuttle thermal tiles were such poor heat conductors that you could grab them by their edges seconds after being in a 2200°C ovenSpace Shuttle Thermal Tile Demonstration  
[![https://www.youtube.com/watch?v=Pp9Yax8UNoM](http://img.youtube.com/vi/Pp9Yax8UNoM/0.jpg)](http://www.youtube.com/watch?v=Pp9Yax8UNoM "Space Shuttle Thermal Tile Demonstration")

* [Modes of Heat Transfer, University of California Davis, phys.libretexts.org](https://phys.libretexts.org/Courses/University_of_California_Davis/Physics_9B_Fall_2020_Taufour/05%3A_Fundamentals_of_Thermodynamics/5.04%3A_Modes_of_Heat_Transfer)

##   Experiencia de Joule
* [Loi de Joule, Physique et simulation numériques](http://subaru.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/thermo/joule.html)  
* [Primer principio, Equivalente mecánico del calor, montes.upm](http://www2.montes.upm.es/dptos/digfa/cfisica/termo1p/joule.html)  
* [Experimento de Joule. Equivalente mecánico del calor, sc.ehu, Curso Interactivo de Física en Internet, Angel Franco García, Copyright © 2016](http://www.sc.ehu.es/sbweb/fisica3/calor/joule/joule.html)  

[ProblemasFisica-Calor.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/calor/ProblemasFisica-Calor.pdf)

Los ficheros se veían anexado a esta página ahora se ven en [drive.fiquipedia recursos/calor](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/calor)

## Radiación
En ESO se ven los 3 mecanismos de propagación de calor (conducción, convección, radiación) y se identifican situaciones cotidianas donde se produce o se evita esa propagación: doble cristal, botella termo, parasol en el coche, encalar/pintar de blanco las paredes ...  

Hay situaciones que parecen contradecir la intuición. Ver este hilo   
[twitter rodrigogpeinado/status/1537094821323612161](https://twitter.com/rodrigogpeinado/status/1537094821323612161)  
Con los termómetros disparados en toda la península, puede ser útil saber cuál es la ropa más conveniente para soportar el calor. Para ello, podemos fijarnos en los pueblos del desierto, que algo saben sobre cómo aguantar el calor extremo.  
Y sí, visten de negro  
Efectivamente, los objetos que son negros lo son porque absorben la radiación que les llega, aumentando su temperatura.  
Por otro lado, los objetos blancos son aquellos que reflejan la radiación que les llega. Esta es la razón por la cual se encalan las casas en el sur.  
Por ello, nos puede resultar paradójico que los pueblos del desierto vistan con ropas oscuras/negras, ya que dichas prendas absorben la radiación con el consecuente aumento de la temperatura.  
Sin embargo, hay que tener en cuenta dos cosas.  
En primer lugar, hay que ser consciente de que, cuando nos ponemos ropa, no es solo la radiación del Sol la que nos llega. Nuestro cuerpo, al estar a una determinada temperatura, también emite radiación.  
A partir de aquí, hay dos opciones. Si vestimos con ropas blancas/claras, ciertamente reflejarán la radiación, pero reflejarán tanto la que incide del Sol hacia exterior como la que incide desde el propio cuerpo humano de vuelta al interior.  
Por otro lado, si vestimos con ropas negras/oscuras, tanto la radiación del Sol como la del cuerpo se quedarán absorbidas por la ropa, impidiendo una vuelta de radiación hacia el interior del cuerpo. Y hay más.  
Cuando usamos ropa negra para que la radiación se quede en la ropa, es FUNDAMENTAL usar ropa negra ancha, ya que esto permite que se generen corrientes de convección entre la ropa y el cuerpo, que poco a poco ventilarán dicho espacio y nos hará estar más frescos.  
Por eso, tanto los beduinos como los tuaregs y otros pueblos del desierto utilizan ropas negras y amplias, para acumular la radiación en la ropa y con ayuda de las corrientes de convección (en el desierto hace mucho aire), refrescarse.  
Os dejo un artículo de Nature en donde unos investigadores hicieron experimentos de este tipo con un beduino en 1980  
[Why do Bedouins wear black robes in hot deserts? - nature.com](https://www.nature.com/articles/283373a0)  
Y otro enlace en donde también lo trata desde el punto de vista de la disipación de calor por sudoración  
[The Science of wearing black and thick clothes in the desert](https://www.veto.be/artikel/the-science-of-wearing-black-and-thick-clothes-in-the-desert)  

## Cámara termográfica  
Puede estar asociada a muchos tipos de procesos  

[La cámara termográfica: una seductora herramienta didáctica. An. Quím., 117 (3), 2021, 203-208. Fernando I. de Prada Pérez de Azpeitia](https://analesdequimica.es/index.php/AnalesQuimica/article/view/1822/2337)  

