
# Concentración

Página como tantas en borrador ...  
Ver también  [recursos disoluciones](/home/recursos/quimica/recursos-disoluciones)  
Al hablar de concentración se suele hacer referencia a  [disoluciones](/home/recursos/quimica/recursos-disoluciones)  con disolvente líquido, pero muchas veces va asociado a concentraciones de disoluciones con disolvente gas, por lo que enlaza con [leyes de los gases](/home/recursos/quimica/leyes-de-los-gases).  
Una disolución es una mezcla homogénea, por lo que enlaza con  [mezclas](/home/recursos/quimica/recursos-mezclas).  
También enlaza con [homeopatía](/home/recursos/quimica/homeopatia)

##  Unidades de molaridad
Puede surgir la duda de si M es correcto como símbolo de molaridad, ya que es el prefijo del sistema internacional para mega.  
Además de habitual es correcto, igual que m es al mismo tiempo símbolo de unidad metro y prefijo del sistema internacional para mili.  
Que sea correcto lo fija que lo diga un organismo de estandarización, en este caso IUPAC, no que sea de uso habitual.    
[IUPAC. Quantities, Units and Symbols in Physical Chemistry Second Edition. 2.10 GENERAL CHEMISTRY](http://old.iupac.org/publications/books/gbook/green_book_2ed.pdf#page=50)  
>(15) The unit mol dm-3 is often used for amount concentration. 'Amount concentration' is an abbreviation for 'amount-of-substance concentration'. (The Clinical Chemistry Division of IUPAC recommends that amount of substance concentration be abbreviated to 'substance concentration'.) When there is no risk of confusion the word 'concentration' may be used alone. The symbol \[B] is often used for amount concentration of entities B. This quantity is also sometimes called molarity. A solution of, for example, 1 mol dm-3 is often called a 1 molar solution, **denoted 1 M solution. Thus M is often treated as a symbol for mol dm-3**.  

[IUPAC. Quantities, Units and Symbols in Physical Chemistry Third Edition (2nd printing), 2.10 GENERAL CHEMISTRY](https://iupac.org/wp-content/uploads/2019/05/IUPAC-GB3-2012-2ndPrinting-PDFsearchable.pdf#page=62)  
>(16) "Amount concentration" is an abbreviation of "amount-of-substance concentration". (The Clinical Chemistry Division of IUPAC recommends that amount-of-substance concentration be abbreviated to "substance concentration" \[14, 73].) The word "concentration" is normally used alone where there is no risk of confusion, as in conjunction with the name (or symbol) of a chemical substance, or as contrast to molality (see Section 2.13, p. 70). In polymer science the word "concentration" and the symbol c is normally used for mass concentration. In the older literature this quantity was often called molarity , a usage that should be avoided due to the risk of confusion with the quantity molality. Units commonly used for amount concentration are mol L−1 (or mol dm−3 ), mmol L−1 , µmol L−1 etc., **often denoted M**, mM, µM etc. (pronounced molar, millimolar, micromolar).  

En la nueva versión se añade una frase sobre evitar el uso de molaridad, pero no afecta a que, si se usa, sea válido o no usar M como símbolo.

##  Simulaciones
Simulaciones Universidad de Colorado asociadas:  
* [Concentración - phet.colorado.edu](https://phet.colorado.edu/es/simulation/concentration) HTML5  
![](https://phet.colorado.edu/sims/html/concentration/latest/concentration-900.png)  
* [Molaridad - phet.colorado.edu](https://phet.colorado.edu/es/simulation/molarity) HTML5  
![](https://phet.colorado.edu/sims/html/molarity/latest/molarity-900.png)  
* [Sales y solubilidad - phet.colorado.edu](https://phet.colorado.edu/es/simulation/soluble-salts) java, cheerpj  
![](https://phet.colorado.edu/sims/soluble-salts/soluble-salts-600.png)  
* [Soluciones de azúcar y sal - phet.colorado.edu](https://phet.colorado.edu/es/simulation/sugar-and-salt-solutions) java, cheerpj   
![](https://phet.colorado.edu/sims/sugar-and-salt-solutions/sugar-and-salt-solutions-600.png)  

##  Apuntes
Hoja resumen con fórmulas  
[Concentration and ideal gas formulae - vaxasoftware.com](http://www.vaxasoftware.com/doc_eduen/qui/disoluc.pdf)  

Apuntes de FisQuiWeb sobre disoluciones. Distintos apuntes por nivel. Licenciamiento cc-by-nc-sa  
[Apuntes - fisquiweb.es](http://www.fisquiweb.es/Apuntes/apuntes.htm)  
* [Disoluciones 3ºESO - fisquiweb.es](http://www.fisquiweb.es/Apuntes/Apuntes3/Disoluciones3.pdf)  
* [Disoluciones 1ºBachillerato - fisquiweb.es](http://www.fisquiweb.es/Apuntes/Apuntes1Bach/Disoluciones.pdf)  

 
