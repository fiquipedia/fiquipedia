
# Recursos ondas electromagnéticas 

Está asociado a [campo magnético](/home/recursos/fisica/recursos-campo-magnetico) y a [movimiento ondulatorio](/home/recursos/fisica/movimiento-ondulatorio)

Un tema separado es el tema de las antenas, que se citaba indirectamente en currículo LOMLOE
 
   * [Electromagnetic Waves - ophysics](https://ophysics.com/em3.html)  
   * [Propagación de las ondas electromagnéticas - java applet](https://www.phys.hawaii.edu/~teb/java/ntnujava/emWave/emWave.html)   
Fu-Kwun Hwang, Dept. of physics, National Taiwan Normal University  

## Antenas

[Antenna on the Cheap (er, Chip) 2001 - oreillynet - archive](https://web.archive.org/web/20050828111234/http://www.oreillynet.com/cs/weblog/view/wlg/448)  

[Cómo hacer antena casera con bote de Pringles 2005](http://www.merlos.org/tutorial/2005/01/como-hacer-antena-casera-con-bote-de-pringles.html)  

[Antenas Wi-Fi. Lo que hay que saber - carballar](https://carballar.com/antenas-wi-fi-lo-que-hay-que-saber)  

[Wi-Fi. Mejorar la cobertura con una antena casera - carballar](https://carballar.com/wifi-mejorar-la-cobertura-con-una-antena-casera)  
![](https://carballar.com/wp-content/uploads/2020/12/wifi-antena-pringle-1-600x285-1.jpg) 

[How to Make a Wi-Fi Antenna Out of a Pringles Can - makeuseof](https://www.makeuseof.com/tag/how-to-make-a-wifi-antenna-out-of-a-pringles-can-nb/)   

[Building the Cylinder (Can) Waveguide - wikirakare](https://www.wikarekare.org/Antenna/WaveguideCan.html)  

[E. Bonilla Jara, D. Caiza Vargas, y M. . Villacrés Meza, «Conjunto de antenas circulares con una línea de transmisión para formar una antena Yagi-Uda para WiFi», Perspectivas, vol. 4, n.º 2, pp. 19–24, ago. 2022.](https://doi.org/10.47187/perspectivas.4.2.161)

[Wi-Fi/3G/4G Disk-Yagi antenna, analysis and calculation](https://3g-aerial.biz/en/disk-yagi-antenna)  
> It should be especially noted that this class of antennas should be rated a very unsuccessful variant for hand-maker who do not have a vector analyzer in terms of design repeatability. On our website you can find much more effective antennas for DIYers, for example for 4G, or for Wi-Fi.

[DIY long range wifi antenna “Patch array 2×3” ](https://3g-aerial.biz/en/diy-long-range-wifi-antenna)  
![](https://3g-aerial.biz/images/pictures/fa20-03.png)  
> One such antenna is Yagi-Uda. However, this antenna requires careful tuning by using the vector analyzer and has a complex matching unit at the feedpoint. This antenna may not work properly on the Wi-Fi range, even if you do everything exactly according to the drawings. Let's call this property the **“repeatability”** of the antenna. In other words, the lower the probability of a negative result when making a DIY antenna, the higher the repeatability. This is an important criterion for selecting the design of a homemade microwave antenna. Cantenna or BiQuad have good repeatability. Yagi has low repeatability. So, we need a high gain Wi-Fi antenna with high repeatability.  
Therefore, the repeatability of “Patch array 2×3” is quite high, and this antenna can be safely recommended for DIY-ers who do not have a microwave impedance meter at hand.

[Cantennator - app Google Play](https://play.google.com/store/apps/details?id=com.radioacoustick.cantennator&hl=es)  
![](https://play.google.com/store/apps/details?id=com.radioacoustick.cantennator&hl=es)

