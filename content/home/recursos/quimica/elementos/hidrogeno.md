
# Hidrogeno

El hidrógeno enlaza con [recursos Física y Química y Medio Ambiente](/home/recursos/recursos-fisica-quimica-y-medio-ambiente) y a su vez con recursos [electroquímica](/home/recursos/quimica/electroquimica) asociado a electrolisis y pilas de combustible. También se puede relacionar el hidrógeno con fusión nuclear, origen del universo, evolución estelar ...  

[eshidrogeno.com](https://eshidrogeno.com/)  

[Electrolizador - iberdrola.com](https://www.iberdrola.com/sostenibilidad/electrolizador)  
![](https://www.iberdrola.com/documents/20125/1226064/Infografia_Aplicaciones_Hidrogeno_Verde.jpeg/5f496754-08fb-a590-fecb-c7e49e6b7de8?t=1639123932462)  

[Los colores del hidrógeno ¿Qué significado tiene cada uno? - fisiquimicamente](https://fisiquimicamente.com/blog/2023/03/18/los-colores-del-hidrogeno/)  

[Electrólisis del agua - quimicafacil.net](https://quimicafacil.net/manual-de-laboratorio/electrolisis-del-agua/)  

## Pila/celda de combustible
El hidrógeno reacciona con el oxígeno y genera directamente electricidad  

[Todo lo que siempre quiso saber acerca de las Celdas de Combustible](http://www.ehu.eus/ingquima/Master_TA/TRAT_DEL_AGUA/MasterIngQuim/MasterCeldasComb/DOCUMENTACIN/tod_Celdas%20de%20Combustible.htm)  

[Celdas de combustible](http://www2.eie.ucr.ac.cr/~jromero/sitio-TCU-oficial/boletines/grupo01/numero-12/Boletin-12.htm)  

[Celdas de hidrógeno - unne.edu.ar](http://ing.unne.edu.ar/pub/celdas_hidrogeno.pdf)  

[Tecnología de celdas de combustible de hidrógeno - donaldson.com](https://www.donaldson.com/es-es/fuel-cells/technical-articles/hydrogen-fuel-cell-technology/)  

[Cómo funciona un coche de pila de combustible de hidrógeno - motorpasion.com](https://www.motorpasion.com/revision/como-funciona-coche-pila-combustible-hidrogeno)  

[![TOYOTA Fuel cell - How does it work?"](https://img.youtube.com/vi/LSxPkyZOU7E/0.jpg)](https://www.youtube.com/watch?v=LSxPkyZOU7E "TOYOTA Fuel cell - How does it work?")   

Ejemplo con tren de hidrógeno que usa pila de combustible, motor eléctrico  
[![Coradia iLint"](https://img.youtube.com/vi/O3bUE9uHkqM/0.jpg)](https://www.youtube.com/watch?v=O3bUE9uHkqM "Coradia iLint")   

[Tenemos fecha del primer vuelo comercial en avión de hidrógeno - eshidrogeno.com](https://eshidrogeno.com/avion-hidrogeno/)  
![](https://eshidrogeno.com/wp-content/uploads/2021/11/avion-hidrogeno.jpg)  

[El camión de hidrógeno - eshidrogeno.com](https://eshidrogeno.com/camion-hidrogeno/)  

[![Kenworth Zero Emission Cargo Transport (ZECT)"](https://img.youtube.com/vi/ShgYjFb4Pp8/0.jpg)](https://www.youtube.com/watch?v=ShgYjFb4Pp8 "Kenworth Zero Emission Cargo Transport (ZECT)")   

[“Ver que nuestro trabajo puede salir de los libros y del laboratorio y servir a la sociedad ya es un premio” - madrimasd.org](https://www.madrimasd.org/notiweb/entrevistas/ver-que-nuestro-trabajo-puede-salir-los-libros-laboratorio-servir-sociedad-ya-es-un-premio)  
> La economía del hidrógeno es un tema supercandente en la actualidad, como para no serlo echando un ojo al cambio climático y a la subida del precio de los combustibles fósiles. ¡Algo hay que hacer! El proyecto es muy puntero porque combina muchas ramas del conocimiento. Yo aporto mi conocimiento en catálisis como química, pero trabajo bajo la guía de un ingeniero que me aporta sus conocimientos mecánicos, físicos y matemáticos. Nos faltaba el tercer ingrediente: los conocimientos específicos en computación y modelización. Voy a trabajar con un grupo de machine learning e inteligencia artificial. Hoy en día es un requisito indispensable la simulación previa al laboratorio. Si podemos saber de antemano los resultados que vamos a obtener a través de técnicas computacionales, nos ahorramos tiempo y dinero en reproducir todo esto en el laboratorio.   

> Uno de los principales problemas a los que nos enfrentamos a la hora de producir energía limpia es que muchas de las fuentes renovables son intermitentes y muy difíciles de transportar. La idea es almacenar toda esta energía verde en forma de hidrógeno para poder generar energía en cualquier momento emitiendo vapor de agua como único residuo. Hoy por hoy esta tecnología existe, pero uno de los problemas es que el catalizador empleado en estas celdas de combustible está hecho de platino. Este platino, además de contar con problemas de abundancia, está entre los 15 materiales más caros del mundo. Mi tesis se centra en buscar sustitutos para este platino que sean más baratos y abundantes pero tan eficaces como el mismo, para poder llevar esta tecnología a toda la sociedad. Es un favor para nuestros bolsillos y para la salud del planeta.  


## Combustión de hidrógeno
El hidrógeno se puede quemar como combustible de manera similar a gasolina, por lo que se tine un motor de combustión, y aprovechar la energía térmica de la combustión, lo que supone rendimiento bajo al ser una máquina térmica  
[Toyota Developing Hydrogen Engine Technologies Through Motorsports](https://global.toyota/en/newsroom/corporate/35209996.html)   
[![Image of a hydrogen engine"](https://img.youtube.com/vi/O0yM5RlD7Zo/0.jpg)](https://www.youtube.com/watch?v=O0yM5RlD7Zo "Image of a hydrogen engine")   

## Hidrogeneras

[Naturgy impulsa la movilidad sostenible con la construcción de sus primeras 38 hidrogeneras en España](https://www.naturgy.com/naturgy_impulsa_la_movilidad_sostenible_con_la_construccion__de_sus_primeras_38_hidrogeneras_en_espana)  
![](https://www.naturgy.com/filesImagesCOM/Hidrogeneras.jpg)  

## Steam Methane Reforming, Syngas

[Steam reforming - wikipedia](https://en.wikipedia.org/wiki/Steam_reforming)  
CH<sub>4</sub> + H<sub>2</sub>O <=> CO + 3 H <sub>2</sub>  

[Making aircraft fuel from sunlight and air -sciencedaily.com](https://www.sciencedaily.com/releases/2021/11/211104115245.htm)  
> Carbon-neutral fuels are crucial for making aviation and maritime transport sustainable. The plant developed in Zurich can be used to produce synthetic liquid fuels that release as much CO2 during their combustion as was previously extracted from the air for their production. CO2 and water are extracted directly from ambient air and split using solar energy. This process yields syngas, a mixture of hydrogen and carbon monoxide, which is then processed into kerosene, methanol, or other hydrocarbons.  

## Hidrógeno y amoniaco
[Green Ammonia and the Electrification of the Haber-Bosch Process Reduce Carbon Emissions](https://guidehouseinsights.com/news-and-views/green-ammonia-and-the-electrification-of-the-haber-bosch-process-reduce-carbon-emissions)  
> Today, the Haber-Bosch process produces about 150 million metric tons of ammonia every year. The process produces over 450 million metric tons of CO2—about 1.2% of global CO2 emissions. According to one estimate, every 1 ton of ammonia produced using the Haber-Bosch process generates 1.87 tons of CO2 emissions, which is not good for climate change. Thankfully, electrification of the Haber-Bosch process is underway.  
...
>  Recent research on the feasibility of electrifying the Haber-Bosch process revealed that it could enable carbon-free ammonia synthesis by replacing **methane reforming (the process used to produce hydrogen** for reaction with nitrogen) with electrolysis and by powering equipment with electricity instead of steam.  

## Situación actual y futura en producción y transporte de hidrógeno 

[Alemania busca el hidrógeno verde más barato del mundo procedente de Namibia - agosto 2021](https://sferaproyectoambiental.org/2021/08/27/alemania-busca-el-hidrogeno-verde-mas-barato-del-mundo-procedente-de-namibia/)  
> La nación africana está muy escasamente poblada con solo 2,5 millones de habitantes y cuenta con más de 3.500 horas de sol, además de fuertes vientos.  
> El Consejo Nacional de Hidrógeno alemán estima que solo la industria alemana, excluidas las refinerías, tendrá un requerimiento de hidrógeno de 1,7 millones de toneladas por año para 2030, que continuará aumentando a partir de entonces”, dijo Karliczek.  

[India: TotalEnergies and Adani Join Forces to Create a World-Class Green Hydrogen Company - junio 2022](https://totalenergies.com/media/news/press-releases/india-totalenergies-and-adani-join-forces-create-world-class-green)  
> To start with, ANIL intends to develop a project to produce 1.3 Mtpa of urea derived from green hydrogen for the Indian domestic market, as a substitution to current urea imports, and will invest around $5 billion in a 2 GW electrolyzer fed by renewable power from a 4 GW solar and wind farm.  

[El plan de Europa de importar hidrógeno verde desde África, ilógico por estas razones - mayo 2022](https://www.motor.es/futuro/plan-europa-importar-hidrogeno-verde-africa-ilogico-202287076.html)  
> Un informe del organismo de control de la UE «Corporate Europe Observatory» y la firma canadiense TFIE Strategies muestra que el plan de la Unión Europea para aumentar drásticamente las importaciones de hidrógeno renovable del norte de África no es realista desde una perspectiva de costes o energía y, en cambio, desvía la electricidad renovable de las necesidades locales y los objetivos climáticos locales.  

[La asociación H2 Chile analiza las trabas al desarrollo del hidrógeno verde en el país y propone soluciones](https://www.energias-renovables.com/hidrogeno/la-asociacion-h2-chile-analiza-las-trabas-20220928/)  

[El CSIC lidera un proyecto europeo para obtener hidrógeno verde a partir de bacterias. Se trata de un proyecto de más de 4 millones de euros para conseguir que unas bacterias modificadas genéticamente produzcan hidrógeno utilizando agua no potable](https://www.csic.es/es/actualidad-del-csic/el-csic-lidera-un-proyecto-europeo-para-obtener-hidrogeno-verde-partir-de)  

[twitter luisglezreyes/status/1644587217824645120](https://twitter.com/luisglezreyes/status/1644587217824645120)  
El 99% del hidrógeno se obtiene de combustibles fósiles y el 37% se usa en refinerías.  
Eso se acerca más a una economía circular fósil que a cualquier tipo de transición energética.  
[Desenmascarando patrañas hidrogeneras - crisisenergetica.org](https://www.crisisenergetica.org/article.php?story=20230215120949819)  
![](https://www.crisisenergetica.org/images/ciclo-valor-hidrogeno.jpg)  

## Vehículos
[Neo Orbis, el primer buque eléctrico alimentado por hidrógeno sólido](https://www.hibridosyelectricos.com/articulo/actualidad/neo-orbis-primer-buque-electrico-propulsion-base-hidrogeno-solido/20220803115850061048.html)  	
> El proyecto H2Ships ha iniciado la construcción del primer navío eléctrico propulsado por hidrógeno sólido. Sus primeras pruebas se esperan que comiencen en junio de 2023.  


## Experimentos

[How to Make Hydrogen (Science Experiment)](https://www.wikihow.com/Make-Hydrogen-%28Science-Experiment%29)  
[Experimentos - eshidrogeno.com](https://eshidrogeno.com/experimentos-hidrogeno/)  

[![5 Amazing Experiments With Hydrogen!"](https://img.youtube.com/vi/EBGdSbTgwmM/0.jpg)](https://www.youtube.com/watch?v=EBGdSbTgwmM "5 Amazing Experiments With Hydrogen!")   

[PREPARATION AND PROPERTIES OF HYDROGEN. EXPERIMENT 4](http://lshmwaco.org/chemtech/cn/cn1105/experiments/hydrogen.pdf)  

[Exploding Balloons of Hydrogen and Oxygen - stevespanglerscience](https://www.stevespanglerscience.com/lab/experiments/hydrogen-oxygen-explosion/)  
