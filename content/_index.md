---
date: 2021-08-23
---

[comentario]: # (con hugo v88 fallan algunos comentarios: aparte de mirar que haber una línea blanca antes del comentario, veo que no va bien con ciertos paréntesis y comillas dentro del texto del comentario)  
[comentario]: # (en sites original www.fiquipedia.es/home mostraba lo mismo que www.fiquipedia.es: se crea _index.md para poder personalizar lo que hay en /. En cuanto a /home, crear _index.md permite poner algo inicial, pero incluye al final de la página cosas adicionales, como si fueran las páginas cambiadas de manera más reciente, es algo de tema hugo. No se usan aliases de hugo. Si se enlaza /home/_index.md a /_index.md se evita duplicidad pero sigue añadiendo al final. Si se enlaza home/index.md a _index.md sí muestran lo mismo pero enlaces no funcionan los enlaces que no estén en raíz. Si se crea home/index.md que supone duplicidad sí parece funcionar en local pero no en remoto, aunque no usa el valor date. 29 agosto 2021: se mantiene enlace de /home/_index.md a /_index.md)  

[comentario]: # (se añade date para que indique año correctamente en parte inferior página inicial, pero no en el resto)   

[comentario]: # (al usar tema hugo elimino imagen logo /images/2017-06-23-LogoFiquipedia.png, que ya incoropora el tema, y quito código QR /images/FiquipediaQR.png, para simplificar página inicial, y tampoco pongo título; antes era "Física y Química", pero en página inicial se añade ya "FiQuiPedia. Recursos de Física y Química")  

[comentario]: # (Dejo referencia cita de RSEQ, que me parece relevante, pero muy breve y sin imagen, enlazando a referencias)
[comentario]: # (No indico por redundante que se puede llegar a las páginas con el menú superior/lateral y que se incluyen aquí enlaces a las más habituales y a un listado, así pongo tabla directamente)  
[comentario]: # (Pongo los enlaces más relevantes en forma de tabla: en Google Sites tenía un pequeño listado)  
[comentario]: # (No pongo todo el listado de subpáginas como estaba en Google Sites ya que carga innecesariamente la página y duplica contenido ya que no se hace automáticamente: enlazo a las principales: legislación, materias, pruebas y recursos)  
[comentario]: # (los comentarios no se incorporan como comentarios html, por lo que no supone carga en la paǵina)  

| [PAU Madrid](/home/pruebas/pruebasaccesouniversidad) | **[Física 📚](/home/pruebas/pruebasaccesouniversidad/paufisica)** | **[Química 📚](/home/pruebas/pruebasaccesouniversidad/pau-quimica)** |  
|-:|:-:|:-:|
| [Acceso Universidad](/home/pruebas/pruebasaccesouniversidad) | [Física 📚](/home/recursos/recursospau/ficheros-enunciados-pau-fisica) | [Química 📚](/home/recursos/recursospau/ficheros-enunciados-pau-quimica) |
| Apuntes 2º Bachillerato | [Física 📚](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato) | [Química 📚](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato) |  
| **[Recursos 🧰](/home/recursos)** | [Física 📡](/home/recursos/fisica) | [Química 🧪](/home/recursos/quimica) |
| [FQ⨯nivel 🪜](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel) | **[Pizarras 📚](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel)** | [Currículo 📜](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/docencia-contenidos-fisica-y-quimica-por-nivel-segun-curriculo) | 
| **[Materias 📑](/home/materias)** | [ESO 📑](/home/materias/eso) | [Bachillerato 📑](/home/materias/bachillerato) | 
| Formulación | [General ⌬](/home/recursos/quimica/formulacion) | [Apuntes  📚](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion) |
| [Opos. FQ 🧗](/home/recursos/recursos-para-oposiciones) | [⨯CCAA 📚](/home/recursos/recursos-para-oposiciones/recursos-para-oposiciones-problemas-por-comunidad/) | [⨯Bloques 📚](/home/recursos/recursos-para-oposiciones/recursos-para-oposiciones-problemas-por-bloques/) | 
| Olimpiadas 🏅| [Física 📚](/home/recursos/fisica/olimpiadas-de-fisica/) | [Química 📚](/home/recursos/quimica/olimpiadas-quimica/) |  
| **[Pruebas](/home/pruebas)** | [FP GM 📚](/home/pruebas/pruebas-de-acceso-a-grado-medio) | [Grad. ESO 📚](/home/pruebas/pruebas-libres-graduado-eso) | 
| [Elab. propia 📚](/home/recursos/recursos-de-elaboracion-propia)  | [Proy. Inv. 📚](/home/recursos/proyectos-de-investigacion)  | [Prác. Lab. 📚](/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia)  | 
|FriQuiExámenes | [Generales🦸](/home/recursos/examenes/friquiexamenes)  | [FiQuiPedia🦸](/home/recursos/examenes/friquiexamenes-fiquipedia)  | 
* [Legislación educativa 📜](/home/legislacion-educativa)

_[Citada por Real Sociedad Española de Química](/referencias-a-fiquipedia)_  
Página en permanente estado de construcción, ver [planteamiento](/planteamiento)   

