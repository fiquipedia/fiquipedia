
# Recursos sistemas materiales

Se trata de recursos asociados al bloque "la materia" en Física y Química 2º ESO y Física y Química 3º ESO (ver  [Docencia contenidos física y química por nivel, según currículo](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/docencia-contenidos-fisica-y-quimica-por-nivel-segun-curriculo) )  
Al hablar de este bloque se engloban otros:   
Concepto de materia, propiedades generales y específicas, y dentro de específicas se trata la densidad  [Recursos densidad](/home/recursos/fisica/recursos-densidad)   
Recursos estados de agregación y cambios de estado: en general encaja junto a la teoría cinético-molecular (como tantas cosas, pendiente crear página separada tras ampliar y ordenar ...)  
[Recursos teoría cinético-molecular](/home/recursos/recursos-teoria-cinetica)  
[Recursos leyes de los gases](/home/recursos/quimica/leyes-de-los-gases)   

[intef.es Investigando los sistemas materiales](https://descargas.intef.es/cedec/proyectoedia/fisica_quimica/contenidos/investigando_sistemas_materiales/index.html) 2º ESO 
Sistemas Materiales  
Técnicas de separación  
Disoluciones y concentración  
Cambios físicos y químicos  
Pruebas PISA  
CEDEC (Centro Nacional de Desarrollo Curricular en sistemas no propietarios), proyecto EDIA, cc-by-sa

Una idea interesante para proyecto sobre estados de agregación :-)  
Physicist Wins Ig Noble Prize For Study On Whether Cats Should Be Classified As Liquids Or Solids  
[Physics-Astronomy.org](https://www.physics-astronomy.org/2018/11/physicist-wins-ig-noble-prize-for-study.html)   
[twitter FiQuiPedia/status/1080857281602957312](https://twitter.com/FiQuiPedia/status/1080857281602957312)  
![](https://pbs.twimg.com/media/Dv_5zgyWsAESI0Q?format=jpg)  
![](https://pbs.twimg.com/media/Dv_554EW0AAE1tz?format=jpg)  

Sobre estados de la materia, enlazando con conceptos de átomos, de física de partículas (quarks), gravitación (agujeros negros) ... genial hilo de Carlos Pazos  
[twitter MolaSaber/status/1171093181817151488](https://twitter.com/MolaSaber/status/1171093181817151488)  
Sólido, líquido, gaseoso, plasma   
Son los estados de la materia más conocidos pero en las estrellas se dan otros más exóticos.   
¿Quieres saber cuáles? Vamos allá.  
![](https://pbs.twimg.com/media/EECPCZbX4AE4iR2?format=jpg)  
Nos ponemos en situación con un repaso rápido por los más conocidos: Sólido Sus partículas poseen nexos rígidos y fuertes. Estas fuerzas de cohesión mantienen estable su forma y volumen y le otorgan cierto margen de dureza y resistencia.  
![](https://pbs.twimg.com/media/EECPDALWsAc0Vr6?format=jpg)  
Seguimos con líquidos. Sus partículas se encuentran lo bastante juntas para conservar una cohesión mínima, y lo suficientemente dispersas para permitir fluidez y cambio de forma.  
![](https://pbs.twimg.com/media/EECPDj8XUAAmeM2?format=jpg)  
Ya falta menos para llegar hasta las estrellas. Gaseoso. Son partículas no unidas, expandidas y con poca fuerza de atracción, lo que hace que no tengan volumen ni forma definida.  
![](https://pbs.twimg.com/media/EECPEKIX4AE2IZp?format=jpg)  
Plasma. Debido a la elevada temperatura los electrones se separan de los átomos dando lugar a iones de carga positiva. Este es el estado común de la materia en LAS ESTRELLAS.  
![](https://pbs.twimg.com/media/EECPEs2WsAU6hsT?format=jpg)  
Materia degenerada. A veces, debido a la gravedad, se da tal presión que apenas queda espacio de separación entre partículas. Como los electrones no pueden ocupar el mismo estado cuántico, no se acercan más entre sí. Esto se da en las ENANAS BLANCAS.  
![](https://pbs.twimg.com/media/EECPFUuWwAAy8Ok?format=jpg)  
Materia neutrónica Si la masa de una estrella es muy grande y colapsa, ni si quiera los electrones pueden oponerse a la fuerza de gravedad. La mayoría de ellos se fusionan con los protones, formando neutrones. Esto ocurre en las ESTRELLAS DE NEUTRONES.  
![](https://pbs.twimg.com/media/EECPF6yXUAc7TWG?format=jpg)  
Plasma de quarks y gluones Si la gravedad es incluso mayor, tampoco los neutrones pueden oponerse y la materia se rompe formando una sopa ultradensa de partículas elementales. Se cree que este estado puede darse en las hipotéticas ESTRELLAS DE QUARKS.  
![](https://pbs.twimg.com/media/EECPGfoWkAAkhqh?format=jpg)  
Finalmente si no se detiene a la gravedad, la materia colapsa el tejido espacio-tiempo y se forma un AGUJERO NEGRO. Que no es un estado de la materia propiamente dicho pero había que llegar hasta aquí.  
![](https://pbs.twimg.com/media/EECPHEXXkAEknub?format=jpg)  
  
También se trata la clasificación de sistemas materiales: sustancias puras y mezclas, tipos de mezclas, técnicas de separación, disoluciones, solubilidad .. [Recursos mezclas](/home/recursos/quimica/recursos-mezclas)  [Recursos disoluciones](/home/recursos/quimica/recursos-disoluciones)   
  
 [Guapo, lo tuyo no es corriente — Cuaderno de Cultura Científica](https://culturacientifica.com/2014/03/28/guapo-lo-tuyo-es-corriente/)   
Conceptos; (además corriente, voltaje, resistencia, potencia), volumen, densidad, viscosidad (además de radianes y velocidad angular que sería para citar en cinemática)  

[Los cambios de estado: gráficas de calentamiento y enfriamiento - cienciadelux.com](https://cienciadelux.com/2016/07/18/los-cambios-de-estado-graficas-de-calentamiento-y-enfriamiento/)  


[![Aprendiendo de los cambios de estado - EduCaixaTV - La2](https://img.youtube.com/vi/-zB5mPADaFY/0.jpg)](https://www.youtube.com/watch?v=-zB5mPADaFY "Aprendiendo de los cambios de estado - EduCaixaTV - La2")


## Ejercicios 

   * Cambios de estado
   * Teoría cinética
   * Leyes de los gases
   * Sustancias puras y mezclas
   * Disoluciones
   * Concentración
   * Todo
 [Quiero aprobar Matemáticas y Fisica](https://yoquieroaprobar.es/35la_materia.html)   

## Laboratorio

[twitter BigVanCiencia/status/1551130404924579841](https://twitter.com/BigVanCiencia/status/1551130404924579841)  
No solo es calor. Para evaporar agua también se puede jugar con la presión. #BigVanCiencia #Clowntífics  

_Jeringuilla de plástico con un poco de agua. Al cerrar la jeringuilla y tirar con fuerza del émbolo, la presión baja y el agua comienza a evaporarse_
  
Laboratorio virtual. Propiedades de la materia  
[TuL@boratorio:](http://fq.iespm.es/images/janavarro/flash/laboratorio/Laboratorio_CS6b.swf) Simula con realismo un laboratorio para realizar experiencias de cálculo de volúmenes, masas, densidades, calentamiento... y manejar material de laboratorio.  
Usa el fichero [Laboratorio_CS6b.swf](http://fq.iespm.es/images/janavarro/flash/laboratorio/Laboratorio_CS6b.swf) José Antonio Navarro: medida volúmenes, densidad ,....  
[Copia local del fichero Laboratorio_CS6b.swf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/media/flash/Laboratorio_CS6b.swf)  
  
Mapa conceptual clasificación sistemas materiales (Jorge Rojo Carrascosa, cc-by-nc-sa)  
![](http://www.profesorjrc.es/apuntes/3%20eso/sistemas%20materiales.jpg)

