---
aliases:
  - /home/recursos/recursos-apuntes/
---

# Apuntes

Los apuntes son en general materiales más breves e informales que los  [libros](/home/recursos/libros) , aunque a veces un capítulo de un libro se pude considerar como apuntes del tema que trata  
Los apuntes se diferencian de los  [materiales autosuficientes](/home/recursos/materiales-autosuficientes)  en que son estáticos, pueden tener actividades pero son de realización no interactiva (los tradicionales enunciados de  [ejercicios](/home/recursos/ejercicios) )  
Intentaré colocar/ enlazar información aquí, y luego separando en páginas aparte según su volumen y contenido.
En la página de cada materia/curso se intentan poner recursos asociados, y también hay de elaboración propia:   
* Recursos para algunas materias (algunos recursos son apuntes):  
    * [Recursos FQ 2º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso)  
    * [Recursos FQ 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso)  
    * [Recursos FQ 4º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso)  
    * [Recursos FQ 1º Bachillerato](https://www.fiquipedia.es/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato)  
    * [Recursos 2º Bachillerato Física](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato)  
    * [Recursos 2º Bachillerato Química](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato)  
* Recuros por contenido (algunos recursos son apuntes):  
    * [Apuntes formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion)  
    * [Recursos Física](/home/recursos/fisica)  
    * [Recursos Química](/home/recursos/quimica)  

## Apuntes de elaboración propia
Además de enlaces a apuntes en la web, en esta página coloco [recursos y apuntes de elaboración propia](/home/recursos/recursos-de-elaboracion-propia)  con licenciamiento cc-by.  

* En la página de [Pizarras Física y Química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel) agrupo "pizarras" para distintos niveles (2ºESO, 3º ESO, 4ºESO, 1º Bachillerato, 2º Bachillerato), y enlazo también algunos apuntes.  
* Por curso se puede ver en materias cada materia, enlazo:  
    * [FQ 2º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso/apuntes-elaboracion-propia-fisica-y-quimica-2-eso/)  
    * [FQ 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso/apuntes-elaboracion-propia-3-eso)  
    * [FQ 4º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso/apuntes-fisica-y-quimica-4-eso-elaboracion-propia)  
    * [FQ 1º Bachillerato](https://www.fiquipedia.es/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato)  
    * [Física 2º Bachillerato](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato)  
    * [Química 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato)  

## Apuntes generales, física y química

Intentaré poner al principio de esta página y/o en otras páginas con apuntes por nivel, enlace a páginas que comparten apuntes interesantes  
   * **FisQuiWeb**, Espacio Web dedicado a la enseñanza de Física y Química. Luis Ignacio García González 
   [Apuntes - FisQuiWeb](http://fisquiweb.es/Apuntes/apuntes.htm)  Materiales bajo licencia Creative Commons (cc-by-nc-sa). Incluye apuntes de varios cursos (3º y 4º ESO, 1º Bachillerato, Física 2º Bachillerato y CMC 1º Bachillerato)
   * **Fisiquimicamente** Recursos de Física y Química, tutoriales y ciencia de actualidad. Rodrigo Alcaraz de la Osa 
   [Apuntes - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/)  cc-by-sa  
   Desde 2º ESO hasta 2º Bachillerato.
   * **CIDEAD**, Enseñanza a distancia  
   [Física y Química para la ESO (3º y 4º) - CIDEAD](http://recursostic.educacion.es/secundaria/edad/index_fq.htm)  cc-by-nc-sa  
   * **educarex**
   [Rincón dicáctico Física y Química - educarex.es](https://fisicayquimica.educarex.es/es/)  
   [Contenidos educativos digitales - educarex.es](https://conteni2.educarex.es/)  
   [Física y Química Secundaria - educarex.es](https://conteni2.educarex.es/?e=3)  
   [Física y Química Bachillerato - educarex.es](https://conteni2.educarex.es/?a=9882)  
   Algunos recursos enlazan con simulaciones  
   * **fisicayquimicaenflash.es** 
   [Apuntes 1º y 2º Bachillerato física](http://fisicayquimicaenflash.es/fisicateoria.htm)  
   [Apuntes 1º y 2º Bachillerato química](http://fisicayquimicaenflash.es/quimicateoria.htm)  
   [Apuntes 2º, 3º y 4º ESO física y química](http://fisicayquimicaenflash.es/eso/eso.html)  
   Ramón Flores Martínez Prof. de Física y Química IES Sánchez Lastra (Mieres - Asturias). Licenciamiento cc-by-nc-nd

   *  [Documentos de física y química para secundaria en LaTeX](http://sabina.pntic.mec.es/lmuf0005/fq.html)   
   [Apuntes de Física y Química en LaTex para modificar y adaptar con licencia de documentación libre. - bitbucket.org](https://bitbucket.org/llantones/apuntesfq/src/master/) Copyleft Luis Muñoz Fuenteluis.munoz.edu arroba juntadeandalucia.es  
   Licencia Creative Commons cc-by-sa 3.0 ES  
   * **Heurema**   
   [APUNTES DE FÍSICA Y QUÍMICA EN BACHILLERATO *waybackmachine*](http://web.archive.org/web/20180809021906/http://www.heurema.com/ApFQBachillG.htm)  
   Apuntes en pdf con ejercicios. Jaime Solá de los Santos, José Luis Hernández Pérez y Ricardo Fernández Cruz. Licenciamiento no detallado.
   [Subsección: Apuntes de Física (16-19 años)](http://web.archive.org/web/20180821000610/http://heurema.com/ApFBachillP.htm)  
   [Subsección: Apuntes de Química (16-19 años)](http://web.archive.org/web/20180821000610/http://heurema.com/ApQBachillP.htm)  
   [ENSEÑANZA DE LA FÍSICA Y LA QUÍMICA. Sección: Enseñanza Programada Personalizada - heurema.com](http://www.heurema.com/EPPG.htm)  
La enseñanza programada personalizada se ha programado en 3 cursos: CIENCIAS DE LA NATURALEZA en 2ºESO (13-14 años), FÍSICA Y QUÍMICA en 3º ESO (14-15 años) y FÍSICA Y QUÍMICA en 4ºESO(15-16 años), para acceder a uno de esos cursos se deberá hacer click cada subsección, con enlace, situado en la cabecera de esta página.  
   * Naturaleza educativa. Portal educativo de ciencias naturales y aplicadas.   
   [Física](http://www.natureduca.com/fis_indice.php)  
   [Química](http://www.natureduca.com/quim_indice.php)  
   Apuntes por bloques, mucho más que física y química. Asociación Española para la Cultura, el Arte y la Educación (ASOCAE O.N.G.D.) - ww.asocae.org - RNA 592727 - CIF.: G70195805 ¦ © Obra bajo licencia Creative Commons cc-by-nc-nd
   * ["Mirar al Cielo" Apuntes Física 2ºBachillerato *waybackmachine*](http://web.archive.org/web/20171228120158/http://www.miraralcielo.com/apuntes/index_apuntes.htm)  
   ["Mirar al Cielo" Apuntes Ámbito Científico Tecnológico *waybackmachine*](http://web.archive.org/web/20171228115849/http://www.miraralcielo.com/malet%C3%ADn%20web/apuntes_amb_cient/indexapuntes_ambito_ci.htm)  Copyright © Matías Vázquez Dereitos Reservados ISBN: 978-84-690-9821-9 
   * Fisilandia  
   [Apuntes](http://www.fisilandia.es/index.php/apuntes)  
   En 2021 redirige a [nuevo sitio en Google Sites](https://sites.google.com/view/fisilandia/inicio-fisilandia)  
   En 2021 2º, 3º ESO, 1º Bachillerato y 2º Bachillerato Física. Gabriel Villalobos con licenciamiento cc-by-nc-nd  
   * [Química 2º Bachillerato. Índice de temas. Apuntes, escritos y ensayos científicos](http://www.escritoscientificos.es/apunquim/inicio.htm)  Felipe Moreno Romero. Licenciamiento cc-by-nc-sa 
   * [Recursos, apuntes Departamento de Física y Química IES Reyes Católicos](https://www.iesreyescatolicos.es/sitio/index.php?option=com_content&view=article&id=79&Itemid=119#)  Apuntes en pdf por curso y bloques. Licenciamiento no detallado pero página web indica © 2021 IES Reyes Católicos
   *  [Material de trabajo  QUÍMICA 2º BACHILLERATO, IES Victoria Kent](http://www.educa.madrid.org/web/ies.victoriakent.torrejondeardoz/Departamentos/DFyQ/Materiales/Bach-2/bach-2.htm)  Sin licenciamiento detallado.
   * Departamento de Física y Química IES Ntra. Sra. de la Almudena - Madrid [http://almudenafyq.webcindario.com/index.html](http://almudenafyq.webcindario.com/index.html) 
   * [Física y química - Javier Robledano](http://sites.google.com/site/javirobledanofq/Home)  Recursos y materiales de aula, por curso y bloque. Javier Robledano Arillo. Licenciamiento cc-by-nc-nd
   * Profesor Jano  
   Materiales digitales de estudio y aprendizaje para bachillerato y preparación para la universidad.  
   [Blog profesor jano, página de Física *waybackmachine*](http://web.archive.org/web/20130625100935/http://www.profesorjano.info:80/p/pagina-de-fisica.html)  
   [Blog profesor Jano, página de Química *waybackmachine*](http://web.archive.org/web/20130414062127/http://www.profesorjano.info:80/p/pagina-de-quimica.html)   
   Esta página contiene los materiales que se van publicando sobre la asignatura de QUÍMICA de 2º de bachillerato. Licenciamiento no detallado.  
   [about.me profesorjano](http://about.me/profesorjano)  
   Blog académico de PROFESOR JANO en donde se publican los materiales que imparto ya sea en el colegio, en clases particulares o en educación a distancia.
   En 2021 en wordpress [PROFESOR JANO es Víctor M. Vitoria - Química](https://profesorjano.wordpress.com/quimica/)  
   *  quimife  
   [REPASO DEL CURSO: FÍSICA Y QUÍMICA 1º BACHILLERATO](http://quimife.wordpress.com/2010/04/12/repaso-del-curso-fisica-y-quimica-1%C2%BA-bachillerato/)  
   [REPASO DEL CURSO: QUÍMICA 2º BACHILLERATO](http://quimife.wordpress.com/2010/04/09/repaso-del-curso/) 
   Licenciamiento no detallado
   * [Asignaturas de Física y Química - 100ciaquimica.net](http://www.100ciaquimica.net/asigfq/index.htm)  Material con Copyright
   * [PÁGINA WEB PERSONAL DE CONTENIDOS DE FÍSICA Y QUÍMICA *waybackmachine*](http://web.archive.org/web/20130726150649/http://www.telefonica.net/web2/fisicayquimica/index.html)  IES ALMICERÁN Departamento de Física y Química. Copyright Francisco José Moreno Hueso
   * Departamento Física y Química IES Carmen Martín Gaite [http://ies.carmenmartingai.moralzarzal.educa.madrid.org/departamentos/quimica/materiales.html](http://ies.carmenmartingai.moralzarzal.educa.madrid.org/departamentos/quimica/materiales.html)  Apuntes y ejercicios por niveles desde 3º ESO hasta 2º Bachillerato
   * Página personal Oscar Carpintero Física y Química [http://www.mipaginapersonal.movistar.es/web3/oscar](http://www.mipaginapersonal.movistar.es/web3/oscar)  Apuntes por niveles
   * Apuntes y ejercicios física y química por niveles [http://www.iesanamariamatute.com/departamentos/dpto_fisica_quimica/fisica_apuntes_ejercicios.asp](http://www.iesanamariamatute.com/departamentos/dpto_fisica_quimica/fisica_apuntes_ejercicios.asp) 
   * Blog de Física y Química. IES Juan de la Cierva [http://aula44.wordpress.com/](http://aula44.wordpress.com/)  Apuntes por niveles desde 2º ESO hasta 2º Bachillerato.
   * [Departamento de Física y Química IES "Rey Fernando VI" @ Jesús Millán Crespo](http://chopo.pntic.mec.es/jmillan/)  Apuntes por niveles, con ejercicios.
   *  [http://www.matematicasfisicaquimica.com/](http://www.matematicasfisicaquimica.com/)  Recursos de Matemáticas, Física y Química por temas. Copyright
   *  [Apuntes /libro texto 3º ESO cation.es](http://www.cation.es/textos/FQ%203%C2%BA%20ESO.%20Apuntes.pdf)  
   [Apuntes /libro texto 4º ESO cation.es](http://www.cation.es/textos/FQ%204%C2%BA%20ESO.%20Apuntes.pdf)  
   Pdf que cita web desaparecida ciencias.com.es donde había apuntes de 1º a 4º ESO en español y bilingües Jaime Ruiz-Mateos  
   Indica "Este es un texto libre. Se puede imprimir, se puede fotocopiar, se puede copiar y transmitir por cualquier
medio mecánico o digital por expreso deseo del autor. Sólo queda prohibido su uso para fines comerciales."  
Ahora ofrece libros para su compra
   *  [Departamento Física y Química del IES Leopoldo Queipo de Melilla](https://fisicayquimicaqueipo.wixsite.com/misitio-1/apuntes-y-ejercicios)  Apuntes y ejercicios desde 3º ESO a 2º Bachillerato Verónica Fernández Martos, . Salvador Molina Burgos, Miguel Sánchez Cazorla, Departamento de Física y Química del IES Leopoldo Queipo de Melilla.
   *  [Apuntes de Física y Química - fqsecundaria.blogspot.com.es](http://fqsecundaria.blogspot.com.es/search/label/Apuntes)  Diego Palacios, curso 2016-2017 de 2º, 3º 4º ESO, 1º y 2ºBach
   *  [twitter colgandoclases](https://twitter.com/colgandoclases) Interesantes, aunque no son "apuntes" formalmente
   * [Física y Química - escire.com *waybackmachine*](http://web.archive.org/web/20190519164744/http://www.escire.com/fisica-y-quimica/)  Apuntes por curso y bloques. Ejercicios y laboratorio.Licenciamiento no detallado. Aurora Lucas.
   * [Materiales didácticos para descargar libremente - Formato (.pdf) *waybackmachine*](http://web.archive.org/web/20130709022607/http://vviana.es:80/materiales.html)  Vicente Viviana Martínez.
   * [Cajón de ciencias](http://www.cajondeciencias.com)  Recursos primaria y secundaria por temas  
   

Para Acceso Universidad y Universidad una fuente de apunte es [Wuolah](https://www.wuolah.com/)  

## Apuntes Física

   *  [Laplace - wiki del Departamento de Física Aplicada III en la Escuela Técnica Superior de Ingeniería de la Universidad de Sevilla.](http://laplace.us.es/wiki)  
   [Tesla - wiki del Departamento de Física Aplicada III en la Escuela Técnica Superior de Ingeniería de la Universidad de Sevilla.](http://tesla.us.es/wiki)  (dos servidores para evitar congestión)  
   El objeto de estas páginas es servir como almacén de apuntes y otros contenidos de las asignaturas impartidas por este Departamento. Este material es elaborado por los profesores del Departamento y no puede ser editado libremente. Licenciamiento cc-by-nc-sa. Aunque sea material de una universidad tiene material utilizable en bachillerato
   * [Física - hiru.eus](https://www.hiru.eus/es/fisica/)  
   Estática y dinámica, Calor y Temperatura, Gravedad, órbitas y rotación, Carga eléctrica y circuitos, Movimiento armónico y ondas, Física nuclear. cc-by-nc-sa Gobierno vasco.
   *  [Física - cajón de ciencias ](http://www.cajondeciencias.com/fisica.html)  Recursos física secundaria por temas
   *  [Yuri Milachay - slideshare.net](http://es.slideshare.net/ymilacha)  Física para medicina, Apuntes por bloques, Yuri Milachay 


### Apuntes Física 2º Bachillerato

   * Ver en [Recursos física 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato) 


## Apuntes Química

   * [Full Química , Química , Química inorgánica](http://www.fullquimica.com/)  César De Paz, Lima - Perú. Copyright
   * An Introduction to Chemistry by Mark Bishop.   
   [Atoms First Version of An Introduction to Chemistry by Mark Bishop](http://preparatorychemistry.com/Bishop_Atoms_First.htm)  
   [Chemistry First Version of An Introduction to Chemistry by Mark Bishop](http://preparatorychemistry.com/Bishop_Chemistry_First.htm)  
   Both versions are available as regular printed books, but they are also available on the Internet with no usernames and no passwords. 
   * [Apuntes/Teoría Facultad de Quimica, Quimica Organica, UNAM](http://organica1.org/teoria.html)  Dr. Carlos Rius Alonso, Universidad Nacional Autónoma de México
   * Chalkbored Chemistry Lessons 
   [Chemistry 11](http://www.chalkbored.com/lessons/chemistry-11.htm)  
   [Chemistry 12](http://www.chalkbored.com/lessons/chemistry-12.htm)  
   On this page you will find resources for high school chemistry: worksheets, labs, handouts, and PowerPoint lessons. All materials posted here are intended for educational nonprofit use. Feel free to use them in your classroom or for your personal use. However, they are not to be shared, sold, or distributed in any manner without permission.
   * [Antología de la química II](http://www.slideshare.net/tango67/antologa-de-quica-ii) 
   *  Apuntes docencia Pablo Fernández uclm.es  
   [Química general](https://blog.uclm.es/pablofdez/asignatura1/)  
   [Químíca analítica](https://blog.uclm.es/pablofdez/asignatura2/)   
   * [Química, cajón de ciencias](http://www.cajondeciencias.com/quimica.html)  
   * [Materiales escritos / resúmenes química de Ernesto de Jesús Alcañiz, Universidad de Alcalá de Henares](http://www2.uah.es/edejesus/resumenes/inicio.htm)  
   [Copyright](http://www2.uah.es/edejesus/copyright.htm) 


### Apuntes Química 2º Bachillerato

   * Ver en [Recursos química 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato) 
	
### Apuntes formulación
 Creada  [página aparte](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion)  
