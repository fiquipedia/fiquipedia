
# Currículo 3º ESO Física y Química (LOE)

Ver DECRETO 23/2007 en  [Legislación](/home/legislacion-educativa)   
  
Este currículo deja de estar en vigor en 2015/2016 según calendario de implantación LOMCE, ver  [currículo Física y Química 3º ESO (LOMCE)](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce)   
  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver  [Contenidos currículo con enlaces a recursos: 3º ESO Física y Química](/home/materias/eso/fisica-y-quimica-3-eso/cotenidoscurriculoenlacesrecursos3esofisicayquimica)   

Ver  [comparación currículo Física y Química 3º ESO](/home/materias/eso/fisica-y-quimica-3-eso/comparacion-curriculo-3-eso-fisica-y-quimica)


##  Objetivos
Los objetivos son de etapa, no específicos de curso, para el área de  [Ciencias de la Naturaleza](/home/materias/eso/ciencias-de-la-naturaleza-1y2eso) 

## Contenidos

### Bloque 1. Introducción a la metodología científica.
   * Utilización de estrategias propias del trabajo científico como el planteamiento de problemas y discusión de su interés, la formulación y puesta a prueba de hipótesis y la interpretación de los resultados. El informe científico. Análisis de datos organizados en tablas y gráficos.
   * Búsqueda y selección de información de carácter científico utilizando las tecnologías de la información y comunicación y otras fuentes.
   * Interpretación de información de carácter científico y utilización de dicha información para formarse una opinión propia, expresarse con precisión y argumentar sobre problemas relacionados con la naturaleza. La notación científica.
   * Valoración de las aportaciones de las ciencias de la naturaleza para dar respuesta a las necesidades de los seres humanos y mejorar las condiciones de su existencia, así como para apreciar y disfrutar de la diversidad natural y cultural, participando en su conservación, protección y mejora.
   * Utilización correcta de los materiales, sustancias e instrumentos básicos de un laboratorio. Carácter aproximado de la medida. Sistema internacional de unidades. El respeto por las normas de seguridad en el laboratorio.

### Bloque 2. Energía y electricidad.

#### El concepto de energía.
   * Energías tradicionales.
   * Energías alternativas.
   * Fuentes de energía renovables.
   * Conservación y degradación de la energía.

#### Electricidad.
   * Fenómenos electrostáticos.
   * Las cargas eléctricas y su interacción: las fuerzas eléctricas.
   * Campo eléctrico. Flujo de cargas. Conductores y aislantes.
   * La energía eléctrica. Generadores, resistores y corriente eléctrica. Circuitos eléctricos sencillos.
   * La electricidad en casa. El ahorro energético

### Bloque 3. Diversidad y unidad de estructura de la materia.

#### La materia, elementos y compuestos.
   * La materia y sus estados de agregación: sólido, líquido y gaseoso.
   * Teoría cinética y cambios de estado.
   * Sustancias puras y mezclas. Métodos de separación de mezclas. Disoluciones. Sustancias simples y compuestas.

#### Átomos, moléculas y cristales.
   * Estructura atómica: partículas constituyentes.
   * Utilización de modelos.
   * Número atómico.
   * Introducción al concepto de elemento químico.
   * Uniones entre átomos: moléculas y cristales.
   * Fórmulas y nomenclatura de las sustancias más corrientes según las normas de la IUPAC.
   * Masas atómicas y moleculares. Isótopos: concepto y aplicaciones.

### Bloque 4. Los cambios químicos y sus aplicaciones.

#### Las reacciones químicas.
   * Perspectivas macroscópica y atómico-molecular de los procesos químicos.
   * Representación simbólica.
   * Concepto de mol.
   * Ecuaciones químicas y su ajuste.
   * Conservación de la masa.
   * Cálculos de masa en reacciones químicas sencillas.
   * Realización experimental de algunos cambios químicos

#### La química en la sociedad.
   * Elementos químicos básicos en los seres vivos.
   * La química y el medioambiente: efecto invernadero, lluvia ácida, destrucción de la capa de ozono, contaminación de aguas y tierras.
   * Petróleo y derivados.
   * Energía nuclear.
   * Medicamentos.

## Criterios de evaluación
1. Determinar los rasgos distintivos del trabajo científico a través del análisis contrastado de algún problema científico o tecnológico de actualidad, así como su influencia sobre la calidad de vida de las personas.  
2. Realizar correctamente experiencias de laboratorio propuestas a lo largo del curso, respetando las normas de seguridad.  
3. Describir las interrelaciones existentes en la actualidad entre sociedad, ciencia y tecnología.  
4. Describir las características de los estados sólido, líquido y gaseoso. Explicar en qué consisten los cambios de estado, empleando la teoría cinética, incluyendo la comprensión de gráficas y el concepto de calor latente.  
5. Diferenciar entre elementos, compuestos y mezclas, así como explicar los procedimientos químicos básicos para su estudio. Describir las disoluciones. Efectuar correctamente cálculos numéricos sencillos sobre su composición. Explicar y emplear las técnicas de separación y purificación.  
6. Distinguir entre átomos y moléculas. Indicar las características de las partículas componentes de los átomos. Diferenciar los elementos. Calcular las partículas componentes de átomos, iones e isótopos.  
7. Formular y nombrar algunas sustancias importantes. Indicar sus propiedades. Calcular sus masas moleculares.  
8. Discernir entre cambio físico y químico. Comprobar que la conservación de la masa se cumple en toda reacción química. Escribir y ajustar correctamente ecuaciones químicas sencillas. Resolver ejercicios numéricos en los que intervengan moles.  
9. Enumerar los elementos básicos de la vida. Explicar cuáles son los principales problemas medioambientales de nuestra época y sus medidas preventivas.  
10. Explicar las características básicas de compuestos químicos de interés social: petróleo y derivados, y fármacos. Explicar los peligros del uso inadecuado de los medicamentos. Explicar en qué consiste la energía nuclear y los problemas derivados de ella.  
11. Demostrar una comprensión científica del concepto de energía. Razonar ventajas e inconvenientes de las diferentes fuentes energéticas. Enumerar medidas que contribuyen al ahorro colectivo o individual de energía. Explicar por qué la energía no puede reutilizarse sin límites.  
12. Describir los diferentes procesos de electrización de la materia. Clasificar materiales según su conductividad. Realizar ejercicios utilizando la ley de Coulomb. Indicar las diferentes magnitudes eléctricas y los componentes básicos de un circuito. Resolver ejercicios numéricos de circuitos sencillos. Saber calcular el consumo eléctrico en el ámbito doméstico.  
13. Diseñar y montar circuitos de corriente continua respetando las normas de seguridad en los que se puedan llevar a cabo mediciones de la intensidad de corriente y de diferencia de potencial, indicando las cantidades de acuerdo con la precisión del aparato utilizado.  

