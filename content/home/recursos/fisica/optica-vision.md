
# Recursos óptica visión / ojo humano

Se puede asociar a una parte de [óptica geométrica](/home/recursos/fisica/optica-geometrica) 

 [How the Human Eye Works - Animagraffs](https://animagraffs.com/human-eye/)   
 
 Se pueden comentar defectos, tests y correciones  
 [Daltonismo - wikipedia.org](https://es.wikipedia.org/wiki/Daltonismo)   

 [Ojo humano - fisicalab](https://www.fisicalab.com/apartado/ojo-humano)  
 
 [Test de Ishihara](https://www.clinicasnovovision.com/blog/test-ishihara/)  
 El test de Ishihara es una de las pruebas más utilizadas para diagnosticar y clasificar las alteraciones de la visión del color (discromatopsias), comúnmente conocidas como daltonismo.  
Este test fue inventado en la Universidad de Tokio por el doctor Shinobu Ishihara en 1917. Consta de 38 tarjetas compuestas por círculos con puntos de colores de tamaños aleatorios en su interior, estos puntos suelen formar números o laberintos.  
 
 ![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Ishihara_9.png/600px-Ishihara_9.png)  
 Ejemplo de una carta de color Ishihara. El número "74" debe ser claramente visible para los individuos con visión normal. Las personas con tricromacia pueden leer "21", y aquellos con acromatopsia no distinguen ningún número.  
 
 Test inverso:  
 Lámina 29: Una persona con visión normal, no vería nada, mientras que si existe anomalía del color se vería una línea.  
 
 [¿Quieres saber si padeces cierto grado de astigmatismo? Test](https://www.saludymedicina.org/post/quieres-saber-si-padeces-cierto-grado-de-astigmatismo-test)  
 ![](https://www.saludymedicina.org/wp-content/uploads/2017/03/ASTIGMATISMO.jpg)  

