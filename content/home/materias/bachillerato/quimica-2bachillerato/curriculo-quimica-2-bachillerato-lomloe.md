# Currículo Química 2º Bachillerato (LOMLOE)

En esta página se incluye el [currículo estatal (Real Decreto)](#Estatal) y el [currículo de Madrid (Decreto)](#Madrid)

## <a name="Estatal"></a> Estatal (Real Decreto)
Revisado con [Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521) citado en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  

En octubre 2021 borrador [Borrador de RD de enseñanzas mínimas de Bachillerato](https://educagob.educacionyfp.gob.es/dam/jcr:916a7585-8ed6-4f16-975b-dc23895a2581/proyecto-rd-bachilleratocompleto-.pdf)  
citado en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica) 

Revisado con [Proyecto de real decreto por el que se establece la ordenación y las enseñanzas mínimas del Bachillerato (PDF)](https://www.educacionyfp.gob.es/dam/jcr:e72aadd9-a4bc-4f00-985e-7167a254425b/prd-ensenanzas-minimas-bachillerato.pdf)  (Química en páginas 487 a 497 (proyecto 9 diciembre 2021) citado en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  
 
Actualizada [comparación currículo Quimica 2º Bachillerato](/home/materias/bachillerato/quimica-2bachillerato/comparacion-curriculo-quimica-2-bachillerato) con proyecto 9 diciembre 2021.  

Algunos cambios diciembre 2021 vs octubre 2021: Química se sustituye por química, alumnos por alumnado, "bases suficientes acerca de la química" por "bases químicas suficientes", "Aunque la metodología de cada docente es muy particular, es deseable que " por "Independientemente de la metodología aplicada en cada caso en el aula, es deseable que"   
El párrafo que comienza por "A través ..." se cambia de sito (pasa de ser 5º a 3º)  

Algunos cambios BOE abril vs diciembre, son mínimos: cambia "empíricas" por "formales", se elimina "al alumnado", "aborda" por "incluye", se elimina "y de todas las ciencias", "despertando" por "motivando", "se dedique" por "quiera dedicarse" y se elimina "tan apasionantes"    


En la naturaleza existen infinidad de procesos y fenómenos que la ciencia trata de explicar a través de diferentes leyes y teorías. El aprendizaje de disciplinas científicas **empíricas** como la química fomenta en los estudiantes el interés por comprender la realidad y valorar la relevancia de esta ciencia tan completa y versátil a partir del conocimiento de las aplicaciones que tiene en distintos contextos. Mediante el estudio de la química se consigue que el alumnado desarrolle competencias para comprender y describir cómo es la composición y la naturaleza de la materia y cómo se transforma. A lo largo de la Educación Secundaria Obligatoria y el 1.er curso de Bachillerato, el alumnado se ha iniciado en el conocimiento de la química y, mediante una primera aproximación, ha aprendido los principios básicos de esta ciencia, y cómo estos se aplican a la descripción de los fenómenos químicos más sencillos. A partir de aquí, el propósito principal de esta materia en 2.º de Bachillerato es profundizar sobre estos conocimientos para aportar al alumnado una visión más amplia de esta ciencia, y otorgarle una base química suficiente y las habilidades experimentales necesarias, con el doble fin de desarrollar un interés por la química y de que puedan continuar, si así lo desean, estudios relacionados.

Para alcanzar esta doble meta, este currículo de la materia de Química en 2.º curso de Bachillerato propone un conjunto de competencias específicas de marcado carácter abierto y generalista, pues se entiende que el aprendizaje competencial requiere de una metodología muy particular adaptada a la situación del grupo. Entender los fundamentos de los procesos y fenómenos químicos, comprender cómo funcionan los modelos y las leyes de la química y manejar correctamente el lenguaje químico forman parte de las competencias específicas de la materia. Otros aspectos referidos al buen concepto de la química como ciencia y sus relaciones con otras áreas de conocimiento, al desarrollo de técnicas de trabajo propias del pensamiento científico y a las repercusiones de la química en los contextos industrial, sanitario, económico y medioambiental de la sociedad actual, completan la formación competencial del alumnado, proporcionándole un perfil adecuado para desenvolverse según las demandas del mundo real.

A través del desarrollo de las competencias y los bloques de saberes asociados se logra una formación completa del alumnado en química. No obstante, para completar el desarrollo curricular de esta materia es necesario definir también sus criterios de evaluación que, como en el resto de materias de este currículo, son de carácter competencial por estar directamente relacionados con cada una de las competencias específicas que se han propuesto y con los descriptores competenciales del bachillerato. Por este motivo, el currículo de la materia de Química de 2.º de Bachillerato presenta, para cada una de las competencias específicas, un conjunto de criterios de evaluación que tienen un carácter abierto, yendo más allá de la mera evaluación de conceptos y contemplando una evaluación holística y global de los conocimientos, destrezas y actitudes propios de las competencias definidas para esta materia.

El aprendizaje de la Química en 2.º de Bachillerato estructura los saberes básicos en tres grandes bloques, que están organizados de manera independiente de forma que permitan abarcar los conocimientos, destrezas y actitudes básicos de esta ciencia adecuados a esta etapa educativa. Aunque se presenten en este documento con un orden prefijado, al no existir una secuencia definida para los bloques, la distribución a lo largo de un curso escolar permite una flexibilidad en temporalización y metodología.

En el primer bloque se profundiza sobre la estructura de la materia y el enlace químico, haciendo uso de principios fundamentales de la mecánica cuántica para la descripción de los átomos, su estructura nuclear y su corteza electrónica, y para el estudio de la formación y las propiedades de elementos y compuestos a través de los distintos tipos de enlaces químicos y de fuerzas intermoleculares.

El segundo bloque de saberes básicos introduce los aspectos más avanzados de las reacciones químicas sumando, a los cálculos estequiométricos de cursos anteriores, los fundamentos termodinámicos y cinéticos. A continuación, se **incluye** el estado de equilibrio químico resaltando la importancia de las reacciones reversibles en contextos cotidianos. Para terminar, se presentan ejemplos de reacciones químicas que deben ser entendidas como equilibrios químicos, como son las que se producen en la formación de precipitados, entre ácidos y bases y entre pares redox conjugados.

Por último, el tercer bloque abarca el amplio campo de la química en el que se describen a fondo la estructura y la reactividad de los compuestos orgánicos. Por su gran relevancia en la sociedad actual, la química del carbono es indicativa del progreso de una civilización, de ahí la importancia de estudiar en esta etapa cómo son los compuestos orgánicos y cómo reaccionan, para aplicarlo en polímeros y plásticos.

Este enfoque está en la línea del aprendizaje STEM, con el que se propone trabajar de manera global todo el conjunto de las disciplinas científicas. Independientemente de la metodología aplicada en cada caso en el aula, es deseable que las programaciones didácticas de esta materia contemplen esta línea de aprendizaje para darle un carácter más competencial, si cabe, al aprendizaje de la Química.

Las ciencias básicas que se incluyen en los estudios de Bachillerato contribuyen, todas por igual y de forma complementaria, al desarrollo de un perfil del alumnado basado en el cuestionamiento y el razonamiento que son propios del pensamiento científico. La química es, sin duda, una herramienta fundamental en la contribución de esos saberes científicos a proporcionar respuestas a las necesidades del ser humano. El fin último del aprendizaje de esta ciencia en la presente etapa es conseguir un conocimiento químico más profundo que desarrolle el pensamiento científico, **motivando** más preguntas, más conocimiento, más hábitos del trabajo característico de la ciencia y, en última instancia, más vocación, gracias a los que el alumnado **quiera dedicarse** a desempeños como la investigación y las actividades laborales científicas.

## Competencias Específicas 

Cambios RD abril vs diciembre 2021:  
En 2 se cambia "a través" por "en", "en base a" por "de acuerdo"  
En 4 se cambia "CPSAA7" por "CPSAA5"  
En 5 se cambia "fluidez" por "solvencia" y se añade CD2.  
En 6 se elimina "para la sociedad" y se cambia "CPSAA6" por "CPSAA3.2"  
 
1. Comprender, describir y aplicar los fundamentos de los procesos químicos más importantes, atendiendo a su base experimental y a los fenómenos que describen, para reconocer el papel relevante de la química en el desarrollo de la sociedad.
 
La química, como disciplina de las ciencias naturales, trata de descubrir a través de los procedimientos científicos cuáles son los porqués últimos de los fenómenos que ocurren en la naturaleza y de darles una explicación plausible a partir de las leyes científicas que los rigen. Además, esta disciplina tiene una importante base experimental que la convierte en una ciencia versátil y de especial relevancia para la formación clave del alumnado que vaya a optar por continuar su formación en itinerarios científicos, tecnológicos o sanitarios.

Con el desarrollo de esta competencia específica se pretende que el alumnado comprenda también que la química es una ciencia viva, cuyas repercusiones no solo han sido importantes en el pasado, sino que también suponen una importante contribución en la mejora de la sociedad presente y futura. A través de las distintas ramas de la química, el alumnado será capaz de descubrir cuáles son sus aportaciones más relevantes en la tecnología, la economía, la sociedad y el medioambiente.

Esta competencia específica se conecta con los siguientes descriptores: STEM1, STEM2, STEM3, CE1.

2. Adoptar los modelos y leyes de la química aceptados como base de estudio de las propiedades de los sistemas materiales, para inferir soluciones generales a los problemas cotidianos relacionados con las aplicaciones prácticas de la química y sus repercusiones en el medioambiente.
 
La ciencia química constituye un cuerpo de conocimiento racional, coherente y completo cuyas leyes y teorías se fundamentan en principios básicos y observaciones experimentales. Sería insuficiente, sin embargo, que el alumnado aprendiese química solo en este aspecto. Es necesario demostrar que el modelo coherente de la naturaleza que se presenta **en** esta ciencia es válido a través del contacto con situaciones cotidianas y con las preguntas que surgen de la observación de la realidad. Así, el alumnado que estudie esta disciplina debe ser capaz de identificar los principios básicos de la química que justifican que los sistemas materiales tengan determinadas propiedades y aplicaciones **de acuerdo** con su composición y que existe una base fundamental de carácter químico en el fondo de cada una de las cuestiones medioambientales actuales y, sobre todo, en las ideas y métodos para solucionar los problemas relacionados con ellas.

Solo desde este conocimiento profundo de la base química de la naturaleza de la materia y de los cambios que le afectan se podrán encontrar respuestas y soluciones efectivas a cuestiones reales y prácticas, tal y como se presentan a través de nuestra percepción o se formulan en los medios de comunicación.

Esta competencia específica se conecta con los siguientes descriptores: CCL2, STEM2, STEM5, CD5, CE1.

3. Utilizar con corrección los códigos del lenguaje químico (nomenclatura química, unidades, ecuaciones, etc.), aplicando sus reglas específicas, para emplearlos como base de una comunicación adecuada entre diferentes comunidades científicas y **como** herramienta fundamental en la investigación de esta ciencia.

La química utiliza lenguajes cuyos códigos son muy específicos y que es necesario conocer para trabajar en esta disciplina y establecer relaciones de comunicación efectiva entre los miembros de la comunidad científica. En un sentido amplio, esta competencia no se enfoca exclusivamente en utilizar de forma correcta las normas de la IUPAC para nombrar y formular, sino que también hace alusión a todas las herramientas que una situación relacionada con la química pueda requerir, como las herramientas matemáticas que se refieren a ecuaciones y operaciones, o los sistemas de unidades y las conversiones adecuadas dentro de ellos, por ejemplo.

El correcto manejo de datos e información relacionados con la química, sea cual sea el formato en que sean proporcionados, es fundamental para la interpretación y resolución de problemas, la elaboración correcta de informes científicos e investigaciones, la ejecución de prácticas de laboratorio, o la resolución de ejercicios, por ejemplo. Debido a ello, esta competencia específica supone un apoyo muy importante para la ciencia en general, y para la química en particular.

Esta competencia específica se conecta con los siguientes descriptores: STEM4, CCL1, CCL5, CPSAA4, CE3.

4. Reconocer la importancia del uso responsable de los productos y procesos químicos, elaborando argumentos informados sobre la influencia positiva que la química tiene sobre la sociedad actual, para contribuir a superar las connotaciones negativas que en multitud de ocasiones se atribuyen al término «químico».
 
Existe la idea generalizada en la sociedad, quizás influida por los medios de comunicación, especialmente en los relacionados con la publicidad de ciertos productos, de que los productos químicos, y la química en general, son perjudiciales para la salud y el medioambiente. Esta creencia se sustenta, en la mayoría de las ocasiones, en la falta de información y de alfabetización científica de la población. El alumnado que estudia Química debe ser consciente de que los principios fundamentales que explican el funcionamiento del universo tienen una base científica, así como ser capaz de explicar que las sustancias y procesos naturales se pueden describir y justificar a partir de los conceptos de esta ciencia.
 
Además de esto, las ideas aprendidas y practicadas en esta etapa les deben capacitar para argumentar y explicar los beneficios que el progreso de la química ha tenido sobre el bienestar de la sociedad y que los problemas que a veces conllevan estos avances son causados por el empleo negligente, desinformado, interesado o irresponsable de los productos y procesos que ha generado el desarrollo de la ciencia y la tecnología.

Esta competencia específica se conecta con los siguientes descriptores: STEM1, STEM5, **CPSAA5**, CE2.

5. Aplicar técnicas de trabajo propias de las ciencias experimentales y el razonamiento lógico-matemático en la resolución de problemas de química y en la interpretación de situaciones relacionadas, valorando la importancia de la cooperación, para poner en valor el papel de la química en una sociedad basada en valores éticos y sostenibles.
 
En toda actividad científica la colaboración entre diferentes individuos y entidades es fundamental para conseguir el progreso científico. Trabajar en equipo, utilizar con solvencia herramientas digitales y recursos variados y compartir los resultados de los estudios, respetando siempre la atribución de los mismos, repercute en un crecimiento notable de la investigación científica, pues el avance es cooperativo. Que haya una apuesta firme por la mejora de la investigación científica, con hombres y mujeres que deseen dedicarse a ella por vocación, es muy importante para nuestra sociedad actual pues implica la mejora de la calidad de vida, la tecnología y la salud, entre otras.
 
El desarrollo de esta competencia específica persigue que el alumnado se habitúe desde esta etapa a trabajar de acuerdo a los principios básicos que se ponen en práctica en las ciencias experimentales y desarrolle una afinidad por la ciencia, por las personas que se dedican a ella y por las entidades que la llevan a cabo y que trabajan por vencer las desigualdades de género, orientación, creencia, etc. A su vez, adquirir destrezas en el uso del razonamiento científico les da la capacidad de interpretar y resolver situaciones problemáticas en diferentes contextos de la investigación, el mundo laboral y su realidad cotidiana.

Esta competencia específica se conecta con los siguientes descriptores: STEM1, STEM2, STEM3, CD1, **CD2**, CD3, CD5.

6. Reconocer y analizar la química como un área de conocimiento multidisciplinar y versátil, poniendo de manifiesto las relaciones con otras ciencias y campos de conocimiento, para realizar a través de ella una aproximación holística al conocimiento científico y global.
 
No es posible comprender profundamente los conceptos fundamentales de la química sin conocer las leyes y teorías de otros campos de la ciencia relacionados con ella. De la misma forma, es necesario aplicar las ideas básicas de la química para entender los fundamentos de otras disciplinas científicas. Al igual que la sociedad está profundamente interconectada, la química no es una disciplina científica aislada, y las contribuciones de la química al desarrollo de otras ciencias y campos de conocimiento (y viceversa) son imprescindibles para el progreso global de la ciencia, la tecnología y la sociedad.
 
Para que el alumnado llegue a ser competente desarrollará su aprendizaje a través del estudio experimental y la observación de situaciones en las que se ponga de manifiesto esta relación interdisciplinar; la aplicación de herramientas tecnológicas en la indagación y la experimentación; y el empleo de herramientas matemáticas y el razonamiento lógico en la resolución de problemas propios de la química. Esta base de carácter interdisciplinar y holístico que es inherente a la química proporciona a los alumnos y alumnas que la estudian unos cimientos adecuados para que puedan continuar estudios en diferentes ramas de conocimiento, y a través de diferentes itinerarios formativos, lo que contribuye de forma eficiente a la formación de personas competentes.
 
Esta competencia específica se conecta con los siguientes descriptores: STEM4, CPSAA3.2, CC4

## Criterios de evaluación

En Real Decreto abril vs diciembre 2021:  
En 1.2 cambia "disciplinas" por "ramas"   
En 5.1 cambia "cada disciplina" por "cada una de ellas"  

### Competencia específica 1
1.1 Reconocer la importancia de la química y sus conexiones con otras áreas en el desarrollo de la sociedad, el progreso de la ciencia, la tecnología, la economía y el desarrollo sostenible respetuoso con el medioambiente, identificando los avances en el campo de la química que han sido fundamentales en estos aspectos.

1.2 Describir los principales procesos químicos que suceden en el entorno y las propiedades de los sistemas materiales a partir de los conocimientos, destrezas y actitudes propios de las distintas **ramas** de la química.

1.3 Reconocer la naturaleza experimental e interdisciplinar de la química y su influencia en la investigación científica y en los ámbitos económico y laboral actuales, considerando los hechos empíricos y sus aplicaciones en otros campos del conocimiento y la actividad humana.

### Competencia específica 2
2.1 Relacionar los principios de la química con los principales problemas de la actualidad asociados al desarrollo de la ciencia y la tecnología, analizando cómo se comunican a través de los medios de comunicación o son observados en la experiencia cotidiana.

2.2 Reconocer y comunicar que las bases de la química constituyen un cuerpo de conocimiento imprescindible en un marco contextual de estudio y discusión de cuestiones significativas en los ámbitos social, económico, político y ético identificando la presencia e influencia de estas bases en dichos ámbitos.

2.3 Aplicar de manera informada, coherente y razonada los modelos y leyes de la química, explicando y prediciendo las consecuencias de experimentos, fenómenos naturales, procesos industriales y descubrimientos científicos.

### Competencia específica 3
3.1 Utilizar correctamente las normas de nomenclatura de la IUPAC como base de un lenguaje universal para la química que permita una comunicación efectiva en toda la comunidad científica, aplicando dichas normas al reconocimiento y escritura de fórmulas y nombres de diferentes especies químicas.

3.2 Emplear con rigor herramientas matemáticas para apoyar el desarrollo del pensamiento científico que se alcanza con el estudio de la química, aplicando estas herramientas en la resolución de problemas usando ecuaciones, unidades, operaciones, etc.

3.3 Practicar y hacer respetar las normas de seguridad relacionadas con la manipulación de sustancias químicas en el laboratorio y en otros entornos, así como los procedimientos para la correcta gestión y eliminación de los residuos, utilizando correctamente los códigos de comunicación característicos de la química.

### Competencia específica 4
4.1 Analizar la composición química de los sistemas materiales que se encuentran en el entorno más próximo, en el medio natural y en el entorno industrial y tecnológico, demostrando que sus propiedades, aplicaciones y beneficios están basados en los principios de la química.

4.2 Argumentar de manera informada, aplicando las teorías y leyes de la química, que los efectos negativos de determinadas sustancias en el ambiente y en la salud se deben al mal uso que se hace de esos productos o negligencia, y no a la ciencia química en sí.

4.3 Explicar, empleando los conocimientos científicos adecuados, cuáles son los beneficios de los numerosos productos de la tecnología química y cómo su empleo y aplicación han contribuido al progreso de la sociedad.

### Competencia específica 5
5.1 Reconocer la importante contribución en la química del trabajo colaborativo entre especialistas de diferentes disciplinas científicas poniendo de relieve las conexiones entre las leyes y teorías propias de cada **una de ellas**.

5.2 Reconocer la aportación de la química al desarrollo del pensamiento científico y a la autonomía de pensamiento crítico a través de la puesta en práctica de las metodologías de trabajo propias de las disciplinas científicas.

5.3 Resolver problemas relacionados con la química y estudiar situaciones relacionadas con esta ciencia, reconociendo la importancia de la contribución particular de cada miembro del equipo y la diversidad de pensamiento y consolidando habilidades sociales positivas en el seno de equipos de trabajo.

5.4 Representar y visualizar de forma eficiente los conceptos de química que presenten mayores dificultades, utilizando herramientas digitales y recursos variados, incluyendo experiencias de laboratorio real y virtual.

### Competencia específica 6
6.1 Explicar y razonar los conceptos fundamentales que se encuentran en la base de la química aplicando los conceptos, leyes y teorías de otras disciplinas científicas (especialmente de la física) a través de la experimentación y la indagación.

6.2 Deducir las ideas fundamentales de otras disciplinas científicas (por ejemplo, la biología o la tecnología) por medio de la relación entre sus contenidos básicos y las leyes y teorías que son propias de la química.

6.3 Solucionar problemas y cuestiones que son característicos de la química utilizando las herramientas provistas por las matemáticas y la tecnología, reconociendo así la relación entre los fenómenos experimentales y naturales y los conceptos propios de esta disciplina.

## Saberes básicos  

Algunos cambios diciembre 2021 vs octubre 2021:  
Casi todos los saberes básicos en octubre 2021 comenzaban con una acción (deducción, aplicación, análisis ...) y pasan a enumerar un concepto. Por ejemplo "análisis de reacciones entre..." pasa por "reacciones entre ..."  
Los "capítulos" I. Enlace químico y estructura de la materia tenían subapartados A, B, ... y ahora pasan a ser A con subapartados 1, 2, ...  
Se elimina "para situarlo en su grupo y periodo correspondiente."  
"D. Enlaces intra e intermoleculares" pasa a ser "Enlace químico y fuerzas intermoleculares", faltaría el 4  
Se elimina "Aplicación del modelo microscópico para deducir ..."  
Se elimina "(naturaleza de los reactivos, temperatura, concentración, presión, área superficial, presencia de un catalizador)"  
Se añade desarrolladas en "moleculares y desarrolladas"  
En reactividad química se elimina "escribir y ajustar"  

Cambios Real Decreto abril 2022 vs diciembre 2021: casi ninguno, se cambia "en base a" por "según"  


A. Enlace químico y estructura de la materia.

1. Espectros atómicos.

- Los espectros atómicos como responsables de la necesidad de la revisión del modelo atómico. Relevancia de este fenómeno en el contexto del desarrollo histórico del modelo atómico.

- Interpretación de los espectros de emisión y absorción de los elementos. Relación con la estructura electrónica del átomo.

2. Principios cuánticos de la estructura atómica.

- Relación entre el fenómeno de los espectros atómicos y la cuantización de la energía. Del modelo de Bohr a los modelos mecano-cuánticos: necesidad de una estructura electrónica en diferentes niveles.

- Principio de incertidumbre de Heisenberg y doble naturaleza onda-corpúsculo del electrón. Naturaleza probabilística del concepto de orbital.

- Números cuánticos y principio de exclusión de Pauli. Estructura electrónica del átomo. Utilización del diagrama de Moeller para escribir la configuración electrónica de los elementos químicos.

3. Tabla periódica y propiedades de los átomos.

- Naturaleza experimental del origen de la tabla periódica en cuanto al agrupamiento de los elementos **según** sus propiedades. La teoría atómica actual y su relación con las leyes experimentales observadas.

- Posición de un elemento en la tabla periódica a partir de su configuración electrónica.

- Tendencias periódicas. Aplicación a la predicción de los valores de las propiedades de los elementos de la tabla a partir de su posición en la misma.

- Enlace químico y fuerzas intermoleculares.

- Tipos de enlace a partir de las características de los elementos individuales que lo forman. Energía implicada en la formación de moléculas, de cristales y de estructuras macroscópicas. Propiedades de las sustancias químicas.

- Modelos de Lewis, RPECV e hibridación de orbitales. Configuración geométrica de compuestos moleculares y las características de los sólidos.

- Ciclo de Born-Haber. Energía intercambiada en la formación de cristales iónicos.

- Modelos de la nube electrónica y la teoría de bandas para explicar las propiedades características de los cristales metálicos.

- Fuerzas intermoleculares a partir de las características del enlace químico y la geometría de las moléculas. Propiedades macroscópicas de compuestos moleculares.

B. Reacciones químicas.

1. Termodinámica química.

- Primer principio de la termodinámica: intercambios de energía entre sistemas a través del calor y del trabajo.

- Ecuaciones termoquímicas. Concepto de entalpía de reacción. Procesos endotérmicos y exotérmicos.

- Balance energético entre productos y reactivos mediante la ley de Hess, a través de la entalpía de formación estándar o de las energías de enlace, para obtener la entalpía de una reacción.

- Segundo principio de la termodinámica. La entropía como magnitud que afecta a la espontaneidad e irreversibilidad de los procesos químicos.

- Cálculo de la energía de Gibbs de las reacciones químicas y espontaneidad de las mismas en función de la temperatura del sistema.

2. Cinética química.

- Teoría de las colisiones como modelo a escala microscópica de las reacciones químicas. Conceptos de velocidad de reacción y energía de activación.

- Influencia de las condiciones de reacción sobre la velocidad de la misma.

- Ley diferencial de la velocidad de una reacción química y los órdenes de reacción a partir de datos experimentales de velocidad de reacción.

3. Equilibrio químico.

- El equilibrio químico como proceso dinámico: ecuaciones de velocidad y aspectos termodinámicos. Expresión de la constante de equilibrio mediante la ley de acción de masas.

- La constante de equilibrio de reacciones en las que los reactivos se encuentren en diferente estado físico. Relación entre KC y KP y producto de solubilidad en equilibrios heterogéneos.

- Principio de Le Châtelier y el cociente de reacción. Evolución de sistemas en equilibrio a partir de la variación de las condiciones de concentración, presión o temperatura del sistema.

4. Reacciones ácido-base.

- Naturaleza ácida o básica de una sustancia a partir de las teorías de Arrhenius y de Brønsted y Lowry.

- Ácidos y bases fuertes y débiles. Grado de disociación en disolución acuosa.

- pH de disoluciones ácidas y básicas. Expresión de las constantes Ka y Kb.

- Concepto de pares ácido y base conjugados. Carácter ácido o básico de disoluciones en las que se produce la hidrólisis de una sal.

- Reacciones entre ácidos y bases. Concepto de neutralización. Volumetrías ácido-base.

- Ácidos y bases relevantes a nivel industrial y de consumo, con especial incidencia en el proceso de la conservación del medioambiente.

5. Reacciones redox.

- Estado de oxidación. Especies que se reducen u oxidan en una reacción a partir de la variación de su número de oxidación.

- Método del ion-electrón para ajustar ecuaciones químicas de oxidación-reducción. Cálculos estequiométricos y volumetrías redox.

- Potencial estándar de un par redox. Espontaneidad de procesos químicos y electroquímicos que impliquen a dos pares redox.

- Leyes de Faraday: cantidad de carga eléctrica y las cantidades de sustancia en un proceso electroquímico. Cálculos estequiométricos en cubas electrolíticas.

- Reacciones de oxidación y reducción en la fabricación y funcionamiento de baterías eléctricas, celdas electrolíticas y pilas de combustible, así como en la prevención de la corrosión de metales.

C. Química orgánica.

1. Isomería.

- Fórmulas moleculares y desarrolladas de compuestos orgánicos. Diferentes tipos de isomería estructural.

- Modelos moleculares o técnicas de representación 3D de moléculas. Isómeros espaciales de un compuesto y sus propiedades.

2. Reactividad orgánica.

- Principales propiedades químicas de las distintas funciones orgánicas. Comportamiento en disolución o en reacciones químicas.

- Principales tipos de reacciones orgánicas. Productos de la reacción entre compuestos orgánicos y las correspondientes ecuaciones químicas.

3. Polímeros.

- Proceso de formación de los polímeros a partir de sus correspondientes monómeros. Estructura y propiedades.

- Clasificación de los polímeros según su naturaleza, estructura y composición. Aplicaciones, propiedades y riesgos medioambientales asociados.

## <a name="Madrid"></a> Madrid (Decreto)

Revisado con [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF)  
Física 2º Bachillerato en página 352 del pdf  

Cambios en [Proyecto de decreto 31 mayo](https://www.comunidad.madrid/transparencia/sites/default/files/04_2_2022-05-31_decreto_bachillerato-a.pdf)  

En decreto Madrid texto inicial cambia completamente de redacción, no uso negrita  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
Se cambia varias veces "situación de aprendizaje"/"situaciones de aprendizaje" por "actividad"/"actividades"  


A la hora de abordar el estudio de la Química como disciplina aislada en Bachillerato es necesario considerar su carácter de ciencia experimental que usa una gran cantidad de modelos y un lenguaje específico para racionalizar y sistematizar los hechos experimentales. En cursos anteriores se han abordado varios de estos aspectos, pero la asignatura de Química en el segundo curso de esta etapa es la primera ocasión donde la materia se presenta de manera independiente, por lo que su desarrollo debe ser más formal y riguroso, atendiendo a la mayor madurez de alumnado. De esta manera, se proporciona la base competencial necesaria para futuros estudios, no solo en Química sino en materias relacionadas como, por ejemplo, la Biología.

Partiendo de esta idea, los contenidos del curso se han distribuido en tres bloques. Sin embargo, la ordenación propuesta permite cierta flexibilidad tanto en el orden de impartición de los contenidos dentro de dichos bloques, como en la temporalización, metodología y profundidad de los mismos.

En el primer bloque, llamado «Enlace químico y estructura de la materia», se hace un repaso de los modelos atómicos clásicos, ya vistos en cursos anteriores, se profundiza en el modelo atómico de Bohr y en el mecano-cuántico. De esta forma, se describen las configuraciones electrónicas y se racionaliza la formación de los distintos tipos de enlaces químicos. Todo esto permite relacionar las propiedades de las sustancias químicas con su estructura. En este bloque, aparte de experiencias de laboratorio, resulta muy útil el uso de simuladores y modelos virtuales que ilustren los conceptos tratados.

En el segundo bloque, denominado «Reacciones químicas», se tratan, en primer lugar, los fundamentos termodinámicos y cinético-mecanísticos que gobiernan las reacciones químicas. En segundo lugar, se introduce el concepto de equilibrio químico, el cual permite afrontar el estudio de las reacciones ácido-base, incluyendo los equilibrios heterogéneos, y las reacciones redox. La realización de prácticas de laboratorio ayuda a reforzar y dotar de significado a los conceptos tratados.

«Química orgánica» es el último bloque de la materia y en él se realiza una aproximación a la estructura y reactividad de los compuestos orgánicos. Tras consolidar la nomenclatura se indaga en los diferentes tipos de isomería. Después de haber visto estos aspectos estructurales, se aborda la reactividad de los grupos funcionales más habituales. Finalmente, se tratan los polímeros por su importancia industrial y biológica. La construcción y uso de modelos moleculares, ya sean reales o virtuales, constituye una valiosa herramienta a la hora de abordar este bloque.

La materia de Química se encuentra dentro de las disciplinas STEM, por lo que en la metodología se trabajará de manera global todo el conjunto de las disciplinas científicas, dado que es necesario subrayar la relación de la Química con otras asignaturas, especialmente Física, Tecnología e Ingeniería y Biología y Geología, así como las repercusiones que la disciplina objeto de estudio tiene en la sociedad. Todo lo anterior es una oportunidad para aumentar el interés del alumnado por la química.

El desarrollo de las competencias asociadas a la asignatura y al Bachillerato en general, implica la comprensión de los fenómenos químicos y de los modelos que los intentan explicar. La resolución de ejercicios y de problemas en contextos variados es un componente fundamental de la asignatura, constituyendo una gran oportunidad para el aprendizaje cooperativo. En la medida de lo posible se llevará a cabo trabajo experimental que permita la adquisición de las competencias de la etapa.

El uso de actividades competenciales bien diseñadas y basadas en situaciones reales permitirá analizar la química desde un enfoque interdisciplinar, donde también se abordarán valores tales como el respeto, el trabajo en equipo, el rechazo hacia actitudes que muestren cualquier tipo de discriminación y el compromiso con la sostenibilidad.

A modo de ejemplo se podría proponer la siguiente actividad: un estudio de la industria del vinagre basado en cuestiones como qué es, cómo se produce y cuál es su etiquetado, aporta un contexto a los conceptos y problemas referentes a las secciones de ácido-base y química orgánica, aparte de las cuestiones relacionadas con la materia de Biología. De esta forma, se podrían desarrollar las competencias específicas 1, 3, 4 y 5 de la asignatura.

## Competencias específicas  
Comparación proyecto Decreto Madrid frente a Real Decreto:  
En todas se añade "recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril"  
En 4 se elimina "el bienestar"  
En 5 se elimina "valorando la importancia de la cooperación, para poner en valor el papel de la química en una sociedad basada en valores éticos y sostenibles." y se elimina "de género, orientación, creencia, etc"  
En 6 se cambia "alumnos y alumnas" por "alumnado"  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
Al enumerar competencias específicas se añade un "y" antes de la final, antes había una coma entre todas.  


1. Comprender, describir y aplicar los fundamentos de los procesos químicos más importantes, atendiendo a su base experimental y a los fenómenos que describen, para reconocer el papel relevante de la química en el desarrollo de la sociedad.  
La química, como disciplina de las ciencias naturales, trata de descubrir a través de los procedimientos científicos cuáles son los porqués últimos de los fenó menos que ocurren en la naturaleza y de darles una explicación plausible a partir de las leyes científicas que los rigen.  
Además, esta disciplina tiene una importante base experimental que la convierte en una ciencia versátil y de especial relevancia para la formación clave del alumnado que vaya a optar por continuar su formación en itinerarios científicos, tecnológicos o sanitarios.  
Con el desarrollo de esta competencia específica se pretende que el alumnado comprenda también que la química es una ciencia viva, cuyas repercusiones no solo han sido importantes en el pasado, sino que también suponen una importante contribución en la mejora de la sociedad presente y futura. A través de las distintas ramas de la química, el alumnado será capaz de descubrir cuáles son sus aportaciones más relevantes en la tecnología, la economía, la sociedad y el medioambiente.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM1, STEM2, STEM3 y CE1.

2. Adoptar los modelos y leyes de la química aceptados como base de estudio de las propiedades de los sistemas materiales, para inferir soluciones generales a los problemas cotidianos relacionados con las aplicaciones prácticas de la química y sus repercusiones en el medioambiente.  
La ciencia química constituye un cuerpo de conocimiento racional, coherente y completo cuyas leyes y teorías se fundamentan en principios básicos y observaciones experimentales. Sería insuficiente, sin embargo, que el alumnado aprendiese química solo en este aspecto. Es necesario
demostrar que el modelo coherente de la naturaleza que se presenta en esta ciencia es válido a través del contacto con situaciones cotidianas y con las preguntas que surgen de la observación de la realidad. Así, el alumnado que estudie esta disciplina debe ser capaz de identificar los principios básicos de la química que justifican que los sistemas materiales tengan determinadas propiedades y aplicaciones de acuerdo con su composición y que existe una base fundamental de carácter
químico en el fondo de cada una de las cuestiones medioambientales actuales y, sobre todo, en las ideas y métodos para solucionar los problemas relacionados con ellas.  
Solo desde este conocimiento profundo de la base química de la naturaleza de la materia y de los cambios que le afectan se podrán encontrar respuestas y soluciones efectivas a cuestiones reales y prácticas, tal y como se presentan a través de nuestra percepción o se formulan en los medios de comunicación.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: CCL2, STEM2, STEM5, CD5 y CE1.

3. Utilizar con corrección los códigos del lenguaje químico (nomenclatura química, unidades, ecuaciones, etc.), aplicando sus reglas específicas, para emplearlos como base de una comunicación adecuada entre diferentes comunidades científicas y como herramienta fundamental en la investigación de esta ciencia.  
La química utiliza lenguajes cuyos códigos son muy específicos y que es necesario conocer para trabajar en esta disciplina y establecer relaciones de comunicación efectiva entre los miembros
de la comunidad científica. En un sentido amplio, esta competencia no se enfoca exclusivamente en utilizar de forma correcta las normas de la IUPAC para nombrar y formular, sino que también hace alusión a todas las herramientas que una situación relacionada con la química pueda requerir, como las herramientas matemáticas que se refieren a ecuaciones y operaciones, o los sistemas de unidades y las conversiones adecuadas dentro de ellos, por ejemplo.  
El correcto manejo de datos e información relacionados con la química, sea cual sea el formato en que sean proporcionados, es fundamental para la interpretación y resolución de problemas, la elaboración correcta de informes científicos e investigaciones, la ejecución de prácticas de laboratorio, o la resolución de ejercicios, por ejemplo. Debido a ello, esta competencia específica supone un apoyo muy importante para la ciencia en general, y para la química en particular.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM4, CCL1, CCL5, CPSAA4 y CE3.

4. Reconocer la importancia del uso responsable de los productos y procesos químicos, elaborando argumentos informados sobre la influencia positiva que la química tiene sobre la sociedad actual, para contribuir a superar las connotaciones negativas que en multitud de ocasiones se atribuyen al término «químico».  
Existe la idea generalizada en la sociedad, quizás influida por los medios de comunicación, especialmente en los relacionados con la publicidad de ciertos productos, de que los productos químicos, y la química en general, son perjudiciales para la salud y el medioambiente. Esta creencia se sustenta, en la mayoría de las ocasiones, en la falta de información y de alfabetización científica de la población. El alumnado que estudia química debe ser consciente de que los principios fundamentales que explican el funcionamiento del universo tienen una base científica, así como ser capaz de explicar que las sustancias y procesos naturales se pueden describir y justificar a partir de los conceptos de esta ciencia.  
Además de esto, las ideas aprendidas y practicadas en esta etapa les deben capacitar para argumentar y explicar los beneficios que el progreso de la química ha tenido sobre la sociedad y que los problemas que a veces conllevan estos avances son causados por el empleo negligente, desinformado, interesado o irresponsable de los productos y procesos que ha generado el desarrollo de la ciencia y la tecnología.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM1, STEM5, CPSAA5 y CE2.

5. Aplicar técnicas de trabajo propias de las ciencias experimentales y el razonamiento lógico-matemático en la resolución de problemas de química y en la interpretación de situaciones relacionadas.  
En toda actividad científica la colaboración entre diferentes individuos y entidades es fundamental para conseguir el progreso científico. Trabajar en equipo, utilizar con solvencia herramientas digitales y recursos variados y compartir los resultados de los estudios, respetando
siempre la atribución de los mismos, repercute en un crecimiento notable de la investigación científica, pues el avance es cooperativo. Que haya una apuesta firme por la mejora de la investigación científica, con hombres y mujeres que deseen dedicarse a ella por vocación, es muy importante para nuestra sociedad actual pues implica la mejora de la calidad de vida, la tecnología y la salud, entre otras.  
El desarrollo de esta competencia específica persigue que el alumnado se habitúe desde esta etapa a trabajar de acuerdo a los principios básicos que se ponen en práctica en las ciencias experimentales y desarrolle una afinidad por la ciencia, por las personas que se dedican a ella y por las entidades que la llevan a cabo y que trabajan por vencer las desigualdades. A su vez, adquirir destrezas en el uso del razonamiento científico les da la capacidad de interpretar y resolver situaciones problemáticas en diferentes contextos de la investigación, el mundo laboral y su realidad cotidiana.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM1, STEM2, STEM3, CD1, CD2, CD3 y CD5.

6. Reconocer y analizar la química como un área de conocimiento multidisciplinar y versátil, poniendo de manifiesto las relaciones con otras ciencias y campos de conocimiento, para realizar a través de ella una aproximación holística al conocimiento científico y global.  
No es posible comprender profundamente los conceptos fundamentales de la química sin conocer las leyes y teorías de otros campos de la ciencia relacionados con ella. De la misma forma, es necesario aplicar las ideas básicas de la química para entender los fundamentos de otras disciplinas científicas. Al igual que la sociedad está profundamente interconectada, la química no es una disciplina científica aislada, y las contribuciones de la química al desarrollo de otras ciencias y campos de conocimiento (y viceversa) son imprescindibles para el progreso global de la ciencia, la tecnología y la sociedad.  
Para que el alumnado llegue a ser competente desarrollará su aprendizaje a través del estudio experimental y la observación de situaciones en las que se ponga de manifiesto esta relación interdisciplinar; la aplicación de herramientas tecnológicas en la indagación y la experimentación; y el empleo de herramientas matemáticas y el razonamiento lógico en la resolución de problemas propios de la química. Esta base de carácter interdisciplinar y holístico que es inherente a la química proporciona al alumnado que la estudia unos cimientos adecuados para que puedan continuar estudios en diferentes ramas de conocimiento, y a través de diferentes itinerarios formativos, lo que contribuye de forma eficiente a la formación de personas competentes.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM4, CPSAA3.2 y CC4.


## Criterios de evaluación
Comparación proyecto Decreto Madrid frente a Real Decreto:   
En 1.1 se elimina "y el desarrollo sostenible respetuoso con el medioambiente"  

Sin cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 

### Competencia específica 1.  
1.1. Reconocer la importancia de la química y sus conexiones con otras áreas en el desarrollo de la sociedad, el progreso de la ciencia, la tecnología *y* la economía, identificando los avances en el campo de la química que han sido fundamentales en estos aspectos.  
1.2. Describir los principales procesos químicos que suceden en el entorno y las propiedades de los sistemas materiales a partir de los conocimientos, destrezas y actitudes propios de las distintas ramas de la química.  
1.3. Reconocer la naturaleza experimental e interdisciplinar de la química y su influencia en la investigación científica y en los ámbitos económico y laboral actuales, considerando los hechos empíricos y sus aplicaciones en otros campos del conocimiento y la actividad humana.  

### Competencia específica 2.  
2.1. Relacionar los principios de la química con los principales problemas de la actualidad asociados al desarrollo de la ciencia y la tecnología, analizando cómo se comunican a través de los medios de comunicación o son observados en la experiencia cotidiana.  
2.2. Reconocer y comunicar que las bases de la química constituyen un cuerpo de conocimiento imprescindible en un marco contextual de estudio y discusión de cuestiones significativas en los ámbitos social, económico, político y ético identificando la presencia e influencia de estas bases en dichos ámbitos.  
2.3. Aplicar de manera informada, coherente y razonada los modelos y leyes de la química, explicando y prediciendo las consecuencias de experimentos, fenómenos naturales, procesos industriales y descubrimientos científicos.  

### Competencia específica 3.  
3.1. Utilizar correctamente las normas de nomenclatura de la IUPAC como base de un lenguaje
universal para la química que permita una comunicación efectiva en toda la comunidad científica, aplicando dichas normas al reconocimiento y escritura de fórmulas y nombres de diferentes especies químicas.  
3.2. Emplear con rigor herramientas matemáticas para apoyar el desarrollo del pensamiento científico que se alcanza con el estudio de la química, aplicando estas herramientas en la resolución de problemas usando ecuaciones, unidades, operaciones, etc.  
3.3. Practicar y hacer respetar las normas de seguridad relacionadas con la manipulación de sustancias químicas en el laboratorio y en otros entornos, así como los procedimientos para la correcta gestión y eliminación de los residuos, utilizando correctamente los códigos de comunicación característicos de la química.  

### Competencia específica 4.
4.1. Analizar la composición química de los sistemas materiales que se encuentran en el entorno más próximo, en el medio natural y en el entorno industrial y tecnológico, demostrando que sus propiedades, aplicaciones y beneficios están basados en los principios de la química.  
4.2. Argumentar de manera informada, aplicando las teorías y leyes de la química, que los efectos negativos de determinadas sustancias en el ambiente y en la salud se deben al mal uso que se hace de esos productos o negligencia, y no a la ciencia química en sí.  
4.3. Explicar, empleando los conocimientos científicos adecuados, cuáles son los beneficios de los numerosos productos de la tecnología química y cómo su empleo y aplicación han contribuido al progreso de la sociedad.  

### Competencia específica 5.  
5.1. Reconocer la importante contribución en la química del trabajo entre especialistas de diferentes disciplinas científicas poniendo de relieve las conexiones entre las leyes y teorías propias de cada una de ellas.  
5.2. Reconocer la aportación de la química al desarrollo del pensamiento científico y a la autonomía de pensamiento crítico a través de la puesta en práctica de las metodologías de trabajo propias de las disciplinas científicas.  
5.3. Resolver problemas relacionados con la química y estudiar situaciones relacionadas con esta ciencia, reconociendo la importancia de la contribución particular de cada miembro del equipo y la diversidad de pensamiento y consolidando habilidades sociales positivas en el seno de equipos de trabajo.  
5.4. Representar y visualizar de forma eficiente los conceptos de química que presenten mayores dificultades utilizando herramientas digitales y recursos variados, incluyendo experiencias de laboratorio real y virtual.  

### Competencia específica 6.  
6.1. Explicar y razonar los conceptos fundamentales que se encuentran en la base de la química aplicando los conceptos, leyes y teorías de otras disciplinas científicas (especialmente de la física) a través de la experimentación y la indagación.  
6.2. Deducir las ideas fundamentales de otras disciplinas científicas (por ejemplo, la biología o la tecnología) por medio de la relación entre sus contenidos básicos y las leyes y teorías que son propias de la química.  
6.3. Solucionar problemas y cuestiones que son característicos de la química utilizando las herramientas provistas por las matemáticas y la tecnología, reconociendo así la relación entre los fenómenos experimentales y naturales y los conceptos propios de esta disciplina.  

## Contenidos
Comparación proyecto Decreto Madrid frente a Real Decreto:   
En Madrid no se llaman saberes básicos  
Se añaden contenidos en rojo, aquí se marcan en negrita  

En A.1 se elimina "Interpretación de los espectros de emisión y absorción de los elementos. Relación con la estructura electrónica del átomo." y se añade "El espectro de emisión del hidrógeno."  
En A.3 cambia "tendencias" por "propiedades"  
En B.2 se reordenan párrafos  
En B.5 texto inicial pasa de "redox" a "de reducción y oxidación (redox)"  
Se añade C.1 completo "1. **Formulación y nomenclatura de compuestos orgánicos.**, **Nombrar y formular hidrocarburos alifáticos y aromáticos, derivados halogenados, alcoholes, éteres, aldehídos, cetonas, ácidos, ésteres, amidas y aminas.**"  
En C.2 se añade literalmente "isomería geométrica" que es algo obsoleto y desaconsejado por IUPAC [geometric isomerism](https://goldbook.iupac.org/terms/view/G02620)  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
En C se cambia "Formulación y nomenclatura" por "nomenclatura" e "isomería geométrica" por "isomería cis-trans" (lo puse en alegaciones)  


A. Enlace químico y estructura de la materia.  
1. Espectros atómicos.  
- **Radiación electromagnética.** Los espectros atómicos como responsables de la necesidad de la revisión del modelo atómico. Relevancia de este fenómeno en el contexto del desarrollo histórico del modelo atómico.  
• **El espectro de emisión del hidrógeno.**  
2. Principios cuánticos de la estructura atómica.  
- **Teoría cuántica de Planck.** Relación entre el fenómeno de los espectros atómicos y la cuantización de la energía.  
- Del modelo de Bohr a los modelos mecano-cuánticos: necesidad de una estructura electrónica en diferentes niveles.  
• **Modelo atómico de Bohr. Postulados. Energía de las órbitas del átomo de hidrógeno.**  
• **Interpretación de los espectros de emisión y absorción de los elementos. Relación con la estructura electrónica del átomo.**  
• **Aciertos y limitaciones del modelo atómico de Bohr.**  
- Principio de incertidumbre de Heisenberg y doble naturaleza onda-corpúsculo del electrón. **Modelo mecano-cuántico del átomo.** Naturaleza probabilística del concepto de orbital.  
- Números cuánticos. Estructura electrónica del átomo. Principio de exclusión de Pauli. **Principio de máxima multiplicidad de Hund. Principio de Aufbau, Building-up o Construcción Progresiva.** Utilización del diagrama de Moeller para escribir la configuración electrónica de los elementos químicos.  
3. Tabla periódica y propiedades de los átomos.  
- Naturaleza experimental del origen de la tabla periódica en cuanto al agrupamiento de los elementos según sus propiedades. La teoría atómica actual y su relación con las leyes experimentales observadas.  
- Posición de un elemento en la tabla periódica a partir de su configuración electrónica.  
- **Propiedades** periódicas: **radio atómico, radio iónico, energía de ionización, afinidad electrónica, electronegatividad**. Aplicación a la predicción de los valores de las propiedades de los elementos de la tabla a partir de su posición en la misma.  
4. Enlace químico y fuerzas intermoleculares.  
- **Enlace químico**. Tipos de enlace a partir de las características de los elementos individuales que lo forman. Energía implicada en la formación de moléculas, de cristales y de estructuras macroscópicas.  
- **Enlace covalente.** Modelos de Lewis, **teoría de repulsión de pares electrónicos de la capa de valencia** (RPECV) y **teoría de enlace de valencia:** hibridación de orbitales. Configuración geométrica de compuestos moleculares. **Polaridad del enlace y de la molécula. Propiedades de las sustancias químicas con enlace covalente y** características de los sólidos **covalentes y moleculares.**  
- **Enlace iónico.** Energía intercambiada en la formación de cristales iónicos. Ciclo de Born-Haber. **Propiedades de las sustancias químicas con enlace iónico.**  
- **Enlace metálico.** Modelos de la nube electrónica y la teoría de bandas para explicar las propiedades características de los cristales metálicos.  
- Fuerzas intermoleculares a partir de las características del enlace químico y la geometría de las moléculas: **enlaces de hidrógeno, fuerzas de dispersión y fuerzas entre dipolos permanentes**. Propiedades macroscópicas de elementos y compuestos moleculares.   

B. Reacciones químicas.  
1. Termodinámica química.  
- Primer principio de la termodinámica: intercambios de energía entre sistemas a través del calor y del trabajo.  
- Ecuaciones termoquímicas. Concepto de entalpía de reacción. Procesos endotérmicos y exotérmicos.  
- Balance energético entre productos y reactivos mediante la ley de Hess, a través de la entalpía de formación estándar o de las energías de enlace, para obtener la entalpía de una reacción.  
- Segundo principio de la termodinámica. La entropía como magnitud que afecta a la espontaneidad e irreversibilidad de los procesos químicos.  
- Cálculo de la energía de Gibbs de las reacciones químicas y espontaneidad de las mismas en función de la temperatura del sistema.  
2. Cinética química.  
- Conceptos de velocidad de reacción. Ley diferencial de la velocidad de una reacción química y los órdenes de reacción a partir de datos experimentales de velocidad de reacción.  
- Teoría de las colisiones como modelo a escala microscópica de las reacciones químicas. **Teoría del estado de transición.** Energía de activación.  
- Influencia de las condiciones de reacción sobre la velocidad de la misma. **Ecuación de Arrhenius.**  
• **Utilización de catalizadores en procesos industriales.**  
3. Equilibrio químico.  
- **Reversibilidad de las reacciones químicas.** El equilibrio químico como proceso dinámico: ecuaciones de velocidad y aspectos termodinámicos. Expresión de la constante de equilibrio mediante la ley de acción de masas.  
- La constante de equilibrio de reacciones en las que los reactivos se encuentren en diferente estado físico. Relación entre Kc y Kp.  
- **Solubilidad.** Producto de solubilidad en equilibrios heterogéneos.  
- Principio de Le Châtelier y el cociente de reacción. Evolución de sistemas en equilibrio a partir de la variación de las condiciones de concentración, presión o temperatura del sistema.  
• **Importancia del equilibrio químico en la industria y en situaciones de la vida cotidiana.**  
4. Reacciones ácido-base.  
- Naturaleza ácida o básica de una sustancia a partir de las teorías de Arrhenius y de Brønsted y Lowry.  
• **Electrolitos.**  
- **Equilibrio de ionización del agua.** Ácidos y bases fuertes y débiles. Grado de disociación en disolución acuosa.  
- pH de disoluciones ácidas y básicas. Expresión de las constantes Ka y Kb.  
- Concepto de pares ácido y base conjugados. Carácter ácido o básico de disoluciones en las que se produce la hidrólisis de una sal.  
- **Disoluciones reguladoras del pH. Concepto y aplicaciones en la vida cotidiana.**  
- Reacciones entre ácidos y bases. Concepto de neutralización. Volumetrías ácido-base.  
- Ácidos y bases relevantes a nivel industrial y de consumo, con especial incidencia en el proceso de la conservación del medioambiente.  
5. Reacciones **de reducción y oxidación** (redox).  
- Estado de oxidación. Especies que se reducen u oxidan en una reacción a partir de la variación de su número de oxidación.  
• **Par redox. Oxidantes y reductores.**  
- Método del ion-electrón para ajustar ecuaciones químicas de oxidación-reducción. Cálculos estequiométricos y volumetrías redox.  
- **Electrodos.** Potencial estándar de un par redox. Espontaneidad de procesos químicos y electroquímicos que impliquen a dos pares redox. **Pilas galvánicas y celdas electroquímicas.**  
• **Electrólisis de sales fundidas y en disolución acuosa.**  
- Leyes de Faraday: cantidad de carga eléctrica y las cantidades de sustancia en un proceso electroquímico. Cálculos estequiométricos en cubas electrolíticas. **Aplicaciones de la electrólisis.**  
- Reacciones de oxidación y reducción en la fabricación y funcionamiento de baterías eléctricas, celdas electrolíticas y pilas de combustible, así como en la prevención de la corrosión de metales.  

C. Química orgánica.  
1. **Nomenclatura de compuestos orgánicos.**  
- **Nombrar y formular hidrocarburos alifáticos y aromáticos, derivados halogenados, alcoholes, éteres, aldehídos, cetonas, ácidos, ésteres, amidas y aminas.**  
2. Isomería. **Isomería de posición, cadena y función. Isomería cis-trans. Representación de moléculas orgánicas.**  
- Fórmulas moleculares y desarrolladas de compuestos orgánicos. Diferentes tipos de isomería estructural.  
- Modelos moleculares o técnicas de representación 3D de moléculas. Isómeros espaciales de un compuesto y sus propiedades.  
3. Reactividad orgánica.  
- Principales propiedades químicas de las distintas funciones orgánicas. Comportamiento en disolución o en reacciones químicas.  
- Principales tipos de reacciones orgánicas: **sustitución, adición, eliminación, condensación y redox**. Productos de la reacción entre compuestos orgánicos y las correspondientes ecuaciones químicas.  
4. Polímeros.  
- Proceso de formación de los polímeros a partir de sus correspondientes monómeros. Estructura y propiedades.  
- Clasificación de los polímeros según su naturaleza, estructura y composición. Aplicaciones, propiedades y riesgos medioambientales asociados.  

