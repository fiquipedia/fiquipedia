
# Recursos PAU Genéricos

Muchos de ellos no se limitan solo a física y química.  
Como la PAU es a nivel autonómico, hay recursos separados por comunidad (la UNED a esos efectos se puede considerar "otra comunidad"), aunque me centro en la de Madrid.  
Intento poner los enlaces oficiales de universidades y los que no son oficiales.  
En 2017 PAU pasa a ser " [evaluación final bachillerato](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-bachillerato) " según LOMCE, nombre es PAU, EBAU, EvAU, EFB, ... según comunidad, pero se sigue centralizando información aquí como PAU  
En la página no pongo más que materias asociadas a Física y Química, con algo de matemáticas: se pueden ver más recursos en ciertos sitios, por ejemplo recopilados aquí  
[twitter CIAandPatri/status/1035502202809450496](https://twitter.com/CIAandPatri/status/1035502202809450496)  
No es por amargaros los últimos días de vacaciones, pero voy a hacer un hilo de Webs, apps y canales de Youtube útiles para Bachillerato y la EVAU ...

En la migración de 2021 veo que hay enlaces desactualizados, algunos los pongo dentro de las tablas de EvAU por comunidades asociados a cada bandera, pendiente revisar y unificar.

##  Concepto de "modelo" de examen
En algunos de los enlaces de recursos PAU hay modelos de exámenes o exámenes.  
El concepto de "modelo" varía según quien lo utilice, no significa siempre lo mismo (se intenta explicar en página física y química sobre diferencias por comunidades, pendiente de unificar), por ejemplo:  
- En Madrid modelo es "borrador preliminar para hacer una idea de cómo es": el modelo de 2015 se publica en septiembre de 2014. Aparte de modelos hay exámenes. Se supone que elaboran varios exámenes, y eligen en septiembre "al azar" cual es el examen modelo, lo publican antes. En Madrid el modelo se queda en modelo; se conoce a priori una variante que llaman modelo.  
- En Andalucía modelo es "cada una de las 6 variantes que se preparan": un modelo se usa en junio, otro en septiembre... Todo son modelos, y se trata de saber qué modelo fue qué examen. Se supone que entre los 6 modelos se elige "al azar" cual es el examen de junio, cual es el examen de septiembre ... En Andalucía cada modelo es un examen, no se conocen a priori qué modelo será cada examen ni los modelos en sí.  
- En Selectividad UNED 2015 parece que funciona igual que Andalucía; hay varios modelos y luego escogen uno de los modelos.

##  Concepto de "coincidentes"
Me lo preguntan varias veces al verlo en nombres de ejercicios, porque ni profesores ni alumnos lo conocen  
Está asociado a que algunas personas por combinaciones materias no pueden hacer el examen con los demás, y hacen un examen distinto otro día, que se llama de incidencias/coincidencias  
No siempre hay examen de una materia de coincidentes si no hay coincidencia para nadie en esa convocatoria, y son difíciles de conseguir porque los hace poca gente

##  Uso de calculadora en PAU
Ver en  [recursos uso calculadora](/home/recursos/recursos-uso-calculadora) 

##  De varias las comunidades autónomas
Para Madrid y otras comunidades se recomienda mirar las páginas de  [PAU Física](/home/pruebas/pruebasaccesouniversidad/paufisica)  y  [PAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) , donde se citan enlaces a páginas con exámenes y soluciones que a veces incluyen otras materias adicionales.  

[http://www.selectividad.profesores.net/](http://www.selectividad.profesores.net/)  
Todas las comunidades. (En octubre 2013 da error en el último paso en el que se accede a los exámenes resultado de la búsqueda)  

[granada-clases-matematicas.blogspot.com.es 2011 Examenes Corregidos Resueltos de Matematicas, Fisica y Quimica: Selectividad de Andalucia. Todos los años](http://granada-clases-matematicas.blogspot.com.es/2011/09/matematicas-fisica-quimica-selectividad.html)  
Varios: Andalucía, Madrid, Zaragoza... matemáticas, física, química, tecnología industrial,...  

[http://www.cienciatotal.es/ciencias/spip.php?article135](http://www.cienciatotal.es/ciencias/spip.php?article135)  

Rincón Didáctico Física y Química. Consejería de Educación y Cultura. Gobierno de Extremadura.  
[rincones.educarex.es rincón didáctico FyQ selectividad](https://fisicayquimica.educarex.es/es/2-bachillerato/selectividad)  

Colección de problemas resueltos extraídos de problemas de selectividad.  
[http://www.selectividad.tv/](http://www.selectividad.tv/)  
Licenciamiento no detallado. Un autor / responsable por materia.  
*"Colección de cerca de 1000 problemas y ejercicios resueltos de 10 asignaturas: Matemáticas, Física, Biología, Química, Dibujo Técnico, Historia, Lengua y Literatura, Filosofía, Historia del Arte e Inglés. Todos estos problemas y ejercicios han sido extraídos de las Pruebas de acceso a la Universidad ("Selectividad") realizadas en España durante estos últimos años y como tales reflejan adecuadamente el nivel educativo esperado de los estudiantes preuniversitarios.*  
*Está colección de problemas resueltos tiene como principales objetivos:*  
*Ayudar a todos los estudiantes a preparar sus exámenes en general y el tan temido examen de Selectividad en particular.*  
*Servir de referencia y recurso educativo a los profesores de Bachillerato.*  
*Ofrecer una herramienta gratuita de apoyo al estudio accesible en cualquier momento y desde cualquier lugar.*  

[http://www.selectividadonline.com/](http://www.selectividadonline.com/)  
Copyright © 2009 - 2010 HIGHTICH GROUP ALIVE.  
Apuntes (cc-by), exámenes de varias comunidades y años.  

[http://graviton.blogspot.com.es/p/examenes-selectividad.html](http://graviton.blogspot.com.es/p/examenes-selectividad.html)  
Recopilación enlaces. José Ramón Ramos, cc-by-nc-sa  

[http://www.examenes-selectividad.com/](http://www.examenes-selectividad.com/)  
cc-by-nc-nd  

[http://fisquim.torrealmirante.net/selectividad.html](http://fisquim.torrealmirante.net/selectividad.html)  

[http://www.luis-vives.es/index.php/otros/modelos-examen](http://www.luis-vives.es/index.php/otros/modelos-examen)  

[http://www.examenesdepau.com/](http://www.examenesdepau.com/) 

##  UNED
Está asociado a "estudios extranjeros"; los que se presentan desde sistemas educativos de otros países  
[uned.es Acceso a la universidad](http://portal.uned.es/portal/page?_pageid=93,27072135&_dad=portal&_schema=PORTAL)  

Selectividad UNED - Alumnos con bachillerato extranjero [http://www.luis-vives.es/index.php/otros/modelos-examen](http://www.luis-vives.es/index.php/otros/modelos-examen) 

[Descàrrega exàmens anys anteriors. EXÀMENS ACCÉS, GRAUS, MASTERS I CUID - unedcervera.com](https://www.unedcervera.com/cacervera/examens.php)  
Remite a [Acceso al Depósito de Exámenes - calatayud.und.es](http://www.calatayud.uned.es/Examenes/examenes_step_0.asp)  
> Conforme al acuerdo del Consejo de Gobierno de la UNED de 22 de diciembre de 2010, únicamente se permite acceso al Depósito de Exámenes a usuarios del Campus UNED.  
Conforme al acuerdo del Consejo de Gobierno de la UNED de 30 de junio de 2015, se advierte que los equipos docentes no quedan vinculados a los contenidos y respuestas de los exámenes de convocatorias anteriores.  

[UNDassis](https://unedasiss.uned.es/)  
> UNEDasiss evalúa los expedientes académicos de los estudiantes internacionales y les realiza las pruebas necesarias para acceder a estudios de grado en las universidades españolas.  

[UNEDassis exámenes Pruebas de competencias específicas (PCE) ](https://unedasiss.uned.es/examenes)  
[UNEDassis Oferta de asignaturas de Pruebas de Competencias Específicas (PCE)](https://unedasiss.uned.es/oferta_asignaturas)  



##  Comunidad de Madrid


###  Universidades Madrid  
Universidad Complutense
[ucm.es Evaluación para el Acceso a la Universidad (EvAU). ](https://www.ucm.es/evau-)  

Universidad Carlos III
[uc3m.es EvAU](https://www.uc3m.es/pruebasacceso/inicio)  
[uc3m.es Modelos de exámenes](https://www.uc3m.es/pruebasacceso/modelos-examenes)

Universidad Autónoma de Madrid
[uam.es Acceso a estudios universitarios de grado](https://www.uam.es/uam/estudios/acceso-estudios-grado)  
 
Universidad Rey Juan Carlos  
[urjc.es admisión a grado](http://www.urjc.es/estudiar-en-la-urjc/admision/273-grado-admision#informaci%C3%B3n-para-estudiantes)  
[urjc.es admisión y matrícula, estudiantes de bachillerato y formación profesional (LOMCE)](https://www.urjc.es/estudiar-en-la-urjc/admision/407-estudiantes-de-bachillerato-y-formacion-profesional-lomce)  

Universidad de Alcalá de Henares
[uah.es Pruebas de acceso Evaluación para el Acceso a la Universidad Exámenes, soluciones y criterios de corrección](http://www.uah.es/acceso_informacion_academica/primero_segundo_ciclo/acceso/selectividad/examenes_selectividad.asp)  
Formulario donde se pueden consultar exámenes de cualquier asignatura, solamente a partir de 2010  

Universidad Politécnica
[upm.es Evaluación para el acceso a la Universidad (EvAU)](http://www.upm.es/FuturosEstudiantes/Ingresar/Acceso/EvAU)  (enlace validado 2018)

###  Otros 
En general me centro en PAU asociada a acceso desde Bachillerato, aunque también hay otros accesos. En emes.es > acceso universidad se mencionan:  
[Mayores de 25](/home/recursos/recursospau/recursos-pau-mayores-25-anos)  
FP-Modulos III  
Titulados Universitarios  
Estudios Extranjeros  
Mayores 40 y 45 Universidad para Mayores  

Legislación / normativa estatal sobre el acceso a la universidad:  
[todofp.es Acceso a Enseñanzas Universitarias desde FP](https://www.todofp.es/profesores/normativa/legislacion/normativa-estatal/acceso-ensenanzas-universitarias.html)  

[educacionyfp.gob.es opciones al terminar bachillerato](https://www.educacionyfp.gob.es/va/contenidos/estudiantes/bachillerato/informacion-general/opciones-al-terminar.html)  

[comunidad.madrid Acceso a la universidad desde el Bachillerato](https://www.comunidad.madrid/servicios/educacion/acceso-universidad-bachillerato)  
  
[Enlaces asociados a PAU / Selectividad del IES Carlos III](http://www.educa.madrid.org/web/ies.carlostercero.madrid/selectividad.htm) 

##  Andalucía
 [EXÁMENES Y ORIENTACIONES SOBRE LA PRUEBA DE ACCESO Y/O ADMISIÓN A LA UNIVERSIDAD](https://www.juntadeandalucia.es/economiaconocimientoempresasyuniversidad/sguit/?q=grados&d=g_b_examenes_anteriores.php)  
 Exámenes distrito único andaluz desde 2008  
  
 [fiquimakcc.es Selectividad física 2006, 2007, 2008, 2009, 2010, 2011 y 2012 Andalucía resueltos](http://www.fiquimakcc.es/index.php?option=com_content&view=article&id=91:fissel2008&catid=88&Itemid=481)  
 Kico Calvillo, Licenciado en Física  
 
 Para Ceuta y Melilla parece que la PAU se hace en Andalucía: en junio 2014 se indica en  
 [ugr.es Inscripción Pruebas de Acceso a la Universidad](http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion)  
 donde se cita " Aquí puedes encontrar una presentación que te ayudará a realizar el registro: Alumnos que este curso aprobarán el 2º de Bachillerato o el 2º curso del CFGS en un centro de Granada, Ceuta, Melilla o centros adscritos a la Universidad de Granada en Marruecos..."

##  Aragón
 [unizar.es Evaluación para el Acceso a la Universidad [EvAU]](https://academico.unizar.es/acceso-admision-grado/evau/evau)  
 [unizar.es Exámenes de años anteriores](https://academico.unizar.es/acceso-admision-grado/evau/exame)

##  Asturias
 [uniovi.es Prueba de acceso a la Universidad (EBAU)Exámenes PAU / EBAU](https://www.uniovi.es/accesoyayudas/estudios/ebau/examenes)  
 Exámenes desde 1998

##  Baleares
 [uib.es Models d'exàmens PBAU](https://estudis.uib.cat/estudis-de-grau/Com-hi-pots-accedir/acces/batxiller/ModelsExamenPBAU/)  
Exámenes (a través de un formulario) Modelos solo disponibles en la versión en catalán.  

##  Canarias
 [gobiernodecanarias.org Prueba de Acceso a la Universidad (PAU) >Exámenes, recursos y coordinación >Materias PAU](http://www.gobiernodecanarias.org/educacion/web/bachillerato/pau/examenes-recursos-coordin-materias/materias-pau/)  
 Exámenes desde 1996

##  Cantabria
 [unican.es Evaluación de Bachillerato para el Acceso a la Universidad (EBAU)](https://web.unican.es/admision/acceso-a-estudios-de-grado/evaluacion-de-bachillerato-para-el-acceso-a-la-universidad)  
 
 En esta página podrás disponer de las PAU (Pruebas de Acceso a la Universidad) de la Universidad de Cantabria desde el año 1995 hasta 2013 de las siguientes materias: QUÍMICA, FÍSICA, MATEMÁTICAS II  
 [elfisicoloco.blogspot.com.es exámenes PAU](http://elfisicoloco.blogspot.com/p/pau-cantabria-new.html)  
 Javier Sánchez

##  Castilla La Mancha
 [http://www.uclm.es/Preuniversitario/paeg/](http://www.uclm.es/Preuniversitario/paeg/) Exámenes desde 1999 

##  Castilla y León
 [uva.es EBAU](https://www.uva.es/export/sites/uva/2.docencia/2.01.grados/2.01.01.pruebasdeacceso/2.01.02.01.ebau/index.html)  
 Universidad de Salamanca. Exámenes y criterios de corrección desde 2010

##  Cataluña
 [http://universitats.gencat.cat Modelos de exámenes y criterios](http://universitats.gencat.cat/es/pau/model_examens/) Modelos de examen desde 2011  
 
 [Selecat PAU (Proves d'accés a la universitat): Exàmens i informació; de les materies dels anys 2000-2021](http://www.selecat.cat/)  

##  Extremadura
 [Coordinación EBAU (Prueba de Evaluación de Bachillerato para el Acceso a la Universidad) ](http://www.unex.es/organizacion/organos-unipersonales/vicerrectorados/vicealumn/funciones/car_20050411_001)  
 Exámenes desde 2006

##  Galicia
 [ABAU Proba de avaliación de bacharelato para o acceso á universidade.(https://ciug.gal/gal/abau) 

##  Navarra
 [unavarra.es Evaluación de Bachillerato para el Acceso a la Universidad](https://www.unavarra.es/sites/estudios/acceso-y-admision/evau-para-estudiantes.html)  
 Exámenes desde 2006

##  Murcia
 [um.es FÍSICA - Pruebas de Acceso a la Universidad, Murcia (2003-2019)](http://www.um.es/phi/aguirao/Selectividad.html)  
 Exámenes desde 2000, con soluciones desde 2003  
 
 [um.es PAU Descarga de exámenes](https://examenesacceso.um.es/examenesacceso/indexacceso.seam?umu=ms) Selectividad  
 [um.es PAU Descarga de exámenes mayores 25 y 45 años](https://examenesacceso.um.es/examenesacceso/indexacceso.seam?umu=mo) 
 
 [https://ebaumurcia.es/fisica/](https://ebaumurcia.es/fisica/)  
 [https://ebaumurcia.es/quimica/](https://ebaumurcia.es/quimica/) 

##  País vasco
 [ehu.eus Ejercicios de la prueba de acceso a la Universidad](https://www.ehu.eus/es/web/sarrera-acceso/prueba-acceso-universidad/ejercicios)  
 En 2014 solamente exámenes desde 2010, se indica que han eliminado los anteriores  
 [UPV/EHU Acceso a la Universidad](https://www.ehu.eus/es/web/sarrera-acceso) 

##  Rioja
 [unirioja.es MODELOS DE EXÁMENES, Evaluación de Bachillerato para Acceso a la Universidad (EBAU)](https://www.unirioja.es/estudiantes/acceso_admision/EBAU/EBAU_exa.shtml)  
 Exámenes PAU desde 2002

##  Valencia
 [Proves d'accés Prova d'accés a la universitat (PAU) Informació PAU](https://innova.gva.es/va/web/universidad/informacion-pau)  

