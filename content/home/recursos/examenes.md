# Exámenes

En general la evaluación se hace muchas veces con pruebas escritas, exámenes.   
No los veo más que un tipo concreto de ejercicios, aunque hay cursos como 2º Bachillerato donde enlazan con selectividad.  
Un examen, bien diseñado y con tiempo suficiente, es una prueba objetiva donde una persona puede demostrar de manera práctica su conocimiento.  
Me gustaría tener una colección de ejercicios con licenciamiento creative-commons suficientemente amplia para las clases y para exámenes, que fuesen indistinguibles: al menos en los exámenes que planteo intento que sean intercambiables, de hecho a veces lo hago en dos sobres cerrados y qué es examen y qué son ejercicios lo decide el azar.  

**Puede haber información relacionada y de utilidad en apartados dentro de [Pruebas](/home/pruebas)**  

En  [2º Bachillerato Física he realizado algunos ejercicios de elaboración propia](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato) , y bastantes de ellos han salido en exámenes.  

En esta página en borrador pongo algunos enlaces a exámenes frikis o peculiares, aunque puede haber más en otras páginasAlgunos pueden ser nivel ESO y Bachillerato y otros nivel universitario.

[Exámenes: ¿Problemas tipo o problemas de idea feliz? - lacienciaparatodos](https://lacienciaparatodos.wordpress.com/2019/06/10/examenes-problemas-tipo-o-problemas-de-idea-feliz/)  

[¿Cuánto restan los errores en exámenes tipo test?](https://www.ciencia-explicada.com/2013/06/cuanto-restan-los-errores-en-examenes.html)
> Nuestro objetivo es diseñar una penalización por error tal que si se responde por puro azar, se saque un cero.

##  [FriQuiExamenes](/home/recursos/examenes/friquiexamenes) 

##  Comparativa histórica de exámenes
Artículo en prensa mayo 2016 donde se citan algunos exámenes antiguos de varias materias, incluyendo Física  
EDUCACIÓN Comparativa de pruebas de acceso a la universidad  
[¿Son los exámenes de ahora más fáciles que los de antes?](http://www.elmundo.es/sociedad/2016/05/31/574d70bcca474170388b4578.html) 

##  Examen de cuaderno
[Examen de cuaderno - tierradenumeros](https://tierradenumeros.com/post/examen-de-cuaderno/)  

[twitter rosa_linaresr/status/1481631844378554372](https://twitter.com/rosa_linaresr/status/1481631844378554372)  
Comparto «Kit básico para evaluar y calificar», guía muy útil que responde a las dudas más habituales sobre estos asuntos. Preguntas del tipo "¿se puede atribuir un porcentaje de calificación al cuaderno?" La respuesta sencilla: No.  
[Kit básico para evaluar y calificar - gobiernodecanarias.org](https://www.gobiernodecanarias.org/cmsweb/export/sites/educacion/web/_galerias/descargas/otros/a_web_kit_basico_para_evaluar_y_calificar.pdf)  
¿Por qué?   
Suelo poner entre 5/10%.   
Excepto si no lo entregan, y lo aviso con muuuuchooo tiempo, nunca les baja la nota.  

Te resumo lo que dice: la calificación solo puede tener como referentes los criterios de evaluación. El cuaderno no es ni siquiera un instrumento de evaluación (no digamos ya un criterio), es tan solo un soporte.  

Igual que el folio del examen, o una cartulina de un mural, también son soportes. No puedes evaluar el soporte, pero puedes evaluar algo que haya en ese soporte, si figura en los criterios de evaluación.  



Se pueden citar ideas sobre evaluar cuaderno  

[twitter LaCrono__/status/1457116386078117891](https://twitter.com/LaCrono__/status/1457116386078117891)  
Si algo saqué en claro es que la niña entendía que trabajar significaba estar un rato delante de la hoja en blanco esperando a que alguien resolviera los ejercicios en la pizarra para poder copiarlos.  
[twitter laguiri/status/1457282101406019585](https://twitter.com/laguiri/status/1457282101406019585)  
Crono habla de una niña pequeña, pero esto pasa muchísimo en la ESO porque hay profesores que valoran una barbaridad El Cuaderno De Clase.  
Hablemos del Cuaderno De Clase y del Trabajo Diario  
...
Insisto una vez más en que la única causa para tener negativos es NO PREGUNTAR LAS DUDAS.   
Corregimos el ejercicio. esperaersperaesperaespera. Todos, incluso los que no se han enterado de nada, copian frenéticamente las soluciones.   
¿Qué estáis copiando y por qué?  
No tengo ni idea de quién ha decidido que el alumnado de la ESO debe ser evaluado por su capacidad de tener los ejercicios de clase, no los deberes,   
ordenados  
completos  
corregidos  
sin tachones.   
Con una nota numérica que hace media con los exámenes.   
Yo no lo hago. Me niego.  

## Exámenes y didáctica 

Se puede comentar mucho sobre tipo de exámenes, teach to the test, ...

[twitter emollick/status/1450485990632697857](https://twitter.com/emollick/status/1450485990632697857)  
Three VERY effective things that teachers do (that annoy students):  
Giving lots of small tests, which is one of the most effective ways to boost memory & learning  
Randomized seating almost eliminates cheating  
Emphasizing attendance, the best predictor of exam scores (r=.44)  

[La cultura del examen, Pablo Del Pozo Toscano - eldiariodelaeducacion.com](https://eldiariodelaeducacion.com/2022/03/22/la-cultura-del-examen/)  

## Generadores de exámenes

[Programa generador de exámenes en papel y en Moodle - wordpress](https://lacienciaparatodos.wordpress.com/2022/07/02/programa-generador-de-examenes-en-papel-y-en-moodle/) Javier Fernández Panadero  

[📝 Generador de Exámenes - github](https://javierfpanadero.github.io/generador-examenes/)  

##  Exámenes online y COVID19
Es un tema que surge con la suspensión de las clases presenciales.  
En abril 2020 elaboro un documento con normas para Bachillerato que adjunto en esta página  
[NormasExamenesOnlineBachillerato - fiquipedia](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/examenes/NormasExamenesOnlineBachillerato-fiquipedia.pdf)

Más tarde descubro una propuesta alternativa de marzo 2020 de Enrique García de Bustos Sierra, que es "Vídeo de veracidad"; se trata de que el alumno con time lapse se grabe durante el examen y luego envíe junto al examen el vídeo grabado.  
[twitter enriquegbs/status/1240721592314888192](https://twitter.com/enriquegbs/status/1240721592314888192) 
 Os comparto un [tutorial para alumnos con una idea para hacer exámenes a distancia con G-Suite.](https://docs.google.com/presentation/d/14ukGO-f2xc6GmjyeImk3Ko_uxs0jb-T_E-qVIMx6EVE/edit#slide=id.p) 
"El Vídeo de Veracidad (VV) es un vídeo construido a partir de muchas imágenes tomadas durante el tiempo que haces el examen.  
Es la forma en la que se comprobará que has realizado el examen en condiciones adecuadas y que puede formar parte de tu calificación."  

También se pueden comentar cosas sobre configuración y problemas  
[https://community.jitsi.org/t/problemas-con-camara-y-microfono/28617/14](https://community.jitsi.org/t/problemas-con-camara-y-microfono/28617/14)  
Incorporo información también en un post del blog sobre protección de datos  
  
[Normativa webs, aplicaciones y protección de datos en educación](https://algoquedaquedecir.blogspot.com/2019/06/normativa-webs-aplicaciones-y-proteccion-datos.html)  

Surgen comentarios en redes sobre herramientas de monitorización  

En 2021 creo post en el blog sobre [Exámenes online](https://algoquedaquedecir.blogspot.com/2021/02/examenes-online.html)

- Informe de iniciativas y herramientas de evaluación online universitaria en el contexto del Covid-19  
Elaborado por: Manuel González, Ernest Marcoy Toret Medina  
Ministerio de Universidades  
[Informe de iniciativas y herramientas de evaluación online universitaria en el contexto del Covid-19 - madrimasd.org](https://www.madrimasd.org/uploads/Informe%20de%20iniciativas%20y%20herramientas%20de%20evaluaci%C3%B3n%20online%20universitaria%20en%20el%20contexto%20del%20Covid-19.pdf)
- Artículo sobre universidades de Argentina  
[Exámenes cronometrados y trabajos prácticos: las universidades definen cómo tomarán parciales y finales](https://www.infobae.com/educacion/2020/04/28/examenes-cronometrados-y-trabajos-practicos-las-universidades-definen-como-tomaran-parciales-y-finales/)  
“Funciona con un temporizador, que pone un plazo máximo para la resolución del examen. **Para evitar copias, no se entrega el examen completo, sino que se van arrojando consignas de a una y en orden aleatorio.** Es decir, el orden de los cuestionarios es distinto en cada alumno. Además de la respuesta de cada pregunta, se le pide a cada estudiante que adjunte una foto del proceso que llevó adelante para llegar a la respuesta del ejercicio”, explicó Mirian Capelari, su secretaria académica.  
- [Tratamiento y Protección de los Datos Personales en desarrollo pruebas de Evaluación No Presenciales - ugr.es](https://covid19.ugr.es/noticias/tratamiento-proteccion-datos-personales-eveluacion-no-presencial)  
- [RESOLUCIÓN DE LA RECTORA DE 4 DE MAYO DE 2020 SOBRE EL TRATAMIENTO Y PROTECCIÓN DE LOS DATOS PERSONALES EN EL DESARROLLO DE LAS PRUEBAS DE EVALUACIÓN NO PRESENCIALES. - ugr.es](http://covid19.ugr.es/sites/servicios_files/servicios_covid19/public/ficheros/noticias/2020-05/resol.pdf)  

11 mayo 2020    
[Castells: "Hay que estar listos para establecer la enseñanza y evaluaciones online por completo"](https://www.publico.es/entrevistas/castells-hay-listos-establecer-ensenanza-evaluaciones-online-completo.html)  
Estudiantes han denunciado que han tenido exámenes tipo test en los que no han tenido ni tiempo de leer las respuestas. ¿Hay universidades o docentes más preocupadas en que los estudiantes no copien a que adquieran conocimientos?  

Puede ser un problema. La obsesión de que no copien es un reflejo de una vieja pedagogía autoritaria. Si copian bien y lo interpretan inteligentemente es prueba de inteligencia. Hoy en día tenemos sistemas informáticos que detectan textos copiados, que esos sí que son punibles. Pero la velocidad de los test es un mal sistema pedagógico. Hablo como profesor, no como ministro, porque ahí sí que hay desigualdad por motivos psicológicos que nada tienen que ver con el conocimiento.  

Otra queja de los estudiantes es que tienen que hacer exámenes con cámaras y micrófonos, ¿cree que puede suponer una invasión de la intimidad?  

Sí que lo es, pero no menos que la que hacen las redes sociales que utilizamos cada día. En el mundo digital la privacidad ya no existe… Personalmente no veo la necesidad de por qué los exámenes requieren cámara abierta, pero yo vengo de otra galaxia académica. Aprendo esta cada día, ya me lo explicarán.  

12 mayo 2020  
[Castells critica los exámenes a contrarreloj para evitar que se copie: "Es reflejo de una vieja pedagogía autoritaria"](https://www.europapress.es/sociedad/educacion-00468/noticia-castells-critica-examenes-contrarreloj-evitar-copie-reflejo-vieja-pedagogia-autoritaria-20200512183033.html)  
El ministro de Universidades defiende que si los estudiantes "copian bien y lo interpretan inteligentemente es prueba de inteligencia"  


15 mayo 2020  
[Admitida la demanda de un alumno de la UCO por la videovigilancia en exámenes 'online'](https://www.diariocordoba.com/amp/noticias/cordobalocal/admitida-demanda-alumno-uco-videovigilancia-examenes-online_1370330.html)  

14 enero 2021   
[Los exámenes “on-line” no son exámenes](https://nadaesgratis.es/juan-luis-jimenez/los-examenes-on-line-no-son-examenes)  

## Exámenes y evaluación competencial 
Surge con LOMLOE y competencias específicas  
[twitter juanintrevias/status/1583717061787758594](https://twitter.com/juanintrevias/status/1583717061787758594)  
“A mi nadie me va a quitar de hacer un EXAMEN”. Claro que no… pero estoy deseando ver cómo haces luego una rúbrica de cada ejercicio del EXAMEN a partir de los criterios y relacionada con competencias específicas para conocer la competencia y el saber hacer. Deseándolo estoy.  

La duda es ¿Por qué rubrica?  
Se trata de conocer la competencia y la clave es cómo se asocian calificaciones con competencias específicas.  
La realización de ejercicios, pruebas escritas, como la realización de pequeños proyectos de investigación se pueden considerar situaciones de aprendizaje y permiten evaluar y conocer la competencia  

Ejemplo examen que cita saberes y competencias específicas, asociado a [rúbricas](/home/recursos/rubricas)  
[twitter currofisico/status/1744476571178615050](https://twitter.com/currofisico/status/1744476571178615050)  

Ejemplo de [friquiexamen](/home/recursos/examenes/friquiexamenes) que cita saberes y competencias específicas
[twitter ProfaDeQuimica/status/1728022378033160539](https://twitter.com/ProfaDeQuimica/status/1728022378033160539)  
\#Frikiexamen de #Química (1° #Bachillerato). Evalúo criterios 1.1, 1.2, 2.2, 2.3 y 3.1 usando saberes 1 y 3 del bloque B (moles, composición centesimal, fórmula empírica y molecular, mezclas de gases y propiedades de disoluciones).  
 




