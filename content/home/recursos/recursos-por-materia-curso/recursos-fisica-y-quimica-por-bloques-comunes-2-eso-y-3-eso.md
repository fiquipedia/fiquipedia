
# Recursos Física y Química por bloques comunes 2º ESO y 3º ESO

 Por la similitud de currículo LOMCE entre 2º y 3º ESO y evitar duplicidad de mantenimiento se incluye aquí una lista única por bloques / temática: puede haber recursos asociados o similares entre  [recursos física y química de 2º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso)   
y [recursos física y química de 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso) , además puede haber en otros lugares como  [recursos relativos a adaptaciones curriculares](/home/recursos/recursos-adaptaciones-curriculares-fisica-quimica)   
  
 [Recursos método científico](/home/recursos/recursos-metodo-cientifico)   
  
 [Recursos medida](/home/recursos/recursos-medida)  [Recursos notación científica](/home/recursos/recursos-notacion-cientifica)   
 [Cambio de unidades con factores de conversión](/home/recursos/factores-de-conversion)  
  
 [Recursos laboratorio](/home/recursos/practicas-experimentos-laboratorio)  [Recursos sistemas materiales](/home/recursos/fisica/recursos-sistemas-materiales)  (dentro se incluyen propiedades generales y específicas,  [densidad](/home/recursos/fisica/recursos-densidad) , estados y cambios de estado,  [teoría cinético-molecular](/home/recursos/recursos-teoria-cinetica) ,  [leyes de los gases](/home/recursos/quimica/leyes-de-los-gases) , sustancias puras y  [mezclas](/home/recursos/quimica/recursos-mezclas) ,  [disoluciones](/home/recursos/quimica/recursos-disoluciones) ) [Recursos estructura del átomo/modelos atómicos](/home/recursos/quimica/recursos-estructura-del-atomo)  [Recursos tabla periódica / elementos químicos](/home/recursos/quimica/recursos-tabla-periodica)  [Recursos enlace químico](/home/recursos/quimica/enlace-quimico)  [Recursos cambios químicos](/home/recursos/quimica/recursos-cambios-quimicos)   
 [Recursos ajuste reacciones químicas](/home/recursos/quimica/recursos-ajuste-reacciones-quimicas)  [Apuntes formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion)  [Recursos cinemática](/home/recursos/fisica/cinematica)   
  
 [Recursos dinámica](/home/recursos/fisica/dinamica)   
 Pueden ser útiles algunos recursos sobre muelles/ley de Hooke colocados en  [movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio)   
  
 Pueden ser útiles algunos recursos asociados a  [máquinas simples](/home/recursos/fisica/maquinas-simples)   
  
 Pueden ser útiles algunos recursos de simulaciones simples colocados en [Recursos campo eléctrico](/home/recursos/fisica/recursos-campo-electrico)  [Recursos campo magnético](/home/recursos/fisica/recursos-campo-magnetico)  [Recursos gravitación](/home/recursos/fisica/recursos-gravitacion)   
 [Recursos energía](/home/recursos/energia)   
  
 [Recursos Física y Química y Medio Ambiente](/home/recursos/recursos-fisica-quimica-y-medio-ambiente)   
 
