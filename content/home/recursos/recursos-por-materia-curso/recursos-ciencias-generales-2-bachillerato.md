# Recursos Ciencias Generales 2º Bachillerato

Ver  [recursos Ciencias para el Mundo Contemporáneo 1º Bachillerato (LOE) ](/home/recursos/recursos-por-materia-curso/recursos-ciencias-para-el-mundo-contemporaneo)   
y  [recursos Cultura Científica 1º Bachillerato (LOMCE)](/home/recursos/recursos-por-materia-curso/recursos-cultura-cientifica-1-bachillerato)   

Se pueden ver recursos en [Recursos Física y Química y Medio Ambiente](/home/recursos/recursos-fisica-quimica-y-medio-ambiente)  

A nivel de recursos tras implantarse en 2023-2024 algunos docentes comparten materiales  
[Temario Ciencias Generales - Raúl Alba cc-by-nc-nd](https://raulalba.com.es/index.php/2bach/2bachcg/2bachcgtemario)  




