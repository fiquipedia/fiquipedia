
# Recursos ciencia y mujer

La idea es que en la ciencia históricamente hay majoría masculina, pero se pueden visibilizar las mujeres y fomentar vocaciones científicas de mujeres y visualizar la igualad de capacidades  
Intento recopilar ideas aquí  
 [La Tabla Periódica de las Científicas - Naukas](https://naukas.com/2018/11/23/la-tabla-periodica-de-las-cientificas/)   
 [Los Gatos con Batas | ¡A jugar!](https://www.gatosconbatas.com/ludoteca) "¿Quién es ella?", nuestro juego  
Descarga nuestro juego y disfrútalo este verano en familia.   
Se nos han quedado muchas científicas pendientes ¡y seguimos dibujando! Si este juego tiene éxito, sacaremos nuevos tableros.  
Para apoyar la iniciativa, comparte en las redes y cítanos @gatosconbatas. 
[¿​Quién es ella​? Un juego de mesa para descubrirlas  #MujeresconCiencia (pdf)](https://docs.wixstatic.com/ugd/6bdde2_13df8a1c92a7448d838f2473d80cdec5.pdf)   
Otro con idea similar y no limitado a ciencia  

[Un juego al estilo "¿Quién es quién?" con mujeres importantes de la historia](https://lacriaturacreativa.com/2018/11/15/un-juego-al-estilo-quien-es-quien-con-mujeres-importantes-de-la-historia/)   
![](https://i0.wp.com/lacriaturacreativa.com/wp-content/uploads/2018/11/who-is-she-700x405.png)  


[CSIC 4 Girls. Las chicas son de ciencias](http://csic4girls.es/)  	
El programa Las chicas son de ciencias (CSIC4girls) tiene como objetivo despertar vocaciones científicas entre estudiantes de primaria. Las niñas de 6-12 años tendrán la oportunidad de entrar en contacto con la cultura científica y de explorar y experimentar con la ciencia y la tecnología del medio ambiente.  
[CSIC 4 Girls. Las chicas son de ciencias. Comic](http://csic4girls.es/comic)  	
“¿Qué es una científica?” es un precioso cómic científico cuyo hilo conductor se basa en la esencia de las experiencias de todas las niñas que han participado a lo largo de este proyecto de Cultura Científica.  
Es una pequeña historia sobre cómo no necesitamos compararnos con ningún modelo preestablecido para ser científicas/os.  

Hilo Twitter [@Anisotropia](https://twitter.com/Anianisotropia/)  
[twitter Anianisotropia/status/1348545455077150724](https://twitter.com/Anianisotropia/status/1348545455077150724)  
Queda un mes para el día de la mujer y la niña en la ciencia #11defebrero. Como mucha gente conoce muchos más científicos que científicas, vamos a equilibrar la balanza. Voy a empezar con este Hilo- juego, #Conoce1CientíficaCadaDía  a ver cuántas adivináis (si las dibujo bien)  
![](https://pbs.twimg.com/media/Et7juH4XIAEm0K7?format=jpg)  
[Científicas - slideshare.net](https://www.slideshare.net/ANAISABELSALVADORJUN/cientificas-242414690)  Ana Isabel Salvador Junco  

[Presentaciones 11defebrero.org](https://11defebrero.org/materiales-2/presentaciones-11f/)  


[El “rosco” de las científicas - mujeresconciencia.com](https://mujeresconciencia.com/2019/12/31/el-rosco-de-las-cientificas/)  
![](https://mujeresconciencia.com/app/uploads/2019/12/El-rosco.jpg)  
![](https://mujeresconciencia.com/app/uploads/2019/12/Las-26-768x441.jpg)  

Con el caso excepcional de Marie Curie hay mucha información  
[Radioactive - fisiquimicamente.com](https://fisiquimicamente.com/blog/2021/08/24/radioactive/)  

[![Marie Curie, los inventores](https://img.youtube.com/vi/HjOJhIlheHY/0.jpg)](https://www.youtube.com/watch?v=HjOJhIlheHY "Marie Curie, los inventores")  

[La habitación científica - molasaber.org](https://molasaber.org/2021/12/27/la-habitacion-cientifica/)  
![](https://molasaber.files.wordpress.com/2021/12/la_habitacion_cientifica.jpg?w=648)  
[twitter MolaSaber/status/1476220233308426241](https://twitter.com/MolaSaber/status/1476220233308426241)  
Por otro lado, he destacado la Luna por una razón. _Cuando estaba fabricando este juego, quería meter a más mujeres._ En el caso de la Luna, quería introducir a  Katherine Johnson, pero por desgracia no se me ocurrió una manera adecuada y no ambigua de hacerlo.  

[GPS Only Exists Because Of Two People: Albert Einstein And Gladys West](https://www.forbes.com/sites/startswithabang/2021/02/18/gps-only-exists-because-of-two-people-albert-einstein-and-gladys-west/)  


Febrero 2022  
[twitter krispamparo/status/1491916095997063168](https://twitter.com/krispamparo/status/1491916095997063168)  
La versión digital del mural para el #DiaDeLaMujeryLaNinaEnLaCiencia que vamos a colgar en el @iesjaumei. Os lo explico por partes y cuento cositas.  
![](https://pbs.twimg.com/media/FLRZxanXEAMuLAr?format=jpg)  
[twitter krispamparo/status/1491916152272039938](https://twitter.com/krispamparo/status/1491916152272039938)  
![](https://pbs.twimg.com/media/FLRZ027WYAEg_dK?format=jpg)  
Agosto 2022  
[twitter krispamparo/status/1558573058696298498](https://twitter.com/krispamparo/status/1558573058696298498)  
Mas vale tarde que nunca, pero aquí dejo la versión descargable del mural con Mujeres Científicas que realicé este año para mi @iesjaumei  en colaboración con mis compañeras, que realizaron la lista y aportaron datos. En unos mesecitos, la versión 2022/2023  
![](https://pbs.twimg.com/media/FaEop6-XwAIQzZ9?format=jpg)  
[mujeres científicas.tif](https://drive.google.com/file/d/1c0SBFMZVV8OvCEkZvo2K2aa4G3Zf2bOn/view)  

[Women in Science. Historias de mujeres científicas que han cambiado el mundo, que perseveraron por el bien del conocimiento](https://fisiquimicamente.com/blog/2023/07/31/women-in-science/)  
Women in Science (WIS), Sci-Illustrate Stories es una iniciativa de Sci-Illustrate. En esta entrada nos hacemos eco de 45 de sus preciosas ilustraciones (muchas de ellas animadas), resumiendo las principales aportaciones de mujeres científicas que han cambiado el mundo, perseverando por el bien del conocimiento.

