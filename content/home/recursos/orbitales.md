
# Recursos Orbitales

## Orbitales atómicos
  
 Los orbitales enlazan con  [configuración electrónica, para la que creo una página aparte](/home/recursos/quimica/recursos-configuracion-electronica) .  
 
[Twitter ThePhysicsMemes/status/1828445227247350113](https://x.com/ThePhysicsMemes/status/1828445227247350113)
![](https://pbs.twimg.com/media/GV_xPgmWsAA6bti?format=png)  
  
Origen de letras s,p,d,f y g. Aparte de estar asociada a la forma del orbital (número cuántico l, l=0->s, l=1->p...)  
 [why-spdf.shtml](http://antoine.frostburg.edu/chem/senese/101/electrons/faq/why-spdf.shtml)   
  
 The Orbitron: a gallery of atomic orbitals and molecular orbitals on the WWW  
 [The Orbitron: a gallery of atomic orbitals on the WWW](http://winter.group.shef.ac.uk/orbitron/)   
 Copyright 2002-2006 Mark Winter [The University of Sheffield]. All rights reserved.   
 En los apuntes de átomo de Química de 2º Bachillerato incluyo una tabla periódica con orbitales tomada de esa web, que en 2024 ya no está en la web. La cito en recursos de tabla periódica, junto con otras que también usan orbitales.  
  
 [Periodic Table of the Elements, in Pictures and Words](http://elements.wlonk.com/index.htm#Orbitals)   
 [Atomic Orbitals (1 page)](http://elements.wlonk.com/Orbitals.pdf)   
 [Atomic Orbitals (Black) (1 page)](http://elements.wlonk.com/Orbitals2.pdf)   
  
 HOPV: Hybridized Orbital Preview  
 [HOPV, Hybridized Orbital Preview](http://www.adomas.org/hopv/)   
  
 [QUÍMICA PARA TODOS: Orbitales atómicos. Animación](http://quimparatodos.blogspot.com.es/2012/03/orbitales-atomicos-animacion.html)   
 Dos vídeos: s and p orbitals, d orbitals  
Microscopia de fotoionización para observar orbitales átomicos  
 [Microscopia de fotoionización para observar orbitales átomicos - La Ciencia de la Mula Francis](http://francis.naukas.com/2013/05/24/microscopia-de-fotoionizacion-para-observar-orbitales-atomicos/) relacionado [¿Se pueden ver los átomos? — Cuaderno de Cultura Científica](https://culturacientifica.com/2020/10/19/se-pueden-ver-los-atomos/)   
  
 Wikimedia, public domain,  [File:Hydrogen Density Plots.png - Wikimedia Commons](http://commons.wikimedia.org/wiki/File:Hydrogen_Density_Plots.png)   
![http://upload.wikimedia.org/wikipedia/commons/e/e7/Hydrogen_Density_Plots.png](http://upload.wikimedia.org/wikipedia/commons/e/e7/Hydrogen_Density_Plots.png "http://upload.wikimedia.org/wikipedia/commons/e/e7/Hydrogen_Density_Plots.png")
  
 [Zoom in on Atom or Unknown Physics of Short Distances | Reformulation instead of Renormalizations](http://vladimirkalitvianski.wordpress.com/2010/12/02/zoom-in-atom-or-unknown-physics-of-short-distances/)   
  
 [File:Atomic orbitals and periodic table construction.ogv - Wikimedia Commons](http://commons.wikimedia.org/w/index.php?title=File%3AAtomic_orbitals_and_periodic_table_construction.ogv)   
 Vídeo Jubobroff, cc-by-sa  

[Orbitales atómicos, una mentira de instituto - cuentos-cuanticos.com (archive.org)](http://web.archive.org/web/20210504011738/http://cuentos-cuanticos.com/2011/08/26/orbitales-atomicos-una-mentira-de-instituto/)  
  
Cosas raras por poner en algún sitio :-)  
 [News articles | TU Wien](https://www.tuwien.ac.at/en/news/news_detail/article/125643/)   
 Exotic State of Matter: An Atom Full of Atoms  
 Scientists from TU Wien (Vienna, Austria) and the USA have provided proof for a new state of matter: an electron orbits a nucleus at a great distance, while many other atoms are bound inside the orbit.  

[Orbitales Javier de Lucas ](http://www.javierdelucas.es/orbitalescuanticos2.htm)  

 
[twitter DavidBarondeau/status/1475964745777831937](https://twitter.com/DavidBarondeau/status/1475964745777831937)  
![](https://pbs.twimg.com/media/FHut4yVVUAUw6-2?format=png)  

### Simulaciones / apps
  
 [Atomic Orbitals](http://www.orbitals.com/orb/index.html)   
 Copyright 1998-2001 by David Manthey.  
Incluye orbital viewer, programa windows  
Incluye una impresionante tabla de orbitales con su representación gráfica, más allá de orbitales g  [Grand Orbital Table by Manthey](http://www.orbitals.com/orb/orbtable.htm)   
  
Visualizador orbitales  
 [Hydrogen Atom Orbital Viewer](http://www.falstad.com/qmatom/)   
 This java applet displays the wave functions (orbitals) of the hydrogen atom (actually the hydrogenic atom) in 3-D. Select the wavefunction using the popup menus at the upper right. Click and drag the mouse to rotate the view.  
 This applet displays real orbitals (as typically used in chemistry) by default; to display complex orbitals (as typically used in physics) select "Complex Orbitals" from the popup menu in the top upper right.  
 You can also view combinations of orbitals.   
 java@falstad.com  

[Hydrogen Separated Equation Solutions - hyperphysics.phy-astr.gsu.edu](http://hyperphysics.phy-astr.gsu.edu/hbase/quantum/hydwf.html#c3)  

  
 Los orbitales virtuales 3D, Enteriosoft [](https://play.google.com/store/apps/dev?id=8827829994088552831)   
 [Virtual Orbitals 3D Chemistry - Apps on Google Play](https://play.google.com/store/apps/details?id=com.AnuragAnandHazaribag.VirtualOrbitals3D)   
  
 Hydrogen Atom Orbitals, Voladd [](https://play.google.com/store/apps/developer?id=Voladd)   
 [Hydrogen Atom Orbitals - Apps on Google Play](https://play.google.com/store/apps/details?id=com.vlvolad.hydrogenatom)   

## Comprensión cualitativa de orbitales
 Entendiendo el electrón como una onda, se puede asociar el orbital a la forma de las distintas "ondas estacionarias" posibles   
 [Atomic orbital - Wikipedia](http://en.wikipedia.org/wiki/Atomic_orbital#Qualitative_understanding_of_shapes)   
  
 Simulación de oscilaciones membrana circular, asociables cualitativamente a orbitales  
 [Circular Membrane Applet](http://www.falstad.com/circosc/)   
El modo fundamental sería el asociado a orbital 1s (u01 indicado en wikipedia)  
Para orbitales 2s, 3s (u02 y u03 indicados en wikipedia), se eligen modos de oscilación inferiores al fundamental, no ambos al tiempo.  
Para orbitales p se eligen posiciones laterales.  
 Es interesante visualización 3D+2D  
 
 
 [![Los Átomos NO Son Así](https://img.youtube.com/vi/wxIxWTTsBj4/0.jpg)](https://www.youtube.com/watch?v=wxIxWTTsBj4 "Los Átomos NO Son Así")  
 
[![A Better Way To Picture Atoms](https://img.youtube.com/vi/W2Xb2GFK2yc/0.jpg)](https://www.youtube.com/watch?v=W2Xb2GFK2yc "A Better Way To Picture Atoms")  


## Orbitales y la tabla periódica
Vídeo breve muy gráfico, recomendado para visualizar ideas de configuración electrónica, construcción, orbitales y tabla periódica  [Tout est quantique](http://www.toutestquantique.fr/#atome)   
  
 En Orbitron hay una tabla periódica con orbitales que me parece muy buen resumen  
 [The Orbitron: a gallery of atomic orbitals on the WWW](http://winter.group.shef.ac.uk/orbitron/)   
 Copyright 2002-2006 Mark Winter [The University of Sheffield]. All rights reserved.   
 [![](http://www.webelements.com/shop/shopimages/products/extras/POS0007-A2-orbitron-2010-800.jpg "") ](https://www.webelements.com/shop/wp-content/uploads/2015/05/POS-0007-A2-poster-orbitron-11.jpg)   


## Hibridación de orbitales

[![Hibridación de orbitales sp, sp2 y sp3 1min 36s](https://img.youtube.com/vi/XFvwJWuLPPQ/0.jpg)](https://www.youtube.com/watch?v=XFvwJWuLPPQ "Hibridación de orbitales sp, sp2 y sp3 1min 36s")

[![Por qué Todos los Orbitales son Híbridos](https://img.youtube.com/vi/SOkxbYY5Fao/0.jpg)](https://www.youtube.com/watch?v=SOkxbYY5Fao "Por qué Todos los Orbitales son Híbridos")

[![Orbitales Moleculares Sigma,Pi e Hibridacion](https://img.youtube.com/vi/Br2qjSdB8Lw/0.jpg)](https://www.youtube.com/watch?v=Br2qjSdB8Lw "Orbitales Moleculares Sigma,Pi e Hibridacion")

 [https://web.archive.org/web/20100424221053/http://www.profeblog.es/jose/2009/12/18/hibridacion-de-orbitales-atomicos/](https://web.archive.org/web/20100424221053/http://www.profeblog.es/jose/2009/12/18/hibridacion-de-orbitales-atomicos/)   
  
 [9.5: Hybrid Orbitals - Chemistry LibreTexts](https://chem.libretexts.org/Bookshelves/General_Chemistry/Map%3A_Chemistry_-_The_Central_Science_(Brown_et_al.)/09._Molecular_Geometry_and_Bonding_Theories/9.5%3A_Hybrid_Orbitals)   
  
[Hybrid Orbitals - Chemistry LibreTexts](https://chem.libretexts.org/Bookshelves/Organic_Chemistry/Supplemental_Modules_(Organic_Chemistry)/Fundamentals/Hybrid_Orbitals)   
Orbitales pi con imagen [9.7: Molecular Orbitals - Chemistry LibreTexts](https://chem.libretexts.org/Bookshelves/General_Chemistry/Map%3A_Chemistry_-_The_Central_Science_(Brown_et_al.)/09._Molecular_Geometry_and_Bonding_Theories/9.7%3A_Molecular_Orbitals)   

## Orbitales moleculares 
En recursos asociados a  [geometría molecular](/home/recursos/quimica/geometria-molecular)  hay algunos visualizadores que permiten ver el orbital molecular además de la geometría.También puede haber información dentro de  [enlace químico](/home/recursos/quimica/enlace-quimico)   
 [Molecular Orbital Viewer](http://www.falstad.com/qmmo/)   
 This java applet displays the molecular wave functions (molecular orbitals) of the hydrogen molecular ion (H<sub>2</sub><sup>+</sup>) in 3-D. Select the wavefunction using the popup menu at the upper right. Click and drag the mouse to rotate the view. java@falstad.com  
  
 The active molecular orbitals forming the chemical bond between two uranium atoms  
 [Quantum chemical calculations show that the uranium molecule U2 has a quintuple bond | Nature](http://www.nature.com/nature/journal/v433/n7028/fig_tab/nature03249_F1.html)   

