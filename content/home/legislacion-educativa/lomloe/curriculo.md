# Currículo LOMLOE

Creo una página para ir recopilando información.  
Aquí pongo los aspectos generales, en principio en orden cronológico, e iré separando en páginas partes según vaya organizando o creciendo.  
Antes de LOMLOE no tenía una página sobre currículo en general, puede haber cosas generales que se saquen a página aparte  
[twitter tonisolano/status/1464305575106383880](https://twitter.com/tonisolano/status/1464305575106383880)  
Un currículo para gobernarlos a todos 🙄 ¿Cuántos currículos hay? ¿Cuántos de ellos son de verdad relevantes para los fines de la Escuela? Guadalupe Jover #IIICongrésInclusiva  
![](https://pbs.twimg.com/media/FFJCJ-rXEAgT5At?format=jpg)  
En la imagen aparecen:  
* Currículo legislado  
* Currículo editado  
* Currículo programado  
* Currículo en la acción  
* Currículo evaluado  
Aparte se comenta el "currículo oculto"  


En 2018 cuando comenzó tramitación LOMLOE compartí esto  
[twitter FiQuiPedia/status/1061318515360186368](https://twitter.com/FiQuiPedia/status/1061318515360186368)  
Los docentes y cada nueva reforma educativa, con su nuevo currículo y su nueva jerga para rehacer todas las programaciones @whensithometime
![](https://pbs.twimg.com/media/DrqPj3FXgAALakn?format=jpg)  


* La parte de Ciencias / Física y Química en página separada  [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  
* La parte de conceptos y estructura currículo en página separada  [Currículo LOMLOE concepto y estructura](/home/legislacion-educativa/lomloe/curriculo-concepto-estructura)  

## Noviembre 2020  
[La ministra de Educación y FP, Isabel Celaá, inaugurará el foro virtual ‘El currículo a debate’, organizado por el Ministerio](https://www.educacionyfp.gob.es/prensa/actualidad/2020/11/20201120-forocurriculo.html)

[LA REFORMA DEL CURRÍCULO EN EL MARCO DE LA LOMLOE, DOCUMENTO BASE](https://curriculo.educacion.es/wp-content/uploads/2020/11/DOCUMENTO-BASE-CURRICULO-MEFP-NOV-2020.pdf)

## Marzo 2021  
[PERFIL DE SALIDA DEL ALUMNADO AL TÉRMINO DE LA EDUCACIÓN BÁSICA, DOCUMENTO BASE MARCO CURRICULAR DE LA LOMLOE](https://www.magisnet.com/wp-content/uploads/2021/03/Perfil-de-salida-del-alumnado-al-te%CC%81rmino-de-la-Educacio%CC%81n-Ba%CC%81sica.pdf)  

* [PROPUESTA DE ESTRUCTURA CURRICULAR PARA LA ELABORACIÓN DE LAS ENSEÑANZAS MÍNIMAS](https://www.magisnet.com/wp-content/uploads/2021/03/Borrador-de-Propuesta-Curricular-1.pdf)  (pdf de 7 páginas, incluye definiciones y diagrama en página 7)  
![](https://i1.wp.com/josesande.com/wp-content/uploads/2021/10/image-1.png?w=592&ssl=1)  

## Abril 2021  
Consulta pública  
* [Proyecto de Real Decreto por el que se establecen los aspectos básicos del currículo, que constituyen las enseñanzas mínimas del bachillerato ](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/consulta-publica-previa/cerrados/2021/prd-minimas-bachillerato.html)
* [Proyecto de Real Decreto por el que se establecen los aspectos básicos del currículo, que constituyen las enseñanzas mínimas de la educación secundaria obligatoria ](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/consulta-publica-previa/cerrados/2021/prd-minimas-secundaria.html)
* [Proyecto de Real Decreto por el que se establecen los aspectos básicos del currículo, que constituyen las enseñanzas mínimas de la educación primaria ](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/consulta-publica-previa/cerrados/2021/prd-minimas-primaria.html)
* [Proyecto de Real Decreto por el que se establecen los aspectos básicos del currículo, que constituyen las enseñanzas mínimas de la educación infantil ](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/consulta-publica-previa/cerrados/2021/prd-minimas-infantil.html)

## Junio 2021  
Concreso [Nuevo currículo para nuevos desafíos](https://curriculo.educacion.es/)  

## 27 septiembre 2021  
[Más optativas en la ESO y dos nuevas modalidades en Bachillerato: así cambian los institutos con la reorganización que prepara el Gobierno - elpais.com](https://elpais.com/educacion/2021-09-27/mas-optativas-en-la-eso-y-dos-nuevas-modalidades-en-bachillerato-asi-cambian-los-institutos-con-la-reorganizacion-que-prepara-el-gobierno.html)  

[Educación propone a las CCAA ampliar a cinco las modalidades de Bachillerato e incorporar asignaturas nuevas - europapress.es](https://amp.europapress.es/sociedad/educacion-00468/noticia-educacion-propone-ccaa-ampliar-cinco-modalidades-bachillerato-incorporar-asignaturas-nuevas-20210927181411.html)  

## 29 septiembre 2021

[El Ministerio continúa avanzando con las comunidades autónomas en el desarrollo de la Ley de Educación - educacionyfp.gob.es](https://www.educacionyfp.gob.es/prensa/actualidad/2021/09/20210929-comisioneducacion.html)  

Incluye documento de 6 páginas donde se ven materias ESO y Bachillerato con las modalidades de Bachillerato  

[PROPUESTA DE ORDENACIÓN ACADÉMICA DE LAS ETAPAS DE EDUCACIÓN INFANTIL, PRIMARIA, SECUNDARIA OBLIGATORIA Y BACHILLERATO.](https://www.educacionyfp.gob.es/dam/jcr:bd062b93-7f89-4643-a1d7-c304c23453de/propuesta-de-ordenaci-n-acad-mica-cuadros.pdf)  (6 páginas)  

[Pedimos al Gobierno central aplazar la entrada en vigor de los currículos educativos - comunidad.madrid](https://www.comunidad.madrid/noticias/2021/09/29/pedimos-gobierno-central-aplazar-entrada-vigor-curriculos-educativos)  

> La Comunidad de Madrid ha solicitado al Gobierno central que aplace al curso escolar 2023/24 la entrada en vigor de los nuevos currículos educativos prevista para el de 2022/23 en 1º, 3º y 5º de Primaria, 1º y 3º de ESO y 1º de Bachillerato. La petición se produce al tener serias dudas de que los nuevos contenidos lleguen a tiempo.  

[Las comunidades piden al Gobierno más tiempo para implantar los nuevos currículos escolares - abc.es](https://www.abc.es/sociedad/abci-comunidades-piden-gobierno-mas-tiempo-para-implantar-nuevos-curriculos-escolares-202109291718_noticia.html)  

## 7 octubre 2021 

[Proyecto de real decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Infantil (octubre 2021)](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/abiertos/2021/prd-ordenacion-educacion-infantil.html) Ver Artículo 2. Definiciones.  

## 8 octubre 2021

[El borrador del nuevo diseño de la ESO: más práctica, más autonomía para los institutos y con perspectiva de género - elpais.com](https://elpais.com/educacion/2021-10-08/el-borrador-del-nuevo-diseno-de-la-eso-mas-practica-mas-autonomia-para-los-institutos-y-con-perspectiva-de-genero.html)  

[Así será el horario y las nuevas materias de la ESO: incluirán desde el feminismo y los derechos LGTBI a la alfabetización científica y digital - elpais.com](https://elpais.com/educacion/2021-10-08/feminismo-derechos-lgtbiq-y-alfabetizacion-digital-los-contenidos-y-las-horas-de-todas-las-asignaturas-que-se-estudiaran-en-el-instituto.html)  

## 11 octubre 2021
[Igualdad y Educación afectiva y sexual se trabajarán en todas las materias de la ESO - magisnet.com](https://www.magisnet.com/2021/10/igualdad-y-educacion-afectiva-y-sexual-se-trabajaran-en-todas-las-materias-de-la-eso/)  

[DOCUMENTO I: Proyecto de Real Decreto de Ordenación y enseñanzas mínimas de la ESO (PDF)](https://www.magisnet.com/wp-content/uploads/2021/10/21.10.06_RD_ESO.pdf)  

[DOCUMENTO II: Perfil de salida del alumnado al término de la enseñanza básica (PDF)](https://www.magisnet.com/wp-content/uploads/2021/10/ESO.pdf)  

## 15 octubre 2021
Trámite de audiencia del currículo de primaria  
[Proyecto de real decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Primaria](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/abiertos/2021/prd-ordenacion-ensenanzas-minimas.html)  
En Artículo 2. Definiciones se citan los conceptos de currículo que se veían en borradores   
[Proyecto de real decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Primaria (PDF)](https://www.educacionyfp.gob.es/dam/jcr:cb5a4b85-292a-42a9-b2db-10aa7f830dac/proyecto-rd-educaci-n-primaria-con-anexos.pdf)  

## 19 octubre 2021

[Currículo. Buscando la convergencia. Conversaciones en el Consejo Escolar del Estado](https://www.educacionyfp.gob.es/mc/cee/buscando-convergencia/curriculo.html)  

## 26 octubre 2021

[Dictamen nº 26: Al Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Infantil. (26-10-21) Consejo Escolar del Estado](https://www.educacionyfp.gob.es/dam/jcr:6f6a0ccc-04f3-49e9-90ea-229954ffe493/dictamen-26-2021.pdf)  

## 29 octubre 2021
Se publica documentación oficial por ministerio, incluyendo borradores.

[twitter educaciongob/status/1454055665870147589](https://twitter.com/educaciongob/status/1454055665870147589)  
Consulta en nuestro portal #educagob los primeros documentos de trabajo de los currículos de Infantil, Primaria, ESO y Bachillerato.

[CURRÍCULO > Debate público sobre el currículo > Documentación del Ministerio para el debate público](https://educagob.educacionyfp.gob.es/curriculo/debate-curriculo/documentacion-debate.html)  


[Documento base](https://educagob.educacionyfp.gob.es/dam/jcr:fe618ad3-3e43-47fa-baf1-321a5d934c65/documento-base-curriculo-mefp-nov-2020.pdf)

### Proyectos normativos en fase de tramitación

* [RD de enseñanzas mínimas de Educación Infantil](https://educagob.educacionyfp.gob.es/dam/jcr:cc29fe27-3e03-4f12-bb35-5dfe2c63271f/proyecto-rd-ed-infantil-con-anexos-cge-.pdf)
* [RD de ensenañzas mínimas de Educación Primaria](https://educagob.educacionyfp.gob.es/dam/jcr:a7840694-b80b-4b8a-8494-8120ab6f8a9d/proyecto-rd-educaci-n-primaria-con-anexos.pdf)

### Borradores de trabajo

* [Borrador de RD de enseñanzas mínimas de la ESO](https://educagob.educacionyfp.gob.es/dam/jcr:366c73cd-b09e-4997-9260-2c51f9bf0d83/proyecto-rd-secundaria-completo-.pdf)
* [Borrador de RD de enseñanzas mínimas de Bachillerato](https://educagob.educacionyfp.gob.es/dam/jcr:916a7585-8ed6-4f16-975b-dc23895a2581/proyecto-rd-bachilleratocompleto-.pdf)

## 11 noviembre 2021
Trámite de audiencia del currículo de ESO
[Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/abiertos/2021/prd-ordenacion-ensenanzas-minimas-eso.html)  

[Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria (PDF)](https://www.educacionyfp.gob.es/dam/jcr:a8262050-698f-4929-ba04-751ec36ad89e/prd-ordenacion-eso-con-anexos.pdf)  


Se deberían [publicar aquí](https://www.educacionyfp.gob.es/mc/cee/actuaciones/dictamenes/dictamenes2021/pag-6.html) dictámenes Consejo Escolar Estado (en prensa 12 noviembre 2021 se cita el de ESO pero a 12 noviembre solo está el de infantil de 26 octubre) 

## 18 noviembre 2021

[Dictamen nº 27: Al Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Primaria. (18-11-21)](https://www.educacionyfp.gob.es/dam/jcr:d0d26dfc-46cf-4a1c-b4aa-65ebb9cad23c/dictamen-27-2021.pdf)  


22 noviembre 2021 envío mínimas aportaciones a través del formulario  

---

En CD1 se indica "para recuperados, referenciados y reutilizados" y entiendo es una errata y debería ser "para recuperarlos, referenciarlos y reutilizarlos"  

Física y Química:  
4ºESO: intentando dejarlo abierto, se concretan pocos detalles, lo que creo que, aunque puede permitir concretar, al tiempo permite que pueda no hacerse haciéndolo indistinguible a efectos prácticos de una adaptación curricular; considero que sin bajar a detalles absurdos, sí se puede concretar más para evitar que se pueden generar diferencias excesivas de las concreciones de CCAA y de centros  
En saberes básicos B se debería usar "nomenclatura", que engloba formular y nombrar según IUPAC, tal y como se hace en competencia específica 3.   
Mientras que en 1º a 3º ESO se concreta "sustancias simples, iones monoatómicos y compuestos binarios", en 4º ESO no se concreta, y se podrían citar oxácidos y oxisales en inorgánica y funciones oxigenadas y nitrogenadas en orgánica, habituales en ese nivel.  
Mientras que en 1º a 3º ESO se cita explícitamente "leyes de Newton", no se citan en 4ºESO ni como leyes de la Dinámica.   
En 4ºESO se cita "movimiento de un cuerpo...situaciones cotidinas" sin concretar tipos de movimiento; en este nivel es habitual tratar MRUA y MCU. Considero que se debería citar la idea de movimientos curvilíneos enlazándolos con fuerzas de efecto centrípeto.
En 4ºESO se cita peso sin citar gravitación universal, que es habitual introducir y tratar en este nivel.  
En 4ºESO en se cita "Reconocimiento de los distintos procesos de transferencia de energía en los que están implicados fuerzas o diferencias de temperatura, como base de la resolución de problemas cotidianos.", y se podría ampliar "... para describir los efectos que producen y las cantidades que intervienen", para permitir concretar con elementos habitualmente tratados en ese nivel como puede ser dilatación, calores específicos y calores latentes.   

Matemáticas:  
No se cita explícitamente en todo el texto del proyecto asociado a matemáticas el término vector ni vectorial, cuando sí se usa en Física y Química 4º ESO "Uso del álgebra vectorial básica para la realización gráfica y numérica de operaciones con fuerzas..."  
Es necesario coordinar ambas materias, para garantizar que los alumnos tienen en el momento adecuado la competencia matemática requerida para abordar ciertas partes de Física y Química.  

---

## 9 diciembre 2021
Trámite de audiencia del currículo de Bachillerato
[Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas del Bachillerato](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/abiertos/2021/prd-ensenanzas-minimas-bachillerato.html)  

[Proyecto de real decreto por el que se establece la ordenación y las enseñanzas mínimas del Bachillerato (PDF)](https://www.educacionyfp.gob.es/dam/jcr:e72aadd9-a4bc-4f00-985e-7167a254425b/prd-ensenanzas-minimas-bachillerato.pdf)  

El 26 diciembre 2021 envío mínimas aportaciones a través del formulario  

---

En Física 2º Bachillerato, saber básico B, respecto borrador octubre 2021 se elimina "y la interacción entre ellos" por lo que se limita a interacción con cargas y se elimina interacción entre hilos. La interacción entre hilos rectilíneos que se enlaza con la definción de amperio es un concepto básico que se trata habitualmente en 2º Bachillerato y aparece habitualmente en pruebas de acceso a la universidad. Creo que omitir la referencia a interacción entre hilos conductores puede ser un error grave, del calibre de la omisión que se hizo en LOMCE a espejos esféricos que luego ha impedido considerar espejos esféricos en pruebas de acceso a universidad.  
En Física 2º Bachillerato, saber básico C, respecto a borrador octubre 2021 se elimina mención explícita a "las modificaciones de sus propiedades en función del desplazamiento del emisor y/o el receptor" dejando solo de manera genérica "fenómenos ondulatorios": creo que dejarlo así de abierto permite que pueda haber diferencias quizá excesivas entre centros y CCAA en prueba de acceso.  

En Química 2º Bachillerato, saber básico A, se indica "Enlace químico y fuerzas intermoleculares" que parece ser una errata, ya que debería ser "4. Enlace químico y fuerzas intermoleculares". Tras "estructura electrónica en diferentes niveles" falta un punto final.  

En Física y Química 1º Bachillerato, saber básico A, es algo que se trata en 2º, 3º, 4º y 2º Bachillerato: es un contenido cuya secuenciación correcta es esencial para no ser repetitivo y que su tratamiento suponga aprendizaje real. En el saber básico B de Física y Química 4ºESO se indica  
"- Reconocimiento de los principales modelos atómicos y de los constituyentes
de los átomos para establecer su relación con los avances de la física y de la
química más relevantes de la historia reciente.  
- Relación, a partir de su configuración electrónica, de la distribución de los
elementos en la tabla periódica con sus propiedades fisicoquímicas más
importantes para encontrar generalidades."  

En 1º Bachillerato se indica  
"- Desarrollo de la tabla periódica: contribuciones históricas a su elaboración
actual e importancia como herramienta predictiva de las propiedades de los
elementos.  
- Estructura electrónica de los átomos tras el análisis de su interacción con la
radiación electromagnética: explicación de la posición de un elemento en la
tabla periódica y de la similitud en las propiedades de los elementos químicos
de cada grupo."  

Considero que no hay una diferencia relevante entre 4ºESO y 1ºBachillerato, como sí la hay entre 1º Bachillerato y Química 2º Bachillerato al citar explícitamente cuántico, Bohr, orbital, Pauli, Heisenberg, onda-corpúsculo, Moeller. Si en currículo LOMLOE se recupera el átomo en 1º Bachillerato como en la situación anterior a LOMCE, debería dejarse claro qué saberes básicos se incorporan que no se hayan tratado antes y que no se vaya a tratar en 2º de Bachilerato, evitando duplicidades que pueden hastiar a los alumnos y restar avances con nuebos saberes.  

En Física y Química 1º Bachillerato, saber básico E, se cita "sólido rígido" precedido de "o": creo que dejar algo como opcional en un Real Decreto de mínimos permite que pueda haber diferencias quizá excesivas entre centros y CCAA.  

---

### 24 febrero 2022
[Las tres cartas de Ossorio para pedir que se aplacen los nuevos currículos escolares - abc.es](https://www.abc.es/espana/madrid/abci-tres-cartas-ossorio-para-pedir-aplacen-nuevos-curriculos-escolares-202202241056_noticia.html)   

Ideas: el calendario de aplicación está en la ley, en LOE genérico no cambiado por LOMCE ni LOMLOE

[Disposición final octava. Entrada en vigor.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#dfoctava)  

>La presente Ley orgánica entrará en vigor a los veinte días de su publicación en el «Boletín Oficial del Estado»  

[Disposición adicional primera. Calendario de aplicación de la Ley.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#daprimera)  

>El Gobierno, previa consulta a las Comunidades Autónomas, aprobará el calendario de aplicación de esta Ley, que tendrá un ámbito temporal de cinco años, a partir de la entrada en vigor de la misma. En dicho calendario se establecerá la implantación de los currículos de las enseñanzas correspondientes.  

En LOMLOE [Disposición final quinta. Calendario de implantación.](https://boe.es/buscar/act.php?id=BOE-A-2020-17264#df-5)  

>4. Las modificaciones introducidas en el currículo, la organización, objetivos y programas de educación secundaria obligatoria se implantarán para los cursos primero y tercero en el curso escolar que se inicie un año después de la entrada en vigor de esta Ley, y para los cursos segundo y cuarto en el curso que se inicie dos años después de dicha entrada en vigor.  

>5. Las modificaciones introducidas en el currículo, la organización y objetivos de bachillerato se implantarán para el primer curso en el curso escolar que se inicie un año después de la entrada en vigor de esta Ley, y para el segundo curso en el curso que se inicie dos años después de dicha entrada en vigor.  


Como LOMLOE es una ley orgánica, modificarla implica otra ley orgánica.  

[Dictamen Consejo Estado Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Primaria.](https://boe.es/buscar/doc.php?id=CE-D-2022-102)  

### 1 marzo 2022
[Real Decreto 157/2022, de 1 de marzo, por el que se establecen la ordenación y las enseñanzas mínimas de la Educación Primaria.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-3296)  

### 29 marzo 2022
[El nuevo decreto de ESO reforzará la orientación del alumnado para reducir el abandono temprano](https://www.educacionyfp.gob.es/prensa/actualidad/2022/03/20220329-rdsecundaria.html)  

### 30 marzo 2022
[Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975)  

En [transparencia.gob.es > Publicidad activa > Normativa y otras disposiciones > Normas con tramitación finalizada > Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://transparencia.gob.es/servicios-buscador/contenido/normavigente.htm?id=NormaEV18D2-20221202&lang=es&fcAct=Tue%20Jun%2014%2016:00:12%20CEST%202022)  
Se publica documentación como
*  [Memoria del Análisis de Impacto Normativo (MAIN) Texto final](https://servicios.mpr.es/transparencia/VisorDocTransparencia.ashx?data=EFVWMdCXIR1uFm3%2fx44H4Fv69MxutYU%2blaTLubOgDfWcgAWusyD4PLzYhxfGJnxzGpc5ItfwUFGfxf2Uw%2ffLIw0%2bAGBw7GVdf3Yr2C4Pr%2bDs3OLxV0zQtaRbXgr6yPypNZd8rvenNboFas0ZB%2bZH03M%3d) donde se analizan las aportaciones.  
*  [Dictamen del Consejo de Estado](https://servicios.mpr.es/transparencia/VisorDocTransparencia.ashx?data=EJJn3n%2bkjXIk97DcHqIS5wRISe4LIy1r5IFjqVvf3D0LUHhK9%2fONPFdOY5b8%2f5HwNY%2b92Z%2fsliQm1U3VtrzTrQR1z9WQ8G4biWVIMMLLiLgy1insxUlN0IIh4ZO2loyFeYScCLfE87OI5o0egXozmoE%3d)  


### 5 abril 2022
[El Gobierno aprueba el nuevo Bachillerato, que contará con cuatro modalidades y será más flexible](https://www.educacionyfp.gob.es/prensa/actualidad/2022/04/20220405-bachillerato.html)  

### 6 abril 2022
[Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521)  

En [transparencia.gob.es > Publicidad activa > Normativa y otras disposiciones > Normas con tramitación finalizada > Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://transparencia.gob.es/servicios-buscador/contenido/normavigente.htm?id=NormaEV18D2-20221201&lang=es&fcAct=Tue%20Jun%2014%2016:00:13%20CEST%202022)  
Se publica documentación como
*  [Memoria del Análisis de Impacto Normativo (MAIN) Texto final](https://servicios.mpr.es/transparencia/VisorDocTransparencia.ashx?data=EHZAPP02XYEtvSRZ94m8Q08gw%2fnQlerN64dP0sASHc9e8Nkk8m4Oa6e%2ftjZpP8q6ZtXLaU0wZ2ZFKSoAjKKZpQEpvNLbKFKmJt9WUJiwRMG%2fv%2fa3o9pWoLbKLl2HL0gdsFryndZ%2bxVVUF0peXbbPdlc%3d) donde se analizan las aportaciones.  
*  [Dictamen del Consejo de Estado](https://servicios.mpr.es/transparencia/VisorDocTransparencia.ashx?data=EAPDT8bcemGD60uDSxpz4Jkc5XNwpNa2MKd6ITSi9ft0BqdllDi9qRGQ6QBCuRMeo77irF%2fl21X69fFVrpxrfqdX5IxNujQNU6YRAYlugNwVmci1yIXHmXB3rsviy1MfUdyyAEe4pjFiEx7B38vlfqE%3d)  


### 7 abril 2022
[Alejandro Tiana: “La enseñanza actual tiene un aprendizaje excesivamente superficial y memorístico”](https://www.eldiario.es/sociedad/alejandro-tiana-ensenanza-actual-aprendizaje-excesivamente-superficial-memoristico_128_8899622.html)  
> El 'número dos' del Ministerio de Educación explica lo que se ha querido hacer con los currículos, señala que muchas críticas parten de “ideas preconcebidas”, rechaza que haya una rebaja en la exigencia y defiende el cambio de modelo porque hará “más profundo el aprendizaje” 

### 23 mayo 2022
Circular de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial sobre la ordenación y la organización de las enseñanzas de educación secundaria obligatoria en el curso académico 2022-2023  
Documento firmado 23 mayo 2022  
Descargable con CSV 1259225377451556959320  
Se fija carga horaria: en 3º ESO se indican 3 horas para Física y Química  

### 24 mayo 2022
Se publica trámite de audiencia
[Proyecto de decreto por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria.](https://www.comunidad.madrid/transparencia/proyecto-decreto-que-se-establece-comunidad-madrid-ordenacion-y-curriculo-educacion-secundaria)  
Plazo para formular alegaciones (ambos incluidos):   
De 25/05/2022 hasta 02/06/2022  
[Proyecto de decreto 11 abril](https://www.comunidad.madrid/transparencia/sites/default/files/2022-04-11_decreto_eso-completo_rv.pdf)  
[Proyecto de decreto 23 mayo](https://www.comunidad.madrid/transparencia/sites/default/files/2022-05-23_decreto_eso-completo.pdf)  

### 30 mayo 2022
Circular de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial sobre la ordenación y la organización del Bachillerato para el curso académico 2022-2023  
Descargable con CSV 0907653293109904746308  
Se fija carga horaria y se permite que en curso 2022-23 se imparta Cultura Científica como optativa de 4 horas.  

### 2 junio 2022
Se publica trámite de audiencia Madrid
[Proyecto de decreto del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid la ordenación y el currículo del Bachillerato.](https://www.comunidad.madrid/transparencia/proyecto-decreto-del-consejo-gobierno-que-se-establece-comunidad-madrid-ordenacion-y-curriculo-del)  
Plazo para formular alegaciones (ambos incluidos):   
De 03/06/2022 hasta 13/06/2022  
[Proyecto de decreto 28 abril](https://www.comunidad.madrid/transparencia/sites/default/files/02_1_textoproyectodecretopdf.pdf)  
[Proyecto de decreto 31 mayo](https://www.comunidad.madrid/transparencia/sites/default/files/04_2_2022-05-31_decreto_bachillerato-a.pdf)  

El 10 junio 2022 planteo mínimas alegaciones currículo Bachillerato  
Veo que algunos nombres de disciplinas que en Real Decreto estaban en mayúsculas (Física, Química) han pasado a minúsculas. Es un tema de detalle que no comento [nombres de asignaturas y licenciaturas, con mayúsculas iniciales - fundeu](https://www.fundeu.es/recomendacion/nombres-de-asignaturas-y-licenciaturas-con-mayusculas-iniciales-781/)   

---
General: en varias situaciones donde Real Decreto indicaba solo nomenclatura se pasa a indicar "formulación y nomenclatura", y es innecesario, ya que según IUPAC, nomenclatura es un término que incluye ambos. Se incluye texto (el propio título del documento tiene solo nomenclatura aunque trata ambos) con fuente, ver "names AND formulae"   
http://old.iupac.org/publications/books/rbook/Red_Book_2005.pdf  
IR-1.3 AIMS OF CHEMICAL NOMENCLATURE  
The primary aim of chemical nomenclature is to provide methodology for assigning descriptors (names and formulae) to chemical species so that they can be identified without ambiguity, thereby facilitating communication  

* Física y Química 1º Bachillerato:  
En texto inicial hay una errata grave: donde Real Decreto indica "caso particular" se pasa a poner "estudio de las partículas" y no tiene ningún sentido.   
En bloque B se añade "termoquímica", "entalpía", "ley de Hess": considero que es un error ya que está en Química de 2º Bachillerato en bloque de contenidos B.1, y supone una sobrecarga repetirlo en 1º; el planteamiento general es que la situación sea previa a LOMCE, cuando se trataba en 2º pero no en 1º.  
En bloque C de contenidos hay erratas: "las IUPAC" debería ser "la IUPAC", y "serie homologa" debería ser "serie homóloga"  
En bloque D se indica "Relatividad de Galileo" y lo correcto es "Relatividad de Galilei", de la misma manera que lo correcto son Leyes de Newton y no Leyes de Isaac.  

* Ciencias Generales 2º Bachillerato:  
En 3.1 se elimina "sostenible" y no tiene sentido ya que se cita explícitamente y se mantiene en Real Decreto de enseñanzas mínimas, bloques de saberes básicos B y C  

* Física 2º Bachillerato:  
En bloque B se elimina referencia explícita a motores que sí aparece en Real Decreto de enseñanzas mínimas.  
En bloque D se usa "1. Principios de la Relatividad." y más tarde se usa " principios fundamentales de la relatividad especial": considero que puede dar lugar a confusión, sería más claro usar en el título otro término, por ejemplo "conceptos básicos"  

* Química 2º Bachillerato:  
En A.1 se elimina "Interpretación de los espectros de emisión y absorción de los elementos. Relación con la estructura electrónica del átomo.", texto que aparece en Real Decreto de enseñanzas mínimas, y aunque se añade en su lugar "El espectro de emisión del hidrógeno." debería mantenerse el texto original  
En C.2 se añade literalmente "isomería geométrica" que es algo obsoleto y desaconsejado por IUPAC "geometric isomerism" https://goldbook.iupac.org/terms/view/G02620  

---

## 19 julio 2022
[DICTAMEN del Pleno de la Comisión Jurídica Asesora de la Comunidad de Madrid, aprobado por unanimidad, en su sesión de 19 de julio de 2022, emitido ante la consulta formulada por el vicepresidente, consejero de Educación y Universidades al amparo del artículo 5.3 de la Ley 7/2015, de 28 de diciembre, por la que se somete a dictamen el “proyecto de decreto del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria.”.](https://www.comunidad.madrid/sites/default/files/dictamenes/2022/dictamen_492-22_des.pdf)  

[DICTAMEN del Pleno de la Comisión Jurídica Asesora de la Comunidad de Madrid, aprobado por unanimidad, en su sesión de 19 de julio de 2022, emitido ante la consulta formulada por el vicepresidente, consejero de Educación y Universidades al amparo del artículo 5.3 de la Ley 7/2015, de 28 de diciembre, por la que se somete a dictamen el “proyecto de decreto del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la etapa de Bachillerato”.](https://www.comunidad.madrid/sites/default/files/dictamenes/2022/dictamen_480-22_des.pdf)  

### 27 julio 2022

[Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF)  
[Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-2.PDF)  

### 6 septiembre 2022
Circular de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial en relación con la organización y el currículo de los ámbitos del programa de diversificación curricular de la Educación Secundaria Obligatoria durante el curso 2022-2023.  
CSV 1202871551640335491638  

### 15 septiembre 2023
[ACUERDO de 13 de septiembre de 2023, del Consejo de Gobierno, por el que se procede a la corrección de errores del Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del bachillerato.](https://bocm.es/boletin/CM_Orden_BOCM/2023/09/15/BOCM-20230915-1.PDF)  


