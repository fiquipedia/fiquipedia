
# Elementos

La idea es crear una página por elemento / compuesto y centralizar en cada una información asociada.  
De momento hay pocas páginas y solo de elmentos, quizá si crece separe también en compuestos  

[Carbono](/home/recursos/quimica/elementos/carbono)   
[Hidrógeno](/home/recursos/quimica/elementos/hidrogeno)  
[Hierro](/home/recursos/quimica/elementos/hierro)   
[Oro](/home/recursos/quimica/elementos/oro)   
[Plata](/home/recursos/quimica/elementos/plata)   
[Zinc](/home/recursos/quimica/elementos/zinc)  
