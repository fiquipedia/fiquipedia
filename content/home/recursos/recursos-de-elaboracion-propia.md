
# Recursos de elaboración propia

Llamo recursos de elaboración propia a recursos que suelo subir como pdf, no incluyo texto que pueda tener en página web  
Comentarios generales: los recursos de elaboración propia, suelen ser "muy compactos/densos", y es deliberado, ya que pretendo que ocupen pocas hojas. De hecho en algunos pongo una limitación de 1 o dos caras porque así los alumnos tienen "la hoja que trata del tema tal" localizada y no son páginas y páginas. He comenzado por recursos para niveles altos (2º Bachillerato) en los que los alumnos son capaces de filtrar información en documentos densos / no maquetados muy estéticamente, pero poco a poco me gustaría hacer materiales para todos los niveles bajando hasta ESO y ahí es posible que haya que prestar atención al formato. Una idea de materiales propios son una colección de "pizarras" para cada curso: prefiero llamarlo pizarras y no transparencias, y la idea es que cuando explico algo en la pizarra a varios grupos, me doy cuenta que siempre sigo un esquema similar en la pizarra, por lo que si pongo en formato digital el contenido de la pizarra (que no es muy denso), puede ser un material útil para dar clase si se dispone de proyector. Se puede ver en  [pizarras Física y Química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel) 

##  [](/) Ejercicios recopilados con enunciados de dominio público y soluciones de elaboración propia
 La clave es que cuando hay pruebas públicas (PAU, pruebas libre graduado ESO, pruebas acceso Grado Medio, pruebas acceso Grado Superior, ...) los enunciados se publican y son de dominio público, por lo que son interesantes como ejemplos de problemas. Yo intento agruparlos y organizarlos, y a veces resolverlos, y esa recopilación y solución li que publico con licenciamiento cc-by, porque los enunciados son libres.

###  [](/) Colecciones de enunciados y soluciones de PAU 

   *  [PAU Física](/home/pruebas/pruebasaccesouniversidad/paufisica) 
   *  [PAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) 
   *  [PAU Electrotecnia](/home/pruebas/pruebasaccesouniversidad/pau-electrotecnia) 


###  [](/) Selección de ejercicios/apartados de ejercicios PAU cuyo nivel es válido para realizarlos en ESO

   *  [Enlace químico](/home/recursos/quimica/enlace-quimico)  (selección ejercicios PAU para ESO)
   *  [Configuración electrónica](/home/recursos/quimica/recursos-configuracion-electronica)  (selección ejercicios PAU para ESO)


###  [](/) Colecciones de enunciados y soluciones Grado Medio / Prueba Libre Graduado ESO 

   *  [Enunciados y soluciones pruebas acceso Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio)  (energía y materia (incluye  [mezclas](/home/recursos/quimica/recursos-mezclas) ), mecánica (cinemática, dinámica) 
   *  [Prueba libre Graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)  (Materia y estados,  [mezclas](/home/recursos/quimica/recursos-mezclas) , elementos y compuestos, átomo y enlace, reacciones químicas, cinemática, dinámica, presión, energía) 


###  [](/) Problemas de oposición resueltos

   *  [Problemas de oposición resueltos](/home/recursos/recursos-para-oposiciones) 


###  [](/) Problemas de fase local Madrid Olimpiadas Física

   * Ver en [olimpiadas de física](/home/recursos/fisica/olimpiadas-de-fisica) 


##  [](/) Apuntes de elaboración propia

   * Intento realizar "miniapuntes"/esquemas breves de pocas caras de folio (ver comentarios generales), que traten solamente un contenido, y poco a poco voy subiéndolos con licenciamiento cc-by en distintas páginas. Intento citar aquí todas, aunque como es algo disperso igual falta alguna. En 2º bachillerato tras realizar las soluciones de PAU agrupo por bloques apuntes de teoría, y los coloco en las páginas de recursos de los cursos correspondientes o en una página aparte que se enlaza desde ellos. También hago "pizarras": presentaciones simples asociadas a lo que se pondría en la pizarra / en proyector. [Pizarras Física y Química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel) 

   *  [Apuntes elaboración propia Física 2º Bachillerato](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato) 
   *  [Apuntes elaboración propia Química 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato) 
   *  [Apuntes elaboración propia Física y Química 1º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato) 
   *  [Apuntes elaboración propia Cultura Científica 1º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-cultura-cientifica-1-bachillerato)  ("pizarras")
   *  [Apuntes elaboración propia Física y Química 4º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso/apuntes-fisica-y-quimica-4-eso-elaboracion-propia)  ("pizarras")
   *  [Apuntes elaboración propia Física y Química 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso/apuntes-elaboracion-propia-3-eso)  (apuntes y "pizarras")
   *  [Apuntes elaboración propia Física y Química 2º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso/apuntes-elaboracion-propia-fisica-y-quimica-2-eso)  ("pizarras")
   *  [Cinemática](/home/recursos/fisica/cinematica)  (con apuntes para nivel 4º ESO y 1º Bachillerato)
   *  [Dinámica](/home/recursos/fisica/dinamica)  (con apuntes para nivel 4º ESO y 1º Bachillerato)
   *  [Fuerzas en fluidos](/home/recursos/fisica/fuerzas-en-fluidos)  (con apuntes 4º ESO)
   *  [Isomería](/home/recursos/quimica/isomeria)  (con apuntes 1º Bachillerato)
   *  [Apuntes formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion) 
   *  [Estados oxidación](/home/recursos/quimica/estados-de-oxidacion) 
   *  [Propiedades sustancias](/home/recursos/quimica/recursos-propiedades-sustancias) 
   *  [Magnitudes, medición, unidades](/home/recursos/recursos-medida)  Uso del separador decimal, alfabeto griego en física y química ...
   * A veces creo diagramas dentro de los apuntes, y otras también por separado, por ejemplo asociado a [Modelo Estándar](/home/recursos/fisica/recursos-modelo-estandar) 


##  [](/) Ejercicios de elaboración propia
 En general intento reutilizar enunciados de ejercicios, pero de vez en cuando creo mis propios ejercicios, que a veces uso en clase o en exámenes, donde intento utilizar temas actuales, datos reales e imágenes... (que requieren revisar el licenciamiento). En el curso de 2014 sobre física de partículas creé algunos.  
A partir de 2015 intento compartir aquí algunos puestos en exámenes: en exámenes a veces pongo variantes de ejercicios que se han hecho del libro, por lo que no soy demasiado purista en licenciamiento y los exámenes no los comparto aquí al no ser totalmente cc-by, pero sí lo haré con los ejercicios creados, por ejemplo para física de 2º de Bachillerato
   *  [Ejercicios elaboración propia Física 2º Bachillerato](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato) 
También hay ejercicios recopilados por bloques, a veces tomados de exámenes, a veces de recursos como PAU de Mecánica
   *  [Cinemática](/home/recursos/fisica/cinematica)  (con ejercicios por bloques)
   *  [Dinámica](/home/recursos/fisica/dinamica)  (con ejercicios por bloques y hojas con ejercicios para entregar / fotocopiar en 1º Bachillerato)
   *  [Termoquímica](/home/recursos/quimica/termoquimica)  (con hojas con ejercicios para entregar / fotocopiar en 1º Bachillerato)
También ejercicios propios en  [FriQuiExámenes](/home/recursos/examenes/friquiexamenes) 

##  [](/) Prácticas de laboratorio de elaboración propia

   * Ver en [prácticas de laboratorio de elaboración propia](/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia) 


##  [](/) Adaptaciones curriculares de elaboración propia
 Ver en  [adaptaciones curriculares](/home/recursos/recursos-adaptaciones-curriculares-fisica-quimica) 

##  [](/) Proyectos de investigación de elaboración propia

   * Ver en [proyectos de investigación](/home/recursos/proyectos-de-investigacion) 


##  [](/) Materiales en algunos cursos que he realizado.
 Los creo con licencia cc-by y los comparto aquí, por orden cronológico inverso (lo más reciente lo primero)
   * (mayo 2014) [Seminario 2013-2014 Física de partículas en el aula](http://ctif.madridcapital.educa.madrid.org/index.php?option=com_content&view=article&id=1007&Itemid=83) . Aparte de otros trabajos del seminario,  [documento con actividades de aula sobre física de partículas (cinemática, MRU, MRUA, MCU, y un ejemplo de física moderna)](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-de-elaboracion-propia/ActividadesParticulas.pdf) 
   *  [(diciembre 2013) Trabajo final "Óptica Geométrica" del curso "Recursos Educativos Abiertos en la enseñanza de Ciencias", INTEF.](http://agrega.educacion.es/ODE2/es/es_2013120212_9122007) 
   *  [(mayo 2013) Proyecto para programa formación en el CERN 2013](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-de-elaboracion-propia/ProyectoProgramaFormacionCERN-EnrigueGarc%C3%ADaSim%C3%B3n.pdf) . Para participar se indicaba "Todas las personas inscritas presentarán un proyecto que contemple los siguientes aspectos: Objetivos de los participantes. Debe indicarse qué pretende el solicitante alcanzar al participar en este programa, cómo prevé que esta formación en Física de Altas Energías influya en sus planteamientos docentes/curriculares. Aplicaciones didácticas en el aula, en el laboratorio y en otros ámbitos que se prevé podrán realizarse tras la finalización del programa. Diseño de actividades que permitan introducir los conceptos físicos de Altas Energías y fomentar la curiosidad y el interés del alumnado. Su extensión no debe exceder de 6 páginas y deberá estar escrito con letra Arial, de tamaño 12 e interlineado doble."  
Lo comparto aquí con licencia Creative Commons para, tal y como indicaba, pueda ser modificado y ampliado. En 2013 el enlace con información era  [http://gestiondgmejora.educa.madrid.org/cern/](http://web.archive.org/web/20130608210320/http://gestiondgmejora.educa.madrid.org:80/cern/) *waybackmachine*   
   *  [(octubre 2011) Trabajo final del curso "La competencia digital en secundaria", Formación en línea Comunidad de Madrid. 2011, Una unidad didáctica de física y química de 4º ESO sobre el bloque "El átomo y propiedades de las sustancias" ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-de-elaboracion-propia/TICD_ProyectoFinal2011-UD-4ESOFQ-Elátomoypropiedadesdelassustancias-EnriqueGarciaSimon.pdf) 
   *  [(mayo 2011) Trabajo final del curso "Mecánica cuántica", Centro Territorial de Innovación y Formación Madrid-Sur, "Propuesta de puesta en práctica en el aula" para física de 2º de Bachillerato.](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-de-elaboracion-propia/TrabajoFinalMecanicaCuantica-EnrigueGarc%C3%ADaSim%C3%B3n.pdf) 
   *  [(febrero 2011) Trabajo final del curso "El cine, un recurso didáctico", Formación en línea Comunidad de Madrid. 2011. Una unidad didáctica de física y química de 4º ESO sobre el bloque "Fuerzas en fluidos: Presión".](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-de-elaboracion-propia/TareaBloqueFinal-EnrigueGarc%C3%ADaSim%C3%B3n.pdf) 
   *  [CURSO 035 -FUNCIONARIOS EN PRÁCTICAS DEL CUERPO DE PROFESORES DE ENSEÑANZA SECUNDARIA (GRUPO 1). CURSO 2016-2017. Memoria sobre las funciones del profesorado en relación con la docencia, organización y funcionamiento de los Centros Públicos](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-de-elaboracion-propia/GARCIASIMON_ENRIQUE_C35_1617.pdf)  
   *  [Memoria final profesores funcionarios en prácticas 2016-2017](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-de-elaboracion-propia/2017-MemoriaFinalFuncionariosPracticas-EnriqueGarciaSimon.pdf)  
   
   
