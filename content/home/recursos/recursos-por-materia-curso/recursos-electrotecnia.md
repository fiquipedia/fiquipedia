
# Recursos Electrotecnia 2º Bachillerato

Hay partes de electricidad, electromagnetismo ... que se pueden considerar de física  
Puede haber recursos no limitados a 2º Bachillerato   
  
 [Electrotecnia - etitudela.com](http://www.etitudela.com/Electrotecnia/index.html)   
Trabajo realizado en la convocatoria de proyectos de innovación, promovida por el Programa de Nuevas Tecnologías de la Educación del Gobierno de Navarra, para el curso escolar 2003-2004.   
Autores: Agustín Labarta, Juan Pablo Lázaro, Migual A. Lizaldre, Fernando Pascual y Nacho Pelayo, Tudela 2003/2004  
 
 [Electrotècnia (autoformació IOC) - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=14)  
  
 [Electrotecnia Fernando Martínez Moya](https://fermoya.es/category/electrotecnia/) cc-by-nc-sa Fernando Martínez Moya  
  
 [Electrotecnia - tuveras.com](http://www.tuveras.com/electrotecnia/electrotecnia.htm)   
Tú verás. Web de tecnología eléctrica. Juan Luis Hernández  
  
 [Electrotecnia. Asignatura: Electrificación Rural, Grado en Ingeniería del Medio Rural. ETSIAM . Córdoba ](http://www.trifasica.net/)   
 
  
 [Física – Diseño Industrial. Impedancias, corrientes y fasoriales](http://www.ing.unlp.edu.ar/cys/DI/Alterna.pdf)   
Resumen 4 páginas. Ing. Cristian Zujew   
  
 [Apuntes de Electrotecnia](http://platea.pntic.mec.es/~jalons3/Electrotecnia/apu-elec.htm)   
José Ignacio Alonso del Olmo  
Profesor de Tecnología en el Instituto de E.S. Julián Marías de Valladolid  
  
Electrotecnia  
 [http://elefp.wikispaces.com/](http://elefp.wikispaces.com/)   
Apuntes y ejercicios. En algunos ejercicios se indica "I.E.S. “Pablo Ruíz Picasso”: Instalaciones Electrotécnicas y Automáticas"  
  
Electrotecnia para circuitos industriales  
 [Electrotecnia para aplicaciones industriales](http://www.sapiensman.com/electrotecnia/index.htm)   
Autor y licenciamiento no indicado  
  
IES Sierra Magna. Departamento tecnología. Zona de descarga.  
 [descarga.htm](http://www.juntadeandalucia.es/averroes/~23005153/d_tecnologia/descarga.htm)   
Enunciados problemas electrotecnia por bloques  
  
 [TEMA 8 MAGNETISMO Y ELECTROMAGNETISMO.pdf](http://iesbernatguinovart.com/04a_matematiques/carpeta_arxius/TEMA%208%20MAGNETISMO%20Y%20ELECTROMAGNETISMO.pdf)   
  
 [Documento sin t&iacute;tulo](http://www.geocities.ws/lpepe2000/raiz/Electrividad.html)   
Autor: JOSÉ LUIS ORTEGA  
En agosto 2014 es una página en prueba, con partes en boceto/creación.  
  
"Electrotecnia - pepeweb"  
 [index.html](http://www.netcom.es/pepeele/index.html)   
Teoría, problemas, controles, actividades, exámenes. Muy bien elaborados. Página "pepeweb" elaborada por José Luis Álvarez  
 Página principal  [index.html](http://www.netcom.es/pepeweb/index.html)  con más contenidos: electrónica, automatismos,tecnología industrial, Parece asociada al blog  [Tecnocangas](http://tecnocangas.blogspot.com.es/)  donde se menciona IES Rey Pelayo de Cangas de Onis.  
*No operativa en agosto de 2014*  
  
 [Circuitos RLC.pdf](http://www.labc.usb.ve/paginas/mgimenez/Ec1181ele/Material/Circuitos%20RLC/Circuitos%20RLC.pdf)   
Capítulo 6 Circuitos RLC. César Sánchez Norato  
  
 [Docencia e Investigacin](http://www3.uah.es/vivatacademia/anteriores/n43/docencia.htm#Breve%20introducci%C3%B3n%20a%20corrientes%20polif%C3%A1sicas)   
Breve introducción a corrientes polifásicas  
Arturo Pérez París. Universidad de Alcalá  
  
(Pendiente mover a apartado de libros)  
Principios de electrotecnia  
 [Principios de electrotecnia - Adolf Senner - Google Libros](http://books.google.es/books?id=PW7jBPNU8hwC&printsec=frontcover&hl=es#v=onepage&q&f=false)   
Adolf Senner. Editorial Reverté, ISBN 84-291-3448-4  
  
 [http://www.catedu.es/aratecno/](http://www.catedu.es/aratecno/)   
Asociables a electrotecnia apartados electricidad y electrónica  
El proyecto Aratecno lo crea hace ya unos años la profesora Pilar Latorre del IES Salvador Victoria, como Web de recursos para profesores y alumnos de TECNOLOGÍA.  
Han participado más profesores  
  
ELECTRÓNICA BÁSICA, Curso de Electrónica Básica en Internet, Andrés Aranzabal Olea  
 [default](http://www.sc.ehu.es/sbweb/electronica/elec_basica/default.htm)   
  
 [Direct Current Circuit Concepts](http://hyperphysics.phy-astr.gsu.edu/hbase/electric/dccircon.html#c1)   
  
Ángel Franco García, Curso "Fundamentos físicos de energías renovables", Física > Campo eléctrico > Condensadores,   
 [El condensador plano-paralelo](http://www.sc.ehu.es/sbweb/fisica3/electrico/plano_paralelo/plano.html)   
Ángel Franco García, Curso "Física con ordenador"  
 [Electromagnetismo](http://www.sc.ehu.es/sbweb/fisica_/elecmagnet/elecmagnet.html#condensadores)   
 [electrotecnia-2-bachillerato.html](http://clasesfisicayquimica.blogspot.com.es/p/electrotecnia-2-bachillerato.html)   
  
Santos Benito, J.V.. Problemas de Física Aplicada.  
Editorial Club Universitario (www.ecu.fm). Alicante.  
ISBN.: 84-8454-099-5. Depósito legal A-17-2002.  
96 problemas resueltos, 45 problemas propuestos, 114 páginas, 209 ilustraciones.(17 x 24 cm)  
Descarga gratuita  
 [Problemas_de_Física_Aplicada.pdf](http://www.fisicaenppt.esy.es/Problemas_de_F%C3%ADsica_Aplicada.pdf)   
La obra contiene más de 100 problemas relativos al temario de la asignatura: Circuitos de corriente continua, circuitos de corriente alterna, principios de electrónica ...  

