
# 1º Bachillerato Ampliación Física y Química I (LOMCE)
  
Ver ORDEN 2200/2017, de 16 de junio, de la Consejería de Educación, Juventud y Deporte, por la que se aprueban materias de libre configuración autonómica en la Comunidad de Madrid. [BOCM-20170627-15.PDF](http://www.bocm.es/boletin/CM_Orden_BOCM/2017/06/27/BOCM-20170627-15.PDF)   
Se puede ver cierta relación con contenidos de  [Física de 2º Bachillerato](/home/materias/bachillerato/fisica-2bachillerato)  y de  [Química 2º Bachillerato](/home/materias/bachillerato/quimica-2bachillerato) , y de aspectos de materias no vigentes en 2017 como  [Mecánica de 2º Bachillerato (LOGSE)](/home/materias/bachillerato/mecanica-2-bachillerato)   

## Introducción 
La presente materia está diseñada para su oferta en primer curso del Bachillerato. Los cambios sociales experimentados en los últimos siglos se deben en gran parte a los logros conseguidos por la ciencia y por la actividad de los científicos, sobre todo en aspectos relacionados con la salud, el medio ambiente y el desarrollo tecnológico.  
En un mundo cada vez más tecnificado, los ciudadanos deben tener competencia científica. La competencia científica es importante para comprender los problemas ambientales, médicos, económicos y de otro tipo a los que se enfrentan las sociedades modernas, que dependen enormemente del progreso tecnológico y científico  
Además, el rendimiento de los mejores alumnos de un país en las materias científicas tiene repercusiones en el papel que el mismo desempeñe el día de mañana en el sector de las tecnologías avanzadas y en su competitividad internacional en general. Por el contrario, las deficiencias en competencia matemática y científica pueden tener consecuencias negativas para las perspectivas laborales y económicas de los individuos, así como para su capacidad de participar plenamente en la sociedad.  
La Física y Química, junto con el resto de las materias que componen el conocimiento científico, aparece hoy en día como imprescindible para una sociedad, pues:  
— Forma parte de la cultura general, si por cultura entendemos el conjunto de conocimientos científicos, históricos, literarios y artísticos.  
— Proporciona las bases para comprender el desarrollo social, económico y tecnológico que caracteriza el momento actual que ha permitido al hombre alcanzar a lo largo del tiempo una mayor esperanza y calidad de vida.  
— Proporciona un evidente enriquecimiento personal porque despierta y ayuda a la formación de un espíritu crítico.  
— Es modeladora de valores sociales, precisamente por su propio carácter social.  
— Proporciona las bases del conocimiento y la práctica del método científico.  
— Permite a las personas intervenir con criterios propios en muchos de los grandes temas presentes en la sociedad actual: cambio climático, conservación del medio ambiente, biotecnología, energías renovables,etcétera.  
— Es la base de un gran número de salidas profesionales.  
La materia Ampliación de Física y Química permite a los alumnos profundizar en contenidos que se abordan de forma más general en la Física y Química de primer curso del Bachillerato y estudiar otros que le serán de utilidad para estudios posteriores. En cualquiera de los casos, esta materia enriquecerá tanto a los alumnos que finalizan sus estudios en esta etapa, como a aquellos que los continuarán en la formación profesional de grado superior o los estudios superiores.  
La idea de que la Física y la Química, como todas las ciencias, tiene implicaciones con la tecnología y la sociedad debe ponerse de manifiesto en la metodología, planteando cuestiones teóricas y prácticas mediante las que el alumno comprenda que uno de los objetivos de la ciencia es determinar las leyes que rigen la naturaleza. El proceso de adquisición de una cultura científica, además del conocimiento y la comprensión de los conceptos, implica el aprendizaje de procedimientos y el desarrollo de actitudes y valores propios del trabajo científico. La realización de actividades prácticas y el desarrollo de algunas fases del método científico permitirán alcanzar habilidades que servirán de motivación para lograr nuevos conocimientos y poner en práctica métodos del trabajo experimental.  
Los contenidos de la materia quedan distribuidos en seis bloques:  
- Dinámica de sistemas de partículas  
- Dinámica de rotación  
- Estructura atómica  
- Enlace  
- Reacciones redox y electroquímica  
- Cinética química  

## Contenidos


### Bloque 1. Dinámica de sistemas de partículas
- Introducción.  
- Sistemas de partículas. Sistemas discretos y continuos.  
- Centro de masas de un sistema de partículas.  
- Cinemática del centro de masas para un sistema discreto:  
o Posición.  
o Velocidad.  
o Aceleración.  
- Dinámica del centro de masas para un sistema discreto:  
o Fuerza. Ecuación fundamental de la dinámica.  
o Momento lineal. Teorema del momento lineal y teorema de conservación.  
- Posición del centro de masas de sistemas continuos con geometría sencilla.

### Bloque 2. Dinámica de rotación
- Aproximación del sólido rígido.  
- Analogías y diferencias entre traslación y rotación.  
- Magnitudes asociadas a la dinámica de rotación.  
o Momento de Inercia.  
o Momento de una fuerza. Ecuación fundamental de la dinámica de rotación.  
o Momento angular. Teoremas.  
o Energía de un sólido en rotación. Teoremas.

### Bloque 3. Estructura atómica
- Partículas subatómicas y elementales. Tubos de descarga. Modelos atómicos de Thomson y de Rutherford.  
o Descripción y análisis de los acontecimientos históricos que desembocaron en el asentamiento de las bases sobre la estructura del átomo.  
- Número atómico. Número másico. Isótopos.  
o Estudio de familias especialmente importantes de isótopos: hidrógeno, carbono, uranio, etc.  
- Espectros atómicos. Hipótesis de Planck. Modelo atómico de Bohr.  
o Estudio de la importancia de los espectros y muestra de aplicaciones especialmente importantes de estos: identificación de sustancias, astronomía, etc.  
- Hipótesis de De Broglie. Orbital atómico. Principio de incertidumbre de Heisenberg.  
o Resolución de ejercicios en los que intervengan el principio de incertidumbre de Heisenberg y la hipótesis de De Broglie.  
- Números cuánticos. Configuración electrónica de un átomo.o Obtención de la configuración electrónica de un elemento y asignación a cada uno de sus electrones los números cuánticos n, l, m, s.  
- Radiactividad.o Descripción de los tipos de radiactividad natural y sus consecuencias sobre los seres vivos.  
Radiactividad artificial y aplicaciones: reacciones nucleares, fisión nuclear, fusión nuclear y aplicaciones de la radiactividad.  
- Sistema Periódico. Configuración electrónica y periodicidad. Energía de ionización. Afinidad electrónica.  
Electronegatividad.  
o Estudio del Sistema Periódico para familiarizarse con el lugar que ocupa cada elemento.  
o Resolución de ejercicios en los que se pida determinar propiedades de los elementos en función de su posición en el Sistema Periódico.

### Bloque 4. Enlace
- Enlace químico. Estabilidad energética. Teoría de Lewis. Estructuras de Lewis.  
o Representación de la configuración electrónica de un elemento y, a partir de ella, indicar si tiene tendencia a formar un enlace iónico o covalente.  
o Utilización de la teoría y los diagramas de Lewis para representar la estructura de moléculas sencillas.  
- Enlace iónico. Energía reticular.o Exposición y justificación de las propiedades más notables de las sustancias iónicas.o Visualización de dibujos y modelos geométricos que muestren la estructura de algunas redes cristalinas.  
- Enlace covalente. Covalencia. Promoción electrónica. Enlace covalente dativo. Polaridad.  
o Exposición y justificación de las propiedades más notables de las sustancias covalentes.  
- Enlace metálico. Modelo del mar de electrones.  
o Exposición del modelo del mar de electrones en los metales.  
o Exposición y justificación de las propiedades más notables de las sustancias metálicas.  
- Fuerzas de Van der Waals. Enlaces de hidrógeno.  
o Visualización de modelos geométricos en los que se representen, mediante varillas, los puentes de hidrógeno.

### Bloque 5. Reacciones redox y electroquímica
- Reacciones de oxidación-reducción.o Concepto de oxidación y de reducción y su evolución. Concepto actual.o La oxidación y la reducción como procesos complementarios. Interpretarlos, por tanto, como un intercambio de electrones entre dos sustancias químicas.o Oxidantes y reductores. Pares redox.o Número de oxidación. Ajuste de reacciones redox: método del ion-electrón (en medio ácido y en medio básico).o Reglas para asignar números de oxidación. Diferencia con el concepto de carga eléctrica y el de valencia.o Ajuste de reacciones redox. Método del ion-electrón: ajuste en medio ácido y en medio básico.- Estequiometría de los procesos redox.o Tipos de procesos redox. Valoraciones redox. Cálculo de masas equivalentes. Indicadores redox.o Cálculos de equivalentes redox en diversos procesos.o Aplicar las leyes de la estequiometría a las reacciones redox.o Realización de experiencias sencillas de laboratorio, ejercicios y problemas, sobre las valoraciones redox. Determinación de la concentración de una disolución.- Electrólisis.o Electrólisis de sales fundidas. Electrólisis del agua. Electrólisis de sales en disolución acuosa.o Aspectos cuantitativos de la electrólisis. Constante de Faraday. Resolución de ejercicios y problemas.

### Bloque 6. Cinética química
- Cinética química.  
o Concepto de velocidad de reacción. Unidades de velocidad.  
o Ecuación cinética de una reacción química. Ley diferencial de velocidad. Órdenes parciales y orden total de una reacción química.  
o Aplicación de los conceptos cinéticos para determinar la ecuación de velocidad.  
o Resolución de ejercicios y problemas sencillos sobre cinética química.  
- Mecanismo de las reacciones químicas.o Etapas elementales. Etapa limitante. Intermedios de reacción. Molecularidad. Algunos tipos de mecanismos de reacción.  
- Factores que influyen en la velocidad de reacción.o Naturaleza del proceso químico. Concentración y estado físico de los reactivos. Catalizadores.  
o Temperatura: ecuación de Arrhenius.  
o Energía de activación.  
o Predicción y justificación de cómo varía la velocidad de una reacción química dada con algunos de los factores ya estudiados.  
- Teoría de las reacciones químicas.o Estudio de las reacciones químicas en términos de la teoría de colisiones. Choques eficaces. Energía de activación. Factor estérico.  
o Teoría del estado de transición.  
o Dibujo del diagrama entálpico de una reacción dada, ubicando en él las distintas magnitudes energéticas puestas en juego en el proceso a estudiar: entalpía de reacción, energía de activación, etc.  
- Catalizadores.  
o Propiedades de los catalizadores. Mecanismo general de la catálisis.  
o Tipos de catálisis. Catálisis heterogénea. Catálisis homogénea. Catálisis enzimática.  
o Aplicación del uso de catalizadores en las reacciones químicas, particularizando dicho uso en algún proceso industrial o biológico de especial relevancia.

## Criterios de evaluación y estándares de aprendizaje evaluables

### Bloque 1. Dinámica de sistemas de partículas
1. Conocer qué es un sistema de partículas.  
1.1. Conoce las condiciones que debe cumplir un conjunto de partículas para poder considerarlo un sistema.  
2. Diferenciar un sistema de partículas discreto y continuo.  
2.1. Entiende las diferencias entre sistemas de partículas continuos y discretos.  
3. Comprender el concepto de centro de masas.  
3.1. Comprende la importancia de la aproximación del centro de masas de un sistema de partículas y su utilidad.  
3.2. Diferencia claramente las fuerzas internas y las fuerzas externas a un sistema de partículas, su importancia y cómo influyen en el estado físico del sistema.  
4. Saber calcular la posición del centro de masas en sistemas discretos y en sistemas continuos sencillos.  
4.1. Calcula correctamente la posición del centro de masas de sistemas discretos.  
5. Aplicar las ecuaciones asociadas al centro de masas para la resolución de problemas y cuestiones teóricas similares a los resueltos para la dinámica de la partícula.  
5.1. Sabe analizar el movimiento del centro de masas y aplicar a dicho movimiento sus conocimientos sobre la cinemática de una masa puntual.  
5.2. Es capaz de establecer una relación entre la dinámica del centro de masas y la dinámica de una masa puntual.  
5.3. Calcula correctamente la posición del centro de masas de sistemas continuos de geometría sencilla.  
5.4. Aplica las ecuaciones y teoremas estudiados a la resolución de problemas.

### Bloque 2. Dinámica de rotación
1. Diferenciar claramente los movimientos de traslación y rotación.  
1.1. Diferencia claramente un movimiento de traslación de uno de rotación.  
1.2. Cita ejemplos de movimientos compuestos por una rotación y una traslación y comprende que su estudio se plantea como una composición de ambos.  
2. Conocer la aproximación del sólido rígido.  
2.1. Sabe en qué casos se puede aplicar la aproximación del sólido rígido y lo que implica.  
3. Definir las magnitudes asociadas al movimiento de rotación (momento de inercia, momento de una fuerza, momento angular) relacionándolas con las magnitudes angulares estudiadas en cinemática.  
3.1. Entiende el concepto de momento de inercia, su relación con la masa y sabe calcularlo para sistemas discretos.  
4. Relacionar entre si las magnitudes asociadas a un movimiento de traslación y las correspondientes a uno de rotación.  
4.1. Establece las analogías y diferencias entre las magnitudes asociadas a un movimiento de rotación y uno de traslación.  
5. Conocer la ecuación fundamental de la dinámica de rotación y los teoremas asociados al momento angular.  
5.1. Conoce el concepto de momento angular, su definición y su importancia en el estudio de la rotación.  
6. Aplicar correctamente las ecuaciones y teoremas a la resolución de problemas y cuestiones teóricas.  
6.1. Aplica los conceptos, ecuaciones y teoremas en el razonamiento de cuestiones teóricas y la resolución de problemas.

### Bloque 3. Estructura atómica
1. Considerar el desarrollo histórico del conocimiento del átomo, analizando en profundidad el modelo de Bohr y el concepto de cuantización, y conocer la estructura del átomo.  
1.1. Conoce la composición del átomo y entiende los modelos de Thomson, Rutherford y Bohr, así como la hipótesis de Planck, la hipótesis de De Broglie, el principio de Heisenberg y el concepto de cuantización.  
1.2. Toma de conciencia del valor del método científico como manera de trabajar rigurosa y sistemática, útil no solo en el ámbito de las ciencias.  
1.3. Reflexiona críticamente sobre cómo los conocimientos científicos aceptados en un momento de la historia pueden ser desechados o modificados posteriormente a la luz de nuevos descubrimientos o interpretaciones.  
2. Comprender la radiactividad natural y valorar sus aplicaciones y peligros. Radiactividad artificial y aplicaciones: reacciones nucleares, fisión nuclear, fusión nuclear y aplicaciones de la radiactividad.  
2.1. Entiende el concepto de radiactividad y conoce las radiaciones alfa, beta y gamma.  
2.2. Adquiere interés por conocer los riesgos que conlleva la exposición a fuentes de radiación, tales como la gamma.  
2.3. Evalúa de forma crítica la utilización que de la ciencia hace la sociedad, siendo consciente de los beneficios que reporta su buen uso y de los graves perjuicios que al medio ambiente y a la humanidad puede causar el uso indebido de los avances científicos.  
3. Entender el logro que ha supuesto la clasificación periódica de los elementos químicos y relacionarla con su estructura electrónica.  
3.1. Conoce el desarrollo histórico de la clasificación de los elementos y la estructura del Sistema Periódico actual.  
3.2. Analiza el papel de los sucesivos descubrimientos de elementos a lo largo de la historia y de las aplicaciones que de ellos se han venido haciendo.  
4. Obtener la información que recoge el Sistema Periódico a partir de la posición que ocupa un elemento en él.  
4.1. Obtiene la configuración electrónica de un elemento y la asocia a su posición en la tabla. Entiende cómo varían las propiedades de los elementos en función de su posición en el Sistema Periódico.

### Bloque 4. Enlace
1. Comprender los mecanismos que permiten que los átomos se unan para dar lugar a estructuras superiores y las propiedades asociadas a los diferentes tipos de enlaces.  
1.1. Comprende la pérdida de energía que acompaña la formación del enlace y su relación con la estabilidad.  
1.2. Entiende las características del enlace iónico, covalente y metálico y las propiedades de los compuestos correspondientes.  
2. Comprender algunos mecanismos que posibilitan que las moléculas se unan para formar estructuras macroscópicas.  
2.1. Comprende los enlaces intermoleculares: puentes de hidrógeno y fuerzas de Van der Waals.  
3. Predecir el tipo de enlace que se espera de dos elementos químicos en función del lugar que ocupan en el Sistema Periódico.  
3.1. Relaciona el tipo de enlace que corresponde a dos elementos dados con su electronegatividad.  
4. Aproximarse a la estructura de una molécula y a su fórmula química a partir de la configuración electrónica de los átomos que la forman.  
4.1. Obtiene una primera aproximación de la estructura electrónica de una molécula, conociendo la configuración de sus átomos constituyentes y aplicando la regla del octeto. 
4.2. Valoración del conocimiento de la estructura de la materia para la mejor comprensión de la naturaleza.  
4.3. Toma de conciencia del logro intelectual que ha supuesto conocer la geometría de numerosas moléculas y la explicación de sus mecanismos de unión.

### Bloque 5. Reacciones redox y electroquímica
1. Comprender los principales conceptos en las reacciones de oxidación-reducción y relacionarlos connumerosos procesos que ocurren en nuestra vida diaria.1.1. Calcula números de oxidación para los átomos que intervienen en un proceso redox dado.  
1.2. Identifica reacciones de oxidación y de reducción en procesos que puedan tener diversasaplicaciones en la sociedad.  
1.3. Interés por conocer y estudiar algunos procesos redox de nuestra vida diaria, y su importancia,tanto a nivel biológico como industrial, en beneficio de la sociedad.1.4. Valoración del impacto medioambiental que puede producir un mal uso de sustancias oxidantes oreductoras.  
2. Ajustar ecuaciones o procesos redox utilizando los métodos más usuales, principalmente el método del ionelectrón.  
2.1. Resuelve ejercicios de ajuste estequiométrico en procesos redox que transcurran en medio ácido.  
2.2. Resuelve ejercicios de ajuste estequiométrico en procesos redox que transcurran en medio básico.  
3. Plantear alguna experiencia sencilla de laboratorio donde tenga lugar un proceso redox y encontrar alguna aplicación práctica de interés.  
3.1. Calcula la concentración de una disolución mediante una volumetría redox.  
4. Entender los fenómenos de electrólisis y sus aplicaciones en la sociedad.  
4.1 Resuelve ejercicios y problemas relativos a fenómenos de electrólisis.  
5. Realizar experiencias sencillas de laboratorio.  
5.1. Valora la pulcritud y el rigor en el trabajo, tanto de laboratorio como teórico.

### Bloque 6. Cinética química
1. Conocer y explicar los principales conceptos cinéticos, destacando la ausencia de relación entre los aspectos energéticos y los cinéticos.  
1.1. Resuelve ejercicios y problemas sencillos derivados del cálculo de las magnitudes cinéticas fundamentales en una reacción química dada.  
2. Comprender las ideas fundamentales acerca de la teoría de colisiones.  
2.1. Aplica la teoría de colisiones a una reacción química dada.  
3. Conocer y comprender los distintos factores que inciden en la velocidad de una reacción química.  
3.1. Justifica cómo afecta a la velocidad de una reacción la variación de diversos factores, tales como la temperatura, la concentración, etc.4. Valorar la importancia que tienen los catalizadores en la cinética de una reacción dada.  
4.1. Conoce los tipos de catalizadores y cómo modifican la velocidad de un proceso químico dado.  
4.2. Valora la importancia que tienen los catalizadores en diversos procesos industriales y biológicos.  
5. Realizar experiencias sencillas de laboratorio.  
5.1. Relaciona los diferentes factores que afectan a la velocidad de una reacción.  
 
