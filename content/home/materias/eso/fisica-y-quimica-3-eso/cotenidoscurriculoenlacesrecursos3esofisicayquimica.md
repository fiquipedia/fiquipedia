
# Contenidos currículo con enlaces a recursos: 3º ESO Física y Química (LOE)

Esta página está en construcción, es solamente un primer borrador. Pretende conseguir uno de los objetivos puestos en la página inicial de la web.  
Para ver el currículo completo "estático", mirar  [Currículo 3º ESO Física y Química](/home/materias/eso/fisica-y-quimica-3-eso/curriculofisicayquimica-3eso)   

### Bloque 1. Introducción a la metodología científica.
   * Utilización de estrategias propias del trabajo científico como el planteamiento de problemas y discusión de su interés, la formulación y puesta a prueba de hipótesis y la interpretación de los resultados. El informe científico. Análisis de datos organizados en tablas y gráficos.
   * Búsqueda y selección de información de carácter científico utilizando las tecnologías de la información y comunicación y otras fuentes.
   * Interpretación de información de carácter científico y utilización de dicha información para formarse una opinión propia, expresarse con precisión y argumentar sobre problemas relacionados con la naturaleza. La notación científica.
   * Valoración de las aportaciones de las ciencias de la naturaleza para dar respuesta a las necesidades de los seres humanos y mejorar las condiciones de su existencia, así como para apreciar y disfrutar de la diversidad natural y cultural, participando en su conservación, protección y mejora.
   * Utilización correcta de los materiales, sustancias e instrumentos básicos de un laboratorio. Carácter aproximado de la medida. Sistema internacional de unidades. El respeto por las normas de seguridad en el laboratorio.

### Bloque 2. Energía y electricidad.

#### El concepto de energía.
   * Energías tradicionales.
   * Energías alternativas.
   * Fuentes de energía renovables.
   * Conservación y degradación de la energía.

#### Electricidad.
   * Fenómenos electrostáticos.
   * Las cargas eléctricas y su interacción: las fuerzas eléctricas.
   * Campo eléctrico. Flujo de cargas. Conductores y aislantes.
   * La energía eléctrica. Generadores, resistores y corriente eléctrica. Circuitos eléctricos sencillos.
   * La electricidad en casa. El ahorro energético

### Bloque 3. Diversidad y unidad de estructura de la materia.

#### La materia, elementos y compuestos.
   * La materia y sus estados de agregación: sólido, líquido y gaseoso.
   *  [Teoría cinética](/home/recursos/recursos-teoria-cinetica)  y cambios de estado.
   * Sustancias puras y mezclas. Métodos de separación de mezclas. Disoluciones. Sustancias simples y compuestas.

#### Átomos, moléculas y cristales.
   * Estructura atómica: partículas constituyentes.
   * Utilización de modelos.
   * Número atómico.
   * Introducción al concepto de elemento químico.
   * Uniones entre átomos: moléculas y cristales.
   *  [Fórmulas y nomenclatura de las sustancias más corrientes según las normas de la IUPAC](/home/recursos/quimica/formulacion) .
   * Masas atómicas y moleculares. Isótopos: concepto y aplicaciones.

### Bloque 4. Los cambios químicos y sus aplicaciones.

#### Las reacciones químicas.
   * Perspectivas macroscópica y atómico-molecular de los procesos químicos.
   * Representación simbólica.
   * Concepto de mol.
   * Ecuaciones químicas y su ajuste.
   * Conservación de la masa.
   * Cálculos de masa en reacciones químicas sencillas.
   * Realización experimental de algunos cambios químicos

#### La química en la sociedad.

   * Elementos químicos básicos en los seres vivos.
   * La química y el medioambiente: efecto invernadero, lluvia ácida, destrucción de la capa de ozono, contaminación de aguas y tierras.
   * Petróleo y derivados.
   * Energía nuclear.
   * Medicamentos.

