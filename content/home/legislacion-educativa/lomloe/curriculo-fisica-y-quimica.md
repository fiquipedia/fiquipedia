# Currículo LOMLOE Física y Química

Creo una página para ir recopilando información centrada en Ciencias / Física y Química, ver lo general en [Currículo LOMLOE](/home/legislacion-educativa/lomloe/curriculo)  

Hay una parte general sobre Física y Química asociada a LOMLOE que coloco inicialmente en [currículo Física y Química ESO](/home/materias/eso/curriculo-fisica-y-quimica-eso). Desde esa página se enlaza lo que se separa para cada curso:   
*  [Currículo Física y Química 2º ESO (LOMLOE)](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso-lomloe)  
*  [Currículo Física y Química 3º ESO (LOMLOE)](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomloe)  
*  [Currículo Física y Química 4º ESO (LOMLOE)](/home/materias/eso/fisicayquimica-4eso/curriculo-fisica-y-quimica-4-eso-lomloe)  

## Marzo 2021  
[PERFIL DE SALIDA DEL ALUMNADO AL TÉRMINO DE LA EDUCACIÓN BÁSICA, DOCUMENTO BASE MARCO CURRICULAR DE LA LOMLOE](https://www.magisnet.com/wp-content/uploads/2021/03/Perfil-de-salida-del-alumnado-al-te%CC%81rmino-de-la-Educacio%CC%81n-Ba%CC%81sica.pdf)  

Ver apartado 5.1.3. COMPETENCIA MATEMÁTICA Y COMPETENCIA EN CIENCIA, TECNOLOGÍA E INGENIERÍA (STEM)  

## Agosto 2021  
Se comparten borradores de currículo primaria (borradores dados a las CCAA, inspección, sindicatos), pendiente de trámite de audiencia
[twitter USIE_IE/status/1425733805176930306](https://twitter.com/USIE_IE/status/1425733805176930306)  
@USIE_IE pone al servicio de la comunidad educativa los borradores LOMLOE de enseñanzas mínimas de infantil y primaria del @educaciongob
[Carpeta drive (dos subcarpetas INFANTIL y PRIMARIA)](https://drive.google.com/drive/folders/1HBuk6vlt-esE9HsKFYwASqcDWFv1zWbn)  
En PRIMARIA asociado a Física y Química (enlaza con [Recursos adaptaciones curriculares Física y Química](/home/recursos/recursos-adaptaciones-curriculares-fisica-quimica))  
está [Conocimiento del medio](https://drive.google.com/file/d/1Ku76hp3oze0607CwDAP0cjROINmDefDG/view?usp=sharing)  

>**PRIMER CICLO**  
Saberes básicos  
A. Cultura científica  
1\. Iniciación a la actividad científica  
Técnicas de indagación adecuadas a las necesidades de la investigación (observación en el tiempo, identificación y clasificación, búsqueda de patrones...).  
Instrumentos y dispositivos apropiados para realizar observaciones y mediciones de acuerdo a las necesidades de las diferentes investigaciones.  
Vocabulario científico básico relacionado con las diferentes investigaciones.  
La ciencia y la tecnología como actividades humanas, similitudes y diferencias en las profesiones relacionadas con la ciencia y la tecnología desde una perspectiva de género.  
El conocimiento científico presente en la vida cotidiana.  
...   
3\. Materia, fuerzas y energía  
La luz y el sonido como formas de energía cercanas que se pueden sentir. Fuentes y uso en la vida cotidiana.
Propiedades observables de los materiales, su procedencia y su uso en objetos de la vida cotidiana de acuerdo a las necesidades de diseño para los que fueron fabricados.  
Las sustancias puras y las mezclas: distinguir y separar mezclas heterogéneas mediante distintos medios.  
Estructuras resistentes, estables y útiles. Efecto de las fuerzas de carga sobre estructuras construidas con materiales de uso común en el aula con diferente sección y geometría.  

>**SEGUNDO CICLO**  
A. Cultura científica  
1\. Iniciación a la actividad científica  
Técnicas de indagación adecuadas a las necesidades de la investigación (observación en el tiempo, identificación y clasificación, búsqueda de patrones, creación de modelos, investigación a través de búsqueda de información, experimentos con control de variables...).  
Instrumentos y dispositivos apropiados para realizar observaciones y mediciones precisas de acuerdo a las necesidades de la investigación.
Vocabulario científico básico relacionado con las diferentes investigaciones.  
Avances en el pasado relacionados con la ciencia y la tecnología que han contribuido a transformar nuestra sociedad para mostrar modelos
desde una perspectiva de género.  
La importancia del uso de la ciencia y la tecnología para ayudar a comprender las causas de las propias acciones, tomar decisiones razonadas y realizar tareas de forma más eficiente.  
...  
3\. Materia, fuerzas y energía
El calor. Fuentes y formas de producción. Cambios de estado, materiales conductores y aislantes, instrumentos de medición y
aplicaciones en la vida cotidiana.  
Los cambios reversibles e irreversibles que experimenta la materia desde un estado inicial a uno final para reconocer los procesos y
transformaciones que ocurren en la materia en situaciones de la vida cotidiana.  
Fuerzas de contacto y a distancia. Efecto sobre los objetos dependiendo de su tamaño, masa, forma o la interacción de dos objetos.  
Propiedades de las máquinas simples y su efecto sobre las fuerzas.  
Aplicaciones y usos en la naturaleza y en la vida cotidiana.  

>**TERCER CICLO**
A. Cultura científica
1\. Iniciación a la actividad científica  
Fases de la investigación científica (observación, formulación de preguntas y predicciones, planificación y realización de experimentos,
recogida y análisis de información y datos, comunicación de resultados...).  
Instrumentos y dispositivos apropiados para realizar observaciones y mediciones precisas de acuerdo con las necesidades de la investigación.  
 Vocabulario científico básico relacionado con las diferentes investigaciones.  
 La ciencia, la tecnología y la ingeniería como actividades humanas, las profesiones STEM en la actualidad desde una perspectiva de género.
 La relación entre los avances en matemáticas, ciencia, ingeniería y tecnología para comprender la evolución de la sociedad en el ámbito
científico-tecnológico.  
...
3\. Materia, fuerzas y energía  
 Masa y volumen. Instrumentos para calcular la masa y la capacidad de un objeto. Concepto de densidad y su relación con la flotabilidad de un objeto en un líquido.  
La energía eléctrica. Fuentes, transformaciones, transferencia y uso en la vida cotidiana. Los circuitos eléctricos y las estructuras robotizadas.  
 Las formas de energía, fuentes y las transformaciones. Las fuentes de energías renovables y no renovables y su influencia en la contribución al desarrollo sostenible de la sociedad.  
 Principios básicos de aerodinámica. Las propiedades del aire, la interacción del aire con los objetos dependiendo de su forma y las
fuerzas implicadas en el vuelo.  

> Pendiente revisar currículo primaria con BOE, y comentarios, enlaza con adaptaciones  
[twitter germanrosm/status/1518706878464806912](https://twitter.com/germanrosm/status/1518706878464806912)  
Ha salido el nuevo currículo de Educacion Primaria en la C. de Madrid. Como profesor de ciencias y formador de futuros maestros, triste y desolador. Hilo  
Por fin se han incluido contenidos específicos de tecnología e ingeniería como programación, robótica y diseño de ingeniería. En la linea de la competencia STEM. Muy bien pero claro, ¿de dónde sacamos el tiempo? Pues de Ciencias de la Naturaleza que total ya incluía 4 disciplinas
como son Biologia, Física, Química y Geologia. Y todo ello sin aumentar el tiempo. Y ojo, que los nuevos contenidos son aproximadamente 1/3 del total. Veamos por disciplinas.  
Geología ya había poco, ahora menos. Algo de clasificación de rocas y minerales y procesos geológicos básicos de formacion de relieve. Y listo  
Vamos con Química. Se reduce y desaparecen las reacciones químicas (habla de cambios irreversibles, podría meterse ahí pero será de soslayo en todo caso). Quedan sustancias puras y mezclas, técnicas de separación y poco más.  
De Biologia, lo siento. Siempre ha sido y sigue siendo la disciplina que más contenidos tiene, pero no sé lo que ha cambiado. Agradezco si alguien completa este punto.  
Y voy con Física que es lo mío. Hemos perdido mucho. En breve, se ha perdido sonido, luz y magnetismo. Se ha añadido un poco de fuerzas y lo que llama artefactos, en concreto nuevo el vuelo. Detallo.  
Sonido. Era un tema básico y muy chulo, se estudiaban las cualidades del sonido, el eco y se podía relacionar con la asignatura de música. Solo queda como forma de energía.  
Luz. También solo como forma de energía. Se pierde la reflexión y refracción, espejos y lentes, el arco iris. Era un tema precioso, con muchas experiencias maravillosas para sorprender, despertar la curiosidad y enlazar con las artes visuales. Muy triste  
Magnetismo. Todo fuera. Nada de imanes, ni magnetismo terrestre, brújulas... No te cuento ya inducción o electroimanes como antes. Un tema maravilloso que explica la mayor parte de la tecnología moderna.  
Perdemos luz y sonido que conectaban con artes y magnetismo que lo hace con la ingeniería. Genial para fomentar STEAM....  
Quedan máquinas simples, energía, calor, electricidad y circuitos, cambios de estado, densidad y flotación. Como decía se añade un poco de fuerzas y lo del vuelo.  
Vaya visión de la Física. Si encima en secundaria seguimos casi solo dando Mecánica no me extraña la enorme ignorancia científica y las pocas actitudes positivas que seguiremos generando.  
Muy pobre. Y ya que me caliento... ¿Por qué con 4 disciplinas más ahora la tecnología tenemos las mismas horas que Sociales (que incluye solo 2, geografía e historia), Educación Física o artes? Y ya me pongo a tope si veo que hasta Religión tiene las mismas horas.  
Venga algo bueno. Parece que se insiste más que antes en la experimentación y la indagación. Aunque sigue hablando de método  científico en singular (esto va por ti @inigorodriguez2). Aunque esto ya estaba y luego nunca se hace casi nada.

## 29 septiembre 2021

[El Ministerio continúa avanzando con las comunidades autónomas en el desarrollo de la Ley de Educación - educacionyfp.gob.es](https://www.educacionyfp.gob.es/prensa/actualidad/2021/09/20210929-comisioneducacion.html)  

Incluye documento de 6 páginas donde se ven materias ESO y Bachillerato con las modalidades de Bachillerato  

[PROPUESTA DE ORDENACIÓN ACADÉMICA DE LAS ETAPAS DE EDUCACIÓN INFANTIL, PRIMARIA, SECUNDARIA OBLIGATORIA Y BACHILLERATO.](https://www.educacionyfp.gob.es/dam/jcr:bd062b93-7f89-4643-a1d7-c304c23453de/propuesta-de-ordenaci-n-acad-mica-cuadros.pdf)  (6 páginas)  

Esto es relevante respecto a la optatividad:  
En LOMLOE pasa a ser posible cursar Bachillerato de Ciencias sin cursar Física y Química en 1º ni Física ni Química en 2º.  

## 11 octubre 2021
[Igualdad y Educación afectiva y sexual se trabajarán en todas las materias de la ESO - magisnet.com](https://www.magisnet.com/2021/10/igualdad-y-educacion-afectiva-y-sexual-se-trabajaran-en-todas-las-materias-de-la-eso/)  
Se incluyen borradores del currículo ESO  

## 29 octubre 2021
Se publican documentos borradores oficiales por el ministerio

Inicialmente ponía aquí lo asociado a Física y Química, pero para no hacer crecer demasiado esta página lo muevo a páginas separadas por materias  
*  [Currículo Física y Química ESO (incluye LOMLOE)](/home/materias/eso/curriculo-fisica-y-quimica-eso)  
*  [Currículo Física y Química 3º ESO (LOMLOE)](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomloe)  
*  [Currículo Física y Química 4º ESO (LOMLOE)](/home/materias/eso/fisicayquimica-4eso/curriculo-fisica-y-quimica-4-eso-lomloe)  

## 28 octubre 2021

Se filtran borradores Bachillerato

[El título de Bachillerato podrá obtenerse con un suspenso y hacerlo en tres años - magisnet.com](https://www.magisnet.com/2021/10/el-titulo-de-bachillerato-podra-obtenerse-con-un-suspenso-y-hacerlo-en-tres-anos/)  

[DOCUMENTO I: Proyecto de Real Decreto de Bachillerato (PDF)](https://www.magisnet.com/wp-content/uploads/2021/10/21.10.26-Proyecto-RD-Bachillerato-1.pdf)  

[DOCUMENTO II: Anexos Bachillerato (PDF)](https://www.magisnet.com/wp-content/uploads/2021/10/21.10.26-Anexos-Bachillerato.pdf)  

## 29 octubre 2021 
Se publica documentación oficial por ministerio, incluyendo borradores.

[CURRÍCULO > Debate público sobre el currículo > Documentación del Ministerio para el debate público](https://educagob.educacionyfp.gob.es/curriculo/debate-curriculo/documentacion-debate.html)  

Inicialmente ponía aquí lo asociado a Física y Química, para no hacer crecer demasiado esta página lo muevo a páginas separadas por materias.
* [Currículo Física y Química 1º Bachillerato (LOMLOE)](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomloe)  
* [Currículo Física 2º Bachillerato (LOMLOE)](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomloe)  
* [Currículo Química 2º Bachillerato (LOMLOE)](/home/materias/bachillerato/quimica-2bachillerato/curriculo-quimica-2-bachillerato-lomloe)  
* [Currículo Ciencias Generales 2º Bachillerato](/home/materias/bachillerato/ciencias-generales-2-bachillerato/curriculo-ciencias-generales-2-bachillerato-lomloe)  

## 11 noviembre 2021
Trámite de audiencia del currículo de ESO
[Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/abiertos/2021/prd-ordenacion-ensenanzas-minimas-eso.html)  

[Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria (PDF)](https://www.educacionyfp.gob.es/dam/jcr:a8262050-698f-4929-ba04-751ec36ad89e/prd-ordenacion-eso-con-anexos.pdf)  
 
## 9 diciembre 2021
Trámite de audiencia del currículo de Bachillerato
[Proyecto de Real Decreto por el que se establece la ordenación y las enseñanzas mínimas del Bachillerato](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/abiertos/2021/prd-ensenanzas-minimas-bachillerato.html)  

[Proyecto de real decreto por el que se establece la ordenación y las enseñanzas mínimas del Bachillerato (PDF)](https://www.educacionyfp.gob.es/dam/jcr:e72aadd9-a4bc-4f00-985e-7167a254425b/prd-ensenanzas-minimas-bachillerato.pdf)  

## 30 marzo 2022
[Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975)  

## 6 abril 2022
[Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521)  

## Comunidad de Madrid  

Surgen borradores en abril 2022, pero no los incluyo hasta que no sean más oficiales con trámite de audiencia  

### 23 mayo 2022
Circular de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial sobre la ordenación y la organización de las enseñanzas de educación secundaria obligatoria en el curso académico 2022-2023  
Documento firmado 23 mayo 2022  
Descargable con CSV 1259225377451556959320  
Se fija carga horaria: en 3º ESO se indican 3 horas para Física y Química  

## Junio 2022 
[Física y Química en la LOMLOE: Una mirada al nuevo currículo de ESO y Bachillerato (Luis Moreno, Almudena de la Fuente y Alejandro Rodríguez‐Villamil), 4-14, Boletín informativo del grupo especializado en Didáctica e Historia de la Física y la Química, común a las Reales Sociedades Españolas de Física y de Química, Nº 37, Junio 2022](https://gedh.rseq.org/wp-content/uploads/2022/06/Boletin-37-GEDH-red.pdf)  



## Diversificación curricular y ámbitos
Lo comento dentro de [currículo LOMLOE concepto y estructura](/home/legislacion-educativa/lomloe/curriculo-concepto-estructura)  
Física y Química queda dentro del ámbito científico-tecnológico, así como la posibilidad de integrar todas las materias en ámbitos en los 3 primeros cursos de ESO.  
	
## Comparación de contenidos LOE/LOMCE vs saberes básicos LOE/LOMLOE

### ESO 
Se puede hacer una asociación de bloques de contenidos LOE/LOMCE vs saberes básicos LOE/LOMLOE.  
Que aparezcan en cierto orden en el texto de normativa no indican que ese sea el orden en el que deben ser tratados.   

| **Bloques de contenidos LOE/LOMCE** | **Saberes básicos LOE/LOMLOE** |
|:-|:-|
| Bloque 1. La actividad científica | A. Las destrezas científicas básicas |
| Bloque 2. La materia | B. La materia |
| Bloque **3.** Los cambios | **E.** El cambio |
| Bloque 4. El movimiento y las fuerzas | D. La interacción |
| Bloque **5.** La energía | **C.** La energía |

Si dentro de cada bloque comparamos 

(LOE contenidos + criterios (+ estándares LOMCE))  
vs   
(saberes básicos + criterios = competencias específicas + descriptores de perfil de salida + competencias clave )   

opino que lo único concreto en LOMLOE son los saberes básicos, ya que resto es demasiado generalizable.  
Por ejemplo de lo más concreto es la competencia específica 3.2  

>3.2. Utilizar adecuadamente las **reglas básicas de la física y la química**, incluyendo el **uso de unidades de medida, las herramientas matemáticas** y las **reglas de formulación y nomenclatura**, para facilitar una comunicación efectiva con toda la comunidad científica.  

Esa competencia 3.2 se puede asumir que incluye el manejo de expresiones de cinemática, dinámica y estequiometría, pero tal y como está readactada, combinada con cómo está redactado el saber básico, son tan pocos detallados que no concretan realmente nada. La concreción queda en mano de CCAA, centro y docente, y sin bajar a la concreción (como sí hacían LOE/LOMCE) no veo posible realmente la comparación. Por ejemplo la diferencia de concreción en orgánica en 4º ESO impide la comparación.   

### Comparación currículos

Lo realizo por materias.  
Me centro en contenidos/saberes básicos.  
* [Comparación currículo Física y Química 3º ESO](/home/materias/eso/fisica-y-quimica-3-eso/comparacion-curriculo-3-eso-fisica-y-quimica)
* [Comparación currículo Física y Química 4º ESO](/home/materias/eso/fisicayquimica-4eso/comparacion-curriculo-fisica-y-quimica-4-eso)
* [comparación currículo Física y Química 1º Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/comparacion-curriculo-fisica-y-quimica-1-bachillerato) 
* [comparación currículo Física 2º Bachillerato](/home/materias/bachillerato/fisica-2bachillerato/comparacion-curriculo-fisica-2-bachillerato)
* [comparación currículo Quimica 2º Bachillerato](/home/materias/bachillerato/quimica-2bachillerato/comparacion-curriculo-quimica-2-bachillerato)

### Coordinación entre materias

La Física y Química necesita coordinación con otras materias, especialmente matemáticas. Ciertos contenidos necesitan como requisito conocer conceptos matemáticos, por lo que si no hay coordinación, a veces surge que en Física y Química "impartamos matemáticas / más bien demos recetas básicas de matemáticas para poder abordar el concepto físico que usa ese elemento matemático", por una combinación de andar justos de tiempo y de no ser especialistas en didáctica de matemáticas.  

Cito algunos  

#### Vectores  
Necesario para manejar magnitudes vectoriales: fuerzas con LOMCE se introducían en FQ2ESO.  
Además se necesita manejar el productor escalar (para el trabajo) y el producto escalar (para momento angular y para fuerza de Lorentz en campo magnético)   
En LOMCE se forzó poner leyes de Kepler y conservación de momento angular en FQ1Bach, cuando el producto vectorial estaba en 2ºBachillerato, lo que no tiene sentido. Me consta que la RSEF informó de la inconsistencia y solicitó que se corrigiese, pero no se modificó.  

LOMLOE:   
ESO:  
No se cita en matemáticas.  
Se cita solo en FQ4ESO
> Uso del álgebra vectorial básica para la realización gráfica y numérica de
operaciones con fuerzas ...

Bachillerato:   
Se cita explícitamente en FQ1 (aparte de citar magnitudes vectoriales como momento lineal e impulso mecánico)  
> Para alcanzar un nivel de significación mayor en el aprendizaje con 
respecto a la etapa anterior, en este curso se trabaja desde un enfoque vectorial, 
de  modo  que  la  carga  matemática  de  esta  unidad  se  vaya  adecuando  a  los 
requerimientos del desarrollo madurativo de los y las adolescentes.  
>  Aprovechando el estudio vectorial 
del bloque anterior, el alumnado aplica esta herramienta a describir los efectos 
de las fuerzas sobre partículas y sobre sólidos rígidos...   
> Predicción, a partir de la composición vectorial ...   
> Relación de la mecánica vectorial ...   

Se cita en Matemáticas I    
> Estrategias de comprensión de las propiedades y las representaciones 
de la adición y el producto escalar de vectores.   
> Desarrollo  de  destrezas  para  operar  con  números  reales  y  vectores, 
utilizando  el  cálculo  mental  o  escrito  en  los  casos  sencillos  y  de 
herramientas tecnológicas en los casos más complicados.   
> Estrategias  de  comprensión  de  los  vectores  como  estructuras  que 
poseen algunas de las propiedades de los números reales.   
> Modelización  de  la  posición  y  el  movimiento  de  un  objeto  en  el  plano 
utilizando vectores.   
Se cita en Matemáticas II   
> Comprensión y uso adecuado de las propiedades y las 
representaciones de la adición y la multiplicación de vectores y matrices.   
> Desarrollo  de  destrezas  para  operar  con  números  reales,  vectores  y 
matrices, utilizando el cálculo mental o escrito en los casos sencillos y 
de herramientas tecnológicas en los casos más complicados.   
> Estrategias de comprensión de los vectores y las matrices como 
estructuras que poseen algunas de las propiedades de los números 
reales.  
> Modelización de la posición y el movimiento de un objeto en el espacio 
utilizando vectores.   

Se cita en FIS2  
> Representación y tratamiento vectorial del efecto que una masa...  
> Tratamiento vectorial de los campos eléctrico y magnético ...

#### Trigonometría  
Necesaria para descomposición vectores. Planos inclinados con LOMCE se veían en FQ4ESO, tiro parabólico en FQ1Bach  

Se cita en MATEMÁTICAS B  
> Reconocimiento de las razones trigonométricas de un ángulo agudo.  
> Utilización de las razones trigonométricas y sus relaciones en la
resolución de problemas.  

#### Derivación  
Necesaria para obtener velocidad a partir de posición y aceleración a partir de velocidad.  Se trata en FQ1Bach  

Se cita en Matemáticas Generales  
> Construcción del concepto de derivada a partir  del estudio  del cambio 
en diferentes contextos.   
> Interpretación de la derivada como función pendiente y como razón de 
cambio en contextos concretos y con medios tecnológicos.   
> Aplicación de los conceptos de límite y derivada a la representación y al 
estudio de situaciones susceptibles de ser tratadas mediante las 
funciones.  

Se cita en Matemáticas Aplicadas a las Ciencias Sociales I  
> Construcción del concepto de derivada a partir  del estudio del cambio 
en diferentes contextos.   


Se cita en Matemáticas II   
> Aplicación de los conceptos de límite, continuidad y derivabilidad, a la 
representación y al estudio de situaciones susceptibles de ser 
modelizadas mediante funciones.   

Se cita en Matemáticas aplicadas a las Ciencias Sociales II  
> La  derivada  como  razón  de  cambio  en  resolución  de  problemas  de 
optimización en contextos diversos.   
> Aplicación de los conceptos de límite y derivada a la representación y al 
estudio de situaciones susceptibles de ser modelizadas mediante 
funciones.  

#### Integración 
Necesario para manejar correctamente trabajo como integral y obtener expresiones de energía potencial. Se utiliza con ley de Gauss y ley de Ampère.    

Se cita en Matemáticas II   
> Interpretación de la integral definida como el área bajo una curva.   
> Técnicas elementales para el cálculo de primitivas. Aplicación al cálculo 
de áreas.   
> Técnicas para la aplicación del concepto de integral a la resolución de 
problemas que impliquen cálculo de superficies planas o volúmenes de 
revolución.  

Se cita en Matemáticas aplicadas a las Ciencias Sociales II  
> Interpretación de la integral definida como el área bajo una curva.   
> Técnicas elementales para el cálculo de primitivas. Aplicación al cálculo 
de áreas.   

#### Logaritmos   
Se utilizan en Física 2º Bachillerato para sonido (dB) y en Química 2º Bachillerato (pH)  
Normalmente cuando se utilizan ya se han visto hace tiempo.   

Se cita en MATEMÁTICAS B
> Interpretación de la relación entre dos variables, valorando
gráficamente con herramientas tecnológicas la pertinencia de una
regresión lineal, cuadrática, potencial, exponencial o logarítmica.  


## Mi opinión hasta ahora

### ESO 
Me parece que tan poca concreción es peligrosa, como comento en [Currículo LOMLOE concepto y estructura](/home/legislacion-educativa/lomloe/curriculo-concepto-estructura) y creo que las [situaciones de aprendizaje](/home/legislacion-educativa/lomloe/curriculo-situaciones-de-aprendizaje) y la concreación de Bachillerato pueden ayudar a aclarar ideas, pero en Física y Química veo llamativo:   

- Hablar de química orgánica sin nombrar ningún grupo funcional ni acotar la profundidad es absurdo. De las pocas concreciones es citar IUPAC. La diferencia con LOMLOE en 4ºESO es brutal  
[twitter FiQuiPedia/status/1447885389461663747](https://twitter.com/FiQuiPedia/status/1447885389461663747)  
- En 4º ESO se habla interacción (cinemática y dinámica) sin citar Newton y que no se cite lo veo hasta forzado, aunque sí aparecen citadas "leyes de Newton" en saberes de 1º a 3º ESO.  
- Todo lo asociado a estática de fluidos queda reducido a citar la palabra "empuje"  

### Bachillerato
En este caso sí hay concreción. Habrá que ver cómo encaja el salto de indefinición ESO vs concreción Bachillerato: la llegada a Bachillerato ya era un cambio importante, pero lo puede ser más en función de cómo hayan concretado los centros donde se haya titulado 4º ESO.  

