# Recursos Cultura Científica 1º Bachillerato

En curso 2020-2021 imparto la materia usando aula virtual de EducaMadrid y aunque parto de recursos que tenía aquí, actualizo allí algunos recursos, por lo que puede que no estén en ambos sitios. Pendiente unificar.  
No imparto en curso 2021-2022, pero dejo el aula con acceso de invitado  
[Aula virtual Moodle Cultura Científica 1º Bachillerato](https://aulavirtual3.educa.madrid.org/ies.alkalanahar.alcala/course/view.php?id=190)    

## Recursos elaboración propia

Ver comentarios tras la tabla

| Bloque / Contenido |  Apuntes / Pizarras |  
|:-:|:-:|
| Bloque 2: La tierra |    [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/ApuntesxCurso/CC1/CC1-Pizarras-Bloque2-Tierra.pdf "CC1-Pizarras-Bloque2-Tierra.pdf")    |  
| Bloque 2: La vida |    [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/ApuntesxCurso/CC1/CC1-Pizarras-Bloque2-Vida.pdf "CC1-Pizarras-Bloque2-Vida.pdf")    |  
| Bloque 3: Avances en Biomedicina |    [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/ApuntesxCurso/CC1/CC1-Pizarras-Bloque3-AvancesEnBiomedicina.pdf "CC1-Pizarras-Bloque3-AvancesEnBiomedicina.pdf")    |  
| Bloque 4: La revolución genética |    [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/ApuntesxCurso/CC1/CC1-Pizarras-Bloque4-LaRevolucionGenetica.pdf "CC1-Pizarras-Bloque4-LaRevolucionGenetica.pdf")    |  
| Bloque 5: Nuevas tecnologías comunicación e información |    [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/ApuntesxCurso/CC1/CC1-Pizarras-Bloque5-NuevasTecnologiasComunicacionInformaci%C3%B3n.pdf "CC1-Pizarras-Bloque5-NuevasTecnologiasComunicacionInformaci%C3%B3n.pdf")    |  
 
Los planteo como "pizarras", ver [recursos pizarras Física y Química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel). En el curso 2016-2017 es la primera vez que imparto la materia y decido no usar libro de texto), y que se centre en los criterios de evaluación y estándares de aprendizaje evaluables del currículo (al iniciar curso 2016-2017 todavía estaban vigentes las evaluaciones finales de Bachillerato y en ese momento suponía que al llegar a 2º curso podrían elegir examinarse de esta materia de 1º: aunque deja de aplicar en diciembre de 2016, ya había iniciado así los materiales y los termino manteniendo ese planteamiento de revisar que se sigue currículo LOMCE). Los pongo en google drive ya que tienen imágenes y ocuparán mucho  
Pongo enlace a carpeta [drive.fiquipedia ApuntesxCurso CC1](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/ApuntesxCurso/CC1)  
  
  
## Recursos generales

Ver [recursos Ciencias para el Mundo Contemporáneo 1º Bachillerato (LOE) ](/home/recursos/recursos-por-materia-curso/recursos-ciencias-para-el-mundo-contemporaneo), [recursos Cultura Científica 4º ESO (LOMCE)](/home/recursos/recursos-por-materia-curso/recursos-cultura-cientifica-4-eso) y [recursos de Biología y Geología](/home/recursos/biologia-y-geologia)   

Ver [Escepticismo y pensamiento crítico](/home/recursos/escepticismo-pensamiento-critico) 

 En algunos centros, aunque no esté fijado libro de texto para uso por alumno, puede haber libros de texto de alumno que pueden servir como referencia, incluyendo libros de recursos para profesorado (por ejemplo Santillana tiene un libro de recursos, ISBN 978-84-680-3105-7). Ver en  [libros de texto](/home/recursos/libros/libros-de-texto)   
  
Se pueden ver recursos asociados a materias donde se traten conceptos: por ejemplo energía interna de la Tierra en  [Ciencias Naturales de 1º y 2º ESO](/home/materias/eso/ciencias-de-la-naturaleza-1y2eso) ,  [Biología y Geología 4º ESO](/home/materias/eso/4-eso-biologia-y-geologia) , Biología y Geología en 1º Bachillerato  

[Cultura Científica - elortegui.org](http://ciencia.elortegui.org/datos/1BACHCUC/CUC.htm)  
  
 [CULTURA+CIENTÍFICA](https://fisicayquimicaayora.wikispaces.com/CULTURA+CIENT%C3%8DFICA)   
 [La importancia social de la divulgación y la cultura científicas | El vuelo de la lechuza](https://elvuelodelalechuza.com/2017/11/04/la-importancia-social-de-la-divulgacion-y-la-cultura-cientifica/)   
  
2017, Science Bits ¿Por qué aprendemos ciencia en la escuela? (1 min 27 s)  
[![¿Por qué aprendemos ciencia en la escuela? - YouTube](https://img.youtube.com/vi/etmYwBTDpqA/0.jpg)](https://www.youtube.com/watch?v=etmYwBTDpqA)   

[TEMA 1. Resumen Cultura Científica](https://docs.google.com/document/d/1RRZxRW9qmZFqBT_xwZimKsCg-c13nc-HXJ142V-pshU/edit)  
Javier Fernandez Panadero  

## Recursos asociados a Bloque 2. La Tierra y la vida: "Estructura, formación y dinámica de la Tierra"

### Películas / vídeos
  
 Home (Yann Arthus-Bertrand, 2009). Documental disponible íntegro en youtube  
 [home-educacion](http://www.homethemovie.org/es/educacion/home-educacion)   
 [![HOME (ES) - YouTube](https://img.youtube.com/vi/SWRHxh6XepM/0.jpg)](https://www.youtube.com/watch?v=SWRHxh6XepM)   
  
 Inside Planet Earth (Brian Skilton y Martin Williams, 2009) Documental  
 [](http://www.imdb.com/title/tt1454110/)   
  
 The Core (Jon Amiel,2003). Película muy incorrecta científicamente pero se pueden usar secuencias para debatir ideas.  
 [The Core (2003) - IMDb](http://www.imdb.com/title/tt0298814/)   
  
 [El Núcleo (The Core) y el error que no fue - El profe de Física](http://elprofedefisica.naukas.com/2016/10/14/el-nucleo-the-core-y-el-error-que-no-fue/)   
  
 Ice Age: Continental Drift | Ice Age 4: Scrat Continental Crack Up HD | 20th Century FOX  
 [![Ice Age: Continental Drift | Ice Age 4: Scrat Continental Crack Up HD | Fox Family Entertainment - YouTube](https://img.youtube.com/vi/zocutif0cQY/0.jpg)](https://www.youtube.com/watch?v=zocutif0cQY)   
Corto de humor de 2 min 40 s  
  
### Artículos  
Un geofísico propone una misión no tripulada al centro de la Tierra Malen Ruiz de Elvira, El País, 4 junio 2003  
 [1054677605_850215.html](http://elpais.com/diario/2003/06/04/futuro/1054677605_850215.html) Nature 2003, Planetary science: Mission to Earth's core — a modest proposal, David J. Stevenson  
 [Mission to Earth's core — a modest proposal | Nature](http://www.nature.com/nature/journal/v423/n6937/full/423239a.html)   
 Plumbing the Earth's depths, By Ivan Noble   
 [3021255.stm](http://news.bbc.co.uk/2/hi/science/nature/3021255.stm)   
  
 El País, CIENCIAS DE LA TIERRA | Geofísica, El Norte y el Sur magnéticos tardan unos 7.000 años en intercambiarse, Antonio Fraguas, 28 abril 2004  
 [El Norte y el Sur magnéticos tardan unos 7.000 años en intercambiarse | Futuro | EL PAÍS](http://elpais.com/diario/2004/04/28/futuro/1083103202_850215.html)   
  
 Scientists Say Earth's Center Rotates Faster Than Surface, KENNETH CHANG, New York Times, August 26, 2005  
 [Scientists Say Earth&#x27;s Center Rotates Faster Than Surface - The New York Times](http://query.nytimes.com/gst/fullpage.html?res=9405E0D71E3EF935A1575BC0A9639C8B63)   
  
 Investigación y Ciencia Julio 1993 Nº 202 Donde el núcleo limita con el manto; Jeanloz, Raymond; Lay, Thorne (artículo online completo es de pago)  
 [Donde el núcleo limita con el manto | Investigación y Ciencia | Investigación y Ciencia](http://www.investigacionyciencia.es/revistas/investigacion-y-ciencia/numero/202/donde-el-ncleo-limita-con-el-manto-5045)   

[Ohio House passes bill allowing student answers to be scientifically wrong due to religion](https://local12.com/news/local/ohio-house-passes-bill-allowing-student-answers-to-be-scientifically-wrong-due-to-religion) It also bans schools from restricting students from religious expression when they complete homework assignments. While some critics have said that means students can incorrectly answer questions based on their religion, Ginter says students will still have to answer questions based on what they're taught at school.  
“Under House Bill 164, a Christian or Jewish student would not be able to say my religious texts teach me that the world is 6,000 years old, so I don't have to answer this question. They're still going to be tested in the class and they cannot ignore the class material,” said Ginter.  
  
[Google ayudará a detectar terremotos usando los sensores de tu móvil, 11 agosto 2020](https://hipertextual.com/2020/08/google-ayudara-detectar-terremotos-android) 

[Un plan audaz (y algo loco) para hacer habitable Marte - abc.es](https://www.abc.es/ciencia/abci-plan-audaz-y-algo-loco-para-hacer-posible-terraformacion-marte-202111240202_noticia.html)  

[How to create an artificial magnetosphere for Mars - sciencedirect.com](https://www.sciencedirect.com/science/article/pii/S0094576521005099)  


### Páginas web con recursos asociados
Suelen ser apuntes relacionados con geología, de otros cursos  
  
 [Proyecto Biosfera](http://recursos.cnice.mec.es/biosfera/alumno/1bachillerato/estrucinternatierra/index.htm)   
 [ http://recursos.cnice.mec.es/biosfera/alumno/4ESO/MedioNatural1I/contenido1.htm](http://recursos.cnice.mec.es/biosfera/alumno/4ESO/MedioNatural1I/contenido1.htm)   
  
 [Proyecto Biosfera](http://recursos.cnice.mec.es/biosfera/alumno/1bachillerato/estrucinternatierra/contenido2.htm)   
  
 [Tema-1-E-I-Tierra-I.htm](http://www.educa.madrid.org/web/ies.alonsoquijano.alcala/carpeta5/carpetas/quienes/departamentos/ccnn/CCNN-1-2-ESO/2eso/2ESO-12-13/Bloque-II/Tema-1-Energia-interna-Tierra-I/Tema-1-E-I-Tierra-I.htm)   
  
 El interior terrestre  
 [homeTC.asp](http://www.librosvivos.net/smtc/homeTC.asp?TemaClave=1187&idIdioma=ES)   
  
 [TECTÓNICA DE PLACAS](https://bioprofe4.blogspot.com.es/search/label/TECT%C3%93NICA%20DE%20PLACAS)   
  
 [James Ussher - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/James_Ussher)  Creación de la Tierra: «el anochecer previo al domingo 23 de octubre» (o sea, el sábado 22 de octubre a las 18:00) del 4004 a. C..  
 Expulsión de Adán y Eva del Paraíso: el lunes 10 de noviembre de 4004 a. C.  
 Final del Diluvio Universal (el arca de Noé se posa sobre el monte Ararat): el miércoles 5 de mayo del 2348 a. C.  
  
Edad de la Tierra [Así se descubrió la edad de la Tierra](https://www.lavozdegalicia.es/noticia/sociedad/2018/12/30/descubrio-edad-tierra/0003_201812H30P51991.htm)   
 [¿Cómo se determina la edad de la Tierra? - Quora](https://es.quora.com/C%C3%B3mo-se-determina-la-edad-de-la-Tierra)   

### Terraplanismo
[ La Tierra es plana, y yo tengo un dragón que ES el garaje](https://algoquedaquedecir.blogspot.com/2017/11/la-tierra-es-plana-y-yo-tengo-un-dragon.html)   

[La Tierra es plana. Aprendemos a argumentar](https://sites.google.com/view/la-tierra-es-plana/)  

[‘El movimiento terraplanista confirma la pérdida del prestigio social de la ciencia’, según el profesor Cabos de la UAH](http://portalcomunicacion.uah.es/diario-digital/reportaje/el-movimiento-terraplanista-confirma-la-perdida-del-prestigio-social-de-la-ciencia-segun-el-profesor-cabos-de-la-uah.html)   

[No puedes convencer a un terraplanista y eso debería preocuparte](https://elpais.com/elpais/2019/02/27/ciencia/1551266455_220666.html)  
Negar que la Tierra es esférica es el caso más extremo de un fenómeno que define esta época: recelar de los datos, ensalzar la subjetividad, rechazar lo que nos contradice y creer falsedades propagadas en redes  


  
## Recursos asociados a Bloque 2. La Tierra y La vida: "El origen de la vida. Teorías de la evolución. Darwinismo y genética. Evolución de los homínidos."

### Películas/vídeos
[imdb.com Espèces d'espèces (2008) Denis Van Waerebeke](http://www.imdb.com/title/tt1537761/)  

Versión doblada al castellano   
[![](https://img.youtube.com/vi/cpyQRzx5exg/0.jpg)](https://www.youtube.com/watch?v=cpyQRzx5exg "Especies en evolución")   
  
[imdb.com Curiosity: Mankind Rising (2012)](http://www.imdb.com/title/tt2914058/)   
  
[imdb.com How Life Began (2008)](http://www.imdb.com/title/tt1270091/)   
  
En youtube completo, "imagen y texto! invertidos de izquierda a derecha" (operativo en 2016)  
[![watch](https://img.youtube.com/vi/B-m1gx0s0Ro/0.jpg)](https://www.youtube.com/watch?v=B-m1gx0s0Ro)   
  
[imdb.com Quest for Fire (1981) Jean-Jacques Annaud](http://www.imdb.com/title/tt0082484)   
  
[The Clan of the Cave Bear (1986) Michael Chapman](http://www.imdb.com/title/tt0090848/)   
  
Carl Sagan explica la evolución en 8 minutos  
 [![Carl Sagan Explains Evolution - YouTube](https://img.youtube.com/vi/P0giXTjclyQ/0.jpg)](https://www.youtube.com/watch?v=P0giXTjclyQ)   
  
 "El motor flagelar bacteriano, "Complejidad irreducible" y "Designio inteligente""   
 [![watch](https://img.youtube.com/vi/Jno9DqbVDdY/0.jpg)](https://www.youtube.com/watch?v=Jno9DqbVDdY)   
Crítica al Darwinismo. El vídeo termina con "Ahora vas y lo cascas (texto) Zas, en toda la boca (audio)". Opinión personal: se citan textos de Darwin como si fueran "la biblia" de los no creacionistas/no creyentes en un ser superior que controla la creación/evolución   
  
 ¿Qué Es La Vida? (Capítulo REDES 348)   
 [![watch](https://img.youtube.com/vi/reeWIckqbUw/0.jpg)](https://www.youtube.com/watch?v=reeWIckqbUw)   
 
[twitter ScienceIsNew/status/1444720464262090753](https://twitter.com/ScienceIsNew/status/1444720464262090753)  
Climbing Perch fish 🐟  
They can survive out of water for up to 6 days! This is possible because of their labyrinth organ, a structure in the fish's head which allows it to breathe atmospheric oxygen.  

### Artículos
 Tom Wolfe: "La teoría de la evolución es un cuento", El mundo, 2016  
 [Tom Wolfe: &quot;La teoría de la evolución es un cuento&quot; | Papel | EL MUNDO](http://www.elmundo.es/papel/lideres/2016/10/30/58121c89468aebbe468b4585.html)   
  
 La historia de la Vida  
 [La historia de la vida — Cuaderno de Cultura Científica](http://culturacientifica.com/2016/10/02/la-historia-la-vida/)   
Juan Ignacio Pérez, 2016  
Incluye esta imagen  
![Árbol de familia de la vida en la Tierra. ¿Te encuentras? ](http://culturacientifica.com/app/uploads/2016/10/La-historia-de-la-vida.png "Árbol de familia de la vida en la Tierra. ¿Te encuentras? ")
  
 Enredos en la familia. La evolución humana ya no se explica como una simple cadena lineal de eslabones perdidos. La ciencia nos revela un entramado más complejo de elementos, con una mayor diversidad entre especies, Javier Sampedro, 2016  [Evolución humana: Enredos en la familia | Ciencia | EL PAÍS](http://elpais.com/elpais/2016/09/21/ciencia/1474454952_783786.html)   
  
 Un cráneo de 1,8 millones de años reabre el debate sobre las especies de homínidos, Teresa Guerrero, 2013  
 [Un crneo de 1,8 millones de aos reabre el debate sobre las especies de homnidos | Ciencia | elmundo.es](http://www.elmundo.es/elmundo/2013/10/17/ciencia/1382029806.html)   
  
 El creacionismo llega a España, 2008  
 [El creacionismo llega a España | Sociedad | EL PAÍS](http://elpais.com/diario/2008/01/10/sociedad/1199919603_850215.html)   
  
 [El origen de la vida (entrevista a Robert Shapiro)| Desde El Exilio](http://www.desdeelexilio.com/2009/01/14/el-origen-de-la-vida-entrevista-a-robert-shapiro/)   
 
 [Redo of a Famous Experiment on the Origins of Life Reveals Critical Detail Missed for Decades - 2021](https://www.scientificamerican.com/podcast/episode/redo-of-a-famous-experiment-on-the-origins-of-life-reveals-critical-detail-missed-for-decades/)  
 The Miller-Urey experiment showed that the conditions of early Earth could be simulated in a glass flask. New research finds the flask itself played an under appreciated, though outsized role.  

### Páginas web con recursos asociados
 [Becoming Human](http://www.becominghuman.org/)   
  
 Árboles filogenéticos  
 [El árbol filogenético circular](http://elrastaqueespecula.blogspot.com.es/2010/06/el-arbol-filogenetico-circular.html)   
 [Download Graphic Images from the Hillis/Bull Lab](http://www.zo.utexas.edu/faculty/antisense/DownloadfilesToL.html)   
  
## Recursos asociados a Bloque 3. Avances en Biomedicina  

 Diagnósticos y tratamientos.  
 Trasplantes.  
 La investigación farmacéutica. --> Tratar el concepto de ensayo clínico   
 Principios activos: Genéricos. --> Tratar idea de patentes en medicina   
 Sistema sanitario.  
 Medicina alternativa. --> Tratar (desmontar) la homeopatía científicamente    
  
*Un planteamiento es ver la genética antes / al tiempo enlazando la genética como asociada a enfermedades, a diagnóstico y a tratamiento*   
*No se cita explícitamente el tema de las vacunas, pero creo que es interesante citarlo junto con una visión crítica/científica del movimiento "antivacunas"*  

### Recursos sobre investigación farmacéutica
 [proceso_de_ID_de_un_medicamento2.pdf](http://www.farmaindustria.es/web/wp-content/uploads/sites/2/2014/01/proceso_de_ID_de_un_medicamento2.pdf)  cc-by-nc-sa  
  
 [I+D Archivos - FarmaIndustria](https://www.farmaindustria.es/web/area/id/) 24 abril 2020  
¿Cuánto tiempo se tarda (y por qué) en desarrollar un medicamento?  
 [repor_ID_medicamentos-2-1.pdf](https://www.farmaindustria.es/web/wp-content/uploads/sites/2/2020/04/repor_ID_medicamentos-2-1.pdf)   
 [twitter GCentinela/status/919306201724260354](https://twitter.com/GCentinela/status/919306201724260354)  
 ![](https://pbs.twimg.com/media/DMIIHDJX4AAE0_0.jpg)  
 ![](https://pbs.twimg.com/media/DMIIHDBXUAA6ta_.jpg)  
 ![](https://pbs.twimg.com/media/DMIIHDfXUAAkq-I.jpg)
  
 [twitter waltzing_piglet/status/959805490854326277](https://twitter.com/waltzing_piglet/status/959805490854326277)   
 Creo que ya está bien de tanta risa con lo de @chenoaoficial y el yogur, así que voy a romper una lanza en su defensa. #hilo  
 Porque en @el_hormiguero apenas tuvo tiempo para explicarse con claridad. A ver si después de leer esto tenéis ganas de reír.  
 Porque @chenoaoficial no hizo esas afirmaciones sobre los efectos probióticos vaginales de los yogures así, al tun tun.  
 Como sospechaba que una cantante de esa calidad no podía hacer afirmaciones tan estrafalarias sin una base, empecé a investigar.  
 Me puse en contacto con alguien cercano a la cantante (permitidme que no diga el nombre) quien me contó la verdad sobre el asunto.  
 Porque @chenoaoficial hizo un ESTUDIO CIENTÍFICO SERIO que apoyaba esas afirmaciones.  
 Para empezar, @chenoaoficial reclutó a un grupo de 351 voluntarias que sufrían con frecuencia de candidiasis. Las dividió en tres grupos.  
 A un primer grupo lo tuvo con una aplicación vaginal de yogur Hacendado mañana y noche.  
 A un segundo grupo les hizo el mismo tratamiento, pero previamente mataba las bacterias del yogur con radiación.  
 Al tercer grupo, de control, no les hizo ningún tratamiento.  
 Por supuesto, todo este estudio contó con la aprobación de un comité ético y los permisos correspondientes.  
 Tras tres meses de tratamiento, realizó un análisis estadístico exhaustivo y comprobó que el grupo 1 mejoraba respecto a los otros dos.  
 Obviamente, calculó un valor p que resultó mucho menor que 0.01, lo cual indicaba que la mejora era estadísticamente significativa.  
 Ha enviado estos resultado al British Medical Journal y está esperando respuesta.  
 Mientras tanto, y viendo la eficacia del tratamiento, se ha decidido a hacer un estudio con una muestra mucho mayor de mujeres.  
 Para ello ha pedido un proyecto al ministerio.  
 Y esto o algo similar, @chenoaoficial, es lo que hay que hacer antes de decir por la tele a millones de personas que un tratamiento es eficaz y poner en peligro su salud.  
 
[Método Kirby-Bauer - wikipedia.org](https://es.m.wikipedia.org/wiki/M%C3%A9todo_Kirby-Bauer)   
![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/KB_test.jpg/300px-KB_test.jpg)  
  
### Recursos sobre vacunas
 [Cómo desmontar científicamente los 4 principales argumentos antivacunas - Blog - ISGLOBAL](https://www.isglobal.org/healthisglobal/-/custom-blog-portlet/como-desmontar-cientificamente-los-4-principales-argumentos-antivacunas/3098670/0)   
  
 [FarmaIndustria :: El Valor de las Vacunas](http://www.farmaindustria.es/web/el-valor-de-las-vacunas/)   
  
 [Éxitos | Vacunas | Medicamentalia](http://medicamentalia.org/vaccines/exitos/)   
  
 [El impacto de la introducción de las vacunas contra enfermedades contagiosas | Microsiervos (Ciencia)](https://www.microsiervos.com/archivo/ciencia/impacto-introduccion-vacunas-contra-enfermedades-contagiosas.html)   
  
 [Anti-Vaxxers Fund Study That Finds Zero Link Between Vaccinations And Autism | IFLScience](http://www.iflscience.com/health-and-medicine/anti-vaxxers-fund-study-finds-zero-link-between-vaccinations-and-autism/)   
  
 [No link between MMR and autism, major study concludes | MMR | The Guardian](https://www.theguardian.com/society/2015/apr/21/no-link-between-mmr-and-autism-major-study-concludes)   
  
 [Autism and Vaccines | Vaccine Safety | CDC](https://www.cdc.gov/vaccinesafety/concerns/autism.html)   
 
 [twitter EricTopol/status/1332771238771630080](https://twitter.com/EricTopol/status/1332771238771630080)  
 This will go down in history as one of science and medical research's greatest achievements. Perhaps the most impressive.
I put together a preliminary timeline of some key milestones to show how several years of work were compressed into months.  
![](https://pbs.twimg.com/media/En7zQUrXYAYT_n4?format=jpg)  

[twitter Plaza_Bickle/status/1623425969452113921](https://twitter.com/Plaza_Bickle/status/1623425969452113921)  
Este mes de febrero se cumplen 25 años de uno de los fraudes científicos más conocidos de la historia reciente: la publicación en @TheLancet, luego retractada, del artículo de A. Wakefield que sugería un (falso) vínculo entre la vacuna triple vírica y el autismo. 🧵 Va #hilo  
El caso Wakefield, además de un ejemplo paradigmático de fraude científico, muestra bien cómo la desinformación se acompaña de intereses y puede generar cambios sociales relevantes, con percepciones y comportamientos infundados y potencialmente peligrosos.  
Andrew Wakefield, un médico gastroenterólogo que trabajaba en los años 90 en el Hospital Royal Free de Londres, llevaba tiempo investigando el origen y desarrollo de diversas enfermedades inflamatorias, lo que empezaba a darle ciento nombre entre la comunidad científica.  
En 1995, tres años antes del famoso Lancet fraudulento, Wakefield publicó una investigación en la que sugería una relación entre el virus del sarampión, la vacunación y un mayor riesgo de enfermedades digestivas como el Crohn y la colitis ulcerosa 👉  
Esta publicación, unida a su línea de trabajo sobre inflamación y patologías digestivas, fueron haciéndole más conocido en su entorno, hasta que llegó la publicación que marcaría su carrera y que provocó un impacto clínico y social tremendo.  
El 28 de febrero de 1998, con otros investigadores, publicó 'Ileal-lymphoid-nodular hyperplasia, non-specific colitis, and pervasive developmental disorder in children'. Parecía un paper más sobre gastroenterología, pero escondía una hipótesis preocupante.   
[RETRACTED: Ileal-lymphoid-nodular hyperplasia, non-specific colitis, and pervasive developmental disorder in children](https://www.thelancet.com/journals/lancet/article/PIIS0140673697110960/fulltext)  
  
### Recursos sobre homeopatía
 [Diagrama de flujo que explica la homeopatía - Naukas](http://naukas.com/2013/12/11/diagrama-de-flujo-que-explica-la-homeopatia/)   
 [Carta abierta de un químico a los homeópatas - Naukas](http://naukas.com/2017/01/23/carta-abierta-quimico-los-homeopatas/)  
 [Muerte por homeopatía - El profe de Física Arturo Quirantes Sierra](http://elprofedefisica.naukas.com/2017/05/27/muerte-por-homeopatia/)  
 [¿Homeopatia? Va a ser que no : Arturo Quirantes Sierra : Free Download, Borrow, and Streaming : Internet Archive](https://archive.org/details/AQSLibroHomeopatia)  
 Libro "Homeopatía. Va a ser que no" Arturo Quirantes Sierra, cc-by-nc.sa  
 [¿Homeopatía? Va a ser que no | Tutorías](http://elprofedefisica.es/libro_homeopatia/)  
 [El genial hilo en respuesta a un tuit sobre la homeopatía](http://www.publico.es/tremending/2017/06/21/twitter-el-genial-hilo-en-respuesta-a-un-tuit-sobre-la-homeopatia/)  
 [¿Te lo explico con marionetas? - ¿La homeopatía funciona?](http://marionetas.sinergiasincontrol.com/?id=2)  
 ¿La homeopatía funciona?Una página para que entiendas de una %&$# vez el tema  
 [Las bases físicas de la homeopatía: el artículo Rey - Naukas](http://naukas.com/2013/12/05/las-bases-fisicas-de-la-homeopatica-el-articulo-rey/)  
 [Otra prueba de las bases físicas de la homeopatía (o no)](http://elprofedefisica.naukas.com/2016/02/15/otra-prueba-de-las-bases-fisicas-de-la-homeopatia-o-no/)   
 
### Recursos sobre técnicas diagnósticas 

[Así es como funciona un TAC por dentro - dailymotion.com](https://www.dailymotion.com/video/x7lrkah)   

### Películas/vídeos
 [Soylent Green (1973) - IMDb](http://www.imdb.com/title/tt0070723)   
Distopía sobre recursos y sobrepoblación  
  
 [Awakenings (1990) - IMDb](http://www.imdb.com/title/tt0099077/)   
 [Despertares - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Despertares)   
  
 [Erin Brockovich (2000) - IMDb](http://www.imdb.com/title/tt0195685/)   
 [Erin Brockovich (película) - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Erin_Brockovich_(pel%C3%ADcula))   
Enfermedades producidas por contaminación agua  
  
 [The Constant Gardener (2005) - IMDb](http://www.imdb.com/title/tt0387131/)   
 [El jardinero fiel (película) - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/El_jardinero_fiel_(pel%C3%ADcula))     
    
 [Sicko (2007) Michael Moore - IMDb](http://www.imdb.com/title/tt0386032/)   
 [Sicko - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Sicko)   
  
La fórmula de la felicidad (2014)  
 [Better Living Through Chemistry (película) - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Better_Living_Through_Chemistry_(pel%C3%ADcula))   
 Comedia  
  
 [Vaxxed (2016) - IMDb](http://www.imdb.com/title/tt5562652/)   
 [Vaxxed - Wikipedia](https://en.wikipedia.org/wiki/Vaxxed)   
Película "antivacunas" que intenta justificar relación entre autismo y vacunas  

### Artículos
 2015: Por qué cada vez hay más miopes   
 Los expertos vaticinan que al final de la década un tercio de la población mundial usará gafas. Y no es solo por genética. Usted puede evitarlo  
 [Miopía: Por qué cada vez hay más miopes | Salud | BuenaVida | EL PAÍS](http://elpais.com/elpais/2015/04/14/buenavida/1429007036_893966.html)     
  
 2016: Un peldaño más en el largo camino de la vacuna contra la malaria  
 [Un peldaño más en el largo camino de la vacuna contra la malaria | Biociencia | EL MUNDO](http://www.elmundo.es/salud/2016/07/14/57868a0e46163f6f338b4578.html)   
  
2017: Antivacunas y Trump  
 [Donald Trump da alas al movimiento antivacunas](http://www.eldiario.es/sociedad/antivacunas-Trump-salud_0_601240014.html)   
  
La nueva vacuna del ébola es eficaz al 100%   
 Concluyen con éxito los análisis del medicamento en Guinea, en los que participaron más de 11.000 personas [La nueva vacuna del ébola es eficaz al 100% | Ciencia | EL PAÍS](http://elpais.com/elpais/2016/12/23/ciencia/1482509617_105561.html)   
  
2017: Obamacare y Trump, sistema sanitario  
 [twitter brutofficiel/status/870329503020720129](https://twitter.com/brutofficiel/status/870329503020720129)   

## Recursos asociados a Bloque 4. La revolución genética
Los cromosomas.  
 Los genes como bases de la herencia.  
 El código genético.  
 Ingeniería genética: transgénicos, terapias génicas.  
 El Proyecto Genoma Humano.  
 Aspectos sociales relacionados con la ingeniería genética.  
 La clonación y sus consecuencias médicas.  
 La reproducción asistida, selección y conservación de embriones.  
 Células madre: tipos y aplicaciones.  
 Bioética.

### Recursos generales genética
 [CSHL DNA Learning Center](https://www.dnalc.org/)  DNA Learning Center [Biology Animations - CSHL DNA Learning Center](https://www.dnalc.org/resources/3d/)   
 [educacion.navarra.es Genética 3º ESO](http://docentes.educacion.navarra.es/metayosa/3esogenetica1.html)  
 [¿Por qué las mitocondrias cuentan con su propio ADN? | Actualidad | Investigación y Ciencia](https://www.investigacionyciencia.es/noticias/por-qu-las-mitocondrias-cuentan-con-su-propio-adn-13995)   
 

### Recursos ingeniería genética
 [educacion.navarra.es Ingeniería genética 3º ESO](http://docentes.educacion.navarra.es/metayosa/3esogenetica11.html) 

### Recursos sobre CRISPR
 [This Video Perfectly Explains Why CRISPR Really Will Change The World Forever](http://www.sciencealert.com/this-video-explains-perfectly-why-crispr-really-will-change-humanity-forever)  
 [microBIO: Las bacterias también se &#8220;vacunan&#8221;: el sistema CRISPR/Cas](http://microbioun.blogspot.com.es/2015/07/las-bacterias-tambien-se-vacunan-el.html) (artículo que incluye enlaces a vídeos y otros artículos) “Sistemas CRISPR-Cas, una revolución biotecnológica con origen bacteriano”, Dr. Francisco M. Mojica   
 [![watch](https://img.youtube.com/vi/GOK6FkfmHdQ/0.jpg)](https://www.youtube.com/watch?v=GOK6FkfmHdQ)   
  
 17 febrero 2017   
 Why the CRISPR patent verdict isn’t the end of the story   
 From legal challenges to ongoing experimentation, the story of who owns the rights to CRISPR–Cas9 gene editing is still being written.   
 [Why the CRISPR patent verdict isn’t the end of the story | Nature](http://www.nature.com/news/why-the-crispr-patent-verdict-isn-t-the-end-of-the-story-1.21510?WT.ec_id=NEWSDAILY-20170220)   
 El vídeo es (agosto 2016)  
 Genetic Engineering Will Change Everything Forever – CRISPR,Kurzgesagt – In a Nutshell   
 [![https://www.youtube.com/watch?v=jAhjPd4uNFY ](https://img.youtube.com/vi/jAhjPd4uNFY/0.jpg)](https://www.youtube.com/watch?v=jAhjPd4uNFY)   
  
 Five big mysteries about CRISPR’s origins   
 Where did it come from? How do organisms use it without self-destructing? And what else can it do?   
 Heidi Ledford, 12 January 2017  [Five big mysteries about CRISPR’s origins | Nature](http://www.nature.com/news/five-big-mysteries-about-crispr-s-origins-1.21294)   
 
### Recursos sobre proteoma  

[La inteligencia artificial de Google predice la estructura de todas las proteínas conocidas y abre un nuevo universo para la ciencia](http://elpais.com/ciencia/2022-07-28/la-inteligencia-artificial-de-google-predice-la-estructura-de-todas-las-proteinas-conocidas-y-abre-un-nuevo-universo-para-la-ciencia.html)  

### Películas/vídeos
 [Gattaca (1997) - IMDb](http://www.imdb.com/title/tt0119177)  [Gattaca](https://archive.org/details/Gattaca)   
    
 [Blade Runner (1982) - IMDb](http://www.imdb.com/title/tt0083658/)   
 [Blade Runner - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Blade_Runner)   
 Planteamiento sobre en qué consiste el ser humano / vida ingeligente, ingeniería genética  
  
 [La isla (2005) - IMDb](http://www.imdb.com/title/tt0399201/)   
Clonación  
  
 Epigenetics, 2008  
 [Genes, ¿somos lo que comemos? (Epigenetics).2008 (Documental C.Odisea) | terapeuticaenfermera](https://terapeuticaenfermera.wordpress.com/2011/02/15/genes-%C2%BFsomos-lo-que-comemos-epigenetics-2008-documental-c-odisea/)   
 [![Somos Lo Que Comemos [Epigenética y Genes] (Documental Completo Canal Odisea 2008) - YouTube](https://img.youtube.com/vi/wTVamcOpjGI/0.jpg)](https://www.youtube.com/watch?v=wTVamcOpjGI)   
 [](https://terapeuticaenfermera.wordpress.com/2011/02/15/genes-%C2%BFsomos-lo-que-comemos-epigenetics-2008-documental-c-odisea/) 

### Artículos
 2012  
 [El proyecto ENCODE dice adiós al ADN &quot;basura&quot;: el 80% del ADN tiene funciones bioquímicas - La Ciencia de la Mula Francis](http://francis.naukas.com/2012/09/06/el-proyecto-piloto-encode-dice-adios-al-adn-basura-el-80-del-adn-tiene-funciones-bioquimicas/)   
  
 2015  
 La edición genética, más precisa  
 Una nueva técnica denominada CRISPR permite modificar el ADN a partir del sistema defensivo de las bacterias. Su aplicación podría revolucionar la medicina, pero hay quien teme sobre su uso incontrolado.  
 [la-edicin-gentica-ms-precisa-12843](http://www.investigacionyciencia.es/revistas/investigacion-y-ciencia/numero/461/la-edicin-gentica-ms-precisa-12843)   
  
 2016  
 Prueban por primera vez en humanos la técnica de edición genética CRISPR  
 Este nuevo sistema está llamado a revolucionar la medicina, pero los ensayos con pacientes todavía no habían tenido lugar. Se inicia así una carrera similar a la espacial  
 [Prueban por primera vez en humanos la técnica de edición genética CRISPR](http://www.elconfidencial.com/tecnologia/2016-11-16/la-tecnica-de-edicion-genetica-crispr-es-probada-por-primera-vez-en-humanos_1290397/)   
  
  
[Prueban en humanos la técnica de edición genética CRISPR para curar el cáncer](http://www.nationalgeographic.com.es/ciencia/actualidad/prueban-tecnica-edicion-genetica-crispr-para-curar-cancer_10868)   
  
Octubre 2016  
 [Nace un bebé con la nueva técnica de ‘tres padres genéticos’](http://elpais.com/elpais/2016/09/27/ciencia/1474989059_678680.html)   
 El pequeño tiene cinco meses y se le aplicó la técnica para librarlo de una enfermedad hereditaria de su madre  
  
 2017  
 [EE UU aplica la edición genética a embriones humanos por primera vez | Ciencia | EL PAÍS](https://elpais.com/elpais/2017/07/27/ciencia/1501150753_958985.html)   
  Investigadores consiguen eliminar defectos congénitos gracias a la técnica CRISPR  

2022  
[The complete sequence of a human genome - science.org](https://www.science.org/doi/10.1126/science.abj6987)  

[twitter JaimeCaroM/status/1598035417600823330](https://twitter.com/JaimeCaroM/status/1598035417600823330)  
 Entre la élite de Silicon Valley -Big Techs y los fondos de alto capital riesgo- hay un movimiento creciente "pronatalists" que buscan tener más bebes "genéticamente mejores"(y ricos) para "salvar el planeta"  
No es la película Gattaca, pero podría, Musk está metido  
Hilo  
Las elites, históricamente, siempre han pensado que sus retoños son mejores porque son más inteligentes y están mejor alimentados y educados que los de clase trabajadora. En palabras de una pareja en el que él trabaja en una Big Tech y ella en un Venture Capital:  
¿Qué es mejor, reproducir al 10% más bajo de la población o al 0.01% "mejor"? Su respuesta está clara, por eso quiere tener, por lo menos, 11 hijos con su pareja. Calculan que en 11 generaciones, sus retoños podrian ser los dominadores de la tierra y, obviamente, superiores.  
Este movimiento *no* es nuevo pero está siendo cada vez más influyente entre los multimillonarios que poseen empresas en Silicon Valley y en su apéndice en Texas. Tanto Elon Musk como Peter Thiel se han declarado varias veces a favor de este movimiento.  
De hecho, Musk, no sólo se declara a favor de este movimiento, lo practica. Actualmente tiene 10 hijos con distintas mujeres. Aquí tenéis más info:  
[Elon Musk had twins last year with one of his top executives](https://www.businessinsider.com/elon-musk-shivon-zilis-secret-twins-neuralink-tesla)  
Pero no es sólo que estos multimillonarios estén "practicando" este movimiento, es que están mezclando su capital y la tecnología de las empresas de SV para, en su megalomanía, mejorar la Humanidad a través de sus hijos "genéticamente superiores"  
Peter Thiel, Elon Musk, y su amigo, Steve Jurvetson han calculado que el mercado de la fertilidad mueve alrededor de los 78 mil millones de dólares y están invirtiendo ahí.  
Actualmente, amigos y empleados (OpenAI)cercanos de Musk han creado la compañia Genomic Prediction.  
Esta compañia esta diseñada para crear unos nuevos tipos de tests, muy caros, que sirven para que los padres que tienen varios embriones congelados puedan saber cual de ellos son "genéticamente más perfectos". (Cualquier genetista sabe que esto es una magufada)  
Los seguidores de este "movimiento" están obsesionados con tener más hijos y supuestamente "mejores" que los de clase trabajadora. Piensan que el mayor problema del mundo es la poca tasa de natalidad y están dispuestos a intentar resolverlo ellos porque son, literalmente, ricos.  
Para ellos la Humanidad se enfrenta a grandes peligros como el "colapso natal", el cambio climático o las guerras y por ello, lo mejor es que esta élite se reproduzca cuanto más mejor.  
[Billionaires like Elon Musk want to save civilization by having tons of genetically superior kids. Inside the movement to take 'control of human evolution.'](https://www.businessinsider.com/pronatalism-elon-musk-simone-malcolm-collins-underpopulation-breeding-tech-2022-11)  
[The Pandora’s Box of Embryo Testing Is Officially Open. Genetic testing companies promise they can predict someone’s probable future health. Some parents don’t want to stop there.](https://www.bloomberg.com/news/features/2022-05-26/dna-testing-for-embryos-promises-to-predict-genetic-diseases)  
['Pronatalism' explained: Why tech titans like Elon Musk want to have tons of kids to save the world](https://www.businessinsider.com/pronatalism-elon-musk-population-tech-2022-11)  

### Libros

[La vida a la velocidad de la luz: Desde la doble hélice a los albores de la vida digital, J. Craig Venter, 2013](https://books.google.es/books?id=XCQ9BgAAQBAJ)   

## Recursos asociados a Bloque 5. Nuevas tecnologías en comunicación e información
 Analógico frente a digital.  
 Ordenadores: evolución y características.  
 Almacenamiento digital de la información.  
 Imagen y sonido digital.  
 Telecomunicaciones: TDT, telefonía fija y móvil.  
 Historia de Internet.  
 Conexiones y velocidad de acceso a Internet. La fibra óptica.  
 Redes sociales.  
 Peligros de Internet.  
 Satélites de comunicación.  
 GPS: funcionamiento y funciones. --> Tratar GLONASS, Galileo, Beidu  
 Tecnología LED.  
 Comunicaciones seguras: clave pública y privacidad. Encriptación de la información.  
 Firma electrónica y la administración electrónica. --> citar DNIe  
 La vida digital. .---> citar Bitcoin  

### Recursos sobre Internet / uso de Internet  


 [http://www.evolutionoftheweb.com/](http://www.evolutionoftheweb.com/)   

 [webfoundation.org History of the Web](https://webfoundation.org/about/vision/history-of-the-web/)  
  
 Encuesta sobre Equipamiento y Uso de Tecnologías de Información y Comunicación en los Hogares (2016)  
 [INEbase / Nivel y condiciones de vida (IPC) /Condiciones de vida /Encuesta sobre equipamiento y uso de tecnologías de información y comunicación en los hogares / Últimos datos](http://www.ine.es/dyngs/INEbase/es/operacion.htm?c=Estadistica_C&cid=1254736176741&menu=ultiDatos&idp=1254735976608)   
 
[Submarine Cable Map](https://www.submarinecablemap.com/)   

[Japón destroza el récord de transmisión de datos con 1,02 petabits por segundo. Y con una fibra compatible con las actuales](https://www.xataka.com/investigacion/japon-destroza-record-transmision-datos-1-02-petabits-segundo-fibra-compatible-actuales)  
 

### Recursos sobre comunicaciones seguras

[How Does HTTPS Work? RSA Encryption Explained](https://tiptopsecurity.com/how-does-https-work-rsa-encryption-explained/)  
 ![](https://tiptopsecurity.com/wp-content/uploads/2017/06/How-HTTPS-Works.png "How https encryption works")  

### Recursos sobre redes sociales
 [10 infografías que te ayudarán a utilizar las redes sociales](http://flexo.es/10-infografias-que-te-ayudaran-a-utilizar-mejor-las-redes-sociales/)   
 [Las redes sociales, explicadas para niños](https://www.blogpocket.com/2013/02/19/las-redes-sociales-explicadas-para-ninos/) 

### Recursos sobre almacenamiento
 [Domain Registered at Safenames](http://mozy.com/blog/infographics/physical-storage-vs-digital-storage/)   
 
 [Inventan un sistema de almacenamiento capaz de comprimir 500 TB en un disco del tamaño de un CD - gizmondo.com](https://es.gizmodo.com/descubren-un-sistema-de-almacenamiento-capaz-de-comprim-1847980159)  
  
Referencias a BigData ...  

### Recursos sobre software libre

[Qué es el software libre, por qué surgió, qué ventajas tiene y en qué se diferencia del código abierto - maldita.es](https://maldita.es/malditatecnologia/20210918/software-libre-surgio-ventajas-diferencias-codigo-abierto/)  

### Recursos sobre algoritmos / inteligencia artificial

[El Algoritmo Que Transformó Al Mundo. La Historia De Nasir Ahmed.](https://www.youtube.com/watch?v=nNmREQIF4Ik)  

[A Style-Based Generator Architecture for Generative Adversarial Networks](https://arxiv.org/abs/1812.04948)  
[StyleGAN - wikipedia](https://en.m.wikipedia.org/wiki/StyleGAN)  
[this person does not exist](https://www.thispersondoesnotexist.com/)  

[Esta IA es capaz de realizar un retrato de una persona solo con la voz](https://es.gizmodo.com/esta-ia-es-capaz-de-realizar-un-retrato-de-una-persona-1848761807)  
[Speech2Face](https://speech2face.github.io/)  

[Artflow](https://artflow.ai/)  
Artflow lets users generate visual content with the help of AI. Create unique avatars with ease and turn any description into a portrait.  

[Cómo funciona la compresión JPEG, una explicación paso a paso - microsiervos](https://www.microsiervos.com/archivo/ordenadores/como-funciona-compresion-jpeg-paso-a-paso.html)  

Sobre GPT-3, ver hilo  
[twitter estudiarfisica/status/1562413324272340992](https://twitter.com/estudiarfisica/status/1562413324272340992)  
A mí, por ejemplo, esto me afecta en algo tan sencillo como que en Cultura Científica pido a los alumnos que escriban reflexiones sobre lo visto en clase como parte del seguimiento. Y GPT3 resulta que hace esto.  
(En gris lo que hace el programa)  

[twitter Juanito_web3/status/1600502897904689157](https://twitter.com/Juanito_web3/status/1600502897904689157)  
Da miedo la velocidad a la que avanza la IA.  
Ya hay muchas más aplicaciones de las que te imaginas, y si no las conoces te quedarás obsoleto.  
En este hilo comparto 13 tipos de IA y 39 herramientas en total. Si no lo guardas te arrepentirás (te lo aseguro)  
1️⃣ Texto a Imagen (T2I)

✅ https://craiyon.com / @craiyonAI
 
✅ https://stability.ai / @StabilityAI
 
✅ http://gaugan.org/gaugan2/ 
✅ https://linktr.ee/wonder_ai / @wonderaiapp
 
✅ https://artssy.co 

✅ https://jasper.ai / @heyjasperai

...

4️⃣ Texto a Texto (T2T)

✅ https://jasper.ai / @heyjasperai

✅ https://grammarly.com / @Grammarly
 
✅ https://copy.ai / @copy_ai

✅ https://copysmith.ai / @copysmith_ai

✅ https://textcortex.com / @TextCortex
 
✅ https://frase.io / @fraseHQ

[18 herramientas de Inteligencia Artificial que te sorprenderán](https://www.ayudaparamaestros.com/2022/12/18-herramientas-de-inteligencia.html)  

1. Jasper: es herramienta con inteligencia artificial entrenada para escribir contenido completamente original a la vez que creativo. Se trata de una herramienta perfecta para aquellos usuarios que buscan ahorrar tiempo con sus proyectos sin que se vea afectado el nivel del contenido. Todo ello gracias a consultas con expertos en SEO y marketing de respuesta directa.

2. Artssy: es como tener tu propio asistente artístico personal. Crea imágenes únicas generadas por IA con un solo clic.

3. Runway: software para automatizar la edición de video y para generar contenido nuevo a partir de algoritmos y con solo con escribir una frase.

4. Fliki: crea vídeos a partir de guiones o de publicaciones de blog usando voces realistas.

5. Resemble: generador de voz que permite crear en segundos voces en off similares a las humanas.

6. Play.HT: permite convertir instantáneamente texto en voz con sonido natural y descargarlo como archivos de audio MP3 y WAV.

7. Frase: ayuda a investigar, escribir y optimizar contenido SEO de alta calidad en minutos.

8. Copy.AI: generador de texto automático en el que podrás crear textos de diversa índole en segundos gracias a la inteligencia artificial.

9. Articoolo: crea contenido de texto único en un instante.

10. Concured: es una plataforma estratégica de contenidos impulsada por inteligencia artificial para detectar y predecir qué va a causar mayor interacción o engagement con tu público objetivo.

11. Chat OpenAI: modelo de IA generadora de texto conversacional capaz de ejecutar múltiples tareas relativas a la generación de texto como crear textos, completarlos, traducirlos, responder preguntas, clasificar conceptos o ejecutar conversaciones, entre otras.

12. Midjourney: programa de inteligencia artificial con el cual podemos crear imágenes a partir de descripciones textuales.

13. Wombo: tiene una Inteligencia Artificial incorporada que genera dibujos en base a una frase que le escribamos.

14. Craiyon: generador de imágenes IA en línea a partir de texto.

15. Rytr: utiliza OpenAI/GPT-3, una tecnología de Inteligencia artificial que permite generar contenidos para diversos usos, en varios idiomas y con diversas intenciones.

16. Copymatic: escribe automáticamente textos o contenido únicos, atractivos y de alta calidad: publicaciones de blog largas, landing pages, anuncios digitales, etc.

17. Peppertype: solución que ayuda a automatizar el proceso de creación de contenido e ideas. Utiliza aprendizaje automático avanzado e inteligencia artificial para analizar procesos, interpretar marcas y público objetivo y producir contenido original para ti.

18. Desgnify: crea diseños automáticos usando tus fotos favoritas. Elige cualquier imagen para crear diseños impulsados ​​por IA mediante la eliminación automática de fondos, la mejora de colores, el ajuste de sombras inteligentes y mucho más. Guarda, descarga o comparte sus diseños.

[Ethical guidelines on the use of artificial intelligence and data in teaching and learning for educators - ec.europa.eu](https://education.ec.europa.eu/news/ethical-guidelines-on-the-use-of-artificial-intelligence-and-data-in-teaching-and-learning-for-educators)  


## Recursos sobre competencia digital en alumnado

Enlaza con desinformación  
[Guidelines for teachers and educators on tackling disinformation and promoting digital literacy through education and training - ec.europa.eu](https://education.ec.europa.eu/news/guidelines-for-teachers-and-educators-on-tackling-disinformation-and-promoting-digital-literacy-through-education-and-training)  


### Películas / vídeos
Muchas películas enlazan con ciencia-ficción general: las posibilidades de la tecnología y la red, hackers, inteligencia artificial, ...  
Aparte hay bastantes documentales  
  
 Tron, Steven Lisberger (1982)  [](http://www.imdb.com/title/tt0084827/)   
 Proyecto Brainstorm, Douglas Trumbullm (1983)  
 [Brainstorm (1983) - IMDb](http://www.imdb.com/title/tt0085271/)   
 [La red, Irwin Winkler (1995)](http://www.imdb.com/title/tt0113957/)   
 [Hackers (Piratas informáticos), Ian Softley (1995)](http://www.imdb.com/title/tt0113243/)   
 [Enemigo público, Tony Scott (1998)](http://www.imdb.com/title/tt0120660/)   
 [The Matrix, Wachowski Brothers (1999) - IMDb](http://www.imdb.com/title/tt0133093/)   
 [Nivel 13, Josef Rusnak (1999)](http://www.imdb.com/title/tt0139809/)   
 [Asalto final, Joe Chapelle (2000)](http://www.imdb.com/title/tt0159784/)   
 [Revolution OS (2001) - IMDb](http://www.imdb.com/title/tt0308808/) Documental  
 [The Code, Hannu Puttonen, (2001)](http://www.imdb.com/title/tt0315417/)  Documental, emitido por La2 y compartido en [Documental Codigo Linux - youtube](https://www.youtube.com/watch?v=cwptTf-64Uo)  

  
[A.I. Artificial Intelligence, Steven Spielberg (2001) - IMDb](http://www.imdb.com/title/tt0212720/)   
 [Conspiración en la red (Antitrust), Peter Howitt (2001) - IMDb](http://www.imdb.com/title/tt0218817/)   
 [ Yo, Robot, Alex Proyas (2004) - IMDb](http://www.imdb.com/title/tt0343818/)   
  
 [Hacking Democracy; Simon Ardizzone, Russell Michaels (2006) - IMDb](http://www.imdb.com/title/tt0808532/)   
  
[The Social Network, David Fincher  (2010) - IMDb](http://www.imdb.com/title/tt1285016/)     
 [Code 2600, Jeremy Zerechak (2011)](http://www.imdb.com/title/tt1830538/)   
    
 [Terms and Conditions May Apply, Cullen Hoback (2013)](http://www.imdb.com/title/tt2084953/)   
 Documental  
 
 [We Steal Secrets: The Story of WikiLeaks, Alex Gibney (2013)](http://www.imdb.com/title/tt1824254/)   
Documental  
  
 [Downloaded, Alex Winter (2013) - IMDb](http://www.imdb.com/title/tt2033981/)   
Documental  
  
 [Steve Jobs, Danny Boyle (2015)](http://www.imdb.com/title/tt2080374/)   
  
 [Deep Web, Alex Winter (2015)](http://www.imdb.com/title/tt3312868/)   
Documental  

### Artículos
 2017  
 [Year Zero: WikiLeaks revela que la CIA espía a través del móvil, la tele, Telegram y Whatsapp | Público](http://www.publico.es/internacional/wikileaks-cia-movil-television-whatsapp.html)   
 [El FBI retira los cargos contra un pedófilo para no revelar cómo lo identificaron en la Darknet ](https://www.genbeta.com/seguridad/el-fbi-retira-los-cargos-contra-un-pedofilo-para-no-revelar-como-lo-identificaron-en-la-darknet)   
  
 [ ¿Llegará el smartphone a sustituir al ordenador? &raquo; Enrique Dans](https://www.enriquedans.com/2017/03/llegara-el-smartphone-a-sustituir-al-ordenador.html)   
 [IBM unveils world’s first 5nm chip | Ars Technica](https://arstechnica.com/gadgets/2017/06/ibm-5nm-chip/)   

### Libros
Criptonomicón, Neal Stephenson (1999) [Criptonomicón - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Criptonomic%C3%B3n)   
Steve Jobs, Walter Isaacson (2011) [Steve Jobs (libro) - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Steve_Jobs_(libro)) Biografía autorizada


