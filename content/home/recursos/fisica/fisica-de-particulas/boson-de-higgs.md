
# Bosón de Higgs

Ver página  [El bosón de Higgs en ficción](/home/recursos/fisica/fisica-de-particulas/el-boson-de-higgs-en-ficcion)   

El Bosón de Higgs (por fin) Explicado a Fondo. QuantumFracture  
[![](https://img.youtube.com/vi/wZCWNLLpmZQ/0.jpg)](https://www.youtube.com/watch?v=wZCWNLLpmZQ "El Bosón de Higgs (por fin) Explicado a Fondo")  
    
[The Higgs boson, explained simply (the real story)](http://coherence.wordpress.com/2012/07/08/the-higgs-boson-simply-explained/)   
 
[1, 2, 3, 4, .... ¡Higgs! El bosón de Higgs para todos](http://www.rinconsolidario.org/higgs/index.htm) Miguel Ángel Queiruga, Trinidad Ruiz, ... cc-byEl mecanismo de Higgs para estudiantes de bachillerato I, Cuentos cuánticos,   
 
[El mecanismo de Higgs para estudiantes de bachillerato I - cuentos-cuanticos](https://cuentoscuanticos.wordpress.com/2014/06/26/el-mecanismo-de-higgs-para-estudiantes-de-bachillerat-i/)   
 
[How the Higgs Field Works (with math) | Of Particular Significance](https://profmattstrassler.com/articles-and-posts/particle-physics-basics/how-the-higgs-field-works-with-math/)   
  
[The discovery and measurements of a Higgs boson; F. Gianotti, T. S. Virdee](http://rsta.royalsocietypublishing.org/content/373/2032/20140384)   
 
[The US and the Higgs boson > Resources for Teachers - archive.org](https://web.archive.org/web/20150206120846/http://www.uslhc.us/higgs/Resources_for_Teachers)   


