
# Recursos óptica atmosférica

La óptica atmosférica se puede asociar a óptica física, dentro de [óptica](/home/recursos/fisica/recursos-optica)  
[Óptica atmosférica - wikipedia](https://es.wikipedia.org/wiki/%C3%93ptica_atmosf%C3%A9rica)  
> La óptica atmosférica es una disciplina que intenta explicar cómo las propiedades ópticas distintivas de la atmósfera terrestre causan una amplia gama de fenómenos ópticos.  

En un dibujo infantil en el que se dibuja el Sol amarillo, el cielo azul y las nubes blancas, se está aplicando la dispersión asociada a óptica atmosférica.  
Al dibujar rayos del Sol se está aplicando difracción: ver [óptica física](/home/recursos/fisica/optica-fisica)  
[twitter FiQuiPedia/status/1742165176755855426](https://x.com/FiQuiPedia/status/1742165176755855426)  
Desde niños pintamos diagramas de difracción al dibujar el Sol y las estrellas, incluyendo la de Belén.   
La ciencia está en todas partes.  


Una recopilación gráfica se puede ver en [Fotometeoros - WMO (Organización Meteorológica Mundial)](https://cloudatlas.wmo.int/es/photometeors-other-than-clouds.html)  

[Fenómenos atmosféricos. Unidad didáctica - Instituto de Astrofísica de Canarias](http://astroaula.net/wp-content/uploads/2018/07/unidadfenomenos.pdf)  

[Óptica atmosférica - Beatriza Pardo Rivera - csic.es](https://digital.csic.es/handle/10261/223919)  

[![](http://hyperphysics.phy-astr.gsu.edu/hbasees/atmos/imgatm/atmoscon.gif)](http://hyperphysics.phy-astr.gsu.edu/hbasees/atmos/atmoscon.html#c1 "Óptica atmosférica - hyperphysics")  

[Atmospheric refraction - wikipedia](https://en.wikipedia.org/wiki/Atmospheric_refraction)  
![](https://upload.wikimedia.org/wikipedia/commons/3/39/Atmospheric_refraction_-_sunset_and_sunrise.png)  

[Light scattering by airborne ice crystals – An inventory of atmospheric halos](https://www.sciencedirect.com/science/article/pii/S0022407322002485)  
[twitter GritsevichMaria/status/1724032875853406716](https://twitter.com/GritsevichMaria/status/1724032875853406716)  
Renowned halo expert Jarmo Moilanen explains 17 different halos observed in the display, including rare ones like Tape and heliac arcs  
![](https://pbs.twimg.com/media/F-z-UTjW0AArN3z?format=jpg)  

[Grupo de óptica atmosférica de Universidad e Valladolid](http://goa.uva.es/the-group/)  

[Foto álbum de Imágenes Ópticas Atmosféricas - windows2universe.org](https://www.windows2universe.org/earth/Atmosphere/clouds/optics_album.html&lang=sp)  

## Dispersión

[Dispersión de Rayleigh - wikipedia](https://es.wikipedia.org/wiki/Dispersi%C3%B3n_de_Rayleigh)  
Dispersión / difusión de Mie / Tyndall  

[5. Dispersión de la luz - catedu.es](https://e-ducativa.catedu.es/44700165/aula/archivos/repositorio/3000/3236/html/5_dispersin_de_la_luz.html)  

[Dispersión de Rayleigh. ¿Por qué el cielo es azul?](https://elrincondemaxwell.wordpress.com/2016/04/06/dispersion-de-rayleigh-por-que-el-cielo-es-azul/)  
![](https://elrincondemaxwell.files.wordpress.com/2016/04/rayleigh-scattering.gif)  

[Esparcimiento - aulafacil](https://www.aulafacil.com/cursos/fisica-y-quimica/el-color/esparcimiento-l37431)  
![](https://www.aulafacil.com/uploads/cursos/5843/20164_esp_final.es.png)  

[¿Por qué el cielo se pone rojo al atardecer?. Los responsables son la dispersión de Rayleigh y el hecho de que la luz sea una combinación de diferentes colores. - muyinteresante](https://www.muyinteresante.es/curiosidades/28734.html)  

[Experimento Naukas: ¿Por qué el cielo es azul?](https://naukas.com/2014/05/05/experimento-naukas-por-que-el-cielo-es-azul/)  

[Esparcimiento de Rayleigh. REDALYC (Revista Eureka sobre Enseñanza y Divulgación de las Ciencias), vol. 13, núm. 2, pp. 505-510, 2016 ](https://www.redalyc.org/journal/920/92044744019/html/)  

[Mie Scattering](https://apollo.nvu.vsc.edu/classes/met130/notes/chapter19/mie_scatt.html)  
![](https://apollo.nvu.vsc.edu/classes/met130/notes/chapter19/graphics/mie_scatt_cloud_schem.jpg)  
> this is why clouds appear white....  
[ Mie Scattering giving clouds their white appearance](http://ww2010.atmos.uiuc.edu/(Gh)/guides/mtr/opt/mch/sct/mie.rxml)  

Dispersión refractiva  
[Dispersion of Light. oPhysics: Interactive Physics Simulations - geogebra](https://ophysics.com/l8.html)  

[¿De qué color es el cielo en otros planetas? #CienciaADomicilio - DGDCUNAM ](https://www.facebook.com/DGDCUNAM/photos/a.471034523015586/4501393346646330/?type=3)  

Imágenes dispersión Tyndall asociadas a logo Windows 10
[Windows 10 Desktop](https://gmunk.com/Windows-10-Desktop/)  


## Fenómenos de óptica atmosférica
Se ponene algunos enlaces e imágenes por orden alfabético

## Alpenglow

[Alpenglow - aberron](https://aberron.substack.com/p/alpenglow)  
![](https://substackcdn.com/image/fetch/w_1456,c_limit,f_webp,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F33c7f3da-8910-4e17-b352-33573d99cf9f_1196x850.jpeg)  

## Amanecer / atardecer 

[Diagram showing displacement of the Sun's image at sunrise and sunset](https://en.wikipedia.org/wiki/Atmospheric_refraction#/media/File:Atmospheric_refraction_-_sunset_and_sunrise.png)  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Atmospheric_refraction_-_sunset_and_sunrise.png/800px-Atmospheric_refraction_-_sunset_and_sunrise.png)  

## Arco iris

[![](https://img.youtube.com/vi/24GfgNtnjXc/0.jpg)](https://www.youtube.com/watch?v=24GfgNtnjXc "Why no two people see the same rainbow - Veritasium") 

[Rainbow Formation in 3D. oPhysics: Interactive Physics Simulations - geogebra](https://ophysics.com/l18.html)  

[twitter Rainmaker1973/status/1694044477394977237](https://twitter.com/Rainmaker1973/status/1694044477394977237 )  
In theory, every rainbow is a circle, but from the ground, usually only its upper half can be seen. Things change when you're flying or observing from an elevated vantage point  
![](https://pbs.twimg.com/media/F4Jz-c7XAAAhZ-8?format=jpg)  

[Rainbow Formation in 3D - geogebra - oPhysics: Interactive Physics Simulations](https://ophysics.com/l18.html)  
> This is a simulation of the processes involved in the formation of a rainbow. The 3D image on the right shows the location of a rainbow as seen by an observer on the ground. The green plane represents the ground. The blue dot on the green plane represents the position of the person viewing the rainbow. Change the angle of the sun above the horizontal and watch how the position of the rainbow changes.  
> On the right in the 2D view you can see the actual refraction and reflection that occurs in each raindrop. The white light enters the spherical raindrop, and it undergoes refraction (bending) and dispersion (different wavelengths/colors bending by slightly different amounts). Only the extreme red and violet rays are shown. On the right side of the raindrop reflection occurs. Near the bottom of the raindrop the light leaves the drop, again refracting and dispersing. The light from the red end of the spectrum comes out of the drop below the light from the violet end of the spectrum. This confuses some, as red is the color located at the top of a rainbow and violet is on the bottom. You can zoom in on the raindrops in the 3D view (by clicking the button). This view may help people understand this apparent contradiction. The drop that sends the red light to the viewer needs to be higher in the sky than the drop that sends the violet ray to the viewer (with all the other colors in between, of course).  

[Rainbow formation, Peo](https://commons.wikimedia.org/wiki/File:Rainbow_formation.png?uselang=es)   
![](https://upload.wikimedia.org/wikipedia/commons/8/8e/Rainbow_formation.png)  

## Cinturón de Venus

[Cinturón de Venus](https://es.wikipedia.org/wiki/Cintur%C3%B3n_de_Venus)  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/ALMA_and_Chajnantor_at_Twilight.jpg/800px-ALMA_and_Chajnantor_at_Twilight.jpg)  

## Eclipse lunar

[Lo que hay que saber sobre el eclipse lunar - nasa.gov](https://ciencia.nasa.gov/ciencia/sistema-solar/lo-que-hay-que-saber-sobre-el-eclipse-lunar/)  
![](https://ciencia.nasa.gov/_ipx/animated_true&w_1280&f_webp/https://smd-cms.nasa.gov/ciencia/wp-content/uploads/sites/2/2023/06/3_2-jpeg.webp%3Fw=2000)  

## Fata Morgana

[Qué es la 'fata morgana': el fenómeno que hace que los barcos floten en el aire	](https://www.20minutos.es/noticia/4609723/0/que-es-fata-morgana-fenomeno-hace-barcos-floten-aire/)  
![](https://imagenes.20minutos.es/files/image_640_360/uploads/imagenes/2021/03/07/fata-morgana.png)  

[Fata Morgana: Un posible espejismo del Titanic](https://www.tiempo.com/ram/22077/fata-morgana-un-posible-espejismo-del-titanic/)  

## Halo
[twitter GritsevichMaria/status/1724032875853406716](https://twitter.com/GritsevichMaria/status/1724032875853406716)  
Renowned halo expert Jarmo Moilanen explains 17 different halos observed in the display, including rare ones like Tape and heliac arcs  
![](https://pbs.twimg.com/media/F-z-UTjW0AArN3z?format=jpg)  

[Light scattering by airborne ice crystals – An inventory of atmospheric halos - sciencedirect.com](https://www.sciencedirect.com/science/article/pii/S0022407322002485)  

## Luna de sangre

[![](https://img.youtube.com/vi/o6JfLYksP5g/0.jpg)](https://www.youtube.com/watch?v=o6JfLYksP5g "Luna de Sangre... ¿AZUL? - Quantumfracture))") 

## Luz púrpura
[Luz púrpura - wmo.int](https://cloudatlas.wmo.int/es/purple-light.html)  

## Nubes 

[The Color of Clouds - noaa.gov](https://www.noaa.gov/jetstream/clouds/color-of-clouds)   
[Física de las nubes iridiscentes - culturacientifica](https://culturacientifica.com/2013/03/08/fisica-de-las-nubes-iridiscentes/)  

## Pilares

[La física atmosférica de los pilares de luz - francis.naukas.com](https://francis.naukas.com/2018/01/17/pilares-de-luz/)  

## Rayos crepusculares

[Espectáculo de rayos crepusculares - lavanguardia](https://www.lavanguardia.com/participacion/las-fotos-de-los-lectores/20200505/48992259049/espectaculo-rayos-crepusculares-gava.html)  

## Split screen sunset 

[Gorgeous, Freaky Sunset Photo Looks Split Down the Middle - livescience.com](https://www.livescience.com/65971-split-sunset-photo.html)  
![](https://cdn.mos.cms.futurecdn.net/BDgBYzF32hJSyeayqyvfBj-650-80.jpg.webp)  
![](https://cdn.mos.cms.futurecdn.net/YjvxT3SZGcYtBiUb2tiJwf-970-80.jpg)  

[![](https://img.youtube.com/vi/vGqLUkJ3koo/0.jpg)](https://www.youtube.com/watch?v=vGqLUkJ3koo "The sky in Florida divided into two parts") 
A "split screen" sunset happens when clouds on the right half of the footage are higher in the sky and are thus still picking up some of the Sun’s lingering rays.  


