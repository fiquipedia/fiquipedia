
# Diagrama Lewis

Nota: en español a veces se pronuncia /legüis/, pero la pronunciación correcta es /luis/  
[Cómo pronunciar Lewis - forvo](https://es.forvo.com/word/lewis/#en)  
[Pronunciación en inglés de Lewis structure - dictionary.cambridge](https://dictionary.cambridge.org/es/pronunciaci%C3%B3n/ingles/lewis-structure)  

Una representación útil para enlaces covalentes, centrada en la idea de formar enlaces para conseguir el octeto.  
En la página sobre tabla periódica hay una tabla periódica con cada elemento con su diagrama de Lewis.  

Asociado a  [geometría molecular](/home/recursos/quimica/geometria-molecular), en algunas imágenes se muestran entre otros diagramas de Lewis, o estructuras de moléculas que se puede asociar a "Lewis" sin indicar los electrones no enlazantes  
[Hypochlorous acid - chemspider](http://www.chemspider.com/Chemical-Structure.22757.html)  

Además de los casos sencillos, dentro de Lewis se puede tratar la resonancia: La idea básica no hay una estructura de Lewis única y 100% correcta, sino que hay varias y "resuena" entre ellas.  
Un ejemplo de un compuesto habitual y conocido es el caso del ozono, O3  
En el diagrama del ozono se ponen dos enlaces dobles pero con una de las líneas punteadas como si fuera "medio enlace"  
[Ozone-1,3-dipole - wikimedia](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Ozone-1%2C3-dipole.png/800px-Ozone-1%2C3-dipole.png)  
Razonando con esos diagramas de Lewis utilizando TRPECV, sería triangular plana y serían 120º, pero como tiene un par de electrones no enlazantes, la realidad es algo menor, 116,8º  

[Resonancia (química) - wikipedia](http://es.wikipedia.org/wiki/Resonancia_%28qu%C3%ADmica%29)  

[Construye una estructura de Lewis. Visualizaciones en química - uv.es](https://www.uv.es/quimicajmol/lewis/index.htm)  
Se elige una molécula y luego permite  
"Pulsa sobre un átomo para poner o quitar electrones. Pulsa sobre un enlace para cambiar el orden de enlace."  
"Revisa la estructura" 

Algunos ejercicios química incluyendo Lewis  
[Spring Chemistry Homework #6 Answer Key - evergreen.edu](http://archives.evergreen.edu/webpages/curricular/2000-2001/MANDM2000/Spring/homework/hw6/hw6.htm)  
© 2014 The Evergreen State College  

[Lewis structure calculator | Lewis structure generator - idealcalculator](https://idealcalculator.com/lewis-structure-calculator-lewis-structure-generator/)  

[Lewis Dot Structures Quiz - chemquiz.net](https://chemquiz.net/lew/)  

[Unit 3 - Covalent Bonding and Molecular Structure. Lewis Structures #1](https://www.sciencegeek.net/Chemistry/taters/Unit3LewisStructures.htm)  
[Unit 3 - Covalent Bonding and Molecular Structure. Lewis Structures #2](https://www.sciencegeek.net/Chemistry/Quizzes/LewisStructures/)  

[Construct a Lewis Structure - St. Olaf College General Chemistry Toolkit](https://www.stolaf.edu/depts/chemistry/courses/toolkits/121/js/lewis/)  

Como recurso, Wolframapha da estructuras Lewis a partir de fórmula  
[lewis structure H2SO4 - wolframalpha](https://www.wolframalpha.com/input/?i=lewis+structure+H2SO4)  

[Lewis structure builder - ncsu.edu](https://go.distance.ncsu.edu/lewis-structure-builder/)  
> This app is designed to help illustrate the basics of building Lewis Structures.   

[Demos > Lewis Dot Structures - chemdoodle](https://web.chemdoodle.com/demos/lewis-dot-structures#customise-template)  
> This demo will convert a skeletal figure, provided by a drawing in the HTML5 SketcherCanvas component on the left, into a Lewis Dot Structure in the Canvas on the right.  

[Lewis Structures & shapes of molecules (incluye respuestas)](http://www.nobraintoosmall.co.nz/students/chemistry/NCEA_Level2/pdfs/che_90308_shape0611_plus_answers.pdf)  

[twitter quirogafyq/status/1332059740495605762](https://twitter.com/quirogafyq/status/1332059740495605762)  
Trabajando las estructuras de Lewis en 4° ESO. Lo manipulativo es siempre mejor!!!  
![](https://pbs.twimg.com/media/EnxtRHjWEA8hrrZ?format=jpg)  
![](https://pbs.twimg.com/media/EnxtRHgWEAA2yOX?format=jpg)  
![](https://pbs.twimg.com/media/EnxtRHiWEAIhSFw?format=jpg)  
![](https://pbs.twimg.com/media/EnxtRHgW8AIx9wj?format=jpg)  


