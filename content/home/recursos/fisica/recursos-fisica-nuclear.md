
# Recursos física nuclear

Ver apuntes del bloque física nuclear en  [apuntes de física de 2º de bachillerato de elaboración propia](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato)   
Puede haber recursos relacionados en  [recursos de física de partículas](/home/recursos/fisica/fisica-de-particulas)  y  [recursos de física cuántica](/home/recursos/fisica/recursos-fisica-cuantica)   
La resonancia magnético nuclear se puede asociar también a  [campo magnético](/home/recursos/fisica/recursos-campo-magnetico)   
También puede haber recursos sencillos / divulgativos relacionados con radiactividad/isótopos/energía nuclear y aplicaciones/contanminación a niveles ESO  
También relacionado con  [medio ambiente](/home/recursos/recursos-fisica-quimica-y-medio-ambiente) 

## Recursos generales

[Las aplicaciones de la física nuclear - intef.es](https://descargas.intef.es/recursos_educativos/ODES_SGOA/Bachillerato/FQ/3B.6_-_Fsica_nuclear/crditos_y_descarga.html)    

CPEP (Contemporary Physics Education Project). Nuclear Science   
 [CPEP - Contemporary Physics Education Project - CPEP](http://www.cpepweb.org/nuclear.html)   
   * Nuclear Chart  
 [CPEP - Contemporary Physics Education Project - CPEP](http://www.cpepweb.org/nuclear-chart.html) 
      * Teacher's Guide [Guide to Nuclear Science Wall Chart](http://www2.lbl.gov/abc/wallchart/guide.html)   

   * ABC's of Nuclear Science [The ABC's of Nuclear Science](http://www2.lbl.gov/abc/)  Produced by the Nuclear Science Division --- Lawrence Berkeley National Laboratory. Varios autores, créditos en  [Credits](http://www2.lbl.gov/abc/credits.html) 
Grupo Heurema. Apuntes de Física en Bachillerato (16-19 años)  
 [ApFBachill11.htm](http://heurema.com/ApFBachill11.htm)   
  
A Guide to the Different Types of Radiation  
 [A Guide to the Different Types of Radiation &#8211; Compound Interest](http://www.compoundchem.com/2015/09/10/radiation/)  [![A Guide to the Different Types of Radiation, Compoundchem , cc-by-nc-nd](http://www.compoundchem.com/wp-content/uploads/2015/09/A-Guide-to-Different-Common-Types-of-Radiation-1024x724.png "A Guide to the Different Types of Radiation, Compoundchem , cc-by-nc-nd") ](http://www.compoundchem.com/wp-content/uploads/2015/09/A-Guide-to-Different-Common-Types-of-Radiation-1024x724.png)   
fisica2spp WIKI educativa para alumn@s de FISICA 2º Bachillerato   
UNIDAD VII. ELEMENTOS DE FÍSICA NUCLEAR.  
 [UNIDAD+VII](https://fisica2spp.wikispaces.com/UNIDAD+VII)   
Profesor: Crescencio Pascual Sanz, Colegio San Pedro Pascual  
Profesor: Juan José Serrano Pérez, Instituto de Ciencia Molecular, Universitat de València  
Licenciamiento cc-by-nc-sa  
  
CHAPTER 2: NUCLEAR MASSES AND BINDING ENERGY  
 [AKJain_IITR_Ch_2.htm](http://pms.iitk.ernet.in/ICT/physics_courses/akj/AKJain_IITR_Ch_2.htm)   
  
 [](http://fisicaymates.com/fisica-selectividad-pau/fisica-nuclear/)   
Ejercicios resueltos en vídeo  
  
 [radioactive-decay](https://www.khanacademy.org/science/chemistry/radioactive-decay)   
Clases en vídeo  
  
VISUALIZACIÓN DE LAS RADIACIONES - CANAL SUR   
 [![Radiaciones nucleares: como verlas - YouTube](https://img.youtube.com/vi/oHXgBvPecBE/0.jpg)](https://www.youtube.com/watch?v=oHXgBvPecBE)   
Lo relativo a cámara de niebla se centraliza en  [recursos de física de partículas](/home/recursos/fisica/fisica-de-particulas)   
  
Radioactivity and Atomic Physics Explained (demo limitada en tiempo)  
 [Radioactivity - animated lessons and simulations to teach radioactivity](http://www.furryelephant.com/content/radioactivity) /  
 [Teaching and learning radioactivity](http://www.furryelephant.com/content/radioactivity/teaching-learning/)   
  
Isótopos y radiactividad  
 [index.html](http://www.maloka.org/fisica2000/isotopes/index.html)   
  
The Basics of NMR  
 [Basics of NMR](http://www.cis.rit.edu/htbooks/nmr/bnmr.htm)   
Joseph P. Hornak, Ph.D.  
Copyright © 1997-99 J.P. Hornak. All Rights Reserved.   

## Recursos historia y cultura nuclear 

[National Museum of Nuclear Science & History](https://www.nuclearmuseum.org/see/exhibits)  

[Los Alamos National Laboratory](https://www.lanl.gov/media/publications)  

[Atomic culture Universidad de Columbia](https://amnesia.americanstudies.columbia.edu/content/atomic-culture)  

## Datos sobre isótopos
Información en   
 [Periodic Table - Ptable](https://www.ptable.com/#Isotope)   
Live Chart of Nuclides  
nuclear structure and decay data [Livechart - Table of Nuclides - Nuclear structure and decay data](https://www-nds.iaea.org/relnsd/vcharthtml/VChartHTML.html)   
 [radionuclide-half-life-measurements-data](https://www.nist.gov/pml/radionuclide-half-life-measurements-data)   
 [radionuclide-half-life-measurements-data](https://www.nist.gov/pml/radionuclide-half-life-measurements/radionuclide-half-life-measurements-data)   
 
[FÍSICA NUCLEAR - mheducation.es](https://www.mheducation.es/bcv/guide/capitulo/8448169549.pdf)  

[Física 2º Bachillerato. IES La Magdalena. Avilés. Asturias Física Nuclear](http://fisquiweb.es/Apuntes/Apuntes2Fis/Desintegraci%C3%B3n%20beta.pdf)  

## Recursos sobre dispositivos basados en radiactividad

[Geiger counter - wikipedia](https://en.wikipedia.org/wiki/Geiger_counter)  

[What is a Geiger Counter - nrc.gov](https://www.nrc.gov/reading-rm/basic-ref/students/science-101/what-is-a-geiger-counter.html)  

[The Just Bananas Method for Generating True Random Numbers. Arduino-based Banana Random Number Generator (BRNG). - hackster.io](https://www.hackster.io/news/the-just-bananas-method-for-generating-true-random-numbers-0e67c763dc1a)  


## Recursos datación radiométrica  

 [datacion_radiometrica.pdf](https://www.ugr.es/~eaznar/matgeo/apuntes/datacion_radiometrica.pdf)  los elementos radioactivos más útiles para datar rocas antiguas son el uranio 238, el potasio 40 y el rubidio 87. Para escalas de tiempo menores se usa el carbono 14.  

## Recursos fusión
[![La Energía de Fusión explicada - Futuro o Fracaso - Kurzgesagt](https://img.youtube.com/vi/mZsaaturR6E/0.jpg)](https://www.youtube.com/watch?v=mZsaaturR6E "La Energía de Fusión explicada - Futuro o Fracaso - Kurzgesagt")   

[Reactores de fusión nuclear - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Reactores_de_fusi%C3%B3n_nuclear)   

[![](https://img.youtube.com/vi/oOcs_LjpJBk/0.jpg)](https://www.youtube.com/watch?v=oOcs_LjpJBk "Fusión nuclear por confinamiento magnético. EERR - T9")  

 
[![https://www.youtube.com/watch?v=k3zcmPmW6dE](https://img.youtube.com/vi/k3zcmPmW6dE/0.jpg)](https://www.youtube.com/watch?v=k3zcmPmW6dE "Inside a General Fusion Power Plant - General Fusion")   

[General Fusion Founder and Chief Scientist Delivers a talk at TED 2014](https://generalfusion.com/2014/03/general-fusion-founder-and-chief-scientist-delivers-a-talk-at-ted-2014/)  
 
[La fusión nuclear cada vez más cerca de acabar con las obsoletas eléctricas - elconfidencial.com](https://www.elconfidencial.com/amp/tecnologia/novaceno/2021-08-20/fusion-record-ignicion-estados-unidos_3242310/)  
> 1,3 millones de julios. 10 billones de vatios durante 100 billonésimas de segundo. Ése es el nuevo récord de fusión con láser que ha conseguido un equipo científico estadounidense en la National Ignition Facility (NIF).  

"Fusion Power - Latest Breakthroughs/Updates. noviembre 2021"  
[![](https://img.youtube.com/vi/SGHuLVagKhA/0.jpg)](https://www.youtube.com/watch?v=SGHuLVagKhA "Fusion Power - Latest Breakthroughs/Updates. noviembre 2021")  

[Nuclear fusion breakthrough achieved with new projectile technique - newscientist.com](https://www.newscientist.com/video/2315208-nuclear-fusion-breakthrough-achieved-with-new-projectile-technique/)  
[![](https://img.youtube.com/vi/3odtCyOty-Q/0.jpg)](https://www.youtube.com/watch?v=3odtCyOty-Q "Nuclear fusion breakthrough achieved with new projectile technique")  

[Astronomy 110 Fall 2005   Section 006 Homework 7 : Sunshine & Sunspots](https://home.ifa.hawaii.edu/users/acowie/class05/home7_sol.html)  
Ejercicios sobre energía solar, relacionables con fusión  

[Un nuevo tipo de reactor de fusión está rompiendo todas las expectativas - julio 2022](https://www.elconfidencial.com/tecnologia/novaceno/2022-07-22/1200-millones-nueva-fusion-nuclear-record_3464684/)  
> Qué tiene de nuevo este reactor  
> En lugar del clásico reactor tokamak con forma de rosquilla que se utiliza en instalaciones como el ITER, TAE ha creado una estructura larga y delgada que han llamado Configuración de Campo Invertido Impulsada por un Haz (FRC). Este sistema genera dos nubes de plasma hidrógeno a ambos extremos del reactor y los hace chocar en el centro de la cámara para producir la reacción. El plasma se mantiene estable gracias a potentes imanes controlados por inteligencia artificial.   

[twitter OperadorNuclear/status/1602528987737165825](https://twitter.com/OperadorNuclear/status/1602528987737165825)  
HITO EN LA FUSIÓN NUCLEAR POR CONFINAMIENTO INERCIAL  
El @Livermore_Lab de Estados Unidos 🇺🇸 informa que ha logrado la fusión nuclar generando más energía que la suministrada. ¿Qué significa este hito y cuáles son las siguientes etapas? Te lo expico en un HILO.  

[twitter OperadorNuclear/status/1602529071908405248](https://twitter.com/OperadorNuclear/status/1602529071908405248)  
Etapas de la fusión según el @Livermore_Lab:  
🟢 Demostrar que el principio de la fusión nuclear es sólido.  
🟢 Ganancia de energía neta.  
🔴 Resolver los desafíos técnicos.  
🔴 Construir y demostrar una central eléctrica prototipo (LIFE).  
🔴 Construir centrales comerciales.  
![](https://pbs.twimg.com/media/Fj1TsgjWAAEISWd?format=jpg)  

[La fusión nuclear, Ícaro y el pensamiento tecno-mágico - ctxt.es](https://ctxt.es/es/20221201/Firmas/41614/)

REFERENCIAS  
 
[📖 NIF FAQ.](https://lasers.llnl.gov/content/assets/docs/news/pk_faqs.pdf)  
[📖 National Ignition Facility User Guide.](https://lasers.llnl.gov/content/assets/docs/for-users/nif-user-guide.pdf)  
[📖 The NIF: Bringing Star Power to Earth.](https://mipse.eecs.umich.edu/files/Dunne_presentation.pdf)  
[📖 Laser Inertial Fusion Energy (LIFE), a path to US energy independence.](https://www.sseb.org/downloads/AM-2012/presentations/Dunne.pdf)  

[National Ignition Facility achieves fusion ignition](https://www.llnl.gov/news/national-ignition-facility-achieves-fusion-ignition)  

## Recursos energía nuclear

En general se puede ver el perfil de Twitter de Alfredo García [Operador nuclear](https://twitter.com/OperadorNuclear)  

Our friend, the atom (Walt Disney, 1957, 49 min)  [![watch](https://img.youtube.com/vi/_RTyhIo2qFY/0.jpg)](https://www.youtube.com/watch?v=_RTyhIo2qFY) Planteamiento de uso didáctico comentado   
 [twitter ProfaDeQuimica/status/934559458503643136](https://twitter.com/ProfaDeQuimica/status/934559458503643136)  
 [twitter RadiactivoMan/status/934725965825150976](https://twitter.com/RadiactivoMan/status/934725965825150976)   
 [twitter OperadorNuclear/status/1003179331689111552](https://twitter.com/OperadorNuclear/status/1003179331689111552)  
 La energía nuclear es una tecnología estigmatizada que acumula una enorme cantidad de mitos y medias verdades por diversas causas.En un gran HILO, que quedará abierto para posibles ampliaciones, intentaré desmontar todos esos mitos con sólidos argumentos y referencias.  
 
[Oponerse a la energía nuclear es populista - lasexta.com](https://www.lasexta.com/el-muro/deborah-garcia/oponerse-energia-nuclear-populista_2022072062d7b749444e510001fc6b23.html)  
[OPONERSE HOY A LA ENERGÍA NUCLEAR ES CIENCIA Y ECONOMÍA ](https://www.linkedin.com/pulse/oponerse-hoy-la-energ%25C3%25ADa-nuclear-es-ciencia-y-econom%25C3%25ADa-juan)  autor es CEO de [TSER](https://tser.es/)  

[twitter MadiHilly/status/1550148385931513856](https://twitter.com/MadiHilly/status/1550148385931513856)  
MYTH: We don't have a solution to nuclear's "waste problem"  
REALITY: Nuclear waste isn't a problem. In fact, it’s the best solution we have to meeting our energy needs while protecting the natural environment!  
Here's what you need to know:  
Nuclear waste concerns are overwhelmingly focused on “high-level waste”, which is almost entirely spent nuclear fuel.   
Nuclear fuel is made up of metal tubes containing small pellets of uranium. These tubes are gathered into bundles for loading and unloading into the reactor.  
After that, several bundles are placed inside concrete and steel "casks" and placed in rows next to the reactor.  
(Dry cask storage at Palo Verde)  
Because uranium is very energy dense, the amount of waste is relatively small.   
All of the fuel rods ever used by the commercial nuclear industry since the late 1950s could fit on a single football field stacked about 50 feet high.  
The best part: when the fuel rods are done in the reactor, over 90% of the potential energy from the uranium is still left in them!  
That means we can recycle the spent fuel and turn it into new fuel, which is already routinely done in Europe, Russia, and Japan.  
In summary, nuclear waste:  
- is solid (not glowing green goo)  
- is tiny compared to the waste from all other energy technologies  
- is easily contained  
- has a perfect safety record  
Nuclear waste does have a problem, however...  
...which is that policymakers and the public think that nuclear waste has a problem!   
The prevailing belief is that nuclear waste is uniquely dangerous and that the industry doesn't know what to do with it.  
Let's start with uniquely dangerous.   
The main concern associated with spent nuclear fuel – radioactivity – diminishes with time.  
About 40 years after it's done making power, the heat and radioactivity of the fuel bundle will have fallen by over 99%.  
...  

## Recursos radiación
 [twitter OperadorNuclear/status/954608179572301825](https://twitter.com/OperadorNuclear/status/954608179572301825)  
 Dosis de Radiación. Recomiendo mirar esta infografía con calma para poner la radiación en perspectiva y disipar miedos. Nota: el asterisco del pie del primer recuadro es absolutamente genial.  
![](https://pbs.twimg.com/media/DT0H5xLWAAIucrJ.jpg)  
 [twitter OperadorNuclear/status/1098300143298850816](https://twitter.com/OperadorNuclear/status/1098300143298850816)  
 ¿Qué es la radiactividad? #ApuntesOperador  
 ![](https://pbs.twimg.com/media/Dz3yGeGXcAAgLnO.jpg)  
 
[Cuestionario ¿Por qué en Chernóbil no se puede vivir y en Hiroshima sí?](https://www.estonoentraenelexamen.com/2020/02/28/cuestionario-por-que-en-chernobil-no-se-puede-vivir-y-en-hiroshima-si/)  
 [![](https://img.youtube.com/vi/PEQE_oGPHok/0.jpg)](https://www.youtube.com/watch?v=PEQE_oGPHok "¿Por qué en Chernóbil no se puede vivir y en Hiroshima sí? - Date un voltio")   
 
[twitter OperadorNuclear/status/1694736440461017575](https://twitter.com/OperadorNuclear/status/1694736440461017575)   
Japón 🇯🇵 va a diluir en el océano SOLO 16 GRAMOS DE TRITIO repartidos en 1300 millones m³ durante 30 años, respetando los estándares internacionales y con la supervisión del @iaeaorg  de Naciones Unidas.  
Sigue leyendo para poner en contexto los datos y conocer los riesgos reales.  
☢️ El medio ambiente de toda la Tierra contiene unos 25.000 gramos (25 kg) de tritio.  
☢️ Cada año se producen 1302 g de tritio en la atmósfera de forma natural por los rayos cósmicos.  
☢️ Las centrales nucleares de todo el mundo diluyen legalmente un total de unos 515 g de tritio cada año.  
☢️ Japón diluirá los 16 g de tritio durante al menos 30 años, lo que supone añadir al océano algo más de MEDIO GRAMO DE TRITIO cada año.  
☢️ El tritio es un isótopo del hidrógeno muy ligeramente radiactivo. Su emisión de electrones es tan débil que no es capaz de atravesar la membrana celular, por lo que no puede afectar al ADN ni provocar cáncer.  
☢️ No se han apreciado nunca daños en la salud de las personas por el tritio. Los límites legales en cada país son diferentes y se establecen por el principio de precaución.  
☢️ El tritio tiene un periodo de semidesintegración de 12,3 años, lo que significa que 12,3 años desaparece la mitad.  
![](https://pbs.twimg.com/media/F4TpzuiXcAAaImX?format=jpg)  
REFERENCIAS

📖 [METI (2020). The Outline of the Handling of ALPS Treated Water at Fukushima Daiichi NPS (FDNPS).](https://meti.go.jp/english/earthquake/nuclear/decommissioning/pdf/brief20200203.pdf)  
📖 [UNSCEAR.](https://unscear.org/unscear/uploads/documents/publications/UNSCEAR_2016_Annex-C.pdf)  
📖 [@iaeaorg  Preguntas frecuentes.](https://iaea.org/es/temas/respuesta/descarga-del-agua-tratada-de-fukushima-daiichi/preguntas-frecuentes)  
📖 [@iaeaorg Comprehensive report on the safety review of the alps-treated water at the fukushima daiichi nuclear power station.](https://iaea.org/sites/default/)  

## Ejercicios
En los  [ejercicios de EvAU Física por bloques](/home/pruebas/pruebasaccesouniversidad/paufisica)  hay ejercicios dentro del bloque de Física moderna.Ahí hay algún ejercicio de elaboración propia, como uno que hice con datos reales sobre "la sábana santa"Otra idea  [Exclusive: Age of Jesus Christ’s Purported Tomb Revealed](https://news.nationalgeographic.com/2017/11/jesus-tomb-archaeology-jerusalem-christianity-rome/) Mortar sampled from between the original limestone surface of the tomb and a marble slab that covers it has been dated to around A.D. 345. According to historical accounts, the tomb was discovered by the Romans and enshrined around 326.  
  
 [https://twitter.com/bjmahillo/status/1072508500339757056](https://twitter.com/bjmahillo/status/1072508500339757056) Ejercicios de radiactividad 3º ESO FyQ con temática de los Simpson. Espero que le gusten a @RadiactivoMan. 😉 [![](https://pbs.twimg.com/media/DuJQSVUWoAQD82c.jpg "") ](https://pbs.twimg.com/media/DuJQSVUWoAQD82c.jpg)   

## Recursos laboratorios virtuales / simulaciones
Control de la potencia de una planta nuclear  
 [http://www.ida.liu.se/~her/npp/demo.html#instructions](http://www.ida.liu.se/%7Eher/npp/demo.html#instructions)  [fimode.htm](http://iesfgcza.educa.aragon.es/depart/fisicaquimica/fisicasegundo/fimode.htm)   
Recursos, animaciones... Incluye juntos todos los bloques de física moderna:  [física relativista](/home/recursos/fisica/recursos-fisica-relativista) ,  [física cuántica](/home/recursos/fisica/recursos-fisica-cuantica)  y física nuclear  
Luis Ortiz de Orruño 

### Desintegración Radiactiva / ley de desintegración   

 [Ley de la Desintegraci&oacute;n Radiactiva](http://www.walter-fendt.de/html5/phes/lawdecay_es.htm)   
© Walter Fendt, 16 Julio 1998  
© Traducción: Juan Muñoz, 9 Marzo 1999  
  
 [radioactive_decay.html](http://www.maloka.org/fisica2000/isotopes/radioactive_decay.html)   
 [radioactive_decay3.html](http://www.maloka.org/fisica2000/isotopes/radioactive_decay3.html)   
  
 [decay.htm](http://lectureonline.cl.msu.edu/~mmp/applist/decay/decay.htm)   
© W. Bauer, 1999  
  
Series  
 [Series de Desintegraci&oacute;n Radiactiva](http://www.walter-fendt.de/html5/phes/decaychains_es.htm)   
© Walter Fendt, 20 Julio 1998  
© Traducción: Juan Muñoz, 9 Marzo 1999  
  
Simulaciones Universidad de Colorado  
[![](https://phet.colorado.edu/sims/html/build-a-nucleus/latest/build-a-nucleus-900.png)](https://phet.colorado.edu/es/simulations/build-a-nucleus)  
[Construye un núcleo](https://phet.colorado.edu/sims/html/build-a-nucleus/latest/build-a-nucleus_es.html)  
Permite ver núcleos estables e inestables y desintegración con distintas emisiones.

 [MRI simplificado (Resonancia Magnética Nuclear)](https://phet.colorado.edu/es/simulations/mri)   
 ![](https://phet.colorado.edu/sims/mri/mri-600.png)  
 
 [Desintegración alfa](http://phet.colorado.edu/es/simulation/alpha-decay)  Permite ver como se van produciendo desintegraciones con 1 núcleo o con muchos. Permite visualizar el núcleo como un pozo de potencial   
 ![](https://phet.colorado.edu/sims/nuclear-physics/alpha-decay-600.png)  
 
 [Desintegración beta](http://phet.colorado.edu/es/simulation/beta-decay)  Permite ver como se van produciendo desintegraciones con 1 núcleo o con muchos.  
 ![](https://phet.colorado.edu/sims/nuclear-physics/beta-decay-600.png)  
 
 [Fisión nuclear](https://phet.colorado.edu/es/simulations/nuclear-fission)   
 ![](https://phet.colorado.edu/sims/nuclear-physics/nuclear-fission-600.png)  
 
Licenciamiento cc-by ó gnu-gpl  [licensing](http://phet.colorado.edu/en/about/licensing)   
  
Physique et simulations numériques.   
 [Physique et simulation](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/mndivers.html)   
Jean-Jacques ROUSSEAU. Faculté des Sciences exactes et naturelles. Université du Maine   
Francés. Niveaux : Lycée, premier et second cycles.  
 [Décroissance radioactive](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/radioac1.html)   
 [Filiations radioactives](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/radioac2.html)   
 [Filiations radioactives (animation)](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/radioaanim.html)   
 [Activités et équilibres radioactifs](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/equilibreRad.html)   
  
 [Teaching and learning radioactivity](http://www.furryelephant.com/content/radioactivity/teaching-learning/)  (demo limitada en tiempo)  
Incluye algunas simulaciones como  

   * 1. Alfa, beta and gamma [Furry Elephant Physics](http://www.furryelephant.com/player.php?subject=physics&jumpTo=re/2Ms101) 
   * 2. Half-life part 2 [Furry Elephant Physics](http://www.furryelephant.com/player.php?subject=physics&jumpTo=re/15Ms185) 
   * 3: Half-life part 1 [Furry Elephant Physics](http://www.furryelephant.com/player.php?subject=physics&jumpTo=re/3Ms21) 
   * 2: Alpha, Beta and Gamma [Furry Elephant Physics](http://www.furryelephant.com/player.php?subject=physics&jumpTo=re/2Ms31) 
  
Applet: Nuclear Isotope Half-lifes  
 [nuc.htm](http://lectureonline.cl.msu.edu/~mmp/kap30/Nuclear/nuc.htm)   
© W. Bauer, 1999  
  
Radiactividad. Problema 2º Bachillerato  
 [RADIACTIVIDAD](http://recursostic.educacion.es/newton/web/materiales_didacticos/radiactividad_prob/index.html)   
Mª Josefa Grima y Javier Soriano, cc-by-nc-sa  


### Simulaciones reacción en cadena
 [chain.htm](http://lectureonline.cl.msu.edu/~mmp/applist/chain/chain.htm)   
© W. Bauer, 1999  
  
Physique et simulations numériques.   
 [Physique et simulation](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/mndivers.html)   
Jean-Jacques ROUSSEAU. Faculté des Sciences exactes et naturelles. Université du Maine   
Francés. Niveaux : Lycée, premier et second cycles.  
 [Réaction en chaîne](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/divers/reacchaine.html)   

[![](https://img.youtube.com/vi/hKZoThmpbD0/0.jpg)](https://www.youtube.com/watch?v=hKZoThmpbD0 "Fisión Nuclear - Reacción en cadena")  

## Explosiones nucleares
Enlaza con escalas

[The Top 10 Largest Nuclear Explosions, Visualized - visualcapitalist.com](https://www.visualcapitalist.com/largest-nuclear-explosions/)  
![](https://www.visualcapitalist.com/wp-content/uploads/2022/05/Top-10-Largest-Nuclear-Explosions.png)  

## Interacción fuerte

La fuerza sin rival en el universo: LA FUERZA FUERTE | CROMODINÁMICA CUÁNTICA  
[![](https://img.youtube.com/vi/ERHaTx-6Ayc/0.jpg)](https://www.youtube.com/watch?v=ERHaTx-6Ayc "La fuerza sin rival en el universo: LA FUERZA FUERTE | CROMODINÁMICA CUÁNTICA")   


