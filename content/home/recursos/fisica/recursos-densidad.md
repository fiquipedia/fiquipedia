
# Recursos densidad

La densidad se trata en varios cursos:  
- En [2º ESO (LOMCE)](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso) / [3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso) al introducir/tratar la materia y sus propiedades específicas  
- En [4º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso) al tratar [fuerzas en fluidos](/home/recursos/fisica/fuerzas-en-fluidos) (estática de fluidos y flotación  
- En cursos posteriores se utiliza asumiéndose ya conocida (por ejemplo en química en problemas de disoluciones, en física en problemas de gravitación)  
- Las propiedades básicas de la materia es algo que aparece también en los ámbitos científico tecnológicos (diversificación curricular, formación adultos, FP Básica)  
Se incluyen aquí algunos recursos  
[Problemas Densidad, Nivel Fácil](http://www.fullquimica.com/2011/04/problemas-densidad-nivel-facil.html)  
Ver recursos de  [simulaciones](/home/recursos/simulaciones)  

Simulación Densidad  
[![](https://phet.colorado.edu/sims/html/density/latest/density-900.png)](https://phet.colorado.edu/sims/html/density/latest/density_es.html)  

[Densidad - Masa | Volumen. Simulaciones Interactivas de PhET - colorado.edu](https://phet.colorado.edu/sims/html/density/latest/density_es.html)  
  
[Inicación interactiva a la materia. Densidad (Flash)](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/propiedades/densidad.htm)  

[50|50 Cube - Precision Desktop Dual Metal Element Block](https://www.kickstarter.com/projects/1284571885/50-50-cube-precision-desktop-dual-metal-element-bl) 50|50 Cube || tungsten (W) and magnesium (Mg) -- physics fun  

[Density simulation, AACT](https://teachchemistry.org/periodical/issues/september-2015/density)  
In this simulation, students will investigate the effect of changing variables on both the volume and the density of a solid, a liquid and a gas sample. Students will analyze the different states of matter at the particle level as well as quantitatively.  
Similar [Estados de agregación de la materia - educaplus](http://www.educaplus.org/game/estados-de-agregacion-de-la-materia), pero no indica masas y no permite calcular densidades y ver cómo varían  

[twitter bjmahillo/status/1187006491414482944](https://twitter.com/bjmahillo/status/1187006491414482944)  
Hoy en 2° ESO hemos visto este vídeo de @SteveSpangler (a partir del 2:20) para ver la diferencia de densidades entre el hexafluoruro de azufre y el aire:¡Es muy bueno y a los chavales les ha encantado! Ha habido incluso aplausos al final.  
[![Anti-Helium - The Deep Voice Gas - DIY Sci](http://img.youtube.com/vi/z4_DMzzpJ8k/0.jpg)](http://www.youtube.com/watch?v=z4_DMzzpJ8k "Anti-Helium - The Deep Voice Gas - DIY Sci")  

[Densidad - laboratorio virtual Salvador Hurtado](https://labovirtual.blogspot.com/2015/06/densidad.html)  
[Laboratorio de densidad - educaplus.org](https://www.educaplus.org/game/laboratorio-de-densidad)  

[PW 2 Density - Density and flotation](https://padlet.com/collabscience/q08w0lkhzhjj/wish/352580740)  
[twitter ThePlanetaryGuy/status/1294659335952445440](https://twitter.com/ThePlanetaryGuy/status/1294659335952445440)  
Density of gold: 19.3 g/ccDensity of sand: 1.6 g/cc  
WHY DID HE THINK THIS WOULD WORK  
![](https://pbs.twimg.com/media/EfeNxK_XYAEJRvS?format=jpg)  

[twitter javyfeu/status/1401021794572771336](https://twitter.com/javyfeu/status/1401021794572771336)  
La densidad de hielo por su estructura cristalina es menor que la del agua. Al caer una pared del glaciar Columbia en #Alaska el empuje del agua hace que este se eleve. Un espectáculo maravilloso. #Geology #Physics #nature #mar #glaciers  
[Vídeo](https://video.twimg.com/ext_tw_video/1401021737425424385/pu/vid/720x890/N5fz1xmLiYzzSzcp.mp4?tag=12)  

[twitter onio72/status/1469194225300516865](https://twitter.com/onio72/status/1469194225300516865)  
He propuesto a mis alumnos de FQ2ESO fabricar con una botella de plástico un vaso medidor casero para poder tomar porciones de 250 gramos de diferentes alimentos. Creo que esta sencilla tarea puede ayudar a consolidar los conceptos de volumen, masa y densidad. ¿Qué os parece? ⁦   
![](https://pbs.twimg.com/media/FGOgW3mX0AMeTII?format=jpg)  

[Escape impostor (densidad) - genial.ly](https://view.genial.ly/607d010ac54e6b0d0da6b94c/interactive-content-densidad-2)  

Existe una actividad de SM "Unidad 2 La materia y sus propiedades. Física y Química 2.º ESO El detective de las sustancias" donde usando la densidad se analiza un puñal, usando la curva de calentamiento se analiza un líquido, y con densidad se analizan unas esmeraldas.  

Trozos de un hilo de Twitter que da para una situación de aprendizaje ...

[Twitter FerFrias/status/1810642891166961989](https://x.com/FerFrias/status/1810642891166961989)  
Resulta que, siempre según el transportista, el paquete pesaba más de lo que declaramos. En concreto nos manda este cuadro:  
![](https://pbs.twimg.com/media/GSCyGznXAAMLOC5?format=png)  
Según ese transportista (que SEURo que lo habrá medido con todo cuidado), mi paquete de 0,00091 m³ de volumen pesaba 120 kg.  
Lo cual, haciendo unos números, nos da una densidad de 131.868 kg/m³  
Claro, lo primero que piensa uno en estos casos es “¿pero qué creen que había en el paquete? ¿Plomo?”  
Pero no: el plomo tiene una densidad de 11.340 kg/m³. Ni siquiera el metal más denso, el osmio, se acerca a mi paquete: solo pesa 22.587 kg/m³, la sexta parte de mi envío.  
El flerovio, por ejemplo (hola, @Ununcuadio), tiene una densidad (teórica) de unos 12.000 kg/m³, más o menos, y el organesón unos 13.650. Una porquería al lado de mi paquete.  
Por ejemplo: si mi paquete fuese de osmio, como he comentado más arriba, pesaría 22.587 kg/m³, de modo que para alcanzar los 131.868 kg/m³ que el transportista ha pesado bastaría una gravedad superficial 5,8 veces superior a la de la Tierra.  
 
