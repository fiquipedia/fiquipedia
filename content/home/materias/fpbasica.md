# Materias FP Básica

Enlaces a materias FP Básica:  
* [Módulo Profesional "Ciencias Aplicadas I", Código 3009](/home/materias/fpbasica/moduloprofesionalcienciasaplicadasi3009)

Las materias que puede impartir un profesor de la especialidad de Física y Química en FP Básica son las siguientes:  
(Ver  [Especialidades y materias afines, Tabla especialidades - materias](https://sites.google.com/site/especialidadesymateriasafines/home/especialidades-y-materias-afines-secundaria/tabla-especialidades-materias) )  

La LOMCE hace desaparecer los PCPI (Programas de Cualificación Profesional Inicial) que son sustituidos por FP Básica, comenzándose la implantación de FP Básica en septiembre de 2014.  
Tras aprobarse la LOMCE en diciembre de 2013, la normativa de FP Básica se aprueba a nivel estatal a lo largo de 2014  
[Ciclos Formativos de FP Grado Básico - todofp.es](https://www.todofp.es/que-estudiar/ciclos/fp-grado-basico.html)  Incluye enlaces sobre cada ciclo: 
>Real decreto  
>Currículo Ministerio 	
>Currículos CCAA  
>Perfiles profesionales  
>Dónde estudiar  

Enlaces a normativa  
## LOMCE 

   *  [Real Decreto 774/2015, de 28 de agosto, por el que se establecen seis Títulos de Formación Profesional Básica del catálogo de Títulos de las enseñanzas de Formación Profesional.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2015-9462) 
   *  [Real Decreto 356/2014, de 16 de mayo, por el que se establecen siete títulos de Formación Profesional Básica](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-5591) 
   *  [Real Decreto 127/2014, de 28 de febrero, por el que se regulan aspectos específicos de la Formación Profesional Básica](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-2360) 
PROPIA DE CEUTA Y MELILLA
   *  [Orden ECD/1633/2014, de 11 de septiembre, por la que se establece el currículo de siete ciclos formativos de formación profesional básica en el ámbito de gestión del Ministerio de Educación, Cultura y Deporte.](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-9335) 
   *  [Orden ECD/1030/2014, de 11 de junio, por la que se establecen las condiciones de implantación de la Formación Profesional Básica y el currículo de catorce ciclos formativos de estas enseñanzas en el ámbito de gestión del Ministerio de Educación, Cultura y Deporte.](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-6431) 
MADRID
   *  [DECRETO 107/2014, de 11 de septiembre, del Consejo de Gobierno, por el que se regula la Formación Profesional Básica en la Comunidad de Madrid y se aprueba el Plan de Estudios de veinte títulos profesionales básicos.](http://w3.bocm.es/boletin/CM_Orden_BOCM/2014/09/15/BOCM-20140915-1.PDF) 
   * INSTRUCCIONES DE LA DIRECCIÓN GENERAL DE EDUCACIÓN SECUNDARIA, FORMACIÓN PROFESIONAL Y ENSEÑANZAS DE RÉGIMEN ESPECIAL, RELATIVAS AL PROCESO DE EVALUACIÓN DE LOS ALUMNOS INCORPORADOS EN EL CURSO ACADÉMICO 2014-2015 AL PRIMER CURSO DE CICLOS DE FORMACIÓN PROFESIONAL BÁSICA DE LA COMUNIDAD DE MADRID. (fecha 14 enero 2015) [gestiona3.madrid.org cove_web](https://gestiona3.madrid.org/cove_web/run/j/VerificacionCertificadoAccion.icm)  Código 0999670544926889730397  
   *  [ORDEN 1409/2015, de 18 de mayo, de la Consejería de Educación, Juventud y Deporte, por la que se regulan aspectos específicos de la Formación Profesional Básica en la Comunidad de Madrid.](http://www.bocm.es/boletin/CM_Orden_BOCM/2015/06/08/BOCM-20150608-28.PDF) 

## LOMLOE   

En LOMLOE pasan a ser Ciclos Formativos de Grado Básico, regulados junto a ESO  
[Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975)  
[Artículo 25. Ciclos Formativos de Grado Básico.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975#a2-7)  

> 4. En el anexo V se fijan, para los ámbitos a los que se refieren los párrafos a) y b) del apartado anterior, las competencias específicas, así como los criterios de evaluación y los contenidos, enunciados en forma de saberes básicos  

Anexo V se puede ver solo en el pdf   
[Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria. - pdf consolidado](https://www.boe.es/buscar/pdf/2022/BOE-A-2022-4975-consolidado.pdf)  

[Disposición final tercera. Calendario de implantación.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975#df-3)  
> 2. Lo dispuesto en este real decreto se implantará para el primer curso de los Ciclos Formativos de Grado Básico en el curso escolar 2022-2023 y para el segundo curso, en el curso escolar 2023-2024.

[Real Decreto 659/2023, de 18 de julio, por el que se desarrolla la ordenación del Sistema de Formación Profesional.](https://www.boe.es/buscar/act.php?id=BOE-A-2023-16889)  


[Real Decreto 498/2024, de 21 de mayo, por el que se modifican determinados reales decretos por los que se establecen títulos de Formación Profesional de grado básico y se fijan sus enseñanzas mínimas.](https://www.boe.es/buscar/doc.php?id=BOE-A-2024-10683)  



Se deroga currículo LOMCE con la LOMLOE ??  
En LOMLOE [Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975)  
[Artículo 25. Ciclos Formativos de Grado Básico.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975#a2-7)  
> 4. En el anexo V se fijan, para los ámbitos a los que se refieren los párrafos a) y b) del apartado anterior, las competencias específicas, así como los criterios de evaluación y los contenidos, enunciados en forma de saberes básicos  

Pero ese anexo no separa contenidos entre 1º y 2º  

En [Real Decreto 127/2014](https://www.boe.es/buscar/act.php?id=BOE-A-2014-2360) se indica  
> Norma derogada, con efectos de 23 de julio de 2023, a excepción del apartado b) del artículo 1 y de los anexos I a XIV, por la [disposición derogatoria única.3 del Real Decreto 659/2023, de 18 de julio](https://www.boe.es/buscar/act.php?id=BOE-A-2023-16889#dd-2)  

Precisamente el currículo está en los anexos I a IV  



[Formación Profesional Grado Básico - Comunidad de Madrid](https://www.comunidad.madrid/servicios/educacion/estudiar-fp-formacion-profesional-grado-basico)  

   
 Existen una serie de títulos profesionales básicos.  
 Cada ciclo formativo asociado a un título profesional básico está formado por una serie de módulos profesionales, cada uno de los cuales se indica con una codificación.  
 Hay algunos módulos profesionales comunes entre distintos títulos, a veces idénticos y a veces con variaciones para estar contextualizados al campo del perfil profesional del título.  
 Los asociados a la especialidad de Física y Química son:  
* [ Módulo Profesional: Ciencias aplicadas I, Código: 3009.](/home/materias/fpbasica/moduloprofesionalcienciasaplicadasi3009)  
* Módulo Profesional: Ciencias aplicadas II. Código: 3010.  
* Módulo Profesional: Ciencias aplicadas II. Código: 3019.  
* Módulo Profesional: Ciencias aplicadas II. Código: 3042.  
* Módulo Profesional: Ciencias aplicadas II. Código: 3059.   
