
# Recursos mezclas

Página, como tantas, en borrador, por ir anotando cosas ...  
Las mezclas se tratan en  [Física y Química 2º ESO](/home/materias/eso/fisica-y-quimica-2-eso)  y  [Física y Química 3º ESO](/home/materias/eso/fisica-y-quimica-3-eso)  

En 2015 incluyo recopilación de ejercicios asociados a mezclas en  [pruebas libre de graduado en ESO](/home/pruebas/pruebas-libres-graduado-eso)  y asociados a materia (que incluyen mezclas) en  [pruebas de acceso a Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio) , que pueden seleccionarse y servir por ejemplo para 3ºESO y FPBásica.  

Los sistemas materiales, sustancias puras y las mezclas se ven en física y química de 3º ESO. Está relacionado con  [disoluciones](/home/recursos/quimica/recursos-disoluciones)  y  [concentración](/home/recursos/quimica/concentracion)  
 
[Proyecto Ulloa (Recursos para Química). Sistemas materiales, mezclas, separación, 3º ESO](http://recursostic.educacion.es/ciencias/ulloa/web/ulloa2/3eso/secuencia2/menu.html) cc-by-nc-sa  

[Clasificación de la materia. Sustancias puras: elementos y compuestos. Mezclas: homogéneas y heterogéneas](http://www.objetos.unam.mx/quimica/sustanciasPuras/index.html)  

[Iniciación interactiva a la materia. Clasificación sustancias](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/clasif/clasifica1.htm)  Incluye actividad donde hay mezclas

Sobre el tema de usar sal para hacer helados y usar sal para el deshielo de carreteras, que es una pregunta habitual de los alumnos en ESO  
[Ciencia en la Cocina (VII): La aparente contradicción entre los helados eutécticos y el deshielo de carreteras](http://scientiablog.com/2015/02/05/ciencia-en-la-cocina-vii-la-aparente-contradiccion-entre-los-helados-eutecticos-y-el-deshielo-de-carreteras/)  

[Sustancias puras y mezclas, cidead](http://recursostic.educacion.es/secundaria/edad/3esofisicaquimica/3quincena4/impresos/quincena4.pdf)  

Sustancias puras y mezclas  [https://sites.google.com/site/arandajm/](https://sites.google.com/site/arandajm/)  

[Mezclas y Sustancias puras](http://www.icarito.cl/2012/12/364-9672-9-quinto-basico-mezclas-y-sustancias-puras.shtml/)  

[Tipos de materia](http://elprofelopezsmp.blogspot.com.es/2015/05/tipos-de-materia.html)  

[2ºESO Tema 6: Mezclas, disoluciones y sustancias puras - EL GATO DE SCHRÖDINGER. Blog de física y química. ](http://www3.gobiernodecanarias.org/medusa/ecoblog/mramrodp/2016/11/08/mezclas-disoluciones-y-sustancias-puras-esquemas-y-actividades-del-tema/) 

##  Separación mezclas
 [Separación de mezclas, librosvivos.net (Flash)](http://www.primaria.librosvivos.net/archivosCMS/3/3/16/usuarios/103294/9/5EP_Cono_cas_ud6_separacion_mezclas/frame_prim.swf)  
 
 [Jugando a separar](http://odas.educarchile.cl/objetos_digitales/odas_ciencias/22_jugando_separar/LearningObject/index.html) 
   
 [How to teach evaporation, filtration and crystallisation David Paterson](https://eic.rsc.org/cpd/evaporation-filtration-and-crystallisation/3009017.article)  
 
 Tema 3(2ªparte).-Técnicas de separación de mezclas  [https://sites.google.com/site/arandajm/](https://sites.google.com/site/arandajm/)   
 
 Ejercicios resueltos de separación de mezclas  [https://sites.google.com/site/arandajm/](https://sites.google.com/site/arandajm/) 

###  Cromatografía
 [twitter GordonsScience/status/881188393014702080](https://twitter.com/GordonsScience/status/881188393014702080)  
 Time-lapse chromatography of magic pens @FromPaperchase Water then NH3 soln only 4/5 change. 3 years & I still don't know how they all work.  
 
[twitter cientifidor/status/1481291138552238086](https://twitter.com/cientifidor/status/1481291138552238086)  
Pues quizás una posible práctica para explicar la cromatografía sea desmontar un test de antígenos. En 2º de ESO ya aparece este método y los alumn@s no suelen entenderlo. Tendré que pensar como enfocar la práctica pero creo que a ell@s les gustará bastante.  

###  Cristalización
La cristalografía es más que una separación de mezclas; se puede crear página separada, enlazar con concurso cristalización ...  
[Blog de cristalización](http://cristalesdelaboratorio.blogspot.com.es/) José Ángel Guardiola (enlace operativo en 2018)  
[Cristales: Fosfato monoamónico ](http://cristalesdelaboratorio.blogspot.com.es/2011/04/cristales-fosfato-monoamonico.html)  

[http://www.lec.csic.es/concurso_13/](http://www.lec.csic.es/concurso_13/)  
EL MISTERIO DE LOS CRISTALES GIGANTES (50' - V.O. Castellano)  [https://www.trianatech.com/index.php?option=com_content&view=article&id=149&lang=es](https://www.trianatech.com/index.php?option=com_content&view=article&id=149&lang=es) 

##  Coloides
 En currículo LOMCE se añaden coloides; en algunos libros de texto no hacen una definición clara, limitándose a usar idea de fase dispersante y fase dispersa, en algunos dicen que es un tipo de homogénea /de heterogénea / situación intermedia entre ambas, e incluso en algunos ponen ejemplos como niebla y humo que usan como de varios tipos (mezcla homogénea en tabla en citan ejemplos según estados de soluto y disolvente, o coloide en otros ejemplos)  
 Una definición sencilla es que es una mezcla homogénea (partes no distinguibles a simple vista) pero sí distinguibles a microscopio electrónico (en torno a nm ó micra)  
 [http://goldbook.iupac.org/C01172.html](http://goldbook.iupac.org/C01172.html)  
 [Coloide - wikipedia](https://es.wikipedia.org/wiki/Coloide)  
 [Coloides o dispersiones coloidales](http://www.escuelapedia.com/coloides-o-dispersiones-coloidales/)  Imágenes al microscopio de leche y sangre  
 También se puede citar efecto Tyndall 
 
 
## Minas
Asociado a mezclas y a elementos se puede pensar en minas  

[For What It's Worth - dillonmarsh.com](http://dillonmarsh.com/fwiw.html)  
> Whether they are active or long dormant, mines speak of a combination of sacrifice and gain. Their features are crude, unsightly scars on the landscape - unlikely feats of hard labour and specialised engineering, constructed to extract value from the earth but also exacting a price.  
>These images combine photography and computer generated elements in an effort to visualise the output of various mines in South Africa. The CGI objects represent scale models of the materials removed from the ground. By doing so, the intention is to create a kind of visualisation of the merits and shortfalls of this industry that has shaped the history and economy of the country so radically.

![](http://dillonmarsh.com/images/copper06.jpg)  
Copperton Mine - 462,500 tonnes of copper, 934,500 tonnes of zinc, and 1,000 tonnes of silver  
![](http://dillonmarsh.com/images/copper07.jpg)  
Palabora Mine - 4.1 million tonnes of copper  
![](http://dillonmarsh.com/images/diamonds03.jpg)  
Koffiefontein Mine - 7.6 million carats of diamonds  

