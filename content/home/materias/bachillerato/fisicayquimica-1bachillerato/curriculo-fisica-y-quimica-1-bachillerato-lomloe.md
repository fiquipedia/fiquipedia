# Currículo Física y Química 1º Bachillerato (LOMLOE)

En esta página se incluye el [currículo estatal (Real Decreto)](#Estatal) y el [currículo de Madrid (Decreto)](#Madrid)

# <a name="Estatal"></a> Estatal (Real Decreto)
Revisado con [Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521), ver en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  

Actualizada [comparación currículo Física y Química 1º Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/comparacion-curriculo-fisica-y-quimica-1-bachillerato) con proyecto 9 diciembre 2021, sin cambios relevantes   

Algunos cambios diciembre 2021 vs octubre 2021:  
Se elimina "En 1.º de Bachillerato Física y Química es una materia de modalidad en el bachillerato de Ciencias y Tecnología, si bien es una modalidad optativa y su elección deja en manos del alumnado y de su familia la capacidad de decisión y la autonomía propias de un 
adolescente con criterio."  
Se cambia "profesiones que todavía no se pueden siquiera imaginar" por "profesiones que todavía no existen en el mercado laboral actual"  
Se cambia "bioquímica" por "Biología"  
Se elimina ", cálculos termoquímicos basados en la ley de Hess –lo que relaciona este bloque con el de energía–"  
Se cambia "la estructura de los mismos, incluyendo la isomería y cuál es su reactividad" por "estructura y reactividad de los mismos"  

Algunos cambios BOE abril 2021 vs diciembre 2021:  
En intro cambia "carácter optativo" por "carácter de materia de modalidad"  


El Bachillerato es una etapa de grandes retos para el alumnado, no solo por la necesidad de afrontar los cambios propios del desarrollo madurativo de los adolescentes de esta edad, sino también porque en esta etapa educativa los aprendizajes adquieren un carácter más profundo, con el fin de satisfacer la demanda de una preparación del alumnado suficiente para la vida y para los estudios posteriores. Las enseñanzas de Física y Química en Bachillerato aumentan la formación científica que el alumnado ha adquirido a lo largo de toda la Educación Secundaria Obligatoria y contribuyen de forma activa a que cada estudiante adquiera, con ello, una base cultural científica rica y de calidad que le permita desenvolverse con soltura en una sociedad que demanda perfiles científicos y técnicos para la investigación y para el mundo laboral.

La separación de las enseñanzas del Bachillerato en modalidades posibilita una especialización de los aprendizajes que configura definitivamente el perfil personal y profesional de cada alumno y alumna. Esta materia tiene como finalidad profundizar en las competencias que se han desarrollado durante toda la Educación Secundaria Obligatoria y que ya forman parte del bagaje cultural científico del alumnado, aunque su carácter de materia de modalidad le confiere también un matiz de preparación para los estudios superiores de aquellos estudiantes que deseen elegir una formación científica avanzada en el curso siguiente, en el que Física y Química se desdoblará en dos materias diferentes, una para cada disciplina científica.

El enfoque STEM que se pretende otorgar a la materia de Física y Química en toda la enseñanza secundaria y en el Bachillerato prepara a los alumnos y alumnas de forma integrada en las ciencias para afrontar un avance que se orienta a la consecución de los Objetivos de Desarrollo Sostenible. Muchos alumnos y alumnas ejercerán probablemente profesiones que todavía no existen en el mercado laboral actual, por lo que el currículo de esta materia es abierto y competencial, y tiene como finalidad no solo contribuir a profundizar en la adquisición de conocimientos, destrezas y actitudes de la ciencia, sino también encaminar al alumnado a diseñar su perfil personal y profesional de acuerdo a las que serán sus preferencias para el futuro. Para ello, el currículo de Física y Química de 1.º de Bachillerato se diseña partiendo de las competencias específicas de la materia, como eje vertebrador del resto de los elementos curriculares. Esto organiza el proceso de enseñanza y aprendizaje y dota a todo el currículo de un carácter eminentemente competencial.

A partir de las competencias específicas, este currículo presenta los criterios de evaluación. Se trata de evitar la evaluación exclusiva de conceptos, por lo que los criterios de evaluación están referidos a las competencias específicas. Para la consecución de los criterios de evaluación, el currículo de Física y Química de primero de Bachillerato organiza en bloques los saberes básicos, que son los conocimientos, destrezas y actitudes que han de ser adquiridos a lo largo del curso, buscando una continuidad y ampliación de los de la etapa anterior pero que, a diferencia de esta, no contemplan un bloque específico de saberes comunes de las destrezas científicas básicas, puesto que estos deben ser trabajados de manera transversal en todos los bloques.

El primer bloque de los saberes básicos recoge la estructura de la materia y del enlace químico, lo que es fundamental para la comprensión de estos conocimientos en este curso y el siguiente, no solo en las materias de Física y de Química, sino también en otras disciplinas científicas como la Biología.

A continuación, el bloque de reacciones químicas proporciona al alumnado un mayor número de herramientas para la realización de cálculos estequiométricos avanzados y cálculos en general con sistemas fisicoquímicos importantes, como las disoluciones y los gases ideales.

Los saberes básicos propios de Química terminan con el bloque sobre química orgánica, que se introdujo en el último curso de la Educación Secundaria Obligatoria, y que se presenta en esta etapa con una mayor profundidad incluyendo las propiedades generales de los compuestos del carbono y su nomenclatura. Esto preparará a los estudiantes para afrontar en el curso siguiente cómo es la estructura y reactividad de los mismos, algo de evidente importancia en muchos ámbitos de nuestra sociedad actual como, por ejemplo, la síntesis de fármacos y de polímeros.

Los saberes de Física comienzan con el bloque de cinemática. Para alcanzar un nivel de significación mayor en el aprendizaje con respecto a la etapa anterior, este bloque se presenta desde un enfoque vectorial, de modo que la carga matemática de esta unidad se vaya adecuando a los requerimientos del desarrollo madurativo del alumnado. Además, comprende un mayor número de movimientos que les permite ampliar las perspectivas de esta rama de la mecánica.

Igual de importante es conocer cuáles son las causas del movimiento, por eso el siguiente bloque presenta los conocimientos, destrezas y actitudes correspondientes a la estática y a la dinámica. Aprovechando el enfoque vectorial del bloque anterior, el alumnado aplica esta herramienta a describir los efectos de las fuerzas sobre partículas y sobre sólidos rígidos en lo referido al momento que produce una fuerza, deduciendo cuáles son las causas en cada caso. El hecho de centrar este bloque en la descripción analítica de las fuerzas y sus ejemplos, y no en el caso particular de las fuerzas centrales, que se incluyen en Física de 2.º de Bachillerato, permite una mayor comprensión para sentar las bases del conocimiento significativo.

Por último, el bloque de energía presenta los saberes como continuidad a los que se estudiaron en la etapa anterior, profundizando más en el trabajo, la potencia y la energía mecánica y su conservación; así como en los aspectos básicos de termodinámica que les permitan entender el funcionamiento de sistemas termodinámicos simples y sus aplicaciones más inmediatas. Todo ello encaminado a comprender la importancia del concepto de energía en nuestra vida cotidiana y en relación con otras disciplinas científicas y tecnológicas.

Este currículo de Física y Química para 1.º de Bachillerato se presenta como una propuesta integradora que afianza las bases del estudio, poniendo de manifiesto el aprendizaje competencial, y que despierta vocaciones científicas entre el alumnado. Combinado con una metodología integradora STEM se asegura el aprendizaje significativo del alumnado, lo que resulta en un mayor número de estudiantes de disciplinas científicas.

## Competencias Específicas 

Algunos cambios en proyecto 9 diciembre 2021 frente a borrador octubre 2021:  
En competencia 2 se elimina "en especial el que estudia la modalidad de Ciencias y Tecnología 
–itinerario en el que se cursa esta materia–"  
En competencia 3 se elimina "que ha optado por cursar esta materia en Bachillerato"  
En competencia 6 se cambia "acciones científicamente fundamentadas" por "acciones científicamente fundamentadas en su vida cotidiana y
entorno social."   

Algunos cambios en BOE abril 2022 frente a proyecto diciembre 2021:  
En competencia 1 se añade "toma **y registro** de datos", "de la realidad socioambiental global", y "CPSAA2." pasa a "CPSAA1.2."  
En competencia 3 se elimina "que se necesita para la construcción de una sociedad mejor."  
En competencia 5 se cambia sexo por género.  
En competencia 6 cambia transmitir por dotar, y CPSAA7 por CPSAA5. 

**1. Resolver problemas y situaciones relacionados con la física y la química, aplicando las leyes y teorías científicas adecuadas, para comprender y explicar los fenómenos naturales y evidenciar el papel de estas ciencias en la mejora del bienestar común y en la realidad cotidiana.**

Aplicar los conocimientos científicos adecuados a la explicación de los fenómenos naturales requiere la construcción de un razonamiento científico que permita la formación de pensamientos de orden superior necesarios para la construcción de significados, lo que a su vez redunda en una mejor comprensión de dichas leyes y teorías científicas en un proceso de retroalimentación. Entender de este modo los fenómenos fisicoquímicos, implica comprender las interacciones que se producen entre cuerpos y sistemas en la naturaleza, analizarlas a la luz de las leyes y teorías fisicoquímicas, interpretar los fenómenos que se originan y utilizar herramientas científicas para la toma y registro de datos y su análisis crítico para la construcción de nuevo conocimiento científico.

El desarrollo de esta competencia requiere el conocimiento de las formas y procedimientos estándar que se utilizan en la investigación científica del mundo natural y permite al alumnado, a su vez, forjar una opinión informada en los aspectos que afectan a su realidad cercana para actuar con sentido crítico en su mejora a través del conocimiento científico adquirido. Así pues, el desarrollo de esta competencia específica permite detectar los problemas del entorno cotidiano y de la realidad socioambiental global, y abordarlos desde la perspectiva de la física y de la química, buscando soluciones sostenibles que repercutan en el bienestar social común.

Esta competencia específica se conecta con los siguientes descriptores: STEM1, STEM2, STEM5, CPSAA1.2.

**2. Razonar con solvencia, usando el pensamiento científico y las destrezas relacionadas con el trabajo de la ciencia, para aplicarlos a la observación de la naturaleza y el entorno, a la formulación de preguntas e hipótesis y a la validación de las mismas a través de la experimentación, la indagación y la búsqueda de evidencias.**

El alumnado ha de desarrollar habilidades para observar desde una óptica científica los fenómenos naturales y para plantearse sus posibles explicaciones a partir de los procedimientos que caracterizan el trabajo científico, particularmente en las áreas de la física y de la química. Esta competencia específica contribuye a lograr el desempeño de investigar los fenómenos naturales a través de la experimentación, la búsqueda de evidencias y el razonamiento científico, haciendo uso de los conocimientos que el alumnado adquiere en su formación. Las destrezas que ha adquirido en etapas anteriores le permiten utilizar en Bachillerato la metodología científica con mayor rigor y obtener conclusiones y respuestas de mayor alcance y mejor elaboradas.

El alumnado competente establece continuamente relaciones entre lo meramente académico y las vivencias de su realidad cotidiana, lo que le permite encontrar las relaciones entre las leyes y las teorías que aprenden y los fenómenos que observan en el mundo que les rodea. De esta manera, las cuestiones que plantean y las hipótesis que formulan están elaboradas de acuerdo con conocimientos fundamentados y ponen en evidencia las relaciones entre las variables que estudian en términos matemáticos y las principales leyes de la física y la química. Así, las conclusiones y explicaciones que se proporcionan son coherentes con las teorías científicas conocidas.

Esta competencia específica se conecta con los siguientes descriptores: STEM1, STEM2, CPSAA4, CE1.

**3. Manejar con propiedad y solvencia el flujo de información en los diferentes registros de comunicación de la ciencia como la nomenclatura de compuestos químicos, el uso del lenguaje matemático, el uso correcto de las unidades de medida, la seguridad en el trabajo experimental, para la producción e interpretación de información en diferentes formatos y a partir de fuentes diversas.**

Para lograr una completa formación científica del alumnado es necesario adecuar el nivel de exigencia al evaluar sus destrezas para la comunicación científica. Para ello, el desarrollo de esta competencia en esta etapa educativa pretende que los alumnos y alumnas comprendan la información que se les proporciona sobre los fenómenos fisicoquímicos que ocurren en el mundo cotidiano, sea cual sea el formato en el que les sea proporcionada, y produzcan nueva información con corrección, veracidad y fidelidad, utilizando correctamente el lenguaje matemático, los sistemas de unidades, las normas de la IUPAC y la normativa de seguridad de los laboratorios científicos, con la finalidad de reconocer el valor universal del lenguaje científico en la transmisión de conocimiento.

El correcto uso del lenguaje científico universal y la soltura a la hora de interpretar y producir información de carácter científico permiten a cada estudiante crear relaciones constructivas entre la física, la química y las demás disciplinas científicas y no científicas que son propias de otras áreas de conocimiento que se estudian en el Bachillerato. Además, prepara a los estudiantes para establecer también conexiones con una comunidad científica activa, preocupada por conseguir una mejora de la sociedad que repercuta en aspectos tan importantes como la conservación del medioambiente y la salud individual y colectiva, lo que dota a esta competencia específica de un carácter esencial para este currículo.

Esta competencia específica se conecta con los siguientes descriptores: CCL1, CCL5, STEM4, CD2.

**4. Utilizar de forma autónoma, crítica y eficiente plataformas digitales y recursos variados, tanto para el trabajo individual como en equipo, consultando y seleccionando información científica veraz, creando materiales en diversos formatos y comunicando de manera efectiva en diferentes entornos de aprendizaje, para fomentar la creatividad, el desarrollo personal y el aprendizaje individual y social.**

El desarrollo de las competencias científicas requiere el acceso a diversidad de fuentes de información para la selección y utilización de recursos didácticos, tanto tradicionales como digitales. En la actualidad muchos de los recursos necesarios para la enseñanza y el aprendizaje de la física y la química pueden encontrarse en distintas plataformas digitales de contenidos, por lo que su uso autónomo facilita el desarrollo de procesos cognitivos de nivel superior y propicia la comprensión, la elaboración de juicios, la creatividad y el desarrollo personal. Su uso crítico y eficiente implica la capacidad de seleccionar, entre los distintos recursos existentes, aquellos que resultan veraces y adecuados para las necesidades de formación, ajustados a las tareas que se están desempeñando y al tiempo disponible.

A su vez, es necesaria la autonomía, responsabilidad y uso crítico de las plataformas digitales y sus diferentes entornos de aprendizaje como, por ejemplo, las herramientas de comunicación para el trabajo colaborativo mediante el intercambio de ideas y contenidos, citando las fuentes y respetando los derechos de autor, a partir de documentos en distintos formatos de modo que se favorezca el aprendizaje social. Para esto, es necesario que el alumnado aprenda a producir materiales tradicionales o digitales que ofrezcan un valor, no solo para sí mismos, sino también para el resto de la sociedad.

Esta competencia específica se conecta con los siguientes descriptores: STEM3, CD1, CD3, CPSAA3.2, CE2.

**5. Trabajar de forma colaborativa en equipos diversos, aplicando habilidades de coordinación, comunicación, emprendimiento y reparto equilibrado de responsabilidades, para predecir las consecuencias de los avances científicos y su influencia sobre la salud propia y comunitaria y sobre el desarrollo medioambiental sostenible.**

El aprendizaje de la física y de la química, en lo referido a métodos de trabajo, leyes y teorías más importantes, y las relaciones entre ellas, el resto de las ciencias y la tecnología, la sociedad y el medioambiente, implica que el alumnado desarrolle una actitud comprometida en el trabajo experimental y el desarrollo de proyectos de investigación en equipo, adopte ciertas posiciones éticas y sea consciente de los compromisos sociales que se infieren de estas relaciones.

Además, el proceso de formación en ciencias implica el trabajo activo integrado con la lectura, la escritura, la expresión oral, la tecnología y las matemáticas. El desarrollo de todas estas destrezas de forma integral tiene mucho más sentido si se realiza en colaboración dentro de un grupo diverso que respete las diferencias de género, orientación, ideología, etc., en el que forman parte no solo la cooperación, sino también la comunicación, el debate y el reparto consensuado de responsabilidades. Las ideas que se plantean en el trabajo de estos equipos son validadas a través de la argumentación y es necesario el acuerdo común para que el colectivo las acepte, al igual que sucede en la comunidad científica, en la que el consenso es un requisito para la aceptación universal de las nuevas ideas, experimentos y descubrimientos. No se deben olvidar, por otra parte, las ventajas de desarrollar el trabajo colaborativo por la interdependencia positiva entre los miembros del equipo, la complementariedad, la responsabilidad compartida, la evaluación grupal, etc., que se fomentan a través del desarrollo de esta competencia específica.

Esta competencia específica se conecta con los siguientes descriptores: STEM3, STEM5, CPSAA3.1, CPSAA3.2.

**6. Participar de forma activa en la construcción colectiva y evolutiva del conocimiento científico, en su entorno cotidiano y cercano, para convertirse en agentes activos de la difusión del pensamiento científico, la aproximación escéptica a la información científica y tecnológica y la puesta en valor de la preservación del medioambiente y la salud pública, el desarrollo económico y la búsqueda de una sociedad igualitaria.**

Por último, esta competencia específica pretende dotar al alumnado de la destreza para decidir con criterios científicamente fundamentados y valorar la repercusión técnica, social, económica y medioambiental de las distintas aplicaciones que tienen los avances, las investigaciones y los descubrimientos que la comunidad científica acomete en el transcurso de la historia, con la finalidad de construir ciudadanos y ciudadanas competentes comprometidos con el mundo en el que viven. El conocimiento y explicación de los aspectos más importantes para la sociedad de la ciencia y la tecnología permite valorar críticamente cuáles son las repercusiones que tienen, y así el alumnado puede tener mejores criterios a la hora de tomar decisiones sobre los usos adecuados de los medios y productos científicos y tecnológicos que la sociedad pone a su disposición.

Asimismo, esta competencia específica se desarrolla a través de la participación activa del alumnado en proyectos que involucren la toma de decisiones y la ejecución de acciones científicamente fundamentadas en su vida cotidiana y entorno social. Con ello mejora la conciencia social de la ciencia, algo que es necesario para construir una sociedad del conocimiento más avanzada.

Esta competencia específica se conecta con los siguientes descriptores: STEM3, STEM4, STEM5, CPSAA5, CE2.
 
## Criterios de evaluación

Algunos cambios BOE abril 2022 vs diciembre 2021:  
En 6 se añade "la resolución de los grandes retos ambientales"  

### Competencia específica 1  
1.1 Aplicar las leyes y teorías científicas en el análisis de fenómenos fisicoquímicos cotidianos, comprendiendo las causas que los producen y explicándolas utilizando diversidad de soportes y medios de comunicación.

1.2 Resolver problemas fisicoquímicos planteados a partir de situaciones cotidianas, aplicando las leyes y teorías científicas para encontrar y argumentar las soluciones, expresando adecuadamente los resultados.

1.3 Identificar situaciones problemáticas en el entorno cotidiano, emprender iniciativas y buscar soluciones sostenibles desde la física y la química, analizando críticamente el impacto producido en la sociedad y el medioambiente.

 
### Competencia específica 2
2.1 Formular y verificar hipótesis como respuestas a diferentes problemas y observaciones, manejando con soltura el trabajo experimental, la indagación, la búsqueda de evidencias y el razonamiento lógico-matemático.

2.2 Utilizar diferentes métodos para encontrar la respuesta a una sola cuestión u observación, cotejando los resultados obtenidos y asegurándose así de su coherencia y fiabilidad.

2.3 Integrar las leyes y teorías científicas conocidas en el desarrollo del procedimiento de la validación de las hipótesis formuladas, aplicando relaciones cualitativas y cuantitativas entre las diferentes variables, de manera que el proceso sea más fiable y coherente con el conocimiento científico adquirido.

### Competencia específica 3  

3.1 Utilizar y relacionar de manera rigurosa diferentes sistemas de unidades, empleando correctamente su notación y sus equivalencias, haciendo posible una comunicación efectiva con toda la comunidad científica.

3.2 Nombrar y formular correctamente sustancias simples, iones y compuestos químicos inorgánicos y orgánicos utilizando las normas de la IUPAC, como parte de un lenguaje integrador y universal para toda la comunidad científica.

3.3 Emplear diferentes formatos para interpretar y expresar información relativa a un proceso fisicoquímico concreto, relacionando entre sí la información que cada uno de ellos contiene y extrayendo de él lo más relevante durante la resolución de un problema.

3.4 Poner en práctica los conocimientos adquiridos en la experimentación científica en laboratorio o campo, incluyendo el conocimiento de sus materiales y su normativa básica de uso, así como de las normas de seguridad propias de estos espacios, y comprendiendo la importancia en el progreso científico y emprendedor de que la experimentación sea segura, sin comprometer la integridad física propia ni colectiva.

### Competencia específica 4

4.1 Interactuar con otros miembros de la comunidad educativa a través de diferentes entornos de aprendizaje, reales y virtuales, utilizando de forma autónoma y eficiente recursos variados, tradicionales y digitales, con rigor y respeto y analizando críticamente las aportaciones de todo el mundo.

4.2 Trabajar de forma autónoma y versátil, individualmente y en equipo, en la consulta de información y la creación de contenidos, utilizando con criterio las fuentes y herramientas más fiables, y desechando las menos adecuadas, mejorando así el aprendizaje propio y colectivo.

### Competencia específica 5  

5.1 Participar de manera activa en la construcción del conocimiento científico, evidenciando la presencia de la interacción, la cooperación y la evaluación entre iguales, mejorando el cuestionamiento, la reflexión y el debate al alcanzar el consenso en la resolución de un problema o situación de aprendizaje.

5.2 Construir y producir conocimientos a través del trabajo colectivo, además de explorar alternativas para superar la asimilación de conocimientos ya elaborados y encontrando momentos para el análisis, la discusión y la síntesis, obteniendo como resultado la elaboración de productos representados en informes, pósteres, presentaciones, artículos, etc.

5.3 Debatir, de manera informada y argumentada, sobre las diferentes cuestiones medioambientales, sociales y éticas relacionadas con el desarrollo de las ciencias, alcanzando un consenso sobre las consecuencias de estos avances y proponiendo soluciones creativas en común a las cuestiones planteadas.

### Competencia específica 6

6.1 Identificar y argumentar científicamente las repercusiones de las acciones que el alumno o alumna emprende en su vida cotidiana, analizando cómo mejorarlas como forma de participar activamente en la construcción de una sociedad mejor.

6.2 Detectar las necesidades de la sociedad sobre las que aplicar los conocimientos científicos adecuados que ayuden a mejorarla, incidiendo especialmente en aspectos importantes como la resolución de los grandes retos ambientales, el desarrollo sostenible y la promoción de la salud.

## Saberes básicos 
Algunos cambios diciembre 2021 vs octubre 2021:  
A: cambia "Formulación y nomenclatura" por "nomenclatura"  
B: cambia "Interpretación de la estequiometría y la termoquímica" por "Estequiometría de las reacciones químicas"  
C: se elimina "para establecer un lenguaje universal de comunicación entre las distintas comunidades científicas."  
D: se elimina "exponiendo argumentos de forma razonada y elaborando hipótesis que puedan ser comprobadas mediante la experimentación y el 
razonamiento científico."  
E: se elimina "como parte del proceso de verificación de hipótesis por medio del razonamiento científico y la experimentación."  

Algunos cambios BOE abril 2022 vs diciembre 2021:  
- Se añade: "partícula o un sólido rígido" cambia por "partícula **y** un sólido rígido **bajo la acción de un par de fuerzas.**"  
- Se elimina: "mecánica vectorial aplicada sobre una partícula **o un sólido rígido** con su estado de reposo o de movimiento" cambia por "mecánica vectorial aplicada sobre una partícula con su estado de reposo o de movimiento"   
 
## Saberes básicos

**A. Enlace químico y estructura de la materia.**

- Desarrollo de la tabla periódica: contribuciones históricas a su elaboración actual e importancia como herramienta predictiva de las propiedades de los elementos.

- Estructura electrónica de los átomos tras el análisis de su interacción con la radiación electromagnética: explicación de la posición de un elemento en la tabla periódica y de la similitud en las propiedades de los elementos químicos de cada grupo.

- Teorías sobre la estabilidad de los átomos e iones: predicción de la formación de enlaces entre los elementos, representación de estos y deducción de cuáles son las propiedades de las sustancias químicas. Comprobación a través de la observación y la experimentación.

- Nomenclatura de sustancias simples, iones y compuestos químicos inorgánicos: composición y aplicaciones en la vida cotidiana.

**B. Reacciones químicas.**

- Leyes fundamentales de la química: relaciones estequiométricas en las reacciones químicas y en la composición de los compuestos. Resolución de cuestiones cuantitativas relacionadas con la química en la vida cotidiana.

- Clasificación de las reacciones químicas: relaciones que existen entre la química y aspectos importantes de la sociedad actual como, por ejemplo, la conservación del medioambiente o el desarrollo de fármacos.

- Cálculo de cantidades de materia en sistemas fisicoquímicos concretos, como gases ideales o disoluciones y sus propiedades: variables mesurables propias del estado de los mismos en situaciones de la vida cotidiana.

- Estequiometría de las reacciones químicas: aplicaciones en los procesos industriales más significativos de la ingeniería química.

**C. Química orgánica.**

- Propiedades físicas y químicas generales de los compuestos orgánicos a partir de las estructuras químicas de sus grupos funcionales: generalidades en las diferentes series homólogas y aplicaciones en el mundo real.

- Reglas de la IUPAC para formular y nombrar correctamente algunos compuestos orgánicos mono y polifuncionales (hidrocarburos, compuestos oxigenados y compuestos nitrogenados).

**D. Cinemática.**

- Variables cinemáticas en función del tiempo en los distintos movimientos que puede tener un objeto, con o sin fuerzas externas: resolución de situaciones reales relacionadas con la física y el entorno cotidiano.

- Variables que influyen en un movimiento rectilíneo y circular: magnitudes y unidades empleadas. Movimientos cotidianos que presentan estos tipos de trayectoria.

- Relación de la trayectoria de un movimiento compuesto con las magnitudes que lo describen.

**E. Estática y dinámica.**

- Predicción, a partir de la composición vectorial, del comportamiento estático o dinámico de una partícula y un sólido rígido bajo la acción de un par de fuerzas.

- Relación de la mecánica vectorial aplicada sobre una partícula con su estado de reposo o de movimiento: aplicaciones estáticas o dinámicas de la física en otros campos, como la ingeniería o el deporte.

- Interpretación de las leyes de la dinámica en términos de magnitudes como el momento lineal y el impulso mecánico: aplicaciones en el mundo real.

**F. Energía.**

- Conceptos de trabajo y potencia: elaboración de hipótesis sobre el consumo energético de sistemas mecánicos o eléctricos del entorno cotidiano y su rendimiento.

- Energía potencial y energía cinética de un sistema sencillo: aplicación a la conservación de la energía mecánica en sistemas conservativos y no conservativos y al estudio de las causas que producen el movimiento de los objetos en el mundo real.

- Variables termodinámicas de un sistema en función de las condiciones: determinación de las variaciones de temperatura que experimenta y las transferencias de energía que se producen con su entorno.

# <a name="Madrid"></a> Madrid (Decreto)

Revisado con [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF)  
Física y Química 1º Bachillerato en página 145 del pdf  
Revisado con [ACUERDO de 13 de septiembre de 2023, del Consejo de Gobierno, por el que se procede a la corrección de errores del Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del bachillerato.](https://bocm.es/boletin/CM_Orden_BOCM/2023/09/15/BOCM-20230915-1.PDF)  

Cambios en [Proyecto de decreto 31 mayo](https://www.comunidad.madrid/transparencia/sites/default/files/04_2_2022-05-31_decreto_bachillerato-a.pdf)  
Modifica texto inicial reordena texto, elimina y añade.  
Se intentan marcar en negrita cambios o añadidos.   
Los cuatro párrafos iniciales cambian bastante y no se marcan diferencia.  
Cambia "saberes" por "contenidos"  
Cambia "que se incluyen" por "que serán objeto de estudio"  
Se elimina "significativo"  
**ERRATA GRAVE** Cambian "caso particular" por "estudio de las partículas"  
Cambia "bloque presenta los conocimientos, destrezas y actitudes correspondientes a la estática y a la dinámica." por "siguiente bloque, «Estática y dinámica», presenta los conceptos fundamentales de estas dos ciencias."  
Se elimina "Para alcanzar un nivel de significación mayor en el aprendizaje con respecto a la etapa anterior,"  
El párrafo final original que menciona STEM se cambia por dos, en el último se habla de situación de aprendizaje con otras materias.   

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
Se elimina "formulación y nomenclatura" y se indica solo "nomenclatura"  (es algo que sugerí en elegaciones)  
Se cambia "(incluyendo la isomería)" por "(incluyendo la isomería cis-trans)"  
Se elimina errata de proyecto que ponía "estudio de las partículas" en lugar de "caso particular" y se deja ese párrafo tal y como es originalmente, incorporando de nuevo "significativo"  
Se cambia "se recomienda poner en práctica situaciones de aprendizaje o actividades competenciales" por "se recomienda poner en práctica actividades competenciales"  
En párrafo final se cambia dos veces "situación de aprendizaje" por "actividad"  


Física y Química es una materia de modalidad en el Bachillerato de Ciencias y Tecnología, la cual tiene como finalidad profundizar sobre las competencias que se han abarcado durante toda la Educación Secundaria Obligatoria y que forman parte del bagaje cultural científico del alumnado.  
Esta materia servirá de preparación al alumnado para los estudios superiores de aquellos estudiantes que deseen elegir una formación científica avanzada en el curso siguiente, un curso en el cual Física y Química se desdoblará en dos materias diferentes, una para cada disciplina científica.  
El currículo de Física y Química de primero de Bachillerato pretende no solo contribuir en la profundización de la adquisición de conocimientos, destrezas y actitudes de la ciencia, sino también adquirir y poner en práctica el pensamiento científico para enfrentarse a los posibles problemas de la sociedad que nos rodea y disfrutar de un conocimiento más profundo del mundo. De ahí su propuesta integradora: que afiance las bases del estudio y realmente ponga de manifiesto el aprendizaje competencial de los alumnos, despertando vocaciones científicas entre ellos.  
Los contenidos se encuentran organizados en seis bloques que buscan una continuidad yampliación de aquellos de la etapa anterior, pero, a diferencia de esta, no se contempla un bloque específico de contenidos comunes sobre las destrezas científicas básicas, sino que al haber sido adquiridas estas por los alumnos previamente deben ser trabajadas de manera transversal en todos los apartados de la materia.  
En el primer bloque, **llamado «Enlace químico y estructura de la materia», se retoma el estudio** de la estructura de la materia y del enlace químico, lo **cual** es fundamental para la **adecuada adquisición** de conocimientos en este curso y el siguiente, no solo en las materias de Física y de Química sino también en otras disciplinas científicas **que se apoyan en estos contenidos y que pueden ser elegidas en el futuro por el alumno como, por ejemplo**, Biología **en segundo curso de Bachillerato**.  
A continuación, el bloque «Reacciones Químicas» **profundiza en lo que el alumnado ha aprendido durante la etapa de Educación Secundaria Obligatoria, proporcionándole** un mayor número de herramientas para la realización de cálculos estequiométricos avanzados, **cálculos termoquímicos basados en la ley de Hess (lo que relaciona este bloque con el de «Energía»)**, y cálculos en general con sistemas fisicoquímicos importantes, como las disoluciones y los gases ideales.  
Los **contenidos** de Química terminan con el bloque «Química orgánica», que se introdujo en el último curso de la Educación Secundaria Obligatoria, y que se **aborda** en esta etapa con mayor profundidad. **Los objetivos fundamentales de este bloque son dos: conocer** las propiedades generales de los compuestos del carbono y **dominar** su **formulación y** nomenclatura. Esto preparará a los estudiantes para afrontar en el curso siguiente cómo es la estructura de los mismos, **(incluyendo la isomería) y cuál es su reactividad**, algo de evidente importancia en muchos ámbitos de nuestra sociedad actual como, por **nombrar un** ejemplo, la síntesis de fármacos y de polímeros, **y también para entender otras disciplinas como Biología.**  
Los **contenidos** de Física comienzan con **un estudio profundo** de «Cinemática». En este curso este bloque se **trabaja** desde un enfoque vectorial, de modo que la carga matemática de esta unidad se vaya adecuando a los requerimientos del desarrollo madurativo del alumnado. Además, **el estudio** de un mayor número de movimientos le permite ampliar las perspectivas de esta rama de la mecánica.  
Igual de importante es conocer cuáles son las causas del movimiento, por eso el siguiente bloque, «Estática y dinámica», presenta los conceptos fundamentales de estas dos ciencias. Igual de importante es conocer cuáles son las causas del movimiento, por eso el siguiente bloque, «Estática y dinámica», presenta los conceptos fundamentales de estas dos ciencias. Aprovechando el estudio vectorial del bloque anterior, el alumnado aplica esta herramienta para describir los efectos de las fuerzas sobre las partículas y sobre los sólidos rígidos en lo referido al estudio del momento que produce una fuerza, deduciendo cuáles son las causas en cada caso. El hecho de centrar este bloque en la descripción analítica de las fuerzas y sus ejemplos, y no en el caso particular de las fuerzas centrales (que serán objeto de estudio en Física de **segundo** de Bachillerato), permite una mayor comprensión para sentar las bases del conocimiento significativo.  
**Para cerrar la materia**, el bloque **llamado** «Energía» presenta **contenidos** como continuidad de los que se estudiaron en la etapa anterior, profundizando más en el trabajo, la potencia y la energía mecánica y su conservación; así como en los aspectos básicos de Termodinámica que permiten entender el funcionamiento de sistemas termodinámicos simples y sus aplicaciones más inmediatas. Todo ello encaminado a comprender la importancia del concepto de energía en nuestra vida cotidiana, y en relación con otras disciplinas científicas y tecnológicas.  
**De esta forma, se podría plantear trabajar de manera interdisciplinar los contenidos de los bloques D y E, «Cinemática» y «Estática y dinámica» respectivamente, junto con el bloque A de la asignatura Educación Física, llamado «Vida activa y saludable» – cuyo contenido recoge «Prácticas de actividad física con efectos positivos sobre la salud personal y colectiva: la práctica de la bicicleta como medio de transporte habitual» –, y con el bloque B, llamado «Materiales y fabricación», de la materia Tecnología e Ingeniería I a través de la siguiente actividad: los alumnos, divididos en grupos, analizarían las variables cinemáticas que intervienen en los movimientos propios del ciclismo, investigando el efecto de cambiar la posición del centro de masas del sistema bicicleta-cuerpo al modificar la altura del sillín de la bicicleta. También se puede investigar qué sucede cuando se utiliza el freno delantero, el trasero o ambos en un movimiento. Del mismo modo, se realizarían cálculos cinemáticos sobre otros deportes para que comprendan e integren los contenidos adquiridos en la materia. Esta actividad contribuiría a desarrollar las competencias específicas 1, 2, 3 y 5 de la asignatura.**
 
## Competencias específicas

Comparación proyecto Decreto Madrid frente a Real Decreto:  
Se marca en negrita cambios y añadidos: en todas se añade "**recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**"  
En competencia específica 1 se elimina "a su vez", y se cambia "sociambiental" por "social y ambiental"  
En texto inicial competencia 3 se cambia "nomenclatura" por "formulación y nomenclatura"  
En competencia 3 se cambia "alumnos y alumnas" por "alumnos"   
En competencia 3 se elimina "individual y colectiva"  
En texto inicial competencia 4 se elimina "individual y social"  
En competencia 4 se cambia "colaborativo" por "en grupo"  
En texto inicial competencia 5 se cambia "de forma colaborativa en equipos" por "en colaboración con equipos", y se cambia "salud propia y comunitaria y sobre el desarrollo medioambiental sostenible" por "salud y sobre el entorno"  
En competencia 5 se elimina "diverso que respete las diferencias de género, orientación, ideología, etc.", "consensuado"  
En competencia 5 se cambia "colaborativo" por "en grupo"  
En texto inicial competencia 6 se elimina "colectiva y evolutiva" y "y la puesta en valor de la preservación del medioambiente y la salud pública, el desarrollo económico y la búsqueda de una sociedad igualitaria."  
En competencia 6 se elimina ", con la finalidad de construir ciudadanos y ciudadanas competentes comprometidos con el mundo en el que viven", "activa", y "Con ello mejora la conciencia social de la ciencia, algo que es necesario para construir una sociedad del conocimiento más avanzada."  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
Sin cambios en 1, 2, 3 ni 4.  
En 5 cambia título "Trabajar en colaboración con equipos" por "Trabajar en equipos"  
En 6 cambia "participación" por "participación activa"  

 
**1. Resolver problemas y situaciones relacionados con la física y la química, aplicando las leyes y teorías científicas adecuadas, para comprender y explicar los fenómenos naturales y evidenciar el papel de estas ciencias en la mejora del bienestar común y en la realidad cotidiana.**

Aplicar los conocimientos científicos adecuados a la explicación de los fenómenos naturales requiere la construcción de un razonamiento científico que permita la formación de pensamientos de orden superior necesarios para la construcción de significados, lo que redunda en una mejor comprensión de dichas leyes y teorías científicas en un proceso de retroalimentación. Entender de este modo los fenómenos fisicoquímicos, implica comprender las interacciones que se producen entre cuerpos y sistemas en la naturaleza, analizarlas a la luz de las leyes y teorías fisicoquímicas, interpretar los fenómenos que se originan y utilizar herramientas científicas para la toma y registro de datos y su análisis crítico para la construcción de nuevo conocimiento científico.

El desarrollo de esta competencia requiere el conocimiento de las formas y procedimientos estándar que se utilizan en la investigación científica del mundo natural y permite al alumnado, a su vez, forjar una opinión informada en los aspectos que afectan a su realidad cercana para actuar con sentido crítico en su mejora a través del conocimiento científico adquirido. Así pues, el desarrollo de esta competencia específica permite detectar los problemas del entorno cotidiano y de la realidad **social y ambiental** global, y abordarlos desde la perspectiva de la física y de la química, buscando soluciones sostenibles que repercutan en el bienestar social común.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM1, STEM2, STEM5, CPSAA1.2.

**2. Razonar con solvencia, usando el pensamiento científico y las destrezas relacionadas con el trabajo de la ciencia, para aplicarlos a la observación de la naturaleza y el entorno, a la formulación de preguntas e hipótesis y a la validación de las mismas a través de la experimentación, la indagación y la búsqueda de evidencias.**

El alumnado ha de desarrollar habilidades para observar desde una óptica científica los fenómenos naturales y para plantearse sus posibles explicaciones a partir de los procedimientos que caracterizan el trabajo científico, particularmente en las áreas de la física y de la química. Esta competencia específica contribuye a lograr el desempeño de investigar los fenómenos naturales a través de la experimentación, la búsqueda de evidencias y el razonamiento científico, haciendo uso de los conocimientos que el alumnado adquiere en su formación. Las destrezas que ha adquirido en etapas anteriores le permiten utilizar en Bachillerato la metodología científica con mayor rigor y obtener conclusiones y respuestas de mayor alcance y mejor elaboradas.

El alumnado competente establece continuamente relaciones entre lo meramente académico y las vivencias de su realidad cotidiana, lo que le permite encontrar las relaciones entre las leyes y las teorías que aprenden y los fenómenos que observan en el mundo que les rodea. De esta
manera, las cuestiones que plantean y las hipótesis que formulan están elaboradas de acuerdo con conocimientos fundamentados y ponen en evidencia las relaciones entre las variables que estudian en términos matemáticos y las principales leyes de la física y la química. Así, las conclusiones y explicaciones que se proporcionan son coherentes con las teorías científicas conocidas.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM1, STEM2, CPSAA4, CE1.

**3. Manejar con propiedad y solvencia el flujo de información en los diferentes registros de comunicación de la ciencia como la formulación y nomenclatura de compuestos químicos, el uso del lenguaje matemático, el uso correcto de las unidades de medida, la seguridad en el trabajo experimental, para la producción e interpretación de información en diferentes formatos y a partir de fuentes diversas.**

Para lograr una completa formación científica del alumnado es necesario adecuar el nivel de exigencia al evaluar sus destrezas para la comunicación científica. Para ello, el desarrollo de esta competencia en esta etapa educativa pretende que los alumnos comprendan la información que se les proporciona sobre los fenómenos fisicoquímicos que ocurren en el mundo cotidiano, sea cual sea el formato en el que les sea proporcionada, y produzcan nueva información con corrección, veracidad y fidelidad, utilizando correctamente el lenguaje matemático, los sistemas de unidades, las normas de la IUPAC y la normativa de seguridad de los laboratorios científicos, con la finalidad de reconocer el valor universal del lenguaje científico en la transmisión de conocimiento.

El correcto uso del lenguaje científico universal y la soltura a la hora de interpretar y producir información de carácter científico permiten a cada estudiante crear relaciones constructivas entre la física, la química y las demás disciplinas científicas y no científicas que son propias de otras áreas de conocimiento que se estudian en el Bachillerato. Además, prepara a los estudiantes para establecer también conexiones con una comunidad científica activa, preocupada por conseguir una mejora de la sociedad que repercuta en aspectos tan importantes como la conservación del medioambiente y la salud, lo que dota a esta competencia específica de un carácter esencial para este currículo.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: CCL1, CCL5, STEM4, CD2.

**4. Utilizar de forma autónoma, crítica y eficiente plataformas digitales y recursos variados, tanto para el trabajo individual como en equipo, consultando y seleccionando información científica veraz, creando materiales en diversos formatos y comunicando de manera efectiva en diferentes entornos de aprendizaje, para fomentar la creatividad, el desarrollo personal y el aprendizaje.**

El desarrollo de las competencias científicas requiere el acceso a diversidad de fuentes de información para la selección y utilización de recursos didácticos, tanto tradicionales como digitales. En la actualidad muchos de los recursos necesarios para la enseñanza y el aprendizaje de la física y la química pueden encontrarse en distintas plataformas digitales de contenidos, por lo que su uso autónomo facilita el desarrollo de procesos cognitivos de nivel superior y propicia la comprensión, la elaboración de juicios, la creatividad y el desarrollo personal. Su uso crítico y eficiente implica la capacidad de seleccionar, entre los distintos recursos existentes, aquellos que resultan veraces y adecuados para las necesidades de formación, ajustados a las tareas que se están desempeñando y al tiempo disponible.

A su vez, es necesaria la autonomía, responsabilidad y uso crítico de las plataformas digitales y sus diferentes entornos de aprendizaje como, por ejemplo, las herramientas de comunicación para el trabajo **en grupo** mediante el intercambio de ideas y contenidos, citando las fuentes y respetando los derechos de autor, a partir de documentos en distintos formatos de modo que se favorezca el aprendizaje social. Para esto, es necesario que el alumnado aprenda a producir materiales
tradicionales o digitales que ofrezcan un valor, no solo para sí mismos, sino también para el resto de la sociedad.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM3, CD1, CD3, CPSAA3.2, CE2.

**5. Trabajar en equipos diversos, aplicando habilidades de coordinación, comunicación, emprendimiento y reparto equilibrado de responsabilidades, para predecir las consecuencias de los avances científicos y su influencia sobre la salud y sobre el entorno.**

El aprendizaje de la física y de la química, en lo referido a métodos de trabajo, leyes y teorías más importantes, y las relaciones entre ellas, el resto de las ciencias y la tecnología, la sociedad y el medioambiente, implica que el alumnado desarrolle una actitud comprometida en el trabajo experimental y el desarrollo de proyectos de investigación en equipo, adopte ciertas posiciones éticas y sea consciente de los compromisos sociales que se infieren de estas relaciones.

Además, el proceso de formación en ciencias implica el trabajo activo integrado con la lectura, la escritura, la expresión oral, la tecnología y las matemáticas. El desarrollo de todas estas destrezas de forma integral tiene mucho más sentido si se realiza en colaboración dentro de un grupo, en el que forman parte no solo la cooperación, sino también la comunicación, el debate y el reparto de responsabilidades. Las ideas que se plantean en el trabajo de estos equipos son validadas a través de la argumentación y es necesario el acuerdo común para que el colectivo las acepte, al igual que sucede en la comunidad científica, en la que el consenso es un requisito para la aceptación universal
de las nuevas ideas, experimentos y descubrimientos. No se deben olvidar, por otra parte, las ventajas de desarrollar el trabajo en grupo por la interdependencia positiva entre los miembros del equipo, la complementariedad, la responsabilidad compartida, la evaluación grupal, etc., que se fomentan a través del desarrollo de esta competencia específica.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM3, STEM5, CPSAA3.1, CPSAA3.2.

**6. Participar de forma activa en la construcción del conocimiento científico, en su entorno cotidiano y cercano, para convertirse en agentes activos de la difusión del pensamiento científico, la aproximación escéptica a la información científica y tecnológica.**

Por último, esta competencia específica pretende dotar al alumnado de la destreza para decidir con criterios científicamente fundamentados y valorar la repercusión técnica, social, económica y medioambiental de las distintas aplicaciones que tienen los avances, las investigaciones y los descubrimientos que la comunidad científica acomete en el transcurso de la historia. El conocimiento y explicación de los aspectos más importantes para la sociedad de la ciencia y la tecnología permite valorar críticamente cuáles son las repercusiones que tienen, y así el alumnado puede tener mejores criterios a la hora de tomar decisiones sobre los usos adecuados de los medios y productos científicos y tecnológicos que la sociedad pone a su disposición.

Asimismo, esta competencia específica se desarrolla a través de la participación **activa** del alumnado en proyectos que involucren la toma de decisiones y la ejecución de acciones científicamente fundamentadas en su vida cotidiana y entorno social.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: STEM3, STEM4, STEM5, CPSAA5, CE2.

## Criterios de evaluación

Comparación proyecto Decreto Madrid frente a Real Decreto:   
En 1.3 se elimina ", analizando críticamente el impacto producido en la sociedad y el medioambiente."  
En 3.2 se elimina "integrador y"  
En 3.4 se elimina ", sin comprometer la integridad física propia ni colectiva"  
En 4.1 se elimina "y analizando críticamente las aportaciones de todo el mundo."  
En 5.2 se cambia "colectivo" por "en grupo"  
En 6.1 se cambia "alumno o alumna" por "alumno" y se elimina "como forma de participar activamente en la construcción de una sociedad mejor."  
En 6.2 se elimina "el desarrollo sostenible"  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
En general se recuperan cosas eliminadas en el proyecto  
En 1.3 se añade "analizando críticamente el impacto producido."  
En 3.4 se añade "sin comprometer la integridad física."  
En 4.1 se añade "y analizando críticamente las aportaciones ajenas."  
En 5.1 se cambia "situación de aprendizaje" por "actividad"  


### Competencia específica 1.

1.1. Aplicar las leyes y teorías científicas en el análisis de fenómenos fisicoquímicos cotidianos, comprendiendo las causas que los producen y explicándolas utilizando diversidad de soportes y medios de comunicación.

1.2. Resolver problemas fisicoquímicos planteados a partir de situaciones cotidianas, aplicando las leyes y teorías científicas para encontrar y argumentar las soluciones, expresando adecuadamente los resultados.

1.3. Identificar situaciones problemáticas en el entorno cotidiano, emprender iniciativas y buscar soluciones sostenibles desde la física y la química, analizando críticamente el impacto producido.

### Competencia específica 2.

2.1. Formular y verificar hipótesis como respuestas a diferentes problemas y observaciones, manejando con soltura el trabajo experimental, la indagación, la búsqueda de evidencias y el razonamiento lógico-matemático.

2.2. Utilizar diferentes métodos para encontrar la respuesta a una sola cuestión u observación, cotejando los resultados obtenidos y asegurándose así de su coherencia y fiabilidad.

2.3. Integrar las leyes y teorías científicas conocidas en el desarrollo del procedimiento de la validación de las hipótesis formuladas, aplicando relaciones cualitativas y cuantitativas entre las diferentes variables, de manera que el proceso sea más fiable y coherente con el
conocimiento científico adquirido.

### Competencia específica 3.

3.1. Utilizar y relacionar de manera rigurosa diferentes sistemas de unidades, empleando correctamente su notación y sus equivalencias, haciendo posible una comunicación efectiva con toda la comunidad científica.

3.2. Nombrar y formular correctamente sustancias simples, iones y compuestos químicos inorgánicos y orgánicos utilizando las normas de la IUPAC, como parte de un lenguaje universal para toda la comunidad científica.

3.3. Emplear diferentes formatos para interpretar y expresar información relativa a un proceso fisicoquímico concreto, relacionando entre sí la información que cada uno de ellos contiene y extrayendo de él lo más relevante durante la resolución de un problema.

3.4. Poner en práctica los conocimientos adquiridos en la experimentación científica en laboratorio o campo, incluyendo el conocimiento de sus materiales y su normativa básica de uso, así como de las normas de seguridad propias de estos espacios, y comprendiendo la importancia en el progreso científico y emprendedor de que la experimentación sea segura, sin comprometer la integridad física.

### Competencia específica 4.

4.1. Interactuar con otros miembros de la comunidad educativa a través de diferentes entornos de aprendizaje, reales y virtuales, utilizando de forma autónoma y eficiente recursos variados, tradicionales y digitales, con rigor y respeto, y analizando críticamente las aportaciones ajenas.

4.2. Trabajar de forma autónoma y versátil, individualmente y en equipo, en la consulta de información y la creación de contenidos, utilizando con criterio las fuentes y herramientas más fiables, y desechando las menos adecuadas, mejorando así el aprendizaje propio y colectivo.

### Competencia específica 5.

5.1. Participar de manera activa en la construcción del conocimiento científico, evidenciando la presencia de la interacción, la cooperación y la evaluación entre iguales, mejorando el cuestionamiento, la reflexión y el debate al alcanzar el consenso en la resolución de un problema o **actividad**.

5.2. Construir y producir conocimientos a través del trabajo **en grupo**, además de explorar alternativas para superar la asimilación de conocimientos ya elaborados**,** encontrando momentos para el análisis, la discusión y la síntesis, **y** obteniendo como resultado la elaboración de productos representados en informes, pósteres, presentaciones, artículos, etc.

5.3. Debatir, de manera informada y argumentada, sobre las diferentes cuestiones medioambientales, sociales y éticas relacionadas con el desarrollo de las ciencias, alcanzando un consenso sobre las consecuencias de estos avances y proponiendo soluciones creativas en común a las cuestiones planteadas.

### Competencia específica 6.

6.1. Identificar y argumentar científicamente las repercusiones de las acciones que el alumno emprende en su vida cotidiana, analizando cómo mejorarlas.

6.2. Detectar las necesidades de la sociedad sobre las que aplicar los conocimientos científicos adecuados que ayuden a mejorarla, incidiendo especialmente en aspectos importantes como la resolución de los grandes retos ambientales y la promoción de la salud.

## Contenidos

Comparación proyecto Decreto Madrid frente a Real Decreto:   
En bloque B y E se reordenan párrafos.  
En bloque C errata "las IUPAC"  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
En A se elimina "formulación y nomenclatura" y se indica solo "nomenclatura"  (es algo que sugerí en elegaciones)  
En C se elimina errata "laS IUPAC" y se pone "la IUPAC", y se pone tilde en "homóloga"  (es algo que sugerí en elegaciones)  


**A. Enlace químico y estructura de la materia.**  

- Desarrollo de la tabla periódica: contribuciones históricas a su elaboración actual e importancia como herramienta predictiva de las propiedades de los elementos.  
• **Primeros intentos de clasificación de los elementos químicos: las triadas de Döbereiner y las octavas de Newlands, entre otros.**  
• **Clasificaciones periódicas de Mendeleiev y Meyer.**  
• **La tabla periódica actual.**  
- Estructura electrónica de los átomos tras el análisis de su interacción con la radiación electromagnética: explicación de la posición de un elemento en la tabla periódica y de la similitud en las propiedades de los elementos químicos de cada grupo.  
• **Los espectros atómicos y la estructura electrónica de los átomos.**  
• **La configuración electrónica y el sistema periódico.**  
• **Propiedades periódicas de los elementos químicos: radio atómico, energía de ionización y afinidad electrónica.**  
- **Utilización de las** teorías sobre la estabilidad de los átomos e iones para predecir la formación de los enlaces entre los elementos y su representación y, a partir de ello, deducir cuáles son las propiedades de las sustancias químicas, comprobándolas por medio de la observación y la experimentación.  
• **El enlace covalente: estructuras de Lewis para el enlace covalente. La polaridad de las moléculas. Fuerzas intermoleculares. Estructura y propiedades de las sustancias con enlace covalente: sustancias moleculares y redes covalentes.**  
• **El enlace iónico. Cristales iónicos. Propiedades de los compuestos iónicos.**  
• **El enlace metálico. Estructura y propiedades. Propiedades de las sustancias con enlace metálico.**  
- Nomenclatura de sustancias simples, iones y compuestos químicos inorgánicos **mediante las normas establecidas por la IUPAC como herramienta de comunicación en la comunidad científica y reconocimiento de su** composición y **sus** aplicaciones en la vida cotidiana.  

**B. Reacciones químicas.**  

- Cálculo de cantidades de materia en sistemas fisicoquímicos concretos, como gases ideales o disoluciones y sus propiedades: variables mesurables propias del estado de los mismos en situaciones de la vida cotidiana.  
• **Constante de Avogadro. Concepto de mol. Masa atómica, masa molecular y masa fórmula. Masa molar.**  
• **Leyes de los gases ideales. Volumen molar. Condiciones normales o estándar de un gas. Ley de Dalton de las presiones parciales.**  
• **Concentración de una disolución: concentración en masa, molaridad y fracción molar.**  
- **Aplicación de las** leyes fundamentales de la química **para comprender las** relaciones estequiométricas en las reacciones químicas y en la composición de los compuestos. Resolución de cuestiones cuantitativas relacionadas con la química en la vida cotidiana.  
• **Ley de Lavoisier de conservación de la masa, ley de Proust de las proporciones definidas y ley de Dalton de las proporciones múltiples. Composición centesimal de un compuesto.**  
• **Cálculos estequiométricos en las reacciones químicas. Riqueza de un reactivo. Rendimiento de una reacción. Reactivo limitante y reactivo en exceso.**  
- **Interpretación de la** estequiometría **y la termoquímica** de las reacciones químicas **para justificar las** aplicaciones que tienen en los procesos industriales más significativos de la ingeniería química.  
• **Los sistemas termodinámicos en química. Variables de estado. Equilibrio térmico y temperatura.**  
• **Procesos a volumen y presión constantes. Concepto de Entalpía.**  
• **La ecuación termoquímica y los diagramas de entalpía.**  
• **Determinación experimental de la entalpía de reacción.**  
• **Entalpías de combustión, formación y de enlace. La ley de Hess.** 
- Clasificación de las reacciones químicas: relaciones que existen entre la química y aspectos importantes de la sociedad actual como, por ejemplo, la conservación del medioambiente o el desarrollo de fármacos.  
• **Reacciones exotérmicas y endotérmicas.**  
• **Reacciones de síntesis, sustitución, doble sustitución, descomposición y combustión.**  
• **Observación de distintos tipos de reacciones y comprobación de su estequiometría.**  
• **Importancia de las reacciones de combustión y su relación con la sostenibilidad y el medio ambiente.**  
• **Importancia de la industria química en la sociedad actual.**  

**C. Química orgánica.**  

- Propiedades físicas y químicas generales de los compuestos orgánicos a partir de las estructuras químicas de sus grupos funcionales: generalidades en las diferentes series homólogas y aplicaciones en el mundo real.  
• **Características del átomo de carbono. Enlaces sencillos, dobles y triples. Grupo funcional y serie homóloga.**  
• **Propiedades físicas y químicas generales de los hidrocarburos, los compuestos oxigenados y los nitrogenados**  
- **Estudio de las** reglas de la IUPAC para formular y nombrar correctamente algunos compuestos orgánicos mono y polifuncionales (hidrocarburos, compuestos oxigenados y compuestos nitrogenados).  

**D. Cinemática.**

- **Empleo del razonamiento lógico-matemático y la experimentación para justificar la necesidad de definir un sistema de referencia y de interpretar y describir las** variables cinemáticas en función del tiempo en los distintos movimientos que puede tener un objeto, con o sin fuerzas externas: resolución de situaciones reales relacionadas con la física y el entorno cotidiano.  
• **Variables cinemáticas: posición, desplazamiento, velocidad media e instantánea, aceleración, componentes intrínsecas de la aceleración. Carácter vectorial de estas magnitudes.**  
- **Clasificación de los movimientos y análisis de las** variables que influyen en un movimiento rectilíneo y circular: magnitudes y unidades empleadas. Movimientos cotidianos que presentan estos tipos de trayectoria.  
• **Clasificación de los movimientos en función del tipo de trayectoria y de las composiciones intrínsecas de la aceleración.**  
• **Estudio y elaboración de gráficas de movimientos a partir de observaciones experimentales y/o simulaciones interactivas.**  
• **Estudio de los movimientos rectilíneo y uniforme, rectilíneo uniformemente acelerado, circular uniforme y circular uniformemente acelerado.**  
- Relación de la trayectoria de un movimiento compuesto con las magnitudes que lo describen, **exponiendo argumentos de forma razonada y elaborando hipótesis que puedan ser comprobadas mediante la experimentación y el razonamiento científico.**  
• **Relatividad de Galileo.**  
• **Composición de movimientos: tiro horizontal y tiro oblicuo.**  

**E. Estática y dinámica.**

- Predicción, a partir de la composición vectorial, del comportamiento estático o dinámico de una partícula y un sólido rígido bajo la acción de un par de fuerzas.  
• **Composición vectorial de un sistema de fuerzas. Fuerza resultante.**  
• **La fuerza peso y la fuerza normal. Centro de gravedad de los cuerpos. La fuerza de rozamiento. La fuerza tensión. Determinación experimental de fuerzas en relación con sus efectos.**  
• **La fuerza elástica. Ley de Hooke.**  
• **La fuerza centrípeta. Dinámica del movimiento circular.**  
• **Leyes de Newton de la dinámica. Condiciones de equilibrio de traslación.**  
• **Concepto de sólido rígido. Momentos y pares de fuerzas. Condiciones de equilibrio de rotación.**  
- Interpretación de las leyes de la dinámica en términos de magnitudes como el momento lineal y el impulso mecánico: aplicaciones en el mundo real.  
• **Momento lineal e impulso mecánico. Relación entre ambas magnitudes. Conservación del momento lineal.**  
• **Reformulación de las leyes de la dinámica en función del concepto de momento lineal.**  
- Relación de la mecánica vectorial aplicada sobre una partícula con su estado de reposo o de movimiento: aplicaciones estáticas o dinámicas de la física en otros campos, como la ingeniería o el deporte.  
• **El centro de gravedad en el cuerpo humano y su relación con el equilibrio en la práctica deportiva.**  
• **El centro de gravedad en una estructura y su relación con la estabilidad.**  

**F. Energía.**

- **Aplicación de los** conceptos de trabajo y potencia **para la** elaboración de hipótesis sobre el consumo energético de sistemas mecánicos o eléctricos del entorno cotidiano y su rendimiento **, verificándolas experimentalmente, mediante simulaciones o a partir del razonamiento lógico-matemático.**  
• **El trabajo como transferencia de energía entre los cuerpos: trabajo de una fuerza constante, interpretación gráfica del trabajo de una fuerza variable.**  
• **Potencia. Rendimiento o eficiencia de un sistema mecánico o eléctrico.**  
- Energía potencial y energía cinética de un sistema sencillo: aplicación a la conservación de la energía mecánica en sistemas conservativos y no conservativos y al estudio de las causas que producen el movimiento de los objetos en el mundo real.  
• **Energía cinética. Teorema del trabajo-energía.**  
• **Fuerzas conservativas. Energía potencial: gravitatoria y elástica.**  
• **La fuerza de rozamiento: una fuerza no conservativa.**  
• **Principio de conservación de la energía mecánica en sistemas conservativos y no conservativos.**  
- Variables termodinámicas de un sistema en función de las condiciones: determinación de las variaciones de temperatura que experimenta y las transferencias de energía que se producen con su entorno.  
• **El calor como mecanismo de transferencia de energía entre dos cuerpos.**  
• **Energía interna de un sistema. Primer principio de la termodinámica. Clasificación de los procesos termodinámicos.**  
• **Conservación y degradación de la energía. Segundo principio de la termodinámica.**  


