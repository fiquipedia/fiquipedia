# Escepticismo y pensamiento crítico

Utilizo el término escepticismo junto a pensamiento crítico; es la actitud de dudar de afirmaciones y apoyarse en la ciencia   

[Escepticismo - rae.es](https://dle.rae.es/escepticismo)  
> Desconfianza o duda de la verdad o eficacia de algo.  

Es algo asociado a la ciencia / mala ciencia, relacionado con el [método científico](/home/recursos/recursos-metodo-cientifico)  y con [Cultura Científica](/home/recursos/recursos-por-materia-curso/recursos-cultura-cientifica-1-bachillerato)   

En [Currículo Cultura Científica 1º Bachillerato](/home/materias/bachillerato/cultura-cientifica-1-bachillerato/curriculo-cultura-cientifica-1-bachillerato/)  se cita

> 1. **Obtener, seleccionar y valorar informaciones relacionadas con la ciencia y la tecnología a partir de distintas fuentes de información.**  
1.1. Analiza un texto científico o una fuente científico-gráfica, **valorando de forma crítica, tanto su rigor y fiabilidad, como su contenido**.  
1.2. **Busca, analiza, selecciona, contrasta**, redacta y presenta información sobre un tema relacionado con la ciencia y la tecnología, utilizando tanto los soportes tradicionales como Internet.  
...   
**Diferenciar la información procedente de fuentes científicas de aquellas que proceden de pseudociencias o que persiguen objetivos meramente comerciales.**  

Está asociado a valorar críticamente la información que recibimos y que encontramos  

[Infografía: Guía rápida para detectar la mala ciencia - naukas.com](naukas.com/2014/05/02/infografia-guia-rapida-para-detectar-la-mala-ciencia/)  
![](https://naukas.com/fx/uploads/2014/05/Rough-Guide-to-Bad-Science-ESP.png)  

[La gente debe saber que la ciencia no da certezas, pero es lo único a lo que agarrarnos - agenciasinc.es](https://www.agenciasinc.es/Entrevistas/La-gente-debe-saber-que-la-ciencia-no-da-certezas-pero-es-lo-unico-a-lo-que-agarrarnos)  

[twitter HelenaMatute/status/1312448795792216066](https://twitter.com/HelenaMatute/status/1312448795792216066)  
Cuando algo nos encaja en nuestro sistema de creencias nos lo tragamos sin más, sin preguntarnos siquiera si es cierto o qué pruebas hay. Asimilar el pensamiento científico nos hace ser más críticos. Es taan necesario  

[twitter 2qblog/status/1312450509878358016](https://twitter.com/2qblog/status/1312450509878358016)  
Una cosa que no dejo de repetir a mis alumnos: la capacidad de actuar científicamente no es innata y además, si tienes la suerte de adquirirla (generalmente por educación), tienes que hacer un esfuerzo para no dejarla de lado en algunas ocasiones.  

[twitter MolaSaber/status/1396116485177847809](https://twitter.com/MolaSaber/status/1396116485177847809)  
Que salga en un vídeo no lo hace verdad.  
Que valide tu opinión no lo hace verdad.  
Que suene creíble no lo hace verdad.  
Que lo diga alguien con autoridad no lo hace verdad.  
La verdad no es absoluta.   
¿Entonces cómo saber lo que es verdad y lo que no?  
(hilo)  

[Pseudoscience, What is it? How can I recognize it?](https://www.chem1.com/acad/sci/pseudosci.html)  

[twitter ProfFeynman/status/1312576350260088833](https://twitter.com/ProfFeynman/status/1312576350260088833)  
SCIENCE:  
If you don't make mistakes, you're doing it wrong.  
If you don't correct those mistakes, you're doing it really wrong.  
If you can't accept that you're mistaken, you're not doing it at all.  

Hay "pseudociencia" que debe ser abordada desde la ciencia y el escepticismo: terraplanismo, homeopatía, efecto de radicaciones, negacionismo de evidencias científicas (antivacunas, negación del cambio climático)....

En 2021 surgió esto con la nevada de Filomena
[Conspiracy Theorists Are Burning Snow to Prove It's Fake](https://www.popularmechanics.com/science/a35590304/conspiracy-theorists-burning-snow-viral-tiktok-videos/)  

[La nieve falsa y el mechero](https://ko-fi.com/post/La-nieve-falsa-y-el-mechero-Z8Z836Y1D)  

[twitter FiQuiPedia/status/1378647328295387139](https://twitter.com/FiQuiPedia/status/1378647328295387139)  
Me sugiere una actividad en Cultura Científica de 1° Bachillerato, poniendo este hilo como ejemplo de revisión científica de una "noticia" en "medios".
Gracias, @Shora

3 abril 2021
[https://twitter.com/Shora/status/1378304880947892228](https://twitter.com/Shora/status/1378304880947892228)  
Hoy vamos a analizar críticamente un estudio científico sobre mascarillas para ver lo que dice este y la diferencia que hay con la noticia que recoge Europa Press y que han difundido multitud de medios de comunicación. Bienvenidos a mi Journal Club.  
Primero, echemos un ojo a la revista donde se publica: "Cardiovascular Innovations and Applications". Se trata de una revista de open acess creada en 2014 que no cuenta con factor de impacto (https://es.wikipedia.org/wiki/Factor_de_impacto) lo cual ya pone un poco en guardia.  
[Running with Face Masks or Respirators Can Be Detrimental to the Respiratory and Cardiovascular Systems](https://www.ingentaconnect.com/content/cscript/cvia/pre-prints/content-cvia_244_01)  
Tampoco es una revista científica indexada ni en [SCIMAGO](https://scimagojr.com) ni en [Scopus](https://scopus.com/sources.uri) lo que tampoco es buena señal.  
Pero bueno, no nos llevemos por los prejuicios. Veamos el artículo, qué han hecho en el estudio y qué conclusiones obtienen.  
Al empezar a leer vemos con sorpresa que el "estudio" tiene tres páginas contadas. Bueno, evitemos los prejuicios, sigamos analizándolo.  


[twitter brmu/status/1394184569037934592](https://twitter.com/brmu/status/1394184569037934592)  
En plena era de las fake news y la desinformación ejercitar el juicio crítico es vital. Por ello la @brmu lanza su guía sobre pensamiento crítico #GuíaREFLEXIÓNbrmu. Seleccionada y redactada por @lagamez Para leer en línea o descargar en:   
[Guia de lectura para el pensamiento crítico. Biblioteca Regional de Murcia 2021](https://bibliotecaregional.carm.es/wp-content/uploads/2021/05/GUIApensamientocriticoBRMU.pdf) #Libréate  


[twitter Sonicando/status/1413414499487567872](https://twitter.com/Sonicando/status/1413414499487567872)  
Dando una vuelta por twitter me encuentro con esto:  
¿Cuántas cervezas se deben tomar al día? El CSIC revela la cantidad que es saludable  
El consumo moderado de cerveza tiene beneficios para la salud  
Es el resultado de una investigación publicada en la revista 'Nutrients'  
Ha sido elaborada por expertos en nutrición pertenecientes al CSIC  
[¿Cuántas cervezas se deben tomar al día? El CSIC revela la cantidad que es saludable](https://www.eleconomista.es/actualidad/noticias/11311914/07/21/Cuantas-cervezas-se-deben-tomar-al-dia-El-CSIC-revela-la-cantidad-que-es-saludable-.html)  
ya no son las cervezas que se puedan o no tomar para que no nos hagan daño. Son las que se DEBEN tomar. Toma ya.  
Vamos a por el paper, que es OPEN ACCESS  

[El oficio de la duda](https://jblanca.net/el_oficio_de_la_duda/)   
El oficio de la duda es una introducción a la filosofía de la ciencia escrita por José Blanca.  
En una sociedad que depende de la ciencia y la tecnología para cuestiones tan críticas como la salud o la alimentación es muy importante que los ciudadanos se planteen algunas cuestiones fundamentales: ¿Qué caracteriza al conocimiento científico, qué lo hace especial, cuáles son sus fortalezas y sus limitaciones? ¿En qué circunstancias es razonable confiar en lo que dicen los expertos y cuándo es preferible reservar el juicio? En el oficio de la duda se abordarán estos asuntos siguiendo el desarrollo de las ideas filosóficas que, partiendo de la Grecia clásica, condujeron al desarrollo de la ciencia moderna, evaluando las críticas radicales planteadas por Kuhn y compañía en los años 70 y resumiendo los principales avances del siglo XXI.  
Puedes leer el libro en la web o descargarlo.  

[No le creas a todos los expertos: Jerarquía de evidencia científica](https://baiafood.com/blogs/blog/jerarquia-de-evidencia-cientifica)  
![](https://cdn.shopify.com/s/files/1/1246/6251/articles/unnamed_2eb4c0a7-c876-4208-aa53-62a64656ff7c_1024x1024.jpg?v=1636412143)  


## Bulos en Internet y herramientas de verificación 
También está asociado a los bulos que se difunden por Internet y redes sociales y a verificar, a veces también noticias no solo científicas  

[twitter ONU_es/status/1524374766190276608](https://twitter.com/ONU_es/status/1524374766190276608)  
La difusión de información falsa impide que las personas se enteren de lo que pasa y tomen las precauciones debidas durante una crisis.  
No coompartas rumores  
Elige contenido de fuentes confiables.  
 #PrometoPausar. 
[Verificado](https://shareverified.com/es/)  
Verificado es una iniciativa de las Naciones Unidas que nos motiva a todos y todas a comprobar los consejos que compartimos.  
![](https://pbs.twimg.com/media/FSekk7CXoAIxm7l?format=jpg)  


[EFE Verifica](https://verifica.efe.com/)  
[Newtral](https://www.newtral.es/)  
[Maldita](https://maldita.es/)  

[Rompe cadenas](http://algoquedaquedecir.blogspot.com/2018/01/rompe-cadenas.html)  



