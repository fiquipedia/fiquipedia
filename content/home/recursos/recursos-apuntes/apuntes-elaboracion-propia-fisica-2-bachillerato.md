# Apuntes elaboración propia Física 2º Bachillerato

Se pueden ver actividades / ejercicios asociadas a EvAU en [EvAU Física](/home/pruebas/pruebasaccesouniversidad/paufisica).  
También se pueden ver [ejercicios de elaboración propia de 2º Bachillerato](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato), que también enlaza con [proyectos de investigación](/home/recursos/proyectos-de-investigacion)  
|  Bloque |  Apuntes  | 
|:-:|:-:|
|  [Movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio)  <br>(con LOMCE pasa a  [1º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato) ) |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F3.1-MAS-Teor%C3%ADa.pdf)  | 
|  [Movimiento ondulatorio y ondas sonoras](/home/recursos/fisica/movimiento-ondulatorio)  |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F3.2-3-Ondas-Teor%C3%ADa.pdf)  | 
|  [Gravitación](/home/recursos/fisica/recursos-gravitacion) <br>(con LOMCE parte pasa a  [1º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato) ) |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F2-Gravitaci%C3%B3n-Teor%C3%ADa.pdf)  | 
|  [Campo eléctrico](/home/recursos/fisica/recursos-campo-electrico)  |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F4.1-CampoEl%C3%A9ctrico-Teor%C3%ADa.pdf)  | 
|  [Campo magnético e inducción electromagnética](/home/recursos/fisica/recursos-campo-magnetico)  |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F4.2-3-CampoMagn%C3%A9tico-Teor%C3%ADa.pdf)  | 
|  [Óptica física](/home/recursos/fisica/recursos-optica-fisica) <br> (con LOMCE pasa de bloque óptica a bloque ondas) |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F5.1-%C3%93pticaF%C3%ADsica-Teor%C3%ADa.pdf)  | 
|  [Óptica geométrica](/home/recursos/fisica/optica-geometrica)  |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F5.2-%C3%93pticaGeom%C3%A9trica-Teor%C3%ADa.pdf)  | 
|  [Física relativista](/home/recursos/fisica/recursos-fisica-relativista)  |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F6.1-F%C3%ADsicaRelativista-Teor%C3%ADa.pdf)  | 
|  [Física cuántica](/home/recursos/fisica/recursos-fisica-cuantica)  |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F6.2-F%C3%ADsicaCu%C3%A1ntica-Teor%C3%ADa.pdf)  | 
|  [Física nuclear](/home/recursos/fisica/recursos-fisica-nuclear)  |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F6.3-F%C3%ADsicaNuclear-Teor%C3%ADa.pdf)  | 
|  [Física de partículas](/home/recursos/fisica/fisica-de-particulas)  (nuevo con LOMCE) |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F2B-F%C3%ADsicaPart%C3%ADculas-Teor%C3%ADa.pdf)  | 
|  [Cosmología](/home/recursos/fisica/recursos-cosmologia)  (nuevo con LOMCE) |  [📖 ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato/F2B-Cosmolog%C3%ADa-Teor%C3%ADa.pdf)  |

Además de  [apuntes generales](/home/recursos/apuntes)  y los  [recursos asociados a física de 2º de Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato) , incluyendo  [ejercicios de elaboración propia de física de 2º de bachillerato](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato) , coloco aquí los "apuntes" de elaboración propia. Intento que sean unas hojas "muy breves" de resumen que puedan tenerse como referencia básica a falta de libro de texto, y que se amplían durante la clase. 

Los apuntes son por bloques, al igual que el currículo y que las soluciones de  [problemas PAU de física](/home/pruebas/pruebasaccesouniversidad/paufisica) , aunque la separación en bloques es bajo mi criterio el orden en el que yo lo impartiría: hay aspectos entre ondas, óptica física y geométrica que otras personas y apuntes agrupan de otra manera.

*En general intento revisar que se cubre el currículo oficial de Madrid, e intento reflejarlo indicando _subrayados_ los epígrafes / contenidos que aparecen en el currículo oficial. 

**Nota importante: Se agradecen comentarios sobre erratas y sobre todo, sugerencias de mejora (qué sobra, qué falta, qué poner de otra manera ...). Todos los documentos ponen en la cabecera la fecha de revisión y mail de contacto.**

En febrero 2014 se sube la primera versión de todos los bloques, inicialmente separado en 10 bloques, que anexo en la página como pdf y que pongo aquí como tabla más legible. 

En febrero de 2014 me surge la idea y se sube la primera versión de aPAUntes física: un resumen de fórmulas, que no apuntes, limitado a 2 caras de folio.

En agosto 2016 comienzo a revisar los apuntes para adaptarlos al  [currículo física 2º Bachillerato LOMCE](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce) , que se implanta en 2º de Bachillerato 2016/2017: además de subrayar todos los contenidos, intento marcar criterios de evaluación y estándares de aprendizaje evaluables, aunque de estos no se pueden poner todos.

En octubre 2014 publico la primera versión de Tipología, que podría colocar aquí asociado a los apuntes, pero considero más relacionado con los  [problemas PAU de física](/home/pruebas/pruebasaccesouniversidad/paufisica)  y los pongo con ellos, y en los que realiza una agrupación de problemas por tipo adicional a la separación en bloques.

Algunos bloques retoman contenidos vistos en cursos anteriores; se intenta dar una visión completa más orientada a 2º de Bachillerato  
- Lo básico de movimiento oscilatorio (ecuación MAS, ley Hooke) se ve por encima en 4º ESO. Con cambio legislación LOMCE (se implanta en 1º de Bachillerato en 2015-2016 y en 2º de Bachillerato en 2016-2017, el movimiento oscilatorio se ve en 1º Bachillerato, ya no en 2º Bachillerato) 
- Ondas se ve por encima en 4º ESO -Gravitación se ve de manera simple en 4º ESO y en 1º de Bachillerato 
- Campo eléctrico se ve en 1º Bachillerato 
