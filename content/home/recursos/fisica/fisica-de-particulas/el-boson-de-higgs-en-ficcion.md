
# El bosón de Higgs en ficción

Esta página surge porque el 19 marzo 2018 se borra la página  
 [Higgs_boson_in_fiction](https://en.wikipedia.org/wiki/Higgs_boson_in_fiction)   
 en la que había añadido algún comentario  
El por qué se borra de Wikipedia explicado aquí   
 [Wikipedia:Articles for deletion/Higgs boson in fiction - Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:Articles_for_deletion/Higgs_boson_in_fiction)   
  
 This is pretty much an indiscriminate collection of things that happen to mention the  [Higgs boson](https://en.wikipedia.org/wiki/Higgs_boson) . Every prose section is either a minor portrayal or about a non-notable work (no article). Most of the works in the Others section are also very minor portrayals or mere references. The article is mostly unsourced, and all of the references are primary. (The one that does not go to a work of fiction is irrelevant to the context.) Finally, there is little discussion of the list itself (all of which is unsourced  [WP:OR](https://en.wikipedia.org/wiki/Wikipedia:OR) ), and there is no indication of  [WP:LISTN](https://en.wikipedia.org/wiki/Wikipedia:LISTN) .  [LaundryPizza03](https://en.wikipedia.org/wiki/User:LaundryPizza03)  ( [talk](https://en.wikipedia.org/wiki/User_talk:LaundryPizza03) ) 20:40, 12 March 2018 (UTC)  
  
Al estar borrada hace poco la puedo recuperar vía caché de google, junto con mis contribuciones.   
  
Lo reproduzco aquí (con el HTML original). Todos los créditos son para las personas que en su día realizaron las contribuciones a Wikipedia  

Aparte de eso cito aquí y enlazo vídeos  
Capítulo 42 del Pequeño Reino de Ben y Holly, "Nanny Plum And The Wise Old Elf Swap Jobs For One Whole Day"  
 [BosonHiggs-en.mpg](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/video/BosonHiggs-en.mpg)   
 [BosonHiggs-es.mpg](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/video/BosonHiggs-es.mpg)   
En español dicen: "Supongo que desmontaría el quark del bosón de Higgs booleano del núcleo de la bios y reconectaría el..."  
  
---  
  
Esta es la versión en caché de <a dir="ltr" href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction">https://en.wikipedia.org/wiki/Higgs_boson_in_fiction</a> de Google. Se trata de una captura de pantalla de la página tal como esta se mostraba el 12 Mar 2018 20:45:05 GMT. <br />
<br />
The <b><a href="https://en.wikipedia.org/wiki/Higgs_boson" title="Higgs boson">Higgs boson</a></b> has appeared in several works of <a href="https://en.wikipedia.org/wiki/Fiction" title="Fiction">fiction</a> in <a href="https://en.wikipedia.org/wiki/Popular_culture" title="Popular culture">popular culture</a>. These references rarely reflect the expected properties of the hypothetical <a href="https://en.wikipedia.org/wiki/Elementary_particle" title="Elementary particle">elementary particle</a>, or do so only vaguely, and often imbue it with fantastic properties. Some examples are shown below.<br />
<br />
<h2><a name="TOC-God-s-Spark"></a><span>God's Spark</span></h2>
<p>In <a href="https://en.wikipedia.org/w/index.php?title=Dr._Norman_P._Johnson%27s&amp;action=edit&amp;redlink=1" title="Dr. Norman P. Johnson's (page does not exist)">Dr. Norman P. Johnson's</a> <i><a href="http://www.godsspark.info" rel="nofollow">God's Spark</a></i>, Science-based fictional novel, the <a href="https://en.wikipedia.org/wiki/Higgs_boson" title="Higgs boson">Higgs Boson</a> and the search for other fundamental elementary particles at the <a href="https://en.wikipedia.org/wiki/Large_Hadron_Collider" title="Large Hadron Collider">Large Hadron Collider</a>
 beyond the Standard Model are an integral part of the plot. In the not 
too distant future,the LHC will be upgraded with more energy and 
intensity along with scintillating fiber optic detectors and super 
quantum computers that will push the discovery energies as close as 
possible to the <a href="https://en.wikipedia.org/wiki/Planck_scale" title="Planck scale">Planck limit</a>.
 This novel suggests that accidental discoveries of new super particles 
representing gigantic forces present only at the first few seconds of 
the Big Bang might be entirely possible and unpredictable. The story is 
fictional but it contains discussions of extensions to the Standard 
Model along with new astronomical problems such as <a href="https://en.wikipedia.org/wiki/Dark_matter" title="Dark matter">Dark Matter</a> and <a href="https://en.wikipedia.org/wiki/Dark_energy" title="Dark energy">Dark Energy</a>
 that clearly demonstrate significant discoveries beyond the Higgs 
sector is tantalizingly just beyond current technology. It also includes
 aspects of the current political conflict between fundamental religious
 dogma and fact-based science.</p>
<h2><a name="TOC-Lexx"></a><span>Lexx</span></h2>
<p>In the science fantasy series <i><a href="https://en.wikipedia.org/wiki/Lexx" title="Lexx">Lexx</a></i>, it is said that planets which develop on a path similar to Earth are type 13 planets which are sometimes destroyed by all-out <a href="https://en.wikipedia.org/wiki/Nuclear_warfare" title="Nuclear warfare">nuclear war</a>,
 but it is much more common for such planets to be obliterated by 
physicists attempting to determine the precise mass of the Higgs boson 
particle. The particle colliders used to perform the calculations reach 
critical mass at the moment the mass of the particle is known, causing 
an implosion which destroys the planet and then collapses it into a 
nugget of super-dense matter "roughly the size of a <a href="https://en.wikipedia.org/wiki/Pea" title="Pea">pea</a>."<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-lexx1-1">[1]</a></sup> The mass of the particle was a repeating 131313 matching the name "type 13 planet"<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-lexx2-2">[2]</a></sup> and this also is in the predicted range of the particle's mass.<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-3">[3]</a></sup></p>
<h2><a name="TOC-Solaris"></a><span>Solaris</span></h2>
<p>In <a href="https://en.wikipedia.org/wiki/Steven_Soderbergh" title="Steven Soderbergh">Steven Soderbergh</a>'s <a href="https://en.wikipedia.org/wiki/Solaris_(2002_film)" title="Solaris (2002 film)">2002 adaptation</a> of <a href="https://en.wikipedia.org/wiki/Stanis%C5%82aw_Lem" title="Stanisław Lem">Stanisław Lem</a>'s novel <i><a href="https://en.wikipedia.org/wiki/Solaris_(novel)" title="Solaris (novel)">Solaris</a></i>, the script has a reference to Higgs bosons, absent in the original book: <i>"So, if we created a negative <a href="https://en.wikipedia.org/wiki/Higgs_field" title="Higgs field">Higgs field</a>, and bombarded them with a stream of Higgs anti-bosons, they might disintegrate."</i></p>
<h2><a name="TOC-White-Mars"></a><span>White Mars</span></h2>
<p>In <a href="https://en.wikipedia.org/wiki/Brian_Aldiss" title="Brian Aldiss">Brian Aldiss</a>'s <i>White Mars</i>, an expedition is established to go to <a href="https://en.wikipedia.org/wiki/Mars" title="Mars">Mars</a>
 to find Higgs bosons which is believed to hold the key to solving the 
question of where mass comes from. The reason that Mars is considered is
 that Earth and the Moon are "too noisy" from all the human activity for
 the experiment. The way they go about it is by having a ring of 
superconductive fluid, Argon 36, and waiting till they see an error 
which will signify a Higgs particle has passed through the fluid.</p>
<h2><a name="TOC-Flashforward"></a><span>Flashforward</span></h2>
<p>In <a href="https://en.wikipedia.org/wiki/Robert_J._Sawyer" title="Robert J. Sawyer">Robert J. Sawyer</a>'s <i><a href="https://en.wikipedia.org/wiki/Flashforward_(novel)" title="Flashforward (novel)">Flashforward</a></i>, an experiment at <a href="https://en.wikipedia.org/wiki/CERN" title="CERN">CERN</a>
 to find the Higgs particle causes the consciousness of the entire human
 race to be temporarily sent twenty-one years into the future. Later, 
the experiment is repeated with the specific intention to view the 
future – however this time it instead creates the Higgs Boson.<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-4">[4]</a></sup></p>
<h2><a name="TOC-Into-the-Looking-Glass"></a><span>Into the Looking Glass</span></h2>
<p>In <a href="https://en.wikipedia.org/wiki/John_Ringo" title="John Ringo">John Ringo</a>'s <i><a href="https://en.wikipedia.org/wiki/Into_the_Looking_Glass" title="Into the Looking Glass">Into the Looking Glass</a></i>,
 the University of Central Florida is destroyed by a 60 kiloton 
explosion that is first thought to be a nuclear weapon, but turns out to
 be a mishap from a Higgs boson research experiment. Following the 
explosion, gateways to other worlds are opened and a war with the aliens
 on the other side of the gates begins.</p>
<h2><a name="TOC-The-God-Particle"></a><span>The God Particle</span></h2>
<p>In <a href="https://en.wikipedia.org/wiki/Richard_L._Cox" title="Richard L. Cox">Richard Cox</a>'s <i>The God Particle</i>,
 American business man Steve Keeley is thrown out of a window and falls 
three storeys, then wakes up and begins to see the world in a different 
way; he is able to accurately predict future events, read others' 
thoughts, and manipulate his environment.<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-5">[5]</a></sup></p>
<h2><a name="TOC-A-Hole-In-Texas"></a><span>A Hole In Texas</span></h2>
<p>In <a href="https://en.wikipedia.org/wiki/Herman_Wouk" title="Herman Wouk">Herman Wouk</a>'s <i><a href="https://en.wikipedia.org/wiki/A_Hole_In_Texas" title="A Hole In Texas">A Hole In Texas</a></i>,
 the real science behind the Higgs boson is used as a backdrop for a 
satire on Washington politics, the chase for funding in scientific 
communities, and Hollywood blockbusters. In fact, the Hollywood portion 
of the satire has much to do with the wild flights of fancy in evidence 
in many of the other entries on this list. Much of the plot is based on 
the aborted <a href="https://en.wikipedia.org/wiki/Superconducting_Super_Collider" title="Superconducting Super Collider">Superconducting Super Collider</a> project.<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-6">[6]</a></sup></p>
<h2><a name="TOC-Others"></a><span>Others</span></h2>
<p>The particle also appears in other, non-narrative art forms:</p>
<ul><li><a href="https://en.wikipedia.org/wiki/Nick_Cave_and_the_Bad_Seeds" title="Nick Cave and the Bad Seeds">Nick Cave and the Bad Seeds</a> have a song titled 'Higgs Bosun Blues' on their album <a href="https://en.wikipedia.org/wiki/Push_the_Sky_Away" title="Push the Sky Away">Push the Sky Away</a>, released in 2013.</li>
<li><a href="https://en.wikipedia.org/wiki/Frank_Zappa" title="Frank Zappa">Frank Zappa</a>'s posthumously released album <a href="https://en.wikipedia.org/wiki/Trance-Fusion" title="Trance-Fusion">Trance-Fusion</a> contains an instrumental track called 'Finding Higgs' Boson'.</li>
<li>In the Japanese science fiction anime <a href="https://en.wikipedia.org/wiki/Martian_Successor_Nadesico" title="Martian Successor Nadesico">Martian Successor Nadesico</a>
 'boson jumping', which is shown as the activation of a sort of 
temporary subspace wormhole by bending the laws of physics, plays a 
large role in the plot. The name 'boson jump' comes from the (apparently
 incidental) increase of boson particles in the vicinity and the anime 
also includes mention of the <a href="https://en.wikipedia.org/wiki/Advanced_potential" title="Advanced potential">advanced potential</a> of the <a href="https://en.wikipedia.org/wiki/Wheeler%E2%80%93Feynman_absorber_theory" title="Wheeler–Feynman absorber theory">Wheeler–Feynman Time-Symmetric Theory</a>.</li>
<li>On the television show <i><a href="https://en.wikipedia.org/wiki/Numb3rs" title="Numb3rs">Numb3rs</a></i>, <a href="https://en.wikipedia.org/wiki/Peter_MacNicol" title="Peter MacNicol">Peter MacNicol</a>'s character, Dr. Larry Fleinhardt is working as part of a team in search of the Higgs boson.</li>
<li>More realistically, CERN's <a href="https://en.wikipedia.org/wiki/Large_Hadron_Collider" title="Large Hadron Collider">Large Hadron Collider</a> is the subject of a CERN-produced <a href="https://en.wikipedia.org/wiki/Rapping" title="Rapping">rap</a> video featuring <a href="https://en.wikipedia.org/wiki/Katherine_McAlpine" title="Katherine McAlpine">some of its own staff</a>.<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-7">[7]</a></sup><sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-8">[8]</a></sup></li>
<li>On the day that the Large Hadron Collider was activated, <a href="https://en.wikipedia.org/wiki/BBC_Radio_4" title="BBC Radio 4">BBC Radio 4</a> broadcast "<a href="https://en.wikipedia.org/wiki/Lost_Souls_(Torchwood)" title="Lost Souls (Torchwood)">Lost Souls</a>", a science fiction radio drama set at CERN, dealing with the discovery of the Higgs boson.</li>
<li>In the <a href="https://en.wikipedia.org/wiki/Anime" title="Anime">anime</a> series <i><a href="https://en.wikipedia.org/wiki/Code_Geass" title="Code Geass">Code Geass</a></i>, the piloted robots called <a href="https://en.wikipedia.org/wiki/Knightmare_Frame" title="Knightmare Frame">Knightmare Frames</a>, make use of a flight pack that is said to generate a Higgs field in order to counteract the force of gravity.</li>
<li>In the manga/anime series <i><a href="https://en.wikipedia.org/wiki/Knights_of_Sidonia" title="Knights of Sidonia">Knights of Sidonia</a></i>,
 "Heigus" particles are used as a source of power by the humans aboard 
the siege ship "Sidonia" and by their enemies, the shapeshifting alien 
race known as "Gauna".</li>
<li>In <a href="https://en.wikipedia.org/wiki/Terry_Pratchett" title="Terry Pratchett">Terry Pratchett</a>'s novel <i><a href="https://en.wikipedia.org/wiki/Nation_(novel)" title="Nation (novel)">Nation</a></i>, there is a character named Bo'sn Higgs.</li>
<li>The Higgs boson is mentioned briefly on several occasions in the <i><a href="https://en.wikipedia.org/wiki/Angels_%26_Demons_(film)" title="Angels &amp; Demons (film)">Angels &amp; Demons</a></i> film.</li>
<li>The GN Particle in the anime series <i><a href="https://en.wikipedia.org/wiki/Gundam_00" title="Gundam 00">Mobile Suit Gundam 00</a></i>,
 appears to be a name substitution for the Higgs boson particle as well 
as containing several of the property the Higgs boson is theorized to 
have.</li>
<li>In an episode of <a href="https://en.wikipedia.org/wiki/Syfy" title="Syfy">Syfy</a>'s <i><a href="https://en.wikipedia.org/wiki/Eureka_(U.S._TV_series)" title="Eureka (U.S. TV series)">Eureka</a></i>, they mention the possibility of finding the Higgs boson when using a rotating collider.</li>
<li>Post-Rock band, Tides from Nebula, has a track on their album "Aura" called Higgs boson.</li>
<li>The <a href="https://en.wikipedia.org/wiki/Coen_brothers" title="Coen brothers">Coen brothers</a> 2009 film, <i><a href="https://en.wikipedia.org/wiki/A_Serious_Man" title="A Serious Man">A Serious Man</a></i>, reveals endless layers of references to particle physics, and to the Higgs boson explicitly.</li>
<li>Electronic music group Shpongle has an EP called "The God Particle", released 2010.</li>
<li>In an episode of <i><a href="https://en.wikipedia.org/wiki/The_Big_Bang_Theory" title="The Big Bang Theory">The Big Bang Theory</a></i>
 in which "Higgs Boson Particle" is the solution to Dr. Sheldon Cooper's
 Pictionary puzzle. Also mentioned in several more episodes.</li>
<li>In the February 21, 2012 edition of the <i><a href="https://en.wikipedia.org/wiki/Dilbert" title="Dilbert">Dilbert</a></i> comic strip by <a href="https://en.wikipedia.org/wiki/Scott_Adams" title="Scott Adams">Scott Adams</a>, Dilbert finds the Higgs boson, which immediately commands him to "build an ark."<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-9">[9]</a></sup></li>
<li>In the July 16, 2012 edition of the <i><a href="https://en.wikipedia.org/wiki/Arlo_and_Janis" title="Arlo and Janis">Arlo and Janis</a></i> comic strip by <a href="https://en.wikipedia.org/wiki/Jimmy_Johnson_(cartoonist)" title="Jimmy Johnson (cartoonist)">Jimmy Johnson</a>,
 Arlo and Janis are sitting at the kitchen table discussing the 
discovery of the Higgs boson. When Arlo explains, "It could be the 
reason we have weight and mass," Janis asks, "How many Higgs Bosons are 
in a slice of chocolate cake?", to which Arlo replies, "A lot!"<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-10">[10]</a></sup></li>
<li>In the <i><a href="https://en.wikipedia.org/wiki/Penguins_of_Madagascar" title="Penguins of Madagascar">Penguins of Madagascar</a></i> episode, Endangerous Species, Kowalski's <a href="https://en.wikipedia.org/wiki/Cloning" title="Cloning">cloning machine</a>'s <a href="https://en.wikipedia.org/wiki/Fuel_supply" title="Fuel supply">fuel supply</a> is the Higgs Boson particle, which is lost in <a href="https://en.wikipedia.org/wiki/Outer_space" title="Outer space">space</a> when it explodes &amp; Kowalski claimed there was only one in the <a href="https://en.wikipedia.org/wiki/Observable_universe" title="Observable universe">known universe</a> &amp; he'd found it in Rico's stomach</li>
<li>In the <a href="https://en.wikipedia.org/wiki/MMOG" title="MMOG">MMOG</a> <a href="https://en.wikipedia.org/wiki/Augmented_reality" title="Augmented reality">augmented reality</a> game <a href="https://en.wikipedia.org/wiki/Ingress_(game)" title="Ingress (game)">Ingress</a>, research into the Higgs particle at <a href="https://en.wikipedia.org/wiki/CERN" title="CERN">CERN</a> uncovers an alien infrastructure of <a href="https://en.wikipedia.org/wiki/Exotic_matter" title="Exotic matter">exotic matter</a> on Earth which the competing factions believe is either for the good or bane of humanity.<sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-11">[11]</a></sup><sup><a href="https://en.wikipedia.org/wiki/Higgs_boson_in_fiction#cite_note-12">[12]</a></sup></li>
<li>In <i><a href="https://en.wikipedia.org/wiki/The_Powerpuff_Girls_(2016_TV_series)" title="The Powerpuff Girls (2016 TV series)">The Powerpuff Girls (2016 TV series)</a></i> episode 13, Arachno-Romance, the Professor writes a song to Sapna Nehru where Higgs Boson appears.</li>
<li>In the <i><a href="https://en.wikipedia.org/wiki/Ben_%26_Holly%27s_Little_Kingdom" title="Ben &amp; Holly's Little Kingdom">Ben &amp; Holly's Little Kingdom</a></i>
 season 2, episode 42, Nanny Plum And The Wise Old Elf Swap Jobs For One
 Whole Day, Ben Elf explains to Nanny Plum how would Wise Old Elf repair
 the robot using "the Higgs boson quark"</li></ul>
<h2><a name="TOC-See-also"></a><span>See also</span></h2>
<ul><li><a href="https://en.wikipedia.org/wiki/Particle_accelerators_in_popular_culture" title="Particle accelerators in popular culture">Particle accelerators in popular culture</a></li>
<li><a href="https://en.wikipedia.org/wiki/The_God_Particle_(disambiguation)" title="The God Particle (disambiguation)">The God Particle (disambiguation)</a></li></ul>
<br />
<h2><a name="TOC-References"></a><span>References</span><br />
</h2>
<li><span><b><a href="#cite_ref-lexx1_1-0">1 ^</a></b></span> <span><cite>"<a href="https://en.wikipedia.org/wiki/List_of_Lexx_episodes#Season_4_.282001-2002.29" title="List of Lexx episodes">Little Blue Planet</a>". <i>Lexx</i>.</cite><span title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=unknown&amp;rft.btitle=Lexx&amp;rfr_id=info%3Asid%2Fen.wikipedia.org%3AHiggs+boson+in+fiction"><span style="display:none"> </span></span></span></li>
<li><span><b><a href="#cite_ref-lexx2_2-0">2 ^</a></b></span> <span><cite>"<a href="https://en.wikipedia.org/wiki/List_of_Lexx_episodes#Season_4_.282001-2002.29" title="List of Lexx episodes">Yo Way Yo</a>". <i>Lexx</i>.</cite><span title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=unknown&amp;rft.btitle=Lexx&amp;rfr_id=info%3Asid%2Fen.wikipedia.org%3AHiggs+boson+in+fiction"><span style="display:none"> </span></span></span></li>
<li><span><b><a href="#cite_ref-3">3 ^</a></b></span> <span>This upper bound for the Higgs boson mass is a prediction within the minimal Standard Model assuming that it remains a consistent theory up to the <a href="https://en.wikipedia.org/wiki/Planck_scale" title="Planck scale">Planck scale</a>. In extensions of the SM, this bound can be loosened or, in the case of supersymmetry theories, lowered. The lower bound which results from direct experimental exclusion by <a href="https://en.wikipedia.org//wiki/Large_Electron%E2%80%93Positron_Collider" title="Large Electron–Positron Collider">LEP</a> is valid for most extensions of the SM, but can be circumvented in special cases. G. Bernardi et al. <a href="http://pdg.lbl.gov/2010/reviews/rpp2010-rev-higgs-boson.pdf" rel="nofollow">Higgs bosons: theory and searches</a>. pdg.lbl.gov. Updated May 2010</span></li>
<li><span><b><a href="#cite_ref-4">4 ^</a></b></span> <span>Sawyer R., <i>Flashforward</i>, Tor Books, 1999, <a href="https://en.wikipedia.org/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a> <a href="https://en.wikipedia.org/wiki/Special:BookSources/0-312-86712-3" title="Special:BookSources/0-312-86712-3">0-312-86712-3</a></span></li>
<li><span><b><a href="#cite_ref-5">5 ^</a></b></span> <span>Cox R., <i>The God Particle</i>, Random House, 2005, <a href="https://en.wikipedia.org/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a> <a href="https://en.wikipedia.org/wiki/Special:BookSources/978-0-345-46285-5" title="Special:BookSources/978-0-345-46285-5">978-0-345-46285-5</a></span></li>
<li><span><b><a href="#cite_ref-6">6 ^</a></b></span> <span>Wouk H., <i>A Hole in Texas</i>, Little, Brown and Company, 2004, <a href="https://en.wikipedia.org/wiki/International_Standard_Book_Number" title="International Standard Book Number">ISBN</a> <a href="https://en.wikipedia.org/wiki/Special:BookSources/0-316-52590-1" title="Special:BookSources/0-316-52590-1">0-316-52590-1</a></span></li>
<li><span><b><a href="#cite_ref-7">7 ^</a></b></span> <span>Joshua Topolsky (2008-08-08). <a href="https://www.engadget.com/2008/08/08/cern-rap-video-about-the-large-hadron-collider-creates-a-black-h/" rel="nofollow">CERN rap video about the Large Hadron Collider creates a black hole of awesomeness</a>. engadget.com</span></li>
<li><span><b><a href="#cite_ref-8">8 ^</a></b></span> <span>Will Barras. <a href="http://www.vimeo.com/1431471?pg=embed&amp;sec=1431471" rel="nofollow">CERN Rap</a>. video. vimeo.com</span></li>
<li><span><b><a href="#cite_ref-9">9 ^</a></b></span> <span><a href="http://www.dilbert.com/strips/2012-02-21/" rel="nofollow">Dilbert comic strips archive at the official Dilbert website</a>. Dilbert.com. Retrieved on 2012-07-06.</span></li>
<li><span><b><a href="#cite_ref-10">10 ^</a></b></span> <span><a href="http://www.gocomics.com/arloandjanis/2012/07/16#mutable_806682" rel="nofollow">Arlo and Janis comic strips archive</a>. GoComics.com.</span></li>
<li><span><b><a href="#cite_ref-11">11 ^</a></b></span> <span><cite><a href="http://www.net4tech.net/2012/12/google-launches-ingress-its-first.html" rel="nofollow">"Google launches "Ingress" its first mobile game augmented reality"</a>.</cite><span title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=unknown&amp;rft.btitle=Google+launches+%22Ingress%22+its+first+mobile+game+augmented+reality&amp;rft_id=http%3A%2F%2Fwww.net4tech.net%2F2012%2F12%2Fgoogle-launches-ingress-its-first.html&amp;rfr_id=info%3Asid%2Fen.wikipedia.org%3AHiggs+boson+in+fiction"><span style="display:none"> </span></span></span></li>
<li><span><b><a href="#cite_ref-12">12 ^</a></b></span> <span><cite><a href="http://gaming.stackexchange.com/questions/95716/what-determines-where-xm-is-generated" rel="nofollow">"ingress - What determines where XM is generated? - Arqade"</a>.</cite><span title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=unknown&amp;rft.btitle=ingress+-+What+determines+where+XM+is+generated%3F+-+Arqade&amp;rft_id=http%3A%2F%2Fgaming.stackexchange.com%2Fquestions%2F95716%2Fwhat-determines-where-xm-is-generated&amp;rfr_id=info%3Asid%2Fen.wikipedia.org%3AHiggs+boson+in+fiction"><span style="display:none"> </span></span></span></li>
  
  
