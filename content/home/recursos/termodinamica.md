# Termodinámica

En 2022 con LOMLOE se trata en 1º Bachillerato, ver [apuntes elaboración propia 1º Bachillerato](https://www.fiquipedia.es/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato), en concreto   
[Pizarras termodinámica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato/FQ1B-Pizarras-Termodin%C3%A1mica.pdf), aparte de lo asociado a termoquímica.  

[twitter _luigiruffolo/status/1227485996465491968](https://twitter.com/_luigiruffolo/status/1227485996465491968)
One of the best book openings ever (via Johnson Shrestha)  
![](https://pbs.twimg.com/media/EQjoAK4XsAAuJn1?format=jpg)  

[Goodstein’s ‘States of Matter’ Opening Quote - truthorfiction.com](https://www.truthorfiction.com/goodsteins-states-of-matter-opening-quote/)  

[The four laws of thermodynamics The Royal Institution - youtube](https://www.youtube.com/playlist?list=PLbnrZHfNEDZxmZmL3fZHPTeoUO8amBox2)  

 * [Convenio de signos Q y W](#TOC-Convenio-de-signos-Q-y-W)

Al hablar de termodinámica normalmente se asocia a física; se suele introducir en primeros cursos universitarios y en bachillerato se trata en Tecnología Industrial.  
Existen conceptos relacionados con química como la entropía y la  [termoquímica](/home/recursos/quimica/termoquimica).  
Asociable a bloque de Química de 2º de Bachillerato, pendiente añadir y separar información (entropía, leyes de la termodinámica ...); se trata en los [apuntes de elaboración propia de Química de 2º de Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato)  
Se pueden asociar conceptos generales y puede haber información dentro de  [recursos sobre energía](/home/recursos/energia) , trabajo y  [calor](/home/recursos/calor).  
En la serie de vídeos "universo mecánico" el capítulo 47 trata la entropía, y tiene estas frases iniciales de resumen geniales  
>La ciencia de la termodinámica se basa en cuatro postulados fundamentales o axiomas, llamados los cuatro primeros principios de la termodinámica.  
>De esos 4 principios, el 2º fue el 1º que se descubrió, el 1º fue el 2º en descubrirse, el 3er principio que se descubrió se llama principio 0, y el 4º se llama 3er principio.  
>Pero todo eso tiene perfecto sentido, porque la termodinámica es la más implacablemente lógica de todas las ciencias.  
>Les diré brevemente lo que son esos cuatro principios:  
>El principio cero dice precisamente que la temperatura tiene sentido  
>El primero es la conservación de la energía  
>**El segundo principio es el principio de la entropía**  
>El tercer principio dice que hay una temperatura tan baja que nunca se puede alcanzar  
>"Dr. David L. Goodstein, California Institute of Tecnology.

##  Apuntes  
[Fundamentos de termodinámica, Universidad Autónoma de Madrid, Departamento de Química Física Aplicada - joule.qfa.uam.es (archive.org)](https://web.archive.org/web/20170511055857/http://joule.qfa.uam.es/beta-2.0/index.php)  

[Termoquímica - selectividadonline (archive.org)](https://web.archive.org/web/20150802080826/http://www.selectividadonline.com/descargar-documento/termoquimica) Eduardo Montoya Marín, cc-by-nc-sa  

[Termodinámica, primer principio](http://www2.montes.upm.es/dptos/digfa/cfisica/termo1p/termo1p_portada.html)  
[Termodinámica, segundo principio](http://www2.montes.upm.es/dptos/digfa/cfisica/termo2p/termo2p_portada.html)  
 Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM).

[Apuntes de termodinámica elemental E. Barrull, 1994](http://www.biopsychology.org/apuntes/termodin/termodin.htm)  
©* Biopsychology.org, 1998 - 2006. Consultado en enero 2021 su última actualización de 2006  

[Silvia Pérez Casas, Cálculo propiedades termodinámicas para substancias puras.](http://depa.fquim.unam.mx/amyd/archivero/PROPIEDADESTERMODINAMICAS_14534.pdf)  Tiene "Interludio Matemático" inicial interesante  

[Thermodynamics, Professor Z. S. Spakovszky, Fall 2008, MIT](http://web.mit.edu/16.unified/www/FALL/thermodynamics/)  

[Libros sobre Ingeniería Energética, CopyrightPedro Fernández Díez](https://pfernandezdiez.es/es/)   
[Termodinámica técnica](https://pfernandezdiez.es/es/libro?id=2)  

[Termodinámica - youphysics](https://www.youphysics.education/es/termodinamica-indice/)  

[termodinámica - fikasi](https://sites.google.com/view/fikasi/termodin%C3%A1mica)  

[ Tecnologia industrial (autoformació IOC) > Màquines > Termodinàmica -  educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/mod/book/view.php?id=596&chapterid=334)  

##  Ejercicios
[Problemas resueltos de termodinámica, Física con ordenador](http://www.sc.ehu.es/sbweb/ocw-fisica/problemas/estadistica/termo.xhtml)  
Ángel Franco García  

[Problemas y ejercicios resueltos de Termodinámica I](https://web.archive.org/web/20181123194605/http://perso.wanadoo.es/lolymestre/juliweb/librotermodinamica.pdf) (waybakmachine, no operativo en 2021)  
[http://juliweb.es/](https://web.archive.org/web/20180809093350/http://juliweb.es/) (waybackmachine, no operativo en 2021)  
Julián Moreno Mestre  

[Examen de Termodinámica I Junio 2004, Industriales ETSII, UPM](http://www.cartagena99.com/recursos/alumnos/examenes/examenes.pdf) 

[Collection of Solved Problems in Physics - thermodynamics - physicstasks.eu ](http://physicstasks.eu/en/physics/thermodynamics)  
This collection of Solved Problems in Physics is developed by Department of Physics Education, Faculty of Mathematics and Physics, Charles University in Prague since 2006.  
The Collection contains tasks at various level in mechanics, electromagnetism, thermodynamics and optics. The tasks mostly do not contain the calculus (derivations and integrals), on the other hand they are not simple such as the use just one formula. Tasks have very detailed solution descriptions. Try to solve the task or at least some of its parts independently. You can use hints and task analysis (solution strategy).  
Difficulty level  
Level 1 – Lower secondary level   
Level 2 – Upper secondary level   
Level 3 – Advanced upper secondary level   
Level 4 – Undergraduate level    

[Cuestiones y problemas resueltos de Termodinámica técnica - upv.es](https://riunet.upv.es/bitstream/handle/10251/148958/TOC_0201_03_01.pdf)  


##  Convenio de signos Q y W  <a name="TOC-Convenio-de-signos-Q-y-W"></a>
Convenio IUPAC  
[First law of thermodynamics, description - Wikipedia](http://en.wikipedia.org/wiki/First_law_of_thermodynamics#Description)  
Se cita Quantities, Units and Symbols in Physical Chemistry (IUPAC Green Book) See Sec. 2.11 Chemical Thermodynamics  
[Quantities, Units and Symbols in Physical Chemistry (IUPAC Green Book) 3rd Edition 2ndPrinting 22apr2011 PDF](http://media.iupac.org/publications/books/gbook/IUPAC-GB3-2ndPrinting-Online-22apr2011.pdf)  
[IUPAC Green Book, Quantities, Units, and Symbols in Physical Chemistry](https://iupac.org/what-we-do/books/greenbook/)  
[Quantities, Units and Symbols in Physical Chemistry (IUPAC Green Book), 3rd Edition 2012 2ndPrinting PDF](https://iupac.org/wp-content/uploads/2019/05/IUPAC-GB3-2012-2ndPrinting-PDFsearchable.pdf)  
Se indica
>(1)... The given equation in integrated form is ΔU=Q+W. Q>0 and W>0 indicate an increase in the energy of the system  

[laplace.us.es, Trabajo en termodinámica (GIE), Convenio de signos](http://laplace.us.es/wiki/index.php/Trabajo_en_termodin%C3%A1mica_(GIE)#Convenio_de_signos)  

El convenio en el que ΔU=Q-W es más antiguo y se suele llamar el convenio de Clausius. Lo cito en resolución de problemas de oposiciones de termodinámica  
[laplace.us.es, Primer principio de la termodinámica (GIE)](http://laplace.us.es/wiki/index.php/Primer_principio_de_la_termodin%C3%A1mica_(GIE))  
[oscardeabril.aq.upm.es Tema 9 Primer principio de la termodinamica.pdf](http://oscardeabril.aq.upm.es/clases/pdfs/Tema%209_Primer%20principio%20de%20la%20termodinamica.pdf)  
Página 3  
>Convenio termodinámico o de Clausius*  
>del sistema en el entorno:  
>W > 0 → lo realiza el sist.  
>W < 0 → se realiza sobre el sist.  
>Convenio mecánico o de la IUPAC**  
>del entorno en el sistema:  
>W > 0 → se realiza sobre el sist.  
>W < 0 → lo realiza el sist.  
>*El convenio de Clausius es más adecuado para el estudio de máquinas térmicas, ya que el trabajo útil se trata como positivo.
>**El convenio de la IUPAC (o tradicional) es el utilizado en mecánica 

[twitter memecrashes/status/1670563581757214724](https://twitter.com/memecrashes/status/1670563581757214724)  
![](https://pbs.twimg.com/media/Fy8IuRIWYAYcadu?format=jpg)  

## Unidades de temperatura

[Resolution 3 of the 13th CGPM (1967). SI unit of thermodynamic temperature (kelvin) ](https://www.bipm.org/en/committees/cg/cgpm/13-1967/resolution-3)  
[COMPTES RENDUS DES SÉANCES TREIZIÈME CONFÉRENCE GÉNÉRALE DES POIDS ET MESURES](https://www.bipm.org/documents/20126/17314988/CGPM13.pdf/ff522dd4-7c97-9b8d-127b-4fe77f3fe2bc?version=1.2&t=1587104721230&download=true#page=104)  

> 1º l'unité de température thermodynamique est désignée sous le nom "kelvin" et son symbole est K;  
> 2º ce même nom et ce même symbole sont utilisés pour exprimer un intervalle de température;  
> 3º l'unité de température thermodynamique ne sera plus désignée sous le nom "degré Kelvin", symbole ºK;  

[Real Decreto 2032/2009, de 30 de diciembre, por el que se establecen las unidades legales de medida.](https://www.boe.es/buscar/act.php?id=BOE-A-2010-927)  

Tabla 1 Unidades SI básicas  
Magnitud, Nombre de la unidad, Símbolo de la unidad  
temperatura termodinámica, kelvin, K  

[Green Book. Quantities, Units, and Symbols in Physical Chemistry](https://iupac.org/wp-content/uploads/2019/05/IUPAC-GB3-2012-2ndPrinting-PDFsearchable.pdf)  
> The SI unit of Celsius temperature is the degree Celsius, ◦C, which is equal to the kelvin, K. ◦C shall be treated as a single symbol, with no space between the ◦ sign and the C. The symbol ◦K, and the symbol ◦, shall no longer be used for the unit of thermodynamic temperature.  

ISO 80000-5. Quantities and units — Part 5: Thermodynamics
> The units of thermodynamic and Celsius temperature difference or change are identical. Such differences or changes may be expressed either in kelvins, symbol K, or in degrees Celsius, symbol, ºC.  
> It should be noted that the symbol for the degree Celsius shall be preceded by a space (see ISO 31-0, 3.4).  

##  Definición de temperatura. Temperaturas negativas
A nivel de secundaria y bachillerato se asume el 0 K como un valor límite, pero se puede abrir la mente, la temperatura es una definición estadística y asociada a ciertas situaciones ...  
[Qué significa que un gas cuántico tiene una temperatura negativa](http://naukas.com/2013/01/04/que-significa-que-un-gas-cuantico-tiene-una-temperatura-negativa/)  
[Francis en Naukas (con adenda): Qué significa que un gas cuántico tiene una temperatura negativa](http://francis.naukas.com/2013/01/04/francis-en-naukas-con-adenda-que-significa-que-un-gas-cuantico-tiene-una-temperatura-negativa/)  
Cito: 
>En un sistema fuera del equilibrio el concepto de temperatura no está bien definido, luego puede ser positiva, nula e incluso negativa. ¿Puede tener un sistema en equilibrio termodinámico una “temperatura negativa,” por ejemplo, un gas cuántico ultrafrío? Por supuesto, todo depende, de qué depende, depende de la definición de temperatura que se utilice

[twitter gregeganSF/status/1540950512186429442](https://twitter.com/gregeganSF/status/1540950512186429442)  
\[1/16] You might have heard it claimed that the temperature of a system is an “emergent” property that only makes sense if the system has a large number of degrees of freedom. This isn’t true!  
There are certainly things that can be said about the temperature of a system that are  
\[2/16] only approximately true, in general, but become *vastly* better approximations for systems with many degrees of freedom. But temperature itself is perfectly well defined, regardless.  
In classical mechanics, suppose we have a system with f degrees of freedom whose total  
\[3/16] energy is known to lie between E and E+δE. Maybe we know some other things about it. But when we describe the system in phase space — the abstract space that has one dimension for the coordinate and one for the momentum associated with each of the f degrees of freedom —  
\[4/16] suppose we can only say that the system lies in some subset of this 2f-dimensional space that’s also a 2f-dimensional set.
For example, for a single particle in 3 dimensions, we haven’t pinned the system down to a point, or a line, etc., in its 6-dimensional phase space.  
...
It’s worth adding that our definition of temperature allows for negative temperatures.  
For example, suppose we have a set of particles fixed in place but free to switch their spins from parallel to antiparallel to a magnetic field. Their total energy will depend on the direction  
of their spins, but it can’t increase without bound, and near the maximum energy, adding more energy *shrinks* the possible states the system can be in. So dΩ/dE will be negative.  
But when one temperature is -ve and another +ve:  
T1 T2 < 0  
and energy will flow from the system  
with negative temperature to the one with positive temperature, because it’s  
\[T1-T2]/ (T1 T2)  
that must be positive, for energy to flow from system 1 to system 2.  
This makes systems with negative temperature act as if they’re “hotter” than any system with positive temperature.  



##  Vídeos  
[The four laws of thermodynamics, The Royal Institution, playlist](https://www.youtube.com/playlist?list=PLbnrZHfNEDZxmZmL3fZHPTeoUO8amBox2) 

##  Referencias
 [http://kinetics.nist.gov/janaf/](http://kinetics.nist.gov/janaf/) 

##  Primer principio
[Termodinámica, primer principio](http://www2.montes.upm.es/dptos/digfa/cfisica/termo1p/termo1p_portada.html)  
[laplace.us.es, Primer principio de la termodinámica (GIE)](http://laplace.us.es/wiki/index.php/Primer_principio_de_la_termodin%C3%A1mica_(GIE))  
[laplace.us.es Problemas del primer principio de la termodinámica (GIE)](http://laplace.us.es/wiki/index.php/Problemas_del_primer_principio_de_la_termodin%C3%A1mica_%28GIE%29)  
[chemwiki.ucdavis.edu The First Law of Thermodynamics](http://chemwiki.ucdavis.edu/Core/Physical_Chemistry/Thermodynamics/Chemical_Energetics/The_First_Law_of_Thermodynamics) 

##  Segundo principio
[montes.upm.es, Termodinámica, primer principio](http://www2.montes.upm.es/dptos/digfa/cfisica/termo2p/termo2p_portada.html)  
[laplace.us.es, Segundo Principio de la Termodinámica](http://laplace.us.es/wiki/index.php/Segundo_Principio_de_la_Termodin%C3%A1mica) 

[twitter fermatslibrary/status/1532718253365202945](https://twitter.com/fermatslibrary/status/1532718253365202945)  
2nd Law of Thermodynamics  
The entropy, also known disorder, of a system will  increase over time.   
The Universe is full of irreversible processes that continuously increase its total entropy thus tending to disorder rather than order.  
![](https://pbs.twimg.com/media/FUVPIK6WUAI_VXI?format=jpg)  

### Procesos termodinámicos

[¿Qué es el proceso termodinámico? Definición - thermal-engineering.org](https://www.thermal-engineering.org/es/que-es-el-proceso-termodinamico-definicion/)  

Isocórico (V=cte)  

Isobárico (P=cte)  

Adiabático (Q=0)  

Isotérmico (T=cte)  

### [Entropía]((/home/recursos/entropia)

###  Ciclo de Carnot
 [laplace.us.es, Ciclo de Carnot](http://laplace.us.es/wiki/index.php/Ciclo_de_Carnot) 

## Máquinas térmicas 
El ciclo de Carnot es una máquina térmica ideal  

[Máquinas térmicas - montes.upm.es](http://www2.montes.upm.es/dptos/digfa/cfisica/termo2p/maquinas.html)  


Ciclo Otto: motores de combustión interna de gasolina encendido provocado  
En aproximación teórica todo el calor se aporta a volumen constante.  
[Ciclo de Otto - edithyurani12](https://edithyurani12.wordpress.com/tercer-corte/ciclo-de-otto/)  
[EL CICLO OTTO - navarraof.orgfree.com](http://navarrof.orgfree.com/Docencia/Termodinamica/CiclosGeneracion/ciclo_otto.htm)  
[Motor de cuatro tiempos encendido por chispa – Ciclo Otto - stefanelli.eng.br](https://www.stefanelli.eng.br/es/ciclo-otto-motor-cuatro-tiempos/) Incluye animación  

[Ciclo Otto y ciclo diesel slideshare](https://es.slideshare.net/fran8melen/ciclo-otto-diesel)  

Ciclo diesel: motores de combustión interna gasoil encendido por compresión  
[Qué es el ciclo diesel – Motor diesel – Definición - thermal-engineering.org](https://www.thermal-engineering.org/es/que-es-el-ciclo-diesel-motor-diesel-definicion/)  
[U2 análisis termodinámico del motor diesel - tecsup, slideshare](https://es.slideshare.net/oliver4ever/u2-anlisis-termodinmico-del-motor-diesel)  
[Motor de cuatro tiempos de inyección de combustible – Ciclo Diesel - stefanelli.eng.br](https://www.stefanelli.eng.br/es/ciclo-diesel-motor-cuatro-tiempos/) Incluye animación  


Ciclo Brayton: turbinas de gas  
[Brayton cycle - wikipedia.org](https://en.wikipedia.org/wiki/Brayton_cycle)


## Memes
[twitter FIKASI1/status/1486453449919184896](https://twitter.com/FIKASI1/status/1486453449919184896)  
Mañana trabajaremos en tecnología industrial este meme para introducir algunos principios termodinámicos. Interesante para la época COVID con tanta ventana abierta en clase...  
![](https://pbs.twimg.com/media/FKDxiQCWUAILOWJ?format=jpg)  

##  Simulaciones
Simulador termodinámico  
[Termograf.unizar.es](http://termograf.unizar.es/index.htm)  
Ejecutable windows y mutiplataforma

###  Experimento de Joule
 [www.vascak.cz Joule](https://www.vascak.cz/data/android/physicsatschool/templateimg.php?s=mf_joule&l=es)  
 [subaru.univ-lemans.fr Joule](http://subaru.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/thermo/joule.html)  

 
 
