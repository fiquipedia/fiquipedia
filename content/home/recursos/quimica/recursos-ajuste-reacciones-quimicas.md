
# Recursos ajuste reacciones químicas

A veces se trata asociado a  [reacciones y cálculos estequiométricos](/home/recursos/quimica/recursos-reacciones-y-calculos-estequiometricos) , por ejemplo  
"Las reacciones químicas" [concurso.cnice.mec.es la estequiometría en las reacciones químicas](http://concurso.cnice.mec.es/cnice2005/35_las_reacciones_quimicas/curso/lrq_est_01.html)  (Rafael Jiménez Prieto, Pastora Maria Torres Verdugo) tiene ejemplos de ajuste gráfico de reacciones.  
(Enlace en 2019:  [ntic.educacion.es Las reacciones químicas](http://ntic.educacion.es//w3//eos/MaterialesEducativos/mem2004/las_reacciones_quimicas/index.html) )  

Relacionado con la idea conservación de la masa.
Reactivo limitante, pureza, rendimiento se comentan en [reacciones y cálculos estequiométricos](/home/recursos/quimica/recursos-reacciones-y-calculos-estequiometricos)  

Documentos con ejemplos hay en  [apuntes](/home/recursos/apuntes)  

[alonsoformula.com 3º ESO Ajuste reacciones](https://www.alonsoformula.com/FQESO/3_6__reaccion_quimica.htm#AJUSTE%20DE%20ECUACIONES%20QU%C3%8DMICAS)  

[gobiernodecanarias.org lentiscal Lecciones interactivas de química](https://www3.gobiernodecanarias.org/medusa/ecoescuela/recursosdigitales/2015/03/22/lecciones-interactivas-de-quimica/)  tenía apartados sobre ajustes, pero en 2021 no operativo   

[edistribucion.es/anayaeducacion Unidad 7. Programa de ajuste de ecuaciones químicas](http://www.edistribucion.es/anayaeducacion/8440052/recursos/07/ecuaciones_quimicas.html)  

[academia.edu Ejercicios_de_ajustes_de_reacciones_químicas](https://www.academia.edu/10248884/Ejercicios_de_ajustes_de_reacciones_qu%C3%ADmicas) Uploaded by Альбус Д. Клиффорд

##   Conservación de la masa
 [twitter SchmidtChemist/status/1230211008972673026](https://twitter.com/SchmidtChemist/status/1230211008972673026)  
 Here are the two #phenomena experiments (steel wool vs ethanol burning) my advanced chemistry students had to do Claim, Evidence and Reasoning for in groups of 4. You could use 1g of each instead, but I did 2g in my demo. @NGSSphenomena  
 
 [twitter rafaelsolana2/status/1302202766073434112](https://twitter.com/rafaelsolana2/status/1302202766073434112)  
 Ojo a la lectura de la báscula!  
  Al quemar lana de acero, se vuelve más pesada⁣ al oxidarse. El peso agregado es el oxígeno en la combustión.  
  Fe (hierro) pasa a Fe2O3 (óxido de hierro): a cada átomo de Fe se unen 1,5 átomos de O. En total, se agregan 24 g por cada mol de Fe.  
  
[reddit.com When you burn steel wool, it gets heavier](https://www.reddit.com/r/Damnthatsinteresting/comments/eipubt/when_you_burn_steel_wool_it_gets_heavier/)  

##   Calculadoras asociadas a ajustes
En algunos sitios hay calculadoras que permiten el ajuste  
[http://es.webqc.org/balance.php](http://es.webqc.org/balance.php)  

Ejemplo  
[es.webqc.org/balance K4Fe(CN)6  + KMnO4 +  H2SO4 = KHSO4  + Fe2(SO4)3 +  MnSO4  + HNO3  + CO2  + H2O](https://es.webqc.org/balance.php?reaction=K4Fe%28CN%296%2BKMnO4%2BH2SO4%3DKHSO4%2BFe2%28SO4%293%2BMnSO4%2BHNO3%2BCO2%2BH2O)  

[Balanceador de Ecuaciones Químicas - chemicalaid](https://www.chemicalaid.com/tools/equationbalancer.php)  

Asociada a ajustes redox  
[periodni.com BALANCING REDOX REACTIONS by the ion-electron method](https://www.periodni.com/half-reaction_method.php) 

[gregthatcher.com BalanceChemicalEquations](http://www.gregthatcher.com/Chemistry/BalanceChemicalEquations.aspx)  
 
[funbasedlearning.com Chemistry > Classic Chembalancer](http://funbasedlearning.com/chemistry/chemBalancer/default.htm)  
[funbasedlearning.com Chemistry > Review Chembalancer](http://funbasedlearning.com/chemistry/chemBalancer2/default.htm)  

[play.google.com Equation Balancer - Balance Chemical Equation KharbLabs](https://play.google.com/store/apps/details?id=com.kharblabs.balancer.equationbalancer) 

##   Ejercicios corrección online
* [Generador ejercicios igualar reacciones](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/igualacion_reacciones_jquery.htm?t=1466103169291)  

[test_reacc.swf](http://web.archive.org/web/20190522155220/http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq4eso/test_reacc.swf) waybackmachine. Propone reacciones químicas para ajustar y las corrige.   
[ajuste_reacciones_moleculas.swf](http://web.archive.org/web/20190522155220/http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq3eso/ajuste_reacciones_moleculas.swf) waybackmachine. Para estudiar las reacciones químicas. Puedes ajustarlas añadiendo moléculas, ver la reacción en el laboratorio, o lo que les ocurre a las partículas a nivel microscópico.   
[Ejercicios sobre cálculos con reacciones químicas (estequiometría). Aplicación Flash.](http://web.archive.org/web/20190522155220/http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/fq4eso/ejercicios_reacciones.swf). Propone ejercicios de estequiometría y los va corrigiendo paso a paso. Cada vez, un ejercicio diferente.  
José Antonio Navarro  
Blog del autor [clasesfisicayquimica.blogspot.com](http://clasesfisicayquimica.blogspot.com/p/aplicaciones-multimedia.html) remite a [http://fq.iespm.es/](http://fq.iespm.es/)  

Ver también [IES Al Ándalus Enlaces a aplicaciones informáticas útiles (flash, scripts, etc). elaboradas por José Antonio Navarro.](http://web.archive.org/web/20190522155220/http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=139&Itemid=199)  

##   Ejercicios (ajustes y cálculos estequiométricos)
[fisquiweb Cálculos estequiométricos 1Bach PDF](http://fisquiweb.es/Apuntes/Apuntes1Bach/ProbEsteq_1Bach.pdf) 

[Ajuste de reacciones – Ejercicios resueltos - uv.es](https://www.uv.es/jbosch/PDF/AjusteReacciones.pdf)  

##   Simulaciones
[phet.colorado.edu reactivos, productos y excedentes](https://phet.colorado.edu/sims/html/reactants-products-and-leftovers/latest/reactants-products-and-leftovers_es.html)  
 
Simulación con producción de amoniaco, disociación del agua, combustión metano, y modo juego  
[phet.colorado.edu Balanceo de ecuaciones químicas](https://phet.colorado.edu/sims/html/balancing-chemical-equations/latest/balancing-chemical-equations_es.html)  

##   Reacciones químicas de ajuste complicado
Ya citado [es.webqc.org/balance K4Fe(CN)6  + KMnO4 +  H2SO4 = KHSO4  + Fe2(SO4)3 +  MnSO4  + HNO3  + CO2  + H2O](https://es.webqc.org/balance.php?reaction=K4Fe%28CN%296%2BKMnO4%2BH2SO4%3DKHSO4%2BFe2%28SO4%293%2BMnSO4%2BHNO3%2BCO2%2BH2O)  
 
Gracias a @vielvein [twitter vielbein/status/995296646639570944](https://twitter.com/vielbein/status/995296646639570944)  
[![](https://pbs.twimg.com/media/DdABAnHWAAEtYfB.jpg "") ](https://pbs.twimg.com/media/DdABAnHWAAEtYfB.jpg)  
[![](https://pbs.twimg.com/media/DdABAlxW0AAhZ8x.jpg "") ](https://pbs.twimg.com/media/DdABAlxW0AAhZ8x.jpg)  
 
 P2I4 + H3PO4 -> P4 + H2O + PH4I Interesante, y la web  
 [Chemistry or Mathematics? CHEMICAL EQUATIONS COEFFICIENTS](http://www.trimen.pl/witek/calculators/wspolczynniki.html) 

##   Actividades
 [twitter bjmahillo/status/972376701446520832](https://twitter.com/bjmahillo/status/972376701446520832)  
 Viendo esta estupenda idea se me ocurre que se puede hacer lo mismo en FyQ de 2°ESO para que los alumnos aprendan a ajustar reacciones químicas. 1 tapón= 1 átomo  
 
 [twitter magarciava/status/968190044606476293](https://twitter.com/magarciava/status/968190044606476293)  
 Y cuando haciendo ecuaciones una alumna te dice:  
 ¿Puedo hacer una foto? Es para que mi madre vea para qué usamos los tapones en clase de matemáticas! @iesfgb #ClaustroPMAR @aomatos @aaronasenciofer @cfiesalamanca @CFIESAmate  
 
Además veo que se podría encajar con  [Esquema de colores CPK - wikipedia](https://es.wikipedia.org/wiki/Esquema_de_colores_CPK)  
Ya que son habituales tapones rojos, negros y blancos, asociados a O, C y H  

[twitter bjmahillo/status/1224707992098148353](https://twitter.com/bjmahillo/status/1224707992098148353)  
En 2° ESO los alumnos aprenden a ajustar reacciones químicas con tapones de colores reciclados. ¡Exitazo! ¡Todos trabajando! 😃  
En la escuela pública nos faltan recursos, pero andamos sobrados de imaginación.  
[![](https://pbs.twimg.com/media/EP8JW2dWAAQtYBe?format=jpg&name=360x360 "") ](https://pbs.twimg.com/media/EP8JW2dWAAQtYBe?format=jpg&name=360x360)  
[![](https://pbs.twimg.com/media/EP8JXztX4AsK4LH?format=jpg&name=360x360 "") ](https://pbs.twimg.com/media/EP8JXztX4AsK4LH?format=jpg&name=360x360)  
[![](https://pbs.twimg.com/media/EP8JZkuWAAIQWVx?format=jpg&name=360x360 "") ](https://pbs.twimg.com/media/EP8JZkuWAAIQWVx?format=jpg&name=360x360)  
[![](https://pbs.twimg.com/media/EP8Ja6rX0AEfKxm?format=jpg&name=360x360 "") ](https://pbs.twimg.com/media/EP8Ja6rX0AEfKxm?format=jpg&name=360x360)  

[twitter bjmahillo/status/1222542277106655232](https://twitter.com/bjmahillo/status/1222542277106655232)  
Este curso las bolitas de poliestireno me están dando mucho juego.  
Aquí está la formación de HI. Con velcros rompo y formo enlaces para que se entienda que una reacción química los átomos se reorganizan.  
[![](https://pbs.twimg.com/media/EPdXs5bXUAAUtM7?format=jpg&name=small "") ](https://pbs.twimg.com/media/EPdXs5bXUAAUtM7?format=jpg&name=small)  

[twitter currofisico/status/1237725411531948034](https://twitter.com/currofisico/status/1237725411531948034)  
Dulce ajuste de ecuaciones químicas.  
[![](https://pbs.twimg.com/media/ES1IslFWsAAKu7T?format=jpg&name=small "") ](https://pbs.twimg.com/media/ES1IslFWsAAKu7T?format=jpg&name=small)  

[twitter MariolaScience/status/1228083457064161280](https://twitter.com/MariolaScience/status/1228083457064161280)  
Práctica 1BAT: relación entre germinación, luz y pigmentos fotosintéticos #ProfesBioGeo  
[![](https://pbs.twimg.com/media/EQsHWSTWoAAW9G1?format=jpg&name=large "") ](https://pbs.twimg.com/media/EQsHWSTWoAAW9G1?format=jpg&name=large)  

[twitter mer_lopez21/status/1231351679796359168](https://twitter.com/mer_lopez21/status/1231351679796359168)  
El ajuste con gominolas también queda chulo. Mis alumnos de 2 ESO se divirtieron un montón y la Ley de conservación de la masa les quedó "clarísima"!!!(Y lo que me gustó verlos así)  
[![](https://pbs.twimg.com/media/ERajxnQWkAEFNYy?format=jpg "") ](https://pbs.twimg.com/media/ERajxnQWkAEFNYy?format=jpg)  
[![](https://pbs.twimg.com/media/ERajyodX0AcxxkM?format=jpg "") ](https://pbs.twimg.com/media/ERajyodX0AcxxkM?format=jpg)  
[![](https://pbs.twimg.com/media/ERajzmtWoAEM09J?format=jpg "") ](https://pbs.twimg.com/media/ERajzmtWoAEM09J?format=jpg)  
[![](https://pbs.twimg.com/media/ERaj0iRWAAAPqfT?format=jpg "") ](https://pbs.twimg.com/media/ERaj0iRWAAAPqfT?format=jpg)  

[twitter pablofcayqca/status/1246042323596779521](https://twitter.com/pablofcayqca/status/1246042323596779521)  
¡Muy buenas! Por aquí dejo la última tarea que propuse en 2°ESO para repasar la representación de sustancias químicas (ya tenía ganas de darle uso al montón de tapones que he ido acumulando ), por si os es de utilidad  
[https://facebook.com/56812073360920](https://facebook.com/56812073360920)  
[![](https://pbs.twimg.com/media/EUrU4lwU8AAm46U?format=jpg "") ](https://pbs.twimg.com/media/EUrU4lwU8AAm46U?format=jpg)  
[![](https://pbs.twimg.com/media/EUrU4lxU0AASgNx?format=jpg "") ](https://pbs.twimg.com/media/EUrU4lxU0AASgNx?format=jpg)  
[![](https://pbs.twimg.com/media/EUrU4lzVAAEeXEI?format=jpg "") ](https://pbs.twimg.com/media/EUrU4lzVAAEeXEI?format=jpg)  

[twitter colgandoclases/status/1387354156852260866](https://twitter.com/colgandoclases/status/1387354156852260866)  
\#FQ3ESO Otro ejercicio de #REACCIONESQUÍMICAS resuelto  
[![](https://pbs.twimg.com/media/E0DfP4lXoA4GysZ?format=jpg "") ](https://pbs.twimg.com/media/E0DfP4lXoA4GysZ?format=jpg)  


Los ficheros estaban inicialmente se veían anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia quimica/recursos-ajuste-reacciones-quimicas](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/quimica/recursos-ajuste-reacciones-quimicas) 

* [ActividadAjusteReacciones.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/quimica/recursos-ajuste-reacciones-quimicas/ActividadAjusteReacciones.pdf)  
* [ActividadTaponesMoleculasAjustes.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/quimica/recursos-ajuste-reacciones-quimicas/ActividadTaponesMoleculasAjustes.pdf)


