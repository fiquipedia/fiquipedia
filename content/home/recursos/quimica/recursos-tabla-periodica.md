---
aliases:
  - /home/recursos/quimica/recursos-tabla-peridica/
---

# Recursos Tabla Periódica / elementos químicos

Puede haber información relacionada separable sobre isótopos, radiactividad ...  
Ejemplo noticia enero 2018  
[elpais.com La tabla periódica se asoma a una nueva fila por primera vez en la historia](https://elpais.com/elpais/2018/01/04/ciencia/1515101255_058583.html)  
Científicos de Japón intentan sintetizar el elemento químico 119, “jamás creado en el universo”  

##   2019, año internacional de la tabla periódica (150 años de la tabla de Mendeléyev)
 [Un siglo y medio de la tabla que reunió a todos los elementos - agenciasinc](https://www.agenciasinc.es/Reportajes/Un-siglo-y-medio-de-la-tabla-que-reunio-a-todos-los-elementos)  
 [Matemáticas alrededor de la tabla periódica - elpais](https://elpais.com/elpais/2019/01/02/ciencia/1546451546_136309.html)  
 
[Una Tabla Periódica con Mucha vida: Los elementos dan la cara](https://www.quimicaysociedad.org/una-tabla-periodica-con-mucha-vida-los-elementos-dan-la-cara/)  
[Una Tabla Periódica con Mucha vida: Los elementos dan la cara comic](https://150tpuscsic.wixsite.com/150tp/comic)  
[Una Tabla Periódica con Mucha vida: Los elementos dan la cara PDF](https://7dde302b-d152-48de-80b5-41af2754e2f1.filesusr.com/ugd/aabf3b_81075692dae9482a9625f84147de8272.pdf)  

[La tabla periódica cumple 150 años - nationalgeographic.com](https://www.nationalgeographic.com.es/ciencia/actualidad/tabla-periodica-cumple-150-anos_13721)  

## Nombres de los elementos 113, 115, 117 y 118 (y otros acuerdos sobre nombres en castellano de elementos)

17 junio 2016  
[Cuatro elementos químicos ¿nuevos? - investigacionyciencia.es](https://www.investigacionyciencia.es/blogs/fisica-y-quimica/24/posts/cuatro-elementos-qumicos-nuevos-14289)  
> ¿Cuál debería ser su nombre en español? En catalán Pep Anton Vieta pronostica que probablemente se acaben llamando nihoni, moscovi, tennessi y oganessó. En español serían nihonio, moscovio, tennessio y oganessón.  

30 noviembre 2016  
[IUPAC Announces the Names of the Elements 113, 115, 117, and 118 - iupac.org](https://iupac.org/iupac-announces-the-names-of-the-elements-113-115-117-and-118/)  

1 diciembre 2016  
[Los cuatro nombres de los nuevos elementos de la tabla periódica - elpais.com](https://elpais.com/elpais/2016/12/01/ciencia/1480597013_139617.html)  
> los elementos 113, 115, 117 y 118 han sido bautizados como Nihonio, Moscovio, Téneso y Oganesón  
[La tabla periódica que tienes en casa acaba de quedarse obsoleta - computerhoy.com](https://computerhoy.com/noticias/life/tabla-periodica-que-tienes-casa-acaba-quedarse-obsoleta-54916)  
> Nihonio (Nh), Moscovio (Mc), Tenesino (Ts) y Oganesón (Og)  

2 diciembre 2016  
[twitter Fundeu/status/804659636750155776](https://twitter.com/Fundeu/status/804659636750155776)  
[teneso y oganesón, mejor que tenesino y oganesson - fundeu.es](https://www.fundeu.es/recomendacion/teneso-y-oganeson-mejor-que-tenesino-y-oganesson/)  

15 diciembre 2016  
RAE propone unos nombres sin consultar a RSEQUIMICA  
[twitter FiQuiPedia/status/809405091086303232](https://twitter.com/FiQuiPedia/status/809405091086303232)  

28 diciembre 2016  
[twitter RAEinforma/status/814083403612033024](https://twitter.com/RAEinforma/status/814083403612033024)  
La Comisión de Vocabulario Científico y Técnico de la RAE aprobó el 22 de dic. los nombres «teneso» y «oganesón».  
Estos son los argumentos que sustentan esa decisión: http://ow.ly/d/5IH5 y http://ow.ly/d/5IH6  

[Nombres y símbolos en español de los elementos con números atómicos 113, 115, 117 y 118 aceptados por la IUPAC el 28 de noviembre de 2016. An. Quím., 112 (4), 2016, 200-204](https://analesdequimica.es/index.php/AnalesQuimica/article/view/945/1215)  

[Nombres y símbolos en español de los elementos aceptados por la IUPAC el 28 de noviembre de 2016 acordados por la RAC, la RAE, la RSEQ y la Fundéu](https://rseq.org/mat-didacticos/nombres-y-simbolos-en-espanol-de-los-elementos-aceptados-por-la-iupac-el-28-de-noviembre-de-2016-acordados-por-la-rac-la-rae-la-rseq-y-la-fundeu/)  

Se citan tres documentos, uno de ellos como "Nombres y símbolos en español de los elementos aceptados por la IUPAC", pero remite a 
[Adiciones y correcciones. Anales de química. Nombres y símbolos en español de los elementos aceptados por la IUPAC el 28 de noviembre de 2016 acordados por la RAC, la RAE, la RSEQ y la Fundéu](https://rseq.org/wp-content/uploads/2018/09/20170404NOMBRES_Y_SYMBOLOS_EN_ESPAYOL_DE_LOS_ELEMENTOS_QUYMICOS_ACEPTADOS_POR_LA_IUPAC_EL_28_DE_NOVIEMBRE_DE_2016.pdf)  
El título del documento no refleja que también se citan (se prefiere el primero y se acepta el segundo como variante)  
zinc / cinc  
kriptón / criptón  
circonio / zirconio  
telurio / teluro  
yodo / iodo  

## La numeración de los grupos en la tabla periódica

En algunas tablas antiguas se citan nombres de grupos en romanos  
IUPAC desde hace tiempo usa números de 1 a 18:  

[Tabla periódica de los elementos, grupos - wikipedia.org](https://es.wikipedia.org/wiki/Tabla_peri%C3%B3dica_de_los_elementos#Grupos)  
> Anteriormente se utilizaban números romanos según la última cifra del convenio de denominación de hoy en día —por ejemplo, los elementos del grupo 4 estaban en el IVB y los del grupo 14 en el IVA—. En Estados Unidos, los números romanos fueron seguidos por una letra «A» si el grupo estaba en el bloque s o p, o una «B» si pertenecía al d. En Europa, se utilizaban letras en forma similar, excepto que «A» se usaba si era un grupo precedente al 10, y «B» para el 10 o posteriores. Además, solía tratarse a los grupos 8, 9 y 10 como un único grupo triple, conocido colectivamente en ambas notaciones como grupo VIII. En 1988 se puso en uso el nuevo sistema de nomenclatura IUPAC y se desecharon los nombres de grupo previos.  

Se cita [Pure&Appl. Chern., Vol. 60, No. 3, pp. 431-436,  1988. Printed in Great Britain. @ 1988 IUPAC](https://doi.org/10.1351/pac198860030431)  
También publicado como [New notations in the periodic table](https://www.degruyter.com/document/doi/10.1351/pac198860030431/html)  

En documentos posteriores ya solo se hace referencia a la numeración de 1 a 18  
[NOMENCLATURE OF INORGANIC CHEMISTRY. IUPAC Recommendations 2005. Red Book.](http://old.iupac.org/publications/books/rbook/Red_Book_2005.pdf#page=63)  

>IR-3.5 ELEMENTS IN THE PERIODIC TABLE  
>The groups of elements in the periodic table (see inside front cover) are numbered from 1 to 18.  

##   Aplicaciones sobre tabla periódica
 Gperodic  [http://gperiodic.seul.org/](http://gperiodic.seul.org/)  
 Linux Kalzium  [http://www.kde.org/applications/education/kalzium/](http://www.kde.org/applications/education/kalzium/)  
 Linux Pendiente revisar para iPad, para android...


###   Apps
 Periodic game, Apple,  [http://rabq.mx/](http://rabq.mx/)  
 
[Apps para repasar la tabla periódica en Secundaria, Educación 3.0](https://www.educaciontrespuntocero.com/recursos/secundaria/app-repasar-tabla-periodica-secundaria/) 

## Ejercicios

[Bancos de preguntas Moodle tabla periódica - Enrique García de Bustos Sierra](https://www.educa2.madrid.org/web/enrique.garciadebustos/lafisicaesfacil/recursos/moodle)  

##   Enlaces a distintas tablas periódicas

###   Tablas periódicas para imprimir / mudas ...
 En ocasiones se necesitan tablas con poca información y parece complicado encontrar algo intermedio entre una en blanco completamente y una completa!  
 

* [TablaPeriodicaMuda-ESO.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/recursos-tabla-peridica/TablaPeriodicaMuda-ESO.pdf)   
 
 Incluyo aquí enlaces separados, que pueden repetirse con más detalles en esta misma página  
 [Printable Periodic Tables (PDF)](http://chemistry.about.com/od/periodictableelements/a/printperiodic.htm)  
 [SPImprimible.pdf](http://fisquiweb.es/Apuntes/Apuntes1Bach/SPImprimible.pdf)  
 [Tabla vacía, vaxasoftware](http://www.vaxasoftware.com/doc_eduen/qui/tpvacia.pdf)  
 [Tabla del libro rojo IUPAC 2005 sobre inorgánica: solamente tiene Z, símbolos, nº de grupo y nº de periodo](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content//home/recursos/quimica/recursos-tabla-peridica/Red_Book_2005-page2-table.pdf)  (tomado del original y redimensionado: falta por actualizar nombres símbolos hasta 118)  
 
 La tabla actualizada con nombres, pero más datos en  [IUPAC Periodic Table of the Elements, 2016](https://www.iupac.org/cms/wp-content/uploads/2015/07/IUPAC_Periodic_Table-28Nov16.pdf)  
 
 [Updated Periodic Table - printable, RSC](http://www.rsc.org/learn-chemistry/resource/res00001469/updated-periodic-table-printable)   
 
 Free Printable Periodic Tables (PDF and PNG)  
 [https://sciencenotes.org/printable-periodic-table/](https://sciencenotes.org/printable-periodic-table/)  
 
 [Empty tables of the periodic system](https://www.periodni.com/download.html#empty_table_of_the_periodic_system)  Generalic, Eni. "Download printable materials." EniG. Periodic Table of the Elements. KTF-Split, 22 Jan. 2021. Web. 15 Aug. 2021.   
 
 [https://www.ptable.com/print/](https://www.ptable.com/print/)  
 Se pueden elegir capas del pdf para hacer una tabla muda  
 
 [http://norris.org.au/expet/ptab/](http://norris.org.au/expet/ptab/) 

###   Tablas periódicas asociadas a estados de oxidación
 A veces referenciados como valencias. Ver  [página aparte](/home/recursos/quimica/estados-de-oxidacion) 

###   Recursos asociados a memorización tabla periódica
El tema de en qué momento hay que memorizarla es tema aparte: personalmente creo que en 2º y 3º ESO (Física y Química es obligatoria, puede ser su único contacto con la tabla) no debe memorizarse, sino reconocerla y saber entender la información que contiene y utilizarla. En documento RSEQ 2016 se indican recursos asociados a memorización: memorizar puede ser útil en cursos posteriores, y cada persona tiene su propias estrategias sin que haya algo que valga para todos  

*Es preciso que los estudiantes se acostumbren a reconocer la TP como una cuadrícula de columnas y filas donde se alojan los elementos químicos. Esta disposición de los elementos permite jugar con ella, a la vez que se va completando su aprendizaje. Los estudiantes guiados por sus profesores **aprenderán los elementos a través de canciones, reglas mnemotécnicas y juegos,** como la guerra de barcos donde el tablero de operaciones navales es una TP debidamente preparada para aprender los elementos químicos jugando con ellos, bien dando las coordenadas de los barcos, que están formados por dos o más elementos, o por su número atómico, o bien por su nombre o símbolo. \[3-5]*  

[3. P. Román Polo, La marcha de los elementos químicos, An. Quím., 2011, 107 (3), 262–265.](http://analesdequimica.es/index.php/AnalesQuimica/article/view/243/236)  
[4. P. Román Polo, La tabla periódica de los elementos químicos para niños y abogados, An. Quím., 2015, 111 (4), 247–253.](http://analesdequimica.es/index.php/AnalesQuimica/article/view/776/1028)  

Ejemplos grupo 1 “LiNa Ke es Rubia Casó en Francia”  
periodo 3 NorMAl. SusPenSo ClAro  
grupo 18 gases nobles “Hermano Negro Ármate Kontra la Xenofobia y el Racismo”  
5. K. Tripp, Periodic Table Battleship,  [http://teachbesideme.com/](http://teachbesideme.com/) ,  [bit.ly/1ZvG3wU](http://bit.ly/1ZvG3wU) , visitada el 23/02/2016.  

[![APRENDER RÁPIDO LA TABLA PERIÓDICA | Tips y Trucos. Breaking Vlad](https://img.youtube.com/vi/WnSS5mu28qs/0.jpg "APRENDER RÁPIDO LA TABLA PERIÓDICA | Tips y Trucos. Breaking Vlad")](https://www.youtube.com/watch?v=WnSS5mu28qs)  

Hilo con comentarios  
[twitter Biochiky/status/966925655005892608](https://twitter.com/Biochiky/status/966925655005892608)  
La BBC No Funciona (Li Be B C N O F Ne)  
eSCandinavia TIene Voluminosos CRáteres MuNdiales FEos, COmo NInguno CUando ZumbaN (Sc,Ti,V, Cr,Mn,Fe,Co,Ni,Cu,Zn).  
el Banco Central No Ofrece Fondos NEgociables  

[Reglas Mnemotécnicas Tabla Periódica](https://estudianteforever.com/reglas-mnemotecnicas-tabla-periodica/)  

Se citan otras ideas de memorizar en FQ, pendiente recolocar  
- Ácidos y bases fuertes: los que ni dan ni piden grado disociación  
- "reductor es dador y oxidante un mangante"  
- "los 7 magníficos" (los 7 elementos diatómicos)  
- Chatelier, que las rx. exotérmicas son como el verano, al que le viene mal un ⬆️T (y por eso el equilibrio va hacia atrás).  
- atraco en hora punta en metro (energía de ionización): hora punta, línea C4 Parla-Sol. Bolso en hombro con cremallera abierta y sin hacerle caso-->robo seguro. Misma situación, ahora bolso en bandolera, cogido con ambas manos y bien fuerte-->posibilidad de robo? Sí, pero más difícil. Conclusión: cuanto más pequeño es el átomo y más retenido tiene sus electrones, más energía tengo que darle para quitar un electrón, mayor energía de ionización.  

[IES Al Ándalus Enlaces a aplicaciones informáticas útiles (flash, scripts, etc). elaboradas por José Antonio Navarro.](http://web.archive.org/web/20190522155220/http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=139&Itemid=199)  
[Test sobre la tabla periódica:](http://web.archive.org/web/20190522155220/http://www.iesalandalus.com/joomla3/images/stories/FisicayQuimica/flash/formulacion/test_tablaperiodica_3eso.swf)  waybackmachine 
Comprueba tus conocimientos sobre los elementos de la tabla (nombre, símbolo, grupos).  
Jose Antonio Navarro Dominguez  

Hay recursos online adicionales, como  
[Tabla periódica, thatquiz.org](https://www.thatquiz.org/es-m/ciencia/tabla-periodica/)  
[Tabla periódica muda, educaplus](http://www.educaplus.org/sp2002/juegos/jtpmuda.html)  

* [TablaPeriodicaActividadesMemorización-ESO.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content//home/recursos/quimica/recursos-tabla-peridica/TablaPeriodicaActividadesMemorización-ESO.pdf)  

[twitter quimicaPau/status/1458441003543695363](https://twitter.com/quimicaPau/status/1458441003543695363)  
¿Con qué frases graciosas te aprendiste la tabla periódica?  

 [Memorize the Periodic Table](https://teachbesideme.com/memorize-periodic-table/)  
 [Memorize the First 20 Chemical Elements in 30 Minutes - memorize.academy](https://www.memorize.academy/first-20-elements-periodic-table)  
 Memorize the Periodic Table is a set of video lessons that uses visual memory to help you QUICKLY memorize all 118 elements of the periodic table.


###   Conversor texto a elementos de tabla periódica
Periodic Table Writer  
[http://www.myfunstudio.com/designs/pt/?source=mfs](http://www.myfunstudio.com/designs/pt/?source=mfs)  
[https://www.chemspeller.com/](https://www.chemspeller.com/)  
[https://www.lmntology.com/](https://www.lmntology.com/)  
[https://elementals.fun/](https://elementals.fun/)  
[https://www.bgreco.net/periodic/](https://www.bgreco.net/periodic/)  
[https://gist.github.com/Experiment5X/5257897](https://gist.github.com/Experiment5X/5257897) 

###   Enlaces generales
   * RSEQ Tabla periodica castellano citada en [Nombres y símbolos en español de los elementos aceptados por la IUPAC el 28 de noviembre de 2016 acordados por la RAC, la RAE, la RSEQ y la Fundéu](https://rseq.org/mat-didacticos/nombres-y-simbolos-en-espanol-de-los-elementos-aceptados-por-la-iupac-el-28-de-noviembre-de-2016-acordados-por-la-rac-la-rae-la-rseq-y-la-fundeu/)  
    ![](https://rseq.org/wp-content/uploads/2019/03/tabla-periodica-ultima-marzo_8_1_1.jpg)  

   * [12 Different Ways to Organize the Periodic Table of Elements - visualcapitalist.com](https://www.visualcapitalist.com/different-periodic-table-visualizations/)  
   ![](https://www.visualcapitalist.com/wp-content/uploads/2023/08/12-Different-Periodic-Tables.png)  
   * [IUPAC. What we do. Periodic table of elements](https://iupac.org/what-we-do/periodic-table-of-elements/) 
   * [RSEQ Nomenclatura Química y Normas de la IUPAC en Español (incluye tablas)](https://dialnet.unirioja.es/descarga/libro/873818.pdf)  
   * [Dynamic Periodic Table, ptable.com/](http://www.ptable.com/)  
   Michael Dayah. Material con derechos de autor (la redistribución electrónica está prohibida, pero permite su uso didáctico) Tabla dinámica en html, sin flash, que permite mostrar distintas informaciones. **Muy buena**
   *  The Periodic Table of Elements, in Pictures and Words  
   [http://elements.wlonk.com/ElementsTable.htm](http://elements.wlonk.com/ElementsTable.htm)  
   Copyright © 2005-2016 Keith Enevoldsen.  
   [Elements in Pictures and Words (2 pages)](http://elements.wlonk.com/Elements_Pics+Words_11x8.5.pdf)  
   [Elements in Pictures (1 page)](http://elements.wlonk.com/Elements_Pics_11x8.5.pdf)  
   [Elements in Pictures and Words (1 big page)](http://elements.wlonk.com/Elements_Pics+Words_11x17.pdf)  
   * [Google Research Periodic Table of Elements](http://research.google.com/bigpicture/elements/)   
   * [Google ArtsExperiments](https://artsexperiments.withgoogle.com/periodic-table/)  Permite ver los modelos de Bhor 3D, enlaza con AR  
   * [ELEMENTS. Experiments in characer design](http://kcd-elements.tumblr.com/)  
   Proyecto de diseño con personajes para cada elemento, Kaycie D.
   *  [twitter FiQuiPedia/status/1155196722186006530](https://twitter.com/FiQuiPedia/status/1155196722186006530)  
   Idea de actividad viendo esta genial tabla periódica de @nicgaston: pedir a los alumnos que busquen asociaciones ente emojis y elementos, explicando el por qué de esa asociación y aportando referencias. #IYPT2019  
   [![Tabla periódica emojis nicgaston](https://pbs.twimg.com/media/EAgVSuvWkAAMvWx?format=jpg "Tabla periódica emojis nicgaston") ](https://pbs.twimg.com/media/EAgVSuvWkAAMvWx?format=jpg) 
   * [Royal Society of Chemistry Periodic Table](http://www.rsc.org/periodic-table/) 
   * [The INTERNET Database of Periodic Tables](http://www.meta-synthesis.com/webbook/35_pt/pt_database.php)  
   There are hundreds of periodic tables in web space, but there is only one comprehensive database of periodic tables & periodic system formulations. If you know of an interesting periodic table that is missing, please contact the database curator: Dr Mark R Leach. 
   * Foro Nuclear. Tabla periódica. Rincón educativo  
   [Tabla periódica de los elementos químicos](http://www.rinconeducativo.org/contenidoextra/tablasperiodicas/tabla/tabla.php#tit)  
   [Línea temporal de la tabla periódica](http://rinconeducativo.org/contenidoextra/tablasperiodicas/lineatemporal/) 
   *  [periodictable.ru](http://www.periodictable.ru/index_en.html)  Incluye vídeos e imágenes sobre cada elemento (por ejemplo mirar estaño ...)
   *  [Interactive Periodic Table of the Elements, sciencenotes](https://sciencenotes.org/periodic-table-of-the-elements/) 
   ![https://commodity.com/wp-content/uploads/2017/11/PeriodicTableWorks.png](https://commodity.com/wp-content/uploads/2017/11/PeriodicTableWorks.png)  (tabla estática, fuente es sciencenotes.org)
   *  [WebElements: the periodic table on the web](http://www.webelements.com/) 
   *  [Chem4kids: Periodic Table](http://www.chem4kids.com/files/elem_pertable.html) 
   *  [Thatquiz.org: Tests Tabla periódica](http://www.thatquiz.org/es-m/ciencia/tabla-periodica/) 
   * Enlaces compartidos en Diigo, Aida Aivars  
   [diigo aidaivars tablas-periodicas](http://www.diigo.com/list/aidaivars/tablas-periodicas) 
   * The Most Beautiful Periodic Table Displays in the World  
   [Periodic table displays showcasing outstanding element samples](https://118displays.com/products/140-mm/) Comercial, jecha en madera, con muestras reales de elementos.
   *  [Conoce tus elementos, eltamiz](http://eltamiz.com/conoce-tus-elementos/)  Solo los 27 primeros (noviembre 2011)
   *  [La Tabla Periódica de las Científicas presenta una mujer por cada elemento químico con toda la información para conocerlas](https://www.quimicaysociedad.org/tabla-periodica/)  (incluye formato A3 y A0, enlace validado noviembre 2019)
   *  [Tabla etimológica de los elementos](http://www.acienciasgalilei.com/qui/tablaperiodica0-etimologica.htm) 
   *  [Etimología y geografía de la tabla periódica. Una propuesta](https://www.fronterad.com/etimologia-y-geografia-de-la-tabla-periodica-una-propuesta/) 
   * The Photographic Periodic Table of the Elements  
   [http://periodictable.com/](http://periodictable.com/)  Con fotografías, muchas de ellas se pueden rotar y ver en 3D
   *  [http://ciencianet.com/tabla.html](http://ciencianet.com/tabla.html)  
   Muy simple. Copyright 2000 Antonio Varela
   *  [Tabla periódica, lenntech](http://www.lenntech.es/periodica/tabla-periodica.htm)  
   Cada elemento químico contiene un enlace que explica sus propiedades químicas, efectos sobre la salud, efectos sobre el medio ambiente, datos de aplicación, fotografía y también información acerca de la historia y el descubridor de cada elemento. Copyright © 1998-2011 Lenntech B.V
   *  [http://www.chemicool.com/](http://www.chemicool.com/)  © chemicool.com
   *  [Periodic table, infoplease](http://www.infoplease.com/periodictable.php)  © 2000–2012 Pearson Education, publishing as Infoplease 
   * Tabla periódica con diagrama de Lewis  
   [Lewis Dot Diagrams of Selected Elements](http://hyperphysics.phy-astr.gsu.edu/hbase/pertab/perlewis.html)  HyperPhysics (©C.R. Nave, 2010)
   * Elementos y tabla periódica en IUPAC  
   [Periodic Table of the Elements, IUPAC](http://old.iupac.org/reports/periodic_table/index.html)  
   Tablas periódicas, en febrero 2012 incluye versión enero 2011 de IUPAC, con copyright  
   [The chemical elements. IUPAC](http://old.iupac.org/general/FAQs/elements.html)  Información general sobre elementos
   * [The Orbitron. A gallery of atomic orbitals and a few molecular orbitals](http://winter.group.shef.ac.uk/orbitron/)  
   Curiosa manera de ver la tabla periódica, a mí me encanta. Enlazo pero no incluyo la imagen porque tiene derechos de autor. Copyright 2002-2006 Mark Winter [The University of Sheffield]. All rights reserved. El enlace realmente es más sobre recursos relativos a orbitales.
   * [‘Sciart’ celebrating periodic table on show at St Catharine’s College, Cambridge](https://www.cambridgeindependent.co.uk/education/sciart-celebrating-periodic-table-on-show-at-st-catharine-9281697/)  
   ![](https://www.cambridgeindependent.co.uk/_media/img/750x0/L9ACVQXVY50QJLK1XPNG.jpg)  
   ![](https://www.cambridgeindependent.co.uk/_media/img/750x0/B4532MKIOC8HDMZKOUG2.jpg)  
   ![](https://www.cambridgeindependent.co.uk/_media/img/750x0/FGIHT8R7TY05EGZW07LY.jpg)  
   * Tabla de orbitales, Credit: Meszi, visualizing.all.things.science@gmail.com dentro de [The INTERNET Database of Periodic Tables](https://www.meta-synthesis.com/webbook/35_pt/pt_database.php)  
   ![](https://www.meta-synthesis.com/webbook/35_pt/orbitals_PT.png)  
   * [Earth Scientist's Periodic Table of the Elements and Their Ions](http://web.archive.org/web/20200218070631/http://www.gly.uga.edu/railsback/PTTalk/PTTalk002.html)  (waybackmachine)  
   Presentación explicando por qué una tabla periódica distinta y cómo interpretarla. Centrada en cómo se encuentran elementos, por ejemplo cada elemento aparece más de una vez si en la naturaleza suelen encontrarse iones distintos ...Bruce Railsack.
   * [Periodic Table of the Elements (Janet form)](http://www.ipgp.fr/%7Etarantola/Files/Professional/Mendeleev/)  
   Albert Tarantola.
   * [The Pictorial Periodic Table](http://web.archive.org/web/20141018085449/http://dwb.unl.edu/teacher/nsf/c04/c04links/chemlab.pc.maricopa.edu/periodic/about.html)  (waybackmachine)
   [Periodic tables alternatives](http://web.archive.org/web/20141018091835/http://dwb.unl.edu/Teacher/NSF/C04/C04Links/chemlab.pc.maricopa.edu/periodic/periodic.html)  (waybackmachine) 
   Información interesante como la lista de "Alternate styles for the Periodic Table" que incluye Stowe, Benfey, Zmaczynski, Filling, Giguere y Mendeleev. Copyright ©, 1995-1998 by Chris Heilman.
   * Tabla periódica FisQuiWeb, cc-by-nc-sa  
   [SPImprimible.pdf](http://fisquiweb.es/Apuntes/Apuntes1Bach/SPImprimible.pdf)  
   [SistemaPeriodico1B.pdf](http://fisquiweb.es/Apuntes/Apuntes1Bach/SistemaPeriodico1B.pdf) 
   * Tabla periódica WebQC  
   [http://www.webqc.org/periodictable.php](http://www.webqc.org/periodictable.php)  Copyright 2012 Vitalii Vanovschi
   * Ver tablas asociadas a [estados de oxidación / valencias](/home/recursos/quimica/estados-de-oxidacion) 
   * Tablas en VaxaSoftware, recursos libres para educación  
   [Tabla en color, solamente símbolo, Z, nombre, masa atómica. vaxasoftware](http://www.vaxasoftware.com/doc_eduen/qui/tpcolor.pdf)  
   [Tabla vacía, vaxasoftware](http://www.vaxasoftware.com/doc_eduen/qui/tpvacia.pdf)  
   *  [http://table.minutephysics.com/#mag](http://table.minutephysics.com/#mag) 
   *  [http://www.sciencegeek.net/tables/tables.shtml](http://www.sciencegeek.net/tables/tables.shtml)  Incluye muda en inglés
   *  [Hay una forma mejor de organizar los elementos químicos: un reloj](http://es.gizmodo.com/hay-una-forma-mejor-de-organizar-los-elementos-quimicos-1603495734)   
   Tabla periódica en forma de reloj!
   *  [http://kcd-elements.tumblr.com/](http://kcd-elements.tumblr.com/)  
   Experiments in character design All characters and artwork © Kaycie D. 2011-2017 “Elements - Experiments in Character Design” was my senior thesis project, which premiered at the Milwaukee Institute of Art & Design Senior Thesis Exhibition from April 2011 - May 2011 for 72 of the elements. In November of 2011, the remaining 40 elements were completed.
   *  [2019 Año internacional de la tabla periódica](http://institutoculturaldeleon.org.mx/icl/story/6546/2019-A-o-Internacional-de-la-Tabla-Peri-dica)  
   Incluye tabla por números de oxidación  
   [![](http://institutoculturaldeleon.org.mx/stories/6546.jpg "") ](http://institutoculturaldeleon.org.mx/stories/6546.jpg) 
   *  [https://periodic.donghwi.dev/](https://periodic.donghwi.dev/)  
   (visión en 3D de propiedades periódicas)
   * [PERIODIC TABLE OF THE ELEMENTS](https://www.periodni.com/index.html)   Generalic, Eni. "Periodic Table of the Elements, Calculators, and Printable Materials." EniG. Periodic Table of the Elements. KTF-Split, 22 Jan. 2021. Web. 15 Aug. 2021.  
   * [Tablas periódicas aprendiendofisicaconbertotomas](https://www.aprendiendofisicaconbertotomas.com/tablas)  
   Contenido en octubre 2024: THE ELEMENTS, ORBITALES, ESPECTROS, Dístein 1, Dístein 2, TPT, Notebook 1, Notebook 2, 150 aniversario, Notebook 3, XL New 1, XL New 2, XL New 3, XL New 4, XL New 5, XL, XXL, elements  
   ![](https://static.wixstatic.com/media/66bfed_a6c61ca36a674915b92fefa234b2f455~mv2.png/v1/fill/w_980,h_441,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/66bfed_a6c61ca36a674915b92fefa234b2f455~mv2.png)  
   ![](https://static.wixstatic.com/media/66bfed_f07bfba1e41042e88bab0dead36b6dd4~mv2.png/v1/fill/w_980,h_441,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/66bfed_f07bfba1e41042e88bab0dead36b6dd4~mv2.png)  
   * [Stamp Smarter Interactive Philatelic Periodic Table](https://stampsmarter.org/Learning/PeriodicTable.html)  
   [Tabla periódica filatélica interactiva](https://ztfnews.wordpress.com/2020/08/03/tabla-periodica-filatelica-interactiva/)  
   ![](https://ztfnews.files.wordpress.com/2020/08/95837998_246556196603030_9047697407613075456_n.jpg?w=450&h=572)  
   * [Tabla Periódica de Científicos - fisiquimicamente](https://fisiquimicamente.com/blog/2020/07/28/tabla-periodica-de-cientificos/)  
   * [ The Elements According to Relative Abundance - 1970 - digital.sciencehistory.org](https://digital.sciencehistory.org/works/b5644s48g)  
![](https://scihist-digicoll-production-derivatives.s3.amazonaws.com/80a937e0-00fd-47be-9c2a-9e5a4c52650f/thumb_large/ecb595854557da5a41b009b28e1a8f9c.jpg)  
   * [twitter DivuLCC/status/1605607912939950081](https://twitter.com/DivuLCC/status/1605607912939950081)  
   Como obsequio navideño, os dejo el enlace de la "Tabla scikuriódica", un librito en pdf donde describo cada elemento químico mediante un #sciku. Espero que os guste y ya me diréis cuál es vuestro favorito. ¡Felices fiestas!
[http://bit.ly/tabla_sciku](https://drive.google.com/file/d/1FpSiK0NV340uyyX0NV4cPbkURbwXPd6X/view)  
   
##   Canciones / vídeos

[![](https://img.youtube.com/vi/7kCCWWtCrpA/0.jpg)](https://www.youtube.com/watch?v=7kCCWWtCrpA "300 years of element discovery in 99 seconds") 300 years of element discovery in 99 seconds Jamie Gallagher  
[![](https://img.youtube.com/vi/ptg4iI8bXcA/0.jpg)](https://www.youtube.com/watch?v=ptg4iI8bXcA "300 years of element discovery in 60 seconds") 300 years of element discovery in 60 seconds Jamie Gallagher  



[![The Periodic Table Song | SCIENCE SONGS](https://img.youtube.com/vi/VgVQKCcfwnU/0.jpg "The Periodic Table Song | SCIENCE SONGS")](https://www.youtube.com/watch?v=VgVQKCcfwnU)  
(2015, no incluye nombres de 2016)

[twitter fqsaja1/status/1326049608489897985](https://twitter.com/fqsaja1/status/1326049608489897985)  
Aquí está (por favor leed hasta el final, hay varias cosas)  
Todo lo que usted siempre quiso saber sobre la tabla periódica y no se atrevía a preguntar.  
Un auténtico Vademecum sobre la TP. Una vídeo tabla periódica. Durante 18 meses #CovaGutierrez grabó esto  
Comenzado en el Año Internacional de la tabla periódica e interrumpido por la COVID. Trabajo descomunal que involucra a 120 alumnos y profesores de varios Centros de Cantabria. Información actualizada y rigurosa hasta conformar un mosaico impactante. Así  
[![Promo vídeos tabla periódica](https://img.youtube.com/vi/1RglQZT_Gqo/0.jpg "Promo vídeos tabla periódica")](https://www.youtube.com/watch?v=1RglQZT_Gqo)

[VIDEO-TABLA PERIÓDICA](http://www.fqsaja.com/?p=9797)   
[![](http://www.fqsaja.com/wp-content/uploads/2020/11/tabla1200-1024x506.jpg "") ](http://www.fqsaja.com/wp-content/uploads/2020/11/tabla1200-1024x506.jpg)  
La “VIDEO-TABLA PERIÓDICA” del departamento de Física y Química del IES Valle del Saja se puede ver en la siguiente dirección web:  
 [Video-Tabla Periódica I.E.S. Valle del Saja, thinglink](https://www.thinglink.com/scene/1374766234444234754)  
 Pero esta tabla es una tabla interactiva ya que se debe clicar en la etiqueta que aparece sobre el símbolo químico para, de esa forma, tener acceso al vídeo en el que se cuenta la información relativa al elemento correspondiente a dicho símbolo.  
 Además se han dispuesto individualmente cada uno de los 120 vídeos correspondientes a los 118 elementos descubiertos y a los de números atómicos 119 y 120 aún por descubrir, en esta misma web, concretamente en el apartado de vídeos etiquetado como «Tabla periódica»:  
  [I.E.S. Valle del Saja, vídeos](http://www.fqsaja.com/?page_id=7430)  
  En esa dirección pueden verse ordenados por su número atómico y número de orden en la tabla periódica.

[![5. LA TABLA PERIÓDICA - LMNs español, dibujos animados](https://img.youtube.com/vi/bluv42N3eTc/0.jpg "Promo vídeos tabla periódica")](https://www.youtube.com/watch?v=bluv42N3eTc "5. LA TABLA PERIÓDICA - LMNs español, dibujos animados")

##   Actividades tabla periódica

   * Ejercicios para practicar basados en thatquiz (ver enlace asociado)
- Asociar Z a elemento  
[thatquiz.org es, periodos 1 a 4, nº atómico, tabla periódica](http://www.thatquiz.org/es-m/?-j140f-l4-n14-p0)  
[thatquiz.org es, periodos 1 a 4, nº atómico, parejas](http://www.thatquiz.org/es-m/?-j300f-l4-n14-p0)  
- Asociar nombre y símbolo  
[thatquiz.org es, periodos 1 a 4, símbolo, texto sencillo](http://www.thatquiz.org/es-m/?-j84f-l4-n14-p0)  
[thatquiz.org es, periodos 1 a 3, símbolo, texto sencillo](http://www.thatquiz.org/es-m/?-j847-l3-ni-p0)  
- Configuración electrónica  
[thatquiz.org es, periodos 1 a 4, electrones, tabla periódica](http://www.thatquiz.org/es-m/?-j50f-l4-n14-p0)  

Se puede elegir un tipo de prueba en  
[thatquiz.org es tabla periódica](http://www.thatquiz.org/es-m/ciencia/tabla-periodica/) 


[twitter onio72/status/1449408700796612613](https://twitter.com/onio72/status/1449408700796612613)  
[FQ4 Tabla periódica y propiedades](https://www.geogebra.org/m/pectrhad)  
Animación con @geogebra para trabajar propiedades periódicas y datos que nos ofrece la tabla periódica a nivel de 4º de ESO.  
Antonio González García  

Aunque se propone para 4ºESO, puede servir como repaso en bachillerato para propiedades periódicas  


   * Actividad asociada tabla periódica, basada en un libro, desconozco si las preguntas tienen derecho de autor
Asociada a una pregunta del libro "Cien preguntas básicas sobre la Ciencia" de Isaac Asimov, "¿Cuáles son los elementos químicos más activos y por qué?"  
[Libros maravillosos, Cien preguntas](http://www.librosmaravillosos.com/cienpreguntas/index.html)  
Preguntas que se pueden responder consultando la tabla:  
a) Razona qué elemento será más activo, el sodio, el magnesio o el aluminio.  
b) Razona qué elemento será más activo, el fósforo, el azufre o el cloro  
c) Razona qué elemento será más activo, el berilio, el magnesio o el calcio  
d) Razona qué elemento será más activo, el oxígeno, el azufre o el selenio  
e) ¿Qué quiere decir que el isótopo más estable del francio tiene una vida media de 21 minutos?  

[Periodic Table Battleship!: A Fun Way To Learn the Elements](http://www.openculture.com/2016/10/periodic-table-battleship-a-fun-way-to-learn-the-elements.html)  

[Sweet 16 Periodic Table Tournament](https://mathequalslove.blogspot.com.es/2016/10/sweet-16-periodic-table-tournament.html)  
[![](https://4.bp.blogspot.com/-AIMQ9EpOR8I/V_75aseYO8I/AAAAAAAAz0c/x-F1Srx7HDM6VFP0Q4Zm1XDfn6pzYznlACKgB/s400/IMG_20161012_100634664.jpg "Sweet 16 Periodic Table Tournament") ](https://4.bp.blogspot.com/-AIMQ9EpOR8I/V_75aseYO8I/AAAAAAAAz0c/x-F1Srx7HDM6VFP0Q4Zm1XDfn6pzYznlACKgB/s1600/IMG_20161012_100634664.jpg)  

[Sweet 16 Periodic Table Tournament](http://www.flinnsci.com/media/910126/cf10943.pdf) Flinn Scientific  

[twitter Beatriz_Nino/status/974236884242137088](https://twitter.com/Beatriz_Nino/status/974236884242137088)  
Dejo por aquí las cuatro versiones del torneo de la tabla periódica, visto en el blog de @**mathequalslove** basado en  
[Sweet 16 Periodic Table  Tournament, Flinn Scientific](https://www.flinnsci.com/api/library/Download/e7d675182a624e968a639e5db4e6307d)  
Las cuatro versiones tienen los mismos elementos, pero cambia el enunciado de las reglas y el "formato Champions"  
[TORNEO DE LA TABLA PERIÓDICA.pdf](https://drive.google.com/file/d/1higDG1ALTbFGLOXGWMFa2eIgTbuPVoSs/view)  

[#ChemistryAdvent #IYPT2019 Day 22: A periodic table of element mistakes](https://www.compoundchem.com/2019advent/day22/)  
[![#ChemistryAdvent #IYPT2019 Day 22: A periodic table of element mistakes](https://i1.wp.com/www.compoundchem.com/wp-content/uploads/2019/12/22-–-The-periodic-table-of-common-mistakes.png "#ChemistryAdvent #IYPT2019 Day 22: A periodic table of element mistakes") ](https://i1.wp.com/www.compoundchem.com/wp-content/uploads/2019/12/22-%E2%80%93-The-periodic-table-of-common-mistakes.png)  

[Locating elements Walk Around](http://passionatelycurioussci.weebly.com/google-form-walk-arounds.html#elements)  


[Kahoot tabla periódica FQ 3º ESO - fruzgz](https://create.kahoot.it/share/kahoot-tabla-periodica-fq-3-eso/c4a4555a-f8ec-4790-9f78-2edf51ac252a)

###   Juegos
[Quimitris *waybackmachine*](http://web.archive.org/web/20210303061610/http://quimitris.com/)  (necesita adoble Flash)  
Quimitris es un juego basado en el Tetris para el aprendizaje de la Tabla Periódica de los elementos de forma entretenida. Las fichas, formadas por uno, dos, tres o cuatro elementos químicos, caerán desde la parte superior del tablero y deberás colocarlas de forma correcta en la Tabla Periódica. El juego se encuentra dividido en diferentes  [niveles y fases](http://www.quimitris.com/web/niveles_y_fases.php)  que aumentarán su dificultad de forma progresiva.  

En 2021 indica "Quimitris ya no funciona pues utiliza la tecnología Flash. Y esta tecnología dejó de funcionar en los navegadores en enero de 2021." aunque sí se puede hacer funcionar la versión archivada, ver [simulaciones, aspectos técnicos](/home/recursos/simulaciones/recursos-simulaciones-aspectos-tecnicos) 

Este juego [Tabla periódica de los elementos - redribera](https://www.redribera.es/games/jugar_a_m3159_tabla-periodica-de-los-elementos) no está operativo en 2021 ni el swf archivado en waybackmachine.


[52 games with the periodic table - español](https://52gamespt.wordpress.com/category/espanol/)  

16 enero 2016  
Convierte la tabla periódica en un juego de hundir la flota para enseñar a sus hijos  
[Convierte la tabla periódica en un juego de hundir la flota para enseñar a sus hijos](http://amp.europapress.es/desconecta/curiosity/noticia-convierte-tabla-periodica-juego-hundir-flota-ensenar-hijos-20160114125111.html) 

### Quimingo (Bingo químico)  

[Jugando y aprendiendo con la Tabla periódica” Juego: QUIMINGO - fcq.nc.edu.ar](https://w3.fcq.unc.edu.ar/sites/default/files/nochedelosmuseos/quimingo_-_jugando_y_aprendiendo_con_la_tabla_periodica.pdf)  


### "Quién es quién"  
Surge primero como idea a partir de un adaptación para geometría  
[twitter mamaterremoto/status/992383333932982278](https://twitter.com/mamaterremoto/status/992383333932982278)  
[twitter spanishcuesta/status/992225808675758080](https://twitter.com/spanishcuesta/status/992225808675758080)  
El segundo quién es quién pero ahora de geometría, me encanta!!   
[![El segundo quién es quién pero ahora de geometría, me encanta!! ](https://pbs.twimg.com/media/DcUYHDDX0AAScaw.jpg "El segundo quién es quién pero ahora de geometría, me encanta!!") ](https://pbs.twimg.com/media/DcUYHDDX0AAScaw.jpg) 
Se hace primero una adaptación para científicos  

25 julio 2018  
Primera versión  
[twitter quimicahipolito/status/1022160116211220481](https://twitter.com/quimicahipolito/status/1022160116211220481)  
Juego del "Quién es quien científico" con 24 personajes históricos de la ciencia. Lo que dan de si las vacaciones jejeje  

31 julio 2018  
[twitter quimicahipolito/status/1035566500638281728](https://twitter.com/quimicahipolito/status/1035566500638281728)  
"Quién es quién de personajes científicos " con soporte de tabla de madera de ranuras finas e inclinadas. Adjunto dirección con las fichas para imprimir y si alguien quiere se lo puedo enviar por email en Word o me decís dónde colgarlo.  

Hay versiones asociadas a  [recursos ciencia y mujer](/home/recursos/ciencia-y-mujer)   

25 agosto 2018  
[twitter quimicahipolito/status/1033292684461522944](https://twitter.com/quimicahipolito/status/1033292684461522944)  
Nuevo juego que he creado "Quién es quien de la Tabla Periódica" . Esta vez hecho sobre el juego original. Solo queda probarlo en algún día de clase.  
[![Quién es quien de la Tabla Periódica](https://pbs.twimg.com/media/Dlb-EvUX4AA1jNw.jpg "Quién es quien de la Tabla Periódica") ](https://pbs.twimg.com/media/Dlb-EvUX4AA1jNw.jpg)  
[![Quién es quien de la Tabla Periódica](https://pbs.twimg.com/media/Dlb-MPPW4AE6Tz8?format=jpg "Quién es quien de la Tabla Periódica") ](https://pbs.twimg.com/media/Dlb-MPPW4AE6Tz8?format=jpg)  

9 septiembre 2018  
[twitter quimicahipolito/status/1038811061120589825](https://twitter.com/quimicahipolito/status/1038811061120589825)  
Comparto con todos las fichas para crear el juego Quién es quien de la Tabla Periódica sobre el original, con alguna ficha extra de repuesto.  

Surgen comentarios  
[twitter PichonsotOW/status/1033801556754395136](https://twitter.com/PichonsotOW/status/1033801556754395136)  
Es motivador sin duda. Su buscás en google "Guess Who Periodic Table" encontrás cosas parecidas. También recuerda a este otro juego:  
[Adivina quién soy](https://clickmica.fundaciondescubre.es/recursos/juegos/adivina-quien-soy/)  

Variante con compuestos químicos  
[twitter pablofcayqca/status/1295706081247858693](https://twitter.com/pablofcayqca/status/1295706081247858693)  
¡Muy buenas! Estoy organizando los recursos de la página para que se puedan utilizar más fácilmente ☺️ Dejo por aquí el enlace, por si os pueden ser de utilidad (hoy el Quién es quién de compuestos químicos):  
![](https://pbs.twimg.com/media/EftFz62X0AEx-hv?format=jpg)  
[Aprendizaje Basado en Juegos - facebook.com](https://www.facebook.com/568120733609203/posts/995355730885699/)  
[Quién es quién (Pablo - Física y Química).pdf](https://drive.google.com/file/d/1O8FMz4OnEt7jBXlP7hDk0LZiHTHasxda/view)  
  
### Hundir la flota / barcos 

[Periodic Table Game for Kids: Periodic Table Battleship](https://teachbesideme.com/periodic-table-battleship/)  

[twitter pablofcayqca/status/1306989827397746688](https://twitter.com/pablofcayqca/status/1306989827397746688)  
¡Muy buenas! Comparto por aquí “La granja química”, una adaptación del Hundir la flota con la que aprender la posición de los elementos químicos de una manera divertida y segura:  
[Presentación la granja química](https://view.genial.ly/5f64c03479626a0d7128bff5/presentation-la-granja-quimica)  (+ info  [facebook](https://facebook.com/568120733609203/posts/1020063755081563/?extid=M3GUXB6jIhcOEWwD&d=n) ) #ABJentiemposdelCOVID

[twitter ivanquifis/status/1774737662001217826](https://twitter.com/ivanquifis/status/1774737662001217826)
Usando en el aula la fantástica actividad 'Hundir la flota periódica' de @JGilMunoz, se me ocurrió que podría incluir una hoja de movimientos para trabajar otros conceptos, además del de grupo y período.  
[Original](http://radicalbarbatilo.blogspot.com/2021/02/hundir-la-flota-periodica.html)  
[Adaptada](https://drive.google.com/file/d/1K1D6A68ucx9GOeo2T8LHX8LbVkfgIsW_/view?usp=share_link)  

### Rosco / pasapalabra

[twitter margaritatm/status/1089443814580912128](https://twitter.com/margaritatm/status/1089443814580912128)  
\#CienciaCiertaFyQ #IYPT2019 @IUPAC #WeLikeChemistry  
[![](https://pbs.twimg.com/media/Dx57ATyW0AAlCWs.jpg "") ](https://pbs.twimg.com/media/Dx57ATyW0AAlCWs.jpg)  

[twitter iesjoseplanes/status/1088736958740922368](https://twitter.com/iesjoseplanes/status/1088736958740922368)  
\#PASAporTABLA #StoTomås #Gooooo  
[![](https://pbs.twimg.com/media/Dxv4cj-W0AEymbv.jpg "") ](https://pbs.twimg.com/media/Dxv4cj-W0AEymbv.jpg)  

### Cartas de la tabla periódica

Hay una [baraja de Kaycie D.](https://ko-fi.com/kcdstudios/shop)  

[Tabla periódica, grupo SM](https://www.grupo-sm.com/es/post/tabla-periodica)  
La Asamblea General de Naciones Unidas ha proclamado 2019 como el Año Internacional de la Tabla Periódica de los Elementos Químicos. Para celebrarlo, os presentamos una baraja con 72 cartas para jugar a las “familias” y a otros juegos, usando cartas de elementos químicos y de científicos y científicas que tuvieron un papel relevante en relación con la tabla periódica. Además cuentas con dos comodines.  
La baraja de la Tabla Periódica es una colaboración entre SM, Carmen Arribas (ETSIAE-UPM) y Bernardo Herradón (IQOG-CSIC), miembros de la junta de gobierno de la sección territorial de Madrid de la Real Sociedad Española de Química (RSEQ-STM).  
Para descargar la baraja de cartas hay que registrarse, es gratuito.  
[Baraja tabla periódica](https://mkg.educamos.com/baraja-tabla-periodica)  
[Instrucciones_Baraja-1-3.pdf](https://www.grupo-sm.com/es/es/sites/sm-espana/files/resources/imagenes/MKT/Blog/descargables/Instrucciones_Baraja-1-3.pdf)  
Se pueden descargar datos por grupo  
[Grupo1.pdf](https://www.grupo-sm.com/sites/sm-espana/files/resources/imagenes/MKT/Blog/descargables/Grupo1.pdf)  

Comentario sobre cartas asociado a quién es quién 
[twitter josefpm/status/1033802903667068928](https://twitter.com/josefpm/status/1033802903667068928)  
La idea es muy buena, yo usaría en lugar el juego cartas por que así puedes incluir los 118 elementos y no solo 48 sería el mismo juego solo que en lugar de lo de plástico con cartas, demás ya hay barajas de tablas periódicas así que trabajo ahorrado  

### Bingo de la tabla periódica

[twitter leoalrom/status/1033797026834776066](https://twitter.com/leoalrom/status/1033797026834776066)  
En mi clase adaptamos un bingo para aprender los símbolos de la tabla periódica. Yo canto el nombre y en el cartón pone el símbolo. Encontré el juego en Internet, no es invento mío

[BINGO Elementos de la tabla periódica - quimicadeunvistazo.blogspot.com](https://quimicadeunvistazo.blogspot.com/2022/06/bingo-elementos-de-la-tabla-periodica.html)  
![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhN8kgs06DmO4Sjwqu-9gz8MYg9v4T2rMBjt9I_QJSYFfd2dwRQsrcf1KqPqWQK0OG3uEpSdBgr4yPxMK-YCuAGHTebNcrL6hZn4dgE10gY1OBAMXBZoI-yGJyEL8-WqBhP1yZWFchaQ3xueotPlrvgnTsot-xVOCvVLy7GgDAz7fMP6ST3lmvP5RugRA/w453-h640/1.png)  
[Bingo de la tabla periódica. Canva](https://www.canva.com/design/DAFD-ArNAAA/JqfsnZ4I5rZV2rrF0giZfA/view?utm_content=DAFD-ArNAAA&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)  


##   Propiedades periódicas
En ptable.com se pueden ver gráficamente.  
[Propiedades periódicas.pdf](https://machete2000.files.wordpress.com/2018/09/propiedades-peric3b3dicas.pdf)  
[Tamaño de los átomos: Radios atómicos e iónicos](http://quimicaconnosotras.blogspot.com/p/tama.html)  
[De modelos va la cosa: Radio atómico y radio iónico](https://www.taringa.net/+ciencia_educacion/de-modelos-va-la-cosa-radio-atomico-y-radio-ionico_13obwn)  
[Diferencia entre el radio atómico y el radio iónico](https://es.sawakinome.com/articles/science/difference-between-atomic-radius-and-ionic-radius.html)  
[![Diferencia entre el radio atómico y el radio iónico](https://www.sawakinome.com/img/images/difference-between-atomic-radius-and-ionic-radius_3.png "Diferencia entre el radio atómico y el radio iónico") ](https://www.sawakinome.com/img/images/difference-between-atomic-radius-and-ionic-radius_3.png)  
[Sizes of Atoms and Ions, Principles of General Chemistry](https://2012books.lardbucket.org/books/principles-of-general-chemistry-v1.0/s11-02-sizes-of-atoms-and-ions.html)  
[![Figure 7.9 Ionic Radii (in Picometers) of the Most Common Oxidation States of the s-, p-, and d-Block Elements](https://2012books.lardbucket.org/books/principles-of-general-chemistry-v1.0/section_11/d94e877d1d04a26ba570df5bf8dec412.jpg "Figure 7.9 Ionic Radii (in Picometers) of the Most Common Oxidation States of the s-, p-, and d-Block Elements") ](https://2012books.lardbucket.org/books/principles-of-general-chemistry-v1.0/section_11/d94e877d1d04a26ba570df5bf8dec412.jpg)  

[Periodic table of mangetism - minutelabs.io](https://labs.minutelabs.io/PeriodicTable/#mag)  

## Apantallamiento

[Electron Shielding - chem.libretexts.org](https://chem.libretexts.org/Bookshelves/Introductory_Chemistry/Introductory_Chemistry_(CK-12)/06%3A_The_Periodic_Table/6.18%3A_Electron_Shielding)  


##   **Comics/dibujos sobre tabla periódica, elementos y sus propiedades**  

[twitter FiQuiPedia/status/1211083599711277056](https://twitter.com/FiQuiPedia/status/1211083599711277056)
Viendo este tuit parece ser que el autor es F.J.Moyano en los 80  
[twitter LTPeriodica/status/1210818161420967936](https://twitter.com/LTPeriodica/status/1210818161420967936)  
Localizada versión de 1983 en 
[Sistema Peryódico](http://www.oocities.org/es/matesbueno/sistema_peryodico.htm)  
![](https://pbs.twimg.com/media/EM6iGi8WwAE75Os?format=jpg "Sistema Peryódico 1983")  

![](https://pbs.twimg.com/media/EMsy4EzXsAI9Pnd?format=jpg "Sistema Peryódico 1996")  

 [Cómo lucirían los elementos químicos si fueran personas](https://genial.guru/creacion-arte/como-lucirian-los-elementos-quimicos-si-fueran-personas-279610/)  
 [![Flúor amigo de todos](https://wl-genial.cf.tsp.li/resize/728x/jpg/e34/827/5db7fc5270b4b5090a060a9f5d.jpg "Flúor amigo de todos") ](https://wl-genial.cf.tsp.li/resize/728x/jpg/e34/827/5db7fc5270b4b5090a060a9f5d.jpg)   

[[Thorium chemical avengers2, wirdou](https://wirdou.com/2012/07/11/thorium-chemical-avengers-2/)  
![](https://wirdou.files.wordpress.com/2012/07/thor.jpg "Thorium chemical avengers2, wirdou")  
[Captain americium the-chemical avengers3, wirdou](https://wirdou.com/2012/07/20/captain-americium-the-chemical-avengers-3/ "Captain americium the-chemical avengers3, wirdou")  
![](https://wirdou.files.wordpress.com/2012/07/captain-america.jpg "Captain americium the-chemical avengers3, wirdou")  
[Hulk the chemical avengers 4, wirdou](https://wirdou.com/2012/07/23/hulk-the-chemical-avengers-4/)  
[![](https://wirdou.files.wordpress.com/2012/07/hulk.jpg "Hulk the chemical avengers 4, wirdou") ](https://wirdou.files.wordpress.com/2012/07/hulk.jpg)  
[Cemical avengersassemble, wirdou](https://wirdou.com/2012/08/06/chemical-avengers-assemble/)  
[![](https://wirdou.files.wordpress.com/2012/08/chemical-avengers.jpg "Cemical avengersassemble, wirdou") ](https://wirdou.files.wordpress.com/2012/08/chemical-avengers.jpg)  
[Noble gases, wirdou](https://wirdou.com/2012/07/15/noble-gases/)  
[![](https://wirdou.files.wordpress.com/2012/07/noble-gases.jpg "Noble gases, wirdou") ](https://wirdou.files.wordpress.com/2012/07/noble-gases.jpg)  
[Heavy metals, wirdou](https://wirdou.com/2013/03/20/heavy-metals/) (Sin imagen en 2019)  
[Larga visa al heavy metal, wirdou](https://wirdou.com/science/espanol/larga-vida-al-heavy-metal/)  
[![](https://wirdou.files.wordpress.com/2017/02/larga-vida-al-heavy-metal.jpg "Larga visa al heavy metal, wirdou") ](https://wirdou.files.wordpress.com/2017/02/larga-vida-al-heavy-metal.jpg)  
[Noble metals, wirdou](https://wirdou.com/2015/12/13/noble-metals/)  
[![](https://wirdou.files.wordpress.com/2015/12/noble-metals.jpg "Noble metals, wirdou") ](https://wirdou.files.wordpress.com/2015/12/noble-metals.jpg)   
[Lead Zeppelin, wirdou](https://wirdou.com/2013/05/17/lead-zeppelin/)  
[![](https://wirdou.files.wordpress.com/2013/05/lead-zeppelin.jpg "Lead Zeppelin, wirdou") ](https://wirdou.files.wordpress.com/2013/05/lead-zeppelin.jpg)  
[Freddie Mercury, devianart](https://www.deviantart.com/wirdoudesigns/art/Freddie-Mercury-305299641)  
[Freddie Mercury, wirdou](https://wirdou.com/2012/04/15/freddie-mercury/)  
[![](https://wirdou.files.wordpress.com/2012/04/freddie-mercury1.jpg "Freddie Mercury, Wirdou") ](https://wirdou.files.wordpress.com/2012/04/freddie-mercury1.jpg)  
![](https://wirdou.files.wordpress.com/2019/03/web-helium.jpg "He")  
![](https://wirdou.files.wordpress.com/2019/03/web-fluor.jpg "F")  
![](https://wirdou.files.wordpress.com/2019/03/web-neon.jpg "Ne")  
![](https://wirdou.files.wordpress.com/2019/03/web-chlorine.jpg "Cl")  
![](https://wirdou.files.wordpress.com/2019/03/web-argon.jpg "Ar")  
![](https://wirdou.files.wordpress.com/2012/11/chemical-superman.jpg "Kr")  
![](https://wirdou.files.wordpress.com/2019/03/web-krypton.jpg "Kr")  
![](https://wirdou.files.wordpress.com/2012/09/silver-surfer.jpg "Ag")  
![](https://wirdou.files.wordpress.com/2019/03/web-xenon.jpg "Xe")   
![](https://wirdou.files.wordpress.com/2019/03/web-gollum.jpg "Au")  
![](https://wirdou.files.wordpress.com/2019/03/web-radocc81n.jpg "Rn")  
![](https://wirdou.files.wordpress.com/2019/06/web-neptunium-aquaman.jpg "Np")

[twitter MongeDraws/status/1039609192347324417](https://twitter.com/MongeDraws/status/1039609192347324417)  
Badum Tssssss!! #bateria #litio #quimica #rock #musica  
[![](https://pbs.twimg.com/media/Dm1vCz6X4AI0Rz3.jpg "") ](https://pbs.twimg.com/media/Dm1vCz6X4AI0Rz3.jpg)   
[twitter Biochiky/status/1207541655445950464](https://twitter.com/Biochiky/status/1207541655445950464)  
Ahora todo tiene sentido.  
Imagen tomada en la conferencia "Clausura del Año Internacional de la Tabla Periódica"  
[![](https://pbs.twimg.com/media/EMIMufmWkAAaxF4?format=jpg "") ](https://pbs.twimg.com/media/EMIMufmWkAAaxF4?format=jpg) 

##   Vídeos sobre la tabla periódica (humor)
[![Chemical Party (Fiesta de Quimica)](https://img.youtube.com/vi/hCgVGzhLpXo/0.jpg "Chemical Party (Fiesta de Quimica)")](https://www.youtube.com/watch?v=hCgVGzhLpXo)


###   Tablas periódicas en ficción
 Pueden ser tablas periódicas o referencias a "elementos de ficción, no existentes en la tabla periódica real"  
En los Simpsons aparece el salchichonio (que aparece en olimpiadas química fase local Madrid 2018)  
[![The Simpsons present: salchichonio-bolognium-pamplinum](https://img.youtube.com/vi/ay0dnLeCGGw/0.jpg "The Simpsons present: salchichonio-bolognium-pamplinum")](https://www.youtube.com/watch?v=ay0dnLeCGGw)  

En un capítulo ven la tabla de una escuela privada que tiene 250 elementos  

[The Bart Wants What It Wants". Season 13 / Episode 11](https://frinkiac.com/caption/S13E11/212671)  
> Their periodic table has 250 elements!  
And our school board's cut us back to 16.  
All of them lanthanides.  

[twitter xkcd/status/1613184044933730304](https://twitter.com/xkcd/status/1613184044933730304)  
![](https://pbs.twimg.com/media/FmMuVpBWAAAXaNA?format=jpg)   

  
En un capítulo de Bob esponja especial navidad aparece una tabla periódica y el elemento "malvadonio"  

En Iron Man surge un nuevo elemento "Vibranio"  [Vibranium - wikipedia](https://es.wikipedia.org/wiki/Vibranium)  

Otra referencia  [Metal Nth - wikipedia](https://es.wikipedia.org/wiki/Metal_Nth)  

[twitter quimicafacilnet/status/1618097848809316355](https://twitter.com/quimicafacilnet/status/1618097848809316355)  
Ideal para el examen visual de los amantes de la #química #chemistry #Ciencia #Science. Créditos a su autor  
![](https://pbs.twimg.com/media/FnSjawGXoAAiFqX?format=jpg)  


###   Otras "tablas periódicas" no relacionadas con química
 El formato de tabla periódica se usa en otras cosas:  

Títulos de canciones [![](https://i2.wp.com/www.compoundchem.com/wp-content/uploads/2019/12/21-%E2%80%93-The-periodic-table-of-elements-in-song-titles-v2.png?ssl=1)](https://www.compoundchem.com/2019advent/day21/ "#ChemistryAdvent #IYPT2019 Day 21: A periodic table of elements in song titles")  

 FP  [twitter tecnifor/status/977819239682801665](https://twitter.com/tecnifor/status/977819239682801665)  (tiene enlaces a información, en marzo 2018 v3.0  
 
[La tabla periódica de la ortografía, sinfaltas](https://sinfaltas.com/2016/12/08/la-tabla-periodica-de-la-ortografia/)  
 
[La tabla periódica de las tonterías irracionales, naukas](https://listadelaverguenza.naukas.com/2019/01/02/la-tabla-periodica-de-las-tonterias-irracionales/)  
 
[The Periodic Table of Storytelling](https://jamesharris.design/periodic/)  
 
 [http://jamesharris.design/periodic/](http://jamesharris.design/periodic/)  
 
 [twitter SoutoManel/status/1280531977016410114](https://twitter.com/SoutoManel/status/1280531977016410114)  
 I made this Periodic Table using Marvel characters to be used as mnemonic for young students to help them memorize the elements.  Enjoy! #ChemTwitter #PeriodicTableChallenge  
 [![](https://pbs.twimg.com/media/EcVZBu2WAAA7RG8?format=jpg "") ](https://pbs.twimg.com/media/EcVZBu2WAAA7RG8?format=jpg)  
 
 
[twitter alcarazr/status/1282952622882922497](https://twitter.com/alcarazr/status/1282952622882922497)  
Inspirado por @SoutoManel aquí está mi versión DC. Próximamente en alta resolución y con enlaces a cada personaje / Inspired by @SoutoManel here's my DC version. Coming soon in high resolution and with links to each character  
[![](https://pbs.twimg.com/media/Ec32dTRWoAAMjUL?format=jpg "") ](https://pbs.twimg.com/media/Ec32dTRWoAAMjUL?format=jpg) 
  
