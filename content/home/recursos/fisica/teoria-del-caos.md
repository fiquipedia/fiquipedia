
# Teoría del caos

Se introduce en el [currículo de Física de 2º de Bachillerato LOMCE](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomce)
> caos determinista  
>7. Interpretar el caos determinista en el contexto de la interacción gravitatoria.  
>7.1. Describe la dificultad de resolver el movimiento de tres cuerpos sometidos a la interacción gravitatoria mutua utilizando el concepto de caos.  

Deja de citarse en el [currículo de Física de 2º de Bachillerato LOMLOE](/home/materias/bachillerato/fisica-2bachillerato/curriculo-fisica-2-bachillerato-lomloe)
  
 Aunque históricamente la idea de caos surge en  [gravitación](/home/recursos/fisica/recursos-gravitacion)  y asociado al problema de los tres cuerpos, es algo más general y que aplica a varias disciplinas además de la física.  
Uno de los precursores fue Poincaré, que presentó un estudio sobre la estabilidad del Sistema Solar como parte de un concurso en el siglo XIX  
 [Henri Poincaré - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Henri_Poincar%C3%A9#El_problema_de_los_tres_cuerpos)   
Un caso importante es el  [problema restringido](https://es.wikipedia.org/wiki/Problema_de_los_tres_cuerpos#El_problema_de_los_tres_cuerpos_restringido_o_de_Euler) , en el que la masa de uno de los tres objetos es despreciable, que tiene a su veces otros casos como el problema restringido circular  
 En la solución del problema restringido circular surgen 5 puntos de equilibrio que se llaman puntos de Langrange y se numeran de L1 a L5  
 [Puntos de Lagrange - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Puntos_de_Lagrange)   
Son puntos de utilidad directa: en L1 está  [SOHO (Solar and Heliospheric Observatory)](https://es.wikipedia.org/wiki/SOHO)  y en L2 está  [Wilkinson Microwave Anisotropy Probe (WMAP)](https://es.wikipedia.org/wiki/WMAP)  ( [NASA](https://es.wikipedia.org/wiki/NASA) )  
Existen algunas soluciones concretas (en 1993 se encontró una solución con trayectoria en forma de 8)   
  
En general la teoría del caos sin pensar solamente en gravitación se puede ver como un caso de dinámica no lineal. Dinámica no hace referencia solamente a movimiento de cuerpos, sino en "evolución del valor de una variable de un sistema". Aplica a movimiento de osciladores no lineales ( [péndulo doble](https://es.wikipedia.org/wiki/Doble_p%C3%A9ndulo) ,  [péndulo magnético](https://es.wikipedia.org/wiki/P%C3%A9ndulo_ca%C3%B3tico) ), evolución de poblaciones, economía, dinámica de fluidos, y hay ejemplos matemáticos como el  [atractor de Lorentz](https://es.wikipedia.org/wiki/Atractor_de_Lorenz)   
  
 Aunque se habla de caos, se trata de caos **determinista** porque el sistema aunque es caótico en el sentido de tener un comportamiento que parece poco previsible, sí se pueden encontrar ecuaciones para el movimiento fijadas las condiciones iniciales. Comparando:  

| Sistemas no caóticos   | Sistemas caóticos   |
|:-|:-| 
| Sí existe proporcionalidad: un cambio pequeño en variables produce cambios pequeños | No existe proporcionalidad: un pequeño cambio puede producir cambios enormes (enlaza con la metáfora del efecto mariposa: el aleteo de una mariposa puede producir un tornado en el otro extremo de la Tierra) | 
| Existe la aditividad/linealidad: el efecto total es la suma de efectos | **No existe aditividad/linealidad**: el efecto total no es la suma de efectos. | 
| Son reproducibles/previsibles: con condiciones iniciales determinadas se reproduce resultado | No es reproducible/previsible debido a la **alta sensibilidad a las condiciones iniciales**, en general es complejo reproducir exactamente y por pequeñas variaciones habrá resultados distintos.   |   

Otras ideas asociadas a sistemas caóticos son:  
- **Recurrencia**: aunque las trayectorias sea erráticas no se pierden en el infinito, sino que vuelven de manera recurrente a ciertos puntos "atractores"  
- **Autosimilaridad**: se producen estructuras que se asemejan a sí mismas a distintas escalas de observación (enlaza con fractales)  
  
  
Introducción al régimen caótico  
[El oscilador de Fermi](http://www.sc.ehu.es/sbweb/fisica3/oscilaciones/fermi/fermi.html)  
Un sistema que experimenta un movimiento caótico nunca se repite a sí mismo, sino que más bien se comporta de forma continuamente diferente, el movimiento puede parecer totalmente aleatorio y desordenado. No obstante, el movimiento caótico está muy lejos de ser totalmente desordenado y por el contrario, exhibe una estructura definida que resulta de pronto aparente. Otro aspecto del caos, es su extrema sensibilidad a las condiciones iniciales que es lo que vamos a ver en esta simulación.  
  
## Recursos generales
  
 En la píldora 10 de la RSEF se indican recursos sobre "introducción al caos"  [PildoraFisicaGEEFN10.pdf](http://rsef.es/images/Fisica/PildoraFisicaGEEFN10.pdf)   
En las jornadas de la RSEF del 1 de diciembre de 2016 se trató y hay una presentación  [Conferencia sobre el Caos determinista. Miguel Ángel Fernández Sanjuán](https://rsef.es/images/Fisica/CAOS_RSEF_Sanjuan_2016.pdf)   

 [Real Sociedad Española de Física - Jornada de Enseñanza de la Fisica. 1 de diciembre de 2016](https://rsef.es/noticias-actividades-geef/item/896-jornada-de-ensenanza-de-la-fisica-1-de-diciembre-de-2016)  
 
[TEORIA DEL CAOS O EFECTO MARIPOSA](https://es.slideshare.net/Euler/teoria-del-caos)   
Presentación 14 páginas  

## Vídeos
 Entrevista con James Yorke, el padre de la Teoría del Caos (English)   
 [![Entrevista con James Yorke, el padre de la Teoría del Caos (English) - YouTube](https://img.youtube.com/vi/CBj70zSjKx4/0.jpg)](https://www.youtube.com/watch?v=CBj70zSjKx4)   
  
 [![](https://img.youtube.com/vi/d3uOKlIEIoU/0.jpg)](https://www.youtube.com/watch?v=d3uOKlIEIoU "La teoría del caos, explicada con un péndulo. Materia Ciencia - YouTube")   

 [![](https://img.youtube.com/vi/fDek6cYijxI/0.jpg)](https://www.youtube.com/watch?v=fDek6cYijxI "Chaos: The Science of the Butterfly Effect - veritasium")   
 
 [![](https://img.youtube.com/vi/ovJcsL7vyrk/0.jpg)](https://www.youtube.com/watch?v=ovJcsL7vyrk "This equation will change how you see the world (the logistic map)")   

[CHAOS. UNA AVENTURA MATEMÁTICA](https://www.chaos-math.org/es/la-pelicula.html)  
CAOS es una película matemática dirigida a todo público, que consta de nueve capítulos con una duración de trece minutos cada uno. Trata de sistemas dinámicos, del efecto mariposa y de la teoría del caos.   

[![](https://img.youtube.com/vi/w-IHJbzRVVU/0.jpg)](https://www.youtube.com/watch?v=w-IHJbzRVVU "Tim Palmer Public Lecture: Climate Change, Chaos, and Inexact Computing. Perimeter Institute for Theoretical Physics")  

[![](https://img.youtube.com/vi/vFdZ9t4Y5hQ/0.jpg)](https://www.youtube.com/watch?v=vFdZ9t4Y5hQ "Magnetic Pendulum: An example of a chaotic system")  

[![](https://img.youtube.com/vi/nDTxJlXLoZE/0.jpg)](https://www.youtube.com/watch?v=nDTxJlXLoZE "3A95.52 Mechanical Chaos Demonstrations - Three Magnet Configuration")  

  
## Fractales 

[DIVULGACIÓN CIENTÍFICA. Fractales: Arte, Ciencia y Naturaleza](https://www.laizquierdadiario.com/Fractales-Arte-Ciencia-y-Naturaleza)  

> ¿Qué es un fractal? Se lo puede definir como una forma geométrica que se repite a sí misma en cualquier escala (o grado de amplificación de una lupa) a la que se la observe. Esa propiedad característica de los fractales de presentar un aspecto similar ante cualquier ampliación se denomina autosemejanza. El término fractal proviene del hecho de que su aspecto puede ser muy irregular y fragmentado.  
> ...  
> En nuestra geometría corriente (euclideana) una línea o una curva es evidentemente un objeto unidimensional y decimos que tiene dimensión 1. De la misma forma podemos asociar una dimensión 2 a una superficie y una dimensión 3 a un volumen. Son las dimensiones 1D, 2D y 3D. La curva fractal de la Figura2 no parece ajustarse a la dimensión 1 del segmento de partida, más bien parece tener cierto espesor. Efectivamente, los fractales tienen una dimensionalidad propia que los caracteriza reflejando su complejidad. Existen formas de medir esa dimensionalidad fractal. En la curva de Koch es D = 1,26…, señalando que la figura geométrica fractal está entre una línea 1D y una superficie 2D. Es algo así como una curva plana irregular que “casi” llena un plano.  
> ...  
> el conjunto de Cantor está conformado por esos infinitos puntos aislados, separados por espacios vacíos entre ellos. Puede probarse que la longitud de esos infinitos puntos aislados es cero. No obstante, la dimensión fractal no es cero, sino que es D = 0,63… Nuevamente obtenemos un número no entero, en éste caso una fracción entre 0 y 1, que denota que el Conjunto de Cantor es un fractal que está entre la dimensión euclideana cero de los puntos aislados del conjunto y la dimensión euclidea de valor 1 correspondiente a una línea.  

[Soy un fractal - naukas.com](https://naukas.com/2018/04/13/soy-un-fractal/)  
[Geometría fractal y multifractal - blogs.ua.es/dimates](https://blogs.ua.es/dimates/2022/05/15/geometria-fractal/)  
[Geometría fractal para la detección eficaz de tumores -](https://elpais.com/ciencia/cafe-y-teoremas/2022-03-01/geometria-fractal-para-la-deteccion-eficaz-de-tumores.html)  
> Estos métodos permiten caracterizar los cambios de irregularidad que se producen en los contornos de las células, tejidos y redes vasculares durante el desarrollo de alguna masa anormal  
[Geometría fractal en la ciencia del suelo - madrimasd](https://www.madrimasd.org/blogs/universo/2011/09/22/138470)  
[La retención del agua en los suelos, explicada mediante modelos fractales - uab.cat](https://www.uab.cat/servlet/Satellite?cid=1096481466568&pagename=UABDivulga/Page/TemplatePageDetallArticleInvestigar&param1=1177482616804&setletertype=_a)  

[twitter Rainmaker1973/status/1540029318042161160](https://twitter.com/Rainmaker1973/status/1540029318042161160)  
The Sierpiński triangle is probably one of the most popular fractals. There are different ways to construct it: this one is called 'chaos game'.  


## Material para demostraciones
 [The Chaos Machine (Double Pendulum) - instructables.com](http://www.instructables.com/id/The-Chaos-Machine-Double-Pendulum/)   
 [Amazon.com: Fat Brain Toys Chaos Machine Building &amp; Construction for Babies: Toys &amp; Games](https://www.amazon.com/Fat-Brain-Toys-Chaos-Machine/dp/B00PG1MKV0)   
 [Amazon.com: American Educational Products 6-40020 Koontz The Chaotic Pendulum Retail Packaging: Industrial &amp; Scientific](https://www.amazon.com/American-Educational-Products-6-40020-Packaging/dp/B00BWJ48II)   
 [Péndulo caótico - 3bscientific.com.es](https://www.3bscientific.com.es/pendulo-caotico-e-1017531-u8557340-3b-scientific,p_576_25075.html)  
 [Double Pendulum - makezine.com](http://makezine.com/projects/make-22/double-pendulum/)   

## Simulaciones / ejemplo  

Las ecuaciones son complejas, por lo que en Bachillerato se pueden usar simulaciones y ver cualitativamente la dependencia de las condiciones iniciales, la sensibilidad a modificarlas y lo impredecible del comportamiento.  

[ Simulando el comportamiento caótico - elrincondelaciencia](https://sites.google.com/site/elrincondelacienciavkent/el-rinc%C3%B3n-de-la-ciencia/n%C3%BAmeros-anteriores/n%C3%BAmero-19/simulaci%C3%B3n-caos)  
nº 19 (Diciembre-2002) (RC-49b) José Antonio Martínez Pons   


### Gravitación - tres o más cuerpos "problema de los tres cuerpos" 
  
 Mi sistema solar, permite poner más de 2 cuerpos  
 [Mi sistema solar (HTML)](https://phet.colorado.edu/sims/html/my-solar-system/latest/my-solar-system_es.html)   
  
 [3-Body Gravitational Problem](http://faraday.physics.utoronto.ca/PVB/Harrison/Flash/Chaos/ThreeBody/ThreeBody.html)   
 [Chaotic Planets | MinuteLabs.io](http://labs.minutelabs.io/Chaotic-Planets/)   

[File:5 4 800 36 downscaled.gif](https://en.m.wikipedia.org/wiki/File:5_4_800_36_downscaled.gif)  
![](https://upload.wikimedia.org/wikipedia/commons/5/5a/5_4_800_36_downscaled.gif)  

[The Three-Body Problem, Longitude at Sea, and Lagrange’s Points](https://galileo-unbound.blog/2019/07/05/the-three-body-problem-longitude-at-sea-and-lagranges-points/)  

[A few three body periodic orbits @PeRossello - youtube shorts](https://www.youtube.com/shorts/6EvaV-BYVxM)  

[Three-Body Periodic Orbits - jpl.nasa.gov](https://ssd.jpl.nasa.gov/tools/periodic_orbits.html) 


### Gravitación, geología y caos
   
[twitter ecosdelfuturo/status/1464691807116316680](https://twitter.com/ecosdelfuturo/status/1464691807116316680)  
Si miran con ojos de científico a esta imagen del Monte Gibliscemi en Sicilia verán cosas que no creerían que fuesen posibles: ciclos orbitales, cambios climáticos extraordinarios e incluso una manera de doblegar el caos en el Sistema Solar. Acompáñenme en este viaje fasciante  
Empezamos en un periodo húmedo Africano durante el Tortoniense (hace 8-9 Ma), cuando la escorrentía del Nilo estratificó la superficie del Mediterráneo, inhibió la ventilación del fondo marino y provocó la preservación de sedimentos ricos en orgánicos formando el Sapropel.  
Las bandas más oscuras de la imagen del Monte Gibliscemi están hechas de este Sapropel en patrones repetidos de manera uniformemente espaciada con intercalación de margas de calcita (bandas blanquecinas) correspondiente a periodos de sequía africana.  
![](https://pbs.twimg.com/media/FFOg2UFWUAkN-Um?format=png)  
Las bandas de Sapropel pueden relacionarse con los ciclos orbitales de Milankovitch y están dominadas por la periodicidad de cien mil años que forzó los ciclos de sequía y humedad de influencia persistente en el clima del norte y este de África.  
![](https://pbs.twimg.com/media/FFOXWODXMAoAB2c?format=png)  
No estamos seguros de los mecanismos que provocan los ciclos climáticos a partir del cambio de insolación producido por los movimientos orbitales, pero ¡la conexión está ahí impresa en las rocas sedimentarias!  
[Milankovitch (Orbital) Cycles and Their Role in Earth's Climate - nasa.gov](https://climate.nasa.gov/news/2948/milankovitch-orbital-cycles-and-their-role-in-earths-climate/)  
El comportamiento caótico del sistema solar impone, además, un límite de ∼50 Ma para identificar una solución orbital única, ya que pequeñas diferencias en las condiciones iniciales hacen que las soluciones astronómicas diverjan alrededor de esa edad.  
![](https://pbs.twimg.com/media/FFR4sqkWQAgDMkh?format=png)  
En 2017, los autores de un estudio en Nature utilizaron la alternancia de piedra caliza y pizarra en rocas sedimentaria de la formación Niobrara en Colorado para descubrir una transición de resonancia caótica de las órbitas de Marte y la Tierra hace 87 millones de años.  
Estas resonancias ocurren cuando los dos planetas tiran periódicamente el uno del otro al aproximarse en sus órbitas. La transición de la resonancia por cambios orbitales es un indicador de caos en el Sistema Solar, ¡una predicción de 1989 confirmada!  
[From rocks in Colorado, evidence of a ‘chaotic solar system’](https://ls.wisc.edu/news/from-rocks-in-colorado-evidence-of-a-chaotic-solar-system)  
Pero todavía no existía una solución numérica única. En 2019, Zeebe y Lourens consiguieron identificarla y prolongar su validez en 8 Ma al analizar datos de sedimentos en testigos perforados en el fondo del Océano Atlántico durante el límite del Paleoceno-Eoceno (58-53 Ma)  
Y consiguieron algo más asombroso aún: utilizar esta identificación de la solución que encajaba con el registro geólogico para poner una fecha muy precisa a uno de los cambios climáticos más extraordinarios que se nos aparecen en dicho registro: el PEMT  
![](https://pbs.twimg.com/media/FFR5fYSWYAQVgDN?format=jpg)  
El PETM (Máximo Térmico Paleoceno-Eoceno) fue un calentamiento global probablemente inducido por emisiones de CO2 de una Provincia Ígnea que durante decenas de miles de años elevó la tª oceánica unos 5-8ºC y que, sin embargo, no puede rivalizar con la velocidad del actual.  
![](https://pbs.twimg.com/media/FFR-7xXWQAUjmtb?format=png)  
Esta línea de investigación continúa llevando la ciencia aún más allá de lo increíble. Paul Olsen y colaboradores consiguieron identificar la existencia del ciclo de Milankovitch de 405.000 años hasta hace uno 200 Ma  	
[Scientists track deep history of planets' motions, and effects on Earth's climate](https://phys.org/news/2019-03-scientists-track-deep-history-planets.html)  
Además de obtener valores muy precisos para la precesión del perihelio de los planetas interiores a partir de sedimentos de lagos tropicales de 223 a 199 millones de años de antigüedad, ¡circunnavegado así el problema del caos del Sistema Solar!  
[Mapping Solar System chaos with the Geological Orrery](https://www.pnas.org/content/116/22/10664)  
Para acabar, un último estudio que amplia el análisis de los ciclos de Milankovitch ¡hasta hace 1400 Ma! a la vez que reconstruye la historia de las características del sistema solar, incluida la distancia entre la Tierra y la Luna y la duración del día.  
[Proterozoic Milankovitch cycles and the history of the solar system](https://www.pnas.org/content/115/25/6363)  


### Péndulo caótico / péndulo doble  

 [Chaotic Pendulum | MinuteLabs.io](http://labs.minutelabs.io/Chaotic-Pendulum/)   
 
 [Double Pendulum - myphysicslab](https://www.myphysicslab.com/pendulum/double-pendulum-en.html)  
 [Chaotic Pendulum - myphysicslab](https://www.myphysicslab.com/pendulum/chaotic-pendulum-en.html)  
 [Two Chaotic Pendulums - myphysicslab](https://www.myphysicslab.com/pendulum/compare-pendulum-en.html)  
  
 Double Pendulum Simulator, OpenSourcePhysicsSG [](https://play.google.com/store/apps/dev?id=7915608832562607433)   
 [Double Pendulum Simulator - Apps on Google Play](https://play.google.com/store/apps/details?id=com.ionicframework.dpendulumapp262907)   
  
  
 Pendulum Studio, Voladd (péndulo en general, incluye péndulo doble)  [](https://play.google.com/store/apps/developer?id=Voladd)   
 [Pendulum Studio - Apps on Google Play](https://play.google.com/store/apps/details?id=com.vlvolad.pendulumstudio)   
  
 Double Double-pendulum, Ashley P. Willis [](https://play.google.com/store/apps/developer?id=Ashley+P.+Willis)   
 [Double Double-pendulum - Apps on Google Play](https://play.google.com/store/apps/details?id=org.openpipeflow.doubledoublependulum)   
  
 [twitter InertialObservr/status/1178455832385605632](https://twitter.com/InertialObservr/status/1178455832385605632) 41 triple pendulums with 𝑠𝑙𝑖𝑔𝘩𝑡𝑙𝑦 different initial conditions   
  
 (source: [Cp2AZd3V52nIuMQ5WiwV8vg7uuLc6UKjRsA3VZH6ObM.gif](https://external-preview.redd.it/Cp2AZd3V52nIuMQ5WiwV8vg7uuLc6UKjRsA3VZH6ObM.gif?format=mp4&s=345404eed06a65d6138df53328bfdd8c193366fc) )  

 [![butterfly effect - one million double pendulums](https://img.youtube.com/vi/U7SLv0ePWU0/0.jpg)](https://www.youtube.com/watch?v=U7SLv0ePWU0 "butterfly effect - one million double pendulums")   

[twitter pendulum_bot](https://twitter.com/pendulum_bot)  
Every couple of hours I post a GIF of double pendulum released from a random position to swing for 30 seconds.  

### Circuito de Chua
 [Circuito de Chua - wikipedia.org](https://es.wikipedia.org/wiki/Circuito_de_Chua)   

 [Simulation - chuacircuits.com](http://www.chuacircuits.com/sim.php)   


### Oscilaciones en régimen caótico
El oscilador de Fermi [El oscilador de Fermi](http://www.sc.ehu.es/sbweb/fisica3/oscilaciones/fermi/fermi.html)   
La máquina de Zeeman [La m&aacute;quina de Zeeman](http://www.sc.ehu.es/sbweb/fisica3/oscilaciones/zeeman/zeeman.html)   
El goteo de un grifo [El goteo de un grifo](http://www.sc.ehu.es/sbweb/fisica3/oscilaciones/gota/gota.html)   


### Reacción BZ
  
 [Reacción de Beloúsov-Zhabotinski - wikipedia.org](https://es.wikipedia.org/wiki/Reacci%C3%B3n_de_Belo%C3%BAsov-Zhabotinski)   
 
