
# Proyectos de investigación 

##  Normativa sobre proyectos de investigación
 Con LOMCE en Física y Química se introduce como contenidos en varios cursos la idea de realizar un proyecto/trabajo de investigación.  
 Se puede ver en  [Docencia contenidos física y química por niveles según currículo](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/docencia-contenidos-fisica-y-quimica-por-nivel-segun-curriculo)  
La definición que se realiza en los criterios y estándares del  [currículo de física y química de 3º ESO](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce) , comunes a 2º ESO, da una idea.  

*6. Desarrollar pequeños **trabajos de investigación** en los que se ponga en práctica la aplicación del método científico y la utilización de las TIC.*  
*6.1. Realiza pequeños **trabajos de investigación **sobre algún tema objeto de estudio aplicando el método científico, y utilizando las TIC para la búsqueda y selección de información y presentación de conclusiones.*  
*6.2. Participa, valora, gestiona y respeta el trabajo individual y en equipo.*  

También aparece en 4º ESO  

*8. Elaborar y defender un ***proyecto de investigación**, aplicando las TIC.*  
*8.1. Elabora y defiende un **proyecto de investigación**, sobre un tema de interés científico, utilizando las TIC.*  

En 1º Bachillerato  

*1.6. A partir de un texto científico, extrae e interpreta la información, argumenta con rigor y precisión utilizando la terminología adecuada.*  
*2.1. Emplea aplicaciones virtuales interactivas para simular experimentos físicos de difícil realización en el laboratorio. 2.2. Establece los elementos esenciales para el diseño, la elaboración y defensa de un **proyecto de investigación**, sobre un tema de actualidad científica, vinculado con la Física o la Química, utilizando preferentemente las TIC.*  
 
En 2º Bachillerato Química  
*4.4. Realiza y defiende un ***trabajo de investigación** utilizando las TIC.*

##  El proyecto de investigación no solamente hay que realizarlo, hay que presentarlo y defenderlo
Se puede ver en normativa citada:  
2º ESO presentación  
4º ESO defender  
1º Bachillerato defensa  
2º Bachillerato defiende  

###  Proyectos de investigación en Bachillerato excelencia Madrid
Es algo obligatorio según normativa  
En decreto 63/2012 se indica  
*Artículo 3.- Características generales del Programa de Excelencia en Bachillerato*  
...  
3. El Programa de Excelencia en Bachillerato deberá contemplar la organización de actividades, cursos o seminarios de profundización en las distintas materias que componen el plan de estudios. **Los alumnos que cursen el Programa de Excelencia realizarán un proyecto de investigación.**

En orden ORDEN 11995/2012 se indica  

*Artículo 5.- Horario general del centro*  
...  
3. En horario vespertino, los alumnos del Programa participarán en las distintas -actividades programadas por el equipo docente destinadas **a la elaboración del proyecto de investigación** y a profundizar en su formación.  
...  
*Artículo 7.- **Proyecto de investigación**  
1. Los alumnos que cursen el Programa de Excelencia realizarán un **proyecto de investigación que se desarrollará, con carácter general, a lo largo de los dos cursos del Bachillerato.**  
2. La calificación obtenida por los alumnos en el proyecto de investigación podrá ser tenida en cuenta por los profesores en la evaluación de las distintas materias que cursen en segundo de Bachillerato.  
3. En caso de aplicación de lo recogido en el apartado anterior, los departamentos de coordinación didáctica incluirán en las correspondientes programaciones didácticas la repercusión del proyecto de investigación en la calificación de las materias que se cursen en segundo de Bachillerato, e informarán de ello al comienzo del período lectivo, haciendo públicos los criterios establecidos.  

En web del IES Pintor Antonio López comentan un planteamiento  
[iespintorantoniolopez.org Instrucciones para la realizacion del Proyecto de Investigacion](http://iespintorantoniolopez.org/files/Instrucciones%20para%20la%20realizacion%20del%20Proyecto%20de%20Investigacion.pdf)  

En web del IES Alkal'a Nahar se comparte información [IES Alkal'a Nahar Proyectos/Bach. Excelencia](https://www.educa2.madrid.org/web/centro.ies.alkalanahar.alcala/bach.-excelencia) 

##  Proyectos de investigación y STEM (Science, Technology, Engineering and Mathematics)
 Está relacionado, se plantean proyectos con varias disciplinas y de varias sesiones. A veces se combina con más disciplinas ESTREAM (Ethics, Reading, Arts).  
[https://www.teachengineering.org/](https://www.teachengineering.org/)  
"STEM curriculum for K-12" se plantean actividades por áreas, con buscador.

##  Planteamiento de los proyectos de investigación


Un proyecto de investigación real supondría una "tutorización" para guiar al alumno a que investigue de verdad algo personalizado y abierto (como ocurre en Bachillerato de excelencia), pero con las ratios reales los profesores pueden hacer poco más que encargar "tareas generales y cerradas"


###  Proyectos de investigación cerrados
 Se hace un planteamiento simple para ESO / Bachillerato; se da un "tema" y un "guión" ya elaborado. Se sabe de antemano el resultado que debe tener esa investigación.  
 Si hay desdobles los proyectos de investigación se pueden enlazar con las  [prácticas de laboratorio](/home/recursos/practicas-experimentos-laboratorio)  pero se trata de algo más ligado a las TIC y a procesamiento de información, o a experimentos caseros.  
 Se puede enlazar a contenidos del currículo condicionados por laboratorio, TIC y simulaciones (mencionado en los currículos, por ejemplo en  [currículo de física y química de 3º ESO](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce) )  
Creo que es relevante que normativa no indica cuantos debe haber por curso: por ejemplo puede ser uno en todo el curso o uno por trimestre.  

De manera general se trata de realizar una pequeña investigación (enlaza con el bloque "Actividad científica" presente en todos los cursos) en la que usar el método científico: plantear hipótesis, realizar experimentos o procesar datos, y analizar resultados.  

Se puede plantear que el resultado sea un informe científico simple, individual o en grupo, con cierto formato / extensión mínima y máxima / vía de entrega (ver comentarios sobre guión de práctica en  [prácticas de laboratorio de elaboración propia](/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia) )  
En los cursos de ESO al introducir el método científico se puede plantear como obligatorio que al inicio del proyecto haya una hipótesis de partida y se descarte o valide. En otros cursos y para investigaciones que no sean experimentando pueden no ser necesarias hipótesis.  
Por ejemplo  
1. Título del proyecto y miembros del grupo de laboratorio que la han realizado (en caso de que la elaboración sea en grupo).  
2. Descripción del proyecto: tema a investigar, posibles hipótesis, experimento a realizar  
3. Experimento o investigación realizada: materiales, procedimiento seguido  
4. Resultados: se indican las medidas realizadas, cálculos realizados y resultados, datos obtenidos consultando, que en algún caso pueden darse en forma de tabla o gráfica.  
5. Conclusiones: qué podemos deducir del análisis de los resultados obtenidos. Si se han planteado hipótesis, debe confirmarlas o descartarlas.  
6. Fuentes de información consultadas: libros, internet, dando referencia (citando autor y fecha si es posible)

###  Proyectos de investigación asociados pseudociencia / escepticismo / cuestionarse afirmaciones
 Se puede ver dentro de los cerrados, porque el objetivo es claro, pero la metodología puede ser abierta.  
 Se puede ver como ejemplos de aplicación de método científico y espíritu escéptico para cuestionarse si son científicas ciertas afirmaciones.  
Investigar sobre homeopatía, medicinas alternativas, ...  
Un ejemplo podría ser buscar vídeos "pop corn mobile" y realizar un trabajo con cálculos sobre si es posible con datos de potencia radiada por móviles y con la energía necesaria que necesita un grano de maíz.

###  Proyectos de investigación abiertos
Se puede plantear el proyecto de una manera mucho más abierta, dejando elegir el tema, los pasos, intentando fomentar la creatividad.  
Un ejemplo es enmarcarlo en algunos concursos; será un proyecto por curso y de mayor envergadura  
Ejemplos  
[http://www.injuve.es/formacion/noticia/certamen-jovenes-investigadores-2015](http://www.injuve.es/formacion/noticia/certamen-jovenes-investigadores-2015)  
[http://beamline-for-schools.web.cern.ch/](http://beamline-for-schools.web.cern.ch/)  
[https://www.raspberrypi.org/blog/announcing-european-astro-pi-challenge/](https://www.raspberrypi.org/blog/announcing-european-astro-pi-challenge/)  
[http://www.odysseus-contest.eu/es/](http://www.odysseus-contest.eu/es/)   

###  Proyectos de investigación enlazados con ABP
Se puede enlazar el Aprendizaje Basado en Proyectos (ABP) con pequeñas investigaciones que pueden servir como proyectos de investigación sin que se esté realizando del todo un ABP  
Ver  [Recursos Aprendizaje Basado en Proyectos en Física y Química](/home/recursos/recursos-aprendizaje-basado-proyectos-fisica-quimica)  

###  Proyectos de investigación experimentales / teóricos
Lo ideal es que supongan implementar en cierto modo el método científico: hipótesis, experimentos con variables controladas, dependientes, independientes, análisis de resultados (esas ideas se mencionan en Física y Química de 4ºESO), aunque también pueden teóricos, con búsqueda de información científica, contrastando información de distintas fuentes, validando la importancia de la fuente y aprendiendo a interpretar información científica, que pueden ser experimentos realizados por otros.  
Ejemplos / ideas relacionadas:  

[http://francis.naukas.com/2018/01/01/la-fisica-del-reto-de-la-botella/](http://francis.naukas.com/2018/01/01/la-fisica-del-reto-de-la-botella/)  
En resumen, un bonito ejercicio elemental de física que será del gusto de muchos jóvenes estudiantes; de hecho, la mayoría seguro que se enfrentó al reto hace un año y pico. Los profesores de física deben aprovechar todas las oportunidades para motivar a sus estudiantes en el estudio de temas que, a priori, son difíciles de asimilar, como el movimiento rotacional.  

[http://francis.naukas.com/2015/05/05/emdrive-un-horno-de-microondas-troncoconico-autopropulsado/](http://francis.naukas.com/2015/05/05/emdrive-un-horno-de-microondas-troncoconico-autopropulsado/)  
El artículo contiene un grave error que indica que el autor no domina la teoría de la relatividad especial (siendo ingeniero aeronáutico no es descabellado). Recomiendo a todos los estudiantes de física que lean esto que se lean dicho artículo y como ejercicio encuentren dicho error. Recomiendo a todos los profesores de física que lean esto que les pongan como ejercicio a sus alumnos encontrar el error en este artículo de Shawyer. 

[twitter bblanc0/status/1506405276353769483](https://twitter.com/bblanc0/status/1506405276353769483)  
“El caudal del grifo de mi casa”  
Un mini proyecto para hacer en casa.  
Lo primero que les he preguntado es que caudal piensan que arroja un grifo 🚰de los de su casa. Mejor si piensan en uno concreto.   
Les he pedido lo que anoten a boli, para no borrarlo, en la libreta y sin mirar lo que han anotado sus compis.  
No es fácil dar una estimación a ojo de una magnitud de este tipo. Estoy segura de que a muchos adultos les resultaría complicado.  
Puestos en situación hemos hablado de cómo podían hacer para medir este caudal en sus casa en un grifo concreto.  
Fijando un tiempo y midiendo el agua. Fijando una cantidad de agua y midiendo el tiempo.   
A partir de ahí hacer los cálculos para estimar en un minuto.  
Ahora toca hacer.   
Para quedar constancia han grabado un vídeo en el que no necesitaba que salieran sus caras bastaba sus manos y su voz.   
Tenía que contener:  
- Presentación de ellos y del proyecto
- Materiales que iban a usar. 
- Medición.
- Cálculos
- Conclusión.
Medir el caudal de una fuente⛲️ es una de las actividades que suelo  incluir en la ruta por Guareña con @mathcitymap
.  
Con los chicos de refuerzo es una actividad que suelo hacer en el instituto. En ese caso me gusta usar un cubo de un dm^3 para recoger el agua.   

##  Recursos sobre proyectos de investigación
[PROYECTO DE INVESTIGACIÓN FÍSICA Y QUÍMICA 3º ESO](http://www.educa2.madrid.org/web/educamadrid/principal/files/5c0d8d25-9f47-4cb4-9516-c8c8feade079/PROYECTO%20DE%20INVESTIGACI%C3%93N.pdf?t=1442265023431)  
 
En libros de la editorial SM de Física y Química de 2º y 4º ESO vienen ejemplos de proyectos de investigación, 3 por curso  
Ejemplo 2º ESO  
- "Limpiar la plata" (reacciones químicas)  
- "Hogar dulce hogar" (transmisión de calor)  
- "El tren de la ciencia" (velocidad)  
Ejemplo edebé 2º ESO:
- Comprobar la absorción de agua de distintos tipos de materiales  

[https://procomun.educalab.es/es/articulos/proyecto-de-investigacion-los-caminos-de-la-luz](https://procomun.educalab.es/es/articulos/proyecto-de-investigacion-los-caminos-de-la-luz)  
Es un ejemplo no directamente asociado a Física y Química, pero muestra que hay una etiqueta en procomun y pueden ir compartiéndose más  

[Proyecto de Investigación. 3º ESO - quifiblogeso](http://quifiblogeso.blogspot.com.es/2015/10/proyecto-de-investigacion-3-eso.html)   

Se citan ejemplos  [http://www.esdelibro.es/investigadores/-in-asignaturas/asignaturas/ciencias](http://www.esdelibro.es/investigadores/-in-asignaturas/asignaturas/ciencias)  

Trabajos de investigación en Bachillerato  
Guía del Alumno  
Rev.2 / 2014-2015  
I.E.S. «Infanta Elena»  
Bachillerato de Investigación  
[edu.xunta.gal GUIA_DEL_ALUMNO_14-15.pdf](https://www.edu.xunta.gal/centros/iesames/aulavirtual2/pluginfile.php/17434/mod_resource/content/0/GUIA_DEL_ALUMNO_14-15.pdf)  (no operativo en 2021)  

[fisquiweb.es Laboratorio virtual muelle](https://fisquiweb.es/Laboratorio/MetodoCientifico3/AdapLabVirtual.htm)  
Se sugiere realizar un pequeño trabajo de investigación: estudiar la forma en que se estira un muelle al colgar de él masas crecientes. El objetivo final es encontrar una expresión matemática que nos relacione la masa con el alargamiento experimentado por el muelle.  

[fisquiweb.es Laboratorio virtual péndulo](https://fisquiweb.es/Laboratorio/MetodoCientifico/AdapLabVirtual.htm)  
Se propone investigar el movimiento oscilatorio de un péndulo simple.

A la hora de plantear ideas sobre proyectos, se pueden tomar ideas de los ig Nobel

[Anexo:Galardonados con el premio Ig Nobel - wikipedia.org](https://es.wikipedia.org/wiki/Anexo:Galardonados_con_el_premio_Ig_Nobel)  

[Sitio web oficial de los premios Ig Nobel](https://www.improbable.com/)  

[Investigation of hindwing folding in ladybird beetles by artificial elytron transplantation and microcomputed tomography](https://www.pnas.org/content/114/22/5624.full)

[twitter emulenews/status/1558151071092166661](https://twitter.com/emulenews/status/1558151071092166661)  
[Fluid physics of telescoping cardboard boxes](https://journals.aps.org/prfluids/abstract/10.1103/PhysRevFluids.7.044101)  
The sliding motion of the lid is controlled by the flow in a thin film of air in the gap separating the lid and the base of the box. The process is primarily controlled by the shape of the gap ...  
![](https://pbs.twimg.com/media/FZ-pr0aXwAIGjHz?format=jpg)  

##  Proyectos de investigación de elaboración propia
Intento elaborar algunos y compartirlos aquí. Los pongo asociados a un curso concreto, aunque puede variar según los alumnos.  
El guión de informe científico a presentar se puede comentar inicialmente, incluir con variaciones en la información sobre el primer proyecto a realizar.  
Algunos son poco más que una "tarea" asociada a ciertos estándares de evaluación LOMCE que requieren laboratorio / investigación / búsqueda de información / uso TIC ... (no cito el estándar en el documento pero se puede asociar fácilmente en algunos) y en lugar de ponerles nombres de "tarea" o "actividad" los pongo en este apartado y en el nombre pongo "proyectos de investigación". El tema es que para algunos estándares se piden instrumentos de evaluación asociados, no son directamente tratables sin una "tarea" del alumno que sería esta.  

Los ficheros se veían inicialmente anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia recursos/proyectos-de-investigacion](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/proyectos-de-investigacion) 
