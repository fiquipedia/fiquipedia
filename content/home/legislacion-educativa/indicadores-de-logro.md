# Indicadores de logro

Es un concepto que aparece en normativa de 2015, asociada a LOMCE, en una normativa que se deroga en 2022 con LOMLOE sin que la nueva normativa los cite

[Orden ECD/65/2015, de 21 de enero, por la que se describen las relaciones entre las competencias, los contenidos y los criterios de evaluación de la educación primaria, la educación secundaria obligatoria y el bachillerato. Artículo 7. La evaluación de las competencias clave.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-738#a7)  

> 4. Los niveles de desempeño de las competencias se podrán medir a través de **indicadores de logro**, tales como rúbricas o escalas de evaluación. Estos indicadores de logro deben incluir rangos dirigidos a la evaluación de desempeños, que tengan en cuenta el principio de atención a la diversidad.  

Aunque no se citan en normativa estatal LOMLOE, sí se citan en la de Andalucía, y la definición la hace asociable a [criterios de evaluación](/home/legislacion-educativa/lomloe/curriculo-criterios-de-evaluacion)  

[INSTRUCCIÓN CONJUNTA 1 /2022, DE 23 DE JUNIO, DE LA DIRECCIÓN GENERAL DE ORDENACIÓN Y EVALUACIÓN EDUCATIVA Y DE LA DIRECCIÓN GENERAL DE FORMACIÓN PROFESIONAL, POR LA QUE SE ESTABLECEN ASPECTOS DE ORGANIZACIÓN Y FUNCIONAMIENTO PARA LOS CENTROS QUE IMPARTAN EDUCACIÓN SECUNDARIA OBLIGATORIA PARA EL CURSO 2022/2023.](https://anpeandalucia.es/openFile.php?link=notices/att/1/instruccion1-2022organizacioneso_t1657715309_1_3.pdf)  
> 3. En los cursos primero y tercero, los criterios de evaluación han de ser medibles, por lo que se han de
establecer mecanismos objetivos de observación de las acciones que describen, así como indicadores claros,
que permitan conocer el grado de desempeño de cada criterio. Para ello, se establecerán indicadores de logro
de los criterios, en soportes tipo rúbrica. Los grados o indicadores de desempeño de los criterios de
evaluación de los cursos impares de esta etapa se habrán de ajustar a las graduaciones de insuficiente (del 1 al
4), suficiente (del 5 al 6), bien (entre el 6 y el 7), notable (entre el 7 y el 8) y sobresaliente (entre el 9 y el 10).  

[INSTRUCCIÓN 13/2022, DE 23 DE JUNIO, DE LA DIRECCIÓN GENERAL DE ORDENACIÓN Y EVALUACIÓN EDUCATIVA, POR LA QUE SE ESTABLECEN ASPECTOS DE ORGANIZACIÓN Y FUNCIONAMIENTO PARA LOS CENTROS QUE IMPARTAN BACHILLERATO PARA EL CURSO 2022/2023.](https://www.juntadeandalucia.es/educacion/portals/delegate/content/d779a4a8-b864-4d39-bb08-c597e8b73b4c/Instrucci%C3%B3n%2013/2022,%20de%2023%20de%20junio,%20por%20la%20que%20se%20establecen%20aspectos%20de%20organizaci%C3%B3n%20y%20funcionamiento%20para%20los%20centros%20que%20impartan%20bachillerato%20para%20el%20curso%202022/2023)  
> 3. En el primer curso, los criterios de evaluación han de ser medibles, por lo que se han de establecer
mecanismos objetivos de observación de las acciones que describen, así como indicadores claros, que
permitan conocer el grado de desempeño de cada criterio. Para ello, se establecerán indicadores de logro de
los criterios, en soportes tipo rúbrica. Los grados o indicadores de desempeño de los criterios de evaluación se
habrán de ajustar a las graduaciones de insuficiente (del 1 al 4), suficiente ( 5), bien (6), notable (entre el 7 y el
8) y sobresaliente (entre el 9 y el 10).  


Comento citas asociadas a LOMLOE  

[twitter pablofcayqca/status/1682792660484980736](https://twitter.com/pablofcayqca/status/1682792660484980736)  
¡Muy buenas!  
Me lanzo con un nuevo formato en el que iremos conociendo los distintos aspectos relacionados con la LOMLOE, dando comienzo con los indicadores de logro (1ª parte) y los niveles de consecución (2ª parte): qué son, cómo se establecen y ejemplos prácticos  
...

Hola, Pablo. Estoy haciendo mi presentación de la Situación de Aprendizaje y me ha surgido una duda: ¿en qué ley aparecen los indicadores de logro? Los he usado pero ahora no encuentro cómo justificarlo, no los veo en la LOMLOE no en el RD, ni en la ley autonómica. ¡Gracias!  
...  
Hola! No tiene por qué aparecer, forma parte de la parte pedagógica, es una manera de conectar criterio, saber y situación de aprendizaje. Un saludo!  

Ideas asociadas  
[twitter followero/status/1683022361635815424](https://twitter.com/followero/status/1683022361635815424)  
Hombre, esto me recuerda a mí hace 3 semanas escribiendo los descriptores de logro en que fallaban 3 muchachos de 6° que no me habían entregado absolutamente nada en Plástica. No había ningún descriptor referido a no hacer ni el huevo.  

Hola, Andrés! Yo eso lo indico en los niveles de consecución (IN: no sabe nada y/o no hace ni el huevo). Con otras palabras, claro  

Sí, ya. Pero no queda suficientemente descrita la causa del no-logro. A eso me refería.  

O sea, lo que quiero decir es que al final del proceso falta información importante para quien llegue después (suspenden porque no entregan nada) si uno lo escribe haciendo uso estricto de los descriptores que dan.   

Entiendo lo que dices. Yo redacto los descriptores atendiendo al aprendizaje, y los niveles concretan el grado en el que este se consigue alcanzar, pero, claro, las causas se quedan en el aire. Supongo que entrarían en las observaciones de los boletines y demás.  

[twitter pablofcayqca/status/1683052303454986240](https://twitter.com/pablofcayqca/status/1683052303454986240)  
¡Hola de nuevo!  
Hoy vengo a hablar de los elementos que harán que nuestra evaluación LOMLOE sea realmente formativa y objetiva: los niveles de consecución (qué son, cómo se establecen y qué debemos tener en cuenta para hacerlo).  

