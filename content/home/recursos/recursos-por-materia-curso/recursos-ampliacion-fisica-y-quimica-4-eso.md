---
aliases:
  - /home/recursos/recursos-por-materia-curso/recursos-ampliacin-fsica-y-qumica-4-eso/
---

# Recursos Ampliación Física y Química 4º ESO

Se trata de una materia que se desarrolla básicamente en el laboratorio: ver [prácticas y experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio)  


[IES Victoria Kent](http://www.educa.madrid.org/web/ies.victoriakent.torrejondeardoz/Departamentos/DFyQ/Materiales/AFQ/AFQ.htm)   
Actividades, materiales, vídeos  


