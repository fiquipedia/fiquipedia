# Currículo Física y Química 4º ESO (LOMLOE)

En esta página se incluye el [currículo estatal (Real Decreto)](#Estatal) y el [currículo de Madrid (Decreto)](#Madrid)

# <a name="Estatal"></a> Estatal (Real Decreto)

Revisado con [Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975) citado en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  

Ver [comparación currículo Física y Química 4º ESO](/home/materias/eso/fisicayquimica-4eso/comparacion-curriculo-fisica-y-quimica-4-eso)

Cambios en trámite audiencia noviembre 2021 respecto a borrador octubre:  
En competencia específica 3.1 cambia ""variadas para interpretar y comunicar" por "variadas, fiables y seguras para seleccionar, interpretar, organizar y comunicar"  
En competencia específica 3.2 cambia "formulación y nomenclatura" por "nomenclatura"  
En competencia específica 5.1 cambia "constructivas" por "constructivas y coeducativas", y "para los demás" por "para la comunidad"  
En competencia específica 6.1 cambia "Reconocer" por "Reconocer y valorar", añade "logrados por mujeres y hombres" y cambia "en construcción" por "en permanente construcción"  
En competencia específica 6.2 cambia "todos los ciudadanos" por "toda la ciudadanía"  
En saber básico C se cambia "su uso responsable." por "su producción y su uso responsable."  

Cambios en BOE marzo 2022 respecto a trámite de audiencia noviembre 2021:  
Retoques mínimos en competencias específicas:  
Competencia específica 1 añade "con corrección y precisión." y se puede considerar una mención a tratamiento de errores.  
Competencia específica 4 añade "aprendizaje autónomo"  
Competencia específica 6 elimina "hombres y mujeres en ellas, aplicaciones directas" y cambia "sociales, económicas y medioambientales" por "importantes"  
Retoques mínimos en saberes básicos:  
En A se elimina "mediante el uso de la experimentación" y "pertinentes y generales", entornos y normas se separan y se citan explícitamente símbolos, se añade "justa, equitativa e igualitaria.", 
En B se añade "desarrollo histórico", "entre otros sistemas materiales significativos.", "clásicos y cuánticos", se deja de usar "constituyentes de los átomos" que cambia por "partículas subatómicas", se añade **"cálculo del número de moles"**, se concreta formulación que solo hablaba de compuestos añadiendo "sustancias simples, iones y compuestos químicos binarios y **ternarios**", y se añade asociado a orgánicos **"monofuncionales"**  
En C se añade "La luz y el sonido como ondas que transfieren energía."  
En D se añade "Ley de la gravitación universal: atracción entre los cuerpos que componen el universo. Concepto de peso.", se añade texto "Fuerzas y presión en los fluidos" y "estudiando los principios fundamentales que las describen.", lo que sugiere sin nombrarlos Pascal y Arquímedes.  
En E se cita explícitamente **"estequiometría"**, se elimina "comprobando experimentalmente algunos de sus parámetros", se dejan de citar "variables termodinámicas y cinéticas"  

**CUARTO CURSO**  

**Criterios de evaluación**

**Competencia específica 1.**

1.1 Comprender y explicar con rigor los fenómenos fisicoquímicos cotidianos a partir de los principios, teorías y leyes científicas adecuadas, expresándolos de manera argumentada, utilizando diversidad de soportes y medios de comunicación.

1.2 Resolver los problemas fisicoquímicos planteados mediante las leyes y teorías científicas adecuadas, razonando los procedimientos utilizados para encontrar las soluciones y expresando los resultados con corrección y precisión.

1.3 Reconocer y describir situaciones problemáticas reales de índole científica y emprender iniciativas colaborativas en las que la ciencia, y en particular la física y la química, pueden contribuir a su solución, analizando críticamente su impacto en la sociedad y en el medio ambiente.

**Competencia específica 2.**

2.1 Emplear las metodologías propias de la ciencia en la identificación y descripción de fenómenos científicos a partir de situaciones tanto observadas en el mundo natural como planteadas a través de enunciados con información textual, gráfica o numérica.

2.2 Predecir, para las cuestiones planteadas, respuestas que se puedan comprobar con las herramientas y conocimientos adquiridos, tanto de forma experimental como deductiva, aplicando el razonamiento lógico-matemático en su proceso de validación.

2.3 Aplicar las leyes y teorías científicas más importantes para validar hipótesis de manera informada y coherente con el conocimiento científico existente, diseñando los procedimientos experimentales o deductivos necesarios para resolverlas y analizando los resultados críticamente.

**Competencia específica 3.**

3.1 Emplear fuentes variadas fiables y seguras para seleccionar, interpretar, organizar y comunicar información relativa a un proceso fisicoquímico concreto, relacionando entre sí lo que cada una de ellas contiene, extrayendo en cada caso lo más relevante para la resolución de un problema y desechando todo lo que sea irrelevante.

3.2 Utilizar adecuadamente las reglas básicas de la física y la química, incluyendo el uso correcto de varios sistemas de unidades, las herramientas matemáticas necesarias y las reglas de nomenclatura avanzadas, consiguiendo una comunicación efectiva con toda la comunidad científica.

3.3 Aplicar con rigor las normas de uso de los espacios específicos de la ciencia, como el laboratorio de física y química, asegurando la salud propia y colectiva, la conservación sostenible del medio ambiente y el cuidado por las instalaciones.

**Competencia específica 4.**

4.1 Utilizar de forma eficiente recursos variados, tradicionales y digitales, mejorando el aprendizaje autónomo y la interacción con otros miembros de la comunidad educativa, de forma rigurosa y respetuosa y analizando críticamente las aportaciones de cada participante.

4.2 Trabajar de forma versátil con medios variados, tradicionales y digitales, en la consulta de información y la creación de contenidos, seleccionando y empleando con criterio las fuentes y herramientas más fiables, desechando las menos adecuadas y mejorando el aprendizaje propio y colectivo.

**Competencia específica 5.**

5.1 Establecer interacciones constructivas y coeducativas, emprendiendo actividades de cooperación e iniciando el uso de las estrategias propias del trabajo colaborativo, como forma de construir un medio de trabajo eficiente en la ciencia.

5.2 Emprender, de forma autónoma y de acuerdo a la metodología adecuada, proyectos científicos que involucren al alumnado en la mejora de la sociedad y que creen valor para el individuo y para la comunidad.

**Competencia específica 6.**

6.1 Reconocer y valorar, a través del análisis histórico de los avances científicos logrados por mujeres y hombres, así como de situaciones y contextos actuales (líneas de investigación, instituciones científicas, etc.), que la ciencia es un proceso en permanente construcción y que esta tiene repercusiones e implicaciones importantes sobre la sociedad actual.

6.2 Detectar las necesidades tecnológicas, ambientales, económicas y sociales más importantes que demanda la sociedad, entendiendo la capacidad de la ciencia para darles solución sostenible a través de la implicación de la ciudadanía.

**Saberes básicos**   

**A. Las destrezas científicas básicas.**

- Trabajo experimental y proyectos de investigación: estrategias en la resolución de problemas y el tratamiento del error mediante la indagación, la deducción, la búsqueda de evidencias y el razonamiento lógico-matemático, haciendo inferencias válidas de las observaciones y obteniendo conclusiones que vayan más allá de las condiciones experimentales para aplicarlas a nuevos escenarios.

- Diversos entornos y recursos de aprendizaje científico como el laboratorio o los entornos virtuales: materiales, sustancias y herramientas tecnológicas.

- Normas de uso de cada espacio, asegurando y protegiendo así la salud propia y comunitaria, la seguridad en las redes y el respeto hacia el medio ambiente.

- El lenguaje científico: manejo adecuado de distintos sistemas de unidades y sus símbolos. Herramientas matemáticas adecuadas en diferentes escenarios científicos y de aprendizaje.

- Estrategias de interpretación y producción de información científica en diferentes formatos y a partir de diferentes medios: desarrollo del criterio propio basado en lo que el pensamiento científico aporta a la mejora de la sociedad para hacerla más justa, equitativa e igualitaria.

- Valoración de la cultura científica y del papel de científicos y científicas en los principales hitos históricos y actuales de la física y la química para el avance y la mejora de la sociedad.

**B. La materia.**

- Sistemas materiales: resolución de problemas y situaciones de aprendizaje diversas sobre las disoluciones y los gases, entre otros sistemas materiales significativos.

- Modelos atómicos: desarrollo histórico de los principales modelos atómicos clásicos y cuánticos y descripción de las partículas subatómicas, estableciendo su relación con los avances de la física y la química.

- Estructura electrónica de los átomos: configuración electrónica de un átomo y su relación con la posición del mismo en la tabla periódica y con sus propiedades fisicoquímicas.

- Compuestos químicos: su formación, propiedades físicas y químicas y valoración de su utilidad e importancia en otros campos como la ingeniería o el deporte.

- Cuantificación de la cantidad de materia: cálculo del número de moles de sistemas materiales de diferente naturaleza, manejando con soltura las diferentes formas de medida y expresión de la misma en el entorno científico.

- Nomenclatura inorgánica: denominación de sustancias simples, iones y compuestos químicos binarios y ternarios mediante las normas de la IUPAC.

- Introducción a la nomenclatura orgánica: denominación de compuestos orgánicos monofuncionales a partir de las normas de la IUPAC como base para entender la gran variedad de compuestos del entorno basados en el carbono.

**C. La energía.**

- La energía: formulación y comprobación de hipótesis sobre las distintas formas y aplicaciones de la energía, a partir de sus propiedades y del principio de conservación, como base para la experimentación y la resolución de problemas relacionados con la energía mecánica en situaciones cotidianas.

- Transferencias de energía: el trabajo y el calor como formas de transferencia de energía entre sistemas relacionados con las fuerzas o la diferencia de temperatura. La luz y el sonido como ondas que transfieren energía.

- La energía en nuestro mundo: estimación de la energía consumida en la vida cotidiana mediante la búsqueda de información contrastada, la experimentación y el razonamiento científico, comprendiendo la importancia de la energía en la sociedad, su producción y su uso responsable.

**D. La interacción.**

- Predicción y comprobación, utilizando la experimentación y el razonamiento matemático, de las principales magnitudes, ecuaciones y gráficas que describen el movimiento de un cuerpo, relacionándolo con situaciones cotidianas y con la mejora de la calidad de vida.

- La fuerza como agente de cambios en los cuerpos: principio fundamental de la Física que se aplica a otros campos como el diseño, el deporte o la ingeniería.

- Carácter vectorial de las fuerzas: uso del álgebra vectorial básica para la realización gráfica y numérica de operaciones con fuerzas y su aplicación a la resolución de problemas relacionados con sistemas sometidos a conjuntos de fuerzas, valorando su importancia en situaciones cotidianas.

- Principales fuerzas del entorno cotidiano: reconocimiento del peso, la normal, el rozamiento, la tensión o el empuje, y su uso en la explicación de fenómenos físicos en distintos escenarios.

- Ley de la gravitación universal: atracción entre los cuerpos que componen el universo. Concepto de peso.

- Fuerzas y presión en los fluidos: efectos de las fuerzas y la presión sobre los líquidos y los gases, estudiando los principios fundamentales que las describen.

**E. El cambio.**

- Ecuaciones químicas: ajuste de reacciones químicas y realización de predicciones cualitativas y cuantitativas basadas en la estequiometría, relacionándolas con procesos fisicoquímicos de la industria, el medioambiente y la sociedad.

- Descripción cualitativa de reacciones químicas de interés: reacciones de combustión, neutralización y procesos electroquímicos sencillos, valorando las implicaciones que tienen en la tecnología, la sociedad o el medioambiente.

- Factores que influyen en la velocidad de las reacciones químicas: comprensión de cómo ocurre la reordenación de los átomos aplicando modelos como la teoría de colisiones y realización de predicciones en los procesos químicos cotidianos más importantes.

# <a name="Madrid"></a> Madrid (4º ESO)

Revisado con [Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-2.PDF) 

[Borrador Currículo ESO Madrid 19 abril 2022](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/legislacion-educativa/lomloe/2022-04-19-Madrid-ESO-Curr%C3%ADculo-Borrador.pdf)  
En borrador 19 abril 2022 hay algunas indicaciones en rojo  
Cuando realizo el análisis de 4ºESO ya está el documento de 23 mayo así que no hago el análisis con borrador abril que sí había realizado para 2ESO y 3ESO.  
Hay un cambio común: se reordena para que bloque de cambio esté antes que energía. En RD cambio es E y energía C. En Madrid cambio es C y energía E.  

[Proyecto de decreto por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria. 23 mayo](https://www.comunidad.madrid/transparencia/sites/default/files/2022-05-23_decreto_eso-completo.pdf)  
En competencia específica 1.3 se elimina "colaborativas" y ", analizando críticamente su impacto en la sociedad y en el medio ambiente"  
En competencia específica 3.2 se añade cambiando "nomenclatura" por "formulación y nomenclatura"  
En competencia específica 5.1 se elimiina "interacciones constructivas y coeducativas, emprendiendo" y "e iniciando el uso de las estrategias propias del trabajo colaborativo,"  
En competencia específica 6.1 se elimina "logrados por mujeres y hombres" y " y que esta tiene repercusiones e implicaciones importantes sobre la sociedad actual."  
En competencia específica 6.2 se elimina "sostenible"  

En contenidos en general marco en negrita lo añadido por Madrid (en rojo en su documento)   
En contenidos en bloque energía se elimina ", su producción y su uso responsable."  

Cambios [Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-2.PDF) vs proyecto  
En criterio 3.2 y en saber básico B se cambia varias veces "formulación y nomenclatura" por "nomenclatura" (fue una de mis alegaciones)  
En saber básico E, manteniendo la frase "La luz y el sonido como ondas que transfieren energía." se eliminan dos líneas añadidas en proyecto  
"• **Concepto de onda. Características y propiedades.**  
• **Luz y sonido. Aplicaciones.**"  

**En decreto BOCM hay errata ya que se indica "Borh" en lugar de "Bohr"**  

## 4º ESO
### Criterios de evaluación.
#### Competencia específica 1.

1.1. Comprender y explicar con rigor los fenómenos fisicoquímicos cotidianos a partir de los principios, teorías y leyes científicas adecuadas, expresándolos de manera argumentada, utilizando diversidad de soportes y medios de comunicación.

1.2. Resolver los problemas fisicoquímicos planteados mediante las leyes y teorías científicas adecuadas, razonando los procedimientos utilizados para encontrar las soluciones y expresando los resultados con corrección y precisión.

1.3. Reconocer y describir situaciones problemáticas reales de índole científica y emprender iniciativas en las que la ciencia, y en particular la física y la química, pueden contribuir a su solución.

### Competencia específica 2.

2.1. Emplear las metodologías propias de la ciencia en la identificación y descripción de fenómenos científicos a partir de situaciones tanto observadas en el mundo natural como planteadas a través de enunciados con información textual, gráfica o numérica.

2.2. Predecir, para las cuestiones planteadas, respuestas que se puedan comprobar con las herramientas y conocimientos adquiridos, tanto de forma experimental como deductiva, aplicando el razonamiento lógico-matemático en su proceso de validación.

2.3. Aplicar las leyes y teorías científicas más importantes para validar hipótesis de manera informada y coherente con el conocimiento científico existente, diseñando los procedimientos experimentales o deductivos necesarios para resolverlas y analizando los resultados críticamente.

### Competencia específica 3.

3.1. Emplear fuentes variadas fiables y seguras para seleccionar interpretar, organizar y comunicar información relativa a un proceso fisicoquímico concreto, relacionando entre sí lo que cada una de ellas contiene, extrayendo en cada caso lo más relevante para la resolución de un problema y desechando todo lo que sea irrelevante.

3.2. Utilizar adecuadamente las reglas básicas de la física y la química, incluyendo el uso correcto de varios sistemas de unidades, las herramientas matemáticas necesarias y las reglas de nomenclatura avanzadas, consiguiendo una comunicación efectiva con toda la comunidad científica.

3.3. Aplicar con rigor las normas de uso de los espacios específicos de la ciencia, como el laboratorio de física y química, asegurando la salud propia y colectiva, la conservación sostenible del medio ambiente y el cuidado por las instalaciones.

### Competencia específica 4.

4.1. Utilizar de forma eficiente recursos variados, tradicionales y digitales, mejorando el aprendizaje autónomo y la interacción con otros miembros de la comunidad educativa, de forma rigurosa y respetuosa y analizando críticamente las aportaciones de cada participante.

4.2. Trabajar de forma versátil con medios variados, tradicionales y digitales, en la consulta de información y la creación de contenidos, seleccionando y empleando con criterio las fuentes y herramientas más fiables, desechando las menos adecuadas y mejorando el aprendizaje propio
y colectivo.

### Competencia específica 5.

5.1. Establecer actividades de cooperación como forma de construir un medio de trabajo eficiente en la ciencia.

5.2. Emprender, de forma autónoma y de acuerdo a la metodología adecuada, proyectos científicos que involucren al alumnado en la mejora de la sociedad y que creen valor para el individuo y para la comunidad.

### Competencia específica 6.

6.1. Reconocer y valorar, a través del análisis histórico de los avances científicos, así como de situaciones y contextos actuales (líneas de investigación, instituciones científicas, etc.), que la ciencia es un proceso en permanente construcción.

6.2. Detectar las necesidades tecnológicas, ambientales, económicas y sociales más importantes que demanda la sociedad, entendiendo la capacidad de la ciencia para darles solución a través de la implicación de la ciudadanía.

### Contenidos.

**A. Las destrezas científicas básicas.**

- **Diseño del** trabajo experimental y **emprendimiento de** proyectos de investigación: estrategias en la resolución de problemas **mediante el uso de la experimentación** y el tratamiento del error mediante la indagación, la deducción, la búsqueda de evidencias y el razonamiento lógico-matemático, haciendo inferencias válidas de las observaciones y obteniendo conclusiones que vayan más allá de las condiciones experimentales para aplicarlas a nuevos escenarios.  
• **La investigación científica.**  
• **La medida y su error.**  
• **Análisis de datos experimentales.**  
- **Empleo de** diversos entornos y recursos de aprendizaje científico, como el laboratorio o los entornos virtuales, **utilizando de forma correcta los** materiales, sustancias y herramientas tecnológicas **y atendiendo a las** normas de uso de cada espacio asegurando y protegiendo así la salud propia y comunitaria, la seguridad en redes y el respeto hacia el medio ambiente.  
• **Proyecto de investigación sencillo.**  
• **Utilización adecuada del material de laboratorio e instrumentos de medida.**  
• **Aplicación responsable de las normas de seguridad en el laboratorio.**  
- **Uso del** lenguaje científico: manejo adecuado de distintos sistemas de unidades y sus símbolos. **Utilización de** herramientas matemáticas adecuadas en diferentes escenarios científicos y de aprendizaje.  
• **Las magnitudes.**  
• **Ecuaciones dimensionales.**  
• **El informe científico.**  
• **Expresión de resultados de forma rigurosa en diferentes formatos.**  
- Estrategias de interpretación y producción de información científica en diferentes formatos y a partir de diferentes medios: desarrollo del criterio propio basado en lo que el pensamiento científico aporta a la mejora de la sociedad para hacerla más justa, equitativa e igualitaria.  
• **Utilización de herramientas tecnológicas en el entorno científico.**  
• **Selecciona, comprende e interpreta la información relevante de un texto de divulgación científica.**  
- Valoración de la cultura científica y del papel de científicos y científicas en los principales hitos históricos y actuales de la física y la química para el avance y la mejora de la sociedad.  

**B. La materia.**

- Sistemas materiales: resolución de problemas y situaciones de aprendizaje diversas sobre las disoluciones y los gases, entre otros sistemas materiales significativos.  
• **Los gases. Ley general de los gases.**  
• **Disoluciones.**  
- Modelos atómicos: desarrollo histórico de los principales modelos atómicos clásicos y cuánticos y descripción de las partículas subatómicas, estableciendo su relación con los avances de la física y la química.  
• **Las partículas elementales.**  
• **Evolución de los modelos atómicos hasta el modelo de Bohr-Sommerfeld.**  
- Estructura electrónica de los átomos: configuración electrónica de un átomo y su relación con la posición del mismo en la tabla periódica y con sus propiedades fisicoquímicas.  
• **Configuración electrónica de los elementos y posición en la tabla periódica.**  
- Compuestos químicos: su formación, propiedades físicas y químicas y valoración de su utilidad e importancia en otros campos como la ingeniería o el deporte.  
• **El enlace químico: iónico, covalente y metálico.**  
• **Compuestos químicos de especial interés.**  
- Cuantificación de la cantidad de materia: cálculo del número de moles de sistemas materiales de diferente naturaleza, manejando con soltura las diferentes formas de medida y expresión de la misma en el entorno científico.  
• **Masa atómica y molecular.**  
• **Concepto de mol. Constante de Avogadro.**  
• **Concentración molar de una disolución.** 
- Nomenclatura inorgánica: denominación de sustancias simples, iones y compuestos químicos binarios y ternarios mediante las normas de la IUPAC.  
- Introducción a la nomenclatura **de los compuestos** orgánicos: denominación de compuestos orgánicos monofuncionales a partir de las normas de la IUPAC como base para entender la gran variedad de compuestos del entorno basados en el carbono.  
• **Grupos funcionales principales.**  
• **Nomenclatura de alcanos, alquenos y alquinos.**  
• **Compuestos orgánicos de interés industrial y biológico.**  

**C. El cambio.**

- Ecuaciones químicas: ajuste de reacciones químicas y realización de predicciones cualitativas y cuantitativas basadas en la estequiometría, relacionándolas con procesos fisicoquímicos de la industria, el medioambiente y la sociedad.  
• **Ajuste de ecuaciones químicas.**  
• **Cálculos estequiométricos. Rendimiento de una reacción.**  
• **Reacciones químicas de especial interés.**  
- Descripción cualitativa de reacciones químicas de interés: reacciones de combustión, neutralización y procesos electroquímicos sencillos, valorando las implicaciones que tienen en la tecnología, la sociedad o el medioambiente.  
• **Tipos de reacciones químicas.**  
- Factores que influyen en la velocidad de las reacciones químicas: comprensión de cómo ocurre la reordenación de los átomos aplicando modelos como la teoría de colisiones y realización de predicciones en los procesos químicos cotidianos más importantes.  
• **Aproximación al concepto de velocidad de reacción química.**  
• **Introducción a la energía en las reacciones químicas.**  
• **Mecanismo de las reacciones químicas.**  
• **Factores que influyen en la velocidad de una reacción química.**  

**D. La interacción.**

- Predicción y comprobación, utilizando la experimentación y el razonamiento matemático, de las principales magnitudes, ecuaciones y gráficas que describen el movimiento de un cuerpo, relacionándolo con situaciones cotidianas y con la mejora de la calidad de vida.  
• **Movimiento rectilíneo y uniforme.**  
• **Movimiento rectilíneo uniformemente acelerado.**  
• **Movimiento circular uniforme.**  
- La fuerza como agente de cambios en los cuerpos: principio fundamental de la Física que se aplica a otros campos como el diseño, el deporte o la ingeniería.  
- Carácter vectorial de las fuerzas: uso del álgebra vectorial básica para la realización gráfica y numérica de operaciones con fuerzas y su aplicación a la resolución de problemas relacionados con sistemas sometidos a conjuntos de fuerzas, valorando su importancia en situaciones cotidianas.  
• **Naturaleza vectorial de las fuerzas.**  
- Principales fuerzas del entorno cotidiano: reconocimiento del peso, la normal, el rozamiento, la tensión o el empuje, y su uso en la explicación de fenómenos físicos en distintos escenarios.  
• **Fuerzas que actúan sobre los cuerpos.**  
• **Cálculo de la resultante de las fuerzas que actúan sobre un cuerpo en diferentes situaciones.**  
- Ley de la gravitación universal: atracción entre los cuerpos que componen el universo. Concepto de peso.  
- Fuerzas y presión en los fluidos: efectos de las fuerzas y la presión sobre los líquidos y los gases, estudiando los principios fundamentales que las describen.  
• **Concepto de Presión. Presión hidrostática. Presión atmosférica.**  
• **Principio de Arquímedes y Principio de Pascal.**  
• **Física de la atmósfera.**  

**E. La energía.**

- La energía: formulación y comprobación de hipótesis sobre las distintas formas y aplicaciones de la energía, a partir de sus propiedades y del principio de conservación, como base para la experimentación y la resolución de problemas relacionados con la energía mecánica en
situaciones cotidianas.  
• **Energía cinética y energía potencial.**  
• **Energía mecánica. Conservación de la energía mecánica.**  
- Transferencias de energía: el trabajo y el calor como formas de transferencia de energía entre sistemas relacionados con las fuerzas o la diferencia de temperatura.  
• **El trabajo y la energía mecánica. Potencia.**  
• **Efecto del calor sobre los cuerpos.**  
• **Transformación entre calor y trabajo.**  
- **Reconocimiento del transporte de energía mediante ondas mecánicas y electromagnéticas.** La luz y el sonido como ondas que transfieren energía.  
• **Utilización de la energía del Sol como fuente de energía limpia y renovable.**  
- La energía en nuestro mundo: estimación de la energía consumida en la vida cotidiana mediante la búsqueda de información contrastada, la experimentación y el razonamiento científico, comprendiendo la importancia de la energía en la sociedad.


