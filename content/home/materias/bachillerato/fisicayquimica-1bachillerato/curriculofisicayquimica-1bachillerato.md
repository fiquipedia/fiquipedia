
# Currículo Física y Química 1º Bachillerato (LOE)

En la Comunidad de Madrid se desarrolla en  [DECRETO 67/2008, de 19 de junio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid el currículo del Bachillerato, B.O.C.M. Núm. 152 de VIERNES 27 DE JUNIO DE 2008](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=5081&cdestado=P) , páginas  [62 a 64 en versión pdf ](http://www.bocm.es/bocm/Satellite?c=Page&cid=1188556258222&detalle=1&elemento=Boletin%2FCM_Seccion_BOCM%2FBOCM_pintarDetalleSeccion&idBoletin=1142467211638&idSeccionN1=1142467211646&idSeccionN2=1142467211650&idSeccionN3=1142467211655&idSumario=1142467211641&language=es&pagename=Boletin%2FCM_Seccion_BOCM%2FBOCM_detalleSeccion&subtype=Boletin)   
  
Este currículo deja de aplicar en 2015/2016 según el calendario de implantación de la LOMCE, mirar  [currículo Física y Química 1º Bachillerato (LOMCE)](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomce)   
  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver  [Contenidos currículo con enlaces a recursos: Física y Química 1º Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/contenidoscurriculoenlacesrecursosfisicaquimica1bachillerato)   

Ver [comparación currículo Física y Química 1º Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/comparacion-curriculo-fisica-y-quimica-1-bachillerato)

## Introducción
La materia de Física y Química ha de continuar facilitando la impregnación en la cultura científica, iniciada en la etapa anterior, para lograr una mayor familiarización con la naturaleza de la actividad científica y tecnológica y la apropiación de las competencias que dicha actividad conlleva. Al mismo tiempo, esta materia, de la modalidad de Ciencias y Tecnología, ha de seguir contribuyendo a aumentar el interés de los estudiantes hacia las ciencias físico-químicas, poniendo énfasis en una visión de las mismas que permita comprender su dimensión social y, en particular, el papel jugado en las condiciones de vida y en las concepciones de los seres humanos.  
Por otra parte, la materia ha de contribuir a la formación del alumnado para su participación como ciudadanos, y, en su caso, como miembros de la comunidad científica, en la necesaria toma de decisiones en torno a los graves problemas con los que se enfrenta hoy la humanidad. Es por ello por lo que el desarrollo de la materia debe prestar atención igualmente a las relaciones entre ciencia, tecnología, sociedad y ambiente (CTSA), y contribuir, en particular, a que los alumnos conozcan aquellos problemas, sus causas y medidas necesarias, en los ámbitos tecnocientífico, educativo y político, para hacerles frente y avanzar hacia un futuro sostenible.  
Los contenidos de la materia se organizan en bloques relacionados entre sí. Se parte de un bloque de contenidos comunes destinados a familiarizar a los alumnos con las estrategias básicas de la actividad científica que, por su carácter transversal, deberán ser tenidos en cuenta al desarrollar el resto. En la primera parte, dedicada a la Física, los contenidos se estructuran en torno a la mecánica y la electricidad. La mecánica se inicia con una profundización en el estudio del movimiento y las causas que lo modifican con objeto de mostrar el surgimiento de la ciencia moderna y su ruptura con dogmatismos y visiones simplistas de sentido común. Se trata de una profundización del estudio realizado en el último curso de la Educación Secundaria Obligatoria, con una aproximación más detenida que incorpore los conceptos de trabajo y energía para el estudio de  
los cambios. Ello ha de permitir una mejor comprensión de los principios de la dinámica y de conservación y transformación de la energía y de las repercusiones teóricas y prácticas del cuerpo de conocimientos construido.  
El estudio de la electricidad que se realiza a continuación ha de contribuir a un mayor conocimiento de la estructura de la materia y a la profundización del papel de la energía eléctrica en las sociedades actuales, estudiando su generación, consumo y las repercusiones de su utilización.  
En la segunda parte, dedicada a la Química, los contenidos se estructuran alrededor de dos grandes ejes. El primero profundiza en la teoría atómico-molecular de la materia partiendo de conocimientos abordados en la etapa anterior, así como la estructura del átomo, que permitirá explicar la semejanza entre las distintas familias de elementos, los enlaces y las transformaciones químicas. El segundo eje profundiza en el estudio de la química del carbono, iniciado en el curso anterior, y ha de permitir que el alumnado comprenda la importancia de las primeras síntesis de sustancias orgánicas, lo que supuso la superación del vitalismo, que negaba la posibilidad de dicha síntesis, contribuyendo a la construcción de una imagen unitaria de la materia e impulsando la síntesis de nuevos materiales de gran importancia por sus aplicaciones. Este estudio de las sustancias orgánicas dedicará una atención particular a la problemática del uso de los combustibles fósiles y la necesidad de soluciones para avanzar hacia un futuro sostenible.  

## Objetivos
La enseñanza de la Física y Química en el Bachillerato tendrá como finalidad contribuir al desarrollo de las siguientes capacidades:  
1. Conocer y comprender los conceptos, leyes, teorías y modelos más importantes y generales de la Física y la Química, así como las estrategias empleadas en su construcción, con el fin de tener una visión global del desarrollo de estas ramas de la ciencia y de su papel social, de obtener una formación científica básica y de generar  
interés para poder desarrollar estudios posteriores más específicos.  
2. Comprender y aplicar los conceptos, leyes, teorías y modelos aprendidos a situaciones de la vida cotidianas para poder participar, como ciudadanos y, en su caso, futuros científicos, en la necesaria toma de decisiones fundamentadas en torno a problemas locales y globales a los que se enfrenta la humanidad y contribuir a construir un futuro sostenible, participando en la conservación, protección y mejora del medio natural y social.  
3. Utilizar, con autonomía creciente, estrategias de investigación propias de las ciencias (planteamiento de problemas, formulación de hipótesis fundamentadas; búsqueda de información; elaboración de estrategias de resolución y de diseños experimentales; realización de experimentos en condiciones controladas y reproducibles, análisis de resultados, etcétera), relacionando los conocimientos aprendidos con otros ya conocidos y considerando su contribución a la construcción de cuerpos coherentes de conocimientos y a su progresiva interconexión.  
4. Resolver supuestos físicos y químicos, tanto teóricos como prácticos, mediante el empleo de los conocimientos adquiridos.  
5. Familiarizarse con la terminología científica para poder emplearla de manera habitual al expresarse en el ámbito científico, así como para poder explicar expresiones científicas del lenguaje cotidiano y relacionar la experiencia diaria con la científica.  
6. Utilizar de manera habitual las tecnologías de la información y la comunicación para realizar simulaciones, tratar datos y extraer y utilizar información de diferentes fuentes, evaluar su contenido y adoptar decisiones.  
7. Familiarizarse con el diseño y realización de experimentos físicos y químicos, utilizando la tecnología adecuada para un funcionamiento correcto, con una atención particular a las normas de seguridad de las instalaciones.  
8. Reconocer el carácter tentativo y creativo del trabajo científico, como actividad en permanente proceso de construcción, analizando y comparando hipótesis y teorías contrapuestas a fin de desarrollar un pensamiento crítico, así como valorar las aportaciones de los grandes debates científicos al desarrollo del pensamiento humano.  
9. Apreciar la dimensión cultural de la Física y la Química para la formación integral de las personas, así como saber valorar sus repercusiones en la sociedad y en el medio ambiente, contribuyendo a la toma de decisiones que propicien el impulso de desarrollos científicos, sujetos a los límites de la biosfera, que respondan a necesidades humanas y contribuyan a hacer frente a los graves problemas que hipotecan su futuro.  

## Contenidos

### 1. Contenidos comunes.
— Utilización de estrategias básicas de la actividad científica tales como el planteamiento de problemas y la toma de decisiones acerca del interés y la conveniencia o no de su estudio; formulación de hipótesis, elaboración de estrategias de resolución y de diseños experimentales y análisis de los resultados y de su fiabilidad.  
— Búsqueda, selección y comunicación de información y de resultados utilizando la terminología adecuada.  
— Magnitudes: Tipos y su medida. Unidades. Factores de conversión. Representaciones gráficas. Instrumentos de medida: Sensibilidad y precisión. Errores en la medida.  

### 2. Estudio del movimiento.
— Importancia del estudio de la cinemática en la vida cotidiana y en el surgimiento de la ciencia moderna.  
— Elementos que integran un movimiento. Sistemas de referencia inerciales. Magnitudes necesarias para la descripción del movimiento. Iniciación al carácter vectorial de las magnitudes que intervienen.  
— Estudio de los movimientos con trayectoria rectilínea y del movimiento circular uniforme.  
— Las aportaciones de Galileo al desarrollo de la cinemática y de la ciencia en general. Superposición de movimientos. Aplicación a casos particulares: Tiro horizontal y tiro oblicuo.  
— Importancia de la educación vial. Estudio de situaciones cinemáticas de interés, como el espacio de frenado, la influencia de la velocidad en un choque, etcétera.  

### 3. Dinámica.
— De la idea de fuerza de la Física aristotélico-escolástica al concepto de fuerza como interacción.  
— Revisión y profundización de las Leyes de la dinámica de Newton. Momento lineal e impulso mecánico. Variación y conservación del momento lineal.  
— Dinámica del movimiento circular uniforme. Interacción gravitatoria: Ley de gravitación universal. Importancia de esta ley.  
— Estudio de algunas situaciones dinámicas de interés: Peso, fuerzas de fricción en superficies horizontales e inclinadas, fuerzas elásticas y tensiones.  

### 4. La energía y su transferencia. Trabajo y calor.
— Revisión y profundización de los conceptos de energía, trabajo y calor y sus relaciones. Eficacia en la realización de trabajo: Potencia. Formas de energía. Energía debida al movimiento. Teorema de las fuerzas vivas. Energía debida a la posición en el campo gravitatorio. Energía potencial elástica.  
— Principio de conservación y transformación de la energía. Sistemas y variables termodinámicas. Transferencias de energía. Calor y trabajo termodinámico. Principios cero y primero de la termodinámica. Degradación de la energía.  

### 5. Electricidad.
— Revisión de la fenomenología de la electrización y la naturaleza eléctrica de la materia ordinaria. Interacción electrostática.  
— Introducción al estudio del campo eléctrico. Concepto de potencial. Diferencia de potencial entre dos puntos de un campo eléctrico.  
— La corriente eléctrica. Ley de Ohm; aparatos de medida y asociación de resistencias. Aplicación al estudio de circuito. Efectos energéticos de la corriente eléctrica. Generadores de corriente.  
— La energía eléctrica en las sociedades actuales: Profundización en el estudio de su generación, consumo y repercusiones de su utilización.  

### 6. Teoría atómico-molecular de la materia.
— Revisión y profundización de la teoría atómica de Dalton. Interpretación de las leyes básicas asociadas a su establecimiento: Leyes ponderales. Ley de los volúmenes de combinación. Ley de Avogadro. Constante de Avogadro. Leyes de los gases.  
— Masas atómicas y moleculares. La cantidad de sustancia y su unidad, el mol.  
— Ecuación de estado de los gases ideales.  
— Determinación de fórmulas empíricas y moleculares.  
— Preparación de disoluciones de concentración determinada. Uso de la concentración en cantidad de sustancia.  

### 7. El átomo y sus enlaces.
— Primeros modelos atómicos: Thomson y Rutheford. Interacción de la radiación electromagnética con la materia: Los espectros atómicos. El modelo atómico de Bohr. Distribución electrónica en niveles energéticos. Introducción cualitativa al modelo cuántico.  
— Abundancia e importancia de los elementos en la naturaleza. El sistema periódico. Ordenación periódica de los elementos: Su relación con los electrones externos.  
— Estabilidad energética y enlace químico. Enlaces covalente, iónico, metálico e intermoleculares. Propiedades de las sustancias en relación con el tipo de enlace.  
— Formulación y nomenclatura de los compuestos inorgánicos, siguiendo las normas de la IUPAC.  

### 8. Estudio de las transformaciones químicas.
— Importancia del estudio de las transformaciones químicas y sus implicaciones.  
— Interpretación microscópica de las reacciones químicas. Velocidad de reacción. Factores de los que depende: Hipótesis y puesta a prueba experimental.  
— Relaciones estequiométricas de masa y/o volumen en las reacciones químicas utilizando factores de conversión. Reactivo limitante y rendimiento de una reacción. Cálculos en sistemas en los que intervienen disoluciones.  
— Tipos de reacciones químicas. Estudio de un caso habitual: Reacciones de combustión.  
— Química e industria: Materias primas y productos de consumo. Implicaciones de la química industrial.  
— Valoración de algunas reacciones químicas que, por su importancia biológica, industrial o repercusión ambiental, tienen mayor interés en nuestra sociedad. El papel de la química en la construcción de un futuro sostenible.  

### 9. Introducción a la química del carbono.
— Orígenes de la química orgánica: Superación de la barrera del vitalismo. Importancia y repercusiones de las síntesis orgánicas.  
— Posibilidades de combinación del átomo de carbono. Grupos funcionales. Introducción a la formulación de los compuestos de carbono. Isomería.  
— Los hidrocarburos, aplicaciones, propiedades y reacciones químicas. Fuentes naturales de hidrocarburos. El petróleo y sus aplicaciones. Repercusiones socioeconómicas, éticas y medioambientales asociadas al uso de combustibles fósiles.  
— El desarrollo de los compuestos orgánicos de síntesis: De la revolución de los nuevos materiales a los contaminantes orgánicos permanentes. Ventajas e impacto sobre la sostenibilidad.  

## Criterios de evaluación
1. Analizar situaciones y obtener información sobre fenómenos físicos y químicos utilizando estrategias básicas del trabajo científico.  
2. Aplicar estrategias características de la metodología científica al estudio de los movimientos estudiados: Uniforme, rectilíneo y circular, y rectilíneo uniformemente acelerado. Utilizar el tratamiento vectorial y analizar los resultados obtenidos, interpretando los posibles diagramas. Resolver ejercicios y problemas sobre movimientos específicos, tales como lanzamiento de proyectiles, encuentros de móviles, caída de graves, etcétera, empleando adecuadamente las unidades y magnitudes apropiadas.  
3. Identificar y representar mediante diagramas las fuerzas que actúan sobre los cuerpos, reconociendo y calculando dichas fuerzas cuando hay rozamiento, cuando la trayectoria es circular, e incluso cuando existan planos inclinados. Describir los principios de la dinámica en función del momento lineal. Aplicar el principio de conservación del momento lineal para explicar situaciones dinámicas cotidianas.  
4. Aplicar la Ley de gravitación universal para la atracción de masas, especialmente en el caso particular del peso de los cuerpos.  
5. Aplicar los conceptos de trabajo y energía, y sus relaciones, en el estudio de las transformaciones. Aplicar el principio de conservación y transformación de la energía al caso práctico de cuerpos en movimiento y/o bajo la acción del campo gravitatorio terrestre en la resolución de problemas de interés teórico y práctico.  
6. Interpretar la interacción eléctrica y los fenómenos asociados, así como sus repercusiones. Conocer los elementos de un circuito y los aparatos de medida más corrientes. Aplicar las estrategias de la actividad científica y tecnológica para el estudio, tanto teórico como experimental de los diferentes tipos de circuitos que se puedan plantear.  
7. Interpretar las leyes ponderales, las relaciones volumétricas de Gay-Lussac y la ecuación de estado de los gases ideales. Aplicar el concepto de cantidad de sustancia y su medida tanto si la sustancia se encuentra sólida, gaseosa o en disolución. Determinar fórmulas empíricas y moleculares.  
8. Justificar la existencia y evolución de los modelos atómicos, valorando el carácter tentativo y abierto del trabajo científico. Describir las ondas electromagnéticas y su interacción con la materia, deduciendo de ello una serie de consecuencias. Describir la estructura de los átomos y los isótopos. Conocer el tipo de enlace que mantiene unidas las partículas constituyentes de las sustancias de forma que se puedan explicar sus propiedades. Escribir y nombrar correctamente sustancias químicas inorgánicas.  
9. Reconocer la importancia del estudio de las transformaciones químicas y sus repercusiones. Interpretar microscópicamente una reacción química, emitir hipótesis sobre los factores de los que depende la velocidad de una reacción, sometiéndolas a prueba. Realizar cálculos estequiométricos en ejemplos de interés práctico, utilizando la información que se obtiene de las ecuaciones químicas.  
10. Identificar las propiedades físicas y químicas de los hidrocarburos así como su importancia social y económica y saber formularlos y nombrarlos aplicando las reglas de la IUPAC. Valorar la importancia del desarrollo de las síntesis orgánicas y sus repercusiones. Describir los principales tipos de compuestos del carbono así como las situaciones de isomería que pudieran presentarse.  
11. Realizar correctamente en el laboratorio las experiencias propuestas a lo largo del curso.  
12. Describir las interrelaciones existentes en la actualidad entre sociedad, ciencia, tecnología y ambiente dentro de los conocimientos abarcados este curso.  

