
# Cinética química

Recursos asocidos a cinética química, relacionados con bloque [equilibrio químico](/home/recursos/quimica/equilibrio-quimico)

[Cinética química - wikipedia.org](https://es.wikipedia.org/wiki/Cin%C3%A9tica_qu%C3%ADmica)  

[Cinética química. Laboratorio virtual. Salvador Hurtado Fernández - labovirtual.blogspot.com](https://labovirtual.blogspot.com/2010/12/cinetica-quimica.html)  
Indica fecha 2010.  
OBJETIVOS  
1 - Determinar experimentalmente la cinética de la reacción A + B → C  
2 - Determinar el orden parcial respecto de los reactivo A y B  
3 - Estudiar la variación de la velocidad de reacción con la temperatura  
4 - Determinar el valor de la energía de activación y del factor de frecuencia  

[Laboratorio virtual de cinética química](http://eduteka.icesi.edu.co/post/1287)  
Hace referencia a fichero [cinequim.swf](https://web.archive.org/web/20180728194402/http://salvadorhurtado.wikispaces.com/file/view/cinequim.swf) flash de Salvador Hurtado que indica fecha 2010 y no disponible en esa url. Guardo una copia local, aunque parece que está operativa en [este enlace]((https://labovirtual.blogspot.com/2010/12/cinetica-quimica.html)) sin necesitar flash, aunque sigue indicando 2010.   

[Cinética química. Laboratorio virtual. Salvador Hurtado Fernández - po4h36.wixsite.com](https://po4h36.wixsite.com/laboratoriovirtual/blank-10)  


[Cinética Química: Determinación del Orden de Reacción y Energía de Activación. Universidad de Oviedo](http://navarrof.orgfree.com/Docencia/FQaplicada/UT2/Material1.pdf)  
[Cinética Química - heurema.com](http://www.heurema.com/TestQ/TestQ28-CinQ1/TQ28-CINQ1.pdf)  
[Unidad IV. Cinética Química - uba.ar](http://materias.fi.uba.ar/6730/Tomo1Unidad4.pdf)  

[Ciencia e Ingeniería de Materiales › Química de los Materiales › Material de clase › Tema 5. Cinética química, termodinámica y equilibrio (I) - ocw.uc3m.es](http://ocw.uc3m.es/ciencia-e-oin/quimica-de-los-materiales/Material-de-clase/tema-5.-cinetica-quimica-termodinamica-y-equilibrio-i)  

[Prácticas de Química para Educación Secundaria](http://dpto.educacion.navarra.es/publicaciones/pdf/qui_dg.pdf)    
Página 48 VELOCIDAD DE REACCIÓN  

Enlaza con la idea de química física y física química  
[First reported case of hydrosilane activation mediated by hydrogen quantum tunnelling](https://mappingignorance.org/2022/09/29/first-reported-case-of-hydrosilane-activation-mediated-by-hydrogen-quantum-tunnelling/)    

[Cinética de reacción – oxidación del ion yoduro por el ion persulfato - quimicafacil.net](https://quimicafacil.net/manual-de-laboratorio/cinetica-de-reaccion-ion-yoduro-ion-persulfato/)  

