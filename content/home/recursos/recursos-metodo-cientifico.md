
# Recursos método científico

Algunas categorías de recursos relacionados:  [notación científica](/home/recursos/recursos-notacion-cientifica) ,  [prácticas - experimentos - laboratorio](/home/recursos/practicas-experimentos-laboratorio) ,  [recursos medida](/home/recursos/recursos-medida) ...  

También enlaza con el propio concepto de ciencia, por lo que puede haber recursos relacionados asociados a [Cultura Científica](/home/recursos/recursos-por-materia-curso/recursos-cultura-cientifica-1-bachillerato)   

En currículo se cita a veces dentro de un bloque de "actividad científica"  

[twitter IrenaBuzarewicz/status/1545288782898991106](https://twitter.com/IrenaBuzarewicz/status/1545288782898991106)  
Science saves lifes 
![](https://pbs.twimg.com/media/FXH38f1WYAEekho?format=jpg)  

[twitter CarlosJGarJ/status/1271388712350466049](https://twitter.com/CarlosJGarJ/status/1271388712350466049)  
Pues inspirado por el trabajo de @currofisico, voy a intentar resumir mis 15 UD de #1Bach en una simple hoja (espero no tener que tirar de dos páginas) de cara a la programación didáctica. Este es mi primer intento:  
![](https://pbs.twimg.com/media/EaTe5WTWAAA-81w?format=png)  

Puede enlazar con  [proyectos de investigación](/home/recursos/proyectos-de-investigacion)  

También enlaza con  [matemáticas](/home/recursos/matematicas)  para realizar gráficas, hallar ecuaciones de funciones ...  

En 2º y 3º ESO se encuadra dentro primer bloque de contenidos  
En 4º ESO también en primer bloque contenidos, aunque ya más centrado en la parte experimental sin la visión global del método científico.  

[Metodología científica 4º ESO. Categoría en aprendiendociencias](http://aprendiendociencias.wordpress.com/category/metodologia-cientifica-4-eso/) Aída Aivars, licenciamiento cc-by-nc-sa. Incluye lecturas  

[![](https://img.youtube.com/vi/4gpECWx8sns/0.jpg)](https://www.youtube.com/watch?v=4gpECWx8sns "Cosmos - Carl Sagan (Eratostenes y la circunferencia de la tierra)") 

Ejemplos de experimentos sencillos con los que ilustrar la idea: 
- Aparición de moho según se laven o no las manos  
[El experimento viral de una profesora que ha conseguido que todos los alumnos se laven las manos](http://www.lasexta.com/noticias/ciencia-tecnologia/experimento-viral-profesora-que-conseguido-que-todos-alumnos-laven-manos_2017091459bacd110cf2b1432c7f2086.html)  
![](https://fotografias.lasexta.com/clipping/cmsimages02/2017/09/14/73F0624D-7229-4AFD-A7CD-730347DBCD15/97.jpg?crop=960,540,x135,y0&width=1600&height=900&optimize=low&format=webply)  
[https://www.facebook.com/donna.g.allen.5/posts/10154772846583199](https://www.facebook.com/donna.g.allen.5/posts/10154772846583199)   
- [twitter.com/joseramosvivas/status/968602592082366466](https://twitter.com/joseramosvivas/status/968602592082366466)  
\2. ¿Que pasa si vamos al baño y al tirar de la cadena no bajamos la tapa? Hemos hecho la prueba.  
![](https://pbs.twimg.com/media/DXEq6juWAAEMWG_?format=jpg)  
- ¿Flota un huevo en el agua? Varía si añadimos sal?  
- ¿Medimos lo mismo por la mañana y por la noche?  
- ¿Caen dos objetos de la misma masa al tiempo al suelo? Dependencia: coger dos folios, uno hacerlo una bola y otro no  
- ¿Hierve el agua a la misma temperatura a distintas alturas?  
- ¿El periodo de oscilación del péndulo depende de la masa que colguemos de él?  
- ¿Depende el número de gotas que caben en una moneda del tamaño de la moneda?  

[Práctica virtual 3ºESO. El método científico (péndulo simple) - Departamento de Física y Química IES Valle de del Saja](https://drive.google.com/file/d/1owiVZCn_q65ysah9wkTMEPT7nKGVclwe/view)  Práctica virtual con un vídeo grabado en laboratorio  
[![](https://img.youtube.com/vi/2GDKNIyOCSc/0.jpg)](https://www.youtube.com/watch?v=2GDKNIyOCSc "Método científico. Estudio del periodo de un péndulo - Departamento de Física y Química IES Valle de del Saja")  



[twitter fqsaja1/status/1576131593260834816](https://twitter.com/fqsaja1/status/1576131593260834816)  
PRIMER CLIP de la temporada y práctica💪💪  
Para 2º ESO y con guion de @SylviukaHerrera grabamos ayer este clip explicativo con mis nuevas compañeras #LuciadelBarrio y @BlancoSoraya
 (adjuntamos guion)  
👏👏👏  
Alucinantes las imágenes que genera la tensión superficial.  
Guion. Un poco diferente al formato habitual para dirigir mejor a los alumnos que se enfrentan por primera vez al laboratorio,  
[Práctica virtual 2ºESO. El método científico (monedas y agua) - Departamento de Física y Química IES Valle de del Saja](https://drive.google.com/file/d/1-7Qnann0yZqy3oarmFUROKy72XRPU4fA/view)  
[![](https://img.youtube.com/vi/Y9gnmDfAmSw/0.jpg)](https://www.youtube.com/watch?v=Y9gnmDfAmSw "Gota - Departamento de Física y Química IES Valle de del Saja")  

[twitter onio72/status/1537767732573265924](https://twitter.com/onio72/status/1537767732573265924)  
¿Depende el número de gotas que caben sobre una moneda de su diámetro? ¿Y si añadimos sal al agua? ¿Y con jabón? #2ºESO  
![](https://pbs.twimg.com/media/FVc_mUlXoAAVYgJ?format=jpg)  

[twitter fqsaja1/status/1704810975252111491](https://twitter.com/fqsaja1/status/1704810975252111491)  
Comparto por aquí una ficha de trabajo para el alumno de 2º ESO para método científico. Es un resumen de una  presentación de #JuanVázquez realizada por mi compañera @sorayafyq. Os dejo la ficha y la presentación original.  
[¿Cómo trabajan los científicos? (pdf, 2 páginas)](https://drive.google.com/file/d/1q61mT9y-nfxe3HSQAFSzwO9xwd_U8g5a/view)  
[Cómo pensar como un científico. Ciencia, 1º ESO. Cuestionario de la lección. Profesor Juan Vázquez (pdf, 21 páginas)](https://drive.google.com/file/d/1KgeBxF5p288H6gzSyLhQD89Cptm05woy/view)  

[La actividad científica. Método científico, magnitudes y laboratorio escolar - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2eso-3eso/actividad-cientifica/)  



Para el tema de la caída se pueden ver vídeos asociados (pluma y martillo en la Luna, bola de bolos y pluma en  [recursos astronomía](/home/recursos/recursos-astronomia) )  

Escenas de la serie de televisión "Victor Ros" se pueden usar asociadas al método científico  
[http://www.rtve.es/alacarta/videos/victor-ros/](http://www.rtve.es/alacarta/videos/victor-ros/) 
>...misteriosos, que otros no pueden resolver, es en los **métodos científicos** y deductivos. Una serie basada en las conocidas novelas de misterio e investigación de Jerónimo Tristante y protagonizada por Carles Francino.  

Vídeos en 2º ESO del dinotren "tengo una hipótesis"  

En la serie "100 cosas que hacer antes de ir al instituto", de Nickelodeon, hay un capítulo sobre el método científico  
[100 cosas que hacer antes de ir al instituto - imdb](https://www.imdb.com/title/tt3904078/)  
 
[Antonio Dieguez ¿Existe 'El Método Científico'? Filosofía y ciencia en el siglo XXI. Respuesta corta: así con mayúsculas y en singular, no, no existe](https://blogs.elconfidencial.com/cultura/tribuna/2020-06-16/metodo-cientifico-filosofia-ciencia_2639264/)  

[Proyecto EDIA nº 31. Investigando las propiedades de la materia. Física y Química. Educación Secundaria - educacion.gob.es](https://sede.educacion.gob.es/publiventa/proyecto-edia-n-31-investigando-las-propiedades-de-la-materia-fisica-y-quimica-educacion-secundaria/educacion-secundaria-obligatoria-fisica-quimica/24211)  
> REA Investigando las propiedades de la materia está dirigido al alumnado de 2.º de ESO, de la materia de Física y Química (aunque también es adecuado para otros cursos). Se une al REA "Investigando la actividad científica" ya publicado anteriormente como parte de una serie de recursos que abordan el currículo a través de retos y que se denominan, en su conjunto, "Investigando". La propuesta se basa en el "aprendizaje basado en la investigación", proceso en el que el alumnado se introduce en la indagación por medio del método científico. El aprendizaje basado en la investigación es una estrategia didáctico¿ pedagógica que permite a los estudiantes observar, analizar, reflexionar, y resolver problemas. La propuesta presenta unas situaciones problema variadas, planteadas a modo de pequeños retos de algunas sesiones de duración. Cada reto se resuelve por medio de una serie de actividades investigativas guiadas y dirigidas a una producción. El docente puede elegir si hacer solo uno o varios de los retos, seguidos o alternados, o con las modificaciones que considere necesarias, sin interferir en su sistema de trabajo, complementándolo. Los retos se articulan en 4 bloques de contenido: Propiedades de la materia, Estados de agregación y teoría cinética,  [Investigando: La actividad científica - educalab.es](http://descargas.educalab.es/cedec/proyectoedia/fisica_quimica/contenidos/actividad_cientifica/index.html)  

[La diferencia entre una teoría y una ley científica - identidadgeek.com](http://identidadgeek.com/la-diferencia-entre-una-teoria-y-una-ley-cientifica/2011/06/)  

[Tina en el bosque de Charnia, la vida que no podía existir - lapizarradeyuri.com](http://www.lapizarradeyuri.com/2015/08/26/tina-en-el-bosque-de-charnia-la-vida-que-no-podia-existir/)  

[¿El método científico es un axioma? - edocet.naukas.com](http://edocet.naukas.com/2016/11/04/el-metodo-cientifico-es-un-axioma/)  

[Ni las teorías científicas son falsables, ni existe el método científico - edocet.naukas.com](http://edocet.naukas.com/2014/10/02/ni-las-teorias-cientificas-son-falsables-ni-existe-el-metodo-cientifico/)  

[El Rey León, la falsabilidad y los cuasicristales - culturacientifica.com](https://culturacientifica.com/2014/09/26/el-rey-leon-la-falsabilidad-y-los-cuasicristales/)  

Imagine. Nobel laureate has a postdoc who says some of their published results don’t replicate. Instead of threats, they get support in chasing it down - up to & including retracting original papers.  
THIS is how science self-corrects  
[”Definitely embarrassing:” Nobel Laureate retracts non-reproducible paper in Nature journal - retractionwatch.com](http://retractionwatch.com/2017/12/05/definitely-embarrassing-nobel-laureate-retracts-non-reproducible-paper-nature-journal/)  
[twitter LizNeeley/status/939123898427236353](https://twitter.com/LizNeeley/status/939123898427236353)  
[twitter frascafrasca/status/1179559798234390528](https://twitter.com/frascafrasca/status/1179559798234390528)  

El método científico explicado con memes  
![](https://pbs.twimg.com/media/EF6jYRPXYAAmzVF?format=jpg)  

[twitter MolaSaber/status/1695378535119044733](https://twitter.com/MolaSaber/status/1695378535119044733)  
Adquirir nuevo conocimiento puede ser duro.  
![](https://pbs.twimg.com/media/F4cxyNNWAAA2wLO?format=jpg)  

La revisión de la ciencia se puede asociar también a Física Moderna
[twitter thePiggsBoson/status/1534184168589905920](https://twitter.com/thePiggsBoson/status/1534184168589905920)  
A brief history of Physics  
![](https://pbs.twimg.com/media/FUqEXrZUEAEDz5g?format=jpg)  
 Science: Abridged Beyond the Point of Usefulness by Zach Weinersmith  
> Aristotle said a bunch of stuff that was wrong. Galileo and Newton fixed things up. Then Einstein broke everything again. Now, we’ve basically got it all worked out, except for small stuff, big stuff, hot stuff, cold stuff, fast stuff, heavy stuff, dark stuff, turbulence, and the concept of time  


[twitter biologiayarte/status/1179822155426680833](https://twitter.com/biologiayarte/status/1179822155426680833)  
3º ESO. MÉTODOS CIENTÍFICOS. FASES DEL MÉTODO CIENTÍFICO. LA HISTORIA DE LA PRIMERA VACUNA. #biologíamolinos3  
![](https://pbs.twimg.com/media/EF-R9gQX0AAT0g_?format=png)  
![](https://pbs.twimg.com/media/EF-R9geXkAEvMYc?format=png)  
![](https://pbs.twimg.com/media/EF-R9hhWoAIvemD?format=png)  
![](https://pbs.twimg.com/media/EF-R9gxWsAEeXLX?format=png)  
![](https://pbs.twimg.com/media/EF-STGhWkAEhrHm?format=png)  
![](https://pbs.twimg.com/media/EF-STGkWsAE5M4h?format=png)  
![](https://pbs.twimg.com/media/EF-STGpXoAEovjl?format=png)  
![](https://pbs.twimg.com/media/EF-STGsWsAEZtLr?format=png)  

[twitter CaRoLCrEsPo_FyQ/status/1184133969488633859](https://twitter.com/CaRoLCrEsPo_FyQ/status/1184133969488633859)  
Método científico 👨‍🔬 hilo va: está basado en una experiencia que realice en un workshop #SPW33 con la European Schoolnet #science #ciencia #scientix  
![](https://pbs.twimg.com/media/EG7jkFDW4AAfTK2?format=jpg)  
Como es un desdoble, realicé 4 cajas 📦 sorpresa (pequeños tupper) y escondí cosas dentro (de diferentes tamaños y/o materiales) Las cajas están precintadas (no encontré cajas semitransparentes o de colores)  
Utilicé bolsas de bocadillo blancas para meter los materiales (en cantidades también variables) y di una caja por grupo. Además disponían de una caja vacía, una balanza, varias bolsas de plástico y un imán 🧲  
La hoja de prácticas únicamente contaba que el objetivo era intentar averiguar el contenido y para ello debían seguir los pasos del Método Científico. Planteando hipótesis, experiencias y anotando observaciones de modo que pudieran llegar a una conclusión.  
Los chicos han demostrado imaginación y algunas cajas eran más evidentes que otras. Las únicas pistas que tenían era que no había líquidos ni comida. Cuando acababan con una podían intentar otra. Ha funcionado bastante bien en 2 eso. Y todos intentaron las 4 📦  
Ahora solo queda que redacten en casa todo el proyecto. Dentro de un mes abriré las cajas en clase.  

Arturo Quirantes  
ACME (Anumerismo, Ciencia, Método y Escepticismo)  
[Lección 1: Introducción al método científico](https://elprofedefisica.es/cursos/acme/lecciones/leccion-1/)  

[![](https://img.youtube.com/vi/1ERWcy89Xbg/0.jpg)](https://www.youtube.com/watch?v=1ERWcy89Xbg "Apuntes de Física 1.1 - Introducción a la Física. El método científico.")  
Apuntes de Física 1.1 - Introducción a la Física. El método científico. Arturo Quirantes.   

[twitter alfredo24404099/status/1680522951718760449](https://twitter.com/alfredo24404099/status/1680522951718760449)  
Hola a tod@s. Dejo una SITUACIÓN DE APRENDIZAJE completa llamada "El primer sospechoso" donde se trabaja el método científico para cursos de secundaria. Os dejo también la ficha completa con los criterios, desarrollo sesiones y contenidos, rúbricas, etc.  Sigo hilo...  
En este enlace está la gamificación de la situación de aprendizaje....  
[el primer sospechoso - genial.ly](https://view.genial.ly/64ac20b08d698e0010da737e/interactive-content-el-primer-sospechoso)  
Y en [este otro enlace dejo el pdf con la Situación de Aprendizaje desarrollada.](https://drive.google.com/file/d/18sIruIgfFxhJqafqzInw_GbTOIKZvNEh/view?usp=drive_link)  
En ella lleváis desarrollados todos los aspectos para particularizar en vuestra aula. Agradezco difusión si os gusta y si la usáis agradezco referencia. Gracias.  
Y por supuesto queda colgada en mi página de recursos por si quieres curiosear más ideas.🤗  
[fiquillados - Francisco Collados](https://franciscojuancolla.wixsite.com/fiquillados/)  

[twitter OrmazabalIon/status/1707042076951527923](https://twitter.com/OrmazabalIon/status/1707042076951527923)  
Estos días he llevado a cabo una experiencia muy sencilla en 3ESO BIOGEO. Llevo al aula una caja de pasas y un vaso de precipitados lleno de agua. Dejo caer las pasas al agua y les pido que propongan una hipótesis: ¿va a ocurrir algún cambio? ¿Por qué?  
Muchos contestan que las pasas se hincharán de agua, y aciertan. Pero no muchos o ninguno conoce el por qué, y es es la parte más complicada de la experiencia. Explicar la ósmosis y cómo afecta a las células un medio hipertónico o hipotónico.  
En este vídeo que preparé hace tiempo explico en qué consiste el proceso y cómo devolver a las pasas su forma y tamaño inicial.  
[twitter OrmazabalIon/status/1324994094276497409](https://twitter.com/OrmazabalIon/status/1324994094276497409)  
Hoy os presento un experimento para hacer con vuestros peques y no tan peques. La clave está en lo siguiente: qué ocurre si metemos... pasas en un vaso de agua?  
Materiales: pasas, agua y curiosidad.  
Video ON  
_Se meten las pasas normales en agua y luego las pasas hinchadas en miel_ 

En esta ocasión, en el aula, y a falta de miel para "revertir" el proceso, les planteé pensar cómo podríamos hacer para crear un medio con menos concentración de agua que nuestras pasas hinchadas, una disolución con mayor concentración de soluto que el interior de la pasa  
Y decidimos probar con sal. Agua con muuuuuucha sal. Lo primero que observamos es que las pasas hinchadas flotaban en el agua. Un par de zagales lo achacaron a la alta densidad del agua salada. Bien jugado.   
Hoy hemos comprobado cómo estaban esas pasas y...¡si! ¡Han perdido agua!  
Se han arrugado. No han vuelto al nivel inicial, pero la diferencia es notable.   
Y eso es todo. Podéis hacer este mini-experimento con vuestros peques en casa. Este finde lo haré con mi pitufa a ver qué dice.  

[Enseñar el método científico en institutos demuestra ser útil para desmontar falsos mitos - agenciasinc](https://www.agenciasinc.es/Noticias/Ensenar-el-metodo-cientifico-en-institutos-demuestra-ser-util-para-desmontar-falsos-mitos)  
La denominada ‘ilusión de causa-efecto’ es un sesgo por el que las personas validamos supersticiones o pseudociencias. Una falsa relación muy extendida entre la población. Un equipo de investigadores ha desarrollado y aplicado un taller sencillo de metodología y pensamiento científico y ha mostrado que, si se realiza en centros educativos, disminuye un 78 % estas falsas creencias a corto y largo plazo.  

[Martínez, N., Matute, H., Blanco, F., & Barberia, I. “A large-scale study and six-month follow-up of an intervention to reduce causal illusions in high school students”. Royal Society Open Science.](https://royalsocietypublishing.org/doi/10.1098/rsos.240846)  

> This brief educational intervention, lasting for approximately 80 min, focused on training participants in base rate, experimental control conditions and confounding variables, as an appropriate approach for everyday causal inference. The intervention comprised two phases that were conducted face-to-face: a bias-induction staging phase (where participants were asked to experience the purported benefits of a bogus product in improving cognitive and physical abilities) followed by a training phase (where participants were revealed that the product did not have the claimed benefits and were trained in the design of experiments with appropriate control conditions for judging whether a product or a treatment is effective). The effectiveness of the intervention was assessed using a standard computerized contingency learning task. In this task, participants were required to imagine they were doctors evaluating the influence of a drug (i.e. a potential cause) on the recovery from a disease (i.e. the outcome) in a group of patients. They observed a number of patients (i.e. trials), and for each of them, they could decide whether to administer the drug or not, and they received feedback regarding the subsequent recovery of the patient. After all trials were completed, participants were asked to give a causal judgement indicating the degree to which they considered the drug to be effective against the disease.  

[Implementation and Assessment of an Intervention to Debias Adolescents against Causal Illusions. Itxaso Barberia, Fernando Blanco, Carmelo P. Cubillas, Helena Matute](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0071303)  


## Serendipia
[Serendipia - RAE](https://dle.rae.es/serendipia)  
> Hallazgo valioso que se produce de manera accidental o casual.  
[List of works based on dreams: Science - wikipedia.org](https://en.m.wikipedia.org/wiki/List_of_works_based_on_dreams#Science)  
 
