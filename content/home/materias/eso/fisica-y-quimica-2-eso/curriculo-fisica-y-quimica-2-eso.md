
# Currículo 2º ESO Física y Química (LOMCE)

Ver DECRETO 48/2015 en  [legislación](/home/legislacion-educativa)   
El decreto fija los criterios de evaluación y estándares de aprendizaje evaluables de manera conjunta para 2º y 3º ESO, no se ponen de nuevo aquí: están en  [Física y Química 3º ESO (LOMCE)](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce) , que es la que se implanta primero en 2015-2016 según el calendario de implantación.  
Los contenidos son iguales para 2º ESO y 3º ESO en BOE, pero distintos en Decreto 48/2015 de Madrid. En territorio MEC Orden ECD/1361/2015 también varían entre 2º y 3º  [BOE-A-2015-8149.pdf#page=132](http://www.boe.es/boe/dias/2015/07/21/pdfs/BOE-A-2015-8149.pdf#page=132)   
  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver Contenidos currículo con enlaces a recursos: 2º ESO Física y Química (LOMCE) (pendiente)  
  
Es una materia que aparece en LOMCE, antes en 2º ESO con LOE había  [Ciencias de la Naturaleza](/home/materias/eso/ciencias-de-la-naturaleza-1y2eso)   

## [Comparación currículo Física y Química 2º ESO](/home/materias/eso/fisica-y-quimica-2-eso/comparacion-curriculo-fisica-y-quimica-2-eso)


## Currículo 2º ESO condicionado por laboratorio, TIC y simulaciones
El currículo cita a veces laboratorio y simulaciones, por lo que si no hay laboratorio (por ejemplo por falta de desdoble es habitual) o no hay posibilidad de un aula de informática para todos los alumnos, hay ciertas partes del currículo que no se pueden cubrir, y se citan aquí. Hay algunos para los que sí es posible (por ejemplo identificar material de laboratorio, o proponer un experimento).  
Comentado junto a  [3º ESO LOMCE](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce) , ya que está asociado a criterios de evaluación y estándares de aprendizaje evaluables, comunes en LOMCE entre 2º y 3º  

## Contenidos 2º ESO

### Bloque 1. La actividad científica
1. El método científico: sus etapas.  
2. Medida de magnitudes. Sistema Internacional de Unidades.  
- Notación científica.  
3. Utilización de las tecnologías de la información y la comunicación.  
4. El trabajo en el laboratorio.  
5. Proyecto de Investigación  

### Bloque 2. La materia
1. Propiedades de la materia  
2. Estados de agregación.  
- Cambios de estado.  
- Modelo cinético-molecular  
3. Sustancias puras y mezclas  
4. Mezclas de especial interés: disoluciones acuosas, aleaciones y coloides  
5. Métodos de separación de mezclas  
6. Estructura atómica.  
7. Uniones entre átomos: moléculas y cristales.  
8. Elementos y compuestos de especial interés con aplicaciones industriales, tecnológicas y biomédicas.  

### Bloque 3. Los cambios
1. Cambios físicos y cambios químicos  
2. La reacción química  
3. La química en la sociedad y el medio ambiente  

### Bloque 4. El movimiento y las fuerzas
1. Las fuerzas.  
- Efectos.  
- Velocidad media.  
2. Máquinas simples.  
3. Las fuerzas de la naturaleza.  

### Bloque 5. Energía
1. Energía.  
- Unidades.  
2. Tipos.  
- Transformaciones de la energía y su conservación  
3. Energía térmica.  
- El calor y la temperatura.  

