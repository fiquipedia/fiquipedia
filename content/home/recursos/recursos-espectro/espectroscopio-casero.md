
# Espectroscopio casero

Enlaza con  [prácticas de laboratorio](/home/recursos/practicas-experimentos-laboratorio)   
 [Construccion_Espectroscopio_casero.pdf](http://gybugandofisica.scienceontheweb.net/Materiales/Construccion_Espectroscopio_casero.pdf)   
Gymnázium Budě jovická, en español. Incluye plantilla, se usa parte de CD.  
  
[Espectroscopio con materiales caseros](http://www.cienciafacil.com/ESPECTROSCOPIO.html)   
Copyright, Ciencia Facil   

[Experimentos de Física y Química en tiempos de crisis. Editum. Ediciones de la Universidad de Murcia.](https://doi.org/10.6018/editum.2421)  
29 Espectroscopio de reflexión
  
   
[CONSTRUCCIÓN DE UN ESPECTROSCOPIO (pdf) - cac.es](http://www.cac.es/cursomotivar/resources/document/2010/4.pdf)   
Incluye plantilla, se usa CD entero. Patricia Lago Santolaya, IES Vicenta Ferrer Escrivá, Valencia   
  
[Cómo hacer un  Espectroscopio - comolohago.cl](http://www.comolohago.cl/como-hacer-un-espectroscopio/)   
Se usa trozo de CD y rollo de papel higiénico.  
  
CÓMO CONSTRUIR UN ESPECTROSCOPIO CASERO CON UN CD  
Revista Eureka sobre Enseñanza y Divulgación de las Ciencias, vol. 6, núm. 3, 2009, pp. 491-495  
[CÓMO CONSTRUIR UN ESPECTROSCOPIO CASERO CON UN CD](http://www.redalyc.org/articulo.oa?id=92013010014)   
Heredia Avalos, Santiago, Asociación de Profesores Amigos de la Ciencia: EUREKA, Cádiz, España  
Incluye plantilla, se usa CD entero, incluye referencias  

[Construyendo un espectroscopio - unlp.edu.ar](http://pagciencia.quimica.unlp.edu.ar/spectrsc.htm)  
  
[Espectroscopio - monografias.com](http://www.monografias.com/trabajos95/espectroscopio/espectroscopio.shtml)   
Mariana Rodríguez Gutiérrez, incluye referencias  
  
[Construcción de un espectroscopio](http://rincondelaciencia.educa.madrid.org/Practica/PR-14/PR-14.html)   
José Ricardo Belmonte  
I.E.S. Gregorio Marañón (Madrid)  
  
 [Espectrómetro Casero - Ikkaro](https://www.ikkaro.com/espectrometro-casero/)   
  
Smartphone Spectrometer, Making a Spectrometer from a Smartphone Camera  
 [http://alberding.phys.sfu.ca/wordpress/?p=447](http://alberding.phys.sfu.ca/wordpress/?p=447)   
  
[Foldable Spectrometry Starter Kit - publiclab.org](http://publiclaboratory.org/wiki/foldable-spec)   
[Preparing a DVD-R to act as a diffraction grating - publiclab.org](http://publiclab.org/notes/MrBumper/01-11-2015/preparing-a-dvd-r-to-act-as-a-diffraction-grating)   
  
[SpectralWorkbench](http://spectralworkbench.org/)   
[Spectrometry - publiclab.org](https://publiclab.org/wiki/spectrometry)  
 
Realizo una plantilla que junta 3 recortables en un único PDF
[A4-mini-spec3.8-3units.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-espectro/espectroscopio-casero/A4-mini-spec3.8-3units.pdf)  
  
2017:  [3D printed mini spectrometer - publiclab.org](https://publiclab.org/notes/rthalman/01-19-2017/3d-printed-mini-spectrometer)   
  
2019:  [Fabrication of a low-cost and high-resolution papercraft smartphone spectrometer - arxiv.org](https://arxiv.org/abs/1910.12616) Fabrication of a low-cost and high-resolution papercraft smartphone spectrometerYoung-Gu Ju  
(Submitted on 22 Oct 2019)  
We demonstrated the fabrication of a low-cost and high-resolution papercraft smartphone spectrometer and characterized its performance by recording spectra from gas discharge lamps. The optical design and a lab-made narrow slit used in the fabrication led to fine images of the slit on the image sensor, resulting in high spectral resolution. The spectral resolution of the fabricated papercraft smartphone spectrometer was measured to be 0.5 nm, which is similar to that of the best smartphone spectrometer reported thus far. Extending the exposure time of the phone's camera revealed the fine structure of a spectrum with high sensitivity. The build cost of the papercraft smartphone spectrometer was less than $3. We demonstrated that the papercraft smartphone spectrometer is a low-cost device that can record spectra with high resolution and high sensitivity.  

