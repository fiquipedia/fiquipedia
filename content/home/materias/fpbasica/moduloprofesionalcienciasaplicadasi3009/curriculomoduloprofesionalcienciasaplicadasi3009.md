
# Currículo Módulo Profesional "Ciencias Aplicadas I", Código 3009

En principio surge asociado a LOMCE, pero con LOMLOE, pero se mantiene inicialmente en LOMLOE, ver [FP Básica](/home/materias/fpbasica)

## Objetivos
Los objetivos no están asociados al módulo, sino al título.  
Por ejemplo según [Real Decreto 127/2014](https://www.boe.es/buscar/act.php?id=BOE-A-2014-2360), ANEXO VIII Título Profesional Básico en Peluquería y Estética, apartado 3.1. Objetivos generales del título, los objetivos generales de este ciclo formativo son los siguientes:  
a) Reconocer productos y materiales de estética y peluquería, así como los métodos para su limpieza y desinfección, relacionándolos con la actividad correspondiente para preparar los equipos y útiles.  
b) Seleccionar los procedimientos de acogida del cliente relacionándolos con el tipo de servicio para acomodarlo y protegerlo con seguridad e higiene  
c) Seleccionar operaciones necesarias sobre uñas de manos y pies vinculándolas al efecto perseguido para aplicar técnicas básicas de embellecimiento.  
d) Analizar los tipos de depilación valorando los efectos sobre el vello y la piel para aplicar técnicas de depilación.  
e) Elegir productos adecuados a cada piel valorando los tiempos de exposición para decolorar el vello.  
f) Reconocer las técnicas y procedimientos básicos de maquillaje relacionándolos con el efecto buscado y las características del cliente para realizar maquillaje social y de fantasía.  
g) Reconocer las técnicas de lavado y acondicionado de cabello relacionándolos con cada tipo de servicio para lavarlo y acondicionarlo  
h) Seleccionar técnicas de peinado justificándolos en función del estilo perseguido para iniciar el peinado.  
i) Reconocer los tipos de cambios permanentes en el cabello eligiendo equipamiento y materiales propios de cada uno para efectuarlos  
j) Identificar técnicas de decoloración, coloración y tinte relacionándolas con los diferentes materiales y tiempos de aplicación para cambiar el color del cabello.  
k) Comprender los fenómenos que acontecen en el entorno natural mediante el conocimiento científico como un saber integrado, así como conocer y aplicar los métodos para identificar y resolver problemas básicos en los diversos campos del conocimiento y de la experiencia.  
l) Desarrollar habilidades para formular, plantear, interpretar y resolver problemas aplicar el razonamiento de cálculo matemático para desenvolverse en la sociedad, en el entorno laboral y gestionar sus recursos económicos.  
m) Identificar y comprender los aspectos básicos de funcionamiento del cuerpo humano y ponerlos en relación con la salud individual y colectiva y valorar la higiene y la salud para permitir el desarrollo y afianzamiento de hábitos saludables de vida en función del entorno en el que se encuentra.  
n) Desarrollar hábitos y valores acordes con la conservación y sostenibilidad del patrimonio natural, comprendiendo la interacción entre los seres vivos y el medio natural para valorar las consecuencias que se derivan de la acción humana sobre el equilibrio medioambiental.  
ñ) Desarrollar las destrezas básicas de las fuentes de información utilizando con sentido crítico las tecnologías de la información y de la comunicación para obtener y comunicar información en el entorno personal, social o profesional.  
o) Reconocer características básicas de producciones culturales y artísticas, aplicando técnicas de análisis básico de sus elementos para actuar con respeto y sensibilidad hacia la diversidad cultural, el patrimonio histórico-artístico y las manifestaciones culturales y artísticas.  
p) Desarrollar y afianzar habilidades y destrezas lingüísticas y alcanzar el nivel de precisión, claridad y fluidez requeridas, utilizando los conocimientos sobre la lengua castellana y, en su caso, la lengua cooficial para comunicarse en su entorno social, en su vida cotidiana y en la actividad laboral.  
q) Desarrollar habilidades lingüísticas básicas en lengua extranjera para comunicarse de forma oral y escrita en situaciones habituales y predecibles de la vida cotidiana y profesional.  
r) Reconocer causas y rasgos propios de fenómenos y acontecimientos contemporáneos, evolución histórica, distribución geográfica para explicar las características propias de las sociedades contemporáneas.  
s) Desarrollar valores y hábitos de comportamiento basados en principios democráticos, aplicándolos en sus relaciones sociales habituales y en la resolución pacífica de los conflictos.  
t) Comparar y seleccionar recursos y ofertas formativas existentes para el aprendizaje a lo largo de la vida para adaptarse a las nuevas situaciones laborales y personales.  
u) Desarrollar la iniciativa, la creatividad y el espíritu emprendedor, así como la confianza en sí mismo, la participación y el espíritu crítico para resolver situaciones e incidencias tanto de la actividad profesional como de la personal.  
v) Desarrollar trabajos en equipo, asumiendo sus deberes, respetando a los demás y cooperando con ellos, actuando con tolerancia y respeto a los demás para la realización eficaz de las tareas y como medio de desarrollo personal.  
w) Utilizar las tecnologías de la información y de la comunicación para informarse, comunicarse, aprender y facilitarse las tareas laborales.  
x) Relacionar los riesgos laborales y ambientales con la actividad laboral con el propósito de utilizar las medidas preventivas correspondientes para la protección personal, evitando daños a las demás personas y en el medio ambiente.  
y) Desarrollar las técnicas de su actividad profesional asegurando la eficacia y la calidad en su trabajo, proponiendo, si procede, mejoras en las actividades de trabajo.  
z) Reconocer sus derechos y deberes como agente activo en la sociedad, teniendo en cuenta el marco legal que regula las condiciones sociales y laborales para participar como ciudadano democrático.  
  


## Contenidos. 
En  [Orden ECD/1030/2014](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-6431)  de junio se repite para los 14 títulos con el mismo contenido.  
  
En pdf de 399 páginas se indica el título asociado y la página donde se indica "Módulo Profesional: Ciencias aplicadas I. Código: 3009. Duración: 160 horas."  
I SERVICIOS ADMINISTRATIVOS: página 16  
II ELECTRICIDAD Y ELECTRÓNICA: página 41  
III FABRICACIÓN Y MONTAJE: página 70  
IV INFORMÁTICA Y COMUNICACIONES: página 96  
V COCINA Y RESTAURACIÓN: página 124  
VI MANTENIMIENTO DE VEHÍCULOS:página 151  
VII AGRO-JARDINERÍA Y COMPOSICIONES FLORALES:página 183  
VIII PELUQUERÍA Y ESTÉTICA: página 213  
IX SERVICIOS COMERCIALES:página 241  
X CARPINTERÍA Y MUEBLE:página 267  
XI REFORMA Y MANTENIMIENTO DE EDIFICIOS:página 296  
XII ARREGLO Y REPARACIÓN DE ARTÍCULOS TEXTILES Y DE PIEL:página 324  
XIII TAPICERÍA Y CORTINAJE:página 351  
XIV VIDRIERÍA Y ALFARERÍA:página 380  
  
Estos contenidos de estos 14 títulos en  [Orden ECD/1030/2014](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-6431)  de junio (para Ceuta y Melilla) no son idénticos a los contenidos de  [Real Decreto 127/2014](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-2360)  de febrero para los mismos 14 títulos, donde tenía una duración de 90 h en lugar de 160 h.   
En  [Real Decreto 356/2014](http://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-5591)  también tienen duración de 90 h, pero no hay una normativa posterior.  
  
BOE: 160 h para Ciencias Aplicadas I código 3009 y 160 h para Comunicación y Sociedad I 3011BOCM: 130 h para Ciencias Aplicadas I código 3009 y 190 h para Comunicación y Sociedad I 301  
  
En BOE de junio pone "Primer curso 32 sem.", así que 32x5=160h  
En el BOCM ponen como nota asociada a las 190 h totales y 7 semanales del código 3011 "Comunicación y Sociedad I"  
"(1) Cuando este módulo profesional se divida en las unidades formativas: “Comunicación en Lengua Castellana y Sociedad” y “Comunicación en Lengua Inglesa”, el horario semanal asignado a cada una de ellos será de 5 horas y 2 horas, respectivamente."  
  
A 5 horas semana, si ponen 130 en total, serían, 130/5=26 semanas ...  
A 5 horas semana, si ponen 190 en total, serían 190/7=27,14 semanas  
Madrid cumple el BOE de febrero, pero es llamativo- Los mismos contenidos, coma por coma, en Ceuta y Melilla tienen 30 h más de clase para verlos que en Madrid.- Aunque pasan de 160 a 130 h, siguen siendo 5 h/semana en ambos sitios, lo que implicaría 26 semanas en Madrid en lugar de 32 en Ceuta y Melilla... luego debe haber prácticas o algo así y el curso será más corto en Madrid.  
El  [Decreto 107/2014 ](http://www.bocm.es/boletin/CM_Orden_BOCM/2014/09/15/BOCM-20140915-1.PDF) de Madrid para el módulo de código 3009 indica exactamente los mismos contenidos que el BOE de junio para Ceuta y Melilla: hay un cambio mínimo en la redacción del apartado "Localización de estructuras anatómicas básicas" pero diciendo lo mismo.  
En el BOCM son un "poco más listos" que en el BOE y no repiten los contenidos del código 3009 tantas veces como títulos hay, sino que lo incluyen una única vez.  
Lo que está añadido en contenidos en junio BOE / septiembre BOCM respecto a febrero se indica en negrita  
  
En 2020 se modifica en BOCM, añadiendo 2 h de Educación Física en Ciencias Aplicadas dentro de 3009  
 [BOCM-20200518-1.PDF#page=22](http://www.bocm.es/boletin/CM_Orden_BOCM/2020/05/18/BOCM-20200518-1.PDF#page=22)   


### Resolución de problemas mediante operaciones básicas. 
Reconocimiento y diferenciación de los distintos tipos de números.  
Representación en la recta real.  
Utilización de la jerarquía de las operaciones.  
**Uso de paréntesis en cálculos que impliquen las operaciones de suma, resta, producto, división y potencia.**  
Interpretación y utilización de los números reales y las operaciones en diferentes contextos. **Notación más adecuada en cada caso.**  
Proporcionalidad directa e inversa.  
**Aplicación a la resolución de problemas de la vida cotidiana.**  
Los porcentajes en la economía.  
**Interés simple y compuesto.**  

### Reconocimiento de materiales e instalaciones de laboratorio:
Normas generales de trabajo en el laboratorio.  
Material de laboratorio. Tipos y utilidad de los mismos.  
Normas de seguridad. **Reactivos. Utilización, almacenamiento y clasificación.**  
Técnicas de observación ópticas. Microscopio y lupa binocular.  

### Identificación de las formas de la materia:
Unidades de longitud: **el metro, múltiplos y submúltiplos**.   
Unidades de capacidad: **el litro, múltiplos y submúltiplos.**  
Unidades de masa: **el gramo, múltiplos y submúltiplos.**  
Materia. Propiedades de la materia. **Sistemas materiales.**  
Sistemas materiales homogéneos y heterogéneos.  
Naturaleza corpuscular de la materia. **Teoría cinética de la materia.**  
Clasificación de la materia según su estado de agregación y composición.  
Cambios de estado de la materia.  
**Temperatura de Fusión y de Ebullición.**  
Concepto de temperatura.  
Diferencia de ebullición y evaporación.  
Notación científica.   

### Separación de mezclas y sustancias:
Diferencia entre sustancias puras y mezclas.  
Técnicas básicas de separación de mezclas: **decantación, cristalización y destilación.**  
Clasificación de las sustancias puras.  
Tabla periódica.  
Diferencia entre elementos y compuestos.  
Diferencia entre mezclas y compuestos.  
Materiales relacionados con el perfil profesional.  
**Elementos más importantes de la tabla periódica y su ubicación.**  
Propiedades más importantes de los elementos básicos.  

###  Reconocimiento de la energía en los procesos naturales:
Manifestaciones de la energía en la naturaleza: **terremotos, tsunamis, volcanes, riadas, movimiento de las aspas de un molino y energía eléctrica obtenida a partir de los saltos de agua en los ríos, entre otros.**  
La energía en la vida cotidiana.  
Distintos tipos de energía.  
Transformación de la energía.  
Energía, calor y temperatura. Unidades.  
Fuentes de energía renovables y no renovables.  
**Fuentes de energía utilizadas por los seres vivos.**  
Conservación de las fuentes de energías  

### Localización de estructuras anatómicas básicas:
Niveles de organización de la materia viva.  
Proceso de nutrición: **en qué consiste, que aparatos o sistemas intervienen, función de cada uno de ellos, integración de los mismos.**  
Proceso de excreción: **en qué consiste, que aparatos o sistemas intervienen, función de cada uno de ellos, integración de los mismos.**  
Proceso de relación: **en qué consiste, que aparatos o sistemas intervienen, función de cada uno de ellos, integración de los mismos.**  
Proceso de reproducción: **en qué consiste, que aparatos o sistemas intervienen, función de cada uno de ellos, integración de los mismos.**  

### Diferenciación entre salud y enfermedad:
La salud y la enfermedad.  
El sistema inmunitario.  
**Células que intervienen en la defensa contra las infecciones.**  
Higiene y prevención de enfermedades.  
Enfermedades infecciosas y no infecciosas.  
Tipos de enfermedades infecciosas más comunes.  
Las vacunas.  
Trasplantes y donaciones** de células, sangre y órganos**.  
Enfermedades de transmisión sexual. Prevención.  
La salud mental: prevención de drogodependencias y de trastornos alimentarios.  

### Elaboración de menús y dietas:
Alimentos y nutrientes**, tipos y funciones.**  
Alimentación y salud.  
Hábitos alimenticios saludables.  
Dietas y elaboración de las mismas.  
Reconocimiento de nutrientes presentes en ciertos alimentos, discriminación de los mismos. **Representación en tablas o en murales.**  
Resultados y sus desviaciones típicas.  
Aplicaciones de salud alimentaria en entorno del alumno.  

### Resolución de ecuaciones sencillas:
Progresiones aritméticas y geométricas.  
**Análisis de sucesiones numéricas.**  
Sucesiones recurrentes.  
Las progresiones como sucesiones recurrentes  
Curiosidad e interés por investigar las regularidades, relaciones y propiedades que aparecen en conjuntos de números.  
Traducción de situaciones del lenguaje verbal al algebraico.  
Transformación de expresiones algebraicas. **Igualdades notables.**  
Desarrollo y factorización de expresiones algebraica.  
Resolución de ecuaciones de primer grado con una incógnita.  
**Resolución de problemas mediante la utilización de ecuaciones.**  
  

