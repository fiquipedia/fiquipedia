# Recursos adaptaciones curriculares Física y Química

En 2016 (estando en vigor LOMCE) creo esta página para intentar poner información asociada a realizar adaptaciones curriculares para ACNEAE (Alumnos Con Necesidades Especiales de Atención Educativa)  
En 2025 reviso y ya aplica LOMLOE, con algunos temas como plan incluYO de Madrid.   
Cuando son "significativas" ó "individualizadas" (ACIs) muchas veces el nivel curricular es de cursos previos: por ejemplo un alumno cursando 2º ESO puede tener un nivel curricular de primaria en algunas materias.   

El problema es que aunque ese concepto esté claro en matemáticas o lengua, que están en todos los cursos, no está tan claro en física y química, que no existe en cursos anteriores a 2ºESO.  

Por lo tanto una primera idea es conocer los contenidos de "conocimiento del medio" "ciencias de la naturaleza" de cursos previos a que pasen a denominarse física y química, para hacer pruebas de evaluación y determinar su nivel curricular, y luego usar materiales adaptados para trabajar acorde a su nivel, o acorde a las limitaciones. Hay adaptaciones de medio (para discapacidades motoras o visuales que es un tema distinto, aunque intentaré poner información). También puede haber adaptaciones asociadas a alumnado de altas capacidades intelectuales.  

Se puede plantear si hay que hacer una adaptación de "ciencias de la naturaleza", incluyendo aspectos que no tengan nada que ver con física y química.  

Física y Química es una materia donde se comienza la abstracción (se manejan conceptos abstractos que generalizan ejemplos) y donde se usan fórmulas y gráficas: es necesaria cierta competencia matemática. Las adaptaciones puede estar relacionadas o condicionadas por su nivel en matemáticas, su nivel de comprensión en lengua.  

En cursos posteriores a 2º ESO se puede retroceder la adaptación a enseñar el nivel de 2º ESO, pero en 2º ESO hay que retroceder necesariamente a ciencias naturales, intentando centrarse en aspectos cualitativos, sin fórmulas.

## Normativa sobre adaptaciones curriculares
En general la normativa citada en  [legislación](/home/legislacion-educativa)  

**Pendiente documentar bien el tema de notas con * y si esas notas permiten promocionar / titular, en el caso de Madrid (puede variar según comunidad)**  

[Artículo 68. Programaciones didácticas. Real Decreto 83/1996, de 26 de enero, por el que se aprueba el Reglamento orgánico de los institutos de educación secundaria.](https://www.boe.es/buscar/act.php?id=BOE-A-1996-3834#a68)  
> 2. La programación didáctica de los departamentos incluirá, necesariamente, los siguientes
aspectos para cada una de las áreas, materias y módulos asignados al mismo o integrados en él,
como consecuencia de lo establecido en los apartados 3 y 4 del artículo 48 de este Reglamento:  
>…  
k) Las medidas de atención a la diversidad y las adaptaciones curriculares para los alumnos que
las precisen.  

**LOE (modificada por LOMLOE)**  
[Artículo 22 Ley Orgánica 2/2006](http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#a22)  
>7. Las medidas de atención a la diversidad que adopten los centros estarán orientadas a la consecución de los objetivos de la educación secundaria obligatoria por parte de todo su alumnado y no podrán, en ningún caso, suponer una discriminación que les impida alcanzar dichos objetivos y la titulación correspondiente.  

  
[Sección primera. Alumnado que presenta necesidades educativas especiales Ley Orgánica 2/2006](http://www.boe.es/buscar/act.php?id=BOE-A-2006-7899#sprimera-2)  

[DECRETO 23/2023, de 22 de marzo, del Consejo de Gobierno, por el que se regula la atención educativa a las diferencias individuales del alumnado en la Comunidad de Madrid](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/03/24/BOCM-20230324-1.PDF)  
> Artículo 12  
Medidas específicas de atención educativa  
Las medidas específicas de atención educativa que, con carácter general y sin perjuicio de la regulación propia de cada enseñanza, podrán aplicarse al alumnado con necesidades educativas especiales son las siguientes:  
a) Adaptaciones curriculares en las enseñanzas de Educación Infantil, Educación
Primaria y Educación Secundaria Obligatoria. Se podrán adecuar o modificar los
contenidos y criterios de evaluación de las diferentes áreas, materias o ámbitos
con el objeto de minimizar las barreras para el aprendizaje y la participación en el
alumnado identificado con necesidades educativas especiales. En el caso de que
las adaptaciones curriculares abarquen la programación de contenidos o criterios
de evaluación de ciclos o cursos anteriores se denominarán adaptaciones curriculares significativas.  
Se elaborará una adaptación curricular por cada una de las áreas, materias o ámbitos que incluirá las modificaciones oportunas y buscará el máximo desarrollo posible de las competencias.  
Las adaptaciones curriculares que se realicen estarán orientadas a la consecución
de los objetivos de las etapas correspondientes y el perfil de salida de la Educación Secundaria Obligatoria, según la reglamentación de cada etapa.  


[ORDEN 1712/2023, de 19 de mayo, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en la Educación Secundaria Obligatoria.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/05/31/BOCM-20230531-17.PDF)
Capítulo III  
Atención a las diferencias individuales  

>Artículo 10  
>Medidas específicas para los alumnos con necesidades educativas especiales  
>1. El alumnado con necesidades educativas especiales se definen en el artículo 10 del
Decreto 23/2023, de 22 de marzo, y requiere determinados apoyos y atenciones educativas específicas para la consecución de los objetivos de aprendizaje adecuados a su desarrollo.  
>2. Las necesidades educativas de estos alumnos se identificarán y determinarán mediante la correspondiente evaluación psicopedagógica e informe psicopedagógico asociado.  
>3. Sin perjuicio de la aplicación de cuantas medidas ordinarias procedan, los alumnos con necesidades educativas especiales podrán contar con las siguientes medidas específicas:  
a) Adaptación curricular individualizada y significativa (ACIS) en las materias que
sea preciso. Las ACIS suponen una adaptación de los criterios de evaluación y
contenidos que se apartan significativamente de los recogidos en el currículo establecido con carácter general para cada materia y curso en el anexo II del Decreto 65/2022, de 20 de julio. Dichas ACIS se realizarán buscando el máximo desa-
rrollo posible de las capacidades del alumno y tendrán como finalidad el progreso
en su aprendizaje y formación, debiendo facilitar que el alumno pueda alcanzar los
objetivos y competencias establecidos en la etapa.  
b) Adaptaciones específicas de acceso al currículo, que supondrán la adopción de medidas organizativas y metodológicas sin que se alteren los elementos curriculares establecidos para cada materia. Estas medidas garantizarán que los alumnos
con necesidades educativas especiales que lo precisen puedan acceder al currículo. Entre estas adaptaciones se podrá contemplar la adecuación de los espacios, los recursos materiales y personales, así como los que permitan el acceso a la información y a la comunicación.  
c) Adecuación de los procesos de evaluación, con el fin de facilitar el acceso a las actividades de evaluación. Entre las adaptaciones de los procesos de evaluación se contemplará el aumento en los tiempos para el desarrollo de las actividades de
evaluación, el uso de instrumentos de evaluación diversos y la adaptación de los
formatos de las pruebas de evaluación.  
d) Apoyo específico a las materias o ámbitos en los que se haya realizado una ACIS con la atención educativa por parte del profesorado especialista en Pedagogía Terapéutica o Audición y Lenguaje. Estos apoyos tendrán como finalidad que el
alumno progrese en su aprendizaje y formación y deberán facilitar que pueda alcanzar los objetivos y competencias establecidos en la etapa.  
e) Medidas de flexibilización y alternativas metodológicas para alcanzar el máximo desarrollo posible del alumno, en caso de que las necesidades educativas especiales se deriven de alguna discapacidad que impida la realización de determinadas
actividades de enseñanza y aprendizaje. En particular, en la enseñanza y evaluación de la Lengua Extranjera para el alumnado con dificultades de compresión y expresión oral, se establecerán instrumentos y procedimientos de evaluación que
faciliten la comunicación por medios alternativos. Asimismo, en la enseñanza y evaluación de la Educación Física para el alumnado con dificultades motoras, se establecerán las adaptaciones curriculares de aquellas actividades de enseñanza y
evaluación que el alumno no pueda realizar debido a su condición física. Cuando estas adaptaciones afecten a elementos curriculares se recogerán en la ACIS correspondiente.  
f) Flexibilización de las enseñanzas. Sin menoscabo de que el alumno se hubiera
acogido o no a la prórroga de permanencia establecida en el artículo 22.6 del Decreto 65/2022, de 20 de julio, la escolarización de este alumnado en la Educación Secundaria Obligatoria en centros ordinarios podrá prorrogarse un año adicional más, siempre que ello favorezca la obtención del título de Graduado en Educación Secundaria Obligatoria y no se haya aplicado esta medida en la Educación Primaria.  
>4. La dirección general competente en materia de Ordenación Académica de Educación Secundaria Obligatoria establecerá los modelos de documentación que podrán utilizar los centros para el registro de las medidas adoptadas.

[ORDEN 130/2023, de 23 de enero, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan aspectos de organización y funcionamiento, evaluación y autonomía pedagógica en la etapa de Educación Primaria en la Comunidad de Madrid.](https://gestiona.comunidad.madrid/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=13091)  

### Plan incluYO Madrid 
[Orienta 16. Boletines del Consejo Escolar de la Comunidad de Madrid. Plan incluYO](https://www.madrid.org/bvirtual/BVCM050604-16.pdf)  

[Atención a las diferencias individuales del alumnado - comunidad.madrid](https://www.comunidad.madrid/servicios/educacion/atencion-diferencias-individuales-alumnado)

[ORIENTACIONES DE LA VICECONSEJERÍA DE POLÍTICA EDUCATIVA PARA EL DISEÑO, ELABORACIÓN Y APLICACIÓN DEL PLAN INCLUYO, 4 diciembre 2023](https://www.comunidad.madrid/sites/default/files/doc/educacion/392-orientaciones_plan_incluyo.pdf)  

[Anexos orientaciones 2024 plan incluYO](https://www.comunidad.madrid/sites/default/files/doc/educacion/anexos_orientaciones_2024_plan_incluyo.pdf)  

[Instrucciones de la Dirección General de Educación Secundaria, Formación Profesional y Régimen Especial por las que se concretan las orientaciones para la elaboración del Plan de atención a las diferencias individuales del alumnado (Plan IncluYO) de los centros que imparten Educación Secundaria Obligatoria y Bachillerato y se establecen los modelos para
el registro de las medidas ordinarias y específicas adoptadas para el alumnado del centro. 22 enero 2024](https://www.comunidad.madrid/sites/default/files/doc/educacion/2024-01-22_instrucciones_modelos_docs_atencion_a_la_diversidad.pdf)
Incluye plantillas de anexos II.a, II.b, II.c, II.d, III.a, III.b, ...

## Recursos generales sobre programaciones
 Las adaptaciones curriculares son un caso concreto de programación, y hay que aplicar ciertas ideas básicas:  
 
 Redactar criterios de evaluación:  
 [3_criterios_de_evalucin.html](http://aularagon.catedu.es/materialesaularagon2013/formacion_lomce/bloque_1/Modulo_1_1/3_criterios_de_evalucin.html)  
 *Los criterios de evaluación incluyen implícitamente: los procesos mentales expresados con verbos en infinitivo + los contenidos + los contextos reales, simulados o virtuales.*  
 
 Redactar estándares de aprendizaje evaluables / resultados de aprendizaje  
 [Resultados_aprendizaje.pdf](http://www.cnde.es/cms_files/Resultados_aprendizaje.pdf)  
>Un resultado de aprendizaje bien escrito debe contener los siguientes componentes:  
>1. Un verbo que indique lo que el estudiante se espera que sea capaz de realizar al finalizar el período de aprendizaje.  
>2. Una palabra/s que indiquen sobre qué o con qué el estudiante actúa. Si el resultado es sobre habilidades entonces la palabra o palabras deberían describir el modo en el que la habilidad se ejecuta (p.ej.: saltar eficazmente arriba y abajo).  
>3. Una palabra/s que indique la naturaleza (en contexto o en términos de estándar) de la ejecución requerida como evidencia de que el aprendizaje se ha logrado.

## Recursos sobre concepto y planteamiento de adaptaciones curriculares
 Fundación Cantabria Ayuda al Déficit de Atención e Hiperactividad Tipos de adaptaciones curriculares individualizadas (A.C.I.)  
 [Tipos de adaptaciones curriculares individualizadas (A.C.I.)](http://www.fundacioncadah.org/web/articulo/tipos-de-adaptaciones-curriculares-individualizadas.html)  
>Para la puesta en práctica de este tipo de adaptación se debe redactar un documento escrito que contendrá al menos 4 apartados:
>   1. Datos personales y escolares del alumno.
>   2. Informe o valoración de la competencia curricular del alumno.
>   3. Delimitación de las necesidades educativas especiales del alumno.
>   4. Determinación del tipo de currículo adaptado que seguirá el alumno con sus respectivas modificaciones (objetivos, contenidos, metodología, actividades y criterios de evaluación) así como el seguimiento de la adaptación curricular. Se concretarán, además, los recursos humanos y materiales necesarios.  

[▷ Blog de Pedagogía Infantil y Primaria](http://www.mundoprimaria.com/pedagogia-primaria/adaptaciones-curriculares.html)  
Una idea para dislexia es usar otras fuentes  
Comic Sans  [Contra haters y snobs: una defensa de la Comic Sans](https://www.xataka.com/otros/por-que-deje-de-odiar-la-comic-sans)   
OpenDislexic  [Home | OpenDyslexic](https://opendyslexic.org/) 

## Recursos didáctica Física y Química en Primaria
 Enlaza con la idea de mirar el currículo asociable a "Física y Química" de niveles previos a 2º ESO.  
 Algunos enlazan con  [prácticas / experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio)  (ver apartado Prácticas asociadas a primaria)  
 
Manuales docentes de Educación Primaria  
Didáctica de la Física y Química Emigdia Repetto Jiménez  
Mª Carmen Mato Carrodeguas  
ISBN13: 978-84-96502-78-9  
Depósito Legal: GC 721-2007  
[2909.pdf](http://acceda.ulpgc.es/bitstream/10553/1268/1/2909.pdf)  
[Cinemática. Física y Química para 3er ciclo de Primaria - Didactalia: material educativo](https://didactalia.net/comunidad/materialeducativo/recurso/cinematica-fisica-y-quimica-para-3-ciclo-de/adb6a008-6f5e-4cde-98de-b954ea267333) 

## Recursos generales asociados a pruebas oficiales
Los cito aqúi por su posible utilidad como material de adaptaciones pero hay cosas que pueden ser más generales    

Se pueden considerar asociados, aunque dependerá del nivel a adaptar y del desfase curricular:  
- materiales de [Pruebas acceso Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio)  
- materiales de [Prueba libre graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)  
- materiales de [Pruebas de evaluaciones nacionales](/home/pruebas/pruebas-evaluaciones-nacionales) relativas a ciencias
- materiales de [Pruebas de evaluaciones internacionales](/home/pruebas/pruebas-evaluaciones-internacionales) relativas a ciencias  

## Currículo asociable a "Física y Química" de niveles previos a 2º ESO
En el momento de crear esta página me centré en LOMCE que era lo vigente; intento revisar para LOMLOE  
Al mirar materiales hay que tener presente que podrían ser antiguos y hacer referencia a LOE / tener detalles propios de LOE / LOMCE no de LOMLOE  

En LOE hay contenidos y criterios de evaluación, en LOMCE estándares y en LOMLOE saberes básicos y competencias específicas.   
Lo previo a 2º ESO incluye 1º ESO en secundaria y primaria.  
En primaria se habla de tres ciclos: primer (1º y 2º), segundo (3º y 4º) y tercer ciclo (5º y 6º).  

[Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975)  

[Real Decreto 157/2022, de 1 de marzo, por el que se establecen la ordenación y las enseñanzas mínimas de la Educación Primaria.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-3296)  

[Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria.](https://gestiona.comunidad.madrid/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=12793)

[Decreto 61/2022, de 13 de julio, del Consejo de Gobierno, por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la etapa de Educación Primaria.](https://gestiona.comunidad.madrid/wleg_pub/servlet/Servidor?opcion=VerHtml&nmnorma=12774)  

Me centro en niveles previos a 2º ESO: para niveles posteriores, mirar  [docencia física y química por niveles](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel)  
Pendiente revisar otras materias primaria como ciencias sociales  
Pendiente revisar si en otras materias hay cosas sobre electricidad / ley de Ohm

Se mantienen contenidos de leyes anteriores porque pueden orientar a qué se puede tratar en esos niveles; formalente no existe la materia de Física y Química y hacer una adaptación significativa de nivel primaria es algo bastante libre.  

### Biología y Geología 1º ESO (LOMLOE)
A. Proyecto científico.  
– Iniciación y características básicas de la metodología científica.  
– Formulación de preguntas, hipótesis y conjeturas científicas básicas y adecuadas a la edad
del alumnado.  
– Estrategias de utilización de herramientas digitales básicas para la búsqueda de información,
la colaboración y la comunicación de resultados e ideas en diferentes formatos (presentación,
gráfica, vídeo, póster, informe, etc.).  
– Realización de pequeños trabajos experimentales sencillos y de forma guiada para responder
a una cuestión científica determinada utilizando instrumentos y espacios necesarios
(laboratorio, aulas, entorno, etc.) de forma adecuada a su edad.  
· Obtención y selección de información a partir de datos experimentales.  
– Uso de modelos básicos para la representación y comprensión de procesos o elementos de la
naturaleza.  
– Introducción a los métodos de observación y de toma de datos de fenómenos naturales.  
– La labor científica y las personas dedicadas a la ciencia: contribución a las ciencias biológicas
y geológicas e importancia social..  

### Biología y Geología 1º ESO (LOMCE)
 Bloque 1. Habilidades, destrezas y estrategias. Metodología científica.  
 Los principales modelos sobre el origen del Universo.

### Ciencias de la Naturaleza 1º ESO (LOE)
 Propiedades generales de la materia constitutiva del Universo: definición de superficie, volumen, masa y densidad. Unidades (S.I.).  
 - Estados en los que se presenta la materia en el universo: características y relación con la temperatura. Cambios de estado. Temperatura de fusión y de ebullición de una sustancia.  
 - Reconocimiento de situaciones y realización de experiencias sencillas en las que se manifiesten las propiedades elementales de sólidos, líquidos y gases.  
 - Identificación de sustancias puras y mezclas. Homogeneidad y heterogeneidad. Concepto de disolución y de suspensión. Ejemplos de materiales de interés y su utilización en la vida cotidiana.  
 - Utilización de técnicas de separación de sustancias.  
 - Átomos y moléculas. Símbolos y fórmulas.  
 - Los elementos que forman el Universo. El hidrógeno y el helio.

### Geografía e Historia 1º ESO (LOMLOE) 
 A. Geografía física.  
– El planeta Tierra y la ubicación espacial:  
· La Tierra en el Sistema Solar.  
· La forma y los movimientos de la Tierra. Rotación y traslación.  

### Área Conocimiento del Medio Natural, Social y Cultural (LOMLOE)

Real Decreto indica que puede desdoblar en Ciencias de la Naturaleza y Ciencias Sociales.
En Madrid se desdobla.

#### Primer ciclo (Real Decreto)

A. Cultura científica.  
1\. Iniciación en la actividad científica.  
– Procedimientos de indagación adecuados a las necesidades de la investigación
(observación en el tiempo, identificación y clasificación, búsqueda de patrones...).  
– Instrumentos y dispositivos apropiados para realizar observaciones y mediciones de
acuerdo con las necesidades de las diferentes investigaciones.  
– Vocabulario científico básico relacionado con las diferentes investigaciones.  
– La curiosidad y la iniciativa en la realización de las diferentes investigaciones.  
– Las profesiones relacionadas con la ciencia y la tecnología desde una perspectiva de
género.  
– Estilos de vida sostenible e importancia del cuidado del planeta a través del
conocimiento científico presente en la vida cotidiana.  

3\. Materia, fuerzas y energía.  
– La luz y el sonido como formas de energía. Fuentes y uso en la vida cotidiana.  
– Propiedades observables de los materiales, su procedencia y su uso en objetos de la
vida cotidiana de acuerdo con las necesidades de diseño para los que fueron fabricados.  
– Las sustancias puras y las mezclas. Identificación de mezclas homogéneas y
heterogéneas. Separación de mezclas heterogéneas mediante distintos métodos.  
– Estructuras resistentes, estables y útiles.  

C. Sociedades y territorios.  
1\. Retos del mundo actual.  
– La Tierra en el universo. Elementos, movimientos y dinámicas relacionados con la
Tierra y el universo y sus consecuencias en la vida diaria y en el entorno. Secuencias
temporales y cambios estacionales.  
– La vida en la Tierra. Fenómenos atmosféricos y su repercusión en los ciclos biológicos
y en la vida diaria. Observación y registro de datos atmosféricos.  
– Retos sobre situaciones cotidianas. Funciones básicas del pensamiento espacial y
temporal para la interacción con el medio y la resolución de situaciones de la vida cotidiana.
Itinerarios y trayectos, desplazamientos y viajes.  

#### Primer ciclo Ciencias Naturaleza (Decreto Madrid)

A. Cultura científica.  
1\. Iniciación en la actividad científica.  
- Procedimientos de indagación y formulación de hipótesis adecuados a las
necesidades de la investigación (observación en el tiempo y espacio,
identificación y clasificación, búsqueda de patrones...).  
- Instrumentos y dispositivos apropiados para realizar observaciones y
mediciones, usados con seguridad y de acuerdo con las necesidades de las
diferentes investigaciones.  
- Vocabulario científico básico.  
- Curiosidad e iniciativa en la actividad científica.  
- Las profesiones relacionadas con la ciencia y la tecnología. Referentes científicos.  
- Importancia del cuidado del planeta.  

3\. Materia, fuerzas y energía.  
- La luz y el sonido como formas de energía. Fuentes y uso en la vida cotidiana.  
- Propiedades observables de los materiales (color, forma, plasticidad, dureza…),
su procedencia y su uso en objetos o situaciones de la vida cotidiana de acuerdo
con las necesidades de diseño y uso para los que fueron fabricados.  
- Identificación de algunas máquinas y aparatos de la vida cotidiana: utilidad y
funcionamiento.  
- Las sustancias puras y las mezclas. Identificación de mezclas homogéneas y
heterogéneas. Separación de mezclas heterogéneas mediante distintos
métodos.  
- Estructuras resistentes, estables y útiles.  

#### Primer ciclo Ciencias Sociales (Decreto Madrid)

C. Sociedades y territorios.  
1\. Retos del mundo actual.  
- La Tierra en el universo. Elementos, movimientos y dinámicas relacionados con la
Tierra y el universo y sus consecuencias en la vida diaria y en el entorno.
Secuencias temporales y cambios estacionales.  
...  
- Retos sobre situaciones cotidianas. Funciones básicas del pensamiento espacial y
temporal para la interacción con el medio y la resolución de situaciones de la vida
cotidiana. Itinerarios y trayectos, puntos cardinales, desplazamientos y viajes.

#### Segundo ciclo (Real Decreto)

A. Cultura científica.  
1\. Iniciación en la actividad científica.  
– Procedimientos de indagación adecuados a las necesidades de la investigación
(observación en el tiempo, identificación y clasificación, búsqueda de patrones, creación de
modelos, investigación a través de búsqueda de información, experimentos con control de
variables...).  
– Instrumentos y dispositivos apropiados para realizar observaciones y mediciones
precisas de acuerdo con las necesidades de la investigación.  
– Vocabulario científico básico relacionado con las diferentes investigaciones.
– Fomento de la curiosidad, la iniciativa y la constancia en la realización de las
diferentes investigaciones.  
– Avances en el pasado relacionados con la ciencia y la tecnología que han contribuido
a transformar nuestra sociedad mostrando modelos que incorporen una perspectiva de
género.  
– La importancia del uso de la ciencia y la tecnología para ayudar a comprender las
causas de las propias acciones, tomar decisiones razonadas y realizar tareas de forma más
eficiente.  

3\. Materia, fuerzas y energía.  
– El calor. Cambios de estado, materiales conductores y aislantes, instrumentos de
medición y aplicaciones en la vida cotidiana.  
– Los cambios reversibles e irreversibles que experimenta la materia desde un estado
inicial a uno final identificando los procesos y transformaciones que experimenta en
situaciones de la vida cotidiana.  
– Fuerzas de contacto y a distancia. Las fuerzas y sus efectos.
– Propiedades de las máquinas simples y su efecto sobre las fuerzas. Aplicaciones y
usos en la vida cotidiana.  

C. Sociedades y territorios.  
1\. Retos del mundo actual.  
– La Tierra y las catástrofes naturales. Elementos, movimientos, dinámicas que ocurren
en el universo y su relación con fenómenos físicos que afectan a la Tierra y repercuten en la
vida diaria y en el entorno.  
– Conocimiento del espacio. Representación del espacio. Representación de la Tierra a
través del globo terráqueo, los mapas y otros recursos digitales. Mapas y planos en distintas
escalas. Técnicas de orientación mediante la observación de los elementos del medio físico
y otros medios de localización espacial.  

#### Segundo ciclo Ciencias de la Naturaleza (Decreto Madrid)

A. Cultura científica.  
1\. Iniciación en la actividad científica.  
- Procedimientos de indagación y formulación de hipótesis adecuados a las
necesidades de la investigación (observación en el tiempo y espacio, identificación
y clasificación, búsqueda de patrones, creación de modelos, investigación a través
de búsqueda de información, experimentos con control de variables...).  
- Instrumentos y dispositivos apropiados para realizar observaciones y mediciones
precisas, usados con seguridad, de acuerdo con las necesidades de la investigación.
- Vocabulario científico básico y adecuado a su edad, de tipo técnico y aplicado,
relacionado con las diferentes investigaciones.  
- Fomento de la curiosidad, la iniciativa y la constancia en la realización de las
diferentes investigaciones.  
- El ensayo y error como parte de los inicios de la actividad científica.
- Avances en el pasado relacionados con la ciencia y la tecnología que han
contribuido a transformar nuestra sociedad mostrando modelos que incorporen
la igualdad entre hombres y mujeres.  
- La importancia del uso de la ciencia y la tecnología para ayudar a comprender
las causas de las propias acciones, tomar decisiones razonadas y realizar tareas
de forma más eficiente.  

3\. Materia, fuerzas y energía.  
- El calor y la temperatura. Cambios de estado, efectos del calor sobre diferentes
materiales, materiales conductores y aislantes, instrumentos de medición y
aplicaciones en la vida cotidiana.  
- Los cambios reversibles e irreversibles que experimenta la materia desde un
estado inicial a uno final identificando los procesos y transformaciones que
experimenta en situaciones de la vida cotidiana.  
- Fuerzas de contacto y a distancia. Las fuerzas y sus efectos.  
- Herramientas, máquinas e instrumentos. Propiedades de las máquinas simples
y su efecto sobre las fuerzas. Aplicaciones y usos en la vida cotidiana.
Diferencias entre las máquinas simples y las compuestas. Importantes
descubrimientos e inventos.  
- Las sustancias puras y las mezclas. Tipos de mezclas. Separación de las
mezclas homogéneas mediante distintos métodos.

#### Segundo ciclo Ciencias Sociales (Decreto Madrid)

C. Sociedades y territorios.  
1\. Retos del mundo actual.  
- La Tierra y las catástrofes naturales. Elementos, movimientos, dinámicas que
ocurren en el universo y su relación con fenómenos físicos que afectan a la Tierra
y repercuten en la vida diaria y en el entorno.  
- Conocimiento del espacio. Representación del espacio. Representación de la
Tierra a través del globo terráqueo, los mapas y otros recursos digitales. Mapas y
planos en distintas escalas. Técnicas de orientación mediante la observación de
los elementos del medio físico y otros medios de localización espacial.

#### Tercer ciclo (Real Decreto)

A. Cultura científica.  
1\. Iniciación en la actividad científica.  
– Fases de la investigación científica (observación, formulación de preguntas y
predicciones, planificación y realización de experimentos, recogida y análisis de información
y datos, comunicación de resultados...).  
– Instrumentos y dispositivos apropiados para realizar observaciones y mediciones
precisas de acuerdo con las necesidades de la investigación.  
– Vocabulario científico básico relacionado con las diferentes investigaciones.  
– Fomento de la curiosidad, la iniciativa, la constancia y el sentido de la responsabilidad
en la realización de las diferentes investigaciones.  
– La ciencia, la tecnología y la ingeniería como actividades humanas. Las profesiones
STEM en la actualidad desde una perspectiva de género.  
– La relación entre los avances en matemáticas, ciencia, ingeniería y tecnología para
comprender la evolución de la sociedad en el ámbito científico-tecnológico.  

3\. Materia, fuerzas y energía.  
– Masa y volumen. Instrumentos para calcular la masa y la capacidad de un objeto.
Concepto de densidad y su relación con la flotabilidad de un objeto en un líquido.
– La energía eléctrica. Fuentes, transformaciones, transferencia y uso en la vida
cotidiana. Los circuitos eléctricos y las estructuras robotizadas.  
– Las formas de energía, las fuentes y las transformaciones. Las fuentes de energías
renovables y no renovables y su influencia en la contribución al desarrollo sostenible de la
sociedad.  
– Artefactos voladores. Principios básicos del vuelo.  

C. Sociedades y territorios.  
1\. Retos del mundo actual.  
– El futuro de la Tierra y del universo. Los fenómenos físicos relacionados con la Tierra y
el universo y su repercusión en la vida diaria y en el entorno. La exploración espacial y la
observación del cielo; la contaminación lumínica.  

#### Tercer ciclo Ciencias de la Naturaleza (Decreto Madrid)

A. Cultura científica.  
1\. Iniciación en la actividad científica.  
- Fases de la investigación científica (observación sistemática, formulación de
preguntas, hipótesis y predicciones, planificación y realización de experimentos
y modelos, control de variables y muestras, recogida y análisis de información y
datos, comunicación y presentación de resultados...).  
- Instrumentos y dispositivos apropiados para realizar observaciones y mediciones
precisas, usados en condiciones de seguridad, de acuerdo con las necesidades
de la investigación.  
- Vocabulario científico, técnico y aplicado básico, adecuado a su edad,
relacionado con las diferentes investigaciones.  
- Fomento de la curiosidad, la iniciativa, la constancia y el sentido de la
responsabilidad en la realización de las diferentes investigaciones.  
- El ensayo y error en el método científico.  
- La ciencia, la tecnología y la ingeniería. Profesiones actuales relacionadas.  
- La relación entre los avances en matemáticas, ciencia, ingeniería y tecnología
para comprender la evolución de la sociedad en el ámbito científico-tecnológico.  

3\. Materia, fuerzas y energía.  
- Propiedades de la materia: generales (masa, volumen…) y específicas (color,
dureza, densidad…).  
- Masa y volumen. Instrumentos para calcular la masa y la capacidad de un objeto.
Concepto de densidad y su relación con la flotabilidad de un objeto en un líquido.  
- La energía eléctrica. Fuentes, transformaciones, transferencia y uso en la vida
cotidiana. Los circuitos eléctricos y las estructuras robotizadas.  
- Las formas de energía, las fuentes y las transformaciones. Las fuentes de
energías renovables y no renovables y su influencia en la contribución al
desarrollo de la sociedad.  
- Artefactos voladores. Principios básicos del vuelo.  
- Artefactos marinos. Principios básicos de flotabilidad e inmersión.  
- Artefactos terrestres. Principios básicos del movimiento a través del rozamiento
y de la rodadura.  

#### Tercer ciclo Ciencias Sociales (Decreto Madrid)

C. Sociedades y territorios.  
1\. Retos del mundo actual.  
- El futuro de la Tierra y del universo. Los fenómenos físicos relacionados con la
Tierra y el universo y su repercusión en la vida diaria y en el entorno. La exploración
espacial y la observación del cielo; la contaminación lumínica.  

### Ciencias de la Naturaleza Primaria (LOMCE)
 [Real Decreto 126/2014, de 28 de febrero, por el que se establece el currículo básico de la Educación Primaria.](https://www.boe.es/buscar/act.php?id=BOE-A-2014-2222#ani)  
 Bloque 1. Iniciación a la actividad científica  
 Bloque 4. Materia y energía  
 Bloque 5. La tecnología, objetos y máquinas

#### Ciencias Sociales 1º Primaria (LOMCE)

Primer curso  
Geografía. El mundo en que vivimos  
El Universo y el Sistema Solar. La Tierra y la Luna.  
1\. Identifica el Sol como el centro del Sistema Solar.  
2\. Describe los movimientos de la Tierra y de la Luna.  
3\. Conoce la equivalencia entre las distintas unidades para medir el tiempo: año, mes, semana, día y hora.  
...  
11\. Explica los problemas de la contaminación del agua, las características del agua potable y la necesidad de un consumo responsable

#### Ciencias de la Naturaleza 2º Primaria (LOMCE)

Materia y energía. Tecnología, objetos y máquinas  
Características de los materiales.  
11\. Observa algunos materiales y describe sus características según su color, forma, plasticidad, dureza, etcétera.  
Utilidad de algunos avances, productos y materiales para el progreso de la sociedad.  
Máquinas y aparatos. Inventos y descubrimientos importantes para la vida del hombre.  
12\. Identifica algunas máquinas y aparatos de la vida cotidiana y explica su utilidad y funcionamiento.  
13\. Construye algún aparato sencillo y explica su utilidad.  
14\. Explica los cambios que, inventos y descubrimientos como el fuego, la rueda o el arado, introdujeron en la forma de vida del hombre.

#### Ciencias Sociales 2º Primaria (LOMCE)

Segundo curso Geografía. El mundo en que vivimos  
El Sistema Solar. La Tierra. Representación de la Tierra. Planisferio, mapas y planos.  
1\. Conoce los nombres de los planetas y su situación con respecto al Sol.  
2\. Describe los movimientos de los planetas del Sistema Solar, especialmente de la Tierra y su satélite, la Luna.

#### Ciencias de la Naturaleza 3º Primaria (LOMCE)

Materia y energía. Tecnología, objetos y máquinas  
Estados de la materia.  
14\. Observa las propiedades de sólidos, líquidos y gases.  
15\. Identifica el agua en los tres estados.  
La energía. La electricidad.  
16\. Explica el efecto del calor sobre diferentes materiales.  
17\. Distingue conductores y aislantes.Sustancias puras y mezclas.  
18\. Realiza algunas mezclas y explica sus características.  
Máquinas y aparatos en la vida cotidiana. Importantes inventos y descubrimientos.  
19\. Describe alguna máquina y aparato de la vida cotidiana explicando sus componentes, funcionamiento y utilidad.  
20\. Identifica la importancia de la invención de la máquina de vapor y del telégrafo.

#### Ciencias de la Naturaleza 4º Primaria (LOMCE)

Materia y energía. Tecnología, objetos y máquinas  
Estudio y clasificación de algunos materiales.  
14\. Observa, identifica, describe y clasifica algunos materiales por sus propiedades (dureza, solubilidad, estado de agregación y conductividad térmica).  
El peso de un cuerpo.  
15\. Utiliza diferentes procedimientos para la medida del peso de un cuerpo.  
Flotación de los cuerpos en un medio líquido.  
16\. Identifica y explica las principales características de la flotabilidad en un medio líquido.  
Cambios en el movimiento de los cuerpos por efecto de las fuerzas.  
17\. Realiza experiencias sencillas que permitan predecir cambios en el movimiento, en la forma o en el estado de los cuerpos por efecto de las fuerzas.  
Máquinas que facilitan la vida del hombre. Importantes inventos y descubrimientos.  
18\. Observa y explora la utilidad de la palanca, polea y plano inclinado.  
19\. Identifica algunos inventos de Arquímedes.  
20\. Identifica a Isaac Newton como descubridor de la gravedad.

#### Ciencias de la Naturaleza 5º Primaria (LOMCE)

Materia y energía. Tecnología, objetos y máquinas  
Diferentes formas de energía.  
8\. Identifica y explica algunas de las principales características de las diferentes formas de energía: mecánica, lumínica, sonora, eléctrica, térmica y química.  
Efectos del calor sobre los cuerpos.  
9\. Observa y explica los efectos del calor en el aumento de temperatura y dilatación de algunos materiales.  
Fuentes de energía y materias primas. Energías renovables y no renovables.  
10\. Identifica y explica algunas de las principales características de las energías renovables y no renovables, identificando las diferentes fuentes de energía y materias primas y el origen del que provienen.  
Utilización de la energía. Hábitos de ahorro energético.  
11\. Identifica y explica los beneficios y riesgos relacionados con la utilización de la energía: agotamiento, lluvia ácida y radiactividad.  
La luz como fuente de energía. Electricidad: la corriente eléctrica.  
12\. Realiza experiencias diversas para estudiar las propiedades de materiales de uso común y su comportamiento ante la luz, el sonido, el calor, la humedad y la electricidad.  
13\. Observa algunos fenómenos de naturaleza eléctrica y sus efectos (luz y calor).Atracción y repulsión de cargas eléctricas.  
14\. Conoce leyes básicas que rigen algunos fenómenos: la reflexión de la luz.  
La electricidad en el desarrollo de las máquinas. Importantes inventos y descubrimientos.  
15\. Observa e identifica los elementos de un circuito eléctrico y construye uno.  
16\. Identifica algún descubrimiento de Thomas Edison.

#### Ciencias de la Naturaleza 6º Primaria (LOMCE)

Materia y energía. Tecnología, objetos y máquinas  
Métodos de separación de mezclas.  
10\. Realiza y explica el resultado de experiencias sencillas de separación de componentes de una mezcla mediante destilación, filtración, evaporación o imantación.  
Reacciones químicas: la combustión, la oxidación y la fermentación.  
11\. Expone e identifica las principales características de las reacciones químicas: la combustión, la oxidación y la fermentación.  
Electricidad y Magnetismo. El magnetismo terrestre. La brújula.  
12\. Realiza experiencias sencillas que permitan observar la relación entre la electricidad y el magnetismo.  
13\. Observa el efecto de un imán sobre diferentes materiales.  
14\. Explica la utilidad de la brújula.  
Conocimiento y utilización de las tecnologías de la información y de la comunicación.  
Tratamiento de textos. Búsqueda guiada de información en la red.  
15\. Utiliza el tratamiento de textos para realizar trabajos escritos.  
16\. Conoce y aplica estrategias de acceso y trabajo en Internet.  
17\. Utiliza con responsabilidad algunos recursos a su alcance proporcionados por las tecnologías de la información y la comunicación.

#### Contenidos comunes para toda la etapa 

Iniciación a la actividad científica. Utilización de diversas fuentes de información.  
1\. Busca, selecciona y organiza información concreta y relevante, la analiza, obtiene conclusiones, comunica su experiencia, reflexiona acerca del proceso seguido y lo comunica oralmente y por escrito.  
2\. Consulta y utiliza documentos escritos, imágenes y gráficos.  
3\. Desarrolla estrategias adecuadas para acceder a la información de textos de carácter científico.Técnicas de estudio y trabajo. Desarrollo de hábitos de trabajo. Esfuerzo y responsabilidad.  
4\. Manifiesta autonomía en la planificación y ejecución de acciones y tareas y tiene iniciativa en la toma de decisiones.  
5\. Utiliza, de manera adecuada, el vocabulario correspondiente a cada uno de los bloques de contenidos.  
6\. Expone oralmente de forma clara y ordenada contenidos relacionados con el área manifestando la compresión de textos orales y/o escritos.Utilización de las tecnologías de la información y comunicación para buscar y seleccionar información, simular procesos y presentar conclusiones.  
7\. Usa de forma autónoma el tratamiento de textos (ajuste de página, inserción de ilustraciones o notas, etcétera).  
8\. Hace un uso adecuado de las TIC como recurso de ocio.  
9\. Conoce y utiliza las medidas de protección y seguridad que debe utilizar en el uso de las TIC. Planificación y realización de proyectos y presentación de informes.  
10\. Realiza experiencias sencillas y pequeñas investigaciones: planteando problemas, enunciando hipótesis, seleccionando el material necesario, realizando, extrayendo conclusiones y comunicando los resultados.  
11\. Realiza un proyecto, trabajando de forma individual o en equipo, y presenta un informe, utilizando soporte papel y/o digital, recogiendo información de diferentes fuentes (directas, libros e Internet), con diferentes medios y comunicando de forma oral la experiencia realizada, apoyándose en imágenes y textos escritos.  
12\. Presenta trabajos de forma ordenada en soporte papel y digital de forma individual y en equipo.

## Recursos asociados a didáctica con ACNEE

[El aprendizaje de las ciencias en niños ciegos y deficientes visuales](https://www.researchgate.net/publication/28308865_El_aprendizaje_de_las_ciencias_en_ninos_ciegos_y_deficientes_visuales)  
Bermejo, M. L., Fajardo, M. I., Mellado, V. (2002). Integración, 2002, 38, 25-34

Se cita  
Sevilla, J., Ortega, J., Blanco, F., Sánchez, B. y Sánchez, C. (1990). Física general para estudiantes ciegos: método y recursos didácticos. Revista Española de Física, 4(2), 45-52.  
[twitter Joaquin_Sevilla/status/1454041120611217416](https://twitter.com/Joaquin_Sevilla/status/1454041120611217416)  
La principal conclusión es que con una enseñanza a través del tacto (con modelos físicos de conceptos con contenido geométrico) mejoraban enormemente su competencia en ese tipo de cuestiones, frente a cuestiones de contenido puramente lexico.
Pero bueno, eso apra otro día...  
![](https://pbs.twimg.com/media/FC3KpymXsAIeOZr?format=png)   

## Recursos con materiales asociados a adaptaciones
Puede haber libros de texto / cuadernillos de editoriales; ver en  [libros de texto](/home/recursos/libros/libros-de-texto)  

Aquí solamente enlazo a recursos disponibles en internet; el que alberga los recursos es responsable de revisar los derechos de autor de lo que cuelga.  
A veces pueden ser recursos asociados a cursos previos/ciertos cursos: 1º ESO, 2º ESO LOE. A veces incluso de primaria A veces se indica un nivel concreto, pero no los clasifico: hay que tener presente si los materiales se crearon asociados a cierta ley educativa y hay variaciones.  

[Temario ciencias naturales](http://www.apuntesmareaverde.org.es/grupos/cn/Temario_01.htm)  
Bloques asociados a Física y Química como:  
Tema 1.- El Universo y el Sistema Solar.  
Tema 2.- La materia en el Universo.   

[Adaptación curricular El laboratorio 2º y 3º ESO - apuntesmareaverde.org](http://www.apuntesmareaverde.org.es/grupos/fyq/2eso/AC_laboratorio.pdf)  

[NO MÁS BÚSQUEDA: UNIDADES DIDÁCTICAS  ADAPTADAS: 1º, 2º Y 3º ESO](http://nomasbusqueda.blogspot.com.es/2011/03/unidades-didacticas-adaptadas-1-2-y-3.html)  

[EL PATIO DE GEMMA: ESO CIENCIAS NATURALES](http://elpatiodegemma.blogspot.com.es/search/label/ESO%20CIENCIAS%20NATURALES)  

[Ejercicios de ciencias naturales - Mundo Primaria](http://www.mundoprimaria.com/fichas-para-imprimir/ejercicios-ciencias-naturales/)  

[Echar un rato: La materia y sus propiedades](http://florentinosm.blogspot.com.es/2013/11/la-materia-y-sus-propiedades.html)  

[ESO - Aula PT](http://www.aulapt.org/esob/)  
Incluye enlaces:  
Adaptaciones de Germán Santos González para Ciencias Naturales de 1º E.S.O.  
[MIS ARCHIVOS DE NECESIDADES EDUCATIVAS ESPECIALES](http://necesidadeseducativasespecialesarch.blogspot.com.es/)  

TEMAS ADAPTADOS por Mª Carmen García de Dionisio Aranda (IES Felx RodrÍguez de la Fuente) Sevilla (2º ESO LOE)  

TEMA 1  [La materia.](http://aulapt.files.wordpress.com/2008/11/tema1-la-materia-_2_.pdf)  
TEMA 2  [La energía.](http://aulapt.files.wordpress.com/2008/11/tema-2-la-energia.pdf)  
TEMA 3  [El movimiento y la fuerza](http://aulapt.files.wordpress.com/2008/11/tema-3-el-movimiento-y-la-fuerza.pdf)  

TEMAS ADAPTADOS por Mª Carmen García de Dionisio Aranda (IES Felix Rodríguez de la Fuente) Sevilla (3º ESO LOE)  
[El método científico](http://aulapt.files.wordpress.com/2008/09/tema-el-metodo-cientifico1.pdf)  

TEMAS ADAPTADOS POR Daniel Romano Muñoz, Valladolid (3º ESO)  
[La materia](http://aulapt.files.wordpress.com/2008/09/unidad_2_materia-ii1.pdf)  

CUADERNO DE VERANO. CIENCIAS DE LA NATURALEZA 1º ESO (23 páginas)  
[INFORME ESPECIAL: la última inversión de Dani Rovira asombra a los expertos y aterroriza a los grandes bancos](http://www.adaptacionescurriculares.com/matesocien1.pdf)   

Apuntes de FÍSICA Y QUÍMICA 3º ESO Adaptación curricular Jaime Ruiz-Mateos Garrido  
http://ciencias.com.es/images/textos/fq3esoadaptacion.pdf (no operativo en 2019)  

[35005.pdf](https://yoquieroaprobar.es/_pdf/35005.pdf)  

[ADAPTACIONES CURRICULARES | José; Aurelio Pina Romero.](http://www.pinae.es/adaptaciones-curriculares/)  
JOSE AURELIO PINA ROMERO  

Santillana, serie avanza con adaptaciones curriculares  
Muestras:  
- 2º ESO Estados de la materia  [782787.pdf](http://www.santillana.es/file/repository/782787.pdf) 
- 3º ESO La ciencia y la medida  [695239.pdf](http://www.santillana.es/file/repository/695239.pdf)  

[LIBRO DE CIENCIAS NATURALES ADAPTADO 1 - Página web de cienciasyeso](http://cienciasyeso.jimdo.com/ciencias-naturales-1%C2%BA-eso/adaptaciones-curriculares/)  

[BLOGS DE PRIMARIA](http://www.primerodecarlos.com/)  

Materiales por curso de primaria Puede haber recursos asociados a crucigramas, revisando el nivel  
[fichasparaninos&#46;com](https://www.fichasparaninos.com/category/planetas/)  

[Crucigrama de Elementos - Para Imprimir Gratis - ParaImprimirGratis.com](http://paraimprimirgratis.com/crucigrama-de-elementos)  

[Crucigramas de Elementos Químicos](http://neoparaiso.com/imprimir/crucigramas-de-elementos-quimicos.html)  

 M. a VICTORIA REYZÁBAL  
 Coordinadora RESPUESTAS EDUCATIVAS AL ALUMNADO CON ALTAS CAPACIDADES INTELECTUALES  
 Guía para elaborar el Documento Individual de Adaptaciones Curriculares de ampliación o enriquecimiento  
 ISBN: 84-451-2959-3 Depósito Legal: M-4396-2007  
 [RESPUESTAS EDUCATIVAS AL ALUMNADO CON ALTAS CAPACIDADES INTELECTUALES](http://www.madrid.org/bvirtual/BVCM001680.pdf)  
 CAPÍTULO 3  
 ORIENTACIONES PARA LA ELABORACIÓN DE LA ADAPTACIÓN CURRICULAR INDIVIDUAL  
 CAPÍTULO 4  
 EJEMPLIFICACIÓN PRÁCTICA DE ADAPTACIÓN CURRICULAR DE AMPLIACIÓN O ENRIQUECIMIENTO

### Propuestas de adaptaciones de elaboración propia
Primeros borradores para 2º ESO en 2016 anexados en la página  
 
Los ficheros se veían inicialmente anexados a esta página, ahora se pueden ver en  
[gitlab.com/fiquipedia/drive.fiquipedia recursos-adaptaciones-curriculares-fisica-quimica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/recursos-adaptaciones-curriculares-fisica-quimica) 
 
 
