
# Recursos Ciencia Ficción

Relacionado con [libros](/home/recursos/libros) y películas, y al mismo tiempo con [divulgación científica](/home/recursos/recursos-divulgacion-cientifica)

[Ciencia Ficción dura. Novelas representativas - wikipedia](https://es.wikipedia.org/wiki/Ciencia_ficci%C3%B3n_dura#Novelas_representativas)  

Cita con Rama (1973), Arthur C. Clarke.  
Huevo del dragón (1980), Robert L. Forward.  
Cronopaisaje (1980), Gregory Benford  
Contact (1985), Carl Sagan.  
Fiasco (1986), Stanisław Lem.  
Cuarentena (1992), Greg Egan.  

[twitter gisbanos/status/1736296158215950471](https://twitter.com/gisbanos/status/1736296158215950471)  
El otro día no tuve tiempo, pero aquí os dejo cinco novelas de ciencia ficción hard solo para muy nerds.  
Cronopaisaje (1980), incluso inspiró a Gregory Benford un paper para el Physical Review.  
Huevo del dragón (1988), de Robert L. Forward. ¿Vida en una estrella de neutrones? Sí, y respetando lo máximo posible las leyes de la física.  
El instante Aleph (1995), de Greg Egan. Uno de mis autores favoritos escribiendo sobre teorías del todo. Espectacular.  
Visión ciega (2006), de Peter Watts. Historia de primer contacto tan hard que alguno se ha tropezado con ella y aún no se ha recuperado.  
Y no podía faltar Seveneves (2015), de Neal Stephenson. Nuestro manual de lavadora favorito sobre mecánica orbital.  

[Best science fiction films about space, according to an astrophysicist - newscientist.com](https://www.newscientist.com/article/2384184-best-science-fiction-films-about-space-according-to-an-astrophysicist/)  

[twitter gisbanos/status/1738515880600817801](https://twitter.com/gisbanos/status/1738515880600817801)  
¿Qué es la ciencia ficción?

Todos lo tenemos bastante claro hasta... que intentamos definirla. Por eso os dejo aquí diez definiciones de ciencia ficción de algunos de los autores que la hicieron posible.

¿Con cuál estás más de acuerdo?

¡Abro hilo!
