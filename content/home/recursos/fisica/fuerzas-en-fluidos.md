---
aliases:
  - /home/recursos/fisica/recursos-fuerzas-en-fluidos/
---

# Recursos fuerzas en fluidos

Las fuerzas en fluidos es algo que se trata en Física y Química 4º ESO. La idea de  [presión](/home/recursos/presion)  y leyes de gases se trata en 3º, junto con teoría cinética, y en bachillerato.  
Algunos recursos se pueden poner por separado, intentaré enlazar las relaciones  
Por ejemplo  [recursos flotabilidad](/home/recursos/fisica/recursos-fuerzas-en-fluidos/flotabilidad)   
  
 [index_fluidos.html](http://neuro.qi.fcen.uba.ar/ricuti/No_me_salen/FLUIDOS/index_fluidos.html)   
Ricardo Cabrera. cc-by-nc-nd, ver  [licencia.html](http://neuro.qi.fcen.uba.ar/ricuti/licencia.html)   
  
Hiru.com  
 [presion-hidrostatica-el-principio-de-arquimedes](http://www.hiru.com/fisica/presion-hidrostatica-el-principio-de-arquimedes)   
cc-by-nc-sa Gobierno vasco.  
  
En 2014 incluyo unos pequeños apuntes de elaboración propia para 4º ESO  
  
 [Education | National Geographic Society](http://education.nationalgeographic.org/deepsea-challenge/?ar_a=1)   
   
[![Open siphon effect, dipping](https://img.youtube.com/vi/g4od-h7VoRk/0.jpg)](https://www.youtube.com/watch?v=g4od-h7VoRk "Open siphon effect, dipping")   

[twitter Rainmaker1973/status/1429103498709270528](https://twitter.com/Rainmaker1973/status/1429103498709270528)  
[Pythagoras cup](https://www.kiwico.com/diy/stem/motion-mechanics/pythagoras-cup)  
[Siphon (varios vasos y coloreados)](https://imgur.com/0vI8dbE), [vídeo](https://i.imgur.com/0vI8dbE.mp4)  

[Marfisik hidromecánica](https://nixmat15.wordpress.com/hidrodinamica/)  
Recopilación de recursos, simulaciones, ...


[![](https://img.youtube.com/vi/3yNoy4H2Z-o/0.jpg)](https://www.youtube.com/watch?v=3yNoy4H2Z-o "Wave tank demonstration showing the impact of coastal defences on flood risk")   

[Para los anales de la ingeniería hidráulica. Los manatíes controlan su nivel de flotación en parte a través de la acumulación y liberación de pedos. - datosfreak.org](https://www.datosfreak.org/datos/slug/manaties-regulan-flotabilidad-con-pedos/)  
![](https://www.datosfreak.org/wp-content/uploads/2022/12/manaties-regulan-flotabilidad-con-pedos.jpg)  

[Un estudio sugiere que la pirámide de Saqqara fue construida con ayuda de un ascensor hidráulico - nationalgeographic.com.es](https://historia.nationalgeographic.com.es/a/nuevo-estudio-sugiere-sistema-hidraulico-creacion-piramide-zoser-saqqara_22061)

[On the possible use of hydraulic force to assist with building the step pyramid of saqqara - plos.org](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0306690)  
![](https://journals.plos.org/plosone/article/figure/image?size=large&id=10.1371/journal.pone.0306690.g015)  


## Pascal y aplicaciones
 [Sistema de Frenos Explicados Fácilmente](http://www.areatecnologia.com/mecanismos/sistema-de-frenos.html)   
 [Funcionamiento de un ascensor hidráulico - GMV Blog - Ascensores](http://blog.gmveurolift.es/funcionamiento-de-un-ascensor-hidraulico/)   
  
 [ejercicios_resueltos_pascal.pdf](http://www.jcabello.es/documentos/docfisyqui4/ejercicios_resueltos_pascal.pdf)   
  
 [676-ejercicios-resueltos-principio-pascal-prensa-hidraulica.html](http://www.matematicasfisicaquimica.com/fisica-quimica-eso/42-fisica-y-quimica-4o-eso/676-ejercicios-resueltos-principio-pascal-prensa-hidraulica.html)   
 
[Crazy Hydraulic Press @hydraulicpress - youtube](https://www.youtube.com/@hydraulicpress)   
Vídeos de prensas hidraulicas   
[![](https://img.youtube.com/vi/zYy7hpTTBiY/0.jpg)](https://www.youtube.com/watch?v=zYy7hpTTBiY "TOP 8 BEST HYDRAULIC PRESS VIDEOS 2023")   

## Vídeos presión hidrostática

 [![10m Water Pressure on Plastic Air Bottle - Poor Knights - Free Diving - HD - YouTube](https://img.youtube.com/vi/ELltLFFK6yg/0.jpg)](https://www.youtube.com/watch?v=ELltLFFK6yg "10m Water Pressure on Plastic Air Bottle - Poor Knights - Free Diving - HD") 
 
 [![Underwater fun with plastic bottles - Free Diving - HD - YouTube](https://img.youtube.com/vi/CyqGODM_o5s/0.jpg)](https://www.youtube.com/watch?v=CyqGODM_o5s "Underwater fun with plastic bottles") 

 [![](https://img.youtube.com/vi/rChKQC6gN8E/0.jpg)](https://www.youtube.com/watch?v=rChKQC6gN8E "El frasco de Mariotte. UA - Universitat d'Alacant / Universidad de Alicante")  
Más información [Experiencias de Física: Demostraciones y Prácticas de Laboratorio. Universidad de Alicante. Departamento de Física, Ingeniería de Sistemas y Teoría de la Señal](http://rua.ua.es/dspace/handle/10045/45805)   

## Corte por agua 
[Introducción a las Máquinas de Corte con Agua](https://1804349064.blogspot.com/2017/06/introducciona-las-maquinas-de-corte-con.html)  
> El secreto de lograr un corte con agua radica en dirigir un chorro de agua a través de una boquilla de zafiro, rubí o diamante de diámetro muy pequeño (0,1 – 0,4 mm) impulsado por una bomba capaz de suministrar presiones tan elevadas como 6000 bares (600 MPa) o más, y velocidades ultrasónicas. Si a este dispositivo se añade el aporte de un abrasivo que se mezcla previamente con el agua, el poder de corte se incrementa y la variedad de aplicaciones se amplía a prácticamente cualquier material.  

 [![](https://img.youtube.com/vi/gwZe-i7HoqI/0.jpg)](https://www.youtube.com/watch?v=gwZe-i7HoqI "letterSystems/CBA Corte por agua-waterjet-Acero")  


## Tensión superficial
Insane Stone Skipping! 2014 World Record - Kurt Steiner 88 Skips [with Count Overlay]   
 [![Kurt Steiner - Insane Stone Skipping - World Record - 88 Skips [with Count Overlay] - YouTube](https://img.youtube.com/vi/S1KfuErAcj0/0.jpg)](https://www.youtube.com/watch?v=S1KfuErAcj0)  [](http://eltamiz.com/2013/07/05/mecanica-de-fluidos-i-tension-superficial/)   
Soap Film Loops   
 [![](https://img.youtube.com/vi/e0fhh1830Kc/0.jpg)](https://www.youtube.com/watch?v=e0fhh1830Kc "Soap Film Loops - YouTube") 

## Meteolorología
En 4º ESO se cita presión atmosférica y meteorología  
 [05_cnWfronts.html](http://www.mesoscale.iastate.edu/agron206/animations/05_cnWfronts.html)   
  
 [Fronts swf](http://highered.mheducation.com/olcweb/cgi/pluginpop.cgi?it=swf::430::356::/sites/dl/free/0078741831/357045/Fronts_2.swf::Fronts%20swf)   
  
 [PHSchool.com Retirement Notice - Savvas Learning Company](http://www.phschool.com/atschool/phsciexp/active_art/weather_fronts/weather_fronts.swf)   

## Simulaciones
Presión hidrostática  
 [presión hidrostática](https://labovirtual5.blogspot.com.es/search/label/presi%C3%B3n%20hidrost%C3%A1tica) Presión atmosférica [Laboratorio Virtual 5: variación de la presión atmosférica con la altura](https://labovirtual5.blogspot.com.es/search/label/variaci%C3%B3n%20de%20la%20presi%C3%B3n%20atmosf%C3%A9rica%20con%20la%20altura)   

[Bajo presión - colorado.edu](https://phet.colorado.edu/sims/html/under-pressure/latest/under-pressure_es.html) "presión, agua, atmósfera"  
![](https://phet.colorado.edu/sims/html/under-pressure/latest/under-pressure-600.png)  

[Flotabilidad: intro - colorado.edu](https://phet.colorado.edu/sims/html/buoyancy-basics/latest/buoyancy-basics_all.html?locale=es)  
![](https://phet.colorado.edu/sims/html/buoyancy-basics/latest/buoyancy-basics-900.png)  
 
[Flotabilidad - colorado.edu](https://phet.colorado.edu/sims/html/buoyancy/latest/buoyancy_all.html?locale=es) 
![](https://phet.colorado.edu/sims/html/buoyancy/latest/buoyancy-900.png)   
  
Arquímedes  
 [Laboratorio Virtual 5: Principio de Arquímedes](https://labovirtual5.blogspot.com.es/search/label/Principio%20de%20Arqu%C3%ADmedes)   
    
  
[Presión del fluido y flujo - colorado.edu](https://phet.colorado.edu/es/simulation/fluid-pressure-and-flow)   
![](https://phet.colorado.edu/sims/fluid-pressure-and-flow/fluid-pressure-and-flow-screenshot.png)  

[Gato hidráulico – simulation, animation – eduMedia](http://www.edumedia-sciences.com/es/media/442)   
Gato hidraúlico (animación de prueba)  
    
[Presión hidrostática, principio Pascal y prensa hidraúlica - unam.mx](http://objetos.unam.mx/fisica/pascal/index.html)   
  
[Laboratorio Virtual 5: variación de la presión atmosférica con la altura](https://labovirtual5.blogspot.com.es/search/label/variaci%C3%B3n%20de%20la%20presi%C3%B3n%20atmosf%C3%A9rica%20con%20la%20altura)   
  
Prensa hidráulica (necesita java)  
 [La prensa hidrulica](http://www.sc.ehu.es/sbweb/fisica/fluidos/estatica/prensa/prensa.htm)   
  
Animación prensa hidraúlica  
 [Physics Animations](http://www.mrwaynesclass.com/teacher/Fluids/hydraulics/home.html)   
 

 [Experimentando hidrostática (portugués) - mec.gob.br](http://portaldoprofessor.mec.gov.br/storage/recursos/925/index.html)  (flash)  
  
## [Dinámica de fluidos](/home/recursos/fisica/recursos-fuerzas-en-fluidos/dinamica-de-fluidos) 

