
# Recursos quimiofobia

En esta página intento poner recursos asociados a la quimiofobia (fundeu recomienda quimiofobia y no quimifobia, ver  [EL BLOG DEL BÚHO, Un alegato contra la Quimiofobia, Rectificando, que es de sabios....](http://elblogdebuhogris.blogspot.com.es/2013/04/rectificando-que-es-de-sabios.html) )  

Muy relacionado con [recuros química y alimentos](/home/recursos/quimica/alimentos), aunque también pueden estar asociada a bioquímica y conocimiento general de química en situaciones cotidianas, como presencia de compuestos en ciertos lugares o el papel de ciertos compuestos en [bioquímica](/home/recuros/quimica/bioquimica)  

Ver también [Química y biología](/home/recursos/quimica/recursos-quimica-biologia)  


[eldiario.es Quimiofobia o cómo se ha llegado al punto de rechazar grandes avances científicos como las vacunas](https://www.eldiario.es/castilla-la-mancha/de-ciencia/quimiofobia-llegado-punto-rechazar-grandes-avances-cientificos-vacunas_132_6401161.html)  

La quimiofobia además de alimentos enlaza con el conocimiento de la medicina. Los productos "naturales" también son productos químicos, y los distintos medicamentos están basados en avance del conocimiento químico.  
La anestesia es un gran logro de la medicina, y de la química  
[twitter compoundchem/status/1450479543236583432](https://twitter.com/compoundchem/status/1450479543236583432)  
It's 175 years since William Morton demonstrated the use of ether as an anaesthetic. The latest edition of #PeriodicGraphics in @cenmag
 looks at the compounds used for anaesthesia since and what we know about how they work:  
[Periodic Graphics: The chemistry of anesthetics](https://cen.acs.org/pharmaceuticals/Periodic-Graphics-chemistry-anesthetics/99/i38)  
![](https://pbs.twimg.com/media/FCEjNGsWQAs7o42?format=png)  

[What is the Moon Made Of? - Reactions Infographic - acs.org](https://www.store.acs.org/eweb/ACSTemplatePage.aspx?site=ACS_Store&WebCode=storeItemDetail&parentKey=334c11b9-103a-48c4-9ed3-025e7ef98ff2&catKey=3268039d-813d-4511-98a9-8d724bd29137)  
![](https://www.store.acs.org/eweb/images/ACSStore/402-100.jpg)  

[Chlorination & Pee in the Pool: The Chemistry of Swimming Pools - compoundchem](https://www.compoundchem.com/2015/08/12/swimming-pools/)    
![](https://i0.wp.com/www.compoundchem.com/wp-content/uploads/2023/08/The-chemistry-of-swimming-pools-2023.png)  

[twitter compoundchem/status/1686817415722635264](https://twitter.com/compoundchem/status/1686817415722635264)  
So, clearly, peeing in the pool? Not cool.  
Peeing in the sea? Science says: feel free!  
![](https://pbs.twimg.com/media/F2jG2jdWQAYBxHP?format=png)  

[Yes, It's Ok to Pee in the Sea - acs.org](https://www.acs.org/content/dam/acsorg/pressroom/reactions/infographics/its-ok-to-pee-in-the-sea.pdf)  

[Periodic Graphics: Summer hair color changes - acs.org](https://cen.acs.org/environment/Periodic-Graphics-Summer-hair-color/102/i23)  
![](https://s7d1.scene7.com/is/image/CENODS/10223-feature4-graphic?$responsive$&wid=700&qlt=90,0&resMode=sharp2)  
