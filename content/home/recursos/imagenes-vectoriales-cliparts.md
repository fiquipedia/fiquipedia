
# Recursos Imágenes (vectoriales, cliparts) 

Esta página es un poco forzada: surgió como requisito de un curso de publicar imágenes indicando su licenciamiento, al igual que una página separada sobre recursos de imágenes bitmap.  
Sí se indican desde aquí enlaces a buscadores de recursos de imágenes: a veces encontrar con el licenciamiento adecuado es complicado  
Tras años sin tocar esta página, añado algunos recursos asociados a iconos y dibujos

[Aprendiendo física con Berto Tomás, ilustraciones](https://www.aprendiendofisicaconbertotomas.com/ilustraciones)  
![](https://static.wixstatic.com/media/66bfed_fb0eff84186445c7a89c36f96148ddc2~mv2.png/v1/fill/w_980,h_577,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/66bfed_fb0eff84186445c7a89c36f96148ddc2~mv2.png)  


[thedoodlelibrary.com](https://www.thedoodlelibrary.com/) Simple, reusable drawings  
Increase comprehension, retention, and delight audiences with over 1,000 unique hand drawn SVG line art doodles.  
Free to download, share and adapt under Creative Commons Attribution 4.0 International License.  
  
También puede haber asociados recursos para dibujar material de laboratorio y montajes de prácticas, que pongo dentro de  [prácticas/experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio)   
  
Puede haber también iconos  
 [Iconos Física gratis - Flaticon](https://www.flaticon.es/resultados?word=f%C3%ADsica)   
 [Physics - Iconfinder](https://www.iconfinder.com/search/?q=physics)   
 [Free Vectors, Stock Photos & PSD Downloads | Freepik](https://www.freepik.com/)   

[bioicons](https://bioicons.com/)  
CC 0 · CC by SA · MIT License  
Tiene [repositorio GitHub](https://github.com/duerrsimon/bioicons)  
Tiene categorías como Chemistry y Lab apparatus   

Los iconos enlazan con buscar caracteres, por lo que cito dos ideas:  

[ShapeCatcher](http://shapecatcher.com/) Permite buscar, a través de dibujo, caracteres unicode (algunos son "emojis")  
[Detextify](http://detexify.kirelabs.org/classify.html)  Permite buscar, a través de dibujo, símbolos latex  


**Pendiente crear una página de recursos programas dibujo física** similar a [recursos programas dibujo químico](/home/recursos/quimica/recursos-programas-dibujo-quimico)    

Puede haber imágenes en herramientas de dibujo 
[Chemix](https://chemix.org/)  

También se puede usar [Mathcha.io](https://www.mathcha.io/)  para dibujos física  
[twitter FIKASI1/status/1520338402842972160](https://twitter.com/FIKASI1/status/1520338402842972160)  
![](https://pbs.twimg.com/media/FRlTozdWYAE7eBi?format=jpg)  

Enlaza con [Recursos plano inclinado](/home/recursos/fisica/recursos-dinamica/recursos-plano-inclinado)  

  
Los gifs animados enlazarían con vídeos  
  
Stickman Physics Animated GalleryFind stickman physics animated GIFs for a variety of concepts in our stickman physics gallery. Look for the link below to longer videos in MP4 format. Right click and save the file so you can add them to your lessons.  
 [https://www.stickmanphysics.com/stickman-physics-animated-gallery/](https://www.stickmanphysics.com/stickman-physics-animated-gallery/)   
  
 [¿Buscas imágenes/música/audios /efectos... para tus proyectos, webs, blogs ...? | Pearltrees](http://www.pearltrees.com/rfa2009/buscas-imagenes-otros-recursos/id12895922)   
 [Imágenes Creative Commons / Copyleft / Free / Public Domain | Pearltrees](http://www.pearltrees.com/rfa2009/imagenes-creative-commons/id12896101)   
Rosa Fernández Alba  
  
Las imágenes aisladas como recursos son una excepción: lo normal es encontrarlas integradas dentro de algún recurso más elaborado.  
 Las imágenes vectoriales son preferibles para diagramas ya que no pierden calidad cuando son ampliadas. Un ejemplo de recurso donde hay muchas imágenes vectoriales de elaboración propia es en la resolución de los problemas de selectividad de óptica geométrica.  
  
 [![Campo magnético debido a una corriente rectilínea](http://etc.usf.edu/clipart/36000/36057/mag_field_36057_sm.gif "Campo magnético debido a una corriente rectilínea") ](http://etc.usf.edu/clipart/36000/36057/mag_field_36057_sm.gif) Magnetic Field Due to Current”,  [Magnetic Field Due to Current | ClipArt ETC](http://etc.usf.edu/clipart/36000/36057/mag_field_36057.htm) , Copyright: 2009, Florida Center for Instructional Technology, [consultado 22 noviembre 2011].Material bajo condiciones de licencia especiales  [License | ClipArt ETC](http://etc.usf.edu/clipart/license/license.htm)  : *Educational Use. A maximum of fifty (50) clipart items may be used in any non-commercial, educational project (report, presentation, display, website, etc.) without special permission. *  
  
 Johny_automatic ( [Openclipart - Clipping Culture](http://www.openclipart.org/user-detail/johnny_automatic) ) “Magnet”, junio 2006,  [magnet - Openclipart](http://www.openclipart.org/detail/1039/magnet-by-johnny_automatic)  , [consultado 18 octubre 2011].Material bajo  [licencia CC0 1.0 Universal Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/)   
 
