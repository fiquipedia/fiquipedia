
# Isomería

La isomería aparece en el  [currículo de física y química de 1º de Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculofisicayquimica-1bachillerato)  y en  [Química de 2º de Bachillerato](/home/materias/bachillerato/quimica-2bachillerato/curriculoquimica-2bachillerato).  

En 2013 elaboro unos  [miniapuntes (2 caras) de isomería](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/isomeria/Isomeria.pdf) , tomando información básica de wikipedia y otros libros, que anexo aquí y que iré actualizando.  

En 2018 creo unas "pizarras" de isomería para 1º Bachillerato basadas en esos apuntes, dentro de  [Apuntes Física y Química 1º Bachillerato elaboración propia](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato)   

Isomería dentro de "La Química del Carbono"  
[http://www.juntadeandalucia.es/averroes/recursos_informaticos/concurso1998/accesit8/ci.htm](http://www.juntadeandalucia.es/averroes/recursos_informaticos/concurso1998/accesit8/ci.htm)  
Abelardo López Lorente . Accésit en el Concurso de Programas Informáticos Educativos y Páginas Web Educativas, 1998  

Isomería en blog Profesor Jano.  
[http://www.profesorjano.info/2012/09/isomeria.html](http://www.profesorjano.info/2012/09/isomeria.html)  

Introducción, isomerías planas. y Estereoisomerías Licenciamiento cc-by-nc-nd.  
[http://www.natureduca.com/quim_compucar_isomeria01.php](http://www.natureduca.com/quim_compucar_isomeria01.php)  
[http://www.natureduca.com/quim_compucar_isomeria02.php](http://www.natureduca.com/quim_compucar_isomeria02.php)  

Apuntes y ejercicios isomería, muy breves. Material con todos los derechos reservados. Autor Alberto Pérez Montesdeoca.  
[http://www.matematicasfisicaquimica.com/fisica-quimica-bachillerato/45-quimica-2o-bachillerato/652-quimica-carbono-isomeria.html](http://www.matematicasfisicaquimica.com/fisica-quimica-bachillerato/45-quimica-2o-bachillerato/652-quimica-carbono-isomeria.html)  
[http://www.matematicasfisicaquimica.com/fisica-quimica-bachillerato/45-quimica-2o-bachillerato/633-ejercicios-resueltos-quimica-carbono-isomeria.html](http://www.matematicasfisicaquimica.com/fisica-quimica-bachillerato/45-quimica-2o-bachillerato/633-ejercicios-resueltos-quimica-carbono-isomeria.html)  
Nota octubre 2013: los ejercicios son resueltos, pero requiere una suscripción de pago ver las soluciones ...  

ENANTIÓMEROS Y MOLÉCULAS QUIRALES  
[http://rabfis15.uco.es/lvct/tutorial/30/informacion web/73frame1.htm](http://rabfis15.uco.es/lvct/tutorial/30/informacion%20web/73frame1.htm)  
Licenciamiento no detallado. Validado en febrero 2013.  

chemguide. Helping you to understand Chemistry  
[http://www.chemguide.co.uk/basicorg/isomermenu.html#top](http://www.chemguide.co.uk/basicorg/isomermenu.html#top) 
 © Jim Clark 2000 (modified July 2007)  
 
 OCW UNEDQUÍMICA DEL CARBONO.  
 Autora: Mª José Morcillo Ortega  
 Ficha 14 Isomería  
 [http://ocw.innova.uned.es/quimicas/pdf/qo/qo05.pdf](http://ocw.innova.uned.es/quimicas/pdf/qo/qo05.pdf)   
 © 2008 Universidad Nacional de Educación a Distancia  
 
 
Ojo; se cita "geometric" pero está desaconsejado por IUPAC (comentado en apuntes elaboración propia  
[http://goldbook.iupac.org/G02620.html](http://goldbook.iupac.org/G02620.html)  
geometric isomerism \[obsolete]  
Obsolete synonym for cis-trans isomerism. (Usage strongly discouraged).  
Source: PAC, 1996, 68, 2193 (Basic terminology of stereochemistry (IUPAC Recommendations 1996)) on page 2209  

y comentado en la propia página  
*Geometric isomerism is actually a term that is ‘strongly discouraged’ by IUPAC (the International Union of Pure & Applied Chemistry), who prefer ‘cis-trans’, or ‘E-Z’ in the specific case of alkenes. However, ‘geometric isomerism’ is still consistently used in many A Level courses to refer to both, so for that reason I’ve used that name here.*)  

[8.5: The E/Z System (when cis/trans does not work) - chem.libretexts.org](https://chem.libretexts.org/Bookshelves/Organic_Chemistry/Map%3A_Organic_Chemistry_(Wade)_Complete_and_Semesters_I_and_II/Map%3A_Organic_Chemistry_(Wade)/08%3A_Structure_and_Synthesis_of_Alkenes/8.05%3A_The_E_Z_System_(when_cis_trans_does_not_work))  
![](https://chem.libretexts.org/@api/deki/files/144011/E_2_bromobutene.png?revision=1&size=bestfit&width=344&height=169)  
> This example should convince you that cis and Z are not synonyms. Cis/trans and E,Z are determined by distinct criteria. There may seem to be a simple correspondence, but it is not a rule. Be sure to determine cis,trans or E,Z separately, as needed.  

[http://www.compoundchem.com/2014/05/22/typesofisomerism/](http://www.compoundchem.com/2014/05/22/typesofisomerism/)  
[![A Brief Guide to Types of Isomerism in Organic Chemistry, compoundchem.com](http://www.compoundchem.com/wp-content/uploads/2014/05/A-Guide-to-Types-of-Organic-Isomerism-1024x724.png "A Brief Guide to Types of Isomerism in Organic Chemistry, compoundchem.com") ](http://www.compoundchem.com/wp-content/uploads/2014/05/A-Guide-to-Types-of-Organic-Isomerism-1024x724.png)  

Una idea para buscar isómeros es usar la fórmula molecular en  
[https://webbook.nist.gov/chemistry/form-ser/](https://webbook.nist.gov/chemistry/form-ser/)   

Otra idea es usar wolframalpha  
Ejemplo  [https://www.wolframalpha.com/input/?i=isomers+of+C6H14](https://www.wolframalpha.com/input/?i=isomers+of+C6H14)  

[Organic Chemistry Memes facebook](https://es-la.facebook.com/ochemmemes)  
![](https://scontent-mad1-1.xx.fbcdn.net/v/t1.18169-9/15079034_1696380774007390_5135929597291875889_n.png?_nc_cat=106&ccb=1-7&_nc_sid=9267fe&_nc_ohc=duDqAOuIrv8AX84I6NM&_nc_ht=scontent-mad1-1.xx&oh=00_AfBjSM5Thrj0GKufOckVp6Trs6jNxjmWZXFPLj7LBcC3OA&oe=64921ED2 "Cisformers Transformers")  

[On Cats, Part 3: Newman Projections - masterorganicchemistry.com](https://www.masterorganicchemistry.com/2010/11/22/on-cats-part-3-newman-projections/)  
![](https://cdn.masterorganicchemistry.com/wp-content/uploads/2010/11/1-samedifferent-copy.jpg)  

[twitter jamesashchem/status/1275117882771415042](https://twitter.com/jamesashchem/status/1275117882771415042)  
![](https://pbs.twimg.com/media/EbIg7dJXsAEVTro?format=jpg)  


