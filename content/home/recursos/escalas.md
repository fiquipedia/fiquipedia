
# Recursos escalas
  
Asociado a la [notación científica](/home/recursos/recursos-notacion-cientifica) es interesante ver escalas entre distintas cosas, y la propia escala del universo, que enlazaría con  [recursos de astronomía](/home/recursos/recursos-astronomia)   
 Por ejemplo vídeos que permiten hacerse una idea de las escalas de las cosas y la utilidad de la notación científica.  
 
 Algunos vídeos sobre el átomo y partículas como neutrinos también son asociables a escalas.  
 
[MetaBallStudios - canal YouTube con muchas comparaciones](https://www.youtube.com/c/MetaBallStudios/videos)  
 

### Escala del universo
 [Scale of the Universe | The Universe in Perspective | Small to Big Fun!](http://scaleofuniverse.com/)   
 En 2014 agrupa 4 recursos:  
 - "The scale of the universe 2"  
- "Powers of Ten"  
- "The most astounding fact"  
- "Observable universe"  

### Powers of Ten / potencias de 10
 Mencionado en  [recursos de vídeo](/home/recursos/videos) , es muy interesante el vídeo "Powers of ten" originalmente de 1968.  

### Cosmic Zoom
 Vídeo de 1968 " [Cosmic Zoom](http://en.wikipedia.org/wiki/Cosmic_Zoom) "   

### The scale of the universe 2
 [Scale of the Universe 2](http://htwins.net/scale2/?bordercolor=white)   
 En abril 2013 actualizado en 2012. Copyright 2012 Cary and Michael Huang.  
 Hay una versión anterior de 2012  [HTwins.net - The Scale of the Universe](http://htwins.net/scale/)   
Disponible en varios idiomas  

### Universe Size Comparison 3D
 [![Universe Size Comparison 3D - YouTube](https://img.youtube.com/vi/isppUA0MZmw/0.jpg)](https://www.youtube.com/watch?v=isppUA0MZmw)   
11 mar 2017 (10 min)  
Del canal  [](https://www.youtube.com/user/reigarw)  Reigarw Comparisons  [UCcM-EIxza5g1S0dpnoO_ePA](https://www.youtube.com/channel/UCcM-EIxza5g1S0dpnoO_ePA)   
 [![Universe Size Comparison 3D - YouTube](https://img.youtube.com/vi/i93Z7zljQ7I/0.jpg)](https://www.youtube.com/watch?v=i93Z7zljQ7I) Harry Evett 10 nov 2017 (5 min)  

### Size Comparison of the Universe 2017
 Times Infinity  
 [![Size Comparison of the Universe 2017 - YouTube](https://img.youtube.com/vi/kIiJZINJFiw/0.jpg)](https://www.youtube.com/watch?v=kIiJZINJFiw)   

### If the Moon were only 1 pixel
 [If the Moon Were Only 1 Pixel - A tediously accurate map of the solar system](http://joshworth.com/dev/pixelspace/pixelspace_solarsystem.html)   

### The Solar System to scale
Plutón 1 pixel [ http://www.scalesolarsystem.66ghz.com](http://www.scalesolarsystem.66ghz.com)   

### The Solar System 1 pixel = ~1,000 km
 [http://www.troybrophy.com/projects/solarsystem/](http://www.troybrophy.com/projects/solarsystem/)   

### To Scale: The Solar System
 [![To Scale: The Solar System](https://img.youtube.com/vi/zR3Igc3Rhfg/0.jpg)](https://www.youtube.com/watch?v=zR3Igc3Rhfg "To Scale: The Solar System")   
vídeo a escala siendo la Tierra del tamaño de una canica  

### ¿Cuánto de la Tierra puedes ver a la vez?

 [![¿Cuánto de la Tierra puedes ver a la vez?](https://img.youtube.com/vi/mxhxL1LzKww/0.jpg)](https://www.youtube.com/watch?v=mxhxL1LzKww "¿Cuánto de la Tierra puedes ver a la vez?")   Ver de 44 s a 55 s escala Everest frente a la Tierra  

### Cantidad de agua en la Tierra frente a su tamaño comparada con otros objetos del Sistema Solar

[How Much Water is There on Earth? - usgs.gov](https://www.usgs.gov/special-topics/water-science-school/science/how-much-water-there-earth)  
![](https://d9-wret.s3.us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/thumbnails/image/all-the-worlds-water.jpg)  

[Comparison of the liquid water volume of Earth, Europa, and Titan to scale](http://annesastronomynews.com/photo-gallery-ii/the-solar-system/comparison-of-the-liquid-water-volume-of-earth-europa-and-titan-to-scale/)  
![](http://annesastronomynews.com/wp-content/uploads/2012/02/Comparison-of-the-liquid-water-volume-of-Earth-Europa-and-Titan-to-scale.jpg)  

[twitter aussiastronomer/status/1283258143623876608](https://twitter.com/aussiastronomer/status/1283258143623876608)  
![](https://pbs.twimg.com/media/Ec8MPdqUEAA2t3l?format=jpg)  

[Earth is a desert planet compared to these ocean worlds in the solar system](https://www.businessinsider.com.au/water-space-volume-planets-moons-2016-10)  

[All of Earth’s Water and Air, Félix Pharand-Deschênes](https://gammasbox.tumblr.com/post/68344075722/all-of-earths-water-and-air-felix)  
![](https://64.media.tumblr.com/6bf2296fea0059eead9829c796f5196d/tumblr_mwyej4xeaj1sank31o1_640.jpg)  


### Quantum to Cosmos. Perimeter Institute
 [Quantum to Cosmos](https://quantumtocosmos.ca/)  [Quantum to Cosmos](https://quantumtocosmos.ca/#/scale) 

### Microorganisms Size Comparison
 [![MICROORGANISMS Size Comparison - 3D - YouTube](https://img.youtube.com/vi/h0xTKxbIElU/0.jpg)](https://www.youtube.com/watch?v=h0xTKxbIElU) MetaBallStudios  

### Voyage into the world of atoms (CERN, 2018)
  
 [![Voyage into the world of atoms](https://img.youtube.com/vi/7WhRJV_bAiE/0.jpg)](https://www.youtube.com/watch?v=7WhRJV_bAiE "Voyage into the world of atoms")   

### Zoom en el ojo hasta el nivel atómico (Smart Biology, 2019)
  
 [twitter SmartBiology3D/status/1178325615625277440](https://twitter.com/SmartBiology3D/status/1178325615625277440) Everything we see is as manifestation of the level below. From organism, to organs, cells, macromolecules, molecules, atoms, to subatomic particles. This quick zoom in from our new Animated Textbook 'Life' allows students to appreciate this concept. #biology #animation #education  
 [W3FPiXTjGx0gyOO2.mp4](https://video.twimg.com/ext_tw_video/1178325539922350080/pu/vid/1280x720/W3FPiXTjGx0gyOO2.mp4?tag=10)   

### Prefijos SI

[![](https://img.youtube.com/vi/R6nbWVFg6kw/0.jpg)](https://www.youtube.com/watch?v=R6nbWVFg6kw "TODOS los PREFIJOS del Sistema Internacional a ESCALA. Metaball studios")  

### VFX y escala, varios
 [a-sense-of-scale-vfx-artist-shows-you-how-big-the-biggest-stars-are](https://www.syfy.com/syfywire/a-sense-of-scale-vfx-artist-shows-you-how-big-the-biggest-stars-are)   
VFX Artist Shows you how Big SpaceX Rockets Really Are! [![VFX Artist Reveals How Big SpaceX Rockets Really Are! - YouTube](https://img.youtube.com/vi/dlo3rBFDLug/0.jpg)](https://www.youtube.com/watch?v=dlo3rBFDLug)  (2018)  
VFX Artist Reveals the True Scale of the Universe  [![VFX Artist Reveals the True Scale of the Universe - YouTube](https://img.youtube.com/vi/GCTuirkcRwo/0.jpg)](https://www.youtube.com/watch?v=GCTuirkcRwo)  (2018)  

### Ballpoint Pen Super Zoom
 [Ballpoint Pen Super Zoom  &raquo;  TwistedSifter](https://twistedsifter.com/videos/ballpoint-pen-super-zoom/) Zoom into the quantum world in this fantastic CG animation by Pedro Machado.  

### Cosmic Eye
  
 [![Cosmic Eye (Original HD Version) - YouTube](https://img.youtube.com/vi/8Are9dDbW24/0.jpg)](https://www.youtube.com/watch?v=8Are9dDbW24)  
 This is the original landscape-format version of the short movie Cosmic Eye, designed by astrophysicist Danail Obreschkow. The movie zooms through all well-known scales of the universe from minuscule elementary particles out to the gigantic cosmic web. In doing so, it shows the ultimate size comparison in our universe. The video drew inspiration from a progression of increasingly accurate graphical representations of the scales of the universe, including the classical essay "Cosmic View" by Kees Boeke (1957), the short movie "Cosmic Zoom" by Eva Szasz (1968), and the legendary movie "Powers of Ten" by Charles and Ray Eames (1977). Cosmic Eye takes these earlier representations to the state-of-the-art by displaying real photographs obtained with modern detectors, telescopes, and microscopes. Other views are renderings of modern computer models. Smart vector-based blending techniques are used to create a seamless zoom.  
  
 This 2018-version of Cosmic Eye contains improved graphics and minor technical corrections compared to the 2011-version in portrait format.  

### NASA: Our Milky Way Galaxy: How Big is Space?
 [Our Milky Way Galaxy: How Big is Space? – Exoplanet Exploration: Planets Beyond our Solar System](https://exoplanets.nasa.gov/resources/2209/our-milky-way-galaxy-how-big-is-space/)   

### Zoom Into NGC 2276
This video showcases a zoom into the galaxy NGC 2276, a spiral galaxy 120 million light-years away in the constellation of Cepheus.  
 [Zoom Into NGC 2276 | ESA/Hubble](https://esahubble.org/videos/heic2106a/)   
 
### Rectangular log map-scheme of the Observable Universe

[Rectangular log map-scheme of the Observable Universe](https://www.pablocarlosbudassi.com/2021/02/atlas-of-universe-is-linear-version-of_15.html?m=1)   Pablo Carlos Budassi  

### Escala y personajes de ficción
Se pueden plantear ideas sobre escala en películas   
  
Viaje alucinante (1966)  
 [](https://www.imdb.com/title/tt0060397/)   
Cariño, he encogido a los niños (1989)  
 [](https://www.imdb.com/title/tt0097523/)   
El chip prodigioso (1987)  
 [](https://www.imdb.com/title/tt0093260/)   
  
 [Helping Marvel superheroes to breathe](https://phys.org/news/2018-11-marvel-superheroes.html) Marvel comics superheroes Ant-Man and the Wasp—nom de guerre stars of the eponymous 2018 film—possess the ability to temporarily shrink down to the size of insects, while retaining the mass and strength of their normal human bodies. But a new study suggests that, when bug-sized, Ant-Man and the Wasp would face serious challenges, including oxygen deprivation.  
  
También hay ciencia asociada a la física de las articulaciones si varía tamaño 
 ["Hablando de física a la salida del cine: licencias cinematográficas"; Antoni Amengual Colom; UIB, 2005 ](https://books.google.es/books?id=eWwnADbTZJ0C)  
 En capítulo "Habilidades de araña" se habla sobre variación volumen y masa vs sección y resistencia extremidades  
 [cienciaoficcion.com La ley cuadrático-cúbica, o por qué el tamaño sí importa ](http://cienciaoficcion.com/la-ley-cuadratico-cubica-o-por-que-el-tamano-si-importa/)   
 [fisica-ciencia_ficcion-superheroes-Jordi_Jose-Manuel_Moreno-UPC_0_563794465.html](https://www.eldiario.es/hojaderouter/ciencia/fisica-ciencia_ficcion-superheroes-Jordi_Jose-Manuel_Moreno-UPC_0_563794465.html) Aunque los cambios de escala sirven desde hace mucho tiempo para crear argumentos de ficción (entre otras cosas para satirizar la realidad de la época, como sucedía con los liliputieneses a los que conocía Gulliver), en la realidad es todo más complejo.  

### Escala de billón, trillón ... y número de Avogadro
Enlaza con la idea de mol y número de Avogadro(tuit cita billón como millón de millones para diferenciarlo de "billion" inglés que es mil millones) [twitter becarioenhoth/status/1025809206983749632](https://twitter.com/becarioenhoth/status/1025809206983749632) Cuando a veces oímos 'billón' (como millón de millones) no alcanzamos a notar el poder del exponencial: -Un millón de segundos son 11 días-Mil millones de segundos son 32 años-Un billón de segundos son 32.000 años, más tiempo del que tiene la civilización en la Tierra  
  
Idea: ver eso me hace plantear qué cantidad de tiempo sería un mol de segundos ... y es algo descomunalPrimero cuantos años contiene un mol de segunds: 6,022e23/(365,25*24*3600)=1,9e16 (que es un cuatrillón , y si cuesta visualizar un billón...)  
Si planteamos que el universo tiene una edad de unos 14000 millones de años, eso supone14000e6*365,25*24*3600=4,4e17 s, que no llega ni a un mol.  
Si miramos la proporción: 6,022e23/4,4e17=1,367e6  
Se puede decir que un mol de segundos es una cantidad de segundos mayor que un millón de veces los segundos que tiene de edad el universo  

### Escalas velocidades 

Animales
[twitter PhysicsVideo_/status/1030424390935166976](https://twitter.com/PhysicsVideo_/status/1030424390935166976) These are the speeds of the fastest animals on Earth, both on land and in the sky   
 
 [Flight speed - minutelabs.io](https://labs.minutelabs.io/flight-speed/#/)  

SPEED COMPARISON 3D | Fastest Man Made Objects - Red side. 8 min  
[![](https://img.youtube.com/vi/tFt9WDhWOXo/0.jpg)](https://www.youtube.com/watch?v=tFt9WDhWOXo "SPEED COMPARISON 3D | Fastest Man Made Objects - Red side, 8 min")   

Our relative velocities. Dr James O'Donoghue, 16 s  (Velocidades en el espacio)  
[![](https://img.youtube.com/vi/MY3-3nsD9GA/0.jpg)](https://www.youtube.com/watch?v=MY3-3nsD9GA "Our relative velocities. Dr James O'Donoghue, 16 s")   

Light speed journey to Mars in real time. Dr James O'Donoghue, 3 min  
[![](https://img.youtube.com/vi/-PpfrcoI_fs/0.jpg)](https://www.youtube.com/watch?v=-PpfrcoI_fs "Light speed journey to Mars in real time. Dr James O'Donoghue, 3 min")   
 


### Escalas máquinas
[![](https://img.youtube.com/vi/j8WXsF718HY/0.jpg)](https://www.youtube.com/watch?v=j8WXsF718HY "Las MÁQUINAS PESADAS Terrestres más GRANDES. Metaball Studios")  

### Escalas hongos atómicos
  
 [twitter Rainmaker1973/status/1389127590586195971](https://twitter.com/Rainmaker1973/status/1389127590586195971)   
How the atomic mushroom clouds are actually bigger than they look  [Mushroom cloud - Wikipedia](http://bit.ly/2sG6AlC)  [source of the image:  [nuclear explosion scale - Imgur](https://buff.ly/31ZGBoL) ]  
 [nuclear explosion scale - Imgur](https://imgur.com/gallery/f3qCUsh)   

### Órdenes de magnitud en wikipedia
 [Order of magnitude - Wikipedia](https://en.wikipedia.org/wiki/Order_of_magnitude)   
 [Order of magnitude - Wikipedia](https://en.wikipedia.org/wiki/Order_of_magnitude#/media/File:Orders_of_magnitude_(english_annotations).png)   
  
 [Category:Orders of magnitude - Wikipedia](https://en.wikipedia.org/wiki/Category:Orders_of_magnitude)   
 [twitter ciencia__infusa/status/1232552245142728704](https://twitter.com/ciencia__infusa/status/1232552245142728704) Viajando por un universo de órdenes de magnitud. Desde lo microscópico a lo masivo. Ver las cosas en contexto da vértigo #escala #tamaños #magnitud #universo  
 
### Escala de la biomasa

[twitter markabelan/status/1429048784600903691](https://twitter.com/markabelan/status/1429048784600903691)   
NEW PIECE: Visualizing the Biomass of Life 🐂🐬🐝🐠🦩🦨🐢🐄#sciart #biomass #scientificillustration #illustration #animals #dataviz #datavisualization #visualjournalism #biodiversity
![](https://pbs.twimg.com/media/E9T_vxFXoAElGd0?format=jpg)  

[All the Biomass of Earth, in One Graphic](https://www.visualcapitalist.com/all-the-biomass-of-earth-in-one-graphic/)  

### Emisiones CO2 por país

[![](https://img.youtube.com/vi/5ol99Eq1uWo/0.jpg)](https://www.youtube.com/watch?v=5ol99Eq1uWo "EMISIONES de CO2 por País ► (Comparativa 3D)") 4 min  

### Colocar objetos en mapas a escala  

[Evergiven everywhere](https://evergiven-everywhere.glitch.me/)  
[https://parkmyspaceship.com/](https://parkmyspaceship.com/)  
 
### Lunas de Júpiter

[![](https://img.youtube.com/vi/owpNfnLbeXU/0.jpg)](https://www.youtube.com/watch?v=owpNfnLbeXU "Comparativa de TODAS las LUNAS de JUPITER. MetaBallStudios") 3 min  

### Escala equivalencias masa - energía 

[Escala masa energía ](http://labs.minutelabs.io/Mass-Energy-Scale/)  
Understandable masses and energies and how they relate  
A scale of common, and not-so-common masses and energies to give you a sense for just how much energy is stored as mass. (maybe more than you think!)  

### Escalas procesamiento 

[twitter mrebollo/status/1533744166521524224](https://twitter.com/mrebollo/status/1533744166521524224)  
¿por qué es un hito que un superordenador llegue a la "exaescala" (exaflops) Porque es la velocidad estimada del cerebro humano. Pero por lo demás, seguimos ganando por goleada. La singularidad está lejos aún  
![](https://pbs.twimg.com/media/FUj0LaeWAAEBViC?format=jpg)  ç

### Partículas en el aire

[Zooming In: Visualizing the Relative Size of Particles - visualcapitalist.com](https://www.visualcapitalist.com/visualizing-relative-size-of-particles/)  
![](https://www.visualcapitalist.com/wp-content/uploads/2020/10/RelativeSizeofParticles-Infographic-1200px_v8.jpg)  

Relacionado con partículas PM [WHO global air quality guidelines: particulate matter (‎PM2.5 and PM10)‎, ozone, nitrogen dioxide, sulfur dioxide and carbon monoxide](https://www.who.int/publications/i/item/9789240034228)  

