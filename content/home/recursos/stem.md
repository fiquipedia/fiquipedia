# STEM

STEM son las siglas de **S**cience, **T**echnology, **E**ngineering and **M**athematics   

También se habla de STEAM y STREAM  
[STEM, STEAM AND STREAM. What Do They All Mean?](https://funacademy.fi/stem-steam-and-stream/)  
STEAM adds ‘Arts’ to the elements of STEM  
STREAM incorporates another layer to STEM and STEAM by adding ‘Reading’ into the equation  

También he escuchado ESTREAM, añadiendo una E inicial asociada a Ethics   
 
Es algo que aparece citado explícitamente en [currículo LOMLOE](/home/legislacion-educativa/lomloe/curriculo)  

Suele estar asociado a igualdad, ya que las mujeres históricamente suelen realizar menos estudios asociados a disciplinas STEM  

Se puede asociar a [proyectos de investigación](/home/recursos/proyectos-de-investigacion)  

Puede haber recursos que sean situaciones de aprendizaje, recursos específicos ... que se pueden encuadrar en STEM globalmente o en algún área concreta, solo química, solo física. A veces pueden aplicar a varios niveles y varias disciplinas y es complicado clasificarlos. Un ejemplo es ESERO 


# Definición real de STEM

A veces es simplemente hacer proyectos interdisciplinares relacionados con ciertas materias.

Esta pregunta planteada en 2019 me parece interesante
[researchgate.net Definir STEM. ¿Por qué lo llamamos STEM cuando en realidad es solo ciencia?](https://www.researchgate.net/post/Definir_STEM_Por_que_lo_llamamos_STEM_cuando_en_realidad_es_solo_ciencia)  
> STEM fue el tema principal de la conferencia internacional ASTE 2019, con al menos 8 pósteres, 27 presentaciones orales y 3 talleres que promovieron las aulas STEM, la instrucción/enseñanza STEM, las lecciones STEM, los campamentos de verano STEM, los clubes STEM y las escuelas STEM sin proporcionar una conceptualización o definición operativa de lo que es STEM. Algunas presentaciones defendían la integración de las disciplinas, pero el ejemplo proporcionado fue principalmente prácticas "indagatorias" y de "diseño de ingeniería" que de hecho no diferían del tipo de actividades en el aula hands-on/minds-off mal conceptualizadas y epistemológicamente incongruentes.  
Por lo tanto, vale la pena considerar:  
(1) ¿Por qué lo llamamos STEM si no difiere de las prácticas aplicadas durante décadas (por ejemplo, indagación, actividades hands-on)?  
(2) ¿Qué beneficios (si los hubiere) puede aportar esta mentalidad/tendencia de STEMinificación a la educación científica y su investigación?  

[«De STEM nos gusta todo menos STEM». Análisis crítico de una tendencia educativa de moda](https://ensciencias.uab.cat/article/view/v39-n1-toma-garcia/3093-pdf-es)  
ENSEÑANZA DE LAS CIENCIAS, 39-1 (2021), 65-80  
Investigaciones didácticas  
https://doi.org/10.5565/rev/ensciencias.3093  
ISSN (impreso): 0212-4521 / ISSN (digital): 2174-6486  
Radu Bogdan Toma  
Departamento de Didácticas Específicas. Universidad de Burgos, España.  
Antonio García-Carmona  
Departamento de Didáctica de las Ciencias Experimentales y Sociales. Universidad de Sevilla, España.  

> RESUMEN • En este artículo se presenta un análisis crítico sobre los desafíos y limitaciones de pro-puestas e investigaciones didácticas enmarcadas en el movimiento STEM. Se argumenta que la mayo-ría de las propuestas didácticas catalogadas como STEM son educativamente deficitarias, además de poco novedosas respecto de planteamientos anteriores para la enseñanza de las ciencias, la tecnología y las matemáticas. Se cuestiona también la viabilidad de un enfoque STEM en el contexto educativo español, y se discute sobre el uso abusivo del término, empleado a menudo como eslogan para atraer financiación,  o  hacer  propaganda  de  iniciativas  y  materiales  educativos  añosos,  rebautizados  ahora  como STEM. Se finaliza con una reflexión sobre la necesidad de un proceso de validación didáctica riguroso que oriente sobre las posibilidades y limitaciones de una enseñanza STEM.  

## Recursos STEM  

* [STEMadrid](http://educacionstem.educa.madrid.org/)  Plan diseñado por la Comunidad de Madrid para fomentar el estudio de las disciplinas STEM entre el alumnado madrileño.  
Existe una relación de centros STEM.  
* [code.intef.es ChicaSTEM](http://code.intef.es/chicastem/)  
* [intef.es Proyectos STEM en Europa (2019)](https://intef.es/Noticias/proyectos-stem-en-europa/)  
* [educacionyfp.gob.es Mujeres STEAM > Estrategias STEAM del Ministerio](https://www.educacionyfp.gob.es/mc/intercambia/mujeres-steam/estrategia-steam-mefp.html)  
* [TeachEngineering STEM Curriculum for K-12](https://www.teachengineering.org/curriculum/browse)  

## ESERO 

[ESERO (European Space Education Resource Office) España](https://esero.es/)  
Del espacio al aula  
La Oficina Europea de Recursos para la Educación Espacial en España (ESERO Spain)  
Con el lema «del espacio al aula» y aprovechando la fascinación que el alumnado siente por el espacio, tiene como objetivo principal proporcionar recursos a docentes de primaria y secundaria para mejorar su alfabetización y competencias en materias CTIM (Ciencias, Tecnología, Ingeniería y Matemáticas).  

[Proyecto STEAM: Investigación Aeroespacial aplicada al aula. - juntadeandalucia.es](https://blogsaverroes.juntadeandalucia.es/tde/proyecto-steam-investigacion-aeroespacial-aplicada-al-aula/)  

[ESERO Guías didácticas](https://esero.es/tipo-recurso/guia/)  
Por ejemplo en secundaria:  
[Aprovecha la energía del agua. Cómo producir oxígeno e hidrógeno en la Luna](https://esero.es/recurso/aprovecha-la-energia-del-agua/)  
[Aterrizaje en la Luna. Planificación y diseño de un módulo de aterrizaje](https://esero.es/recurso/aterrizaje-en-la-luna/)  
