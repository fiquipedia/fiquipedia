
# Recursos programas dibujo químico

Especialmente en orgánica, es interesante un programa para dibujar compuestos.  
En los problemas [PAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) he utilizado bkchem, por ejemplo para diagramas de Lewis.  

También hay algunos programas que son apps para móvil, ver  [apps química](/home/recursos/quimica/apps-quimica)  

Puede haber programas de dibujo químicos asociados a montajes de laboratorio, ver [Prácticas / experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio), como  [Chemix](https://chemix.org/) 
 
[BKChem, free chemical drawing program. ](http://bkchem.zirael.org/)  
BKChem está en metapaquete ubuntu science-chemistry, disponible en 22.04  

Otra opción en ubuntu 20.04 es [chemtool](http://ruby.chemie.uni-freiburg.de/~martin/chemtool/chemtool.html)    

[ACD/ChemSketch Freeware](https://www.acdlabs.com/resources/freeware/chemsketch/)  
ACD/ChemSketch Freeware is a drawing package that allows you to draw chemical structures including organics, organometallics, polymers, and Markush structures.  
En 2022 este es el [enlace directo descarga](https://acdusdownload.s3.amazonaws.com/ACDLabs202121_ChemSketchFree_Install.zip) y la limitación es que no puede nombrar compuestos de más de 50 átomos (se incluyen los hidrógenos aunque en esqueletal no se representen)  

Se puede dibujar a partir de SMILES  
Para dibujar "semidesarrollada", se puede seleccionar Tools > structure properties y marcar "Show carbons" All (no lo está por defecto), aunque no visto si se puede fijar "ángulos de 90º" y eliminar uniones entre -CH2-, lo que sería útil   

[MarvinSketch. Create and Design - chemaxon.com](https://chemaxon.com/products/marvin)  

[KingDraw](http://www.kingdraw.cn/en/index.html)  
Professional chemical structure editor.  
KingDraw App is a free chemical drawing editor that allows users to sketch molecules and reactions as well as organic chemistry objects and pathways. Users can also use it to analyze compound property, convert chemical structures to IUPAC names, view 3D models, etc. KingDraw will provide strong software support for chemical research, including more chemical related functions and new structure drawing modes to connect Android&iOS device and PC, realizing rapid transforming from KingDraw to Office, ChemDraw and picture. And finally you will build an all-platform chemical structure system through your KingDraw cloud account.   

Tiene una colección de compuestos que es KDpedia (KingDrawPedia)  
[KingDraw Tutorial-KDpedia on Phones](https://kingdraw.medium.com/kingdraw-tutorial-kdpedia-on-phones-f81011acc6f9)  
> The KDpedia is in the upper right corner of the KingDraw APP drawing board. It supports selection to query, and also tap to search.  
KDpedia contains 150 million compound data, supports detailed compound information query, and 7.5 million compound information is related to suppliers worldwide, which is convenient for query and purchase.  

  
[JChemPaint. Chemical 2D structure editor application/applet based on the Chemistry Development Kit](http://jchempaint.github.io/)  

[The JChemPaint Applet on github](http://jchempaint.github.io/EditorApplet.html)  

[Chem-space](https://chem-space.com/)  
> Chemspace is the largest online catalogue of small molecules: Building Blocks, Fragments, Screening Compounds, Reagents, and Intermediates. We also provide sourcing and procurement services.  
Ejemplo [Estructura en SMILES CC(=C)CC(C)(C)C](https://chem-space.com/search?smiles=CC%28%3DC%29CC%28C%29%28C%29C)  

Permite [buscar compuestos](https://chem-space.com/search) por estructura y por nombre, y al dibujar se apoya en "Marvin JS by ChemAxon"  

[Marvin JS demo ChemAxon](https://marvinjs-demo.chemaxon.com/latest/demo.html)  
  
También se puede usar para dibujo químico [Mathcha.io](https://www.mathcha.io/)  


Separo aquí algunos enlaces para relacionar dibujo (estructura) y nombre. Algunos pueden combinar ambos, hacerlo solo en un sentido, y pueden estar citados antes. Separo SMILES porque no es el nombre ni una estructura, es "una notación"  

## Estructura a nombre / nombre a estructura
Lo permite ACD/ChemSketch Freeware  
Lo permite KingDraw  

[Chemical Name and Structure Conversion - ChemAxon](https://chemaxon.com/chemical-naming-and-structure-conversion)  
Es de pago, pero tiene versión gratis (con registro) en [Chemicalize](https://chemicalize.com)  

[Pestaña Name to Structure Demos > IUPAC Naming. chemdoodle.com](https://web.chemdoodle.com/demos/iupac-naming#written_tab)  

[Name To Structure - openmolecules.org](http://www.openmolecules.org/name2structure.html)  

[OPSIN: Open Parser for Systematic IUPAC nomenclature](https://opsin.ch.cam.ac.uk/)  
A partir de nombre genera dibujo y smiles  

[Chemical Name and Structure Conversion - ChemAxon](https://chemaxon.com/chemical-naming-and-structure-conversion)  
Es de pago, pero tiene versión gratis (con registro) en [Chemicalize](https://chemicalize.com)  

## SMILES a estructura / estructura a nombre
Lo permite ACD/ChemSketch Freeware  

[Online SMILES Translator and Structure File Generator](https://cactus.nci.nih.gov/translate/)  
Cita [JSME Structure Editor by P. Ertl](https://cactus.nci.nih.gov/translate/editor.html) que permite dibujar y obtiene SMILES del dibujo  


