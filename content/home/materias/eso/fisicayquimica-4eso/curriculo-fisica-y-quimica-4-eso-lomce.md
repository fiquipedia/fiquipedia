
# Currículo Física y Química 4º ESO (LOMCE)

Ver DECRETO 48/2015 en  [legislación](/home/legislacion-educativa) El decreto fija contenidos, criterios de evaluación y estándares de aprendizaje evaluables para la comunidad de Madrid.  
Esta página es estática y sin enlaces: para ver enlaces a recursos ver Contenidos currículo con enlaces a recursos: 4º ESO Física y Química (LOMCE) (pendiente)

##  Comparación para Física y Química de 4º ESO de currículo LOMCE con  [currículo LOE](/home/materias/eso/fisicayquimica-4eso/curriculofisicayquimica-4eso) 

Ver  [comparación currículo Física y Química 4º ESO](/home/materias/eso/fisicayquimica-4eso/comparacion-curriculo-fisica-y-quimica-4-eso)

##  Currículo de 4º ESO condicionado por laboratorio, TIC y simulaciones
El currículo cita a veces laboratorio y simulaciones, por lo que si no hay laboratorio (por ejemplo por falta de desdoble es habitual) o no hay posibilidad de un aula de informática para todos los alumnos, hay ciertas partes del currículo que no se pueden cubrir, y se citan aquí. Hay algunos para los que sí es posible (por ejemplo identificar material de laboratorio, o proponer un experimento).  
Una opción para tratarlos sería que los alumnos cursen  [Ampliación de Física y Química 4ºESO](/home/materias/eso/ampliacion-fisica-y-quimica-4-eso) , que obliga a grupos reducidos.  
Bloque 2. La materia  
1. Reconocer la necesidad de usar modelos para interpretar la estructura de la materia utilizando aplicaciones virtuales interactivas para su representación e identificación.  
5.3. Diseña y realiza ensayos de laboratorio que permitan deducir el tipo de enlace presente en una sustancia desconocida.  

Bloque 3. Los cambios  
2.2. Analiza el efecto de los distintos factores que afectan a la velocidad de una reacción química ya sea a través de experiencias de laboratorio o mediante aplicaciones virtuales interactivas en las que la manipulación de las distintas variables permita extraer conclusiones.  
6. Identificar ácidos y bases, conocer su comportamiento químico y medir su fortaleza utilizando indicadores y el pH-metro digital.  
7. Realizar experiencias de laboratorio en las que tengan lugar reacciones de síntesis, combustión y neutralización, interpretando los fenómenos observados.  
7.1. Diseña y describe el procedimiento de realización una volumetría de neutralización entre un ácido fuerte y una base fuertes, interpretando los resultados.  
7.2. Planifica una experiencia, y describe el procedimiento a seguir en el laboratorio, que demuestre que en las reacciones de combustión se produce dióxido de carbono mediante la detección de este gas.  

Bloque 4. El movimiento y las fuerzas  
5. Elaborar e interpretar gráficas que relacionen las variables del movimiento partiendo de experiencias de laboratorio o de aplicaciones virtuales interactivas y relacionar los resultados obtenidos con las ecuaciones matemáticas que vinculan estas variables.  
5.2. Diseña y describe experiencias realizables bien en el laboratorio o empleando aplicaciones virtuales interactivas, para determinar la variación de la posición y la velocidad de un cuerpo en función del tiempo y representa e interpreta los resultados obtenidos.  
14. Diseñar y presentar experiencias o dispositivos que ilustren el comportamiento de los fluidos y que pongan de manifiesto los conocimientos adquiridos así como la iniciativa y la imaginación.  
14.1. Comprueba experimentalmente o utilizando aplicaciones virtuales interactivas la relación entre presión hidrostática y profundidad en fenómenos como la paradoja hidrostática, el tonel de Arquímedes y el principio de los vasos comunicantes.  

Bloque 5. La energía  
4.4. Determina experimentalmente calores específicos y calores latentes de sustancias mediante un calorímetro, realizando los cálculos necesarios a partir de los datos empíricos obtenidos.  
5.2. Realiza un trabajo sobre la importancia histórica del motor de explosión y lo presenta empleando las TIC.  
6.2. Emplea simulaciones virtuales interactivas para determinar la degradación de la energía en diferentes máquinas y expone los resultados empleando las TIC.

##  Contenidos


###  Bloque 1. La actividad científica
1. La investigación científica.  
2. Magnitudes escalares y vectoriales.  
3. Magnitudes fundamentales y derivadas.  
4. Ecuación de dimensiones.  
5. Errores en la medida.  
6. Expresión de resultados.  
7. Análisis de los datos experimentales.  
8. Tecnologías de la información y la comunicación en el trabajo científico.  
9. Proyecto de investigación.

###  Bloque 2. La materia
1. Modelos atómicos.  
2. Sistema Periódico y configuración electrónica.  
3. Enlace químico: iónico, covalente y metálico.  
4. Fuerzas intermoleculares.  
5. Formulación y nomenclatura de compuestos inorgánicos según las normas IUPAC.  
6. Introducción a la química orgánica.

###  Bloque 3. Los cambios
1. Reacciones y ecuaciones químicas.  
2. Mecanismo, velocidad y energía de las reacciones.  
3. Cantidad de sustancia: el mol.  
4. Concentración molar.  
5. Cálculos estequiométricos.  
6. Reacciones de especial interés.

###  Bloque 4. El movimiento y las fuerzas
1. El movimiento.  
2. Movimientos rectilíneo uniforme, rectilíneo uniformemente acelerado y circular uniforme.  
3. Naturaleza vectorial de las fuerzas.  
4. Leyes de Newton.  
5. Fuerzas de especial interés: peso, normal, rozamiento, centrípeta.  
6. Ley de la gravitación universal.  
7. Presión.  
8. Principios de la hidrostática.  
9. Física de la atmósfera.

###  Bloque 5. La energía
1. Energías cinética y potencial.  
2. Energía mecánica.  
3. Principio de conservación.  
4. Formas de intercambio de energía: el trabajo y el calor.  
5. Trabajo y potencia.  
6. Efectos del calor sobre los cuerpos.  
7. Máquinas térmicas.

##  Criterios de evaluación y estándares de aprendizaje


###  Bloque 1. La actividad científica
1. Reconocer que la investigación en ciencia es una labor colectiva e interdisciplinar en constante evolución e influida por el contexto económico y político.  
1.1. Describe hechos históricos relevantes en los que ha sido definitiva la colaboración de científicos y científicas de diferentes áreas de conocimiento.  
1.2. Argumenta con espíritu crítico el grado de rigor científico de un artículo o una noticia, analizando el método de trabajo e identificando las características del trabajo científico.  
2. Analizar el proceso que debe seguir una hipótesis desde que se formula hasta que es aprobada por la comunidad científica.  
2.1. Distingue entre hipótesis, leyes y teorías, y explica los procesos que corroboran una hipótesis y la dotan de valor científico.  
3. Comprobar la necesidad de usar vectores para la definición de determinadas magnitudes.  
3.1. Identifica una determinada magnitud como escalar o vectorial y describe los elementos que definen a esta última.  
4. Relacionar las magnitudes fundamentales con las derivadas a través de ecuaciones de magnitudes.  
4.1. Comprueba la homogeneidad de una fórmula aplicando la ecuación de dimensiones a los dos miembros.  
5. Comprender que no es posible realizar medidas sin cometer errores y distinguir entre error absoluto y relativo.  
5.1. Calcula e interpreta el error absoluto y el error relativo de una medida conocido el valor real.  
6. Expresar el valor de una medida usando el redondeo y el número de cifras significativas correctas.  
6.1. Calcula y expresa correctamente, partiendo de un conjunto de valores resultantes de la medida de una misma magnitud, el valor de la medida, utilizando las cifras significativas adecuadas.  
7. Realizar e interpretar representaciones gráficas de procesos físicos o químicos a partir de tablas de datos y de las leyes o principios involucrados.  
7.1. Representa gráficamente los resultados obtenidos de la medida de dos magnitudes relacionadas infiriendo, en su caso, si se trata de una relación lineal, cuadrática o de proporcionalidad inversa, y deduciendo la fórmula.  
8. Elaborar y defender un proyecto de investigación, aplicando las TIC.  
8.1. Elabora y defiende un proyecto de investigación, sobre un tema de interés científico, utilizando las TIC.

###  Bloque 2. La materia
1. Reconocer la necesidad de usar modelos para interpretar la estructura de la materia utilizando aplicaciones virtuales interactivas para su representación e identificación.  
1.1. Compara los diferentes modelos atómicos propuestos a lo largo de la historia para interpretar la naturaleza íntima de la materia, interpretando las evidencias que hicieron necesaria la evolución de los mismos.  
2. Relacionar las propiedades de un elemento con su posición en la Tabla Periódica y su configuración electrónica.  
2.1. Establece la configuración electrónica de los elementos representativos a partir de su número atómico para deducir su posición en la Tabla Periódica, sus electrones de valencia y su comportamiento químico.  
2.2. Distingue entre metales, no metales, semimetales y gases nobles justificando esta clasificación en función de su configuración electrónica  
3. Agrupar por familias los elementos representativos y los elementos de transición según las recomendaciones de la IUPAC.  
3.1. Escribe el nombre y el símbolo de los elementos químicos y los sitúa en la Tabla Periódica.  
4. Interpretar los distintos tipos de enlace químico a partir de la configuración electrónica de los elementos implicados y su posición en la Tabla Periódica.  
4.1. Utiliza la regla del octeto y diagramas de Lewis para predecir la estructura y fórmula de los compuestos iónicos y covalentes.  
4.2. Interpreta la diferente información que ofrecen los subíndices de la fórmula de un compuesto según se trate de moléculas o redes cristalinas.  
5. Justificar las propiedades de una sustancia a partir de la naturaleza de su enlace químico.  
5.1. Explica las propiedades de sustancias covalentes, iónicas y metálicas en función de las interacciones entre sus átomos o moléculas.  
5.2. Explica la naturaleza del enlace metálico utilizando la teoría de los electrones libres y la relaciona con las propiedades características de los metales.  
5.3. Diseña y realiza ensayos de laboratorio que permitan deducir el tipo de enlace presente en una sustancia desconocida.  
6. Nombrar y formular compuestos inorgánicos ternarios según las normas IUPAC.  
6.1. Nombra y formula compuestos inorgánicos ternarios, siguiendo las normas de la IUPAC.  
7. Reconocer la influencia de las fuerzas intermoleculares en el estado de agregación y propiedades de sustancias de interés...  
7.1. Justifica la importancia de las fuerzas intermoleculares en sustancias de interés biológico.  
7.2. Relaciona la intensidad y el tipo de las fuerzas intermoleculares con el estado físico y los puntos de fusión y ebullición de las sustancias covalentes moleculares, interpretando gráficos o tablas que contengan los datos necesarios.  
8. Establecer las razones de la singularidad del carbono y valorar su importancia en la constitución de un elevado número de compuestos naturales y sintéticos.  
8.1. Explica los motivos por los que el carbono es el elemento que forma mayor número de compuestos.  
8.2. Analiza las distintas formas alotrópicas del carbono, relacionando la estructura con las propiedades.  
9. Identificar y representar hidrocarburos sencillos mediante las distintas fórmulas, relacionarlas con modelos moleculares físicos o generados por ordenador, y conocer algunas aplicaciones de especial interés.  
9.1. Identifica y representa hidrocarburos sencillos mediante su fórmula molecular, semidesarrollada y desarrollada.  
9.2. Deduce, a partir de modelos moleculares, las distintas fórmulas usadas en la representación de hidrocarburos.  
9.3. Describe las aplicaciones de hidrocarburos sencillos de especial interés.  
10. Reconocer los grupos funcionales presentes en moléculas de especial interés.  
10.1. Reconoce el grupo funcional y la familia orgánica a partir de la fórmula de alcoholes, aldehídos, cetonas, ácidos carboxílicos, ésteres y aminas.

###  Bloque 3. Los cambios
1. Comprender el mecanismo de una reacción química y deducir la ley de conservación de la masa a partir del concepto de la reorganización atómica que tiene lugar.  
1.1. Interpreta reacciones químicas sencillas utilizando la teoría de colisiones y deduce la ley de conservación de la masa.  
2. Razonar cómo se altera la velocidad de una reacción al modificar alguno de los factores que influyen sobre la misma, utilizando el modelo cinético-molecular y la teoría de colisiones para justificar esta predicción.  
2.1. Predice el efecto que sobre la velocidad de reacción tienen: la concentración de los reactivos, la temperatura, el grado de división de los reactivos sólidos y los catalizadores.  
2.2. Analiza el efecto de los distintos factores que afectan a la velocidad de una reacción química ya sea a través de experiencias de laboratorio o mediante aplicaciones virtuales interactivas en las que la manipulación de las distintas variables permita extraer conclusiones.  
3. Interpretar ecuaciones termoquímicas y distinguir entre reacciones endotérmicas y exotérmicas.  
3.1. Determina el carácter endotérmico o exotérmico de una reacción química analizando el signo del calor de reacción asociado.  
4. Reconocer la cantidad de sustancia como magnitud fundamental y el mol como su unidad en el Sistema Internacional de Unidades.  
4.1. Realiza cálculos que relacionen la cantidad de sustancia, la masa atómica o molecular y la constante del número de Avogadro.  
5. Realizar cálculos estequiométricos con reactivos puros suponiendo un rendimiento completo de la reacción, partiendo del ajuste de la ecuación química correspondiente.  
5.1. Interpreta los coeficientes de una ecuación química en términos de partículas, moles y, en el caso de reacciones entre gases, en términos de volúmenes.  
5.2. Resuelve problemas, realizando cálculos estequiométricos, con reactivos puros y suponiendo un rendimiento completo de la reacción, tanto si los reactivos están en estado sólido como en disolución.  
6. Identificar ácidos y bases, conocer su comportamiento químico y medir su fortaleza utilizando indicadores y el pH-metro digital.  
6.1. Utiliza la teoría de Arrhenius para describir el comportamiento químico de ácidos y bases.  
6.2. Establece el carácter ácido, básico o neutro de una disolución utilizando la escala de pH.  
7. Realizar experiencias de laboratorio en las que tengan lugar reacciones de síntesis, combustión y neutralización, interpretando los fenómenos observados.  
7.1. Diseña y describe el procedimiento de realización una volumetría de neutralización entre un ácido fuerte y una base fuertes, interpretando los resultados.  
7.2. Planifica una experiencia, y describe el procedimiento a seguir en el laboratorio, que demuestre que en las reacciones de combustión se produce dióxido de carbono mediante la detección de este gas.  
8. Valorar la importancia de las reacciones de síntesis, combustión y neutralización en procesos biológicos, aplicaciones cotidianas y en la industria, así como su repercusión medioambiental.  
8.1. Describe las reacciones de síntesis industrial del amoníaco y del ácido sulfúrico, así como los usos de estas sustancias en la industria química.  
8.2. Justifica la importancia de las reacciones de combustión en la generación de electricidad en centrales térmicas, en la automoción y en la respiración celular.  
8.3. Interpreta casos concretos de reacciones de neutralización de importancia biológica e industrial.

###  Bloque 4. El movimiento y las fuerzas
1. Justificar el carácter relativo del movimiento y la necesidad de un sistema de referencia y de vectores para describirlo adecuadamente, aplicando lo anterior a la representación de distintos tipos de desplazamiento.  
1.1. Representa la trayectoria y los vectores de posición, desplazamiento y velocidad en distintos tipos de movimiento, utilizando un sistema de referencia.  
2. Distinguir los conceptos de velocidad media y velocidad instantánea justificando su necesidad según el tipo de movimiento.  
2.1. Clasifica distintos tipos de movimientos en función de su trayectoria y su velocidad.  
2.2. Justifica la insuficiencia del valor medio de la velocidad en un estudio cualitativo del movimiento rectilíneo uniformemente acelerado (M.R.U.A), razonando el concepto de velocidad instantánea.  
3. Expresar correctamente las relaciones matemáticas que existen entre las magnitudes que definen los movimientos rectilíneos y circulares.  
3.1. Deduce las expresiones matemáticas que relacionan las distintas variables en los movimientos rectilíneo uniforme (M.R.U.), rectilíneo uniformemente acelerado (M.R.U.A.), y circular uniforme (M.C.U.), así como las relaciones entre las magnitudes lineales y angulares.  
4. Resolver problemas de movimientos rectilíneos y circulares, utilizando una representación esquemática con las magnitudes vectoriales implicadas, expresando el resultado en las unidades del Sistema Internacional.  
4.1. Resuelve problemas de movimiento rectilíneo uniforme (M.R.U.), rectilíneo uniformemente acelerado (M.R.U.A.), y circular uniforme (M.C.U.), incluyendo movimiento de graves, teniendo en cuenta valores positivos y negativos de las magnitudes, y expresando el resultado en unidades del Sistema Internacional.  
4.2. Determina tiempos y distancias de frenado de vehículos y justifica, a partir de los resultados, la importancia de mantener la distancia de seguridad en carretera.  
4.3. Argumenta la existencia de vector aceleración en todo movimiento curvilíneo y calcula su valor en el caso del movimiento circular uniforme.  
5. Elaborar e interpretar gráficas que relacionen las variables del movimiento partiendo de experiencias de laboratorio o de aplicaciones virtuales interactivas y relacionar los resultados obtenidos con las ecuaciones matemáticas que vinculan estas variables.  
5.1. Determina el valor de la velocidad y la aceleración a partir de gráficas posición-tiempo y velocidad-tiempo en movimientos rectilíneos.  
5.2. Diseña y describe experiencias realizables bien en el laboratorio o empleando aplicaciones virtuales interactivas, para determinar la variación de la posición y la velocidad de un cuerpo en función del tiempo y representa e interpreta los resultados obtenidos.  
6. Reconocer el papel de las fuerzas como causa de los cambios en la velocidad de los cuerpos y representarlas vectorialmente.  
6.1. Identifica las fuerzas implicadas en fenómenos cotidianos en los que hay cambios en la velocidad de un cuerpo.  
6.2. Representa vectorialmente el peso, la fuerza normal, la fuerza de rozamiento y la fuerza centrípeta en distintos casos de movimientos rectilíneos y circulares.  
7. Utilizar el principio fundamental de la Dinámica en la resolución de problemas en los que intervienen varias fuerzas.  
7.1. Identifica y representa las fuerzas que actúan sobre un cuerpo en movimiento tanto en un plano horizontal como inclinado, calculando la fuerza resultante y la aceleración.  
8. Aplicar las leyes de Newton para la interpretación de fenómenos cotidianos.  
8.1. Interpreta fenómenos cotidianos en términos de las leyes de Newton.  
8.2. Deduce la primera ley de Newton como consecuencia del enunciado de la segunda ley.  
8.3. Representa e interpreta las fuerzas de acción y reacción en distintas situaciones de interacción entre objetos.  
9. Valorar la relevancia histórica y científica que la ley de la gravitación universal supuso para la unificación de las mecánicas terrestre y celeste, e interpretar su expresión matemática.  
9.1. Justifica el motivo por el que las fuerzas de atracción gravitatoria solo se ponen de manifiesto para objetos muy masivos, comparando los resultados obtenidos de aplicar la ley de la gravitación universal al cálculo de fuerzas entre distintos pares de objetos.  
9.2. Obtiene la expresión de la aceleración de la gravedad a partir de la ley de la gravitación universal, relacionando las expresiones matemáticas del peso de un cuerpo y la fuerza de atracción gravitatoria.  
10. Comprender que la caída libre de los cuerpos y el movimiento orbital son dos manifestaciones de la ley de la gravitación universal.  
10.1. Razona el motivo por el que las fuerzas gravitatorias producen en algunos casos movimientos de caída libre y en otros casos movimientos orbitales.  
11. Identificar las aplicaciones prácticas de los satélites artificiales y la problemática planteada por la basura espacial que generan.  
11.1. Describe las aplicaciones de los satélites artificiales en telecomunicaciones, predicción meteorológica, posicionamiento global, astronomía y cartografía, así como los riesgos derivados de la basura espacial que generan.  
12. Reconocer que el efecto de una fuerza no solo depende de su intensidad sino también de la superficie sobre la que actúa.  
12.1. Interpreta fenómenos y aplicaciones prácticas en las que se pone de manifiesto la relación entre la superficie de aplicación de una fuerza y el efecto resultante.  
12.2. Calcula la presión ejercida por el peso de un objeto regular en distintas situaciones en las que varía la superficie en la que se apoya, comparando los resultados y extrayendo conclusiones.  
13. Interpretar fenómenos naturales y aplicaciones tecnológicas en relación con los principios de la hidrostática, y resolver problemas aplicando las expresiones matemáticas de los mismos.  
13.1. Justifica razonadamente fenómenos en los que se ponga de manifiesto la relación entre la presión y la profundidad en el seno de la hidrosfera y la atmósfera.  
13.2. Explica el abastecimiento de agua potable, el diseño de una presa y las aplicaciones del sifón utilizando el principio fundamental de la hidrostática.  
13.3. Resuelve problemas relacionados con la presión en el interior de un fluido aplicando el principio fundamental de la hidrostática.  
13.4. Analiza aplicaciones prácticas basadas en el principio de Pascal, como la prensa hidráulica, elevador, dirección y frenos hidráulicos, aplicando la expresión matemática de este principio a la resolución de problemas en contextos prácticos.  
13.5. Predice la mayor o menor flotabilidad de objetos utilizando la expresión matemática del principio de Arquímedes.  
14. Diseñar y presentar experiencias o dispositivos que ilustren el comportamiento de los fluidos y que pongan de manifiesto los conocimientos adquiridos así como la iniciativa y la imaginación.  
14.1. Comprueba experimentalmente o utilizando aplicaciones virtuales interactivas la relación entre presión hidrostática y profundidad en fenómenos como la paradoja hidrostática, el tonel de Arquímedes y el principio de los vasos comunicantes.  
14.2. Interpreta el papel de la presión atmosférica en experiencias como el experimento de Torricelli, los hemisferios de Magdeburgo, recipientes invertidos donde no se derrama el contenido, etc. infiriendo su elevado valor.  
14.3. Describe el funcionamiento básico de barómetros y manómetros justificando su utilidad en diversas aplicaciones prácticas.  
15. Aplicar los conocimientos sobre la presión atmosférica a la descripción de fenómenos meteorológicos y a la interpretación de mapas del tiempo, reconociendo términos y símbolos específicos de la meteorología.  
15.1. Relaciona los fenómenos atmosféricos del viento y la formación de frentes con la diferencia de presiones atmosféricas entre distintas zonas.  
15.2. Interpreta los mapas de isobaras que se muestran en el pronóstico del tiempo indicando el significado de la simbología y los datos que aparecen en los mismos.

###  Bloque 5. La energía
1. Analizar las transformaciones entre energía cinética y energía potencial, aplicando el principio de conservación de la energía mecánica cuando se desprecia la fuerza de rozamiento, y el principio general de conservación de la energía cuando existe disipación de la misma debida al rozamiento.  
1.1. Resuelve problemas de transformaciones entre energía cinética y potencial gravitatoria, aplicando el principio de conservación de la energía mecánica.  
1.2. Determina la energía disipada en forma de calor en situaciones donde disminuye la energía mecánica.  
2. Reconocer que el calor y el trabajo son dos formas de transferencia de energía, identificando las situaciones en las que se producen.  
2.1. Identifica el calor y el trabajo como formas de intercambio de energía, distinguiendo las acepciones coloquiales de estos términos del significado científico de los mismos.  
2.2. Reconoce en qué condiciones un sistema intercambia energía. en forma de calor o en forma de trabajo  
3. Relacionar los conceptos de trabajo y potencia en la resolución de problemas, expresando los resultados en unidades del Sistema Internacional así como otras de uso común.  
3.1. Halla el trabajo y la potencia asociados a una fuerza, incluyendo situaciones en las que la fuerza forma un ángulo distinto de cero con el desplazamiento, expresando el resultado en las unidades del Sistema Internacional u otras de uso común como la caloría, el kWh y el CV.  
4. Relacionar cualitativa y cuantitativamente el calor con los efectos que produce en los cuerpos: variación de temperatura, cambios de estado y dilatación.  
4.1. Describe las transformaciones que experimenta un cuerpo al ganar o perder energía, determinando el calor necesario para que se produzca una variación de temperatura dada y para un cambio de estado, representando gráficamente dichas transformaciones.  
4.2. Calcula la energía transferida entre cuerpos a distinta temperatura y el valor de la temperatura final aplicando el concepto de equilibrio térmico.  
4.3. Relaciona la variación de la longitud de un objeto con la variación de su temperatura utilizando el coeficiente de dilatación lineal correspondiente.  
4.4. Determina experimentalmente calores específicos y calores latentes de sustancias mediante un calorímetro, realizando los cálculos necesarios a partir de los datos empíricos obtenidos.  
5. Valorar la relevancia histórica de las máquinas térmicas como desencadenantes de la revolución industrial, así como su importancia actual en la industria y el transporte.  
5.1. Explica o interpreta, mediante o a partir de ilustraciones, el fundamento del funcionamiento del motor de explosión.  
5.2. Realiza un trabajo sobre la importancia histórica del motor de explosión y lo presenta empleando las TIC.  
6. Comprender la limitación que el fenómeno de la degradación de la energía supone para la optimización de los procesos de obtención de energía útil en las máquinas térmicas, y el reto tecnológico que supone la mejora del rendimiento de estas para la investigación, la innovación y la empresa.  
6.1. Utiliza el concepto de la degradación de la energía para relacionar la energía absorbida y el trabajo realizado por una máquina térmica.  
6.2. Emplea simulaciones virtuales interactivas para determinar la degradación de la energía en diferentes máquinas y expone los resultados empleando las TIC.  

