
# Recursos reacciones y cálculos estequiométricos

Reacciones se puede asociar al concepto básico de [cambios químicos](/home/recursos/quimica/recursos-cambios-quimicos) frente a cambios físicos que se ve en primaria y primeros curso de ESO  
También asociado a  [recursos ajuste reacciones químicas](/home/recursos/quimica/recursos-ajuste-reacciones-quimicas)  

Algunos de los recursos de ajustes también permiten "completar" reacciones (al indicar alguno de los reactivos / productos) y pueden servier para ver ejemplos de reacciones.  
Recursos sobre reacciones puede haber en más páginas, por ejemplo asociadas a [seguridad laboratorio](/home/recursos/quimica/seguridad-laboratorio)  

[Ecuaciones químicas online](https://chemequations.com/es/)  
[Ecuaciones químicas online búsqueda avanzada](https://chemequations.com/es/busqueda-avanzada/)  

##  Cálculos estequiométricos
En ejercicios selectividad Química hay un bloque de ejercicios.  
Ejemplos de exámenes en  [FriQuiExámenes](/home/recursos/examenes/friquiexamenes)  
[twitter profedefyq/status/942754988555726848](https://twitter.com/profedefyq/status/942754988555726848)  
[twitter profedefyq/status/942755173310574599](https://twitter.com/profedefyq/status/942755173310574599)  

* [Generador ejercicios estequiometría elemental](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/estequiometria_elemental_jquery.htm?t=1466098457722) Vicente Martín Morales
* [Generador ejercicios estequiometría](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/estequiometria_jquery.htm?t=1466098473450) Vicente Martín Morales

[CÁLCULOS ESTEQUIOMÉTRICOS 1Bach - fisquiweb.es](http://fisquiweb.es/Apuntes/Apuntes1Bach/ProbEsteq_1Bach.pdf)  
24 problemas (con solución, no con resolución)   

[FQ4ESO Átomos en fórmula. Antonio González García](https://www.geogebra.org/m/dqvuhpun)  
[FQ4ESO Átomos cualquiera en fórmula. Antonio González García](https://www.geogebra.org/m/uj87yytw)  


[twitter quirogafyq/status/1222205664367345664](https://twitter.com/quirogafyq/status/1222205664367345664)  
Me gustaría compartir con vosotros una actividad que tengo pensada en relación al #HolocaustRemembranceDay "Cuando la química burló al nazismo".Sirva como homenaje a todas esas personas que lucharon, luchan y luchamos para que jamás se repita. #NuncaMas [![](https://pbs.twimg.com/media/EPYlkaeW4AErE2G?format=jpg&name=small "") ](https://pbs.twimg.com/media/EPYlkaeW4AErE2G?format=jpg&name=small)  

[![](https://pbs.twimg.com/media/EPYlkagXUAcZe7U?format=jpg&name=small "") ](https://pbs.twimg.com/media/EPYlkagXUAcZe7U?format=jpg) Aquí la actividad por si queréis descargarla.  
[https://sites.google.com/view/eldelafisicaylaquimica/bloque-3-los-cambios/cuando-la-química-burló-al-nazismo](https://sites.google.com/view/eldelafisicaylaquimica/bloque-3-los-cambios/cuando-la-qu%C3%ADmica-burl%C3%B3-al-nazismo)  
Basado en el maravilloso trabajo de @JGilMunoz [http://radicalbarbatilo.blogspot.com/2015/07/cuando-la-quimica-burlo-al-nazismo.html](http://radicalbarbatilo.blogspot.com/2015/07/cuando-la-quimica-burlo-al-nazismo.html)  

Enlazando con frikiexámenes, mención a reacciones químicas en ficción:  
Dennis & Gnasher: Desenfrenados - El concurso de pasteles de Beanotown (diciembre 2020)  
[https://www.rtve.es/alacarta/videos/dennis-gnasher-desenfrenados/concurso-pasteles-beanotown/5748920/](https://www.rtve.es/alacarta/videos/dennis-gnasher-desenfrenados/concurso-pasteles-beanotown/5748920/)  
3:06 "La cocina es básicamente una serie de reacciones químicas. En una manzana las moléculas de azúcar están en espirales adheridas.Con el calor las espirales empiezan a desenrollarse.NaHCO3 + H positivo se transforma en Na positivo + H2O + CO2.El pastel perfecto"  
[![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/media/images/ReaccionesQuímicasDennisGnasher.png "") ](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/ReaccionesQuímicasDennisGnasher.png)  
[twitter Xion_Sempai/status/1398147965211062273](https://twitter.com/Xion_Sempai/status/1398147965211062273)  
[![](https://pbs.twimg.com/media/E2c4KiSX0AEzUK4?format=jpg "") ](https://pbs.twimg.com/media/E2c4KiSX0AEzUK4?format=jpg)  

[La producción de películas delgadas de LK-99 por deposición química de vapor podría ser clave de su posible superconductividad - francis.naukas.com](https://francis.naukas.com/2023/08/14/la-produccion-de-peliculas-delgadas-de-lk-99-por-deposicion-quimica-de-vapor-podria-ser-clave-de-su-posible-superconductividad/)  
Se citan algunas reacciones   
![](https://francis.naukas.com/files/2023/08/D20230814-arxiv-2308-05776-LK-99-seven-chemical-synthesis-techniques-I-is-korean-one-II-VII-alternatives.png)  


## Reactivo limitante, pureza y rendimiento

Ver apuntes separados en pizarras
[Reactivos limitantes y rendimiento porcentual](](https://es.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/limiting-reagent-stoichiometry/a/limiting-reagents-and-percent-yield))
![](https://cdn.kastatic.org/ka-perseus-images/6f6fc12e11ff082817e1bf54e183423ecbcbcebd.png)

## Saponificación

[Calculadora de saponificación - mendrulandia.es](https://calc.mendrulandia.es/?lg=es)  
[App calculadora de saponificación mendrulandia](https://play.google.com/store/apps/details?id=com.mendru.mendrulandia)  

## Ejemplos de situaciones con reacciones  
En algunos friquiexámenes intento poner reacciones para enlazar con situaciones conocidas  

[twitter OrmazabalIon/status/1634252851185590274](https://twitter.com/OrmazabalIon/status/1634252851185590274)  
Imagina que un alumno de 1ESO se queda encerrado en su aula. Las ventanas y puertas están cerradas herméticamente y no entra aire por ningún resquicio. Teniendo en cuenta únicamente la disponibilidad de O2, ¿Cuánto tiempo podría aguantar?  
Todo hipotético. Esto NUNCA ocurriría.  
Otro alumno del mismo grupo es un zagal inquieto y te hace la siguiente pregunta: dado que las plantas liberan O2 en la fotosíntesis... ¿cuántas plantas necesitaría en el aula para poder sobrevivir en las condiciones de la primera pregunta?  
Hace unos días dos alumnos me hicieron estas preguntas en clase. Curiosas, ¿verdad? De hecho, se las plantearon porque cuando les expliqué cómo funciona la respiración externa, les expliqué que no podrían sobrevivir en una habitación cerrada herméticamente durante mucho tiempo.  
Sin embargo, y con el fin de dar una respuesta más certera al zagal (la curiosidad hay que alimentarla) propuse esta misma actividad en una clase de 1 BACH BIOGEO aprovechando que era una sesión con menos alumnos por un motivo que ahora no viene al cuento.  
Las preguntas eran las mismas de las encuestas, y las alumnas tenían la pizarra a su disposición, posibilidad de consultar Internet y ganas de currar, así que os cuento cómo fue la sesión. Les eché una mano con algún dato que no tenían.  
Entre un par de alumnas miden el aula: 8,87m de largo, 6,5 de ancho y 3m de altura. Alguna pregunta cómo se calculaba el volumen. Otra ya lo está calculando: aprox 173 m3. Espero a ver qué hacen con ese dato. Sin problema lo pasan a litros: 173 m3 = 173000dm3= 173000 litros  
"¡Ya tenemos los litros de O2!"  
"¿Seguro?..."  
"  Ah no...no todo es oxígeno..."  
Buscan los datos (saben más o menos la cantidad pero no se acuerdan exactamente).  
21 x173000/100 = 36360 l de O2.  
¿Cuánto oxígeno gasta el alumno? Buscan en Internet y lo primero que encuentran es el gasto diario medio de un adulto. No nos vale. Queremos algo un poco más específico. Finalmente lo encuentran:  
3,5ml de O2/kg/min (en reposo).  
Deciden que 50 kg es un peso aproximado y hacen los cálculos. Un niño de 50 kg necesita 252 l O2/día.  
Dividen el total de litros de oxígeno en la habitación por las necesidades diarias del zagal et... voilà! 145 días.   
"Mucho me parece..." les comento. "Tampoco va a aguantar hasta que no quede NADA de oxígeno, debería existir algún limite"  
Me pongo a buscar y encuentro un documento con recomendaciones de un consejo de seguridad de una empresa. Lo proyecto y observamos que, según el documento, cuando la concentración de O2 en aire baja hasta 8% en pocos minutos se produce la muerte por asfixia. ¡Lo tenemos!  
Hacen los cálculos y el tiempo se reduce a 90 días.  
En ese momento una alumna se levanta y abre del todo un par de ventanas. Por si acaso.  

Segunda actividad.  
Con el dato del gasto en O2 diario ya en la mano, o en la pizarra, necesitan saber cuánto oxígeno produce una planta a diario para saber cuántas necesitaría el alumno para sobrevivir en el aula.  
"¡Vaya preguntas te hacen en 1ESO! ¿Cómo dices que se llama?"  
Les dejo buscar un rato, encuentran diferentes datos sobre árboles, pero sobre plantitas de casa rien de rien, así que les muestro la página que @biologueando me pasó con un dato del que no sabíamos si fiarnos mucho, pero para hacer un problemilla en clase nos sobra   
Dividimos las necesidades diarias del zagal que teníamos calculadas, 252l O2/día, por los 2,4 l que suponemos produce una planta y... premio! Aprox. 100 plantas.  
Ya tengo la respuesta a las preguntas de los alumnos de primero. Hemos pasado un buen rato.  
Ya para acabar, les comento que igual habría alguna alternativa a esto de las plantas en el aula para producir oxígeno (alternativa viva, se entiende) y que investiguen.  
 No hace falta, buscando info sobre la producción de O2 ya han encontrado lo que les pido  y me lo explican:  
"Para empezar, no todas las plantas producen la misma cantidad de O2, podríamos utilizar especies que produzcan mayor cantidad."  
"OK, ¿y una alternativa?"  
" ¡Fitoplancton! Una pecera con fitoplancton"  
"¡Yes sir!"  
Y es que se han encontrado con las mismas webs que yo, y han descubierto que lo de que los bosques son los pulmones de la Tierra...así así...  
Que el fitoplancton produce entre 50% y 70% del oxígeno que respiramos. Lo comentaba por aquí @BiodiverSiTal  
