
# FriQuiExámenes
 
Para los de elaboración propia ver [FriQuiExamenes FiQuiPedia](/home/recursos/examenes/friquiexamenes-fiquipedia) 

Pongo la palabra exámenes en el título de la entrada pero también pueden ser ejercicios  
Se pueden nombrar tanto como [Friki](https://dle.rae.es/friki)exámenes como [FriQui](https://dle.rae.es/friki)exámenes; suelo usar esa segunda porque me parece que enlaza más con Física y Química.   
Algunos ejercicios (con su punto friki como El Principito, en  [Ejercicios Física 2º Bachillerato de Elaboración Propia](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato)  
Me intento centrar en Física y Química, pero a veces pueden ser cosas de ciencias en general.  
Inicialmente surge como recopilación de tuits, en mayo 2018 creo una carpeta donde intento empezar a compartir en pdf, más fáciles de imprimir (bien a partir de imágenes o bien si se comparten en ese formato, incluyendo a veces soluciones. [drive.fiquipedia FriQuiExámenes](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/FriQuiEx%C3%A1menes) )   
En enero 2019 sale la idea en prensa (cuando se cita FiQuiPedia puede haber información en [referencias](/referencias-a-fiquipedia)  
* [telecinco.es Bea, la profesora de los 'friki exámenes' que acaba de triunfar con uno de Star Wars](https://www.telecinco.es/informativos/sociedad/friki-examenes-star-wars-simpson-vengadores-harry-pottter_0_2693250103.html) 
* [educaciontrespuntocero.com ‘Frikiexámenes’ para implicar y reducir el estrés de los estudiantes](https://www.educaciontrespuntocero.com/experiencias/frikiexamenes-estres-estudiantes/100115.html)   
En mayo 2019 sale la idea de frikiexámenes en prensa con @quirogafyq  
* [diariodepontevedra.es Entre Goku y Los Vengadores](https://www.diariodepontevedra.es/articulo/pontevedra/entre-goku-y-los-vengadores/201905191404561035642.html)  
En noviembre 2019 la idea es citada en un libro  
[twitter envozalta_libro/status/1195324627083550721](https://twitter.com/envozalta_libro/status/1195324627083550721)  
Pues a cambio de un tuit tan bonito, aquí va un regalín desde nuestro nuevo libro. Lo mismo te suena  
[![](https://pbs.twimg.com/media/EJalaAKX0AAEIB2?format=jpg "") ](https://pbs.twimg.com/media/EJalaAKX0AAEIB2?format=jpg)  [![](https://pbs.twimg.com/media/EJalaAJW4AARO1n?format=jpg "") ](https://pbs.twimg.com/media/EJalaAJW4AARO1n?format=jpg)  

[Friki-exámenes como herramienta de evaluación en Física y Química, Miguel Juanals Márquez. Máster Universitario en Profesor de Educación Secundaria Obligatoria y Bachillerato, Formación Profesional y Enseñanza de Idiomas. Universidad de Salamanca. Curso 2020-2021](https://gredos.usal.es/bitstream/handle/10366/146924/2021_TFM_MUPES_Friki-ex%C3%A1menes%20como%20herramienta%20de%20evaluaci%C3%B3n%20en%20F%C3%ADsica%20y%20Qu%C3%ADmica.pdf?sequence=1)  


En Twitter se pueden ver buscando etiquetas, pero no todos están etiquetados  
[twitter hashtag #frikiexamen](https://twitter.com/hashtag/frikiexamen)  
[twitter hashtag #frikiexamenes](https://twitter.com/hashtag/frikiexamenes)  

Se puede comentar sobre si los frikiexámenes suponen una dificultad adicional, o motivan ... como siempre depende del contexto, el grupo, hay que ver en qué momentos cambiar. Hay grupos que los piden y grupos que los rechazan. También es importante no pasarse con el frikismo; se trata de un contexto, no de un examen que evalúe conocimientos frikis, pongo este comentario creo que acertado:  
[twitter laguiri/status/1137262015658024962](https://twitter.com/laguiri/status/1137262015658024962)  
Un consejo para profes frikis que hagan preguntas de examen sobre GOT, Mundodisco o una aldea gala: Dad vuestro examen a leer a alguien que no sea del fandom. Tal vez estéis añadiendo sin querer un extra de dificultad, y no, no todo el mundo ve lo mismo que vosotros.

Enlazo tuits: si son borrados o la cuenta pone candado pueden no ser visibles a todos o no verse algunas imágenes

##  FriQuiProfes
En febrero 2018 creo la lista  
[twitter FiQuiPedia/lists/friquiprofes](https://twitter.com/FiQuiPedia/lists/friquiprofes)  
Pongo aquí algunos enlaces a cosas que comparten  
En abril 2018 cito esta lista en el post  [¡Compartid, malditos!](http://algoquedaquedecir.blogspot.com/2018/04/compartid-malditos.html)  
Son fundamentalmente de Física y Química, pero hay más.  
Intento poner por orden alfabético de nombres (aunque a veces el nombre a partir de cuenta de Twitter no es claro)  

###  ACme ([@acme_10](https://twitter.com/acme_10))
 
[twitter acme_10/status/1140629331342962689](https://twitter.com/acme_10/status/1140629331342962689)  
Y una vez mas nos atrevemos, nuestro #frikiexamen es para alumnos universitarios de Fisica, tan chulo como los de @bjmahillo  
![](https://pbs.twimg.com/media/D9RUVD1XkAAiBuE?format=jpg) 

###  Alejandro Gallardo ([@alegallardo28](https://twitter.com/alegallardo28))
 
[twitter alegallardo28/status/1069174357535150086](https://twitter.com/alegallardo28/status/1069174357535150086)  
El ya clásico problema de Pepa Pig y Hulk que empujan un armario. 2ª ley de Newton. Gana Pepa Pig, por supuesto #frikiexámenes  
![](https://pbs.twimg.com/media/DtZ4VbZWoAAwRyQ.jpg)  

###  Alexandra Prada ([@xandraprada](https://twitter.com/xandraprada))
 
[twitter xandraprada/status/1132362876998569989](https://twitter.com/xandraprada/status/1132362876998569989)  
Después de leer tanto a @ProfaDeQuimica @bjmahillo y @FiQuiPedia pues subo mi primer #examenfriki. Me costó hacerlo y a ellos también, pero era una subida de nota. #1ºBACH #FyQ #profesfrikis  
![](https://pbs.twimg.com/media/D7b162TXsAEPBC_.png)  
![](https://pbs.twimg.com/media/D7b18gnWkAE3WKJ.png)  

### Álvaro ([@alvaroarse](https://twitter.com/alvaroarse))
[twitter alvaroarse/status/1456315287217836045](https://twitter.com/alvaroarse/status/1456315287217836045)  
Me enganché a la lectura de los #FriQuiExámenes gracias @ProfaDeQuimica y @FiQuiPedia. Hoy me he estrenado yo. El resultado para los alumnos depende de muchas factores, lo que creo es que el nivel de ansiedad y nerviosismo baja considerablemente.   
![](https://pbs.twimg.com/media/FDXepcaWEAUXjlN?format=jpg)  
![](https://pbs.twimg.com/media/FDXerh4XsAkEKCI?format=jpg)  

[twitter alvaroarse/status/1529005180649455616](https://twitter.com/alvaroarse/status/1529005180649455616)  
Hoy tenemos prueba de Física. Una alumna juega en el @CDBFemenino la prueba está contextualizada. No sé si llamarlo #friquiexamen ? Que lo diga el #claustrovirtual . Lo que sí sé, es que al leer la prueba, la mayoría han sonreído y se han relajado, un poquito.  
![](https://pbs.twimg.com/media/FTgeGc3XEAAhCfQ?format=jpg)  
![](https://pbs.twimg.com/media/FTgeGc5WYAYM7xw?format=jpg)  
![](https://pbs.twimg.com/media/FTgeGc2XEAEKFlx?format=jpg)  
![](https://pbs.twimg.com/media/FTgeGerXwAIJSah?format=jpg)  

###  Anna Olivella ([@educannt](https://twitter.com/educannt))
 
[twitter educannt/status/1090360802501148672](https://twitter.com/educannt/status/1090360802501148672)  
Aquí otra que se suma al clan de los #frikiexamen !! Gracias @ProfaDeQuimica @profedefyq @bjmahillo @Biochiky por la inspiración! a ver cómo se lo toman mañana...🤣 #fiq #fyq #3ESO #fiq3eso #fyq3eso  
![](https://pbs.twimg.com/media/DyG9LgTXgAAVrte.jpg)  
![](https://pbs.twimg.com/media/DyG9LgUXcAASMqK.jpg)  

###  Antonio Ruiz Relea ([@profedefyq](https://twitter.com/profedefyq/)) 
 
[twitter profedefyq/status/942754988555726848](https://twitter.com/profedefyq/status/942754988555726848)  
Frikiexámenes, parte 1.  
![](https://pbs.twimg.com/media/DRVWpNSXkAAIOrR.jpg)  
[twitter profedefyq/status/942755173310574599](https://twitter.com/profedefyq/status/942755173310574599)  
Frikiexámenes parte 2. Querido @octavio_pr , el 4 en su honor.  
![](https://pbs.twimg.com/media/DRVW0aTWkAAdtlM.jpg)  
[twitter profedefyq/status/966357240264486913](https://twitter.com/profedefyq/status/966357240264486913)  
Dedicado a mis frikis favoritxs @bjmahillo @Beatriz_Nino @ProfaDeQuimica y al gran @octavio_pr  
![](https://pbs.twimg.com/media/DWkwxf8WkAAeP6q.jpg)  
*incluye imágenes que instagram no parece dejar enlazar directamente*  
[twitter profedefyq/status/972191967697342469](https://twitter.com/profedefyq/status/972191967697342469)  
Último examen del trimestre. Sorprendentes resultados:Bart muere por el impacto, el Fénix no…   
*incluye imágenes que instagram no parece dejar enlazar directamente*   
[twitter profedefyq/status/992417582123769857](https://twitter.com/profedefyq/status/992417582123769857)  
![](https://pbs.twimg.com/media/DcXGhbwX4AEeoNf.jpg)  
[twitter profedefyq/status/992417602646441986](https://twitter.com/profedefyq/status/992417602646441986)  
![](https://pbs.twimg.com/media/DcXGivlWsAMDkzj.jpg)  
![](https://pbs.twimg.com/media/DcXG0RmW4AEpv4Y.jpg)  
![](https://pbs.twimg.com/media/DcXG2ZFXkAAy1kS.jpg)  
[twitter profedefyq/status/999948332473765888](https://twitter.com/profedefyq/status/999948332473765888)  
![](https://pbs.twimg.com/media/DeCHqqsXkAARNau.jpg)  
![](https://pbs.twimg.com/media/DeCHsJJXUAItyeR.jpg)  
[twitter profedefyq/status/999982358140018690](https://twitter.com/profedefyq/status/999982358140018690)  
![](https://pbs.twimg.com/media/DeCmoZrWAAAVpJ0.jpg)  
![](https://pbs.twimg.com/media/DeCmpRAWsAAZvCV.jpg)  
[twitter profedefyq/status/940948289398206470](https://twitter.com/profedefyq/status/940948289398206470)  
frikienunciados "Enunciados raros" dicen los alumnos...con lo que me ha costado encontrar el Moste potente potions en la sección prohibida 😫😫😫  
![](https://pbs.twimg.com/media/DQ7rdy0WsAEouEa.jpg)  
[twitter profedefyq/status/1059436177449779200](https://twitter.com/profedefyq/status/1059436177449779200)  
Lunes de Frikiexamen. @monkeyislandsp quiero mi camiseta!!!#frikiexamen #geekmodeon  
![](https://pbs.twimg.com/media/DrPfkUzXgAA2_8f.jpg)  
![](https://pbs.twimg.com/media/DrPfk-MXQAEuRAq.jpg)  
[twitter profedefyq/status/1063425416109703168](https://twitter.com/profedefyq/status/1063425416109703168)  
Friki-examen para segundo de la ESO. Lo pongo por aquí también,por si a alguien le viene bien/le inspira. No es gran cosa, pero no doy pa más. #frikiexamen  
![](https://pbs.twimg.com/media/DsILwFVWkAApA_B.jpg)  
![](https://pbs.twimg.com/media/DsILw5kXoAA6QYM.jpg)  
![](https://pbs.twimg.com/media/DsILxgRXgAAOAih.jpg)  
[twitter profedefyq/status/1061989905885249539](https://twitter.com/profedefyq/status/1061989905885249539)  
Friki-examen para 3° de E.S.O. Puede que el último para este nivel de este curso escolar. Pero por si a alguien le sirve, ahí lo dejo.  
![](https://pbs.twimg.com/media/DrzyIbvX0AEWRUo.jpg)  
![](https://pbs.twimg.com/media/DrzyJioXcAALhUR.jpg)  
![](https://pbs.twimg.com/media/DrzyKhgW4AAX-Bv.jpg)  
[twitter profedefyq/status/1072132098432139265](https://twitter.com/profedefyq/status/1072132098432139265)  
Friki-examen para 3° de la E.S.O. Hay alguna errateja corregida en la versión definitiva pero, por si a alguien le sirve, por aquí lo dejo. #frikiexamen  
![](https://pbs.twimg.com/media/DuD6cAzXcAECMEk.jpg)  
![](https://pbs.twimg.com/media/DuD6cq9X4AATkuJ.jpg)  
![](https://pbs.twimg.com/media/DuD6dIuXQAAYISW.jpg)  
[twitter profedefyq/status/1073571484805795842](https://twitter.com/profedefyq/status/1073571484805795842)  
frikiexamen de 2° de la ESO. No hay nada como recitar la inscripción del Anillo Único (cual Gandalf) a mis pequeños hobbits. Pero creo que mucha gracia no les ha hecho. 🤔🤔Y Shuri es top. He dicho.  
![](https://pbs.twimg.com/media/DuYXi2AXcAA97tK.jpg)  
![](https://pbs.twimg.com/media/DuYXjnxXQAAsLTn.jpg)  
![](https://pbs.twimg.com/media/DuYXkW8W4AYkPQd.jpg)  
[twitter profedefyq/status/1073573006411210752](https://twitter.com/profedefyq/status/1073573006411210752)  
Y, finalmente, el #frikiexamen de 1° de Bachillerato. Me he venido tan arriba que,al inventarme los datos, sale un compuesto con más del 100% de composición elemental. Con un par de Madmardigans.  
![](https://pbs.twimg.com/media/DuYY7eNX4AAVP7P.jpg)  
![](https://pbs.twimg.com/media/DuYY8MfWkAcaReq.jpg)  
![](https://pbs.twimg.com/media/DuYY8uOXQAADdi6.jpg)  
[twitter profedefyq/status/1091338489885609984](https://twitter.com/profedefyq/status/1091338489885609984)  
Primer #frikiexamen de la segunda evaluación. Un poco de todo: Aragorn, El Rey León, Dr Strange y un poco de literatura. Por si a alguien le sirve 😉Abro ☂️.  
![](https://pbs.twimg.com/media/DyU2i80WsAAzMiQ.jpg)
![](https://pbs.twimg.com/media/DyU2j0wX0AA89nr.jpg)  
[twitter profedefyq/status/1092782313547722752](https://twitter.com/profedefyq/status/1092782313547722752)   
Frikiexamen de estos días para 3° de la E.S.O. Sigo haciendo batiburrillo de frikeces, al menos hasta el jueves en el examen de 4°, pero, por si a alguien le sirve, ahí queda.  
![](https://pbs.twimg.com/media/DypXrdAXcAEswuf.jpg)  
![](https://pbs.twimg.com/media/DypXtdCX0AY-K5S.jpg)  
[twitter profedefyq/status/1093530445973721088](https://twitter.com/profedefyq/status/1093530445973721088)  
Nuevo #frikiexamen para 4° de la E.S.O. temático de Marvel. Primero de este año para este curso. No sé si es demasiado complicado para este nivel. Y quien guste, que lo use a discreción.  
![](https://pbs.twimg.com/media/Dy0AHgCWsAALOlS.jpg)  
![](https://pbs.twimg.com/media/Dy0AIxTWsAIh69I.jpg)  
[twitter profedefyq/status/1093868394800136200](https://twitter.com/profedefyq/status/1093868394800136200)  
Último #frikiexamen de la semana. Y ya, hasta dentro de un mes, no volverán. O no deberían. Por petición propia de los nenes. Stranger Things, minions y Harry Potter. Aunque quizás no les ha gustado demasiado  
![](https://pbs.twimg.com/media/Dy4zfJYXcAALMdp.jpg)  
![](https://pbs.twimg.com/media/Dy4zf7aX0AAy4r6.jpg)  
[twitter profedefyq/status/1103710942628642819](https://twitter.com/profedefyq/status/1103710942628642819)  
¿Jueves de examen? No. Jueves de #frikiexamen  
![](https://pbs.twimg.com/media/D1ErN33XcAAL1fc.jpg)  
![](https://pbs.twimg.com/media/D1ErOjCX4AEM99o.jpg)  
![](https://pbs.twimg.com/media/D1ErPGeXgAAZw2a.jpg)  
![](https://pbs.twimg.com/media/D1ErPvbWsAUFegy.jpg)  
[twitter profedefyq/status/1105130483313242112](https://twitter.com/profedefyq/status/1105130483313242112)  
Último #frikiexamen del trimestre. Un poco de todo y algún enunciado reciclado. El ejercicio 4 tiene una errata. La velocidad del murciélago es de 15. Todo sea por salvar a la chica. Si alguien desea usarlo, como siempre, tiene toda la libertad del mundo. #geekmodeon  
![](https://pbs.twimg.com/media/D1Y2TEKW0AELT62.jpg)  
![](https://pbs.twimg.com/media/D1Y2T3GXcAAXz3k.jpg)  
[twitter profedefyq/status/1185241711167115264](https://twitter.com/profedefyq/status/1185241711167115264)  
Primer #frikiexamen del curso. Opiniones y sentimientos encontrados. Eso sí, Azathoth sigue durmiendo...de momento  
Compartido en formato pdf  
[PRIMER PARCIAL 1ª EVAL.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/FriQuiEx%C3%A1menes/profedefyq/2019-10-18-FIS2-profedefyq-PRIMER%20PARCIAL%201%C2%AA%20EVAL.pdf)  
[twitter profedefyq/status/1186999892750864384](https://twitter.com/profedefyq/status/1186999892750864384)  
frikiexamen para química de 2 Bach (por petición suya, he de añadir). Por si a alguien le sirve. Compartido en formato pdf  
[PARCIAL 1 QUIMICA FRIKIENUNCIADOS.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/FriQuiEx%C3%A1menes/profedefyq/2019-10-23-ProfedeFyQ-QUI2-PARCIAL%201%20QUIMICA%20FRIKIENUNCIADOS.pdf)  
[twitter profedefyq/status/1187346048538333184](https://twitter.com/profedefyq/status/1187346048538333184)  
4° ESO. Cinemática. Último #frikiexamen de la tanda de parciales.Hasta dentro de un mes, más o menos.  
![](https://pbs.twimg.com/media/EHpM6pUXkAAucB2?format=jpg)  
![](https://pbs.twimg.com/media/EHpM7h0WkAMR6oF?format=jpg)  
En pdf [PRIMER PARCIAL 4º PRIMERA EVAL.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/FriQuiEx%C3%A1menes/profedefyq/2019-10-24-FQ4-ProfedeFyQ-PRIMER%20PARCIAL%204%C2%BA%20PRIMERA%20EVAL.pdf)  
[twitter profedefyq/status/1200424108745465856](https://twitter.com/profedefyq/status/1200424108745465856)  
Segunda tanda de #frikiexamenes que se han hecho estos días. Se te va a echar de menos, @profesorafyq  
Espero que os sirva. Feliz finde.  
![](https://pbs.twimg.com/media/EKjDUFUWoAcPL6c?format=jpg)  
![](https://pbs.twimg.com/media/EKjDVaOWsAAi8Im?format=jpg)  
![](https://pbs.twimg.com/media/EKjDV6jWwAENkLv?format=jpg)  
![](https://pbs.twimg.com/media/EKjDWZuXsAYuolj?format=jpg)  
[twitter profedefyq/status/1228257581971492864](https://twitter.com/profedefyq/status/1228257581971492864)  
-Profe, no esperaba un #frikiexamen hoy. - Ni yo. Lo he montado en 15 minutos. Y eso explica por qué estará lleno de fallos y cogido con pinzas. Pero, por si a alguien le sirve, ahí queda.  
![](https://pbs.twimg.com/media/EQulv8oWsAE8vEm?format=jpg)  
![](https://pbs.twimg.com/media/EQulwaeX0AAEP5f?format=jpg)  
En pdf en  [FINAL 2ª eval FRIKIEXAMEN.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/FriQuiEx%C3%A1menes/profedefyq/2020-02-14-FIS2-ProfedeFyQ-FINAL%202%C2%AA%20eval%20FRIKIEXAMEN.pdf)  
[twitter profedefyq/status/1232631541588779008](https://twitter.com/profedefyq/status/1232631541588779008)  
Hoy, #frikiexamen en 4° de ESO, por petición suya. Recuerden: Leer el examen con la bso de Endgame es bien. Por si a alguien le sirve.  
![](https://pbs.twimg.com/media/ERsv1wXXYAEE3cd?format=jpg)  
En pdf  [FINAL 4º 2ª EVAL FRIKIEX.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/FriQuiEx%C3%A1menes/profedefyq/2020-02-25-FQ4-profedefyq-FINAL%204%C2%BA%202%C2%AA%20EVAL%20FRIKIEX.pdf)  
[twitter profedefyq/status/1245626340000141315](https://twitter.com/profedefyq/status/1245626340000141315)  
El #frikiexamen que pudo ser y no fue. Se les ha enviado a los alumnos para que, en la medida en que puedan, sigan trabajando pese a la incertidumbre.  Comparto por si a alguien le vale (o le arranca una sonrisa, que en estos días hace mucha falta).  
![](https://pbs.twimg.com/media/EUlah_lUYAAhGq7?format=jpg)  
![](https://pbs.twimg.com/media/EUlair_VAAcXMDz?format=jpg)  
En pdf  [Frikiexamen optica simulacro FISICA coronavirus v final.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/FriQuiEx%C3%A1menes/profedefyq/2020-04-02-FIS2-profedefyq-Frikiexamen%20optica%20simulacro%20FISICA%20coronavirus%20v%20final.pdf)  
[twitter profedefyq/status/1317097158080925696](https://twitter.com/profedefyq/status/1317097158080925696)  
Pues, como avanzábamos, hoy es viernes de #frikiexamen De temática pulpitos, además, que es como le gusta a @OscarRecioColl y otros adoradores de primigenios.  
![](https://pbs.twimg.com/media/EkdE4a3WAAIrtWR?format=jpg)  
![](https://pbs.twimg.com/media/EkdE4mMXYAA00-h?format=jpg)  
[twitter profedefyq/status/1451507875029913603](https://twitter.com/profedefyq/status/1451507875029913603)  
Hoy es viernes...  
Viernes de #frikiexamen . De nuevo, Star wars pero... Es tan divertido entrar con la marcha imperial al examen...  
Comparto por si a alguien sirve, ilumina, ayuda, inspira o lo que sea.   
A ser felices, amantes de la tiza.   
![](https://pbs.twimg.com/media/FCTKtdQXoAIojjz?format=jpg)  

###  Beatriz Jiménez Mahíllo ([@bjmahillo](https://twitter.com/bjmahillo/)) 
Compartidos en pdf en esta carpeta  [https://drive.google.com/drive/folders/1TCf7YRd8QtnzVOtMv8KDutCD6JuTi8ij](https://drive.google.com/drive/folders/1TCf7YRd8QtnzVOtMv8KDutCD6JuTi8ij)  
[twitter bjmahillo/status/963462140672774144](https://twitter.com/bjmahillo/status/963462140672774144)  
Aquí comparto el examen de #química de 4º ESO que he hecho, mi pequeño homenaje a #Mortadelo y #Filemón en su 60º aniversario. ¡Grande F. Ibáñez!   
![](https://pbs.twimg.com/media/DV7nl5GW4AETwdE?format=jpg)  
![](https://pbs.twimg.com/media/DV7nnHoXUAAaFk8?format=jpg)  
[twitter bjmahillo/status/975281977459986432](https://twitter.com/bjmahillo/status/975281977459986432)  
Porque en 2° Bachillerato también es posible hacer exámenes de #Física diferentes... #AC⚡DC #HighwayToHell  
![](https://pbs.twimg.com/media/DYjlulBWAAAV5-R?format=jpg)  
![](https://pbs.twimg.com/media/DYjlwucWkAIPXlp?format=jpg)  
[twitter bjmahillo/status/987065090896748544](https://twitter.com/bjmahillo/status/987065090896748544)  
Y allá va la tanda de problemas de física de #JuegodeTronos de 4º ESO. #repasofriki  
![](https://pbs.twimg.com/media/DbLB3IBWAAAVhcO?format=jpg)  
![](https://pbs.twimg.com/media/DbLB4JIWkAAxFxb?format=jpg)  
[twitter bjmahillo/status/988424993519427584](https://twitter.com/bjmahillo/status/988424993519427584)  
Examen de física de Supermán en 4°ESO. Lo prometido es deuda.  [#frikiexamen](https://twitter.com/hashtag/frikiexamen?src=hash)  
![](https://pbs.twimg.com/media/DbeXQZAW0AAcytc?format=jpg)  
[twitter bjmahillo/status/991640061245644800](https://twitter.com/bjmahillo/status/991640061245644800)  
![](https://pbs.twimg.com/media/DcMBcbZXcAAMp3h?format=jpg)  
![](https://pbs.twimg.com/media/DcMBdUPX4AA7gSG?format=jpg)  
[twitter bjmahillo/status/1055458277448974336](https://twitter.com/bjmahillo/status/1055458277448974336)  
Física de superhéroes. #Superlópez también se merece un examen de 2° Bachillerato. 💃Afrontamos la #EvAU de otra manera. 💪  
![](https://pbs.twimg.com/media/DqW9psDXcAAUbWW?format=jpg)  
![](https://pbs.twimg.com/media/DqW9sXCWoAYl_-1?format=jpg)  
[twitter bjmahillo/status/1056087347111059456](https://twitter.com/bjmahillo/status/1056087347111059456)  
Examen de E.T. en Física de 2° Bachillerato.  
![](https://pbs.twimg.com/media/Dqf50YIWkAA2CC3?format=jpg)  
[twitter bjmahillo/status/1060523921454362625](https://twitter.com/bjmahillo/status/1060523921454362625)  
Ejercicios de repaso Física 2° Bach. Toy Story y Doraemon por petición de los alumnos. Y la profe se deja llevar. 😉  
![](https://pbs.twimg.com/media/Dre82MIWoAAPXSI?format=jpg)  
![](https://pbs.twimg.com/media/Dre83mdWsAASttW?format=jpg)  
[twitter bjmahillo/status/1073853687259369472](https://twitter.com/bjmahillo/status/1073853687259369472)  
Y allá va otra tanda de ejercicios de química de #JuegoDeTronos para 4° ESO. ¡Qué ganas de ver la última temporada! #GameOfThrones #ValarMorghulis  
![](https://pbs.twimg.com/media/DucYK8iWwAA0_jP?format=jpg)  
![](https://pbs.twimg.com/media/DucYNZTW4AEofe8?format=jpg)  
[twitter bjmahillo/status/1085597341648850944](https://twitter.com/bjmahillo/status/1085597341648850944)  
Un alumno de Física 2º Bach. llevaba semanas pidiéndome un #frikiexamen de #StarWars. Llegó el momento. Me debo a mi exigente público. 😉 @StarWarsSpain  
![](https://pbs.twimg.com/media/DxDQcJjXQAIwadb?format=jpg)  
![](https://pbs.twimg.com/media/DxDQc9RWsAAb1b4?format=jpg)  
[twitter bjmahillo/status/1091010181620776962](https://twitter.com/bjmahillo/status/1091010181620776962)  
Visitamos el @vangoghmuseum con este examen artístico de química en 4º ESO basado en #VanGogh 🎨. Gracias por la inspiración a @Oskar_KimikArte y su estupendo #kimikArte. 👌  
![](https://pbs.twimg.com/media/DyQKHvAWwAAjlRD?format=jpg)  
![](https://pbs.twimg.com/media/DyQKIl4WwAE25zw?format=jpg)  
[twitter bjmahillo/status/1093202263957557248](https://twitter.com/bjmahillo/status/1093202263957557248)  
Hoy, atendiendo de nuevo la petición de una alumna en Física 2º Bach., he hecho el #frikiexamen más loco hasta la fecha: nos vamos de fiesta con las #Kardashian.Y las risas que les he arrancado a los chicos me las llevo de regalo. 😊  
![](https://pbs.twimg.com/media/DyvVEikWoAEakIf?format=jpg)  
[twitter bjmahillo/status/1096509285025177603](https://twitter.com/bjmahillo/status/1096509285025177603)  
Literatura + Física 2° Bach. Actividades de repaso de #EvAU con #Frankenstein. Comparto por si alguien le sirve o le da ideas.  
![](https://pbs.twimg.com/media/DzeVTEDXcAEh_WQ?format=jpg)  
![](https://pbs.twimg.com/media/DzeVWCyXgAAEjRT?format=jpg)  
[twitter bjmahillo/status/1105897015416901634](https://twitter.com/bjmahillo/status/1105897015416901634)  
¡Vuelvo a la carga! #Frikiexamen en Física de 2º Bach. sobre #Queen y el gran Freddie Mercury. Tengo varios alumnos a los que les gusta mucho la música.🎤🎸 El ejercicio 3, mi favorito. 😉  
![](https://pbs.twimg.com/media/D1juoVTWwAApCYL?format=jpg)  
![](https://pbs.twimg.com/media/D1juq42WoAUlf_g?format=jpg)  
[twitter bjmahillo/status/1106565874792701952](https://twitter.com/bjmahillo/status/1106565874792701952)  
\#Frikiexamen de Física 2° Bach. sobre la gala de los #Óscar2019. Todo el glamour con Lady Gaga, Julia Roberts, Javier Bardem... 
![](https://pbs.twimg.com/media/D1tPvNiWwAAyuhn?format=jpg)  
![](https://pbs.twimg.com/media/D1tPxUqWsAA69dD?format=jpg)  
[twitter bjmahillo/status/1113787802284695552](https://twitter.com/bjmahillo/status/1113787802284695552)  
\#Frikiexamen de #HarryPotter en Física 2° Bach. Este mago da mucho juego. 😄 Una alumna me lo pidió hace tiempo y se ha hecho esperar.  
![](https://pbs.twimg.com/media/D3T4CbaXkAE4DZ_?format=jpg)  
![](https://pbs.twimg.com/media/D3T4EyOXoAAt4F7?format=jpg)  
[twitter bjmahillo/status/1114429824758616064](https://twitter.com/bjmahillo/status/1114429824758616064)  
Último #frikiexamen del curso en Física 2° Bach. sobre los #Simpsons. Como le prometí a @RadiactivoMan, le he dedicado un ejercicio. Espero que le sirva para recuperar su licencia de superhéroe. 😜 Aunque el cansancio hace mella y he perdido inspiración.  
![](https://pbs.twimg.com/media/D3c__OqW0AEVJsm?format=jpg)  
![](https://pbs.twimg.com/media/D3dAAeoXoAEZZL1?format=jpg)  
[twitter bjmahillo/status/1123218171614330880](https://twitter.com/bjmahillo/status/1123218171614330880)  
Tra, tra. Hoy ha tocado #frikiexamen de física de #Rosalía en 4° ESO. Y es que en mis clases @rosaliavt tiene muchos fans.🎤🎶  
![](https://pbs.twimg.com/media/D5Z46ckX4AElk0V?format=jpg)  
[twitter bjmahillo/status/1132302977446961157](https://twitter.com/bjmahillo/status/1132302977446961157)  
Último examen que subo por aquí este curso en el día del #OrgulloFriki. Me gusta mucho la peli de Mary Poppins (por ñoña que sea), así que me lo he autorregalado. Esto de los #frikiexamenes motiva a los alumnos pero, últimamente, sobre todo, a mí.  
![](https://pbs.twimg.com/media/D7a_gL-X4AArPM9?format=jpg)  
![](https://pbs.twimg.com/media/D7a_gL-X4AArPM9?format=jpg)  
[twitter bjmahillo/status/1189278562169344013](https://twitter.com/bjmahillo/status/1189278562169344013)  
Aquí está mi pequeño homenaje al @museodelprado en su 200º aniversario.¡Física y pintura en un #frikiexamen! 🎨  
![](https://pbs.twimg.com/media/EIEqgE2WsAAf5fe?format=jpg)  
![](https://pbs.twimg.com/media/EIEqgE_X4AMaqkZ?format=jpg)  
[twitter bjmahillo/status/1197589180714491904](https://twitter.com/bjmahillo/status/1197589180714491904)  
Ya llevaba tiempo con ganas de dedicarle un #frikiexamen a James Bond (me encantan las pelis y series de espías). Reconozco que algunos ejercicios están pillados por los pelos (la palanca de cambios del Aston Martin es muuy grande 🤭). Estoy algo agobiada y eso se nota...  
![](https://pbs.twimg.com/media/EJ6xATUWkAEaHPm?format=jpg)  
![](https://pbs.twimg.com/media/EJ6xATWWoAIWJH9?format=jpg)  
[twitter bjmahillo/status/1215292342833438720](https://twitter.com/bjmahillo/status/1215292342833438720)  
Nos metemos en la cocina de @MasterChef_es para repasar química de 2º ESO en este principio de año.¡Ñam, ñam!🍴🍳🧂🦞¿Serán los alumnos buenos cocineros? 👩‍🍳👨‍🍳  
![](https://pbs.twimg.com/media/EN2V78kX0AIFI3l?format=jpg)  
![](https://pbs.twimg.com/media/EN2V78tWoAArPhI?format=jpg)  
[twitter bjmahillo/status/1218195552191512578](https://twitter.com/bjmahillo/status/1218195552191512578)  
Y mi pequeño homenaje al final de la saga de #StarWars es este #frikiexamen de Física de 2º Bachillerato.¡Que la fuerza nos acompañe! 🙂#ElAscensoDeSkywalker  
![](https://pbs.twimg.com/media/EOfmZFmWoAAjeI_?format=jpg)  
![](https://pbs.twimg.com/media/EOfmZFkXkAAsrmU?format=jpg)  
[twitter bjmahillo/status/1226067444499873798](https://twitter.com/bjmahillo/status/1226067444499873798)  
¿Se puede hacer un #frikiexamen de Física de 2º Bachillerato sobre el @Atleti? Se puede e, incluso, mencionar a Joaquín Sabina.  
Ha sido un examen polémico entre los alumnos. Chico/as, que haya elegido al Atlético de Madrid es pura coincidencia... #AupaAtleti  
![](https://pbs.twimg.com/media/EQPd1nXWkAAzEvT?format=jpg)  
![](https://pbs.twimg.com/media/EQPd1nXWAAETngB?format=jpg)  
[twitter bjmahillo/status/1242857715191484418](https://twitter.com/bjmahillo/status/1242857715191484418)  
En estos días tan oscuros he pretendido que los alumnos de 2º Bachillerato, que siguen trabajando, se vean un poco iluminados por la luz de #JoaquínSorolla.  Comparto para aquel que le sirva.  @MuseoSorolla #QuédateEnCasa  
![](https://pbs.twimg.com/media/ET-DOCOXgAM-i6x?format=jpg)  
![](https://pbs.twimg.com/media/ET-DO9wXQAAz0kk?format=jpg)  
[twitter bjmahillo/status/1246124187586113536](https://twitter.com/bjmahillo/status/1246124187586113536)  
Comparto este simulacro de examen artístico de Física de 2º Bachillerato sobre "La Anunciación" de Fra Angélico, en el @museodelprado Saqué la idea de este artículo de Manuel Vicent de @elpais_cultura:  [elpais.com Cabalgando un rayo de oro](https://elpais.com/cultura/2020-03-27/cabalgando-un-rayo-de-oro.html)  Física+Arte en estos tiempos. #QuédateEnCasa  
![](https://pbs.twimg.com/media/EUsdmpIXsAAuHxQ?format=jpg)  
![](https://pbs.twimg.com/media/EUsdnb0XkAMFocd?format=jpg)  
[twitter bjmahillo/status/1255924727639662600](https://twitter.com/bjmahillo/status/1255924727639662600)  
Estoy sobrepasada 😩 con tanta corrección online, redacción de materiales, correos, copieteos, plataformas... He preparado este #frikiexamen de Física de 2ª Bach sobre #RegresoAlFuturo 🕑 para desengrasar un poco, que le tenía ganas. 😉  
![](https://pbs.twimg.com/media/EW3vm7YWAAEhZrQ?format=jpg)  
![](https://pbs.twimg.com/media/EW3vnzDXgAEym4B?format=jpg)  
[twitter bjmahillo/status/1270419811844206592](https://twitter.com/bjmahillo/status/1270419811844206592)  
Comparto este #frikiexamen (simulacro) de Física de 2º Bachillerato sobre #StrangerThings. Creo que será el último que subo por aquí este curso. Había varios fans en clase, así que tuve que sacrificarme y ver la primera temporada para pillar el contexto.  
![](https://pbs.twimg.com/media/EaFvVBMXgAQDegu?format=jpg)  
![](https://pbs.twimg.com/media/EaFvWQ1WsAE4P79?format=jpg)  

###  Beatriz Niño ([@Beatriz_Nino](https://twitter.com/Beatriz_Nino/)) 
[twitter Beatriz_Nino/status/970047175366410241](https://twitter.com/Beatriz_Nino/status/970047175366410241)  
¡Tus alumnos y los míos también! Les puse estos dos en un examen el viernes. No obstante, yo les señalo “la chicha” del ejercicio en negrita.  
![](https://pbs.twimg.com/media/DXZMwkfXkAA-NzY.jpg)  

###  Carolina Clavijo ([@Carolinaciencia](https://twitter.com/Carolinaciencia))
 
[twitter Carolinaciencia/status/1259181762338598912](https://twitter.com/Carolinaciencia/status/1259181762338598912)  
frikiexámenes en confinamiento, formularios de google con mis alumnos @iesitaca Cinemática. Nos divertimos con la Física con Fornite, evento Travis Scott, Endgame, La casa de papel. Gracias @bjmahillo por darnos ideas. Disponibles en :  [Frikiexámenes](https://sites.google.com/iesitaca.org/fisica-yquimicacarolinaclavijo/frikiex%C3%A1menes)  
![](https://pbs.twimg.com/media/EXmCqMiXYAEfjCW?format=png)  
![](https://pbs.twimg.com/media/EXmCvUDXYAIDS-o?format=png)  
 
[twitter Carolinaciencia/status/1130775098217160704](https://twitter.com/Carolinaciencia/status/1130775098217160704) Ayer último día de @JuegoDeTR , y no me he resistido a un #frikiexamen de cinemática y juego de tronos. Todo un éxito en mis alumnos. 
![](https://pbs.twimg.com/media/D7FR8x-X4AAj2XM.jpg)  
[twitter Carolinaciencia/status/1274039620116848645](https://twitter.com/Carolinaciencia/status/1274039620116848645)  
Últimos #frikiexamenes del año, fuerzas #fisicayquimica 4º he disfrutado con la Crew Dragon y Apolo XI, aprendemos disfrutando incluso en confinamiento @bjmahillo  
[Frikiexámenes](https://sites.google.com/iesitaca.org/fisica-yquimicacarolinaclavijo/frikiex%C3%A1menes)  
![](https://pbs.twimg.com/media/Ea5MGQMWoAEp40-?format=png)  
![](https://pbs.twimg.com/media/Ea5MGQVXQAEPg_t?format=png)  
![](https://pbs.twimg.com/media/Ea5MKzKXYAMyMdk?format=png)  
![](https://pbs.twimg.com/media/Ea5MK0CXYAEN-_m?format=png)  

###  Conchi ([@Conce_Ciencia](https://twitter.com/Conce_Ciencia)) 
 
[twitter Conce_Ciencia/status/974541865255972866](https://twitter.com/Conce_Ciencia/status/974541865255972866)  
![](https://pbs.twimg.com/media/DYZEpY0UQAQPHgT.jpg)  
[twitter Conce_Ciencia/status/991649349468815360](https://twitter.com/Conce_Ciencia/status/991649349468815360)  
![](https://pbs.twimg.com/media/DcML0TCWAAAm9Sm.jpg)  
[twitter Conce_Ciencia/status/998607482594516992](https://twitter.com/Conce_Ciencia/status/998607482594516992)  
![](https://pbs.twimg.com/media/DdvENEPV4AAdjDU.jpg)  
![](https://pbs.twimg.com/media/DdvENETV4AAPnHG.jpg)  
[twitter Conce_Ciencia/status/996668492513447936](https://twitter.com/Conce_Ciencia/status/996668492513447936)  
A partir de un magnífico examen de @Biochiky ( Gracias !!) con alguna modificación para mis chicos #friquiexamenes  
![](https://pbs.twimg.com/media/DdTgtIoW4AAqXvl.jpg)  
![](https://pbs.twimg.com/media/DdTgtInWsAEPDkl.jpg)  

###  David Sánchez Sánchez ([@biologiayarte](https://twitter.com/biologiayarte))
[twitter biologiayarte/status/987969351331532800](https://twitter.com/biologiayarte/status/987969351331532800)  
Comparte [carpeta drive.google](https://drive.google.com/drive/folders/1feeal7w2rRY1vHFQSfW2f9Lx_Rx5N9JJ)  
[twitter biologiayarte/status/993828575613718534](https://twitter.com/biologiayarte/status/993828575613718534)  
![](https://pbs.twimg.com/media/DcrJyvEXkAAprB3.jpg)  
![](https://pbs.twimg.com/media/DcrJz5PWsAA1kPr.jpg)  
[twitter biologiayarte/status/1004954649374461952](https://twitter.com/biologiayarte/status/1004954649374461952)  
Aprovechando el estreno de Avengers: Infinity War y algún #friquiexamen visto por aquí...mi último examen de física y química del curso para 2° ESO #friquiprofes Al final he conseguido disfrutar de una asignatura que no es la mía :)  
![](https://pbs.twimg.com/media/DfJQ3bjXkAA8IqG?format=jpg)  
![](https://pbs.twimg.com/media/DfJQ5QLWsAIePbc?format=jpg)  
![](https://pbs.twimg.com/media/DfJQ6WiWkAAso4S?format=jpg)  
[twitter biologiayarte/status/1005717687924862976](https://twitter.com/biologiayarte/status/1005717687924862976)  
Y para despedir el curso, último #frikiexamen de 2º ESO Física y Química. El movimiento. Da gusto la retroalimentación positiva que se genera entre los #frikiprofes que andamos por twitter, al final acaba por hacer que una asignatura que no es la tuya te guste¡¡¡¡  
![](https://pbs.twimg.com/media/DfUGcFVX0AAQI22.jpg)  
![](https://pbs.twimg.com/media/DfUGcFiWAAAXNvt.jpg)  
![](https://pbs.twimg.com/media/DfUGcFkXcAAorJH.jpg)  ](https://pbs.twimg.com/media/DfUGcFkXcAAorJH.jpg)  
[twitter biologiayarte/status/1096677364388556801](https://twitter.com/biologiayarte/status/1096677364388556801)   
1º BACHILLERATO ANATOMÍA APLICADA. APARATO LOCOMOTOR #frikiexamen SI EL B LO HABÍA TENIDO, EL C NO PODÍA SER MENOS.... TODO UN PARTIDO DE LIGA ENTRE MADRID Y ATLETICO CONCENTRADO EN UN EXÁMEN Y CON SERGIO RAMOS COMO PROTAGONISTA JAJAJAJAJAJA  
![](https://pbs.twimg.com/media/Dzgt738X0AA4IYF.png)  
![](https://pbs.twimg.com/media/Dzgt73eXcAEur-Q.png)  
![](https://pbs.twimg.com/media/Dzgt73oWkAI4Rab.png)  
[twitter biologiayarte/status/1387022778008756225](https://twitter.com/biologiayarte/status/1387022778008756225)  
1° ESO. Examen invertebrados. Harry Potter y el Invertebrado filosofal #HarryPotter #profesbiogeo #recursosbiogeo #examenesfrikis #frikiexamen  
![](https://pbs.twimg.com/media/Ez-xyfxX0AAnbkU?format=jpg)  
![](https://pbs.twimg.com/media/Ez-x0E1WUAY4qNQ?format=jpg)  
![](https://pbs.twimg.com/media/Ez-x2ERWQAMsf7I?format=jpg)  
[twitter biologiayarte/status/1530058908953366528/](https://twitter.com/biologiayarte/status/1530058908953366528/)  
4° ESO GENÉTICA #Marvel. Lo prometido es deuda: el #Frikiexamen para cerrar el tema de La Herencia.  
![](https://pbs.twimg.com/media/FTvcdG2XwAEeZ1j?format=jpg)  
![](https://pbs.twimg.com/media/FTvcddpXwAQcykR?format=jpg)  
![](https://pbs.twimg.com/media/FTvcd-4XwAAWQLx?format=jpg)  


###  Enrique García ([@FiQuiPedia](https://twitter.com/FiQuiPedia))

Ver [FriQuiExamenes FiQuiPedia](/home/recursos/examenes/friquiexamenes-fiquipedia) 

Los de elaboración propia en pdf resueltos en **[drive.fiquipedia FriQuiExámenes](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/FriQuiEx%C3%A1menes)**  
En abril 2022 había 79 resueltos   
[https://twitter.com/FiQuiPedia/status/1511381024508325894](https://twitter.com/FiQuiPedia/status/1511381024508325894)  
Con este van 79 #FriQuiExámenes de elaboración propia con enunciado y resolución, 36 FQ1, 32 ESO, 6 FIS2, y 5 orgánica  

No tan frikis como mis compis. Intento basarlos en elementos reales y a veces son un poco frikis. Estoy en proceso de que los ejercicios planteados en exámenes estén dentro de listas de ejercicios, sin ser propiamente exámenes. Por ejemplo esto que fue un examen real, y de friki solamente la parte de la cebra y el monje Shaolin, inicialmente lo paso a [Problemas dinámica - Presión](/home/recursos/fisica/recursos-dinamica/ProblemasFisica-Dinamica-Presion.pdf)  [Problemas dinámica - Fuerzas fluidos](/home/recursos/fisica/recursos-dinamica/ProblemasFisica-Dinamica-FuerzasFluidos.pdf)  
Intento centralizar en carpetas con pdf que incluye soluciones. Intento que sean "temáticos", en mismo examen todas las preguntas de la misma temática.  

[twitter FiQuiPedia/status/947850173845266432](https://twitter.com/FiQuiPedia/status/947850173845266432)  
![](https://pbs.twimg.com/media/DSduxs0XUAUg5gP.jpg)  
[twitter FiQuiPedia/status/989468050763239425](https://twitter.com/FiQuiPedia/status/989468050763239425)  
![](https://pbs.twimg.com/media/DbtLVtxW4AAD_kd.jpg)  
[twitter FiQuiPedia/status/1000006324464340997](https://twitter.com/FiQuiPedia/status/1000006324464340997)  
![](https://pbs.twimg.com/media/DeC7wPjW4AAEX_o.jpg)  
![](https://pbs.twimg.com/media/DeC71p9XcAAThfz.jpg)  

###   Eric López Platón ([@elprofefriki](https://twitter.com/elprofefriki))
 
[twitter elprofefriki/status/995604804574556160](https://twitter.com/elprofefriki/status/995604804574556160)  
![](https://pbs.twimg.com/media/DdEZGLqX0AE7Iqc.jpg)  

###  ([@FIKASI1](https://twitter.com/FIKASI1))
 
[twitter FIKASI1/status/1333016739064451073](https://twitter.com/FIKASI1/status/1333016739064451073)  
Mi primer #Frikiproblema sobre propiedades coligativas con temática Harry Potter. Gracias por la inspiración.  
![](https://pbs.twimg.com/media/En_TqNDXcAA1hq9?format=jpg)  

[twitter FIKASI1/status/1583855479524327424](https://twitter.com/FIKASI1/status/1583855479524327424)  
Harry Potter, el andén 9 y 3/4 y el Hogwarts Express en el examen de "Tecnología e Ingeniería" de primero de bachillerato. #frikiexamen  
![](https://pbs.twimg.com/media/Ffr7vvZWAAA0uDz?format=jpg)  

[twitter FIKASI1/status/1597260154667675648](https://twitter.com/FIKASI1/status/1597260154667675648)  
En tecnología e ingeniería tenían que inventarse un problema en el que calcularan el trabajo de una fuerza variable mediante la integral y resolver dicha integral analíticamente y usando @geogebra. Dos alumnas han diseñado este problema con su correspondiente resolución.   
![](https://pbs.twimg.com/media/FiqaDZvWQAIfx2r?format=jpg)  
![](https://pbs.twimg.com/media/FiqaDaOXkAIRRWW?format=jpg)  
![](https://pbs.twimg.com/media/FiqaDfKWIAEtwM6?format=jpg)  
![](https://pbs.twimg.com/media/FiqaDkpWQAAWa-u?format=jpg)  

### [Fisiquímicamente](https://twitter.com/fqmente/), Rodrigo Alcaraz y equipo  

[twitter fqmente/status/1582047046944854016](https://twitter.com/fqmente/status/1582047046944854016)  
Me hace una enorme ilusión anunciaros que @ProfaDeQuimica se une al equipo de #fisiquimicamente. De momento esto significa que ya podéis encontrar, enlazados en los apuntes correspondientes, sus geniales 𝕗𝕣𝕚𝕜𝕚𝕖𝕩𝕒𝕞𝕖𝕟𝕖𝕤, con contribuciones también de @quirogafyq  

[Frikiexámenes repaso química 1º Bachillerato Leticia Cabezas](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2bach/quimica/repaso-quimica-1Bach-frikiexamenes.pdf)  
[Frikiexámenes cinética química 2º Bachillerato Leticia Cabezas](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2bach/quimica/cinetica-quimica/cinetica-equilibrio-frikiexamenes.pdf)  
[Frikiexámenes movimiento Física y Química 4º ESO Leticia Cabezas](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/4eso/movimientos/movimientos-frikiexamenes.pdf)  
[Frikiexámenes fluidos energía calor Física y Química 4º ESO Leticia Cabezas](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/4eso/fluidos/fluidos-energia-calor-frikiexamenes.pdf)  

###  Gonzalo Prados ([@Gonprados](https://twitter.com/Gonprados))
 
[twitter Gonprados/status/1002211513648734210](https://twitter.com/Gonprados/status/1002211513648734210)  
![](https://pbs.twimg.com/media/DeiR75OWkAE_ylQ.jpg)  
![](https://pbs.twimg.com/media/DeiR9TPWsAEsRii.jpg)  
![](https://pbs.twimg.com/media/DeiSDuzWAAAvM-P.jpg)  

###  Iris Morey ([@fisquiris](https://twitter.com/fisquiris))
Enmarcado dentro de un proyecto de gamificación [https://fisquiris.wixsite.com/elinstitutodepapel?lang=es](https://fisquiris.wixsite.com/elinstitutodepapel?lang=es)  
[twitter fisquiris/status/1321109460727848961](https://twitter.com/fisquiris/status/1321109460727848961)  
Profes del #claustrovirtual, hoy os comparto mi primer #frikiexamen por si os sirve. Es de Cinemática (#FyQ) y está basado en 5 escenas de la serie @LaCasaDePapelTV, como contexto dentro de mi gamificación de 4º ESO.  
[https://drive.google.com/file/d/1fe8n3MnrxGClWpn7_4IP1stxPmhVCAPT/view](/drive/Frikiexamen T1 - Iris Morey.pdf)  
![](https://pbs.twimg.com/media/ElWFHqvX0AEwzVI?format=jpg)  
![](https://pbs.twimg.com/media/ElWFHrCXIAEW2l-?format=jpg)  

###  Javier Vargas ([@elfaroestelar](https://twitter.com/elfaroestelar/))
 
[twitter elfaroestelar/status/991736065693290501](https://twitter.com/elfaroestelar/status/991736065693290501)  
![](https://pbs.twimg.com/media/DcNan5gW4AAIoh5.jpg)  
![](https://pbs.twimg.com/media/DcNan5eXcAAoQyj.jpg)  
[twitter elfaroestelar/status/1003349874023325697](https://twitter.com/elfaroestelar/status/1003349874023325697)  
![](https://pbs.twimg.com/media/DeydWS2XkAAcKsm.jpg)  
![](https://pbs.twimg.com/media/DeydWSuX4AYkZsB.jpg)  

###  José Pol Lezcano ([@JosePolLezcano](https://twitter.com/josepollezcano)) 
 
[twitter JosePolLezcano/status/992463066980929536](https://twitter.com/JosePolLezcano/status/992463066980929536)  
![](https://pbs.twimg.com/media/DcXvs3qX4AsEgHo.jpg)
[twitter JosePolLezcano/status/993048243469783040](https://twitter.com/JosePolLezcano/status/993048243469783040)  
![](https://pbs.twimg.com/media/DcgCT2EW4AEsWXY.jpg)  

###  Juan Francisco ([@juanfisicahr](https://twitter.com/juanfisicahr))
 [En homenaje a Don Juan Arencibia de Torres  - estonoentraenelexamen](https://www.estonoentraenelexamen.com/2020/01/16/en-homenaje-a-don-juan-arencibia-de-torres/)  
 [https://drive.google.com/open?id=1nHRiucpnSlozi3CNKnHR8CL8sORuQ9Pt](/drive/CONTROL TIRO PARABÓLICO CAÑON TIGRE.docx)  
 ![](https://www.estonoentraenelexamen.com/wp-content/uploads/2020/01/2020-01-16-13_02_13-CONTROL-TIRO-PARABÓLICO-CAÑON-TIGRE-Guardado-por-última-vez-por-el-usuario-.png)  
 
[twitter juanfisicahr/status/1597314091043139584](https://twitter.com/juanfisicahr/status/1597314091043139584)   
Aprovechando que tenían que hacer un examen de complejos para hablarles de la llegada de las cenizas a San Cristóbal de La Laguna del gran físico canario Blas Cabrera  
![](https://pbs.twimg.com/media/FirMsIeXkAcPWH2?format=jpg)  
![](https://pbs.twimg.com/media/FirMsZAXwAUrxHZ?format=jpg)  
 
###  Joaquín C.A. ([@Ioaquinus](https://twitter.com/Ioaquinus))
 
[twitter Ioaquinus/status/1228062406435319808](https://twitter.com/Ioaquinus/status/1228062406435319808)  
Pues al final sí hubo #frikiexamen sobre Mortadelo y Filemón en 4º de ESO. Gracias a @bjmahillo y @rcabreratic por su ayuda al compartir los suyos y por la inspiración (os cito en el encabezamiento 😄👍🏻) #fisicayquimica  
![](https://pbs.twimg.com/media/EQr0PzWWkAECiQ6?format=jpg)  
![](https://pbs.twimg.com/media/EQr0PzWWoAEQcNT?format=jpg)  
[twitter Ioaquinus/status/1396881509928288260](https://twitter.com/Ioaquinus/status/1396881509928288260)  
Comparto #frikiexamen de cinemática de 4º ESO, temática “Los Vengadores”. Es un poco tarde por fechas, pero si le sirve a alguien del #claustrovirtual genial.  
![](https://pbs.twimg.com/media/E2K4SxHX0AIkYXT?format=jpg)  
![](https://pbs.twimg.com/media/E2K4SxCXsAUFn51?format=jpg)  

###  jmgm ([@erlangcola](https://twitter.com/erlangcola))
 
[twitter erlangcola/status/1126590395457126403](https://twitter.com/erlangcola/status/1126590395457126403) #frikiexamen de #4FQ sobre movimientos rectilíneos con temática de Avengers. Les ha gustado  
[![https://pbs.twimg.com/media/D6Jz9xFW0AIqND8?format=jpg](https://pbs.twimg.com/media/D6Jz9xFW0AIqND8?format=jpg "https://pbs.twimg.com/media/D6Jz9xFW0AIqND8?format=jpg") ](https://pbs.twimg.com/media/D6Jz9xFW0AIqND8?format=jpg)  
[![https://pbs.twimg.com/media/D6Jz-xmXsAElmxU?format=jpg](https://pbs.twimg.com/media/D6Jz-xmXsAElmxU?format=jpg "https://pbs.twimg.com/media/D6Jz-xmXsAElmxU?format=jpg") ](https://pbs.twimg.com/media/D6Jz-xmXsAElmxU?format=jpg) 

###  Leticia Cabezas ([@ProfaDeQuimica](https://twitter.com/ProfaDeQuimica/)) 

En 2022 comparte en web de FisiQuímicamente en pdf (web de Rodrigo Alcaraz y colaboradores, la coloco alfabéticamente por la F) , los enlazo desde allí, puede haber algunos repetidos, allí en pdf y aquí en imágenes de tuits    
En 2023 comparte [Frikiejercicios de Química (1° BACH.) Ejercicios de Química contextualizados en cine y series de TV.](https://padlet.com/laprofadequimica/frikiejercicios-de-qu-mica-1-bach-71yhrzyqrf4v)  

(Pongo tuits, hay gran parte en hilo, pongo enlace al primero y otros, pero hilo va creciendo)  
[twitter ProfaDeQuimica/status/940617662064930816](https://twitter.com/ProfaDeQuimica/status/940617662064930816)  
Mi examen de 4° ESO de #Física de hoy #profesfrikis #examenfriki #soyfriki #geekphysics [![](https://pbs.twimg.com/media/DQ2-wyBXcAAd0D1.jpg "") ](https://pbs.twimg.com/media/DQ2-wyBXcAAd0D1.jpg)  
[twitter ProfaDeQuimica/status/966975602296320000](https://twitter.com/ProfaDeQuimica/status/966975602296320000)  
![](https://pbs.twimg.com/media/DWtjJhVWAAAdqFq.jpg)  
[twitter ProfaDeQuimica/status/974683408700977152](https://twitter.com/ProfaDeQuimica/status/974683408700977152)  
![](https://pbs.twimg.com/media/DYbFX0TXkAAHc6t.jpg)  
[twitter ProfaDeQuimica/status/989188029997899781](https://twitter.com/ProfaDeQuimica/status/989188029997899781)  
Mi examen de 3° ESO de #Física de hoy 😎. El primero temático (#LOTR) y en este grupo. Pese al miedo a que fuera un fracaso, los resultados han sido mejores que de costumbre. 👏🏻 #profesfrikis #soyfriki #geekphysics #friki #frikiexamen #frikiprofe #compartidmalditos  
![](https://pbs.twimg.com/media/DbpNP-8XkAAR9Ed.jpg)  
[twitter ProfaDeQuimica/status/1057298620423290881](https://twitter.com/ProfaDeQuimica/status/1057298620423290881)  
Primer #frikiexamen del curso. Primero en mi nuevo centro y primero en Química de 2° Bachillerato. Con temática de #Halloween (como no podía ser de otra forma por las fechas) por petición de algunos alumnos.  
![](https://pbs.twimg.com/media/DqxHdoGX4AAueow.jpg)  
![](https://pbs.twimg.com/media/DqxHenrXcAIREWS.jpg)  
[twitter ProfaDeQuimica/status/1060224551886438400](https://twitter.com/ProfaDeQuimica/status/1060224551886438400)  
Segundo #frikiexamen del curso: #Cinemática y #Dinámica de 4° #ESO. #Batman vuelve a hacer acto de presencia (las pelis de persecuciones no son lo mío... 🤣) Pero #InfinktyWar y la 3ª temporada de #TheFlash son candente actualidad en mi filmoteca 😉.#SoyFriki #ProfesFrikis  
![](https://pbs.twimg.com/media/DrashaeX4AAADEF.jpg)  
![](https://pbs.twimg.com/media/DrasmLFWwAA8nxs.jpg)  
[twitter ProfaDeQuimica/status/1064872196395474948](https://twitter.com/ProfaDeQuimica/status/1064872196395474948)  
Segundo #frikiexamen para #Química de 2° #Bachillerato. El avatar de @octavio_pr (y comentar que podría ser buen tema para sus próximas actividades contextualizadas) me dio la idea. Todos son ejercicios EvAU de Madrid.#soyfriki #frikiprofes #breakingbad  
![](https://pbs.twimg.com/media/Dscvg55W0AAXpqH.jpg)  
![](https://pbs.twimg.com/media/DscvkBEWwAAfdfu.jpg)  
[twitter ProfaDeQuimica/status/1065355093866893312](https://twitter.com/ProfaDeQuimica/status/1065355093866893312)  
Mi particular homenaje a Stan Lee en Mecánica + Gravitación de 4° ESO.#Marvel #StanLee #Avengers #InfinityWar #Thor #Física #profesfrikis #soyfriki #comics #físicadivertida #frikiexamen #frikiprofes  
![](https://pbs.twimg.com/media/Dsjmy1aXQAAOT__.jpg)  
![](https://pbs.twimg.com/media/DsjmzaHX4AA0gFy.jpg)  
[twitter ProfaDeQuimica/status/1091004799548641280](https://twitter.com/ProfaDeQuimica/status/1091004799548641280)  
Primer #frikiexamen del trimestre: Electroquímica y Redox en #Química de 2° Bach. Un poco de Little Nicky, @BreakingBad_AMC, @juegotronosplus y War Machine y Electro de @MarvelSpain.La semana que viene, más.#Friki #SoyFriki #ProfesFrikis #Frikiprofes  
![](https://pbs.twimg.com/media/DyQHD09XgAAClV6.jpg)  
![](https://pbs.twimg.com/media/DyQHEmMWwAAIdu5.jpg)  
[twitter ProfaDeQuimica/status/1093564827107319808](https://twitter.com/ProfaDeQuimica/status/1093564827107319808)  
Nuevo #frikiexamen temático: La #Física de la película "El retorno del rey", que adapta la obra de #Tolkien, el gran maestro de la literatura fantástica.¡Espero que os guste!#soyfriki #profesfrikis #friki #frikiexámenes #LOTR #ESDLA@ElAnilloUnico @TolkienRivendel @SocTolkien  
![](https://pbs.twimg.com/media/Dy0fZYIX4AYDaCI.jpg)  
![](https://pbs.twimg.com/media/Dy0fZ9aXcAAqG4-.jpg)  
[twitter ProfaDeQuimica/status/1100506259185455105](https://twitter.com/ProfaDeQuimica/status/1100506259185455105)  
Exportando el carnaval a los Madriles: Examen de Química de 2° de Bachillerato inspirado en agrupaciones de las semifinales del #COAC2019.Aparecen @comparsaobdc @davidcarapapa @ChirigotaBizc8 y @Comparsa41500.¡Espero que os guste! 😊#YoTambiénSoyCarneDeFebrero #COAC2019S3  
![](https://pbs.twimg.com/media/D0XIl5xWwAA7Vvb.jpg)  
![](https://pbs.twimg.com/media/D0XImkaWwAIFKJT.jpg)  
[twitter ProfaDeQuimica/status/1101190527633014786](https://twitter.com/ProfaDeQuimica/status/1101190527633014786)  
Último #frikiexamen del trimestre: Hidrostática, Energía, Átomo y Sistema Periódico de 4° ESO. Dos temáticas: series de TV y Titanic.Que sí. Que Jack cabía de sobra en la tabla. Pero... ¿se habrían salvado ambos de haberse subido? ¡Que hable Arquímedes!  
![](https://pbs.twimg.com/media/D0g27zsW0AApb-9.jpg)  
![](https://pbs.twimg.com/media/D0g28VaW0AEEvHm.jpg)  
[twitter ProfaDeQuimica/status/1195263565013422082](https://twitter.com/ProfaDeQuimica/status/1195263565013422082)  
Nuevo #frikiexamen de #Física de 2° Bachillerato.. No podía dejar pasar el año del #Bicentenario del @museodelprado sin el homenaje. (Dedicado a las compis @bjmahillo y @Biochiky, que ya se inspiraron en esta temática, y a @inmitacs, cuya visita en julio me inspiró para ello. 😊)  
![](https://pbs.twimg.com/media/EJZt0CFXkAEBeu_?format=jpg)  
![](https://pbs.twimg.com/media/EJZt2OdXsAENy1f?format=jpg)  
[twitter ProfaDeQuimica/status/1187421696464228353](https://twitter.com/ProfaDeQuimica/status/1187421696464228353)  
Primer #frikiexamen del curso. Me estreno en #Física de #2ºBachillerato con el bloque de #Gravitación usando #AvengersEndgame como hilo conductor. He tratado de no hacer mucho spoiler...  😅  
![](https://pbs.twimg.com/media/EHqRvACWkAEySda?format=jpg)  
![](https://pbs.twimg.com/media/EHqRvANWsAA3DCx?format=jpg)  
[twitter ProfaDeQuimica/status/1191766604704571392](https://twitter.com/ProfaDeQuimica/status/1191766604704571392)  
Primer #frikiexamen de Química de 1° de Bachillerato para este curso (llevaba dos años sin impartir este nivel y le tenía muchas ganas). Inspirado en @GameOfThrones  
![](https://pbs.twimg.com/media/EIoBZfLXsAAxXzN?format=jpg)  
![](https://pbs.twimg.com/media/EIoBZ-KXsAErQcG?format=jpg)  
[twitter ProfaDeQuimica/status/1200341431665274880](https://twitter.com/ProfaDeQuimica/status/1200341431665274880)  
Hoy, varias escenas del capítulo 1x01 de @themandalorian, del universo @StarWarsSpain, nos han servido de contexto en otro #frikiexamen de Física de 2° de Bachillerato.Baby Yoda incluido.  
![](https://pbs.twimg.com/media/EKh4HknW4AAUnxs?format=jpg)  
![](https://pbs.twimg.com/media/EKh4JUfWsAE78z-?format=jpg)  
[twitter ProfaDeQuimica/status/1204815008846221313](https://twitter.com/ProfaDeQuimica/status/1204815008846221313)  
Último #frikiexamen del trimestre en Física y Química de 1° de #Bachillerato. Dedicado al universo literario de #HarryPotter (ellos me dieron la idea al hablarme del Expecto Patronum hace varias semanas). Porque los de Ciencias también leemos 😉#potterhead #friki #soyfriki  
![](https://pbs.twimg.com/media/ELhc2j8XsAAAb3W?format=jpg)  
![](https://pbs.twimg.com/media/ELhc2_CWsAEhWZW?format=jpg)  
[twitter ProfaDeQuimica/status/1216466687895199746](https://twitter.com/ProfaDeQuimica/status/1216466687895199746)  
Frikiexamen navideño de #Física de 2° de Bachillerato (#Magnetismo) que no había subido aún aquí. Descubrí esta maravilla de comedia delirante en diciembre (la tenéis en @NetflixES).En V.O. cuenta con las voces de Hugh Laurie y James McAvoy.Os la recomiendo mucho.  
![](https://pbs.twimg.com/media/EOHB_TCXkAEIlvU?format=jpg)  
![](https://pbs.twimg.com/media/EOHB_6KWsAMGvfW?format=jpg)  
[twitter ProfaDeQuimica/status/1230407190575538176](https://twitter.com/ProfaDeQuimica/status/1230407190575538176)  
Hoy estamos en vísperas de Carnaval con el #frikiexamen de 1° Bach.:"La Química de la final del COAC 2020".Enhorabuena a @LaChiriDeFyA, @comparsatino, @elseludecadi, @ComparsaAres y los demás por el pase y gracias por hacernos soñar.#COAC2020Final #COAC2020FCC: @josemaguilera  
![](https://pbs.twimg.com/media/ERNIzH4WAAAxqvk?format=jpg)  
![](https://pbs.twimg.com/media/ERNIz0rXYAAOHwX?format=jpg)  
[twitter ProfaDeQuimica/status/1230872502483800064](https://twitter.com/ProfaDeQuimica/status/1230872502483800064)  
Aquí va mi personal homenaje, en forma de #frikiexamen (con vídeo incluido), a una serie que me ha parecido una auténtica obra de arte y ha ascendido a mi podio seriéfilo #Daredevil. ❤️#Ondas y #sonido de #Física de 2° de Bachillerato. #EvAU#SaveDaredevil #Marvel @NetflixES  
![](https://pbs.twimg.com/media/ERTwAFjWkAEhWXd?format=jpg)  
![](https://pbs.twimg.com/media/ERTwAsnWoAAEZxP?format=jpg)  
[twitter ProfaDeQuimica/status/1238012972397723648](https://twitter.com/ProfaDeQuimica/status/1238012972397723648)  
Cambio de tercio.En riguroso directo, #frikiexamen de #FyQ 1° de #Bachillerato (#Cinemática) sobre... #Daredevil.Ellos también merecían uno. Y ya desde el principio de la serie me enamoró la escena del extintor (episodio 1x02) como ejercicio de caída libre. ❤️#SaveDaredevil  
![](https://pbs.twimg.com/media/ES5OOPlXkAAtqw4?format=jpg)  
![](https://pbs.twimg.com/media/ES5OO0xX0AAXH0I?format=jpg)  
[twitter ProfaDeQuimica/status/1313491497883504642](https://twitter.com/ProfaDeQuimica/status/1313491497883504642)  
1er #frikiexamen del curso 20/21 para #Química de #2Bachillerato: repaso de nomenclatura inorgánica, disoluciones y estequiometría. Si otros no les hacen recomendaciones de lectura de cómics, aquí viene La Profa a remediarlo: #ElHombreSinMiedo, de Miller y Romita Jr. #Daredevil  
![](https://pbs.twimg.com/media/Ejp1jNCWoAAWjha?format=jpg)  
![](https://pbs.twimg.com/media/Ejp1jidXgAAfw3X?format=jpg)  
[twitter ProfaDeQuimica/status/1318496609735421953](https://twitter.com/ProfaDeQuimica/status/1318496609735421953)  
1er #frikiexamen de #Física y #Química de 1° de Bachillerato (y probablemente de su vida) del curso y 1° con temática de #rol para mí. Como no podía ser de otra forma, con #DungeonsAndDragons.  Se inspira en una escena que jugué con mis queridos #LosChucas el pasado verano.  
![](https://pbs.twimg.com/media/Ekw9rMGW0AAd7m9?format=jpg)  
![](https://pbs.twimg.com/media/Ekw9raYXYAUwEPx?format=jpg)  
[twitter ProfaDeQuimica/status/1323258764649222144](https://twitter.com/ProfaDeQuimica/status/1323258764649222144)  
Esta mañana, mis chavales de 2° #Bachillerato han realizado su #frikiexamen de #CinéticaQuímica y #EquilibrioQuímico. Como ya me conocen, sabían que por la fecha tocaba temática de #Halloween, así que seguimos con #rol de #terrorcósmico con #LaLlamadaDeCthulhu, de @Edge_Ent  
![](https://pbs.twimg.com/media/El0o1D3XYAU-Oe3?format=jpg)  
![](https://pbs.twimg.com/media/El0o1TgXIAI2Xc6?format=jpg)  
[twitter ProfaDeQuimica/status/1329356240095424512](https://twitter.com/ProfaDeQuimica/status/1329356240095424512)  
Hoy ha tocado #frikiexamen de Disoluciones y Estequiometría en 1º de Bachillerato. En esta ocasión se ha basado en el capítulo 2x02 de #TheMandalorian. Porque fue un capítulo lleno de cuquez, porque la serie me flipa mogollón y porque Yodita merece eso y más.  #ThisIsTheWay  
![](https://pbs.twimg.com/media/EnJW-OiXEAEDbMO?format=jpg)  
![](https://pbs.twimg.com/media/EnJW-OaXYAExy3H?format=jpg)  
[twitter ProfaDeQuimica/status/1394599493316792323](https://twitter.com/ProfaDeQuimica/status/1394599493316792323)  
Hoy he tenido el último #frikiexamen de #Física del curso con los de 1° de Bachillerato: #Energía y #MAS. Y la temática, #Madagascar2, ha sido una sugerencia de mi grupo rolero, el GRC. ¡Muchas gracias por el chivatazo!  
![](https://pbs.twimg.com/media/E1qc2SEXIAY64Gz?format=jpg)  
[twitter ProfaDeQuimica/status/1367394424393711629](https://twitter.com/ProfaDeQuimica/status/1367394424393711629)  
Último #frikiexamen del trimestre. Este para 1° de #Bachillerato (elementos matemáticos de la Cinemática, MRU, MRUA y MCU).  
Y con el fangirleo que tengo últimamente, no he podido evitar tematizarlo en #Hamilton. ❤️  
![](https://pbs.twimg.com/media/Evn1-fmXcAA6Azu?format=jpg)  
![](https://pbs.twimg.com/media/Evn1-07WQAEqXpF?format=jpg)  
[twitter ProfaDeQuimica/status/1446417320537206786](https://twitter.com/ProfaDeQuimica/status/1446417320537206786)  
1er #frikiexamen del curso 21/22, en mi nuevo IES y con este alumnado de 2° Bach.: repaso de formulación inorgánica, disoluciones, gases y estequiometría. Y basado en una de mis películas preferidas, #Stardust. #claustrovirtual  
En riguroso directo parece que se está dando bien.  
![](https://pbs.twimg.com/media/FBK04htXsAMYu5A?format=jpg)  
![](https://pbs.twimg.com/media/FBK04xcXsAIYZ66?format=jpg)  
[twitter ProfaDeQuimica/status/1592500580085542914](https://twitter.com/ProfaDeQuimica/status/1592500580085542914)  
Últimamente la BIDA™️ me tiene vapuleada, así que hasta hoy no he podido confeccionar el primer #frikiexamen del curso: gases, disoluciones y reacción química de 1° de #Bachillerato basado en #Frozen. Lo comparto con licencia @creativecommons.  
\#ClaustroVirtual #Química  
![](https://pbs.twimg.com/media/Fhmy1ROXEAE18MU?format=jpg)  
![](https://pbs.twimg.com/media/Fhmy1jvWYAAEcoO?format=jpg)  
[twitter ProfaDeQuimica/status/1665121921866842114](https://twitter.com/ProfaDeQuimica/status/1665121921866842114)  
Aquí va el único #frikiexamen inédito que he podido hacer este curso. Poco tiempo y mucha formación realizada (la mayoría... inútil), impartida (muy interesante) y proyectos finalizados o en marcha (maravillosos).  
Dinámica clásica de 3º ESO con #LosSimpson y "Muellín".  
![](https://pbs.twimg.com/media/Fxuziw1XgAECP3O?format=jpg)  
![](https://pbs.twimg.com/media/Fxuziw1XoAMmmBx?format=jpg)  
[twitter ProfaDeQuimica/status/1728022378033160539](https://twitter.com/ProfaDeQuimica/status/1728022378033160539)  
\#Frikiexamen de #Química (1° #Bachillerato). Evalúo criterios 1.1, 1.2, 2.2, 2.3 y 3.1 usando saberes 1 y 3 del bloque B (moles, composición centesimal, fórmula empírica y molecular, mezclas de gases y propiedades de disoluciones).  
Temática: #SamuráiDeOjosAzules #BlueEyeSamurai  
![](https://pbs.twimg.com/media/F_srL-tWIAARsI9?format=jpg)  
![](https://pbs.twimg.com/media/F_srMKoW8AAdaxE?format=jpg)  

###  Olga M Herranz ([@herranz_olga](https://twitter.com/herranz_olga))
 
[twitter herranz_olga/status/1087427557736226816](https://twitter.com/herranz_olga/status/1087427557736226816)  
Primer #frikiexamen que pongo a los chicos de mi tutoría Y todo gracias a las ideas y friki exámenes de @ProfaDeQuimica @profedefyq @bjmahillo @Biochiky Poco a poco tendrán más nivel  😀  
![](https://pbs.twimg.com/media/DxdRkufWsAo8B9D?format=jpg)  
![](https://pbs.twimg.com/media/DxdRlYFX0AAGZWQ?format=jpg)  
![](https://pbs.twimg.com/media/DxdRmC4X0AAghm1?format=jpg)  

###  M. Angeles Calatrava ([@mari6cs](https://twitter.com/mari6cs))
 
[twitter mari6cs/status/1092039275267407877](https://twitter.com/mari6cs/status/1092039275267407877)  
Aquí dejo unos ejercicios basados en los frikiexamenes de @ProfaDeQuimica y @bjmahillo, me han parecido geniales y los he adaptado a ejercicios de repaso antes de los exámenes, a ver si me animo a ponerles ya algún #frikiexamen  
![](https://pbs.twimg.com/media/Dyez7CsWkAAQnqA.jpg)  
![](https://pbs.twimg.com/media/Dyez7CvWsAAvu6X.jpg)  
[twitter mari6cs/status/1096714776653123584](https://twitter.com/mari6cs/status/1096714776653123584)  
Por fin un rato para subir mis primeros #frikiejercicios basados en los #frikiexamenes que descubrí por aquí, estos son de fuerzas de 4 ESO, espero que le sirvan a alguien   
![](https://pbs.twimg.com/media/DzhQRX_XQAAHVZH.jpg)  
![](https://pbs.twimg.com/media/DzhQRX-XgAEM8DZ.jpg)  
[twitter mari6cs/status/1096716816515244032](https://twitter.com/mari6cs/status/1096716816515244032)  
Y después de Superman pasamos a Thor para unos #frikiejercicios del movimiento circular Por ahora mis preferidos son los de fluidos se ve que con la influencia de @ProfaDeQuimica y de @bjmahillo y demás se va mejorando  
![](https://pbs.twimg.com/media/DzhSIK8WsAADCRg.jpg)  
![](https://pbs.twimg.com/media/DzhSIK1WsAAvHGo.jpg)  

###  Mónica Bermejo ([@monicandmoon](https://twitter.com/monicandmoon))
 
[twitter monicandmoon/status/1085807001748324352](https://twitter.com/monicandmoon/status/1085807001748324352)  
¡Me encanta! Compartimos pasiones #Física y #StarWarsYo hice un escape room como repaso y con premio incluido 🤩  
![](https://pbs.twimg.com/media/DxGPtO7WsAA-Qj5.jpg)  

###  Miguel Quiroga Boveda ([@quirogafyq](https://twitter.com/quirogafyq))
 
En octubre 2019 publica en su web [https://sites.google.com/view/eldelafisicaylaquimica/frikiexámenes?authuser=0](https://sites.google.com/view/eldelafisicaylaquimica/frikiex%C3%A1menes?authuser=0)  
[twitter BovedaQuiroga/status/1064264137944023043](https://twitter.com/BovedaQuiroga/status/1064264137944023043)  
Mi primer #frikiexamen de física y química para 2º de ESO, todo un éxito! Gracias @ProfaDeQuimica por servirme de inspiración  
![](https://pbs.twimg.com/media/DsUFo4sW0AEmhOu.jpg)  
![](https://pbs.twimg.com/media/DsUFo4yW0AAkML8.jpg)  
![](https://pbs.twimg.com/media/DsUFs5YWsAAN7PC.jpg)  
[twitter quirogafyq/status/1066414974447300608](https://twitter.com/quirogafyq/status/1066414974447300608)  
Boletín de ejercicios sobre disoluciones basado en Harry Potter para 2º ESO.Inspirado por @vielbein.#frikiexamen  
![](https://pbs.twimg.com/media/DsyqbrHWoAUzWfq.jpg)  
[twitter quirogafyq/status/1077882922617786368](https://twitter.com/quirogafyq/status/1077882922617786368)  
Examen de Física y Química para 2º ESO, este sobre los #Minions. Disoluciones, clasificación de la materia y modelos atómicos.#frikiexamen #frikiprofe  
![](https://pbs.twimg.com/media/DvVoRc7WwAAKqHD.jpg)  
![](https://pbs.twimg.com/media/DvVoVKZW0AAreXz.jpg)  
![](https://pbs.twimg.com/media/DvVoVj0WkAAaiGP.jpg)  
[twitter quirogafyq/status/1089590533012553730](https://twitter.com/quirogafyq/status/1089590533012553730)  
Aquí va mi #frikiexamen para la recuperación del primer trimestre de Física y Química de 2ºESO, este sobre dibujos animados, #Disney , #LosSimpsons , #DragonBall, #rickAndMorty ... Un poco de todo. 😆  
![](https://pbs.twimg.com/media/Dx7_fvRX4AEXZ5Q.jpg)  
![](https://pbs.twimg.com/media/Dx7_9W-WsAIV0GQ.jpg)  
![](https://pbs.twimg.com/media/Dx8AFoSX4AEZ2fR.jpg)  
![](https://pbs.twimg.com/media/Dx8AG9tX4AMwX4a.jpg)  
[twitter quirogafyq/status/1102978236550733825](https://twitter.com/quirogafyq/status/1102978236550733825)  
Y seguimos con mas #frikiexamen. Este sobre arte! Su cara fué genial cuando lo vieron. Creo que no se lo esperaban para nada. Este para 2º de ESO, estructura de la materia y cambios químicos.  
![](https://pbs.twimg.com/media/D06QManWkAIMulh.jpg)  
![](https://pbs.twimg.com/media/D06QMbcXcAM1edR.jpg)  
![](https://pbs.twimg.com/media/D06QW_PWwAYOBuH.jpg)  
[twitter quirogafyq/status/1111679621005787142](https://twitter.com/quirogafyq/status/1111679621005787142)  
¿Examen de cinemática? NO! Mejor #frikiexamen de cinemática! Esta vez sobre El señor de los anillos. @ESDLA_Fans.  
![](https://pbs.twimg.com/media/D2159-AX4AAEK2C.jpg)  
![](https://pbs.twimg.com/media/D215_8hW0AAPGpF.jpg)  
![](https://pbs.twimg.com/media/D216BNAX0AI14xv.jpg)  
![](https://pbs.twimg.com/media/D216CWrWsAAtnLu.jpg)  
[twitter quirogafyq/status/1131943140112392194](https://twitter.com/quirogafyq/status/1131943140112392194)  
Vaya alegría al ver que hoy @PirataYSuBanda de @RockFM_ES ha hablado de los #fikiexamenes! Sois la caña! Mil gracias gente Además el útlimo es sobre rock!!! Mejor imposible!! Espero que os guste!  
![](https://pbs.twimg.com/media/D7V36AMW4AAjoJs.jpg)  
![](https://pbs.twimg.com/media/D7V38NuWkAEcAwV.jpg)  
![](https://pbs.twimg.com/media/D7V38uqWkAUmn85.jpg)  
![](https://pbs.twimg.com/media/D7V39O2XoAAeX8k.jpg)  
[twitter quirogafyq/status/1127504495863369728](https://twitter.com/quirogafyq/status/1127504495863369728)  
Unos alumnos se han quedado en el mundo del revés durante la 2ª Evaluación. Toca #Frikiexamen de recuperación de Física y Química. Esta vez sobre #StrangerThings. @Stranger_Things @NetflixES  
![](https://pbs.twimg.com/media/D6WyqWrX4AAzPBZ?format=jpg)  
![](https://pbs.twimg.com/media/D6WysP-X4AEkuek?format=jpg)    
[twitter quirogafyq/status/1208733137808842752](https://twitter.com/quirogafyq/status/1208733137808842752)  
Ahora con un poco mas de tiempo comparto el #frikiexamen de 4º de ESO sobre los dioses del Olimpo!, enlace químico y química orgánica. Creo que se me empieza a ir de las manos...  
![](https://pbs.twimg.com/media/EMZIYHZWoAEJKgJ?format=jpg)  
![](https://pbs.twimg.com/media/EMZIYHdW4AAdLfc?format=jpg)  
![](https://pbs.twimg.com/media/EMZIYzvW4AAr1QW?format=jpg)  
![](https://pbs.twimg.com/media/EMZIYzxXYAYzeyH?format=jpg)  
![](https://pbs.twimg.com/media/EMZIZqUWoAALqOI?format=jpg)  
[twitter quirogafyq/status/1201442628790865920](https://twitter.com/quirogafyq/status/1201442628790865920)  
Primer #frikiexamen de física y química para 4º de ESO. Ha tocado #Batman para trabajar estructura atómica, configuración electrónica y diagramas de Lewis!. #frikiexamenes  
![](https://pbs.twimg.com/media/EKxhr8PWwAU6KVQ?format=jpg)  
![](https://pbs.twimg.com/media/EKxhr8RX0AEUyT3?format=jpg)  
[twitter quirogafyq/status/1204418920284852229](https://twitter.com/quirogafyq/status/1204418920284852229)  
Y con este #frikiexamen comenzaron su aventura por el mundo friki mis pequeños de 2º de ESO de Física y Química. Creo que ya son fans de los #frikiexamenes. Este sobre @TheSimpsons @neox  
![](https://pbs.twimg.com/media/ELb0mm7UwAE9pLU?format=jpg)  
![](https://pbs.twimg.com/media/ELb0mnJU4AAAMZx?format=jpg)  
[twitter quirogafyq/status/1330896389069484032](https://twitter.com/quirogafyq/status/1330896389069484032)  
Inicio en el mundo de los #frikiexamen de mi alumnado de 1ºBach. Propiedades coligativas y cantidad de sustancia con juego de tronos! #GameOfThrones #juegodetronos  Valar moghulis.  
[https://cutt.ly/qhtjOW2](https://cutt.ly/qhtjOW2)  
[miguelquiroga.es frikiexámenes 2020-2021 1º-bach](https://www.miguelquiroga.es/frikiex%C3%A1menes/2020-2021/1%C2%BA-bach)  
![](https://pbs.twimg.com/media/EnY1A0IXIAE3rkB?format=jpg)  
![](https://pbs.twimg.com/media/EnY1Az_WEAIdjjy?format=jpg)  
![](https://pbs.twimg.com/media/EnY1Az_XIAQ7zfm?format=jpg)  
![](https://pbs.twimg.com/media/EnY1A0LXMAAVqwu?format=jpg)  
[twitter quirogafyq/status/1250015907214176256](https://twitter.com/quirogafyq/status/1250015907214176256)  
Siguen los #frikiejercicios. En esta playlist resuelvo unos de dinámica. Dirty dancing, el señor de los anillos, iron man... Comparto también pdf con los enunciados  
[Dinámica - Miguel Quiroga - youtube.](https://youtube.com/playlist?list=PLgyDmmD7nqEtoZV5SH67kVwoGYDrdfKjc)  
[https://drive.google.com/drive/folders/1P7dJjYsB4HkMG85DdscrNIkJM4UYhN6Q?usp=sharing](https://drive.google.com/drive/folders/1P7dJjYsB4HkMG85DdscrNIkJM4UYhN6Q?usp=sharing)  profesqueayudan  
![](https://pbs.twimg.com/media/EVjymkxXYAI7NOf?format=jpg)  


###  MPV ([@puente84](https://twitter.com/puente84))
 
[twitter puente84/status/1197551376315887617](https://twitter.com/puente84/status/1197551376315887617)  
Hoy por fin me he estrenado con un #frikiexamen en 2º de ESO !  A partir de ahora soy la profe que hace spoiler de los #Minions  🤣 Todavía me estoy riendo de la cara con la que se han quedado...  
![](https://pbs.twimg.com/media/EJ6On3iWwAEGsuz?format=jpg)  
![](https://pbs.twimg.com/media/EJ6On3ZWkAQslYe?format=jpg)  
![](https://pbs.twimg.com/media/EJ6On3aXkAAw4LY?format=jpg)  
![](https://pbs.twimg.com/media/EJ6On3bXUAA-6il?format=jpg)  

###  Pablo - Física y Química ([@pablofcayqca](https://twitter.com/pablofcayqca))
 
[twitter pablofcayqca/status/1124319262619860992](https://twitter.com/pablofcayqca/status/1124319262619860992)  
Pues no tenía yo ganas de apuntarme a los #frikiexamen de @bjmahillo Aquí va el que he preparado para la Unidad de Dinámica en 4°ESO 😁:  
![](https://pbs.twimg.com/media/D5piaCVWsAE3_wv?format=jpg)  
![](https://pbs.twimg.com/media/D5piaPRWsAIsIFI?format=jpg)  

###  Nerea ([@MissBoutureira](https://twitter.com/MissBoutureira/))
 
[twitter MissBoutureira/status/1000689950751428608](https://twitter.com/MissBoutureira/status/1000689950751428608)  
Mis alumnos de 4ºESO han hecho una prueba de Superman vs Newton. Aplicaban sus conocimientos de física para ayudar a Superman o enseñarle que se equivocaba. Todos disfrutamos y ellos afrontaron con más emoción el examen. Podéis verlo aquí:  
[Superman contra Newton - scienceisanadventure.blogspot.com.es](http://scienceisanadventure.blogspot.com.es/2018/05/superman-contra-newton.html)  
![](https://3.bp.blogspot.com/-yQTrzR3Nsvw/WwqJu-fVI9I/AAAAAAAACDE/a6gj-o7HJbIZ8Vdy2d3GTaOh_RPlGpgNACLcBGAs/s400/0001.jpg)  
![](https://4.bp.blogspot.com/-ViAArZdJNRM/WwqJuzrLCmI/AAAAAAAACDA/BClTHHs-w0oY9PDq8bGwwMLj82olVX9NQCLcBGAs/s400/0002.jpg)  
![](https://1.bp.blogspot.com/-CtOkPyBZBWw/WwqJuoJ8kGI/AAAAAAAACC8/5LJQ-AQVUqMAKOq2oBfu9qQ5KN_lMpwEACLcBGAs/s400/0003.jpg)  
![](https://1.bp.blogspot.com/-sGG1aSonJ3M/WwqJvv4P0eI/AAAAAAAACDI/_MJpAYvg4QAI-tY7njw2XpiLyg--9UKPQCLcBGAs/s400/0004.jpg)  
[twitter MissBoutureira/status/1003655402322612224](https://twitter.com/MissBoutureira/status/1003655402322612224)  
![](https://pbs.twimg.com/media/De2zQzdWkAA_YIw.jpg)  

###  Pablo Santos ([@pablo_sf](https://twitter.com/pablo_sf))
[twitter pablo_sf/status/1072566780143509504](https://twitter.com/pablo_sf/status/1072566780143509504)  
Siguiendo el ejemplo de @ProfaDeQuimica me he liado la manta a la cabeza y he preparado un par de #frikiexámenes el segundo es mañana y ya lo publico... le cogido el gustillo  
![](https://pbs.twimg.com/media/DuKFyvfWsAQAjl4.jpg)  
![](https://pbs.twimg.com/media/DuKFyvmX4AAto53.jpg)  

###  @profesorafyq ([@profesorafyq](https://twitter.com/profesorafyq))
[twitterprofesorafyq/status/1198876005450039296](https://twitter.com/profesorafyq/status/1198876005450039296)  
Ahí va el frikiexamen que están haciendo ahora mismo bajo supervisión de mi adorado mentor, @profedefyq . Vamos, que lo cuelgo "live" total, je.#frikiexamenes #fisicayquimica #1Bach  
![](https://pbs.twimg.com/media/EKNDVztXUAAj2oK?format=jpg)  
![](https://pbs.twimg.com/media/EKNDVzsXsAE40eS?format=jpg)  

###  Radical Barbatilo ([@JGilMunoz](https://twitter.com/JGilMunoz))
 
[twitter JGilMunoz/status/1108459841595551745](https://twitter.com/JGilMunoz/status/1108459841595551745)  
Me uno a la moda con este #FrikiExamen de Física con #DragonBall [@JGilMunoz FrikiExamenDragonBall Física CampoGravitatorioYElectrico.pdf](https://drive.google.com/open?id=1gHyJJhM6SJvT_-mNaIIl5Otbl29EsJIh)  
![](https://1.bp.blogspot.com/-JlNm9aZ-N-0/XJKRH9ofZCI/AAAAAAABXi4/ejxXzYdLMQgn9U57AarzdcNRPU1VMSuxwCEwYBhgL/s1600/%40JGilMunoz_FrikiExamen_F%C3%ADsica_CampoGravitatorioYElectrico01.jpg)  
![](https://1.bp.blogspot.com/--pFGD73aLmM/XJKRJXyeCmI/AAAAAAABXi8/QqTbGANsYm8bTiV5iffZVwDYZmV5giL3gCEwYBhgL/s1600/%40JGilMunoz_FrikiExamen_F%C3%ADsica_CampoGravitatorioYElectrico02.jpg)  

###  Rafael Cabrera ([@rcabreratic](https://twitter.com/rcabreratic))
 
[twitter rcabreratic/status/1223580127805747200](https://twitter.com/rcabreratic/status/1223580127805747200)  
Hola Beatríz, no había visto el tuyo. Me parece muy bueno. Yo tenía la misma reacción de la nitroglicerina y la había trabajado con mis alumnos de 1º Bachillerato desde un punto de vista de termoquímica y le he dado otro enfoque.  
[Mortadelo y Filemón](https://docs.google.com/document/d/1a5q5U2eYFVZbt0T8ByQvJc1m0NZ849oIdnnnVm0rYlI/edit) 

###  Rocío Calzado ([@rcalzadom](https://twitter.com/rcalzadom))
 
[twitter rcalzadom/status/1076604465292161024](https://twitter.com/rcalzadom/status/1076604465292161024)  
Me apetecía intentar un #frikiexamen y, de momento, un ejercicio de campo gravitatorio para #Física de 2º de bachillerato. ¿qué os parece? Creo que algunos de mis alumnos se soñaron con el examen....🤦‍♀️  
![](https://pbs.twimg.com/media/DvDdMl1X4AA64GZ.jpg)  

###  Rubén Contreras ([@quimicahipolito](https://twitter.com/quimicahipolito)) 
 
[twitter quimicahipolito/status/986946474155106304](https://twitter.com/quimicahipolito/status/986946474155106304)  
Mira a ver si te gusta este de juego de tronos.  
![](https://pbs.twimg.com/media/DbJWhfJXUAAAVDb.jpg)  
![](https://pbs.twimg.com/media/DbJWjs0WAAESGgS.jpg)  

[twitter quimicahipolito/status/987688916114427905](https://twitter.com/quimicahipolito/status/987688916114427905)  
Otro examen, esta vez del Titanic y la presión y el empuje en 4°ESO. Con foto introducción del museo de Belorado, mi pueblo y su exposición del Titanic. @bjmahillo @MUSEOBELORADO  
![](https://pbs.twimg.com/media/DbT5w-YXkAAUG1C.jpg)  
![](https://pbs.twimg.com/media/DbT5zjaX0AAy_Q0.jpg)  
[twitter quimicahipolito/status/991379947234185223](https://twitter.com/quimicahipolito/status/991379947234185223)  
![](https://pbs.twimg.com/media/DcIWv29W4AErHjH.jpg)  
![](https://pbs.twimg.com/media/DcIWx7XXcAEXFK2.jpg)  
[twitter quimicahipolito/status/1181318945787301889](https://twitter.com/quimicahipolito/status/1181318945787301889)  
Retorno a los frikiexamenes Examen de 4°ESO de #MortadeloyFilemón.  
![](https://pbs.twimg.com/media/EGTjS-dVUAAcvtw?format=jpg)  
![](https://pbs.twimg.com/media/EGTjTu8UUAALQLk?format=jpg)  
[twitter quimicahipolito/status/1321489720073981952](https://twitter.com/quimicahipolito/status/1321489720073981952)  
Frikiexamen de 2°Bachillerato de Física. Infinity War es el hilo conductor del examen. @ProfaDeQuimica @profedefyq @bjmahillo #Marvel #infinityWar  
![](https://pbs.twimg.com/media/Elbf5OLW0AEC7t4?format=jpg)  
![](https://pbs.twimg.com/media/Elbf5ZGWkAAkkk4?format=jpg)  
[twitter quimicahipolito/status/1326491763201232902](https://twitter.com/quimicahipolito/status/1326491763201232902)  
Otro frikiexamen de 2°Bach Física. La carrera espacial y la gravitación.  
![](https://pbs.twimg.com/media/EmilN6fXcAEYsU4?format=jpg)  
![](https://pbs.twimg.com/media/EmilOWwWMAoom4C?format=jpg)  
[twitter quimicahipolito/status/1356330860392439811](https://twitter.com/quimicahipolito/status/1356330860392439811)  
Examen de dinámica de 4° ESO de Fisica y Química. Conmemorando el VIII Centenario de la Catedral de Burgos 2021 en el @ieshrlopez de Belorado @beloradoturismo @catedralDburgos @Catedral2021 @Aytoburgos  
![](https://pbs.twimg.com/media/EtKnubvXIAAtkbY?format=jpg)  
![](https://pbs.twimg.com/media/EtKnuo0XIAAnIR2?format=jpg)  
[twitter quimicahipolito/status/1461094556821295203](https://twitter.com/quimicahipolito/status/1461094556821295203)  
Examen de cinemática de 4°ESO de física y química. La temática es: JJOO Tokyo 2020.  
![](https://pbs.twimg.com/media/FEbZwMSWYAEWhVq?format=jpg)  
![](https://pbs.twimg.com/media/FEbZwYXWUAwBsyd?format=jpg)  

###  SaraFyQ ([@SaraFyQ1](https://twitter.com/SaraFyQ1))
 
[twitter SaraFyQ1/status/1200839620944187392](https://twitter.com/SaraFyQ1/status/1200839620944187392)  
Siguiendo los pasos de @ProfaDeQuimica, @FiQuiPedia , @bjmahillo y otros muchos más. Me estreno en el maravilloso mundo de los #frikiexámenes  ☺️ Empezamos con el tema de Dinámica de 4 ESO #FísicayQuímica  
![](https://pbs.twimg.com/media/EKo9QsqWwAAEwOS?format=jpg)  

### Rocio Calzado ([@rcalzadom](https://twitter.com/rcalzadom))
[twitter rcalzadom/status/1465718050376847363](https://twitter.com/rcalzadom/status/1465718050376847363)  
El curso pasado utilicé de nuevo los #frikiexamenes en física y química de 2º ESO añadiendo un ejercicio de vez en cuando. El primero les sorprendió, el segundo les hizo gracia, y los demás me los pidieron.  
![](https://pbs.twimg.com/media/FFdGDfiX0AQL6CI?format=jpg)  
![](https://pbs.twimg.com/media/FFdGFGIXIAcGtn7?format=jpg)  
![](https://pbs.twimg.com/media/FFdGFGIXEAQ261C?format=jpg)  
![](https://pbs.twimg.com/media/FFdGFGMXMAAW6u8?format=jpg)  

###  Santos Mondéjar ([@estapillao](https://twitter.com/estapillao))
 
[twitter estapillao/status/993188796463271937](https://twitter.com/estapillao/status/993188796463271937)  
![](https://pbs.twimg.com/media/DciD6-DXcAAbpE7.jpg)  
[twitter estapillao/status/993189288270581760](https://twitter.com/estapillao/status/993189288270581760)  
![](https://pbs.twimg.com/media/DciEXysW4AA2R2e.jpg)  
[twitter estapillao/status/993236444897599488](https://twitter.com/estapillao/status/993236444897599488)  
![](https://pbs.twimg.com/media/DcivQ70X4AAtGyA.jpg)  
[twitter estapillao/status/1056972695269793792](https://twitter.com/estapillao/status/1056972695269793792)  
Hoy ha tocado examen "musical". Hay ciencia en todas partes.  
![](https://pbs.twimg.com/media/DqsfBkbX4AADgkB.jpg)  
[twitter estapillao/status/1074738899434528774](https://twitter.com/estapillao/status/1074738899434528774)  
Pido perdón a los compañeros de #frikiexamenes por tenerlos abandonados  
Me pondré al día prontoAquí avanzo el del Profesor Layton y Los misterios de la Dinámica  
![](https://pbs.twimg.com/media/Duo9KIqXcAc8QQT.jpg)  
[twitter estapillao/status/1078069813270982661](https://twitter.com/estapillao/status/1078069813270982661)  
Seguimos poniéndonos al día con los #frikiexámenesA veces hay que hacer ciertas cosas para que los alumnos estén contentosHasta un examen de Operación Triunfo  
![](https://pbs.twimg.com/media/DvYSoG5W0AADi3j.jpg)  
[twitter estapillao/status/1091406872622579713](https://twitter.com/estapillao/status/1091406872622579713)  
Nuevo examen en 4º ESOTemática: las noticias que me han llamado la atención en enero #frikiexamen  
![](https://pbs.twimg.com/media/DyV0rzvXgAIe1CI.jpg)  
[twitter estapillao/status/1197443172038774784](https://twitter.com/estapillao/status/1197443172038774784)  
Este curso no tengo tiempo para nada, pero no me he podido resistir a preparar un #frikiexamen sobre las elecciones  
![](https://pbs.twimg.com/media/EJ4sNsyWkAAW4vl?format=png)  
[twitter estapillao/status/1222133080774840320](https://twitter.com/estapillao/status/1222133080774840320)  
Examen 2 bachillerato.  
-Si no hemos estudiado, ¿podemos sacar los apuntes?  
+Claro, así aprovecháis el tiempo  
-¿Alguien me deja sus apuntes?  
+...  
Continúa el espectáculo:  
-Hombre, has puesto la teoría más larga (con los apuntes en la mano)  
-Yo es que he leído las cuestiones y he visto que no las sabía...  
Al rato  
¿Que hay algo en catalán?  
![](https://pbs.twimg.com/media/EPXjhkwWsAEjJb7?format=jpg)  


###  Sergio García ([@SeGA_Gu](https://twitter.com/SeGA_Gu))
 
[twitter SeGA_Gu/status/1330959798548967426](https://twitter.com/SeGA_Gu/status/1330959798548967426)  
 Por fin empezamos la temporada de #frikiexamen con cinemática y 4º ESO. Mis Quimions lo han sufrido y veo carencias de no dejarnos avanzar durante el confinamiento del año pasado. En breves más y algún ejercicio robado a @FiQuiPedia (pero con mención claro!)  
![](https://pbs.twimg.com/media/EniE4FBXMAMNU6a?format=jpg)  
![](https://pbs.twimg.com/media/EniE4FHXMAEaSHk?format=jpg)  

###  ([@vielbein](https://twitter.com/vielbein))
 
[twitter vielbein/status/992381692479442944](https://twitter.com/vielbein/status/992381692479442944)  
Compartido en pdf de 8 páginas en  [2018-FQ3-FQ4-FQ1-vielbein-examencop.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/FriQuiExámenes/2018-FQ3-FQ4-FQ1-vielbein-examencop.pdf)   
![](https://pbs.twimg.com/media/DcWl2CyX0AA6OSk?format=jpg)  
![](https://pbs.twimg.com/media/DcWl33WW4AAKYll?format=jpg)  
[twitter vielbein/status/1260224795775557632](https://twitter.com/vielbein/status/1260224795775557632)  
Mi examen de hoy de pendientes, con connotaciones friquis...#friquiexamenes  
![](https://pbs.twimg.com/media/EX03wCgWsAIg4k0?format=jpg)  

### Yolanda CV / Neko ([@biochicky](https://twitter.com/biochiky)) 
 
[twitter Biochiky/status/949970835573563392](https://twitter.com/Biochiky/status/949970835573563392)  
![](https://pbs.twimg.com/media/DS75byDXkAAw3B_.jpg)  
[twitter Biochiky/status/988381729659473920](https://twitter.com/Biochiky/status/988381729659473920)  
![](https://pbs.twimg.com/media/Dbdv8ZeXcAAGqns.jpg)  
[twitter Biochiky/status/992447047486799878](https://twitter.com/Biochiky/status/992447047486799878)  
![](https://pbs.twimg.com/media/DcXhSqqXkAAvLLJ.jpg)  
![](https://pbs.twimg.com/media/DcXhUJRWsAMg9Xq.jpg)  
[twitter Biochiky/status/993861657519681536](https://twitter.com/Biochiky/status/993861657519681536)  
![](https://pbs.twimg.com/media/Dcrn3wvW4AAHxaY.jpg)  
![](https://pbs.twimg.com/media/Dcrn5HQX4AAIR_d.jpg)  
[twitter Biochiky/status/999521836462497792](https://twitter.com/Biochiky/status/999521836462497792)  
![](https://pbs.twimg.com/media/Dd8Dv0SUwAA8OMX.jpg)  
![](https://pbs.twimg.com/media/Dd8DxdmU8AAAIJh.jpg)  
![](https://pbs.twimg.com/media/Dd8DzBpUwAAe4K-.jpg)  
[twitter Biochiky/status/1001178196849692673](https://twitter.com/Biochiky/status/1001178196849692673)  
![](https://pbs.twimg.com/media/DeTmQSAW0AEI9uj.jpg)  
![](https://pbs.twimg.com/media/DeTmQdTWAAAcKSD.jpg)  
![](https://pbs.twimg.com/media/DeTmQ2VXcAAPqkO.jpg)  
[twitter Biochiky/status/1001899236982775810](https://twitter.com/Biochiky/status/1001899236982775810)  
![](https://pbs.twimg.com/media/Ded2CSZXkAENasz.jpg)  
![](https://pbs.twimg.com/media/Ded2CqqX4AAQHmE.jpg)  
[twitter Biochiky/status/1054736341668429824](https://twitter.com/Biochiky/status/1054736341668429824)  
![](https://pbs.twimg.com/media/DqMtC4LX4AAeAH1.jpg)  
![](https://pbs.twimg.com/media/DqMtERIWwAEeRTi.jpg)  
![](https://pbs.twimg.com/media/DqMtFPuXQAASwiE.jpg)  
![](https://pbs.twimg.com/media/DqMtGdHWkAY1Ehm.jpg)  
![](https://pbs.twimg.com/media/DqMtIKeWsAAv9dR.jpg)  
[twitter Biochiky/status/1055802121424121856](https://twitter.com/Biochiky/status/1055802121424121856)  
2° ESO y Halloween.  
![](https://pbs.twimg.com/media/Dqb2WT8XQAICRlE.jpg)  
![](https://pbs.twimg.com/media/Dqb2YOUXQAYmrWQ.jpg)  
![](https://pbs.twimg.com/media/Dqb2ZgzW4AAiC_m.jpg)  
![](https://pbs.twimg.com/media/Dqb2axlXcAANz0C.jpg)  
[https://twitter.com/Biochiky/status/1057576497282736128](https://twitter.com/Biochiky/status/1057576497282736128)  
Recién salido del horno. Física 2° bachillerato.  
![](https://pbs.twimg.com/media/Dq1ELvNWkAAlHOK.jpg)  
![](https://pbs.twimg.com/media/Dq1ENFUWwAErjcm.jpg)  
[twitter Biochiky/status/1059433496966520832](https://twitter.com/Biochiky/status/1059433496966520832)  
5/nov/2018Crimen+arte+química.Examen Química 2 bachillerato.Agradecer a @Oskar_KimikArte su trabajo y material que me ha permitido idear/elaborar la opción B del examen.  
![](https://pbs.twimg.com/media/DrPdHeFWsAAv7gx.jpg)  
![](https://pbs.twimg.com/media/DrPdI6eXgAAQ6Kt.jpg)  
[twitter Biochiky/status/1067504300786622466](https://twitter.com/Biochiky/status/1067504300786622466)  
Examen final 3° ESO. No podía tener otra temática.  
![](https://pbs.twimg.com/media/DtCJbg5XgAE_-9R.jpg)  
![](https://pbs.twimg.com/media/DtCJcjqX4AA4vZf.jpg)  
![](https://pbs.twimg.com/media/DtCJeUSX4AIgwJH.jpg)  
![](https://pbs.twimg.com/media/DtCJfbSW0AAT3z3.jpg)  
[twitter Biochiky/status/1067504386153299968](https://twitter.com/Biochiky/status/1067504386153299968)  
Examen final 2° ESO. Mantenemos temática con algunas modificaciones.  
![](https://pbs.twimg.com/media/DtCJhZAWwAAg4kn.jpg)  
![](https://pbs.twimg.com/media/DtCJiiFXcAEdOJ6.jpg)  
![](https://pbs.twimg.com/media/DtCJjmBW0AIgNFZ.jpg)  
![](https://pbs.twimg.com/media/DtCJkqFWsAARsmB.jpg)  
[twitter Biochiky/status/1068544713639247873](https://twitter.com/Biochiky/status/1068544713639247873)  
Final Física 2° bach. Opción A.  
![](https://pbs.twimg.com/media/DtQ7uvqWwAEwPwN.jpg)  
![](https://pbs.twimg.com/media/DtQ7vkrXcAELwOf.jpg)  
Opción B.  
![](https://pbs.twimg.com/media/DtQ7wnEWwAE8QSS.jpg)  
![](https://pbs.twimg.com/media/DtQ7xngXgAIh_YP.jpg)  
[twitter Biochiky/status/1083736113737580544](https://twitter.com/Biochiky/status/1083736113737580544)  
Sigo con mi frikismo infantiloide. Agradecer a @biogeocarlos por los nombres científicos de los pececitos y la idea.  
![](https://pbs.twimg.com/media/Dwo0OPYW0AcE7of.jpg)  
![](https://pbs.twimg.com/media/Dwo0PclX0AE-SRA.jpg)  
[twitter Biochiky/status/1092423050136555520](https://twitter.com/Biochiky/status/1092423050136555520)  
Bueno, pues hoy ha tocado un poco de análisis detectivesco. Mencionar a @JernimoTristant que me ha permitido que el subinspector Víctor Ros apareciera en el examen. ¡Gracias! También a @profedefyq, que me he copiado un pelín de su último examen. Me lo perdonará (o eso creo).  
![](https://pbs.twimg.com/media/DykQ8sqWwAEKh4y.jpg)  
![](https://pbs.twimg.com/media/DykQ94kXQAEZ00E.jpg)  
[twitter Biochiky/status/1099274613123616768](https://twitter.com/Biochiky/status/1099274613123616768)  
Final Física 2°.No podía ser de otra forma:Diálogos en el Prado sobre Cª y Arte. Agradecer a D.José D. de la Fuente el libro del que está sacada la idea e intro a los enunciados, así como a D.Vicente Camarasa el desentrañar cada cuadro.Espero que no les importe @mariajesuscueto  
![](https://pbs.twimg.com/media/D0FoaAoX0AA7KfI.jpg)  
![](https://pbs.twimg.com/media/D0FobISXcAAf3VL.jpg)  
[twitter Biochiky/status/1100017104055619585](https://twitter.com/Biochiky/status/1100017104055619585)  
Este ha sido el examen final de Química 2°bach. Temática por petición de los alumnos. Creo que querían saber hasta donde llega mi locura. No sé si he perdido todo el respeto.Opción A:  
![](https://pbs.twimg.com/media/D0QLsS0XcAA1cma.jpg)  
![](https://pbs.twimg.com/media/D0QLtrmWwAARW1N.jpg)  
Opción B  
![](https://pbs.twimg.com/media/D0QLvw_W0AETymh.jpg)  
![](https://pbs.twimg.com/media/D0QLxI5X0AE5Rf5.jpg)  
[twitter Biochiky/status/1108492107625521153](https://twitter.com/Biochiky/status/1108492107625521153)  
Los rojoazules los dejaré para otra ocasión. Toca esta locura que lees entre guardia y guardia: una mezcla de periódico @20m, @eswikipedia e imaginación para llevarlo a Química 2° bach.  
![](https://pbs.twimg.com/media/D2Inrt0X4AAYObf.jpg)  
![](https://pbs.twimg.com/media/D2InsNCWoAEyT3X.jpg)  
[twitter Biochiky/status/1132956865439182848](https://twitter.com/Biochiky/status/1132956865439182848)  
Muy desanimada y cabreada. - Profe, ¿por qué nos obligas a leer? - Porque me parece vergonzoso que gente de 15 años no sepa que significa "despreciar" como me acabas de preguntar. En fin, me quedo con mis recuerdos.Aprobaran la mayoría por los parciales que eran de a, b, c y d.  
![](https://pbs.twimg.com/media/D7kSNn4W0AAVwXU.jpg)  
![](https://pbs.twimg.com/media/D7kSOnsX4AAZxQC.jpg)  
![](https://pbs.twimg.com/media/D7kSPgAW4AAgZ3I.jpg)  
![](https://pbs.twimg.com/media/D7kSQVFXoAASvA-.jpg)  
[twitter Biochiky/status/1135535290452516865](https://twitter.com/Biochiky/status/1135535290452516865)  
Se acabó lo que se daba. Último examen del curso. En esta ocasión, sobre mis Pitufos (me lo debía) Subida nota en 1°bach. Hora y media de examen que se ha extendido, pero se lo han currado. Ahora a ver los resultados pero por lo que he visto durante el examen, no pintaba mal.  
![](https://pbs.twimg.com/media/D8I7SyyW4AEjYor.jpg)  
![](https://pbs.twimg.com/media/D8I7Tt8WsAAiZUW.jpg)  
![](https://pbs.twimg.com/media/D8I7UkAXkAEXeDq.jpg)  
[twitter Biochiky/status/1183654165697957888](https://twitter.com/Biochiky/status/1183654165697957888)  
Espero que a @Alexny_85 no le importe que haya hecho capturada de pantalla de algunos de sus tweets para que, junto con ejercicios de olimpiadas, evau y libros básicos de clase 1°/2° bach, crear estas fichas para mi chavalería de Química 2 bach.  
![](https://pbs.twimg.com/media/EG0vIXLX0AINnEp?format=jpg)  
![](https://pbs.twimg.com/media/EG0vJeCXYAACS7I?format=jpg)  
![](https://pbs.twimg.com/media/EG0vJeCXYAACS7I?format=jpg)  
![](https://pbs.twimg.com/media/EG0vLpXXYAARQTc?format=jpg)  
Y espero que a @javyfeu tampoco.Solo pretendo reflejar las competencias clave CMCT y CD de la LOMCE 🤦🏻‍♀️  
![](https://pbs.twimg.com/media/EG0vMviX0AEMu6L?format=jpg)  
![](https://pbs.twimg.com/media/EG0vNzGXUAAPNjF?format=jpg)  

##  Preguntas frikis creadas por alumnos
 
[twitter ticitec/status/1068886374957359105](https://twitter.com/ticitec/status/1068886374957359105)  
Nuestros alumnos 4ESO de #E4efectos de @LaPurisimaValen montaron este Padlet con enunciados inventados por ellos mismos utilizando sus pelis preferidas. En esta promoción ganan los fans de Harry Potter.  
[https://padlet.com/eligomez/tgugntr709nx](https://padlet.com/eligomez/tgugntr709nx) #frikienunciadosEnunciados de PELIS elaborados y resueltos por los alumnos de E4efectos. Colegio La Purísima-ValenciaEli Gomez Oltra 
[twitter ProfaDeQuimica/status/1239160358348128259](https://twitter.com/ProfaDeQuimica/status/1239160358348128259)  
Aquí, creando escuela. 🥰🥰🥰  
![](https://pbs.twimg.com/media/ETJhxnLWAAAuOiH?format=jpg)  
Exámenes frikis eventos deportivos 
[twitter realracingclub/status/851510279548657669](https://twitter.com/realracingclub/status/851510279548657669)  
¡Examen de física real! @PratsAbdon, @DanielAquino11 y el @AlbaceteBPSAD son los protagonistas. Pregunta: "¿subimos o no subimos?"  
![](https://pbs.twimg.com/media/C9Er7inXYAMkCyO.jpg)  
[twitter JGilMunoz/status/987284226163838976](https://twitter.com/JGilMunoz/status/987284226163838976)  
Problema propuesto a mis alumnos. También con @DaniCarvajal92 y @Cristiano se puede aprender #Física 😄 #TiroParabólico #CR7Rules @realmadrid  
![](https://pbs.twimg.com/media/DbOJwQ9WAAAtZy1.jpg)  

##  Exámenes frikis universitarios

[twitter elprofedefisica/status/693166271333007360](https://twitter.com/elprofedefisica/status/693166271333007360)  
Esta pregunta la puse en un examen en 2009  Y estas son las respuestas de mis alumnos  
![](https://pbs.twimg.com/media/CZ53D0iWEAApNd0?format=jpg)  
[Los exámenes de un profe friki - elprofedefisica](http://elprofedefisica.naukas.com/2011/06/16/los-examenes-de-un-profe-friki/)  


[La historia detrás de esta genial pregunta de un examen en la Carlos III](http://www.huffingtonpost.es/2016/01/23/examen-carlos-iii_n_9049554.html)  
[Examen Ingeniería Térmica sobre la Estrella de la Muerte - docentesconeducacion.es](http://docentesconeducacion.es/viewtopic.php?f=92&t=4104)  
![](http://i.huffpost.com/gen/3916156/thumbs/o-EXAMEN-570.jpg )  

[twitter vorkomi/status/920255983766704128](https://twitter.com/vorkomi/status/920255983766704128)  
La actualidad política también llega a la facultad de física de la Universidad de Sevilla...  

[twitter spidermanzano/status/1517081816619962368](https://twitter.com/spidermanzano/status/1517081816619962368)  
Sigo con mi empeño de crear ejercicios con aplicaciones prácticas para motivar al alumnado. Ahora estamos con el tema del magnetismo. Aquí tenéis dos nuevos #frikiejercicios  
![](https://pbs.twimg.com/media/FQ3BuRiXsAE4rVJ?format=jpg)  
![](https://pbs.twimg.com/media/FQ3Bxm2WUAAisk7?format=jpg)  

[Relativa independencia catalana - docentesconeducacion.es](http://docentesconeducacion.es/viewtopic.php?f=92&t=5927)  
['¿Cuánto duró la independencia catalana?': la pregunta de un examen de Física de la Universidad de Sevilla - ideal.es](http://www.ideal.es/sociedad/duro-independencia-catalana-20171019102456-nt.html)  
![](https://pbs.twimg.com/media/DMVn7VVXcAUHAsG.jpg)  

[twitter MestreMolar/status/1009086112424439808](https://twitter.com/MestreMolar/status/1009086112424439808)  
Algo friki este catedrático es. Facultad de Químicas de València.  
![](https://pbs.twimg.com/media/DgD-dhsXcAQLQMx.jpg)  

[twitter DrGraciaUCLM/status/1017374533500751872](https://twitter.com/DrGraciaUCLM/status/1017374533500751872)  
Te recuerdo que también existen superhéroes españoles. Un examen mío:  
![](https://pbs.twimg.com/media/Dh5wj0AW4AAAobl.jpg)  

[twitter MientrasEnFisic/status/780138379291820032](https://twitter.com/MientrasEnFisic/status/780138379291820032)  
Mañana mis alumnos van a descubrir la física de Pokémon  
![](https://pbs.twimg.com/media/CtObqkuXYAAX8VD.jpg)  

[twitter FCarrillo_UCLM/status/1225823858235789312](https://twitter.com/FCarrillo_UCLM/status/1225823858235789312)  
Dedicado a mi @ProfaDeQuimica favorita, por hacernos ver que divertido no es lo contrario de serio, sino de aburrido. #Químifrikis  
![](https://pbs.twimg.com/media/EQMATFIXUAEwZAt?format=jpg)  

##  Exámenes Frikis no solamente de Física y Química
 
[twitter InmaculadaRojo/status/971463416044015617](https://twitter.com/InmaculadaRojo/status/971463416044015617)  
Les ha encantado!! Lo mejor, cuando se han levantado, se han puesto la mano en el corazón y han hecho el juramento!! Especialmente dirigido a @biologiayarte . Gracias @MartaVelzquez6 por tu aportación de #eritrocistus , ya que pide mejorar lo que tenía hecho!! 1ª parte  
![](https://pbs.twimg.com/media/DXtUtSYXkAAYapm.jpg)  

[huffingtonpost.es La lección viral de un profesor de la Universidad de Granada a sus alumnos "antes y después" de un examen](https://www.huffingtonpost.es/2018/06/11/la-leccion-viral-de-un-profesor-de-la-universidad-de-granada-a-sus-alumnos-antes-y-despues-de-un-examen_a_23456118/)  
[elmundo.es Las "instrucciones para el examen" de un profesor de Granada que dieron la vuelta a Twitter](https://www.elmundo.es/f5/estudia/2018/06/11/5b1e3dede5fdea0f618b45cb.html)  
> "1\. Estoy convencido de que conocéis las preguntas, están extraídas de lo que hemos trabajado en clase muchas veces. Confiad en los conocimientos que habéis recibido y en vuestro trabajo.  
2\. Los exámenes son únicamente muestras del conocimiento en el momento actual. Soy consciente de que estás nervios@ y que tienes más asignaturas, pero puedes hacerlo. Si te "atascas", pregunta, yo intentaré guiarte para que seas capaz de continuar por ti mism@. Eso es un examen, simplemente vas a escribir sobre lo que sabes de lo que has elegido estudiar. Puedes hacerlo y lo vas a hacer bien.  
3\. Por favor, evitad en la medida de los posible la descarga simpática y el estrés. Ya sabéis lo perjudicial que es el estrés para todos los sistemas, y estoy aquí para ayudaros.  
4\. Si el resultado no es el esperado por ti, podrás mejorar en sucesivas convocatorias y te servirá para saber que conocimientos debes reforzar más".  

[twitter IKEASpain/status/1009816455511203840](https://twitter.com/IKEASpain/status/1009816455511203840)  
Los suecos somos un poco fríos pero con esto nos has ablandado el corazón. No sabemos si serán muy rebuscados o no, pero esperamos que con este examen paséis un buen rato en clase.  
![](https://pbs.twimg.com/media/DgOWrONWAAAPiNV.jpg)  

[twitter b0urbaki/status/943067393139576832](https://twitter.com/b0urbaki/status/943067393139576832)  
El examen de hoy de Matemáticas I para 1° de Bachillerato#frikiexamenes Espero que les salga de cine ;-)  
![](https://pbs.twimg.com/media/DRZyg5QWkAAfm-R.jpg)  
[twitter pbeltranp/status/1067467349769351168](https://twitter.com/pbeltranp/status/1067467349769351168)  
No son #frikiexámenes como los de @ProfaDeQuimica, pero suelo incluir alguna tontería para intentar rebajar el nivel de ansiedad y recordar cosas que deberían ser evidentes. El reverso (no tenebroso) del de esta mañana de 2ºBachillerato @IESValdesparter.  
![](https://pbs.twimg.com/media/DtBmFtDX4AMz9jt.jpg)  
[twitter ClaraGrima/status/1085597175051112448](https://twitter.com/ClaraGrima/status/1085597175051112448)  
Un adelanto para mis estudiantes de MD del examen de mañana ;-)  
![](https://pbs.twimg.com/media/DxDQo7XX0AAAZQ1.jpg) 
[twitter EnriqueFG79/status/1088536524696608768](https://twitter.com/EnriqueFG79/status/1088536524696608768)  
Unos ejercicios que he preparado para 2º ESO. Matemáticas aplicadas a los Carnavales. Trabajando la Competencia Gadita.  
![](https://pbs.twimg.com/media/DxtCLW6X4AMAwlj.jpg)  
[twitter Nebesu_/status/1087401371731722241](https://twitter.com/Nebesu_/status/1087401371731722241)  
He redactado algunos ejercicios de genética frikis y he pensado en compartirlos con vosotros ¿Qué os parece la idea? No son muy complicados, son para un nivel de 4º de ESO. #CompartiendoApuntesPokémon, animales fantásticos y el gran showman para la primera parte  
![](https://pbs.twimg.com/media/Dxc5NosWkAAktjB.jpg)  
![](https://pbs.twimg.com/media/Dxc5NoKX0AEVRSi.jpg)  
![](https://pbs.twimg.com/media/Dxc5NphX0AIxIqP.jpg)  
![](https://pbs.twimg.com/media/Dxc5NpnXgAc6g1w.jpg)  
Furias nocturnas, más pokémon, Harry Potter y unas buenas plantas para acabar con esos zombies que molestan en tu jardín.  
![](https://pbs.twimg.com/media/Dxc5VzTXcAA3MVh.jpg)  
![](https://pbs.twimg.com/media/Dxc5VzpWsAEQTNV.jpg)  
![](https://pbs.twimg.com/media/Dxc5V0UWoAcDvj4.jpg)  
![](https://pbs.twimg.com/media/Dxc5V06XQAAaezC.jpg)  
Y para terminar mis favoritos: mi novia furry Canela y un poquito de Jurassic Park para terminar una buena sesión de mendeliana.Espero que os haya gustado esta sesión de frikismo ilustrado y nos vemos en mi próxima charla TED.  
![](https://pbs.twimg.com/media/Dxc5g77XQAYEWWm.jpg)  
![](https://pbs.twimg.com/media/Dxc5g8RWsAA0rMe.jpg)  
[twitter maitematiques/status/1090323656281075712](https://twitter.com/maitematiques/status/1090323656281075712)  
He creado un monstruo. Mis alumn@s de 4º ESO dándolo todo en la pregunta voluntaria: Imagina que E.T. Llega a tu casa, cómo le explicarías para qué sirven las mates que estudias?  
![](https://pbs.twimg.com/media/DyGblQjXgAItOon.jpg)  
![](https://pbs.twimg.com/media/DyGblQkWsAE4YF5.jpg)  
![](https://pbs.twimg.com/media/DyGblQnWoAE4Ww1.jpg)  
[![](https://pbs.twimg.com/media/DyGblQlX4AIbunU.jpg)  

[twitter FiQuiPedia/status/1134464615381905408](https://twitter.com/FiQuiPedia/status/1134464615381905408)  
Hay mucho poder friki oculto en los claustros. Comparto un frikiexamen de mi compi de matemáticas Rafa Oliver
¡Compartid, malditos!
[Enlace pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/FriQuiEx%C3%A1menes/2019-05-RafaOliver-examen%20trigonometria%20y%20vectores.pdf?inline=false) (en tuit original enlace era a Google drive, no operativo tras migración a gitlab en 2021)  

[twitter Toni_azbel/status/1664552722568232960](https://twitter.com/Toni_azbel/status/1664552722568232960)  
¿Tú te crees el examen que he hecho hoy en mi clase? @alo_oficial  
![](https://pbs.twimg.com/media/Fxmt37uWIAIB4jJ?format=jpg)  

[twitter Soy_Ingeniero1/status/1744014877859926463](https://twitter.com/Soy_Ingeniero1/status/1744014877859926463)  
—Profe, ¿el parcial puede ser selección múltiple?  
—Si claro...  
![](https://pbs.twimg.com/media/GDP8SiVXUAAqNdr?format=jpg)  

###  Octavio Prieto (@octavio_pr)
 
[twitter octavio_pr/status/1060598865450143744](https://twitter.com/octavio_pr/status/1060598865450143744)  
Pues ya está, @ProfaDeQuimica Es el primero. Ya me saldrán más chulos para primero que da más juego:  
![](https://pbs.twimg.com/media/DrgBAisW4AEaCKp.jpg)  
![](https://pbs.twimg.com/media/DrgBB2PX4AAN4Md.jpg)  
[twitter octavio_pr/status/1075679033445285888](https://twitter.com/octavio_pr/status/1075679033445285888)  
![](https://pbs.twimg.com/media/Du2UXfXX4AAoUnB.jpg)  
[twitter octavio_pr/status/1075827403094716416](https://twitter.com/octavio_pr/status/1075827403094716416)  
En otros exámenes he puesto que un número sin si unidad detrás es un número desnudo, un número en pelotas. 
[twitter octavio_pr/status/1197180857993322502](https://twitter.com/octavio_pr/status/1197180857993322502)  
Por fin cayó el examen sorpresa, inspirado en @ProfaDeQuimica  
![](https://pbs.twimg.com/media/EJ09ohkWsAI_-T4?format=jpg)  
[twitter octavio_pr/status/1205202891134455808](https://twitter.com/octavio_pr/status/1205202891134455808)  
Uso los nombres de las empresas en los ejercicios. Mensajes grupales y personalizados:  
![](https://pbs.twimg.com/media/ELm9oehXYAADGrF?format=jpg)  
![](https://pbs.twimg.com/media/ELm9o0-XYAAIK83?format=jpg)  
[twitter octavio_pr/status/1251922678761631745](https://twitter.com/octavio_pr/status/1251922678761631745)  
![](https://pbs.twimg.com/media/EV-5B-_XQAAkoY8?format=jpg)  
![](https://pbs.twimg.com/media/EV-5CbgXsAEys93?format=jpg)    
[twitter octavio_pr/status/1377660440113778688](https://twitter.com/octavio_pr/status/1377660440113778688)  
Me parece muy bien ese correo, que conste. Pero cada uno según su forma de ser, unos animando y a otros nos toca desanimar:  
![](https://pbs.twimg.com/media/Ex5u2lZXAAE0pCW?format=png)   
Citaba no un frikiexamen, sino un "plan de trabajo para Pascua" (tuit borrado)  

[twitter octavio_pr/status/1446111526000082944](https://twitter.com/octavio_pr/status/1446111526000082944)  
Excusas para todo:   
- Octavio, yo no hago el examen. No puedo. Me da mucho miedo.  
![](https://pbs.twimg.com/media/FBGeoamUcAMgaTt?format=png)  

[twitter octavio_pr/status/1446393818291572738](https://twitter.com/octavio_pr/status/1446393818291572738)  
![](https://pbs.twimg.com/media/FBKfgjqWYAY2TNS?format=jpg)  

[twitter octavio_pr/status/1446496326066315300](https://twitter.com/octavio_pr/status/1446496326066315300)  

###  Pedro Cifuentes (@krispamparo)·
 
[twitter krispamparo/status/1225339243255468033](https://twitter.com/krispamparo/status/1225339243255468033)  
Os dejo por aquí otro de mis “exámenes aventura”. Este lo andan trasteando mis chavales ahora mismo. 21 en el aula y ni han chistado desde que lo he repartido a las 8.20. Mini hilo.Previamente cuentan con una rúbrica para que sepan cómo organizar la redacción y qué cosas les exijo.Hemos estado trabajando el tema durante seis sesiones...Y según mi experiencia: desde que sustituyo las pruebas más convencionales por este tipo de salidas de madre, me va mucho mejor.Los chavales se expresan, entienden que les corrija también lo que está mal redactado y las justificaciones que les proporciono cuando entrego el examen. Si sois profes de sociales, sabréis que esto suele ser un Caballo de batalla.Valoro también el plus de originalidad. Algunos son muy divertidos. Ellos disfrutan haciendo la prueba... y yo leyendo los tostones, que poco a poco van creciendo con ellos.Chavales de 2ESO desarrollando motu propio, con su expresión limitada, su redacción con lagunas, un tema más o menos complejo.Expresión que poco a poco irán mejorando...Y temas complejos que lentamente aprenderán a desarrollar con concreción y originalidad.Por eso es importante entender bien lo que significa el trabajo por competencias.  
![](https://pbs.twimg.com/media/EQFHiZfX0AAXvrs?format=jpg)  

###  Ernesto Boixader Gil (@eboixader)
 
[twitter eboixader/status/1316776179005894656](https://twitter.com/eboixader/status/1316776179005894656)  
Mi primer Frikiexamen! Después de ver los de @biologiayarte y M. Florencia en @BlogSimbiosis. Cuesta más prepararlo pero la satisfacción de ver que se disfruta y se divierte haciendo un examen, no tiene precio. Aunque me han dejado claro que debo ver más los cómics de Marvel  
![](https://pbs.twimg.com/media/EkYesbgXcAAsRq_?format=jpg)  
![](https://pbs.twimg.com/media/EkYesbuWsAAia0K?format=jpg)  
[twitter eboixader/status/1399662356808994819](https://twitter.com/eboixader/status/1399662356808994819)  
Comparto un #frikiexamen (o parecido) de #cinematica para #4eso. Cuesta prepararlos, pero la motivación e implicación para resolverlos por parte del alumnado compensa el esfuerzo  
[Laboratorio de Balística 20-21.pdf](https://drive.google.com/file/d/1QwvMCaKhAwi2Qp7l4xOE8oa9wId3YwH-/view)  
[twitter eboixader/status/1364984756165836801](https://twitter.com/eboixader/status/1364984756165836801)  
Nuevamente enganchado a un #frikiexamen (espero cumpla con los cánones @bjmahillo @FiQuiPedia) Durante toda la mañana ha sido el tema de conversación del grupo, les ha gustado. Aunque es un faenón, vale la pena #claustrovirtual #aprenderesdivertido
[Els elements A2 EXAMEN 20_21.pdf](https://drive.google.com/file/d/1gjtmODTa8RmX6HCRPwDTtNQ-9UIyRB_r/view)  
[twitter eboixader/status/1367508902938558466](https://twitter.com/eboixader/status/1367508902938558466)  
Por fin! Si no tenéis otra cosa que hacer, organizada mi web [elboixader.com](http://eboixader.com): clase DiNvertida, Libros y Breakouts Digitales, frikiexamenes, Laboratorios Virtuales, Herramientas útiles y referentes del #claustrovirtual al que tanto tengo que agradecer. Sólo es por ayudar  

###  @faraon
 
[twitter faraon/status/1138075993258766336](https://twitter.com/faraon/status/1138075993258766336)  
En el acto de graduación los alumnos me han mencionado como autor del examen más difícil de toda la carrera.Esa pandilla entrañable sabe como llegarle al corazón a uno, me han emocionao y se les echará de menos. :______)  
![](https://pbs.twimg.com/media/D8tAmDVXsAEX7ge?format=jpg)  

### Ana López (@Anitalks) 

[twitter Anitalks/status/1323738192504410112](https://twitter.com/Anitalks/status/1323738192504410112)  
Hace mucho que no os comparto cosas de las que hago en el aula, así que hoy he decidido regalaros el primero de muchos frikiexámenes de Lengua en versión multinivel para 4°, 5° y 6° de Primaria.  
Hemos utilizado el mismo texto de base para los tres cursos. Este año toca cómic.  
Y la temática del cómic no podía empezar por nadie más que por Wonder Woman. Así que extraemos un trocito de un artículo de una revista y nos ponemos manos a la obra.  
Conseguir que todos los ejercicios de los tres cursos encajen en el mismo texto no siempre es fácil.  
Pero merece la pena utilizar ese texto común, porque después del examen escrito aprovechamos a debatir sobre el texto y así evaluamos mediante observación también la tan olvidada expresión oral.  
Las aulas multinivel tienen sus ventajas y sus desventajas, hay que saber adaptarse.  
Y, dicho esto, empiezo por subiros imágenes del examen de 4° de Primaria.  
Se intenta que todo el examen verse en torno al texto de inicio y a ellos les llama la atención ese detalle. Es algo que solo pasa cuando preparo yo los ejercicios fuera de libro.  
![](https://pbs.twimg.com/media/El7eVomXYAQOg3j?format=jpg)  
![](https://pbs.twimg.com/media/El7eWLSX0AANnPG?format=jpg)  
![](https://pbs.twimg.com/media/El7eWLSX0AANnPG?format=jpg)  
![](https://pbs.twimg.com/media/El7eWLSX0AANnPG?format=jpg)  
El examen de 5° evalúa diferentes contenidos. Así que mantenemos la lectura y las preguntas de la misma y variamos el resto de ejercicios. Aquí hubo un momento en el que fue imposible ligar alguna pregunta con el texto, tuvimos que apelar a otros recursos.  
![](https://pbs.twimg.com/media/El7euXcXUAEiwbt?format=jpg)  
![](https://pbs.twimg.com/media/El7eu6aXYAMHUD2?format=jpg)  
![](https://pbs.twimg.com/media/El7evUwWoAMfeH9?format=jpg)  
![](https://pbs.twimg.com/media/El7evweXIAAsTt0?format=jpg)  
En 6° repetimos la lectura una vez más (no la subo, que este tiene una página más y si no las imágenes no entran en un tweet) y mantenemos las preguntas de comprensión. Cambiamos nuevamente los ejercicios relacionados con los contenidos a evaluar.  
![](https://pbs.twimg.com/media/El7flIhXgAA-vag?format=jpg)  
![](https://pbs.twimg.com/media/El7flgiWkAAKJOA?format=jpg)  
![](https://pbs.twimg.com/media/El7f078WMAI9tM0?format=jpg)  
![](https://pbs.twimg.com/media/El7fmX5XIAAxHhE?format=jpg)  
