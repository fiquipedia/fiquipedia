
# Recursos PAU mayores de 25 años

Los exámenes son distintos a la PAU de bachillerato, por lo que se ponen enlaces distintos a los  [recursos genéricos PAU](/home/recursos/recursospau) , aunque a veces se llega navegando dentro de la misma página  

Los exámenes se empiezan a recopilar en 2016, inicialmente solamente de Madrid. Son enunciados útiles como ejemplos para 1º Bachillerato e incluso para 4º ESO  

Los exámenes no se ponen dentro de la  [tabla de enunciados originales PAU de física](/home/recursos/recursospau/ficheros-enunciados-pau-fisica)  ni de la  [tabla de enunciados originales de química](/home/recursos/recursospau/ficheros-enunciados-pau-quimica) , pero sí en drive asociado dentro de física y química dentro de cada comunidad   
Pongo la comunidades comenzando por Madrid y luego el resto por orden alfabético


##   Madrid
[Acceso a la universidad para mayores de 25 años](https://www.comunidad.madrid/servicios/educacion/acceso-universidad-mayores-25-anos) 

Hay exámenes distintos por cada universidad (Alcalá de Henares, Autónoma, Carlos III, Complutense, Politécnica, Rey Juan Carlos)  
 
[drive.fiquipedia Física AccesoMayores25](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/PAUxComunidades/fisica/madrid/AccesoMayores25)  
[drive.fiquipedia Química AccesoMayores25](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/PAUxComunidades/quimica/madrid/AccesoMayores25)  

Fuentes de enunciados de exámenes:  
[http://juliweb.es/](http://juliweb.es/) 

[EMES, modelos de exámenes mayores de 25 años](https://web.archive.org/web/20171124103316/http://www.emes.es/AccesoUniversidad/Mayoresde25/Modelosdeexamen/tabid/357/Default.aspx) waybackmachine

##   Andalucía
 [CONDICIONES DE ACCESO PARA MAYORES DE 25 AÑOS](https://www.juntadeandalucia.es/economiaconocimientoempresasyuniversidad/sguit/?q=grados&d=g_25_procedimiento_acceso.php)  
 [EXÁMENES Y ORIENTACIONES SOBRE LA PRUEBA DE ACCESO PARA MAYORES DE 25 AÑOS](https://www.juntadeandalucia.es/economiaconocimientoempresasyuniversidad/sguit/?q=grados&d=g_b_examenes_anteriores.php&tipo=M25) 

##   Aragón
 [Prueba de acceso para mayores de 25 años](https://academico.unizar.es/acceso-admision-grado/acceso-mayores-25/pam25)  
 [Exámenes y criterios de corrección de convocatorias anteriores](https://academico.unizar.es/acceso-admision-grado/acceso-mayores-25/exame) 

##   Asturias
 [Procedimiento de acceso de estudiantes Mayores de 25 años](http://www.uniovi.es/accesoyayudas/estudios/mayores25) 

##   Baleares
 [Acceso para mayores de 25](http://estudis.uib.es/es/grau/acces/mes_grans25/) 

##   Canarias
 [Curso de Acceso a la Universidad para Mayores de 25 y 45 años](https://www.ulpgc.es/acceso/mayores25y45) 

##   Cantabria
 [Pruebas de Acceso para Mayores de 25, 40 y 45 Años](https://web.unican.es/admision/acceso-a-estudios-de-grado/mayores-de-25-40-y-45-anos) 

##   Castilla-La Mancha
 [Mayores de 25 años (M25) y mayores de 45 años (M45)](https://www.uclm.es/Perfiles/Preuniversitario/Acceso/ModosAcceso/MayoresM25M45) 

##   Castilla y León
 [Pruebas para mayores de 25 años](https://www.unileon.es/estudiantes/futuros-estudiantes/acceso/mayores25) 

##   Cataluña
 [Tinc més de 25 anys](http://universitats.gencat.cat/ca/que_puc_fer/25_anys/) 

##   Extremadura
 [Mayores 25-40-45 ](http://www.unex.es/organizacion/servicios-universitarios/servicios/alumnado/funciones/m25) 

##   Galicia
 [Acceso a la universidad para mayores de 25 años](https://www.usc.gal/es/servizos/oiu/Maiores-25.html) 

##   Navarra
 [Acceso: mayores de 25](http://www.unavarra.es/sites/estudios/acceso-y-admision/otras-vias-de-acceso/acceso-para-25.html) 

##   Murcia
 [Pruebas de acceso a la Universidad para mayores de 25 y 45 años](https://www.um.es/web/vic-estudios/contenido/acceso/mayores-25-45) 

##   País Vasco
 [Pruebas de acceso a la Universidad a mayores de 25](https://www.ehu.eus/es/web/sarrera-acceso/vias-acceso-y-pruebas/pruebas-acceso-mayores-25) 

##   Rioja
 [PRUEBA DE ACCESO PARA MAYORES DE 25 AÑOS](http://www.unirioja.es/estudiantes/acceso_admision/PAU/Pau_mayores_25/) 

##   Valencia
 [PRUEBAS DE ACCESO A LA UNIVERSIDAD PARA MAYORES DE 25 AÑOS](https://www.upv.es/entidades/SA/acceso/396893normalc.html) 

##   UNED
 [Exámenes acceso universidad mayores 25 años UNED](http://www.examenes-selectividad.com/index.php/examenes-acceso-universidad-mayores-25-anos/acceso-mayores-de-25-uned)  
