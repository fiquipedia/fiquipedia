# Recursos Física y Química 3º ESO

Los recursos podrán estar como  [apuntes](/home/recursos/apuntes) ,  [ejercicios](/home/recursos/ejercicios) , asociados a enlaces dentro de la  [materia](/home/materias)  y su currículo ...  
Se ponen inicialmente enlaces a algunas páginas, al ser genéricos habrá recursos no asociados a este nivel de 3º ESO.  
Pendiente enlazar los recursos asociados,...  
Pendiente crear currículo con enlaces a recursos, actualizando con currículo LOMCE que se implanta en curso 2015-2016

* [Apuntes elaboración propia Física y Química 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso/apuntes-elaboracion-propia-3-eso)   
* [Apuntes 3º ESO - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/3eso)  Rodrigo Alcaraz de la Osa  cc-by-sa
* [Apuntes 3º ESO - fisquiweb](http://fisquiweb.es/Apuntes/apun3.htm) Luis Ignacio García González   
* [Física y Química 3º ESO - elfisicoloco.blogspot.com.es](http://elfisicoloco.blogspot.com.es/p/fyq-3-eso.html)  
* [Didactica Física y Química](http://didacticafisicaquimica.es/)   
 [Física y Química de 3º de ESO](http://didacticafisicaquimica.es/fisica-y-quimica-3-eso/)   
 [Cuaderno de fichas Física y Química de 3º de ESO](http://didacticafisicaquimica.es/cuaderno-de-fichas-fisica-y-quimica-3-eso/)   
 [Física y Química de 3º de ESO – LOMCE](http://didacticafisicaquimica.es/fisica-y-quimica-de-3o-de-eso-lomce/)   
 [Enunciados de problemas 3º ESO](http://didacticafisicaquimica.es/enunciados-problemas-3oeso/)  
* [3ºESO FyQ - profesorjrc.es](https://profesorjrc.es/3eso.htm) Jorge Rojo Carrascosa   
* [3ºESO FyQ - quifiblogeso](http://quifiblogeso.blogspot.com/search/label/3%C2%BA%20ESO) Inma Rodríguez  
* [3ºESO FyQ - elortegui.org](http://ciencia.elortegui.org/datos/3ESO/ESO3fyq.htm)  
* [3ºESO FyQ - mainquifi](https://mainquifi.blogspot.com/p/3-eso-fisica-y-quimica.html)  
[Libro de FYQ 3°ESO ADAPTADO A LA LOMLOE GRATIS (pdf, 196 MB, 207 páginas - mainquifi](https://drive.google.com/file/d/1WiPeKH8knryMPKdc6PZgBUABx3ROBN7I/view?usp=drivesdk)  


## [Recursos Física y Química por bloques comunes 2º ESO y 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-por-bloques-comunes-2-eso-y-3-eso) 
Aunque inicialmente mantenía una lisa de recursos duplicada entre 2º ESO y 3º ESO, la misma en ambos casos por similitud de currículo, decido unificarla en 2017 Además de recursos comunes a 2º ESO, se pueden citar otros recursos: [Concepto de mol](/home/recursos/quimica/concepto-de-mol)  (en LOE se veía en 3º ESO, en LOMCE no aparece explícitamente hasta 4º ESO, pero se puede ver en 3º ya que se indican "cálculos estequiométricos sencillos")Vídeos Antonioprofe, 3º de ESO (14-15 años) [https://www.youtube.com/playlist?list=PL16zm8z0lIv3ci15MVaqXwlUhYb7Jkjzi](https://www.youtube.com/playlist?list=PL16zm8z0lIv3ci15MVaqXwlUhYb7Jkjzi) 

##  Recursos generales
Pueden combinar al mismo tiempo apuntes, ejercicios, simulaciones ...  
Curso completo online, apuntes y ejercicios  
* [Física y Química 3º ESO - CIDEAD](http://recursostic.educacion.es/secundaria/edad/3esofisicaquimica/index.htm)  cc-by-nc-sa  
Autores: Jesús M. Muñoz Calle, Luis Ramírez Vicente, Joaquín Recio Miñarro, Carlos Palacios Gómez, Mª Josefa Grima Rojas, Javier Soriano Falcó, Enric Ripoll Mira, José Luis san Emeterio Peña

[Proyecto "Antonio de Ulloa". Recursos para Química.](http://educalab.es/recursos/historico/ficha?recurso=579)  
cc-by-nc-sa. En 2021 no operativo online, incluye nivel 3º ESO.

[http://clasesfisicayquimica.blogspot.com.es/p/unidad-1-la-ciencia.html](http://clasesfisicayquimica.blogspot.com.es/p/unidad-1-la-ciencia) José Antonio Navarro  

[https://fisicayquimicaqueipo.wixsite.com/misitio-1/apuntes-y-ejercicios](https://fisicayquimicaqueipo.wixsite.com/misitio-1/apuntes-y-ejercicios) Apuntes y ejercicios. Salvador Molina Burgos  

Educamix: 3º E.S.O.: Materiales de Trabajo  
[http://platea.pntic.mec.es/pmarti1/educacion/3_eso_materiales/3_eso_materiales.htm](http://platea.pntic.mec.es/pmarti1/educacion/3_eso_materiales/3_eso_materiales.htm)  
Incluye materiales para el profesor. Pedro Martínez  

Departamento de Física y Química. IES "Rey Fernando VI" @ Jesús Millán Crespo  
[http://chopo.pntic.mec.es/jmillan/3_de_ESO.htm](http://chopo.pntic.mec.es/jmillan/3_de_ESO.htm)  

Blog de Física y Química. IES Juan de la Cierva  
[http://aula44.wordpress.com/3%C2%BA-eso/3%C2%BA-eso-fisica-y-quimica/](http://aula44.wordpress.com/3º-eso/3º-eso-fisica-y-quimica/)  

Catalán [https://sites.google.com/a/xtec.cat/planetaciencia/eso/fisica---quimica/fq-3-eso](https://sites.google.com/a/xtec.cat/planetaciencia/eso/fisica---quimica/fq-3-eso)  

Eureka. 3º F. y Q.  
[http://blog.educastur.es/eureka/otros-cursos/](http://blog.educastur.es/eureka/otros-cursos/)  

Recursos interactivos Física y Química 3º ESO  
[http://jcabello.es/fyq3eso.html](http://jcabello.es/fyq3eso.html)  

[http://e3fyq.blogspot.com.es](http://e3fyq.blogspot.com.es/)  
José Ignacio Melero. Apuntes, actividades, vídeos ...  [https://www.youtube.com/user/melerojosei/about](https://www.youtube.com/user/melerojosei/about)  

[http://fq3baeloclaudia.blogspot.com.es/](http://fq3baeloclaudia.blogspot.com.es/)  
José Manuel Aranda, IES Baelo Claudia  

CATEDU Centro Aragonés de Tecnologías EDUcativas, CIENCIARAGÓN, Portal aragonés para la enseñanza de la física y la química  
[http://www.catedu.es/cienciaragon/index.php?option=com_content&view=category&id=75&Itemid=45](http://www.catedu.es/cienciaragon/index.php?option=com_content&view=category&id=75&Itemid=45)  
Estructura de la materia, Electricidad, Medida, Reacciones químicas  

[Dpto ciencias colegio Virgen de la Vega. Física y Química 3ºESO](https://dptocienciascvv.blogspot.com/p/fisica-y-quimica-3-eso.html)  


##  Apuntes
 [http://web.educastur.princast.es/proyectos/fisquiweb/Apuntes/apun3.htm](http://web.educastur.princast.es/proyectos/fisquiweb/Apuntes/apun3.htm)  
 Fisquiweb, apuntes 3º ESO, cc-by-nc-sa Luis Ignacio García González  
 
 [http://www.bioygeo.info/ApuntesFQ3.htm](http://www.bioygeo.info/ApuntesFQ3.htm)  © manuelgvs 2007

###  Apuntes elaboración propia 3º ESO
Ver página separada  [apuntes elaboración propia física y química 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso/apuntes-elaboracion-propia-3-eso) 


##  Ejercicios
Problemas resueltos de física y química [http://www.profesorparticulardefisicayquimica.es/#terceroeso](http://www.profesorparticulardefisicayquimica.es/#terceroeso)  
Profesor: A. Zaragoza López  

[http://www.educa2.madrid.org/web/cesar.arenas/ejercicios3fyq](http://www.educa2.madrid.org/web/cesar.arenas/ejercicios3fyq)  
Incluye PDFs por tema que indican "FÍSICA Y QUÍMICA 3.° ESO MATERIAL FOTOCOPIABLE © SANTILLANA EDUCACIÓN, S. L."  
Hay ejercicios sencillos sobre la materia, cambios de estado, cambios físicos y químicos, movimiento, fuerzas, energía ...en  [ejercicios prueba libre graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)  y en  [ejercicios prueba de acceso Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio) 

###  Ejercicios elaboración propia 3º ESO
Inicialmente se pueden utilizar ejercicios de  [pruebas de acceso a grado medio](/home/pruebas-de-acceso-a-grado-medio)  y ejercicios de  [prueba libre de graduado ESO](/home/pruebas/pruebas-libres-graduado-eso) Además, dentro de los recursos de  [configuración electrónica](/home/recursos/quimica/recursos-configuracion-electronica)  y  [recursos sobre enlace químico](/home/recursos/quimica/enlace-quimico) , hay una selección de ejercicios PAU que son válidos para realizar en ESO.  


