
# Píldoras Física RSEF

Son documentos con ideas y recursos para su uso en clase, tal y como los describe la Real Sociedad Española de Física  
*Con las "Píldoras de Física" queremos ofrecer recursos útiles para la enseñanza de la Física, remitiendo a enlaces de la web que contengan alguna propuesta que, convenientemente adaptada y/o contextualizada por el profesorado, creemos que puede usarse para ayudar a docentes y estudiantes en el proceso de enseñanza-aprendizaje.*  
[http://rsef.es/noticias-actividades-geef](http://rsef.es/noticias-actividades-geef)  
En general los recursos que puedan citar deberían estar citados en la página concreta asociada al tema tratado  

