# Recursos teoría cinético-molecular

Se trata de un contenido que se introduce en  [Física y Química 2º (LOMCE)](/home/materias/eso/fisica-y-quimica-2-eso)  /  [Física y Química 3º ESO](/home/materias/eso/fisica-y-quimica-3-eso) , pero luego se utiliza en cursos posteriores, como en Química de 4º, 1º y 2º Bachillerato al razonar la cinética de las reacciones  
Se puede relacionar con varias cosas, hay cosas que están mezcladas y un mismo recurso puede aplicar a varias ideas:  
- Encajo dentro el tema de cambios de estado: ponerlo aislado es muy simple, creo que se ve lo suficiente antes de secundaria.  
- Gases como sistemas materiales (tienen masa, pueden golpear: ideas como aire al sacar mano del coche, echar CO<sub>2</sub> sobre una vela para apagarla, y vídeos como apagar velas con un puñetazo sin tocarlas  [twitter TapasDeCiencia/status/937290889474576385](https://twitter.com/TapasDeCiencia/status/937290889474576385) )  
- También se puede asociar a dilatación y  [recusos densidad](/home/recursos/fisica/recursos-densidad)  
- También asociable a  [sistemas materiales](/home/recursos/fisica/recursos-sistemas-materiales)  y propiedades como cambios de estado  

También asociable a [disoluciones](/home/recursos/quimica/disoluciones)  

##  Leyes de los gases
Ver en  [leyes de los gases](/home/recursos/quimica/leyes-de-los-gases) 

##  Concepto de presión y temperatura desde el punto de vista cinético-molecular
Temperatura como energía cinética media de traslación y presión como medida del número de choques y fuerzas contra paredes del recipietne.

##  Estados de agregación y sus propiedades desde el punto de vista cinético-molecular
Por ejemplo compresibilidad de los gases, poca compresibilidad de sólidos y líquidos.Dilatación de sólidos.Se puede citar el estado de plasma, y curiosidad, los televisores de plasma sí están relacionados con ese estado  
[Pantalla de plasma - wikipedia.org](https://es.wikipedia.org/wiki/Pantalla_de_plasma) 

[twitter obdriftwood/status/1541424217060741122](https://twitter.com/obdriftwood/status/1541424217060741122)  
![](https://pbs.twimg.com/media/FWQ9JtSXEAAVIYh?format=jpg)  
Autor es [@tomgauld](https://twitter.com/tomgauld). Libro “El departamento de teorías alucinantes”.  

##  Cambios de estado y gráficas de calentamiento
[Gráfica calentamiento de una sustancia, flash](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/media/flash/cambiosestado.swf)  
Tomado de un enlace no operativo en noviembre 2019, comparto una copia que realicé   

[Curva de calentamiento del agua - educaplus.org](http://www.educaplus.org/game/curva-de-calentamiento-del-agua)  
Permite ver el avance sobre la gráfica según se va calentando al tiempo que la termperatura y el estado, sin representación de partículas  

[Proyecto Ulloa (Recursos para Química). Estados de agregación y teoría cinética, 3º ESO  - recursostic.educacion.es](http://recursostic.educacion.es/ciencias/ulloa/web/ulloa2/3eso/secuencia3/menu)  
cc-by-nc-sa  
Incluye animación interactiva de cambios de estado con termómetro y representación partículas  

[Control de los cambios de estado del agua (Flash) - mhe.es](https://www.mhe.es/secundaria/cienciasnaturaleza/8448153847/archivos/media/esp/unidad_4/agua.swf)  

Muestra al tiempo visión de moléculas de agua  
[HeatingCurve (Flash) - ncssm.edu](http://www.dlt.ncssm.edu/tiger/Flash/phase/HeatingCurve)  

[Los cambios de estado: gráficas de calentamiento y enfriamiento - cienciadelux](https://cienciadelux.com/2016/07/08/la-teoria-cinetico-molecular-y-los-estados-de-agregacion-de-la-materia/)  
Blog de Enrique Castaños dedicado a la enseñanza y la divulgación de la ciencia  
[![Aprendiendo los cambios de estado (15 min, La 2 TVE, Educaixa)](https://img.youtube.com/vi/-zB5mPADaFY/0.jpg)](https://www.youtube.com/watch?v=-zB5mPADaFY "Aprendiendo los cambios de estado (15 min, La 2 TVE, Educaixa)")

3D molecular visualisation - Water turning into ice CSIRO's Data61  
[![](https://img.youtube.com/vi/zRUFzJrDtq0/0.jpg)](https://www.youtube.com/watch?v=zRUFzJrDtq0 "3D molecular visualisation - Water turning into ice CSIRO's Data61")


###  Efecto Leidenfrost
 [Efecto Leidenfrost - wikipedia.org](https://es.wikipedia.org/wiki/Efecto_Leidenfrost)  
 [Viral Video Shows Man Putting His Hand Through Molten Metal. So What's Really Going On? - iflscience.com](http://www.iflscience.com/physics/viral-video-shows-man-putting-his-hand-through-molten-metal-so-whats-really-going-on/) 

###  Punto triple
 [twitter FECYT_Ciencia/status/1258338913967575040](https://twitter.com/FECYT_Ciencia/status/1258338913967575040)  
 El agua puede estar, con la combinación de presión y temperatura adecuadas, en los estados sólido, líquido y gaseoso en un equilibrio estable. Se produce a una temperatura de 273,16 K (0,0098 °C) y a una presión parcial de vapor de agua de 611,73 pascales.  
 [Triple point of water GIF](https://gfycat.com/colorlesssardonicamazonparrot-triple-point) 

##  Movimiento browniano

[Brownian motion - minutelabs.io](http://labs.minutelabs.io/Brownian-Motion/)  

Incluye visión a microscopio
[![Brownian Motion (1 min)](https://img.youtube.com/vi/2Vdjin734gE/0.jpg)](https://www.youtube.com/watch?v=2Vdjin734gE "Brownian Motion (1 min)") 
 
Analogía con bola de corcho y bolitas más pequeñas  
[![Random Force & Brownian Motion - Sixty Symbols (5 min)](https://img.youtube.com/vi/FAdxd2Iv-UA/0.jpg "Random Force & Brownian Motion - Sixty Symbols (5 min)")](https://www.youtube.com/watch?v=FAdxd2Iv-UA)
 
[![Brownian motion simulation 2D (25 s)](https://img.youtube.com/vi/bhNPyUUtUDw/0.jpg "Brownian motion simulation 2D (25 s)")](https://www.youtube.com/watch?v=bhNPyUUtUDw)

[![Brownian motion simulation 3D (36 s)](https://img.youtube.com/vi/6VdMp46ZIL8/0.jpg "Brownian motion simulation 3D (36 s)")](https://www.youtube.com/watch?v=6VdMp46ZIL8)

[![What is Brownian motion? | Chemistry for All | The Fuse School (3 min)](https://img.youtube.com/vi/NHo6LTXdFns/0.jpg "What is Brownian motion? | Chemistry for All | The Fuse School (3 min)")](https://www.youtube.com/watch?v=NHo6LTXdFns)

##  Recursos simulaciones
Ver  [recursos de simulaciones](/home/recursos/simulaciones), en concreto:  

[Iniciación interactiva a la materia, Mariano Gaite Cuesta - mec.es](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/indice.htm)  (flash), incluye visualización estados y cambios de estado   

[Gas Molecules Simulation Applet - falstad.com](http://www.falstad.com/gas/)  

> This java applet is a simulation that demonstrates the kinetic theory of gases. The color of each molecule indicates the amount of kinetic energy it has. Use the Setup popup menu at the upper right to select one of several examples. At the bottom of the applet is a velocity histogram showing the distribution of velocities of the molecules. Again, color is used to indicate kinetic energy.  


[Teoría cinética de los gases. Física Estadística y Termodinámica - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica/estadistica/gasIdeal/gasIdeal)  

Dos simulaciones muy similares:
[Estados de la materia: intro - phet.colorado.edu](https://phet.colorado.edu/sims/html/states-of-matter-basics/latest/states-of-matter-basics_es) (HTML5, en esta hay dos opciones, estados y cambios de estado)  
[Estados de la materia - phet.colorado.edu](https://phet.colorado.edu/es/simulation/states-of-matter)  (HTML5, en esta hay 3 opciones, se añade respecto a la anterior, interacción, nivel Bachillerato)  
![](https://phet.colorado.edu/sims/html/states-of-matter/latest/states-of-matter-900.png)  

También se pueden poner vídeos y simulaciones sobre el movimiento browniano.  
Ver recursos en  [3.2. Estructura de la materia - catedu.es/cienciaragon _waybackmachine_](http://web.archive.org/web/20101130062748/http://www.catedu.es:80/cienciaragon/index.php?option=com_content&task=view&id=60&Itemid=44)  

[Estados de agregación de la materia - educaplus.org](http://www.educaplus.org/game/estados-de-agregacion-de-la-materia)  

Esta simulación de teachchemistry.org es similar a la anterior de educaplus, pero además incluye masa y la opción de calcular densidades  
[Density simulation - teachchemistry.org](https://teachchemistry.org/classroom-resources/the-density-simulation)  
AACT American Association of Chemistry Teachers  

[Solid, liquid, gas -sunflowerlearning.com](https://www.sunflowerlearning.com/launch/KY8477)  
Vídeo simple asociado a una app que es de pago  
[![Solids, liquids and gases,  Sunflower Learning - youtube](https://img.youtube.com/vi/kPOSeSLDe58/0.jpg "Solids, liquids and gases,  Sunflower Learning - youtube")](https://www.youtube.com/watch?v=kPOSeSLDe58)  

[twitter LebreroPastor/status/1736098297125744939](https://twitter.com/LebreroPastor/status/1736098297125744939)  
Os dejo por aquí una pequeña simulación, hecha con @algodoo, para poder visualizar el comportamiento de las moléculas de un gas, según la Teoría cinético molecular, dentro de una habitación. ¿Por qué olemos un perfume que se abre en el otro extremo de la habitación?  


