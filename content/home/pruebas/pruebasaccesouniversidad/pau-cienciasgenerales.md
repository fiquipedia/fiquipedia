# PAU Ciencias Generales

La materia de Ciencias Generales surge en 2º Bachillerato LOMLOE, ver [Ciencias Generales 2º de bachillerato](/home/materias/bachillerato/ciencias-generales-2-bachillerato).  
Hay contenidos asociables a Física y Química: de momento se enlazan enunciados, y ya se agruparán por bloques o se asociarán a recopilaciones de ejercicios de ciertos tipos.  
Por ejemplo el primer modelo de ABAU 2024 de Galicia tiene un ejercicio de termoquímica nivel Bachillerato, y otro de cinemática nivel ESO.   


##  [Recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales)](/home/recursos/recursospau/ficheros-enunciados-pau-cienciasgenerales) 
