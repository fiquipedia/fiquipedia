
# Material para aula laboratorio

 Asociado a  [prácticas de laboratorio](/home/recursos/practicas-experimentos-laboratorio) , aquí se intenta poner información sobre posibles materiales (fungibles y no fungibles), características, cómo usarlos, recomendaciones, sitios donde comprarlo.  
 
La información sobre pictogramas en material de laboratorio se coloca de momento en esta página.  
 
También incluye información asociada a colocación del laboratorio, manejo de materiales y su procesado  

Hay información asociada en [seguridad laboratorio](/home/recursos/quimica/seguridad-laboratorio)  
  
 [Beautiful Instruments &mdash; Beautiful Chemistry](https://www.beautifulchemistry.net/beautiful-instrument) Instrumentos de laboratorio, recreación histórica  

Cuestionarios 
[Material de laboratorio. Conoce el material que se usa en el laboratorio de química.](https://es.educaplay.com/recursos-educativos/624520-material_de_laboratorio.html)  

[Bancos de preguntas Moodle laboratorio - Enrique García de Bustos Sierra](https://www.educa2.madrid.org/web/enrique.garciadebustos/lafisicaesfacil/recursos/moodle)  


[MATERIAL DE LABORATORIO - gobiernodecanarias.org](http://www3.gobiernodecanarias.org/medusa/contenidosdigitales/ucticee/s2/CD120000011/#/lang/es/pag/b2fec68de2689236004b62d4ef5ca948)  

[twitter onio72/status/1452894155357708290](https://twitter.com/onio72/status/1452894155357708290)  
FQ2ESO Animación de @geogebra para mostrar en la pizarra el material de laboratorio   
[FQ2ESO T01 Material de laboratorio (pizarra)](https://www.geogebra.org/m/ypthguy5)  
Licencia by-nc-sa
Imágenes del recurso "Material de laboratorio" del Gobierno de Canarias  
Antonio González García   

[twitter onio72/status/1455438275590516737](https://twitter.com/onio72/status/1455438275590516737)  
Test sobre material de laboratorio para 2º ESO. Permite su inclusión en Moodle para exportar su puntuación a la hoja de calificaciones. Licencia by-nc-sa. Imágenes del recurso "Material de laboratorio" del Gobierno de Canarias.  
[FQ2ESO T01 Test material de laboratorio](https://www.geogebra.org/m/vgzkw29n)  

[Materiales. Laboratorio de ciencias - lamanzanadenewton.com](https://www.lamanzanadenewton.com/materiales/mat_m_lab1.html)  
- El material del laboratorio de Química
- Normas de comportamiento en el laboratorio

[![](https://static.wixstatic.com/media/66bfed_c590affbf65641c2a104d05bd8bcff26~mv2.png/v1/fill/w_980,h_577,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/66bfed_c590affbf65641c2a104d05bd8bcff26~mv2.png)](https://drive.google.com/drive/folders/1EiVHGOXLfHS0HUS2FbEIWTZ6xzPNIMcj)  

 [Material de laboratorio. Guía de prácticas de Química general e introducción al laboratorio químico. Primer curso grado en farmacia. Departamento de química inorgánica y bioinorgánica. Facultad de Farmacia. Universidad Complutense de Madrid](http://147.96.70.122/Manual_de_Practicas/2__material_de_laboratorio.htm)  

[Labobingo - Luis Moreno Martínez](https://luismormz.jimdo.com/labobingo/)  
Se trata de una adaptación del popular juego de bingo para reforzar el aprendizaje de los principales materiales de laboratorio.  
[Labobingo.pdf Colección de 44 cartones acompañados del solucionario en el que se enumeran los 20 materiales de laboratorio](https://luismormz.jimdo.com/app/download/10003658469/Labobingo.pdf?t=1699908799)  

## Documentación de material de laboratorio

### Calibre / pie de rey  

 [Calibre (instrumento) - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Calibre_%28instrumento%29)   

### Nonius
 [Nonio - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Nonio)   
Se utiliza en calibre / pie de rey y en más instrumentos  

### Eudiómetro
 [Eudiómetro - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Eudi%C3%B3metro)   

### Redes de difracción
Pueden aparecer algunas en el laboratorio y no tener especificaciones  
[Blende mit 3 Doppelspalten - leybold-shop.de](https://www.leybold-shop.de/46992.html)  
![](https://www.leybold-shop.de/media/phk/images/150dpi/46992.jpg)  
>    Technische Daten  
>    Kalibrierskala: 50-mm-Teilung  
>    Abmessungen (Rahmen): 50 mm · 50 mm  
>    Spaltbreite: 0,12 mm , Spaltenabstand: 0,6 mm  
>    Spaltbreite: 0,24 mm , Spaltenabstand: 0,6 mm  
>    Spaltbreite: 0,24 mm , Spaltenabstand: 1,2 mm  

### Cronovibrador

Se intenta colocar aquí información asociada a cronovibradores. Tiene varios usos, inicialmente recopilo información para poner operativa una cubeta de ondas con un vibrador con un electroimán y resistencia casi cero que no vibra. Es un material caro y en algunos centros hay material de dotaciones antiguas no operativos  

[Construcción de un cronovibrador casero - heurema-fq](https://heurema-fq.com/wp-content/uploads/2022/10/8MPF-Construccion-de-un-cronovibrador-casero.pdf)  

Una opción es acoplar un altavoz
[Cubeta de ondas - ucm.es](https://www.ucm.es/data/cont/docs/76-2013-07-09-07_Ripple_tank.pdf)  
> 1. Un oscilador en la forma de un altavoz con un soporte sujeto a su membrana.  
> 2. Una varilla enganchada al soporte de la membrana del altavoz, a la que se suelda otra varilla
perpendicular en el extremo opuesto al altavoz.  
> 3. Un sistema rígido de soporte del altavoz.  
> ...  
> Para montar la experiencia, en primer lugar se coloca el altavoz en su soporte, orientado hacia abajo,
y se coloca bajo él el recipiente de agua lleno hasta la mitad, colocando previamente una hoja de papel
blanco bajo el recipiente (para maximizar el efecto visual). Posteriormente, se engancha en la membrana
del altavoz la varilla rígida, de forma que el segmento perpendicular queda paralelo a la superficie del
agua, y en contacto con ésta. Por último, se conecta el oscilador al altavoz y se ilumina el recipiente con
la lámpara estroboscópica, eligiendo para su frecuencia la misma que la del oscilador, típicamente entre
10 y 30 Hz. De esta forma, al oscilar la varilla verticalmente, lo mismo hace la superficie del agua en
contacto con ella, generando en el recipiente olas con geometría de onda plana.  

Opciones comerciales:

[Física > Mecánica > Magnitudes > Tiempo, frecuencia, pulsos. 1212.1236 Cronovibrador, acero niquel - didaciencia](https://www.didaciencia.com/product?id=1236)  

[Accesorios de mecánica > CRONOVIBRADOR - ibdciencia](https://www.ibdciencia.com/es/accesorios-de-mecanica-para-laboratorio/234-cronovibrador.html)  


## Sitios donde comprar material / productos químicos  

 Productos Aldo: Reactivos, productos químicos y complementos  
 [Los productos de aldo (redirige a Hanssell)](http://www.losproductosdealdo.com/)   
 [Hanssell](http://www.hanssell.eu)   

Para reactivos, una marca muy conocida es panreac  

En 2018  [Panreac](http://www.panreac.com/)  remite a  [Home - ITW Reagents](https://www.itwreagents.com/iberia/en/home)   
 [pharma-production-excipients](https://www.itwreagents.com/iberia/en/pharma-production-excipients)   

 [CymitQuimica](https://cymitquimica.com/)  
 [Material didáctico-práctico para laboratorios de ciencias - IBD ciencia](http://www.ibdciencia.com/)   
  
 [Gama de Productos PHYWE para Secundarias y Bachilleratos](http://www.phywe-es.com/884/Escuela.htm)   
    
 [Ventus Ciencia Experimental, S.L.](http://www.ventusciencia.com/)   
  
 [Distribución de material para la enseñanza - NAVAS](http://www.navasdistribuciones.com/)   

 [Dazek - Dazek Quimicos Laboratorio](http://dazek.jimdo.com/)   
  
 [Labotienda - tienda online de material de laboratorio](http://www.labotienda.com/)   

 [Comprar Productos Químicos Online - VadeQuímica](https://www.vadequimica.com/)   

 [PRODUCTOS QU&Iacute;MICOS - Tienda MR1866](http://manuelriesgo.com/461-productos-quimicos)   
  
 [Tienda de productos químicos - Mezcla Perfecta](https://mezclaperfecta.com/)   

 [PRODUCTOS QUIMICOS | Material de laborario | Equipos de laborario | Letslab.es](http://www.letslab.es/productos-quimicos.lab)   

 [CTS](https://shop-espana.ctseurope.com/)  

 [Física - 3bscientific.com.es](https://www.3bscientific.com.es/fisica,pg_83.html)   

 [Proserquisa. Equipo de laboratorio didáctico](https://proserquisa.com/principal/inicio/)  

 [Leboriz](https://www.leboriz.com/)  
 
 [Laboquimia](http://www.laboquimia.es/index.php)  
LABOQUIMIA es una empresa española distribuidora y fabricante de material de laboratorio de ámbito multinacional.   

  Material para cristalización y cristalografía  
  [trianatech.com](http://trianatech.com/)   
Sobre compra de reactivos "a granel"  
En 2017 me comentan que parece que no se puede comprar ya agua oxigenada al 30% como se hacía antes, y sin embargo sí permiten comprar bidones grandes. Parece una incongruencia, y tras buscar información, parece que la lógica es que por seguridad solamente se permite vender productos etiquetados completos, para seguimiento de compradores. El agua oxigenada antes se tenía en bidones grandes y se vendía a granel, pero ahora no es posible así, aunque en algunos sitios siguen teniendo recipientes de 1 L etiquetados y sigue siendo posible.  


## Sitios para reciclar material
 [Requimsa ](http://www.requimsa.es/)   
  
 [Recogida de residuos y destrucción de documentación](http://www.acicla.es/)   
  
Otra opción es contactar con el punto limpio de la localidad, que según el tamaño de la localidad puede ocuparse previo aviso  [productos químicos en IES - FORO Docentes con Educación](http://docentesconeducacion.es/viewtopic.php?f=57&t=6094&p=27251#p27251)   

En la documentación sobre gestión de recursos (ver apartado normativa sobre gestión de recursos) se cita un " listado de la Comunidad de Madrid de Gestores de Residuos Peligrosos" de 2018 que en 2023 es

[Listados de gestores y transportistas de residuos - comunidad.madrid](https://www.comunidad.madrid/servicios/urbanismo-medio-ambiente/listados-gestores-transportistas-residuos)  

[Empresas autorizadas por la Comunidad de Madrid para la realización de actividades de gestión de residuos peligrosos. Actualizado al 05 de mayo de 2023](https://www.comunidad.madrid/sites/default/files/doc/medio-ambiente/listado_gestores_residuos_peligrosos_8.pdf)   
donde aparece

>160506 PRODUCTOS QUÍMICOS DE LABORATORIO QUE CONSISTEN EN, O CONTINEN, SUSTANCIAS PELIGROSAS, INCLUIDAS LAS MEZCLAS DE PRODUCTOS QUÍMICOS DE LABORATORIO.

Ejemplo

> Razón social: AMBAR PLUS SL   
> CIF: B80554512  
> Teléfono: 918775300  
> Fax: 918827017  
> Nº de inscripción/autorización: AAI/MD/G18/15175  
> Dirección del centro: CARRETERA AJALVIR, Km. 2,2. 28806 Alcalá de Henares - Madrid    
> NIMA: 2800070018  
> Alcance: CENTRO DE TRANSFERENCIA DE RESIDUOS PELIGROSOS  

## Documentación sobre colocación de material en laboratorio 

[Chemical Storage Compatibility - university of Wisconsin](https://www.wisconsin.edu/ehs/hazmat/chemical-storage/)  

[Chemical Segregation and Storage Table - nih.gov](https://ors.od.nih.gov/sr/dohs/Documents/chemical-segregation-table.pdf)  

[Dangerous Goods & Combustible Liquids Storage Compatibility Chart](https://www.australiansafetysigns.net.au/products/dangerous-goods-combustible-liquids-storage-compatibility-chart?variant=870686241)  

[Hazardous Chemical & Waste Compatibility Chart - Adhesive Vinyl](https://www.hclco.com/Hazardous_Chemical_Compatibility_Chart_p/ssp-0112.htm)  

[NTP 725: Seguridad en el laboratorio: almacenamiento de productos químicos](https://www.insst.es/documents/94886/327446/ntp_725.pdf/8d7db0e4-c89d-4b56-94da-c554b1abee32)  

[GRADO EN INGENIERÍA QUÍMICA. Registro, clasificación, organización y almacenamiento seguro de productos químicos en un laboratorio. TFG 2017](https://uvadoc.uva.es/bitstream/handle/10324/25735/TFG-I-706.pdf)  

[ALMACENAMIENTO SEGURO DE PRODUCTOS QUÍMICOS Servicio de Prevención de Riesgos laborales - unirioja.es](https://www.unirioja.es/servicios/sprl/pdf/almacenamiento_pq.pdf)  

## Normativa sobre gestión de residuos
  
  
[S02 Almacenamiento de productos químicos en el laboratorio](https://www.youtube.com/watch?v=13LVhrDm54Q)    
Se citan frases R asociada a riesgos y S de seguridad son una fuente de información sobre gestión de residuos  
Aparecen en las fichas de seguridad, y por ejemplo para S60 Elimínese el producto y su recipiente como residuos peligrosos  

Se puede ver en 
[Fichas Internacionales de Seguridad Química (ICSCs)](http://www.ilo.org/dyn/icsc/showcard.listcards3?p_lang=es)   
  
INFORME TÉCNICO DE ASESORAMIENTO  
RESIDUOS PELIGROSOS EN CENTROS EDUCATIVOS DE EDUCACIÓN SECUNDARIA DE LA COMUNIDAD DE MADRID  
Ref.: 19OASRCO1  
Dirección General de la Función Pública  
VICEPRESIDENCIA, CONSEJERÍA DE PRESIDENCIA Y PORTAVOCÍA DEL GOBIERNO  
csv 1239408976140655076386  

[Residuos peligrosos en centros docentes de secundaria: gestión intracentro. Instituto Nacional de Seguridad en el Trabajo](https://www.insst.es/documents/94886/327740/ntp-767.pdf/44f422c9-b6a6-44af-8d4a-dddc4befc169)  
Se citan

REFERENCIAS LEGALES Y BIBLIOGRAFIA
(1) [Ley 31/1995 de 8.11. (Jef. Est., BOE 10.11.1995). Ley de prevención de riesgos laborales.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-24292)  
(2) [Real Decreto 952/1997 de 5.7. (M. M. Amb., BOE 5.7.1997), por el que se modifica el Reglamento para la ejecución de la Ley 20/1986, de 14 de mayo, Básica de Residuos Tóxicos y Peligrosos, aprobado mediante Real Decreto 833/1988, de 20 de junio.](https://www.boe.es/buscar/doc.php?id=BOE-A-1997-14934)  
(3) [Ley 11/1997 de 24.4 (Jef. Est., BOE 25.4.1997). Ley de envases y residuos de envases.](https://www.boe.es/buscar/act.php?id=BOE-A-1997-8875) derogada por [Real Decreto 1055/2022, de 27 de diciembre, de envases y residuos de envases.](https://www.boe.es/buscar/doc.php?id=BOE-A-2022-22690)  
(4) [Ley 10/1998 de 21.4. (Jef. Est., BOE 22.4.1998). Ley de residuos.](https://www.boe.es/buscar/act.php?id=BOE-A-1998-9478) derogada por [Ley 22/2011, de 28 de julio, de residuos y suelos contaminados](https://www.boe.es/buscar/act.php?id=BOE-A-2011-13046) a su vez derogada por [Ley 7/2022, de 8 de abril, de residuos y suelos contaminados para una economía circular.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5809)  
(5) [Real Decreto 374/2001 de 6.4. (M. Presid., BBOOE 1.5, rect. 30.5 y 22.6.2001). Protección de la salud y seguridad de los trabajadores contra los riesgos relacionados con los agentes químicos durante el trabajo.](https://www.boe.es/buscar/act.php?id=BOE-A-2001-8436)  
(6) [Orden MAM/304/2002 de 8.2. (BBOOE 19.2, rect. 12.3.2002) por la que se publican las operaciones de valorización y eliminación de residuos y la lista europea de residuos.](https://www.boe.es/buscar/act.php?id=BOE-A-2002-3285) derogado por [Ley 7/2022, de 8 de abril, de residuos y suelos contaminados para una economía circular.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5809)  
(7) INSTITUTO NACIONAL DE SEGURIDAD E HIGIENE EN EL TRABAJO. Notas Técnicas de Prevención (NTP). Barcelona. INSHT.

