
# Recursos hologramas

Es algo que enlaza con  [recursos apps](/home/recursos/recursos-apps)  que implican uso de móvil.

##  [](/) Realizar el proyector para móvil
(En inglés DIY son las siglas de Do It Yourself)  
[http://www.cafundo.tv/diyhologram/diyhologram_sheet_cafundo.pdf](http://www.cafundo.tv/diyhologram/diyhologram_sheet_cafundo.pdf)  
No CD Case, No Tape 3D Hologram Pyramid: the Quickest and Easiest Way to Make Your Own. (templates Included)  
[https://www.instructables.com/id/No-CD-case-no-tape-3D-hologram-pyramid-the-quickes/](https://www.instructables.com/id/No-CD-case-no-tape-3D-hologram-pyramid-the-quickes/)  
[https://www.wikihow.com/Make-a-Holographic-Illusion-Pyramid](https://www.wikihow.com/Make-a-Holographic-Illusion-Pyramid)  
[![](https://www.wikihow.com/images/thumb/0/0d/Make-a-Hologram-Step-11-Version-2.jpg/aid7045377-v4-728px-Make-a-Hologram-Step-11-Version-2.jpg.webp "") ](https://www.wikihow.com/images/thumb/0/0d/Make-a-Hologram-Step-11-Version-2.jpg/aid7045377-v4-728px-Make-a-Hologram-Step-11-Version-2.jpg.webp)  

[https://maker.pro/custom/projects/diy-hologram](https://maker.pro/custom/projects/diy-hologram)  

[https://androtalk.es/2015/08/construye-tu-propio-proyector-holografico-y-usa-tu-movil-para-ver-videos-en-3d/](https://androtalk.es/2015/08/construye-tu-propio-proyector-holografico-y-usa-tu-movil-para-ver-videos-en-3d/)  

Cómo hacer un holograma casero para el móvil o celular (Experimentos Caseros) (6:42) ExpCaseros  
[https://www.youtube.com/watch?v=OIaL0jLmFC0](https://www.youtube.com/watch?v=OIaL0jLmFC0)  

Aprende a hacer un holograma casero! (3:47) unComo  
[https://www.youtube.com/watch?v=Lh6dgD6AsrE](https://www.youtube.com/watch?v=Lh6dgD6AsrE)  

Como Hacer un Gigante Proyector de Hologramas Casero (DIY Giant Hologram Projector) (6:16) Experimentar En Casa  
[https://www.youtube.com/watch?v=JXSNcz0eczM](https://www.youtube.com/watch?v=JXSNcz0eczM) 

##  [](/) Vídeos de hologramas
Se pueden encontrar algunos: la idea sería poder crearlos para un objeto concreto.  
Suele haber enlaces a vídeos junto a las instrucciones para realizar el proyector.  

Vídeo de átomos para hologramas (50 s) Habilidad verde [https://www.youtube.com/watch?v=VySOUW1L8Ww](https://www.youtube.com/watch?v=VySOUW1L8Ww)  

[https://twitter.com/Biochiky/status/1074416160349806593](https://twitter.com/Biochiky/status/1074416160349806593)  
Holograma casero del átomo.  

[https://twitter.com/ProfaDeQuimica/status/1076075949769281536](https://twitter.com/ProfaDeQuimica/status/1076075949769281536)  
Y aquí el video con el holograma tridimensional animado.¡Gracias, @Biochiky, por la idea!   

 
