
# Juegos

En esta página intento recopilar juegos relacionados con Física y Química o ciencia en general.Hay simulaciones o recursos interactivos que a veces tienen un planteamiento como juego (por ejemplo de PhET sims, dan estrellas de puntuación / ponen caras sonrientes al acertar)  
  
En general son juegos no virtuales, también hay cosas asociadas a realidad virtual  
Hay algunos dispersos en algunas páginas:   
 [Recursos tabla periódica /elementos químicos](/home/recursos/quimica/recursos-tabla-periodica)   
 [Recursos orbitales](/home/recursos/quimica/recursos-configuracion-electronica)   
 [Apuntes formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion)  (por ejemplo dados o fichas plastificadas)  
A veces se pueden plantear adaptar otros:  
 [https://twitter.com/iesbovalar/status/1045401939977150464](https://twitter.com/iesbovalar/status/1045401939977150464) Se pueden hacer cartas con científicos (y mezclar con no científicos para una visión histórica global), con inventos o descubrimientos ... y hacer líneas de tiempo y asociaciones.  
  
El CEL (Catálogo de Experiencias Lúdicas) es un banco de experiencias de uso de juegos de mesa en el aula como recurso educativo (Aprendizaje Basado en Juegos). [Experiencias &#8211; Catàleg d&#039;Experiències Lúdiques](https://www.elcel.org/es/experiencias/)  
[Juegos &#8211; Catàleg d&#039;Experiències Lúdiques](https://www.elcel.org/es/juegos/)   
  
 [112. Repasando Física y Química con Password &#8211; Catàleg d&#039;Experiències Lúdiques](https://www.elcel.org/es/experiencias/112-repasando-fisica-y-quimica-con-password/)  
 Adaptación del juego televisivo «Password» que recoge los distintos contenidos relativos a Física y Química (nivel 4º ESO).El juego está compuesto de 36 cartas (con 4 palabras por carta) y los alumnos, organizados en grupos de 6 y formando 3 parejas entre sí, competirán por adivinar el mayor número de palabras durante sus respectivos turnos.  
 [Password-Física-y-Química.pdf](https://www.elcel.org/wp-content/uploads/jobmonster/Password-F%C3%ADsica-y-Qu%C3%ADmica.pdf)   
 
