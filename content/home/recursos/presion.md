---
aliases:
  - /home/recursos/recursos-presion/
---

# Recursos presión

Borrador para ir colocando cosas  
Asociado a  [leyes gases](/home/recursos/quimica/leyes-de-los-gases) ,  [fuerzas en fluidos](/home/recursos/fisica/fuerzas-en-fluidos).  
También enlaza con [termodinámica](/home/recursos/termodinamica/) por el papel de la presión, y con [teoría cinético molecular](/home/recursos/recursos-teoria-cinetica/) asociado a choques, cambios de estado, diagramas de fases.  

[recursostic.educacion.es Experimento de Torricelli (swf)](http://recursostic.educacion.es/secundaria/edad/1esobiologia/1quincena5/paginas/presionatm.swf) 

##  Unidades de presión
Se introducen unidades (Pa, atm, bar, mmHg)  
Aparte de Pa (pascal), hay otras aceptadas por el SI  
[bipm.org 8th edition of the SI Brochure (2006)](https://www.bipm.org/documents/20126/41483022/si_brochure_8.pdf/632d85cd-cd9f-cfae-21d2-9ffcfc2bb198?version=1.6&download=true) Table 8. Other non-SI units
|  Quantity |  Name of unit  |  Symbol for unit  |  Value in SI unit | 
|:-|:-|:-|:-|
|  pressure |  bar (a) | bar  |  1 bar = 0.1 MPa = 100 kPa = 105 Pa  | 
|  |  millimetre of mercury (b) | mmHg |  1 mmHg ≈ 133.322 Pa |  

(a) The bar and its symbol are included in Resolution 7 of the 9th CGPM (1948; CR, 70). Since 1982 one bar has been used as the standard pressure for tabulating all thermodynamic data. Prior to 1982 the standard pressure used to be the standard atmosphere, equal to 1.013 25 bar, or 101 325 Pa.  
(b) The millimetre of mercury is a legal unit for the measurement of blood pressure in some countries.  

[monografias.com Introducción a la neumática](https://www.monografias.com/docs111/introduccion-neumatica/introduccion-neumatica.shtml)  
1/10 bar (10,000 Pa) es aprox. lo más fuerte que una persona promedio puede soplar

###  La atmósfera (unidad)
Una atm son 101325 Pa (en libro de texto reciente veo que indica 101300 Pa, y son 101235 Pa desde 1954)  
[bipm.org Definition of the standard atmosphere](https://www.bipm.org/en/committees/cg/cgpm/10-1954/resolution-4)  
*1 standard atmosphere = 1 013 250 dynes per square centimetre, i.e., 101 325 newtons per square metre.*  
In 1954, the definition of the atmosphere was revised by the [10e Conférence Générale des Poids et Mesures (10th CGPM)](https://www.bipm.org/en/committees/cg/cgpm/10-1954/resolution-4) to the currently accepted definition: one atmosphere is equal to 101325 pascals. The torr was then redefined as 1/760 of one atmosphere. This yields a precise definition that is unambiguous and independent of measurements of the density of mercury or the acceleration due to gravity on Earth.

###  Milímetros de mercurio
El símbolo es mmHg según SI: junto, no separado.  
Se puede usar torr (nombre torr, símbolo Torr), pero se desaconseja  

[nist.gov NIST Guide to the SI, Chapter 5: Units Outside the SI](https://www.nist.gov/pml/special-publication-811/nist-guide-si-chapter-5-units-outside-si)  
Table 11. Example of other unacceptable units
|  Name | Symbol  |  Value in SI units | 
|:-|:-|:-|
|  torr | Torr  | 1 Torr = (101 325/760) Pa  |  

[wikipedia.org Torr Nomenclature and common errors](https://en.wikipedia.org/wiki/Torr#Nomenclature_and_common_errors)  
The unit's name torr is written in lower case, while its symbol ("Torr") is always written with upper-case initial; including in combinations with prefixes and other unit symbols, as in "mTorr" (millitorr) or "Torr⋅L/s" (torr-litres per second).\[2] The symbol (uppercase) should be used with prefix symbols (thus, mTorr and millitorr are correct, but mtorr and milliTorr are not).The torr is sometimes incorrectly denoted by the symbol "T", which is the SI symbol for the tesla, the unit measuring the strength of a magnetic field. Although frequently encountered, the alternative spelling "Tor" is incorrect.

##  Presión estándar
Citado en apuntes de termoquímica de Bachillerato  
[goldbook.iupac.org STP](http://goldbook.iupac.org/html/S/S06036.html)  
STP  
Abbreviation for standard temperature (273.15 K or 0 °C) and pressure (105 Pa); usually employed in reporting gas volumes. Note that flow meters calibrated in standard gas volumes per unit time often refer to volumes at 25 °C, not 0 °C.

##  Ejercicios
 [matematicasfisicaquimica.com Resolución del Ejercicio FQ4EE1940, de Presión para Física y Química de 4º de la E.S.O.](http://www.matematicasfisicaquimica.com/ejercicios-fisica-y-quimica-4-eso/226-ejercicios-presion-fisica-4-eso/1320-ejercicio-resuelto-presion-densidad-4-eso-fisica.html) 


## Imágenes / vídeos
Pueden estar asociados a experimentos  
[![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Plastic_bottle_at_14000_feet%2C_9000_feet_and_1000_feet%2C_sealed_at_14000_feet.png/640px-Plastic_bottle_at_14000_feet%2C_9000_feet_and_1000_feet%2C_sealed_at_14000_feet.png )](https://commons.wikimedia.org/wiki/File:Plastic_bottle_at_14000_feet,_9000_feet_and_1000_feet,_sealed_at_14000_feet.png "Plastic bottle at 14000 feet, 9000 feet and 1000 feet, sealed at 14000 feet")  

¿SE PUEDE HERVIR AGUA CON LA GRAVEDAD? Diagrama de fases del AGUA - TERMODINÁMICA - HRom  
[![](https://img.youtube.com/vi/npbSzAUVR9w/0.jpg)](https://www.youtube.com/watch?v=npbSzAUVR9w "¿SE PUEDE HERVIR AGUA CON LA GRAVEDAD? Diagrama de fases del AGUA - TERMODINÁMICA - HRom")   



[![](https://img.youtube.com/vi/ejWf8iXjXZk/0.jpg)](https://www.youtube.com/watch?v=ejWf8iXjXZk "Slo-mo Balloon Bang! - Bang Goes the Theory - BBC One")  

##  Experimentos

###  Experimento aplastar lata con presión atmosférica
 [![Experimentos para entender la presión atmosférica](https://img.youtube.com/vi/ZL30PJ1fF6Q/0.jpg)](https://www.youtube.com/watch?v=ZL30PJ1fF6Q "Experimentos para entender la presión atmosférica") 

###  Experimento presión, vela y agua que sube
 [naukas.com La presión y la vela](https://naukas.com/2015/02/24/la-presion-y-la-vela/)  
 [naukas.com Sí, pero no por eso…](https://naukas.com/2015/02/08/si-pero-no-por-eso/)  
 
 Vídeo hecho en laboratorio  
  [twitter garlegor/status/1060236327734706178](https://twitter.com/garlegor/status/1060236327734706178) 

### Presión sobre una hoja de papel 

[Break a ruler with #atmosphericpressure ! 📏 #physics #tamu #science #fyp #education #learnontiktok #doitathome](https://www.tiktok.com/@tamuphysastr/video/7114701810849353003)  
Tomado de [tamuphysastr TAMU Physics & Astronomy](https://www.tiktok.com/@tamuphysastr)  

### Ludion / diablillo de Descartes

Se comenta en prácticas de laboratorio  
[twitter LebreroPastor/status/1648441769292615680](https://twitter.com/LebreroPastor/status/1648441769292615680)  
Os dejo por aquí un ludión o diablillo de descartes que hemos hecho para nuestras charlas y del que hoy han disfrutado l@s chic@s del @criedenavaleno. Han estado cerca de dejarlo suspendido en el centro. Hay que ser muy finos para conseguirlo.

###  Experimentos
 [twitter ciencia__infusa/status/1351423391035744256](https://twitter.com/ciencia__infusa/status/1351423391035744256)  
 Ya habíamos explicado la manera más eficiente de vaciar una botella, pero @coolchemistryguy nos muestra cómo podemos aprovechar una disminución súbita de presión para llenarla en menos de un segundo #presion #vacio #botella #experimento  
 
 
 
