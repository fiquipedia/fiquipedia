
# Situaciones de aprendizaje

Página creada para albergar recursos que se citen como situaciones de aprendizaje.  

Tal y como se comenta en [Currículo LOMLOE: Situaciones de aprendizaje](/home/legislacion-educativa/lomloe/curriculo-situaciones-de-aprendizaje/) también se pueden considerar actividades, y a veces se citan como unidades didácticas.   

Algunos recursos pueden ser situaciones aprendizaje en el currículo y estar aquí y/o en [Currículo LOMLOE: Situaciones de aprendizaje](/home/legislacion-educativa/lomloe/curriculo-situaciones-de-aprendizaje/), donde también se cita Documentación sobre diseño de situaciones de aprendizaje

Pueden haber también situaciones de aprendizaje dentro de las páginas asociadas a ciertos contenidos.  
No tienen por qué estar limitadas a Física y Química.  
Pueden estar asociados a [REA Recursos Educativos Abiertos](/home/recursos/recursos-educativos-abiertos)

[Situaciones de aprendizaje - intef.es](https://intef.es/recursos-educativos/situaciones-aprendizaje/)  
Con buscador por niveles  

[El misterioso envenenamiento del señor Lafarge. Situación de aprendizaje diseñada para la materia de Física y Química y dirigida a alumnado de 3.º de ESO. - intef](https://descargas.intef.es/recursos_educativos/ODES_SGOA/ESO/FQ/3B.1_-_El_misterioso_envenenamiento/crditos_y_descarga.html)  


[Trabajo experimental con potenciales y campos eléctricos Situación de aprendizaje diseñada para la materia de Física de 2.º de Bachillerato - intef](https://descargas.intef.es/recursos_educativos/ODES_SGOA/Bachillerato/FQ/3B.7_-_Campos_elctricos/index.html)  

[Las aplicaciones de la física nuclear. Situación de aprendizaje diseñada para la materia de Física de 2.º de Bachillerato - intef](https://descargas.intef.es/recursos_educativos/ODES_SGOA/Bachillerato/FQ/3B.6_-_Fsica_nuclear/index.html)  


[El desafío de los plásticos. Situación de aprendizaje diseñada para la materia de Química de 2.º de Bachillerato - intef](https://descargas.intef.es/recursos_educativos/ODES_SGOA/Bachillerato/FQ/3B.5_-_El_desafo_de_los_plsticos/index.html)  


[Proyecto EDIA: Recursos Educativos Abiertos](https://cedec.intef.es/proyecto-edia/)  
EDIA ofrece un banco de REA con situaciones de aprendizaje para Infantil, Primaria, Secundaria, Bachillerato y Formación Profesional.  

[Proyecto EDIA. Recurso de Física y Química para 3º de Secundaria. “El arte de formular”](https://cedec.intef.es/proyecto-edia-recurso-de-fisica-y-quimica-para-3o-de-secundaria-el-arte-de-formular/)  
Proyecto / situación de aprendizaje: al final se realiza en grupo un concurso de formulación e individualmente una guía de formulación


[Recursos LOMLOE y Diseño de Situaciones de Aprendizaje](https://sites.google.com/g.educaand.es/recursoslomloeysda)  
La información que este SITE contiene procede del SERVICIO DE ORDENACIÓN DE ENSEÑANZAS DE REGIMEN GENERAL de la JUNTA DE ANDALUCÍA, excepto alguna de la que se indicará su procedencia.  

[Transformación Digital Educativa Proyecto REA/DUA Situaciones de aprendizaje y guías didácticas - juntadeandalucia](https://www.juntadeandalucia.es/educacion/portals/web/transformacion-digital-educativa/situaciones-de-aprendizaje-y-guias-didacticas)  

[Programa CREA. Recursos - educarex](https://programacrea.educarex.es/recursos)  
> El Programa “Creación de Recursos Educativos Abiertos” (CREA) apoya y fomenta la innovación y el éxito educativos a partir de la filosofía REA (Recursos Educativos Abiertos) auspiciada por la UNESCO y el modelo de Educación Abierta. Además, promueve nuevos modelos de enseñanza-aprendizaje apoyados en los recursos educativos abiertos (REA), y ofrece apoyo y orientación a aquellos docentes que comienzan a generar, adaptar y aplicar en el aula sus recursos educativos abiertos para ponerlos a disposición de la comunidad educativa.  
El Programa CREA se sitúa dentro de INNOVATED, el Plan de Educación y Competencia Digital de Extremadura que ayuda a la comunidad educativa a integrar las tecnologías en los procesos educativos y a desarrollar programas innovadores. Está publicado en la Instrucción 14/2024 de la Dirección General de Formación Profesional, Innovación e Inclusión Educativa y recoge tanto los objetivos que se desean alcanzar como los programas en que centros educativos y docentes pueden participar.

[Sitúate. Revista Digital Situaciones de Aprendizaje - gobiernodecanarias.org](https://www3.gobiernodecanarias.org/medusa/ecoescuela/sa/revistas/?revista=34&mes=noviembre&anio=2019)  
> Se inicia en 2014 y finaliza sus publicaciones en 2019.  

Se puede ver por fechas que es LOMCE, anterior a LOMLOE, pero usa el término situaciones de aprendizaje

[Situaciones de aprendizaje. Ángel Vázquez Sánchez - recursospdifgl](https://www.recursospdifgl.com/recursos-y-actividades-educativas/situaciones-de-aprendizaje/)  

[LOMLOE. Materiales de apoyo. DOCUMENTOS - educastur.es](https://www.educastur.es/lomloe-materiales-apoyo-documentos/-/document_library/gz9QuWYItBrY/view/2768644)  

[SA_Física_y_Química_ Bachillerato.pdf - educastur.es](https://www.educastur.es/documents/34868/2768644/SA_F%C3%ADsica_y_Qu%C3%ADmica_+Bachillerato.pdf/e32bc9ae-7f1c-a25b-9e81-d4f5e453e8d4?version=1.0&t=1672321937900)  

[twitter aratecno/status/1612866047299784741](https://twitter.com/aratecno/status/1612866047299784741)  
SA ESO en el currículo de Aragón. Este documento recoge las situaciones de aprendizaje de Secundaria del currículo de Aragón. Están divididas por asignaturas y se indica: título (si lo hay), nivel, temporalización, descripción y nº de página donde aparece  
[SITUACIONES DE APRENDIZAJE CURRÍCULO DE ARAGÓN](https://drive.google.com/file/d/1P-EvBeQE08pkQwtIicGhYq6w_YkcQpi6/view)  

[MEDIATECA x Fundación Pryconsa. Materiales de enriquecimiento educativo para alumnos y profesores de Educación Infantil, Primaria, Secundaria Obligatoria y Bachillerato](https://www.fundacionpryconsa.es/mediateca/)  
> La MEDIATECA x Fundación Pryconsa surge del Convenio entre la Fundación Pryconsa y la Consejería de Educación y Universidades de la Comunidad de Madrid. Ofrece materiales para dar respuesta a las necesidades del alumnado con altas capacidades intelectuales en el aula inclusiva, como eje vertebrador del Programa de diferenciación curricular de la Comunidad de Madrid.  
Es el resultado de la labor de más de 20 docentes y otros profesionales educativos, cuenta con **Situaciones de aprendizaje** vinculadas al currículo de la nueva legislación LOMLOE, e incluye materiales para el alumnado, guías para el profesorado y más material complementario, todo ello listo para poder descargar con licencias de uso abiertas.  
Todo el contenido de la MEDIATECA x Fundación Pryconsa tiene carácter educativo y se encuentra bajo esta licencia Creative Commons.

[MEDIATECA x Fundación Pryconsa. Búsqueda por áreas, niveles y competencias clave](https://www.fundacionpryconsa.es/mediateca/buscador/)  


Vuelvo a citar esto mencionado en [Currículo LOMLOE: Situaciones de aprendizaje](/home/legislacion-educativa/lomloe/curriculo-situaciones-de-aprendizaje/) pero poniendo aquí los ejemplos  

[twitter currofisico/status/1788859206889615513](https://x.com/currofisico/status/1788859206889615513)  
Lo que veo en el área de física y química, y supongo que en las demás áreas lo mismo, es una deriva hacia una "tecnificación de la evaluación", ...rúbricas absurdas, idoceistas, trabajo cooperativo sin rumbo, uso de las tic sin un fin claro...¿No sabemos hacer situaciones de aprendizaje? Es como si nunca hubiéramos hecho nada y estuviéramos descubriendo la pólvora por primera vez. Lo llames situaciones de aprendizaje, o como te de la gana, lo que subyace a la dificultad del diseño es que no tenemos claro qué es lo que queremos que el alumnado aprenda cegados por el ruido normativo. Todos/as deseando que vengan las editoriales a evangelizarnos comercialmente con sus fantasticas propuestas. Pues hay cosas hechas hace años que son joyas, delicias de leer, estudiar e implementar...pero hay q echarle un rato.  
Se incluyen capturas de dos libros y de bibliografía  
- Ciencias de la naturaleza, Cuarto curso de Educación Secundaria Obligatoria, Ministerio de Educación y Ciencia, Edelvives  
- [Cinemática y dinámica ;Antonio Carmona García-Galán ... ](https://datos.bne.es/edicion/bimo0000134194.html)  ISBN 	84-87215-41-6   
Y dos capturas de bibliografía  

Con tiempo intentaré ponerlo aquí como texto y comentar algunas  
* [La "metodología de la superficialidad" y el aprendizaje de las ciencias March 1985 Enseñanza de las Ciencias Revista de investigación y experiencias didácticas 3(2):113-120](https://www.researchgate.net/publication/303471418_La_metodologia_de_la_superficialidad_y_el_aprendizaje_de_las_ciencias)  
Tiene dos cuestionarios interesantes, con resultados obtenidos por alumnos y docentes  

Similar
[Ciencias de la naturaleza. Secundaria obligatoria 4º curso. Materiales didácticos 1. Ministerio de Educación y Ciencia. Centro de desarrollo curricular](https://www.libreria.educacion.gob.es/libro/ciencias-de-la-naturaleza-secundaria-obligatoria-4o-curso-materiales-didacticos-1_147538/)  .Luis Rodríguez Barreiros , María Jesús Caballer Senabre , Jesús Molledo Cea , Félix A. Gutiérrez Muzquiz  
[pdf 178 páginas](https://www.libreria.educacion.gob.es/ebook/182334/free_download/)  

[Bienestar emocional en el ámbito educativo](https://dgbilinguismoycalidad.educa.madrid.org/bienestaremocional/)  
> Las Unidades Didácticas/**Situaciones de Aprendizaje** suponen un programa formativo completo en todos los factores que influyen en el Bienestar Emocional del alumnado y también pueden ser utilizados de manera independiente para abordar un tema en concreto.  

Puede haber recursos asociados a situaciones de aprendizaje en 
[Scientix.eu resources](https://www.scientix.eu/resources)  

Puede haber recursos asociados en [El nou currículum. Batxillerat. Activitats d’avaluació - xtec.cat](https://projectes.xtec.cat/nou-curriculum/batxillerat/activitats-avaluacio/)  
Nou currículum - Batxillerat : Exemples  
[GCI Gestió i comunicació de la informació](https://www.google.com/url?q=https://drive.google.com/file/d/1ceozBcIUrVqprwHW7RlDmD6NB7yJFTC2/view?usp%3Dshare_link&sa=D&source=editors&ust=1728066752307775&usg=AOvVaw3a4Lb6HzE7ATGNwl3GfiQp)  
[RP Resolució de problemes](https://www.google.com/url?q=https://drive.google.com/file/d/1sudt_HuO28DruY9Scwh4r6W2DuKW0z_t/view?usp%3Dshare_link&sa=D&source=editors&ust=1728066752307827&usg=AOvVaw2rYc1oDOkQFlw8v4QmgSfa)  
[PC Pensament crític](https://www.google.com/url?q=https://drive.google.com/file/d/1NTgywLhJY5cWDD7CtMJlPVyvTWeGa0Db/view?usp%3Dshare_link&sa=D&source=editors&ust=1728066752307879&usg=AOvVaw0sKSirD60xK-7sQUPMBO7I)




