# Memes

Los memes son un recurso didáctico más, ya que permiten expresar ciertas ideas de manera cercana al lenguaje de los alumnos; de hecho los pueden realizar los propios alumnos.  

En la página había enlazado algunos memes, y puede haber por varios sitios, pero creo esta página en 2021 para centralizar información sobre memes, de Física, de Química y de ciencia en general. La idea de hacer una recopilación surge comentando algunos memes compartidos por Beatríz Mahíllo realizados por sus alumnos de Física 2º Bachillerato en 2019. La idea en general, como en el resto de la página, es enlazar, solo almacenando aquí en casos especiales, citando autor y tras pedir permiso.  

Pendiente separar, se puede relacionar con imágenes de camisetas  
[twitter DayInLab/status/1479081145576955906](https://twitter.com/DayInLab/status/1479081145576955906)  
[Official Science Like Magic But Real Shirt](https://mysimplyshirt.com/product/official-science-like-magic-but-real-shirt/)  
[twitter FiQuiPedia/status/589511966986805249](https://twitter.com/FiQuiPedia/status/589511966986805249)  
[twitter saravarela_97/status/632101532226469888](https://twitter.com/saravarela_97/status/632101532226469888)  
[twitter FerFrias/status/1485606154826829831](https://twitter.com/FerFrias/status/1485606154826829831)  

Asociado a memes existe el concepto de "Troll Physics"  
[Troll Science / Troll Physics - knowyourmeme.com	](https://knowyourmeme.com/memes/troll-science-troll-physics)  
Troll Science, also known as “Troll Physics”, is a rage comic series illustrating various experiments that are based on faulty understanding of scientific concepts. The intentionally pseudo-scientific drawings are often used to mislead the viewers or frustrate science educators and students for their obvious errors.  

![](https://i.kym-cdn.com/photos/images/newsfeed/000/074/256/1285770302993.jpg)  
[Why Can’t a Magnet-Propelled Truck Actually Work?](https://www.wired.com/story/why-cant-a-magnet-propelled-truck-actually-work/)  


Sin ser memes, hay ideas de imágenes interesantes  
[Science logotypes. Lalaland graphics](https://m.facebook.com/media/set/?set=a.333370443494953.1073741829.332375486927782&type=1)  

También enlaza con chistes y bromas
[Chemistry Jokes and Puns - chemicalaid](https://www.chemicalaid.com/fun/jokes.php)  


## Otros sitios donde enlazo memes

Algunas cosas que recuerdo en varios sitios. Puede haber cosas que no son memes como imágenes curiosas asociadas a la tabla periódica.  

* [No podemos poner eso](https://algoquedaquedecir.blogspot.com/2018/03/no-podemos-poner-eso.html)  
![](https://pbs.twimg.com/media/DQZQ1P7UIAAGvtn.jpg "Al que me empuje, le parto la cara. Sr. Newton, no podemos poner eso.
Está bien, entonces pone ... A toda acción corresponde una reacción de igual magnitud, pero de sentido contrario")  
* [Apuntes formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion/)  
![](https://pbs.twimg.com/media/Enwi8VoXcAEIGRr?format=jpg)  
* [Dinámica](/home/recursos/fisica/dinamica/)  
![](https://pbs.twimg.com/media/Ds4H6eEWsAUWrCR.jpg)    
* [Campo eléctrico](/home/recursos/fisica/recursos-campo-electrico/)  
![](https://pbs.twimg.com/media/E0cgMUUWQAAIp6A?format=jpg)  
* [Estructura del átomo](/home/recursos/quimica/recursos-estructura-del-atomo/)
![](https://i0.wp.com/quimicafacil.net/wp-content/uploads/2020/04/Modelos-atomicos-muerte.jpg)  
* [Configuración electrónica](/home/recursos/quimica/recursos-configuracion-electronica/)  
![](https://pics.me.me/3d10-1s2-2s2-2p6-3s2-3p6-biotecno-memes-3d10-4s2-46238487.png)  
* [Recursos uso calculadora](/home/recursos/recursos-uso-calculadora/)  
![](https://64.media.tumblr.com/468cfa314c24fde9112e9912176eb76a/tumblr_msckn7TOZo1sa0ryio1_500.jpg)  

## Usos de memes

[twitter Librosalaula/status/1495837884066287620](https://twitter.com/Librosalaula/status/1495837884066287620)  
Venga, marchando los pdf. Están pensados para papel de pegatinas de 3x7   
Primera plantilla:  
[memes pegatinas.docx.pdf](https://drive.google.com/file/d/1is_xnQYgcmmNa9CH85j9fLagTmZP9Ayv/view)  

## Algunos memes

[twitter memecrashes/status/1696899815852933193](https://twitter.com/memecrashes/status/1696899815852933193)  
Physicists:  
You cannot take g=10 it's 9.8!  
Also physicists:  
Cat is dead and alive  
sin x = x  
Let penguin be a cylinder  
Assume no friction  
Imagine ideal body  
![](https://pbs.twimg.com/media/F4yZYQfWUAA43Cn?format=jpg)  

[twitter Wikingenieria/status/1802326087394894066](https://x.com/Wikingenieria/status/1802326087394894066)  
Qué significará la K  
kilo, constante de Coulomb, kelvin, constante de velocidad, constante elástica, constante de Henry, potasio, kaón, número angular de onda, constante de equilibrio  
![](https://pbs.twimg.com/media/GQMmBS-WYAED4ng?format=jpg)  
 

Physics course, Repourposed mathematics  
![](https://preview.redd.it/h1wvpli4f8091.jpg?width=640&crop=smart&auto=webp&s=82df41ac6d2c0cec8a07726bbd9c38bf899e8933)  

Noble gas , guillotine  
![](https://preview.redd.it/72d1vqnxcvz81.jpg?width=640&crop=smart&auto=webp&s=4b8ef20d704e1fbcf281b2ea2a8b907297ac06ee)  

Weak int. Electromagnetism Strong Int. Gravity  
![](https://preview.redd.it/0gmq8jl12fz81.jpg?width=640&crop=smart&auto=webp&s=d6270961a74e6d4af377a446638874ed7bd89490)  

Electron spin
![](https://i.redd.it/5p5pdlwfos091.jpg)  

Ironman female
![](https://preview.redd.it/u50am37h3wx81.png?width=640&crop=smart&auto=webp&s=37ccec67baa37d94b414725ae87366c7767caa06)  

Termodinámica
![](https://preview.redd.it/xlkpojspbpz81.jpg?width=640&crop=smart&auto=webp&s=9cb252148e240fa54c46551f4d1daa14138990a0)  

[twitter memecrashes/status/1671500645482786817](https://twitter.com/memecrashes/status/1671500645482786817)  
Observer quantum state  
![](https://pbs.twimg.com/media/FzJc-e3XwAg1rso?format=jpg)  

[twitter EliLevensonFalk/status/1310777022113697793](https://twitter.com/EliLevensonFalk/status/1310777022113697793)  
![](https://pbs.twimg.com/media/EjDP_ARUwAAoPf1?format=jpg)  
GR, CMP, E&M, Chemistry, Optics, Pretty much everything else ... being explained by QM   

## Memes química
When I remember that the chemists from before pipet the H2SO4 with their mouths  
![](https://preview.redd.it/v7gho844ac091.jpg?width=640&crop=smart&auto=webp&s=4db1458b7f06f17c4ed2078148c597f5da94801e)  

Chemists in 1925 vs Chemists now 
![](https://i.redd.it/d4lzivzkhtz81.jpg)  

Tautomería cetona-enol
![](https://i.redd.it/bgcu18zcqb191.jpg)  

Notación esqueletal
![](https://i.redd.it/ofhmhfkqzz191.jpg)  

[Chemistry nobel - xkcd](https://xkcd.com/2214/)  
![](https://imgs.xkcd.com/comics/chemistry_nobel.png)  

OH Na any halogen acid
![](https://i.redd.it/osxdx4u1gaz81.jpg)  

[twitter i_junaid_hanif/status/1644956952605360129](https://twitter.com/i_junaid_hanif/status/1644956952605360129)  
Only chemistry student can understand  
![](https://pbs.twimg.com/media/FtQPoWQaUAA9eA3?format=jpg)  
aeroplane, aeroplene, aeroplyne  

## Memes Beatríz Mahíllo 2019

En 2021 la cuenta de Twitter tiene candado y comparto las imágenes almacenadas en local tras pedir permiso.   

[twitter bjmahillo/status/1191360405152714752](https://twitter.com/bjmahillo/status/1191360405152714752)  
Tras el primer y desastroso examen de Física de 2° Bach. he mandado a los alumnos hacer memes con sus errores.   
Porque no se extrañan cuando:   
- Les salen planetas de masas de 60 kg.  
- Los periodos de satélites son de 0,3 s.  
- La gravedad de un planeta vale 20000 m/s2.  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-04-bjmahillo-CuandoTusAlumnosNoHanCambiadoLasUnidades.jpeg)  
Ya me han entregado los alumnos de Física de 2º Bach. los memes que han hecho con los fallos de su último examen. Espero que les haya servido para repasar los errores más frecuentes y no se vuelvan a repetir.  
Lo han hecho muy bien y nos hemos reído mucho.  
Aquí los mejores  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoElRadioTeDaMenor.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoEstasComentandoLosResultados.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoHasHechoBienElProcedimientoDelEjercicio.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoNoSustituyesLosDatos.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoSeTeOlvidaDibujar.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoSeTeOlvidaElevarLaVelocidadAlCuadrado.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoTeDaUnTrabajoNegativoY.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoTeDaUnaDistanciaNegativa.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoTeSaleBienElProblema.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-CuandoTienesTodoElProcesoBien.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-DeducirLasFormulasYLasUnidades.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-DibujarLosEjes.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-LaEpEsNegativaYLoSabes.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-LaProfesoraAlVerQueNoPonesLosResultadosEnNotacionCientifica.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-LaProfesoraAlVerQueSacasVescConFgFcp.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-MeDisisteQuePodiaUtilizarla.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-PasarLaCalculadoraDeRadianesAGrados.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-SacarBuenaNota.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-VamoAOlvidarnoDeLasUnidades.jpeg)  
![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/memes/2019-11-18-bjmahillo-YoBienFelizConElResultado.jpeg)  

## Memes Marta 2022
[twitter martamedeunavez/status/1526858975421026304](https://twitter.com/martamedeunavez/status/1526858975421026304)   
Efecto Doppler (desplazamiento hacia el rojo / azul)  
![](https://pbs.twimg.com/media/FTB-JO7XEAIqxii?format=jpg)  
Efecto fotoeléctrico  
![](https://pbs.twimg.com/media/FTB-JO7XEAIqxii?format=jpg)  
Los platos de Schrödinger  
![](https://pbs.twimg.com/media/FTCHiWfWAAEgMry?format=jpg)  
Why are there measurement deviations between your results  
![](https://pbs.twimg.com/media/FTCHiwrWYAEpF0R?format=jpg)  
Has excess electrons  
![](https://pbs.twimg.com/media/FTCH4dkWIAEs6uO?format=jpg)  
Schrödinger cat  
![](https://pbs.twimg.com/media/FTCH4o-XwAAIJO8?format=jpg)  
Potencial energy - kinetic energy  
![](https://pbs.twimg.com/media/FTCH4-PX0AApj3K?format=jpg)  
O+O=O2, C+O2 = CO2, N2+H2=NH3  
![](https://pbs.twimg.com/media/FTCHcYbWYAAas_7?format=jpg)  
Ferrari, Agrrari, Aurrari  
![](https://pbs.twimg.com/media/FTCHcrTXwAE58AB?format=jpg)  
p=mv vs Ek  
![](https://pbs.twimg.com/media/FTCIYNRWIAA57IL?format=jpg)  

## Memes matemáticas y otras ciencias distintas Física y Química  
A veces chistes  
[twitter tocamates/status/1538645421509189634](https://twitter.com/tocamates/status/1538645421509189634)  
¿Me ayudas? Estoy recopilando chistes (gráficos o textuales) que se puedan utilizar en primaria o secundaria para enseñar matemáticas.  
![](https://pbs.twimg.com/media/FVpd2VmX0AAT-xU?format=jpg)  
![](https://pbs.twimg.com/media/FVpd2eIXoAADKdt?format=jpg)  
![](https://pbs.twimg.com/media/FVpd2pdWUAE27RD?format=jpg)  
![](https://pbs.twimg.com/media/FVpd213WIAEccXQ?format=jpg)  

[twitter p_millerd/status/1548305118038110209](https://twitter.com/p_millerd/status/1548305118038110209)  
This maps well to my life  
![](https://pbs.twimg.com/media/FXyvOsxVsAEG9zS?format=png)  

[twitter navarrotradmed/status/987446764566347777](https://twitter.com/navarrotradmed/status/987446764566347777)  
Sonrían, por favor, que se viene el fin de semana. @tomgauld en @newscientist
![](https://pbs.twimg.com/media/DbQdl4zXkAA8f5U?format=jpg)  


## Generadores de memes

* [memegenerator.es](https://www.memegenerator.es/)  
* [Meme Generator - imgflip.com](https://imgflip.com/memegenerator)  
* [meme-generator.com](https://meme-generator.com/)  
* [memegenerator.net](https://memegenerator.net/)  
* [meme maker - meme-generator.net](https://meme-generator.net/meme-maker)  
* [make a meme](https://makeameme.org/)  

* [Supermeme AI](https://www.supermeme.ai/)  Turn text into memes using AI. Describe your thoughts and let AI create original memes for you. In 110+ languages.  

Hay algunas "categorías de memes", por ejemplo "What people think I do", que se puede buscar para profesores, químicos, físicos ...
![](https://qph.cf2.quoracdn.net/main-qimg-6a8bd87113894a81dbb1265b897da528)  

## Fuentes de memes en ciencia

Intento recopilar algunas, lista borrador. Pueden salir puntualmente en algunas cuentas, aunque hay cuentas centradas en memes.  

[Memes FIKASI](https://sites.google.com/view/fikasi/memes)  
[twitter FIKASI1/status/1585336821248032779](https://twitter.com/FIKASI1/status/1585336821248032779)  

[A meme page to check every time MatLab crashes @memecrashes](https://twitter.com/memecrashes)  

[Memes de Astronomía @MemesDeAstro](https://twitter.com/MemesDeAstro)  

[@biotecnomemes](https://twitter.com/biotecnomemes)  

[Science - me.me](https://me.me/t/science)  

[Memedroid](https://www.memedroid.com/)  

[18 Ingeniosos memes científicos que combinan perfectamente humor y conocimiento - boredpanda.es](https://www.boredpanda.es/memes-ciencia/)  

Número 10  
![](https://static.boredpanda.es/blog/wp-content/uploads/2021/07/memes-ciencia-12-60dd9bd682553__700.jpg)  
Número 11  
![](https://static.boredpanda.com/blog/wp-content/uploads/2021/06/60d98cdb67fc7_sp9650jr1xc61__700.jpg)  

Usuarios de pinterest que recopilan y comparten  
[Memes de química. Colección de Bruna Macedo](https://br.pinterest.com/brumacedoo/memes-de-qu%C3%ADmica/)  

[reddit physicsmemes](https://www.reddit.com/r/physicsmemes/)  

[reddit chemistrymemes](https://www.reddit.com/r/chemistrymemes/)  

En 2022 el colegio de químicos de Madrid pone tuits, a veces con memes  
[twitter ColAsocQuimMad/status/1552958084267606017](https://twitter.com/ColAsocQuimMad/status/1552958084267606017)  

[twitter BigVanCiencia/status/1575446288874741763](https://twitter.com/BigVanCiencia/status/1575446288874741763)  
![](https://pbs.twimg.com/media/FcO8iNIWAAYKgyp?format=jpg)  
![](https://pbs.twimg.com/media/FcO7-ocWYAERlqB?format=jpg)  

[twitter ChemistryTe/status/1579549539786452993](https://twitter.com/ChemistryTe/status/1579549539786452993)  
![](https://pbs.twimg.com/media/Feuv5dHXwAI5ze9?format=jpg)  
