# Recursos test online

Asociados a crear test para realizar online, pueden no estar relacionados con física y química o ya elaborados  
Generales:  

* moodle (las aulas virtuales de EducaMadrid son Moodle), ...  
* [http://www.daypo.com/](http://www.daypo.com/)  
* [http://www.thatquiz.org/es/](http://www.thatquiz.org/es/)  
tiene algunos tests y permite diseñar e importar tests creados  

Algunas cosas relacionadas (puede ser gamificación, códigos QR, ...) como tantas páginas un batiburrillo por guardar aquí los enlaces  
* [https://kahoot.it/](https://kahoot.it/) (Ahí se juega con un código que se proporciona entrando en kahoot.com tras registrarse lleva a  [https://create.kahoot.it/](https://create.kahoot.it/)  y ahí buscar un test o diseñarlo) 
* [https://www.socrative.com/](https://www.socrative.com/)  
* [https://www.goconqr.com/](https://www.goconqr.com/)  
* [https://www.plickers.com/](https://www.plickers.com/)  
* [https://twitter.com/ProfaDeQuimica/status/934416845737775104](https://twitter.com/ProfaDeQuimica/status/934416845737775104)  
Yo desde que lo he descubierto he dejado a Kahoot/Socrative de lado. Al menos en mi centro, donde los alumnos no tienen tablets (ni el centro suficiente dotación de pcs) pero si hay ordenador+proyector en cada aula. Y es muy rápido y fácil de usar 
