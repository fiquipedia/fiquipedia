---
aliases:
  - /home/materias/eso/fisica-y-quimica-2-eso/recursos-fisica-y-quimica-2-eso/
---

# Recursos Física y Química 2º ESO

Ver  [recursos comunes a 2º y 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-por-bloques-comunes-2-eso-y-3-eso)  
* [Apuntes elaboración propia Física y Química 2º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso/apuntes-elaboracion-propia-fisica-y-quimica-2-eso)   
* [Apuntes 2º ESO - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2eso)  Rodrigo Alcaraz de la Osa  cc-by-sa
* [Apuntes 2º ESO - fisquiweb](http://fisquiweb.es/Apuntes/apun2.htm)  Luis Ignacio García González  
* [Didactica Física y Química](http://didacticafisicaquimica.es/)   
 [Física y Química de 2º de ESO. Libro del alumno](http://didacticafisicaquimica.es/?p=1504&preview=true)   
 [Física y Química de 2º de ESO. Secuencia de actividades](http://didacticafisicaquimica.es/?p=1519&preview=true)   
 [Física y Química de 2º de ESO. Libro del profesor](http://didacticafisicaquimica.es/?p=1528&preview=true)   
* [2ºESO FyQ - profesorjrc.es](https://profesorjrc.es/2eso.htm) Jorge Rojo Carrascosa  
* [2ºESO FyQ - quifiblogeso](http://quifiblogeso.blogspot.com/search/label/2%C2%BA%20ESO) Inma Rodríguez  
* [2ºESO FyQ - elortegui.org](http://ciencia.elortegui.org/datos/2ESO/ESO2.htm)  

# Recursos Física y Química 2º ESO (LOMCE)

Los recursos podrán estar como  [apuntes](/home/recursos/apuntes) ,  [ejercicios](/home/recursos/ejercicios) , asociados a enlaces dentro de la  [materia](/home/materias)  y su currículo ...  
Se ponen inicialmente enlaces a algunas páginas, al ser genéricos habrá recursos no asociados a este nivel de 2º ESO.   
Por la similitud de currículo puede haber recursos asociados o similares en  [recursos física y química de 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso)   
También puede haber cosas asociadas a ciencias naturales 2º ESO, materia LOE que trataba partes  
  
Aunque inicialmente mantenía una lisa de recursos duplicada entre 2º ESO y 3º ESO, la misma en ambos casos por similitud de currículo, y veía que la gente accedía primero a esta página (ya que había pocas cosas en internet que indicasen asociadas a 2º ESO) y no mira la otra, decido unificarla en 2017 en   

[Recursos Física y Química por bloques comunes 2º ESO y 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-por-bloques-comunes-2-eso-y-3-eso) 
  
Pendiente enlazar los recursos asociados,...  
  
En 2018 creo materiales de teoría "pizarras" para cubrir todo el curso en  [pizarras física y química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel)   
  
 Apuntes Física y Química 2º ESO Fisquiweb, cc-by-nc-sa  
 [FisQuiWeb. Apuntes 2. ESO](http://fisquiweb.es/Apuntes/apun2.htm)   
 
[IES Albero. Física y Química 2º ESO ](https://www.iesalbero.es/page-section/2-eso-fisica-y-quimica-1/)  
En octubre 2021 tiene EJERCICIOS DE CLASE, EJERCICIOS DE REPASO, PLANES DE TRABAJO

* Presentaciones Rafael Ruiz Guerrero [https://es.slideshare.net/rafaruizguerrero/tema-2-lamateriaysuspropiedadesfq2eso](https://es.slideshare.net/rafaruizguerrero/tema-2-lamateriaysuspropiedadesfq2eso)  
* [https://es.slideshare.net/rafaruizguerrero/la-materia-y-su-diversidad-tema-3-fsica-y-qumica-2-eso](https://es.slideshare.net/rafaruizguerrero/la-materia-y-su-diversidad-tema-3-fsica-y-qumica-2-eso)  
* [https://es.slideshare.net/rafaruizguerrero/tema-5-los-cambios-qumicos-fsica-y-qumica-2-eso](https://es.slideshare.net/rafaruizguerrero/tema-5-los-cambios-qumicos-fsica-y-qumica-2-eso)  

  
 [ http://fq2arandajm.blogspot.com.es/](http://fq2arandajm.blogspot.com.es/)   
José Manuel Aranda  
 [fisica-y-quimica-2-eso.html](http://clasesfisicayquimica.blogspot.com.es/p/fisica-y-quimica-2-eso.html) José Antonio Navarro [materiales](http://fq.iespm.es/index.php/secundaria-obligatoria/fisica-y-quimica-2-eso/materiales)   
 Materiales para ESO, incluyendo 2º ESO  
 [fq_eso](http://fisicayquimicaenflash.es/eso/eso.html)   
  
 [EL GATO DE SCHRÖDINGER. Blog de física y química. 2º ESO](https://www3.gobiernodecanarias.org/medusa/ecoblog/mramrodp/category/cna-2o-eso/)   
  
 Javier Robledano  
 [ FQ 2ºESO | Clasefyqrobledano | EducaMadrid ](https://www.educa2.madrid.org/web/clasefyq/fq-2-eso)   
Vídeos youtube Antonioprofe, 2º de ESO (13-14 años)  
 [2º de ESO (13-14 años) - YouTube](https://www.youtube.com/playlist?list=PL16zm8z0lIv16dqCv1Z_8q-c2cd1nigUH)   
  
Pendiente crear currículo con enlaces a recursos, actualizando con currículo LOMCE que se implanta en curso 2016-2017  
  
Rol online para repasar Física y Química en 2º ESO  
 Leticia Isabel Cabezas Bermejo  
 Licenciada y Doctora en Ingeniería Química. Departamento de Física y Química. IES Clavero Fernández de Córdoba. Almagro (Ciudad Real)  
 Cuadernos de Pedagogía, Sección Experiencia / Secundaria, 20 de Julio de 2020, Wolters Kluwer  
 «La bruma maldita» es una aventura de rol diseñada para realizar un repaso curricular de Física y Química de 2º de ESO. Se ha llevado a cabo entre la evaluación ordinaria y extraordinaria por vía online en pleno confinamiento por la Covid-19.  
 [Documento.aspx](http://www.cuadernosdepedagogia.com/Content/Documento.aspx?params=H4sIAAAAAAAEAFXMuwrDMAxG4bfxrDoknTTFj-C9qNFvMDVScS6Qt2-zFHrmw1dVOWX6NtyGaZzCgb5WN44Uie6Rgrkip5l3U5Rq0Gup5Uy-5PMNLtJWBDzdX3_S4ycsDdKTbJilwVQ6577jAwZVn2N6AAAAWKE)   
 [materiales.html](http://currofisico.blogspot.com/p/materiales.html)   




## Ejercicios
 Hay ejercicios sencillos sobre la materia, cambios de estado, cambios físicos y químicos, movimiento, fuerzas, energía ...en  [ejercicios prueba libre graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)  y en  [ejercicios prueba de acceso Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio)   
Ejercicios asociados a  [Ciencias de la Naturaleza 1º y 2º ESO](/home/materias/eso/ciencias-de-la-naturaleza-1y2eso)  que pueden ser aplicables a Física y Química 2º ESO  
 [14-b-·-cuaderno-verano-2º-eso.html](http://6con02.com/24-bloques/ciencias-naturales/14-b-%C2%B7-cuaderno-verano-2%C2%BA-eso.html)  [cuad_verano_2eso.pdf](http://6con02.com/_data/cuadernos/cuad_verano_2eso.pdf)  
