# PAU Tecnología Industrial

La materia de [Tecnología Industrial II de 2º Bachillerato](https://www.fiquipedia.es/home/materias/bachillerato/tecnologia-industrial-2bachillerato/) desaparece con LOMLOE, en cierto modo ocupa su lugar [Tecnología e Ingeniería 2º de bachillerato](/home/materias/bachillerato/tecnologia-e-ingeneria-2-bachillerato).  

Hay contenidos asociables a Física y Química: tiene problemas de Termodinámica tratada en 1º Bachillerato LOMLOE.  

##  [Recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales) de Tecnología Industrial II de 2º Bachillerato](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/PAUxComunidades/tecnologiaindustrial) 
De momento no en formato tabla como en otras materias 
