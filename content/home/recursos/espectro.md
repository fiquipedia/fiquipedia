---
aliases:
  - /home/recursos/recursos-espectro/
---

# Recursos relativos al espectro

En 2023 creo materiales para un taller sobre el espectro, con teoría y práctica sobre espectro sonoro y electromagnético  
[Taller espectro](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-espectro/TallerEspectro.pdf)  

## Recursos generales
El espectro de la radiación, Carlos S. Chinea  
 [casanchi.com is for sale | HugeDomains](http://casanchi.com/did/er.htm)  
CONCEPTOS DE TRANSFERENCIA RADIATIVA  
Atenuación de la radiación en la atmósfera [Module contents](http://rammb.cira.colostate.edu/wmovl/VRL/Tutorials/euromet/courses/spanish/satmet/s2720/contentf.htm) 

[twitter NataliaVartan/status/1454746534512402433](https://twitter.com/NataliaVartan/status/1454746534512402433)  
¿Alguna vez os habéis preguntado por qué el agua es transparente? La respuesta a esta pregunta es más compleja de lo que parece y tiene un origen que a mí me parece fascinante. Venid si queréis y lo vemos  
Bueno, he decir que el agua no es completamente transparente, sino un poquito pequeño azul. A esto volveremos más tarde, pero lo cierto es que si estamos bajo agua y abrimos los ojos, vemos a través de ella. Esto es la clave de todo, así que quedémonos con ello y no lo olvidemos.  
El primer paso de nuestro camino hacia una respuesta es el espectro electromagnético. La luz es una onda electromagnética y la clasificamos en diferentes tipos en función de su longitud: de la distancia entre los picos de la onda.  
Así, de mayor a menor distancia, la dividimos en ondas de radio, microondas, infrarrojo, visible, ultravioleta, rayos-X y rayos gamma (ver imagen). El ojo humano sólo puede ver en un rango de esas longitudes. Es decir, solo ve la luz cuya onda tiene cierta distancia entre picos.  


## Concepto de espectro
Un tema fundamental en física y química es entender el concepto de espectro, que se entiende relativo a  [espectro de frecuencias](http://es.wikipedia.org/wiki/Espectro_de_frecuencias)  (o de longitudes de onda)  
El concepto es aplicable a distintos tipos de ondas: desde espectro acústico (que se puede relacionar con los ecualizadores y la representación gráfica de muchos reproductores de música), hasta espectro electromagnético (muy relacionado con espectros atómicos)  
  
Por ello puede haber recursos relacionados con  [movimiento ondulatorio](/home/recursos/fisica/movimiento-ondulatorio)  (con relación directa con fenómenos como Doppler) y con  [óptica física](/home/recursos/fisica/recursos-optica-fisica)   
  
Una introducción básica está como parte del curso " [Iniciación interactiva a la materia](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/index.html) ", en  [Espectros at&oacute;micos](http://concurso.cnice.mec.es/cnice2005/93_iniciacion_interactiva_materia/curso/materiales/atomo/espectros.htm) , material con copyright de Mariano Gaite Cuesta  
  
Una simulación útil para visualizar la idea de espectros atómicos de emisión es  [Luces de Neón y otras Lámparas de Descarga - PhET](http://phet.colorado.edu/es/simulation/discharge-lamps)  que en agosto 2022 no es HTML5, necesita Java  
![](https://phet.colorado.edu/sims/discharge-lamps/discharge-lamps-600.png)  
  
Poster CERN sobre espectro.  [Copyright CERN, condiciones de uso](http://copyright.web.cern.ch/) .  
 [RF basics - CERN Document Server](http://cds.cern.ch/record/1615396)   
También se puede asociar a espectrografía en química, que enlaza con espectrómetros de masas como un caso concreto (en  [recursos de campo magnético](/home/recursos/fisica/recursos-campo-magnetico) ). [indespectro.html](https://www.liceoagb.es/quimiorg/indespectro.html)   
 [Esquema de un espectrofotómetro | Química Orgánica](http://www.quimicaorganica.net/esquema-espectrofot%C3%B3metro.html)   

### Imágenes resumen espectro electromagnético  

[![](https://static.wixstatic.com/media/66bfed_518065a26d9341babf4b06634207080e~mv2.png/v1/fill/w_980,h_577,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/66bfed_518065a26d9341babf4b06634207080e~mv2.png)](https://bit.ly/3p7oMUs)  

 [Archivo:Electromagnetic spectrum-es.svg - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Archivo:Electromagnetic_spectrum-es.svg)  cc-by-sa  
 [![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Electromagnetic_spectrum-es.svg/800px-Electromagnetic_spectrum-es.svg.png "") ](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Electromagnetic_spectrum-es.svg/800px-Electromagnetic_spectrum-es.svg.png)   
 [Archivo:EM Spectrum Properties es.svg - Wikipedia, la enciclopedia libre](https://es.m.wikipedia.org/wiki/Archivo:EM_Spectrum_Properties_es.svg)  
 [![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/EM_Spectrum_Properties_es.svg/700px-EM_Spectrum_Properties_es.svg.png "") ](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/EM_Spectrum_Properties_es.svg/700px-EM_Spectrum_Properties_es.svg.png)   

Humor  
[xkcd en español](https://es.xkcd.com/strips/espectro-electromagnetico/)   
  
Espectro divulgativo para niños  
[El espectro electromagnético, versión «para niños» | Microsiervos (Ciencia)](http://www.microsiervos.com/archivo/ciencia/espectro-electromagnetico-para-ninos.html)   
 [Electromagnetic spectrum | ALMA Kids](http://kids.alma.cl/?portfolio=el-espectro-electromagnetico)   

### Ensayos a la llama
Relacionado con experimentos laboratorio [](http://www.fqsaja.com/?portfolio_page=espectros-atomicos-ii)   

##  [Espectroscopio casero](/home/recursos/recursos-espectro/espectroscopio-casero) 

## Fuentes de iluminación para espectro 
Se habla a veces en general de lámparas, que hay de varios tipos, pueden usar lámparas de sodio de baja presión (LPS/SOX), lámparas de mercurio ... lámparas de descarga, con gases nobles  
 [Gas-discharge lamp - Wikipedia](https://en.wikipedia.org/wiki/Gas-discharge_lamp#Color)   
  
Información general   
 [08. Lamparas.pdf](http://www.ehu.eus/alfredomartinezargote/tema_4_archivos/luminotecnia/08.%20Lamparas.pdf)   
Ahí se cita  
"**Descarga eléctrica a alta tensión entre electrodos fríos (tubos de gases nobles)**  
El llenado del tubo de descarga se hace con gases nobles como son el **neón**, que emite una luz intensa de color rojo-anaranjado, o el helio que emite una luz de color rosa-claro, y también con vapores metálicos, especialmente el vapor de mercurio que emite una luz blanco-azulado y, mezclado con el gas neón, una luz de color azul intenso.  
Las tensiones de arranque y de funcionamiento son elevadas, necesitándose de **600 a 1.000 voltios por metro de longitud.** El consumo de potencia media, también por metro de longitud, es de unos 33 W, con un rendimiento luminoso de 2 ́5 a 5 lm/W"  
  
"**Descarga eléctrica a baja tensión entre electrodos calientes (lámparas de vapor metálico)**  
Si se introduce en un tubo de vidrio previamente evacuado una cierta cantidad de **sodio** sólido o mercurio líquido y un gas noble para lograr transformar el metal en vapor al producirse la descarga eléctrica, se obtiene una descarga de vapor metálico en el seno de un gas, que puede incluso provocarse** a una tensión baja normal (220 V)**, con electrodos precalentados o calentados (cátodos calientes)"  
  
Necesitan una fuente de alta tensión, ejemplo visto en vídeo de FisQuiWeb  [index2.htm](http://web.educastur.princast.es/proyectos/fisquiweb/Videos/Espectroscopia/index2.htm)  IRWIN EHT Power Supply  
 [products.html](http://www.irwinpowersupplies.co.uk/products.html)   
  
Fuente para espectroscopía  
[v08-2detalle.asp](http://www.ventusciencia.com/cgi-bin/v08-2detalle.asp?cl=485&i=si&p=1&cch=140424)   
Equipo completo de espectroscopía  
[v08-2detalle.asp](http://www.ventusciencia.com/cgi-bin/v08-2detalle.asp?cl=484&i=si&p=1&cch=140424)   
  
[A5.pdf](http://www.ptolomeo.unam.mx:8080/xmlui/bitstream/handle/132.248.52.100/689/A5.pdf?sequence=5)   
Información sobre cómo construir fuentes de alimentación  

## Recurso de animaciones / simulaciones
Spectrum Tuner (italiano)  
 [http://ww2.unime.it/weblab/ita/st/](http://ww2.unime.it/weblab/ita/st/)   
Universitá degli Studi Messina  
Original applet © by Guy McArthur  
Adapted applet © 2002 by Carlo Sansotta for IFMSA WebLab  
  
The Applet Collection  
Spectrum  [s.htm](http://lectureonline.cl.msu.edu/~mmp/applist/Spectrum/s.htm)   
  
¿A qué se debe la formación de los espectros atómicos de absorción y de emisión?  
En la animación puedes seleccionar entre el espectro de emisión o de absorción. Puedes utilizar los botones con la flecha para avanzar o retroceder en los elementos o escribir el número atómico y pulsar intro.  
 [spespectro.htm](http://www.gobiernodecanarias.org/educacion/3/usrn/lentiscal/1-cdquimica-tic/FlashQ/Estructura%20A/espectrostotal/spespectro.htm)   

### Espectros atómicos
 [Espectros de emisión y absroción, simulación Educaplus](http://www.educaplus.org/luz/espectros.html) 
 
 [Understanding Atomic Spectra - chem.libretext.org](https://chem.libretexts.org/Courses/Furman_University/CHM101:_Chemistry_and_Global_Awareness_(Gordon)/04:_Valence_Electrons_and_Bonding/4.02:_Understanding_Atomic_Spectra)  

### Fourier
Fourier: Fabricacion de Ondas  
[fourier](http://phet.colorado.edu/es/simulation/fourier)   
Simulación Universidad de Colorado. Algunos derechos reservados  [licensing](http://phet.colorado.edu/es/about/licensing)   
  
But what is the Fourier Transform? A visual introduction  
 [![But what is the Fourier Transform?  A visual introduction. - YouTube](https://img.youtube.com/vi/spUNpyF58BY/0.jpg)](https://www.youtube.com/watch?v=spUNpyF58BY)   
Vídeo 20 min  
  
 [the-fourier-transform-explained-in-one-sentence.html](http://blog.revolutionanalytics.com/2014/01/the-fourier-transform-explained-in-one-sentence.html)   
 [![](http://revolution-computing.typepad.com/.a/6a010534b1db25970b019b0172129c970c-800wi "") ](http://revolution-computing.typepad.com/.a/6a010534b1db25970b019b0172129c970c-800wi)   
 [twitter OperadorNuclear/status/1005298701424910336](https://twitter.com/OperadorNuclear/status/1005298701424910336)   

## Apps para móvil
Asociado al espectroscopio casero se puede usar el móvil, y luego hay análisis de espectro   
Analizador de espectro, MazuApps Ltd  [details](https://play.google.com/store/apps/details?id=com.mazu.audio.spectrumanalyzer&hl=es)   
Publicidad algo molestaSpectrum Analyzer, Raspberrywood  [Spectrum Analyzer - Aplicaciones en Google Play](https://play.google.com/store/apps/details?id=com.raspw.SpectrumAnalyze&hl=es)   
SpecScope Spectrum Analyzer, NFX Development  [details](https://play.google.com/store/apps/details?id=com.nfx.specscope&hl=es)   
SmartScope Oscilloscope LabNation  [SmartScope Oscilloscope - Apps en Google Play](https://play.google.com/store/apps/details?id=com.lab_nation.smartscope&hl=es_419)   

[Light Analyzer (need Diffraction Grating) Open Source Physics Singapore](https://play.google.com/store/apps/details?id=com.ionicframework.lightanalyzer)  
![](https://play-lh.googleusercontent.com/bXPLuaf34wuyaxjyH0DrCrnhzVmNu3Ys0Syn-rdmzY4P1XGtxRDThd08lTkvwE3BBRs=w526-h296)  

[Spectroid. Carl Reinke](https://play.google.com/store/apps/details?id=org.intoorbit.spectrum&hl=es)  
![](https://play-lh.googleusercontent.com/hIHAfW-YhJfQnBl2aO7jpaJ6xVaLI1diITAJ54J9xPMXZYLBFz0and_M3YJHSXLpiA=w526-h296)  

### Ejemplos de análisis de espectros de manera experimental 
 [El Tercer Precog: La física 2.0 del crujir de nudillos](http://eltercerprecog.blogspot.com/2015/07/la-fisica-20-del-crujir-de-nudillos.html)  A los chavales de Secundaria seguro que les encantará comprobar que sus smartphones sirven para algo más que "guasapear" y hacerse "selfies"...Fuente original:Andreas Müller, Patrik Vogt, Jochen Kuhn, and Marcus Müller, Cracking knuckles - A smartphone inquiry on bioacoustics The Physics Teacher, Vol. 53, 307 (2015)  
También se puede ver (se cita en recursos movimiento oscilatorio asociado a resonancia)  
[Cómo obtener las frecuencias fundamentales de una estructura con tu Smartphone](http://estructurando.net/2014/06/23/como-obtener-las-frecuencias-fundamentales-de-una-estructura-con-tu-smartphone/) 

## Ejemplos   

 [Atomic Spectra - 'fingerprints' for elements](http://www.cyberphysics.co.uk/topics/atomic/spectra.htm)   
 [El espectro electromagn&eacute;tico](http://www.sc.ehu.es/sbweb/fisica_//cuantica/experiencias/espectro/espectro.html)   

### Espectros sonoros
Con muchos reproductores de sonido se puede ver el espectro del sonido (en el rango de frecuencias audibles). Por ejemplo VLC permite en menú Audio > visualizaciones > espectro.  
Es útil visualizar el espectro para sonidos de una única frecuencia, comprobando como aparece una única raya en el espectro y al variar la frecuencia se mueve en el espectro: sería bueno mostrar eso con sonido, que considero que es didáctico, y luego extrapolarlo a ondas electromagnéticas.  
Sería algo del estilo de las imágenes que se muestran en este programa (no es libre y no funciona en ubuntu con wine no va bien http://www.64bitprogramlar.com/spectrum-analyzer-pro-live-2011-22679.html). Un ejemplo libre es  [Sonic Visualiser](http://www.sonicvisualiser.org/) , que es bastante más complejo pero tiene opción de espectro. Se puede ver la señal (sinusoidal, donde se aprecia periodo y frecuencia) y el espectro al mismo tiempo.  

Si no se tiene permisos para instalar en windows, no hay versiones portables, pero se puede usar una versión antigua  
[https://code.soundsoftware.ac.uk/projects/sonic-visualiser/files](https://code.soundsoftware.ac.uk/projects/sonic-visualiser/files) , por ejemplo versión 2.2. está como zip y una vez descomprimido es ejecutable.  

[A collection of sound files for audio test purposes - dantimax.dk](http://www.dogstar.dantimax.dk/testwavs/index.htm)
Un ejemplo útil con frecuencia creciente en  [3stepoct.zip](http://www.dogstar.dantimax.dk/testwavs/3stepoct.zip)  (Compliled May-June 2002 by Fred Nachbaur)  
> It has 2 s samples of fixed tones from 20 Hz to 20 480 Hz, spaced logarithmically in 1/3 octave increments, for a total of 31 samples.   

Otro fichero con barrido de frecuencias
[Sweep Tone 20 20000 Hz - wikipedia](https://es.wikipedia.org/wiki/Archivo:SweepTone_20-20000hz.ogg)  

[twitter engineers_feed/status/1737511906695598225](https://twitter.com/engineers_feed/status/1737511906695598225)  
See how many Hz max you can hear  
[twitter LeoMozoloa/status/1737550564303516027](https://twitter.com/LeoMozoloa/status/1737550564303516027)  
Again, flawed test, the video file doesn't go past 15.5-ish KHz when it comes to the sine, due to pretty heavy audio compression, frequency above and bellow are artefacts of said compression and disappear with the sine at 15,5k anyway  
![](https://pbs.twimg.com/media/GB0EPiuW0AA_59b?format=png)  

[twitter RadiactivoMan/status/1273583631676723200](https://twitter.com/RadiactivoMan/status/1273583631676723200)  
La frecuencia máxima que podemos oir depende de la edad. De niños podemos percibir sonidos hasta de 20000 Hz pero este valor va bajando.  
Lo hago en una práctica, usando el osciloscopio. Mis alumnos dejan de escuchar entre 14000 y 16000 Hz y yo a los 13000 Hz.   
¿Y vosotros?:  

También hay vídeos en internet, y se puede hacer con algunas aplicaciones que generan sonidos de frecuencias concretas  
20 - 20,000 Hz Audio Sweep | Range of Human Hearing. Sonic Electronix  
[![](https://img.youtube.com/vi/PAsMlDptjx8/0.jpg)](https://www.youtube.com/watch?v=PAsMlDptjx8 "20 - 20,000 Hz Audio Sweep | Range of Human Hearing. Sonic Electronix")  

También hay tests online, algunos asociados a empresas de audífonos  
[Online Tone Generator. The 60 second online hearing test. It's free, simple and no sign up required.](https://onlinetonegenerator.com/hearingtest.html)  

[twitter fqsaja1/status/1620663639555596289](https://twitter.com/fqsaja1/status/1620663639555596289)  
Una de las cuestiones peor explicadas en los libros de física es la famosa “voz de Helio” afirmando que la voz se vuelve más aguda. Si eso fuese verdad desafinaríamos al cantar. Os parece que desafina esta alumna? Vamos con  
Incluso se afirma que por ser menos denso el Helio las cuerdas vocales vibran más rápido. Eso es desconocer mucho el mecanismo de producción de la voz humana. Veamos el espectro de frecuencias de la misma chica al emitir la nota Do con aire.  
A diferencia de un instrumento de viento (donde la longitud del tubo y la v del sonido en el gas que lo rellena selecciona las notas resonantes) en la voz las notas ya vienen seleccionadas por la cuerdas vocales. Muy parecido a lo que sucede en una guitarra.  
La tensión de las cuerdas y la densidad de estas determinan la frecuencia fundamental. Por eso cuando estamos tensos o chillamos nuestra voz se vuelve más aguda. A partir de ahí los múltiplos de esta frecuencia fundamental (armónicos) también podrán oírse.  
Veamos el espectrograma de la nota Do anterior. Vemos la frecuencia fundamental (261 Hz) y sus armónicos…Más allá de los 532 Hz apenas hay sonido...  
Y qué pasa con el helio?? Repitamos la nota inhalando helio. Como se puede ver la chica afina el Do prácticamente perfecto pero LA CLAVE ESTÁ EN LOS ARMÓNICOS. Los resonadores vocales, laríngeos etc son pequeñas cajas de resonancia que resaltan más o menos ciertas frecuencias…  
Y aquí, sí. Dependiendo del gas encerrado en esas cavidades las frecuencias de resonancia serán unas u otras como en los tubos sonoros(f=v/4L) y a mayor v de propagación se resaltan las frecuencias más agudas. Se incrementa la intensidad de los armónicos agudos, NO su frecuencia  



Relacionado   

El Consejo de Europa pide prohibir los aparatos ultrasónicos que ahuyentan a jóvenes  
 [El Consejo de Europa pide prohibir los aparatos ultrasónicos que ahuyentan a jóvenes | Sociedad | EL PAÍS](https://elpais.com/sociedad/2010/06/25/actualidad/1277416807_850215.html) Bruselas no acepta los aparatos que ahuyentan a los jóvenes mediante el sonido  
 [Bruselas no acepta los aparatos que ahuyentan a los jóvenes mediante el sonido | Tecnología | EL PAÍS](https://elpais.com/tecnologia/2008/05/22/actualidad/1211444884_850215.html) 23 marzo 2011Reply to RecommendationProhibiting the marketing and use of the “Mosquito” youth dispersal device [Xref-DocDetails-EN.asp](http://assembly.coe.int/nw/xml/XRef/Xref-DocDetails-EN.asp?FileID=12727&lang=EN) Relacionado  
[Real Decreto 1367/2007, de 19 de octubre, por el que se desarrolla la Ley 37/2003, de 17 de noviembre, del Ruido, en lo referente a zonificación acústica, objetivos de calidad y emisiones acústicas.](https://www.boe.es/buscar/act.php?id=BOE-A-2007-18397)  
Tabla B. Objetivos de calidad acústica para ruido aplicables al espacio interior habitable de edificaciones destinadas a vivienda, usos residenciales, hospitalarios, educativos o culturales (1) y Tabla B2. Valores límite de ruido transmitido a locales colindantes por actividadestienen valores para "aulas", que se comentan aquí [https://www.noisess.com/legislacion-acustica-en-espana/](https://www.noisess.com/legislacion-acustica-en-espana/) y también [El «mosquito» que sólo escuchan los jóvenes, ¿un arma contra el botellón?](https://www.abc.es/sociedad/abci-mosquito-solo-escuchan-jovenes-arma-contra-botellon-200910060300-103427561754_noticia.html) que cita LRAD [Dispositivo acústico de largo alcance - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Dispositivo_ac%C3%BAstico_de_largo_alcance) 

[twitter ManoloAlonso1/status/1537134220983812097](https://twitter.com/ManoloAlonso1/status/1537134220983812097)  
Hoy vas a poner a prueba tus oídos y vas a ver para qué seres tan inteligentes como los delfines o dispositivos hechos por el hombre aprovechan ondas de frecuencias más allá de nuestro rango auditivo. En el hilo de hoy, hablamos de los ultrasonidos.🧵  
1⃣ Tanto el sonido como el ultrasonido son ondas mecánicas longitudinales. Éstas se propagan por un medio a partir de las vibraciones de las partículas que lo componen. Lo que distingue al sonido del ultrasonido no es más que el valor de la frecuencia de vibración de la onda.  
2⃣ Ésta se mide en hercios (Hz), unidad llamada así por el físico H. R. Hertz. 1 Hz = 1 oscilación por segundo. Los humanos podemos oír ondas sonoras con frecuencias desde unos 20 a 20000 Hz; a partir de ahí entramos en el terreno de los ultrasonidos.  
3⃣ En realidad, cada uno tenemos nuestra frontera particular respecto a lo que, para nosotros, es ultrasonido. Por ejemplo: ¿oyes esto? 🔊 \[¡Ojo! Reproduce estos videos a volumen bajo; oírlo alto o durante mucho tiempo puede dañar tu oído interno.]  
4⃣ La mayoría de vosotros habréis oído un pitido agudo; algún oyente más veterano, nada. El siguiente audio, sin embargo, yo no lo puedo oír mientras que quienes seáis muy jóvenes y hayáis cuidado vuestro oído, sí.  
![](https://img.youtube.com/vi/3tBI-T2SQgQ/0.jpg)](https://www.youtube.com/watch?v=3tBI-T2SQgQ "18000 Hz Test Tone")  
5⃣ Para mí y muchos de vosotros, el sonido de 4., de 18000 Hz, ya no forma parte de la gama de sonidos que podemos escuchar. El sonido de 3., inaudible para los más veteranos, era de 14000 Hz. Con la edad, pero también por otros motivos, perdemos rango auditivo. ¿Por qué?  
6⃣ El motivo parece ser la ubicación de nuestros sensores. En la cóclea, situada en el oído interno, tenemos las células ciliadas, que transforman la energía mecánica de las ondas en señales eléctricas. Aquellas que detectan las frecuencias + altas se encuentran solo en su base.  
![](https://pbs.twimg.com/media/FVUBNEtWYAEmdco?format=png)  
7⃣ Si bien aún hay debate sobre el origen del daño auditivo por el envejecimiento en sí, se ha demostrado el daño a estas células en la base de la cóclea por varios factores. Por supuesto por el sonido alto y prolongado, pero también por algunos fármacos: [ototoxicidad](https://es.wikipedia.org/wiki/Ototoxicidad)  
8⃣ Haciendo una analogía con la luz, la pérdida auditiva que sufrimos con la edad sería como si dejáramos de ver progresivamente los colores más violetas. Visto así da pena, aunque afortunadamente el sonido que nos interesa para el día a día no cae en esas frecuencias tan altas.  
9⃣ Sin embargo, otros animales sí interaccionan con frecuencias en el límite o por encima de nuestro rango auditivo. En lo que queda de hilo vamos a ver su uso de esas frecuencias de decenas de miles de hercios y, brevemente, cómo las empleamos nosotros. (📷: Wikipedia)  
![](https://pbs.twimg.com/media/FVUCZs1X0AAbk9O?format=jpg)  
🔟Muchos animales usan los ultrasonidos como una forma de ecolocalización. El principio es sencillo: emiten una onda acústica, la cual se reflejará si hay otro animal por ahí y volverá en t = 2r/v. Algo análogo a cuando gritas en un lugar con eco y vuelves a oírte poco después.  
1⃣1⃣ Los delfines son un conocido ejemplo de ecolocalización. Igual te preguntas por qué para esto suelen usar ultrasonidos. Cuanto mayor es la frecuencia de una onda, menor es su longitud de onda. Así, sus altas frecuencias les permiten detectar animales más pequeños.  
1⃣2⃣ En cambio, para comunicarse entre ellos, suelen usar frecuencias audibles para nosotros. ¿El motivo? Igual que solo oyes las frecuencias más bajas de la música que ponen los vecinos, estas frecuencias se atenúan menos en el agua, permitiendo la comunicación mayor distancia.  
1⃣3⃣ También los seres humanos usamos las ondas acústicas en la navegación, inspirados en la ecolocalización animal. La técnica se conoce como SONAR, y se usa para detectar distintas cosas en el mar: animales de pesca, submarinos enemigos, tesoros hundidos…  
1⃣4⃣ Permitidme 1 tweet más de delfines🐬. Fijaos en esta noticia. Según investigaciones recientes, en los delfines “nariz de botella” (Tursiops) cada individuo tiene su silbido particular, como un nombre, que emite y ante el que responde entusiasmado:  
[Dolphins 'call each other by name' - bbc](https://www.bbc.com/news/science-environment-23410137)  
1⃣5⃣ Pasemos a otros animales. Había oído que los murciélagos también utilizaban el ultrasonido para encontrar a las presas. Lo que no sabía era que polillas como las “Tigre” han desarrollado la posibilidad de emitir ultrasonidos para advertir al murciélago de que son venenosas.  
1⃣6⃣ Yendo todavía a mayor frecuencia, nos encontramos con los ultrasonidos en imagen médica. Para este hilo, solo deciros que, como los delfines, se tiene en cuenta el balance de usar altas frecuencias para una mayor resolución o bajas frecuencias para llegar a órganos internos.  
1⃣7⃣ Una última curiosidad. Al pegar el transductor a nuestro cuerpo, se vierte un gel en él. ¿Sabes para qué? Para “ajustar la impedancia”, término que conocerá quien trabaje en electrónica o en algunas aplicaciones ópticas. ¿Qué significa en este contexto? (📷: R. M. Guillemi)  
1⃣8⃣ La impedancia acústica es el producto de la densidad del medio y la velocidad de la onda. La densidad del aire es unas 1000 veces menor que la del transductor de US o los componentes de nuestro cuerpo. Sin gel, el aire entre el transductor y el cuerpo reflejaría toda la onda  
  

### Espectros de emisión electromagnéticos  

Para entender los modelos atómicos es necesario ver un espectro: a falta de un tubo de gas y un espectroscopio, se pueden mostrar imágenes a los alumnos.   
En wikipedia se pueden encontrar algunos ejemplos (ver licenciamiento asociado)  
 [Hydrogen spectral series - Wikipedia](http://en.wikipedia.org/wiki/Hydrogen_spectral_series)   
  
En algunos aparece el fondo negro (que sería lo real, son saltos cuánticos y solamente se emiten frecuencias discretas), y en otros aparece un fondo coloreado; suele ser para orientar su posición en el rango espectral, aunque también puede ser para indicar que esas líneas están dentro del espectro "contínuo" emitido por un objeto como el Sol, debido a su temperatura.  
  
Espectro solar (tomado de  [este enlace de la NASA](http://imagine.gsfc.nasa.gov/docs/teachers/lessons/xray_spectra/worksheet-specgraph2-sol.html) )  
![](http://imagine.gsfc.nasa.gov/docs/teachers/lessons/xray_spectra/images/carbon-solution.jpg)  
Emisión Hidrógeno y Helio, se pueden reconocer en el anterior  
![](http://imagine.gsfc.nasa.gov/docs/teachers/lessons/xray_spectra/images/spectra.jpg)  
Emisión de Hidrógeno sobre fondo negro, solamente espectro visible  [![http://upload.wikimedia.org/wikipedia/commons/f/fb/Emission_spectrum-H_labeled.svg](http://upload.wikimedia.org/wikipedia/commons/f/fb/Emission_spectrum-H_labeled.svg "http://upload.wikimedia.org/wikipedia/commons/f/fb/Emission_spectrum-H_labeled.svg") ](http://upload.wikimedia.org/wikipedia/commons/f/fb/Emission_spectrum-H_labeled.svg) Emisión de Hidrógeno sobre fondo negro, remarcando que hay partes fuera del rango visible (tomado de  [http://www.unlu.edu.ar/~qui10017/Quimica COU muestra para IQ10017/cap1.htm](http://www.unlu.edu.ar/%7Equi10017/Quimica%20COU%20muestra%20para%20IQ10017/cap1.htm) , licenciamiento no detallado)  
![](http://www.unlu.edu.ar/~qui10017/Quimica COU muestra para IQ10017/cap1/image013.jpg)  
  
Emisión Nitrógeno  
 [![](http://upload.wikimedia.org/wikipedia/commons/3/38/Nitrogen.Spectrum.Vis.jpg "") ](http://upload.wikimedia.org/wikipedia/commons/3/38/Nitrogen.Spectrum.Vis.jpg)   
Emisión Hierro  
![](http://upload.wikimedia.org/wikipedia/commons/4/43/Emission_spectrum-Fe.png)  
Espectros tomados de  [Write Science: Image](http://writescience.files.wordpress.com/2012/11/atomicspectra.png)  en la página  [In Living Color | Write Science](http://writescience.wordpress.com/2012/11/21/in-living-color/)   
 [![](http://writescience.files.wordpress.com/2012/11/atomicspectra.png "") ](http://writescience.files.wordpress.com/2012/11/atomicspectra.png)  
  
Espectros de varios elementos (tomados de  [Una versión íntima de la materia - unlu.edu.ar (archive.org)](https://web.archive.org/web/20070820155840/http://www.unlu.edu.ar/~qui10017/Quimica%20COU%20muestra%20para%20IQ10017/cap1.htm) , licenciamiento no detallado)  
![](https://web.archive.org/web/20130723035843/http://www.unlu.edu.ar/~qui10017/Quimica%20COU%20muestra%20para%20IQ10017/cap1/image015.jpg)  
Varios espectros tomados de  [espectros.jpg.html](http://s241.beta.photobucket.com/user/quimarchivos/media/Laboratorio/espectros.jpg.html)  (licenciamiento no detallado)  
![](http://i241.photobucket.com/albums/ff256/quimarchivos/Laboratorio/espectros.jpg)  
![](http://i241.photobucket.com/albums/ff256/quimarchivos/Laboratorio/espectros2.jpg)  

[Espectros atómicos (I). Departamento de Física y Química IES Valle del Saja](http://www.fqsaja.com/?portfolio_page=espectros-atomicos-i)  
[Espectros atómicos (II). Departamento de Física y Química IES Valle del Saja](http://www.fqsaja.com/?portfolio_page=espectros-atomicos-ii)  



### Espectros de estrellas
Tomado de wikipedia ( [Stellar classification - Wikipedia](http://en.wikipedia.org/wiki/Spectral_class) ), la fuente es la NASA  
Son espectros de absorción  

[Stellar Spectral Types: OBAFGKM. Credit & Copyright: KPNO 0.9-m Telescope, AURA, NOAO, NSF](https://apod.nasa.gov/apod/ap040418.html)  
![](https://apod.nasa.gov/apod/image/0105/obafgkm_noao_big.jpg)   

Espectro del Sol (de tipo espectral G2), con las líneas de Fraunhofer (tomado de  [Fraunhofer lines - Wikimedia Commons](http://commons.wikimedia.org/wiki/File:Fraunhofer_lines.jpg) )  
![](http://upload.wikimedia.org/wikipedia/commons/3/34/Fraunhofer_lines.jpg)   

## Espectro y astronomía / cosmología 

[¿Con qué ojos observará ALMA? - eso.org](https://www.eso.org/public/spain/products/flyers/flyer_0004/?lang)  
![](https://www.eso.org/public/archives/flyers/screen/flyer_0004.jpg)  

### Tablas periódicas de espectros
[Field Tested Systems. Real time spectroscopy. Periodic table of spectra](https://www.fieldtestedsystems.com/ptable/)  
![](https://cdn.shopify.com/s/files/1/1405/8616/products/Periodic_Poster_092017-small_1024x1024.jpg?v=1545082962)  

[Tabela periódica com espectros de emissão atômica dos elementos - tabelaperiodica.org ](https://www.tabelaperiodica.org/tabela-periodica-com-espectros-de-emissao-atomica-dos-elementos/)   
[Fichero .png alta resolución](https://www.tabelaperiodica.org/wp-content/uploads/2019/05/espectro-final-v2019b-A4.png)  	
  
[tabla priodica espectros emision – triplenlace.com](https://triplenlace.com/tabla-priodica-espectros-emision-triplenlace-com/)   
![](https://triplenlacecom.files.wordpress.com/2014/06/tabla-priodica-espectros-emision-triplenlace-com_.jpg)  

## Espectro y resonancia

[twitter Llorens_MRC/status/1512467349416427523](https://twitter.com/Llorens_MRC/status/1512467349416427523)  
Algunas tardes suelo hacerme un café con leche al que añado más leche aún, y el otro día al remover con la cucharilla me di cuenta que el sonido del chocar con la taza iba cambiando de frecuencia, primero pasó a más grave y luego lentamente volvió a tornarse agudo...  
...es una más de estas cosas habituales a las que nunca dedicas mucha atención ...pero me intrigó... ¿que cosa cambia, cuál es la variable para que la resonancia de la taza cambie de forma tan rápida y evidente?  ...¿sería por la temperatura al calentarse la loza?   
...Y como nunca me ha gustado acudir primero a la imaginación ante las incógnitas, decidí efectuar algunas pruebas, la primera unir un termómetro termopar a la taza vacía y echarle dentro agua caliente, a la vez que, con un micrófono, grabar los "clink, clink" y ver su espectro.  
El programa de audio será el conocido Audacity, que nunca he manejado pero recuerdo que algún compañero me había hablado muy bien de él...  
Así pues he arrancado la grabación, he dado algunos golpecitos de test en la taza y he vertido agua cliente. En pocos segundos la temperatura ha pasado de 20 a 41 Cº, subiendo luego lentamente hasta los 75 Cº... Y mientras tanto yo dándole a la taza golpecitos con la cuchara...  
...Pero el caso es que a oído ya no he conseguido notar el efecto del cambio de timbre que era habitual. Tal vez el software fuera más fino para notarlo, y para ello he pasado a la visión de espectro, que es donde un cambio de timbre tendría que verse...  
Con la ventana de espectro, he comprobado diferencias en el pico de resonancia más visible, y salvo por el hecho que la taza vacía resuena a 2100 Hz, y a cuando está con el líquido baja a 1835 Hz, luego apenas varía con el tiempo, con independencia del cambio térmico...  
Este análisis ya descarta la temperatura del recipiente como causa del cambio de timbre, y si no es el contenedor pienso que podría ser el contenido... Así que lo siguiente fue plantear el experimento en condiciones reales, con máquina nexpreso, café, leche, el azúcar y demás...  


