
# 1º Bachillerato Técnicas experimentales en ciencias

Es una materia LOE que desaparece con LOMCE.  
[Resolución de 7 de julio de 2008, de la Dirección General de Educación Secundaria y Enseñanzas Profesionales, por la que se establecen las materias optativas del Bachillerato en la Comunidad de Madrid ](https://www.bocm.es/boletin/CM_Boletin_BOCM/2008/07/29/17900.pdf)  

De momento reflejo aquí currículo sin separar en otra página

TÉCNICAS EXPERIMENTALES EN CIENCIAS

Materia optativa de 1.º de Bachillerato, vinculada a la modalidad de Ciencias y Tecnología

Introducción

La formación científica debe tener en cuenta diversos aspectos.
En primer lugar, que las ciencias experimentales tienen como objetivo intentar explicar racionalmente la realidad natural y los retos
tecnológicos que la sociedades modernas se plantean. En segundo
lugar, que las ciencias experimentales no son exclusivamente un
conjunto de conocimientos: Conceptos, leyes y teorías (saber ciencia); también incluyen estrategias, técnicas y habilidades de investigación relacionadas con la resolución de problemas científico-
tecnológicos (hacer ciencia). Si quiere formarse científicamente al
alumnado es también necesario que aprenda todo este conjunto de
saber hacer.

La resolución de problemas supone el desarrollo de contenidos de
tipo procedimental, tales como estrategias, diseño y desarrollo experimental y elaboración de informes científicos. Los procedimientos
de investigación incluyen el análisis de problemas, la formulación
de hipótesis de acuerdo a las teorías vigentes, establecimiento de las
variables dependiente, independiente y de control, relaciones entre
variables, diseño y desarrollo experimental (medida, clasificación,
procesamiento, análisis e interpretación de datos). Las conclusiones
obtenidas, generalmente en forma de leyes, se comunican e integran
dentro del cuerpo de conocimiento.

La investigación científica consiste en un proceso de indagación
acerca de algún aspecto de la realidad. Ante su complejidad, los problemas deben ser identificados, planteados y analizados adecuada-
mente, para poder decidir cuáles son las variables relevantes, y
formular hipótesis que puedan ser contrastadas. Por tanto, los alumnos tienen que aprender a diseñar, planificar y realizar, pequeñas ac-
tividades de investigación. Evaluar los resultados de la experimentación y deducir de ellos las conclusiones adecuadas, modificando el
camino seguido si es necesario, son cuestiones que formar parte del
aprendizaje científico.

La realización de experiencias de laboratorio implica, entre otros
aspectos, la observación, la medida y la clasificación. Adquirir la capacidad de observar de forma esmerada, honesta y rigurosa es im-
portante para reconocer semejanzas y diferencias, para comprender
lo que es significativo y para la obtención de datos experimentales
fiables. Para ello es necesario el aprendizaje de técnicas y el uso del
instrumental científico, todo ello bajo las normas básicas de buenas
prácticas de laboratorio.

Por otro lado, los científicos utilizan un lenguaje específico en su
tarea para elaborar cuadernos de trabajo, informes, artículos, comunicaciones, etcétera. Aprender a trabajar como un científico supone
conocer este lenguaje, que es el vehículo de comunicación entre
ellos y con el resto de la sociedad, para exponer y debatir las ideas
científicas y los avances tecnológicos. Por tanto, el conocimiento y la
comprensión de este lenguaje también forma parte de la enseñanza/aprendizaje de esta materia. Asimismo, no hay que olvidar que un
aspecto esencial del trabajo científico supone el análisis de material
escrito o audiovisual, la utilización de diversas fuentes de información (utilizando las Tecnologías de la Información y de la Comunicación), y la elaboración de informes y de proyectos.

Esta materia deber tener un enfoque procedimental, sin olvidarnos que no se puede enseñar y aprender procedimientos sin conceptos e, incluso, actitudes. En esta materia se introducirá al estudiante
en la experimentación básica de un laboratorio y se reforzarán, mediante la misma, los conceptos básicos estudiados en las distintas
materias científicas.
La relación de contenidos incluidos en esta materia debe adecuarse a las características de cada Centro y de cada grupo de alumnos.
Siempre atendiendo al fundamento experimental de la Ciencia, sin
que debamos confundirla con un elemental adiestramiento en el manejo de instrumentos, y aplicación de técnicas y herramientas.
La realización de pequeñas investigaciones de laboratorio pondrá
al alumno frente al desarrollo de la metodología científica, le ayudará a enfrentarse con la problemática del quehacer científico, sirviendo de soporte para entender y analizar los retos tecnológicos,
energéticos, medioambientales y de la salud de la Sociedad actual.

Objetivos

La enseñanza de la materia optativa Técnicas experimentales en
ciencias en el Bachillerato tendrá como finalidad el desarrollo de las
siguientes capacidades:

1. Realizar medidas con diferentes aparatos e instrumentos, tanto analógicos como digitales, controlando los errores, interpretando los datos mediante representaciones gráficas, cálculos numéricos o tratamiento informático y comparándolas con los resultados teóricos.  
2. Expresar con claridad las ideas de las ciencias experimentales, oralmente y por escrito, utilizando, cuando sea necesario, gráficos, diagramas, símbolos y ecuaciones.  
3. Utilizar, analizar e interpretar, textos científicos y divulgativos, así como información presentada en forma de datos numéricos, esquemas, dibujos, o representaciones gráficas.  
4. Describir y nombrar el material y los montajes básicos utilizados en los laboratorios de ciencias experimentales.  
5. Plantear problemas, formular hipótesis, analizar variables, diseñar y realizar experimentos y montajes, recoger adecuadamente los datos, interpretarlos, elaborar conclusiones y comunicar resultados de los trabajos prácticos, de las investigaciones y de los proyectos.  
6. Comprobar experimentalmente diferentes leyes de las ciencias experimentales y sus aplicaciones tecnológicas. Saber realizar un trabajo práctico, haciendo los ensayos de los diferentes componentes y dispositivos, siguiendo un guión con instrucciones con diferentes grados de complejidad.  
7. Comprender los conceptos, los principios, las teorías y los modelos de las ciencias experimentales en los que se basan las aplicaciones prácticas que se estudian; así como relacionar las aplicaciones tecnológicas con la ciencia y la sociedad.  
8. Utilizar las tecnologías de la información y de la comunicación como herramienta necesaria para la investigación, para visualizar simulaciones o para hacer tratamiento de datos, aprovechándolas también para la realización de esquemas, planos e informes.  
9. Analizar, interpretar y evaluar los factores que relacionan las ciencias experimentales con la industria, el medio ambiente, la sociedad y la calidad de vida.  
10. Manipular aparatos, instrumentos y productos de laboratorio de manera responsable y realizar las operaciones del laboratorio
con precisión, siguiendo las normas de seguridad y utilizando los reglamentos y normativas pertinentes.  
11. Respetar las normas de uso de los talleres, los laboratorios y las instalaciones, y mantener el puesto de trabajo en las condiciones de limpieza y orden que permita hacer la tarea en condiciones.   
12. Tratar los residuos producidos en el taller y en los laboratorios de manera adecuada y respetuosa con el medio ambiente.

Contenidos

1. Las ciencias experimentales y la tecnología
— La metodología hipotética-deductiva en la ciencia y la tecnología.  
— Identificación, caracterización y planteamiento de necesidades y problemas científicos y tecnológicos.  
— Hipótesis científicas. Formulación de hipótesis. Contrastación de hipótesis: la observación y la experimentación.  
— Diseño experimental. Evaluación de los factores que intervienen y que pueden modificarse en el diseño. Materiales e instrumentos básicos en un laboratorio.  
— La seguridad en el laboratorio. Normas de trabajo. Equipo de protección. Manipulación del vidrio. Manipulación y transporte de reactivos. Eliminación de residuos. Normas de actuación en caso de accidente.  
— Medida de volúmenes, masas, densidades y temperaturas. Tratamiento de datos. Cifras significativas. Notación científica. Unidades. Errores. Precisión. Exactitud. Intervalo de confianza. Representaciones gráficas. Regresión lineal.  
— La comunicación científica. Elaboración de informes. Ética, valores y fraudes en la investigación científica.  
2. Disoluciones. Propiedades de las disoluciones  
— Sustancias puras y disoluciones. Utilización de técnicas de laboratorio para preparar disoluciones de distinta concentración (de solutos sólidos y líquidos).  
— Propiedades características. Temperatura de fusión, tempera tura de ebullición y solubilidad. Determinación experimental de las propiedades características.  
— Propiedades coligativas de una disolución. Crioscopia y ebulloscopia. Determinación de la masa molecular de una sustancia a partir de la variación del punto de congelación de la disolución.  
— Análisis de los diferentes sistemas de desalinización del agua salobre.  
3. Reacciones químicas: Introducción al análisis químico  
— Material específico de un laboratorio de química. Condiciones de uso. Trabajo con vidrio.  
— Operaciones y procesos básicos. Transferencia de sólidos y líquidos. Filtración. Cristalización. Destilación. Centrifugación. Extracción.
— Clasificación de las reacciones químicas. Realización de diferentes tipos de reacciones químicas identificando los productos de la reacción.  
— Reacciones de transferencia de protones. Concepto de pH. Indicadores  
— Reacciones de precipitación. Solubilidad. Redisolución de precipitados. Formación de complejos.  
— Reacciones de transferencia de electrones. Concepto de oxidación-reducción.  
— Métodos de análisis utilizados en un laboratorio de química. Identificación de aniones y cationes de una disolución. Volumetría y gravimetría.  
— Contaminación del agua. Los parámetros de calidad del agua y su estudio experimental. Dureza del agua. Técnicas de valoración por volumetría Diseño y realización de volumetrías para determinar el contenido de Cl−, Ca2+, Mg2+ en el agua.  
4. La energía y las reacciones químicas. Combustibles  
— Calor absorbido o desprendido por un sistema. Entalpía.  
— Calor de disolución y calor de reacción. Determinación experimental de calores de disolución y reacción.  
— Combustibles. Determinación del poder calorífico de un combustible.  
— Petróleo. Tecnología para el aprovechamiento del petróleo. Productos de destilación fraccionada.  
— Gasolinas: “Reformado”, “isomerización” e índice de octano.  
— Biocombustibles (metanol, etanol, hidrógeno).  
5. Química orgánica  
— Propiedades de las sustancias orgánicas. Uso de estrategias adecuadas para el manejo de sustancias orgánicas.  
— Técnicas de separación y purificación. Extracción. Destilación por arrastre de vapor. Cromatografía de capa fina y en columna. Separación de compuestos orgánicos en base a sus características ácido-base. Extracción de cafeína. Fabricación de perfumes.  
— Reacciones orgánicas. Clasificación. Síntesis de compuestos orgánicos sencillos. Estudio experimental de la esterificación. La transesterificación de aceite para la obtención de biodiésel. Fabricación del jabón.  
— Polímeros y plásticos. Principales plásticos utilizados en la vida cotidiana. Reacciones de polimerización.  
6. Determinación de calores específicos  
— Calor y temperatura. Uso del termómetro y del calorímetro. Cambios de temperatura y la energía térmica.  
— Formas de transmisión del calor. Montajes experimentales basados en transferencias de calor. Calor específico.  
— Determinación del equivalente calorífico del calorímetro. Determinación del calor específico de una sustancia.  
7. Cinemática. Estudio de movimientos periódicos  
— Cinemática. Determinación experimental de la velocidad en un movimiento rectilíneo uniforme. Determinación experimental de la aceleración en un movimiento rectilíneo uniformemente acelerado.  
— Movimientos periódicos. Análisis y control de las variables implicadas: período, longitud, masa, constante elástica y amplitud.  
— Comprobación experimental de la relación entre período y otras variables relevantes.  
— Determinación experimental de la intensidad del campo gravitatorio g a partir del período de oscilación de un péndulo.  
— Ley de Hooke y determinación experimental de la constante elástica.  
8. Electrodinámica. Circuitos eléctricos  
— Circuitos eléctricos y la Ley de Ohm. Comprobación experimental de la Ley de Ohm y las relaciones entre las variables implicadas. Uso del polímetro.  
— Determinación experimental de la Resistencia. Resistividad. Identificación de materiales por su resistividad.  
— Resistencia de materiales no lineales. Componentes activos: Diodos.  
— Capacidad y condensadores. Carga y descarga de condensadores. Medición de la constante de tiempo en un circuito RC.  
— Interacción entre corrientes eléctricas e imanes. Experiencia de Öersted. Inducción magnética, solenoides. Motor eléctrico de corriente continua.  
9. Resistencia de materiales  
Equilibrio de fuerzas. Resistencia de materiales. Análisis y control de variables: resistencia, fuerza, anchura, e identificación y detección de otras variables que puedan considerarse relevantes, como por ejemplo la dirección. Diseño de estrategias de investigación para
comparar resistencias.  
10. Índices de refracción y composición de la luz blanca  
Ondas electromagnéticas. Refracción y reflexión. Uso de un banco óptico para la experimentación. Determinación de los ángulos de
incidencia y de refracción y del ángulo límite. Comprobación de la Ley de Snell. Influencia de los materiales en relación a la refracción.
Dispersión de la luz. Espectros.

Criterios de evaluación  

1. Analizar textos científicos e identificar el problema que se intenta investigar, las hipótesis que se formulan y su contrastación, el
análisis de resultados y las conclusiones.  
2. Elaborar un esquema de investigación de un problema científico en el que se incluyan las acciones que tienen que seguirse. Diseñar estrategias de investigación originales.  
3. Experimentar y describir los siguientes tipos de reacciones químicas: Ácido-base, redox, de precipitación y de formación de complejos. Conocer y aplicar reacciones químicas que permitan identificar algunos cationes y aniones en disolución acuosa.  
4. Manipular correctamente el material de laboratorio para realizar una volumetría y una gravimetría y hacer los cálculos adecuados. Nombrar e identificar correctamente el material de laboratorio y los productos químicos y manipularlos siguiendo las normas de se
guridad.  
5. Explicar la utilidad de un proceso de destilación fraccionada en una refinería. Aplicar las propiedades deseables de un buen combustible para escoger el mejor de entre ellos. Explicar cuál es la composición de las gasolinas. Cómo se obtienen y como se mejoran. Ser consciente del papel de los químicos en la obtención y mejora de los combustibles y materiales.  
6. Determinar teórica y experimentalmente el valor de la entalpía de combustión de diferentes sustancias. Determinar experimentalmente el calor de disolución de diferentes solutos y de reacción de diferentes reactivos químicos.  
7. Elaborar gráficos. Interpretar las tablas y gráficos de datos experimentales.  
8. Determinar experimentalmente la masa molecular de una sustancia. Determinar experimentalmente las variaciones de la temperatura de fusión, de la temperatura de ebullición y de la solubilidad de una disolución cuando se modifica el soluto, la concentración o la temperatura.  
9. Explicar los fundamentos de los diferentes métodos de desalinización del agua salobre. Escribir e igualar las reacciones químicas implicadas en el análisis del agua. Buscar información de los parámetros de calidad del agua. Tomar muestras para análisis que sean representativas. Diseñar y realizar volumetrías. Caracterizar una muestra de agua en función de los parámetros de calidad.  
10. Conocer como se sintetizan varias sustancias que se utilizan en el entorno cotidiano. Experimentar con las sustancias orgánicas
en el laboratorio. Realizar la extracción de alguna sustancia como la cafeína. Realizar la síntesis de alguna sustancia orgánica, como un
éster o un polímero. Fabricar jabón a partir de grasas animales o vegetales. Comparar el efecto tensioactivo de diferentes jabones y detergentes. Valorar la importancia que tienen todas las sustancias sintéticas a nivel económico y de recursos.  
11. Diferenciar experimentalmente las distintas formas de transmisión de la energía térmica. Relacionar la cantidad de calor absorbido o perdido por un sistema con el cambio de temperatura. Determinar el equivalente calorífico del calorímetro. Determinar el calor específico de diferentes materiales. Confeccionar curvas de enfriamiento o calentamiento.  
12. Experimentar con fenómenos del ámbito de la cinemática y dinámica. Distinguir variables relevantes. Analizar y controlar diferentes tipos de variables: independientes, dependientes, fijadas.  
13. Experimentar con fenómenos del ámbito de la corriente eléctrica. Descubrir que hay conductores de resistencia variable. Investigar dependencias funcionales indirectas. Caracterizar materiales a partir de una propiedad que se mide indirectamente e identificarlos a partir de informaciones tabuladas. Utilizar correctamente el polímetro.  
14. Definir una nueva magnitud física a partir de un problema. Descubrir relaciones con variables. Descubrir una ley física y definir simultáneamente una constante de proporcionalidad característica de un material. Redefinir una característica a partir de nuevas informaciones.  
15. Experimentar con fenómenos del ámbito de la óptica e interpretar fenómenos ópticos de la vida cotidiana.  
16. Usar una hoja de cálculo para hacer patente una relación entre variables, para ajustar a una función los valores experimentales,
para hacer el cálculo de errores, para representar gráficos, y para facilitar en general el tratamiento de datos experimentales.  


