# Recursos para profesores

Los recursos para profesores se centran en programaciones, unidades didácticas, propuestas / ideas / guiones prácticas laboratorio ...  
También hay recursos asociados a formación y actividades fuera del centro, pero distintas a las pensadas para alumnos que [ tienen página aparte](/home/recursos/recursos-de-actividades-fuera-del-centro). 

También enlaza con [docencia Física y Química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel)   

[Generador de horarios personalizados - schedulebuilder](https://schedulebuilder.org/es/)  

Materiales para profesores
   * Página editorial SM para profesores de Física y Química  
   [http://www.fq.profes.net/](http://www.fq.profes.net/)  
   Incluye un banco de recursos con distintos tipos: Propuestas didácticas, Programaciones, Selectividad, Olimpiadas de Física, Explora la ciencia
   * Portales dedicados a aportar recursos e información a los profesionales de la docencia  
   [http://www.consumer.es/web/es/educacion/extraescolar/2007/09/08/166704.php?page=3](http://www.consumer.es/web/es/educacion/extraescolar/2007/09/08/166704.php?page=3) 

##  Información sobre licenciamiento para profesores
Es algo que los profesores deben conocer, asociable a  [REA](/home/recursos/recursos-educativos-abiertos)  
[http://www.xarxatic.com/infografia-sobre-las-licencias-creative-commons/](http://www.xarxatic.com/infografia-sobre-las-licencias-creative-commons/)  
  
[Why Open Education Matters Video Competition Winners 2012](http://www.ed.gov/blog/2012/07/why-open-education-matters-video-competition-winners-announced/)  
  

[Ponencia Derechos de autor y propiedad intelectual en internet en el ámbito educativo. Licencias para compartir materiales educativos. Raúl Luna Lombardi. cc-by-nc-nd. Mediateca EducaMadrid](http://mediateca.educa.madrid.org/reproducir.php?id_video=29p879tmzltuayua)  

Crear, compartir y distribuir en tiempos revueltos  
[http://issuu.com/educacion_ite/docs/crearentiemposrevueltos?e=2861751/3100761](http://issuu.com/educacion_ite/docs/crearentiemposrevueltos?e=2861751/3100761)  

Curso "Creación, reutilización y difusión de contenidos (Edición autoformación)" cc-by-nc-sa  
[http://www.riate.org/apls/moodle/web/course/view.php?id=28](http://www.riate.org/apls/moodle/web/course/view.php?id=28) 

##  Formación para profesores

   * Programa de formación en el CERN (Comunidad de Madrid)
   Se imparten en español, duran unos 5 días y son en el propio CERN. En el año 2013 se celebra la quinta edición.  
   [http://gestiondgmejora.educa.madrid.org/cern/index.php](http://gestiondgmejora.educa.madrid.org/cern/index.php)  
   Hay información disponible en el CERN como "Spanish Teacher Programme". En el año 2012 no actualiza datos desde 2011, pero incluye el material de ediciones anteriores e indica  [http://education.web.cern.ch/education/Chapter1/Page3_SP.html](http://education.web.cern.ch/education/Chapter1/Page3_SP.html)  
   Programa de la edición de junio de 2013 [http://indico.cern.ch/event/256906/](http://indico.cern.ch/event/256906/) 

   * Foro Nuclear. Recorrido por el ciclo del uranio.En 2013 5 días en distintos puntos de España: Los gastos de matrícula, transporte y alojamiento, corren a cargo del Foro Nuclear.Para profesores de Física y Química, Biología y Geología, Ciencias de la Naturaleza y Tecnología de Enseñanza Secundaria [http://gestiondgmejora.educa.madrid.org/secundaria/index.php/inicio/info](http://gestiondgmejora.educa.madrid.org/secundaria/index.php/inicio/info) 

   * Servicio de Formación en Red de Ministerio de Educación.Al menos desde 2008 a 2013 hay convocatorias cursos online dos veces al año. Los cursos no tienen ninguna sesión presencial. [http://formacionprofesorado.educacion.es/](http://formacionprofesorado.educacion.es/) 

   * Formación en línea Comunidad de Madrid.Al menos desde 2008 a 2013 hay convocatorias cursos online dos veces al año. Los cursos de Madrid tienen requisitos específicos y tienen obligatoriamente una sesión presencial, que suele ser en el CRIF de Las Acacias. [http://formacion.enlinea.educa.madrid.org/](http://formacion.enlinea.educa.madrid.org/) 
CTIF (Centro Territorial de Innovación y Formación) Comunidad de Madrid. Cursos presenciales, convocatorias y lugar variable. Hay uno por DAT  
[CTIF Madrid Capital](http://ctif.madridcapital.educa.madrid.org/)  
[CTIF Madrid Sur](http://ctif.madridsur.educa.madrid.org/)  
[CTIF Madrid Este](http://ctif.madrideste.educa.madrid.org/)  
[CTIF Madrid Norte](http://ctif.madridnorte.educa.madrid.org/)  
[CTIF Madrid Oeste](http://ctif.madridoeste.educa.madrid.org/) 
CRIF (Centro Regional de Innovación y Formación) Comunidad de Madrid. 
[CRIF Acacias](http://crif.acacias.educa.madrid.org/)  
