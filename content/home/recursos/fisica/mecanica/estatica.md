
# Recursos estática

Enlaza con [sólido rígido](/home/recursos/fisica/mecanica/solido-rigido): en el modelo puntual no tiene sentido la rotación.  
Enlaza con momento de fuerzas y momento angular.  

Física-I, 1 Ingeniería Química  
 [12.Estatica. Equilibrio estático. Javier Junquera - unican.es](http://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/12.Estatica.ppt)   
 [Ejercicios estática. Javier Junquera - unican.es](http://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/estatica.pdf)  
[Movimiento de rotación. Javier Junquera - unican.es](https://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/10.movimiento-de-rotacion.pdf)  

[Libro (física sin límites) 8: Equilibrio estático, elasticidad y par - libretexts](https://espanol.libretexts.org/Fisica/Libro%3A_F%C3%ADsica_(sin_l%C3%ADmites)/8%3A_Equilibrio_est%C3%A1tico%2C_elasticidad_y_par)  


[Mecánica clásica. Jorque carrascosa - profesorjrc.es](https://profesorjrc.es/apuntes/2%20bachillerato/fisica/transparencias/mecanica%20clasica.pdf)  
Jorge Carrascosa, apartado dinámica de rotación que cita momento de inercia y momento angular.  

[Estática de la partícula - laplace.us.es](http://laplace.us.es/wiki/index.php/Est%C3%A1tica_de_la_part%C3%ADcula_(GIE))  

[Estática del sólido rígido - laplace.us.es](http://laplace.us.es/wiki/index.php/Est%C3%A1tica_del_s%C3%B3lido_r%C3%ADgido)  

[Problemas de Estática del sólido rígido - laplace.us.es](http://laplace.us.es/wiki/index.php?title=Problemas_de_Est%C3%A1tica_del_s%C3%B3lido_r%C3%ADgido_(G.I.C.))  

[Estática. Jacson Chipana Castro - slideshare](https://es.slideshare.net/jacsonchipanacastro/esttica-01-2014)  

[Sólido rígido - ehu.es](http://www.sc.ehu.es/sbweb/fisica3/solido/portada.html)  
Curso Interactivo de Física en Internet, Ángel Franco  

[Monkey Statics - afreeparticle](https://afreeparticle.com/monkey.html)  

[Hanging rod. Penguin statics - afreeparticle](https://afreeparticle.com/hangingrod.html)  

[![](https://img.youtube.com/vi/2VhX15yondg/0.jpg)](https://www.youtube.com/watch?v=2VhX15yondg "Los tenedores equilibristas (Experimentos Caseros)")  

[Magia con Newton, estática botella, 3 palillos y cuerda - 3m.com.es](https://www.3m.com.es/3M/es_ES/fundacion3m/3m-divulgacion-cientifica/ciencia-en-casa/magia-con-newton/)  

[Práctica Estática (Fis 151/Fis 1513) - fis.puc.cl](http://srv2.fis.puc.cl/mediawiki/index.php/Est%C3%A1tica_%28Fis_151/Fis_1513%29)  


## Ejercicios

[Problemas resueltos estática - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica3/problemas/solido/estatica/estatica.html)  

[NO ME SALEN (EJERCICIOS RESUELTOS Y APUNTES TEÓRICOS DE FÍSICA) Estática ](https://ricuti.com.ar/no_me_salen/estatica/index_est.html)  

[J. Martín Problemas Resueltos de Estática](http://materiales.untrefvirtual.edu.ar/documentos_extras/20452_FISICA_1/Problemas_Resueltos_Estatica.pdf)  

[E.T.S. de Ingenieros de Caminos, Canales y Puertos Barcelona. Problemas de Resistencia de Materiales y Estructuras. Juan Miquel Canet](https://portal.camins.upc.edu/materials_guia/250120/2013/problemas%20resueltos.pdf)  

## Tensegridad

[tensegridad RAE](https://dle.rae.es/tensegridad)  

Tensegrity table explained - WorkingsgotAnimated  
[![](https://img.youtube.com/vi/1BbGW5mL7QQ/0.jpg)](https://www.youtube.com/watch?v=1BbGW5mL7QQ "Tensegrity table explained - WorkingsgotAnimated.")  
