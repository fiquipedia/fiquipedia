
# Recursos Física

Desde la página [recursos](/home/recursos) se enlaza aquí para los asociados a Física  

Aparte de enlaces aparte para  [apuntes](/home/recursos/apuntes) ,  [simulaciones](/home/recursos/simulaciones) ,  [laboratorio/prácticas](/home/recursos/practicas-experimentos-laboratorio)  y por otros temas, se ponen aquí páginas generales sobre física que agrupan varios tipos de recursos  
En esta página irán recursos genéricos sobre física y enlaces a recursos específicos sobre física, como puede ser enlazar a  [recursos física de partículas](/home/recursos/fisica/fisica-de-particulas)  

[Toda la física conocida de un vistazo - molasaber.org](https://molasaber.org/2016/12/02/toda-la-fisica-conocida-de-un-vistazo/)  
[El mapa de lo que sabemos de física (y un poco de lo que no) - microsiervos.com](https://www.microsiervos.com/archivo/ciencia/mapa-de-la-fisica.html)  Dominic Walliman  
![](https://www.microsiervos.com/img/MapaFisicaMolaSaber.jpg)  

Animation vs. Physics. Alan Becker  
[![](https://img.youtube.com/vi/ErMSHiQRnc8/0.jpg)](https://www.youtube.com/watch?v=ErMSHiQRnc8 "Animation vs. Physics. Alan Becker")   

Se intentan poner como subpáginas los recursos asociados solamente a física, por lo que  [recursos física de partículas](/home/recursos/fisica/fisica-de-particulas)  también debería estar accesible en este listado / índice:

## Listado de subpáginas Recursos Física

*  [Cinemática](/home/recursos/fisica/cinematica) 
   *  [MRU](/home/recursos/fisica/cinematica/mru) 
   *  [MRUA](/home/recursos/fisica/cinematica/mrua) 
   *  [MCU](/home/recursos/fisica/cinematica/mcu) 
   *  [Composición](/home/recursos/fisica/cinematica/composicion) 
*  [Movimiento ondulatorio](/home/recursos/fisica/movimiento-ondulatorio) 
*  [Movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio) 
*  [Olimpiadas de Física](/home/recursos/fisica/olimpiadas-de-fisica) 
*  [Píldoras Física RSEF](/home/recursos/fisica/pildoras-fisica-rsef) 
*  [Recursos campo eléctrico](/home/recursos/fisica/recursos-campo-electrico) 
*  [Recursos campo magnético](/home/recursos/fisica/recursos-campo-magnetico) 
*  [Recursos Cosmología](/home/recursos/fisica/recursos-cosmologia) 
*  [Recursos densidad](/home/recursos/fisica/recursos-densidad) 
*  [Recursos dinámica](/home/recursos/fisica/dinamica) 
   *  [Recursos plano inclinado](/home/recursos/fisica/recursos-dinamica/recursos-plano-inclinado) 
*  [Recursos electricidad](/home/recursos/fisica/recursos-electricidad) 
*  [Recursos fuerzas en fluidos](/home/recursos/fisica/fuerzas-en-fluidos) 
   *  [Recursos flotabilidad](/home/recursos/fisica/recursos-fuerzas-en-fluidos/flotabilidad) 
*  [Recursos física cuántica](/home/recursos/fisica/recursos-fisica-cuantica) 
   *  [Dualidad onda corpúsculo](/home/recursos/fisica/recursos-fisica-cuantica/dualidad-onda-corpusculo) 
*  [Recursos física de partículas](/home/recursos/fisica/fisica-de-particulas) 
   *  [Bosón de Higgs](/home/recursos/fisica/fisica-de-particulas/boson-de-higgs) 
   *  [Diagramas de Feynman](/home/recursos/fisica/fisica-de-particulas/diagramas-de-feynman) 
   *  [El bosón de Higgs en ficción](/home/recursos/fisica/fisica-de-particulas/el-boson-de-higgs-en-ficcion) 
   *  [Electrón](/home/recursos/fisica/fisica-de-particulas/electron) 
   *  [Physics Masterclasses](/home/recursos/fisica/fisica-de-particulas/physics-masterclasses) 
*  [Recursos física nuclear](/home/recursos/fisica/recursos-fisica-nuclear) 
*  [Recursos física relativista](/home/recursos/fisica/recursos-fisica-relativista) 
*  [Recursos gravitación](/home/recursos/fisica/recursos-gravitacion) 
*  [Recursos mecánica](/home/recursos/fisica/mecanica) 
   *  [Recursos estática](/home/recursos/fisica/mecanica/estatica) 
*  [Recursos Modelo Estándar](/home/recursos/fisica/recursos-modelo-estandar) 
*  [Recursos máquinas simples](/home/recursos/fisica/maquinas-simples) 
*  [Recursos sistemas materiales](/home/recursos/fisica/recursos-sistemas-materiales) 
*  [Recursos óptica](/home/recursos/fisica/recursos-optica) 
*  [Recursos óptica física](/home/recursos/fisica/recursos-optica-fisica) 
*  [Recursos óptica atmosférica](/home/recursos/fisica/optica-atmosférica) 
*  [Recursos ilusiones ópticas](/home/recursos/fisica/recursos-optica-ilusiones) 
*  [Recursos óptica geométrica](/home/recursos/fisica/optica-geometrica) 
*  [Teoría del caos](/home/recursos/fisica/teoria-del-caos)  
   
   
Con el listado/índice anterior deberían estar todos los enlaces a "recursos física por bloques" dentro de los distintos cursos, especialmente física de 2º de bachillerato  
También puede haber algún enlace no puesto aquí en  [recursos física de 2º de bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato)  

[twitter cossettej/status/1511452441719590913](https://twitter.com/cossettej/status/1511452441719590913)  
It is done! The entire IB Physics SL curriculum with video lectures, pdfs, editable slides, and content guides has been added to the website to download for free!   
[PHYSICS NOTES - passionatelycurioussci](http://passionatelycurioussci.weebly.com/physics-notes.html)  
Click the links below for topic specific pages of lecture videos and notes files in pdf and powerpoint format  

[Real World Physics Problems](https://www.real-world-physics-problems.com/)  
Physics Problems And Solutions For Real World Applications  

[FisQuiMat Recursos de Física, Química y Matemáticas](https://sites.google.com/site/smmfisicayquimica)   Moisés López Caeiro, cc-by-nc-nd

[Física Para estudiantes desesperados. Volumen 1](https://zenodo.org/record/4630661#.YTDnb1uE5Ni)  
   This book is the first volume of a series.  
   It comprehends University Physics taught by the author in a 1 year freshman course for Engineering Physics students, at Universidad de Santiago de Chile.  
   Lautaro Vergara.  

Enunciados exámenes UNED universitarios, incluye física, química ... (en 2016 incluye de 2004 a 2009) 
[Descàrrega exàmens anys anteriors EXÀMENS ACCÉS, GRAUS, MASTERS I CUID - unedcervera.com](https://www.unedcervera.com/cacervera/examens.php). En 2021 remite a [Acceso al Depósito de Exámenes - calatayud.uned.es](http://www.calatayud.uned.es/Examenes/examenes_step_1.asp) e indica "Conforme al acuerdo del Consejo de Gobierno de la UNED de 22 de Diciembre de 2010, **únicamente se permite acceso al Depósito de Exámenes a usuarios del Campus UNED**. Conforme al acuerdo del Consejo de Gobierno de la UNED de 30 de junio de 2015, se advierte que los equipos docentes no quedan vinculados a los contenidos y respuestas de los exámenes de convocatorias anteriores."  

[thestudentroom.co.uk Physics websites](http://www.thestudentroom.co.uk/wiki/physics_websites)  

[Física para Ciencias e Ingeniería - aletosblog.com *waybackmachine*](http://web.archive.org/web/20171002210352/https://aletosblog.com/)  Antonio Marquina Carnicer  
[Física para Ciencias e Ingeniería, presentación - aletosblogdotcom.files.wordpress.com](https://aletosblogdotcom.files.wordpress.com/2013/03/presentacic3b3n1.pdf)  
*"Nace este sitio para recoger el trabajo y la experiencia acumulada por el autor durante sus años de docencia en la asignatura de Física en diferentes niveles, con la intención de que sirva de ayuda a los estudiantes universitarios de esta materia en las carreras de Ciencias e Ingeniería.*  

[ilectureonline.com Courses in Physics](http://www.ilectureonline.com/lectures/subject/PHYSICS)  

[http://www.cyberphysics.co.uk/](http://www.cyberphysics.co.uk/) Cyberphysics - a web-based teaching aid - for students of physics, their teachers and parents...  

[teleformacion.edu.aytolacoruna.es Recursos de física para ESO y Bachillerato.](http://teleformacion.edu.aytolacoruna.es/FISICA/document/index.htm)  
Animaciones, teoría, física interactiva, problemas, prácticas, enlaces. Autor: José Villasuso Gato (I.E.S. Monelos) 1.999-2003  
Buena colección de enlaces, pero muchos desactualizados en 2013.  

[http://www.catfisica.com/index.htm](http://www.catfisica.com/index.htm)  
Página libre de derechos de autor. Organizada por bloques (cinemática, dinámica...) Se pueden imprimir páginas de problemas de 2º de Bachillerato hechos con flash y páginas de teoría y problemas en pdf. Autor: Josep Maria Domènech i Roca. IES Ramon Muntaner. Figueres (Alt Empordà).  

[DEPARTAMENTO DE FÍSICA Y QUÍMICA. IES Leonardo da Vinci. Alicante](https://web.archive.org/web/20181120000442/http://intercentres.edu.gva.es/iesleonardodavinci/Fisica/fisica.htm) waybackmachine   
Manuel Alonso Sánchez, Ana Lozano del Águila, Alberto Vallecillo  
Gran cantidad de materiales por bloques, vídeos, recursos...  

Se puede ver que recibió un premio en 2012 [La web del Dpto. de Física y Química del IES Leonardo da Vinci ha recibido la Mención de Honor de Materiales Didácticos de Ciencias en Ciencia en Acción 2012...](https://fisicayquimica.educarex.es/es/banco-de-recursos/recursos-generales/195-departamento-fisica-y-quimica-ies-leonardo-da-vinci)  

En versión tomada de waybackmachine en 2018 indica 

>AVISO IMPORTANTE: ESTA PÁGINA DEJÓ DE ACTUALIZARSE Y, POCO A POCO, LA IREMOS VACIANDO.  
TENEMOS EL PLACER DE COMUNICAROS LA PUESTA EN MARCHA DE UNA NUEVA PÁGINA WEB PARA LA ENSEÑANZA Y LA DIVULGACIÓN DE LA FÍSICA DESDE LA SECCIÓN LOCAL DE ALICANTE DE LA REAL SOCIEDAD ESPAÑOLA DE FÍSICA, EN DONDE HEMOS INCORPORADO TODOS LOS CONTENIDOS, QUE SEGUIREMOS ACTUALIZANDO Y MEJORANDO DE FORMA PERMANENTE.  
PARA ENTRAR EN LA NUEVA WEB HAZ CLIC AQUÍ Y, LUEGO, ACTUALIZA TUS VÍNCULOS   

Enlaza [ MATERIALES PARA LA ENSEÑANZA Y LA DIVULGACIÓN DE LA FÍSICA (Temas, problemas, experimentos y otros recursos para Secundaria, Bachillerato y cursos básicos de Universidad)](http://rsefalicante.umh.es/fisica.htm) 
  
[laplace.us.es wiki del Departamento de Física Aplicada III en la Escuela Técnica Superior de Ingeniería de la Universidad de Sevilla](http://laplace.us.es/wiki/index.php/P%C3%A1gina_Principal)  
Apuntes y ejercicios física muy bien elaborados  

[Física-I, 1 Ingeniería Química](https://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/Temario-index.html)  [Javier Junquera unican.es](https://personales.unican.es/junqueraj/)  

[matematicasfisicaquimica.com Física y Química 1º de Bachillerato por Temas](http://www.matematicasfisicaquimica.com/fisica-quimica/488-fisica-temas.html)  
Alberto Pérez Montesdeoca. Copyright  

RENA Red Escolar Nacional (Venezuela)  
[RENA Tercera Etapa Física *waybackmachine*](http://web.archive.org/web/20170207200516/http://www.rena.edu.ve/TerceraEtapa/Fisica/index.html)  
[RENA Cuarta Etapa  Física *waybackmachine*](http://web.archive.org/web/20170522231729/http://www.rena.edu.ve/cuartaEtapa/fisica/index.html)  
Recursos desglosadas por "etapas", que son rangos de cursos/edades.  

Este portal auna recursos, experimentos virtuales y laboratorios en linea de dos campos diferentes: la astronomia y fisica de altas energias (High Energy Physics – HEP). Tambien ofrece acceso a una red de telescopios roboticos y a los mayores experimentos que desarrolla el CERN: ATLAS y CMS.  
[http://portal.discoverthecosmos.eu/es](http://portal.discoverthecosmos.eu/es)  
El repositorio de Discover the Cosmos contiene material para la enseñanza en forma de contenidos educativos (fotografías, vídeos, animaciones, ejercicios, gráficos, enlaces) y de actividades de aprendizaje (planes estructurados de clases, organizados de acuerdo a modelos pedagógicos específicos como el aprendizaje basado en la investigación)  
[http://portal.discoverthecosmos.eu/es/repository](http://portal.discoverthecosmos.eu/es/repository)  

[platea.pntic.mec.es 37 Lecciones de Física y Química ©2000-2001. Carlos Palacios](http://platea.pntic.mec.es/~cpalacio/30lecciones.htm)  


[http://www.aboutthemcat.org/physics/physics.php](http://www.aboutthemcat.org/physics/physics.php)  
The Medical College Admissions Test (MCAT) is the standardized exam that all medical colleges in the United States require of prospective students.The MCAT does not test your ability to regurgitate facts, rather the ability to use that knowledge in an applied manner.  

[http://www.physics4kids.com/](http://www.physics4kids.com/) If you are looking for basic physics information, stay on this site. It's not just physics for kids, it's for everyone. We have information on motion, heat and thermodynamics, electricity & magnetism, light, and modern physics topics.  
Read the TERMS & CONDITIONS and PRIVACY POLICY for use of Physics4Kids.com. ©copyright 1997-2015 Andrew Rader Studios, All rights reserved.  

Web de Física  
Animaciones interactivas, ejemplos, ejercicios resueltos, contenidos teóricos... 
[https://www.fisicalab.com/](https://www.fisicalab.com/)  

ETS Ingenieros Aeronaúticos, UPM, Apuntes Física 1, Dr. L. Conde  
[http://www.aero.upm.es/departamentos/fisica/PagWeb/asignaturas/fisica1/ApuntesFisica1C1314.pdf](http://www.aero.upm.es/departamentos/fisica/PagWeb/asignaturas/fisica1/ApuntesFisica1C1314.pdf)  

[https://descubrirlafisica.wordpress.com/](https://descubrirlafisica.wordpress.com/)  
Aida Cañoto

[uco.es Manuel R. Ortega Girón, Lecciones de física, resultados de los problemas](http://www.uco.es/users/mr.ortega/almacen/libros/LFMRP.pdf)  
  
[www.educ.ar -> Recursos Secundaria -> Física](https://www.educ.ar/recursos/buscar?tema=117&audiences=2&subjects=9&levels=3)   

[http://www.cartagena99.com/recursos/recursos_fisica.php](http://www.cartagena99.com/recursos/recursos_fisica.php)  [http://www.ugr.es/~aquiran/docencia.htm](http://www.ugr.es/~aquiran/docencia.htm)  

Arturo Quirantes, tiene recursos y entre ellos  
[https://www.guao.org/tercer_ano/fisica](https://www.guao.org/tercer_ano/fisica)  
[https://www.guao.org/cuarto_ano/fisica](https://www.guao.org/cuarto_ano/fisica)  

[proyectodescartes.org Física Mecánica](https://proyectodescartes.org/descartescms/fisica-y-quimica)  
Libros interactivos  
[proyectodescartes.org Física. Volumen I, Mecánica](https://proyectodescartes.org/descartescms/fisica-y-quimica/item/3235-fisica-volumen-1)  
[proyectodescartes.org Física. Volumen II, Mecánica, Ondas y Acústica](https://proyectodescartes.org/descartescms/fisica-y-quimica/item/3379-fisica-volumen-ii)  
[proyectodescartes.org Física. Volumen III, Termodinámica y Electricidad](https://proyectodescartes.org/descartescms/fisica-y-quimica/item/3291-fisica-volumen-iii)  
Enlace directo a libro  
[proyectodescartes.org iCartesiLibri proyectodescartes.org Física. Volumen II, Mecánica, Ondas y Acústica](https://proyectodescartes.org/iCartesiLibri/materiales_didacticos/Fisica_II/index.html)  

**3 abril 2021**  
Teacher Makes Beautiful Illustrations of Your Favorite Physics Formulas  
From electromagnetism to the law of conservation of energy, this teacher illustrates all your favorite physics formulas.  
[interestingengineering.com Teacher Makes Beautiful Illustrations of Your Favorite Physics Formulas](https://interestingengineering.com/this-physics-teacher-makes-art-out-of-formulas)  
Yuri Kovalenok is a physics teacher from Russia whose physics and engineering notes are truly works of art. You can check out all of his work on his  [Instagram account,](https://www.instagram.com/jurij0001/?hl=en)  for your convenience, we've compiled our 10 favorite notebook pages below.  

[Arturo Quirantes Sierra - Página de docencia](http://www.ugr.es/~aquiran/docencia.htm)  
Incluye materiales y algunos exámenes de años anteriores de la asignatura Física I (Grado en Química)  


[Revista de Enseñanza de la Física (Nov 2010). Simple+mente física. Rafael Garcia Molina](https://doaj.org/article/734543fe4e8049d397b9a2f40b37bc1d)  
Desde el curso 2002-03 llevo proponiendo en los tablones de anuncios de algunas facultades de la Universidad de Murcia (principalmente las de disciplinas científicas y técnicas) las cuestiones de física que aparecen bajo el nombre genérico de Simple+mente física. Cada semana lectiva se presenta una cuestión nueva y, al mismo tiempo, se da la solución de la cuestión anterior.  

[Simple+mente física - um.es](http://bohr.inf.um.es/miembros/rgm/s+mf/index.html)  

[Physics in Advent](https://www.physics-in-advent.org/)  
24 small, simple experiments and physics mysteries: an Advent calendar with a difference. Each day from 1st to 24th December there is a video clip of an experiment which you can do yourself afterwards.  

[Michel van Biezen - canal youtube](https://www.youtube.com/@MichelvanBiezen)  
> This channel contains a complete list of physics videos, as well as hundreds of chemistry, astronomy,  math, and mechanical engineering videos. The physics videos explain the fundamental concepts of physics with some easy to follow examples on how to solve physics problems. The chemistry videos cover all the basic topics of chemistry, the astronomy videos explain the wonders of Earth and our Universe,  and the math videos cover many topics in algebra, trigonometry, pre-calculus, calculus and differential equations.    

[Studyit L3 Physics](https://studyit.govt.nz/Physics/level/16)  

[Once cursos universitarios gratuitos sobre física - genbeta](https://www.genbeta.com/web/once-cursos-universitarios-gratuitos-sobre-fisica)  
MOOC en español y en inglés  

[Libros Física Universitaria volumen 1, 2 y 3 - openstax.org](https://openstax.org/subjects/ciencia#Fisica)   © 1999-2024, Rice University. Salvo que se indique lo contrario, los libros de texto de este sitio están autorizados conforme a la Creative Commons Attribution 4.0 International License.
