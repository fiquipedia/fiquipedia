
# Recursos Química 2º Bachillerato

## Apuntes
Aparte de que en la página de  [recursos - apuntes](/home/recursos/apuntes)  hay apuntes, coloco aquí algunos ya concretos de química de 2º de bachillerato, aparte de enlazar a  [apuntes de elaboración propia de química de 2º de bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato) .  
También se pueden buscar  [libros](/home/recursos/libros)  asociados a 2º de bachillerato, o bien  [recursos de química](/home/recursos/quimica)  en general, o también  [páginas y blog personales](/home/recursos/paginas-blog-personales)   
* [Apuntes 2.º Bach Química | FisiQuímicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/2bach/quimica/)   
* [2ºBach Química - profesorjrc.es](https://profesorjrc.es/2quimica.htm) Jorge Rojo Carrascosa   
* [Apuntes Química 2º Bachillerato - fisquiweb.es](http://fisquiweb.es/Apuntes/apun2BQui.htm) cc-by-nc-sa  
* [Recursos Química 2º Bachillerato - serendiphia.es](http://serendiphia.es/recursos-quimica-2o-bachillerato/)  Rafael Cabrera   
* [2ºBach Química - elortegui.org](http://ciencia.elortegui.org/datos/2BACHQUM/quimica2.htm)  
* [Iniciación a la Química, preparación para el acceso a la Universidad, Junta de Andalucía, ISBN 978-84-8439-393-1, gratuito, pdf](https://www.juntadeandalucia.es/sites/default/files/2022-02/1337161060iniciacion_a_la_quimica_1.pdf)  
* [Temas Química 2º Bachillerato. Fernando Escudero Ramos. IES Fernando de los Ríos, Quintanar del Rey (Cuenca)](http://ies-fernandorios.centros.castillalamancha.es/content/f%C3%ADsica-y-qu%C3%ADmica)  


 [Química (autoformació IOC) - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=19)  
[Química II (Bloc 1) ~ gener 2020 - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=138)  
[Química II (Bloc 2) ~ gener 2020 - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=140)  
   
 [Departamento de Física y Química. Química 2º Bach. IES Ntra. Sra. de la Almudena - Madrid](http://almudenafyq.webcindario.com/index6.html)   
Enlaces a problemas, exámenes, PAU ...  
Incluye enlace a libro que ha preparado gratuitamente la Junta de Andalucía para preparar la Química de Selectividad: Iniciación a la Química (lo incluyo en  [libros](/home/recursos/libros) )  

  
Apuntes, Escritos y Ensayos Científicos  
 [Apuntes qumica 2 Bachillerato](http://www.escritoscientificos.es/apunquim/inicio.htm)   
Felipe Moreno Romero. Prof. Educación Secundaria (Andalucía). cc-by-nc-sa  

[Problemas Química resueltos - Berto Tomás](https://drive.google.com/drive/folders/1QzwgrCuw67HzVwMDVKE_4Up-fu7x9Jun)    
[Aprendiendo Física con Berto Tomás](https://www.aprendiendofisicaconbertotomas.com/)  
  
  
   
 [QUÍMICA DE BACHILLERATO - Página web de laquimicaesfacil](http://laquimicaesfacil.jimdo.com/bachilleratoq/)   
Materiales asociados a academia. Clases particulares en San Fernando, Cádiz  
Tiene en enlaces y alberga cosas con derechos de autor  
PROFESOR JANO es Víctor M. Vitoria  
 [QUÍMICA | PROFESOR JANO es Víctor M. Vitoria](https://profesorjano.wordpress.com/quimica/)   
  
QUÍMICA. 2º de Bachillerato  
 [QU&Iacute;MICA 2&ordm; CLARA CAMPOAMOR](http://fresno.pntic.mec.es/~fgutie6/quimica2/)   
© Fco. Javier Gutiérrez Rodríguez  
    
Departamento física y química IES Valle del Arlanza, Lerma, Burgos.   
 [print.cgi](http://iesvalledelarlanza.centros.educa.jcyl.es/sitio/print.cgi?wid_seccion=14&wid_item=44&wOut=print)   
En 2015 apuntes y ejercicios física 2º bachillerato y química 2º bachillerato  
  
 [Química 2º Bach | La web de Chema Martín](https://chemamartin.wordpress.com/quimica-2o-bach/) Apuntes y ejercicios por bloques cc-by-sa  
  
Colegio Cristo Rey  
 [neninaM](http://colegiocristorey.com/nenuca/nenina/neninaQFbach.html)   
Apuntes, ejercicios, prácticas  
Nenina Martín Ossorio  
  
 [apuntes-de-quimica-2-bachillerato-curso.html](http://fqsecundaria.blogspot.com.es/2015/01/apuntes-de-quimica-2-bachillerato-curso.html)   
Diego Palacios  
  
Materiales didácticos de QUÍMICA Guía práctica de Maturita (cualitativamente una reválida de bachillerato + secundaria) Secciones Bilingües de Eslovaquia   
 [Materiales didácticos de química. Guía práctica de Maturita. Secciones bilingües de Eslovaquia](https://sede.educacion.gob.es/publiventa/materiales-didacticos-de-quimica-guia-practica-de-maturita-secciones-bilinges-de-eslovaquia/ensenanza-lengua-espanola/16102)   

## Recursos por bloques

### [Electroquímica](/home/recursos/quimica/electroquimica)

## Apuntes de elaboración propia
Ver página separada  [apuntes elaboración propia química 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato) .(Estos apuntes de química están en la web en un sitio distinto a los de física, mantengo la url ya que la empecé a usar en códigos QR. El tema es que unos están enlazados desde apuntes (física) y otros desde recursos por materia y curso..)  

