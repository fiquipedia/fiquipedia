
# Biología y geología

Lo primero .... responder a la pregunta ¿Qué pinta una página de Biología y Geología en una web sobre Física y Química?  
Como comento, esta página es para la especialidad de física y química, y hay otras materias que se pueden impartir como por ejemplo [Biología y Geología (4º ESO)](/home/materias/eso/4-eso-biologia-y-geologia)  
Otro ejemplo son la materias de  [Ciencias para el Mundo Contemporáneo de 1º Bachillerato (LOE)](/home/materias/bachillerato/cienciasparaelmundocontemporaneo-1bachillerato)  y las materias de  [Cultura Científica de 4º ESO](/home/materias/eso/cultura-cientifica-4-eso/curriculo-cultura-cientifica-4-eso)  y  [Cultura Científica de 1º Bachillerato](/home/materias/bachillerato/cultura-cientifica-1-bachillerato) , que tienen contenidos relacionados  

También puede tener relación con bioquímica, asociados a  [apuntes formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion) ,  [geometría molecular](/home/recursos/quimica/geometria-molecular)  e  [isomería](/home/recursos/quimica/isomeria). La bioluminiscencia enlaza con la quimioluminiscencia.    

[Map of biology Dominic Walliman](https://dominicwalliman.com/post/168359349085/map-of-biology-by-dominic-walliman-via)  
![](https://live.staticflickr.com/65535/24071850387_5f6b525031_b.jpg "Map of biology Dominic Walliman")  

**Ojo: puede haber recursos en esas páginas, pendiente unificar**  

En lugar de tener desperdigados por ahí recursos, los comparto en esta página. Si crece mucho la separaré por cursos y luego por biología / geología, inicialmente la idea es todo junto  

También puede haber aspectos de biología relacionables con Física (cinemática, dinámica, energía) y con Química (compuestos, reacciones), y con aspectos transversables como medio ambiente. En los frikiexámenes por ejemplo combinan distintos temas


[![](https://img.youtube.com/vi/hsWr_JWTZss/0.jpg)](https://www.youtube.com/watch?v=hsWr_JWTZss "The Trouble With Tumbleweed")   


## Recursos generales
Proyecto Biosfera  
[http://recursostic.educacion.es/ciencias/biosfera/web/](http://recursostic.educacion.es/ciencias/biosfera/web/)  
cc-by-nc-sa

Rincón didáctico Biología y Geología (varios niveles), Junta de Extremadura [http://rincones.educarex.es/byg/index.php](http://rincones.educarex.es/byg/index.php)  

[http://www.educa.madrid.org/web/cc.nsdelasabiduria.madrid/ejercicios4.htm](http://www.educa.madrid.org/web/cc.nsdelasabiduria.madrid/ejercicios4.htm)  Colegio Nuestra Señora de la Sabiduría.  

[http://recursostic.educacion.es/secundaria/edad/index_biogeo.htm](http://recursostic.educacion.es/secundaria/edad/index_biogeo.htm)  

[http://cienciasnaturales.es/index.html](http://cienciasnaturales.es/index.html) Muchos materiales por nivel y contenido, José Antonio Borregero Rolo, Copyright  

[http://docentes.educacion.navarra.es/metayosa/](http://docentes.educacion.navarra.es/metayosa/) "Cosas de ciencias" por Isabel Etayo Salazar

IES Poeta Claudio Rodríguez, Miguel Ángel García  
Recursos por cursos desde ESO a Bachillerato  
[http://iespoetaclaudio.centros.educa.jcyl.es/sitio/index.cgi?wid_seccion=19](http://iespoetaclaudio.centros.educa.jcyl.es/sitio/index.cgi?wid_seccion=19)  

[https://biologia-geologia.com/libro4biogeo.html](https://biologia-geologia.com/libro4biogeo.html) 

## Recursos Biología
 [https://sites.google.com/a/iesitaca.org/juan-ortiz-ortiz/mapas-conceptuales](https://sites.google.com/a/iesitaca.org/juan-ortiz-ortiz/mapas-conceptuales)  
 
Genética  
ADN  
[http://www.dnalc.org/resources/](http://www.dnalc.org/resources/) DNA Learning Center, incluye vídeos (en inglés) © Copyright,  [Cold Spring Harbor Laboratory](http://www.cshl.edu/) . All rights reserved  

[http://www.ornl.gov/sci/techresources/Human_Genome/education/education.shtml](http://www.ornl.gov/sci/techresources/Human_Genome/education/education.shtml)  Human Genome Project Education Resources. Permission to use these documents is not needed, but please credit the U.S. Department of Energy Genome Programs  

La evolución. El origen de la vida. La evolución de los seres vivos. Pruebas de la evolución. Funcionamiento de la evolución. Fuerzas evolutivas. Macroevolución y microevolución. Clasificación de los seres vivos. La Evolución humana.  
[http://recursos.cnice.mec.es/biosfera/alumno/4ESO/evolucion/index.htm](http://recursos.cnice.mec.es/biosfera/alumno/4ESO/evolucion/index.htm) 

## Recursos Geología
 [http://rincones.educarex.es/byg/index.php/origen-y-estructura-de-la-tierra](http://rincones.educarex.es/byg/index.php/origen-y-estructura-de-la-tierra)  
 [http://rincones.educarex.es/byg/index.php/geodinamica-interna](http://rincones.educarex.es/byg/index.php/geodinamica-interna) 

## Recursos Biología molecular
[http://biomodel.uah.es/](http://biomodel.uah.es/)  
Incluye:
- Estereoisomería
- Tabla periódica de radios
- Laboratorios virtuales y técnicas  

[twitter LluisMontoliu/status/1244266995920633857](https://twitter.com/LluisMontoliu/status/1244266995920633857)  
Sí. Es posible explicar #genética básica con las piezas del #TENTE  
He lanzado ya dos vídeos de divulgación científica contando la técnica de diagnóstico por RT-PCR y el significado de las mutaciones en el ADN.  
[Playlist #BIOTENTE en mi canal de #YouTube](https://youtube.com/playlist?list=PL2ulhgu9dpGdKf9OJbUmdh8iJQyH1YcCb)  
[![](https://pbs.twimg.com/media/EUSF-7bXQAI3iuv?format=jpg) ](https://pbs.twimg.com/media/EUSF-7bXQAI3iuv?format=jpg)   
[![](https://pbs.twimg.com/media/EUSF-73XsAI8FR0?format=jpg) ](https://pbs.twimg.com/media/EUSF-73XsAI8FR0?format=jpg)  
