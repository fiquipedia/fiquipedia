[comentario]: # (antes de la migración de 2021 había algunas páginas colgando de recursos con el nombre de recursos inicial y otras que no, por ejemplo home/recursos/recursos-apuntes y home/recursos/fisica, aunque luego en el nombre a veces no tenían la palabra recursos; la página home/recursos/recursos-apuntes tenía de nombre apuntes y en el listado de subpáginas se ordenaba alfabéticamente por la a. Intento usar aliases para unificar sin poner recursos- de modo que si se implementa el camino de migas se pueda usar el nombre de la página / ya se saben que son recursos si en la url van precedidos de recursos, aunque en algunas páginas el nombre de recursos- podría dejarse: recursos-para-profesores, recursos-para-oposiciones ... Aunque se creen alias, si había subcarpetas se siguen mantiendo nombres)

# Recursos

Al final se incluye un listado de subpáginas con todas las asociadas a recursos. El listado es manual (hasta que implemente algo automático) y puede haber omitida alguna página.  
Por evitar duplicidad, no se incluyen aquí las subpáginas de Física y Química, y dentro de ellas se mantien el sublistado asociado  
* **[Recursos Física](/home/recursos/fisica)**  
* **[Recursos Química](/home/recursos/quimica)**  

En esta página se centralizan los distintos enlaces a recursos, que se intentarán agrupar por distintos conceptos, pudiendo estar el mismo recurso en varias agrupaciones, y puede que dentro de una agrupación (por ejemplo un 2º Bachillerato, se vuelva a hacer otra agrupación , por ejemplo por bloques). Por ejemplo en OER Commons se agrupan por Subject áreas (aquí serían física, química, biología, geología, matemáticas y tecnología, que es lo más amplio que cubre impartir ACT), Grade leves (aquí sería curso, pero lo asocio a materia), material types (aquí serían los "tipos"), Course Related Materials (aquí serían cursos online) y Libraries and Collec tions que serían equivalente a repositorios. 
 
Mi idea inicial para organizar los recursos, que básicamente son enlaces, era crear páginas que los vayan recopilando según sus agrupaciones. Tras un tiempo intentando elaborar esa idea, he visto que el volumen la hace inviable, porque la misma información sobre el recurso tiene que estar en muchas páginas. Aunque algún recurso importante lo ponga en la propia página, mi idea (29 noviembre 2011) es poner un enlace a mi cuenta pública de delicious donde estén las etiquetas que corresponden a esa agrupación de recursos. En los propios comentarios de delicious (limitados a 1000 caracteres), pondré los comentarios sobre el enlace (licenciamiento, valoración ...) Mi cuenta en delicious era  [http://delicious.com/hunk/](http://delicious.com/hunk/)  Para ver los enlaces de una agrupación concreta, hay que seleccionar las etiquetas. Aunque pondré los enlaces explícitamente,se puede hacer sobre la propia url si se conocen las etiquetas, y así por ejemplo para buscar recursos de física y química sobre la tabla preriódica:  [http://delicious.com/hunk/FQ+Secundaria+TablaPeriodica+Recursos](http://delicious.com/hunk/FQ+Secundaria+TablaPeriodica+Recursos)    
2014: Esa idea de delicious tampoco la aplico y no me parece viable, porque el orden de los recursos y "el formato" en el listado de delicious no me gusta, así que intentaré tener todos los enlaces posibles según la idea inicial; sea como sea esta página siempre está en construcción y siempre quedará algo pendiente ...
En la migración de 2021 veo que no funcionan los enlaces públicos a https://del.icio.us/ 

Hay muchas páginas de recursos en internet, intentaré corregir aquí lo que echo en falta en muchas de estas recopilaciones (a veces estará en los comentarios de delicious): 
- Orden / clasificación / agrupación: muchas veces es una lista sin más, ya he comentado que intentaré poner aquí todas las agrupaciones posibles.
    - En la derecha de esta página incluyo el listado de páginas de recursos que son todas las páginas con recursos (en la migración a GitLab pasa a estar al final, y no se garantiza que incluya todos porque no se genera automáticamente), y que aparecen ordenadas alfabéticamente por título 
- Validación: muchas veces los enlaces fallan. Siempre que pueda pondré una fecha de comprobación del enlace. 
- Licenciamiento: casi nunca se aclara el tipo de licenciamiento de cada recurso. Siempre que sea posible indicaré recursos abiertos / online, sin problemas de copyright 
- Posibilidad de revisar: en algunos listados al detectar un error no hay dirección de contacto para solicitar corregirlo, pero aquí google sites permite añadir comentarios a cualquier página. 
- Valoración/opinión: a veces los enlaces no son más que eso. Hay que entrar dentro, ver lo que son y valorarlos uno mismo. Intentaré compartir mi opinión de los que haya revisado yo mismo.

También es posible que cite como recursos aulas virtuales Moodle de EducaMadrid de mi centro actual / cedidas a EducaMadrid: a veces recopilar recursos en web y aula virtual supone cierta duplicidad. Las aulas las suelo dejar siempre con acceso de invitado; pueden quedar algunas en otros centros, pero ya no las mantengo allí.  
[EducaMadrid, aulas virtuales, IES Alkal'a Nahar, Departamento de Física y Química](https://aulavirtual3.educa.madrid.org/ies.alkalanahar.alcala/course/index.php?categoryid=21)  

##  [Recursos Educativos Abiertos](/home/recursos/recursos-educativos-abiertos) 

##  [Recursos de elaboración propia](/home/recursos/recursos-de-elaboracion-propia) 

##  [Recursos por materia / curso](/home/recursos/recursos-por-materia-curso) 

##  [Contenidos del currículo con enlaces a recursos](/home/recursos/contenidos-curriculo-con-enlaces-a-recursos) 

##  Recursos por tipo
 Las clasificaciones muchas veces se solapan entre ellas, y puede haber un recurso que encaje como de varios tipos
*  [Materiales autosuficientes](/home/recursos/materiales-autosuficientes) 
*  [Simulaciones](/home/recursos/simulaciones) 
*  [Webquest](/home/recursos/webquest) 
*  [Caza del tesoro](/home/recursos/caza-del-tesoro) 
*  [Apuntes](/home/recursos/apuntes) 
*  [Libros](/home/recursos/libros) 
*  [Imágenes (mapa de bits)](/home/recursos/imagenes-mapa-de-bits) 
*  [Imágenes (vectoriales, cliparts)](/home/recursos/imagenes-vectoriales-cliparts) 
*  [Audio](/home/recursos/audio) 
*  [Vídeo](/home/recursos/videos) 
*  [Pruebas Acceso Universidad (PAU)](/home/recursos/recursospau) 
*  [Cursos online/interactivos](/home/recursos/cursos-online-interactivos) 
*  [Prácticas - experimentos](/home/recursos/practicas-experimentos-laboratorio)  (laboratorio / en casa)
*  [Ejercicios](/home/recursos/ejercicios) 
*  [Páginas ciencia /física/química/recopilatorios de enlaces](/home/recursos/recopilatorios-enlaces-recursos) 
*  [Páginas/blog personales](/home/recursos/paginas-blog-personales) 
*  [Publicaciones](/home/recursos/publicaciones) 
*  [Recursos para profesores](/home/recursos/recursos-para-profesores) 
*  [Recursos para oposiciones](/home/recursos/recursos-para-oposiciones) 
*  [Recursos uso calculadora](/home/recursos/recursos-uso-calculadora) 
*  [Actividades fuera del centro](/home/recursos/recursos-de-actividades-fuera-del-centro) 
*  [Apps para móviles](/home/recursos/recursos-apps) 
*  [Material para el aula del laboratorio](/home/recursos/material-para-aula-laboratorio) 
*  [Recursos divulgación científica](/home/recursos/recursos-divulgacion-cientifica) 
*  [Recursos sobre estrategias de resolución de problemas](/home/recursos/recursos-sobre-estrategias-resolucion-problemas) 
* ...


##  Recursos por contenidos / bloques
 Estas páginas las iré creando colgando del árbol de páginas del site dentro de "Recursos", por lo que intentaré que no contengan de nuevo la palabra recursos, y así se puede intentar buscar un tema concreto poniendo a secas fiquipedia.es/home/recursos/tema Los pongo por orden alfabético
*  [Astronomía](/home/recursos/recursos-astronomia) 
*  [Biología y geología](/home/recursos/biologia-y-geologia) 
*  [Cinemática](/home/recursos/fisica/cinematica) 
*  [Concentración](/home/recursos/quimica/concentracion) 
*  [Concepto de mol](/home/recursos/quimica/concepto-de-mol) 
*  [Didáctica](/home/recursos/didactica) 
*  [Dinámica](/home/recursos/fisica/dinamica) 
*  [Energía](/home/recursos/energia) 
*  [Enlace químico](/home/recursos/quimica/enlace-quimico) 
*  [Equilibrio químico](/home/recursos/quimica/equilibrio-quimico) 
*  [Espectro](/home/recursos/recursos-espectro) 
*  [Estructura del átomo ](/home/recursos/quimica/recursos-estructura-del-atomo) 
*  [Física](/home/recursos/fisica) 
*  [Flotabilidad](/home/recursos/fisica/recursos-fuerzas-en-fluidos/flotabilidad) 
*  [Formulación](/home/recursos/quimica/formulacion) 
*  [Fuerzas intermoleculares](/home/recursos/quimica/fuerzas-intermoleculares) 
*  [Gravitación](/home/recursos/fisica/recursos-gravitacion) 
*  [Isomería](/home/recursos/quimica/isomeria) 
*  [Matemáticas](/home/recursos/matematicas) 
*  [Mecánica](/home/recursos/fisica/mecanica) 
*  [Mecánica cuántica](/home/recursos/fisica/recursos-fisica-cuantica) 
*  [Medición / unidades](/home/recursos/recursos-medida) 
*  [Movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio) 
*  [Movimiento ondulatorio](/home/recursos/fisica/movimiento-ondulatorio) 
*  [Óptica geométrica](/home/recursos/fisica/optica-geometrica) 
*  [Orbitales (atómicos y moleculares)](/home/recursos/orbitales) 
*  [Propiedades sustancias](/home/recursos/quimica/recursos-propiedades-sustancias) 
*  [Química](/home/recursos/quimica) 
*  [Química orgánica](/home/recursos/quimica/quimica-organica) 
*  [Tabla periódica / elementos químicos](/home/recursos/quimica/recursos-tabla-periodica) 
*  [Tecnología](/home/recursos/recursos-tecnologia) 
*  [Termoquímica](/home/recursos/quimica/termoquimica) 
*  [Termodinámica](/home/recursos/termodinamica) 
* ...
   
## Listado de subpáginas

*  [Apuntes](/home/recursos/apuntes) 
   *  [Apuntes elaboración propia Física 2º Bachillerato](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato) 
   *  [Apuntes formulación](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion) 
*  [Biología y geología](/home/recursos/biologia-y-geologia) 
*  [Caza del tesoro](/home/recursos/caza-del-tesoro) 
*  [Contenidos currículo con enlaces a recursos](/home/recursos/contenidos-curriculo-con-enlaces-a-recursos) 
*  [Cursos online / interactivos](/home/recursos/cursos-online-interactivos) 
*  [Docencia contenidos física y química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel) 
   *  [Docencia contenidos Física y Química por nivel, según currículo](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/docencia-contenidos-fisica-y-quimica-por-nivel-segun-curriculo) 
   *  [Docencia contenidos física y química por nivel, visión personal](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/docencia-contenidos-fisica-y-quimica-por-nivel-vision-personal) 
   *  [Pizarras Física y Química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel) 
*  [Ejercicios](/home/recursos/ejercicios) 
   *  [Ejercicios elaboración propia Física 2º Bachillerato](/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato) 
*  [Escepticismo y pensamiento crítico](/home/recursos/escepticismo-pensamiento-critico) 
*  [Exámenes](/home/recursos/examenes) 
   *  [FriQuiExámenes](/home/recursos/examenes/friquiexamenes) 
*  [Historia de la ciencia](/home/recursos/historia-de-la-ciencia) 
*  [Imágenes (Mapa de Bits)](/home/recursos/imagenes-mapa-de-bits) 
*  [Inteligencia artificial](/home/recursos/inteligencia-artificial) 
*  [Juegos](/home/recursos/juegos) 
*  [Lectura](/home/recursos/lectura) 
*  [Libros](/home/recursos/libros) 
   *  [Libros de texto](/home/recursos/libros/libros-de-texto) 
*  [Material para aula laboratorio](/home/recursos/material-para-aula-laboratorio) 
*  [Materiales autosuficientes](/home/recursos/materiales-autosuficientes) 
*  [Memes](/home/recursos/memes) 
*  [Presión](/home/recursos/presion) 
*  [Proyectos de investigación](/home/recursos/proyectos-de-investigacion) 
*  [Prácticas / experimentos / laboratorio](/home/recursos/practicas-experimentos-laboratorio) 
   *  [Prácticas de laboratorio de elaboración propia](/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia) 
*  [Publicaciones](/home/recursos/publicaciones) 
*  [Páginas / blog personales / departamento / centro / comunidad ...](/home/recursos/paginas-blog-personales) 
*  [Páginas ciencia / física / química / recopilatorios enlaces recursos](/home/recursos/recopilatorios-enlaces-recursos) 
*  [Recursos adaptaciones curriculares Física y Química](/home/recursos/recursos-adaptaciones-curriculares-fisica-quimica) 
*  [Recursos apps](/home/recursos/recursos-apps) 
*  [Recursos Aprendizaje Basado en Proyectos en Física y Química](/home/recursos/recursos-aprendizaje-basado-proyectos-fisica-quimica) 
*  [Recursos astronomía](/home/recursos/recursos-astronomia) 
*  [Recursos Audio](/home/recursos/audio) 
*  [Recursos calor y temperatura](/home/recursos/calor) 
*  [Recursos Ciencia ficción](/home/recursos/ciencia-ficcion) 
*  [Recursos ciencia y mujer](/home/recursos/ciencia-y-mujer) 
*  [Recursos cifras significativas](/home/recursos/recursos-cifras-significativas) 
*  [Recursos de actividades fuera del centro](/home/recursos/recursos-de-actividades-fuera-del-centro) 
   *  [Concursos](/home/recursos/concursos) 
*  [Recursos de elaboración propia](/home/recursos/recursos-de-elaboracion-propia) 
*  [Recursos de simulaciones / laboratorio virtual](/home/recursos/simulaciones) 
   *  [Recursos simulaciones: aspectos técnicos](/home/recursos/simulaciones/recursos-simulaciones-aspectos-tecnicos) 
*  [Recursos Divulgacion Científica](/home/recursos/recursos-divulgacion-cientifica) 
*  [Recursos Educativos Abiertos](/home/recursos/recursos-educativos-abiertos) 
*  [Recursos efémerides ciencia](/home/recursos/recursos-efemerides-ciencia) 
*  [Recursos Energía](/home/recursos/energia) 
*  **[Recursos Física](/home/recursos/fisica)** 
*  [Recursos Física y Química y Medio Ambiente](/home/recursos/recursos-fisica-quimica-y-medio-ambiente) 
*  [Recursos Física y Química y Sociedad](/home/recursos/recursos-fisica-y-quimica-y-sociedad) 
*  [Recursos hologramas](/home/recursos/recursos-hologramas) 
*  [Recursos Imágenes (vectoriales, cliparts)](/home/recursos/imagenes-vectoriales-cliparts) 
*  [Recursos matemáticas](/home/recursos/matematicas) 
   *  [Cálculo vectorial](/home/recursos/recursos-matematicas/calculo-vectorial) 
*  [Recursos medida](/home/recursos/recursos-medida) 
*  [Recursos museos y centros de ciencia](/home/recursos/recursos-museos-y-centros-de-ciencia) 
*  [Recursos método científico](/home/recursos/recursos-metodo-cientifico) 
*  [Recursos notación científica](/home/recursos/recursos-notacion-cientifica) 
*  [Recursos Orbitales](/home/recursos/orbitales) 
*  [Recursos para oposiciones](/home/recursos/recursos-para-oposiciones) 
   * [Problemas oposiciones FQ por comunidad](/home/recursos/recursos-para-oposiciones/recursos-para-oposiciones-problemas-por-comunidad)
   * [Problemas oposiciones FQ por bloque](/home/recursos/recursos-para-oposiciones/recursos-para-oposiciones-problemas-por-bloques)         
   *  [Programación y unidades didácticas](/home/recursos/recursos-para-oposiciones/programacion-y-unidades-didacticas) 
   *  [Temarios oposiciones Física y Química](/home/recursos/recursos-para-oposiciones/temarios-oposiciones) 
*  [Recursos para profesores](/home/recursos/recursos-para-profesores) 
*  [Recursos PAU](/home/recursos/recursospau) 
   *  [Ficheros enunciados PAU Electrotecnia: recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales)](/home/recursos/recursospau/ficheros-enunciados-pau-electrotecnia) 
   *  [Ficheros enunciados PAU Física: recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales)](/home/recursos/recursospau/ficheros-enunciados-pau-fisica) 
   *  [Ficheros enunciados PAU mecánica](/home/recursos/recursospau/ficheros-enunciados-pau-mecanica) 
   *  [Ficheros enunciados PAU Química: recopilación local de enunciados completos originales / oficiales (y algunas soluciones oficiales)](/home/recursos/recursospau/ficheros-enunciados-pau-quimica) 
   *  [Recursos PAU Genéricos](/home/recursos/recursospau/recursos-pau-genericos) 
   *  [Recursos PAU mayores de 25 años](/home/recursos/recursospau/recursos-pau-mayores-25-anos) 
*  [Recursos por materia / curso](/home/recursos/recursos-por-materia-curso) 
   *  [Recursos Ampliación Física y Química 4º ESO](/home/recursos/recursos-por-materia-curso/recursos-ampliacion-fisica-y-quimica-4-eso) 
   *  [Recursos Ciencias para el Mundo Contemporáneo 1º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-ciencias-para-el-mundo-contemporaneo) 
   *  [Recursos Electrotecnia 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-electrotecnia) 
   *  [Recursos Física 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato) 
   *  [Recursos Física y Química 1º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato) 
      *  [Apuntes elaboración propia 1º Bachilerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato) 
   *  [Recursos Física y Química 2º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso) 
      *  [Apuntes elaboración propia Física y Química 2º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso/apuntes-elaboracion-propia-fisica-y-quimica-2-eso) 
   *  [Recursos Física y Química 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso) 
      *  [Apuntes elaboración propia 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso/apuntes-elaboracion-propia-3-eso) 
   *  [Recursos Física y Química 4º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso) 
      *  [Apuntes Física y Química 4º ESO elaboración propia](/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso/apuntes-fisica-y-quimica-4-eso-elaboracion-propia) 
   *  [Recursos Física y Química por bloques comunes 2º ESO y 3º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-por-bloques-comunes-2-eso-y-3-eso) 
   *  [Recursos Química 2º Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato) 
      *  [Apuntes de elaboración propia de Química de 2º de Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato) 
*  **[Recursos Química](/home/recursos/quimica)** 
*  [Recursos Realidad Virtual](/home/recursos/recursos-realidad-virtual) 
*  [Recursos relativos al espectro](/home/recursos/recursos-espectro) 
   *  [Espectroscopio casero](/home/recursos/recursos-espectro/espectroscopio-casero) 
*  [Recursos situaciones de aprendizaje](/home/recursos/situaciones-de-aprendizaje) 
*  [Recursos sobre estrategias resolución problemas](/home/recursos/recursos-sobre-estrategias-resolucion-problemas) 
*  [Recursos tecnología](/home/recursos/recursos-tecnologia) 
*  [Recursos teoría cinético-molecular](/home/recursos/recursos-teoria-cinetica) 
*  [Recursos test online](/home/recursos/recursos-test-online) 
*  [Recursos uso calculadora](/home/recursos/recursos-uso-calculadora) 
*  [Rúbricas](/home/recursos/rubricas) 
*  [STEM](/home/recursos/stem) 
*  [Teoremas y leyes](/home/recursos/teoremas-leyes) 
*  [Termodinámica](/home/recursos/termodinamica) 
*  [Vídeos](/home/recursos/videos) 
*  [Webquest](/home/recursos/webquest) 
   
   
