# Evaluación LOMLOE 

Aquí se trata solo lo relacionado con evaluación de [LOMLOE](/home/legislacion-educativa/lomloe)  
De momento se ponen aquí ideas sobre la nueva evaluación de acceso a universidad, y se separará cuando tenga entidad propia  

Ver información adicional en [Promoción y titulación en eduación - algoquedaquedecir](https://fiquipedia.gitlab.io/algoquedaquedecir/post/promocion-y-titulacion-en-educacion)

## Evaluación acceso universidad

Ver [LOMLOE acceso universidad](/home/legislacion-educativa/lomloe/acceso-universidad)  

## Estatal
### Trámite de audiencia   
[Proyecto de real decreto por el que se regula la evaluación, promoción y titulación en Primaria, ESO, Bach y FP](https://www.educacionyfp.gob.es/servicios-al-ciudadano/informacion-publica/audiencia-informacion-publica/cerrados/2021/pdr-evaluacion-promocion.html)  

[PDF Proyecto de real decreto por el que se regulan la evaluación y la promoción en la Educación Primaria, así como la evaluación, la promoción y la titulación en la Educación Secundaria Obligatoria, el Bachillerato y la Formación Profesional, hasta la implantación de las modificaciones introducidas por la Ley Orgánica 3/2020, de 29 de diciembre, por la que se modifica la Ley Orgánica 2/2006, de 3 de mayo de Educación, en el currículo, la organización y objetivos y programas de cada etapa.](https://www.educacionyfp.gob.es/dam/jcr:f7d52fad-bb06-4117-b4d1-3ce8e4111376/1-texto-prd-29-06-21.pdf)  
Fecha de inicio de aportaciones: 2 de julio de 2021   
Fecha de finalización de aportaciones: 23 de julio de 2021   
Remisión de aportaciones a: tramitacion.sgoa@educacion.gob.es  

### 10 septiembre 2021  

[El decreto de evaluación eliminará los exámenes de recuperación en la ESO - elpais.es](https://elpais.com/educacion/2021-09-10/el-decreto-de-evaluacion-eliminara-los-examenes-de-recuperacion-en-la-eso.html)  
La nueva norma, en fase de borrador, cambia el sistema para pasar de curso, en el que la repetición será excepcional. La convocatoria extraordinaria se mantendrá en Bachillerato  

[El Gobierno elimina los exámenes de recuperación en la ESO y se podrá pasar de curso directamente - elmundo.es](https://www.elmundo.es/espana/2021/09/10/613b8140e4d4d86f038b456f.html)  
El Ministerio quiere que las medidas se tomen antes de que el alumno suspenda y que la mala nota no sea criterio para repetir ni graduarse  

Dado que iniciado el curso no está en BOE, su entrada en vigor supone adaptar las programaciones realizadas antes  

### 27 septiembre 2021

[Los exámenes de recuperación de la ESO podrán mantenerse este curso en las comunidades autónomas que así lo quieran - elpais.com](https://elpais.com/educacion/2021-09-27/los-examenes-de-recuperacion-de-la-eso-podran-mantenerse-este-curso-en-las-comunidades-autonomas-que-asi-lo-quieran.html)  
Educación acepta la petición de algunos territorios gobernados por el PP de aplazar su desaparición  

### 8 octubre 2021

[Los padres podrán revisar los exámenes de sus hijos con el currículum que prepara el Gobierno - lavanguardia.com](https://www.lavanguardia.com/vida/20211008/7778464/padres-podran-revisar-examenes-hijos.html)  
El borrador de decreto que deben revisar las comunidades autónomas se refuerzan los aprendizajes competenciales, se eliminan las recuperaciones y se permite pasar de cursos sin límite de suspensos  

Por ver texto, los exámenes ya se podían consultar  
30 enero 2019  
[twitter FiQuiPedia/status/1090723521515438080](https://twitter.com/FiQuiPedia/status/1090723521515438080)  
Los padres pueden pedir copia; la FAPA Giner de los Ríos hizo un informe jurídico en 2016 que enlazo. La normativa de Madrid lo contempla explícitamente para los finales en art 42.6 de ORDEN 2398/2016  

[Informe jurídico de la FAPA sobre el derecho a obtener copia de los exámenes y otros documentos que formen parte del expediente académico de un/a alumno/a. Junio 2016 - fapaginerdelosrios.org](https://www.fapaginerdelosrios.org/documentos?EntryId=1344&Command=Core_Download)  

### 11 octubre 2021
[Igualdad y Educación afectiva y sexual se trabajarán en todas las materias de la ESO - magisnet.com](https://www.magisnet.com/2021/10/igualdad-y-educacion-afectiva-y-sexual-se-trabajaran-en-todas-las-materias-de-la-eso/)  

[DOCUMENTO I: Proyecto de Real Decreto de Ordenación y enseñanzas mínimas de la ESO (PDF)](https://www.magisnet.com/wp-content/uploads/2021/10/21.10.06_RD_ESO.pdf)  

> Artículo 29. Participación y derecho a la información de madres, padres o tutores legales.   
Cuando el alumnado sea menor de edad, las madres, padres o tutores legales deberán 
participar y apoyar la evolución de su proceso educativo, colaborando en las medidas 
de  apoyo  o  refuerzo  que  adopten  los  centros  para  facilitar  su  progreso.  Tendrán, 
además, derecho a conocer las decisiones relativas a su **evaluación** y promoción, así 
como  al  acceso  a  los  documentos  oficiales  de  evaluación  y  a  los  exámenes  y 
documentos de las evaluaciones que se realicen a sus hijos o tutelados, sin perjuicio del 
respeto a las garantías establecidas en la Ley Orgánica 3/2018, de 5 de diciembre, de 
Protección de Datos Personales y garantía de los derechos digitales, y demás normativa 
aplicable en materia de protección de datos de carácter personal.  

### 25 octubre 2021  
[Los alumnos con necesidades especiales podrán tener el título de la ESO aunque no superen los mínimos exigidos en la etapa - elmundo.es](https://www.elmundo.es/espana/2021/10/25/61729e93fdddff95a08b462e.html)  

Lo cito al hablar de [currículo LOMLOE](/home/legislacion-educativa/lomloe/curriculo) porque combina evaluación y currículo

### 28 octubre 2021
Se filtran borradores Bachillerato y citan aspectos asociados a evaluación

[El título de Bachillerato podrá obtenerse con un suspenso y hacerlo en tres años - magisnet.com](https://www.magisnet.com/2021/10/el-titulo-de-bachillerato-podra-obtenerse-con-un-suspenso-y-hacerlo-en-tres-anos/)  

[DOCUMENTO I: Proyecto de Real Decreto de Bachillerato (PDF)](https://www.magisnet.com/wp-content/uploads/2021/10/21.10.26-Proyecto-RD-Bachillerato-1.pdf)  

[DOCUMENTO II: Anexos Bachillerato (PDF)](https://www.magisnet.com/wp-content/uploads/2021/10/21.10.26-Anexos-Bachillerato.pdf)  

[Esto es todo lo que se va a estudiar en bachillerato: así son las 42 asignaturas y el reparto de horarios diseñado por el Gobierno - elpais.com](https://elpais.com/educacion/2021-10-28/estos-son-todos-los-cambios-en-el-bachillerato-que-ha-disenado-el-gobierno-y-su-nuevo-horario.html)  

[El borrador del nuevo decreto de Bachillerato contempla que se pueda hacer la Selectividad con una asignatura suspensa - abc.es](https://www.abc.es/sociedad/abci-borrador-nuevo-decreto-bachillerato-permitira-hacer-selectividad-asignatura-suspensa-202110280856_noticia.html)  

### 29 octubre 2021
Se publica documentación oficial por ministerio, incluyendo borradores.

[twitter educaciongob/status/1454055665870147589](https://twitter.com/educaciongob/status/1454055665870147589)  
Consulta en nuestro portal #educagob los primeros documentos de trabajo de los currículos de Infantil, Primaria, ESO y Bachillerato.

[CURRÍCULO > Debate público sobre el currículo > Documentación del Ministerio para el debate público](https://educagob.educacionyfp.gob.es/curriculo/debate-curriculo/documentacion-debate.html)  

### 12 noviembre 2021  

[Educación eliminará este año los exámenes de recuperación de la ESO - publico.es](https://www.publico.es/politica/educacion-eliminara-este-ano-examenes-recuperacion.html)  
Un dictamen del Consejo Escolar de Estado obliga al Ministerio de Educación a eliminar la moratoria que iba a permitir que algunas comunidades mantuvieran las recuperaciones este curso.  
> "La recomendación especial" del Consejo Escolar del Estado es de obligado cumplimiento y exige, según las fuentes, eliminar el apartado en el que se preveía aplazar el fin de las repeticiones. La argumentación se basa en que no puede haber diferentes criterios en las comunidades autónomas.  

[Educación eliminará este mismo año los exámenes de recuperación de secundaria - elpais.com](https://elpais.com/sociedad/2021-11-12/educacion-eliminara-este-mismo-ano-los-examenes-de-recuperacion-de-secundaria.html)  
> La decisión está ligada al dictamen emitido este viernes por el Consejo Escolar del Estado que establece que no puede haber diferentes criterios sobre este tema en función del territorio autonómico  

### 16 noviembre 2021
[Referencia del Consejo de Ministros, Madrid, martes 16 de noviembre de 2021](https://www.lamoncloa.gob.es/consejodeministros/referencias/Paginas/2021/refc20211116.aspx)  
[Normativa Evaluación](https://educagob.educacionyfp.gob.es/normativa/evaluacion.html) ya lo cita  
[Real Decreto por el que se regulan la evaluación y la promoción en la Educación Primaria, así como la evaluación, la promoción y la titulación en la Educación Secundaria Obligatoria, el Bachillerato y la Formación Profesional.](https://educagob.educacionyfp.gob.es/dam/jcr:90e60ce2-1a2f-46a9-96e0-3c1d33770490/proyecto-rd-evaluacion.pdf)  

### 17 noviembre 2021
[Real Decreto 984/2021, de 16 de noviembre, por el que se regulan la evaluación y la promoción en la Educación Primaria, así como la evaluación, la promoción y la titulación en la Educación Secundaria Obligatoria, el Bachillerato y la Formación Profesional.- boe.es](https://boe.es/buscar/act.php?id=BOE-A-2021-18812)  

**Este Real Decreto 984/2021 se deroga en 2022 por los Reales Decretos de currículo, aunque siguió aplicando transitoriamente**  

Normativa básica por disposición final primera  

Se elimina evaluación extraordinaria en artículo 10.5   
> 5. Con independencia del seguimiento realizado a lo largo del curso, el equipo docente llevará a cabo la evaluación del alumnado de forma colegiada en una **única sesión que tendrá lugar al finalizar el curso escolar**.

En artículo 18 se fija que en FP Básica ya siempre se obtiene el título de ESO, ya no depende del equipo docente
Ver [ESO parece pero ESO no es](https://algoquedaquedecir.blogspot.com/2018/04/eso-parece-pero-eso-no-es.html)  
> Artículo 18. Obtención del título de Graduado en Educación Secundaria Obligatoria.  
>La superación de la totalidad de los módulos incluidos en un ciclo de Formación Profesional Básica conducirá a la obtención del título de Graduado en Educación Secundaria Obligatoria.  

### Marzo 2022
Salen los RD de currículo de infantil y primaria.  
Hay que tener en cuenta que modifican evaluación, modifican RD 984/2021  

[Real Decreto 157/2022, de 1 de marzo, por el que se establecen la ordenación y las enseñanzas mínimas de la Educación Primaria. Disposición derogatoria única. Derogación normativa.](https://boe.es/buscar/act.php?id=BOE-A-2022-3296#dd)  

> 2. Asimismo, queda derogado el capítulo II del Real Decreto 984/2021, de 16 de noviembre por el que se regulan la evaluación y la promoción en la Educación Primaria, así como la evaluación, la promoción y la titulación en la Educación Secundaria Obligatoria, el Bachillerato y la Formación Profesional

[Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria. Disposición derogatoria única. Derogación normativa.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#dd)  

> 2. Asimismo, queda derogado el Real Decreto 984/2021, de 16 de noviembre, por el que se regulan la evaluación y la promoción en la Educación Primaria, así como la evaluación, la promoción y la titulación en la Educación Secundaria Obligatoria, el Bachillerato y la Formación Profesional, en todo lo que se refiera a la Educación Secundaria Obligatoria y, en particular, su capítulo III y los artículos 24 y 26.1.

### Abril 2022 
Con [Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521) se deroga completamente Real Decreto 984/2021  

> 3. Asimismo, queda derogado el Real Decreto 984/2021, de 16 de noviembre, por el que se regulan la evaluación y la promoción en la Educación Primaria, así como la evaluación, la promoción y la titulación en la Educación Secundaria Obligatoria, el Bachillerato y la Formación Profesional  

## Madrid

### 24 noviembre 2021

[Resolución de la Viceconsejería de Política Educativa por la que se dictan instrucciones sobre la evaluación, la promoción y la titulación en la Educación Secundaria Obligatoria, el Bachillerato y la Formación Profesional, así como en las enseñanzas de personas adultas que conduzcan a la obtención de los títulos de Graduado en Educación Secundaria Obligatoria y de Bachiller.](https://www.educa2.madrid.org/web/educamadrid/principal/files/a26dbcc4-9647-44f8-9acc-27a63defd753/RESOLUCION%20EVALUACION%20PROMOCION%20Y%20TITULACION%20VPE.pdf?t=1637744367915)  
CSV 1259076490382908969308  

### 25 marzo 2022
[Resolución Conjunta de las Viceconsejerías de Política Educativa y de Organización Educativa, de 25 de marzo de 2022, por la que se dictan Instrucciones sobre la participación en el proceso de admisión de alumnos en centros docente sostenidos con fondos públicos que imparten segundo ciclo de Educación Infantil, Educación Primaria, Educación Especial, Educación Secundaria Obligatoria y Bachillerato en la Comunidad de Madrid para el curso 2022/2023.](https://www.comunidad.madrid/sites/default/files/doc/educacion/resolucion_admision_25-03_2022-2023.pdf)  

### 20 mayo 2022
[DECRETO 29/2022, de 18 de mayo, del Consejo de Gobierno, por el que se regulan determinados aspectos sobre la evaluación, la promoción y la titulación en la Educación Secundaria Obligatoria, el Bachillerato y la Formación Profesional, así como en las enseñanzas de personas adultas que conduzcan a la obtención de los títulos de Graduado en Educación Secundaria Obligatoria y de Bachiller.](https://bocm.es/boletin/CM_Orden_BOCM/2022/05/20/BOCM-20220520-1.PDF)  

### 31 mayo 2023

[ORDEN 1712/2023, de 19 de mayo, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en la Educación Secundaria Obligatoria.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/05/31/BOCM-20230531-17.PDF)  

### 23 novimbre 2023 

[Resolución de la Viceconsejería de Política Educativa por la que se dictan instrucciones sobre la evaluación, la promoción y la titulación en la Educación Secundaria Obligatoria, el Bachillerato y la Formación Profesional, así como en las enseñanzas de personas adultas que conduzcan a la obtención de los títulos de Graduado en Educación Secundaria Obligatoria y de Bachiller.](https://www.educa2.madrid.org/web/educamadrid/principal/files/a26dbcc4-9647-44f8-9acc-27a63defd753/RESOLUCION%20EVALUACION%20PROMOCION%20Y%20TITULACION%20VPE.pdf?t=1637744367915)

### 21 junio 2023

[ORDEN 2067/2023, de 11 de junio, de la Vicepresidencia, Consejería de Educación y Universidades, por la que se regulan determinados aspectos de organización, funcionamiento y evaluación en el Bachillerato.](https://www.bocm.es/boletin/CM_Orden_BOCM/2023/06/21/BOCM-20230621-14.PDF)  

### 19 diciembre 2023

[STSJ Madrid, a 19 de diciembre de 2023 - ROJ: STSJ M 14107/2023 ECLI:ES:TSJM:2023:14107 Nº de Resolución: 853/2023](https://www.poderjudicial.es/search/AN/openDocument/30c265bc2f4afceda0a8778d75e36f0d/20240116)  

## Baleares

[Educación elabora un material dirigido a familias sobre cómo serán las nuevas evaluaciones Lomloe](https://www.europapress.es/illes-balears/noticia-educacion-elabora-material-dirigido-familias-seran-nuevas-evaluaciones-lomloe-20220914110349.html)  
> La Conselleria de Educación y Formación Profesional ha elaborado un material para informar las familias sobre cómo serán las nuevas evaluaciones LOMLOE en los cursos impares de primaria, ESO y Bachillerato a partir de este curso 2022/23.  

## Evaluación y calificación
Enlaza con competencias específicas y ponderación  
[twitter ubibene/status/1588210218000842753](https://twitter.com/ubibene/status/1588210218000842753)  
Esto no puede ser verdad.  #EvaluaciónLomLoe  
![](https://pbs.twimg.com/media/Fgp0xMZXEAE-Jw9?format=jpg)  

El canal parece ser este [Emisiones en directo de la Consejería de Educación y Empleo](https://youtube.com/c/EmisionesendirectoCEYE) pero cuando escribo esto no localizo el vídeo de la captura  





