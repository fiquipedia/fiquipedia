# Currículos Física y Química ESO 

Esta página pretende centralizar enlaces a currículos de Física y Química en ESO

## LOE 
Física y Química se trataba en 3º y 4º ESO. 
* [Currículo Física y Química 3º ESO (LOE)](/home/materias/eso/fisica-y-quimica-3-eso/curriculofisicayquimica-3eso) 
* [Currículo Física y Química 4º ESO (LOE)](/home/materias/eso/fisicayquimica-4eso/curriculofisicayquimica-4eso)  
Había ciertos contenidos asociados en 1º y 2º ESO, dentro de [Ciencias de la Naturaleza](/home/materias/eso/ciencias-de-la-naturaleza-1y2eso)   

## LOMCE
Se introduce Física y Química en 2º ESO además de mantenerse en 3º y 4º ESO.  
* [Currículo Física y Química 2º ESO (LOMCE)](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso)  
Los criterios de evaluación y estándares de aprendizaje evaluables de manera conjunta para 2º y 3º ESO, varían los contenidos en 2º ESO  
* [Currículo Física y Química 3º ESO (LOMCE)](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomce)  
* [Currículo Física y Química 4º ESO (LOMCE)](/home/materias/eso/fisicayquimica-4eso/curriculo-fisica-y-quimica-4-eso-lomce)  

## LOMLOE 

Aquí se comenta por separado lo general a nivel estatal con Real Decreto y a nivel Comunidad de Madrid con Decreto
Los detalles para cada curso, incluyendo diferencias a nivel estatal y anivel de Comunidad de Madrid en páginas separadas  
*  [Currículo Física y Química 2º ESO (LOMLOE)](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso-lomloe)  
*  [Currículo Física y Química 3º ESO (LOMLOE)](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomloe)  
*  [Currículo Física y Química 4º ESO (LOMLOE)](/home/materias/eso/fisicayquimica-4eso/curriculo-fisica-y-quimica-4-eso-lomloe)  

### LOMLOE (Real Decreto)
A nivel estatal revisado con [Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://boe.es/buscar/act.php?id=BOE-A-2022-4975) citado en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  

Se trata toda la ESO conjuntamente en el currículo, separando luego saberes básicos en dos partes: 
* 1º, 2º y 3º ESO  
    * [Currículo Física y Química 3º ESO (LOMLOE)](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomloe)  
* 4º ESO [Currículo Física y Química 4º ESO (LOMLOE)](/home/materias/eso/fisicayquimica-4eso/curriculo-fisica-y-quimica-4-eso-lomloe)  

Se incluye aquí la parte general de LOMLOE para toda la ESO: introducción y competencias específicas  

Cambios en trámite audiencia noviembre 2021 respecto a borrador octubre:  
Se añade "promoviendo acciones y conductas que provoquen cambios hacia un mundo más justo e igualitario."  
Se elimina "; ejemplificados ambos a través de las distintas situaciones de aprendizaje que se proponen."  
Se añade "Se incide en el papel destacado de las mujeres a lo largo de la historia de la Ciencia como forma de ponerlo en valor y fomentar nuevas vocaciones femeninas  hacia  el  campo  de  las  ciencias  experimentales y la tecnología."
Se elimina "cinemática" citada justo antes de astronomía.   
Se elimina "Por ello, en el anexo a este documento se propone, a modo de ejemplo, una forma de combinar los saberes básicos en situaciones de aprendizaje contextualizadas."  
Se cambia "en la adolescencia" por "durante todas las etapas del desarrollo del alumnado"  
En Competencia específica 1 se añade "y relacionar" y "aplicarlas para"  

#### Física y Química ESO (Real Decreto)

La formación integral del alumnado requiere de una alfabetización científica en la etapa de la Educación Secundaria como continuidad a los aprendizajes relacionados con las ciencias de la naturaleza en Educación Primaria, pero con un nivel de profundización mayor en las diferentes áreas de conocimiento de la ciencia. En esta alfabetización científica, la materia de Física y Química contribuye a que el alumnado comprenda el funcionamiento del universo y las leyes que lo gobiernan, y proporciona los conocimientos, destrezas y actitudes de la ciencia que le permiten desenvolverse con criterio fundamentado en un mundo en continuo desarrollo científico, tecnológico, económico y social, promoviendo acciones y conductas que provoquen cambios hacia un mundo más justo e igualitario.

El currículo de la materia de Física y Química contribuye al desarrollo de las competencias clave y de los objetivos de etapa. Para ello, los descriptores de las distintas competencias clave reflejadas en el Perfil de salida del alumnado al término de la enseñanza básica y los objetivos de etapa se concretan en las competencias específicas de la materia de Física y Química. Estas competencias específicas justifican el resto de los elementos del currículo de la materia y contribuyen a que el alumnado sea capaz de desarrollar el pensamiento científico para enfrentarse a los posibles problemas de la sociedad que lo rodea y disfrutar de un conocimiento más profundo del mundo.

La evaluación de las competencias específicas se realiza teniendo en cuenta los criterios de evaluación, que están enfocados en el desempeño de los conocimientos, destrezas y actitudes asociados al pensamiento científico competencial.

Los saberes básicos de esta materia contemplan conocimientos, destrezas y actitudes que se encuentran estructurados en los que tradicionalmente han sido los grandes bloques de conocimiento de la Física y la Química: «La materia», «La energía», «La interacción» y «El cambio». Además, este currículo propone la existencia de un bloque de saberes básicos comunes que hace referencia a las metodologías de la ciencia y a su importancia en el desarrollo de estas áreas de conocimiento. En este bloque, denominado «Las destrezas científicas básicas», se establece además la relación de las ciencias experimentales con una de sus herramientas más potentes, las matemáticas, que ofrecen un lenguaje de comunicación formal y que incluyen los conocimientos, destrezas y actitudes previos del alumnado y los que se adquieren a lo largo de esta etapa educativa. Se incide aquí en el papel destacado de las mujeres a lo largo de la historia de la ciencia como forma de ponerlo en valor y fomentar nuevas vocaciones femeninas hacia el campo de las ciencias experimentales y la tecnología.

El bloque de «La materia» engloba los saberes básicos sobre la constitución interna de las sustancias, lo que incluye la descripción de la estructura de los elementos y de los compuestos químicos y las propiedades macroscópicas y microscópicas de la materia como base para profundizar en estos contenidos en cursos posteriores.

Con el bloque «La energía» el alumnado profundiza en los conocimientos, destrezas y actitudes que adquirió en la Educación Primaria, como las fuentes de energía y sus usos prácticos o los aspectos básicos acerca de las formas de energía. Se incluyen, además, saberes relacionados con el desarrollo social y económico del mundo real y sus implicaciones medioambientales.

«La interacción» contiene los saberes acerca de los efectos principales de las interacciones fundamentales de la naturaleza y el estudio básico de las principales fuerzas del mundo natural, así como sus aplicaciones prácticas en campos tales como la astronomía, el deporte, la ingeniería, la arquitectura o el diseño.

Por último, el bloque denominado «El cambio» aborda las principales transformaciones físicas y químicas de los sistemas materiales y naturales, así como los ejemplos más frecuentes del entorno y sus aplicaciones y contribuciones a la creación de un mundo mejor.

Todos los elementos curriculares están relacionados entre sí formando un todo que dota al currículo de esta materia de un sentido integrado y holístico. Así, la materia de Física y Química se plantea a partir del uso de las metodologías propias de la ciencia, abordadas a través del trabajo cooperativo y la colaboración interdisciplinar y su relación con el desarrollo socioeconómico, y enfocadas a la formación de alumnos y alumnas competentes y comprometidos con los retos del siglo XXI y los Objetivos de Desarrollo Sostenible. En este sentido, las situaciones de aprendizaje que se planteen para la materia deben partir de un enfoque constructivo, crítico y emprendedor.

La construcción de la ciencia y el desarrollo del pensamiento científico durante todas las etapas del desarrollo del alumnado parten del planteamiento de cuestiones científicas basadas en la observación directa o indirecta del mundo en situaciones y contextos habituales, en su intento de explicación a partir del conocimiento, de la búsqueda de evidencias y de la indagación y en la correcta interpretación de la información que a diario llega al público en diferentes formatos y a partir de diferentes fuentes. Por eso, el enfoque que se le dé a esta materia a lo largo de esta etapa educativa debe incluir un tratamiento experimental y práctico que amplíe la experiencia del alumnado más allá de lo académico y le permita hacer conexiones con sus situaciones cotidianas, lo que contribuirá de forma significativa a que desarrolle las destrezas características de la ciencia. De esta manera se pretende potenciar la creación de vocaciones científicas para conseguir que haya un número mayor de estudiantes que opten por continuar su formación en itinerarios científicos en las etapas educativas posteriores y proporcionar, a su vez, una completa base científica para aquellos estudiantes que deseen cursar itinerarios no científicos.

##### Competencias Específicas (Real Decreto)

**1. Comprender y relacionar los motivos por los que ocurren los principales fenómenos fisicoquímicos del entorno, explicándolos en términos de las leyes y teorías científicas adecuadas, para resolver problemas con el fin de aplicarlas para mejorar la realidad cercana y la calidad de vida humana.**

La esencia del pensamiento científico es comprender cuáles son los porqués de los fenómenos que ocurren en el medio natural para tratar de explicarlos a través de las leyes físicas y químicas adecuadas. Comprenderlos implica entender las causas que los originan y su naturaleza, permitiendo al alumnado actuar con sentido crítico para mejorar, en la medida de lo posible, la realidad cercana a través de la ciencia.

El desarrollo de esta competencia específica conlleva hacerse preguntas para comprender cómo es la naturaleza del entorno, cuáles son las interacciones que se producen entre los distintos sistemas materiales y cuáles son las causas y las consecuencias de las mismas. Esta comprensión dota al alumnado de fundamentos críticos en la toma de decisiones, activa los procesos de resolución de problemas y, a su vez, posibilita la creación de nuevo conocimiento científico a través de la interpretación de fenómenos, el uso de herramientas científicas y el análisis de los resultados que se obtienen. Todos estos procesos están relacionados con el resto de competencias específicas y se engloban en el desarrollo del pensamiento científico, cuestión especialmente importante en la formación integral de personas competentes. Por tanto, para el desarrollo de esta competencia, el individuo requiere un conocimiento de las formas y procedimientos estándar que se utilizan en la investigación científica y su relación con el mundo natural.

Esta competencia específica se conecta con los siguientes descriptores del Perfil de salida: CCL1, STEM1, STEM2, STEM4, CPSAA4.

**2. Expresar las observaciones realizadas por el alumnado en forma de preguntas, formulando hipótesis para explicarlas y demostrando dichas hipótesis a través de la experimentación científica, la indagación y la búsqueda de evidencias, para desarrollar los razonamientos propios del pensamiento científico y mejorar las destrezas en el uso de las metodologías científicas.**

Una característica inherente a la ciencia y al desarrollo del pensamiento científico en la adolescencia es la curiosidad por conocer y describir los fenómenos naturales. Dotar al alumnado de competencias científicas implica trabajar con las metodologías propias de la ciencia y reconocer su importancia en la sociedad. El alumnado que desarrolla esta competencia debe observar, formular hipótesis y aplicar la experimentación, la indagación y la búsqueda de evidencias para comprobarlas y predecir posibles cambios.

Utilizar el bagaje propio de los conocimientos que el alumnado adquiere a medida que progresa en su formación básica y contar con una completa colección de recursos científicos, tales como las técnicas de laboratorio o de tratamiento y selección de la información, suponen un apoyo fundamental para la mejora de esta competencia. El alumnado que desarrolla esta competencia emplea los mecanismos del pensamiento científico para interaccionar con la realidad cotidiana y analizar, razonada y críticamente, la información que proviene de las observaciones de su entorno, o que recibe por cualquier otro medio, y expresarla y argumentarla en términos científicos.

Esta competencia específica se conecta con los siguientes descriptores del Perfil de salida: CCL1, CCL3, STEM1, STEM2, CD1, CPSAA4, CE1, CCEC3.

**3. Manejar con soltura las reglas y normas básicas de la física y la química en lo referente al lenguaje de la IUPAC, al lenguaje matemático, al empleo de unidades de medida correctas, al uso seguro del laboratorio y a la interpretación y producción de datos e información en diferentes formatos y fuentes, para reconocer el carácter universal y transversal del lenguaje científico y la necesidad de una comunicación fiable en investigación y ciencia entre diferentes países y culturas.**

La interpretación y la transmisión de información con corrección juegan un papel muy importante en la construcción del pensamiento científico, pues otorgan al alumnado la capacidad de comunicarse en el lenguaje universal de la ciencia, más allá de las fronteras geográficas y culturales del mundo. Con el desarrollo de esta competencia se pretende que el alumnado se familiarice con los flujos de información multidireccionales característicos de las disciplinas científicas y con las normas que toda la comunidad científica reconoce como universales para establecer comunicaciones efectivas englobadas en un entorno que asegure la salud y el desarrollo medioambiental sostenible. Entre los distintos formatos y fuentes, el alumnado debe ser capaz de interpretar y producir datos en forma de textos, enunciados, tablas, gráficas, informes, manuales, diagramas, fórmulas, esquemas, modelos, símbolos, etc. Además, esta competencia requiere que el alumnado evalúe la calidad de los datos, así como que reconozca la importancia de la investigación previa a un estudio científico.

Con esta competencia específica se desea fomentar la adquisición de conocimientos, destrezas y actitudes relacionadas con el carácter interdisciplinar de la ciencia, la aplicación de normas, la interrelación de variables, la argumentación, la valoración de la importancia de utilizar un lenguaje universal, la valoración de la diversidad, el respeto hacia las normas y acuerdos establecidos, hacia uno mismo, hacia los demás y hacia el medio ambiente, etc., que son fundamentales en los ámbitos científicos por formar parte de un entorno social y comunitario más amplio.

Esta competencia específica se conecta con los siguientes descriptores del Perfil de salida: STEM4, STEM5, CD3, CPSAA2, CC1, CCEC2, CCEC4.

**4. Utilizar de forma crítica, eficiente y segura plataformas digitales y recursos variados, tanto para el trabajo individual como en equipo, para fomentar la creatividad, el desarrollo personal y el aprendizaje individual y social, mediante la consulta de información, la creación de materiales y la comunicación efectiva en los diferentes entornos de aprendizaje.**

Los recursos, tanto tradicionales como digitales, adquieren un papel crucial en el proceso de enseñanza y aprendizaje en general, y en la adquisición de competencias en particular, pues un recurso bien seleccionado facilita el desarrollo de procesos cognitivos de nivel superior y propicia la comprensión, la creatividad y el desarrollo personal y social del alumnado. La importancia de los recursos, no solo utilizados para la consulta de información sino también para otros fines como la creación de materiales didácticos o la comunicación efectiva con otros miembros de su entorno de aprendizaje, dota al alumnado de herramientas para adaptarse a una sociedad que actualmente demanda personas integradas y comprometidas con su entorno.

Es por este motivo por lo que esta competencia específica también pretende que el alumno o alumna maneje con soltura recursos y técnicas variadas de colaboración y cooperación, que analice su entorno y localice en él ciertas necesidades que le permitan idear, diseñar y fabricar productos que ofrezcan un valor para uno mismo y para los demás.

Esta competencia específica se conecta con los siguientes descriptores del Perfil de salida: CCL2, CCL3, STEM4, CD1, CD2, CPSAA3, CE3, CCEC4.

**5. Utilizar las estrategias propias del trabajo colaborativo, potenciando el crecimiento entre iguales como base emprendedora de una comunidad científica crítica, ética y eficiente, para comprender la importancia de la ciencia en la mejora de la sociedad, las aplicaciones y repercusiones de los avances científicos, la preservación de la salud y la conservación sostenible del medio ambiente.**

Las disciplinas científicas se caracterizan por conformar un todo de saberes integrados e interrelacionados entre sí. Del mismo modo, las personas dedicadas a la ciencia desarrollan destrezas de trabajo en equipo, pues la colaboración, la empatía, la asertividad, la garantía de la equidad entre mujeres y hombres y la cooperación son la base de la construcción del conocimiento científico en toda sociedad. El alumnado competente estará habituado a las formas de trabajo y a las técnicas más habituales del conjunto de las disciplinas científicas, pues esa es la forma de conseguir, a través del emprendimiento, integrarse en una sociedad que evoluciona. El trabajo en equipo sirve para unir puntos de vista diferentes y crear modelos de investigación unificados que forman parte del progreso de la ciencia.

El desarrollo de esta competencia específica crea un vínculo de compromiso entre el alumno o alumna y su equipo, así como con el entorno que los rodea, lo que le habilita para entender cuáles son las situaciones y los problemas más importantes de la sociedad actual y cómo mejorarla, cómo actuar para la mejora de la salud propia y comunitaria y cuáles son los estilos de vida que le permiten actuar de forma sostenible para la conservación del medio ambiente desde un punto de vista científico y tecnológico.

Esta competencia específica se conecta con los siguientes descriptores del Perfil de salida: CCL5, CP3, STEM3, STEM5, CD3, CPSAA3, CC3, CE2.

**6. Comprender y valorar la ciencia como una construcción colectiva en continuo cambio y evolución, en la que no solo participan las personas dedicadas a ella, sino que también requiere de una interacción con el resto de la sociedad, para obtener resultados que repercutan en el avance tecnológico, económico, ambiental y social.**

Para completar el desarrollo competencial de la materia de Física y Química, el alumno o alumna debe asumir que la ciencia no es un proceso finalizado, sino que está en una continua construcción recíproca con la tecnología y la sociedad. La búsqueda de nuevas explicaciones, la mejora de procedimientos, los nuevos descubrimientos científicos, etc. influyen sobre la sociedad, y conocer de forma global los impactos que la ciencia produce sobre ella es fundamental en la elección del camino correcto para el desarrollo. En esta línea, el alumnado competente debe tener en cuenta valores como la importancia de los avances científicos por y para una sociedad demandante, los límites de la ciencia, las cuestiones éticas y la confianza en los científicos y en su actividad.

Todo esto forma parte de una conciencia social en la que no solo interviene la comunidad científica, sino que requiere de la participación de toda la sociedad puesto que implica un avance individual y social conjunto.

Esta competencia específica se conecta con los siguientes descriptores del Perfil de salida: STEM2, STEM5, CD4, CPSAA1, CPSAA4, CC4, CCEC1.

### LOMLOE Madrid (Decreto)

Revisado con [Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-2.PDF)  
Física y Química en página 103 del pdf  

[Borrador Currículo ESO Madrid 19 abril 2022](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/legislacion-educativa/lomloe/2022-04-19-Madrid-ESO-Curr%C3%ADculo-Borrador.pdf)  
Cambia respecto a Real Decreto estatal, se marcan diferencias añadidas en negrita (en borrador 19 abril 2022 hay algunas indicaciones en rojo aunque algunas ya estaban en RD). Si hay alguna eliminación o cambio relevante se intenta comentar como texto.

[Proyecto de decreto por el que se establece para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria. 23 mayo](https://www.comunidad.madrid/transparencia/sites/default/files/2022-05-23_decreto_eso-completo.pdf)  
Como en borrador indica diferencias en rojo. Aquí en negrita si son añadidos.  
Se eliminan algunas cosas: por ejemplo en abril se añadía ", estableciendo de este modo también, una relación con contenidos trasversales de la etapa como la sostenibilidad, la economía circular y el consumo responsable" y se elimina en mayo además de eliminar ", promoviendo acciones y conductas que provoquen cambios hacia un mundo más justo e igualitario" 
Se elimina " que permita una participación plena en la sociedad"  
Se elimina texto que se había retocado " Se **incluye** aquí el papel destacado de las mujeres a lo largo de la historia de la ciencia como forma de poner **su aportación** en valor y fomentar nuevas vocaciones femeninas hacia el campo de las ciencias experimentales y la tecnología."  
En bloque energía se elimina "Se incluyen, además, contenidos relacionados con el desarrollo social y económico del mundo real y sus implicaciones medioambientales."  
Se elimina ", abordadas a través del trabajo cooperativo interdisciplinar, y relacionadas con el desarrollo socioeconómico."  
Se elimina párrafo entero añadido en abril "**De este modo, en la materia de Física y Química se pueden plantear situaciones de aprendizaje con un enfoque constructivo, crítico y emprendedor, y enfocadas a la formación de un alumnado competente y comprometido con los retos del siglo XXI y los Objetivos de Desarrollo Sostenible.**"  
Se elimina "que permitan relacionar el consumo que hacemos de la energía con la sostenibilidad y el cuidado del medio ambiente."  
Se cambia "Esta situación de aprendizaje" por "Esta actividad"   

En competencias específicas se elimina "la valoración de la diversidad" "hacia uno mismo, hacia los demás y hacia el medio ambiente, etc., que son fundamentales en los ámbitos científicos por formar parte de un entorno social y comunitario más amplio."  
Se elimina "para uno mismo y para los demás"   
Se cambia "colaborativo, potenciando el crecimiento entre iguales" por "en grupo"  
Se elimina " pues la colaboración, la empatía, la asertividad, la garantía de la equidad entre mujeres y hombres y la cooperación son la base de la construcción del conocimiento científico en toda sociedad"  
Se elimina ", cómo actuar para la mejora de la salud propia y comunitaria y cuáles son los estilos de vida que le permiten actuar de forma sostenible para la conservación del medio ambiente desde un punto de vista científico y tecnológico."  
Se cambia "tecnológico, económico, ambiental y social." por "en distintos ámbitos"  

Madrid no usa término "saberes básicos" sino contenidos o conocimientos.  

En Madrid los Criterios de Evalación con Competencias Específicas y los Contenidos se separan por curso: 
* [Currículo Física y Química 2º ESO (LOMLOE)](/home/materias/eso/fisica-y-quimica-2-eso/curriculo-fisica-y-quimica-2-eso-lomloe)  
* [Currículo Física y Química 3º ESO (LOMLOE)](/home/materias/eso/fisica-y-quimica-3-eso/curriculo-3-eso-fisica-y-quimica-lomloe)  
* [Currículo Física y Química 4º ESO (LOMLOE)](/home/materias/eso/fisicayquimica-4eso/curriculo-fisica-y-quimica-4-eso-lomloe)  

#### Física y Química ESO LOMLOE Madrid 

Comparación proyecto Decreto Madrid frente a Real Decreto:  
Se reordena texto para que bloque cambio esté antes que energía.  
Se añaden párrafos al final citando situaciones de aprendizaje.   

Cambios en [Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-2.PDF) vs proyecto  
Se elimina esta frase "Se **incluye** aquí el papel destacado de las mujeres a lo largo de la historia de la ciencia como forma de poner **su aportación** en valor y fomentar nuevas vocaciones femeninas hacia el campo de las ciencias experimentales y la tecnología."  


La formación integral del alumnado requiere de una alfabetización científica en la etapa de la Educación Secundaria como continuidad a los aprendizajes relacionados con las ciencias de la naturaleza en Educación Primaria, pero con un nivel de profundización mayor en las diferentes áreas de conocimiento de la ciencia. En esta alfabetización científica, la materia de Física y Química contribuye a que el alumnado comprenda el funcionamiento del universo y **de** las leyes que lo gobiernan, y proporciona los conocimientos, destrezas y actitudes que le permiten desenvolverse con criterio en un mundo en continuo desarrollo científico, tecnológico, económico y social.

**Física y Química es una materia que debe cursar todo el alumnado en el segundo y el tercer curso de la Educación Secundaria Obligatoria, de tal forma que siente las bases para una formación científica básica. En el cuarto curso de la Educación Secundaria Obligatoria, Física y Química, de carácter opcional, presenta un currículo más amplio y especializado que incide en la profundización en las destrezas científicas que permitan al alumnado, más allá de despertar su curiosidad, aprender aplicando el pensamiento científico.**

Los **contenidos** de esta materia se encuentran estructurados en los que tradicionalmente han sido los grandes bloques de conocimiento de la Física y la Química: «La materia», **«El cambio»**, «La energía» y «La interacción». Además, este currículo propone la existencia de un bloque de **contenidos** comunes que hace referencia a las metodologías de la ciencia y a su importancia en el desarrollo de estas áreas de conocimiento. En este bloque, denominado «Las destrezas científicas básicas», se establece, además, la relación de las ciencias experimentales con una de sus herramientas más potentes; las matemáticas, que ofrecen un lenguaje de comunicación formal y que incluyen los conocimientos, destrezas y actitudes previos del alumnado y los que se adquieren a lo largo de esta etapa educativa. Se **incluye** aquí el papel destacado de las mujeres a lo largo de la historia de la ciencia como forma de poner **su aportación** en valor y fomentar nuevas vocaciones femeninas hacia el campo de las ciencias experimentales y la tecnología.

El bloque de «La materia» engloba los **conocimientos** sobre la constitución interna de las sustancias, lo que incluye la descripción de la estructura de los elementos y de los compuestos químicos y las propiedades macroscópicas y microscópicas de la materia como base para profundizar en estos contenidos en cursos posteriores.

El bloque denominado «El cambio» aborda las principales transformaciones físicas y químicas de los sistemas materiales y naturales, así como los ejemplos más frecuentes del entorno y sus aplicaciones y contribuciones a la creación de un mundo mejor.

«La interacción» contiene los saberes acerca de los efectos principales de las interacciones fundamentales de la naturaleza y el estudio básico de las principales fuerzas del mundo natural, así como sus aplicaciones prácticas en campos tales como la astronomía, el deporte, la ingeniería, la arquitectura o el diseño.

Por último, en el bloque «La energía» el alumnado profundiza en los conocimientos, destrezas y actitudes que adquirió en la Educación Primaria, como las fuentes de energía y sus usos prácticos o los aspectos básicos acerca de las formas de energía. 

Todos **estos** elementos curriculares están relacionados entre sí formando un todo que dota al **programa** de esta materia de un sentido integrado y holístico. **Englobada en lo que se conoce como disciplinas STEM, la asignatura de Física y Química tendrá una orientación eminentemente práctica, usando las metodologías propias de la ciencia.**

**A modo orientativo de cómo puede plantearse una actividad en el aula se presenta el siguiente ejemplo: con el fin de analizar la eficiencia energética en el entorno doméstico y escolar, el alumnado puede realizar, siguiendo los pasos propios del método científico, un estudio con propuestas viables para la mejora de la eficiencia energética en ambos entornos.**

**Esta actividad se podría desarrollar dentro del bloque «La energía» en cualquiera de los cursos de Educación Secundaria Obligatoria en los que se da la materia, contribuyendo a desarrollar las competencias específicas 5 y 6 de la misma.**

##### Competencias Específicas (Madrid)

Cambios en [Decreto 65/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo de la Educación Secundaria Obligatoria](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-2.PDF) vs proyecto  
En 4 se cambia "valor" por "valor añadido"  

**1. Comprender y relacionar los motivos por los que ocurren los principales fenómenos fisicoquímicos del entorno, explicándolos en términos de las leyes y teorías científicas adecuadas para resolver problemas con el fin de aplicarlas para mejorar la calidad de vida humana.**

La esencia del pensamiento científico es comprender cuáles son los porqués de los fenómenos que ocurren en el medio natural para tratar de explicarlos a través de las leyes físicas y químicas adecuadas. Comprenderlos implica entender las causas que los originan y su naturaleza, permitiendo al alumnado **la capacidad de** actuar con sentido crítico para mejorar, en la medida de lo posible, la realidad a través de la ciencia.

El desarrollo de esta competencia específica conlleva hacerse preguntas para comprender cómo es la naturaleza del entorno, cuáles son las interacciones que se producen entre los distintos sistemas materiales y cuáles son las causas y las consecuencias de las mismas. Esta comprensión dota al alumnado de fundamentos críticos en la toma de decisiones, activa los procesos de resolución de problemas y, a su vez, posibilita la creación de nuevo conocimiento científico a través de la interpretación de fenómenos, el uso de herramientas científicas y el análisis de los resultados que se obtienen. Todos estos procesos están relacionados con el resto de **las** competencias específicas y se engloban en el desarrollo del pensamiento científico. Por tanto, para el desarrollo de esta competencia, el individuo requiere un conocimiento de las formas y procedimientos estándar que se utilizan en la investigación científica y su relación con el mundo natural.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo**: CCL1, STEM1, STEM2, STEM4, CPSAA4.

**2. Expresar las observaciones realizadas por el alumnado en forma de preguntas, formulando hipótesis para explicarlas y demostrando dichas hipótesis a través de la experimentación científica, la indagación y la búsqueda de evidencias, para desarrollar los razonamientos propios del pensamiento científico y mejorar las destrezas en el uso de las metodologías científicas.**

Una característica inherente a la ciencia y al desarrollo del pensamiento científico en la adolescencia es la curiosidad por conocer y describir los fenómenos naturales. Dotar al alumnado de competencias científicas implica trabajar con las metodologías propias de la ciencia y reconocer su importancia en la sociedad. El alumnado que desarrolla esta competencia debe observar, formular hipótesis y aplicar la experimentación, la indagación y la búsqueda de evidencias para comprobarlas y predecir posibles cambios.

Utilizar el bagaje propio de los conocimientos que el alumnado adquiere a medida que progresa en su formación básica y contar con una completa colección de recursos científicos, tales como las técnicas de laboratorio o de tratamiento y selección de la información, suponen un apoyo fundamental para la mejora de esta competencia. El alumnado que desarrolla esta competencia emplea los mecanismos del pensamiento científico para interaccionar con la realidad cotidiana y analizar, la información que proviene de las observaciones de su entorno, o que recibe por cualquier otro medio, y expresarla y argumentarla en términos científicos.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo**: CCL1, CCL3, STEM1, STEM2, CD1, CPSAA4, CE1, CCEC3.

**3. Manejar con soltura las reglas y normas básicas de la física y la química en lo referente al lenguaje de la IUPAC, al lenguaje matemático, al empleo de unidades de medida correctas, al uso seguro del laboratorio y a la interpretación y producción de datos e información en diferentes formatos y fuentes, para reconocer el carácter universal y transversal del lenguaje científico y la necesidad de una comunicación fiable en investigación y ciencia entre diferentes países y culturas.**

La interpretación y la transmisión de información con corrección juegan un papel muy importante en la construcción del pensamiento científico, pues otorgan al alumnado la capacidad de comunicarse en el lenguaje universal de la ciencia, más allá de las fronteras geográficas y culturales del mundo. Con el desarrollo de esta competencia se pretende que el alumnado se familiarice con los flujos de información multidireccionales característicos de las disciplinas científicas y con las normas que toda la comunidad científica reconoce como universales para establecer comunicaciones efectivas englobadas en un entorno que asegure la salud y el desarrollo medioambiental sostenible. Entre los distintos formatos y fuentes, el alumnado debe ser capaz de interpretar y producir datos en forma de textos, enunciados, tablas, gráficas, informes, manuales, diagramas, fórmulas, esquemas, modelos, símbolos, etc. Además, esta competencia requiere que el alumnado evalúe la calidad de los datos **y valore su imprecisión**, así como que reconozca la importancia de la investigación previa a un estudio científico.

Con esta competencia específica se desea fomentar la adquisición de conocimientos, destrezas y actitudes relacionadas con el carácter interdisciplinar de la ciencia, la aplicación de normas, la interrelación de variables, la argumentación, la valoración de la importancia de utilizar un lenguaje universal, el respeto hacia las normas y acuerdos establecidos.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo**: STEM4, STEM5, CD3, CPSAA2, CC1, CCEC2, CCEC4.

**4. Utilizar de forma crítica, eficiente y segura plataformas digitales y recursos variados, tanto para el trabajo individual como en equipo, para fomentar la creatividad, el desarrollo personal y el aprendizaje, mediante la consulta de información, la creación de materiales y la comunicación efectiva en los diferentes entornos de aprendizaje.**

Los recursos, tanto tradicionales como digitales, adquieren un papel crucial en el proceso de enseñanza y aprendizaje en general, y en la adquisición de competencias en particular, pues un recurso bien seleccionado facilita el desarrollo de procesos cognitivos de nivel superior y propicia la comprensión, la creatividad y el desarrollo del alumnado. La importancia de los recursos, no solo utilizados para la consulta de información sino también para otros fines como la creación de materiales didácticos o la comunicación efectiva con otros miembros de su entorno de aprendizaje, dota al alumnado de herramientas para adaptarse a una sociedad que actualmente demanda personas integradas y comprometidas con su entorno.

Es por este motivo por lo que esta competencia específica también pretende que el **alumnado** maneje con soltura recursos y técnicas variadas de colaboración y cooperación, que analice su entorno y localice en él ciertas necesidades que le permitan idear, diseñar y fabricar productos que ofrezcan un valor **añadido**.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo**: CCL2, CCL3, STEM4, CD1, CD2, CPSAA3, CE3, CCEC4.

**5. Utilizar las estrategias propias del trabajo en grupo, como base emprendedora de una comunidad científica crítica, ética y eficiente, para comprender la importancia de la ciencia en la mejora de la sociedad, las aplicaciones y repercusiones de los avances científicos, la preservación de la salud y la conservación sostenible del medio ambiente.**

Las disciplinas científicas se caracterizan por conformar un todo de saberes integrados e interrelacionados entre sí. Del mismo modo, las personas dedicadas a la ciencia desarrollan destrezas de trabajo en equipo. El alumnado competente estará habituado a las formas de trabajo y a las técnicas más habituales del conjunto de las disciplinas científicas, pues esa es la forma de conseguir, a través del emprendimiento, integrarse en una sociedad que evoluciona. El trabajo en equipo sirve para unir puntos de vista diferentes y crear modelos de investigación unificados que forman parte del progreso de la ciencia.

El desarrollo de esta competencia específica crea un vínculo de compromiso entre el **alumnado** y su equipo, así como con el entorno que los rodea, lo que le habilita para entender cuáles son las situaciones y los problemas más importantes de la sociedad actual y cómo mejorarla.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo**: CCL5, CP3, STEM3, STEM5, CD3, CPSAA3, CC3, CE2.

**6. Comprender y valorar la ciencia como una construcción en continuo cambio y evolución, en la que no solo participan las personas dedicadas a ella, sino que también requiere de una interacción con el resto de la sociedad, para obtener resultados que repercutan en el avance en distintos ámbitos.**

Para completar el desarrollo competencial de la materia de Física y Química, el **alumnado** debe asumir que la ciencia no es un proceso finalizado, sino que está en una continua construcción recíproca con la tecnología y la sociedad. La búsqueda de nuevas explicaciones, la mejora de procedimientos, los nuevos descubrimientos científicos, etc. influyen sobre la sociedad, y conocer de forma global los impactos que la ciencia produce sobre ella es fundamental en la elección del camino correcto para el desarrollo. En esta línea, el alumnado competente debe tener en cuenta valores como la importancia de los avances científicos por y para una sociedad demandante, los límites de la ciencia, las cuestiones éticas y la confianza en los científicos y en su actividad.

Todo esto forma parte de una conciencia social en la que no solo interviene la comunidad científica, sino que requiere de la participación de toda la sociedad puesto que implica un avance individual y social conjunto.

Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 217/2022, de 29 de marzo**: STEM2, STEM5, CD4, CPSAA1, CPSAA4, CC4, CCEC1.



