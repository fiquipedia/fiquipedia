
# Reacciones químicas vistosas

Se trata de reacciones que hacer en semanas de la ciencia o similares.  
También pueden ser vídeos de reacciones que no se pueden hacer pero se pueden mostrar.  

Ojo a quedarse solo en lo vistoso  
[twitter profesmadeinuk/status/1550518785173401604](https://twitter.com/profesmadeinuk/status/1550518785173401604)  
Abordar la enseñanza de las ciencias con "experimentos chulos" acaba generando un problema en el que, si bien los alumnos tienden a participar, lo que les resulta memorable es el experimento en sí, y no los conocimientos esenciales que deberían aprender.  
[Mitos sobre la memoria: «no me acuerdo de nada de la clase de química, sólo de aquel día que explotó todo» - investigaciondocente.com](https://investigaciondocente.com/2022/07/21/mitos-sobre-la-memoria-no-me-acuerdo-de-nada-de-la-clase-de-quimica-solo-de-aquel-dia-que-exploto-todo/)  

[Reacciones químicas vistosas para hacer con los alumnos - docentesconeducacion.es](http://docentesconeducacion.es/viewtopic.php?f=92&t=4136#p17933)  

En "Química y Acción" se comentan varias  
[Cuadernillo Química en Acción  biologiacienciasambientalesyquimica.uah.es (PDF)](http://biologiacienciasambientalesyquimica.uah.es/facultad/documentos/quimica-accion/Cuadernillo.pdf)  

[TALLER DE QUIMICA ESPECTACULAR. Escuela Técnica Superior de Ingenieros Industriales. Universidad Politécnica de Madrid. José Vicente Alonso Felipe](http://quim.iqi.etsii.upm.es/vidacotidiana/Tallerquimicaespectacular.pdf)  

[twitter quimicafacilnet/status/1317894472655581186](https://twitter.com/quimicafacilnet/status/1317894472655581186)  
La #belleza de la #quimica vista en varios precipitados comunes en química analítica. Visita nuestra página web y síguenos en redes sociales #chemistrylab #chemistry 
[![](https://pbs.twimg.com/media/EkoaCdnWkAAC7VB?format=jpg "") ](https://pbs.twimg.com/media/EkoaCdnWkAAC7VB?format=jpg)  

Se citan: Ag<sub>2</sub>CrO<sub>4</sub>, PbI<sub>2</sub>, CdS, Bi<sub>2</sub>S<sub>3</sub>, Ni(OH)<sub>2</sub> Al(OH)<sub>3</sub>Cu(OH)<sub>2</sub> Ni(DMG)<sub>2</sub> AgBr SnS<sub>2</sub> Ag<sub>3</sub>AsO<sub>4</sub> AgCl  

[twitter ChemistryTe/status/1424273325245751301](https://twitter.com/ChemistryTe/status/1424273325245751301)  
Cu
![](https://pbs.twimg.com/media/E8QI_Z3WEAE4_Pn?format=jpg)  

Se citan: Cu(Ac)<sub>2</sub> CuCl<sub>2</sub> CuSO<sub>4</sub> Cu(NH<sub>3</sub>)<sub>4</sub>SO<sub>4</sub> CuCl 


[twitter HdAnchiano/status/929804277912096772](https://twitter.com/HdAnchiano/status/929804277912096772)  
Cuando echas permanganato potásico en ácido sulfúrico, se forma heptóxido de manganeso, también transparente, por eso «desaparece» el morado.  

[twitter HdAnchiano/status/929804277912096772](https://twitter.com/HdAnchiano/status/929804277912096772)  

[twitter WRBdB/status/974702500887580678](https://twitter.com/WRBdB/status/974702500887580678)  

Magic Bottle. Instructions:  
[Beyond the ‘blue bottle’ - eic.rsc.org](https://eic.rsc.org/exhibition-chemistry/beyond-the-blue-bottle/2000041.article)  … @RSC_EiC #chemed #RealTimeChem  

[![](https://img.youtube.com/vi/LxBNaAFK5c4/0.jpg)](https://www.youtube.com/watch?v=LxBNaAFK5c4 "Beyond the 'blue bottle' - redox and colour chemistry")  


[5 experimentos de química fascinantes - blogthinkbig.com](https://blogthinkbig.com/5-experimentos-de-quimica-que-te-dejaran-con-la-boca-abierta)  
La serpiente del faraón  
La reacción del camaleón  (Permanganato de potasio + sacarosa en medio básico)  
El increíble volcán  
Pasta de dientes para elefantes (despomposición del agua oxigenada con catalizador y jabón)  
Una calabaza tan terrorífica como colorida  

19 diciembre 2019  
[![(NEW) World Record Elephant Toothpaste w/ David Dobrik](https://img.youtube.com/vi/XXn4fP3CnJg/0.jpg)](https://www.youtube.com/watch?v=XXn4fP3CnJg "(NEW) World Record Elephant Toothpaste w/ David Dobrik")

[![](https://img.youtube.com/vi/-Y20TTx7AKI/0.jpg)](https://www.youtube.com/watch?v=-Y20TTx7AKI "Pasta de dientes para elefante Versión niños!") Se usa levadura con catalasa en lugar de KI  
 
 Además de vistosas, se pueden ver reacciones o situaciones llamativas / cámara lenta:  
 
 [![Incendio de aceite de cocinar / Kitchen oil fire (subtítulos en español)](https://img.youtube.com/vi/eoUzAyYt-gw/0.jpg)](https://www.youtube.com/watch?v=eoUzAyYt-gw "Incendio de aceite de cocinar / Kitchen oil fire (subtítulos en español)")
 
 Vídeos de explosiones a cámara superlenta (documentales)  
 
 Hilo con recopilación  
 [twitter VanekitaNekita/status/1333870675086413825](https://twitter.com/VanekitaNekita/status/1333870675086413825)  
 REACCIONES QUÍMICAS  
 Las reacciones químicas ocurren alrededor de nosotros todo el tiempo. También llamadas cambios químicos o fenómenos químicos, son procesos termodinámicos de transformación de la materia.  #VanekitaNekita 
 
 [Vídeo ensayos llama InCl3 CsCl RbCl H3BO3 CuSO4 NaCl BaCl2 CaCl2 SrCl2 LiCl (.mp4)](https://video.twimg.com/tweet_video/EoLc8NIW8AESwIV.mp4)  

 [Vídeo Cu + 4HNO3 -> Cu(NO3)2 + 2NO42 + 2H2O (.mp4)](https://video.twimg.com/tweet_video/EoLcuj_XMAEmkX7.mp4)  
 
 [twitter Rainmaker1973/status/1400769300810579970](https://twitter.com/Rainmaker1973/status/1400769300810579970)  
 
 The Firevase is a flower vase that you literally throw at a fire like a grenade in order to put it out. Produced by a Samsung subsidiary called Cheil Worldwide, it uses an outer chamber filled with potassium carbonate that quickly cools suppressing oxygen  [http://ow.ly/qsbh30onNmy](http://ow.ly/qsbh30onNmy)  
 [This Samsung flower vase is also a throwable fire extinguisher - theverge.com](https://www.theverge.com/2019/3/28/18285253/samsung-firevase-throwable-fire-extinguisher-south-korean-awareness-campaign)  
 [https://video.twimg.com/ext_tw_video/1400483931678519298/pu/vid/720x720/4SCS5YwxldaODGjM.mp4?tag=12](https://video.twimg.com/ext_tw_video/1400483931678519298/pu/vid/720x720/4SCS5YwxldaODGjM.mp4?tag=12)  
 [![ELIDE FIRE® Demo Live Extinguishing Ball](https://img.youtube.com/vi/Q4qQ1IV4gWQ/0.jpg)](https://www.youtube.com/watch?v=Q4qQ1IV4gWQ "ELIDE FIRE® Demo Live Extinguishing Ball")
  
 
[The Chemistry of Glow Sticks - compoundchem.com](https://www.compoundchem.com/2014/10/14/glowsticks/)   
![](https://i1.wp.com/www.compoundchem.com/wp-content/uploads/2014/10/Chemistry-of-Glow-Stick-Colours-Nov-2016.png?ssl=1 "The Chemistry of Glow Sticks")   

Reacción Old-Nassau  
[twitter fqsaja1/status/1482644858468773896](https://twitter.com/fqsaja1/status/1482644858468773896)  
Aperitivo de domingo por la mañana. Sin receta   
Creo que si eres profesor de FyQ al menos una vez debes hacer una Old-Nassau a tus alumnos.  
(Si alguien quiere receta detallada lo contamos)  
Una buena receta siempre asegura el resultado y la exquisitez de un buen "plato":  
[La reacción Old Nassau.pdf](https://drive.google.com/file/d/1lVp37cUTB-rxcT7aE6FD3J5JBShhrWlU/view?usp=sharing)  
También os paso un resumen de algunas curiosidades sobre la Old Nassau que redactamos para una feria de ciencia de hace ya unos cuantos cursos:  

Reacción del reloj de yodo 
[Iodine clock reaction - wikipedia](https://en.wikipedia.org/wiki/Iodine_clock_reaction)  
[vídeo demostración rebecca1227_ en tiktok](https://www.tiktok.com/@rebecca1227_/video/7098646096968256814)  
[Unidad didáctica reloj de yodo (pdf) - clickmica](https://clickmica.fundaciondescubre.es/files/2017/02/reloj-de-yodo.pdf)  

[twitter NaoCasanova/status/1537495511153909760](https://twitter.com/NaoCasanova/status/1537495511153909760)  
El litio reacciona con el agua formando hidróxido de litio e hidrógeno altamente inflamable.  
Haced el favor de tirar las pilas (las de litio y las que no lo son) en el contenedor adecuado.  

[twitter Biochiky/status/1536073040651505664](https://twitter.com/Biochiky/status/1536073040651505664)  
Estudio de las reacciones REDOX con el Camaleón Químico. Se ve el cambio de color del KMnO4 según el manganeso va cambiando su e.o. por la presencia de la glucosa del chupa chups.   
[EL CAMALEÓN QUÍMICO - mediateca](https://mediateca.educa.madrid.org/video/e5z437rd8e8iplx1) vía @educamadrid
Fte: [Química de colores: reacciones redox con chupachups ](https://www.scienceinschool.org/es/article/2018/colourful-chemistry-redox-reactions-lollipops-es/)  

[![](https://img.youtube.com/vi/GuX-xUDnzLo/0.jpg)](https://www.youtube.com/watch?v=GuX-xUDnzLo "REACCIÓN CAMALEÓN CON CHUPA CHUPS - CLUSTER DIVULGACIÓN CIENTÍFICA")  


[La maleta periódica - unav.edu](https://museodeciencias.unav.edu/actividades/la-maleta-periodica)  
> "La Maleta Periódica" es un proyecto conjunto de la Facultad de Ciencias y del Museo de Ciencias de la Universidad de Navarra, cuyo objetivo es acercar la química a las nuevas generaciones y facilitar la experimentación como método didáctico en centros escolares.   
> La maleta proporciona los reactivos necesarios para la ejecución de los experimentos y las descripciones correspondientes a cada uno de ellos. En los distintos experimentos, se tratan diversos conceptos, por lo que se pueden emplear a lo largo del curso en múltiples temáticas y en distintos niveles de conocimiento.  

[![](https://img.youtube.com/vi/bkQJCkd8M5k/0.jpg)](https://www.youtube.com/watch?v=bkQJCkd8M5k "Maleta periódica")  
[![](https://img.youtube.com/vi/gsignOCOGEE/0.jpg)](https://www.youtube.com/watch?v=gsignOCOGEE "Maleta periódica. Fundiendo metal")  
[![](https://img.youtube.com/vi/U_nL9uHo2Ww/0.jpg)](https://www.youtube.com/watch?v=U_nL9uHo2Ww "Maleta periódica. Humo rosa")  
[![](https://img.youtube.com/vi/-G_M8DHQB8I/0.jpg)](https://www.youtube.com/watch?v=-G_M8DHQB8I "Maleta periódica. Osito de gominola")  
[![](https://img.youtube.com/vi/_1-Fy7WZ7zI/0.jpg)](https://www.youtube.com/watch?v=_1-Fy7WZ7zI "Maleta periódica. Tintas mágicas de hierro")  

[twitter ivanquifis/status/1646913138934120448](https://twitter.com/ivanquifis/status/1646913138934120448)  
Como sabemos que está a ocorrer unha reacción química? Que diferenza hai entre as reaccións exotérmicas e as endotérmicas?  
Para explicalo, hoxe sacrificamos un osiño de Haribo (que, en realidade, non era de Haribo, porque para arder non é preciso ir vestido de marca).  
_introducimos un poco de clorato de potasio en un tubo de ensayo y lo calentamos. El clorato de potasio se descompone en cloruro de potasio y oxígeno. En contacto con la glucosa del osito, el oxígeno da lugar a una combustión espontánea_ 

[Glucose+Sodium Hydroxide+Alkaline Solution+Indigocarmine 'Traffic Light' reaction - reddit.com](https://www.reddit.com/r/chemicalreactiongifs/comments/btocjr/glucosesodium_hydroxidealkaline/)  

[twitter fyqfray/status/1546537565385269250](https://twitter.com/fyqfray/status/1546537565385269250)  
Lluvia de oro  
Disolución nitrato de plomo (II) + disolución yoduro de potasio  
Se juntan ambas disoluciones  
Se forma yoduro de plomo (II), sólido amarillo insoluble  
Se calienta la mezcla hasta que se disuelve todo el yoduro de plomo (II)  
Se enfría rápidamente para bajar la temperatura  
El yoduro de plomo (II) vuelve a precipitar, pero ahora lo hace formando cristales: "lluvia de oro"  

[![](https://img.youtube.com/vi/fBkbWKFv2pE/0.jpg)](https://www.youtube.com/watch?v=fBkbWKFv2pE "EepyBird presents the Mentos & Diet Coke Experiments")  

[Extreme Coke & Mentos - EepyBird - playlist](https://www.youtube.com/playlist?list=PL6B6C6F35DD9211A8)  

[Spurting Science: Erupting Diet Coke with Mentos - scientificamerican.com](https://www.scientificamerican.com/article/bring-science-home-coke-mentos/)  
> To create bubbles, the carbon dioxide needs to interact with itself, which means that the carbon dioxide's bonds with water in the Diet Coke must be broken. A Mentos candy can help with this. Although the candy may look smooth, if you looked at it under a microscope you'd see tiny bumps coating its entire surface. This rough surface allows the bonds between the carbon dioxide gas and the water to more easily break, helping to create carbon dioxide bubbles and cause the classic eruption. The speed at which the Mentos falls through the soda can affect how large the eruption is, and this can be tested by comparing whole with crushed Mentos, the latter of which are less dense.  

[Cinética de reacción – oxidación del ion yoduro por el ion persulfato - quimicafacil.net](https://quimicafacil.net/manual-de-laboratorio/cinetica-de-reaccion-ion-yoduro-ion-persulfato/)  

[![](https://img.youtube.com/vi/xIVONw9Pr4w/0.jpg)](https://www.youtube.com/watch?v=xIVONw9Pr4w "Did you know this about aluminum cans? - 
Chemteacherphil")  

[![](https://img.youtube.com/vi/vzopTRZzh-Y/0.jpg)](https://www.youtube.com/watch?v=vzopTRZzh-Y "20 asombrosos experimentos científicos e ilusiones ópticas Compilación - Home Science")  
Compilación que incluye algunos elementos que no son reacciones químicas  

Este video es una compilación de los mejores 20 experimentos científicos con líquido y fuego.  
00:02 desinfectante de manos inflamable  
00:25 Coca Cola y piscina reacción de cloro  
00:47 Llama itinerante  
01:04 Ilusiones ópticas animadas  
01:27 Motor a reacción en un frasco  
01:54 Hielo caliente  
02:48 Coca Cola VS Coca Cola Zero - Prueba de Azúcar  
03:07 Azúcar y Ácido Sulfúrico - Extraña Reacción Química  
03:42 increíble serpiente de fuego   
04:26 ilusión anamórfica  
05:11 Pasta de dientes de elefante  
05:37 Tornado de fuego  
06:14 Barco propulsado por jabón  
06:36 Holograma de Smartphone hecho en casa  
06:57 Piscina de cloro y reacción de líquido de ruptura  
07:19 Ilusión en la azotea  
07:49 Hacer Flying Balloon en casa  
08:39 agua jabonosa y gas  
09:09 nube instantánea en una botella  
09:28 bolso mágico sin fuga  

[![](https://img.youtube.com/vi/lz5zl8h9Abg/0.jpg)](https://www.youtube.com/watch?v=lz5zl8h9Abg " Prepara un bonito arco iris gracias al pH y la col lombarda. #BigVanCiencia #Clowntífics ")  

[The Floating Soap Bubble](https://www.chemedx.org/blog/floating-soap-bubble)  
NaHCO3 (s) + CH3COOH(aq) → CH3COONa(aq) + H2O + CO2(g)    
 Floating bubbles? 🫧 #shorts #chemistryteacher #science #CO2 #chemicalreaction #scienceexperiment   
[![](https://img.youtube.com/vi/t1fCM3w3T8Y/0.jpg)](https://www.youtube.com/watch?v=t1fCM3w3T8Y " Floating bubbles? 🫧 #shorts #chemistryteacher #science #CO2 #chemicalreaction #scienceexperiment ")  

[Química recreativa con agua oxigenada. Revista Eureka sobre Enseñanza y Divulgación de las Ciencias. pp. 446-453 Publicado: 24-10-2011](https://revistas.uca.es/index.php/eureka/article/view/2737)  

Combustión de magnesio : sencilla y sorprende a los alumnos por la luminosidad y por ver "arder" un metal

Serpiente de Carbono. Deshidratacion de Azúcar con Ácido Sulfúrico - Cienciabit  
[![](https://img.youtube.com/vi/gRYh5mrDdQs/0.jpg)](https://www.youtube.com/watch?v=gRYh5mrDdQs "Serpiente de Carbono. Deshidratacion de Azúcar con Ácido Sulfúrico - Cienciabit")   

H2SO4 + C12H22O11 → C + CO2 + SO2 +H2O

Reacción entre sacarosa y clorato potásico | La pesadilla de los ositos de gominola - Química insólita  
[![](https://img.youtube.com/vi/I_RTNSSClso/0.jpg)](https://www.youtube.com/watch?v=I_RTNSSClso "Reacción entre sacarosa y clorato potásico | La pesadilla de los ositos de gominola - Química insólita")   

KClO3 → 3/2O2(g) + KCl (l)

C12H22O11(s) + 12O2(g) → 12CO2(g) + 11H2O(l)

Esto es lo que ocurre si mezclas agua oxigenada con permanganato - ExpCaseros  
[![](https://img.youtube.com/vi/sNLUcVA-oD8/0.jpg)](https://www.youtube.com/watch?v=sNLUcVA-oD8 "Esto es lo que ocurre si mezclas agua oxigenada con permanganato - ExpCaseros")   

Agua oxigenada y permanganato potásico - elfisicoloco  
[![](https://img.youtube.com/vi/oIHMPCtm6Bo/0.jpg)](https://www.youtube.com/watch?v=oIHMPCtm6Bo "Agua oxigenada y permanganato potásico - elfisicoloco")   

2 KMnO4 + H2O2 → 2 KOH + 2 MnO2 + 2 O2

Al ser exotérmica se libera también vapor de agua  

[Easy science tricks for schoo - melscience](https://melscience.com/US-en/articles/easy-science-tricks-school/)  
> Step-by-step in­struc­tions  
Write some­thing on a sheet of pa­per with an erasable pen and heat it for a short time – the in­scrip­tion dis­ap­pears! It will reap­pear if you leave the pa­per in the freez­er. If you sud­den­ly out­lined the wrong text with a high­lighter, don’t wor­ry: cut a lemon and soak a cot­ton swab in the juice. Then ap­ply it to the out­lined text – the yel­low col­or dis­ap­pears! Dis­as­sem­ble a pen, leav­ing the hol­low body (a cock­tail straw will also work). In a large bak­ing dish, pre­pare a soapy so­lu­tion so that there is some foam on top. In­flate a bal­loon and at­tach it to the pen’s body – now you can write on the sur­face of the so­lu­tion with soap bub­bles!  
Process de­scrip­tion  
Erasable pens usu­al­ly have three com­po­nents. When heat­ed, the bonds be­tween them change, caus­ing the col­or to fade. The process can be re­versed by cool­ing the for­mer in­scrip­tion. Yel­low high­lighter ink con­tains a pyra­nine dye. Its col­or is sen­si­tive to acidic me­dia. Lemon juice con­tains cit­ric acid, which eas­i­ly fades pyra­nine! Bub­bles from or­di­nary wa­ter are un­sta­ble – they quick­ly burst due to the wa­ter’s sur­face ten­sion. But this sur­face ten­sion can be low­ered with soap! If there is also a lit­tle foam on the so­lu­tion’s sur­face, the bub­bles will re­main sta­tion­ary. Now you can write with soap bub­bles!

[Química insólita](https://quimins.wordpress.com/indice/)  
Esta es la web del Curso de Formación Permanente de la UNED Química Insólita, que se inició el 2 de diciembre de 2024 y terminará en 18 de mayo de 2025, estando destinado a profesore/as de Enseñanza Secundaria y público en general.  
Aquí solo se muestran las recetas de los experimentos; el curso universitario se dedica a la explicación de los fenómenos físico-químicos subyacentes. El objetivo es claro: hacer más atractivas las clases de Física y Química a la/os alumna/os y facilitarles la comprensión de los  conceptos teóricos.  

[Química insólita  @quimicainsolita5607 - youtube](https://www.youtube.com/channel/UCyQ9T1YrJXD5bgFooZoghbw)  

 
