
# Contacto

Si quieres encontrarme o contactar (comentarios y sugerencias siempre son bienvenidos), en los comentarios sobre [licenciamiento](/licenciamiento) está mi nombre y el correo electrónico de contacto (también en el icono correo al pie de la web), así como en los materiales de creación propia en los que lo añado en la cabecera junto a licenciamiento.  

## Bluesky

Varias maneras de acceder a mi perfil  
[@fiquipedia.bsky.social](https://bsky.app/profile/fiquipedia.bsky.social)  
[did:plc:ygxyvnpzivzxnytl73jxnyou](https://bsky.app/profile/did:plc:ygxyvnpzivzxnytl73jxnyou)  
[@fiquipedia.es](https://bsky.app/profile/fiquipedia.es)  

## Mastodon
[@fiquipedia@mastodon.social](https://mastodon.social/@fiquipedia)  

## Twitter

En 2013 empecé a usar twitter ([@FiQuiPedia](https://twitter.com/FiQuiPedia)), y tenía mensajes directos abiertos como vía adicional para contactar. En 2025 dejo de usar Twitter y dejo de tener mensajes directos abiertos.    
[![Not f'd — you won't find me on Facebook](http://static.fsf.org/nosvn/no-facebook-me.png) ](http://www.fsf.org/fb)  

## Blog 'Algo queda que decir'

Por separar temas en 2017 creo un blog para temas de opinión/información, con objetivos y formato distinto que esta web [Algo queda que decir - bloger](http://algoquedaquedecir.blogspot.com.es/) que en 2024 migro a gitlab [Algo queda que decir - gitlab](https://fiquipedia.gitlab.io/algoquedaquedecir/)  
Como la cuenta de Twitter es única, en mi TL Twitter verás cosas tanto de web (Física y Química) y blog (otros temas)  

Si quieres conocer algo sobre mi, puedes leer el [Primer post del blog: presentación](https://fiquipedia.gitlab.io/algoquedaquedecir/post/primer-post-del-blog-presentacion/)  
Pongo algunos posts concretos por si al leer alguien tiene curiosidad. Se puede ver que la mayoría está relacionado con educación, transparencia y "activismo". Hay algunos puntuales sobre ciencias, pero si es un tema claramente de ciencias, posiblemente esté en esta página y no en el blog.  
* [Ratios en educación: normativa](https://algoquedaquedecir.blogspot.com/2018/12/ratios-en-educacion-normativa.html)  
* [¡Reclamad, malditos!](https://algoquedaquedecir.blogspot.com/2018/07/reclamad-malditos.html)  
* [¡Compartid, malditos!](https://algoquedaquedecir.blogspot.com/2018/04/compartid-malditos.html)  
* [No podemos poner eso](https://algoquedaquedecir.blogspot.com/2018/03/no-podemos-poner-eso.html)  
* [¿Por qué lo llaman becas cuando quieren decir privatización? (artículo 23 julio 2022 en CTXT)](https://ctxt.es/es/20220701/Firmas/40327/Enrique-Garcia-Simon-becas-privatizacion-educacion-publica-bachillerato-concertado.htm)  

