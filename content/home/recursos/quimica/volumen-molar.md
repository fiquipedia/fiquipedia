
# Volumen molar

Relacionado con el  [concepto de mol](/home/recursos/quimica/concepto-de-mol)  y con las  [leyes de los gases](/home/recursos/quimica/leyes-de-los-gases)  

Es el volumen que ocupa un mol de gas ideal en ciertas condiciones de presión y temperatura, llamadas condiciones estándar.  
La definición de esas condiciones está realizada por IUPAC  [http://goldbook.iupac.org/S06036.html](http://goldbook.iupac.org/S06036.html)  
Usando como condiciones estándar 273,15 K y 105 Pa, el volumen estándar son 22,7 L.  
(El cálculo asociado es 0,082*273,15*(101325/100000)=22,7)  

Hace tiempo se decía que eran 22,4 L ¿dónde está la diferencia y por qué?  
En que antes de 1982 se utilizaba otro valor de presión estándar:  
[standard pressure - goldbook.iupac.org](https://goldbook.iupac.org/terms/view/S05921)  
*standard pressure*  
*In 1982 IUPAC recommended the value 105 Pa, but prior to 1982 the value 101 325 Pa (= 1 atm) was usually used.*  
La confusión es habitual, y aunque por ejemplo en wikipedia en inglés está bien tratado, no lo está en las páginas de la wikipedia en español (en el momento de escribir por primera vez esto, febrero 2016)  
[Molar volume, Ideal gases - wikipedia](https://en.wikipedia.org/wiki/Molar_volume#Ideal_gases)  
[Standar conditions for temperature and pressure, Molar volume of a gas - wikipedia](https://en.wikipedia.org/wiki/Standard_conditions_for_temperature_and_pressure#Molar_volume_of_a_gas)  

Está comentado en  [apuntes de Química de 2º de Bachillerato,](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato)  asociado al bloque Bloque Transformaciones energéticas y espontaneidad  

La definición de 1 atm está comentada en [recursos presión](/home/recursos/presion)  

Ver [termoquímica](/home/recursos/quimica/termoquimica) sobre condiciones  

