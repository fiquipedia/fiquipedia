# Recursos de simulaciones / laboratorio virtual

1. Aspectos técnicos de simulaciones
2. Recursos App
3. Generales
4. Física
5. Química
6. Geogebra
7. Tecnología

Las simulaciones en Física y Química son un elemento muy útil de cara a la comprensión de conceptos, que pueden sustituir o complementar el laboratorio real. Está muy relacionado con **laboratorios virtuales**.  
A veces relacionados con  [laboratorio no virtual / experimentos](/home/recursos/practicas-experimentos-laboratorio)  
A veces dentro de  [materiales autosuficientes](/home/recursos/materiales-autosuficientes)  o  [cursos online / interactivos](/home/recursos/cursos-online-interactivos) , hay alguna simulación.  
También se incluyen applets, aunque parezcan antiguos, son portables y frecuentes para ciertas simulaciones / visualizaciones.  
A veces pueden ser "animaciones" que no permiten mucha interacción, y pueden diferenciarse poco de ciertos GIFs o vídeos.

También puede estar asociado a apps para realizar experimentos [recursos apps](/home/recursos/recursos-apps)  

##  Aspectos técnicos de simulaciones
Inicialmente aquí por su importancia, pero por su volumen retrasaba ver las simulaciones, así que lo muevo a una página separada  [recursos simulaciones: aspectos técnicos](/home/recursos/simulaciones/recursos-simulaciones-aspectos-tecnicos)  

Allí cito como mencionar Flash. Para que algunas simulaciones flash no "se pierdan" guardo copia a aquí  
[drive/fiquipedia /media/flash](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/media/flash)  
El crédito es de cada autor, que intento citar en gitlab (a veces lo indica el propio fichero .swf)  

##  Recursos App
Enlaza con simulaciones, pero los  [recursos para uso en móviles (Android, Apple) en página aparte](/home/recursos/recursos-apps) 

##  Generales
Algunas simulaciones pueden estar citadas asociadas a algún tema que tratan: por ejemplo  [recursos de sistemas materiales](/home/recursos/fisica/recursos-sistemas-materiales) o en recursos óptica.  

   * "Simulaciones interactivas de la universidad de Colorado"  
   [Simulaciones - phet.colorado.edu/en](http://phet.colorado.edu/en/simulations/category/new)  
   Muchas simulaciones, organizadas por temas (física, química, biología ..), muchas de ellas están traducidas al español, y bastantes cuentan con propuestas didácticas de uso. Se pueden descargar y utilizar sin conexión a internet. Son libres, licencia creative commons.  [PhET SOFTWARE AGREEMENT](http://phet.colorado.edu/en/about/licensing)  
   Esa página creo que está dentro del proyecto"Physics 2000" de Carl Wieman, premio Nobel de física, para mejorar la enseñanza de física [CU Nobel Laureate Carl Wieman Launches Project To Improve Physics And Science Education _waybackmachine_](https://web.archive.org/web/20111118090636/http://www.colorado.edu/news/r/4e684b90188ca093233db98ac819cd91.html)  [Welcome to Physics 2000, an interactive journey through modern physics! _waybackmachine_](https://web.archive.org/web/20080709003133if_/http://www.colorado.edu:80/physics/2000/index.pl) 
   * Laboratorio virtual. Propiedades de la materia  
   [fq.iespm.es tuL@boratorio de Química, propiedades características de la materia](http://fq.iespm.es/documentos/janavarro/flash/laboratorio/Laboratorio_CS6b)  
   [fq.iespm.es tuL@boratorio de Química, propiedades características de la materia (.swf)](http://fq.iespm.es/images/janavarro/flash/laboratorio/Laboratorio_CS6b.swf)  
   Simula con realismo un laboratorio para realizar experiencias de cálculo de volúmenes, masas, densidades, calentamiento... y manejar material de laboratorio.José Antonio Navarro: medida volúmenes, densidad ,....  
   **Muy trabajado visualmente, permite ver el material de laboratorio y trabajar con él de manera bastante realista**
   ![](https://pbs.twimg.com/media/DN9oQwmW4AEMb76?format=jpg)  
   * [LabXChage, simulaciones](https://www.labxchange.org/library?t=ItemType%3Asimulation)  
   En 2022 537 simulaciones, clasificadas por contenido  
   * [OLABS interactive simulations](http://www.olabs.edu.in/)  
   The OLabs is based on the idea that lab experiments can be taught using the Internet, more efficiently and less expensively. The labs can also be made available to students with no access to physical labs or where equipment is not available owing to being scarce or costly. This helps them compete with students in better equipped schools and bridges the digital divide and geographical distances. The experiments can be accessed anytime and anywhere, overcoming the constraints on time felt when having access to the physical lab for only a short period of time.  
   Physics, Chemistry, Biology Labs from Class 9 to Class 12.   
   *  [twitter helenrey/status/1282124227278036992](https://twitter.com/helenrey/status/1282124227278036992)  
   I have a spreadsheet of online labs/simulations that you can search by topic/level etc if that helps.   
   [drive.google.com drive folders/1ySmAmvPhzxoVCVYm_4wskTaTHb_RxTKw](https://drive.google.com/drive/folders/1ySmAmvPhzxoVCVYm_4wskTaTHb_RxTKw) 
   * [Experiencias sin laboratorio, fisquiweb](https://fisquiweb.es/experienciasnolab.htm)  
   [twitter garlan2/status/1388020739085852672](https://twitter.com/garlan2/status/1388020739085852672)  
   #FisQuiWeb Actividades de investigación para ser realizadas usando laboratorios virtuales. Pensadas para ser resueltas por los alumnos una vez introducidas y comentadas por parte del profesor. Se suministran fichas del profesor y del alumno.  
   * [Minutelabs.io](https://minutelabs.io/)  
   Tiene simulaciones de varios tipos y algunas relacionadas con Física (teoría del caos, física nuclear, óptica ... y también alguna asociada a química (tabla periódica de magnetismo))  
   * [Física 2000, una jornada interactiva a través de la Física Moderna _waybackmachine_](https://web.archive.org/web/20150310054258if_/http://www.maloka.org:80/fisica2000/)  
   * [The Applet Collection _waybackmachine_](https://web.archive.org/web/20200530163600/http://lectureonline.cl.msu.edu/~mmp/applist/applets.htm)  
   These applets enable you to study simple physical systems in a playful way. They are idealizations of realistic scenarios, which follow the proper equations that also govern the real experimental systems  
   Kinematics, Dynamics, Rotational Motion, Statics, Oscillations, Thermal Physics, Wave Phenomena, Electrostatics, Electrodynamics, Optics, Quantum Physics, Nuclear Physics© W. Bauer, 1999
   * Recopilatorio de simulaciones de la web  
   [http://www.fisicarecreativa.com/](http://www.fisicarecreativa.com/)  
   [Simulación Applets - fisicarecreativa.com](http://www.fisicarecreativa.com/sitios_vinculos/fisica_sg_vinc/simulaciones.htm#general) 
   * [700 Applets de física y química. PRÁCTICAS VIRTUALES, SIMULACIONES Y ANIMACIONES CIENTÍFICAS _waybackmachine_](https://web.archive.org/web/20190627110829/http://perso.wanadoo.es/oyederra/)  
   Recopilación efectuada por Fernando Jimeno Castillo  
   Organizados por cursos desde 1º ESO hasta 2º Bachillerato
   * Animaciones y Simulaciones  
   [http://fqcolindres.blogspot.com.es/p/blog-page_29.html](http://fqcolindres.blogspot.com.es/p/blog-page_29)  
   Recopilación de enlaces de animaciones y simulaciones. IES Valentín Turienzo. cc-by-nc-sa
   * [Recursos Docentes Generales Applets y Juegos - ujaen.es](https://www.ujaen.es/departamentos/fisica/portal-de-recursos-docentes/recursos-docentes-generales/applets-y-juegos)  
   Recopilación de applets y juegos
   * [SIMULADORES PARA EL APRENDIZAJE DE LA FÍSICA. El Mundo de la Física MSc. Franklin Molina Jiménez](https://lilumerena.jimdofree.com/simuladores/)  
   Recopilación de simulaciones  
   * [Laboratorios virtuales de Física gratis en Internet - cuvsi](https://www.cuvsi.com/2014/11/laboratorios-virtuales-de-fisica-gratis.html)  
   CUVSI: Ciudad Universitaria Virtual de San Isidoro. Recopilación de simulaciones  
   * The King's Centre for Visualizatión in Science (KCVS)  
   [http://www.kcvs.ca/site/](http://www.kcvs.ca/site/)  
   The King's University , Edmonton, Alberta, Canada KCVS applets may be linked to and used freely by educators and the general public
   * Laboratorio Virtual  
   [http://labovirtual.blogspot.com.es](http://labovirtual.blogspot.com.es/)  
   Salvador Hurtado Fernández, Profesor de física y química en el I.E.S. Aguilar y Cano de Estepa (Sevilla)  
   Versión HTML5 [https://labovirtual5.blogspot.com.es/](https://labovirtual5.blogspot.com.es/) 
   * [https://www.vlab.co.in/](https://www.vlab.co.in/) 
   * [PLIX - Play, Learn, Interact and Xplore a concept with PLIX. - ck12.org](http://interactives.ck12.org/plix/)  
   [PLIX, Physics - ck12.org](http://interactives.ck12.org/plix/?subject=physics)  
   [PLIX, Chemistry - ck12.org](http://interactives.ck12.org/plix/?subject=chemistry)  
   [PLIX, Physical-science - ck12.org](http://interactives.ck12.org/plix/?subject=physical-science) 
   [Simulations -ck12.org](http://interactives.ck12.org/simulations/) 
   * [BIGS](https://bigs.de/en/)  
   BIGS simulation experiments – the teaching material for schools, teachers and self-motivated students. Available in German, English and Russian and works on pc, laptop, phones, pads, tabs or whiteboards too.  
   Copyright © 2012 BIGS - Lernhilfe für Physik und Technik. All Rights Reserved.Física y tecnología, no en español.
   * [Modellus en Física y Química ](http://modellusfq.blogspot.com/)  
   Francisco José Navarro, licenciamiento cc-by-nc-sa. 
   * [RRLAB - Laboratorio de Realidad Remota - bifi.es](https://www.bifi.es/es/el-bifi-presenta-el-laboratorio-de-realidad-remota/)  
   No es realmente una simulación, sino un sistema de acceso remoto a laboratorio, que ya es rizar el rizo: como no es un laboratorio en directo, lo pongo en esta página de recursos de simulaciones© Instituto de Biocomputación y Física de Sistemas Complejos  
   Algunas frases que aparecen en la documentación que se adjunta recibida en mayo 2012: [bifi_rrlab.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/simulaciones/bifi_rrlab.pdf) y [capturasPantallaRRLAB.pdf](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/simulaciones/capturasPantallaRRLAB.pdf)  
   -"Esta página web estará disponible en el inicio del curso 2012-2013. Para ver detalle de la aplicación consulte documentación anexa y para accedera la demo por favor contacte con nosotros."  
   -"Durante la fase de lanzamiento del sistema, el acceso es gratuito." -> luego puede dejar de serlo
   * [Laboratorio virtual - upv.es/](http://jogomez.webs.upv.es/)  
   CREADO POR: Carlos Barros Vidaurre y Germán Moltó Martínez DIRIGIDO POR: José Antonio Gómez Tejedor. jogomez@fis.upv.es  
   Gran parte de los documentos que aparecen en esta web están basados en el libro: J.A. Gómez Tejedor, et al., "Prácticas de Fundamentos Físicos de la Informática", Servicio de Publicaciones de la Universidad Politécnica, Ref.:2003.526, ISBN 84-9705-310-9.  
   Prácticas virtuales en julio 2014 incluyen Fundamentos Físicos de la Ingeniería:  
   Práctica 1: Cálculo de errores.  
   Práctica 2: Determinación del coeficiente de rozamiento estático.  
   Práctica 3: Caída de graves  
   Práctica 4: Determinación de la constante elástica de un muelle.  
   Práctica 5: Péndulo simple.  
   Práctica 6: Momento de inercia.  
   Práctica 7: Cinemática del sólido rí­gido.  
   Práctica 8: Carril cinemático.  
   Electricidad (programación 1):  
   Práctica 1A: Equipos y aparatos de medida. Montaje de circuitos en corriente continua.  
   Práctica 1B: Evaluación de sistemas de medida de resistencias.  
   Práctica 2: El osciloscopio.  
   Práctica 3: Fenómenos transitorios. La carga y descarga de condensadores.  
   Práctica 6: Resonancia en corriente alterna. Filtros.  
   Electricidad (programación 2):  
   Práctica 1: Introducción. Aparatos de medida.  
   Práctica 2: Medida de resistencia. Error accidental.  
   Práctica 3: Medida de resistencia. Error sistemático.  
   Práctica 4: El osciloscopio. Medida de corrientes variables.  
   Práctica 5: Procesos transitorios. El condensador.
   * [FisQuiWeb. Zona Virtual. Laboratorios interactivos](https://fisquiweb.es/Laboratorio/AccesoZV.htm)  
   Cinemática, dinámica, rozamiento, energía, ondas I y II, Circuitos
   * Animaciones Flash Interactivas Para Aprender.Mecánica, Electricidad, Óptica, Química, Materia  
   [http://www.fisica-quimica-secundaria-bachillerato.es/](http://www.fisica-quimica-secundaria-bachillerato.es/)  
   Los elementos del sitio (animaciones Flash ®, videos, logotipos ...) están protegidos por derechos de autor y son propiedad exclusiva de FQSB. No se les puede reproducir o utilizar en otro sitio que éste. Conforme a lo dispuesto en el artículo L. 122-4 del Código de la propiedad intelectual, cualquier reproducción de un contenido total o parcial de este sitio está prohibida, independientemente de su forma (reproducción, difusión, imbricación, técnica "inline linking" y el "framing" ...). Los enlaces directos a archivos descargables en este sitio también están prohibidos. Se autorizan enlaces a páginas html y el visionado del sitio en clase.
   * Laboratorio Virtuale di Fisica (Italiano)  
   [http://ww2.unime.it/weblab/](http://ww2.unime.it/weblab/)  
   Universitá degli Studi Messina
   * Osciloscopio virtual  
   [http://www.virtual-oscilloscope.com/](http://www.virtual-oscilloscope.com/)  
   Requiere Macromedia Shockwave
   * [Simulación del Osciloscopio Virtual - upv.es](http://personales.upv.es/jogomez/labvir/osciloscopio/Simulador)   
   Applet Java. José Manuel Gómez Soriano: jogoso@inf.upv.es José Vicente Benlloch Dualde: jbenlloc@disca.upv.es
   * [Laboratorio virtual (catalán, flash) _waybackmachine_ ](https://web.archive.org/web/20150329005632/http://www.edu365.cat/eso/muds/ciencies/laboratori/motor/flash_content/index.html)  
   Recorrido virtual por un laboratorio identificando material y describiéndolo
   * Visión Recursos de Ciencias. Simulaciones  
   [http://deciencias.wordpress.com/simulaciones/](http://deciencias.wordpress.com/simulaciones/)  
   Miguel Vaquero
   * General Physics Java Applets  
   [http://surendranath.tripod.com/Apps.html](http://surendranath.tripod.com/Apps)  
   Kinematics, Dynamics, Oscillations, Waves, Heat, Electricity, OpticsEnlaces a otras simulaciones en  
   [http://surendranath.tripod.com/Main/Links.html](http://surendranath.tripod.com/Main/Links)  
   En 2013 tiene:dominio propio  
   [http://www.surendranath.org/](http://www.surendranath.org/)  
   Apps para móvil  
   [https://play.google.com/store/search?q=pub:Surendranath.B.](https://play.google.com/store/search?q=pub:Surendranath.B.) 
   * [Physique et simulations numériques - univ-lemans.fr](http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/index)  
   Francés. Física y algo de química (cristales). Jean-Jacques ROUSSEAU. Faculté des Sciences exactes et naturelles. Université du Maine 
   * T.I.G.E.R., the Teachers' Instructional Graphics Educational Resource for animations, diagrams, videos and other materials for use in the classroom. These materials are developed by NCSSM (North Carolina School of Science and Mathematics) Distance Education and Extended Programs (DEEP) for use in its IVC and online distance education courses, and to supplement residential classes at NCSSM.  
   All materials are copyright ©NCSSM, and are subject to this Creative Commons License.  
   [http://www.dlt.ncssm.edu/tiger/](http://www.dlt.ncssm.edu/tiger/)  
   Many of these materials are also available in a searchable format on the NCSSM STEM web site.  
   [http://www.dlt.ncssm.edu/stem/](http://www.dlt.ncssm.edu/stem/) 
   *  [http://onlinelabs.in/](http://onlinelabs.in/)  
   [http://onlinelabs.in/chemistry](http://onlinelabs.in/chemistry)  
   [http://onlinelabs.in/physics](http://onlinelabs.in/physics)  
   OnlineLabs.in aims to serve as a comprehensive, encyclopedic reference about online labs in a variety of subjects, particularly virtual laboratory simulations for science education. We categorize useful listings for online lab simulations, virtual science experiments and free educational software. We currently feature resources in Chemistry, Physics and Biology to help learners identify free and commercial virtual science labs. We also offer online educational resources and information about simulations and free software in Anatomy, Geology, Astronomy, Design and Math.
   * MATTER (MATerials Teaching Educational Resources) Initiative for Schools website  
   [http://schools.matter.org.uk/Default.htm](http://schools.matter.org.uk/Default.htm)  
   © 1999 MATTER Project, The University of LiverpooNote: Active development of this site finished in 2000. However, it still continues to attract more and more users, and so of course MATTER plans to continue hosting the site indefinitely. And we still really like to hear from those of you - teachers, students and parents - who have enjoyed using it.
   * Laboratorios Virtuales en Ciencia y Tecnología, Departamento de Física Aplicada-Universidad de Córdoba  
   [http://rabfis15.uco.es/lvct/](http://rabfis15.uco.es/lvct/) 
   * Simulaciones en "El rincón de la Ciencia"  
   [http://centros5.pntic.mec.es/ies.victoria.kent/Rincon-C/Simulaci/Simulaci.html](http://centros5.pntic.mec.es/ies.victoria.kent/Rincon-C/Simulaci/Simulaci)  
   La propia página indica "En esta página queremos presentar algunas simulaciones de experimentos, efectos o fenómenos físicos y químicos que ayuden a mejorar su comprensión". Material de la web con licenciamiento cc-by-nc-nd. Consultado en diciembre 2011. **En julio 2014 no está operativo el enlace al propio Rincón de la Ciencia.**
   *  [https://3dlabs.upm.es/laboratorios.php](https://3dlabs.upm.es/laboratorios.php)  
   Laboratorios virtuales gratuitos para los profesores y están enfocados en Secundaria y Bachillerato a las especialidades de Tecnología, Física y Química y Biología; y a las distintas familias profesionales de FP que pueda tener implantadas en su centro, por ejemplo, Electricidad y Electrónica, Sanitaria, Edificación y obra civil, Topografía, Forestales, etc. 
   * Plataforma Graasp y laboratorios virtuales GO-LAB  
   [https://support.golabz.eu/es/node/235](https://support.golabz.eu/es/node/235) 
   * [JavaLab](http://javalab.org/en/)  
   Free interactive science simulation written by JavaScript Simulates various natural phenomena  
   * [SimBucket](https://www.simbucket.com/)  
   SimBucket contains our collection of free, browser-friendly HTML5 simulations.  
   * [Virtual Labs. An Initiative of Ministry of Education, Indica](https://www.vlab.co.in/)  
   * [VALUE Virtual Amrita Laboratories Universalizing Education](https://vlab.amrita.edu/)  
   * [PCCL Physics and Chemistry by a Clear Learning](https://www.physics-chemistry-interactive-flash-animation.com/)  
   Interactive flash simulations - videos   
   * [Labster](https://www.labster.com/simulations)  
   * [educaplus.org](http://www.educaplus.org/)  
   * [objetos UNAM](http://objetos.unam.mx/)  
   * [STEM Collection IES Vicente Aleixandre Sevilla](https://	stemcollection.com/om/70/es)  
   
      Hay aplicaciones como [Chemix](https://chemix.org/) que permite dibujar prácticas de laboratorio de Química y Física, pero no son simulaciones  


##  Física

   * [Physion Interactive Physics Simulations](https://app.physion.net/)  
   Online app for designing and simulating physics experiments  
   *  [Física en la escuela - HTML5 (Física Animaciones/Simulaciones)](https://www.vascak.cz/?p=2192&language=es#demo)  
   [Mecánica ](https://www.vascak.cz/?p=2192&language=es#kapitola0)  
   [Campo gravitatorio ](https://www.vascak.cz/?p=2192&language=es#kapitola1)  
   [Vibraciones mecánicas y ondas ](https://www.vascak.cz/?p=2192&language=es#kapitola2)  
   [Física molecular y termodinámica ](https://www.vascak.cz/?p=2192&language=es#kapitola3)  
   [Electrostática ](https://www.vascak.cz/?p=2192&language=es#kapitola4)  
   [Corriente eléctrica](https://www.vascak.cz/?p=2192&language=es#kapitola5)  
   [Semiconductor ](https://www.vascak.cz/?p=2192&language=es#kapitola6)  
   [Conducción eléctrica en líquidos ](https://www.vascak.cz/?p=2192&language=es#kapitola7)  
   [Conducción de la electricidad y el gas en el vacío ](https://www.vascak.cz/?p=2192&language=es#kapitola8)  
   [Campo magnético ](https://www.vascak.cz/?p=2192&language=es#kapitola9)  
   [Corriente alterna ](https://www.vascak.cz/?p=2192&language=es#kapitola10)  
   [Óptica](https://www.vascak.cz/?p=2192&language=es#kapitola11)  
   [Teoría de la relatividad especial ](https://www.vascak.cz/?p=2192&language=es#kapitola12)  
   [Física atómica ](https://www.vascak.cz/?p=2192&language=es#kapitola13)  
   [Física nuclear ](https://www.vascak.cz/?p=2192&language=es#kapitola14)  
   [Matemáticas](https://www.vascak.cz/?p=2192&language=es#kapitola15)  
   [https://www.vascak.cz/physicsanimations.php?l=es](https://www.vascak.cz/physicsanimations.php?l=es) 
   * McCulley's HTML 5 Physics Lab Simulations  
   The simulations listed below are programs that I wrote for my students to use in lab as a compliment to a live part of the lab. These programs were written to work on computers, tablets, phones and other handheld devices with HTML 5 capable browsers. All functionality for these programs comes from using the mouse or the touch screen and no keyboard usage is required. 
   [https://www.thephysicsaviary.com/Physics/Programs/Labs/](https://www.thephysicsaviary.com/Physics/Programs/Labs/) 
   * [Crocodile Phisycs - emtic.educarex.es](https://emtic.educarex.es/recursos/conoce/laboratorio-de-fisica)  
   * Physics CK-12 Simulations  
   [https://interactives.ck12.org/simulations/physics.html](https://interactives.ck12.org/simulations/physics) 
   * [myPhysicsLab](https://www.myphysicslab.com/) Physics Simulations. Click on one of the physics simulations below... you'll see them animating in real time, and be able to interact with them by dragging objects or changing parameters like gravity.  
   *  [http://www.geocities.ws/fisica1y2/animaciones.htm](http://www.geocities.ws/fisica1y2/animaciones.htm)  
   Flash, Arnaldo González Arias, Universidad de la Habana
   *  [Physics Simulations - physics.bu.edu](http://physics.bu.edu/~duffy/)  
   Andrew Duffy at Boston University  
   [HTML5 simulations - physics.bu.edu](http://physics.bu.edu/~duffy/HTML5) 
   * Laboratorio virtual de física  
   [http://www.fislab.net/](http://www.fislab.net/)  
   Applets cinemática, dinámica ... licencia creative commons by-nc-nd.  
   Mayo 2018 se convierten en ficheros .jar ejecutables en  
   [http://www.xtec.cat/~ocasella/applets/p-applets.htm](http://www.xtec.cat/~ocasella/applets/p-applets.htm)  (inicialmente solamente en catalán)
   *  [http://www.falstad.com/mathphysics.html](http://www.falstad.com/mathphysics)  
   These are some educational java applets I wrote to help visualize various concepts in math, physics, and engineering. java@falstad.comOscillations and Waves, Acoustics, Signal Processing, Electricity and Magnetism: Statics, Electrodynamics, Quantum Mechanics, Linear Algebra, Vector Calculus, Thermodynamics... 
   * Física con ordenador  
   [http://www.sc.ehu.es/sbweb/fisica/](http://www.sc.ehu.es/sbweb/fisica/)  
   Material con derechos de autor. © Ángel Franco García -1998-2011 (en julio 2014 indica Última actualización: Diciembre de 2010)  
   [http://www.sc.ehu.es/sbweb/fisica_/](http://www.sc.ehu.es/sbweb/fisica_/)   
   Nueva versión (en julio 2014 indica Última actualización. Enero 2013)  
   Nivel alto, pero algunas de las simulaciones son aplicables a Bachillerato.
   * Applets java de física. Walter  
   [http://www.walter-fendt.de/](http://www.walter-fendt.de/)  
   Inicialmente tenía variantes, la url da pistas. Los originales son en alemán, con algunas traducciones  
   Una s en la url indica que son traducciones a español (s=spanish)  
   Java 1.4 (tiene "14" en la url)  
   Java 1.6 (tiene "16" en la url)  
   HTML5/Javascript, sin dependencia de java 
   [http://www.walter-fendt.de/html5/phde/](http://www.walter-fendt.de/html5/phde/)  
   En español  [http://www.walter-fendt.de/html5/phes/](http://www.walter-fendt.de/html5/phes/)  (enlace validado 2021)  
   Material con derechos de autor © Walter Fendt, Prof. Ernesto Martin Rodriguez, Juan Muñoz, José Miguel Zamarro, Mario Alberto Gómez García (1998 - 2010).
   * Laboratorio Virtual de Física de NTNU (National Taiwan Normal University)  
   [http://www.phy.ntnu.edu.tw/java/index.html](http://www.phy.ntnu.edu.tw/java/index)  (original en inglés)  
   [http://www.phy.ntnu.edu.tw/ntnujava/](http://www.phy.ntnu.edu.tw/ntnujava/)  (nueva versión)  
   Autor: Fu-Kwun Hwang. Varios espejos en el mundo, algunos en español.  
   [ Mirror del Laboratorio Virtual de Física de NTNU. Autor: Fu-Kwun Hwang - teleformacion.edu aytolacoruna.es](http://teleformacion.edu.aytolacoruna.es/FISICA/document/applets/Hwang/ntnujava/indexH)  
   Traducción español por J.Villasuso.
   * [Simulaciones (applets) para los contenidos de los Currículos Extremeños de Física y Química de 1º y 2º de Bachillerato - grupoorion.unex.es](http://grupoorion.unex.es/web/) (Es un proyecto de 2004, enlace validado en 2022, aunque algunos externos no están operativos)  
   Proyecto de investigación de selección, explicación y traducción de simulaciones, realizado por Juan Rodríguez Fernández y avalado por María Isabel Suero López y Ángel Luis Pérez Rodríguez  
   * Laboratorio de física   
   [http://iris.cnice.mec.es/fisica/](http://iris.cnice.mec.es/fisica/)   
   Ministerio de Educación. Applets para 1º y 2º de Bachillerato, se puede elegir curso, tema y subtema
   * The Physics Classroom » Multimedia Studios  
   [http://www.physicsclassroom.com/mmedia](http://www.physicsclassroom.com/mmedia)  
   Animaciones sencillas, no interactivas.© 1996-2014 The Physics Classroom, All rights reserved.  
   [https://www.physicsclassroom.com/Physics-Interactives](https://www.physicsclassroom.com/Physics-Interactives)  
   Simuaciones interactivas, por categorías, por ejemplo [https://www.physicsclassroom.com/Physics-Interactives/Kinematics](https://www.physicsclassroom.com/Physics-Interactives/Kinematics) 
   * Temas de física desarrollados con java  
   [http://usuarios.multimania.es/pefeco/temas.html](http://usuarios.multimania.es/pefeco/temas)  
   Utiliza applets de otros lugares, indicando *"ORIGEN de los APPLETS Quiero aclarar que no somos los autores de los applets utilizados y, que han sido conseguidos en distintos lugares de la red, bajándolos directamente o, solicitándolos por correo a sus autores. En cualquier caso, haremos una referencia a los mismos y a sus lugares originales para que podáis haceros con ellos."*
   * Physclips, physics animations and film clips  
   [http://www.animations.physics.unsw.edu.au/](http://www.animations.physics.unsw.edu.au/)  
   School of Physics, Sydney. cc-by-nc-nd
   * [Animaciones de física - acer.forestales.upm.es _waybackmachine_](https://web.archive.org/web/20180114045647/http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones.html)   
   Teresa Martín Blas y Ana Serrano Fernández - Universidad Politécnica de Madrid (UPM) 
   * [Physics Applets And Flash Animations - ucalgary.ca](http://people.ucalgary.ca/~alphy/MAP/workinprogr/CalgaryApplets)   
   The applets and Flash animations listed below are part of the MAP (Modular Approach to Physics) tutorial at  [http://canu.ucalgary.ca/](http://canu.ucalgary.ca/)  MAP can be used at the high-school, and first and second-year university levels. Errores, vectores, cinemática, dinámica, 
   * [General Physics Animations - surendranath.org](https://surendranath.org/)  
   Developed by B.Surendranath Reddy, Hyderabad, India
   * High School Physics Online Applets  
   [http://ngsir.netfirms.com/englishVersion.htm](http://ngsir.netfirms.com/englishVersion.htm)  
   Author of the applets : Chiu-king Ng, a high school physics teacher in Hong Kong.Copyright: The author has the copyright on all the applets appearing in this web site (http://ngsir.netfirms.com). Without the author's consent, the applets cannot be used in any commercial activities.
   * Animations for Physics and Astronomy  
   [http://phys23p.sl.psu.edu/phys_anim/Phys_anim.htm](http://phys23p.sl.psu.edu/phys_anim/Phys_anim.htm)  
   by Dr. Michael R. Gallis, Penn State Schuylkill, cc-by
   * Physlets. Web-Based Interactive Physics ProblemsA CD to accompany the book Physlets: Teaching Physics with Interactive Curricular Material by Wolfgang Christian and Mario Belloni.  
   [http://webphysics.davidson.edu/applets/applets.html](http://webphysics.davidson.edu/applets/applets)  
   [http://webphysics.davidson.edu/physletprob/default.htm](http://webphysics.davidson.edu/physletprob/default.htm)  
   Except for the condition noted below, Physlets may be used for non-profit, educational purposes without requesting permission of Davidson College.
   * [Open Source Physics. Simulations. Search- compadre.org](http://www.compadre.org/osp/search/categories.cfm?t=SimSearch) Desarrolladas con Easy Java/Javascript Simulations  
   [Physlet Physics 3E - compadre.org](https://www.compadre.org/physlets/)  
   * [http://www.um.es/fem/EjsWiki/](http://www.um.es/fem/EjsWiki/) 
   * [Physics Flashlets - virginia.edu](http://galileoandeinstein.physics.virginia.edu/more_stuff/flashlets/)  
   Michael Fowler   
   * University of Oregon Virtual Laboratory. Physics Applets  
   [http://jersey.uoregon.edu/vlab/](http://jersey.uoregon.edu/vlab/)  
   Astrophysics, Energy and Environment, Mechanics, thermodynamics, 
   * Physics and Math Applets  
   [http://sourceforge.net/projects/physicsandmatha/](http://sourceforge.net/projects/physicsandmatha/)  
   Proyecto software libre (GPLv2) con código fuente de applets. El de óptica se ha portado a android (ver recursos de [óptica geométrica](/home/recursos/fisica/optica-geometrica) )
   *  [http://science.sbcc.edu/~physics/flash/](http://science.sbcc.edu/~physics/flash/)  
   Physics Flash Animations by Don Ion
   *  [http://www.upscale.utoronto.ca/GeneralInterest/Harrison/Flash/](http://www.upscale.utoronto.ca/GeneralInterest/Harrison/Flash/)   
   David M. Harrison, Dept. of Physics, Univ. of Toronto , Copyright © 2002 - 2011 David M. Harrison, cc-by-nc-sa
   *  [Recursos de Física - teleformacion.edu aytolacoruna.es](http://teleformacion.edu.aytolacoruna.es/FISICA/document/index.htm)  
   [Prácticas Física - teleformacion.edu aytolacoruna.es](http://teleformacion.edu.aytolacoruna.es/FISICA/document/practicas/practicas_indice.htm) 
   * Simulaciones 3-12 años, ciencia y matemáticas  
   [https://www.explorelearning.com/](https://www.explorelearning.com/) 
   *  [http://www.profisica.cl/materialaula/animaciones.html](http://www.profisica.cl/materialaula/animaciones) 
   * LiquidFun is a 2D rigid-body and fluid simulation C++ library for games based upon Box2D. It provides support for procedural animation of physical bodies to make objects move and interact in realistic ways.  
   [http://google.github.io/liquidfun/](http://google.github.io/liquidfun/) 
   * Fermat room  
   Juegos para un escape room, algunos relacionados con Física  
   [Almacén de acertijos de la Habitación de Fermat - fermatroom.com](http://fermatroom.com/fermat/acertijosclasificados/index)  
   Ejemplo  
   [Ali y la ola... de calor (paracaídas, tiro parabólico) - fermatroom.com](http://fermatroom.com/fermat/acertijosclasificados/varios/paraca/paraca)  
   * [Physics - sunflowerlearning](https://www.sunflowerlearning.com/packs#physics)  
   * [oPhysics: Interactive Physics Simulations](http://ophysics.com/)  
   * [Classroom simulations - Perimeter Institute](https://perimeterinstitute.ca/classroom-simulations)  
   * [Physics Interactives - Friedman](https://sites.google.com/k12.somerville.ma.us/friedmanscience/desmos)  
   * [Curso Interactivo de Física en Internet. Ángel Franco - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica3/)  
   * [Algodoo.com](http://www.algodoo.com/)  
   > Algodoo is a unique 2D-simulation software from Algoryx Simulation AB. Algodoo is designed in a playful, cartoony manner, making it a perfect tool for creating interactive scenes. Explore physics, build amazing inventions, design cool games or experiment with Algodoo in your science classes. Algodoo encourages students and children’s own creativity, ability and motivation to construct knowledge while having fun. Making it as entertaining as it is educational. Algodoo is also a perfect aid for children to learn and practice physics at home.  
   [physics interactives - afreeparticle](https://sites.google.com/view/afreeparticle/interactives)  
   Most of the content on this page was created using Desmos.  
   [Here you will find Desmos Simulations of varying quality that I have created to model primarily physics scenarios. daniel_hosey@pittsford.monroe.edu](https://docs.google.com/spreadsheets/d/e/2PACX-1vTu3C5_0CvE9MU8KsSr1wT4p1-Gp5cS_myaj2YZylWQNJXlTWgqaUCak9yYIik9jpJSL48W2dO_phCy/pubhtml?urp=gmail_link)  

##  Química

   * [Simulaciones Química - phet.colorado.edu](http://phet.colorado.edu/es/simulations/category/chemistry/general) 
   * [Virtual Chemistry and Simulations - acs.org](https://www.acs.org/content/acs/en/education/students/highschool/chemistryclubs/activities/simulations.html)  
   * [AACT American Association of Chemistry Teachers simulations](https://teachchemistry.org/classroom-resources/simulations)  
   * [ACS Middle School Chemistry: Remote Learning Assignments](https://www.middleschoolchemistry.com/remotelearning/)  
   * Virtual Chemistry lab  
   [http://www.infoplease.com/chemistry/simlab/](http://www.infoplease.com/chemistry/simlab/) 
   * Chemistry Experiment Simulations and Conceptual Computer Animations  
   [ChemDemos - uoregon.edu](https://chemdemos.uoregon.edu/)  
   Enlazadas desde [Thomas Greenbowe](https://chemistry.uoregon.edu/profile/tgreenbo/)  
   Chemical Education Research Group, Department of Chemistry, University of Oregon
   * Online Resources for Teaching and Learning Chemistry  
   [Virtual Labs - chemcollective.org](http://chemcollective.org/vlabs)  
   [Virtual Labs - chemcollective.oli.cmu.edu](https://chemcollective.oli.cmu.edu/vlab/vlab.php) **url operativa en 2022 cuando dominio org no funcionaba** 
   [Virtual Lab: Version HTML5 Video Walkthrough - chemcollective.org](http://www.chemcollective.org/chem/common/vlab_walkthrouh_html5.php)  
   [Virtual Lab: Version HTML5 Video Walkthrough -chemcollective.oli.cmu.edu](https://chemcollective.oli.cmu.edu/chem/common/vlab_walkthrouh_html5.php)  
   The Virtual Lab is an online simulation of a chemistry lab. It is designed to help students link chemical computations with authentic laboratory chemistry. The lab allows students to select from hundreds of standard reagents (aqueous) and manipulate them in a manner resembling a real lab. More information and offline downloads.Please scroll below to find our collection of pre-written problems, they have been organized by concept and ranked by difficulty.  
   Stoichiometry  
   Thermochemistry  
   Equilibrium  
   Acid-Base  
   Chemistry  
   Solubility  
   Oxidation/Reduction and Electrochemistry  
   Analytical Chemistry/Lab Techniques  
   The ChemCollective site and its contents are licensed under a Creative Commons Attribution 3.0 NonCommercial-NoDerivs License.
   * [Interactive chemistry simulations - chemdo](https://chemdo.org/a-level-home/)   
   * [Applets de QUÍMICA (Bachillerato) - deciencias.net](http://www.deciencias.net/proyectos/4particulares/quimica/index.htm)  
   Applets seleccionados, organizados y corregidos por Miguel Vaquero.
   * [Química en acción (flash) _waybackmachine_](https://web.archive.org/web/20121204000943/http://webs.ono.com/fisicaquimica3eso/fisicayquimica/Otros-ejercicios/animacionesflash/animacionesflash.html)  
   Colección de animaciones de NCSSM (North Carolina School of Science and Mathematics) (ver enlace general)
   * Laboratorio interactivo cambios de estado del agua, con material de laboratorio  
   [ Laboratorio interactivo cambios de estado del agua, con material de laboratorio (Flash) - cnice.mec.es](http://concurso.cnice.mec.es/cnice2005/63_el_agua/actividades/AG2_1.swf) 
   * [Simulations - teachchemistry.org](https://teachchemistry.org/periodical/simulations) 
   * [http://biomodel.uah.es/](http://biomodel.uah.es/)  
   asociado a bilología, también simulaciones y vídeos sobre QuímicaPor ejemplo  [http://biomodel.uah.es/lab/inicio.htm](http://biomodel.uah.es/lab/inicio.htm) 
   * [Visualizaciones en Química - uv.es](https://www.uv.es/quimicajmol/index)  
   Incluye simulaciones  
   [Simulaciones de Química con jsmol - uv.es](https://www.uv.es/quimicajmol/simulaciones/index)   
   * [Chemistry - sunflowerlearning](https://www.sunflowerlearning.com/packs#chemistry)  
   * [VLabQ: Laboratorio virtual de química](https://biblioteca.udgvirtual.udg.mx/jspui/handle/123456789/421)  
   Simulador interactivo de prácticas de Química que simula el efecto de cada uno de los procesos. Descarga gratuita.  
   Se incluye un enlace a programas-gratis.net que es un ejecutable que tiene una encuesta y luego permite descargar el simulador, que se ejecuta en local. En enlace se pueden ver propuestas de software similar [VLabQ programas-gratis-net](https://vlabq-laboratorio-virtual-quimica.programas-gratis.net/)    
   [VLabQ_1_0_0_1.exe](https://files.downloadprogramas.com/VLabQ_1_0_0_1.exe?id=24619)  
   * [Crocodile Chemistry](http://aertia.com/en/productos.asp?pid=330)  
   [Crocodile Chemistry 6.05 portable - archive.org](https://archive.org/details/crocodile-chemistry-6.05-portable)  
   [Crocodile Chemistry - emtic.educarex.es](https://emtic.educarex.es/recursos/conoce/laboratorio-de-quimica)  
   

##  Geogebra
Con geogebra también se pueden hacer simulaciones, asociadas a Física pero también química  


[Vicente Martín Morales](https://www.geogebra.org/u/vicmarmor)   

[Animaciones Física y Química - Rafael Cabrera](https://www.geogebra.org/m/bcdtuz3w)   

[Antonio González García](https://www.geogebra.org/u/onio72)  

[Enseñanza de la fisica a través de simulaciones](https://www.geogebra.org/m/TRa7qwhx)  
Elvira Martinez y Carlos Romero  

[oPhysics: Interactive Physics Simulations](https://ophysics.com)  
Varias categorías  
    [Kinematics](https://ophysics.com/k.html)  
    [Forces](https://ophysics.com/f.html)  
    Conservation  
    Waves  
    Light  
    [E & M](https://ophysics.com/em.html)  
    Rotation  
    Fluids  
    Modern  
    Drawing Tools  
    Fun Stuff  

##  Tecnología
 [https://animagraffs.com/](https://animagraffs.com/)  
 Visión 360º, muy gráfico.  
 Mayormente "cómo funcionan las cosas", algunas relacionadas con Física   
 [animagraffs.com solar-cell-module](https://animagraffs.com/solar-cell-module/)  
 [animagraffs.com iss-international-space-station](https://animagraffs.com/iss-international-space-station/)  
 [animagraffs.com hard-disk-drive](https://animagraffs.com/hard-disk-drive/)  
 [animagraffs.com loudspeaker](https://animagraffs.com/loudspeaker/)  
 [animagraffs.com human-eye](https://animagraffs.com/human-eye/)  
 ![](https://animagraffs.com/wp-content/uploads/preview-human-eye.jpg)  
 
 [simulator.io](https://simulator.io/)  Build and simulate logic circuits
 Build and simulate logic circuits.  
