
# Recursos mecánica

Página asociados a recursos mecánica, rama física que comprende   
* [Cinemática](/home/recursos/fisica/cinematica) : movimiento independiente causas/fuerzas. Fija conceptos y sirve introducción a dinámica.  
* [Estática](/home/recursos/fisica/mecanica/estatica) : equilibrio, acción de fuerzas cuerpos en ausencia movimiento.  
* [Dinámica](/home/recursos/fisica/dinamica) : leyes movimiento a partir fuerzas (causas)  
  
Se puede incluir también el  [movimiento oscilatorio](/home/recursos/fisica/movimiento-oscilatorio) , aunque al tratarlo se suele hacer conjuntamente la cinemática, dinámica y la energía  
  
Se puede relacionar también con  [experimentos y prácticas de laboratorio](/home/recursos/practicas-experimentos-laboratorio)  asociadas  
  
Esta página con el tiempo se debería desdoblar en muchas. Inicialmente dos documentos, me parecen interesantes a nivel didáctico para los docentes.  
  
[ Mecánica: cinemática y dinámica, Albert Gras Martí, cc-by. pdf 244 páginas](https://archive.org/details/2011MecanicaCinematicaYDinamica/mode/1up)  
  
Students’ difficulties with tension in massless strings. Part I.   
 [v55n1a4.pdf](http://www.scielo.org.mx/pdf/rmfe/v55n1/v55n1a4.pdf)   
REVISTA MEXICANA DE FÍSICA E 55 (1) 21–33 JUNIO 2009. S. Flores-García, L.L. Alfaro-Avena, J.E. Chávez-Pierce, J. Estrada, and J.V. Barrón-López...  
 
[Collection of Solved Problems in Physics - Mechanics - physicstasks.eu ](http://physicstasks.eu/en/physics/mechanics)  
This collection of Solved Problems in Physics is developed by Department of Physics Education, Faculty of Mathematics and Physics, Charles University in Prague since 2006.  
The Collection contains tasks at various level in mechanics, electromagnetism, thermodynamics and optics. The tasks mostly do not contain the calculus (derivations and integrals), on the other hand they are not simple such as the use just one formula. Tasks have very detailed solution descriptions. Try to solve the task or at least some of its parts independently. You can use hints and task analysis (solution strategy).  
Difficulty level  
Level 1 – Lower secondary level   
Level 2 – Upper secondary level   
Level 3 – Advanced upper secondary level   
Level 4 – Undergraduate level    
 
  
Students’ difficulties with tension in massless strings. Part II  
Muchos estudiantes de los cursos introductorios de mecánica presentan serias dificultades para comprender el concepto de fuerza como vector en el contexto de la tensión en cuerdas de masa despreciable. Una de las posibles causas es la falta de entendimiento funcional desarrollado durante las clases fundamentadas en una enseñanza tradicional. En este articulo presentamos una colección de este tipo de problemas de aprendizaje que tienen los alumnos pertenecientes a los cursos de física clásica y estática en la Universidad Estatal de Nuevo México, en la Universidad Estatal de Arizona y en la Universidad Autónoma de Ciudad Juárez. Estas dificultades de aprendizaje se obtuvieron durante una investigación conducida tanto en laboratorios como en el salón de clases. En esta segunda parte de la investigación se abordan problemas de entendimiento relacionados con el efecto del ángulo en la tensión y el argumento de “compensación”.  
Descriptores: Tensión; fuerzas en cuerdas; dificultades de aprendizaje; fuerza como una tensión.  
 [v55n1a15.pdf](http://www.scielo.org.mx/pdf/rmfe/v55n1/v55n1a15.pdf)   
REVISTA MEXICANA DE FÍSICA E 55 (1) 118–131 JUNIO 2009 S. Flores-García y otros.  
  
Universidad de Córdoba, Manuel R. Ortega Girón, Lecciones de física (PDFs gratuitos, bastantes lecciones de mecánica)  
 [http://www.uco.es/~fa1orgim/fisica/docencia/index.html](http://www.uco.es/%7Efa1orgim/fisica/docencia/index.html)   
  
Mecánica-FI2001  
Problemas Propuestos y Resueltos  
 [PPR_Mecanicaabril2011.pdf](http://www.cec.uchile.cl/cinetica/pcordero/Mecanica/PPR_Mecanicaabril2011.pdf)   
Kim Hauser Vavra  
Versión abril, 2011  

[Mecánica de bachiller (I) - lawebdefisica.com](https://www.lawebdefisica.com/apuntsfis/bachiller1web/)  

  
Mecánica Clásica, Segundo Curso (EIAE)   
 [MecClas.html](http://www.aero.upm.es/departamentos/fisica/PagWeb/asignaturas/MecClas/MecClas.html)   
Escuela Técnica Superior de Ingenieros Aeronáuticos, Universidad Politécnica de Madrid  
  
Mecánica 2, Segundo Curso (ETSIA)  
 [Mecanica2.html](http://www.aero.upm.es/departamentos/fisica/PagWeb/asignaturas/mecanica2/Mecanica2.html)   
Escuela Técnica Superior de Ingenieros Aeronáuticos, Universidad Politécnica de Madrid  
  
Problemas y ejercicios de Mecánica II  
Propuestos por los profesores de la asignatura en exámenes  
Compilados y resueltos por Manuel Ruiz Delgado  
Escuela Técnica Superior de Ingenieros Aeronáuticos  
Universidad Politécnica de Madrid  
21 de junio de 2010  
 [Problemas Mecanica 2.pdf](https://www.aero.upm.es/departamentos/fisica/PagWeb/asignaturas/mecanica2/prob/Problemas%20Mecanica%202.pdf)   
  
  
Consorcio Proingeniería de la Provincia de Buenos Aires  
Proyecto de Cursos Abiertos  
Mecánica  
 [m1.html](http://www.cienciaredcreativa.org/m1/m1.html)   
  
Física-I, 1 Ingeniería Química: incluye cinemática, estática y dinámica  
 [Temario-index.html](http://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/Temario-index.html)   
Javier Junquera, Universidad de Cantabria  
  
MecánicaCinemática y dinámicaAlbert Gras MartíCon la colaboración de:Azalea Gras Velàzquez [Física I_Módulo1_Mecánica. Cinemática y dinámica.pdf](http://openaccess.uoc.edu/webapps/o2/bitstream/10609/7682/7/F%C3%ADsica%20I_M%C3%B3dulo1_Mec%C3%A1nica.%20Cinem%C3%A1tica%20y%20din%C3%A1mica.pdf) 

## Mecánica analítica
Mecánica Analítica, Tercer Curso. Especialidad de Ciencias y Tecnologías Aeroespaciales (EIAE)  
 [MecAnalitica.html](http://www.aero.upm.es/departamentos/fisica/PagWeb/asignaturas/MecanicaAnalitica/MecAnalitica.html)   
Escuela Técnica Superior de Ingenieros Aeronáuticos, Universidad Politécnica de Madrid [Mec2 05 Intro Analitica 01.pdf](http://www.aero.upm.es/departamentos/fisica/PagWeb/asignaturas/mecanica2/transp/Mec2%2005%20Intro%20Analitica%2001.pdf) Introducción a la mecánica analítica  
 [IMA3.pdf](http://www.fisica.ru/dfmg/teacher/archivos/IMA3.pdf)   
Enrique Cantera del Río, copyright 2012MECÁNICA ANALÍTICA (4º Curso de CC. Físicas), UNED  
 [UNED | Facultad de Ciencias](http://www.uned.es/fac-fisi/cdrom_cfisicas/depart/fismatflu/actividades/asignaturas/mecana/)   
Programa:  
 Principios variacionales y ecuaciones de Lagrange  
 Ecuaciones de Hamilton  
 Transformaciones canónicas  
 Teoría de Hamilton-Jacobi  
 Teoría canónica de perturbaciones  

## Mecánica Langrangiana
 [Mecánica lagrangiana - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Mec%C3%A1nica_lagrangiana)   
 [Lagrangian mechanics - Wikipedia](https://en.wikipedia.org/wiki/Lagrangian_mechanics)   
 [Lagrangiano - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Lagrangiano)   
  
 [112733mats50.pdf](http://ocw.uv.es/ciencias/2/1-2/112733mats50.pdf)   
5\. Introducción a la Formulación Lagrangiana y Hamiltoniana  
Chantal Ferrer Roca 2008  

