
# Currículo Ampliación Física y Química 4º ESO (LOE)
  
Materia optativa en Madrid regulada en  [RESOLUCIÓN de 27 de junio de 2007, de la Dirección General de Ordenación Académica, sobre la optatividad en la Educación Secundaria Obligatoria derivada de la Ley Orgánica 2/2006, de 3 de mayo, de Educación](http://www.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&nmnorma=4653&cdestado=P) )  

## Introducción
La presente materia optativa está diseñada para su oferta en cuarto curso de la Educación Secundaria Obligatoria, y, especialmente, para aquellos alumnos que cursen el itinerario A.  
  
Los cambios sociales experimentados en los últimos siglos se deben en gran parte a los logros conseguidos por la ciencia y por la actividad de los científicos, sobre todo en aspectos relacionados con la salud, el medio ambiente y el desarrollo tecnológico.  
  
En un mundo cada vez más tecnificado, los ciudadanos deben tener competencia científica. La competencia científica es importante para comprender los problemas ambientales, médicos, económicos y de otro tipo a los que se enfrentan las sociedades modernas, que dependen enormemente del progreso tecnológico y científico. Además, el rendimiento de los mejores alumnos de un país en las materias científicas tiene repercusiones en el papel que el mismo desempeñe el día de mañana en el sector de las tecnologías avanzadas y en su competitividad internacional en general. Por el contrario, las deficiencias en competencia matemática y científica pueden tener consecuencias negativas para las perspectivas laborales y económicas de los individuos, así como para su capacidad de participar plenamente en la sociedad.  
  
La Física y química, junto con el resto de las materias que componen el conocimiento científico, aparece hoy en día como imprescindible para una sociedad, pues:  
  
- Forma parte de la cultura general, si por cultura entendemos el conjunto de conocimientos científicos, históricos, literarios y artísticos.    
- Proporciona las bases para comprender el desarrollo social, económico y tecnológico que caracteriza el momento actual que ha permitido al hombre alcanzar a lo largo del tiempo una mayor esperanza y calidad de vida.  
- Proporciona un evidente enriquecimiento personal porque despierta y ayuda a la formación de un espíritu crítico.  
- Es modeladora de valores sociales, precisamente por su propio carácter social.  
- Proporciona las bases del conocimiento y la práctica del método científico.  
- Permite a las personas intervenir con criterios propios en muchos de los grandes temas presentes en la sociedad actual: cambio climático, conservación del medio ambiente, biotecnología, energías renovables, etcétera.  
- Es la base de un gran número de salidas profesionales, correspondientes tanto a los ciclos formativos como a estudios universitarios.  
  
La materia optativa Ampliación de física y química permite a los alumnos profundizar en contenidos que se abordan de forma más general en la Física y química de cuarto curso y estudiar otros que le serán de utilidad para estudios posteriores. En cualquiera de los casos, esta materia enriquecerá tanto a los alumnos que finalizan sus estudios en esta etapa, como a aquellos que los continuarán en la secundaria postobligatoria.  
  
La idea de que la Física y la química, como todas las ciencias, tiene implicaciones con la tecnología y la sociedad debe ponerse de manifiesto en la metodología, planteando cuestiones teóricas y prácticas mediante las que el alumno comprenda que uno de los objetivos de la ciencia es determinar las leyes que rigen la naturaleza. El proceso de adquisición de una cultura científica, además del conocimiento y la comprensión de los conceptos, implica el aprendizaje de procedimientos y el desarrollo de actitudes y valores propios del trabajo científico. La realización de actividades prácticas y el desarrollo de algunas fases del método científico permitirán alcanzar habilidades que servirán de motivación para lograr nuevos conocimientos y poner en práctica métodos del trabajo experimental.  
  
En la Ampliación de física y química se van integrando en cada bloque, de forma contextualizada, aspectos relevantes del trabajo científico, estrategias, técnicas, habilidades y destrezas relacionadas con la metodología de la investigación científica.  
  
En los bloques 1, «Las fuerzas como causa de los cambios de movimiento», y 2, «Energía, trabajo y calor», se estudian el movimiento, las fuerzas y la energía desde el punto de vista mecánico; permiten mostrar el difícil surgimiento de la ciencia moderna y su ruptura con las visiones simplistas «del sentido común». Estos contenidos no deben abordarse como una mera aplicación mecánica de un conjunto de fórmulas y de cálculos, sino que requiere describir, comprender y analizar la realidad lo más acertadamente posible para que sea un referente en la vida adulta del alumnado y le ayude a interpretar las informaciones que pueda encontrar en estudios posteriores o en su vida como ciudadano. Se trata de comprender el carácter relativo del movimiento, de fomentar la observación y el análisis de los movimientos que se producen a nuestro alrededor y de apreciar la diferencia entre el significado científico y el significado coloquial.  
  
Por otra parte, el bloque 3, «El átomo y los cambios químicos», profundiza en el estudio de la estructura atómica, el enlace químico y la química orgánica, como nuevo nivel de organización de la materia, fundamental en los procesos vitales, y se valora la importancia de los compuestos del carbono, tanto en los seres vivos como en los materiales de uso cotidiano. En el apartado de las reacciones químicas, se debe resaltar la distinción entre cambio físico y químico, un modelo de reacción química y las leyes de las reacciones químicas y, además, comprender y valorar algunas reacciones químicas cotidianas relacionadas con la salud, la industria y el medio ambiente.  
  
Este currículo opta por una enseñanza y aprendizaje de la Física y química basada en el desarrollo de competencias en el alumnado y prepararlo para transferir los aprendizajes escolares a la vida cotidiana, explorar hechos y fenómenos cotidianos de interés, analizar problemas, observar, recoger y organizar información relevante. La investigación de problemas activará las capacidades básicas del individuo: Leer de manera comprensiva, reflexionar, identificar un problema, emitir hipótesis elaborar un plan de trabajo para resolverlo, recoger los resultados y verificar el ámbito de validez de las conclusiones, etcétera. Centrar la actividad de las ciencias físico-químicas en abordar la solución de problemas es una buena forma de convencer al alumnado de la importancia de pensar en lo que hace y en cómo lo hace.  
  
Por último, esta materia optativa contribuye al desarrollo de las competencias básicas de la etapa de forma paralela a la materia Ciencias de la naturaleza, por lo que le es de aplicación lo expresado al respecto en el currículo recogido en el Anexo del Decreto 23/2007, de 10 de mayo, por el que se establece para la Comunidad de Madrid el currículo de la Educación Secundaria Obligatoria.  

## Objetivos
La materia optativa Ampliación de física y química tendrá como finalidad la adquisición de las capacidades señaladas en los objetivos del currículo de Ciencias de la naturaleza de la Educación Secundaria Obligatoria, establecidos en el Anexo del Decreto 23/2007, de 10 de mayo, por el que se establece para la Comunidad de Madrid el currículo de la Educación Secundaria Obligatoria.  

## Contenidos


### Bloque 1. Las fuerzas como causa de los cambios de movimiento.
- Movimiento y sistema de referencia. Magnitudes necesarias para la descripción del movimiento. Desplazamiento, velocidad y aceleración; carácter vectorial de las mismas. Componentes intrínsecas de la aceleración y su relación con los cambios de velocidad. Movimiento rectilíneo y uniforme; composición de movimientos uniformes. Movimiento rectilíneo y uniformemente acelerado; movimiento vertical. Movimiento circular uniforme.  
- Interacciones entre los cuerpos: Fuerzas. Sus tipos. Carácter vectorial de las fuerzas. Leyes de la dinámica. Tratamiento cualitativo de la fuerza de rozamiento. Gravitación. Peso de los cuerpos.  
- Fuerzas en el interior de los fluidos. Presión. Principio fundamental de la estática de fluidos. Principio de Arquímedes. Fuerza de empuje; equilibrio de sólidos en fluidos. Presión atmosférica y meteorología.  

### Bloque 2. Energía: Sus formas y su transferencia.
- Trabajo mecánico. Trabajo realizado por fuerzas constantes. Trabajo de rozamiento.  
- Energía mecánica. Energía cinética y energía potencial gravitatoria. Principio de conservación de la energía mecánica.  
- Calor y transferencia de energía. Equivalente mecánico del calor. La temperatura: Escalas termométricas y termómetros. Equilibrio térmico.  
- Ondas: Transferencia de energía. Ondas mecánicas. Ondas longitudinales y transversales. Fenómenos ondulatorios: Reflexión y refracción. Aplicaciones al estudio de la luz y el sonido.  

### Bloque 3. El átomo y los cambios químicos.
- Estructura atómica. Número atómico y masa atómica. Masa molecular. Número de Avogadro. Cantidad de sustancia y mol. Ordenación de los elementos químicos. El enlace químico y el sistema periódico.  
- Formulación y nomenclatura de los compuestos binarios y ternarios de química inorgánica según normas de la IUPAC. Fórmulas y nombres de los ácidos oxácidos y sus sales más importantes. Formulación de compuestos orgánicos sencillos: Hidrocarburos, alcoholes, aldehídos, cetonas, ácidos orgánicos, compuestos aromáticos, compuestos de interés biológico.  
- Las reacciones químicas: Aspectos básicos. Calor de reacción. Reacciones de oxidación reducción y de combustión. Reacciones ácido base: Neutralización. Concepto de pH e indicadores de pH. Relaciones estequiométricas en las reacciones químicas.  

## Criterios de evaluación
1. Utilizar expresiones vectoriales en el estudio del movimiento, deducir y representar la ecuación de la trayectoria y relacionar matemáticamente términos como vector de posición, velocidad, aceleración y componentes intrínsecas de la aceleración.  
2. Diseñar, montar y realizar un experimento sencillo de laboratorio relativo al movimiento. Realizar un informe científico del mismo utilizando notación y metodología científica. Distinguir entre precisión y exactitud en las medidas y presentar tablas de valores con sus gráficos correspondientes.  
3. Aplicar conocimientos vectoriales a la composición de movimientos en direcciones perpendiculares y a la composición y descomposición de fuerzas.  
4. Aplicar las leyes de la dinámica y explicarlas con ejemplos de la vida cotidiana. Resolver problemas numéricos de fuerzas y relacionarlos con problemas de movimiento.  
5. Enunciar la Ley de Gravitación Universal y reconocer la actuación de fuerzas gravitatorias en el movimiento de planetas y satélites; hacer cálculos del peso de los cuerpos en diferentes planetas; hallar la fuerza resultante de varias fuerzas gravitatorias sobre una masa.  
6. Enunciar el Principio de Arquímedes. Diseñar, montar y realizar un experimento para el cálculo del empuje en fluidos. Relacionar diferentes resultados. Medir correctamente masas, volúmenes y densidades.  
7. Reconocer la presencia de trabajo en relación con la fuerza que se realiza, el desplazamiento y el ángulo que forman fuerza y desplazamiento. Conocer el trabajo de las fuerzas de rozamiento.  
8. Aplicar el principio de conservación de energía a la resolución de problemas. Identificar fenómenos de la vida ordinaria en los que se produce transferencia de energía mecánica.  
9. Explicar el concepto de calor como transferencia de energía en situaciones de cambio de temperatura. Reconocer la conservación de la energía en situaciones de intercambio de calor.  
10. Conocer el movimiento ondulatorio. Diseñar, montar y tomar medidas de fenómenos ondulatorios relacionados con ondas mecánicas, el sonido o la luz. Elaborar informes de las experiencias realizadas.  
11. Identificar las partículas que componen el átomo. Relacionar la distribución electrónica con la posición de los elementos en el sistema periódico. Predecir los tipos de sustancias químicas que producirán algunos elementos químicos y las propiedades de las mismas.  
12. Interpretar el lenguaje simbólico de la química y conocer la formulación de compuestos binarios, hidróxidos, oxácidos y las sales más comunes.  
13. Establecer la correspondencia entre masa molecular, mol, cantidad en gramos y número de partículas. Comprender y manejar magnitudes, unidades y constantes necesarias para el cálculo quí-mico.  
14. Distinguir diferentes tipos de reacciones químicas. Escribir y ajustar ecuaciones químicas. Realizar cálculos estequiométricos de masa y volumen.  
15. Realizar una experiencia en la que intervengan reactivos químicos sólidos y en disolución y realizarla con destreza y habilidad. Respetar normas de seguridad en el laboratorio. Redactar un informe.  

