---
aliases:
  - /home/recursos/quimica/Equilibrio-quimico/
---


# Equilibrio químico

Se intentan poner recursos asociados al bloque "equilibrio químico" del currículo de química de 2º de bachillerato, que contiene la parte de [cinética química](/home/recursos/quimica/cinetica-quimica)
Asociado a cinética química se puede ver la teoría cinético molecular  

* [Generador ejercicios equilibrio químico](http://www.educa2.madrid.org/web/educamadrid/principal/files/d063aae0-13c2-4a2b-b2ae-05710d4c255a/equilibrio_quimico_jquery.htm?t=1466098434752)  Vicente Martín Morales   
  
[Equilibrio químico - cica.es _waybackmachine_](https://web.archive.org/web/20170309113940/http://thales.cica.es/cadiz2/ecoweb/ed0765/)  
Práctica final realizada por Carlos Díaz Pérez de Perceval.Curso: DAMER: Diseño avanzado de material didáctico para su uso en la red. Incluye enlaces a recursos  

Tabla de constantes de equilibrio [http://www.vaxasoftware.com/doc_eduen/qui/kckp.pdf](http://www.vaxasoftware.com/doc_eduen/qui/kckp.pdf)  

Velocidades de reacción, java con cheerpj, PhET  
[![](https://phet.colorado.edu/sims/reactions-and-rates/reactions-and-rates-600.png)](http://phet.colorado.edu/es/simulation/reactions-and-rates](http://phet.colorado.edu/es/simulation/reactions-and-rates "Velocidades de reacción")  

Reacciones reversibles, java con cheerpj, PhET   
[![](https://phet.colorado.edu/sims/ideal-gas/reversible-reactions-600.png)](http://phet.colorado.edu/es/simulation/reversible-reactions "Reacciones reversibles")  

  
[ENSEÑANZA DE LAS CIENCIAS, 2006, 24(2), 219–240. ANÁLISIS DE PROBLEMAS DE SELECTIVIDAD DE EQUILIBRIO QUÍMICO: ERRORES Y DIFICULTADES CORRESPONDIENTES A LIBROS DE TEXTO, ALUMNOS Y PROFESORES. Quílez, Juan. Departamento de Física y Química. IES Benicalap. Valencia](http://www.raco.cat/index.php/ensenanza/article/viewFile/75828/96332)  

[The Best Equilibrium Simulation Ever - ti.com](https://education.ti.com/en/bulletinboard/2018/december/the-best-equilibrium-simulation-ever)  
Cita [connchem.org/simulations](https://www.connchem.org/simulations/) que son de pago  
En 2021 dejan las versiones java antiguas en descarga  
[connchem.org/legacy-java/](https://www.connchem.org/legacy-java/)  
[Download java legacy (zip)](https://connchem.org/wp-content/uploads/2019/09/ConnectedChemistry.zip)  
Contiene fichero ConnectedChemistry.jar que se puede ejecutar  
Tiene simulaciones visuales de equilibrios habituales en problemas Bachillerato: NO<sub>2</sub> y N<sub>2</sub>O<sub>4</sub>, PCl<sub>3</sub> y PCl<sub>5</sub>


##  Datos constantes equilibrios de gases
 [Búsqueda de datos de especies por Fórmula Química - webbook.nist.gov](http://webbook.nist.gov/chemistry/form-ser.html)  
 Permite buscar compuestos y para cada uno de ellos da información entre la que se incluye Cp, variación de entalpía y entropía con temperatura que permite obtener constantes  
 
## Datos productos de solubilidad
Relacionado con [disoluciones](/home/recursos/quimica/recursos-disoluciones)  

[Producto de solubilidad - es.wikipedia.org](https://es.wikipedia.org/wiki/Producto_de_solubilidad)   

[Ksp solubility constant for common salts - solubilityofthings.com](http://www.solubilityofthings.com/water/ions_solubility/ksp_chart.php)  

[Ksp Table. Chemistry 112. Spring 2018. General Chemistry Lecture II](https://www.chm.uri.edu/weuler/chm112/refmater/KspTable.html)  

[Solubility Products. Ksp values for many ionic compounds - General Chemistry. University of Texas](https://gchem.cm.utexas.edu/data/section2.php?target=ksp-constants.php)  

A nivel de NIST se puede consultar 
[IUPAC-NIST SOLUBILITY DATA SERIES IUPAC-NIST Solubility Database](https://srdata.nist.gov/solubility/index.aspx)  
[Search for Solubility Data by Two Components](https://srdata.nist.gov/solubility/comp2RegNos.aspx)  
Poniendo nombres CAS de soluto y disolvente, por ejemplo 106-95-6 3-bromoprop-1-eno y 7732-18-5 H2O  

##  Criterio para despreciar / aproximar en ecuaciones equilibrio
 [Equilibrio ácido-base - uclm.es _waybackmachine_](http://web.archive.org/web/20161120143954/http://www.uclm.es/profesorado/pablofernandez/QG-05-equilibrio%20acido%20base/equilibrio%20acido%20base.pdf#page=20) 

##  Unidades de las constantes de equilibrio Kc y Kp
Escribo esto en mayo de 2016: hasta ese momento yo mantenía que no tenían unidades, pero me desdigo y según el tiempo me permita iré actualizando apuntes y ejercicios.  

**[¿Tiene(n) unidades la(s) constante(s) de equilibrio? An. Quím. 2013, 109(1), 34–37 Ana Quílez-Díaz y Juan Quílez-Pardo](https://dialnet.unirioja.es/descarga/articulo/4208013.pdf)**  

[Definición y cálculo de las constantes de equilibrio en los libros de texto de Química general preuniversitarios y universitarios. Ana Quílez-Díaz , Juan Quílez-Pardo](http://www.raco.cat/index.php/Ensenanza/article/download/287556/375709)  
  
[Units of an Equilibrium Constant, To The Editor, Keith J. Laidier university of Ottawa](http://pubs.acs.org/doi/pdf/10.1021/ed067p88.1)  

  
[IUPAC. Quantities, Units and Symb ols in Physical Chemistry Third Edition, 2.11 CHEMICAL THERMODYNAMICS](https://iupac.org/wp-content/uploads/2019/05/IUPAC-GB3-2012-2ndPrinting-PDFsearchable.pdf#page=72) 

## ICE Chart (Initial, Change and Equilibrium)
El planteamiento numérico de equilibrio tiene una estructura que se llama "ICE chart" en inglés:
[Making an ICE Chart. An Aid in Solving Equilibrium Problems - purdue.edu](https://www.chem.purdue.edu/gchelp/howtosolveit/Equilibrium/ICEchart.htm)  

> An useful tool in solving equilibrium problems is an ICE chart.  
>   "I" stands for the initial concentrations (or pressures) for each species in the reaction mixture.  
>   "C" represents the change in the concentrations (or pressures) for each species as the system moves towards equilibrium.  
>   "E" represents the equilibrium concentrations (or pressures) of each species when the system is in a state of equilibrium.  

## Método sistemático y tratamiento condicional para los problemas de equilibrio (incluyendo ácido - base) 
[twitter DaviidMPB/status/1524449337828425729](https://twitter.com/DaviidMPB/status/1524449337828425729)  
A colación de los fantásticos materiales de FyQ que cuelga @fqmente, le pregunté si conocía el tratamiento condicional de los problemas de equilibrio, pues muchas veces simplifica enormemente su resolución. Como me dijo que no le sonaba, voy a intentar explicarlo (hilo largo 🧵).  
Para empezar hay que poner en contexto qué está ocurriendo desde el punto de vista químico en los problemas de equilibrio: generalmente, tenemos una (o varias) especies, que tienen reacciones entre sí o con el agua, y nos pide hallar su concentración y/o el pH, potenciales, etc.  
En 2º Bach, estos temas se resuelven con aproximaciones: en ác/base simplemente se hace un balance de materia despreciando especies minoritarias, y se sustituye en Ka; en redox ni se suele mencionar la ec de Nerst, y en prec ocurre algo similar a ác/base: sustituir en Kps.  
Hasta aquí todo me parece correcto, a nivel de bachillerato (incluso para muchos primeros cursos de grado) no veo necesario ir más allá: con esas aproximaciones se puede trabajar bien y obtener resultados con un error asumible.  
El problema es cuando llegan algunos problemas de Olimpiada y, sobre todo, de Oposición (o de universidad). Ahí la cosa no es tan sencilla. Para muestra, el enunciado aparentemente simplón de una cuestión de la ONE "Calcule el pH de una disolución de HCl 10^-8 M".  
Un estudiante al que no le hayan mencionado que todo lo que aplica en el tema de ác/base son aproximaciones, lo que hará será decir que \[H3O+] = 10^-8M, pues el HCl es un ác fuerte completamente disociado, pero el pH le saldrá 8,00... ¿cómo una disolución de HCl puede se básica?  
Aquí es donde entra en juego la herramienta más potente para la resolución de problemas de equilibrio: el método sistemático, que consiste en ver el número de incógnitas (concentraciones) que tenemos, y buscar un número de ec ≥ que el de conc que desconocemos.  
¿De dónde sacamos las ecuaciones? Pues de los balances de materia de las concentraciones de las especies de partida, de las constantes de equilibrio que intervengan, y del balance de cargas (pues las disoluciones son eléctricamente neutras). El resumen del método en la imagen.  
![](https://pbs.twimg.com/media/FSfum-pXEAMLqpA?format=jpg)  
Lo potente del método sistemático, es que da igual qué tipo de equilibrios estén involucrados, o si hay varios que están teniendo lugar. El problema se reduce a identificar las incógnitas, y plantear tantas ec como sean necesarias para resolver el sistema de ec.  
La mayor dificultad de este planteamiento, es que el sistema de ecuaciones resultante no va a ser lineal, por lo que no será tan fácil de resolver en un primer instante. Ahora bien, haciendo uso de magnitudes como la fracción molar, se reducen enormemente las dificultades.  
Como ejemplo, adjunto la resolución del problema del pH de la disolución de HCl, para que se vea en acción el tratamiento en un caso sencillo (sobre-explicado para que se entienda cada paso).  
![](https://pbs.twimg.com/media/FSeODiUXIAAHm9i?format=jpg)  
![](https://pbs.twimg.com/media/FSeOHlUWYAArDDM?format=png)  
![](https://pbs.twimg.com/media/FSeOPofWUAQQLzR?format=jpg)  
![](https://pbs.twimg.com/media/FSeOThwXsAAjW66?format=png)  
El tratamiento es análogo tanto si tenemos ác como bases, de hecho por eso me meto tanto con el concepto de "hidrólisis", al final tenemos incógnitas y ecuaciones, lo único que para bases es más fácil dejar todo en función de \[OH-].  
En algunas ocasiones el polinomio que te queda es de grado superior a cinco, por lo que no tiene una "fórmula" para poder sacar sus raíces. En ese caso o se intentan hacer diversas aproximaciones (más "leves" que las derivadas del tratamiento habitual), o se resuelve con Matlab.  
Por ejemplo, esta es la ecuación con la que (trabajando convenientemente) se puede llegar a un polinomio de quinto grado que nos daría la cantidad exacta de hidronios (y, por tanto, del resto de especies) de una dis de un anfolito del tipo HX^2-. Al lado, solución aprox y error.  
![](https://pbs.twimg.com/media/FSeP0i3WUAAEFTD?format=jpg)  
![](https://pbs.twimg.com/media/FSeP-fOXoAU79Pn?format=jpg)  
Y repito, este tratamiento es GENERAL para cualquier problema de equilibrio. Se podrá complicar más o menos, pero la metodología es siempre la misma, y no falla, permitiéndonos calcular el resultado exacto o bien hacer una (o varias, en muchos casos) aproximaciones.  
La cosa se complica cuando empezamos a encontrarnos problemas en los que intervienen más de un tipo de equilibrio. como en el archiconocido problema de cómo afecta el pH a la solubilidad del AgCl en presencia de NH3, o el problema de oposición que colgaba resuelto @fqmente  
Veamos un caso intermedio, en el que sólo hay equilibrio ácido/base y de complejos, en el que hemos preparado una disolución que contiene una cierta concentración de FeCl3 y de NaF. Siguiendo el tratamiento sistemático, tendríamos 7 incógnitas, y os aseguro que no es sencillo (*)  
(*) Sólo cuando las concentraciones de FeCl3 y NaF son iguales resulta relativamente fácil (quedando \[Fe3+] en función del pH). Para el caso general, la expresión se complica bastante (imagen de la derecha). Disculpad por si hay errores, he hecho el cálculo rápido.  
![](https://pbs.twimg.com/media/FSe4NppWUAEgV2-?format=jpg)  
![](https://pbs.twimg.com/media/FSe82KHXsAURYra?format=png)  
¿Qué hacemos entonces? ¿Nos tenemos que conformar con resolver situaciones sencillas con cierta "simetría"? Anders Ringbom no estaba de acuerdo, tenía algo que decir al respecto, y sustituyó el tratamiento sistemático clásico por el CONDICIONAL. Ahora bien, ¿qué significa?  
Existen dos tipos principales de constantes, por un lado las Termodinámicas (que son las que están en función de las actividades y en condiciones de fuerza iónica cero), y las Aparentes (que son las que utilizamos día a día, en términos de conc y fuerzas iónicas moderadas).  
Pues bien, Ringbom propone usar un artificio matemático que facilite los cálculos, y lo denomina "constante condicional". Estos "artificios" no son realmente una constante (dependen de las condiciones experimentales) pero nos permiten trabajar cómodamente y reducir nº incógnitas.  
Así, el concepto se fundamenta en que dada una reacción química que llamaremos "principal", tanto los reactivos como los productos pueden participar en otras reacciones químicas dando otras especies, y por tanto de acuerdo con Le Chatelier, ésto afectará al equilibrio principal.  
Estas reacciones se suelen denominar "secundarias" o "laterales", ahora bien, ¿cómo medimos el grado de participación de cada compuesto en sus respectivas reacciones secundarias? Para ello se define el coef de reacciones laterales (α), que es el inverso de la fracción molar (x).  
Se define α como el cociente entre la suma de las concentraciones de todas las especies que puedan generarse en esas reacciones laterales (lo que denotaremos como una concentración primada, \[']), entre la concentración analítica de la especie que participa en la reacc principal.  
![](https://pbs.twimg.com/media/FSfNwwsWYAEfDMH?format=png)  
![](https://pbs.twimg.com/media/FSfW05IWQAAyD8C?format=png)  
En este caso, se asume que ni el FeF^2+ ni el Fe^3+ tienen reacciones laterales, lo cual es cierto para el complejo, pero no tanto para el Fe^3+, el cual a pH moderadamente básicos empieza a formar hidróxidos (reacciona con OH^-). Se han despreciado para simplificar el ejemplo.  
El α de una especie nos dará cuenta del grado que dicho compuesto participa en otras reacciones (pues las concentraciones de posibles productos aumentarán). Lo que pasa es que la reacción principal la hemos modificado, y hemos puesto que participa en la especie F'.  
F' no es más que todas las especies posibles en las que convertirse el F^- (excepto el complejo, pues está en la reacción principal). La nueva "constante" del equilibrio es lo que se denomina constante condicional (que denotamos también con una prima, '), y es función del pH.  
![](https://pbs.twimg.com/media/FSfYnD4XIAAbUzy?format=png)  
![](https://pbs.twimg.com/media/FSfYuH5WUAMRsKB?format=png)  
Lo bueno, es que las constantes condicionales están relacionadas con las constantes aparentes que conocemos tan bien, ¿cómo? Pues jugando un poco con las ecuaciones, y usando el coeficiente de reacciones laterales, vemos cómo es su dependencia (imagen).  
![](https://pbs.twimg.com/media/FSfZs2fXIAIIg2m?format=png)  
De este modo, es fácil deducir de forma cualitativa cómo de favorable será la reacción principal planteada. Si pH es ácido, entonces αF será grande, provocando la cte' sea pequeña, perjudicando la complejación del hierro. Por Le Chatelier, se llega a la misma conclusión.  
Pero no sólo nos permite racionalizar un análisis cualitativo de la situación, si queremos resolver el problema de forma exacta, las cosas se simplifican enormemente, pues hemos pasado de tener 7 incógnitas a simplemente 3: \[Fe^3+], \[F'] y \[FeF2^+], quedando las ec necesarias:  
![](https://pbs.twimg.com/media/FSfd1eHXsAIW6Au?format=png)  
La solución del sistema es muy sencilla pues podemos igualar lo que es la concentración del complejo en cada caso, y sustituir en la cte condicional dejándolo todo en función del Fe^3+ o del F^-, quedándonos un polinomio de segundo grado muy sencillo.  
![](https://pbs.twimg.com/media/FSfeThkXsAY3jVh?format=png)  
Así, una vez el pH se conozca (suele ser un dato del problema) no tendremos más que calcular la cte condicional, (usando la cte aparente y los coeficientes de reacciones laterales) y sustituir en dicho polinomio sencillo, nada del "monstruo" que nos encontramos al principio.  
Una vez conocida \[Fe^3+], inmediatamente se conocen \[FeF^2+] y \[F'] por simples restas, y lo único que nos faltaría por determinar sería \[F^-] y \[HF]. El primero es inmediato una vez calculemos α (ver imagen), y el segundo también, una vez se conozca \[F^-].  
![](https://pbs.twimg.com/media/FSfgHC3WYAIAm_5?format=png)  
En resumen: un problema de aparentemente 7 incógnitas, lo hemos reducido a 3, parametrizando una de ellas  (el pH), y con "artificios" matemáticos que son lo que denominamos magnitudes condicionales ("constantes" y concentraciones), y coeficientes de reacciones laterales (α).  
Ahora, por fin, vamos al problema de oposición que resolvía @fqmente (siento la chapa, pero creo que era necesario para entender la filosofía del procedimiento en sí y por qué es necesario). En el problema se dan cita equilibrio de solubilidad y ácido/base.  
![](https://pbs.twimg.com/media/FSfg-vMWUAAAi3J?format=jpg)  
Vamos a aplicar el método sistemático (condicional) que hemos aprendido. La reacción principal es la de precipitación, y una de sus especies (el anion oxalato) tiene reacciones laterales con los hidronios, por lo que podemos expresar todo en términos de magnitudes condicionales.  
![](https://pbs.twimg.com/media/FSfjGjnWYAM9Ncz?format=jpg)   
De este modo, tenemos en nuestro problema tres incógnitas: \[Ca^2+], \[C2O4'] y s'. Una de las ecuaciones es la propia K'ps, y las otras dos no son más que las relaciones estequiométricas entre los compuestos y s' (en el tweet anterior, ya habíamos incorporado este resultado).  
![](https://pbs.twimg.com/media/FSfkFlFXsAEphXO?format=png)  
No tenemos más que calcular K'ps en las condiciones que nos digan para determinar la solubilidad molar (que será la cantidad real que se disuelva del compuesto), y con ese valor, inmediatamente obtenemos \[Ca^2+] y \[C2o4'] si estuviésemos interesados (no suele ser el caso).  
Para ello, habrá que poner K'ps en función de los coeficientes de reacciones laterales (en este caso, sólo se considera las reacciones del oxalato, aunque el calcio también puede reaccionar con los OH^- a pH básicos), quedando una expresión:  
![](https://pbs.twimg.com/media/FSfmbOMXsAsHXWH?format=png)  
Ésta sería la resolución general, y desde aquí podemos responder sencillamente a la pregunta del problema ¿cuál es la expresión que relaciona la solubilidad molar con \[H3O+]? No tenemos más que sustituir la expresión de αC2O4 en K'ps, y despejamos s'.  
![](https://pbs.twimg.com/media/FSfnJ7jWYAAZ6VW?format=png)  
Que, evidentemente, es lo mismo que en la resolución que han subido ellos. No obstante, (y aunque para este problema quizás términos de "rapidez" la diferencia no es muy significativa), trabajar con magnitudes condicionales de más sentido químico a lo que está ocurriendo.  
1) El oxalato tiene reacciones laterales ácido/base, por tanto, a un pH ácido las concentraciones de ácido oxálico e hidrogenoxalato serán relevantes, lo que matemáticamente se traduce en que su coeficiente de reacciones laterales aumentará a medida que el pH disminuya.  
1) Que αC2O4 aumente implica que la K'ps va a aumentar, equilibrio se desplazará más hacia la derecha, a la disolución del soluto (por Le Chatelier, diríamos que los productos se están consumiendo). De nuevo, matemáticamente con este tratamiento es más directo de ver.  
2) Si quisiéramos calcular la concentración analítica del resto de especies, si no tenemos cuidado puede que se confunda la S que ha utilizado @fqmente  con la solubilidad molar sin otros equilibrios, y puede llevarnos a errores ulteriores como a decir que \[C2O4^2-] = S.  
2) Con el tratamiento condicional, queda más claro y es más difícil meter la pata. Además, calcular el resto de concentraciones es bastante sencillo, pues s' = \[Ca^2+] = \[C2O4'], y con αC2O4 inmediatamente sale \[C2O4^2-]. De la otra forma, calcular \[C2O4^2-] resulta tan directo.  
3) Como espero haber dejado claro, este tratamiento resulta especialmente útil para casos complejos. Este problema podría estar al "límite", pues es relativamente sencillo (aunque tienes que jugar mucho con las ctes para llegar a la expresión final, planteando α es más directo).  
3) Sin embargo, para problemas en donde tenemos complejos-ác/base, o redox-complejos-prec  (donde más de una especie suele tener reacciones laterales), el tratamiento condicional simplifica enormemente la situación, y permite resolver de forma eficiente y rápida el problema.  
Como dije al principio, no creo que esto haya que explicarlo a nivel de Bachillerato, como mucho veo interesante comentarles la metodología del método sistemático (Nº Ec - Nº Incógnitas) a nivel de Olimpiada, yo lo hice el año pasado y creo que le fue de utilidad en la OEQ.  
Pero me parece que para la opo es una herramienta útil. Muchas veces veo resoluciones de problemas  como las de @Tuacademiaenlan
, y el trat clásico condena a realizar desde el minuto 1 aproximaciones (muchas veces sin darse cuenta de ello), y complica innecesariamente las cosas.  
Espero que haya quedado más o menos claro qué es el método sistemático, y en particular el método de las constantes condicionales. Si le sirve a algún opositor o futuro docente (para tener un bagaje extra o para preparación de Olimpiadas), pues bienvenido sea.  
Si alguien detecta algún error o errata, que cite el/los tweets en cuestión. Lo he escrito bastante rápido y sin mucha revisión, así que lo más probable es que haya algún gazapo, aunque he intentado que las explicaciones sean coherentes.  


[La conjetura volumétrica de sleepylavoisier - docentesconeducacion.es](http://www.docentesconeducacion.es/viewtopic.php?t=4014)  
![](http://www.docentesconeducacion.es/download/file.php?id=741&sid=b3aa48bd12697d2257617d36e89dd13c)   



## Visualización equilibrios

Mucha parte visual asociada a variaciones visuales con Châtelier

### Visualización equilibrio en general 

[![](https://img.youtube.com/vi/EAu-j9gYmXs/0.jpg)](https://www.youtube.com/watch?v=EAu-j9gYmXs "Unit 12 Segment 3: Equilibrium Demonstration") 

### NO2 vs N2O4 
Es un equilibrio habitual en problemas, y visual. NO2 es marrón y N2O4 es transparente   

[![](https://img.youtube.com/vi/tlGrBcgANSY/0.jpg)](https://www.youtube.com/watch?v=tlGrBcgANSY "NO2 and N2O4 equilibruim") 

[![](https://img.youtube.com/vi/zVZXq64HSV4/0.jpg)](https://www.youtube.com/watch?v=zVZXq64HSV4 "NO2 N2O4 Gas Equilibrium- LeChatelier's Principle Lab Part 4") 

### Bebidas carbonatadas / con burbujas

Equilibrio H2CO3 con CO2 y H2O  

[![](https://img.youtube.com/vi/x-Ny-ODz5BY/0.jpg)](https://www.youtube.com/watch?v=x-Ny-ODz5BY "Effect of Equilibrium to a Soft Drink | Cheran's Chemistry Task") 

[Principio de Le Chatelier Experimento ejemplo explicación - profesor10demates.com](https://www.profesor10demates.com/2017/03/principio-de-le-chatelier-experimento.html)  
El ácido carbónico se descompone en agua y dióxido de carbono  ...

[Prácticas de Química para Educación Secundaria](http://dpto.educacion.navarra.es/publicaciones/pdf/qui_dg.pdf)    
Página 96 ESTUDIO DEL EQUILIBRIO QUÍMICO, DESPLAZAMIENTO DEL MISMO Y SOLUBILIDAD DE UN GAS. BEBIDAS CARBÓNICAS  

[![](https://img.youtube.com/vi/K-Fc08X56R0/0.jpg)](https://www.youtube.com/watch?v=K-Fc08X56R0 "Tres experimentos de física que te volarán la cabeza")  
Ver primero **¿Porque la soda explota al agitarla?**

¿Por qué sale el gas disparado en las bebidas con burbujas? - Fikasi  
[![](https://img.youtube.com/vi/5l76vVTVt2M/0.jpg)](https://www.youtube.com/watch?v=5l76vVTVt2M "¿Por qué sale el gas disparado en las bebidas con burbujas? - Fikasi") 


### Deshacer hielo con sal 

[Why does salt melt ice?](https://antoine.frostburg.edu/chem/senese/101/solutions/faq/why-salt-melts-ice.shtml)  
> Adding salt to the system will also disrupt the equilibrium. Consider replacing some of the water molecules with molecules of some other substance. The foreign molecules dissolve in the water, but do not pack easily into the array of molecules in the solid. Try hitting the "Add Solute" button in the animation above. Notice that there are fewer water molecules on the liquid side because the some of the water has been replaced by salt. The total number of waters captured by the ice per second goes down, so the rate of freezing goes down. The rate of melting is unchanged by the presence of the foreign material, so melting occurs faster than freezing.  
> That's why salt melts ice.   

[¿Por qué la sal derrite el hielo? - espaciociencia.com](https://espaciociencia.com/por-que-la-sal-derrite-el-hielo)  
> Realmente el hielo no se funde, lo que se funde es la disolución de agua y sal. La sal por si sola nunca disminuiría el punto de congelación del agua, que seguirá siendo por debajo de 0ºC, pero la mezcla de sal y agua forma una disolución donde el punto de congelación ya no se sitúa en los cero grados, sino que a una temperatura inferior.  

[Sal y hielo - ciensacion.org](https://www.ciensacion.org/experimento_manos_en_la_masa/e5024cp_saltNIce.html)  
>  En la superficie del hielo, el derretimiento y el congelamiento generalmente están en equilibrio a unos 0˚ Celsius.  
> Agregar sal disuelve el agua y, por lo tanto, se reduce el ritmo al que las moléculas de agua se fijan al estado sólido, es decir se congelan, pero no reduce el ritmo al que las moléculas del hielo se separan para pasar al estado líquido (fusión).  

La mezcla de Sal y Hielo también aparece en problemas de selectividad de Tecnología Industrial II  
[electividad Tecnología Industrial II BloqueA Materiales Diagramas](https://blogsaverroes.juntadeandalucia.es/sierramagina/files/2016/01/BloqueA-materiales-diagramas.pdf)  
> 5.- En un puerto de montaña cuya temperatura ambiente es de -10 º C, el servicio de mantenimiento
de carreteras arroja sal sobre ellas para conseguir fundir el hielo. Se desea saber, con la ayuda del
diagrama de fases adjunto:  
¿ Qué cantidad relativa, o porcentaje en peso de sal (NaCl) mínimo, deberá tener la mezcla para
conseguir que todo el hielo se funda?.  
Con un camión de 1000 Kg de sal ¿qué cantidad de hielo se puede llegar a fundir a dicha
temperatura?  
(Selectividad andaluza septiembre 98)  

[![](https://img.youtube.com/vi/K-Fc08X56R0/0.jpg)](https://www.youtube.com/watch?v=K-Fc08X56R0 "Tres experimentos de física que te volarán la cabeza")  
Ver tercero **¿El hielo se derrite más rápido en agua fresca o salada?**

### Otros

Complejos disuelto, variación con temperatura y añadiendo NaCl y añadiendo AgNO3  
[![](https://img.youtube.com/vi/cu4rxa2conc/0.jpg)](https://www.youtube.com/watch?v=cu4rxa2conc "Equilibrium Le Chatelier Demonstration Video") 

[![](https://img.youtube.com/vi/7sPXK842CR0/0.jpg)](https://www.youtube.com/watch?v=7sPXK842CR0 "EXPERIMENTO CASERO EQUILIBRIO QUIMICO PRINCIPIO DE LE CHATELIER") 


[twitter StrathavenChem/status/1629164900084862980](https://twitter.com/StrathavenChem/status/1629164900084862980)  
Le Chatelier's Principle in action today with the Higher Chemists, an excellent practical to check pupils understanding of recent lessons on equilibrium  
Cobalt chloride solution made using water and conc hydrochloric acid. Equilibrium can then be shifted by heating/cooling or addition of sodium chloride  

## Efecto ion común 

[Determine the concentration of an unknown NaCl solution - chemcollective.org](http://chemcollective.org/activities/autograded/122)  
[Determine the concentration of an unknown NaCl solution - chemcollective.oli.cmu.edu](https://chemcollective.oli.cmu.edu/vlab/122)  **url operativa en 2022 cuando dominio org no funcionaba** 
> In this activity, you must determine the amount of NaCl in an unknown solution, using measurements of \[Ag+\].  
The Ksp for Silver Chloride is: 1.830e-10.  

Con la simulación se conoce la concentración de ion Ag en equilibrio, pero no la del ion común Cl, y sería plantear equilibrio y averiguarla.  

##  Símbolos para indicar equilibrio
En [Quantities, Units and Symbols in Physical Chemistry (IUPAC Green Book), 3rd Edition 2012 2ndPrinting PDF](https://iupac.org/wp-content/uploads/2019/05/IUPAC-GB3-2012-2ndPrinting-PDFsearchable.pdf)  
se indica en 2.10 General Chemistry (iv) Equations for chemical reactions  
H<sub>2</sub> + Br<sub>2</sub> = 2HBr stoichiometric equation  
H<sub>2</sub> + Br<sub>2</sub> → 2HBr net forward reaction  
H<sub>2</sub> + Br<sub>2</sub> ⇄ 2HBr reaction, both directions  
H<sub>2</sub> + Br<sub>2</sub> ⇌ 2HBr equilibrium  
The two-sided arrow ↔ should not be used for reactions to avoid confusion with resonance structures (see Section 2.10.1 (iii), p. 50)  

Parte técnica:  
[http://unicode-table.com/es/sets/arrows-symbols/](http://unicode-table.com/es/sets/arrows-symbols/)  
El símbolo de reacción completa → es unicode 2192
El símbolo de reacción ⇄ es unicode 21C4 (también está en tipo de letra Windings 3)  
El símbolo de equilibrio ⇌ es unicode 21CC  
El símbolo de resonancia ↔ es unicode 2194  


