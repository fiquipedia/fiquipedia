# MCU (Movimiento Circular Uniforme)  

Esta es una subpágina de la página de [cinemática](/home/recursos/fisica/cinematica) para descargarla centralizando aquí recursos solo sobre MRU, que pueden incluir experiencias o [prácticas experimentos de laboratorio](/home/recursos/practicas-experimentos-laboratorio) asociadas, por lo que puede haber información también, a veces duplicada, en otras páginas.  

Dado que el MRU es un caso de MCUA con aceleración angular cero, puede haber recursos de MCUA que puedan utilizarse, por ejemplo gráficas ángulo-tiempo y velocidad angular-tiempo en la que se pueda fijar aceleración angular a cero.

Conceptos de magnitudes angulares  
[Radián  | Educaplus](http://www.educaplus.org/game/radian)  
[Radián. Cuerda en rueda girando - desmos.com](https://www.desmos.com/calculator/zwnjjuxx7w?lang=es)  

[Periodo y frecuencia en el MCU  | Educaplus](http://www.educaplus.org/game/periodo-y-frecuencia-en-el-mcu)  
[Movimiento circular  | Educaplus](http://www.educaplus.org/game/movimiento-circular)  

[Movimiento circular uniforme  | Educaplus](http://www.educaplus.org/game/movimiento-circular-uniforme)  
[movimientos-circulares - educaplus](http://www.educaplus.org/game/movimientos-circulares)  

[Movimiento Circular Uniforme](http://www.walter-fendt.de/html5/phes/circularmotion_es.htm)  
[Uniform circular motion](http://physics.bu.edu/~duffy/HTML5/circular_motion.html)  

[Simulation - Uniform Circular Motion](https://aplusphysics.com/educators/simulations/ucm.html)  
[Rotation - phet.colorado.edu](https://phet.colorado.edu/en/simulation/legacy/rotation)  

[Movimiento Circular Uniforme - geogebra](https://www.geogebra.org/m/MZRKudEF) Miguel Lara  

