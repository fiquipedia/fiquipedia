
# Recursos química: fuegos artificiales, pirotecnia, ensayos a la llama...

##   Fuegos articiales  

 [Fundamento científico de los artificios pirotécnicos, Revista Eureka sobre Enseñanza y Divulgación de las Ciencias - 2013, 10(2) pp. 273-281](http://rodin.uca.es/xmlui/handle/10498/15121)  
>Los espectaculares efectos que producen los artículos pirotécnicos (luz, color, sonido y humo) están fundamentados en la ciencia del fuego, por lo que es un atractivo recurso didáctico con el que conseguir aumentar: el interés, la imaginación y la admiración de los estudiantes hacia la química, así como el conocimiento científico de los ciudadanos en eventos científicos divulgativos.  
>Fernando Ignacio de Prada Pérez de Azpeitia, cc-by  
[Fundamento científico de los artificios pirotécnicos - uca.es](https://revistas.uca.es/index.php/eureka/article/view/2839)  

[El fuego, química y espectáculo, Anales de la Real Sociedad Española de Química, ISSN 1575-3417, Nº. 2, 2006 , págs. 54-59](http://dialnet.unirioja.es/servlet/articulo;jsessionid=08EE03687F4809F21FF784117CD41CB2.dialnet02?codigo=2006399)  
Fernando Ignacio de Prada Pérez de Azpeitia  

[The Chemistry of Sparklers](http://www.compoundchem.com/2014/11/04/sparklers/)  

[![The Chemistry of Sparklers poster](http://www.compoundchem.com/wp-content/uploads/2014/11/The-Chemistry-of-Sparklers-1024x724.png "The Chemistry of Sparklers poster")](http://www.compoundchem.com/wp-content/uploads/2014/11/The-Chemistry-of-Sparklers-1024x724.png)  

[The Chemistry of Fireworks](http://www.compoundchem.com/2013/12/30/the-chemistry-of-fireworks/)  

[![The Chemistry of Fireworks poster](http://www.compoundchem.com/wp-content/uploads/2013/12/Chemistry-of-Fireworks-POSTER-724x1024.png "The Chemistry of Fireworks poster") ](http://www.compoundchem.com/wp-content/uploads/2013/12/Chemistry-of-Fireworks-POSTER-724x1024.png)  

[https://twitter.com/jsdiaz_/status/550330179659526145](https://twitter.com/jsdiaz_/status/550330179659526145)  
Fuegos artificiales: un impresionante espectáculo de química y física

[![Jorge S. Diaz ‏@jsdiaz_ Fuegos artificiales: un impresionante espectáculo de química y física](https://pbs.twimg.com/media/B6MqYbeCMAIov5s.jpg "Jorge S. Diaz ‏@jsdiaz_ Fuegos artificiales: un impresionante espectáculo de química y física") ](https://pbs.twimg.com/media/B6MqYbeCMAIov5s.jpg)  

##   Ensayos a la llama
[Metal Ion Flame Test Colours Chart](http://www.compoundchem.com/2014/02/06/metal-ion-flame-test-colours-chart/)  

[![Metal Ion Flame Test Colours Chart poster](http://www.compoundchem.com/wp-content/uploads/2014/02/Metal-Ion-Flame-Test-Colours-Jan-15-724x1024.png "Metal Ion Flame Test Colours Chart poster") ](http://www.compoundchem.com/wp-content/uploads/2014/02/Metal-Ion-Flame-Test-Colours-Jan-15-724x1024.png)  

[![RC Unit 4 Demo - Metal Salt Flame Test Using Methanol](https://img.youtube.com/vi/9oYF-HxtoYg/0.jpg "RC Unit 4 Demo - Metal Salt Flame Test Using Methanol")](https://www.youtube.com/watch?v=9oYF-HxtoYg)

Versión en GIF animado
[![https://31.media.tumblr.com/829c4b1b78a274493371cbf1110aa569/tumblr_n9zz4vaIol1tfhbyyo1_500.gif](https://31.media.tumblr.com/829c4b1b78a274493371cbf1110aa569/tumblr_n9zz4vaIol1tfhbyyo1_500.gif ) ](https://31.media.tumblr.com/829c4b1b78a274493371cbf1110aa569/tumblr_n9zz4vaIol1tfhbyyo1_500.gif)  

[![Flame Tests for Unknowns FlinnScientific](https://img.youtube.com/vi/ZsvsptBQUVQ/0.jpg "Flame Tests for Unknowns FlinnScientific")](https://www.youtube.com/watch?v=ZsvsptBQUVQ)

[![Flame Test Lab dchummer](https://img.youtube.com/vi/o3nn4zqzf6M/0.jpg "Flame Test Lab dchummer")](https://www.youtube.com/watch?v=o3nn4zqzf6M)

[![Cómo hacer fuego de colores (Experimentos Caseros)](https://img.youtube.com/vi/Qwxsv-bR848/0.jpg "Cómo hacer fuego de colores (Experimentos Caseros)")](https://www.youtube.com/watch?v=Qwxsv-bR848)



