---
aliases:
  - /home/recursos/fisica/recursos-dinamica/
---

# Recursos dinámica

## General
En general junto con la dinámica se incluye el tratamiento de fuerzas en general, y a veces incluye parte de estática.  
Puede combinar elementos de  [cinemática](/home/recursos/fisica/cinematica) , estar asociado a muchos bloques relacionados con fuerzas, en distintos niveles:  
- [Gravitación](/home/recursos/fisica/recursos-gravitacion)  
- [Presión](/home/recursos/presion)  /  [fuerzas en fluidos](/home/recursos/fisica/fuerzas-en-fluidos).  
- [Campo eléctrico](/home/recursos/fisica/recursos-campo-electrico)   

La dinámica es algo que se trata en  [Física y Química 2º ESO (LOMCE)](/home/materias/eso/fisica-y-quimica-2-eso) ,  [Física y Química 3º ESO (a partir de LOMCE)](/home/materias/eso/fisica-y-quimica-3-eso) ,  [Física y Química de 4º ESO](/home/materias/eso/fisicayquimica-4eso)  y en  [Física y Química de 1º de Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato)  

En la PAU de Física de Andalucía sí hay problemas "limitados" a dinámica.  
Asociados a  [mecánica (2º Bachillerato LOGSE)](/home/materias/bachillerato/mecanica-2-bachillerato)  y PAU de mecánica hay ejercicios de dinámica  

Puede haber recursos asociados a tipos de fuerzas: gravitación, electricidad, magnetismo ...  

En 2014 creo esta página colocando un primer borrador de apuntes de elaboración propia, con la misma idea que otros de que en ciertas situaciones no sea imprescindible un libro de texto  

Acercándonos al LHC, física en el LHC, dinámica. Datos adaptados a secundaria. Relacionado con  [física de partículas](/home/recursos/fisica/fisica-de-particulas) .  

[http://www.lhc-closer.es/2/4/2/0](http://www.lhc-closer.es/2/4/2/0)  Fuerza centrípeta, acción y reacción en el LHC.  

[Dinámica de la partícula](https://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/5.Dinamica_de_la_particula.pdf)  


GENMAGIC.ORG Banco de Objetos Multimedia Educativos  
SEGUNDA LEY DE NEWTON, generador de problemas  
[http://www.genmagic.net/repositorio/displayimage.php?album=11&pos=19](http://www.genmagic.net/repositorio/displayimage.php?album=11&pos=19)  

[http://www.gobiernodecanarias.org/educacion/3/Usrn/fisica/dinamica.htm](http://www.gobiernodecanarias.org/educacion/3/Usrn/fisica/dinamica.htm)  (nivel 1º Bachillerato)  

Dinámica dentro del proyecto Newton  
[http://recursostic.educacion.es/newton/web/materiales_didacticos/dinamica/index.htm](http://recursostic.educacion.es/newton/web/materiales_didacticos/dinamica/index.htm)  

Curso Interactivo de Física en Internet © Ángel Franco García  
Curso Interactivo de Física en Internet problemas dinámica sc.ehu.es/sbweb/fisica_](http://www.sc.ehu.es/sbweb/fisica_/problemas/problemas.html#dinamica)  
[Curso Interactivo de Física en Internet dinámica sc.ehu.es/sbweb/fisica3](http://www.sc.ehu.es/sbweb/fisica3/dinamica/portada.html)  

  
[Problemas de Dinámica del punto. José Carlos Vilches Peña, con soluciones (no resolución)](https://sites.google.com/site/jvilchesp/fisica/cou/dinamica)    


Cesar Antonio Izquierdo Merlo, Universidad de San Caros de Guatemala. Lista de vídeos, algunos sobre dinámica  
[https://www.youtube.com/user/IzquierdoCesar/playlists](https://www.youtube.com/user/IzquierdoCesar/playlists)  

PW2 Forces  
[https://padlet.com/collabscience/q08w0lkhzhjj/wish/352581459](https://padlet.com/collabscience/q08w0lkhzhjj/wish/352581459)  

4º ESO Dinámica  
[http://www.catedu.es/cienciaragon/index.php?option=com_content&view=article&id=21:42-dinamica&catid=76:fq-4oeso&Itemid=48](http://www.catedu.es/cienciaragon/index.php?option=com_content&view=article&id=21:42-dinamica&catid=76:fq-4oeso&Itemid=48)  

También se pueden hacer test con kahoot (necesita estar registrado)  
Un ejemplo es   
[Las fuerzas 2º ESO - create.kahoot.it](https://create.kahoot.it/details/las-fuerzas-2-eso/9b99986d-ff67-4be9-85cd-f1fd3e8fc218) 

[Dinámica - aprendiendofisicaconbertotomas.com](http://bit.ly/3m77SDM)  

## Montaña rusa

[![](https://img.youtube.com/vi/4q2W5SJc5j4/0.jpg)](https://www.youtube.com/watch?v=4q2W5SJc5j4 "The Real Physics of Roller Coaster Loops. 
Art of Engineering")  

## Ejercicios
Ver los recursos de elaboración propia enlazados al final de la página  

[Problemas dinámica I. DINÁMICA DEL PUNTO (ROZAMIENTO NULO) - fisquiweb.es](http://www.fisquiweb.es/Apuntes/Apuntes1Bach/ProbDin1.pdf)  
[Problemas dinámica II. DINÁMICA DEL PUNTO (SISTEMAS CON ROZAMIENTO) - fisquiweb.es](http://www.fisquiweb.es/Apuntes/Apuntes1Bach/ProbDin2.pdf)  

Asociados a  [pruebas libres de graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)  hay ejercicios de dinámica  

[Problemas de Momento lineal y colisiones. Javier Junquera - unican.es](https://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/Colisiones-problemas.html)  
[Problemas de Dinámica de la partícula. Javier Junquera - unican.es ](https://personales.unican.es/junqueraj/JavierJunquera_files/Fisica-1/Dinamica-particula-problemas.html) Incluye Colecciones de problemas de Dinámica elaborada por el Prof. José Javier Sandonís 


Ejercicios dinámica (4º ESO y 1º Bachillerato)  
[http://profesor10demates.blogspot.com.es/2012/12/ejercicios-y-problemas-resueltos-de.html](http://profesor10demates.blogspot.com.es/2012/12/ejercicios-y-problemas-resueltos-de.html)  
Ejercicios básicos 2ª ley [http://genmagic.net/repositorio/albums/userpics/gnewton1c.swf](http://genmagic.net/repositorio/albums/userpics/gnewton1c.swf) Ver al final de la página adjuntados ejercicios de elaboración propia: no se agrupan para un nivel concreto sino por contenidos, se trata de elegir ejercicios según nivel y grupo. [https://www.guao.org/sites/default/files/C.2 Estudio de la dinámica_0.pdf](https://www.guao.org/sites/default/files/C.2%20Estudio%20de%20la%20din%C3%A1mica_0.pdf) 

## Leyes de Newton
[https://archive.org/details/newtonspmathema00newtrich](https://archive.org/details/newtonspmathema00newtrich)  
Newton's Principia : the mathematical principles of natural philosophy by Newton, Isaac, Sir, 1642-1727; Chittenden, N. W. Life of Sir Isaac Newton; Adee, Daniel, ca. 1819-1892. (1846) bkp CU-BANC; Motte, Andrew, d. 1730; Hill, Theodore Preston. Early American mathematics books. CU-BANC  
[https://archive.org/details/principia00newtuoft](https://archive.org/details/principia00newtuoft)  (latín)  

Se puede ver este post en modo de humor  
[No podemos poner eso](https://algoquedaquedecir.blogspot.com/2018/03/no-podemos-poner-eso.html)  
Que enlaza otros más técnicos, que surgen en Twitter  
[https://twitter.com/DivulgaMadrid/status/1056946972970663936](https://twitter.com/DivulgaMadrid/status/1056946972970663936)  
Es mucho más elegante definir sistema inercial como aquel en el que el tiempo es homogéneo y el espacio homogéneo e isótropo. Y de ahí demostrar que en ellos se cumple la ley de inercia, como hace Landau.(se incluye un vídeo)  

[twitter elprofedefisica/status/1056980083867557888](https://twitter.com/elprofedefisica/status/1056980083867557888)  
Fun fact: la 2ª Ley de Newton es F=dp/dt. Ahora bien, como p=mv, se sigue que F=m*dv/dt + v*dm/dt. El primer término es ma; el segundo indica la variación de la masa. En el mundo no relativista m=cte, así que dm/dt=0; en Relatividad, a altas v, dm/dt>>m*dv/dt y v aumenta despacio  

[https://cuentos-cuanticos.com/2018/11/03/leyes-de-newton-no-hay-mas-que-una/](https://cuentos-cuanticos.com/2018/11/03/leyes-de-newton-no-hay-mas-que-una/)  

[Las Leyes de Newton de la mecánica: Una revisión histórica y sus implicaciones en los textos de enseñanza - uv.es](https://ojs.uv.es/index.php/dces/article/view/2241/3323)  
José Sebastiá M. Sebastiá  
Departamento de FísicaUniversidad Simón Bolívar (Venezuela)  
DIDÁCTICA DE LAS CIENCIAS EXPERIMENTALES Y SOCIALES. N.º 27. 2013, 199-217 (ISSN 0214-4379)  

[twitter MientrasEnFisic/status/1066798865549049857](https://twitter.com/MientrasEnFisic/status/1066798865549049857)  
[![](https://pbs.twimg.com/media/Ds4H6eEWsAUWrCR.jpg "") ](https://pbs.twimg.com/media/Ds4H6eEWsAUWrCR.jpg)  

[twitter fqsaja1/status/1355814534053822465](https://twitter.com/fqsaja1/status/1355814534053822465)  
Hace mucho que no presentaba una lámina-cuestión:  
Un clásico. La pesada bola cuelga de un hilo. En su parte inferior dispones de otro hilo del que se puede tirar. Qué ocurre?? Interesante-reproducible-discutible para las clases de 4º. Si os resulta útil traeré más...  
Hay ejemplos en la red para aburrir...este es uno y con un segundo ejemplo explicativo, 
[![](https://img.youtube.com/vi/Wn-IXgxNSiE/0.jpg)](https://www.youtube.com/watch?v=Wn-IXgxNSiE)  
Si queréis artículos, también los hay 

### Primera ley de newton. Inercia
 [twitter ZonePhysics/status/1080062639395561473](https://twitter.com/ZonePhysics/status/1080062639395561473)  
 Best Demonstration of inertia ever.....(con hojas secas en cama elástica)  
 [![https://www.youtube.com/watch?v=KYZA3Vxo0wk](https://img.youtube.com/vi/KYZA3Vxo0wk/0.jpg)](https://www.youtube.com/watch?v=KYZA3Vxo0wk)  
 
 [twitter javyfeu/status/1186853927536361474](https://twitter.com/javyfeu/status/1186853927536361474)  
 Simpática y clara forma de mostrar la primera ley de #Newton o Inercia. Un cuerpo tiende a permanecer en el estado de movimiento en el que estaba (con velocidad constante o en reposo) a menos que una fuerza externa lo modifique. #physics #fisica #CienciaEnTuVida #enseñanzafisica(con un dinosaurio de juguete, una bola y una cama elástica)  
 
 [twitter Rainmaker1973/status/1321398841694978049](https://twitter.com/Rainmaker1973/status/1321398841694978049)  
 Newton's first law visualized by an oddly satisying jump into a snowy fence  [https://buff.ly/2LXbEgt](https://buff.ly/2LXbEgt)  
[source of the gif:  [http://bit.ly/2iHJNTB](http://bit.ly/2iHJNTB) ]  

[https://www.cuantarazon.com/1050698/inercia](https://www.cuantarazon.com/1050698/inercia)  
![](https://statics.memondo.com/p/99/crs/2017/09/CR_1050698_02e12608c5dc4e04951a2945bbc006ba_inercia.jpg?cb=4075366)  

Sistemas de referencia inerciales  
[twitter FIKASI1/status/1522297022883237891](https://twitter.com/FIKASI1/status/1522297022883237891)  
Cuando explicáis la dinámica del movimiento circular uniforme, ¿explicáis desde el punto de vista del observador inercial, no inercial (fuerza centrífuga) o ambas?   
[twitter currofisico/status/1522316147210788866](https://twitter.com/currofisico/status/1522316147210788866)  
Depende del grupo. Si intuyo que me voy a meter al grupo en un fangal solo inercial. Pero cuando el grupo se interesa pues por ejemplo hago este refritillo, pero no solo para el circular.  

[DINAMICA_11_SECUENCIA_APRENDIZAJE.PDF 1º Bachillerato - currofisico.es](https://drive.google.com/file/d/1Wtq0MGQkxFxdE3SRquJDy-0Bo3uqljsn/view)  

[What is a "fictitious force"? - scientificamerican.com](https://www.scientificamerican.com/article/what-is-a-fictitious-force/)  

A veces de indica ficticia la que no tiene quién las ejerce / no tiene pareja asociada a acción-reacción  

Stunt Pilot Pours And Drinks Red Bull Upside Down
 [![](https://img.youtube.com/vi/5XpGXuMaTQA/0.jpg)](https://www.youtube.com/watch?v=5XpGXuMaTQA) 
 

### Segunda ley de Newton

[Experimento de la Segunda Ley de Newton - walter-fendt.de](http://www.walter-fendt.de/html5/phes/newtonlaw2_es.htm)  

[Segunda ley de Newton Elvira Martinez y Carlos Romero - geogebra.org](https://www.geogebra.org/m/TRa7qwhx#material/Qgz8URZS)  
F, m y a visualizando vectores.

[Segunda ley de Newton en plano inclinado Fernándo Martínez - geogebra.org](https://www.geogebra.org/m/kRDTwEcv#material/Rg3cJRe8)  
Si se pone ángulo 0 sirve como plano horizontal con masa colgando.  

[Fuerzas y movimiento: intro - PhET](https://phet.colorado.edu/sims/html/forces-and-motion-basics/latest/forces-and-motion-basics_all.html?locale=es)  
En "Aceleración" se puede visualizar al tiempo F, m y a.  
![](https://phet.colorado.edu/sims/html/forces-and-motion-basics/latest/forces-and-motion-basics-900.png)  

Se incluyen aquí prácticas de laboratorio asociada por descargar la página general de laboratorio 

[SEGUNDA LEY DE NEWTON I - Experimentos realizados por alumnos de Bachillerato en la Ikastola "Pasaia-Lezo", en Guipúzcua.](http://rsefalicante.umh.es/LaboratorioFuerza/mikel-2/segunda-newton.htm)  
> Se quiere comprobar el cumplimiento de la segunda ley de Newton de la Dinámica en el movimiento de rodadura de un carro por un plano horizontal. Tirará del carro una cuerda, de la que, a su vez, tira un porta-pesos colgante a través de una polea. Es decir, un montaje tipo máquina de Atwood, pero mejorado de forma significativa con el uso de sensores. La cuerda se engancha al carro mediante un sensor de fuerza para medir así la fuerza real que en cada momento tira del mismo. En el montaje "tradicional" resulta mucho más difícil medir esta fuerza, puesto que una parte (pequeña pero significativa) de la fuerza del peso colgante se emplea en hacer girar la polea y también se tiene que considerar que el hilo tiene masa (por tanto, otra pequeña parte del peso se emplea en darle aceleración)  

[SEGUNDA LEY DE NEWTON II - Experimentos realizados por alumnos de Bachillerato en el instituto "Erniobea" de Villabona, en Guipúzcua.](http://rsefalicante.umh.es/LaboratorioFuerza/josem-lesaca/segunda-newton.htm)  
> Se pretende comprobar el cumplimiento de la segunda ley de Newton de la Dinámica en el deslizamiento ascendente de un carrito por un plano inclinado. 

[UNIVERSIDAD NACIONAL EXPERIMENTAL “FRANCISCO DE MIRANDA” COMPLEJO ACADÉMICO EL SABINO. AREA DE TECNOLOGIA. DEPARTAMENTO DE FÍSICA Y MATEMATICA. COORDINACION DE LABORATORIOS DE FÍSICA. GUÍAS DE LOS LABORATORIO DE FÍSICA I Y LABORATORIO DE FÍSICA GENERAL. PRACTICA 8: DINÁMICA DE LA PARTÍCULA: SEGUNDA LEY DE NEWTON](https://labfisicasabino.wordpress.com/wp-content/uploads/2008/06/prc3a1ctica-8-dinamica-de-la-partc3adcula.pdf)  

[Trabajo Práctico N°2: Segunda Ley de Newton - unicen.edu.ar](https://users.exa.unicen.edu.ar/catedras/fisica1/TP2LAB2004-dinamica.pdf)  

[P7 SEGUNDA LEY DE NEWTON. VICERRECTORÍA INSTITUCIONAL DE PREGRADO TRADICIONAL Formato general de prácticas de laboratorio. OCTUBRE-2018 - Universidad del Valle de México](https://s22e381c9c9c27c9b.jimcontent.com/download/version/1534804291/module/11719343760/name/P7.2ndaleyNewton.pdf)


### Tercera ley de Newton
[twitter MientrasEnFisic/status/1320848079306825730](https://twitter.com/MientrasEnFisic/status/1320848079306825730)  
Puede que hayas visto este vídeo y hayas pensado: WTF, este hombre vive en el año 3000. Tremendo artilugio ha construido, y yo tengo que ir andando a los sitios. Sin embargo hay un problema: la manera en la que parece propulsarse es físicamente imposible  

[EmDrive: Un horno de microondas troncocónico autopropulsado](https://francis.naukas.com/2015/05/05/emdrive-un-horno-de-microondas-troncoconico-autopropulsado/)  
"uno podría levantarse del suelo y levitar por el aire tirándose de sus propios cordones de los zapatos"  

Tuits con imágenes varias asociadas a 3ª Ley (no todos de Física !)
[twitter Ahdanchiano "Tercera ley"](https://twitter.com/search?q=from%3Ahdanchiano%20tercera%20ley)

[twitter gunsnrosesgirl3/status/1535248029527293953](https://twitter.com/gunsnrosesgirl3/status/1535248029527293953)  
Newton’s third law  
Think of an open inflated balloon, air rushes out, forcing the balloon to move through the air with the same force in the opposite direction  
Here the bag reverses as it hits the bike, escaped air propels it forward around the bike 1/🧵  

Does the snap of a trap-jaw ant hurt? - Ant Lab  
[![](https://img.youtube.com/vi/H2okI6ZszQY/0.jpg)]((https://www.youtube.com/watch?v=H2okI6ZszQY "Does the snap of a trap-jaw ant hurt? - Ant Lab")   


## [Fuerza elástica: muelles, resortes, ley de Hooke, dinamómetros](/home/recursos/recursos-dinamica/recursos-fuerza-elastica)

## Conservación del momento lineal
[twitter elprofedefisica/status/693166271333007360](https://twitter.com/elprofedefisica/status/693166271333007360)  
Esta pregunta la puse en un examen en 2009  Y estas son las respuestas de mis alumnos  
![](https://pbs.twimg.com/media/CZ53D0iWEAApNd0?format=jpg)  
[Los exámenes de un profe friki - elprofedefisica](http://elprofedefisica.naukas.com/2011/06/16/los-examenes-de-un-profe-friki/)  

  
[![](https://img.youtube.com/vi/7GW-dWNIB2U/0.jpg)](https://www.youtube.com/watch?v=7GW-dWNIB2U "Universo Mecánico 15 Conservación Del Momento") 

[Cuna de Newton - walter-fendt.de](https://www.walter-fendt.de/html5/phes/newtoncradle_es.htm)  

[![](https://phet.colorado.edu/sims/html/collision-lab/latest/collision-lab-900.png)](https://phet.colorado.edu/es/simulations/collision-lab "Laboratorio de colisiones - PhET")  

[Momento lineal e impulso - aprendiendofisicaconbertotomas.com](http://bit.ly/3EAPoSb)  

## Momento angular 
A veces llamado momento cinético  
[Cantidad de movimiento y momento cinético (GIE) - laplace.us.es](http://laplace.us.es/wiki/index.php/Cantidad_de_movimiento_y_momento_cin%C3%A9tico_(GIE)#Momento_cin.C3.A9tico)  

Relacionado con el cálculo vectorial, y como momento de una fuerza, y con sólido rígido.  

## Momento de una fuerza 
A veces llamado momento dinámico o torque (de latín torquere (retorcer))    
Relacionado con el momento angular y sólido rígido, se comenta en materiales sólido rígido de 1º Bachillerato LOMLOE  
[Par motor - wikipedia](https://es.wikipedia.org/wiki/Par_motor)  
![](https://upload.wikimedia.org/wikipedia/commons/0/09/Torque_animation.gif)  


## Bloques apilados

[Dos bloques apilados - laplace.us.es](http://laplace.us.es/wiki/index.php/Dos_bloques_apilados) 

[Problemas resueltos. Dinámica de la partícula. sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica3/problemas/dinamica/lineal/lineal.html)  
Problemas 13 y 18
![](http://www.sc.ehu.es/sbweb/fisica3/problemas/dinamica/lineal/prob_14/bloques.gif)  
![](http://www.sc.ehu.es/sbweb/fisica3/problemas/dinamica/lineal/prob_09/prob_7_1.gif)  

[Multiple Object Force Problem: Stacked Boxes with Friction](https://www.uwgb.edu/fenclh/problems/dynamics/multi/1/)  

[PS.2.2 Worked Example: Pushing Stacked Blocks - ocw.mit.edu](https://ocw.mit.edu/courses/physics/8-01sc-classical-mechanics-fall-2016/week-2-newtons-laws/ps.2.2-worked-example-pushing-stacked-blocks/)  

[question on friction, 3 blocks placed one upon another](https://physics.stackexchange.com/questions/267183/question-on-friction-3-blocks-placed-one-upon-another)  

[External force on three blocks stacked](https://www.physicsforums.com/threads/external-force-on-three-blocks-stacked.978380/)  

Ver problema en [fase local Madrid olimpiada 2020](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/Olimpiadas%20F%C3%ADsica/local/Madrid/2020-oef-local-madrid-exam.pdf)  
Ver [problema 1 oposiciones Madrid 2006](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-para-oposiciones/2006-Madrid-F%C3%ADsica1.pdf)   
Ver problema 2002-Madrid-Septiembre-A2-Mecánica  

## Peralte 

[Curva con peralte - ehu.es](http://www.sc.ehu.es/sbweb/fisica_/dinamica/circular/circular/din_circular1.html)  

[twitter af_bertotomas/status/1637896767462047744](https://twitter.com/af_bertotomas/status/1637896767462047744)  
Curva con peralte y fricción también para descarga en la web.  
![](https://pbs.twimg.com/media/Frr6a6tX0AM9H7J?format=jpg)  

## Dinámica del sólido rígido
Ver [Sólido rígido](/home/recursos/fisica/mecanica/solido-rigido).  

## Dinámica de movimiento circular  
Se cita g que enlaza con gravitación  

[Dinámica del movimiento circular uniforme - ehu.es](http://www.sc.ehu.es/sbweb/fisica_/dinamica/dinamica.htm#circular)  

[Watching Jet Pilots Train To Withstand G-Forces Is Just Too Intense](https://www.popularmechanics.com/military/research/a18922/jet-fighter-pilots-high-g-training/)  
 [![Centrifuge 8G ESA](https://img.youtube.com/vi/Hgz7kJJSksM/0.jpg)](https://www.youtube.com/watch?v=Hgz7kJJSksM "Centrifuge 8G ESA")   

[Movimiento circular - www.aprendiendofisicaconbertotomas.com](http://bit.ly/3JT7X7h)  

## Choques
Muchas veces combinados con dinámica del sólido rígido  
[Choques. Dinámica de la partícula - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica_//dinamica/dinamica.htm#choques)  
[Choques en una dimensión. Dinámica de un sistema de partículas - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica_//dinamica/choques/choques/choques.xhtml)  
[Sistemas aislados. Choques. Problemas resueltos - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica3//problemas/sistemas/choques/choques.html)  
 
[Accidente de tráfico - educaplus.org](http://www.educaplus.org/game/accidente-de-trafico)   

[Choque elástico - educaplus.org](https://www.educaplus.org/game/choque-elastico)  

[Choque Elástico e Inelástico - walter-fendt.de](https://www.walter-fendt.de/html5/phes/collision_es.htm)  

[Officials Crashed a Jet into Nuclear Reactor Facility to Test its Walls - interestingengineering.com](https://interestingengineering.com/crashed-jet-nuclear-reactor-test)  

[![Sandia F4 Phantom - 1988 "Rocket-sled test"](https://img.youtube.com/vi/zPe-bKIid8w/0.jpg)](https://www.youtube.com/watch?v=zPe-bKIid8w "Sandia F4 Phantom - 1988 Rocket-sled test")   

[![Crash Test Month: Truck Hitting A Bollard"](https://img.youtube.com/vi/HAkCypsQIQk/0.jpg)](https://www.youtube.com/watch?v=HAkCypsQIQk "Crash Test Month: Truck Hitting A Bollard")   

[![Bullet vs Newton's Cradle at 100,000 FPS - The Slow Mo Guys"](https://img.youtube.com/vi/YcBg6os2dPY/0.jpg)](https://www.youtube.com/watch?v=YcBg6os2dPY "Bullet vs Newton's Cradle at 100,000 FPS - The Slow Mo Guys")   

[twitter Rainmaker1973/status/1541398439036162050](https://twitter.com/Rainmaker1973/status/1541398439036162050)  
Though it may not be intuitive, bullets easily ricochet off water. This is the physics behind the process and the analytical model to estimate the critical ricochet angle  
[Ricochet Angle for Armament Shapes](https://www.ijaiem.org/volume2issue12/IJAIEM-2013-12-10-023.pdf?platform=hootsuite)  
[source of the gif](https://gfycat.com/cheappotablealdabratortoise-pubattlegrounds-pubg)  


## [Máquinas simples](/home/recursos/fisica/maquinas-simples) 

## Rozamiento
Rozamiento estático, dinámico, posibilidad de coeficiente mayor que 1... [http://laplace.us.es/wiki/index.php/Fuerzas_de_rozamiento_(GIE)](http://laplace.us.es/wiki/index.php/Fuerzas_de_rozamiento_(GIE))  

[![](https://phet.colorado.edu/sims/html/friction/latest/friction-900.png)](https://phet.colorado.edu/es/simulations/friction "Fricción")  

Valores de coeficientes [https://www.uv.es/dae/Apunte4.pdf](https://www.uv.es/dae/Apunte4.pdf)  
Incluye tablas que citan fuente  
Coeficientes de rozamiento por deslizamiento para diferentes materiales. Fuente: Koshkin N. I., Shirkévich M. G.. Manual de Física Elemental. Editorial Mir 1975  
Coeficientes de rozamiento estático y cinético. Fuente: Serway R. A.. Física. Editorial McGraw-Hill. (1992) 

[Why can leaves cause train delays? The chemistry of leaves on the line - compoundchem.com](https://www.compoundchem.com/2018/10/22/leavesontheline/)  
![](https://i1.wp.com/www.compoundchem.com/wp-content/uploads/2018/10/Why-can-leaves-cause-rail-delays_.png?w=1654&ssl=1)  
Incluye valores de coeficiente de rozamiento  


## Simulaciones

Ver simulaciones en [Fuerza elástica: muelles, resortes, ley de Hooke, dinamómetros](/home/recursos/recursos-dinamica/recursos-fuerza-elastica)

En el apartado de  [simulaciones](/home/recursos/simulaciones) , dentro de  [http://www.fislab.net/](http://www.fislab.net/)  hay un apartado para dinámica: Plano inclinado, dos objetos, choques, momento de inercia  
En 2018 los applets pasan a ficheros .jar que se pueden ejecutar sin las limitaciones de applets.  

Animación interactiva sobre caída libre con/sin rozamiento del aire  
[http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/roz_aire.swf](http://acer.forestales.upm.es/basicas/udfisica/asignaturas/fisica/animaciones_files/roz_aire.swf)  

[Física, dinámica - educaplus.org](http://www.educaplus.org/games/dinamica)  

## Coriolis
[twitter HdAnchiano/status/1056660696497303557](https://twitter.com/HdAnchiano/status/1056660696497303557)  
Observación del efecto Coriolis en un sistema de referencia en rotación. Hay una aceleración relativa del cuerpo en el sistema y esta aceleración es siempre perpendicular al eje de rotación del sistema y a la velocidad del cuerpo. #FísicaAplicada #ciencia
 
[Coriolis effect - kaiserscience](https://kaiserscience.wordpress.com/physics/rotational-motion/coriolis-effect/)   
![](https://kaiserscience.files.wordpress.com/2015/10/coriolis-effect-merrygoround.gif)  
![](https://upload.wikimedia.org/wikipedia/commons/b/b6/Corioliskraftanimation.gif "https://commons.wikimedia.org/wiki/File:Corioliskraftanimation.gif")  

[Coriolis force - en.wikipedia](https://en.wikipedia.org/wiki/Coriolis_force)  

## Fuerzas centrales

Relacionado con cinemática  
[Fuerza central proporcional a la distancia - sc.ehu.es](http://www.sc.ehu.es/sbweb/fisica3/celeste/central/central.html)  
[FI-21A MECANICA Luis Rodríguez Valencia Facultad de Ciencias Físicas y Matemáticas Departamento de Física Universidad de Chile 14 de agosto de 2008](https://www.u-cursos.cl/ingenieria/2008/2/FI2A1/3/material_docente/bajar?id_material=185160)  
2.6. Campo Central de Fuerza  

## Fuerzas conservativas

[Fuerzas conservativas: su abordaje en libros de texto de física universitaria. Claudia Mariela Zang y Norah Silvana Giacosa. Revista enseñanza de la Física. VOLUMEN 33, NÚMERO EXTRA | Selección de Trabajos Presentados a REF | PP. 673-680 ISSN 2469-052X (en línea)](https://revistas.unc.edu.ar/index.php/revistaEF/article/download/35648/35774/125780)  

## Conceptos básicos fuerzas
Átomos y fuerzas  
[twitter cienciadeldani/status/1003314836854460416](https://twitter.com/cienciadeldani/status/1003314836854460416)  
A menudo la Física es pura poesia. La repulsión de los campos electromagnéticos generados por los electrones de los átomos que nos forman, hace que en realidad nunca nos lleguemos a tocar. Eso no impide que podamos acariar los detalles más minúsculos de la piel.  
*Incluye un trozo de vídeo con el logotipo de FOX, por localizar referencia*  

[Fuerzas y Movimiento: Intro - PhET](https://phet.colorado.edu/es/simulation/forces-and-motion-basics)  
Fuerzas en 1 Dimensión  
![](https://phet.colorado.edu/sims/html/forces-and-motion-basics/latest/forces-and-motion-basics-900.png)  
[Fuerzas y movimiento - PhET (java) ](https://phet.colorado.edu/es/simulations/forces-and-motion)  
![](https://phet.colorado.edu/sims/motion-series/forces-and-motion-600.png)  

## Prácticas / experimentos / laboratorio 
En página de  [prácticas de laboratorio](/home/recursos/practicas-experimentos-laboratorio)  hay algunos separados  

En 2018 veo este tuit con un ejemplo interesante  
[https://twitter.com/fqsaja1/status/1075494350442901504](https://twitter.com/fqsaja1/status/1075494350442901504)  
Siempre es divertido y "refrescante" lanzar unos cohetes de agua con los alumnos de 4º de ESO estudiando las leyes de Newton.@profeFQ @lalibabyboom @SylviukaHerrera @juanfisicahr @simplificamates @rubiq @jolaedi @pepenbici @VictoriaRgil  

Este experimento se hace como "taller de construcción de cohetes" en Robledo de Chavela, y las botellas que se usan son de PET que aguanten la presión de bebidas gaseosas, no de agua mineral sin presión como la del vídeo.  

[twitter gatkri/status/1336379225448869891](https://twitter.com/gatkri/status/1336379225448869891)  
Giving my students this video lab as part of their test on forces- because who doesn't enjoy dinosaurs and toy cars? Enjoy! #iteachphysics

[![Dino Sled Friction Lab](https://img.youtube.com/vi/p8cNBoEFCHQ/0.jpg)](https://www.youtube.com/watch?v=p8cNBoEFCHQ) 


Cubli  
Building cubes that can jump up and balance  
[https://idsc.ethz.ch/research-dandrea/research-projects/archive/cubli.html](https://idsc.ethz.ch/research-dandrea/research-projects/archive/cubli.html) 

[![The Cubli: a cube that can jump up, balance, and 'walk'](https://img.youtube.com/vi/n_6p-1J551Y/0.jpg)](https://www.youtube.com/watch?v=n_6p-1J551Y)

[![Ball Balancing PID System](https://img.youtube.com/vi/57DbEEBF7sE/0.jpg)](https://www.youtube.com/watch?v=57DbEEBF7sE "Ball Balancing PID System")  
Enlaza [ba-bot.com](https://www.ba-bot.com/) que parecen ser siglas de Balancing Bot  

[![Ball and Plate State Space Observer control with position control of PMDC motors](https://img.youtube.com/vi/Rr90hb_Rn3M/0.jpg)](https://www.youtube.com/watch?v=Rr90hb_Rn3M "Ball and Plate State Space Observer control with position control of PMDC motors")  
[SIMULATION AND EXPERIMENTAL STUDY OF BALL POSITION CONTROL AT BIAXIAL PLATFORM USING STATE SPACE APPROACH](http://www3.uniza.sk/komunikacie/archiv/2018/1/1_2018en.pdf)  


## Ejercicios de elaboración propia
Algunos de elaboración propia y otros recopilando ejercicios de la extinta materia de mecánica.  
También hay ejercicios de dinámica recopilados asociados a prueba libre ESO y prueba acceso GM, muy sencillos.  

Los ficheros se veían inicialmente anexados a esta página, ahora se pueden ver en  
[drive.fiquipedia recursos dinámica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/home/recursos/fisica/recursos-dinamica)  

