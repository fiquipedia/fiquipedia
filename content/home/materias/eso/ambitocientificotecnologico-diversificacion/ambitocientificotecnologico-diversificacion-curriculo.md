# Currículo Ámbito Científico Tecnológico Diversificación (LOE)

Desarrollado en  [ORDEN 4265/2007, de 2 de agosto, B.O.C.M. Núm. 198 de MARTES 21 DE AGOSTO DE 2007](http://gestiona.madrid.org/wleg/servlet/Servidor?opcion=VerHtml&idnorma=5715&word=S&wordperfect=N&pdf=S)   

## Objetivos (son comunes a 1º y 2º: de todo el programa de diversificación)  
  
1. Incorporar al lenguaje y a los modos de argumentación habituales las formas elementales de expresión científico-matemática con el fin de comunicarse de manera clara, concisa y precisa.  
2. Utilizar técnicas sencillas y autónomas de recogida de datos, familiarizándose con las que proporcionan las tecnologías de la información y la comunicación, sobre fenómenos y situaciones de carácter científico y tecnológico.  
3. Participar en la realización de actividades científicas y en la resolución de problemas sencillos.  
4. Utilizar los conocimientos adquiridos sobre las Ciencias de la Naturaleza para comprender y analizar el mundo físico que nos rodea.  
5. Adquirir conocimientos sobre el funcionamiento del organismo humano para desarrollar y afianzar hábitos de cuidado y salud  
corporal.  
6. Aplicar con soltura y adecuadamente las herramientas matemáticas adquiridas a situaciones de la vida diaria.  
7. Utilizar procedimientos demedida y realizar el análisis de los datos obtenidos mediante el uso de distintas clases de números y la selección de los cálculos apropiados.  
8. Identificar las formas planas o espaciales que se presentan en la vida diaria y analizar las propiedades y relaciones geométricas entre  
ellas.  
9. Utilizar de forma adecuada los distintos medios tecnológicos (calculadoras, ordenadores, etcétera) tanto para realizar cálculos como para tratar y representar informaciones de índole diversa.  
10. Disponer de destrezas técnicas y conocimientos básicos para el análisis, diseño, elaboración y manipulación de forma segura y precisa de materiales, objetos y sistemas tecnológicos.  
11. Conocer y valorar las interacciones de la ciencia y la tecnología con la sociedad y el medio ambiente, incidiendo en la necesidad  
de búsqueda y aplicación de soluciones a los problemas a los que se enfrenta actualmente la humanidad.  
12. Reconocer y valorar las aportaciones de la ciencia para la mejora de las condiciones de vida de los seres humanos.  
13. Potenciar como valores positivos el esfuerzo personal y la autoestima en el propio proceso de aprendizaje.  

## Contenidos

### Primer curso

#### CIENCIAS DE LA NATURALEZA
Bloque 1. *Introducción a la metodología científica*  
- Utilización de estrategias propias del trabajo científico como el planteamiento de problemas y discusión de su interés, la formulación y puesta a prueba de hipótesis y la interpretación de los resultados. El informe científico. Análisis de datos organizados en tablas y gráficos.  
- Búsqueda y selección de información de carácter científico utilizando las tecnologías de la información y comunicación y otras fuentes.  
- Interpretación de información de carácter científico y utilización de dicha información para formarse una opinión propia, expresarse con precisión y argumentar sobre problemas relacionados con la naturaleza. La notación científica.  
- Valoración de las aportaciones de las ciencias de la naturaleza para dar respuesta a las necesidades de los seres humanos y mejorar las condiciones de su existencia, así como para apreciar y disfrutar de la diversidad natural y cultural, participando en su conservación, protección y mejora.  
- Utilización correcta de los materiales, sustancias e instrumentos básicos de un laboratorio. Carácter aproximado de la medida. Sistema internacional de unidades. El respeto por las normas de seguridad en el laboratorio.  

Bloque 2. *Las personas y la salud*  
- La organización general del cuerpo humano: La célula, tejidos, órganos, sistemas y aparatos.  
- El concepto de salud y el de enfermedad. Los factores determinantes de la salud.  
- La enfermedad y sus tipos. Higiene y prevención de las enfermedades.  
- Sistema inmunitario. Vacunas. El trasplante y donación de células, órganos y sangre.  
- Primeros auxilios.  
La reproducción humana  
- Cambios físicos y psíquicos en la adolescencia.  
- Los aparatos reproductores masculino y femenino.  
- Las enfermedades de transmisión sexual.  
- El ciclo menstrual. Relación con la fecundidad.  
- Fecundación, embarazo y parto.  
- Principales métodos anticonceptivos.  
- La respuesta sexual humana. Sexo y sexualidad. Salud e higiene sexual.  
Alimentación y nutrición humanas  
- Las funciones de nutrición.  
- El aparato digestivo. Principales enfermedades.  
- Hábitos alimenticios saludables. Dietas equilibradas.  
- Prevención de las enfermedades provocadas por malnutrición.  
Otros aparatos que intervienen en la nutrición: Respiratorio, circulatorio y excretor  
- Descripción y funcionamiento.  
- Hábitos saludables.  
- Enfermedades más frecuentes y su prevención.  
Las funciones de relación: Percepción, coordinacióny movimiento  
- La percepción: Los órganos de los sentidos; su cuidado e higiene.  
- La coordinación y el sistema nervioso: Organización y función.  
- El control interno del organismo: El sistema endocrino.  
- Glándulas endocrinas y su funcionamiento. Sus principales alteraciones.  
- El aparato locomotor. Análisis de las lesiones más frecuentes y su prevención.  
Salud mental  
- Factores que repercuten en la salud mental en la sociedad actual.  
- Las sustancias adictivas: El tabaco, el alcohol y otras drogas. Problemas asociados.  
- Actitud responsable ante conductas de riesgo para la salud.  
Bloque 3. *Energía y electricidad*  
El concepto de energía  
- Energías tradicionales.  
- Energías alternativas.  
- Fuentes de energía renovables.  
- Conservación y degradación de la energía.  
Electricidad  
- Propiedades eléctricas de la materia.  
- Las cargas eléctricas y su interacción.  
- La energía eléctrica. Conductores y aislantes. Circuitos eléctricos sencillos.  
- La electricidad en casa. El ahorro energético.

#### MATEMÁTICAS
Bloque 4. *Contenidos comunes*  
- Planificación y utilización de estrategias en la resolución de problemas tales como el recuento exhaustivo, la inducción o la búsqueda de problemas afines, y comprobación del ajuste de la solución a la situación planteada.  
- Descripción verbal de relaciones cuantitativas y espaciales, y procedimientos de resolución utilizando la terminología precisa.  
- Interpretación de mensajes que contengan informaciones de carácter cuantitativo o simbólico o sobre elementos o relaciones espaciales.  
- Confianza en las propias capacidades para afrontar problemas, comprender las relaciones matemáticas y tomar decisiones a partir de ellas.  
- Perseverancia y flexibilidad en la búsqueda de soluciones a los problemas y en la mejora de las encontradas. Utilización de herramientas tecnológicas para facilitar los cálculos de tipo numérico, algebraico o estadístico, las representaciones funcionales y la comprensión de propiedades geométricas.  
Bloque 5. *Números*  
- Números decimales y fracciones. Transformación de fracciones en decimales y viceversa. Números decimales exactos y periódicos. Fracción generatriz.  
- Operaciones con fracciones y decimales. Cálculo aproximado y redondeo. Cifras significativas. Error absoluto y relativo. Utilización de aproximaciones y redondeos en la resolución de problemas de la vida cotidiana con la precisión requerida por la situación planteada.  
- Potencias de exponente entero. Significado y uso. Su aplicación para la expresión de números muy grandes y muy pequeños. Operaciones con números expresados en notación científica. Uso de la calculadora.  
- Representación en la recta numérica. Comparación de fracciones.  
Bloque 6. *Álgebra*  
- Análisis de sucesiones numéricas. Progresiones aritméticas y geométricas.  
- Sucesiones recurrentes. Las progresiones como sucesiones recurrentes.  
- Traducción de situaciones del lenguaje verbal al algebraico.  
- Transformación de expresiones algebraicas. Igualdades notables.  
- Resolución de ecuaciones de primer grado con una incógnita. Sistemas de dos ecuaciones lineales con dos incógnitas.  
- Resolución de problemas mediante la utilización de ecuaciones, sistemas y otros métodos personales. Valoración de la precisión, simplicidad y utilidad del lenguaje algebraico para resolver diferentes situaciones de la vida cotidiana.  
Bloque 7. *Geometría*  
- Determinación de figuras a partir de ciertas propiedades. Lugar geométrico.  
- Aplicación de los teoremas de Tales y Pitágoras a la resolución de problemas geométricos y del medio físico.  
- Traslaciones, simetrías y giros en el plano. Elementos invariantes de cada movimiento.  
- Planos de simetría en los poliedros.- Reconocimiento de los movimientos en la naturaleza, en el arte y en otras construcciones humanas.  
- Coordenadas geográficas y husos horarios. Interpretación de mapas y resolución de problemas asociados.  
Bloque 8. *Funciones y gráficas*  
- Análisis y descripción cualitativa de gráficas que representan fenómenos del entorno cotidiano y de otras materias.  
- Análisis de una situación a partir del estudio de las características locales y globales de la gráfica correspondiente: Dominio, continuidad, monotonía, extremos y puntos de corte. Uso de las tecnologías de la información para el análisis conceptual y reconocimiento de propiedades de funciones y gráficas.  
- Formulación de conjeturas sobre el comportamiento del fenómeno que representa una gráfica y su expresión algebraica.  
- Análisis y comparación de situaciones de dependencia funcional dadas mediante tablas y enunciados. Utilización de modelos lineales para estudiar situaciones provenientes de los diferentes ámbitos de conocimiento y de la vida cotidiana, mediante la confección de la tabla, la representación gráfica y la obtención de la expresión algebraica.  
- Obtención de la expresión algebraica de funciones lineales y afines a partir de diferentes datos.  
Bloque 9. Estadística y probabilidad  
- Necesidad, conveniencia y representatividad de una muestra. Métodos de selección aleatoria y aplicaciones en situaciones reales.  
- Atributos y variables discretas y continuas. Agrupación de datos en intervalos. Histogramas y polígonos de frecuencias.  
- Construcción de la gráfica adecuada a la naturaleza de los datos y al objetivo deseado.- Media, moda, cuartiles y mediana. Significado, cálculo y aplicaciones.  
- Análisis de la dispersión: Rango y desviación típica. Interpretación conjunta de la media y la desviación típica.  
- Utilización de las medidas de centralización y dispersión para realizar comparaciones y valoraciones. Crítica de la información de índole estadística.  
- Utilización de la calculadora y la hoja de cálculo para organizar los datos, realizar cálculos y generar las gráficas más adecuadas.  
- Experiencias aleatorias. Sucesos y espacio muestral. Utilización del vocabulario adecuado para describir y cuantificar situaciones relacionadas con el azar.  
- Cálculo de probabilidades mediante la regla de Laplace. Formulación y comprobación de conjeturas sobre el comportamiento de fenómenos aleatorios sencillos.  
- Cálculo de la probabilidad mediante la simulación o experimentación.- Utilización de la probabilidad para tomar decisiones fundamentadas en diferentes contextos. Reconocimiento y valoración de las matemáticas para interpretar, describir y predecir situaciones inciertas.

#### TECNOLOGÍAS
Bloque 10. *Hardware y software*  
- Instalación de programas y realización de tareas básicas de mantenimiento del sistema. Acceso a recursos compartidos en redes locales y puesta a disposición de los mismos.  
Bloque 11. *Técnicas de expresión y comunicación*  
- Sistemas sencillos de representación. Vistas y perspectivas. Proporcionalidad entre dibujo y realidad. Escalas. Acotación.  
Bloque 12. *Materiales de uso técnico*  
- Introducción a los plásticos: Clasificación. Obtención. Propiedades características.  
- Técnicas básicas para el trabajo con plásticos. Herramientas y uso seguro de las mismas.  
- Materiales de construcción: Pétreos, cerámicos. Propiedades características. 

### Segundo curso

#### CIENCIAS DE LA NATURALEZA
Bloque 1. *La actividad humana y el medio ambiente*  
- Los recursos naturales: Definición y clasificación.  
- Importancia del uso y gestión sostenible de los recursos hídricos.  
- La potabilización del agua y los sistemas de depuración.  
- Utilización de técnicas sencillas para conocer el grado de contaminación y depuración del aire y del agua.  
- Los residuos y su gestión. Valoración del impacto de la actividad humana en los ecosistemas. Análisis crítico de las intervenciones humanas en el medio.  
- Principales problemas ambientales de la actualidad.  
- Valoración de la necesidad de cuidar del medio ambiente y adoptar conductas solidarias y respetuosas con él.  
Bloque 2. *Transformaciones geológicas debidas a la energía externa de la Tierra*  
La energía de procedencia externa del planeta.  
- La energía solar en la tierra.  
- La atmósfera como filtro de la energía solar: Su estructura y dinámica.  
- Interpretación de mapas del tiempo sencillos.  
Agentes geológicos externos  
- Origen de los agentes geológicos externos.  
- Alteraciones de las rocas producidas por la atmósfera: La meteorización.  
- Acción geológica del viento y del hielo.  
- Acción geológica de las aguas superficiales y subterráneas.  
- Aprovechamiento y sobreexplotación de acuíferos.  
- Dinámica marina: Corrientes, mareas y olas. Acción geológica del mar.  
La formación de las rocas sedimentarias  
- Las rocas sedimentarias: Formación y clasificación.  
- Explotación y utilización del carbón, del petróleo y del gas natural. Consecuencias de su agotamiento.  
Bloque 3. Diversidad y unidad de estructura de la materiaLa materia, elementos y compuestos  
- La materia y sus estados de agregación: Sólido, líquido y gaseoso.  
- Teoría cinética y cambios de estado.  
- Sustancias puras y mezclas. Métodos de separación de mezclas. Disoluciones. Sustancias simples y compuestas.  
Átomos y moléculas  
- Estructura atómica. Partículas constituyentes del átomo.  
- Utilización de modelos.  
- Introducción al concepto de elemento químico.  
- Uniones entre átomos: Moléculas.  
- Fórmulas y nomenclatura de las sustancias más corrientes según las normas de la IUPAC.  
- Masas atómicas y moleculares. Isótopos: Concepto y aplicaciones.  
Bloque 4. Los cambios químicos  
Las reacciones químicas  
- Interpretación macroscópica de las reacciones químicas.  
- Representación simbólica.  
- Ecuaciones químicas y su ajuste.  
- Realización experimental de algunos cambios químicos.  
- Reacciones de oxidación y de combustión.  
- La química y el medioambiente: Efecto invernadero, lluvia ácida, destrucción de la capa de ozono, contaminación de aguas y tierras. 

#### MATEMÁTICAS
Bloque 5. *Contenidos comunes*  
- Planificación y utilización de procesos de razonamiento y estrategias de resolución de problemas, tales como la emisión y justificación de hipótesis o la generalización.  
- Expresión verbal de argumentaciones, relaciones cuantitativas y espaciales, y procedimientos de resolución de problemas con la precisión y rigor adecuados a la situación.  
- Interpretación de mensajes que contengan argumentaciones o informaciones de carácter cuantitativo o sobre elementos o relaciones espaciales.  
- Confianza en las propias capacidades para afrontar problemas, comprender las relaciones matemáticas y tomar decisiones a partir de ellas.  
- Perseverancia y flexibilidad en la búsqueda de soluciones a los problemas y en la mejora de las encontradas. Utilización de herramientas tecnológicas para facilitar los cálculos de tipo numérico, algebraico o estadístico, las representaciones funcionales y la comprensión de propiedades geométricas.  
Bloque 6. *Números*  
- Interpretación y utilización de los números y las operaciones en diferentes contextos, eligiendo la notación y precisión más adecuadas en cada caso.  
- Proporcionalidad directa e inversa. Aplicación a la resolución de problemas de la vida cotidiana.  
- Los porcentajes en la economía. Aumentos y disminuciones porcentuales. Porcentajes sucesivos. Interés simple y compuesto.  
- Uso de la hoja de cálculo para la organización de cálculos asociados a la resolución de problemas cotidianos y financieros.  
- Intervalos. Significado y diferentes formas de expresar un intervalo.  
- Representación de números en la recta numérica.  
Bloque 7. *Álgebra*  
- Manejo de expresiones literales para la obtención de valores concretos en fórmulas y ecuaciones en diferentes contextos.  
- Resolución de ecuaciones de segundo grado y de problemas asociados.  
- Resolución gráfica y algebraica de los sistemas de ecuaciones. Resolución de problemas cotidianos y de otras áreas de conocimiento mediante ecuaciones y sistemas.  
- Resolución de otros tipos de ecuaciones mediante ensayo-error o a partir de métodos gráficos con ayuda de los medios tecnológicos.  
Bloque 8. *Geometría*  
- Aplicación de la semejanza de triángulos y de los teoremas de Pitágoras y de Tales para la obtención indirecta de medidas. Resolución de problemas geométricos frecuentes en la vida cotidiana.  
- Utilización de otros conocimientos geométricos en la resolución de problemas del mundo físico: Medida y cálculo de longitudes, áreas, volúmenes, etcétera.  
Bloque 9. *Funciones y gráficas*  
- Interpretación de un fenómeno descrito mediante un enunciado, tabla, gráfica o expresión analítica. Análisis de resultados.  
- La tasa de variación media como medida de la variación de una función en un intervalo. Análisis de distintas formas de crecimiento en tablas, gráficas y enunciados verbales.  
- Estudio y utilización de otros modelos funcionales no lineales: Exponencial y cuadrática. Utilización de tecnologías de la información para su análisis.  
Bloque 10*. Estadística y probabilidad*  
- Identificación de las fases y tareas de un estudio estadístico a partir de situaciones concretas cercanas al alumnado.  
- Análisis elemental de la representatividad de las muestras estadísticas.  
- Gráficas estadísticas: Gráficas múltiples, diagramas de caja. Uso de la hoja de cálculo.  
- Utilización de las medidas de centralización y dispersión para realizar comparaciones y valoraciones. Experiencias compuestas. Utilización de tablas de contingencia y diagramas de árbol para el recuento de casos y la asignación de probabilidades.  
- Utilización del vocabulario adecuado para describir y cuantificar situaciones relacionadas con el azar.

#### TECNOLOGÍAS
Bloque 11. *Proceso de resolución de problemas tecnológicos*  
- Documentos técnicos necesarios para la elaboración de un proyecto.  
- Diseño, planificación y construcción de prototipos mediante el uso de materiales, herramientas y técnicas estudiadas.  
- Empleo de herramientas informáticas, gráficas y de cálculo, para la elaboración, desarrollo y difusión del proyecto.  
- Análisis y valoración de las condiciones del entorno de trabajo.  
Bloque 12. *Electricidad y electrónica*  
- Circuito eléctrico: Magnitudes eléctricas básicas. Simbología. Ley de Ohm.  
- Circuito en serie, paralelo, mixto.  
- Circuito eléctrico: Corriente alterna y corriente continua.  
- Montajes eléctricos sencillos: Circuitos mixtos. Inversor del sentido de giro.  
- Efectos de la corriente eléctrica: Electromagnetismo. Aplicaciones.  
- Aparatos de medida básicos: Voltímetro, amperímetro, polímetro. Realización de medidas sencillas.  
Bloque 13. Tecnologías de la comunicación. Internet  
- El ordenador como medio de comunicación intergrupal: Comunidades y aulas virtuales. Internet.  
- Actitud crítica y responsable hacia la propiedad y la distribución del software y de la información: Tipos de licencias de uso y distribución.

## Criterios de evaluación (son comunes a 1º y 2º: de todo el programa de diversificación)  

1. Determinar las características del trabajo científico a través del análisis de algunos problemas científicos o tecnológicos de actualidad.  
2. Describir las interrelaciones existentes en la actualidad entre sociedad, ciencia y tecnología.  
3. Describir los aspectos básicos del aparato reproductor, diferenciando entre sexualidad y reproducción.  
4. Conocer el funcionamiento de los métodos de control de natalidad y valorar el uso de métodos de prevención de enfermedades de transmisión sexual.  
5. Determinar los órganos y aparatos humanos implicados en las funciones vitales, establecer relaciones entre las diferentes funciones del organismo y los hábitos saludables.  
6. Explicar los procesos fundamentales de la digestión y asimilación de los alimentos, utilizando esquemas y representaciones gráficas, y justificar, a partir de ellos, los hábitos alimenticios saludables, independientes de prácticas consumistas inadecuadas.  
7. Explicar la misión integradora del sistema nervioso y enumerar algunos factores que lo alteran.  
8. Localizar los principales huesos y músculos que integran el aparato locomotor.   
9. Razonar ventajas e inconvenientes de las diferentes fuentes energéticas. Enumerar medidas que contribuyen al ahorro colectivo o individual de energía. Explicar por qué la energía no puede reutilizarse sin límites.  
10. Resolver ejercicios numéricos de circuitos sencillos. Saber calcular el consumo eléctrico en el ámbito doméstico.  
11. Recopilar información procedente de fuentes documentales y de Internet acerca de la influencia de las actuaciones humanas sobre diferentes ecosistemas: Efectos de la contaminación, desertización, disminución de la capa de ozono, agotamiento de recursos y extinción de especies; analizar dicha información y argumentar posibles actuaciones para evitar el deterioro del medio ambiente y promover una gestión más racional de los recursos naturales. Estudiar algún caso de especial incidencia en nuestra Comunidad Autónoma.  
12. Relacionar la desigual distribución de la energía en la superficie del planeta con el origen de los agentes geológicos externos.  
13. Identificar las acciones de dichos agentes en el modelado del relieve terrestre.  
14. Reconocer las principales rocas sedimentarias.  
15. Describir las características de los estados sólido, líquido y gaseoso. Explicar en qué consisten los cambios de estado, empleando la teoría cinética.  
16. Diferenciar entre elementos, compuestos y mezclas, así como explicar los procedimientos químicos básicos para su estudio.  
17. Distinguir entre átomos y moléculas. Indicar las características de las partículas componentes de los átomos. Diferenciar los elementos.  
18. Formular y nombrar algunas sustancias importantes. Indicar sus propiedades.  
19. Discernir entre cambio físico y químico. Comprobar que la conservación de la masa se cumple en toda reacción química. Escribir y ajustar correctamente ecuaciones químicas sencillas.  
20. Explicar los procesos de oxidación y combustión, analizando su incidencia en el medio ambiente.  
21. Manejo de instrumentos de medida sencillos: balanza, probeta, bureta, termómetro. Conocer y aplicar las medidas del SI.  
22. Utilizar los distintos tipos de números y operaciones, junto con sus propiedades, para recoger, transformar e intercambiar información y resolver problemas relacionados con la vida diaria.  
23. Aplicar porcentajes y tasas a la resolución de problemas cotidianos y financieros, valorando la oportunidad de utilizar la hoja de cálculo en función de la cantidad y complejidad de los números.  
24. Expresar mediante el lenguaje algebraico una propiedad o relación dada mediante un enunciado y observar regularidades en secuencias numéricas obtenidas de situaciones reales mediante la obtención de la ley de formación y la fórmula correspondiente, en casos sencillos.  
25. Resolver problemas de la vida cotidiana en los que se precise el planteamiento y resolución de ecuaciones de primer y segundo grado o de sistemas de ecuaciones lineales con dos incógnitas.  
26. Reconocer las transformaciones que llevan de una figura geométrica a otra mediante los movimientos en el plano y utilizar dichos movimientos para crear sus propias composiciones y analizar, desde un punto de vista geométrico, diseños cotidianos, obras de arte y configuraciones presentes en la naturaleza.  
27. Utilizar instrumentos, fórmulas y técnicas apropiadas para obtener medidas directas e indirectas en situaciones reales.  
28. Utilizar modelos lineales para estudiar diferentes situaciones reales expresadas mediante un enunciado, una tabla, una gráfica o una expresión algebraica.  
29. Identificar relaciones cuantitativas en una situación y determinar el tipo de función que puede representarlas.  
30. Analizar tablas y gráficos que representen relaciones funcionales asociadas a situaciones reales para obtener información sobre su comportamiento.  
31. Elaborar e interpretar informaciones estadísticas teniendo en cuenta la adecuación de las tablas y gráficas empleadas, y analizar si los parámetros son más o menos significativos.  
32. Elaborar e interpretar tablas y gráficos estadísticos, así como los parámetros estadísticos más usuales correspondientes a distribuciones discretas y continuas, y valorar cualitativamente la representatividad de las muestras utilizadas.  
33. Hacer predicciones sobre la posibilidad de que un suceso ocurra a partir de información previamente obtenida de forma empírica o como resultado del recuento de posibilidades, en casos sencillos.  
34. Aplicar los conceptos y técnicas de cálculo de probabilidades para resolver diferentes situaciones y problemas de la vida cotidiana.  
35. Planificar y utilizar procesos de razonamiento y estrategias diversas y útiles para la resolución de problemas, tales como el recuento exhaustivo, la inducción o la búsqueda de problemas afines y comprobar el ajuste de la solución a la situación planteada.  
ç36. Expresar verbalmente con precisión razonamientos, relaciones cuantitativas e informaciones que incorporen elementos matemáticos, valorando la utilidad y simplicidad del lenguaje matemático para ello.  
37. Instalar programas y realizar tareas básicas de mantenimiento informático. Utilizar y compartir recursos en redes locales.  
38. Utilizar vistas, perspectivas, escalas, acotación y normalización para plasmar y transmitir ideas tecnológicas y representar objetos y sistemas técnicos.  
39. Conocer las propiedades básicas de los plásticos como materiales técnicos, su clasificación, sus aplicaciones más importantes, identificarlos en objetos de uso habitual y usar sus técnicas básicas de conformación y unión de forma correcta y con seguridad.  
40. Conocer las propiedades básicas de los materiales de construcción, sus aplicaciones más importantes, su clasificación, sus técnicas de trabajo y uso e identificarlos en construcciones ya acabadas.  
41. Elaborar los documentos técnicos necesarios para redactar un proyecto técnico, utilizando el lenguaje escrito y gráfico apropiado.  
42. Realizar las operaciones técnicas previstas en el proyecto técnico incorporando criterios de economía, sostenibilidad y seguridad, valorando las condiciones del entorno de trabajo.  
43. Diseñar, simular y realizar montajes de circuitos eléctricos sencillos en corriente continua, empleando pilas, interruptores, resistencias, bombillas, motores y electroimanes, como respuesta a un fin predeterminado.  
44. Utilizar correctamente las magnitudes eléctricas básicas, sus instrumentos de medida y su simbología.  
45. Emplear Internet como medio activo de comunicación intergrupal y publicación de información.  

