# Competencias

El término competencias es polisémico:
* Competencias en el sentido de capacidades, *competencia matemática*  
* Competencias a nivel de jerarquía de administraciones, *la competencia es estatal o autonómica*  

Se introducen en LOMCE, y existe legislación asociada.   
[Definición LOMCE](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#a2)  
> c) Competencias: capacidades para aplicar de forma integrada los contenidos propios de cada enseñanza y etapa educativa, con el fin de lograr la realización adecuada de actividades y la resolución eficaz de problemas complejos.  

[Definiciones LOMLOE](https://boe.es/buscar/act.php?id=BOE-A-2022-4975#a2)  
> b) Competencias clave: desempeños que se consideran imprescindibles para que el alumnado pueda progresar con garantías de éxito en su itinerario formativo, y afrontar los principales retos y desafíos globales y locales. Las competencias clave aparecen recogidas en el Perfil de salida del alumnado al término de la enseñanza básica y son la adaptación al sistema educativo español de las competencias clave establecidas en la Recomendación del Consejo de la Unión Europea de 22 de mayo de 2018 relativa a las competencias clave para el aprendizaje permanente.  
> c) Competencias específicas: desempeños que el alumnado debe poder desplegar en actividades o en situaciones cuyo abordaje requiere de los saberes básicos de cada materia o ámbito. Las competencias específicas constituyen un elemento de conexión entre, por una parte, el Perfil de salida del alumnado, y por otra, los saberes básicos de las materias o ámbitos y los criterios de evaluación.  


En LOMLE aunque había ya competencias la titulación en ESO dependía en última instancia de evaluación final, que a su vez se basaba en estándares.  
[LOE-LOMCE Artículo 31. Título de Graduado en Educación Secundaria Obligatoria.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899&b=47&tn=1&p=20131210#a31)  
> Para obtener el título de Graduado en Educación Secundaria Obligatoria será necesaria la superación de la evaluación final, así como una calificación final de dicha etapa igual o superior a 5 puntos sobre 10.  

En LOMLOE se deja más claro que la evaluación son las competencias: la titulación en ESO depende de adquirir competencias   
[LOE-LOMLOE Artículo 31. Título de Graduado en Educación Secundaria Obligatoria.](https://boe.es/buscar/act.php?id=BOE-A-2006-7899#a31)  
>Obtendrán el título de Graduado en Educación Secundaria Obligatoria los alumnos y alumnas que al terminar la educación secundaria obligatoria hayan adquirido las competencias establecidas y alcanzado los objetivos de la etapa, sin perjuicio de lo establecido en el apartado 10 del artículo 28.  

(apartado 10 de artículo 28 hace referencia a adaptaciones en alumnos con necesidades especiales)  

[RECOMENDACIÓN DEL PARLAMENTO EUROPEO Y DEL CONSEJO de 18 de diciembre de 2006 sobre las competencias clave para el aprendizaje permanente (2006/962/CE)](http://eur-lex.europa.eu/legal-content/ES/ALL/?uri=CELEX:32006H0962)   

[RECOMENDACIÓN DEL CONSEJO de 22 de mayo de 2018 relativa a las competencias clave para el aprendizaje permanente](https://eur-lex.europa.eu/legal-content/ES/TXT/PDF/?uri=CELEX:32018H0604(01))   

[Orden ECD/65/2015, de 21 de enero, por la que se describen las relaciones entre las competencias, los contenidos y los criterios de evaluación de la educación primaria, la educación secundaria obligatoria y el bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2015-738)

En [Ideas clave de la LOMLOE - Raúl Diego](https://www.rauldiego.es/ideas-clave-de-la-lomloe/)  
Página 38 de 52 se comparan competencias 2006 vs 2018. Siguen siendo 8, pero cambian nombres  


[ Una arqueología de las competencias básicas. La educación como protocolo para unas identidades predecibles. Martínez Cárceles, Ibán. Departamento de Educación de la Generalitat de Cataluña](https://dialnet.unirioja.es/servlet/articulo?codigo=8178258)  
>En este ensayo, desenmascararemos parte del paradigma ideológico del sistema competencial, el cual se está desarrollando en Cataluña de forma acrítica. Para ello, nuestra hermenéutica se desarrollará como una arqueología, es decir, no revisaremos toda la literatura académica sobre las competencias básicas, sino que haremos emerger los elementos que más claramente nos anuncian las mentalidades del tema que analizamos. De ese modo, describiremos el relato expuesto por los que ejercen el poder, al analizar cómo surge la motivación para desarrollar un sistema desde la idea de competencia; cómo, posteriormente, se ha canalizado esa motivación hacia la construcción de un campo epistemológico a partir de la idea de “vida moderna”, y, por último, nos adentraremos en la disputa de conceptos clave como los de “prerrequisitos sociales”, “éxito” y “autonomía”. Todo esto, con la intención de desvelar uno de los posibles fines del relato competencial: crear una narración sobre lo que debe ser una identidad social aceptable.  


11 diciembre 2019. Hilo, se puede ver entero en [thread reader app](https://threadreaderapp.com/thread/1204881730466918400.html)  
[twitter Acontrapelo1/status/1204881730466918400](https://twitter.com/Acontrapelo1/status/1204881730466918400)  
Como veo que hay gente que no sabe de dónde viene el enfoque por competencias, vamos a hacer un hilo para explicar por qué es un planteamiento neoliberal que se está implantando a nivel global. AVISO: Esto va ser un poco largo y con muchas capturas de texto  
Estos son los economistas Gary Becker y Theodore W. Schultz. En los 60 desarrollaron la teoría del capital humano. Concibieron la educación como un factor de producción más, del que depende el crecimiento económico de un país por sus efectos en la productividad.  
Así, la educación es vista como una inversión que debe producir sus tasas de retorno. La teoría tiene la ventaja de librar al sistema de cualquier responsabilidad en el fracaso de los individuos, haciéndola recaer completamente sobre estos por no haber invertido adecuadamente en su educación.  
Este es David McClelland, psicólogo especializado en gestión de recursos humanos y motivación. En 1973 publicó un artículo titulado "Testing for competence rather than for 'intelligence'". Hacía notar que los tests de CI y pruebas de aptitud escolar no eran buenos para predecir el éxito laboral. En otras palabras, no informaban bien acerca del valor real del capital humano. Proponía otro enfoque, centrado en las competencias.   
Por aquellas mismas fechas, la UNESCO publica un informe titulado "Aprender a ser", dirigido por Edgar Fauré.  
Se trataba de ir diseñando los sistemas educativos del futuro para producir el "hombre nuevo" adaptado a las necesidades de la incipiente sociedad capitalista científico-técnológica. Veamos unos extractos:  
![](https://pbs.twimg.com/media/ELiZlzJXkAEDHRo.jpg)  
![](https://pbs.twimg.com/media/ELiZmo2WkAEJQZc.jpg)  
Mientras tanto, en 1968 se había creado el CERI (Center for Educational Research and Innovation), en el marco de la OCDE. Una organización, recordemos, centrada en el diseño de políticas económicas. El CERI desarrollará distintos programas para ir definiendo las competencias,  +   
Por ejemplo, el proyecto INES sobre Indicadores de Sistemas Nacionales de Educación en 1987, a partir del cual se desarrollaron, entre otros, el proyecto de Competencias Curriculares Transversales (CCC) y el de Indicadores de Capital Humano (HCI). Estos sirvieron de base para el proyecto DeSeCo (Definición y Selección de Competencias) a partir de los primeros resultados del programa PISA. Luego volveremos a DeSeCo.  
Antes tenemos que ver qué pasaba mientras en la UE, por entonces aun llamada CEE, de la mano de un personaje crucial: Jaques Delors. 
Presidente de la Comisión Europea entre 1985 y 1995. Artífice de la unión monetaria y, para lo que aquí nos interesa, promotor de dos libros blancos de enorme importancia para la implantación del modelo educativo por competencias. El primero, de 1993, era este:  
En él encontramos lo siguente. Léase atentamente, especialmente lo relativo al "principio fundamental de las diferentes categorías de acciones que deberán emprenderse" en los sistemas educativos.  
![](https://pbs.twimg.com/media/ELiZrhqXUAAxtFX.jpg)  
![](https://pbs.twimg.com/media/ELiZs8MWsAAdPbm.jpg)  
Dos años más tarde la CEE da a conocer un nuevo libro blanco centrado en la educación. Se llamaba 'Enseñar y aprender. Hacia la sociedad cognitiva'. La propuesta clave era desarrollar 'tarjetas personales de competencias' para que cada cual construya su cualificación.  
![](https://pbs.twimg.com/media/ELiZud2WoAEzup2.jpg)  
![](https://pbs.twimg.com/media/ELiZvcyXYAA6Bf6.jpg)  
Además, otro de los objetivos era acercar la escuela a la empresa.Todo ello, naturalmente, concebido en términos de valorización del capital humano. Aunque se diga que no hay que "reducir la finalidad de la educación al empleo", todo apunta en esa dirección.  
![](https://pbs.twimg.com/media/ELiZwHoX0AAIKbx.jpg)  
Acabado su mandato en la Comisión, Delors se pasa a la UNESCO, donde dirige un famoso informe publicado en 1996, 'La Educación Encierra un Tesoro', que actualiza el de Fauré. En él plantea los 4 pilares de la educación: aprender a conocer, aprender a hacer, aprender a ser y aprender a vivir juntos. Respecto al aprender a conocer la descripción empieza así: "este tipo de aprendizaje, que tiende menos a la adquisición de conocimientos clasificados y codificados que al dominio de los instrumentos mismos del saber [?] puede considerarse a la ver medio  y finalidad de la vida humana". Pero es en el desarrollo del aprender a hacer donde nos encontramos con la noción de competencia ¿Recordamos lo de acercar la escuela a la empresa y la valorización del capital humano? Pues...  
![](https://pbs.twimg.com/media/ELiZxOSXYAIY5a2.jpg)  
Al año siguiente la OCDE inicia el programa PISA y se pone en marcha el proyecto DeSeCo. En 1999 todavía no estaba muy claro qué quería decir exactamente el concepto de 'competencia' y cómo medir a partir de él el capital humano.  
![](https://pbs.twimg.com/media/ELiZyJCXkAAPyRb.jpg)  
![](https://pbs.twimg.com/media/ELiZzHsXYAEn-qY.jpg)  
![](https://pbs.twimg.com/media/ELiZzq3XsAAVYkA.jpg)  
Volviendo a la UE, en el año 2000 la Estrategia de Lisboa fijó como objetivo hacer de Europa "la economía basada en el conocimiento más competitiva y dinámica del mundo, capaz de crecer económicamente de manera sostenible con más y mejores empleos y con mayor cohesión social".  
Para ello era esencial la Estrategia Europea de Educación y Formación (ET2010). Fue en el contexto de esa estrategia en el que finalmente se publicó en 2006 un marco de referencia europeo (en España, la LOE ya había incorporado ese mismo año las competencias a la legislación):  
Si habéis llegado hasta aquí y leído las capturas ya sabéis que todo gira en torno al capital humano y la noción de empleabilidad. Pero ¿qué pasa con el mercado laboral? Pues que lleva décadas mostrando una tendencia a la polarización. La OCDE lo dice así:  
![](https://pbs.twimg.com/media/ELiZ3huWkAcowK8.jpg)  
Si el mercado se configura, según la teoría del capital humano, como una compra-venta de competencias, y dada la tendencia a la polarización, cabe decir que la inversión pública en la educación de los que aspirarán a empleos de baja cualificación es económicamente ineficiente.   
Les bastará con adquirir las competencias clave genéricas para su reciclaje continuo (educación para toda la vida). Así lo explica Nico Hirtt, en Educar bajo la dictadura del mercado de trabajo:  
![](https://pbs.twimg.com/media/ELiZ4r5XsAE3H5U.jpg)  
![](https://pbs.twimg.com/media/ELiZ5tSWwAAaRJS.jpg)  
Ahora me decís lo que queráis de que lo de las compentencias es muy innovador y es para favorecer el aprendizaje o cualquier otro mantra de la propaganda diseñada para hacernos tragar. No, es una estafa educativa que va contra los alumnos y contra los docentes. Bonus de la CEOE:  
![](https://pbs.twimg.com/media/ELiZ60VXsAUxv7u.jpg)  
![](https://pbs.twimg.com/media/ELiZ8YyWoAgKvH_?format=jpg)  
![](https://pbs.twimg.com/media/ELiZ7aRXsAUAqzs?format=jpg)  
![](https://pbs.twimg.com/media/ELiZ87IWwAckpE4?format=jpg)  



En [PERFIL DE SALIDA DEL ALUMNADO AL TÉRMINO DE LA EDUCACIÓN BÁSICA, DOCUMENTO BASE MARCO CURRICULAR DE LA LOMLOE (marzo 2021)](https://www.magisnet.com/wp-content/uploads/2021/03/Perfil-de-salida-del-alumnado-al-te%CC%81rmino-de-la-Educacio%CC%81n-Ba%CC%81sica.pdf)   

>La elaboración de este perfil parte de una visión a la vez estructural y funcional (OCDE-DeSeCo, 2002) de las competencias que debe orientar el objetivo que debe alcanzar el alumnado al concluir su  educación  obligatoria.  El  referente  esencial  para  identificar  las  competencias  clave  para  el aprendizaje permanente que han de conformar el Perfil de salida es la **Recomendación del Consejo de la Unión Europea de 2018, que define dichas competencias como «aquellas que todas las personas necesitan para su realización y desarrollo personales, su empleabilidad, integración social, estilo de vida sostenible, éxito en la vida en sociedades pacíficas, modo de vida saludable y ciudadanía activa»**.   
>El anclaje del Perfil de salida a la Recomendación del Consejo proporciona un importante valor añadido en el proceso hacia la deseable homologación europea e internacional de las enseñanzas que constituyen la educación básica del sistema educativo español. Se da con ello un paso alineado con la visión del aprendizaje, el estudio y la investigación europeas para el año 2025 recogido en Refuerzo de la identidad europea a través de la educación y la cultura,el documento que recoge los acuerdos adoptados en la reunión informal de los Jefes de Estado o de Gobierno de Gotemburgo, en noviembre de 2017.  

Cita

[RECOMENDACIÓN DEL CONSEJO de 22 de mayo de 2018 relativa a las competencias clave para el aprendizaje permanente (2018/C 189/01)](https://eur-lex.europa.eu/legal-content/ES/TXT/PDF/?uri=CELEX:32018H0604(01))  

[Refuerzo de la identidad europea a través de la educación y la cultura. Contribución de la Comisión al almuerzo de trabajo de los dirigentes, Gotemburgo, 17 de noviembre de 2017 : la escuela de gobernanza europea y transnacional](https://op.europa.eu/en/publication-detail/-/publication/bba57a9a-b6ff-11e8-99ee-01aa75ed71a1/language-es/format-PDF/source-187742935)  
 
[Refuerzo de la Identidad Europea a través de la Educación y la Cultura. Contribución de la Comisión al almuerzo de trabajo de los dirigentes, Gotemburgo, 17 de noviembre de 2017 INNOVACIÓN Y COMPETENCIA DIGITAL EN EL ÁMBITO DE LA EDUCACIÓN](https://www.culturaydeporte.gob.es/dam/jcr:02bab819-de63-4268-9dfc-fa83f799bfd2/5-got-innovation.pdf)  
Se citan competencias  
> SITUACIÓN ACTUAL  
> 44 % de los europeos no tienen competencias digitales básicas  
> 90 % de los puestos de trabajo exigirá cierto nivel de competencias digitales en el futuro
> Diciembre  de  2016:  puesta  en  marcha  de  la  Coalición  por  las  competencias  y  empleos  digitales  para  
formar a las personas en competencias digitales.   
> PRINCIPALES LOGROS HASTA LA FECHA  
18  Estados  miembros  han  adoptado  estrategias  sobre  competencias  digitales  encaminadas  
a mejorar la alfabetización y competencias digitales o están en proceso de hacerlo: Bulgaria, Chequia, 
Dinamarca, Alemania, Estonia, Irlanda, Grecia, España, Letonia, Lituania, Luxemburgo, Hungría, Malta, los Países 
Bajos, Austria, Portugal, Eslovenia y el Reino Unido.

**BONUS TRACK**
Veo que ese documento de la Unión Europea junto a lo anterior sobre competencias indica esto   
> 65% de los niños que 
comienzan la escuela 
primaria desarrollarán 
su carrera profesional 
en trabajos que 
no existen ahora 
mismo

Que es un mito, comentado aquí 
[El futuro de la educación: el 65% de no-sé-quién va a hacer no-sé-qué - elpais.com](https://elpais.com/elpais/2017/03/10/hechos/1489146364_790212.html)  
Expertos repiten, sin citar fuentes, que dos tercios de estudiantes tendrán empleos que no existen aún  

[Nuno Crato: «Cada vez somos menos exigentes, lo pagaremos caro» - eldiariodelaeducacion.com](https://eldiariodelaeducacion.com/2022/06/01/nuno-crato-cada-vez-somos-menos-exigentes-lo-pagaremos-caro/)  
> Entrevistamos Nuno Crato, ministro de Educación de Portugal entre los años 2011 y 2015, y considerado uno de los artífices del éxito educativo de este país. Su receta es sencilla: 1) currículum muy estructurado y exigente, y 2) una evaluación de sistema bien hecha. La exigencia educativa, sostiene, es la gran amiga del alumnado más desfavorecido.  

> Después de 2015 los resultados bajaron. En Finlandia pasó lo mismo. Los resultados subieron hasta 2016, introdujeron el currículum por competencias y los resultados bajaron. Y en Francia, lo mismo. No hablo bien de mí, hablo bien de las políticas centradas en el currículum y la evaluación.  

> ¿Es compatible poner todo ese empeño en la exigencia y el esfuerzo con un aprendizaje y una evaluación por competencias?  
No. En primer lugar, porque **nadie sabe lo que quiere decir competencias**.

> Entiendo que se refiere a la capacidad de aplicar los conocimientos, ya que al final el conocimiento puede ser algo muy ab>stracto. Yo, por ejemplo, hoy me costaría analizar una oración y en cambio me considero competente escribiendo.  
Yo también lo hago, y tengo dos premios literarios. Y quizás no sé gramática, pero estudié gramática, y algo de aquello se quedó conmigo.  
Pero vuelvo a lo que decía: nadie sabe lo que son exactamente las competencias. La idea de que debes medir la aplicación y no el conocimiento es un poco extraña. Te pongo un ejemplo con el inglés: sería ridículo decir que no tengo conocimiento de inglés pero lo aplico muy bien. Ambas cosas están muy unidas y entremezcladas, **el conocimiento solo lo puedes aplicar cuando lo tienes**.  

> Los partidarios de hablar de un enfoque competencial siempre dicen que no menosprecian los conocimientos.  
Los hay que lo hacen, dicen que el conocimiento no importa y que solo interesa la aplicación. Pongo otro ejemplo: ¿cómo puedes aplicar la filosofía? ¿No te interesa saber lo que dijo Platón pero sí aplicarlo? Es parte de la cultura saber que Platón era más idealista y Aristóteles más pragmático. Es como decir que, si no soy músico, ¿para qué tengo que aprender música? Pues es bueno aprender un poco de música. Esa idea parte de una teoría que no comparto, que es que las personas solo se interesan por las cosas cuando las aplican. No es verdad. Los jóvenes se interesan por los agujeros negros, y para su día a día no tienen ninguna importancia.  

> Parece que poner el énfasis en la aplicación puede atrapar más al estudiante, activar más la motivación…  
A eso no tengo nada que objetar. Pero si tú dices que solo evalúas competencias y no conocimientos estás menospreciando los conocimientos. Porque los hay que no son aplicables inmediatamente y que debes tener. Tú debes saber quién fue Julio César. ¿Eso es aplicable?  


[twitter sdinhamunimelb/status/1294755124619776000](https://twitter.com/sdinhamunimelb/status/1294755124619776000)  
What happened when France moved from a knowledge to a skills based curriculum  
![](https://pbs.twimg.com/media/Effk61oUcAAzuMX?format=jpg)  

[Explicando el declive educativo de Finlandia - unestalalalba.blogspot.com](https://unestelalalba.blogspot.com/2020/12/explicando-el-declive-educativo-de.html)  
> "El uso frecuente de estrategias didácticas autodirigidas y autónomas o de materiales de aprendizaje digitales en el colegio, estaba asociado con unos resultados de aprendizaje más bajos de los estudiantes en un amplio rango de ámbitos de conocimiento. Por contra, las estrategias didácticas dirigidas por el profesor, estaban vinculadas a unos resultados de aprendizaje más altos. Aún más allá, el uso frecuente de materiales de aprendizaje digitales o de estrategias didácticas autodirigidas tenía un impacto más negativo en los resultados de aprendizaje de los alumnos de un contexto socieconómico de riesgo"  

[¿Qué tiene que enseñar la escuela? Educación abre el melón del cambio del currículo educativo - eldiario.es](https://www.eldiario.es/sociedad/educacion-cambiar-curriculo-educativo-contenidos-competencias_1_6497592.html)  
Aunque existe bastante unanimidad en torno a la necesidad de cambiar un currículo "obsoleto y enciclopédico" que prima la memorización de conocimientos, la apuesta del Ministerio por un modelo competencial y transversal no acaba de convencer a todos   

[Prop del 80% dels docents de secundària prefereixen els continguts - fundacioepisteme.cat](https://fundacioepisteme.cat/2021/05/03/prop-del-80-dels-docents-de-secundaria-prefereixen-els-continguts/)  
>El Batxillerat s’ha convertit recentment en un tema central de debat educatiu. Una enquesta realitzada telemàticament per la Fundació Episteme a un univers aproximat de 1.200 docents d’ensenyament secundari mostra que prop del 80% (77.1 %) opina que l’ensenyament basat en continguts serveix per assolir un millor aprenentatge, mentre que només un 22,5 % considera millor l’educació per competències.  

[Las competencias, eje vertebrador del modelo curricular de la Lomloe - eldiariodelaeducacion.com](https://eldiariodelaeducacion.com/2021/07/14/las-competencias-eje-vertebrador-del-modelo-curricular-de-la-lomloe/)  

[twitter PascualGil1/status/1509589197857959940](https://twitter.com/PascualGil1/status/1509589197857959940)  
HILO. Más allá de la "cronologicidad" de la Historia en la LOMLOE...  
Las palabras "gestión" o "gestionar" aparecen 139 veces en el decreto LOMLOE de la ESO, en 219 páginas. La palabra "Filosofía" aparece 0 veces. No es un análisis sesudo ni elevado, pero sí me parece sintomático del estado de salud del sistema educativo de un país, y del país.  
Y con esto no busco atacar a la LOMLOE porque odio al PSOE o me gusta el PP o patatas. No es ningún secreto para nadie mi inclinación decididamente marxista, mi ácida aproximación materialista y, claro está, mi rechazo absoluto al paradigma pedagógico imperante desde hace décadas  
al que considero totalmente mediatizado en sus formas y en sus fines por la cosmovisión parcial e interesada que impone un modelo socioeconómico hegemónico que busca someter a sus lógicas a personas e instituciones, entre ellas la escuela.  
Sí, he leído el currículo de la LOMLOE. Sí, se puede leer la ley y discrepar total o parcialmente. Y yo discrepo porque, precisamente, veo en la LOMLOE un pasito más en pos de esa escuela demasiado alineada con lo que que dicen que la "sociedad" (en realidad, el mercado) demanda.  


[twitter followero/status/1519679910738075650](https://twitter.com/followero/status/1519679910738075650)  
Esto os va encantar (bueno, a alguno puede que no): Resulta que me he puesto a hacer los deberes y he empezado a leerme la LOMLOE. Más de uno tendrá presente el desplante triunfalista de algunos al sugerir: "Chupaos esa, obsoletos, que LA LEY ahora prescribe el enfoque competencial y vuestra obsesión con el CONOCIMIENTO pasará a la historia". En sus mentes la asimilación de conceptos está llamada a desaparecer porque la desterrarán las competencias clave, unas esencias inefables de imprecisa y ambigua definición
"que no sé definir pero yo me entiendo", han llegado a decir algunos como si no resultase fundamental contar con una definición precisa, unívoca y compartida para operar profesionalmente en una categoría que exige coordinación. Bueno, así pues, en anteriores capítulos de "Yo y mis competencias":  
En anteriores capítulos de "Yo y mis competencias":
El cabecilla del equipo de 7 "técnicos" que dan forma a la LOMLOE, el lisensiado César Coll, confiesa su pesar al intentar acordar una definición de competencias. 
[César Coll: “Sería un gran logro que el Ministerio y las CCAA consensuaran los mínimos del nuevo currículo”](https://eldiariodelaeducacion.com/2021/04/27/cesar-coll-seria-un-gran-logro-que-el-ministerio-y-las-ccaa-consensuaran-los-minimos-del-nuevo-curriculo/) Entrevista publicada hace justo 366 días.  
De hecho, cuando se filtró el borrador del currículo este -sorprendentemente- no contenía las definiciones básicas, que es algo así como parirlo muerto. Total, que se ve que tiraron por el camino de enmedio y convinieron en que "nosotros, lo que diga Europa".  
Pero vamos, literalmente, lo que diga Europa. Como si les diera vergüencita simplemente transcribirlas ellos. Te citan el documento al que tienes que ir si quieres enterarte de lo que son las dichosas competencias clave.  
Total, que vas al documento de marras, la Recomendación del Consejo de la U.E. de fecha 22 de mayo de 2018 y, tras 6 páginas de bla, bla, bla, en un anexo encuentras por fin la codiciada definición de competencias clave que reza tal que así:  

> Competencias clave  
> A efectos de esta Recomendación, **se definen las competencias como una combinación de conocimientos, capacidades y actitudes**, en las que:  
> d) los **conocimientos se componen de hechos y cifras, conceptos, ideas y teorías que ya están establecidos y apoyan la comprensión de un área o tema concretos**;  
> e) las capacidades se definen como la habilidad para realizar procesos y utilizar los conocimientos existentes para obtener resultados;  
> f) las actitudes describen la mentalidad y la disposición para actuar o reaccionar ante las ideas, las personas o las
situaciones.

"...Se definen las competencias (clave) como una combinación de conocimientos, capacidades y actitudes".
Vamos, hemos vuelto a la LOGSE de 1990 en la que conocimientos (conceptos), capacidades y actitudes ya se contemplaban como eje vertebrador de los contenidos. Pa' esto tanto.  
Pero, ojo, que por si acaso alguno dice que conocimientos son "nosequécosa" mística, a renglón seguido Europa dice que los conocimientos "se componen de hechos y cifras, conceptos, ideas y teorías que ya están establecidos y apoyan la
comprensión de un área o tema concretos".  
Nenes, HECHOS, CIFRAS (¿fechas?), CONCEPTOS...¿Podría quedar más claro?  
Vamos, que en el siglo XIX no hubieran visto problema en bendecir esta definición.  
Así que no toméis todavía la senda de los elefantes, aunque estad ojo avizor porque el problema existe y está en el empeño puesto en escamotear el sentido de las cosas (difícilmente podría Europa haber puesto por escrito otra cosa diferente a que la escuela está para aprender y que aprender es lo que es) con la interposición entre lo escrito y el ciudadano de una serie de pantallas de humo de la mano de gurús subvencionados y toda una serie de oráculos (prensa, etc.) que te dicen que "el sentido de la escuela será considerado como la parte contratante de la primera parte", para ver si así lo que no ha habido más remedio que poner por escrito queda sin efecto.  

[twitter Indomitus8/status/1557284289837993984](https://twitter.com/Indomitus8/status/1557284289837993984)  
El profesor de educación obligatoria no debe actuar ni organizar su trabajo asumiendo el papel de custodio del currículum ni el de notario de los déficits del estudiantado respecto de ese currículum.  
Para el resto: la legislación vigente y la Declaración de los Derechos Humanos.  
[twitter Indomitus8/status/1557301900265005061](https://twitter.com/Indomitus8/status/1557301900265005061)  
Los conceptos/contenidos/saberes/conocimientos son de rango inferior a la hora de programar y evaluar. (Sigue)  
Lomloe (Lengua). No se contabilizan como notario los contenidos, sino cómo los maneja el estudiante; no se debería programar de contenidos a competencias sino al revés: para que aprenda esto (competencia) necesitará esto (contenidos y habilidades)  
[Real Decreto 217/2022, de 29 de marzo, por el que se establece la ordenación y las enseñanzas mínimas de la Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975)  
> Dado el enfoque inequívocamente global y competencial de la educación lingüística, la gradación entre cursos no se establece tanto mediante la distribución diferenciada de saberes, sino en función de la mayor o menor complejidad de los textos, de las habilidades de producción o interpretación requeridas, del metalenguaje necesario para la reflexión sobre los usos, o del grado de autonomía conferido a los estudiantes.  
[Artículo 17. Título de Graduado en Educación Secundaria Obligatoria.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-4975#a1-9)  
1. Obtendrán el título de Graduado en Educación Secundaria Obligatoria los alumnos y alumnas que, al terminar la Educación Secundaria Obligatoria, hayan adquirido, a juicio del equipo docente, **las competencias clave** establecidas en el Perfil de salida y alcanzado los objetivos de la etapa, sin perjuicio de lo establecido en el artículo 20.3.  
2. Las decisiones sobre la obtención del título serán adoptadas de forma colegiada por el profesorado del alumno o la alumna. Las administraciones educativas podrán establecer criterios para orientar la toma de decisiones de los equipos docentes con relación al grado de adquisición de **las competencias clave** establecidas en el Perfil de salida y en cuanto al logro de los objetivos de la etapa, siempre que dichos criterios no impliquen la fijación del número ni la tipología de las materias no superadas.  
3. El título de Graduado en Educación Secundaria Obligatoria será único y se expedirá sin calificación.  
4. En cualquier caso, todos los alumnos y alumnas recibirán, al concluir su escolarización en la Educación Secundaria Obligatoria, una certificación oficial en la que constará el número de años cursados y el nivel de adquisición de **las competencias clave** definidas en el Perfil de salida.  
5. Quienes, una vez finalizado el proceso de evaluación de cuarto curso de Educación Secundaria Obligatoria, no hayan obtenido el título, y hayan superado los límites de edad establecidos en el artículo 5.1, teniendo en cuenta asimismo la prolongación excepcional de la permanencia en la etapa que se prevé en el artículo 16.7, podrán hacerlo en los dos cursos siguientes a través de la realización de pruebas o actividades personalizadas extraordinarias de las materias o ámbitos que no hayan superado, de acuerdo con el currículo establecido por las administraciones educativas competentes y con la organización que dichas administraciones dispongan.  

Hace tiempo que, por ejemplo, en el MECRL se habla de corregir y evaluar a partir de lo que se sabe hacer, no a partir de los errores que se cometen o déficits —y descontar: “¿sabe el estudiante HACER (x)?”, “en que punto está dentro de la rúbrica estandarizada que se aplica”.  
Sin evaluación formativa (“instrumentos de evaluación variados y con capacidad diagnóstica y de mejora”—captura en tuit anterior), se califica y notifica que “sabes este (x)” (casi siempre un contenido/saber/conocimiento/dato/información) xq permiten aritmomorfizar con facilidad.  
Por eso, el profesor ni actúa de custodio, ni de guardián, ni de mero notario del currículum: vela porque los estudiantes accedan a él y no se limita a notificar quien llega o no a un determinado nivel.  


[twitter NikoDimitratos/status/1566404428340596736](https://twitter.com/NikoDimitratos/status/1566404428340596736)  
La LOMLOE es que tu hijo adquiera la competencias clave, específicas y que por tanto logre CCL2, CCL3, STEM4, CD1, CD2 y CC1 cuando haga un esquema.  
Esto es la ley. Convertir lo sencillo en una gigantesca estupidez.  
Por cierto, en la LOMCE se hacía la misma estupidez respecto a los "estándares evaluables". Seguimos con lo mismo. Da igual el gobierno.  

4 junio 2020  
[¿Se puede evaluar hoy a los alumnos igual que antes de la pandemia? - publico.es](https://blogs.publico.es/otrasmiradas/33918/se-puede-evaluar-hoy-a-los-alumnos-igual-que-antes-de-la-pandemia/)  
Naiara Bilbao Quintana, Profesora del departamento de Didáctica y Organización Escolar, Universidad del País Vasco / Euskal Herriko Unibertsitatea  
Arantzazu López de la Serna, Profesora Facultad de Educacion de Bilbao / Irakaslea Bilboko Hezkuntza Fakultatea, Universidad del País Vasco / Euskal Herriko Unibertsitatea  

> Aprendizaje basado en competencias  
Sin embargo, si se atiende al principio básico que define el aprendizaje basado en competencias de Jaques Delors, es decir, la necesidad de promover en los estudiantes el desarrollo de transferencias, no parece incompatible. Desarrollar transferencias implica poner el énfasis, no tanto en lo que el estudiante sabe y conoce, sino en las competencias y habilidades que se han alcanzado.  

Se cita [Delors, J. (1996.): “Los cuatro pilares de la educación” en La educación encierra un tesoro. Informe a la UNESCO de la Comisión internacional sobre la educación para el siglo XXI, Madrid, España: Santillana/UNESCO. pp. 91-103.](https://uom.uib.cat/digitalAssets/221/221918_9.pdf)  

7 septiembre 2022  
[El secretario de Estado de Educación: "Repetir curso es carísimo y absolutamente ineficaz" - publico.es](https://www.publico.es/sociedad/secretario-educacion-repetir-curso-carisimo-absolutamente-ineficaz.html)  

> El Ministerio de Educación quiere romper con dinámicas ancladas en el pasado. Tal y como piden desde Europa, las reformas implantadas por Pilar Alegría quieren dejar de ver la enseñanza como un espacio en el que memorizar fechas, datos y números, para pasar a un aprendizaje por competencias.   

> La segunda razón es porque el puro embutido de conocimientos no es útil para la vida, no es competencial. ¿De qué nos sirve memorizar un montón de cosas si no tenemos su aplicación práctica? La vida está globalizada y los conocimientos también deben estarlo. Tienen que tener una utilidad práctica porque si no el alumno tampoco estará motivado.  

[twitter Otrosvendran/status/1565960718482116608](https://twitter.com/Otrosvendran/status/1565960718482116608)  
No sé de qué extraña fosa abisal ardiente habrán surgido los tipos que pautan la Educación en la UE, ni quiénes son, ni qué pretenden, ni por qué es imposible acceder a ellos. Opacidad total.  
Pero cuanto más profundizo en la "nueva educación" más destructiva me parece.  
Digo "Europa" porque:  
1- Las famosas "competencias" vienen de una "Recomendación" del parlamento Europeo (2006/962/CE) que aquí aplicamos cagando leches  
2- La actual LOMLOE se fundamenta en "los objetivos fijados por la Unión Europea y la UNESCO para la década 2020-2030"  
Y digo opacidad porque, igual que sucede con la ley española, nunca se podrá saber a quién se le ocurren estas cosas. No se les puede, por tanto, pedir cuenta de la racionalidad de las medidas educativas que hemos de aplicar los docentes y de sufrir los alumnos.  
No caben diálogo ni réplica. Ni la mínima posibilidad de saber el origen los pilares científicos que demuestra la eficacia de cuanto se nos propone o impone.   
El caso es que, resumiendo para legos lo que la nueva Educación trae consigo, podría decirse que consiste en...  
...relegar el conocimiento en bruto a un segundo plano (además de disminuirlo) para potenciar las actividades prácticas donde el curre se lo pegue el alumno, en soledad pero también sí o sí en grupo, con el docente de asesor, controlador y evaluador. También el docente debe...  
...crear las actividades de la nada para tener a los chavales ocupados.   
Aunque la terrible y detestada memorización sigue ahí (porque exámenes habrá), es secundaria. Lo importante es que el alumno pueda aplicar de forma PRÁCTICA lo aprendido en clase. Todo esto, además...  
...sazonado de VALORES. Es decir, que no solo se debe enseñar cómo es el mundo y cómo está estructurado el conocimiento. También se debe enseñar cómo comportarse, cómo ser bueno y cuáles son las opiniones correctas sobre la sociedad.  
Les pongo un ejemplo.  
Vamos con LOMLOE.  
Supongamos que toca impartir El Quijote.  
No basta con explicar la estructura del libro, su génesis, el contexto histórico social, la trama o el por qué de su genialidad. Tampoco basta con leer fragmentos relevantes para que palpen, por ejemplo, su humor y su estilo.  
Se trataría más bien de coger valores "universales" como la Libertad, el papel de la mujer en la sociedad o el idealismo. Es decir, usar el libro como ocasión para potenciar cosas que la ley considera positivas socialmente, no como fin en sí mismo ni como obra maestra total.  
A partir de ahí, se les podrían, por ejemplo, dar a los niños varios fragmentos para comentar episodios como el de los molinos, o el de la pastora Marcela, para comentarlos a la luz de nuestros días. Se les podría poner por grupos para que analizaran las diferencias...  
...entre las condiciones femeninas de hoy y las del pasado. Y luego cada grupo extraería las conclusiones y las expondría en clase.  
A continuación, podrían dárseles varias situaciones de conflicto social femenino que ellos resolvieran al modo de la pastora Marcela, por ejemplo.  
Obviamente, también se les darían datos sobre El Quijote (autor, fecha de publicación, importancia, etc...).  
El ejemplo es algo apresurado, pero para que se me entienda me parece claro.  
Daré otro.  
Supongamos el concepto de "locus amoenus": "Lugar ideal".  
Podría hacerse un recorrido por los "lugares ideales" de la historia de la literatura: los prados de Berceo, el Tajo de Garcilaso, el huerto de Fray Luis...  
Y ellos deberían extraer lo común a todos ellos pero cambiando la perspectiva a través de cada época.  
A todo esto, también es necesario que USEN PANTALLAS (esto al legislador le flipa), de modo que sería necesario que desarrollaran trabajos digitales en que, por ejemplo, explicaran al resto del aula las diferencias mentadas entre Garcilaso y Fray Luis.  
Este enfoque educativo es terrible, a mi juicio.  
Expongo motivos:  
1- Las obras de arte delegan su función estética en su función ética. Es cierto que toda obra de arte es siempre también ideológica. Pero no las estudiamos por eso. Sino por su calidad literaria.  
El hecho de usarlas como "excusa" para nuestro sistema de valores contemporáneo atora el entendimiento recto del pasado y convierte la obra en algo que meramente está ahí para ser usada, no leída, ni entendida desde ella misma, ni disfrutada.  
(Aunque se supone que encima...  
...deben sentir "fruición" (sic) con la lectura: a ver cómo).   
2- Incluso aceptando lo anterior, el proceso metodológico lleva muchísimo más tiempo que si el docente, en CLASE MAGISTRAL participativa, explicara directamente los valores clave. Y punto pelota.  
Es decir, en vez de ponerles a estar entretenidos buscando lo que su edad les dicte y deje, mientras el docente va mirando cada grupo o cada alumno a ver qué se le ha ocurrido, que se sacaran en voz altas las movidas que importan según la ley y listo.  
Así cabría más.  
3- Por experiencia propia (como alumno) y por experiencia docente, si los niños exponen a otros niños, ni los primeros manejan la materia, ni los segundos prestan demasiada atención. Los expositores además pueden estar nerviosos, hablar bajo, ser lentos... y sus compañeros...  
...estar fijándose otras mil cosas.  
Todo eso no sucede si el docente explica el contenido de modo ordenado, riguroso y directo, mientras lo alumnos, que no son idiotas, absorben y comprenden lo explicado.  
En fin, no seré yo quien desdeñe los trabajos grupales o prácticos, ni las exposiciones orales al resto del alumnado, pero convertirlas en el EJE DE LA EDUCACIÓN me parece un modo de impedirles un estudio hondo y reflexivo. Además de que ralentiza hasta el extremo las clases.  
4- La retención de los contenidos se vuelve más etérea, puesto que si yo estudio hasta 20, igual con el tiempo he retenido 5. Pero si estudio hasta 5, con el tiempo acaso recuerde que, en El Quijote, había una pastora enfadada que se llamaba Marcela... y poco más.  
Por todo esto fuera poco, en Lengua Castellana y Literatura (en el resto no lo sé, imagino que menos), el temario se repite EN BUCLE, una y otra vez, y otra vez, hasta el mareo, año tras año. No tanto en Literatura, pero en Lengua es tedioso.  
P.ej: desde 1º de ESO ven las variedades del español. Cada curso se les da más información, pero la base es la misma. La sensación de "déjà vu" les dará mareos. No es nuevo, con la anterior legislación ya pasaba. Pero lo han potenciado.   
Es decir: desinterés garantizado.  
Y todo esto que les he contado ocurre en Lengua y Literatura Castellanas.   
Imaginen ahora cómo enseñar así en Biología, Matemáticas, Física y Química o ¡Educación Física!  
A resultas de estas metodologías que evalúan "competencias" y no conocimiento abstracto...  
...la Educación pierde contenidos por todas partes; los niños pierden también la posibilidad de saber con profundidad; la escuela se parece más a una empresa que trabaja con "proyectos" orientados a un fin práctico, y no como fines en sí mismos; y el docente...  
...se convierte en "pastor de almas" más que en transmisor de conocimientos racionales reglados y objetivo.  
No obstante lo cual, los profesaurios son una clase dura de roer. Los más de los profesores seguirán aplicando como puedan la clase magistral.  
Sazonada de actividades, de vez en cuando. Es decir: la ley, como suele suceder, quizá no cambie tanto las cosas. Pero, a mi juicio (y espero haber separado mi opinión de la descripción), si se aplica funcionará para desproveer AÚN MÁS al alumnado del conocimiento profundo.  
Esta mierda (aquí ya me pongo subjetivo) es lo que se lleva aplicando décadas en EEUU, de forma mayoritaria. Ahora piensen en la cultura general que parece tener el americano medio.  
Les ha ido fenomenal.  
[Uno de cada cuatro estadounidenses no sabe que la Tierra gira alrededor del Sol. Según una encuesta encargada por la National Science Foundation, tan solo el 39% cree que el Universo empezó con una gran explosión](https://www.abc.es/ciencia/20150303/abci-americanos-tierra-gira-201503021754.html)  
Por no hablar de que, como siempre, todo esto está orientado hacia "el empleo del futuro" donde deben aprender a "Aceptar la incertidumbre como una oportunidad" (RD217/2022: Anexo I).  
Como si se pudiera saber qué hará falta de aquí a diez años.   
"Sabiduría" no parece.  
En fin, retomo la cuestión donde lo empecé:  
Que la Educación europea (y también la española) esté controlada y dirigida por gente desconocida que impone metodologías sin probada efectividad, y con la cual no puede ni debatirse, y que no ha sido ni elegida mediante sufragio...  
...debería ponernos, creo yo, los pelos de punta.  
Porque significa que nadie que sepa realmente de educación está gestionando la Educación.  
Ya que quien verdaderamente sabe de algo lo que hace es exponer sus ideas y someterlas a crítica.  
Quien se oculta, en cambio...  
...manifiesta con su propia ocultación las tenebrosas intenciones de lo que quiere imponer sin que nadie se las discuta.   
Y hasta aquí mi hilo sobre por qué esto me parece otro clavo más en el ataúd de la educación racional ilustrada tal y como veníamos concibiéndola.  

[twitter pbeltranp/status/1421547872806391809](https://twitter.com/pbeltranp/status/1421547872806391809)  
Que la noción de competencia es hasta cierto punto ambigua y controvertida es un hecho. Es algo que recogen muchísimos de los investigadores en educación, además de tuiter. No es un problema grave. Cada marco que la emplea, establece su descripción en los fundamentos teóricos.  
A ver cuántos de los que se quejan de que es un concepto "mal definido" sabrían decir qué es un número. Y creo que todos usamos números sin tener que hacer un tratado sobre la cuestión. Para todo. Incluso para tomar decisiones de vida o muerte.  
A mí me gusta la posición que indica Font (2011), que es la de entender la competencia de una forma que no sea contradictoria con las directrices curriculares vigentes.  
[Competencias profesionales en la formación inicial de profesores de matemáticas de secundaria](https://www.researchgate.net/publication/267218034_Competencias_profesionales_en_la_formacion_inicial_de_profesores_de_matematicas_de_secundaria)  
Así, en el seno del CCDM (un modelo de conocimientos y competencias didáctico matemáticos para profesores) se emplea como indicador de competencia "una acción eficaz realizada en un determinado contexto con una determinada finalidad."  
Esto es lo que pone el BOE (RD 1105/2015)  [en el preámbulo](https://www.boe.es/buscar/act.php?id=BOE-A-2015-37#preambulo)  

> En línea con la Recomendación 2006/962/CE del Parlamento Europeo y del Consejo, de 18 de diciembre de 2006, sobre las competencias clave para el aprendizaje permanente, este real decreto se basa en la potenciación del aprendizaje por competencias, integradas en los elementos curriculares para propiciar una renovación en la práctica docente y en el proceso de enseñanza y aprendizaje. Se proponen nuevos enfoques en el aprendizaje y evaluación, que han de suponer un importante cambio en las tareas que han de resolver los alumnos y planteamientos metodológicos innovadores. **La competencia supone una combinación de habilidades prácticas, conocimientos, motivación, valores éticos, actitudes, emociones, y otros componentes sociales y de comportamiento que se movilizan conjuntamente para lograr una acción eficaz. Se contemplan, pues, como conocimiento en la práctica, un conocimiento adquirido a través de la participación activa en prácticas sociales que, como tales, se pueden desarrollar tanto en el contexto educativo formal, a través del currículo, como en los contextos educativos no formales e informales.**  
**Las competencias, por tanto, se conceptualizan como un «saber hacer» que se aplica a una diversidad de contextos académicos, sociales y profesionales. Para que la transferencia a distintos contextos sea posible resulta indispensable una comprensión del conocimiento presente en las competencias, y la vinculación de éste con las habilidades prácticas o destrezas que las integran.**  

Y bueno, el gran error, desde mi punto de vista, es que las competencias del currículo LOMCE son absolutamente generales. Sin haber desarrollado competencias específicas por materias (como en Cataluña, por ejemplo), hablar de competencias (desde el currículo oficial) es ridículo.  

En octubre 2022 descubro que en universidades también se habla de competencias específicas, no sé si antes de LOMLOE  

[twitter eliasmgf/status/1586678536814923777](https://twitter.com/eliasmgf/status/1586678536814923777)  
Es curioso que para establecer el modelo competencial de la LOMLOE y cargarse, de facto, el valor de la memorización de datos, el gobierno se haya basado en una recomendación del Consejo Europeo que nadie se ha tomado la molestia de leerse, y que dice literalmente esto:  
> (7) En la economía del conocimiento, **memorizar hechos y procedimientos es clave**, aunque no suficiente para el progreso y el éxito.   

Ahí tenemos que reconocer que nos han metido un gol a los docentes. “Hay que hacer esto porque lo dice el Consejo Europeo”. Primero, el CE no dice eso. Segundo, aunque lo dijera, no es vinculante. [El enlace](https://eur-lex.europa.eu/legal-content/ES/TXT/PDF/?uri=CELEX:32018H0604(01)&from=SV)  
Además nos hemos tragado un hombre de paja del tamaño del Everest (la educación basada exclusivamente en la memorización, que dudo que se haya producido jamás, salvo en “enseñanza” religiosa), y nos hemos estado defendiendo cuando nos acusan de algo que nunca hemos hecho.  
La recomendación del Consejo Europeo dice que la memorización “es clave”, lo dice blanco sobre negro, porque es evidente que es cierto, como es evidente lo que dice a continuación: que no es lo único importante, que no es suficiente.  
Nuestro gobierno, me temo, necesita imperiosamente reducir unos datos de fracaso escolar que nos dejan en muy mal lugar. Una forma de reducir este fracaso sería emprender una reforma seria de verdad del sistema educativo, poniendo el dinero que hiciera falta.  
Pero es más fácil hacer prescindibles algunos elementos del currículo (las competencias no son ninguna novedad, llevan cientos de años en el sistema educativo, con ese u otro nombre).  
Igual que para reducir la tasa de criminalidad hay dos vías: analizar seriamente el problema, afrontarlo con valentía y echarle dinero, o sacar delitos del Código Penal. Lo segundo es gratis. Si conducir borracho ya no es delito, reduces la criminalidad publicando un BOE.  
Hay muchísimos problemas soterrados o vistos de soslayo en nuestra Educación. ¿En nuestro país, realmente sirve de ascensor social? Si no es así, ¿de qué sirve esforzarse tanto? ¿Para qué quiero aprobar?  
Si el nivel adquisitivo de los padres es uno de los principales o el principal factor predictivo del nivel adquisitivo de los hijos, ¿quién quiere estudiar y para qué?  
Nos estamos dejando millones, muchos millones, en rogar a algunos chicos para que estudien, y muchos de ellos nunca lo van a hacer porque no le ven sentido. Y yo creo que tenemos que  empezar a arreglar la casa por ahí.  


[twitter followero/status/1584141505509621760](https://twitter.com/followero/status/1584141505509621760)  
Fragmento de mi propia programación. El marco legal. (Lo vuelvo a subir porque tenía un lapsus en las siglas, puse LOMCE por LOE, que uno se pierde ya).  
![](https://pbs.twimg.com/media/FfwAIFcXwAAKpmv?format=jpg)  

1 noviembre 2022  
[LOMLOE: de vuelta a la caverna](https://ctxt.es/es/20221101/Firmas/41106/educacion-lomloe-ocde-javier-mestre-competencias-contenidos.htm)  
> Los capitostes de la OCDE no están demasiado interesados en una ciudadanía ilustrada, sino en formar obreros capaces de entender el letrero de prohibido fumar en la fábrica de explosivos  

> Las lumbreras legislativas han fusionado la distopía del DUA con el famoso aprendizaje por competencias. Este último tiene que ver con la entrevista al secretario de Estado, cuando afirma que los conocimientos “tienen que tener una utilidad práctica porque si no el alumno tampoco estará motivado”. Al mostrar a mis alumnos de Bachillerato estas declaraciones, reaccionaron con indignación: “Nos toman por idiotas”. Nada del saber por el saber. Olvidemos que hay tantísimas cosas que aprender sin aplicación práctica porque son escalones del proceso de aprendizaje de las disciplinas y son abstractas e imposibles en la vida real.   

> Por lo demás, el delirante aprendizaje por competencias lleva dos décadas con nosotros y apenas se ha llevado a la práctica en la realidad de la docencia de los últimos años. Seguramente vamos a seguir explicando y poniendo exámenes y notas numéricas, y los estudiantes continuarán memorizando contenidos inútiles y haciendo ejercicios sin ninguna aplicación práctica. Pero todo a costa de un incremento horrible del trabajo burocrático del docente.  

14 noviembre 2022  
[El discurso rojipardo en educación. Colectivo de docentes DIME - ctxt.es](https://ctxt.es/es/20221101/Firmas/41295/discurso-rojipardo-educacion-lomloe-izquierda.htm)  


[Hablan los profesores: la reforma educativa es un experimento y vuestros hijos son cobayas - elconfidencial.com](https://www.elconfidencial.com/cultura/2022-11-14/lomloe-reforma-educativa-ocre-episteme-profesores_3520591/)  

> P. ¿Las competencias son una mala herramienta para evaluar?  
XM. Eso es. Son imprecisas desde la raíz. ¿Qué competencias básicas tiene un alumno que saca un aprobado raso en una prueba de competencias básicas? Fíjate en el adjetivo "básicas". Un cinco significa estar a la mitad de las competencias básicas. Es decir: significa unas carencias brutales. Por lo tanto, el sistema está viciado en sí mismo de entrada.  
IM. Totalmente de acuerdo con Xavier. Los criterios de evaluación actuales son un queso gruyere. Los docentes llevan un inicio de curso angustioso, porque no ven cómo cuadrar su trabajo con la ley, con esos criterios ambiguos. No pueden determinar lo que es un aprobado y lo que es un suspenso. Nos vamos a encontrar con una primera evaluación devastadora para los profesores y el alumnado. Más de uno acabará dando aprobado general por ahorrarse problemas. Los docentes están haciendo constar en sus programaciones unos criterios de calificación grotescos, con variables que cuentan porcentajes absurdos. Es un galimatías infranqueable y carente de sentido o rigor.  
P. ¿Las llamadas competencias son invaluables?  
XM. En primer lugar, están indefinidas. Al congreso asistirá el exministro de educación de Portugal, Nuno Crato, quien dijo hace poco algo muy cierto: que nadie ha sabido definir qué son las competencias. A mí lo que se me ocurre es una definición un poquitín sarcástica: te enseñen a hacer la "o" con un canuto sin saber lo que es la "o" ni lo que es un canuto.   

16 noviembre 2022 
[Currículo escolar y educación democrática](https://ctxt.es/es/20221101/Firmas/41308/)  
>Muchas de las voces críticas con el aprendizaje competencial alegan su vinculación a organismos económicos, llegando, en algunos casos, a conectarlo con un interés en la sombra por formar una ciudadanía inculta, acrítica y manejable, hábil únicamente en las destrezas que reclama el ámbito laboral. La relación que el aprendizaje competencial establece, no ya con el saber, sino con “saber hacer”, ha sumado suspicacias, al considerar que esta exigencia destierra el valor del conocimiento per se. Quienes se sitúan en esta lógica argumental alegan un pretendido vaciado de contenidos en el currículo como parte de una catástrofe calculada.   
Pero el término competencia tiene filiaciones muy anteriores a la OCDE, muy alejadas de sus vinculaciones con la “empleabilidad” de la ciudadanía y de orientación inequívocamente progresista. Como sin duda sabe cualquier docente de Lengua, hace ya décadas que todos los currículos se alinean (tanto en España como en Europa y a uno y otro lado del Atlántico) con los denominados enfoques comunicativos en la enseñanza del lenguaje. Es decir, con el objetivo de desarrollar la competencia comunicativa del alumnado, entendida como “aquello que un hablante necesita saber para comunicarse de manera eficaz en contextos socialmente significantes” (John Gumperz y Dell Hymes, 1972). De lo que se trata, por tanto, es de asegurar que chicas y chicos no solo aprendan cosas acerca de las palabras, sino también a hacer cosas con las palabras: “La competencia comunicativa implica conocer no sólo el código lingüístico, sino también qué decir a quién, y cómo decirlo de manera apropiada en cualquier situación dada” (Saville-Troike, 1982). ¿Son posibles las competencias sin contenidos? En absoluto. Ahora bien, ¿es posible almacenar muchos contenidos sin ser capaz de desplegar competencia alguna? Por supuesto. Lo que resulta disparatado es oponer conocimientos a competencias.  
Cuando se afirma que el concepto de competencia implica saber movilizar un aprendizaje, pasar a la acción, lo que se está poniendo en tela de juicio es que la transmisión de información asegure en todo caso y por sí misma el conocimiento. Se defiende que un conocimiento indiscutiblemente emancipador, renuente a la automatización acrítica, es el que prende en el alumnado de forma dinámica, poniéndole en condiciones de hacer, en sentido prospectivo, todo aquello que permite formar parte de una comunidad culta y crítica: mantener una conversación en una lengua extranjera; calibrar la veracidad de una noticia y advertir sesgos y manipulaciones en la redacción de un titular, la selección de una imagen o la presentación de una gráfica; analizar el momento histórico presente a partir de la comparación con otras situaciones históricas estudiadas; argumentar si el automóvil eléctrico o la energía nuclear son opciones razonables en el actual momento de crisis ecológica; disfrutar de una obra de teatro, una visita a un museo, o un recital de piano; tomar la palabra en público para defender un derecho o argumentar una opinión.  
El problema no es, por tanto, adoptar un enfoque competencial en la elaboración de un currículo. Podría serlo, en todo caso, la selección de competencias con que se construyan sus cimientos y, por descontado, el para qué hacia el que se orienten: ¿para la emancipación o para la sumisión?, ¿para la asunción acrítica del mundo o para su transformación y mejora?  

17 diciembre 2022
[Incompetencias, José Martín](https://jos6martin.medium.com/incompetencias-1c053d7652d8)  


[Aprender a aprender, o por qué es una mala traducción y sus consecuencias para la Sociología de la Educación. Revista de Sociología de la Educación (RASE) 2023, vol. 16, n.º2. ojs.uv.es/rase ISSN: 2605-1923](https://ojs.uv.es/index.php/RASE/article/view/26675/22527)  


Competencias en Escocia (Skills, CfE = Curriculum for Excellence)   
[a curriculum for excellence. The Curriculum Review Group](https://education-uk.org/documents/pdfs/2004-scottish-curriculum-review)  
[curriculum for excellence - gov.scot](https://education.gov.scot/media/wpsnskgv/all-experiencesoutcomes18.pdf)  

31 mayo 2023
[All Learners in Scotland Matter - national discussion on education: final report - gov.scot](https://www.gov.scot/publications/learners-scotland-matter-national-discussion-education-final-report/)  

12 noviembre 2023
[What went wrong with the Scottish education system? - thetimes](https://www.thetimes.co.uk/article/what-went-wrong-with-the-scottish-education-system-7qq8hbzlm)  

13 noviembre 2023
[Skills vs knowledge, 13 years on. What can we learn from widespread dissatisfaction with the Curriculum for Excellence? Daisy Christodoulou](https://substack.nomoremarking.com/p/skills-vs-knowledge-13-years-on)  
> Back in 2010, Scotland introduced a new curriculum - the Curriculum for Excellence. The Curriculum for Excellence explicitly reduced the content knowledge in the curriculum, and organised the curriculum around a set of content-free skills statements like: “Using what I know about the features of different types of texts, I can find, select and sort information from a variety of sources and use this for different purposes.”  
> Yesterday, the Sunday Times had a long article on the curriculum’s performance so far. It does not make for great reading. They quote parents unhappy with low standards and teachers unhappy with vague and unhelpful documentation. Most remarkably, they quote one of the architects of the curriculum, Keir Bloomer, accepting that the curriculum may have gone too far in reducing its emphasis on knowledge.  
“The problem is we did not make sufficiently clear that skills are the accumulation of knowledge. Without knowledge there can be no skills.”  
The skills-knowledge debate is one of the perennial educational debates in many countries. If, after more than a decade, this flagship skills-based curriculum is not working out, then that is of relevance for educators globally and is worth exploring further.  



21 noviembre 2023  
[Escocia abandona la educación basada en competencias - xarxatic](https://xarxatic.com/escocia-abandona-la-educacion-basada-en-competencias/)  


## Competencias en Moodle (Aulas Virtuales EducaMadrid)  

En Moodle hay outcomes que a veces se relacionan con competencias pero son [resultados](https://docs.moodle.org/all/es/30/Resultados)  
> Nota del traductor: La palabra ingles Outcomes se tradujo al Español internacional como Resultados y en el Español de México inicialmente se tradujo como como Competencias pero en 2016 se cambió a Resultados porque Moodle introdujo competencies para otro fin específico.  

[Competencies](https://docs.moodle.org/400/en/Competencies)  

No están inicialmente en Aulas Virtuales EducaMadrid, en grupo Telegram se comenta esto que considero interesante y que no se limita solo a Moodle:  

> Las competencias son muy poco flexibles. Hay que tener muy claro qué hay que hacer antes de empezar a usarlas. Borrarlas es un infierno una vez que se empiezan a usar. Y en Madrid no tenemos completos los detalles sobre cómo evaluar por competencias.  
¿Sabe alguien en qué momento se considera superada una competencia?  
¿Tienen que estar todas superadas? ¿La mitad? ¿Dos tercios? ¿Todas? ¿Todas menos alguna a criterio del equipo docente? Una vez superada una competencia, ¿se ha adquirido para siempre? ¿Y si el alumno repite? ¿Qué se dice si se ha superado? ¿Apto/no apto? ¿Competente/no competente? ¿Hay relación entre calificación IN/SU/BI/NT/SB y las competencias? ¿Dónde se guarda el registro de competencias adquiridas? ¿En Raíces? Si no es ahí, ¿dónde y durante cuánto tiempo hay que guardarlas? ¿Se pueden certificar? ¿Al final de cada curso o solamente al final de cada etapa? Sólo por plantear algunas dudas de las muchas que hay.  

Así que hasta que no tengamos todos claro qué hay que hacer, cuándo, cómo, dónde, quién... Mejor usar algo que podamos moldear de aquí a final de curso.



