
# Prácticas / experimentos / laboratorio

Los recursos se pueden clasificar de varias maneras: intento poner aquí enlaces generales a prácticas/experimentos/laboratorio, aunque también puede haber enlaces a prácticas en recursos propios de un curso o bloque dentro de un curso, como pueden ser en los  [recursos de física de 2º de Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-fisica-2-bachillerato).  

**Por hacer**: esta página es muy voluminosa, revisar esta y otras páginas voluminosas para descargar separándolas en varias.  Por ejemplo en laboratorio la idea sería una página general de laboratorio, con documentos que agrupen prácticas de varios temas y luego remitir a página sobre cada tema que tenga prácticas solo de ese tema, hacer que cada tema tema tenga su apartado asociado a prácticas con marcador. Por ejemplo se podría ir si existe a nombrepagina#practicas


También hay cosas que se pueden considerar en cierto modo prácticas, como las  [physics masterclasses de física de partículas](/home/recursos/fisica/fisica-de-particulas/physics-masterclasses)  
También se pueden plantear como prácticas/experimentos algunos  [proyectos de investigación](/home/recursos/proyectos-de-investigacion)   
Se pueden ver también  [Reacciones químicas vistosas](/home/recursos/quimica/reacciones-quimicas-vistosas)  o vídeos asociados a alguna experiencia. Por ejemplo en  [Recursos ajuste reacciones químicas](/home/recursos/quimica/recursos-ajuste-reacciones-quimicas)  se puede plantear un experimento de quemar lana de hierro / quemar alcohol encima de una báscula.  
Hay página separada para poner información sobre  [materiales para aula de laboratorio](/home/recursos/material-para-aula-laboratorio)  y sitios donde comprarlo.

También puede estar asociado a apps para realizar experimentos [recursos apps](/home/recursos/recursos-apps)  

Ojo a quedarse solo en lo vistoso  
[twitter profesmadeinuk/status/1550518785173401604](https://twitter.com/profesmadeinuk/status/1550518785173401604)  
Abordar la enseñanza de las ciencias con "experimentos chulos" acaba generando un problema en el que, si bien los alumnos tienden a participar, lo que les resulta memorable es el experimento en sí, y no los conocimientos esenciales que deberían aprender.  
[Mitos sobre la memoria: «no me acuerdo de nada de la clase de química, sólo de aquel día que explotó todo» - investigaciondocente.com](https://investigaciondocente.com/2022/07/21/mitos-sobre-la-memoria-no-me-acuerdo-de-nada-de-la-clase-de-quimica-solo-de-aquel-dia-que-exploto-todo/)  

## Prácticas en general (reales o simulaciones), páginas web / canales youtube / vídeos sobre experimentos caseros 
También es posible que haya prácticas virtuales / simulaciones que estarían dentro de  [recursos de simulaciones](/home/recursos/simulaciones)  

Puede haber prácticas/experimentos asociados a ferias de divulgación científica, por ejemplo  [Experimentos - cienciaenaccion.org](https://cienciaenaccion.org/experimento/)  
También asociados a "shows" no necesariamente divulgativos (por ejemplo tipo "El hormiguero" que los alumnos conocen y citan mucho), o cosas como [The Wonders of Physics](http://sprott.physics.wisc.edu/wop.htm)  de [Julien Clinton Sprott](http://sprott.physics.wisc.edu/sprott.htm)  

[Canal Cluster de Divulgación - Sergio Paredes](https://www.youtube.com/@CLUSTERDV)  
En este canal se intenta explicar la ciencia, a partir de experimentos divertidos, curiosos o entretenidos. Algunos se pueden realizar en casa o en un aula, y otros será necesario un laboratorio y material especializado. Espero, como mínimo, entretener y crear curiosidad.
¡¡¡Gracias por entrar!!!  

[Cluster. Aprende ciencia con experimentos y animaciones 3D](http://cluster-divulgacioncientifica.blogspot.com/)  


Con más tiempo intentaré separar las "realizables fuera del laboratorio" vs "las que solamente se pueden hacer en laboratorio"  

  
[Experimentos caseros de física y química - fq-experimentos.blogspot.com.es](http://fq-experimentos.blogspot.com.es/)  
Manuel Díaz Escalera. "Desde hace algunos años trabajo en mis clases con experimentos de Física y Química realizados con materiales corrientes. Ante la necesidad de contar con una buena colección de experimentos de diversos temas decido crear, en octubre de 2007, un blog donde recopilar experimentos que yo mismo me encargo de realizar: fq-experimentos, que ya contiene 250 experimentos relacionados con materiales corrientes. Todos los vídeos de los experimentos son de elaboración propia y están alojados en mi canal en youtube. El canal es muy popular en México, España, Colombia, Perú, Argentina, Ecuador, Venezuela y otros países de Latinoamérica y tiene más de 35.000.000 de reproducciones. Entre otros reconocimientos y distinciones, fq-experimentos fue galardonado con el 3º Premio en la Modalidad B (Categoría V) del Premio Internacional Educared 2011 y ha sido incluido en la Red de Buenas Prácticas 2.0 del Ministerio de Educación de España y en el Banco Internacional de Objetos Educacionais del Ministerio de Educación de Brasil."  
  
[EXPERIMENTOS DE FÍSICA Y QUÍMICA EN TIEMPOS DE CRISIS](https://publicaciones.um.es/publicaciones/public/obras/ficha.seam?numero=2421&edicion=1)  
© Antonio Tomás Serrano y Rafael García Molina, 2015, Universidad de Murcia, ISBN: 978-84-16038-96-1, Depósito Legal MU- 896 -2015   
pdf 180 páginas  
1 Estimación del tiempo de reacción de una persona  
2 Determinación de la densidad de un cuerpo  
3 Determinación de la densidad de un cuerpo. Ampliación  
4 M.R.U. de una bola que rueda sobre un plano horizontal  
5 M.R.U.A. de una canica que baja rodando por un plano inclinado  
6 M.R.U.A. de una lata que rueda sobre un plano horizontal  
7 Construcción de un aerodeslizador  
8 Determinación del coeficiente de rozamiento entre dos superficies  
9 Factores que afectan al periodo de un péndulo  
10 Determinación de la aceleración de la gravedad mediante un péndulo simple  
11 Ley de Hooke  
12 Deformación de una goma elástica  
13 Determinación del centro de gravedad de una figura plana  
14 Medida de la densidad del aceite con un tubo en U  
15 Principio de Arquímedes  
16 Construcción de un densímetro 
17 Experiencia de Plateau  
18 Buzo cartesiano  
19 Una botella que aspira agua  
20 Implosión de una lata  
21 Trabajo realizado para subir por un plano inclinado  
22 Potencia de una persona  
23 Conservación de la energía en un péndulo simple  
24 Construcción de un calorímetro  
25 Determinación del calor específico del agua  
26 Determinación del calor específico de un metal  
27 Equivalente en agua de un calorímetro  
28 Papel que no se quema  
29 Espectroscopio de reflexión  
30 Espectros a la llama  
31 Cristalización en disolución sobresaturada  
32 10 + 10 = 19.2  
33 Preparación de disoluciones  
34 Reacción entre el aluminio y una disolución de Cu(II)  
35 Reacción del NaHCO3 con el CH3COOH 
36 Descomposición catalítica del peróxido de hidrógeno  
37 Estudio de algunos factores que influyen en la corrosión  
38 Indicador ácido-base casero  
39 Azul-transparente-azul-transparente-...  
40 Cañón de alcohol  

  
[Experiencias de Física al alcance de todos - lista youtube ua.es](https://www.youtube.com/playlist?list=PLoGFizEtm_6hVhzdWBZVW4O7TcPemL0c1)  
 
  
[Science projects & experiments for the young at heart! - experiland.com/](http://www.experiland.com/)  
 CAUTION: This website might be addictive | Fun Science projects for kids !  
 Great care has been put into our science projects to provide the young student of today with the basic knowledge of the scientific principles of our world. Try not to have fun - We dare you!  
 
[Unidad Docente de Física - UAH - *waybackmachine*](http://web.archive.org/web/20170607082012/http://www3.uah.es/fisymat/FISICA/demostraciones.html)  
 La Unidad Docente de Física cuenta con un conjunto de experimentos o demostraciones de Física útiles para mostrar en el aula o en actividades de divulgación diversos fenómenos físicos. En esta web se puede descargar la lista de demostraciones disponibles y las fichas correspondientes a cada experimento, donde se indica una breve descripción de su fundamento físico y de su modo de utilización.  
 Germán Ros  
La página no está operativa en 2021, pero cita referencias (actualizo enlaces):

Esta colección está inspirada en la realizada por Chantal Ferrer Roca y sus colaboradores de la Universidad de Valencia  [Demostraciones experimentales de Física para el aula - uv.es](https://fisicademos.blogs.uv.es/)  
Universidades extranjeras y en particular de EEUU tienen gran tradición de colecciones similares. Por ejemplo la que ofrece el [Yale physics lab, Lecture Demostrations](http://yppsweb2.its.yale.edu/physics/demos/demomain.asp).  

[FISICADEMOS. Demostraciones experimentales de Física - uv.es](https://www.uv.es/uvweb/fisica/es/demostraciones-experimentales-fisica-aula/catalogo-demos-1286053686292.html)  



GRUPO HEUREMA. EDUCACIÓN SECUNDARIA. ENSEÑANZA DE LA FÍSICA Y LA QUÍMICA.  Jaime Solá de los Santos   
[Refracción de la luz a través del vidrio: ángulo límite](http://heurema.com/PF44.htm)  
[sección: PRÁCTICAS DE FÍSICA, Refracción de la luz a través del vidrio: ángulo límite](http://web.archive.org/web/20200109091231/http://www.heurema.com/PDF82.htm)  (es de ley de Hooke pero incluye índice con todas las prácticas digitales, tienen url terminada en PDF de Práctics Digitales Física)  
[sección: PRÁCTICAS DIGITALES DE FÍSICA, Introducción a la fotografía digital en la experimentación física (estática y estroboscópica) - *waybackmachine*](http://web.archive.org/web/20130713105842/http://www.heurema.com/PDF0.htm)  

[Prácticas de Laboratorio, Teoría y Problemas de Física y Química](http://juliolarrodera.blogspot.com.es/)     
En este blog voy a ir poniendo prácticas de laboratorio de Física y Química que me parecen interesantes y que son fáciles de realizar en los institutos de Enseñanza Secundaria y vídeos de clases de Física y Química de teoría y problemas.
Julio Larrodera Sanchez. Colaboran: Laura Crespo Felipe, José Bordetas Lasheras, Ana Belén Morales Yuste  
Licenciamiento no detallado.  
[Guiones prácticas laboratorio (En PDF)](https://juliolarrodera.blogspot.com/p/guiones-practicas-laboratorio-en-pdf.html)  

Revista digital innovación y experiencias educativas, Nº 31 – JUNIO DE 2010  
[PRÁCTICAS DE QUÍMICA PARA 1º DE BACHILLERATO, SALVADOR SAMUEL MOLINA BURGOS](https://archivos.csif.es/archivos/andalucia/ensenanza/revistas/csicsif/revista/pdf/Numero_31/SALVADOR_S_MOLINA_1.pdf)  
[PRÁCTICAS DE FÍSICA Y QUÍMICA PARA 4º DE E.S.O., SALVADOR SAMUEL MOLINA BURGOS](https://archivos.csif.es/archivos/andalucia/ensenanza/revistas/csicsif/revista/pdf/Numero_31/SALVADOR_S_MOLINA_2.pdf)  
 
[PhyStorming *waybackmachine*](http://web.archive.org/web/20170616065637/http://matrix.fis.ucm.es/phystorm/experimentos-en-casa)  
Aquí encontrarás experimentos de Física General para realizar en clase y experimentos sencillos para realizar en casa y comprobrar los conceptos teóricos explicados en clase.  
 
[LABORATORIO DE QUÍMICA. Portal de Ciencias Experimentales. - ucm.es](http://webs.ucm.es/info/diciex/programas/quimica/index.html)  
Juan Gabriel Morcillo Ortega. UCM. Departamento dicáctica ciencias experimentales  
 
[Experimenta_wiki, el wiki de la Feria Madrid es Ciencia. *waybackmachine*](http://web.archive.org/web/20180609194026/http://www.madrimasd.org/experimentawiki/feria/Portada)  
Una herramienta colaborativa para generar y ofrecer documentación libre sobre experimentos científicos.  

[100 experimentos sencillos de Física y Química - juntadeandalucia.es](https://www.juntadeandalucia.es/averroes/centros-tic/23200041/helvia/sitio/upload/LIBRO_Experimentos_sencillos_de_fisica_y_quimica.pdf)  Libro 138 páginas
 
Manual de física. Prácticas de "laboratorio didáctico móvil" 
Decenas de guiones de prácticas detallados en documento 137 páginas. Colegio Nacional de Educación Profesional Técnica. Licenciamiento no detallado.  
Parece ser mismo documento que en [este enlace de academia.edu](https://www.academia.edu/21898949/MANUAL_DE_PR%C3%81CTICAS_LABOR%C3%81TORIO_DIDACTICO_M%C3%93VIL_EQUIPAMIENTO_CIENT%C3%8DFICO_Y_TECNOL%C3%93GICO_PARA_ESCUELAS_F%C3%8DSICA)  donde se cita una web de dedutel Desarrollo en Educación Tecnológica, ya no operativa.  
  
[La web de Física - Prácticas de laboratorio](http://www.lawebdefisica.com/contenidos/experim.php)  
Licenciamiento: se indica en la web "© 2003—2031, La web de Física" al mismo tiempo que se aclara licenciamiento "aproximadamente cc-by-sa" en  [Sobre la web de La web de Física](http://www.lawebdefisica.com/about/web.php)  del que se cita un extracto "Todo el material que contiene esta web (excepto la mascota y aquellos documentos donde se especifique explícitamente lo contrario) se distribuye como material de dominio público y libre. Por lo tanto, se permite su copia y redistribución siempre y cuando la redistribución se haga bajo ésta misma licencia. Así mismo, se permie distribución de copias modificadas siempre o completadas, siempre y cuando se cambie el título, que la totalidad de la obra en la cual se han incluido respete las mismas libertades que se garantizan de orígen, e incluya una nota explicativa donde se reconozca el origen y la autoria del material. La solo acceso al material contenido en este website comporta la aceptación de las condiciones especificadas en éste parrafo."  

[Prácticas de física (1º y 2º Bachillerato) - fisicayquimicaenflash.es ](http://fisicayquimicaenflash.es/fisicapractica.htm)  
[Prácticas de Física en PDF - fisicayquimicaenflash.es](http://fisicayquimicaenflash.es/practicasfispdf.htm)    
[Prácticas de química - fisicayquimicaenflash.es](http://fisicayquimicaenflash.es/quimicapractica.htm)  
[Prácticas de Química en PDF - fisicayquimicaenflash.es](http://fisicayquimicaenflash.es/practicaspdf.htm)    
[Prácticas propuestas por la coordinación de la asignatura (física, PAU)](http://fisicayquimicaenflash.es/practpau.htm)  
Ramón Flores Martínez. Prof. de Física y Química IES Alfonso II (Oviendo). Licenciamiento cc-by-nc-nd.  
 
[Experiencias de física. Demostraciones y prácticas de laboratorio - ua.es](http://www.dfists.ua.es/experiencias_de_fisica/index1.html)  
Incluyen guiones y vídeos. Universidad de Alicante. Licenciamiento no detallado. Proyectos con varios autores, dirigidos por Augusto Beléndez Vázquez, subvencionados por el Vicerrectorado de Convergencia Europea y Calidad y por el Vicerrectorado de Tecnología e Innovación Educativa.  


[El tianguis de física *waybackmachine*](http://web.archive.org/web/20070823190214/http://www.tianguisdefisica.com/mapa.htm)  
El Tianguis de Física contiene una colección de experimentos de diversos temas de física de interés para niños y jóvenes de nivel escolar medio. Hay instrucciones para su realización por estudiantes y maestros. Centro de Ciencias Aplicadas y Desarrollo Tecnológico, UNAM, México. Licenciamiento no detallado.  

[DEPARTAMENTO DE QUÍMICA. ÁREA DE FÍSICA APLICADA. Asignaturas con enlace a prácticas laboratorio. - unirioja.es](http://www.unirioja.es/dptos/dq/fa/Asignaturas.htm)  

[Cómo obtener electricidad con una lata de refresco y una moneda - microsiervos.com](http://www.microsiervos.com/archivo/internet/como-obtener-electricidad-lata-refresco-moneda.html)  

[Física General, (C) Ignacio Martín Bragado. imartin@ele.uva.es, 12 febrero 2003 - ucm.es](http://fisicas.ucm.es/data/cont/media/www/pag-39686/fisica-general-libro-completo.pdf)  
Libro 199 páginas  
Incluye Bloque III Prácticas de laboratorio, y algunos ejemplos  
Cambios de fase y descenso ebulloscópico  
Carga y descarga de un condensador.  
Principio de Arquímedes: Determinación de la densidad > 4º ESO  
Experiencia de Faraday. Inducción.  
Iniciación a la hidráulica: Diversas experiencias. > 4º ESO  
Comprobación de la ley de Ohm.  
Experiencia de Oersted.  
Uso elemental de un osciloscopio  
Estudio de un péndulo.  

[ExpCaseros - Canal Youtube](https://www.youtube.com/user/ExpCaseros)  
[ExpCaserosKids - Canal Youtube](https://www.youtube.com/user/ExpCaserosKids)  

[Laboratorio - escire.com *waybackmachine*](http://web.archive.org/web/20160405102755/http://www.escire.com/fisica-y-quimica/laboratorio/)  

[Operaciones básicas del laboratorio de Química, Universidad de Barcelona](http://www.ub.edu/oblq/oblq%20castellano/index.html)  
  
[The High School Laboratory, Teacher Recommended and Developed and/or Requested Demonstrations, Labs and Laboratory Resources *waybackmachine*](http://web.archive.org/web/20160618013634/http://www.hschem.org/Laboratory/labs.htm)  

[Experimentos - ciencianet.com](http://ciencianet.com/experimentos.html)  
Copyright 2000 Antonio Varela Experiencias de Laboratorio  

[Experiencias de laboratorio - 100ciaquimica.net](http://www.100ciaquimica.net/exper/)  
He seleccionado una serie de experiencias de laboratorio relacionadas con las asignaturas de Física y Química a nivel de Secundaria y Bachillerato.Clasificadas en 7 grupos: Experiencias caseras de física / química, secundaria física / química, 1º bachillerato física / química, y 2º Bachillerato química  

[Physics@Home - physics central](http://www.physicscentral.com/experiment/physicsathome/index.cfm)  

[Home Science - Canal de vídeo](https://www.youtube.com/user/maricv84)  
(incluye experimentos)  

[Laboratorio de Química](http://laboratorio-quimico.blogspot.com.es/)  
Blog dedicado a la ciencia de la química, laboratorio de química, implementos de química, vídeos, conceptos y más   

[Experimentos - labvirtual.iciq.es](http://labvirtual.iciq.es/es/experimentos_es/)  
Ponemos a disposición del profesorado, experimentos que muestran diferentes tipos de reacciones químicas que se incluyen en el currículum académico. Facilitamos recetas de experimentos ilustrativos de cada tipo de reacción química para poder realizar en clase. Debido a que algunos experimentos incluyen productos químicos con los que hay que tener cuidado, solicitamos antes un registro de la persona que quiera acceder a la información.  

[Laboratorio químico, Procedimientos Básicos de Laboratorio](https://www.tplaboratorioquimico.com/laboratorio-quimico/procedimientos-basicos-de-laboratorio.html)  

[homeschool science for kids - science projects](https://homeschool.scienceprojectideasforkids.com/category/science-projects/)  Janice VanCleave  

[twitter PhysicsMatters/status/1218673358440628224](https://twitter.com/PhysicsMatters/status/1218673358440628224)  
¿Se puede medir la velocidad de la luz con un aparato de microondas y queso?  
Sí,lo hice con 1Bach y observamos los puntos nodales donde se fundía el queso,lo cual representaba media longitud de onda ...cada vez me cuesta más impresionar a los alumnos con rarezas de estas  
[![](https://pbs.twimg.com/media/EOmY7bxUcAAHiZX?format=jpg&name=small "") ](https://pbs.twimg.com/media/EOmY7bxUcAAHiZX?format=jpg)  [![](https://pbs.twimg.com/media/EOmY8h3W4AEgP-4?format=jpg "") ](https://pbs.twimg.com/media/EOmY8h3W4AEgP-4?format=jpg)  
La distancia de A a B son unos 7cm aproximadamente(media longitud de onda) y la frecuencia del Microondas es de2450MHz según fabricante. Dado v=f x <\.  

[Canal youtube @ElectricExperimentsRobert33](https://www.youtube.com/@ElectricExperimentsRobert33)  
In this channel you will find a variety of electrical, magnetism, electrical DIY experiments.   

[Canal youtube @ Chemteacherphil](https://www.youtube.com/@Chemteacherphil)  
So many people have had a negative experience learning chemistry, and I hope to provide a way for people to have a positive and engaging experience learning science through the videos I create. As a high school chemistry teacher, I try to make videos that reveal how chemistry applies to real life. This ends up taking many forms; sometimes explaining the chemistry behind how things work, or a historical perspective, always through the lens of science.  

[Reacciones de precipitación más fáciles: química a microescala - Science in School Issue 57  scienceinschool.org](https://www.scienceinschool.org/es/article/2022/precipitation-microscale-way/)  
> Los métodos tradicionales usan tubos de ensayo y requieren bastantes reactivos. Además se genera mucho desperdicio que va por el fregadero o se guarda en botellas para residuos. En un artículo anterior sobre indicadores pH se mostró cómo realizar reacciones sobre una hoja plastificada. Las instrucciones están impresas y plastificadas y sobre ellas se trabaja, así se evitan distracciones. Estas actividades se pueden realizar con alumnos de 11 a 18 años.  
![](https://www.scienceinschool.org/wp-content/uploads/2022/03/Figure-4-950x293.jpg)  

## Recursos asociados a identificar material laboratorio
Contenido básico, suele ser un práctica inicial cuando se va la primera vez  
[Crucigrama material laboratorio](http://dazek.jimdo.com/app/download/8544622071/Crucigrama+Mat+Lab.pdf)  

Suele ser una actividad presencial inicial, pero también hay actividades online asociadas  
[Instrumentos de laboratorio - genial.ly](https://view.genial.ly/5f4e020ae8b4e60d6ab32fc7/presentation-dobble-instrumentos-de-laboratorio)  
Dobble (instrumentos de laboratorio)  
Pablo Ortega Rodríguez  
Hay que localizar el elemento que está repetido en ambos conjuntos para poder pasar a la siguiente pantalla.

## Recursos asociados a imágenes de material y montajes el laboratorio
 [Chemix](https://chemix.org/) 
 
 [Recursos digitales > Laboratorio - gobiernodecanarias.org](https://www3.gobiernodecanarias.org/medusa/ecoescuela/recursosdigitales/tag/laboratorio/)  

## Recursos asociados a seguridad en el laboratorio
Carteles, identificación símbolos ...  
![](http://dazek.jimdo.com/app/download/8372297671/Seguridad+en+el+laboratorio.jpg "Seguridad en el laboratorio de Química. dazek.jimdo.com") 

## Libros / materiales de referencia sobre prácticas
Algunos los clasifico como documentados por IES, editoriales, ... y aquí lo demás  

* [Prácticas de Química para Educación Secundaria](http://dpto.educacion.navarra.es/publicaciones/pdf/qui_dg.pdf)    
J.A. Garde Mateo; F.J. Uriz Baztán  
Gobierno de Navarra. Departamento de Educación y Cultura  
Depósito Legal: NA-1314-1997 I.S.B.N.: 84-235-1605-9  
127 páginas
* TERMOLOGÍA. ENOSA (EMPRESA NACIONAL DE ÓPTICA, S.A.)  
Depósito legal: M-44.347-1988, ISBN 84-86916-17-8  
Índice de experiencias: 
TERMOMETRÍA  
Comprobación de un termómetro (1.2.)  
Bimetal (1.3.)  
CALORIMETRÍA  
Capacidad calorífica (2.2.)  
Calorímetro: su equivalente en agua (2.3.)  
Curvas de calefacción de líquidos. Comprobación de la ecuación calorimétrica ...  
* QUÍMICA. ENOSA (EMPRESA NACIONAL DE ÓPTICA, S.A.)  
Índice de experiencias:  
TÉCNICAS GENERALES DE LABORATORIO  
Trabajo del vidrio (I) (1.1.)   
...   
ÁCIDOS, BASES Y SALESAcción sobre los indicadores (2.1.)  
...   
OXÍGENO y ÓXIDOSObtención del oxígeno (3.1.)  
...  
HIDRÓGENO  
Obtención del hidrógeno (4.1.)  
...  
AGUA  
Temperatura de solidificación del agua (5.1.)  
...  
NITRÓGENO Y SUS COMPUESTOS  
Obtención del nitrógeno (6.1.)  
...  
AIRE  
El aire contiene oxígeno (7.1.)  
...  
CLORO Y SUS COMPUESTOS  
Obtención del cloro (8.1.)   
...  

* [El uso de las prácticas de laboratorio en Física y Química en dos contextos educativos diferentes: Alemania y España.](http://revistas.um.es/educatio/article/viewFile/723/753)  
Página 17  "H) Buenas estrategias"  
Página 18 "I) Prácticas que funcionan"  
En relación con las prácticas de Física, seleccionaron las siguientes:  
1) Estudio del principio de Arquímedes (PRDE01 y PRDE09)  
2) Medidas de capacidad térmica (PRDE04)  
3) Estudio de la temperatura de cambios de estado, p.e. fusión del hielo y ebullición del agua (PRDE04 y PRES01)  
4) Fabricación de smog por inversión de la capa térmica (PRDE05)  
5) Estudio de la conductividad eléctrica (PRDE06)  
6) Presión atmosférica, con postal bajo un vaso (PRDE09)  
7) Experimentos con émbolos (PRDE09)  
8) Medición de propiedades de la materia (l, m, v, d) con instrumentos o mediante la aplicación de fórmulas (PRES04, PRES09)  
9) Investigación sobre el tamaño de una gota de agua (PRES07)  
10) Demostración de que el aire es materia (PRES07)  
11) Estudio de las oscilaciones de un péndulo (PRES09)  
En relación con las prácticas de Química, eligieron las siguientes:  
1) Destilación seca de la madera (PRDE03)  
2) Identificación de ácidos y bases desconocidos (PRDE05, PRES09)  
3) Estudio de la conductividad eléctrica en función del tipo de enlace (PRDE06)  
4) Carbonización de sustancias del entorno (azúcar, carne, etc.), por calentamiento, para saber que llevan carbono (PRDE07)  
5) Preparación de disoluciones (PRES01 y PRES06)  
6) Separación de sistemas materiales, como cromatografía en tiza, separar arena y limaduras de hierro,... (PRES02, PRES05, PRES09)  
7) Reconocimiento y caracterización de elementos químicos (PRES03)  
8) Reacciones químicas con cambios de color, con humo... que se puedan observar con la vista (PRES03, PRES04, PRES06)  

* [Diseño, adaptación y evaluación de los trabajos prácticos de enfoque constructivista en el laboratorio de Física y Química, para el sistema educativo L.O.G.S.E.](http://redined.mecd.gob.es/xmlui/bitstream/handle/11162/83553/089900037.pdf?sequence=2)  
Memoria final correspondiente a trabajo de investigación educativa subvencionado por la Secretaría de Estado de Educación y Formación Profesional-Centro de Investigación y Documentación Educativa, del M.E.C. (Convocatoria de 6 de Marzo de 1996, B.O.E. nº 67 de 18 de Marzo). 193 páginas   
* [Recursos asociados al libro "Experimentos de Física usando Tic´s" de S. Gil](http://www.fisicarecreativa.com/exp_fisica/exp_fisica_recur.htm)  
En esta página se pone a disposición de los lectores del presente texto material complementario, programas, datos etc. que esperamos pueda servir para el análisis de los proyectos propuestos en el libro.  

* [Física, guía del laboratorio; Ed. Reverté, ISBN: 84-291-4131-6](https://books.google.es/books?id=cOmpZAPc56sC)  

* [Molina Jordá, J.M. y Santos Benito, J.V. MANUAL DE PRÀCTIQUES DE FÍSICA APLICADA](http://rua.ua.es/dspace/handle/10045/22637)  
ISBN: 978-84-9717-289-9 Depósito legal: A 13-2014  
Descarga libro gratuita  

* [QUÍMICA al laboratori Pràctiques a la carta](http://www.quimics.cat/wp-content/uploads/2016/11/quimica-al-laboratori.pdf)  
pràctiques de Química per alumnes de secundària  
D.L. B 23893-2016ISBN: 978-84-945762-6-3  

## Prácticas / talleres asociadas a primaria 
 [Expermientos online - curiosikid.com (Flash)](http://www.curiosikid.com/view/index.asp?pageMS=23040)  
 Museo de los Niños de Caracas © 2002-2013 | Todos los derechos reservados. | RIF: J-00129109-1  
 
 [Experimentos en educación primaria e infantil](http://primariaexperimentos.blogspot.com.es/)  
 
 [Talleres para enseñar Química en Primaria](https://www.ucm.es/data/cont/docs/153-2015-11-13-LIBRO%20Talleres%20para%20ense%C3%B1ar%20Qu%C3%ADmica%20en%20Primaria57.pdf)  
  Mª Ángeles Arillo Aranda, Rosa Martín del Pozo, Patricia Martín Puig  
 Universidad Complutense de Madrid, Madrid, 2015,  ISBN: 978-84-606-9021-4,  Depósito legal: M-19623-2015  
242 páginas  
III.CUADERNOS DE LOS TALLERES  
Taller de separación de mezclas  
Taller de fabricación de cristales  
Taller de experimentos con vinagre y bicarbonato  
Taller de experimentos con limones  
Taller de experimentos con velas  
Taller de ácidos  
Taller de reacciones químicas espectaculares  
Taller de oxidaciónTaller de combustión  
Taller de fermentación  

A veces enlazan con experimentos "caseros", en "cocina", con alimentos...  
[7 experimentos caseros con alimentos - gominolasdepetroleo.com](http://www.gominolasdepetroleo.com/2015/01/7-experimentos-caseros-con-alimentos.html) 

## Prácticas de laboratorio documentadas por editoriales
Suelen ser material complementario asociado a los libros de texto de profesor  

* Física y Química. Prácticas de laboratorio. 3 ESO SM, Proyecto Conecta 2.0  
Prácticas 1 a 12  
1. Medida de volúmenes en líquidos  
2. Cálculo de la densidad de un sólido y un líquido  
3. Preparación de una disolución  
4. Los colores y la corteza de los átomos. Ensayos a la llama  
5. Identíficación del tipo de enlace de una sustancia  
6. Los cambios químicos y sus manifestaciones  
7. Determinación del pH de los alimentos  
8. Fabricación de jabón  
9. Fabricación de un electroscopio  
10. Construcción de una bombilla casera  
11. Algunas características del campo magnético  
12. Experiencias con distintos generadores eléctricos  
Incluye apartado de "experiencias sorprendentes"  

* Newton, 4º ESO, Prácticas de Laboratorio  
"Una sencilla práctica de laboratorio para cada unidad: Guía para el profesor, Hoja para el alumno (fotocopiable)"  
Prácticas 1 a 15  
1. Estudio de un mru. La combusión  
2. Estudio de un mrua. La tirolina  
3. La fuerza centrípeta como causa de los movimientos circulares  
4. Aplicación de las fuerzas a cuerpos elásticos. La ley de Hooke  
5. Fabricación de un densímetro  
6. Movimientos de la Tierra y la Luna  
7. Trabajo en máquinas simples. El torno  
8. Cálculo del calor latente de fusión del agua  
9. Leyes de la reflexión de la luz  
10. Determinación del tipo de enlace químico  
11. Obtención experimental de coeficientes estequiométricos  
12. Energía desprendida mediante calor en una reacción exotérmica  
13. Pilas electroquímicas  
14. Obtención de jabón  
15. Reconocimiento de glúcidos, lípidos y proteínas  

* PLQ Prácticas de laboratorio, Bloque I: Química. 1º Bachillerato. Edebé  
Material promocional, incluye dos prácticas 3 y 4  
3. Determinación de la masa molar de un líquido volátil.  
4. Determinación del porcentaje de mineral en un huevo

## Prácticas de laboratorio documentadas por fabricantes de material
Muchas veces los propios fabricantes documentan prácticas en las que se usa su material, intentando convencer de comprarles el material necesario para realizar esa práctica  
Las prácticas suelen llevar un listado de material con cada elemento indicando la referencia del fabricante  
[Caída libre: medimos la aceleración de la gravedad - ibdciencia.com](http://www.ibdciencia.com/blog/practicas-de-fisica/caida-libre-medida-de-la-aceleracion-de-la-gravedad) 

### PHYWE
* 01131.24 Experimentos para laboratorios de física  
MECÁNICA  
Trabajos de laboratorio  
Mod. S.C.D. 89  
PHYWE ESPAÑA, S.A.  
Modelo aprobado por el Ministerio de Educación.  
(Abril de 1989)  
Índice de experiencias:  
M 1 Medida de longitudes  
M 1.1. Medida de longitudes con un calibrador  
M 3 Movimientos rectilíneos  
M 3.1.1. Velocidad. Movimiento uniforme.  
M 3.3.1. Movimiento acelerado.  
M 3.4.1. Caída libre  
M 4 Movimiento circular  
M 4.0.1. Movimiento circular  
M 5 Composición de movimientos  
MR 5.2. Composición de movimientos perpendiculares.  
MR 5.3. Composición de dos movimientos armónicos: Curvas de Lissajouss.  
M 6 Movimiento armónico simple  
M 6.1.1. El péndulo simple  
M 6.3.1. Péndulo de resorte helicoidal  
M 6.6.1. Cinemática y dinámica del movimiento armónico simple  
M 7 Composición de fuerzas  
MR 7.1. Fuerzas en la misma dirección  
MR 7.2. Paralelogramo de fuerzas  
MR 7.3. Descomposición de una fuerza en dos componentes perpendiculares  
MR 7.4. Fuerzas en el plano inclinadoM 8 Momento de una fuerza  
M 8.1. Momento de una fuerza  
M 8.2. Condiciones de equilibrio para cuerpos que giran  
M 9 Principio de la dinámica  
M 9.1. Constante de un resorte. El dinamómetro  
M 9.2. Distinción entre masa inercial y peso  
M 9.3.1. Principio fundamental de la Dinámica  
MR 9.4. Acción y reacciónM 10 Rozamiento  
MR 10.1. Medición de coeficientes de rozadura y deslizamiento  
M 19.2. Pérdida de energía mecánica por rozamiento  
M 11 Cantidad de movimiento  
M 11.1.1. Conservación de la cantidad de movimiento  
MR 11.2. Choque elástico y choque inelástico  
M 11.2.0. Choque de dos móviles  
M 12 Dinámica del movimiento armónico  
M 12.1.1. Péndulo compuesto  
M 12.3.1. Medida de "g" con un péndulo compuesto  
M 13 Péndulo de torsión  
M 13.1.1. Oscilaciones de torsión  
M 14 Dinámica de la rotación  
M 14.0.1. Fuerza centrípeta y centrífuga  
M 15 Momento de inercia ...  
M 16 Momento cinético y trabajo ...  
M 17 Ondas transversales y longitudinales  
MR 17.1. Propagación de ondas en cuerdas y muelles  
M 18. Ondas estacionarias  
M 18.5. Tensión y frecuencia en cuerdas vibrantes  
M 19. Velocidad del sonido  
M 19.1. Determinación de la velocidad del sonido en el aire  
M 20. Frecuencia de un sonido  
M 20.1. Determinación de la frecuencia de un sonido  
M 21. Presión hidrostática  
MR 21.1. Medida de la presión. Manómetro.  
MR 21.2. Determinación de la presión hidrostática en los líquidos.  
M 22. Principio de Arquímedes.  
MR 22.1.1. Fuerzas de empuje. Principio de Arquímedes  
MR 22.2. Densidad de sólidos.  
MR 22.3. Densidad de líquidos. Aerómetros.  
M 24. Presión atmosférica  
MR 24.1. Magnitud de la presión atmosférica.  
M 25. Leyes de los gases. ...  
M 26 Máquinas simples. Trabajo  

* 01131.54 Experimentos para laboratorios de física. MECÁNICA. Trabajos de laboratorio. Guía del Profesor. PHYWE ESPAÑA, S.A., Orto Sánchez, Agosto 1987 

[Experimentos científicos para que los niños hagan en casa - 3m.com.es](https://www.3m.com.es/3M/es_ES/fundacion3m/3m-divulgacion-cientifica/ciencia-en-casa/#experiments)


## Prácticas de laboratorio documentadas por consejerías / departamentos IES / profesorado 

**Pendiente separar y organizar intentando asociar a curso / situaciones de aprendizaje**  
  
[Dpto Física y Química IES Valle del Saja - canal youtube](https://www.youtube.com/user/fqsaja)  

[CUADERNO DE PRÁCTICAS FÍSICA y QUÍMICA 4º ESO Prof. Jorge Rojo Carrascosa](http://profesorjrc.es/apuntes/practicas/practicas%204eso%20fyq.pdf)  

[twitter fqsaja1/status/1352933879217348611](https://twitter.com/fqsaja1/status/1352933879217348611)  
Que no paren las prácticas virtuales!!! Hoy le toca el turno a 4º ESO y esta determinación de coeficientes de rozamiento  Es la primera vez que la hacemos. Qué os parece? En el hilo el vídeo completo, póster y guion.  
[Determinación del coeficiente dinámico de rozamiento.pdf](https://drive.google.com/file/d/1NrmIRaarKCZvh8VdIhzvRDOAkJSPEwfK/view)  

[QUÍMICA DIVERTIDA (pdf 58 páginas)](https://pdfcoffee.com/experimentos-de-quimica-9-pdf-free.html)  
NDICE VIDA COTIDIANA.  
PRÁCTICA 0. ¿POR QUÉ PONEMOS BOLITAS ANTIPOLILLA EN EL ARMARIO?  
PRÁCTICA 1. CREMA DE MANOS.  
PRÁCTICA 2. RECICLANDO ACEITE USADO. ÁCIDO BASE  
PRÁCTICA 3. BURBUJAS EXPLOSIVAS.  
PRÁCTICA 4. FABRICACIÓN CASERA DE UN INDICADOR.  
PRÁCTICA 5. UN INDICADOR: LA FENOLFTALEÍNA.  
PRÁCTICA 6. TEST DE RESPIRACIÓN.  
PRÁCTICA 7. CÓMO GENERAR LLUVIA ÁCIDA.  
PRÁCTICA 8. CÓMO FABRICAR UN EXTINTOR.  
PRÁCTICA 9. BOLAS SALTARINAS.  
PRÁCTICA 10. LANZACOHETES.  
PRÁCTICA 11. AGUA CARBONATADA.  
PRÁCTICA 12. VOLCÁN EN ERUPCIÓN.  
PRÁCTICA 13. COMPRIMIDO EFERVESCENTE. COLISIÓN MOLECULAR.  
PRÁCTICA 14. ¿QUÉ LE OCURRE A UN HUEVO SI LO METO EN VINAGRE?  
PRACTICA 15. CÓMO SABER SI UN HUEVO ESTÁ EN MAL ESTADO.  
PRÁCTICA 16. HACER DESAPARECER PLÁSTICO. PROPIEDADES DE ALGUNOS ÁCIDOS  
PRÁCTICA 17. DESHIDRATACIÓN DEL AZÚCAR. REACCIONES DE PRECIPITACIÓN  
PRÁCTICA 18. ROJO, BLANCO, AZUL.  
PRÁCTICA 19. APARECE/DESAPARECE.  
PRÁCTICA 20. LLUVIA DE ORO. EQUILIBRIO QUÍMICO PRÁCTICA  
21. HIGRÓMETRO. REACCIONES DE OXIDACIÓN Y REDUCCIÓN  
PRÁCTICA 22. ¿POR QUÉ SE OXIDA UNA MANZANA?  
PRÁCTICA 23. CLAVO DE COLOR COBRE.  
PRÁCTICA 24. NI LO UNO, NI LO OTRO, PERO LOS DOS A LA VEZ SÍ.  
PRÁCTICA 25. SUPOSITORIO INFLAMABLE.  
PRÁCTICA 26. QUIMIOLUMINISCENCIA.  
PRÁCTICA 27. FUEGO VERDE.  
PRÁCTICA 28. BOTELLA AZUL.  
PRÁCTICA 29. RELOJ DE YODO.  
PRÁCTICA 30. VOLCÁN DE DICROMATO DE AMONIO.  
PRÁCTICA 31. EL TIOCIANATO.  
PRÁCTICA 32. DIBUJAR CON FUEGO.  
PRÁCTICA 33. MONEDAS VERDES.  
PRÁCTICA 34. VINO EN AGUA.  
PRÁCTICA 35. ESPEJO DE PLATA.  
PRÁCTICA 36. EL LIMÓN ELÉCTRICO.  
PRÁCTICA 37. OXIDACIÓN, PERO CON CATALIZADOR.  
PRÁCTICA 38. EL SUEÑO DEL ALQUIMISTA.  
PRÁCTICA 39. CALIENTE DE UN COLOR, FRÍO DE OTRA.

[Química insólita](https://quimins.wordpress.com/indice/)  
Esta es la web del Curso de Formación Permanente de la UNED Química Insólita, que se inició el 2 de diciembre de 2024 y terminará en 18 de mayo de 2025, estando destinado a profesore/as de Enseñanza Secundaria y público en general.  
Aquí solo se muestran las recetas de los experimentos; el curso universitario se dedica a la explicación de los fenómenos físico-químicos subyacentes. El objetivo es claro: hacer más atractivas las clases de Física y Química a la/os alumna/os y facilitarles la comprensión de los  conceptos teóricos.  

[Química insólita  @quimicainsolita5607 - youtube](https://www.youtube.com/channel/UCyQ9T1YrJXD5bgFooZoghbw)  


[![Ley de Proust](https://img.youtube.com/vi/fPAMzYfUUds/0.jpg)](https://www.youtube.com/watch?v=fPAMzYfUUds "Ley de Proust")  
Práctica Virtual para realizar durante la crisis COVID. En este caso es la demostración de la ley de Proust para 1º de Bachillerato.  
 [Ley de Proust (guion práctica virtual).pdf](https://drive.google.com/file/d/1amvrMuqu1OUXAC5njaDNjv5WIOq4Vd1v/view)  
 
 [Laboratorio - fisquiweb.es](https://fisquiweb.es/laboratorio.htm)  
 Se recogen algunas de las experiencias y trabajos realizados en el laboratorio.  
 Como complemento se facilitan apuntes sobre la teoría implicada.  
 En algunos casos para procesar los datos obtenidos se usan hojas de cálculo.  
 Niveles: 3º ESO, 4º ESO, 1º Bach, 2º Física, 2º Química  
 También laboratorios virtuales  
 Luis Ignacio García González, IES Juan A. Suanzes; IES La Magdalena, Avilés.  
  
 [Laboratorio 2º y 3º ESO - fisquiweb.es](https://fisquiweb.es/laboratorio3.htm)  
 Cómo trabajan los científicos.  
 Medidas y errores.  
 Cambios de estado.  
 Temperatura de ebullición  
 Mezclas heterogéneas.  
 Mezclas homogéneas.  
 Construyendo compuestos.  
 Reacciones químicas  
 
[Laboratorio 4º ESO - fisquiweb.es](https://fisquiweb.es/laboratorio4.htm)  
 Cómo trabajan los científicos.  
 Determinación de la aceleración de caída de los cuerpos.  
 Movimiento circular uniforme.  
 Determinación experimental del coeficiente de rozamiento.  
 Principio fundamental de la Hidrostática.  
 Principio de Arquímedes.  
 Equilibrio de cuerpos flotantes.  
 Estudio de la palanca.  
 Obtención y reconocimiento del CO2.  
 Rendimiento de una reacción química.  
 
[Laboratorio 1º Bachillerato - fisquiweb.es](https://fisquiweb.es/laboratorio1B.htm)  
 Tiro oblicuo  
 Descenso por un plano inclinado.  
 Fuerza normal.  
 Fuerzas de rozamiento.  
 Determinación del coeficiente de rozamiento cinético.  
 Determinación del calor específico de un metal.  
 Determinación de la fórmula del óxido de magnesio.  
 Obtención y reconocimiento del CO2  
 Rendimiento de una reacción química  
 
[Laboratorio  2º Bachillerato Física - fisquiweb.es](https://fisquiweb.es/laboratorioF2B.htm)  
 Péndulo simple. Amplitud y periodo.  
 Estudio de un muelle real.  
 Determinación de la velocidad del sonido en el aire.  
 Ondas estacionarias en una cuerda.  
 Determinación del valor de "g" con un péndulo simple.  
 ¿Qué es un muón?  
 Experiencia de Oersted  
 Campo magnético. Fuerzas sobre conductores.  
 Electromagnetismo. Motor eléctrico.  
 Índice de refracción de un vidrio  
 Cálculo de la distancia focal de una lente convergente  
 Espectroscopía  
 
[Laboratorio 2º Bachillerato Química - fisquiweb.es](https://fisquiweb.es/laboratorioQ2B.htm)  
 Sustancias iónicas y moleculares. Estudio de la conductividad.  
 Sustancias iónicas y moleculares. Estudio de la solubilidad.  
 Termoquímica. Entalpía de neutralización ácido-base.  
 Equilibrio químico (I). Principio de Le Chatelier.  
 Equilibrio químico (II). Principio de Le Chatelier.  
 Reacciones de precipitación.  
 Determinación del contenido de ácido acético en un vinagre comercial.  
 Valoración redox. Permanganimetría  
 Pila Daniell Electrolisis  
    
[ Prácticas de laboratorio 1º BACHILLERATO, Departamento de Física y Química IES “Rey Fernando VI”](http://chopo.pntic.mec.es/jmillan/laboratorio_1.pdf)  
 Índice de prácticas  
 1. La Medida. Introducción al método experimental  
 2. Tiro horizontal-Conservación de la energía mecánica  
 3. Determinación del coeficiente de rozamiento con plano inclinado  
 4. Comprobación de la conservación de la energía mecánica  
 5. Preparación de disoluciones de sólidos solubles  
 6. Preparación de disoluciones a partir de líquidos  
 7. Determinación del contenido en ácido acetilsalicílico de una aspirina  
 8. Determinación del contenido de vitamina C que hay en una pastilla  
 9. Valoración del contenido de ácido acético de un vinagre  
 10. Estequiometría. Precipitación de Carbonato de Calcio  
 11. Investigando lo sucedido  
 12. Volumetría Redox: Permanganato con agua oxigenada  
 13. Comprobar la ley de las proporciones definidas.  
   
[Prácticas de laboratorio 4º ESO, Departamento de Física y Química IES “Rey Fernando VI”](http://chopo.pntic.mec.es/jmillan/laboratorio_4.pdf)  
 Índice de prácticas  
 1. Determinación de densidades de sólidos  
 2. Estudio de la caída libre  
 3. Estudio del péndulo simple  
 4. Ley de Hooke  
 5. Principio de Arquímedes  
 6. Determinación de la densidad de un líquido  
 7. Determinación de la densidad de un sólido  
 8. Determinación del calor específico de un sólido  
 9. Ejemplos de Reacciones Químicas  
   
[Prácticas de laboratorio 3º ESO, Departamento de Física y Química IES “Rey Fernando VI”](http://chopo.pntic.mec.es/jmillan/laboratorio_3.pdf)  
 Índice de prácticas  
 1. Sublimación  
 2. Reacciones de Precipitación  
 3. Convertir vino en agua, leche, fresa  
 4. Reacciones Redox en retroproyector  
 5. Equilibrios químicos y ley de Le Chatelier en retroproyector  
 6. Reacciones Ácido-Base en retroproyector  
 7. Formación de cristales de plata en el retroproyector  
 8. Dos reacciones redox  
 9. Reacciones de desprendimiento de gases en retroproyector  
 10. Descomposición del agua oxigenada  
 11. Convertir cobre en plata y oro  
 12. La botella azul  

[Prácticas de Química de Secundaria (ESO y Bachillerato) - Severino Ayuso Jimenez](www.seveayuso.es)  

[CUADERNO DE PRÁCTICAS FÍSICA y QUÍMICA 1º Bachillerato - Jorge Rojo Carrascosa](https://profesorjrc.es/apuntes/practicas/practicas%201bach%20fyq.pdf)  
1.1. EL TRABAJO EXPERIMENTAL  
1.2. PREPARACIÓN DE DISOLUCIONES  
1.3. ESTRUCTURA DE LA MATERIA  
1.4. ORDENACIÓN PERIÓDICA DE LOS ELEMENTOS  
1.5. ENLACE IÓNICO y METÁLICO  
1.6. CÁLCULOS ESTEQUIOMÉTRICOS  
1.7. QUÍMICA DEL CARBONO  
1.8. TERMODINÁMICA  
1.9. MOVIMIENTO I. CINEMÁTICA  
1.10. MOVIMIENTO II. DINÁMICA  
1.11. CONSERVACIÓN DE LA ENERGÍA  
1.12. INDUCCIÓN ELECTROMAGNÉTICA  

   
[DEPARTAMENTO DE FÍSICA Y QUÍMICA I.E.S. SANTA MARÍA DE CARRIZO](http://ficus.pntic.mec.es/vmad0017/wlaboratorio/laboratorio.html)  
 Experiencias por cursos de 2º, 3º, 4º ESO y 1º y 2º Bachillerato  

[Manual de laboratorio. Química general básica - quimicafacil.net](https://quimicafacil.net/category/manual-de-laboratorio/q-general-basica/)  
  
[Proyecto Profundiza IES "Alcrebite" Baza (Granada). EXPERIMENTAMOS EN EL LABORATORIO](http://profundizaalcrebite.blogspot.com.es/search/label/EXPERIMENTAMOS%20EN%20EL%20LABORATORIO)  
 
 IES Jovellanos  
 [Dpto.de Física y Química IES Jovellanos](http://www.iesjovellanos.com/departamentos/f&q/f&q.php)  
 Tiene páginas individuales del profesorado, y algunos comparten prácticas  
 [La Página de Mila *waybackmachine*](http://web.archive.org/web/20200927045735/http://www.iesjovellanos.com/departamentos/f&q/mila.php)  
 [La página de Juan Carlos](http://www.iesjovellanos.com/departamentos/f&q/juan_carlos.php)  
 
 [Departamento de ciencias, Química 2º Bachillerato - colegiocristorey.com](http://colegiocristorey.com/nenuca/nenina/neninaQFbach.html)  
 Nenina Martín Ossorio  
  
   
 [Experimentos con tecnología actual en el laboratorio de Física *waybackmachine*](http://web.archive.org/web/20170922175844/http://www.iesleonardoalacant.es/revista/Articulos-2/Laboratorio_fisica/Laboratorio_fisica.htm)  
> La sección dedicada a los [Experimentos  sobre movimientos sencillos de personas y de carritos](http://web.archive.org/web/20171020133215/http://intercentres.edu.gva.es/iesleonardodavinci/fisica/Laboratorio/Laboratorio02.htm) obtuvo Premio de la Consellería en el II Concurso de Ayudas para el Desarrollo de Recursos Educativos Digitales en 2008. La sección dedicada a Ondas, Campo y Luz fue finalista en Ciencia en Acción en 2009. El conjunto de todos los trabajos experimentales ha sido selecionado entre las 50 mejores aportaciones didácticas del portal europeo e internacional E-learning en 2009, y los temas de Oscilaciones y Ondas han obtenido Premio de la Consellería de Educación en el III Concurso de Ayudas para el Desarrollo de Recursos Educativos Digitales en 2009.  

[Laboratorio Física II (B y G) 2do cuatrimestre 2004](http://cms.iafe.uba.ar/abuccino/teaching_labo2_byg.html)  Andrea Paola Buccino  
 
[Profundización en Física y Química 4º ESO, IES Padre Moret-Irubide](http://irubidefyq.blogspot.com.es/2013/06/profundizacion-en-fisica-y-quimica-4-eso.html)  
 
[Química 2º Bachillerato Materiales de trabajo -elortegui.org](http://www.elortegui.org/ciencia/datos/2BACHQUM/quimica2.htm)  
Se incluyen algunas prácticas de laboratorio  
Entalpía de neutralización  
Velocidad de reacción  
Valoración de la acidez de un vinagre  
Construcción de una pila electrolítica  
Espectros atómicos  
Identificación de aldehidos  

[Técnicas de laboratorio - elortegui.org](http://www.elortegui.org/ciencia/datos/TecLab/TecLab2.htm)  
Muchos guiones

[Física y Química 3º ESO - elfisicoloco.blogspot.com.es](http://elfisicoloco.blogspot.com.es/p/fyq-3-eso.html)  
Incluye prácticas de laboratorio, muchos guiones  

En el canal de vídeo de alonsoformula.com se documentan algunas prácticas  
[https://www.youtube.com/user/alonsoformulacom](https://www.youtube.com/user/alonsoformulacom)  
Ejemplo  
[![](https://img.youtube.com/vi/AfdqPkubUYU/0.jpg)](https://www.youtube.com/watch?v=AfdqPkubUYU "Determinación del agua de cristalización de una sal como el CuSO4. Prácticas de química"  )

[Práctica laboratorio Física y Química. Gel hidroalcohólico - jfrutos.com](http://jfrutos.com/2020/10/20/practica-laboratorio-fisica-y-quimica-gel-hidroalcoholico/)  

[Propuesta didáctica para la obtención de una curva de calibración. Determinación de ácido acético con hidrogenocarbonato de sodio, lavavajillas y un agitador magnético, Otilia Val-Castillo  - analesdequimica.es](https://analesdequimica.es/index.php/AnalesQuimica/article/view/1668/2251)  

[Ciencia en experimentos ESO - elortegui.org](http://ciencia.elortegui.org/datos/LAC/lacmateriales.htm)  

[twitter AGSBioTechs/status/1466762065884897280](https://twitter.com/AGSBioTechs/status/1466762065884897280)  
As promised,here are some Yr 13s practicing their new-found pipetting skills.  
![](https://pbs.twimg.com/media/FFr8SQHWYAAODIX?format=jpg)  

[Prácticas de laboratorio - lamanzanadenewton.com](https://www.lamanzanadenewton.com/materiales/mat_m_lab1.html#xl_opt_lab_pr:195:161cticas)  
- Aparatos de medida
- Determinación de densidades
- Valoraciones ácido-base
- Pilas galvánicas 

[twitter mj_guisado/status/1491043794699309059](https://twitter.com/mj_guisado/status/1491043794699309059)  
Hilo experimentos 2º-3º ESO:  
Viscosidad (dejar caer bola en 4 líquidos distintos / ver como fluyen en una rampa)  
Medida y error (fisquiweb)  
Método científico: ¿Cuántas gotas caben en una moneda? (fqsaja1)  

[twitter JorgeFyQ/status/1642101948848504833](https://twitter.com/JorgeFyQ/status/1642101948848504833)  
Este trimestre hemos hecho una práctica tremendamente sencilla que les ha gustado mucho a los de #2ESO para el tema del movimiento. Medimos la constante de la gravedad. Abro un mini hilo  
Los materiales que usamos fueron muy sencillos. Usamos una cinta métrica para medir la altura y unas bolas de papel que había "requisado" a lo largo del trimestre.  
Además, usamos cronómetros para medir el tiempo que tarda en caer.  
Los chicos lanzaron las bolas desde lo alto de la escalera de emergencia que sale al patio y que tiene una altura de unos 6 metros y medio. Por turnos iban lanzando y el resto cronometraban. El tiempo de caída es de 1,15s aprox. que aún siendo poco da margen a medirlo.  
Previamente había medido las alturas y los tiempos para ver qué valores eran los correctos. Esto es clave porque el tiempo de reacción a la hora de cronometrar varía mucho y pueden salir valores se todos los colores. Hasta 3 segundos y pico llegaron a sacar.  
Luego queda la parte del cálculo de la constante a partir de los datos recopilados. Les puse la ecuación del MRUA y de ahí despejamos en la fórmula la aceleración. Esta parte se les escapa un poco pero con ayuda lo entienden. Despejamos y vemos que solo depende de "Yinicial" y t.  
Está sencilla práctica nos sirvió para que:  
📌 Todo el mundo entendiera y asumiera que hay que medir varias veces el valor del experimento.   
📌 Recordar cómo se calcula la media de unos datos.  
📌 Entender el concepto de que hay valores donde ha habido un error y que podemos descartarlos.  
📌 Que se pueden hacer experimentos de ciencia en cualquier lugar, no necesariamente en un laboratorio.  
La verdad es que la práctica superó todas mis expectativas. Os la recomiendo. Y lo más gracioso, y eso sí fue pura casualidad, les salió 9,7 m/s2.  

[twitter JorgeFyQ/status/1606352065919422464](https://twitter.com/JorgeFyQ/status/1606352065919422464)  
Para acabar el trimestre en el @IESMardearagon, hemos hecho la práctica del arcoiris dulce con los alumnos de 2º de la ESO.  
Consiste en preparar disoluciones de diferente densidad y a cada una se le añade un colorante  
Se prepararn disoluciones de 50 ml con diferente cantidad de azúcar. 1 cucharada sopera, 2, 3 y 4. Se disuelven bien (la de 4 cucharadas cuesta bastante) y luego se les añade un colorante alimenticio  
Se puede jugar con muchos colores o incluso mezclarlos para obtener otros colores diferentes.  
Luego se añaden los líquidos en un vaso de precipitados (o simplemente en un vaso suficientemente grande para que entren todas las disoluciones) teniendo en cuenta dos premisas:  
1) Se añade con un embudo y muy lentamente de manera que el embudo toque el fondo del vaso  
2) Se añade de menor a mayor densidad de manera que conforme se añade una disolución, las anteriores van subiendo porque son menos densas.
De manera que la primera que se añade es la de 1 cucharada y la última la de 4.  
Ya en dependencia de la creatividad de l@s alumn@s se puede obtener una combinación de colores u otra.  
Ellos se lo pasan en grande y nos sirve para interiorizar el concepto de densidad que normalmente asocian de manera errónea a la viscosidad.  



[twitter mj_guisado/status/1491040286562467843](https://twitter.com/mj_guisado/status/1491040286562467843)  
Voy a intentar hacer hilos recopilatorios de experimentos/experiencias interesantes para el aula. Por temas...  
Vamos con la Física:  	
Medición radio Tierra @ANTONIO_MORENO  

[twitter fqsaja1/status/1637895767011598342](https://twitter.com/fqsaja1/status/1637895767011598342)  
Cómo me gusta hacer la doble práctica de #TubosenU #PrincipiodeArquímedes Por qué? Es sencilla, tiene un alto poder explicativo y ofrece resultados casi perfectos. Buscamos eso en una práctica, no?  
Os dejo a los alumnos concentrados.  
![](https://pbs.twimg.com/media/Frr5dj4X0AAN2pN?format=jpg)  
![](https://pbs.twimg.com/media/Frr5dj0WIAEBoK1?format=jpg)  

[Prácticas de electricidad (3º ESO) - Blog de Tecnología – IES José Arencibia Gil – Telde](https://www3.gobiernodecanarias.org/medusa/ecoblog/fsancac/2014/02/18/practicas-de-electricidad-3o-eso/)  

[Prácticas electricidad básica Tecnología. 3º ESO (Curso 2011-2012) - Universitat de Valencia. OpenCourseWare](http://ocw.uv.es/ocw-secundaria-2/1practicaselec.pdf)  

[PRÁCTICAS DE ELECTRICIDAD 3º E.S.O. Repositorio para el alumnado de 3º de E.S.O. del I.E.S. Virgen de Villadiego, de Peñaflor (Sevilla, España) ](https://angelmicelti.github.io/TecnoVilladiego3/4EstruMeca/Electricidad/practicas.html)  

[Dossier cosmética - CENTRO DE EXPERIMENTACIÓN ESCOLAR DE PEDERNALES](https://www.euskadi.eus/contenidos/informacion/lan_ereduak_talleres/en_def/adjuntos/DOSSIER_COSMETICA.pdf)  

[PRÁCTICAS DE ANDAR POR CLASE (I) ¿Cómo determinar la masa molar del gas de los mecheros? Fernando Ignacio de Prada Pérez de Azpeitia- rseq.org](https://gedh.rseq.org/wp-content/uploads/2024/06/Boletin-41.pdf#page=32)  


## Prácticas de laboratorio asociadas a exámenes acceso universidad
[Coordinación de las Pruebas de Acceso a la Universidad de Física. Laboratorio de Física. *waybackmachine*](http://web.archive.org/web/20160607143623/http://www.uclm.es/profesorado/ajbarbero/paeg/Fisica0910_docIII.pdf)   
Se adjuntan propuestas de guiones de prácticas de laboratorio que cubrirían los objetivos experimentales de la asignatura. No se pretende que todo el profesorado realice las mismas prácticas de laboratorio. Se podrá llevar a cabo cualquier práctica de laboratorio que cubra los objetivos experimentales propuestos en el programa. No obstante por si pueden servir de ayuda a algún profesor se adjuntan los guiones detallados de las prácticas  

## Prácticas de laboratorio documentadas por universidades
 [Prácticas de Química Básica, Licenciatura en Farmacia - uah.es](http://www3.uah.es/edejesus/guiones/QB.pdf)  
 © 2002. Departamento de Química Inorgánica. Universidad de Alcalá  
 pdf 28 páginas
 
[GUÍAS DE LABORATORIO. DEPARTAMENTO DE FÍSICA Y GEOLOGÍA. UNIVERSIDAD DE PAMPLONA. FACULTAD DE CIENCIAS BÁSICAS (pdf 102 páginas)](https://www.unipamplona.edu.co/unipamplona/portalIG/home_152/recursos/general/14052018/guia_lab_mecanica.pdf)   
 
[Experiencias de Física: Demostraciones y Prácticas de Laboratorio. Universidad de Alicante. Departamento de Física, Ingeniería de Sistemas y Teoría de la Señal](http://rua.ua.es/dspace/handle/10045/45805)   
[EXPERIENCIAS DE FÍSICA AL ALCANCE DE TODOS. UA - Universitat d'Alacant / Universidad de Alicante. Lista youtube](https://www.youtube.com/playlist?list=PLoGFizEtm_6hVhzdWBZVW4O7TcPemL0c1)  

[ClickOnPhysics](http://www.clickonphysics.es/cms/)  
Actividades Manipulativas en línea para el aprendizaje de la Física en los grados en Ingeniería. Universidad de Vigo.  

[Determinación del grado de acidez de vinagres comerciales de distinta materia prima - us.es](https://idus.us.es/bitstream/handle/11441/95638/Determinaci%C3%B3n%20del%20grado.pdf)

[Collection of Physics Experiments -physicsexperiments.eu ](http://physicsexperiments.eu/en)  
This collection of physics experiments is developed by the Department of Physics Education, Faculty of Mathematics and Physics, Charles University in Prague.  
Main purpose of this Collection is to reach out to current and future physics teachers, mainly in the experimental part of their work. Our goal is to create an evolving and ever-expanding set of physics experiments containing suggestions of fundamental parts of physics encountered by students and teachers (whether at elementary school, high school or university).  
The authors’ goal is to support individual ideas for experiments by the most telling visual means in form of photos and videos, and also to provide users with technical and pedagogical insights that can facilitate the conducting of experiments and organically incorporate them into lessons.  

 Pueden estar relacionadas con pruebas como EvAU / olimpiadas con partes experimentales  
   
 [PRÁCTICAS DE FÍSICA EVAU, CURSO ACADÉMICO 2019-20 (página 9) - uclm.es ](https://www.uclm.es/-/media/Files/A04-Gestion-Academica/PDFEstudiantes/PDFEvAU/CoordinacionMaterias-1920/EVAU-Fisica-2019-20.ashx?la=es)  
Antonio J. Barbero García, José Carlos Mena Arroyo,Senén Martínez Maraña, Alicia Díaz Marcos y Fernando Cirujano Gutiérrez 
PRÁCTICA 1. PÉNDULO SIMPLE.  
Una determinación aproximada de la aceleración de la gravedad  
PRÁCTICA 2. LEY DE LA REFRACCIÓN.  
Medida del índice de refracción de una lámina de vidrio  
PRÁCTICA 3. ÁNGULO LÍMITE.  
Ángulo límite en una lámina de vidrio  
PRÁCTICA 4. INDUCCIÓN ELECTROMAGNÉTICA.  
A. Observación de la fuerza electromotriz inducida por la variación de flujo magnético  

[Prácticas de Laboratorio para Física I, Cátedra 2014 - unicen.edu.ar](http://www.exa.unicen.edu.ar/catedras/fisica1/Trabajos%20de%20Laboratorio/Practicas%20de%20Laboratorio%20FisicaI.pdf)  
Laboratorio #1: Medición del tiempo de reacción   
Laboratorio #2: Medición de longitudes, cálculo de perímetro y superficie  
Laboratorio #3: Medición del período de un péndulo   
Laboratorio #4: Generación de números aleatorios usando un icosaedro   
Laboratorio #5: Medición de velocidad media, velocidad instantánea, aceleración y tiempo de caída  

  
[EXPERIENCIAS DE LABORATORIO DE QUÍMICA. ORIENTACIÓNS DIDÁCTICAS PARA BACHARELATO](https://ciug.gal/PDF/Grupos_Traballo_2021/libropracticas.pdf)  
Coordinador D. José Mendoza Rodríguez. Instituto de Ciencias da Educación Universidade de Santiago de Compostela Autores (Grupo de Traballo Química Loxse)  
D. Darío Prada Rodríguez. Universidade de A Coruña.  
Dª Purificación López Mahía. Universidade de A Coruña.  
D. Alfonso González González. Inspección de E.E.M.M.  
D. José Mendoza Rodríguez. Instituto de Ciencias da Educación.  
D. Manuel Jesús Fontaíña Pérez. I.E.S.P. de Cangas, Cangas.  
D. Juan Alfonso del Valle Codesal. I.E.S. Monte das Moas, A Coruña.  
  
[PRACTICAS DE OPTICA GEOMETRICA Y RADIOMETRICA](https://rua.ua.es/dspace/bitstream/10045/4375/1/Pascual_Villalobos_Pr%C3%A1cticas_de_%C3%B3ptica.pdf) 
ISBN: 84-86809-01-0  
Depósito legal: A-247-1988  
114 páginas  

  
[Laboratorios. Facultad de física de la Pontificia Universidad Católica de Chile - fis.puc.cl](http://srv2.fis.puc.cl/mediawiki/index.php/Main_Page)  
Laboratorios:  
Errores e Instrumentación  
Ondas y Optica  
Termodinamica  
Fisica General  
Fisica Moderna  
Electricidad y Magnetismo  
Calor y Ondas  
Mecánica  

[GUIONES DE TÉCNICAS EXPERIMENTALES. (Sección de Física Cuántica). Departamento de Física de Partículas. CURSO 2006/2007 - usc.es *waybackmachine*](http://web.archive.org/web/20111216020534/http://www.usc.es/gir/docencia_files/te3/guiones.pdf)  
    
[Physics Instructional Laboratories - clemson.edu](http://www.clemson.edu/ces/phoenix/labs/)  

[Physics 6B Lab Manual - Introduction - physics.ucla.edu](http://demoweb.physics.ucla.edu/6b-lab-manual)  
  
[Física 2 (Biólogos y Geólogos) - 2do. cuatr. 2010 - uf.uba.ar](http://users.df.uba.ar/dcs/f2bg/labo/)  

[Prácticas de laboratorio en introducción a los materiales de construcción en ingeniería de la edificación. Departamento de Construcciones Arquitectónicas Universidad de Alicante](https://web.ua.es/es/ice/jornadas-redes/documentos/2013-posters/334608.pdf)  
B. Piedecausa García; S. Chinchón Payá   
  
[Laboratorio de Óptica. Departamento de Óptica. Universidad de Granada](http://www.ugr.es/~laboptic/sesiones_all.htm) 
    
[Departament de Física de la Terra i Termodinàmica, Laboratorio de Física, Grado en Farmacia - uv.es](http://www.uv.es/=termo/Laboratori/Fisica/2012-2013/Farmacia/GUIONES/labo_fis_farm_cast2012-2013.pdf)  
  
[MANUAL 4: PRÁCTICAS DE LABORATORIO DE ANÁLISIS QUÍMICO I](http://matematicas.uis.edu.co/9simposio/sites/default/files/V00Man04AnalQcoI-MFOQ-AQ.01_14122012.pdf)  
Luz Yolanda Vargas Fiallo Jaime Humberto Camargo Hernández  
ESCUELA DE QUÍMICA FACULTAD DE CIENCIAS UNIVERSIDAD INDUSTRIAL DE SANTANDER  
pdf 184 páginas  
  
[PRÁCTICAS DE QUÍMICA ORGÁNICA AMBIENTAL - ugr.es](http://www.ugr.es/~quiored/qoamb/qoamb.htm)  

[Laboratorio de Química 4.0. Proyecto de innovación docente 2011-49 - ugr.es](http://www.ugr.es/~laboratoriodequimica/practicas_II/)  
laboratoriodequimica@ugr.es  
7 prácticas en 2017   
1. Obtención de CO2 y Determinación de su Masa Molecular  
2. Determinación del equivalente-gramo del magnesio ...  

[Universidad de Valladolid. Departamento de Física de la Materia Condensada. Cristalografía y Mineralogía *waybackmachine*](http://web.archive.org/web/20191203111957/http://www4.uva.es/goya/Intranet/Pages/Principal.asp)  
En web original se citan prácticas que se enlazan directamente  
LISTADO Prácticas (2ª sesión)  
[1.- EL GIRÓSCOPO](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/giroscopo.pdf)  
[2.- APARATO DE OSCILACIÓN GIRATORIA. Cálculo de Momentos de Inercia ](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/Momentos%20de%20Inercia.pdf)  
[3.- PÉNDULO DE TORSIÓN. Cálculo de Momentos de Inercia ](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/Pendulo%20Torsion.pdf)  
[4.- RUEDA DE MAXWELL. Cálculo de Momentos de Inercia ](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/maxwell.pdf)  
[5.- VIBRACIÓN DE CUERDAS ](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/Vibraci%C3%B3n%20de%20cuerdas.pdf)  
[6.- TUBO DE RESONANCIA (I). Ondas Estacionarias ](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/resonancia.pdf)  
[7.- TUBO DE RESONANCIA (II). Ondas Estacionarias ](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/kundt.pdf)  
[8.- PÉNDULO DE POHL. Oscilaciones Forzadas](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/pohl.pdf)  
[9.- PÉNDULO FÍSICO FORZADO](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/penduloforzado.pdf)  
[10.- CURVAS ESFUERZO/DEFORMACIÓN PARA DIFERENTES MATERIALES. Cálculo del Módulo de Young ](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas2/2017-2018/esfuerzo.pdf)  
LISTADO Prácticas (1ª sesión)  
[1.- CARRIL NEUMÁTICO](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas1/2017-2018/carril.pdf)  
[2.- MESA NEUMÁTICA](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas1/2017-2018/mesa.pdf)  
[3.- ROZAMIENTO POR DESLIZAMIENTO](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas1/2017-2018/rozamiento.pdf)  
[4.- CONSERVACIÓN DE LA ENERGÍA MECÁNICA (I). Caída Libre](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas1/2017-2018/emecanicaI.pdf)  
[5.- CONSERVACIÓN DE LA ENERGÍA MECÁNICA (II). Resortes Helicoidales](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas1/2017-2018/emecanicaII.pdf)  
[6.- MOVIMIENTO CIRCULAR UNIFORME](http://mudarra.cpd.uva.es/goya/Intranet/pages/programas/laboratorio/fisica1/Practicas1/2017-2018/mvtocircular.pdf)  
  
[PRÁCTICAS DE LABORATORIO. QUÍMICA ORGÁNICA - uva.es](http://www.eis.uva.es/organica/quimica2/practicas/cuaderno.html)  
Práctica 1: Separación y purificación de compuestos orgánicos.  
Práctica 2: Preparación de un jabón Práctica  
3: Preparación de un detergente Práctica  
4: Preparación de Rayón Práctica  
5: Preparación de Paracetamol Práctica  
6: Síntesis de polímeros Práctica  
7: Síntesis de Galatita Práctica  
8: Preparación de Acetato de etilo Práctica  
9: Medida del grado alcohólico  

[DETERMINACIÓN DE ÍNDICES DE REFRACCIÓN - ucm.es](http://webs.ucm.es/info/Geofis/practicas/prac22.pdf)  

[eterminación de la entalpía de fusión del hielo - ucm.es](https://webs.ucm.es/info/Geofis/practicas/prac14r.pdf)  
  
[Oír y Ver. 61 experimentos de acústica y óptica - fseneca.es](https://fseneca.es/web/oir-y-ver-61-experimentos-de-acustica-y-optica)  
[Oír y Ver. 61 experimentos de acústica y óptica (pdf)](http://fseneca.es/web/sites/web/files/oir-y-ver.pdf)  
Alejandro del MAzo Vivar, Santiago Velasco Maíllo y Rafael García Molina  
Murcia: Universidad de Murcia, Universidad de Salamanca y Fundación Séneca, 2016  
Los animales (entre los cuales se encuentra el ser humano) emplean sensores fisiológicos para obtener inforamción de su entorno. Entre ellos podemos encoentrar senores eléctricos, magnéticos, térmicos, químicos, mecánicos y ópticos, por mencionar algunos. Varios de estos sensores son compartidos, en mayor o menos medida, por diversos animales, aunque están más desarrollados en unos que en otros. Esto ocurre en los seres humanos, pues no son igual de eficientes los cincos sentidos (vista, oído, tacto, olfato y gusto) de que disponemos. el sentido más empleado es el de la vista, seguido del oído, seguramente porque ambos permiten la detección a distancia; mientras que el primero se basa en las señales luminosas, el segundo requiere de estímulos acústicos.  
En este libro se presentan experiencias relacionadas con estos dos sentidos, cuyas fuentes de información (luz y sonido, respectivamente) comparten, con los oportunos matices, características de los fenómenos ondulatorios. Los materiales necesarios para cada experiencia son fáciles de conseguir, pues se encuentran en la mayotía de centros de enseñanza y, también, en muchos hogares.  
 
[Universidad Complutense de Madrid :: OSCAR, física visual a un click](https://webs.ucm.es/centros/webs/oscar/index.php) (pongo enlaces a 3 enero 2019)
   * **Fluidos**
      1.  [Tensión superficial: objetos en la superficie del agua](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34323.php) 
      1.  [Tensión superficial y películas de jabón](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34322.php) 
      1.  [Pelota de ping-pong levitando](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34332.php) 

   * **Oscilaciones y ondas**
      1.  [Estudiando un minutero de escalera](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34331.php) 
      1.  [Figuras de Lissajous](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=33837.php) 
      1.  [Ondas estacionarias en una cuerda](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34318.php) 
      1.  [Cubeta de ondas](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34321.php) 
      1.  [Velocidad del sonido](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34320.php) 
      1.  [Tubo de Rubens](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34319.php) 
      1.  [Tubo de Kundt](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34317.php) 

   * **Termodinámica**
      1.  [Motor de Stirling](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34330.php) 

   * **Electricidad y Magnetismo**
      1.  [El Electroscopio](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34325.php) 
      1.  [Campana de Franklin](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34327.php) 
      1.  [Varita mágica](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34334.php) 
      1.  [Generador de Van de Graaff](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34324.php) 
      1.  [Motor líquido](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34329.php) 
      1.  [Pilas Zinc-Cobre](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34328.php) 
      1.  [Cubo de resistencias y LEDs](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34316.php) 
      1.  [Cañón de Gauss](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34313.php) 
      1.  [Levitrón](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34315.php) 
      1.  [Fuerzas de Lorentz](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34309.php) 
      1.  [Atracción y repulsión entre corrientes](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34310.php) 
      1.  [Anillo de Thomson](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34308.php) 
      1.  [El motor eléctrico más sencillo](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34312.php) 
      1.  [Freno magnético](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34307.php) 
      1.  [Corrientes inducidas](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=33836.php) 
      1.  [Simulación de un aerogenerador](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34314.php) 
      1.  [Motor electrostático](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34326.php) 
      1.  [Bobina de Tesla](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=34311.php) 

   * **Óptica**
      1.  [Difracción a través de una rendija](http://www.ucm.es/centros/webs/oscar/index.php?tp=&a=dir2&d=33835.php) 

 Acceso al catálogo completo
 [https://docs.google.com/open?id=0B0yZp1C8vsMqYmZwMl84R3E0VWc﻿](https://docs.google.com/open?id=0B0yZp1C8vsMqYmZwMl84R3E0VWc)  

 [Inducción al aprendizaje autónomo del laboratorio de Química General utilizando dispositivos audiovisuales actuales. Proyecto de Innovación y Mejora de la Calidad Docente](http://147.96.70.122/Manual_de_Practicas/home.html)  
 Queremos expresar nuestro agradecimiento al Vicerrectorado de Evaluación de la Calidad de la Universidad Complutense de Madrid por haber aprobado el proyecto PIMCD 196 en la convocatoria correspondiente a los Proyectos de Innovación y Mejora de la Calidad Docente de 2013.  
 [Guía de prácticas de Química general e introducción al laboratorio químico. Primer curso grado en farmacia. Departamento de química inorgánica y bioinorgánica. Facultad de Farmacia. Universidad Complutense de Madrid](http://147.96.70.122/Manual_de_Practicas/home.html?guia_de_practicas.htm)  
 
### Prácticas documentadas por alumnos  
A veces los alumnos publican en blogs sus prácticas, con información, vídeos ... y pueden ser muy interesantes.  
Los blogs que crean no suelen durar en el tiempo  

[Informes - fisicarecreativa.com](http://www.fisicarecreativa.com/informes/informes.htm)  
Informes de proyectos experimentales realizados en la Universidad de Buenos Aires, Universidad Nacional de Gral. San Martín, Universidad Favaloro, Universidad de san Andrés, Universidad Nacional el Sur y otras universidades de Argentina. 

[proyectointegrado14.blogspot.com.es/](http://proyectointegrado14.blogspot.com.es/)  

[ Química Experimental, Equipo 9. (Semestre 1, 2013) ](http://quimicaexperimental9.blogspot.com.es/)   
  
[MÉTODO CIENTÍFICO ¿Qué tomas cuando tomas homeopatía?](https://iniciamosinvestigacion.blogspot.com/2018/10/metodo-cientifico-que-tomas-cuando.html)  
Somos el alumnado de 1º y 2º de la ESO que cursa la materia "Iniciación a la investigación" en el IES "Bartolomé Pérez Casas" de Lorca (Murcia) durante el curso escolar 2018-2019. 

## [Prácticas con smartphone](/home/recursos/practicas-experimentos-laboratorio/practicas-experimentos-laboratorio-smartphone)  

## Prácticas asociadas a ciencia ciudadana / laboratorios ciudadanos
Participar en proyectos con sensores o computación distribuida, con materiales de hardware libre... DECO, SETI, publiclab ...  
También puede enlazar con la [Cultura maker](https://es.wikipedia.org/wiki/Cultura_Maker), experiencias con elementos reciclados y/o de impresoras 3D  

[twitter LebreroPastor/status/1477703391564513287](https://twitter.com/LebreroPastor/status/1477703391564513287)  
Dentro del proyecto de centro: "Desarrollo de materiales didácticos a partir de residuos", enmarcado en los ODS, hemos hecho un dispositivo que convierte tu móvil en un microscopio. Y ha sido a partir de basura tecnológica y una pieza impresa en PLA.  
Este dispositivo unido a tu móvil te permite fotografiar tus muestras o grabar vídeos. Aquí os mostramos una pequeña galería de insectos, pantallas de móvil, cristales, tejido intestinal, pelo...  
Nuestros alumnos de 4º ESO ya han diseñado el suyo, en breve publicaremos en la web del @iescastilla
 sus diseños. Mientras tanto podéis hacer el vuestro propio a partir del siguiente manual:  
[MANUAL DE FABRICACIÓN DE UN MICROSCOPIO PARA TU SMARTPHONE - Ernesto Pastor Lebrero](https://drive.google.com/file/d/1HlUfQfIBxyniqE7hL6OkG4jY43mKiyi9/view)  

[Red de Laboratorios Ciudadanos Distribuidos - madrimasd.org](https://www.madrimasd.org/cultura-cientifica/red-laboratorios-ciudadanos/red-laboratorios-ciudadanos-distribuidos)  

[Red de Laboratorios CiudadanosLaboratorios en marcha - madrimasd.org](https://www.madrimasd.org/cultura-cientifica/red-laboratorios-ciudadanos/laboratorios-en-marcha)  



##  [Prácticas de laboratorio de elaboración propia](/home/recursos/practicas-experimentos-laboratorio/practicas-de-laboratorio-de-elaboracion-propia) 

##  [Prácticas de laboratorio con arduino](/home/recursos/practicas-experimentos-laboratorio/practicas-experimentos-laboratorio-arduino) 

### Cacharrismo

[![](https://img.youtube.com/vi/LIVRN3if34M/0.jpg)](https://www.youtube.com/watch?v=LIVRN3if34M "Eduhorchata 2018 - Cacharrismo y metacacharrismo - Javier Fernández Panadero")

[![](https://img.youtube.com/vi/FazOCvxdSdc/0.jpg)](https://www.youtube.com/watch?v=FazOCvxdSdc "TALLER DE CACHARRISMO. Joaquín Sevilla, UPNA. Proyecto S-TEAM. Grupo 9 Universidades.")

[Cacharrimos Científicos  - canal youtube](https://www.youtube.com/channel/UC3V6cQC2c46dj6_05CZmxqQ)  

[twitter Joaquin_Sevilla/status/1684966591627239430](https://twitter.com/Joaquin_Sevilla/status/1684966591627239430)  
Cacharrismo: hoja de papel albal, dos ranuras hechas con un cúter y laser de 3 €  
![](https://pbs.twimg.com/media/F2I0L1IWYAEl2jh?format=jpg)  
En la pared, el patrón de interferencia esperado. Si Young le antara la cabeza...  
![](https://pbs.twimg.com/media/F2I0ZmuXYAAKu2X?format=jpg)  

## Experiencias asociables a laboratorio / experimentos en casa  

[Physics Education at home: DIY activities for home learning - iopscience.org](https://iopscience.iop.org/journal/0031-9120/page/physics-education-at-home)  
> With many schools and colleges around the globe currently shut due to the ongoing COVID-19 pandemic, here are some articles from the vast Physics Education archive, to help provide some inspiration for teachers and students alike.  
> These peer reviewed articles tend to be aimed at older school students but may be suitable for 14-16 year olds and in a few cases even younger students. Many of the activities could be simplified for younger age groups but the papers as they are would not be suitable.  
> Articles have been chosen that do not require specialist materials and so can be carried out at home. The focus has also been on articles that have an associated physics activity, such as an experiment, as teachers can often easily set book work or computer based work but activities for older students are more difficult to find.  

half life explained and demonstrated with beer foam - do it yourself! - PhysicsHigh  
[![](https://img.youtube.com/vi/xHDxYaiUofI/0.jpg)](https://www.youtube.com/watch?v=xHDxYaiUofI "half life explained and demonstrated with beer foam - do it yourself! - PhysicsHigh")   


[Experiencias educativas enriquecedoras - knowleb](https://knowleb.com/)  

[Ciensación - Educación científica tangible. Encuentra experimentos «para poner las manos en la masa» para tu próxima clase](https://www.ciensacion.org/)  
[Ciensación - física](https://www.ciensacion.org/busqueda.html?q=F%C3%ADsica)  
[Ciensación - química](https://www.ciensacion.org/busqueda.html?q=Qu%C3%ADmica)  

Algunas requieren material de laboratorio
[Accidente aéreo](https://knowleb.com/wp-content/uploads/2021/10/Knowleb-Experiencias-accidente_aereo.pdf)  
>La experiencia ACCIDENTE AÉREO ha sido diseñada con la intención de simular una situación hipotética en la que se requieran conocimientos de química. Concretamente la reacción que tiene lugar cuando interacciona un metal con un ácido.  
 
>PRÁCTICA METAL + ÁCIDO  
Enviamos a los estudiantes la noticia y el informe final, para que el día de la práctica puedan 
responder a las preguntas.  

[RECETARIO GASTRO FÍSICA](https://estudiantes.rsef.es/ConcursoGastrofisica/Concurso%20Divulgaci%c3%b3n%20Gastrof%C3%ADsica%20-%20Recetario.pdf)  
[Gastrofísica. 11 experimentos-receta relacionados con la cocina, explicando un principio físico presente](https://fisiquimicamente.com/blog/2022/04/24/gastrofisica/)  
    1. Newtoniano o no newtoniano
    2. Lluvia arcoíris
    3. Lágrimas de vino
    4. Desviación de un chorro de agua
    5. Efecto Leidenfrost
    6. Corrientes de convección y difusión
    7. Falsa transición de fase en huevos
    8. Lentes de gelatina
    9. Reflexión interna total
    10. Velocidad de la luz bañada en chocolate
    11. Uva de plasma

[twitter DarekDewey/status/1527004149858111497](https://twitter.com/DarekDewey/status/1527004149858111497)  
Apagar velas a diferentes alturas al cerrarlas en un recipiente cerrado. Se apaga primero la más elevada, y el resto de manera no lineal.  
En el experimento de apagar una vela con CO2 el CO2 es más denso y se va abajo  


Algunas prácticas pueden no ser simulaciones y prácticas con smartphone, pero necesitar un ordenador y un dispositivo que realice medidas y las pase al ordenador  
[twitter fisicamartin/status/1539594259338301441](https://twitter.com/fisicamartin/status/1539594259338301441)  
Corrientes inducidas generadas en una bobina por la caída de un imán (y sus rebotes en la mesa). #Física #Physics #Faraday 
![](https://pbs.twimg.com/media/FV28zgFXwAITLm0?format=jpg)  


[twitter wonderofscience/status/1542522502353547272](https://twitter.com/wonderofscience/status/1542522502353547272)  
This ferrofluid audio-visualizer created by artist Dakd Jung dances to the music.  
[Sound Reactive Bluetooth Speaker Uses Magnetic Ferrofluid to Become a Real-Life Winamp Visualizer - gizmondo](https://gizmodo.com/sound-reactive-bluetooth-speaker-uses-magnetic-ferroflu-1846729756)  
[![](https://img.youtube.com/vi/pgp2sp0EB7w/0.jpg)](https://www.youtube.com/watch?v=pgp2sp0EB7w "Ferrofluid display cell bluetooth speaker")  

[![](https://img.youtube.com/vi/5PFgVtzsXHM/0.jpg)](https://www.youtube.com/watch?v=5PFgVtzsXHM "Worlds Largest Open-Source Ferrofluid Display - Fetch #8")  

[Simple science experiments you can do at home - metdaam](https://www.facebook.com/MetDaanMagazine/videos/simple-science-experiments-you-can-do-at-home/2573093672836329/)  

[50 EXPERIMENTOS DE CIENCIA PARA HACER EN CLASE Y EN CASA - imageneseducativas.com](https://www.imageneseducativas.com/50-experimentos-de-ciencia-para-hacer-en-clase-y-en-casa/)  

[![](https://img.youtube.com/vi/29pgjBjH7So/0.jpg)](https://www.youtube.com/watch?v=29pgjBjH7So "30 EXPERIMENTOS SIMPLES QUE TE SORPRENDERÁN - Ideas en 5 minutos")  

[![](https://img.youtube.com/vi/7mZQZpwbRGU/0.jpg)](https://www.youtube.com/watch?v=7mZQZpwbRGU "18 FÁCILES TRUCOS MÁGICOS Y EXPERIMENTOS DE CIENCIA PARA LA ESCUELA - Ideas en 5 minutos JUEGOS")  

[Physics in Advent](https://www.physics-in-advent.org/)  
24 small, simple experiments and physics mysteries: an Advent calendar with a difference. Each day from 1st to 24th December there is a video clip of an experiment which you can do yourself afterwards.  

[twitter fqsaja1/status/1657285278979981312](https://twitter.com/fqsaja1/status/1657285278979981312)  
Siempre me ha parecido muy elegante esta demostración para apagar velas con el CO2 que se desprende en la práctica de reacciones (2º ESO) con bicarbonato+vinagre.  
Estaría bien hacerlo a escala XXL. Alguien se anima??  

9 Awesome Science Tricks Using Static Electricity!  
 [![](https://img.youtube.com/vi/ViZNgU-Yt-Y/0.jpg)](https://www.youtube.com/watch?v=ViZNgU-Yt-Y "9 Awesome Science Tricks Using Static Electricity!")  

[NASCAR, Online Education, Chemistry in Action](https://www.nascarhall.com/plan-a-visit/online-education/chemistry-in-action)  
> Grade Level: High school  
Student Objectives  
• Review elements and alloys and application with a race car  
• Review acid and base as well as reaction properties  
• Understand how car chemistry impacts racing performance, especially in the engine  
• Simulate engine performance by building a car using household supplies that can move on its own  

### Medición del radio de la Tierra

[![](https://img.youtube.com/vi/giey3fAzJTg/0.jpg)](https://www.youtube.com/watch?v=giey3fAzJTg "Cómo medir la Tierra con un palo de selfie - date un voltio ")  

[twitter fqsaja1/status/1643282052303781888](https://twitter.com/fqsaja1/status/1643282052303781888)  
El diccionario de la RAE define Divulgar como  "Publicar, extender, poner al alcance del público algo"
Pensaba este mediodía mientras ensayábamos una nueva medida del radio terrestre con alumnos que esto se le parece mucho. Lo explico...  
El curso pasado 11 alumnos midieron de manera eficiente el radio terrestre por el método de Eratóstenes y este curso lo harán 8 más. En total 19. Los profesores que miraban por las ventanas con extrañeza eran respondidos por los que ya lo hicieron: "Miden el radio de la tierra!  
Al salir, se acercaban y les aconsejaban con esa vinculación ilimitada que tiene el alumno con el proyecto ya realizado. Otra alumna llamaba a casa para advertir el retraso: "mamá, ya te dije que comeré tarde porque íbamos a medir el radio de la tierra" Sonaba ilusionante!!!  
Si a esos 19 alumnos les añadimos sus 38 orgullosos padres (me consta que lo están cuando los encuentro) habrá, al menos, 57 personas que sabrán de manera directa que el radio terrestre se estima en base a las sombras en el mediodía solar, reconociendo el montaje cuando lo ven..  
En una población de algo más de 5000 habitantes esto supone que un 1% conocerá a través de este trabajo un experimento histórico extraordinario. Y no me parece poco, me parece muchísimo. Y como dice la @RAEinforma extiende y pone al alcance del público la historia de la ciencia..  
Y me siento orgulloso de esta divulgación, de la que hace que personas ajenas a la física me hablen de Eratóstenes o de los hemisferios de Magdeburgo. Mucho más que la divulgación que puedo hacer para los que saben de esto tanto o más que yo. Y además creo que es una obligación  


### Lanzamiento de cohetes con agua

Es un taller en Robledo de Chavela
[Taller de Cohetes de Agua - primaria](https://www.mdscc.nasa.gov/index.php/training-and-visitors-center/visitors-center/activities/espacio-para-profesores/educacion-primaria/taller-de-cohetes/)  
[Taller de Cohetes de Agua - secundaria](https://www.mdscc.nasa.gov/index.php/training-and-visitors-center/visitors-center/activities/espacio-para-profesores/educacion-secundaria/taller-de-cohetes-de-agua/)  

[twitter fqsaja1/status/1515222183202721795](https://twitter.com/fqsaja1/status/1515222183202721795)  
Todos, todos los años hay cohetes de agua con los alumnos de 4º. Les encanta y sin embargo ninguno se atreve a experimentar diseños complicados que consigan más altura. No desisto...  

[![](https://img.youtube.com/vi/_IxOY72fDSM/0.jpg)](https://www.youtube.com/watch?v=_IxOY72fDSM "COHETES DE AGUA 2016.  IES IZPISUA BELMONTE")  


