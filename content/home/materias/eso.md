
# Materias ESO

Enlaces a materias ESO:  
* **[2º ESO Física y Química](/home/materias/eso/fisica-y-quimica-2-eso)**  
* **[3º ESO Física y Química](/home/materias/eso/fisica-y-quimica-3-eso)**  
* **[4º ESO Física y Química](/home/materias/eso/fisicayquimica-4eso)**  
* [4º ESO Ampliación Física y Química (LOE y LOMCE)](/home/materias/eso/ampliacion-fisica-y-quimica-4-eso)  
* [4º ESO Cultura Científica (LOMCE)](/home/materias/eso/cultura-cientifica-4-eso)  
* [4º ESO Ciencias aplicadas a la actividad profesional](/home/materias/eso/ciencias-aplicadas-a-la-actividad-profesional-4-eso)  
* [4º ESO Biología y geología](/home/materias/eso/4-eso-biologia-y-geologia)  
* [Ciencias de la naturaleza](/home/materias/eso/ciencias-de-la-naturaleza-1y2eso)  (1º y 2º ESO, información general sobre el área)  
* [Ámbito Científico Tecnológico - Diversificación (LOE/LOMLOE)](/home/materias/eso/ambitocientificotecnologico-diversificacion)  (paralelos a 3º y 4º ESO, LOE y LOMLOE)  
* [Ámbito Científico Matemático - PMAR (LOMCE)](/home/materias/eso/ambito-cientifico-matematico-pmar) (paralelos a 2º y 3º ESO) 
* [3º ESO Taller de Astronomía](/home/materias/eso/taller-de-astronomia)   
* [4º ESO Proyecto en Investigación Científica e Innovación Tecnológica. Técnicas de laboratorio de Física y Química](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/materias/eso/PropuestaOptativa4ESOProyectoEnT%C3%A9cnicasDeLaboratorioDeF%C3%ADsicaQu%C3%ADmica.pdf) Planteada como una materia de proyecto Madrid LOMLOE que sustituya a [4º ESO Ampliación Física y Química (LOE y LOMCE)](/home/materias/eso/ampliacion-fisica-y-quimica-4-eso) que desaparece en LOMLOE  
* [4ºESO Proyecto en Cultura Científica](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/materias/eso/PropuestaOptativa4ESOCulturaCient%C3%ADfica.pdf) Planteada como una materia de proyecto Madrid LOMLOE que sustituya a [4º ESO Cultura Científica (LOMCE)](/home/materias/eso/cultura-cientifica-4-eso) que desaparece en LOMCE  

Las materias que puede impartir un profesor de la especialidad de Física y Química en ESO son las siguientes:  
(Ver  [Especialidades y materias afines. Tabla especialidades - materias](https://sites.google.com/site/especialidadesymateriasafines/home/especialidades-y-materias-afines-secundaria/tabla-especialidades-materias) )  

La LOMCE hace desaparecer los Programas de Diversificación Curricular de 3º y 4º, que son sustituidos por los Programas de Mejora del Aprendizaje y el Rendimiento (PMAR) en 2º y 3º (3º pasa a estar en primer ciclo que engloba los tres primeros cursos de la ESO)  
En LOMLOE desparece PMAR y se recuperan los Programas de Diversificación Currícular de nuevo en 3º y 4º.  

Hay algunas materias optativas de Madrid, ver [legislación educativa](/home/legislacion-educativa)  

## Currículos
* [Currículo Física y Química ESO](/home/materias/eso/curriculo-fisica-y-quimica-eso)  

