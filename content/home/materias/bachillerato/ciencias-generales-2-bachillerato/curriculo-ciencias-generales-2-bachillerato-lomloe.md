# Currículo Ciencias Generales 2º Bachillerato (LOMLOE)

En esta página se incluye el currículo estatal (Real Decreto) y el de Madrid (Decreto)

## Estatal (Real Decreto)
Revisado con [Real Decreto 243/2022, de 5 de abril, por el que se establecen la ordenación y las enseñanzas mínimas del Bachillerato.](https://www.boe.es/buscar/act.php?id=BOE-A-2022-5521) citado en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  

En octubre 2021 borrador [Borrador de RD de enseñanzas mínimas de Bachillerato](https://educagob.educacionyfp.gob.es/dam/jcr:916a7585-8ed6-4f16-975b-dc23895a2581/proyecto-rd-bachilleratocompleto-.pdf)

Revisado con [Proyecto de real decreto por el que se establece la ordenación y las enseñanzas mínimas del Bachillerato (PDF)](https://www.educacionyfp.gob.es/dam/jcr:e72aadd9-a4bc-4f00-985e-7167a254425b/prd-ensenanzas-minimas-bachillerato.pdf)  (Ciencias Generale sen páginas 82 a 90, 61 a 69 de numeración (proyecto 9 diciembre 2021) citado en [Currículo LOMLOE Física y Química](/home/legislacion-educativa/lomloe/curriculo-fisica-y-quimica)  

Algunos cambios diciembre 2021 vs octubre 2021:  
El párrafo "Acompañando a las competencias ..." pasa a ser el 3º.  
Se cambia "pretende aportar al alumnado y al profesorado las herramientas básicas para reconocer" por  "no solo pretende concienciar
sobre"  
Se añade "sino que
proporcionará al alumnado que desee explorar otros campos profesionales no
vinculados directamente con las ciencias; conocimientos y aprendizajes propios
de las ciencias que permitan un enfoque riguroso y certero en su labor
profesional."  

Algunos cambios Real Decreto 2022 vs diciembre 2021:  
Se añade "social"  
Se cambia "bloque A de saberes" por "bloque «Construyendo ciencia»"  
Se cambia "bloque B" por "«Un universo de materia y energía»" y se cambia redacción del párrafo  
Se cambia "bloque C" por "«El sistema Tierra»"  
Se cambia "bloque D" por "«Biología para el siglo XXI»"  
Se cambia "bloque E" por "«Las fuerzas que nos mueven»", se elimina "Nuevamente se trata de contenidos transversales para todas las disciplinas de la ciencia, los cuales", se cambia "crear" por "incentivar"  


En la sociedad actual multitud de aspectos están relacionados con la actividad científica, tanto en el campo sanitario como en el tecnológico, **el social y** divulgativo. Poseer una formación científica sólida permite a cada individuo defender una opinión fundamentada ante hechos que pueden resultar controvertidos y que forman parte del día a día de nuestro mundo. Esta materia ofrece al alumnado una formación básica en las cuatro disciplinas científicas fundamentales. Además, el enfoque interdisciplinar característico de la enseñanza STEM confiere al currículo un carácter unificador que pone en evidencia que las diferentes ciencias no son más que una especialización dentro del conjunto global y coherente que es el conocimiento científico. De hecho, en el desarrollo de la investigación como actividad laboral, los científicos y científicas relacionan conocimientos, destrezas y actitudes de todas las disciplinas para enriquecer sus estudios y contribuir de forma más eficiente al progreso de la sociedad.

El alumnado que cursa Ciencias Generales adquiere una comprensión general de los principios que rigen los fenómenos del mundo natural. Para ello, esta materia parte de las competencias específicas, que tienen como finalidad **que el alumnado** entienda, explique y movilice conocimientos, destrezas y actitudes no solo relacionados con la situación y las repercusiones de la ciencia en la actualidad, sino también con los procedimientos de la actividad científica y su relevancia en el avance social, la necesidad de un trato igualitario entre personas en la ciencia y el carácter consistente y global del conjunto de las disciplinas científicas. A esta materia podrán acceder diferentes perfiles de estudiantes, con distintas formaciones previas en ciencias, por lo que la adquisición de los aprendizajes esenciales de esta materia se construye a partir de las ciencias básicas que todo alumno y alumna ha cursado durante la Educación Secundaria Obligatoria, profundizando a partir de ahí para alcanzar las competencias y los objetivos propios de la etapa del Bachillerato.
 
Acompañando a las competencias específicas de esta materia se encuentran los criterios de evaluación. Su marcado carácter competencial los convierte en evaluadores de los saberes básicos que el alumnado debe adquirir para desenvolverse en una sociedad que demanda espíritu crítico ante cuestiones científicas. Sus características se corresponden con las de un currículo que pretende desarrollar el pensamiento científico para que la ciudadanía comprenda, explique y razone por qué sin ciencia no hay futuro.

El desarrollo de las competencias específicas se apoya en los saberes básicos de la materia, que se encuentran estructurados en cinco bloques que incluyen los conocimientos, destrezas y actitudes imprescindibles.

El bloque **«Construyendo ciencia»** trata los aspectos básicos de la actividad científica general: el uso de las metodologías científicas para el estudio de fenómenos naturales, la experimentación incluyendo los instrumentos necesarios y sus normas de uso, la utilización adecuada del lenguaje científico y de las herramientas matemáticas pertinentes, etc. Se trata de un bloque introductorio que, lejos de pretender ser tratado de manera teórica, busca desarrollar destrezas prácticas útiles para el resto de los bloques.

El segundo bloque, «Un universo de materia y energía», recoge dos conceptos fundamentales de la ciencia: la materia y la energía. Estos conceptos son esenciales en el estudio y trabajo de la ciencia, pues son la base para la construcción de aprendizajes sobre los sistemas fisicoquímicos, biológicos y geológicos.

En el bloque **«El sistema Tierra»** se hace una aproximación al estudio de la Tierra y los sistemas terrestres desde el punto de vista de la geología planetaria, de la tectónica de placas y de la dinámica de las capas fluidas. Además, incluye aspectos clave encaminados a la concienciación del alumnado sobre la necesidad de adoptar un modelo de desarrollo sostenible y la promoción de la salud.

El bloque **«Biología para el siglo XXI»** trata de algunas cuestiones sobre la biotecnología y su importancia en la investigación de enfermedades, técnicas de agricultura y ganadería o recuperación medioambiental, entre otras.

Por último, el bloque «Las fuerzas que nos mueven» presenta las fuerzas fundamentales de la naturaleza y los efectos que tienen sobre los sistemas. Estos saberes permiten dar explicaciones a aspectos tan importantes como el movimiento de los cuerpos o las deformaciones de la corteza terrestre.

En definitiva, el currículo de Ciencias Generales no solo pretende concienciar sobre la importancia de las ciencias, e **incentivar** vocaciones científicas y formadores científicos que tengan un criterio propio y fundamentado para la difusión de ideas por encima de afirmaciones pseudocientíficas y engañosas, sino que proporcionará al alumnado que desee explorar otros campos profesionales no vinculados directamente con las ciencias, conocimientos y aprendizajes propios de las ciencias que permitan un enfoque riguroso y certero en su labor profesional. Las herramientas que proporciona este currículo invitan al desarrollo de proyectos y a la cooperación interdisciplinar, propios de la investigación científica. Esto confiere al aprendizaje de la ciencia un carácter holístico e integrado, que enriquece la significatividad y prepara al alumnado para afrontar el futuro.
 
## Competencias Específicas 

Algunos cambios diciembre 2021 vs octubre 2021:  
En 1 se añade "siendo consciente de que
las respuestas a procesos, físicos, químicos, biológicos y geológicos son
complejas y necesitan de modelos contrastados y en constante revisión y
validación."  
En 4 se añade "La inclusión de esta competencia específica en el currículo de Ciencias
Generales pretende que el alumnado aprenda que se puede llegar a los mismos
resultados utilizando diferentes herramientas y estrategias, siempre y cuando
sean fiables y estén contrastadas. Asimismo, se busca la consideración del error
como una herramienta para descartar líneas de trabajo y una manera de
aprender en la que se mejoran la autocrítica, la resiliencia y la colaboración entre
iguales."  
En 5 cambia primer párrafo.  

Algunos cambios Real Decreto 2022 vs diciembre 2021:  
En 1 se cambia CPSAA5 por CPSAA4.  
En 2 se cambia CPSAA1 por CPSAA1.1.  
En 3 se cambia "hábitos" por "estilos de vida", se añade "y social" y se cambia "CPSAA3" por "CPSAA2"  
En 4 se cambia CPSAA1 por CPSAA1.1  
En 5 se cambia CPSAA5 por CPSAA4.  

1. Aplicar las metodologías propias de la ciencia, utilizando con precisión, procedimientos, materiales e instrumentos adecuados, para responder a cuestiones sobre procesos físicos, químicos, biológicos y geológicos.

Para conseguir una alfabetización científica básica, cada alumno o alumna debe comprender cuál es el modus operandi de toda la comunidad científica en lo referente al estudio de los fenómenos naturales y cuáles son las herramientas de que se dispone para ello. Las metodologías científicas son procedimientos fundamentales de trabajo en la ciencia. El alumnado debe desarrollar las destrezas de observar, emitir hipótesis y experimentar sobre fenómenos fisicoquímicos y naturales, así como de poner en común con el resto de la comunidad investigadora los resultados que obtenga, siendo consciente de que las respuestas a procesos, físicos, químicos, biológicos y geológicos son complejas y necesitan de modelos contrastados y en constante revisión y validación. 

Asimismo, aunque el alumnado no optase en el futuro por dedicarse a la ciencia como actividad profesional, el desarrollo de esta competencia le otorga algunas destrezas propias del pensamiento científico que puede aplicar en situaciones de su vida cotidiana, como la interpretación de **fenómenos** o el respeto por el mundo natural que le rodea. Esto contribuye a la formación de personas comprometidas con la mejora de su entorno y de la sociedad.

Esta competencia específica se conecta con los siguientes descriptores: CCL3, STEM1, STEM2, STEM3, CD1, CD3, CPSAA4, CE1.

2. Comprender y explicar los procesos del entorno y explicarlos, utilizando los principios, leyes y teorías científicos adecuados, para adquirir una visión holística del funcionamiento del medio natural.
 
El desarrollo de la competencia científica tiene como finalidad esencial comprender los procesos del entorno e interpretarlos a la luz de los principios, leyes y teorías científicas fundamentales. Con el desarrollo de esta competencia específica también se contribuye a desarrollar el pensamiento científico, lo cual es clave para la creación de nuevos conocimientos.
 
Además, la aplicación de los conocimientos está en línea con los principios del aprendizaje STEM, que pretende adoptar un enfoque global de las ciencias como un todo integrado. El alumnado que cursa esta materia aprende a relacionar conceptos, encontrando en ella los conocimientos, destrezas y actitudes necesarios para una alfabetización científica general.

Esta competencia específica se conecta con los siguientes descriptores: CCL1, CCL2, CP1, STEM1, STEM2, STEM4, CD1, CPSAA1.1.

3. Argumentar sobre la importancia de los estilos de vida sostenibles y saludables, basándose en fundamentos científicos, para adoptarlos y promoverlos en su entorno.
 
Actualmente uno de los mayores y más importantes retos a los que se enfrenta la humanidad es la degradación medioambiental que amenaza con poner en peligro el desarrollo económico y la sociedad de bienestar. Una condición indispensable para abordar este desafío es adoptar un modelo de desarrollo sostenible. Para ello, es esencial que la ciudadanía comprenda su dependencia del medio natural para así valorar la importancia de su conservación y actuar de forma consecuente y comprometida con este objetivo. Cabe también destacar que la adopción de **estilos de vida** sostenibles es sinónimo de mantenimiento y mejora de la salud, pues existe un estrecho vínculo entre el bienestar humano y la conservación de los pilares sobre los que este se sustenta.
 
La adquisición y desarrollo de esta competencia específica permitirá al alumnado comprender, a través del conocimiento del funcionamiento de su propio organismo y de los ecosistemas, la relación entre la salud, la conservación del medio ambiente y el desarrollo económico **y social** y convertirse así en personas comprometidas y críticas con los problemas de su tiempo.
 
Esta competencia específica se conecta con los siguientes descriptores: CCL1, CCL2, STEM2, STEM4, CD2, CPSAA2, CC4, CEC1.

4. Aplicar el pensamiento científico y los razonamientos lógico-matemáticos, mediante la búsqueda y selección de estrategias y herramientas apropiadas, para resolver problemas relacionados con las ciencias experimentales.

El razonamiento es una herramienta esencial en la investigación científica, pues es necesario para plantear hipótesis o nuevas estrategias que permitan seguir avanzando y alcanzar los objetivos propuestos. Asimismo, en ciertas disciplinas científicas no es posible obtener evidencias directas de los procesos u objetos de estudio, por lo que se requiere utilizar el razonamiento lógico-matemático para poder conectar los resultados con la realidad que reflejan. Del mismo modo, es común encontrar escenarios de la vida cotidiana que requieren el uso de la lógica y el razonamiento.

La inclusión de esta competencia específica en el currículo de Ciencias Generales pretende que el alumnado aprenda que se puede llegar a los mismos resultados utilizando diferentes herramientas y estrategias, siempre y cuando sean fiables y estén contrastadas. Asimismo, se busca la consideración del error como una herramienta para descartar líneas de trabajo y una manera de aprender en la que se mejoran la autocrítica, la resiliencia y **las destrezas necesarias para** la colaboración entre iguales.

Cabe también destacar que la resolución de problemas es un proceso complejo donde se movilizan no solo las destrezas para el razonamiento, sino también los conocimientos sobre la materia y actitudes para afrontar los retos de forma positiva. Por ello, es imprescindible que el alumnado desarrolle esta competencia específica, pues le permitirá madurar intelectualmente y mejorar su resiliencia, para abordar con éxito diferentes tipos de situaciones a las que se enfrentará a lo largo de su vida personal, social, académica y profesional.

Esta competencia específica se conecta con los siguientes descriptores: CCL3, CP1, STEM1, STEM2, CD1, CPSAA1.1, CC3, CE1.

5. Analizar la contribución de la ciencia y de las personas que se dedican a ella, con perspectiva de género y entendiéndola como un proceso colectivo e interdisciplinar en continua construcción, para valorar su papel esencial en el progreso de la sociedad.

El desarrollo científico y tecnológico contribuye al progreso de nuestra sociedad. Sin embargo, el avance de la ciencia y la tecnología depende de la colaboración individual y colectiva. Por ello, el fin de esta competencia específica es formar una ciudadanía con un acervo científico rico y con vocación científica como vía para la mejora de nuestra calidad de vida.

A través de esta competencia específica, el alumnado adquiere conciencia sobre la relevancia que la ciencia tiene en la sociedad actual. Asimismo, reconoce el carácter interdisciplinar de la ciencia, marcado por una clara interdependencia entre las diferentes disciplinas de conocimiento que enriquece toda actividad científica y que se refleja en un desarrollo holístico de la investigación y el trabajo en ciencia.

Esta competencia específica se conecta con los siguientes descriptores: CCL1, CCL2, STEM4, CD3, CPSAA4, CC1, CEC1.

6. Utilizar recursos variados, con sentido crítico y ético, para buscar y seleccionar información contrastada y establecer colaboraciones.
 
La comunicación y la colaboración son componentes inherentes al proceso de avance científico. Parte de este proceso comunicativo implica buscar y seleccionar información científica publicada en fuentes fidedignas, que debe ser interpretada para responder a preguntas concretas y establecer conclusiones fundamentadas. Para ello, es necesario analizar la información obtenida de manera crítica, teniendo en cuenta su origen, diferenciando las fuentes adecuadas de aquellas menos fiables.
 
La cooperación es otro aspecto esencial de las metodologías científicas y tiene como objetivo mejorar la eficiencia del trabajo al aunar los esfuerzos de varias personas o equipos mediante el intercambio de información **y recursos**, consiguiéndose así un efecto sinérgico.
 
Además, desarrollar esta competencia específica es de gran utilidad en otros entornos profesionales no científicos, así como en el contexto personal y social, por ejemplo, en el aprendizaje a lo largo de la vida o en el ejercicio de una ciudadanía democrática activa. La comunicación y colaboración implican el despliegue de destrezas sociales, sentido crítico, respeto a la diversidad y, con frecuencia, utilización eficiente, ética y responsable de los recursos tecnológicos, por lo que esta competencia es esencial para el pleno desarrollo del alumnado como parte de la **sociedad**.
 
Esta competencia específica se conecta con los siguientes descriptores: CCL3, STEM3, STEM4, CD1, CD2, CD3, CPSAA4, CC3.

## Criterios de evaluación

Algunos cambios diciembre 2021 vs octubre 2021:  
En competencia específica 1 pasa de 4 puntos a 3 y la competencia específica 2 pasa de 3 a 4: el punto "1.3. Analizar y explicar fenómenos naturales representándolos mediante 
expresiones, tablas, gráficas, modelos, simulaciones, diagramas u otros 
formatos." pasa a ser "2.1. Analizar y explicar fenómenos del entorno, representándolos mediante
expresiones, tablas, gráficas, modelos, simulaciones, diagramas u otros
formatos."  
En 3.2 se añade "higiene, vacunación, uso adecuado de antibióticos"  
En 6.2 se elimina " relacionados con fenómenos y 
procesos físicos, químicos, biológicos o geológicos."   

### Competencia específica 1
1.1. Plantear y responder cuestiones acerca de procesos observados en el
entorno, siguiendo las pautas de las metodologías científicas.  
1.2. Contrastar hipótesis realizando experimentos en laboratorios o entornos
virtuales siguiendo las normas de seguridad correspondientes.  
1.3. Comunicar los resultados de un experimento o trabajo científico utilizando
los recursos adecuados y de acuerdo a los principios éticos básicos.  
### Competencia específica 2
2.1. Analizar y explicar fenómenos del entorno, representándolos mediante
expresiones, tablas, gráficas, modelos, simulaciones, diagramas u otros
formatos.  
2.2. Explicar fenómenos que ocurren en el entorno, utilizando principios, leyes y
teorías de las ciencias de la naturaleza.  
2.3. Reconocer y analizar los fenómenos fisicoquímicos más relevantes,
explicándolos a través de las principales leyes o teorías científicas.  
2.4. Explicar, utilizando los fundamentos científicos adecuados, los elementos y
procesos básicos de la biosfera y la geosfera.  
### Competencia específica 3
3.1. Adoptar y promover hábitos compatibles con un modelo de desarrollo
sostenible y valorar su importancia utilizando fundamentos científicos.  
3.2. Adoptar y promover hábitos saludables (dieta equilibrada, higiene,
vacunación, uso adecuado de antibióticos, rechazo al consumo de drogas,
ejercicio físico, higiene del sueño, posturas adecuadas...) valorar su importancia,
utilizando los fundamentos de la fisiología humana.  
### Competencia específica 4
4.1. Resolver problemas relacionados con fenómenos y procesos físicos,
químicos, biológicos y geológicos utilizando el pensamiento científico y el
razonamiento lógico-matemático, buscando estrategias alternativas de
resolución cuando sea necesario.  
4.2. Analizar críticamente la solución de un problema relacionado con fenómenos
y procesos físicos, químicos, biológicos y geológicos, modificando las
conclusiones o las estrategias utilizadas si la solución no es viable, o ante nuevos
datos aportados.  
### Competencia específica 5
5.1. Reconocer la ciencia como un área de conocimiento global analizando la
interrelación e interdependencia entre cada una de las disciplinas que la forman.  
5.2. Reconocer la relevancia de la ciencia en el progreso de la sociedad,
valorando el importante papel que juegan las personas en el desempeño de la
investigación científica.  
### Competencia específica 6
6.1. Buscar, contrastar y seleccionar información sobre fenómenos y procesos
físicos, químicos, biológicos o geológicos en diferentes formatos, utilizando los
recursos necesarios, tecnológicos o de otro tipo.  
6.2. Establecer colaboraciones, utilizando los recursos necesarios en las
diferentes etapas del proyecto científico, en la realización de actividades o en la
resolución de problemas.   

## Saberes básicos 

**A. Construyendo ciencia**  
- Metodologías propias de la investigación científica para la identificación y
formulación de cuestiones, la elaboración de hipótesis y la comprobación
experimental de las mismas.  
- Experimentos y de proyectos de investigación: uso de instrumental
adecuado, controles experimentales y razonamiento lógico-matemático.
Métodos de análisis de los resultados obtenidos en la resolución de
problemas y cuestiones científicas relacionados con el entorno.  
- Fuentes veraces y medios de colaboración: búsqueda de información
científica en diferentes formatos y con herramientas adecuadas.  
- Información científica: interpretación y producción de con un lenguaje
adecuado. Desarrollo del criterio propio basado en la evidencia y el
razonamiento.  
- Contribución de los científicos y las científicas a los principales hitos de la
ciencia para el avance y la mejora de la sociedad.  

**B. Un universo de materia y energía**  
- Sistemas materiales macroscópicos: uso de modelos microscópicos para
analizar sus propiedades y de sus estados de agregación, así como de los
procesos físicos y químicos de cambio.  
- Clasificación de los sistemas materiales en función de su composición:
aplicación a la descripción de los sistemas naturales y a la resolución de
problemas relacionados.  
- La estructura interna de la materia y su relación con las regularidades que se
producen en la tabla periódica. Reconocimiento de su importancia histórica y
actual.  
- Formación de compuestos químicos: la nomenclatura como base de una
alfabetización científica básica que permita establecer una comunicación
eficiente con toda la comunidad científica.  
- Transformaciones químicas de los sistemas materiales y leyes que los rigen:
importancia en los procesos industriales, medioambientales y sociales del
mundo actual.  
- Energía contenida en un sistema, sus propiedades y sus manifestaciones:
teorema de conservación de la energía mecánica y procesos termodinámicos
más relevantes. Resolución de problemas relacionados con el consumo
energético y la necesidad de un desarrollo sostenible.  

**C. El sistema Tierra**  
- El origen del universo, del sistema solar y de la Tierra: relación con sus
características.  
- El origen de la vida en la Tierra: hipótesis destacadas. La posibilidad de vida
en otros planetas.  
- Concepto de ecosistema: relación componentes bióticos y abióticos.
- La geosfera: estructura, dinámica, procesos geológicos internos y externos.
La teoría de la tectónica de placas.  
- Las capas fluidas de la Tierra: funciones, dinámica, interacción con la
superficie terrestre y los seres vivos en la edafogénesis.  
- Los seres vivos como componentes bióticos del ecosistema: clasificación,
características y adaptaciones al medio.  
- Dinámica de los ecosistemas: flujos de energía, ciclos de la materia y
relaciones tróficas. Resolución de problemas relacionados.  
- Principales problemas medioambientales (calentamiento global, agujero de
la capa de ozono, destrucción de los espacios naturales, pérdida de la
biodiversidad...) y riesgos geológicos: causas y consecuencias.  
- El modelo de desarrollo sostenible: importancia. Recursos renovables y no
renovables: importancia de su uso y explotación responsables. Las energías
renovables. La gestión de residuos. La economía circular.  
- La relación entre la conservación medioambiental, la salud humana y el
desarrollo económico de la sociedad.  
- Las enfermedades infecciosas y no infecciosas: causas, prevención y
tratamiento. Las zoonosis y las pandemias. El mecanismo y la importancia de
las vacunas y del uso adecuado de los antibióticos.  

**D. Biología para el siglo XXI**  
- Las principales biomoléculas (glúcidos, lípidos, proteínas y ácidos nucleicos):
estructura básica y relación con sus funciones e importancia biológica.  
- Expresión de la información genética: procesos implicados. Características
del código genético y relación con su función biológica.  
- Técnicas de ingeniería genética: PCR, enzimas de restricción, clonación
molecular y CRISPR-CAS9. Posibilidades de la manipulación dirigida del
ADN.  
- Aplicaciones de la biotecnología: agricultura, ganadería, medicina o
recuperación medioambiental. Importancia biotecnológica de los
microorganismos.  
- La transmisión genética de caracteres: resolución de problemas y análisis de
la probabilidad de herencia de alelos o de la manifestación de fenotipos.  

**E. Las fuerzas que nos mueven**  
- Fuerzas fundamentales de la naturaleza: los procesos físicos más relevantes
del entorno natural, como los fenómenos electromagnéticos, el movimiento
de los planetas o los procesos nucleares.  
- Leyes de la estática: estructuras en relación con la física, la biología, la
geología o la ingeniería.  
- Leyes de la mecánica relacionadas con el movimiento: comportamiento de
un objeto móvil y sus aplicaciones, por ejemplo, en la seguridad vial o en el
desarrollo tecnológico.  

## Madrid (Decreto)

Revisado con [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF)  
Ciencias Generales 2º Bachillerato en página 49 del pdf  

Cambios en [Proyecto de decreto 31 mayo](https://www.comunidad.madrid/transparencia/sites/default/files/04_2_2022-05-31_decreto_bachillerato-a.pdf)  
En decreto Madrid texto inicial cambia bastante de redacción. Se añade párrafo final con situación de aprendizaje.  
Se elimina "Además, incluye aspectos clave encaminados a la concienciación del alumnado sobre la necesidad de adoptar un modelo de desarrollo sostenible y la promoción de la salud."  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto
Se añade por error "como **valores tales como** el respeto"  
Se cambia varias veces "situación de aprendizaje" por "actividad"  
Se cambia por error "contribuiría a desarrolla las" por "contribuiría a **desarrollas** las"  


En la sociedad actual es necesario poseer una formación científica sólida que permita a cada individuo tener una opinión fundamentada ante hechos que pueden resultar controvertidos y que forman parte del día a día de nuestro mundo, ya que multitud de aspectos están relacionados con la actividad científica, tanto en campos sanitarios como tecnológicos o divulgativos.  
La materia de Ciencias Generales ofrece al alumnado una formación básica en las cuatro disciplinas científicas fundamentales para que adquieran la base suficiente para comprender los principios generales que rigen los fenómenos del mundo natural. Además, el enfoque interdisciplinar característico de la enseñanza STEM confiere al currículo un carácter unificador que pone en evidencia que las diferentes ciencias no son más que una especialización dentro de un conjunto global y coherente que es el conocimiento científico. A esta materia podrán acceder diferentes perfiles de estudiantes, con distintas formaciones previas en ciencias, por lo que la adquisición de los aprendizajes esenciales de esta materia se construye a partir de las ciencias básicas que todo alumno ha cursado durante la Educación Secundaria Obligatoria, profundizando a partir de ahí para alcanzar las competencias y los objetivos propios de la etapa del Bachillerato, desarrollándose para ello la materia en cinco bloques de contenidos.  
En el primer bloque, «Construyendo ciencia», se tratan los aspectos básicos de la actividad científica general: el uso de las metodologías científicas para el estudio de fenómenos naturales, la experimentación, incluyendo los instrumentos necesarios y sus normas de uso, la utilización adecuada del lenguaje científico y de las herramientas matemáticas pertinentes, etc. Se trata de un bloque introductorio que, lejos de pretender ser tratado de manera teórica, busca desarrollar destrezas prácticas útiles para el resto de los bloques.  
En el segundo bloque, «Un universo de materia y energía», se describen dos conceptos fundamentales de la ciencia: la materia y la energía. Conocer y utilizar estos conceptos con soltura es fundamental para todos los ámbitos de estudio y trabajo de la ciencia, pues es la base sobre la que construir los conocimientos de los sistemas fisicoquímicos, biológicos y geológicos.  
En el bloque «El sistema Tierra», se hace una aproximación al estudio de la Tierra y los sistemas terrestres desde el punto de vista de la geología planetaria, de la tectónica de placas y de la dinámica de las capas fluidas.  
En el bloque «Biología para el siglo XXI», se tratan algunas cuestiones sobre la biotecnología y su importancia en la investigación de enfermedades, técnicas de agricultura y ganadería o recuperación medioambiental, entre otras.  
Por último, en el bloque «Las fuerzas que **rigen el universo y las fuerzas que** nos mueven», se analizan las fuerzas fundamentales de la naturaleza y los efectos que tienen sobre los sistemas. Se trata de contenidos que permiten dar explicaciones a aspectos tan importantes como el movimiento de los cuerpos o las deformaciones de la corteza terrestre.  
En definitiva, el currículo de Ciencias Generales no solo pretende concienciar sobre la importancia de las ciencias, y crear vocaciones científicas y formadores científicos que tengan un criterio propio y fundamentado para la difusión de ideas, sino que proporcionará al alumnado que desee explorar otros campos profesionales no vinculados directamente con las ciencias; conocimientos y aprendizajes propios de las mismas que permitan un enfoque riguroso y certero en su labor profesional, al tiempo que favorecerá actitudes positivas hacia la ciencia y su aprendizaje.  
La metodología adecuada para trabajar esta materia se basará en las herramientas y fundamentos que proporciona este currículo para el desarrollo de proyectos y la cooperación interdisciplinar, propios de la investigación científica. De este modo se trabajarán valores como **valores tales como** el respeto, el trabajo en equipo, el rechazo hacia actitudes que muestren cualquier tipo de discriminación y el compromiso con el entorno.  
**Como orientación para la práctica docente se aporta la siguiente actividad: los alumnos, en grupos, podrían investigar los planetas rocosos del sistema solar, realizar una geología comparada de los mismos y establecer con criterios científicos posibilidades futuras de viajes interplanetarios. A partir de unos objetivos claros y precisos, el alumnado deberá movilizar la mayor cantidad posible de conocimientos de la materia y relacionar los contenidos de los bloques de «Las fuerzas que rigen el universo y las fuerzas que nos mueven», «El sistema Tierra» y «Biología para el siglo XXI». Tendrán que investigar en fuentes originales en diversos idiomas, desarrollar ejemplos de futuros escenarios viables y buscar obras escritas y películas que traten sobre esta temática. La actividad planteada perseguiría como resultado final, la elaboración de un producto en forma de informe escrito, investigación u obra audiovisual, y contribuiría a desarrollas las competencias específicas 1, 2, 4 y 6 de la materia.**  
 
## Competencias Específicas
Comparación proyecto Decreto Madrid frente a Real Decreto:  
En 1 se cambia "alumnos y alumnas" por "alumnos"  
En 3 se cambia "con poner en peligro el desarrollo económico y la sociedad de bienestar" por "el futuro de nuestra sociedad", se cambia "desarrollo sostenible" por "desarrollo perdurable", se cambia "la ciudadanía" por "los alumnos", se añade "promoción" y "así como la prevención de la enfermedad"  
En 5 se elimina "con perspectiva de género", se elimina "colectivo", se elimina "para valorar su papel esencial en el progreso de la sociedad.", se elimina "Sin embargo, el avance de la ciencia y la tecnología depende de la colaboración individual y colectiva.", se elimina "como vía para la mejora de nuestra calidad de vida." y se cambia "una ciudadanía" por "alumnos"   
En 6 se elimina párrafo "La cooperación es otro aspecto esencial de las metodologías científicas y tiene como objetivo mejorar la eficiencia del trabajo al aunar los esfuerzos de varias personas o equipos mediante el intercambio de información y recursos, consiguiéndose así un efecto sinérgico.", se elimina "o en el ejercicio de una ciudadanía democrática activa" y se elimina "por lo que esta competencia es esencial para el pleno desarrollo del alumnado como parte de la sociedad."   

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
En 3 se cambia "estilos de vida sostenibles" por "estilos de vida adecuados"  
En 5 se cambia título que pasa de "proceso interdisciplinar en continua construcción." a "proceso interdisciplinar en equipo y en continua construcción, para valorar su papel esencial en el progreso de la sociedad.", recuperando redacción de Real Decreto.  


1. Aplicar las metodologías propias de la ciencia, utilizando con precisión, procedimientos, materiales e instrumentos adecuados, para responder a cuestiones sobre procesos físicos, químicos, biológicos y geológicos.  
Para conseguir una alfabetización científica básica, cada alumno debe comprender cuál es el modus operandi de toda la comunidad científica en lo referente al estudio de los fenómenos naturales y cuáles son las herramientas de que se dispone para ello. Las metodologías científicas son procedimientos fundamentales de trabajo en la ciencia. El alumnado debe desarrollar las destrezas de observar, emitir hipótesis y experimentar sobre fenómenos fisicoquímicos y naturales, así como de poner en común con el resto de la comunidad investigadora los resultados que obtenga, siendo consciente de que las respuestas a procesos, físicos, químicos, biológicos y geológicos son complejas y necesitan de modelos contrastados y en constante revisión y validación.  
Asimismo, aunque el alumnado no optase en el futuro por dedicarse a la ciencia como actividad profesional, el desarrollo de esta competencia le otorga algunas destrezas propias del pensamiento científico que puede aplicar en situaciones de su vida cotidiana, como la interpretación de fenómenos o el respeto por el mundo natural que le rodea. Esto contribuye a la formación de personas comprometidas con la mejora de su entorno y de la sociedad.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: CCL3, STEM1, STEM2, STEM3, CD1, CD3, CPSAA4, CE1.  

2. Comprender y explicar los procesos del entorno y explicarlos, utilizando los principios, leyes y teorías científicos adecuados, para adquirir una visión holística del funcionamiento del medio natural.  
El desarrollo de la competencia científica tiene como finalidad esencial comprender los procesos del entorno e interpretarlos a la luz de los principios, leyes y teorías científicas fundamentales. Con el desarrollo de esta competencia específica también se contribuye a desarrollar el pensamiento científico, lo cual es clave para la creación de nuevos conocimientos.  
Además, la aplicación de los conocimientos está en línea con los principios del aprendizaje STEM, que pretende adoptar un enfoque global de las ciencias como un todo integrado. El alumnado que cursa esta materia aprende a relacionar conceptos, encontrando en ella los conocimientos, destrezas y actitudes necesarios para una alfabetización científica general.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: CCL1, CCL2, CP1, STEM1, STEM2, STEM4, CD1, CPSAA1.1.  

3. Argumentar sobre la importancia de los estilos de vida adecuados y saludables, basándose en fundamentos científicos, para adoptarlos y promoverlos en su entorno.  
Actualmente uno de los mayores y más importantes retos a los que se enfrenta la humanidad es la degradación medioambiental que amenaza **el futuro de nuestra sociedad**. Una condición indispensable para abordar este desafío es adoptar un modelo de desarrollo **perdurable**. Para ello, es esencial que **los alumnos** comprendan su dependencia del medio natural para así valorar la importancia de su conservación y actuar de forma consecuente y comprometida con este objetivo.  
Cabe también destacar que la adopción de estilos de vida sostenibles es sinónimo de mantenimiento, **promoción** y mejora de la salud, **así como la prevención de la enfermedad**, pues existe un estrecho vínculo entre el bienestar humano y la conservación de los pilares sobre los que este se sustenta.  
La adquisición y desarrollo de esta competencia específica permitirá al alumnado comprender, a través del conocimiento del funcionamiento de su propio organismo y de los ecosistemas, la relación entre la salud, la conservación del medio ambiente y el desarrollo económico y social y convertirse así en personas comprometidas y críticas con los problemas de su tiempo.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: CCL1, CCL2, STEM2, STEM4, CD2, CPSAA2, CC4, CEC1.

4. Aplicar el pensamiento científico y los razonamientos lógico-matemáticos, mediante la búsqueda y selección de estrategias y herramientas apropiadas, para resolver problemas relacionados con las ciencias experimentales.  
El razonamiento es una herramienta esencial en la investigación científica, pues es necesario para plantear hipótesis o nuevas estrategias que permitan seguir avanzando y alcanzar los objetivos propuestos. Asimismo, en ciertas disciplinas científicas no es posible obtener evidencias directas de los procesos u objetos de estudio, por lo que se requiere utilizar el razonamiento lógico-matemático para poder conectar los resultados con la realidad que reflejan. Del mismo modo, es común encontrar escenarios de la vida cotidiana que requieren el uso de la lógica y el razonamiento.  
La inclusión de esta competencia específica en el currículo de Ciencias Generales pretende que el alumnado aprenda que se puede llegar a los mismos resultados utilizando diferentes herramientas y estrategias, siempre y cuando sean fiables y estén contrastadas. Asimismo, se busca la consideración del error como una herramienta para descartar líneas de trabajo y una manera de aprender en la que se mejoran la autocrítica, la resiliencia y las destrezas necesarias para la colaboración entre iguales.  
Cabe también destacar que la resolución de problemas es un proceso complejo donde se movilizan no solo las destrezas para el razonamiento, sino también los conocimientos sobre la materia y actitudes para afrontar los retos de forma positiva. Por ello, es imprescindible que el
alumnado desarrolle esta competencia específica, pues le permitirá madurar intelectualmente y mejorar su resiliencia, para abordar con éxito diferentes tipos de situaciones a las que se enfrentará a lo largo de su vida personal, social, académica y profesional.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: CCL3, CP1, STEM1, STEM2, CD1, CPSAA1.1, CC3, CE1.

5. Analizar la contribución de la ciencia y de las personas que se dedican a ella, entendiéndola como un proceso interdisciplinar en continua construcción.  
El desarrollo científico y tecnológico contribuye **positivamente** al progreso de nuestra sociedad. Por ello, el fin de esta competencia específica es formar alumnos con un acervo científico rico y con vocación científica.  
A través de esta competencia específica, el alumnado adquiere conciencia sobre la relevancia que la ciencia tiene en la sociedad actual. Asimismo, reconoce el carácter interdisciplinar de la ciencia, marcado por una clara interdependencia entre las diferentes disciplinas de conocimiento que enriquece toda actividad científica y que se refleja en un desarrollo holístico de la investigación y el trabajo en ciencia.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: CCL1, CCL2, STEM4, CD3, CPSAA4, CC1, CEC1.

6. Utilizar recursos variados, con sentido crítico y ético, para buscar y seleccionar información contrastada y establecer colaboraciones.  
La comunicación y la colaboración son componentes inherentes al proceso de avance científico. Parte de este proceso comunicativo implica buscar y seleccionar información científica publicada en fuentes fidedignas, que debe ser interpretada para responder a preguntas concretas y establecer conclusiones fundamentadas. Para ello, es necesario analizar la información obtenida de manera crítica, teniendo en cuenta su origen, diferenciando las fuentes adecuadas de aquellas menos fiables.  
Además, desarrollar esta competencia específica es de gran utilidad en otros entornos profesionales no científicos, así como en el contexto personal y social, por ejemplo, en el aprendizaje a lo largo de la vida. La comunicación y colaboración implican el despliegue de destrezas sociales, sentido crítico, respeto a la diversidad y, con frecuencia, utilización eficiente, ética y responsable de los recursos tecnológicos.  
Esta competencia específica se conecta con los siguientes descriptores **recogidos en el anexo I del Real Decreto 243/2022, de 5 de abril**: CCL3, STEM3, STEM4, CD1, CD2, CD3, CPSAA4, CC3.

## Criterios de evaluación

Comparación proyecto Decreto Madrid frente a Real Decreto:   
En 3.1 se elimina "sostenible"  
En 3.2 se cambia " rechazo al consumo de drogas, legales e ilegales, " eliminando "legales e ilegales"  

Sin cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 


### Competencia específica 1.
1.1. Plantear y responder cuestiones acerca de procesos observados en el entorno, siguiendo las pautas de las metodologías científicas.  
1.2. Contrastar hipótesis realizando experimentos en laboratorios o en entornos virtuales siguiendo las normas de seguridad correspondientes.  
1.3. Comunicar los resultados de un experimento o trabajo científico, utilizando los recursos adecuados y de acuerdo a los principios éticos básicos.  
### Competencia específica 2.
2.1. Analizar y explicar fenómenos del entorno, representándolos mediante expresiones, tablas, gráficas, modelos, simulaciones, diagramas u otros formatos.  
2.2. Explicar fenómenos que ocurren en el entorno, utilizando principios, leyes y teorías de las ciencias de la naturaleza.  
2.3. Reconocer y analizar los fenómenos fisicoquímicos más relevantes, explicándolos a través de las principales leyes o teorías científicas.  
2.4. Explicar, utilizando los fundamentos científicos adecuados, los elementos y procesos básicos de la biosfera y la geosfera.  
### Competencia específica 3.
3.1. Adoptar y promover hábitos compatibles con un modelo de desarrollo y valorar su importancia utilizando fundamentos científicos.  
3.2. Adoptar y promover hábitos saludables (dieta equilibrada, higiene, vacunación, uso adecuado de antibióticos, rechazo al consumo de drogas, ejercicio físico, higiene del sueño, posturas adecuadas…) y valorar su importancia, utilizando los fundamentos de la fisiología humana.  
### Competencia específica 4.
4.1. Resolver problemas relacionados con fenómenos y procesos físicos, químicos, biológicos y geológicos, utilizando el pensamiento científico y el razonamiento lógico-matemático y buscando estrategias alternativas de resolución cuando sea necesario.  
4.2. Analizar críticamente la solución de un problema relacionado con fenómenos y procesos físicos, químicos, biológicos y geológicos, modificando las conclusiones o las estrategias utilizadas si la solución no es viable, o ante nuevos datos aportados.  
### Competencia específica 5.
5.1. Reconocer la ciencia como un área de conocimiento global, analizando la interrelación e interdependencia entre cada una de las disciplinas que la forman.  
5.2. Reconocer la relevancia de la ciencia en el progreso de la sociedad, valorando el importante papel que juegan las personas en el desempeño de la investigación científica.  
### Competencia específica 6.
6.1. Buscar, contrastar y seleccionar información sobre fenómenos y procesos físicos, químicos, biológicos o geológicos en diferentes formatos, utilizando los recursos necesarios, tecnológicos o de otro tipo.  
6.2. Establecer colaboraciones, utilizando los recursos necesarios en las diferentes etapas del proyecto científico, en la realización de actividades o en la resolución de problemas.  

## Contenidos
Comparación proyecto Decreto Madrid frente a Real Decreto:   
En Madrid no se llaman saberes básicos  
Se añaden contenidos en rojo, aquí se marcan en negrita  
En bloque A se cambia "científicos y científicas" por "científicos"  
Se bloque C elimina "importancia de su uso y explotación responsables."  

Cambios en [Decreto 64/2022, de 20 de julio, del Consejo de Gobierno, por el que se establecen para la Comunidad de Madrid la ordenación y el currículo del Bachillerato](https://www.bocm.es/boletin/CM_Orden_BOCM/2022/07/26/BOCM-20220726-1.PDF) vs proyecto 
(Mantengo la negrita para indicar las diferencias respecto a Real Decreto estatal)  
En A se eliminan tres líneas añadidas en proyecto:
"- **Digitalización del conocimiento.**  
 **Nuevas herramientas para el conocimiento. Internet.**  
 **Problemas por las nuevas tecnologías. Delitos informáticos, protección de datos personales, etc.**"  
En C se cambia "Formulación y nomenclatura" por "nomenclatura" (lo puse en alegaciones)  


A. Construyendo ciencia.  
- Metodologías propias de la investigación científica para la identificación y formulación de cuestiones, la elaboración de hipótesis y la comprobación experimental de las mismas.  
 **Actitudes en el trabajo científico: cuestionamiento de lo obvio, necesidad de comprobación, de rigor y de precisión, apertura ante nuevas ideas.**  
- Experimentos y proyectos de investigación: uso de instrumental adecuado, controles experimentales y razonamiento lógico-matemático. Métodos de análisis de los resultados obtenidos en la resolución de cuestiones y problemas científicos relacionados con el entorno.  
 **Tipos de variables. Correlación y causalidad.**  
 **Clasificación, interpretación y comparación de resultados.**  
 **Información cuantitativa y cualitativa. Fundamentos de estadística para el tratamiento de datos.**  
- Fuentes veraces y medios de colaboración: búsqueda de información científica en diferentes formatos y con herramientas adecuadas.  
 **Técnicas de búsqueda y selección de información (autoría, propósito, objetividad, actualización, etc.)**  
- Información científica: interpretación y producción **de informes y trabajos** con un lenguaje adecuado. Desarrollo del criterio propio basado en la evidencia y el razonamiento.
 **Técnicas y herramientas de apoyo para la exposición y defensa en público de trabajos e investigaciones.**  
- Contribución de los científicos a los principales hitos de la ciencia para el avance y la mejora de la sociedad.  
 **Valoración del papel de los grandes científicos en el desarrollo de la ciencia estableciendo su contexto histórico.**  

B. Un universo de materia y energía.  
- Clasificación de los sistemas materiales en función de su composición: aplicación a la descripción de los sistemas naturales y a la resolución de problemas relacionados **con su composición.**  
- Sistemas materiales macroscópicos: uso de modelos microscópicos para analizar sus propiedades y sus estados de agregación, así como los procesos físicos y químicos de cambio.  
 **Modelo cinético-molecular de la materia.**  
- La estructura interna de la materia y su relación con las regularidades que se producen en la tabla periódica. Reconocimiento de su importancia histórica y actual.  
 **Evolución histórica de la tabla periódica hasta la actualidad.**  
 **Estructura atómica de la materia. Números atómicos. Isótopos.**  
 **Números cuánticos. Configuración electrónica y sistema periódico.**  
 **Propiedades periódicas: radio atómico, radio iónico, energía de ionización, afinidad electrónica, electronegatividad.**  
- Formación de compuestos químicos: la nomenclatura como base de una alfabetización científica básica que permita establecer una comunicación eficiente con toda la comunidad científica.  
 **El enlace químico.**  
 **El enlace covalente: estructuras de Lewis y modelo de teoría de repulsión de pares electrónicos de la capa de valencia (RPECV). Geometría molecular. Enlaces intermoleculares. Sustancias covalentes moleculares y cristalinas. Propiedades de las sustancias covalentes.**  
 **El enlace iónico. Cristales iónicos. Propiedades de los compuestos iónicos.**  
 **El enlace metálico. Propiedades de las sustancias con enlace metálico.**  
 **Formulación y nomenclatura de sustancias simples, iones y compuestos químicos inorgánicos mediante las reglas de la IUPAC.**  
- Transformaciones químicas de los sistemas materiales y leyes que los rigen: importancia en los procesos industriales, medioambientales y sociales del mundo actual.  
 **Ajuste de reacciones químicas.**  
 **Leyes ponderales.**  
 **Ley general de los gases.**  
 **Concepto de mol. Constante de Avogadro.**  
 **Disoluciones. Cálculo de la concentración de una disolución.**  
 **Cálculos estequiométricos.**  
 **Importancia de la industria química en la sociedad actual.**  
- Energía contenida en un sistema, sus propiedades y sus manifestaciones: teorema de conservación de la energía mecánica y procesos termodinámicos más relevantes. Resolución de problemas relacionados con el consumo energético, **la eficiencia energética** y la necesidad de un desarrollo sostenible.  
 **Energía cinética y energía potencial.**  
 **Energía mecánica. Principio de conservación de la energía mecánica.**  
 **Trabajo y potencia.**  

C. El sistema Tierra.  
- El origen del universo, del sistema solar y de la Tierra: relación con sus características.  
 Forma y movimientos de la Tierra y la Luna y sus efectos.  
- El origen de la vida en la Tierra: hipótesis destacadas. La posibilidad de vida en otros planetas.  
- La geosfera: estructura, dinámica, procesos geológicos internos y externos. La teoría de la tectónica de placas. Riesgos geológicos.  
 **Estructura y naturaleza físico-química del interior de la Tierra. Diferentes métodos de estudio e interpretación de los datos.**  
 **Manifestaciones de la dinámica litosférica: deformaciones, metamorfismo y magmatismo.**  
- Las capas fluidas de la Tierra: funciones, dinámica, interacción con la superficie terrestre y los seres vivos en la edafogénesis.  
- Concepto de ecosistema: relación componentes bióticos y abióticos.  
 **Hábitat y nicho ecológico.**  
 **Factores limitantes y adaptaciones. Límite de tolerancia.**  
- Los seres vivos como componentes bióticos del ecosistema: clasificación, características y adaptaciones al medio.  
- Dinámica de los ecosistemas: flujos de energía, ciclos de la materia, interdependencia y relaciones tróficas. Resolución de problemas relacionados.  
 **Relaciones tróficas: cadenas y redes. Ciclo de materia y flujo de energía. Pirámides ecológicas.**  
 **Sucesiones ecológicas.**  
- Principales problemas medioambientales (calentamiento global, agujero de la capa de ozono, destrucción de los espacios naturales, pérdida de la biodiversidad, contaminación del aire y el agua, desertificación…) y riesgos geológicos: causas y consecuencias.  
- El modelo de desarrollo sostenible: Recursos renovables y no renovables. Las energías renovables. La prevención y gestión de residuos. La economía circular.  
- La relación entre la conservación medioambiental, la salud humana y el desarrollo económico de la sociedad. Concepto one health (una sola salud).  
 **Sostenibilidad. Concepto de huella de carbono.**  
- Las enfermedades infecciosas **(transmisibles)** y no infecciosas **(no transmisibles)**: causas, prevención y tratamiento. Las zoonosis y las pandemias. El mecanismo y la importancia de las vacunas y del uso adecuado de los antibióticos.  
 **Enfermedades nutricionales.**  
 **El uso racional de los medicamentos.**  
 **Inmunidad natural y artificial o adquirida. Sueros y vacunas. Su importancia en la lucha contra las enfermedades infecciosas.**  

D. Biología para el siglo XXI.  
- Las principales biomoléculas (glúcidos, lípidos, proteínas y ácidos nucleicos): estructura básica y relación con sus funciones e importancia biológica.  
- Expresión de la información genética: procesos implicados. Características del código genético y relación con su función biológica.  
- Técnicas de ingeniería genética: PCR, enzimas de restricción, clonación molecular y CRISPR-CAS9. Posibilidades de la manipulación dirigida del ADN.  
 **Terapias génicas. Aplicaciones en enfermedades humanas**  
 **Organismos transgénicos.**  
 **Aspectos sociales relacionados con la ingeniería genética. La clonación.**  
- **La reproducción asistida, selección y conservación de embriones.**  
- Aplicaciones y repercusiones de la biotecnología: agricultura, ganadería, medicina o recuperación medioambiental. Importancia biotecnológica de los microorganismos.  
 **Aplicaciones en la producción agrícola y animal y en la industria.**  
 **Organismos modificados genéticamente.**  
- La transmisión genética de caracteres: resolución de problemas y análisis de la probabilidad de herencia de alelos o de la manifestación de fenotipos.  
- **Evidencias del proceso evolutivo. Teoría sintética de la evolución. Teorías actuales de la evolución.**  
 **Evolución humana.**  

E. Las fuerzas que rigen el universo y las fuerzas que nos mueven.  
- Leyes de la mecánica relacionadas con el movimiento: comportamiento de un objeto móvil y sus aplicaciones, por ejemplo, en la seguridad vial, **las actividades deportivas** o en el desarrollo tecnológico.  
 **Variables cinemáticas: posición, desplazamiento, velocidad media e instantánea, aceleración, componentes intrínsecas de la aceleración. Carácter vectorial de estas magnitudes.**  
 **Estudio de los movimientos rectilíneo y uniforme, rectilíneo uniformemente acelerado, circular uniforme y circular uniformemente variado.**  
 **Principio de superposición y composición de movimientos: tiro horizontal y tiro parabólico.**  
- Leyes de la estática: **estudio de** estructuras en **equilibrio en** relación con la física, la biología, la geología o la ingeniería.  
 **Fuerzas en equilibrio.**  
 **Esfuerzos en los materiales técnicos y estructuras: compresión, tracción, cortante, flexión y torsión.**  
- Fuerzas fundamentales de la naturaleza: los procesos físicos más relevantes del entorno natural, como los fenómenos electromagnéticos, el movimiento de los planetas o los procesos nucleares.  
 **Ley de Gravitación Universal.**  
 **Estudio del campo eléctrico y magnético.**  
 **Reacciones nucleares.**  
 **Aplicaciones en la generación de energía, el uso de los satélites meteorológicos y de telecomunicaciones, la utilización de los radioisótopos en medicina y las aplicaciones de los dispositivos eléctricos y electrónicos.**  

