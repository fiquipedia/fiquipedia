
# Pruebas
 
* **[Pruebas Acceso Universidad](/home/pruebas/pruebasaccesouniversidad)**   
* **[Pruebas Acceso FP Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio)**  
* **[Pruebas Acceso FP Grado Superior](/home/pruebas/pruebas-de-acceso-a-grado-superior)**
* **[Pruebas Libres Graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)**  
* Pruebas Libres Título Bachiller (pendiente, de momento información junto a Pruebas Libres Graduado ESO)  
* [Pruebas Libres Título FP (Grado Medio, Grado Superior)](/home/pruebas/pruebas-libres-fp)  
* [Pruebas premios extraordinarios](/home/pruebas/pruebas-premios-extraordinarios) (de momento juntos Premios Extraordinarios Educación Secundaria Obligatoria y Premios Extraordinarios Bachillerato)  
* [Pruebas de evaluaciones internacionales](/home/pruebas/pruebas-evaluaciones-internacionales)   
* [Pruebas de evaluaciones nacionales](/home/pruebas/pruebas-evaluaciones-nacionales)  

También se pueden considerar pruebas y se enlazan desde aquí los prácticos de oposiciones
* [Pruebas práctico oposiciones Física y Química](https://www.fiquipedia.es/home/recursos/recursos-para-oposiciones/recursos-para-oposiciones-problemas-por-comunidad/)  

Relacionado con LOMCE, puede haber información sobre pruebas en   
* [LOMCE, Evaluaciones Finales ESO](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-eso)   
* [LOMCE, Evaluaciones Finales Bachillerato](/home/legislacion-educativa/lomce/evaluaciones-finales/evaluaciones-finales-bachillerato)   

Relacionado con LOMLOE, puede haber información sobre pruebas en 
* [LOMLOE, Acceso universidad](/home/legislacion-educativa/lomloe/acceso-universidad)   

También podrían ser útiles ejemplos de pruebas de Bachillerato Internacional, aunque en este caso suelen indicar que tienen derechos de autor.  
[Categoría Física IB - fisicazone.com](https://fisicazone.com/category/bachillerato-internacional/fisica-ib/)   
