# Recursos Física y Química 1º Bachillerato

Los recursos podrán estar como  [apuntes](/home/recursos/apuntes) ,  [ejercicios](/home/recursos/ejercicios) , asociados a enlaces dentro de la  [materia](/home/materias)  y su currículo ...  
En 2013 comienzo a poner algunos enlaces en el  [currículo de Física y Química de 1º de Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculofisicayquimica-1bachillerato) , aunque está pendiente reorganizar y completar  

En 2016 creo una **[página de recursos Física y Química 1º Bachillerato elaborados por mí](/home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato)** donde hay tabla resumen.  
En 2018 creo las  [pizarras de Física y Química por nivel](/home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel/pizarras-fisica-y-quimica-por-nivel)  y en 2019 completo las asociadas a Física y Química de 1º Bachillerato, creando ejercicios en varios apartados:  [cinemática](/home/recursos/fisica/cinematica) ,  [dinámica](/home/recursos/fisica/dinamica) ,  [energía](/home/recursos/energia).  

##  Apuntes
 
* [Apuntes 1º Bachillerato - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/1bach/)  Rodrigo Alcaraz de la Osa  cc-by-sa
* [Apuntes 1º Bachillerato - fisquiweb](http://fisquiweb.es/Apuntes/apun1B.htm) Luis Ignacio García González   
* [Didactica Física y Química](http://didacticafisicaquimica.es/)   
 [Física y Química de 1º de Bachillerato](http://didacticafisicaquimica.es/fisica-y-quimica-1-bachillerato/)   
 [Física y Química de 1º de Bachillerato – LOMCE](http://didacticafisicaquimica.es/?p=816&preview=true)   
 [Enunciados de problemas 1º Bachillerato](http://didacticafisicaquimica.es/enunciados-de-problemas-1o-bachiller/)  
* [1ºBach FyQ - profesorjrc.es](https://profesorjrc.es/1fyq.htm) Jorge Rojo Carrascosa   
* [1ºBachillerato FyQ - quifiblogeso](http://quifiblogeso.blogspot.com/search/label/1%C2%BA%20bachillerato) Inma Rodríguez   
* [Física Bachillerato / Química Bachillerato - elortegui.org](https://www.elortegui.org/ciencia)  
* [1ºBachillerato FyQ - mainquifi](https://mainquifi.blogspot.com/p/1-bachillerato.html)  
[Libro de FYQ 1ºBACHILLERATO ADAPTADO A LA LOMLOE GRATIS (pdf, 128 MB, 245 páginas - mainquifi](https://drive.google.com/file/d/1doHOFODieerFd1s-edAEjmSA9BtFu05i/view?usp=drivesdk)  
* [Temas FyQ 1º Bachillerato. Fernando Escudero Ramos. IES Fernando de los Ríos, Quintanar del Rey (Cuenca)](http://ies-fernandorios.centros.castillalamancha.es/content/f%C3%ADsica-y-qu%C3%ADmica)  

Matèria de modalitat de 1r de batxillerat (en Cataluña no es una única materia "Física y Química", sino dos materias "Física I" y "Química I") 
[Física I (Bloc 1) ~ gener 2020 - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=103)  
[Física I (Bloc 2) ~ gener 2020 - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=104)  
[Química I (Bloc 1) ~ gener 2020 - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=136)  
[Química I (Bloc 2) ~ gener 2020 - educaciodigital.cat](https://educaciodigital.cat/ioc-batx/moodle/course/view.php?id=137)  

[Física en context 1r Batxillerat](https://sites.google.com/a/xtec.cat/fisicaencontext/f%C3%ADsica-en-context/1r-batxillerat)  

José Antonio Navarro  
[http://clasesfisicayquimica.blogspot.com.es/p/fisica-y-quimica-1-bachillerato.html](http://clasesfisicayquimica.blogspot.com.es/p/fisica-y-quimica-1-bachillerato)  

Vídeos 1º Bachillerato, ceroentropía [https://www.youtube.com/playlist?list=PLhJ3K3-ToQfrJ-CvX0Sy9mtAdtLTU0-Is](https://www.youtube.com/playlist?list=PLhJ3K3-ToQfrJ-CvX0Sy9mtAdtLTU0-Is)  

Vídeos Antonioprofe, 1º Bachillerato (16-17 años) [https://www.youtube.com/playlist?list=PL16zm8z0lIv0SmuUBxLQ5VqCB-PrXt8hP](https://www.youtube.com/playlist?list=PL16zm8z0lIv0SmuUBxLQ5VqCB-PrXt8hP)  

[http://currofisico.blogspot.com/p/fisica-y-quimica-1-bachillerato.html](http://currofisico.blogspot.com/p/fisica-y-quimica-1-bachillerato)   

[Mecánica de bachiller (I) - lawebdefisica.com](https://www.lawebdefisica.com/apuntsfis/bachiller1web/)  

##  Ejercicios
[Exámenes de Física y Química 1º de Bachillerato, Curso 2007-08. IES “REY FERNANDO VI” (pdf 19 páginas) jmillan](http://chopo.pntic.mec.es/jmillan/examenes_1.pdf)  

 [http://www.profesorjrc.es/1fyq.htm](http://www.profesorjrc.es/1fyq.htm)  
 Materiales por bloques cc-by-nc-sa  

 Problemas resueltos de física y química  
 [http://www.profesorparticulardefisicayquimica.es/#primerobachillerato](http://www.profesorparticulardefisicayquimica.es/#primerobachillerato)  
Profesor: A. Zaragoza López  

[http://fqsecundaria.blogspot.com.es/2015/01/relaciones-de-problemas-de-fisica-y.html](http://fqsecundaria.blogspot.com.es/2015/01/relaciones-de-problemas-de-fisica-y)  
 
 [Física 1º Bach. Problemas - Berto Tomás](https://drive.google.com/drive/folders/1zY09oa6BLDymLjhuZuKrfz5urwWew_vZ)  
 [Problemas Química 1º Bachillerato - Berto Tomás, Magdalena Homar Pons](https://drive.google.com/file/d/1_E6cVuBxyDmBam803B949yPKyHHajLXt/view)  
 [Aprendiendo Física con Berto Tomás](https://www.aprendiendofisicaconbertotomas.com/)  
 
 [Física y química 1 Bachillerato ejercicios resueltos - profesor10demates](https://www.profesor10demates.com/2013/09/fisica-y-quimica-1-bachiller-ejercicios.html)  


