
# Imágenes (Mapa de Bits)

Esta página es un poco forzada: surgió como requisito de un curso de publicar imágenes indicando su licenciamiento, al igual que una página separada sobre recursos de imágenes vectoriales.  

Intento enlazar imágenes para evitar problemas de licenciamiento, aunque algunas imágenes puntuales las almacen en [drive.fiquipedia images](https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/media/images)  

Recopilatorio de "mapas" de [Dominic Walliman](https://twitter.com/DominicWalliman), canal de youtube [Domain Of Science](https://www.youtube.com/channel/UCxqAWLTk1CmBvZFPzeZMd9A)

[Domain of Science - Dominic Walliman](https://www.flickr.com/photos/95869671@N08/with/50800281138)  
Además de Física, Química y Biología que enlazo en páginas, hay muchos más: matemáticas, física de partículas, ingeniería, computación cuántica, ...

![](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/media/images/ThisIsHowScientistsSeeTheWorld-AbstruseGoose.png "Imagen tomada de https://web.archive.org/web/20180905145408/https://abstrusegoose.com/275" )  

Las imágenes aisladas como recursos son una excepción: lo normal es encontrarlas integradas dentro de algún recurso más elaborado.  
O en bancos de imágenes  

Sí que puede haber colecciones de imágenes asociadas a alguna temática, como material de laboratorio, o temas de química   

[Wallpapers de química (I) - quimicafacil.net](https://quimicafacil.net/varios/wallpapers-de-quimica/)
[Wallpapers de química (II) - quimicafacil.net](https://quimicafacil.net/varios/wallpapers-de-quimica-ii/)
[Wallpapers de química (III) - quimicafacil.net](https://quimicafacil.net/varios/wallpapers-de-quimica-iii/)
[Wallpapers de química (IV) - quimicafacil.net](https://quimicafacil.net/varios/wallpapers-de-quimica-iV/)
[Wallpapers de química (V) - quimicafacil.net](https://quimicafacil.net/varios/wallpapers-de-quimica-v/)

A veces hay recopilatorios de infografías de temas variados, por ejemplo

[Science - visualcapitalist.com](https://www.visualcapitalist.com/category/misc/science-2/)  

[twitter AngleScience/status/1247561688196489216](https://twitter.com/AngleScience/status/1247561688196489216)  
Hoy vamos a hablar de los bancos de IMÁGENES   
[Science source](https://www.sciencesource.com/)  
[Visible Human Project](https://www.nlm.nih.gov/research/visible/visible_human.html)  
[Open I](https://openi.nlm.nih.gov/)  
[Cell Image Library](http://www.cellimagelibrary.org)  
[Pixabay](https://pixabay.com/es/)  
[Picjumbo](https://picjumbo.com/)  
[Unsplash](https://unsplash.com/es)  
[Lifeofpix](https://www.lifeofpix.com/)  
[Picsfree - Freeimages](https://www.freeimages.com/)  
[Pexels](https://www.pexels.com/es-es/)  
[Morguefile](https://morguefile.com/)  


 [Download Free Stock Photos, Images and Backgrounds](http://compfight.com/)   
 [Find your inspiration. | Flickr](https://www.flickr.com/)   
 [Free Vectors, Stock Photos & PSD Downloads | Freepik](http://www.freepik.com/)   
 [Morguefile.com free photographs for commercial use](http://www.morguefile.com/)   
 [New Old Stock](http://nos.twnsnd.co/)   
 [home :  open stock photography : 100% free open source stock photography](http://www.openstockphotography.org/)   
 [Free Stock Photos · Pexels](http://www.pexels.com/)   
 [Free Travel Stock Photos - Tourism images from everywhere around the World](http://photoeverywhere.co.uk/)   
 [picjumbo: Free Stock Photos](https://picjumbo.com/)   
 [http://pics.tech4learning.com/](http://pics.tech4learning.com/)   
 [Más de 1 millón de Imágenes Gratis para Descargar - Pixabay](https://pixabay.com/es/)   
 [Public Domain Pictures - Free Stock Photos](http://www.publicdomainpictures.net/)   
 [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)   
 [Millones de imágenes gratuitas para el aula](http://web.archive.org/web/20180409222430/http://www.xarxatic.com/millones-de-imagenes-gratuitas-para-el-aula/) (waybackmachine, no operativo en 2021)   


[twitter af_bertotomas/status/1415386156125020165](https://twitter.com/af_bertotomas/status/1415386156125020165)
He creado la carpeta ILUSTRACIONES. Para USO EXCLUSIVO en materiales EDUCATIVOS y GRATUITOS. Iré subiendo poco a poco ilustraciones genéricas sin números para que las adaptéis 
[carpeta ILUSTRACIONES dentro de esta carpeta](https://drive.google.com/drive/folders/1akqqtkjCwimxR618abh_NGmNa-i2cp7G)

[Chemistry images gallery](https://www.periodni.com/gallery/images.php)  Generalic, Eni. "Chemistry images gallery." EniG. Periodic Table of the Elements. KTF-Split, 22 Jan. 2021. Web. 15 Aug. 2021
  
Esta página se creó como ejercicio de cómo atribuir imágenes con su licenciamiento  
  
Science photographs from Fundamental photographs  
 [Science Photographs From Fundamental Photographs | Fundamental Photographs - The Art of Science](http://fphoto.photoshelter.com/gallery-list)   
© 2012 Fundamental Photographs - The Art of Science   
  
 [![](http://farm4.staticflickr.com/3487/3776988868_bd10760df8.jpg "") ](http://farm4.staticflickr.com/3487/3776988868_bd10760df8.jpg)   
Alfredo Louro ( [Alfredo Louro | Flickr](http://www.flickr.com/photos/alfredolouro) ), “Visualization of magnetic fields”,  [Visualization of magnetic fields (1) | A cross section of th… | Flickr](http://www.flickr.com/photos/alfredolouro/3776988868) ,Febrero 2007, [consultado 22 noviembre 2011]. Material bajo licencia  [ Creative Commons Reconocimiento-NoComercial-CompartirIgual 2.0 ](http://creativecommons.org/licenses/by-nc-sa/2.0/)   
  
 [![Regla de la mano derecha](http://farm6.staticflickr.com/5179/5452241888_71fff01a46.jpg "Regla de la mano derecha") ](http://farm6.staticflickr.com/5179/5452241888_71fff01a46.jpg)   
 Jesús Pereira ( [regla de la mano derecha | jesus pereira | Flickr](http://www.flickr.com/photos/chuchipi/5452241888/) ), Febrero 2011, [consultado 22 noviembre 2011]. Material bajo licencia  [Creative Commons Reconocimiento-NoComercial-SinObraDerivada 2.0 Genérica](http://creativecommons.org/licenses/by-nc-nd/2.0/)   
  
 [https://twitter.com/larotesmeyer/status/1383760315377086479](https://twitter.com/larotesmeyer/status/1383760315377086479)   
¡¡ Pero, pero, pero, pero... !!  
¡¡ Qué me había perdido yo esta maravilla !!  
Frases de reguetón ilustradas.  
Inlove total.  
@malamusa_ilust [![](https://pbs.twimg.com/media/EzQaOHcVUAEF2sc?format=jpg "") ](https://pbs.twimg.com/media/EzQaOHcVUAEF2sc?format=jpg)  
[![](https://pbs.twimg.com/media/EzQap-2UUAcNvOU?format=jpg "") ](https://pbs.twimg.com/media/EzQap-2UUAcNvOU?format=jpg)   


También puede haber imágenes asociadas a memes  
[memedroid.com memes química](https://es.memedroid.com/memes/tag/quimica/)    
