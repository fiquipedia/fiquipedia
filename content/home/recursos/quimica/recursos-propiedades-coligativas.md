
# Recursos propiedades coligativas

[http://www.ehu.es/biomoleculas/agua/coligativas.htm](http://www.ehu.es/biomoleculas/agua/coligativas.htm)  
JUAN MANUEL GONZÁLEZ MAÑAS, Universidad del País Vasco  

[http://apuntescientificos.org/coligativas-ibq2.html](http://apuntescientificos.org/coligativas-ibq2.html)  
©Derechos reservados "Apuntes Científicos"  

[http://www.chem.iastate.edu/group/Greenbowe/sections/projectfolder/flashfiles/propOfSoln/colligative.swf](http://www.chem.iastate.edu/group/Greenbowe/sections/projectfolder/flashfiles/propOfSoln/colligative.swf)  

[http://eugenio.naukas.com/2017/03/13/el-origen-escrito-de-la-palabra-osmosis/](http://eugenio.naukas.com/2017/03/13/el-origen-escrito-de-la-palabra-osmosis/)  

[Colligative Properties of Solutions. Problems and solutions - sparknotes.com](https://www.sparknotes.com/chemistry/solutions/colligative/problems/)  
