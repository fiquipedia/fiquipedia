# Recursos Física y Química 4º ESO

Aparte de lo indicado aquí, se pueden ver recursos en general de  [física](/home/recursos/fisica) , de  [química](/home/recursos/quimica) , o de temas concretos como  [cinemática](/home/recursos/fisica/cinematica) ,  [dinámica](/home/recursos/fisica/dinamica) ,  [energía](/home/recursos/energia) ,  [átomo](/home/recursos/quimica/recursos-estructura-del-atomo) ,  [enlace químico](/home/recursos/quimica/enlace-quimico) , [concepto de mol](/home/recursos/quimica/concepto-de-mol) , reacciones,...  
También en recursos de  [apuntes](/home/recursos/apuntes) , ...  
Página en borrador / elaboración

##  Apuntes
 
* [Apuntes 4º ESO - fisiquimicamente](https://fisiquimicamente.com/recursos-fisica-quimica/apuntes/4eso)  Rodrigo Alcaraz de la Osa  cc-by-sa
* [Apuntes 4º ESO - fisquiweb](http://fisquiweb.es/Apuntes/apun4.htm) Luis Ignacio García González   
* [Didactica Física y Química](http://didacticafisicaquimica.es/)   
 [Física y Química de 4º de ESO](http://didacticafisicaquimica.es/fisica-y-quimica-4o-eso/)   
 [Enunciados de problemas 4º ESO](http://didacticafisicaquimica.es/enunciados-problemas-4oeso/)  
* [4ºESO FyQ - profesorjrc.es](https://profesorjrc.es/4eso.htm) Jorge Rojo Carrascosa   
* [4ºESO FyQ - quifiblogeso](http://quifiblogeso.blogspot.com/search/label/4%C2%BA%20ESO) Inma Rodríguez  
* [4ºESO FyQ - elortegui.org](http://ciencia.elortegui.org/datos/4ESO/ESO4.htm)  

##   [Apuntes de elaboración propia Física y Química 4º ESO](/home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso/apuntes-fisica-y-quimica-4-eso-elaboracion-propia) 

##  Ejercicios de elaboración propia
Ver en  [pruebas libres de graduado ESO](/home/pruebas/pruebas-libres-graduado-eso)  y en  [pruebas acceso Grado Medio](/home/pruebas/pruebas-de-acceso-a-grado-medio)  

##  Recursos generales
Pueden combinar al mismo tiempo apuntes, ejercicios, simulaciones ...  
Curso completo online, apuntes y ejercicios  
* [Física y Química 4º ESO - CIDEAD](http://recursostic.educacion.es/secundaria/edad/4esofisicaquimica/index.htm)  cc-by-nc-sa  
Autores: Jesús M. Muñoz Calle, Luís Ramírez Vicente, Joaquín Recio Miñarro, José Luís San Emeterio Peña, Inmaculada Sevila Pascual, José Villasuso Gato  

[Proyecto Newton, Unidades didácticas 4º ESO](http://recursostic.educacion.es/newton/web/unidadescursos.php?pulsado=4)  
Incluye apuntes y problemas. cc-by-nc-sa  

[Física y Química 4º ESO - iesalandalus.com _waybackmachine_](http://web.archive.org/web/20190312085801/http://www.iesalandalus.com/joomla3/index.php?option=com_content&task=view&id=64&Itemid=72)  
José Antonio Navarro. Apuntes, ejercicios, exámenes    

[Recursos para Física de 2º de Bachillerato (y algo de 1º y **4º de ESO**). _waybackmachine_](http://web.archive.org/web/20180318124626/http://webs.ono.com/maitetono/)  
José Antonio S Noriega. IES Fernando I de Valencia de Don Juan.  

[Física y Química de 4º ESO - elortegui.org](http://www.elortegui.org/ciencia/datos/4ESO/ESO4.htm)  

[Física y Química 4º ESO - IES San Juan Bautista](https://www.educa2.madrid.org/web/sjb-fq/fisica-y-quimica4)  

[Física y Química 4º ESO - IES San Juan Bautista _waybackmachine_](http://web.archive.org/web/20160310170359/http://www.institutosanjuanbautista.com/moodleSJB/fyq.htm)
4ºESO - FÍSICA y QUÍMICA. Teoría / ejercicios y soluciones por bloques  
1. Cinemática  
2. Dinámica  
3. Energías  
4. Fluidos e Hidrodinámica  
5. Calorimetría  
6. El Átomo  
7. Enlaces Químicos  
8. Reacciones Químicas  
9. Formulación Inorgánica  
Diego E. Pérez-Olea / Dpto. Física y Química. IES San Juan Bautista   

[Física y Química 4ºESO - romanfyq _waybackmachine_](http://web.archive.org/web/20180727212510/https://romanfyq.wikispaces.com/F%C3%ADsica+y+Qu%C3%ADmica+de+4%C2%BA+ESO)  Apuntes y ejercicios. cc-by-sa   

IES Leopoldo Queipo de Melilla.  
[Departamento de Física y Química. IES Leopoldo Queipo. Melilla - fisicayquimicaqueipo.wixsite.com](https://fisicayquimicaqueipo.wixsite.com/misitio-1/apuntes-y-ejercicios)  Apuntes y ejercicios.   
Material Química 4º ESO, Salvador Molina Burgos. Varía según el curso y el momento del curso: apuntes, ejercicios, prácticas de laboratorio  

[Departamento de Física y Química. IES Leopoldo Queipo. Melilla. Verónica Fernández Martos - fisicayquimicaqueipo.wixsite.com](https://fisicayquimicaqueipo.wixsite.com/misitio-1/da-veronica-fernandez-martos)  

[4º ESO. Jesús Millán - pntic.mec.es](http://chopo.pntic.mec.es/jmillan/4_de_ESO.htm)  

[Cuarto ESO - quimiziencia.es](http://www.quimiziencia.es/cuartoeso)  Temas y ejercicios resueltos  

##  Ejercicios
[4º ESO - profesorparticulardefisicayquimica.es](http://www.profesorparticulardefisicayquimica.es/#cuartoeso)  
Problemas resueltos de Física y Química. Profesor: A. Zaragoza López  

[Física y química 4º ESO ejercicios y problemas resueltos - profesor10demates.blogspot.com.es](http://profesor10demates.blogspot.com.es/2013/08/fisica-y-quimica-4-eso-ejercicios-y)  

[4º ESO Física y Química - fisilandia.es](https://sites.google.com/view/fisilandia/4%C2%BA-de-eso-f%C3%ADsica-y-qu%C3%ADmica?authuser=0)  

[Relaciones de problemas de Física y Química (4º ESO) - fqsecundaria.blogspot.com.es](http://fqsecundaria.blogspot.com.es/2015/01/relaciones-de-problemas-de-fisica-y_21)  
Diego Palacios, curso 2015-2016


