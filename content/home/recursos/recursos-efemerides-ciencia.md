
# Recursos efémerides ciencia

La idea es recopilar aquí enlaces, de modo que cualquier día de clase se pueda citar una efeméride concreta  
Algunas son asociadas a nacimientos, descubrimientos, hitos en la cienciaOtros son más curiosos: el día del mol es el 23 de octubre (asociado a la fecha en inglés 10 23)El "día de pi" es el 14 de marzo (asociado a la fecha en inglés 3 14), también asociado al nacimiento de Einstein y la muerte de Hawking  
 [ya-puedes-descargar-el-calendario-cientifico-escolar-2020](https://www.csic.es/es/actualidad-del-csic/ya-puedes-descargar-el-calendario-cientifico-escolar-2020)  [Calendario científico escolar 2020 | Ciencia y más | Mujeres con ciencia](https://mujeresconciencia.com/2020/01/10/calendario-cientifico-escolar-2020/)   
  
Algunas recopilaciones [https://twitter.com/p_pandora/status/1207642778089467905](https://twitter.com/p_pandora/status/1207642778089467905)   
 [Cient&iacute;ficos](https://calendar.google.com/calendar/embed?src=c7lvvnd67devktjcmji020i16k%40group.calendar.google.com&ctz=America%2FSantiago)   
 [Efemerides](https://calendar.google.com/calendar/embed?src=5qs7pa700i3rhkhanf6hao3kfc%40group.calendar.google.com&ctz=America%2FSantiago)   
 
