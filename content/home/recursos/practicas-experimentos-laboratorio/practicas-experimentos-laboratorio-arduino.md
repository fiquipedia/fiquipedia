
# Prácticas / experimentos / laboratorio con arduino

Ver [prácticas con smartphone](/home/recursos/practicas-experimentos-laboratorio/practicas-experimentos-laboratorio-smartphone)  

Se podrían citar otras plataformas pero arduino es la más habitual y es abierta

Pueden enlazar con proyectos de tecnología donde haya una componente física, como puede ser el concurso CANSAT

[Qué es Arduino, cómo funciona y qué puedes hacer con uno - xataka.com](https://www.xataka.com/basics/que-arduino-como-funciona-que-puedes-hacer-uno)

[Un experimento basado en Arduino para estudiar el movimiento de caída libre en el aula](http://revistadefisica.es/index.php/ref/article/view/2641)  
Antonio Ángel Moya Molina  
RSEF abril 2020


[Prácticas de laboratorio de Física para alumnos de Bachillerato con Arduino](https://uvadoc.uva.es/bitstream/handle/10324/6980/TFM-G316.pdf?sequence=1&isAllowed=y)
TRABAJO FIN DE MASTER. Máster En Profesor De Educación Secundaria Obligatoria Y Bachillerato, Formación Profesional Y Enseñanzas De Idiomas. FÍSICA Y QUÍMICA. Daniel Sánchez Pérez. Universidad de Valladolid. Curso 2013-2014

>EXPERIMENTOS PROPUESTOS
>1. El Sol controla la temperatura y humedad durante el día  
>2. La compresión calienta el aire  
>3. Al disolver también se calienta  
>4. Cómo medir la altura de un piso con un barómetro  
>5. El fuego emite Infra-Rojos  
>6. Inducción magnética en un cable  
>7. Electromagnetismo para enviar información  
>8. Robots en el siglo XXI

