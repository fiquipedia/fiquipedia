### Apuntes de elaboración propia de Química de 2º de Bachillerato

Se pueden ver actividades / ejercicios asociadas a EvAU en [EvAU Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica). 

|  Bloque |  Apuntes | 
|:-:|:-:|
| Estequiometría |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/Q0-Estequiometr%C3%ADa-Teor%C3%ADa.pdf)  | 
| Estructura atómica / clasificación periódica elementos |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/Q2-EstructuraAt%c3%b3micaClasificaci%c3%b3nPeri%c3%b3dicaElementos-Teor%C3%ADa.pdf)  | 
| Enlace químico / propiedades sustancias |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/Q3-EnlaceQuimicoPropiedadesSustancias.pdf) <br>[tabla propiedades sustancias según enlace🔗](/home/recursos/quimica/recursos-propiedades-sustancias)  | 
| Transformaciones energéticas / espontaneidad reacciones (con LOMCE pasa a 1º Bachillerato) |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/Q4-TransformacionesEnerg%c3%a9ticasEspontaneidadReacciones-Teor%C3%ADa.pdf) <br> [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/QUI2B-Pizarras-QuimicaEspontaneidad.pdf) | 
| Equilibrio (incluye cinética) |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/Q5-Equilibrio-Teor%C3%ADa.pdf)  | 
| Ácidos y Bases |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/Q6-AcidosYBase-Teoria.pdf)  | 
| Electroquímica (incluye pilas y electrolisis) |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/Q7-PAU-Electroquimica-Teor%C3%ADa.pdf)  | 
| Química del Carbono |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/Q8-PAU-Qu%C3%ADmicaCarbono-Teor%C3%ADa.pdf) <br> [formulación orgánica🔗](/home/recursos/recursos-apuntes/recursos-apuntes-formulacion)<br> [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/quimica/isomeria/Isomeria.pdf "Isomería apuntes 2 caras")<br>[📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/QUI2B-Pizarras-Isomeria.pdf "Isomería pizarras 2º Bachillerato")<br>[isomería🔗](/home/recursos/quimica/isomeria)  | 
|  Resúmenes |  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/aPAUntes-Quimica.pdf)  [📖](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato/TiPAUlog%C3%ADaQu%C3%ADmicaOrg%C3%A1nica1Cara.pdf)  | 

Además de  [apuntes generales](/home/recursos/apuntes)  y los  [recursos propios de química de 2º de Bachillerato](/home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato) , coloco aquí los "apuntes" de elaboración propia. Intento que sean unas hojas muy breves de resumen que puedan tenerse como referencia básica a falta de libro de texto, y que se amplían durante la clase.

Los apuntes son por bloques, al igual que el currículo y que las soluciones de  [problemas PAU de química](/home/pruebas/pruebasaccesouniversidad/pau-quimica) . 

*En general intento revisar que se cubre el currículo oficial de Madrid, e intento reflejarlo indicando __subrayados__ los epígrafes / contenidos que aparecen en el currículo oficial.*

**Nota importante: Se agradecen comentarios sobre erratas y sobre todo, sugerencias de mejora (qué sobra, qué falta, qué poner de otra manera ...). Todos los documentos ponen en la cabecera la fecha de revisión y mail de contacto.**

En abril 2015 se completa una primera versión de todos los bloques, que anexo en la página como pdf.  

El bloque inicial de repaso que llamo "estequiometría" enlaza con contenidos generales de  [física y química de 1º de Bachillerato](/home/materias/bachillerato/fisicayquimica-1bachillerato/curriculo-fisica-y-quimica-1-bachillerato-lomce) , y los bloques de equilibrio químico y de ácido-base, hechos inicialmente en 2012 sin seguir el criterio de subrayado, se revisan en 2015. Al realizar el último bloque de química del carbono comienzo a usar códigos QR al tiempo que intento subrayar también teniendo en cuenta LOE y LOMCE. 

Con todos los bloques en una tabla el objetivo es que la la página tenga un aspecto similar a la página realizada anteriormente  [Apuntes elaboración propia Física 2º Bachillerato](/home/recursos/recursos-apuntes/apuntes-elaboracion-propia-fisica-2-bachillerato)  

En mayo de 2015 se sube la primera versión de aPAUntes química, intentando repetir la misma idea de "aPAUntes" hecha primero en física: un resumen de fórmulas/conceptos, que no apuntes, limitado a 2 caras de folio.

En octubre 2014 publico la primera versión de la tipología, que podría colocar aquí asociado a los apuntes, pero considero más relacionado con los  [problemas PAU de Química](/home/pruebas/pruebasaccesouniversidad/pau-quimica)  y los pongo con ellos, y en los que realiza una agrupación de problemas por tipo adicional a la separación en bloques.

Algunos bloques retoman contenidos vistos en cursos anteriores; se intenta dar una visión completa más orientada a 2º de Bachillerato  
- La estructura atómica y enlaces se introduce en 3º y se profundiza en 4º ESO y en 1º de Bachillerato.
- Termoquímica y cinética a veces se introduce en 4º ESO y 1º Bachillerato. 
- Formulación orgánica se introduce en 4º ESO y se profundiza en 1º Bachillerato
