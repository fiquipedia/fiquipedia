En este documento se incluye información sobre la migración desde Google Sites a GitLab pages realizada en 2021  
A nivel informativo se deja un fichero tree-content.txt con el contenido en /content tras la migración  
No se indica fecha de revisión en este documento ya que la refleja GitLab

# Tablas
Antes de la migración a GitLab pages usaba tablas en Google Sites, es una manera de organizar información que me gusta.  
Las tablas en Google Sites las editaba en HTML para controlar mejor el aspecto.  
Todas las migro a markdown, sin hacerlas en html  
- No uso rowspan ni colspan, que usaba en algunas  
- Sí uso saltos de línea dentro de tablas, pero sí que uso `<br>`, y añado en config.toml 
```
[markup]
[markup.goldmark] [markup.goldmark.renderer]
unsafe = true 
```
La migración la puedo gracias a la opción `--multiline` de gsites2md:  
    - Sin usar esa opción para tablas (como https://www.fiquipedia.es/home/recursos/recursos-para-oposiciones-problemas-por-comunidad/) 
    - Usando la opción para resto de páginas (algunas como los currículos con muchos saltos de línea dentro de texto) 
    
Un tema a tener en cuenta es que en el interior de las tablas, si se pone un enlace, no puede tener un espacio en blanco, ya que rompe la tabla. En algunos casos se enlaza poniendo %20, en otros se renombra el fichero 

## Redirecciones

Básicamente para redireccionar pdfs que se almacenan en drive.fiquipedia, pero también por cambio nombre páginas  
Las redirecciones están en `/public/_redirects`  
Una redirección inicial es `/home / 301` asociada a que en Google Sites /home y / tenían lo mismo. También se pone un enlace simbólico debido a cómo presenta las páginas el tema beautiful hugo usado inicialmente.  
Las redirecciones que no son de ficheros directamente deben terminar en /  

Agosto 2021: las redirecciones pdf (la mayoría) van ok así, pero otras no, y lo soluciono con aliases de hugo, añadiendo en el .md  
Por ejemplo en /home/pruebas/pruebasaccesouniversidad/paufisica.md añado y va ok
```
---
aliases:
  - /home/pruebasaccesouniversidad/paufisica/
---
```

### Redirecciones de pdfs
Al mover ficheros .pdf desde su ruta original a drive.fiquipedia, se redireccionan por si se accede con el enlace (puede estar en otra web, en Twitter ...)

* No se crea un fichero pdf que internamente indique en su contenido solo la nueva dirección en drive.fiquipedia (cada pdf serían unos 10 kB), aunque no habría que modificarlos nunca en [https://gitlab.com/fiquipedia/fiquipedia](https://gitlab.com/fiquipedia/fiquipedia) por lo que no gastarían tiempo de compilación salvo el tiempo asociado a moverlos en el despliegue tras la compilación, pero sería redirección manual.  

* No se puede redireccionar directamente en el fichero pdf cambiando su contenido y haciendo que su contenido tenga un html con la redirección: al tener extensión .pdf el navegador lo procesa como pdf.

* GitLab Pages permite redireccionar https://docs.gitlab.com/ee/user/project/pages/redirects.html pero dentro del mismo repositorio: no se puede enlazar a drive.fiquipedia desde fiquipedia.gitlab.io

* Finalmente lo que se hace es crear una redirección en /public/_redirects a un fichero .html, y que sea el .html el que redireccione  
No parece poder usarse * y splat ya que cambia a partir de una ruta, pero no permite añadir .html al final  
https://docs.netlify.com/routing/redirects/redirect-options/  

Se ejecuta manualmente, revisando, va modificando `_RUTA_` en estos scripts  
La ruta empieza sin / y termina en /  
Ejemplo `home/recursos/recursos-para-oposiciones/`  
Los scripts que se ejecutan manualmente son  
```
for i in `ls *.pdf`; do echo "/_RUTA_$i /_RUTA_$i.html 301" >>redirectsparcial.txt; done  

for i in `ls *.pdf`; do echo "<html><head><meta HTTP-EQUIV=\"""REFRESH\""" content=\"""0; url=https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/_RUTA_"$i"\"></head><body></body></html>"  > $i.html ; done  
```

Las redirecciones funcionan en Twitter, por ejemplo 
https://twitter.com/FiQuiPedia/status/700827390369398785
    https://t.co/0Dm6uHamRY?amp=1
        http://www.fiquipedia.es/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato/ProblemaGravitacion2.pdf
            http://www.fiquipedia.es/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato/ProblemaGravitacion2.pdf.html
                https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/content/home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato/ProblemaGravitacion2.pdf

Sin embargo visto que en aulas virtuales Moodle (EducaMadrid) desde la app, aunque funcionan los enlaces a pdfs si se abren con navegador, desde la app abriendo con un visor de pdf indica que el fichero no es un pdf.        

Revisiones:
1. Hay ficheros que tienen espacios en su nombre (nombres antiguos que ya enlazados desde algún sitio que no se pueden modificar, en los nuevos se intentan evitar espacios). Hay que revisar porque los scripts anteriores no lo contemplan, son pocos casos.   
Ejemplos  
"Q6-PAU-AcidosYBases -soluc.pdf"  
"Q1B-6-Composición centesimal, fórmula empírica y molecular.pdf"  
Estos casos se deben poner entre comillas en _redirects (el espacio asumiría se pasa a indicar la segunda ruta) 

Agosto 2024: detecto que las redirecciones de .html al .pdf dejan de funcionar en la web, aunque el fichero .html el local sí que redirecciona bien al pdf.  
Quizá sea un tema de algún cambio en GitLab  
[.html redirect not working](https://gitlab.com/gitlab-com/content-sites/handbook/-/issues/128)  
[Gitlab pages redirects do not work](https://forum.gitlab.com/t/gitlab-pages-redirects-do-not-work/100235/4)  
Veo que tengo una primera línea de redirección '/home/ / 301' y que poniendo la página /home/ en la web me lleva a un pdf concreto!? Pruebo a eliminar esa línea pero no lo soluciona. Coincidiendo con el abandono total de goog.gl planteo cambiar los enlaces la ruta en gitlab drive que sí es "definitiva", y dar por perdidos algunos enlaces a pdf antiguos, que al menos funcionaron desde la migración de 2021 hasta algún momento antes de agosto 2024.  

### Redirecciones por cambio nombre páginas
Erratas / nombres puestos por Google Sites que evitaba tildes (peridica vs periodica, pginas vs paginas)  
Evitar varios guiones en el nombre, que a veces da errores (matematico---pmar vs matematico-pmar)  
```
/home/materias/eso/ambito-cientifico-matematico---pmar/ /home/recursos/eso/ambito-cientifico-matematico-pmar 301
/home/recursos/cursos-online--interactivos/ /home/recursos/cursos-online-interactivos 301
/home/recursos/pginas--blogs-personales/ /home/recursos/paginas-blogs-personales 301
/home/recursos/quimica/Equilibrio-quimico/ /home/recursos/quimica/equilibrio-quimico 301
/home/recursos/quimica/recursos-estructura-del-tomo/ /home/recursos/quimica/recursos-estructura-del-atomo 301
/home/recursos/quimica/recursos-tabla-peridica/ /home/recursos/quimica/recursos-tabla-periodica 301
/home/recursos/recuros-de-actividades-fuera-del-centro/ /home/recursos/recursos-de-actividades-fuera-del-centro 301 
/home/recursos/recursos-por-materia-curso/recursos-ampliacin-fsica-y-qumica-4-eso/ /home/recursos/recursos-por-materia-curso/recursos-ampliacion-fisica-y-quimica-4-eso 301	
```

### Redirecciones por reorganización
No se reorganizan los ficheros .pdf en drive.fiquipedia para mantener su ruta original, aunque hay que tenerlo en cuenta en las nuevas ubicaciones de los .html intermedios que sí están en carpetas reorganizadas  
```
/home/pruebasaccesouniversidad/ /home/pruebas/pruebasaccesouniversidad 301
/home/pruebasaccesouniversidad/pau-electrotecnia/ /home/pruebas/pruebasaccesouniversidad/pau-electrotecnia 301
/home/pruebasaccesouniversidad/paufisica/ /home/pruebas/pruebasaccesouniversidad/paufisica 301
/home/pruebasaccesouniversidad/pau-quimica/ /home/pruebas/pruebasaccesouniversidad/pau-quimica 301
/home/pruebasaccesouniversidad/pau-mecanica/ /home/pruebas/pruebasaccesouniversidad/pau-mecanica 301
/home/pruebas-de-acceso-a-grado-medio/ /home/pruebas/pruebas-de-acceso-a-grado-medio 301
/home/pruebas-de-acceso-a-grado-medio/normativa-y-curriculo-pruebas-acceso-grado-medio/ /home/pruebas-de-acceso-a-grado-medio/normativa-y-curriculo-pruebas-acceso-grado-medio 301
/home/pruebas-libres-graduado-eso/ /home/pruebas/pruebas-libres-graduado-eso 301
/home/pruebas-libres-graduado-eso/normativa-y-curriculo-pruebas-libres-graduado-eso/ /home/pruebas/pruebas-libres-graduado-eso/normativa-y-curriculo-pruebas-libres-graduado-eso 301
/home/pruebas-premios-extraordinarios/ /home/pruebas/pruebas-premios-extraordinarios 301
/home/materias/eso/fisica-y-quimica-2-eso/recursos-fisica-y-quimica-2-eso/ /home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso 301
/home/materias/eso/cultura-cientifica-4-eso/recursos-cultura-cientifica-4-eso/ /home/recursos/recursos-por-materia-curso/recursos-cultura-cientifica-4-eso 301
/home/materias/eso/curriculo-cultura-cientifica-4-eso/recursos-cultura-cientifica-4-eso/ /home/recursos/recursos-por-materia-curso/recursos-cultura-cientifica-4-eso 301
```

## Migración de páginas con nombres iguales a nombres de directorios
Dado que wget -R se encuentra directorios y ficheros con mismo nombre, no descarga ambos, solo los directorios.
Hay descargar ficheros html de esas páginas con su nombre original, no con nombre index.html colgando del directorio (por ejemplo /home/materias y no /home/materias/index.html)  

Indico los casos que coinciden con directorios

Se indican rutas relativas sin comenzar por /, un ejemplo aproximado de descarga es
`wget _SERVIDOR_/_RUTA_RELATIVA_ ./_RUTA_RELATIVA_[.html]`
El .html permitiría descargarlo en la misma estructura que donde se ha hecho el wget -R y ya existe directorio
El servidor era http://www.fiquipedia.es/ en julio 2021 antes de redirección, luego pasa a ser https://sites.google.com/site/fiquipediabackup05mar2018/ (hasta que Google Sites lo apague)

```
#Nivel 0 (Colgando de /)
home
#Nivel 1 (Colgando de /home)
home/materias
home/legislacion-educativa
home/pruebasaccesouniversidad
home/pruebas-de-acceso-a-grado-medio
home/pruebas-libres-graduado-eso
home/recursos
#Nivel 2 (Colgando de /home/*/)
home/legislacion-educativa/evaluaciones-finales
home/materias/bachillerato
home/materias/eso
home/materias/fpbasica
home/recursos/calor
home/recursos/docencia-contenidos-fisica-y-quimica-por-nivel
home/recursos/ejercicios
home/recursos/energia
home/recursos/examenes
home/recursos/fisica
home/recursos/libros
home/recursos/practicas-experimentos-laboratorio
home/recursos/proyectos-de-investigacion
home/recursos/quimica
home/recursos/recursos-adaptaciones-curriculares-fisica-quimica
home/recursos/recursos-apuntes
home/recursos/recursos-de-elaboracion-propia
home/recursos/recursos-espectro
home/recursos/recursos-matematicas
home/recursos/recursos-notacion-cientifica
home/recursos/recursos-para-oposiciones
home/recursos/recursospau
home/recursos/recursos-por-materia-curso
home/recursos/simulaciones

#Nivel 3 (/home/*/*/)
home/materias/bachillerato/cienciasparaelmundocontemporaneo-1bachillerato
home/materias/bachillerato/cultura-cientifica-1-bachillerato
home/materias/bachillerato/electrotecnia-2bachillerato
home/materias/bachillerato/fisica-2bachillerato
home/materias/bachillerato/fisicayquimica-1bachillerato
home/materias/bachillerato/quimica-2bachillerato
home/materias/eso/ambitocientificotecnologico-diversificacion
home/materias/eso/ampliacion-fisica-y-quimica-4-eso
home/materias/eso/ciencias-aplicadas-a-la-actividad-profesional-4-eso
home/materias/eso/cultura-cientifica-4-eso
home/materias/eso/fisica-y-quimica-2-eso
home/materias/eso/fisica-y-quimica-3-eso
home/materias/eso/fisicayquimica-4eso
home/materias/fpbasica/moduloprofesionalcienciasaplicadasi3009
home/recursos/ejercicios/ejercicios-elaboracion-propia-fisica-2-bachillerato
home/recursos/fisica/cinematica
home/recursos/fisica/fisica-de-particulas
home/recursos/fisica/mecanica
home/recursos/fisica/olimpiadas-de-fisica
home/recursos/fisica/optica-geometrica
home/recursos/fisica/recursos-dinamica
home/recursos/fisica/recursos-fisica-cuantica
home/recursos/fisica/recursos-fuerzas-en-fluidos
home/recursos/recursos-espectro/espectroscopio-casero
home/recursos/recursos-matematicas/calculo-vectorial
home/recursos/quimica/enlace-quimico
home/recursos/quimica/estados-de-oxidacion
home/recursos/quimica/formulacion
home/recursos/quimica/isomeria
home/recursos/quimica/olimpiadas-quimica
home/recursos/quimica/recursos-ajuste-reacciones-quimicas
home/recursos/quimica/recursos-configuracion-electronica
home/recursos/quimica/recursos-disoluciones
home/recursos/quimica/recursos-estructura-del-atomo
home/recursos/quimica/recursos-propiedades-sustancias
home/recursos/quimica/recursos-tabla-periodica
home/recursos/quimica/termoquimica
home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso
home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato
home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso
home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso
home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato

#Nivel 4 (/home/*/*/*/)
home/recursos/recursos-por-materia-curso/recursos-fisica-quimica-4-eso/apuntes-fisica-y-quimica-4-eso-elaboracion-propia
home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-1-bachillerato/apuntes-elaboracion-propia-1-bachilerato
home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-2-eso/apuntes-elaboracion-propia-fisica-y-quimica-2-eso
home/recursos/recursos-por-materia-curso/recursos-fisica-y-quimica-3-eso/apuntes-elaboracion-propia-3-eso
home/recursos/recursos-por-materia-curso/recursos-quimica-2-bachillerato/apuntes-de-elaboracion-propia-de-quimica-de-2-de-bachillerato
```

## Limpieza enlaces
Por temas internos de Google Sites, había enlaces sin texto del estilo `[](goog_131140452)`  que es eliminan todos  
Había algún enlace directo a https://sites.google.com/site/fiquipediabackup05mar2018 e incluso https://sites.google.com/site/fiquipediabackup06may2016  

## Imágenes
En general se intentan tener pocas imágenes almacenadas en la página, se suelen enlazar.  
Las pocas imágenes pasan a estar en https://gitlab.com/fiquipedia/drive.fiquipedia/ dentro de /content/images donde hay un [README-images](https://gitlab.com/fiquipedia/drive.fiquipedia/-/blob/main/content/images/README-images.md)  
https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/content/images  
En algunos casos se sustituyen imágenes de documentos (iconos de pdf, documento, escaneado), por iconos unicode y se comenta allí  

## Subíndices y superíndices  
No los permite directamente markdown, pero son muy necesarios en fórmulas químicas.  
Finalmente se usa `<sub>` HTML, para lo que hay que activar parámetro en config.toml  
De momento no se usa katex ya que no se incluían fórmulas, aunque se prueba katex para subíndices https://pandoc.org/MANUAL.html#math : funciona rodeando $$ pero no inline con $.  
https://shreyas4991.github.io/blog/markdown-math-2/)   


