# Ámbitos

Creo una página para recopilar ideas  
Los ámbitos son algo común a ESO, FP Básica, Adultos ...   

* [Ámbito Científico Tecnológico - Diversificación (LOE y LOMLOE) ](/home/materias/eso/ambitocientificotecnologico-diversificacion)  (paralelos a 3º y 4º ESO, LOE y LOMLOE)  
* [Ámbito Científico Matemático - PMAR (LOMCE)](/home/materias/eso/ambito-cientifico-matematico-pmar) (paralelos a 2º y 3º ESO) 
* [Módulo Profesional "Ciencias Aplicadas I", Código 3009 FP Básica](/home/materias/fpbasica/moduloprofesionalcienciasaplicadasi3009)

## Recursos
Se pueden mirar los asociados a Diversificación, PMAR, FP Básica y adultos  

[Unidades didácticas educación secundaria para personas adultas (castellano) - xunta.gal](http://www.edu.xunta.gal/portal/node/7453)  

Educación Secundaria para Personas Adultas a Distancia (ESPAD)
[ESPAD - educacionyfp.gob.es](https://www.educacionyfp.gob.es/mc/cidead/centro-integrado/etapas-educativas/espad.html)  

[ESPAD - educa.madrid.org](https://recursostic.educa2.madrid.org/espad)   
[ESPAD Nivel I: Ámbitos - educa.madrid.org](https://recursostic.educa2.madrid.org/espad-nivel1)  
[ESPAD Nivel II: Ámbitos - educa.madrid.org](https://www.educa2.madrid.org/web/recursostic/espad-nivel2)  

[Temario de Educación Secundaria para personas adultas a distancia (ESPAD) - educa.jccm.es](https://www.educa.jccm.es/es/estperadult/estudiar-epa/ensenanzas-conducentes-titulacion/educacion-secundaria-personas-adultas-presencial-distancia-/educacion-secundaria-personas-adultas-distancia-espad/acceso-espa-distancia-ordinaria-virtual/temario-educacion-secundaria-personas-adultas-distancia-esp)  

Educación Permanente de Adultos (EPA)
[EPA Ámbito Científico - Tecnológico - educa.jccm.es](https://www.educa.jccm.es/es/estperadult/estudiar-epa/ensenanzas-conducentes-titulacion/educacion-secundaria-personas-adultas-presencial-distancia-/educacion-secundaria-personas-adultas-distancia-espad/acceso-espa-distancia-ordinaria-virtual/temario-educacion-secundaria-personas-adultas-distancia-esp/ambito-cientifico-tecnologico)  

## Referencias
En inglés los ámbitos se citan como "Curriculum integration" y se suelen relacionar con [STEM](/home/recursos/stem)  

[Action in Teacher Education, Volume 18, Issue 1 (1996). Curriculum Integration and Teacher Education](https://www.tandfonline.com/toc/uate20/18/1)  

[The Integrated Curriculum: A Reality Check. Paul S. George](https://www.tandfonline.com/doi/abs/10.1080/00940771.1996.11496183)  

[Research Supporting Integrated Curriculum: Evidence for using this Method of Instruction in Public School Classrooms. Kevin C. Costley, Ph.D. Associate Professor of Early Childhood Education. Arkansas Tech University. Date of Publication: February 1, 2015](https://files.eric.ed.gov/fulltext/ED552916.pdf)  

[twitter jrfercuen/status/1528028227008094208/](https://twitter.com/jrfercuen/status/1528028227008094208/)  
Cita   
Trezise Kathleen 1996 An integrated curriculum in mathematics: an investigation of student achievement  
Paul S. George 1996 The Integrated Curriculum: A Reality Check.  

## La polémica de la imposición de ámbitos en Comunidad Valenciana

En 2020 en Comunidad Valencia se imponen como obligatorios los ámbitos   
[Mil docentes contra la imposición de la educación por ámbitos - elmundo.es 24 mayo 2021](https://www.elmundo.es/comunidad-valenciana/castellon/2021/05/24/60abb0cc21efa0ec208b4576.html)  
[DOTACIÓN PARA RETIRAR LA OBLIGATORIEDAD DE LOS ÁMBITOS EN EDUCACIÓN SECUNDARIA - gvaparticipa 30 mayo 2021](https://gvaparticipa.gva.es/budgets/1/investments/725)  
[Trabajo por ámbitos: el destrozo educativo de la Comunitat Valenciana - eldiariodelaeducacion 25 noviembre 2021](https://eldiariodelaeducacion.com/2021/11/25/trabajo-por-ambitos-el-destrozo-educativo-de-la-comunitat-valenciana/)  
Juan Quílez Pardo  
> No existe ninguna evidencia que apoye la organización de las materias de la secundaria obligatoria en ámbitos de conocimiento. Más bien, al contrario. Diferentes estudios hablan de la pérdida de conocimiento por parte del alumnado cuando se realiza este tipo de organización curricular.  

[La imposición de los ámbitos - lavanguardia 10 febrero 2022](https://www.lavanguardia.com/vida/20220210/8046354/imposicion-ambitos.html)  

[Los jueces suspenden la enseñanza por ámbitos en primero de la ESO en la Comunidad Valenciana - elpais 18 agosto 2022](https://elpais.com/educacion/2022-08-18/los-jueces-trastocan-la-organizacion-escolar-valenciana-paralizan-por-precipitada-una-medida-que-lleva-en-vigor-dos-cursos.html)  

[El TSJCV anula la obligación de la enseñanza por ámbitos y de los proyectos interdisciplinarios en la ESO - europapress.es 10 julio 2023](https://www.europapress.es/comunitat-valenciana/noticia-tsjcv-anula-obligacion-ensenanza-ambitos-proyectos-interdisciplinarios-eso-20230710173513.html)  

## Opiniones relacionadas con ámbitos y Física y Química

[twitter jago2019jose/status/1523247316571934720](https://twitter.com/jago2019jose/status/1523247316571934720)  
Soy profesor de FyQ. Estoy acuerdo con los compañeros de ByG en que la posible pérdida de la 3ª hora en 3°ESO en Madrid es lamentable pero, ¿alguien me puede explicar a qué viene esa petición de recuperar las antigua estructura de CCNN en 1° y 2°ESO? ¿Qué se busca con ello?(hilo)  
Con las estructura actual, las seis horas lectivas de 1º y 2º ESO se reparten por igual entre las 4 Ciencias Naturales que se imparten en estos niveles educativos: Biología, Geología, Física y Química (en dos materias). Y están en manos de los especialistas de dichas materias   
Me resulta difícil creer que nadie pretenda volver (salvo por interés espurio) a la situación de hace una década en la que, en la mayoría de centros, CCNN de 1° y 2° ESO estaba en manos del departamento de ByG y FyQ estaba demasiadas veces arrinconada al 3º trimestre de 2°ESO  
En el caso de no fuera sí, y el 50% de aquellas seis horas lectivas semanales de CCNN en 1º y 2ºESO se destinaran a FyQ, obligaba a docentes no especializados a explicar conceptos fundamentales de  ciencias en la que no eran especialistas. Y se notaba. Y mucho  
Siempre me he posicionado en contra de los ámbitos educativos porque, en general, me parece que suponen, por un lado, una pérdida de calidad de la enseñanza y, por otro, un desperdicio de la especialización docente (conseguida tras estudios universitarios)  
Por lo tanto, siempre estuve en contra de ese "ámbito encubierto" de ciencias que suponía aquella materia de CCNN en 1º y 2ºESO y por eso ahora no entiendo que se abogue por su recuperación por parte de compañeros de ByG  (en su justa lucha para que su materia no pierda horas).  

